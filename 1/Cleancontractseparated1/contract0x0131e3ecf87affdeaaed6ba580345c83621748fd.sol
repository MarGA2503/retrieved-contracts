// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

/**
 * @dev Provides information about the current execution context, including the
 * sender of the transaction and its data. While these are generally available
 * via msg.sender and msg.data, they should not be accessed in such a direct
 * manner, since when dealing with meta-transactions the account sending and
 * paying for execution may not be the actual sender (as far as an application
 * is concerned).
 *
 * This contract is only required for intermediate, library-like contracts.
 */
abstract contract Context {
    function _msgSender() internal view virtual returns (address) {
        return msg.sender;
    }

    function _msgData() internal view virtual returns (bytes calldata) {
        return msg.data;
    }
}"},"ConvexStrategist.sol":{"content":"// SPDX-License-Identifier: MIT
pragma solidity ^0.8.3;

import \"./IERC20.sol\";
import \"./IBooster.sol\";
import \"./IConvexPooL.sol\";
import \"./ICurvePooLSize2.sol\";
import \"./ICurvePooLSize3.sol\";
import \"./ICurvePooLSize4.sol\";
import \"./ICurveRegistry.sol\";
import \"./Ownable.sol\";

contract ConvexStrategist is Ownable {
    
    mapping(address =\u003e bool) public whitelist;
    address constant crv = 0xD533a949740bb3306d119CC777fa900bA034cd52;
    address constant cvx = 0x4e3FBD56CD56c3e72c1403e103b45Db9da5B9D2B;
    address constant curveRegistry = 0x90E00ACe148ca3b23Ac1bC8C240C2a7Dd9c2d7f5;
    address constant cvxBooster = 0xF403C135812408BFbE8713b5A23a04b3D48AAE31;

    /**
     * (see depositAndStakeFor() function.)
     * @param amount            : amount of _token deposited
     * @param nCoins            : Number of coins inside Curve PooL
     * @param indexInAmounts    : Index of the coin deposited inside the amounts array
     * @param inner             : used to decide where are located the tokens
     * @param token             : the ERC20 token deposited in Curve
     * @param curvePooL         : curve pool associated to _token
     * @param convexPooL        : convex pool associated to _token
     * @param curveLPToken      : the liquidity provider token for the current curvePooL
     */
    struct Deposit {
        uint256 amount;
        uint256 nCoins;
        uint256 indexInAmounts;
        bool inner;
        address token;
        address curvePooL;
        address convexPooL;
        address curveLPToken;
    }

    /**
     * (see harvestAndDeposit() function.)
     * @param crvSwapTxData  : paraswap buildTx data to swap tokens within this contract
     * @param cvxSwapTxData  : paraswap buildTx data to swap tokens within this contract
     * @param tokenTo        : destination token swap
     * @param curvePooL      : any curve pool
     * @param convexPooL     : any convex pool
     * @param curveLPToken   : Curve LP Token
     * @param crvAmount      : crv amount to be harvested
     * @param cvxAmount      : cvx amount to be harvested
     * @param nCoins         : Number of coins inside Curve PooL
     * @param indexInAmounts : Index of the coin deposited inside the amounts array
     * @param crvDex         : chosen dex to swap crv
     * @param cvxDex         : chosen dex to swap cvx
     */
    struct Harvest {
        bytes crvSwapTxData;
        bytes cvxSwapTxData;
        address tokenTo;
        address curvePooL;
        address convexPooL;
        address curveLPToken;
        uint256 crvAmount;
        uint256 cvxAmount;
        uint256 nCoins;
        uint256 indexInAmounts;
        address crvDex;
        address cvxDex;
    }

    constructor(address[] memory _whiteListed, address[] memory _approved) {
        for (uint256 i = 0; i \u003c _whiteListed.length; i++) {
            whitelist[_whiteListed[i]] = true;
        }
        for (uint256 i = 0; i \u003c _approved.length; i++) {
            IERC20(crv).approve(_approved[i], type(uint256).max);
            IERC20(cvx).approve(_approved[i], type(uint256).max);
        }
    }

    modifier onlyWhitelisted(address dex) {
        require(isWhitelisted(dex), \"!whitelist\");
        _;
    }

    function addToWhiteList(address _address) external onlyOwner {
        whitelist[_address] = true;
    }

    function removeFromWhiteList(address _address) external onlyOwner {
        delete whitelist[_address];
    }

    function isWhitelisted(address _address) public view returns (bool) {
        return whitelist[_address];
    }

    function setApprovals(
        address _token,
        address _spender,
        uint256 _amount
    ) external onlyOwner {
        IERC20(_token).approve(_spender, _amount);
    }

    /**
     * Withdraw any ERC20
     * @param _to     : recipient
     * @param _token  : token to withdraw
     * @param _amount : amount to withdraw
     */

    function withdrawERC20(
        address _to,
        address _token,
        uint256 _amount
    ) external onlyOwner {
        IERC20(_token).transfer(_to, _amount);
    }

    /**
     * Handle deposit in multiple Curve pool according to the token number within the pool.
     * @param  _curvePooL       : curve pool were we deposit token
     * @param  _amount          : amount of token to deposit
     * @param _curveLPToken     : the liquidity provider token for the current _curvePooL
     * @param _nCoins           : number of tokens in the current _curvePooL
     * @param _indexInAmounts   : index of the token in the _curvePooL token list
     * @return minted           : the minted amount returned
     */
    function addCustomLiquidity(
        address _curvePooL,
        uint256 _amount,
        address _curveLPToken,
        uint256 _nCoins,
        uint256 _indexInAmounts
    ) internal returns (uint256 minted) {
        uint256 minMintAmout;
        uint256 initialBalance = IERC20(_curveLPToken).balanceOf(address(this));
        require(_amount \u003e 0, \"!addCustomLiquidity: _amount \u003e 0\");
        if (_nCoins == 2) {
            uint256[2] memory amounts;
            amounts[_indexInAmounts] = _amount;
            minMintAmout = 0;
            ICurvePooLSize2(_curvePooL).add_liquidity(amounts, minMintAmout);
        } else if (_nCoins == 3) {
            uint256[3] memory amounts;
            amounts[_indexInAmounts] = _amount;
            minMintAmout = 0;
            ICurvePooLSize3(_curvePooL).add_liquidity(amounts, minMintAmout);
        } else if (_nCoins == 4) {
            uint256[4] memory amounts;
            amounts[_indexInAmounts] = _amount;
            minMintAmout = 0;
            ICurvePooLSize4(_curvePooL).add_liquidity(amounts, minMintAmout);
        } else revert(\"!addCustomLiquidity\");
        uint256 finalBalance = IERC20(_curveLPToken).balanceOf(address(this));
        return (finalBalance - initialBalance);
    }

    /**
     * Deposit \u0026 stake _token on msg.sender behalf, in Curve \u0026 Convex PooLs.
     * @param params    : see the Deposit struct.
     * @return          : the return value of stakeFor() from Convex PooL
     */
    function depositAndStakeFor(Deposit memory params) public returns (bool) {
        if (!params.inner) require(IERC20(params.token).transferFrom(msg.sender, address(this), params.amount), \"!transferFrom\");
        IERC20(params.token).approve(params.curvePooL, params.amount);
        uint256 minted = addCustomLiquidity(params.curvePooL, params.amount, params.curveLPToken, params.nCoins, params.indexInAmounts);
        address lpToken = ICurveRegistry(curveRegistry).get_lp_token(params.curvePooL);
        require(IERC20(lpToken).approve(cvxBooster, minted), \"!approve\");
        IBooster(cvxBooster).deposit(IConvexPooL(params.convexPooL).pid(), minted, false);
        address stakingToken = IConvexPooL(params.convexPooL).stakingToken();
        require(IERC20(stakingToken).approve(params.convexPooL, minted), \"!approve\");
        return IConvexPooL(params.convexPooL).stakeFor(msg.sender, minted);
    }

    /**
     * Perform a swap that has been build off chain.
     * @param  _tokenFrom   : token to swap
     * @param  _amountIn    : amount of _tokenFrom to swap
     * @param  _data        : swap data to be executed
     * @param  _dex         : dex address used to swap
     */
    function swapReward(
        address _tokenFrom,
        uint256 _amountIn,
        address _dex,
        bytes memory _data
    ) private onlyWhitelisted(_dex) {
        IERC20(_tokenFrom).transferFrom(msg.sender, address(this), _amountIn);
        (bool success, ) = _dex.call(_data);
        require(success, _tokenFrom == crv ? \"!swap crv\" : \"!swap cvx\");
    }

    /**
     * Harvest a defined amount of CRV and CVX, then dump them to increase the actual deposit
     * @param params         : see the Harvest struct.
     */
    function harvestAndDeposit(Harvest memory params) external returns (bool) {
        uint256 initial = IERC20(params.tokenTo).balanceOf(address(this));
        require(IConvexPooL(params.convexPooL).getReward(msg.sender, true), \"!getReward\");
        swapReward(crv, params.crvAmount, params.crvDex, params.crvSwapTxData);
        swapReward(cvx, params.cvxAmount, params.cvxDex, params.cvxSwapTxData);
        uint256 harvested = IERC20(params.tokenTo).balanceOf(address(this)) - initial;
        return
            depositAndStakeFor(
                Deposit(
                    harvested,
                    params.nCoins,
                    params.indexInAmounts,
                    true,
                    params.tokenTo,
                    params.curvePooL,
                    params.convexPooL,
                    params.curveLPToken
                )
            );
    }

    fallback() external payable {
        revert(\"!fallback\");
    }

    receive() external payable {
        revert(\"!receive\");
    }
}
"},"IAugustusSwapper.sol":{"content":"// SPDX-License-Identifier: UNLICENSED
pragma solidity \u003e=0.7.0 \u003c0.8.4;
pragma experimental ABIEncoderV2;

interface IAugustusSwapper {
    /**
   * @param fromToken Address of the source token
   * @param fromAmount Amount of source tokens to be swapped
   * @param toAmount Minimum destination token amount expected out of this swap
   * @param expectedAmount Expected amount of destination tokens without slippage
   * @param beneficiary Beneficiary address
   * 0 then 100% will be transferred to beneficiary. Pass 10000 for 100%
   * @param referrer referral id
   * @param useReduxToken whether to use redux token or not
   * @param path Route to be taken for this swap to take place

   */
    struct SellData {
        address fromToken;
        uint256 fromAmount;
        uint256 toAmount;
        uint256 expectedAmount;
        address payable beneficiary;
        string referrer;
        bool useReduxToken;
        Path[] path;
    }

    struct MegaSwapSellData {
        address fromToken;
        uint256 fromAmount;
        uint256 toAmount;
        uint256 expectedAmount;
        address payable beneficiary;
        string referrer;
        bool useReduxToken;
        MegaSwapPath[] path;
    }

    struct BuyData {
        address fromToken;
        address toToken;
        uint256 fromAmount;
        uint256 toAmount;
        address payable beneficiary;
        string referrer;
        bool useReduxToken;
        BuyRoute[] route;
    }

    struct Route {
        address payable exchange;
        address targetExchange;
        uint256 percent;
        bytes payload;
        uint256 networkFee; //Network fee is associated with 0xv3 trades
    }

    struct MegaSwapPath {
        uint256 fromAmountPercent;
        Path[] path;
    }

    struct Path {
        address to;
        uint256 totalNetworkFee; //Network fee is associated with 0xv3 trades
        Route[] routes;
    }

    struct BuyRoute {
        address payable exchange;
        address targetExchange;
        uint256 fromAmount;
        uint256 toAmount;
        bytes payload;
        uint256 networkFee; //Network fee is associated with 0xv3 trades
    }

    function getPartnerRegistry() external view returns (address);

    function getWhitelistAddress() external view returns (address);

    function getFeeWallet() external view returns (address);

    function getTokenTransferProxy() external view returns (address);

    function getUniswapProxy() external view returns (address);

    function getVersion() external view returns (string memory);

    /**
     * @dev The function which performs the multi path swap.
     */
    function multiSwap(SellData calldata data) external payable returns (uint256);

    /**
     * @dev The function which performs the single path buy.
     */
    function buy(BuyData calldata data) external payable returns (uint256);

    function swapOnUniswap(
        uint256 amountIn,
        uint256 amountOutMin,
        address[] calldata path,
        uint8 referrer
    ) external payable;

    function buyOnUniswap(
        uint256 amountInMax,
        uint256 amountOut,
        address[] calldata path,
        uint8 referrer
    ) external payable;

    function buyOnUniswapFork(
        address factory,
        bytes32 initCode,
        uint256 amountInMax,
        uint256 amountOut,
        address[] calldata path,
        uint8 referrer
    ) external payable;

    function swapOnUniswapFork(
        address factory,
        bytes32 initCode,
        uint256 amountIn,
        uint256 amountOutMin,
        address[] calldata path,
        uint8 referrer
    ) external payable;

    function simplBuy(
        address fromToken,
        address toToken,
        uint256 fromAmount,
        uint256 toAmount,
        address[] memory callees,
        bytes memory exchangeData,
        uint256[] memory startIndexes,
        uint256[] memory values,
        address payable beneficiary,
        string memory referrer,
        bool useReduxToken
    ) external payable;

    function simpleSwap(
        address fromToken,
        address toToken,
        uint256 fromAmount,
        uint256 toAmount,
        uint256 expectedAmount,
        address[] memory callees,
        bytes memory exchangeData,
        uint256[] memory startIndexes,
        uint256[] memory values,
        address payable beneficiary,
        string memory referrer,
        bool useReduxToken
    ) external payable returns (uint256 receivedAmount);

    /**
     * @dev The function which performs the mega path swap.
     * @param data Data required to perform swap.
     */
    function megaSwap(MegaSwapSellData memory data) external payable returns (uint256);
}
"},"IBooster.sol":{"content":"// SPDX-License-Identifier: MIT
pragma solidity ^0.8.3;

interface IBooster {
    function FEE_DENOMINATOR() external view returns (uint256);

    function MaxFees() external view returns (uint256);

    function addPool(
        address _lptoken,
        address _gauge,
        uint256 _stashVersion
    ) external returns (bool);

    function claimRewards(uint256 _pid, address _gauge) external returns (bool);

    function crv() external view returns (address);

    function deposit(
        uint256 _pid,
        uint256 _amount,
        bool _stake
    ) external returns (bool);

    function depositAll(uint256 _pid, bool _stake) external returns (bool);

    function distributionAddressId() external view returns (uint256);

    function earmarkFees() external returns (bool);

    function earmarkIncentive() external view returns (uint256);

    function earmarkRewards(uint256 _pid) external returns (bool);

    function feeDistro() external view returns (address);

    function feeManager() external view returns (address);

    function feeToken() external view returns (address);

    function gaugeMap(address) external view returns (bool);

    function isShutdown() external view returns (bool);

    function lockFees() external view returns (address);

    function lockIncentive() external view returns (uint256);

    function lockRewards() external view returns (address);

    function minter() external view returns (address);

    function owner() external view returns (address);

    function platformFee() external view returns (uint256);

    function poolInfo(uint256)
        external
        view
        returns (
            address lptoken,
            address token,
            address gauge,
            address crvRewards,
            address stash,
            bool shutdown
        );

    function poolLength() external view returns (uint256);

    function poolManager() external view returns (address);

    function registry() external view returns (address);

    function rewardArbitrator() external view returns (address);

    function rewardClaimed(
        uint256 _pid,
        address _address,
        uint256 _amount
    ) external returns (bool);

    function rewardFactory() external view returns (address);

    function setArbitrator(address _arb) external;

    function setFactories(
        address _rfactory,
        address _sfactory,
        address _tfactory
    ) external;

    function setFeeInfo() external;

    function setFeeManager(address _feeM) external;

    function setFees(
        uint256 _lockFees,
        uint256 _stakerFees,
        uint256 _callerFees,
        uint256 _platform
    ) external;

    function setGaugeRedirect(uint256 _pid) external returns (bool);

    function setOwner(address _owner) external;

    function setPoolManager(address _poolM) external;

    function setRewardContracts(address _rewards, address _stakerRewards) external;

    function setTreasury(address _treasury) external;

    function setVoteDelegate(address _voteDelegate) external;

    function shutdownPool(uint256 _pid) external returns (bool);

    function shutdownSystem() external;

    function staker() external view returns (address);

    function stakerIncentive() external view returns (uint256);

    function stakerRewards() external view returns (address);

    function stashFactory() external view returns (address);

    function tokenFactory() external view returns (address);

    function treasury() external view returns (address);

    function vote(
        uint256 _voteId,
        address _votingAddress,
        bool _support
    ) external returns (bool);

    function voteDelegate() external view returns (address);

    function voteGaugeWeight(
        address[] memory _gauge,
        uint256[] memory _weight // MEMORY ADDED
    ) external returns (bool);

    function voteOwnership() external view returns (address);

    function voteParameter() external view returns (address);

    function withdraw(uint256 _pid, uint256 _amount) external returns (bool);

    function withdrawAll(uint256 _pid) external returns (bool);

    function withdrawTo(
        uint256 _pid,
        uint256 _amount,
        address _to
    ) external returns (bool);
}
"},"IConvexPooL.sol":{"content":"// SPDX-License-Identifier: MIT
pragma solidity ^0.8.3;

interface IConvexPooL {
    function addExtraReward(address _reward) external returns (bool);

    function balanceOf(address account) external view returns (uint256);

    function clearExtraRewards() external;

    function currentRewards() external view returns (uint256);

    function donate(uint256 _amount) external returns (bool);

    function duration() external view returns (uint256);

    function earned(address account) external view returns (uint256);

    function extraRewards(uint256) external view returns (address);

    function extraRewardsLength() external view returns (uint256);

    function getReward() external returns (bool);

    function getReward(address _account, bool _claimExtras) external returns (bool);

    function historicalRewards() external view returns (uint256);

    function lastTimeRewardApplicable() external view returns (uint256);

    function lastUpdateTime() external view returns (uint256);

    function newRewardRatio() external view returns (uint256);

    function operator() external view returns (address);

    function periodFinish() external view returns (uint256);

    function pid() external view returns (uint256);

    function queueNewRewards(uint256 _rewards) external returns (bool);

    function queuedRewards() external view returns (uint256);

    function rewardManager() external view returns (address);

    function rewardPerToken() external view returns (uint256);

    function rewardPerTokenStored() external view returns (uint256);

    function rewardRate() external view returns (uint256);

    function rewardToken() external view returns (address);

    function rewards(address) external view returns (uint256);

    function stake(uint256 _amount) external returns (bool);

    function stakeAll() external returns (bool);

    function stakeFor(address _for, uint256 _amount) external returns (bool);

    function stakingToken() external view returns (address);

    function totalSupply() external view returns (uint256);

    function userRewardPerTokenPaid(address) external view returns (uint256);

    function withdraw(uint256 amount, bool claim) external returns (bool);

    function withdrawAll(bool claim) external;

    function withdrawAllAndUnwrap(bool claim) external;

    function withdrawAndUnwrap(uint256 amount, bool claim) external returns (bool);
}
"},"ICurvePooL.sol":{"content":"// SPDX-License-Identifier: MIT
pragma solidity ^0.8.3;

interface ICurvePooL {
    function get_virtual_price() external view returns (uint256);

    function exchange(
        int128 i,
        int128 j,
        uint256 dx,
        uint256 min_dy
    ) external;

    function get_dy(
        int128 i,
        int128 j,
        uint256 dx
    ) external view returns (uint256);

    function A() external view returns (uint256);

    function lp_token() external view returns (address);

    function calc_token_amount(uint256[3] memory amounts, bool deposit) external view returns (uint256);

    function calc_token_amount(uint256[4] memory amounts, bool is_deposit) external view returns (uint256);

    function calc_token_amount(uint256[2] memory amounts, bool is_deposit) external view returns (uint256);

    function coins(uint256 arg0) external view returns (address);

    function coins(int128 arg0) external returns (address out);
}
"},"ICurvePooLSize2.sol":{"content":"// SPDX-License-Identifier: MIT
pragma solidity ^0.8.3;

interface ICurvePooLSize2 {
    function A() external view returns (uint256);

    function A_precise() external view returns (uint256);

    function get_virtual_price() external view returns (uint256);

    function calc_token_amount(uint256[2] calldata amounts, bool is_deposit) external view returns (uint256);

    function add_liquidity(uint256[2] calldata amounts, uint256 min_mint_amount) external returns (uint256);

    function get_dy(
        int128 i,
        int128 j,
        uint256 dx
    ) external view returns (uint256);

    function exchange(
        int128 i,
        int128 j,
        uint256 dx,
        uint256 min_dy
    ) external returns (uint256);

    function remove_liquidity(uint256 _amount, uint256[2] calldata min_amounts) external returns (uint256[2] memory);

    function remove_liquidity_imbalance(uint256[2] calldata amounts, uint256 max_burn_amount) external returns (uint256);

    function calc_withdraw_one_coin(uint256 _token_amount, int128 i) external view returns (uint256);

    function remove_liquidity_one_coin(
        uint256 _token_amount,
        int128 i,
        uint256 _min_amount
    ) external returns (uint256);

    function ramp_A(uint256 _future_A, uint256 _future_time) external;

    function stop_ramp_A() external;

    function commit_new_fee(uint256 new_fee, uint256 new_admin_fee) external;

    function apply_new_fee() external;

    function revert_new_parameters() external;

    function commit_transfer_ownership(address _owner) external;

    function apply_transfer_ownership() external;

    function revert_transfer_ownership() external;

    function admin_balances(uint256 i) external view returns (uint256);

    function withdraw_admin_fees() external;

    function donate_admin_fees() external;

    function kill_me() external;

    function unkill_me() external;

    function coins(uint256 arg0) external view returns (address);

    function balances(uint256 arg0) external view returns (uint256);

    function fee() external view returns (uint256);

    function admin_fee() external view returns (uint256);

    function owner() external view returns (address);

    function lp_token() external view returns (address);

    function initial_A() external view returns (uint256);

    function future_A() external view returns (uint256);

    function initial_A_time() external view returns (uint256);

    function future_A_time() external view returns (uint256);

    function admin_actions_deadline() external view returns (uint256);

    function transfer_ownership_deadline() external view returns (uint256);

    function future_fee() external view returns (uint256);

    function future_admin_fee() external view returns (uint256);

    function future_owner() external view returns (address);
}
"},"ICurvePooLSize3.sol":{"content":"// SPDX-License-Identifier: MIT
pragma solidity ^0.8.3;

interface ICurvePooLSize3 {
    function A() external view returns (uint256);

    function get_virtual_price() external view returns (uint256);

    function calc_token_amount(uint256[3] calldata amounts, bool deposit) external view returns (uint256);

    function add_liquidity(uint256[3] calldata amounts, uint256 min_mint_amount) external;

    function get_dy(
        int128 i,
        int128 j,
        uint256 dx
    ) external view returns (uint256);

    function get_dy_underlying(
        int128 i,
        int128 j,
        uint256 dx
    ) external view returns (uint256);

    function exchange(
        int128 i,
        int128 j,
        uint256 dx,
        uint256 min_dy
    ) external;

    function remove_liquidity(uint256 _amount, uint256[3] calldata min_amounts) external;

    function remove_liquidity_imbalance(uint256[3] calldata amounts, uint256 max_burn_amount) external;

    function calc_withdraw_one_coin(uint256 _token_amount, int128 i) external view returns (uint256);

    function remove_liquidity_one_coin(
        uint256 _token_amount,
        int128 i,
        uint256 min_amount
    ) external;

    function ramp_A(uint256 _future_A, uint256 _future_time) external;

    function stop_ramp_A() external;

    function commit_new_fee(uint256 new_fee, uint256 new_admin_fee) external;

    function apply_new_fee() external;

    function revert_new_parameters() external;

    function commit_transfer_ownership(address _owner) external;

    function apply_transfer_ownership() external;

    function revert_transfer_ownership() external;

    function admin_balances(uint256 i) external view returns (uint256);

    function withdraw_admin_fees() external;

    function donate_admin_fees() external;

    function kill_me() external;

    function unkill_me() external;

    function coins(uint256 arg0) external view returns (address);

    function balances(uint256 arg0) external view returns (uint256);

    function fee() external view returns (uint256);

    function admin_fee() external view returns (uint256);

    function owner() external view returns (address);

    function initial_A() external view returns (uint256);

    function future_A() external view returns (uint256);

    function initial_A_time() external view returns (uint256);

    function future_A_time() external view returns (uint256);

    function admin_actions_deadline() external view returns (uint256);

    function transfer_ownership_deadline() external view returns (uint256);

    function future_fee() external view returns (uint256);

    function future_admin_fee() external view returns (uint256);

    function future_owner() external view returns (address);
}
"},"ICurvePooLSize4.sol":{"content":"// SPDX-License-Identifier: MIT
pragma solidity ^0.8.3;

interface ICurvePooLSize4 {
    function get_virtual_price() external returns (uint256 out);

    function calc_token_amount(uint256[4] calldata amounts, bool deposit) external returns (uint256 out);

    function add_liquidity(uint256[4] calldata amounts, uint256 min_mint_amount) external;

    function get_dy(
        int128 i,
        int128 j,
        uint256 dx
    ) external returns (uint256 out);

    function get_dx(
        int128 i,
        int128 j,
        uint256 dy
    ) external returns (uint256 out);

    function get_dy_underlying(
        int128 i,
        int128 j,
        uint256 dx
    ) external returns (uint256 out);

    function get_dx_underlying(
        int128 i,
        int128 j,
        uint256 dy
    ) external returns (uint256 out);

    function exchange(
        int128 i,
        int128 j,
        uint256 dx,
        uint256 min_dy
    ) external;

    function exchange_underlying(
        int128 i,
        int128 j,
        uint256 dx,
        uint256 min_dy
    ) external;

    function remove_liquidity(uint256 _amount, uint256[4] calldata min_amounts) external;

    function remove_liquidity_imbalance(uint256[4] calldata amounts, uint256 max_burn_amount) external;

    function commit_new_parameters(
        uint256 amplification,
        uint256 new_fee,
        uint256 new_admin_fee
    ) external;

    function apply_new_parameters() external;

    function revert_new_parameters() external;

    function commit_transfer_ownership(address _owner) external;

    function apply_transfer_ownership() external;

    function revert_transfer_ownership() external;

    function withdraw_admin_fees() external;

    function kill_me() external;

    function unkill_me() external;

    function coins(int128 arg0) external returns (address out);

    function underlying_coins(int128 arg0) external returns (address out);

    function balances(int128 arg0) external returns (uint256 out);

    function A() external returns (uint256 out);

    function fee() external returns (uint256 out);

    function admin_fee() external returns (uint256 out);

    function owner() external returns (address out);

    function admin_actions_deadline() external returns (uint256 out);

    function transfer_ownership_deadline() external returns (uint256 out);

    function future_A() external returns (uint256 out);

    function future_fee() external returns (uint256 out);

    function future_admin_fee() external returns (uint256 out);

    function future_owner() external returns (address out);
}
"},"ICurveRegistry.sol":{"content":"// SPDX-License-Identifier: MIT
pragma solidity ^0.8.3;

interface ICurveRegistry {
    function find_pool_for_coins(address _from, address _to) external view returns (address);

    function find_pool_for_coins(
        address _from,
        address _to,
        uint256 i
    ) external view returns (address);

    function get_n_coins(address _pool) external view returns (uint256[2] memory);

    function get_coins(address _pool) external view returns (address[8] memory);

    function get_underlying_coins(address _pool) external view returns (address[8] memory);

    function get_decimals(address _pool) external view returns (uint256[8] memory);

    function get_underlying_decimals(address _pool) external view returns (uint256[8] memory);

    function get_rates(address _pool) external view returns (uint256[8] memory);

    function get_gauges(address _pool) external view returns (address[10] memory, int128[10] memory);

    function get_balances(address _pool) external view returns (uint256[8] memory);

    function get_underlying_balances(address _pool) external view returns (uint256[8] memory);

    function get_virtual_price_from_lp_token(address _token) external view returns (uint256);

    function get_A(address _pool) external view returns (uint256);

    function get_parameters(address _pool)
        external
        view
        returns (
            uint256 A,
            uint256 future_A,
            uint256 fee,
            uint256 admin_fee,
            uint256 future_fee,
            uint256 future_admin_fee,
            address future_owner,
            uint256 initial_A,
            uint256 initial_A_time,
            uint256 future_A_time
        );

    function get_fees(address _pool) external view returns (uint256[2] memory);

    function get_admin_balances(address _pool) external view returns (uint256[8] memory);

    function get_coin_indices(
        address _pool,
        address _from,
        address _to
    )
        external
        view
        returns (
            int128,
            int128,
            bool
        );

    function estimate_gas_used(
        address _pool,
        address _from,
        address _to
    ) external view returns (uint256);

    function is_meta(address _pool) external view returns (bool);

    function get_pool_name(address _pool) external view returns (string memory);

    function get_coin_swap_count(address _coin) external view returns (uint256);

    function get_coin_swap_complement(address _coin, uint256 _index) external view returns (address);

    function get_pool_asset_type(address _pool) external view returns (uint256);

    function add_pool(
        address _pool,
        uint256 _n_coins,
        address _lp_token,
        bytes32 _rate_info,
        uint256 _decimals,
        uint256 _underlying_decimals,
        bool _has_initial_A,
        bool _is_v1,
        string memory _name
    ) external;

    function add_pool_without_underlying(
        address _pool,
        uint256 _n_coins,
        address _lp_token,
        bytes32 _rate_info,
        uint256 _decimals,
        uint256 _use_rates,
        bool _has_initial_A,
        bool _is_v1,
        string memory _name
    ) external;

    function add_metapool(
        address _pool,
        uint256 _n_coins,
        address _lp_token,
        uint256 _decimals,
        string memory _name
    ) external;

    function add_metapool(
        address _pool,
        uint256 _n_coins,
        address _lp_token,
        uint256 _decimals,
        string memory _name,
        address _base_pool
    ) external;

    function remove_pool(address _pool) external;

    function set_pool_gas_estimates(address[5] memory _addr, uint256[2][5] memory _amount) external;

    function set_coin_gas_estimates(address[10] memory _addr, uint256[10] memory _amount) external;

    function set_gas_estimate_contract(address _pool, address _estimator) external;

    function set_liquidity_gauges(address _pool, address[10] memory _liquidity_gauges) external;

    function set_pool_asset_type(address _pool, uint256 _asset_type) external;

    function batch_set_pool_asset_type(address[32] memory _pools, uint256[32] memory _asset_types) external;

    function address_provider() external view returns (address);

    function gauge_controller() external view returns (address);

    function pool_list(uint256 arg0) external view returns (address);

    function pool_count() external view returns (uint256);

    function coin_count() external view returns (uint256);

    function get_coin(uint256 arg0) external view returns (address);

    function get_pool_from_lp_token(address arg0) external view returns (address);

    function get_lp_token(address arg0) external view returns (address);

    function last_updated() external view returns (uint256);
}
"},"IERC20.sol":{"content":"// SPDX-License-Identifier: MIT
pragma solidity ^0.8.3;

/**
 * @dev Interface of the ERC20 standard as defined in the EIP.
 */
interface IERC20 {
    function name() external view returns (string memory);

    function symbol() external view returns (string memory);

    function decimals() external view returns (uint8);

    /**
     * @dev Returns the amount of tokens in existence.
     */
    function totalSupply() external view returns (uint256);

    /**
     * @dev Returns the amount of tokens owned by `account`.
     */
    function balanceOf(address account) external view returns (uint256);

    /**
     * @dev Moves `amount` tokens from the caller\u0027s account to `recipient`.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transfer(address recipient, uint256 amount) external returns (bool);

    /**
     * @dev Returns the remaining number of tokens that `spender` will be
     * allowed to spend on behalf of `owner` through {transferFrom}. This is
     * zero by default.
     *
     * This value changes when {approve} or {transferFrom} are called.
     */
    function allowance(address owner, address spender) external view returns (uint256);

    /**
     * @dev Sets `amount` as the allowance of `spender` over the caller\u0027s tokens.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * IMPORTANT: Beware that changing an allowance with this method brings the risk
     * that someone may use both the old and the new allowance by unfortunate
     * transaction ordering. One possible solution to mitigate this race
     * condition is to first reduce the spender\u0027s allowance to 0 and set the
     * desired value afterwards:
     * https://github.com/ethereum/EIPs/issues/20#issuecomment-263524729
     *
     * Emits an {Approval} event.
     */
    function approve(address spender, uint256 amount) external returns (bool);

    /**
     * @dev Moves `amount` tokens from `sender` to `recipient` using the
     * allowance mechanism. `amount` is then deducted from the caller\u0027s
     * allowance.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transferFrom(
        address sender,
        address recipient,
        uint256 amount
    ) external returns (bool);

    /**
     * @dev Emitted when `value` tokens are moved from one account (`from`) to
     * another (`to`).
     *
     * Note that `value` may be zero.
     */
    event Transfer(address indexed from, address indexed to, uint256 value);

    /**
     * @dev Emitted when the allowance of a `spender` for an `owner` is set by
     * a call to {approve}. `value` is the new allowance.
     */
    event Approval(address indexed owner, address indexed spender, uint256 value);
}
"},"ISushiswapRouter.sol":{"content":"// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.3;

interface ISushiswapRouter {
    function WETH() external view returns (address);

    function addLiquidity(
        address tokenA,
        address tokenB,
        uint256 amountADesired,
        uint256 amountBDesired,
        uint256 amountAMin,
        uint256 amountBMin,
        address to,
        uint256 deadline
    )
        external
        returns (
            uint256 amountA,
            uint256 amountB,
            uint256 liquidity
        );

    function addLiquidityETH(
        address token,
        uint256 amountTokenDesired,
        uint256 amountTokenMin,
        uint256 amountETHMin,
        address to,
        uint256 deadline
    )
        external
        returns (
            uint256 amountToken,
            uint256 amountETH,
            uint256 liquidity
        );

    function factory() external view returns (address);

    function getAmountIn(
        uint256 amountOut,
        uint256 reserveIn,
        uint256 reserveOut
    ) external pure returns (uint256 amountIn);

    function getAmountOut(
        uint256 amountIn,
        uint256 reserveIn,
        uint256 reserveOut
    ) external pure returns (uint256 amountOut);

    function getAmountsIn(uint256 amountOut, address[] memory path) external view returns (uint256[] memory amounts);

    function getAmountsOut(uint256 amountIn, address[] memory path) external view returns (uint256[] memory amounts);

    function quote(
        uint256 amountA,
        uint256 reserveA,
        uint256 reserveB
    ) external pure returns (uint256 amountB);

    function removeLiquidity(
        address tokenA,
        address tokenB,
        uint256 liquidity,
        uint256 amountAMin,
        uint256 amountBMin,
        address to,
        uint256 deadline
    ) external returns (uint256 amountA, uint256 amountB);

    function removeLiquidityETH(
        address token,
        uint256 liquidity,
        uint256 amountTokenMin,
        uint256 amountETHMin,
        address to,
        uint256 deadline
    ) external returns (uint256 amountToken, uint256 amountETH);

    function removeLiquidityETHSupportingFeeOnTransferTokens(
        address token,
        uint256 liquidity,
        uint256 amountTokenMin,
        uint256 amountETHMin,
        address to,
        uint256 deadline
    ) external returns (uint256 amountETH);

    function removeLiquidityETHWithPermit(
        address token,
        uint256 liquidity,
        uint256 amountTokenMin,
        uint256 amountETHMin,
        address to,
        uint256 deadline,
        bool approveMax,
        uint8 v,
        bytes32 r,
        bytes32 s
    ) external returns (uint256 amountToken, uint256 amountETH);

    function removeLiquidityETHWithPermitSupportingFeeOnTransferTokens(
        address token,
        uint256 liquidity,
        uint256 amountTokenMin,
        uint256 amountETHMin,
        address to,
        uint256 deadline,
        bool approveMax,
        uint8 v,
        bytes32 r,
        bytes32 s
    ) external returns (uint256 amountETH);

    function removeLiquidityWithPermit(
        address tokenA,
        address tokenB,
        uint256 liquidity,
        uint256 amountAMin,
        uint256 amountBMin,
        address to,
        uint256 deadline,
        bool approveMax,
        uint8 v,
        bytes32 r,
        bytes32 s
    ) external returns (uint256 amountA, uint256 amountB);

    function swapETHForExactTokens(
        uint256 amountOut,
        address[] memory path,
        address to,
        uint256 deadline
    ) external returns (uint256[] memory amounts);

    function swapExactETHForTokens(
        uint256 amountOutMin,
        address[] memory path,
        address to,
        uint256 deadline
    ) external returns (uint256[] memory amounts);

    function swapExactETHForTokensSupportingFeeOnTransferTokens(
        uint256 amountOutMin,
        address[] memory path,
        address to,
        uint256 deadline
    ) external;

    function swapExactTokensForETH(
        uint256 amountIn,
        uint256 amountOutMin,
        address[] memory path,
        address to,
        uint256 deadline
    ) external returns (uint256[] memory amounts);

    function swapExactTokensForETHSupportingFeeOnTransferTokens(
        uint256 amountIn,
        uint256 amountOutMin,
        address[] memory path,
        address to,
        uint256 deadline
    ) external;

    function swapExactTokensForTokens(
        uint256 amountIn,
        uint256 amountOutMin,
        address[] memory path,
        address to,
        uint256 deadline
    ) external returns (uint256[] memory amounts);

    function swapExactTokensForTokensSupportingFeeOnTransferTokens(
        uint256 amountIn,
        uint256 amountOutMin,
        address[] memory path,
        address to,
        uint256 deadline
    ) external;

    function swapTokensForExactETH(
        uint256 amountOut,
        uint256 amountInMax,
        address[] memory path,
        address to,
        uint256 deadline
    ) external returns (uint256[] memory amounts);

    function swapTokensForExactTokens(
        uint256 amountOut,
        uint256 amountInMax,
        address[] memory path,
        address to,
        uint256 deadline
    ) external returns (uint256[] memory amounts);
}
"},"ITokenTransferProxy.sol":{"content":"// SPDX-License-Identifier: UNLICENSED
pragma solidity \u003e=0.7.0 \u003c0.8.4;

interface ITokenTransferProxy {
    function transferFrom(
        address token,
        address from,
        address to,
        uint256 amount
    ) external;

    function freeReduxTokens(address user, uint256 tokensToFree) external;
}
"},"ITriCryptoZap.sol":{"content":"// SPDX-License-Identifier: MIT
pragma solidity ^0.8.3;

interface ITriCryptoZap {
    function add_liquidity(uint256[3] memory _amounts, uint256 _min_mint_amount) external returns (uint256);

    function add_liquidity(
        uint256[3] memory _amounts,
        uint256 _min_mint_amount,
        address _receiver
    ) external returns (uint256);

    function remove_liquidity(uint256 _amount, uint256[3] memory _min_amounts) external returns (uint256[3] memory);

    function remove_liquidity(
        uint256 _amount,
        uint256[3] memory _min_amounts,
        address _receiver
    ) external returns (uint256[3] memory);

    function remove_liquidity_one_coin(
        uint256 _token_amount,
        uint256 i,
        uint256 _min_amount
    ) external returns (uint256);

    function remove_liquidity_one_coin(
        uint256 _token_amount,
        uint256 i,
        uint256 _min_amount,
        address _receiver
    ) external returns (uint256);

    function pool() external view returns (address);

    function token() external view returns (address);

    function coins(uint256 arg0) external view returns (address);
}
"},"ITriPoolZap.sol":{"content":"// SPDX-License-Identifier: MIT
pragma solidity ^0.8.3;

interface ITriPoolZap {
    function add_liquidity(
        address _pool,
        uint256[4] memory _deposit_amounts,
        uint256 _min_mint_amount
    ) external returns (uint256);

    function add_liquidity(
        address _pool,
        uint256[4] memory _deposit_amounts,
        uint256 _min_mint_amount,
        address _receiver
    ) external returns (uint256);

    function remove_liquidity(
        address _pool,
        uint256 _burn_amount,
        uint256[4] memory _min_amounts
    ) external returns (uint256[4] memory);

    function remove_liquidity(
        address _pool,
        uint256 _burn_amount,
        uint256[4] memory _min_amounts,
        address _receiver
    ) external returns (uint256[4] memory);

    function remove_liquidity_one_coin(
        address _pool,
        uint256 _burn_amount,
        int128 i,
        uint256 _min_amount
    ) external returns (uint256);

    function remove_liquidity_one_coin(
        address _pool,
        uint256 _burn_amount,
        int128 i,
        uint256 _min_amount,
        address _receiver
    ) external returns (uint256);

    function remove_liquidity_imbalance(
        address _pool,
        uint256[4] memory _amounts,
        uint256 _max_burn_amount
    ) external returns (uint256);

    function remove_liquidity_imbalance(
        address _pool,
        uint256[4] memory _amounts,
        uint256 _max_burn_amount,
        address _receiver
    ) external returns (uint256);

    function calc_withdraw_one_coin(
        address _pool,
        uint256 _token_amount,
        int128 i
    ) external view returns (uint256);

    function calc_token_amount(
        address _pool,
        uint256[4] memory _amounts,
        bool _is_deposit
    ) external view returns (uint256);
}
"},"IUniswapV3Router.sol":{"content":"// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.3;

interface IUniswapV3Router {
    struct ExactInputSingleParams {
        address tokenIn;
        address tokenOut;
        uint24 fee;
        address recipient;
        uint256 deadline;
        uint256 amountIn;
        uint256 amountOutMinimum;
        uint160 sqrtPriceLimitX96;
    }

    struct ExactInputParams {
        bytes path;
        address recipient;
        uint256 deadline;
        uint256 amountIn;
        uint256 amountOutMinimum;
    }

    struct ExactOutputSingleParams {
        address tokenIn;
        address tokenOut;
        uint24 fee;
        address recipient;
        uint256 deadline;
        uint256 amountOut;
        uint256 amountInMaximum;
        uint160 sqrtPriceLimitX96;
    }

    struct ExactOutputParams {
        bytes path;
        address recipient;
        uint256 deadline;
        uint256 amountOut;
        uint256 amountInMaximum;
    }

    function WETH9() external view returns (address);

    function exactInput(ExactInputParams memory params) external payable returns (uint256 amountOut);

    function exactInputSingle(ExactInputSingleParams memory params) external returns (uint256 amountOut);

    function exactOutput(ExactOutputParams memory params) external returns (uint256 amountIn);

    function exactOutputSingle(ExactOutputSingleParams memory params) external returns (uint256 amountIn);

    function factory() external view returns (address);

    function multicall(bytes[] memory data) external returns (bytes[] memory results);

    function refundETH() external;

    function selfPermit(
        address token,
        uint256 value,
        uint256 deadline,
        uint8 v,
        bytes32 r,
        bytes32 s
    ) external;

    function selfPermitAllowed(
        address token,
        uint256 nonce,
        uint256 expiry,
        uint8 v,
        bytes32 r,
        bytes32 s
    ) external;

    function selfPermitAllowedIfNecessary(
        address token,
        uint256 nonce,
        uint256 expiry,
        uint8 v,
        bytes32 r,
        bytes32 s
    ) external;

    function selfPermitIfNecessary(
        address token,
        uint256 value,
        uint256 deadline,
        uint8 v,
        bytes32 r,
        bytes32 s
    ) external;

    function sweepToken(
        address token,
        uint256 amountMinimum,
        address recipient
    ) external;

    function sweepTokenWithFee(
        address token,
        uint256 amountMinimum,
        address recipient,
        uint256 feeBips,
        address feeRecipient
    ) external;

    function uniswapV3SwapCallback(
        int256 amount0Delta,
        int256 amount1Delta,
        bytes memory _data
    ) external;

    function unwrapWETH9(uint256 amountMinimum, address recipient) external;

    function unwrapWETH9WithFee(
        uint256 amountMinimum,
        address recipient,
        uint256 feeBips,
        address feeRecipient
    ) external;
}
"},"IWETH.sol":{"content":"// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.3;

interface IWETH {
    event Approval(address indexed src, address indexed guy, uint256 wad);
    event Transfer(address indexed src, address indexed dst, uint256 wad);
    event Deposit(address indexed dst, uint256 wad);
    event Withdrawal(address indexed src, uint256 wad);

    function name() external view returns (string memory);

    function approve(address guy, uint256 wad) external returns (bool);

    function totalSupply() external view returns (uint256);

    function transferFrom(
        address src,
        address dst,
        uint256 wad
    ) external returns (bool);

    function withdraw(uint256 wad) external;

    function decimals() external view returns (uint8);

    function balanceOf(address) external view returns (uint256);

    function symbol() external view returns (string memory);

    function transfer(address dst, uint256 wad) external returns (bool);

    function deposit() external payable;

    function allowance(address, address) external view returns (uint256);
}
"},"IZapperCurveRegistryV2.sol":{"content":"// SPDX-License-Identifier: MIT
pragma solidity ^0.8.3;

interface IZapperCurveRegistryV2 {
    function CurveRegistry() external view returns (address);

    function FactoryRegistry() external view returns (address);

    function getDepositAddress(address swapAddress) external view returns (address depositAddress);

    function getNumTokens(address swapAddress) external view returns (uint256);

    function getPoolTokens(address swapAddress) external view returns (address[4] memory poolTokens);

    function getSwapAddress(address tokenAddress) external view returns (address swapAddress);

    function getTokenAddress(address swapAddress) external view returns (address tokenAddress);

    function isBtcPool(address swapAddress) external view returns (bool);

    function isCurvePool(address swapAddress) external view returns (bool);

    function isEthPool(address swapAddress) external view returns (bool);

    function isFactoryPool(address swapAddress) external view returns (bool);

    function isMetaPool(address swapAddress) external view returns (bool);

    function isOwner() external view returns (bool);

    function isUnderlyingToken(address swapAddress, address tokenContractAddress) external view returns (bool, uint256);

    function owner() external view returns (address);

    function renounceOwnership() external;

    function shouldAddUnderlying(address) external view returns (bool);

    function transferOwnership(address newOwner) external;

    function updateDepositAddresses(address[] calldata swapAddresses, address[] calldata _depositAddresses) external;

    function updateShouldAddUnderlying(address[] calldata swapAddresses, bool[] calldata addUnderlying) external;

    function update_curve_registry() external;

    function update_factory_registry() external;

    function withdrawTokens(address[] calldata tokens) external;
}
"},"Ownable.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import \"./Context.sol\";

/**
 * @dev Contract module which provides a basic access control mechanism, where
 * there is an account (an owner) that can be granted exclusive access to
 * specific functions.
 *
 * By default, the owner account will be the one that deploys the contract. This
 * can later be changed with {transferOwnership}.
 *
 * This module is used through inheritance. It will make available the modifier
 * `onlyOwner`, which can be applied to your functions to restrict their use to
 * the owner.
 */
abstract contract Ownable is Context {
    address private _owner;

    event OwnershipTransferred(address indexed previousOwner, address indexed newOwner);

    /**
     * @dev Initializes the contract setting the deployer as the initial owner.
     */
    constructor() {
        _transferOwnership(_msgSender());
    }

    /**
     * @dev Returns the address of the current owner.
     */
    function owner() public view virtual returns (address) {
        return _owner;
    }

    /**
     * @dev Throws if called by any account other than the owner.
     */
    modifier onlyOwner() {
        require(owner() == _msgSender(), \"Ownable: caller is not the owner\");
        _;
    }

    /**
     * @dev Leaves the contract without owner. It will not be possible to call
     * `onlyOwner` functions anymore. Can only be called by the current owner.
     *
     * NOTE: Renouncing ownership will leave the contract without an owner,
     * thereby removing any functionality that is only available to the owner.
     */
    function renounceOwnership() public virtual onlyOwner {
        _transferOwnership(address(0));
    }

    /**
     * @dev Transfers ownership of the contract to a new account (`newOwner`).
     * Can only be called by the current owner.
     */
    function transferOwnership(address newOwner) public virtual onlyOwner {
        require(newOwner != address(0), \"Ownable: new owner is the zero address\");
        _transferOwnership(newOwner);
    }

    /**
     * @dev Transfers ownership of the contract to a new account (`newOwner`).
     * Internal function without access restriction.
     */
    function _transferOwnership(address newOwner) internal virtual {
        address oldOwner = _owner;
        _owner = newOwner;
        emit OwnershipTransferred(oldOwner, newOwner);
    }
}
