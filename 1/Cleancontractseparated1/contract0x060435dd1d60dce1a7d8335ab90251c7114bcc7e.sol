// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import \"./Ownable.sol\";
import \"./ReentrancyGuard.sol\";

import \"./IOre.sol\";


contract CollectorPool is Ownable, ReentrancyGuard {
  // Ore token contract interface
  IOre public ore;

  // Determines if collectors can deposit ore to be burnt
  bool private _openForBurning;
  // Keeps track fo the currently active launch counter
  uint256 private _currentLaunch;

  // Keeps track of whitelisted fund sources
  mapping (address =\u003e bool) public fundAddresses;
  // Keeps track of the total claimed rewards by an address for each launch
  mapping (address =\u003e mapping (uint256 =\u003e uint256)) private _claimedByAddress;
  // Keeps track of the total burnt ore by an address for each launch
  mapping (address =\u003e mapping (uint256 =\u003e uint256)) private _burntOreByAddress;
  // Keeps track of the total burnt ore in each launch
  mapping (uint256 =\u003e uint256) private _totalBurntOreByLaunch;
  // Keeps track of the total funds received in each launch
  mapping (uint256 =\u003e uint256) private _totalFundsByLaunch;

  constructor(address _ore) {
    ore = IOre(_ore);
  }

  function currentLaunch() external view returns (uint256) {
    return _currentLaunch;
  }

  function openForBurning() external view returns (bool) {
    return _openForBurning;
  }

  function setBurningState(bool _state) external onlyOwner {
    require(_openForBurning != _state, \"Invalid State\");
    _openForBurning = _state;
  }

  function setCurrentLaunch(uint256 _launch) external onlyOwner {
    require(_currentLaunch \u003c _launch, \"Invalid Launch Number\");
    _currentLaunch = _launch;
  }

  function setFundAddress(address _address, bool _state) external onlyOwner {
    require(_address != address(0), \"Invalid Address\");

    if (fundAddresses[_address] != _state) {
      fundAddresses[_address] = _state;
    }
  }

  function claimedByAddress(address _address, uint256 _launch) external view returns (uint256) {
    return _claimedByAddress[_address][_launch];
  }

  function burntOreByAddress(address _address, uint256 _launch) external view returns (uint256) {
    return _burntOreByAddress[_address][_launch];
  }

  function totalBurntOreByLaunch(uint256 _launch) external view returns (uint256) {
    return _totalBurntOreByLaunch[_launch];
  }

  function totalFundsByLaunch(uint256 _launch) external view returns (uint256) {
    return _totalFundsByLaunch[_launch];
  }

  // Deposits ore to be burned
  function burnOre(uint256 _amount) external nonReentrant {
    require(_openForBurning, \"Not Open For Burning\");
    require(_amount \u003e 0, \"Invalid Ore Amount\");
    require(ore.balanceOf(msg.sender) \u003e= _amount, \"Insufficient Ore\");

    ore.burn(msg.sender, _amount);

    _burntOreByAddress[msg.sender][_currentLaunch] += _amount;
    _totalBurntOreByLaunch[_currentLaunch] += _amount;
  }

  // Calculates and returns the total amount of claimable funds for the specified account and period
  function totalClaimableByAccount(
    address _account,
    uint256 _fromLaunch,
    uint256 _toLaunch
  ) public view returns (
    uint256,
    uint256[] memory
  ) {
    require(_fromLaunch \u003e 0 \u0026\u0026 _toLaunch \u003c= _currentLaunch \u0026\u0026 _fromLaunch \u003c= _toLaunch, \"Invalid Launch Period\");

    // Calculate the total claimable amount along with the detail for each launch in the specified period range
    uint256 totalClaimable = 0;
    uint256 periodSize = _toLaunch - _fromLaunch + 1;
    uint256[] memory maxPerLaunch = new uint256[](periodSize);
    for (uint256 i = _fromLaunch; i \u003c= _toLaunch ; i++) {
      uint256 maxAmount = 0;

      if (_totalBurntOreByLaunch[i] \u003e 0 \u0026\u0026 _totalFundsByLaunch[i] \u003e 0) {
        maxAmount = (_burntOreByAddress[_account][i] * _totalFundsByLaunch[i]) / _totalBurntOreByLaunch[i];
        uint256 claimable = maxAmount - _claimedByAddress[_account][i];

        if (claimable \u003e 0) {
          totalClaimable += claimable;
        }
      }

      uint256 index = i - _fromLaunch;
      maxPerLaunch[index] = maxAmount;
    }

    return (totalClaimable, maxPerLaunch);
  }

  // Can be called by collectors for claiming funds from the ore burning mechanism
  function claim(uint256 _fromLaunch, uint256 _toLaunch) external nonReentrant {
    uint256 totalClaimable;
    uint256[] memory maxPerLaunch;
    (totalClaimable, maxPerLaunch) = totalClaimableByAccount(msg.sender, _fromLaunch, _toLaunch);

    // Check the claimable amount and update the total claimed so far for the account
    require(totalClaimable \u003e 0, \"Insufficient Claimable Funds\");
    for (uint256 i = _fromLaunch; i \u003c= _toLaunch; i++) {
      uint256 index = i - _fromLaunch;
      if (_claimedByAddress[msg.sender][i] \u003c maxPerLaunch[index]) {
        _claimedByAddress[msg.sender][i] = maxPerLaunch[index];
      }
    }

    payable(msg.sender).transfer(totalClaimable);
  }

  // Handles funds received for the pool
  receive() external payable {
    // Make sure enough funds are sent, and the source is whitelisted
    require(msg.value \u003e 0, \"Insufficient Funds\");
    require(fundAddresses[msg.sender], \"Invalid Fund Address\");

    // Update the total received funds for the currently open launch
    _totalFundsByLaunch[_currentLaunch] += msg.value;
  }
}
"},"Context.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

/**
 * @dev Provides information about the current execution context, including the
 * sender of the transaction and its data. While these are generally available
 * via msg.sender and msg.data, they should not be accessed in such a direct
 * manner, since when dealing with meta-transactions the account sending and
 * paying for execution may not be the actual sender (as far as an application
 * is concerned).
 *
 * This contract is only required for intermediate, library-like contracts.
 */
abstract contract Context {
    function _msgSender() internal view virtual returns (address) {
        return msg.sender;
    }

    function _msgData() internal view virtual returns (bytes calldata) {
        return msg.data;
    }
}
"},"IOre.sol":{"content":"// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;


interface IOre {
  function balanceOf(address owner) external view returns (uint256);
  function mint(address account, uint256 amount) external;
  function burn(address account, uint256 amount) external;
}
"},"Ownable.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import \"./Context.sol\";

/**
 * @dev Contract module which provides a basic access control mechanism, where
 * there is an account (an owner) that can be granted exclusive access to
 * specific functions.
 *
 * By default, the owner account will be the one that deploys the contract. This
 * can later be changed with {transferOwnership}.
 *
 * This module is used through inheritance. It will make available the modifier
 * `onlyOwner`, which can be applied to your functions to restrict their use to
 * the owner.
 */
abstract contract Ownable is Context {
    address private _owner;

    event OwnershipTransferred(address indexed previousOwner, address indexed newOwner);

    /**
     * @dev Initializes the contract setting the deployer as the initial owner.
     */
    constructor() {
        _setOwner(_msgSender());
    }

    /**
     * @dev Returns the address of the current owner.
     */
    function owner() public view virtual returns (address) {
        return _owner;
    }

    /**
     * @dev Throws if called by any account other than the owner.
     */
    modifier onlyOwner() {
        require(owner() == _msgSender(), \"Ownable: caller is not the owner\");
        _;
    }

    /**
     * @dev Leaves the contract without owner. It will not be possible to call
     * `onlyOwner` functions anymore. Can only be called by the current owner.
     *
     * NOTE: Renouncing ownership will leave the contract without an owner,
     * thereby removing any functionality that is only available to the owner.
     */
    function renounceOwnership() public virtual onlyOwner {
        _setOwner(address(0));
    }

    /**
     * @dev Transfers ownership of the contract to a new account (`newOwner`).
     * Can only be called by the current owner.
     */
    function transferOwnership(address newOwner) public virtual onlyOwner {
        require(newOwner != address(0), \"Ownable: new owner is the zero address\");
        _setOwner(newOwner);
    }

    function _setOwner(address newOwner) private {
        address oldOwner = _owner;
        _owner = newOwner;
        emit OwnershipTransferred(oldOwner, newOwner);
    }
}
"},"ReentrancyGuard.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

/**
 * @dev Contract module that helps prevent reentrant calls to a function.
 *
 * Inheriting from `ReentrancyGuard` will make the {nonReentrant} modifier
 * available, which can be applied to functions to make sure there are no nested
 * (reentrant) calls to them.
 *
 * Note that because there is a single `nonReentrant` guard, functions marked as
 * `nonReentrant` may not call one another. This can be worked around by making
 * those functions `private`, and then adding `external` `nonReentrant` entry
 * points to them.
 *
 * TIP: If you would like to learn more about reentrancy and alternative ways
 * to protect against it, check out our blog post
 * https://blog.openzeppelin.com/reentrancy-after-istanbul/[Reentrancy After Istanbul].
 */
abstract contract ReentrancyGuard {
    // Booleans are more expensive than uint256 or any type that takes up a full
    // word because each write operation emits an extra SLOAD to first read the
    // slot\u0027s contents, replace the bits taken up by the boolean, and then write
    // back. This is the compiler\u0027s defense against contract upgrades and
    // pointer aliasing, and it cannot be disabled.

    // The values being non-zero value makes deployment a bit more expensive,
    // but in exchange the refund on every call to nonReentrant will be lower in
    // amount. Since refunds are capped to a percentage of the total
    // transaction\u0027s gas, it is best to keep them low in cases like this one, to
    // increase the likelihood of the full refund coming into effect.
    uint256 private constant _NOT_ENTERED = 1;
    uint256 private constant _ENTERED = 2;

    uint256 private _status;

    constructor() {
        _status = _NOT_ENTERED;
    }

    /**
     * @dev Prevents a contract from calling itself, directly or indirectly.
     * Calling a `nonReentrant` function from another `nonReentrant`
     * function is not supported. It is possible to prevent this from happening
     * by making the `nonReentrant` function external, and make it call a
     * `private` function that does the actual work.
     */
    modifier nonReentrant() {
        // On the first call to nonReentrant, _notEntered will be true
        require(_status != _ENTERED, \"ReentrancyGuard: reentrant call\");

        // Any calls to nonReentrant after this point will fail
        _status = _ENTERED;

        _;

        // By storing the original value once again, a refund is triggered (see
        // https://eips.ethereum.org/EIPS/eip-2200)
        _status = _NOT_ENTERED;
    }
}

