pragma solidity \u003e=0.5.0 \u003c=0.5.15;

/*
 * @dev Provides information about the current execution context, including the
 * sender of the transaction and its data. While these are generally available
 * via msg.sender and msg.data, they should not be accessed in such a direct
 * manner, since when dealing with GSN meta-transactions the account sending and
 * paying for execution may not be the actual sender (as far as an application
 * is concerned).
 *
 * This contract is only required for intermediate, library-like contracts.
 */
contract Context {
    // Empty internal constructor, to prevent people from mistakenly deploying
    // an instance of this contract, which should be used via inheritance.
    constructor() internal {}

    // solhint-disable-previous-line no-empty-blocks

    function _msgSender() internal view returns (address payable) {
        return msg.sender;
    }

    function _msgData() internal view returns (bytes memory) {
        this; // silence state mutability warning without generating bytecode - see https://github.com/ethereum/solidity/issues/2691
        return msg.data;
    }
}
"},"Ownable.sol":{"content":"pragma solidity \u003e=0.5.0 \u003c=0.5.15;

import \"./Context.sol\";

/**
 * @dev Contract module which provides a basic access control mechanism, where
 * there is an account (an owner) that can be granted exclusive access to
 * specific functions.
 *
 * This module is used through inheritance. It will make available the modifier
 * `onlyOwner`, which can be applied to your functions to restrict their use to
 * the owner.
 */
contract Ownable is Context {
    address private _owner;

    event OwnershipTransferred(address indexed previousOwner, address indexed newOwner);

    /**
     * @dev Initializes the contract setting the deployer as the initial owner.
     */
    constructor() internal {
        address msgSender = _msgSender();
        _owner = msgSender;
        emit OwnershipTransferred(address(0), msgSender);
    }

    /**
     * @dev Throws if called by any account other than the owner.
     */
    modifier onlyOwner() {
        require(isOwner(), \"Ownable: not the owner\");
        _;
    }

    /**
     * @dev Leaves the contract without owner. It will not be possible to call
     * `onlyOwner` functions anymore. Can only be called by the current owner.
     *
     * NOTE: Renouncing ownership will leave the contract without an owner,
     * thereby removing any functionality that is only available to the owner.
     */
    function renounceOwnership() public onlyOwner {
        emit OwnershipTransferred(_owner, address(0));
        _owner = address(0);
    }

    /**
     * @dev Transfers ownership of the contract to a new account (`newOwner`).
     * Can only be called by the current owner.
     */
    function transferOwnership(address newOwner) public onlyOwner {
        _transferOwnership(newOwner);
    }

    /**
     * @dev Returns true if the caller is the current owner.
     */
    function isOwner() public view returns (bool) {
        return _msgSender() == _owner;
    }

    /**
     * @dev Returns the address of the current owner.
     */
    function owner() public view returns (address) {
        return _owner;
    }

    /**
     * @dev Transfers ownership of the contract to a new account (`newOwner`).
     */
    function _transferOwnership(address newOwner) internal {
        require(newOwner != address(0), \"Ownable: no zero address\");
        emit OwnershipTransferred(_owner, newOwner);
        _owner = newOwner;
    }
}
"},"VerifierList.sol":{"content":"pragma solidity \u003e=0.5.0 \u003c=0.5.15;

import \"./Ownable.sol\";

contract VerifierList is Ownable {
    struct VerifierDetails {
        address owner;
        string typeOfVerifier;
        string verifierParams; // typically a json of parameters required
        bool isCreated;
    }

    event VerifierAdded(string verifier, string typeOfVerifier);

    event VerifierUpdated(string verifier);

    mapping(string =\u003e VerifierDetails) public verifiers;

    string[] public verifierList;

    modifier verifierExists(string memory verifier) {
        require(verifiers[verifier].isCreated, \"verifier doesnt exist\");
        _;
    }

    modifier verifierDoesNotExists(string memory verifier) {
        require(!verifiers[verifier].isCreated, \"verifier already exists\");
        _;
    }

    modifier verifierOwnerOnly(string memory verifier) {
        require(verifiers[verifier].owner == msg.sender, \"not owner of verifier\");
        _;
    }

    function addVerifier(
        string calldata _verifier,
        string calldata _typeOfVerifier,
        string calldata _verifierParams,
        address _owner
    ) external onlyOwner verifierDoesNotExists(_verifier) {
        verifiers[_verifier] = VerifierDetails({owner: _owner, typeOfVerifier: _typeOfVerifier, verifierParams: _verifierParams, isCreated: true});
        verifierList.push(_verifier);
        emit VerifierAdded(_verifier, _typeOfVerifier);
    }

    function updateVerifier(string calldata _verifier, string calldata _verifierParams)
        external
        verifierOwnerOnly(_verifier)
        verifierExists(_verifier)
    {
        verifiers[_verifier].verifierParams = _verifierParams;
        emit VerifierUpdated(_verifier);
    }

    function getVerifierListCount() external view returns (uint256 verifierListCount) {
        return verifierList.length;
    }
}
"},"VerifierRouter.sol":{"content":"pragma solidity \u003e=0.5.0 \u003c=0.5.15;

import \"./Ownable.sol\";
import \"./VerifierList.sol\";

contract VerifierRouter is Ownable {
    VerifierList private verifierListContract;

    mapping(string =\u003e string) public routes;

    constructor(address _verifierListContract) public {
        verifierListContract = VerifierList(_verifierListContract);
    }

    function setVerifier(address _verifierListContract) external onlyOwner {
        verifierListContract = VerifierList(_verifierListContract);
    }

    function setRoute(string memory source, string memory target) public onlyOwner {
        routes[source] = target;
    }

    function verifierList(uint256 index) external view returns (string memory) {
        return verifierListContract.verifierList(index);
    }

    function verifiers(string memory verifierID) public view returns (address owner, string memory typeOfVerifier,string memory verifierParams,bool isCreated) {
        string memory targetVerifierID = verifierID;
        if (keccak256(abi.encodePacked(routes[verifierID])) != keccak256(abi.encodePacked(\"\"))){
            targetVerifierID = routes[verifierID];
        }

        return verifierListContract.verifiers(targetVerifierID);
    }

    function getVerifierListCount() external view returns (uint256 verifierListCount) {
        return verifierListContract.getVerifierListCount();
    }
}
