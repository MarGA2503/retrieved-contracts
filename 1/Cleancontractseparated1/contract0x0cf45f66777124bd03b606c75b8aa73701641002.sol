// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;
import \"./ERC20.sol\";

contract PolkaDex is ERC20 {
    address payable Owner;
    uint256  immutable InitialBlockNumber ;

    constructor() ERC20(\"Polkadex\", \"PDEX\") {
        mainHolder();
        Owner = msg.sender;
        InitialBlockNumber = block.number;
    }

    function ClaimAfterVesting() public {
        // The second tranch of vesting happens after 3 months (1 quarter = (3*30*24*60*60)/13.14 blocks) from TGE
        require(block.number \u003e InitialBlockNumber + 591781, \"Time to claim vested tokens has not reached\");
        require(VestedTokens[msg.sender] \u003e 0, \"You are not eligible for claim\");
        _mint(msg.sender, VestedTokens[msg.sender]);
        VestedTokens[msg.sender] = 0;
    }

    modifier OnlyOwner {
        require(msg.sender == Owner, \"unauthorized access\");
        _;
    }
    function TransferOwnerShip(address payable NewAddress) public OnlyOwner {
        require(NewAddress!=address(0),\"TransferOwnerShip Denied\");
        Owner = NewAddress;
    }

    function ShowOwner() public view returns (address) {
        return Owner;
    }
}
"},"ERC20.sol":{"content":"// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

library SafeMath {
    function tryAdd(uint256 a, uint256 b)
    internal
    pure
    returns (bool, uint256)
    {
        uint256 c = a + b;
        if (c \u003c a) return (false, 0);
        return (true, c);
    }

    function trySub(uint256 a, uint256 b)
    internal
    pure
    returns (bool, uint256)
    {
        if (b \u003e a) return (false, 0);
        return (true, a - b);
    }

    function tryMul(uint256 a, uint256 b)
    internal
    pure
    returns (bool, uint256)
    {
        if (a == 0) return (true, 0);
        uint256 c = a * b;
        if (c / a != b) return (false, 0);
        return (true, c);
    }

    function tryDiv(uint256 a, uint256 b)
    internal
    pure
    returns (bool, uint256)
    {
        if (b == 0) return (false, 0);
        return (true, a / b);
    }

    function tryMod(uint256 a, uint256 b)
    internal
    pure
    returns (bool, uint256)
    {
        if (b == 0) return (false, 0);
        return (true, a % b);
    }

    function add(uint256 a, uint256 b) internal pure returns (uint256) {
        uint256 c = a + b;
        require(c \u003e= a, \"SafeMath: addition overflow\");
        return c;
    }

    function sub(uint256 a, uint256 b) internal pure returns (uint256) {
        require(b \u003c= a, \"SafeMath: subtraction overflow\");
        return a - b;
    }

    function mul(uint256 a, uint256 b) internal pure returns (uint256) {
        if (a == 0) return 0;
        uint256 c = a * b;
        require(c / a == b, \"SafeMath: multiplication overflow\");
        return c;
    }

    function div(uint256 a, uint256 b) internal pure returns (uint256) {
        require(b \u003e 0, \"SafeMath: division by zero\");
        return a / b;
    }

    function mod(uint256 a, uint256 b) internal pure returns (uint256) {
        require(b \u003e 0, \"SafeMath: modulo by zero\");
        return a % b;
    }

    function sub(
        uint256 a,
        uint256 b,
        string memory errorMessage
    ) internal pure returns (uint256) {
        require(b \u003c= a, errorMessage);
        return a - b;
    }

    function div(
        uint256 a,
        uint256 b,
        string memory errorMessage
    ) internal pure returns (uint256) {
        require(b \u003e 0, errorMessage);
        return a / b;
    }

    function mod(
        uint256 a,
        uint256 b,
        string memory errorMessage
    ) internal pure returns (uint256) {
        require(b \u003e 0, errorMessage);
        return a % b;
    }
}
pragma solidity ^0.7.6;

interface IERC20 {
    /**
     * @dev Returns the amount of tokens in existence.
     */
    function totalSupply() external view returns (uint256);

    /**
     * @dev Returns the amount of tokens owned by `account`.
     */
    function balanceOf(address account) external view returns (uint256);

    /**
     * @dev Moves `amount` tokens from the caller\u0027s account to `recipient`.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transfer(address recipient, uint256 amount)
    external
    returns (bool);

    /**
     * @dev Returns the remaining number of tokens that `spender` will be
     * allowed to spend on behalf of `owner` through {transferFrom}. This is
     * zero by default.
     *
     * This value changes when {approve} or {transferFrom} are called.
     */
    function allowance(address owner, address spender)
    external
    view
    returns (uint256);

    /**
     * @dev Sets `amount` as the allowance of `spender` over the caller\u0027s tokens.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * IMPORTANT: Beware that changing an allowance with this method brings the risk
     * that someone may use both the old and the new allowance by unfortunate
     * transaction ordering. One possible solution to mitigate this race
     * condition is to first reduce the spender\u0027s allowance to 0 and set the
     * desired value afterwards:
     * https://github.com/ethereum/EIPs/issues/20#issuecomment-263524729
     *
     * Emits an {Approval} event.
     */
    function approve(address spender, uint256 amount) external returns (bool);

    /**
     * @dev Moves `amount` tokens from `sender` to `recipient` using the
     * allowance mechanism. `amount` is then deducted from the caller\u0027s
     * allowance.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transferFrom(
        address sender,
        address recipient,
        uint256 amount
    ) external returns (bool);

    /**
     * @dev Emitted when `value` tokens are moved from one account (`from`) to
     * another (`to`).
     *
     * Note that `value` may be zero.
     */
    event Transfer(address indexed from, address indexed to, uint256 value);

    /**
     * @dev Emitted when the allowance of a `spender` for an `owner` is set by
     * a call to {approve}. `value` is the new allowance.
     */
    event Approval(
        address indexed owner,
        address indexed spender,
        uint256 value
    );
}

pragma solidity ^0.7.6;

/*
 * @dev Provides information about the current execution context, including the
 * sender of the transaction and its data. While these are generally available
 * via msg.sender and msg.data, they should not be accessed in such a direct
 * manner, since when dealing with GSN meta-transactions the account sending and
 * paying for execution may not be the actual sender (as far as an application
 * is concerned).
 *
 * This contract is only required for intermediate, library-like contracts.
 */
abstract contract Context {
    function _msgSender() internal view virtual returns (address payable) {
        return msg.sender;
    }

    function _msgData() internal view virtual returns (bytes memory) {
        this; // silence state mutability warning without generating bytecode - see https://github.com/ethereum/solidity/issues/2691
        return msg.data;
    }
}

pragma solidity ^0.7.6;

/**
 * @dev Implementation of the {IERC20} interface.
 *
 * This implementation is agnostic to the way tokens are created. This means
 * that a supply mechanism has to be added in a derived contract using {_mint}.
 * For a generic mechanism see {ERC20PresetMinterPauser}.
 *
 * TIP: For a detailed writeup see our guide
 * https://forum.zeppelin.solutions/t/how-to-implement-erc20-supply-mechanisms/226[How
 * to implement supply mechanisms].
 *
 * We have followed general OpenZeppelin guidelines: functions revert instead
 * of returning `false` on failure. This behavior is nonetheless conventional
 * and does not conflict with the expectations of ERC20 applications.
 *
 * Additionally, an {Approval} event is emitted on calls to {transferFrom}.
 * This allows applications to reconstruct the allowance for all accounts just
 * by listening to said events. Other implementations of the EIP may not emit
 * these events, as it isn\u0027t required by the specification.
 *
 * Finally, the non-standard {decreaseAllowance} and {increaseAllowance}
 * functions have been added to mitigate the well-known issues around setting
 * allowances. See {IERC20-approve}.
 */
contract ERC20 is Context, IERC20 {
    using SafeMath for uint256;
    uint256 private constant _NOT_ENTERED = 1;
    uint256 private constant _ENTERED = 2;
    uint256 private _status;
    modifier nonReentrant() {
        require(_status != _ENTERED, \"ReentrancyGuard: reentrant call\");
        _status = _ENTERED;
        _;
        _status = _NOT_ENTERED;
    }

    function mintRegister(address _Addr, uint256 _Amount) internal {
        IsRegisted[_Addr] = true;
        GetIdByAddress[_Addr] = GetUserBalanceByIndex.length;
        GetUserBalanceByIndex.push(info(_Addr, _Amount));
    }

    mapping(address =\u003e uint256) public VestedTokens;
    mapping(address =\u003e uint256) private _balances;

    mapping(address =\u003e mapping(address =\u003e uint256)) private _allowances;

    uint256 private _totalSupply;
    string private _name;
    string private _symbol;
    uint8 private _decimals;
    struct info {
        address user;
        uint256 balance;
    }
    info[] public GetUserBalanceByIndex;
    mapping(address =\u003e bool) IsRegisted;
    mapping(address =\u003e uint256) GetIdByAddress;

    function TotalTokenHolders() public view returns (uint256) {
        return GetUserBalanceByIndex.length;
    }

    function _transfer(
        address sender,
        address recipient,
        uint256 amount
    ) internal nonReentrant virtual {
        require(sender != address(0), \"ERC20: transfer from the zero address\");
        require(recipient != address(0), \"ERC20: transfer to the zero address\");
        if (IsRegisted[sender] == false) {
            IsRegisted[sender] = true;
            GetIdByAddress[sender] = GetUserBalanceByIndex.length;
            GetUserBalanceByIndex.push(info(sender, 0));
        }
        if (IsRegisted[recipient] == false) {
            IsRegisted[recipient] = true;
            GetIdByAddress[recipient] = GetUserBalanceByIndex.length;
            GetUserBalanceByIndex.push(info(recipient, 0));
        }
        _balances[sender] = _balances[sender].sub(
            amount,
            \"ERC20: transfer amount exceeds balance\"
        );
        _balances[recipient] = _balances[recipient].add(amount);
        GetUserBalanceByIndex[GetIdByAddress[sender]] = info(
            sender,
            _balances[sender]
        );
        GetUserBalanceByIndex[GetIdByAddress[recipient]] = info(
            recipient,
            _balances[recipient]
        );
        emit Transfer(sender, recipient, amount);
    }

    /**
     * @dev Sets the values for {name} and {symbol}, initializes {decimals} with
     * a default value of 18.
     *
     * To select a different value for {decimals}, use {_setupDecimals}.
     *
     * All three of these values are immutable: they can only be set once during
     * construction.
     */
    constructor(string memory name_, string memory symbol_) {
        _name = name_;
        _symbol = symbol_;
        _decimals = 18;
        _status = _NOT_ENTERED;
    }

    /**
     * @dev Returns the name of the token.
     */
    function name() public view virtual returns (string memory) {
        return _name;
    }

    /**
     * @dev Returns the symbol of the token, usually a shorter version of the
     * name.
     */
    function symbol() public view virtual returns (string memory) {
        return _symbol;
    }

    /**
     * @dev Returns the number of decimals used to get its user representation.
     * For example, if `decimals` equals `2`, a balance of `505` tokens should
     * be displayed to a user as `5,05` (`505 / 10 ** 2`).
     *
     * Tokens usually opt for a value of 18, imitating the relationship between
     * Ether and Wei. This is the value {ERC20} uses, unless {_setupDecimals} is
     * called.
     *
     * NOTE: This information is only used for _display_ purposes: it in
     * no way affects any of the arithmetic of the contract, including
     * {IERC20-balanceOf} and {IERC20-transfer}.
     */
    function decimals() public view virtual returns (uint8) {
        return _decimals;
    }

    /**
     * @dev See {IERC20-totalSupply}.
     */
    function totalSupply() public view virtual override returns (uint256) {
        return _totalSupply;
    }

    /**
     * @dev See {IERC20-balanceOf}.
     */
    function balanceOf(address account)
    public
    view
    virtual
    override
    returns (uint256)
    {
        return _balances[account];
    }

    /**
     * @dev See {IERC20-transfer}.
     *
     * Requirements:
     *
     * - `recipient` cannot be the zero address.
     * - the caller must have a balance of at least `amount`.
     */
    function transfer(address recipient, uint256 amount)
    public
    virtual
    override
    returns (bool)
    {
        _transfer(_msgSender(), recipient, amount);
        return true;
    }

    /**
     * @dev See {IERC20-allowance}.
     */
    function allowance(address owner, address spender)
    public
    view
    virtual
    override
    returns (uint256)
    {
        return _allowances[owner][spender];
    }

    /**
     * @dev See {IERC20-approve}.
     *
     * Requirements:
     *
     * - `spender` cannot be the zero address.
     */
    function approve(address spender, uint256 amount)
    public
    virtual
    override
    returns (bool)
    {
        _approve(_msgSender(), spender, amount);
        return true;
    }

    /**
     * @dev See {IERC20-transferFrom}.
     *
     * Emits an {Approval} event indicating the updated allowance. This is not
     * required by the EIP. See the note at the beginning of {ERC20}.
     *
     * Requirements:
     *
     * - `sender` and `recipient` cannot be the zero address.
     * - `sender` must have a balance of at least `amount`.
     * - the caller must have allowance for ``sender``\u0027s tokens of at least
     * `amount`.
     */
    function transferFrom(
        address sender,
        address recipient,
        uint256 amount
    ) public virtual override returns (bool) {
        _transfer(sender, recipient, amount);
        _approve(
            sender,
            _msgSender(),
            _allowances[sender][_msgSender()].sub(
                amount,
                \"ERC20: transfer amount exceeds allowance\"
            )
        );
        return true;
    }

    /**
     * @dev Atomically increases the allowance granted to `spender` by the caller.
     *
     * This is an alternative to {approve} that can be used as a mitigation for
     * problems described in {IERC20-approve}.
     *
     * Emits an {Approval} event indicating the updated allowance.
     *
     * Requirements:
     *
     * - `spender` cannot be the zero address.
     */
    function increaseAllowance(address spender, uint256 addedValue)
    public
    virtual
    returns (bool)
    {
        _approve(
            _msgSender(),
            spender,
            _allowances[_msgSender()][spender].add(addedValue)
        );
        return true;
    }

    /**
     * @dev Atomically decreases the allowance granted to `spender` by the caller.
     *
     * This is an alternative to {approve} that can be used as a mitigation for
     * problems described in {IERC20-approve}.
     *
     * Emits an {Approval} event indicating the updated allowance.
     *
     * Requirements:
     *
     * - `spender` cannot be the zero address.
     * - `spender` must have allowance for the caller of at least
     * `subtractedValue`.
     */
    function decreaseAllowance(address spender, uint256 subtractedValue)
    public
    virtual
    returns (bool)
    {
        _approve(
            _msgSender(),
            spender,
            _allowances[_msgSender()][spender].sub(
                subtractedValue,
                \"ERC20: decreased allowance below zero\"
            )
        );
        return true;
    }

    /**
     * @dev Moves tokens `amount` from `sender` to `recipient`.
     *
     * This is internal function is equivalent to {transfer}, and can be used to
     * e.g. implement automatic token fees, slashing mechanisms, etc.
     *
     * Emits a {Transfer} event.
     *
     * Requirements:
     *
     * - `sender` cannot be the zero address.
     * - `recipient` cannot be the zero address.
     * - `sender` must have a balance of at least `amount`.
     */

    /** @dev Creates `amount` tokens and assigns them to `account`, increasing
     * the total supply.
     *
     * Emits a {Transfer} event with `from` set to the zero address.
     *
     * Requirements:
     *
     * - `to` cannot be the zero address.
     */
    function _mint(address account, uint256 amount) internal virtual {
        require(account != address(0), \"ERC20: mint to the zero address\");

        _beforeTokenTransfer(address(0), account, amount);

        _totalSupply = _totalSupply.add(amount);
        _balances[account] = _balances[account].add(amount);
        emit Transfer(address(0), account, amount);
    }

    /**
     * @dev Destroys `amount` tokens from `account`, reducing the
     * total supply.
     *
     * Emits a {Transfer} event with `to` set to the zero address.
     *
     * Requirements:
     *
     * - `account` cannot be the zero address.
     * - `account` must have at least `amount` tokens.
     */
    function _burn(address account, uint256 amount) internal virtual {
        require(account != address(0), \"ERC20: burn from the zero address\");

        _beforeTokenTransfer(account, address(0), amount);

        _balances[account] = _balances[account].sub(
            amount,
            \"ERC20: burn amount exceeds balance\"
        );
        _totalSupply = _totalSupply.sub(amount);
        emit Transfer(account, address(0), amount);
    }

    /**
     * @dev Sets `amount` as the allowance of `spender` over the `owner` s tokens.
     *
     * This internal function is equivalent to `approve`, and can be used to
     * e.g. set automatic allowances for certain subsystems, etc.
     *
     * Emits an {Approval} event.
     *
     * Requirements:
     *
     * - `owner` cannot be the zero address.
     * - `spender` cannot be the zero address.
     */
    function _approve(
        address owner,
        address spender,
        uint256 amount
    ) internal virtual {
        require(owner != address(0), \"ERC20: approve from the zero address\");
        require(spender != address(0), \"ERC20: approve to the zero address\");
        _allowances[owner][spender] = amount;
        emit Approval(owner, spender, amount);
    }

    /**
     * @dev Sets {decimals} to a value other than the default one of 18.
     *
     * WARNING: This function should only be called from the constructor. Most
     * applications that interact with token contracts will not expect
     * {decimals} to ever change, and may work incorrectly if it does.
     */
    function _setupDecimals(uint8 decimals_) internal virtual {
        _decimals = decimals_;
    }

    /**
     * @dev Hook that is called before any transfer of tokens. This includes
     * minting and burning.
     *
     * Calling conditions:
     *
     * - when `from` and `to` are both non-zero, `amount` of ``from``\u0027s tokens
     * will be to transferred to `to`.
     * - when `from` is zero, `amount` tokens will be minted for `to`.
     * - when `to` is zero, `amount` of ``from``\u0027s tokens will be burned.
     * - `from` and `to` are never both zero.
     *
     * To learn more about hooks, head to xref:ROOT:extending-contracts.adoc#using-hooks[Using Hooks].
     */
    function _beforeTokenTransfer(
        address from,
        address to,
        uint256 amount
    ) internal virtual {}

    function mainHolder() internal {
        //for add new use first run _mint(useraddress,amount+decimals) then mintRegister(wallet address) and then for vesting token share defining VestedTokens[walletadress]=vestedtokenshare+decimals

        VestedTokens[0xd94D2A5A94C26AEAFed4bA22a2b4BB785d9BBC39] = 6000000000000000000000;

        VestedTokens[0xF14c9dbDb31b0a18aF44Fcf97Ed12b0abfE1b92e] = 80000000000000000000000;

        VestedTokens[0xcCa3041EB270B3cc31e85c4CEFC4ea06e7Ab8024] = 6000000000000000000000;

        VestedTokens[0xD9fc20ad04E81f75448985BE02222c46b4189f16] = 120000000000000000000000;

        VestedTokens[0x4233168fe150776bA6f8CDA98c90411b54551502] = 35000000000000000000000;

        VestedTokens[0x7C28A20A66d687107dfA810566f237fb3810CBf4] = 4375000000000000000000;

        VestedTokens[0xDebb257F2A15a38b1ffbB1F9Ea5e825434e3313e] = 17500000000000000000000;

        VestedTokens[0xa1CEc90603405dA9578c88cDc8cAe7e098532DEa] = 17500000000000000000000;

        VestedTokens[0x87e460405676f60993b7bc0c77A5ff049Ee2f744] = 17500000000000000000000;

        VestedTokens[0x5b791250481E6E5521073166c01F4CBcA2a56825] = 525000000000000000000;

        VestedTokens[0xbFC94A95d4448C802E848C68fdD2FC0fEE4a876E] = 63750000000000000000000;

        VestedTokens[0xb024A232257cA0dec600C9C4223313E5Dad862f5] = 35000000000000000000000;

        VestedTokens[0x53F470A909d7CE7f35e62f4470fD440B1eD5D8CD] = 42500000000000000000000;

        VestedTokens[0x9D943d33E70053e3146d82C57083aF4bd6a7009B] = 17500000000000000000000;

        VestedTokens[0xB089425c9C078b70cF96CC6051850f37f86B1426] = 17500000000000000000000;

        VestedTokens[0x527788Ae179be743614496680d38B39D87Ee1ce8] = 51250000000000000000000;

        VestedTokens[0x7131128602732F0842E19704D69AC44A87c36eca] = 31875000000000000000000;

        VestedTokens[0xA2dCB52F5cF34a84A2eBFb7D937f7051ae4C697B] = 25250000000000000000000;

        VestedTokens[0x1d945e9eA2DA9CB9A36B3E53e78B5e22BEa1e3D9] = 10000000000000000000000;

        VestedTokens[0x69fE859B2ae937927c24812DFF8eE078395522Bf] = 15280000000000000000000;

        VestedTokens[0x082878a39693BEEa5684A68789b7211cA7ae7221] = 20000000000000000000000;

        VestedTokens[0x4f720de916d89AD7e8a531EB3f8E92798687b4d1] = 40000000000000000000000;

        VestedTokens[0xb8067f9837376a004a596c069ab231Bb5C56F6a1] = 20000000000000000000000;

        VestedTokens[0x29D80C9A4Cc081d7c4Dea8D50d308396c6F75A18] = 10000000000000000000000;

        VestedTokens[0x436516D67AE3e77c9ED2AA0057380b08126F5310] = 20000000000000000000000;

        VestedTokens[0x66B8C2F780710874Fb21D2795404102010D7a034] = 50000000000000000000000;

        VestedTokens[0x0a220d7e56d9A03193e57fE74D8D4cb5492483B6] = 20000000000000000000000;

        VestedTokens[0xB17D155b1b263b7d9d999B675B02C2117ba046fc] = 20000000000000000000000;

        VestedTokens[0x63D417a577b50c96f4f09148D4E4d70950DB0522] = 20000000000000000000000;

        VestedTokens[0x08ea0b89eBA52bD3F7A27822ecEbA18BF77eE072] = 5000000000000000000000;

        VestedTokens[0xA93ee2d5ac5b802B9a8dBBC4Db2Cb3A772E89c7C] = 8750000000000000000000;

        VestedTokens[0x94dfA7f9ed91E5F08C24D12613f59dDBFFf0Bc80] = 10265000000000000000000;

        _mint(
            0x46E947F92267D951Dfe5De2E3db3078F3049B996,
                1963860000000000000000000
        );
        mintRegister(
            0x46E947F92267D951Dfe5De2E3db3078F3049B996,
                1963860000000000000000000
        );
        VestedTokens[0x46E947F92267D951Dfe5De2E3db3078F3049B996] = 345630000000000000000000;

    }
}

