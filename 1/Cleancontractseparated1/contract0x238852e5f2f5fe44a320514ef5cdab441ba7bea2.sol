



// SPDX-License-Identifier: MIT

pragma solidity ^0.7.0;

import \"../utils/Context.sol\";
"
    
// SPDX-License-Identifier: MIT

pragma solidity ^0.7.0;

/**
 * @dev Wrappers over Solidity's arithmetic operations with added overflow
 * checks.
 *
 * Arithmetic operations in Solidity wrap on overflow. This can easily result
 * in bugs, because programmers usually assume that an overflow raises an
 * error, which is the standard behavior in high level programming languages.
 * `SafeMath` restores this intuition by reverting the transaction when an
 * operation overflows.
 *
 * Using this library instead of the unchecked operations eliminates an entire
 * class of bugs, so it's recommended to use it always.
 */
library SafeMath {
    /**
     * @dev Returns the addition of two unsigned integers, with an overflow flag.
     *
     * _Available since v3.4._
     */
    function tryAdd(uint256 a, uint256 b) internal pure returns (bool, uint256) {
        uint256 c = a + b;
        if (c < a) return (false, 0);
        return (true, c);
    }

    /**
     * @dev Returns the substraction of two unsigned integers, with an overflow flag.
     *
     * _Available since v3.4._
     */
    function trySub(uint256 a, uint256 b) internal pure returns (bool, uint256) {
        if (b > a) return (false, 0);
        return (true, a - b);
    }

    /**
     * @dev Returns the multiplication of two unsigned integers, with an overflow flag.
     *
     * _Available since v3.4._
     */
    function tryMul(uint256 a, uint256 b) internal pure returns (bool, uint256) {
        // Gas optimization: this is cheaper than requiring 'a' not being zero, but the
        // benefit is lost if 'b' is also tested.
        // See: https://github.com/OpenZeppelin/openzeppelin-contracts/pull/522
        if (a == 0) return (true, 0);
        uint256 c = a * b;
        if (c / a != b) return (false, 0);
        return (true, c);
    }

    /**
     * @dev Returns the division of two unsigned integers, with a division by zero flag.
     *
     * _Available since v3.4._
     */
    function tryDiv(uint256 a, uint256 b) internal pure returns (bool, uint256) {
        if (b == 0) return (false, 0);
        return (true, a / b);
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers, with a division by zero flag.
     *
     * _Available since v3.4._
     */
    function tryMod(uint256 a, uint256 b) internal pure returns (bool, uint256) {
        if (b == 0) return (false, 0);
        return (true, a % b);
    }

    /**
     * @dev Returns the addition of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity's `+` operator.
     *
     * Requirements:
     *
     * - Addition cannot overflow.
     */
    function add(uint256 a, uint256 b) internal pure returns (uint256) {
        uint256 c = a + b;
        require(c >= a, \"SafeMath: addition overflow\");
        return c;
    }

    /**
     * @dev Returns the subtraction of two unsigned integers, reverting on
     * overflow (when the result is negative).
     *
     * Counterpart to Solidity's `-` operator.
     *
     * Requirements:
     *
     * - Subtraction cannot overflow.
     */
    function sub(uint256 a, uint256 b) internal pure returns (uint256) {
        require(b <= a, \"SafeMath: subtraction overflow\");
        return a - b;
    }

    /**
     * @dev Returns the multiplication of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity's `*` operator.
     *
     * Requirements:
     *
     * - Multiplication cannot overflow.
     */
    function mul(uint256 a, uint256 b) internal pure returns (uint256) {
        if (a == 0) return 0;
        uint256 c = a * b;
        require(c / a == b, \"SafeMath: multiplication overflow\");
        return c;
    }

    /**
     * @dev Returns the integer division of two unsigned integers, reverting on
     * division by zero. The result is rounded towards zero.
     *
     * Counterpart to Solidity's `/` operator. Note: this function uses a
     * `revert` opcode (which leaves remaining gas untouched) while Solidity
     * uses an invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function div(uint256 a, uint256 b) internal pure returns (uint256) {
        require(b > 0, \"SafeMath: division by zero\");
        return a / b;
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
     * reverting when dividing by zero.
     *
     * Counterpart to Solidity's `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function mod(uint256 a, uint256 b) internal pure returns (uint256) {
        require(b > 0, \"SafeMath: modulo by zero\");
        return a % b;
    }

    /**
     * @dev Returns the subtraction of two unsigned integers, reverting with custom message on
     * overflow (when the result is negative).
     *
     * CAUTION: This function is deprecated because it requires allocating memory for the error
     * message unnecessarily. For custom revert reasons use {trySub}.
     *
     * Counterpart to Solidity's `-` operator.
     *
     * Requirements:
     *
     * - Subtraction cannot overflow.
     */
    function sub(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b <= a, errorMessage);
        return a - b;
    }

    /**
     * @dev Returns the integer division of two unsigned integers, reverting with custom message on
     * division by zero. The result is rounded towards zero.
     *
     * CAUTION: This function is deprecated because it requires allocating memory for the error
     * message unnecessarily. For custom revert reasons use {tryDiv}.
     *
     * Counterpart to Solidity's `/` operator. Note: this function uses a
     * `revert` opcode (which leaves remaining gas untouched) while Solidity
     * uses an invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function div(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b > 0, errorMessage);
        return a / b;
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
     * reverting with custom message when dividing by zero.
     *
     * CAUTION: This function is deprecated because it requires allocating memory for the error
     * message unnecessarily. For custom revert reasons use {tryMod}.
     *
     * Counterpart to Solidity's `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function mod(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b > 0, errorMessage);
        return a % b;
    }
}
"
    
// SPDX-License-Identifier: MIT

pragma solidity >=0.6.0 <0.8.0;

/*
 * @dev Provides information about the current execution context, including the
 * sender of the transaction and its data. While these are generally available
 * via msg.sender and msg.data, they should not be accessed in such a direct
 * manner, since when dealing with GSN meta-transactions the account sending and
 * paying for execution may not be the actual sender (as far as an application
 * is concerned).
 *
 * This contract is only required for intermediate, library-like contracts.
 */
abstract contract Context {
    function _msgSender() internal view virtual returns (address payable) {
        return msg.sender;
    }

    function _msgData() internal view virtual returns (bytes memory) {
        this; // silence state mutability warning without generating bytecode - see https://github.com/ethereum/solidity/issues/2691
        return msg.data;
    }
}
"
    
// SPDX-License-Identifier: MIT

pragma solidity ^0.7.0;

/**
 * @dev Library for managing
 * https://en.wikipedia.org/wiki/Set_(abstract_data_type)[sets] of primitive
 * types.
 *
 * Sets have the following properties:
 *
 * - Elements are added, removed, and checked for existence in constant time
 * (O(1)).
 * - Elements are enumerated in O(n). No guarantees are made on the ordering.
 *
 * ```
 * contract Example {
 *     // Add the library methods
 *     using EnumerableSet for EnumerableSet.AddressSet;
 *
 *     // Declare a set state variable
 *     EnumerableSet.AddressSet private mySet;
 * }
 * ```
 *
 * As of v3.3.0, sets of type `bytes32` (`Bytes32Set`), `address` (`AddressSet`)
 * and `uint256` (`UintSet`) are supported.
 */
library EnumerableSet {
    // To implement this library for multiple types with as little code
    // repetition as possible, we write it in terms of a generic Set type with
    // bytes32 values.
    // The Set implementation uses private functions, and user-facing
    // implementations (such as AddressSet) are just wrappers around the
    // underlying Set.
    // This means that we can only create new EnumerableSets for types that fit
    // in bytes32.

    struct Set {
        // Storage of set values
        bytes32[] _values;

        // Position of the value in the `values` array, plus 1 because index 0
        // means a value is not in the set.
        mapping (bytes32 => uint256) _indexes;
    }

    /**
     * @dev Add a value to a set. O(1).
     *
     * Returns true if the value was added to the set, that is if it was not
     * already present.
     */
    function _add(Set storage set, bytes32 value) private returns (bool) {
        if (!_contains(set, value)) {
            set._values.push(value);
            // The value is stored at length-1, but we add 1 to all indexes
            // and use 0 as a sentinel value
            set._indexes[value] = set._values.length;
            return true;
        } else {
            return false;
        }
    }

    /**
     * @dev Removes a value from a set. O(1).
     *
     * Returns true if the value was removed from the set, that is if it was
     * present.
     */
    function _remove(Set storage set, bytes32 value) private returns (bool) {
        // We read and store the value's index to prevent multiple reads from the same storage slot
        uint256 valueIndex = set._indexes[value];

        if (valueIndex != 0) { // Equivalent to contains(set, value)
            // To delete an element from the _values array in O(1), we swap the element to delete with the last one in
            // the array, and then remove the last element (sometimes called as 'swap and pop').
            // This modifies the order of the array, as noted in {at}.

            uint256 toDeleteIndex = valueIndex - 1;
            uint256 lastIndex = set._values.length - 1;

            // When the value to delete is the last one, the swap operation is unnecessary. However, since this occurs
            // so rarely, we still do the swap anyway to avoid the gas cost of adding an 'if' statement.

            bytes32 lastvalue = set._values[lastIndex];

            // Move the last value to the index where the value to delete is
            set._values[toDeleteIndex] = lastvalue;
            // Update the index for the moved value
            set._indexes[lastvalue] = toDeleteIndex + 1; // All indexes are 1-based

            // Delete the slot where the moved value was stored
            set._values.pop();

            // Delete the index for the deleted slot
            delete set._indexes[value];

            return true;
        } else {
            return false;
        }
    }

    /**
     * @dev Returns true if the value is in the set. O(1).
     */
    function _contains(Set storage set, bytes32 value) private view returns (bool) {
        return set._indexes[value] != 0;
    }

    /**
     * @dev Returns the number of values on the set. O(1).
     */
    function _length(Set storage set) private view returns (uint256) {
        return set._values.length;
    }

   /**
    * @dev Returns the value stored at position `index` in the set. O(1).
    *
    * Note that there are no guarantees on the ordering of values inside the
    * array, and it may change when more values are added or removed.
    *
    * Requirements:
    *
    * - `index` must be strictly less than {length}.
    */
    function _at(Set storage set, uint256 index) private view returns (bytes32) {
        require(set._values.length > index, \"EnumerableSet: index out of bounds\");
        return set._values[index];
    }

    // Bytes32Set

    struct Bytes32Set {
        Set _inner;
    }

    /**
     * @dev Add a value to a set. O(1).
     *
     * Returns true if the value was added to the set, that is if it was not
     * already present.
     */
    function add(Bytes32Set storage set, bytes32 value) internal returns (bool) {
        return _add(set._inner, value);
    }

    /**
     * @dev Removes a value from a set. O(1).
     *
     * Returns true if the value was removed from the set, that is if it was
     * present.
     */
    function remove(Bytes32Set storage set, bytes32 value) internal returns (bool) {
        return _remove(set._inner, value);
    }

    /**
     * @dev Returns true if the value is in the set. O(1).
     */
    function contains(Bytes32Set storage set, bytes32 value) internal view returns (bool) {
        return _contains(set._inner, value);
    }

    /**
     * @dev Returns the number of values in the set. O(1).
     */
    function length(Bytes32Set storage set) internal view returns (uint256) {
        return _length(set._inner);
    }

   /**
    * @dev Returns the value stored at position `index` in the set. O(1).
    *
    * Note that there are no guarantees on the ordering of values inside the
    * array, and it may change when more values are added or removed.
    *
    * Requirements:
    *
    * - `index` must be strictly less than {length}.
    */
    function at(Bytes32Set storage set, uint256 index) internal view returns (bytes32) {
        return _at(set._inner, index);
    }

    // AddressSet

    struct AddressSet {
        Set _inner;
    }

    /**
     * @dev Add a value to a set. O(1).
     *
     * Returns true if the value was added to the set, that is if it was not
     * already present.
     */
    function add(AddressSet storage set, address value) internal returns (bool) {
        return _add(set._inner, bytes32(uint256(uint160(value))));
    }

    /**
     * @dev Removes a value from a set. O(1).
     *
     * Returns true if the value was removed from the set, that is if it was
     * present.
     */
    function remove(AddressSet storage set, address value) internal returns (bool) {
        return _remove(set._inner, bytes32(uint256(uint160(value))));
    }

    /**
     * @dev Returns true if the value is in the set. O(1).
     */
    function contains(AddressSet storage set, address value) internal view returns (bool) {
        return _contains(set._inner, bytes32(uint256(uint160(value))));
    }

    /**
     * @dev Returns the number of values in the set. O(1).
     */
    function length(AddressSet storage set) internal view returns (uint256) {
        return _length(set._inner);
    }

   /**
    * @dev Returns the value stored at position `index` in the set. O(1).
    *
    * Note that there are no guarantees on the ordering of values inside the
    * array, and it may change when more values are added or removed.
    *
    * Requirements:
    *
    * - `index` must be strictly less than {length}.
    */
    function at(AddressSet storage set, uint256 index) internal view returns (address) {
        return address(uint160(uint256(_at(set._inner, index))));
    }


    // UintSet

    struct UintSet {
        Set _inner;
    }

    /**
     * @dev Add a value to a set. O(1).
     *
     * Returns true if the value was added to the set, that is if it was not
     * already present.
     */
    function add(UintSet storage set, uint256 value) internal returns (bool) {
        return _add(set._inner, bytes32(value));
    }

    /**
     * @dev Removes a value from a set. O(1).
     *
     * Returns true if the value was removed from the set, that is if it was
     * present.
     */
    function remove(UintSet storage set, uint256 value) internal returns (bool) {
        return _remove(set._inner, bytes32(value));
    }

    /**
     * @dev Returns true if the value is in the set. O(1).
     */
    function contains(UintSet storage set, uint256 value) internal view returns (bool) {
        return _contains(set._inner, bytes32(value));
    }

    /**
     * @dev Returns the number of values on the set. O(1).
     */
    function length(UintSet storage set) internal view returns (uint256) {
        return _length(set._inner);
    }

   /**
    * @dev Returns the value stored at position `index` in the set. O(1).
    *
    * Note that there are no guarantees on the ordering of values inside the
    * array, and it may change when more values are added or removed.
    *
    * Requirements:
    *
    * - `index` must be strictly less than {length}.
    */
    function at(UintSet storage set, uint256 index) internal view returns (uint256) {
        return uint256(_at(set._inner, index));
    }
}
"
    
// SPDX-License-Identifier: MIT

/**
 * Based on https://github.com/OpenZeppelin/openzeppelin-contracts/blob/v3.3.0-solc-0.7/contracts/access/Ownable.sol
 *
 * Changes:
 * - Added owner argument to constructor
 * - Reformatted styling in line with this repository.
 */

/*
The MIT License (MIT)

Copyright (c) 2016-2020 zOS Global Limited

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
\"Software\"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

pragma solidity 0.7.6;

import \"@openzeppelin/contracts/GSN/Context.sol\";

/**
 * @dev Contract module which provides a basic access control mechanism, where
 * there is an account (an owner) that can be granted exclusive access to
 * specific functions.
 *
 * By default, the owner account will be the one that deploys the contract. This
 * can later be changed with {transferOwnership}.
 *
 * This module is used through inheritance. It will make available the modifier
 * `onlyOwner`, which can be applied to your functions to restrict their use to
 * the owner.
 */
abstract contract Ownable is Context {
\taddress private _owner;

\tevent OwnershipTransferred(
\t\taddress indexed previousOwner,
\t\taddress indexed newOwner
\t);

\t/**
\t * @dev Initializes the contract setting the deployer as the initial owner.
\t */
\tconstructor(address owner_) {
\t\t_owner = owner_;
\t\temit OwnershipTransferred(address(0), owner_);
\t}

\t/**
\t * @dev Returns the address of the current owner.
\t */
\tfunction owner() public view returns (address) {
\t\treturn _owner;
\t}

\t/**
\t * @dev Throws if called by any account other than the owner.
\t */
\tmodifier onlyOwner() {
\t\trequire(_owner == _msgSender(), \"Ownable: caller is not the owner\");
\t\t_;
\t}

\t/**
\t * @dev Leaves the contract without owner. It will not be possible to call
\t * `onlyOwner` functions anymore. Can only be called by the current owner.
\t *
\t * NOTE: Renouncing ownership will leave the contract without an owner,
\t * thereby removing any functionality that is only available to the owner.
\t */
\tfunction renounceOwnership() public virtual onlyOwner {
\t\temit OwnershipTransferred(_owner, address(0));
\t\t_owner = address(0);
\t}

\t/**
\t * @dev Transfers ownership of the contract to a new account (`newOwner`).
\t * Can only be called by the current owner.
\t */
\tfunction transferOwnership(address newOwner) public virtual onlyOwner {
\t\trequire(newOwner != address(0), \"Ownable: new owner is the zero address\");
\t\temit OwnershipTransferred(_owner, newOwner);
\t\t_owner = newOwner;
\t}
}
"
    
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

import \"@openzeppelin/contracts/utils/EnumerableSet.sol\";
import \"@openzeppelin/contracts/math/SafeMath.sol\";

import \"./interfaces/IFeeLogic.sol\";
import \"../access/Ownable.sol\";

contract FeeLogic is Ownable, IFeeLogic {
\tusing EnumerableSet for EnumerableSet.AddressSet;
\tusing SafeMath for uint128;
\tusing SafeMath for uint256;

\tstruct FeeLogicArgs {
\t\taddress owner;
\t\taddress recipient;
\t\tuint128 feeRateNumerator;
\t\tuint128 feeRateDenominator;
\t\tExemptData[] exemptions;
\t\tuint256 rebaseInterval;
\t\tuint128 rebaseFeeRateNum;
\t\tuint128 rebaseFeeRateDen;
\t\tExemptData[] rebaseExemptions;
\t}

\t/* Mutable Private State */

\tEnumerableSet.AddressSet private _exempts;
\tuint128 private _feeRateNum;
\tuint128 private _feeRateDen;
\taddress private _recipient;

\tEnumerableSet.AddressSet private _rebaseExempts;
\tuint256 private _rebaseInterval;
\tuint128 private _rebaseFeeRateNum;
\tuint128 private _rebaseFeeRateDen;

\t/* Constructor */

\tconstructor(FeeLogicArgs memory _args) Ownable(_args.owner) {
\t\trequire(
\t\t\t_args.feeRateDenominator > _args.feeRateNumerator,
\t\t\t\"FeeLogic: feeRate is gte to 1\"
\t\t);
\t\trequire(
\t\t\t_args.rebaseFeeRateDen > _args.rebaseFeeRateNum,
\t\t\t\"FeeLogic: rebaseFeeRate is gte to 1\"
\t\t);

\t\taddress sender = _msgSender();

\t\t_recipient = _args.recipient;
\t\temit RecipientSet(sender, _args.recipient);
\t\t_feeRateNum = _args.feeRateNumerator;
\t\t_feeRateDen = _args.feeRateDenominator;
\t\temit FeeRateSet(sender, _args.feeRateNumerator, _args.feeRateDenominator);

\t\tfor (uint256 i = 0; i < _args.exemptions.length; i++) {
\t\t\taddress account = _args.exemptions[i].account;
\t\t\tif (_args.exemptions[i].isExempt) {
\t\t\t\tif (_exempts.add(account)) {
\t\t\t\t\temit ExemptAdded(sender, account);
\t\t\t\t}
\t\t\t} else if (_exempts.remove(account)) {
\t\t\t\temit ExemptRemoved(sender, account);
\t\t\t}
\t\t}

\t\t_rebaseInterval = _args.rebaseInterval;
\t\temit RebaseIntervalSet(sender, _args.rebaseInterval);

\t\t_rebaseFeeRateNum = _args.rebaseFeeRateNum;
\t\t_rebaseFeeRateDen = _args.rebaseFeeRateDen;
\t\temit RebaseFeeRateSet(
\t\t\tsender,
\t\t\t_args.rebaseFeeRateNum,
\t\t\t_args.rebaseFeeRateDen
\t\t);

\t\tfor (uint256 i = 0; i < _args.rebaseExemptions.length; i++) {
\t\t\taddress account = _args.rebaseExemptions[i].account;
\t\t\tif (_args.rebaseExemptions[i].isExempt) {
\t\t\t\tif (_rebaseExempts.add(account)) {
\t\t\t\t\temit RebaseExemptAdded(sender, account);
\t\t\t\t}
\t\t\t} else if (_rebaseExempts.remove(account)) {
\t\t\t\temit RebaseExemptRemoved(sender, account);
\t\t\t}
\t\t}
\t}

\t/* External Views */

\tfunction exemptsAt(uint256 index)
\t\texternal
\t\tview
\t\tvirtual
\t\toverride
\t\treturns (address)
\t{
\t\treturn _exempts.at(index);
\t}

\tfunction exemptsLength() external view virtual override returns (uint256) {
\t\treturn _exempts.length();
\t}

\tfunction feeRate()
\t\texternal
\t\tview
\t\tvirtual
\t\toverride
\t\treturns (uint128 numerator, uint128 denominator)
\t{
\t\tnumerator = _feeRateNum;
\t\tdenominator = _feeRateDen;
\t}

\tfunction getFee(
\t\taddress sender,
\t\taddress, /* recipient_ */
\t\tuint256 amount
\t) external view virtual override returns (uint256) {
\t\tif (_exempts.contains(sender)) {
\t\t\treturn 0;
\t\t}
\t\treturn amount.mul(_feeRateNum) / _feeRateDen;
\t}

\tfunction getRebaseFee(uint256 amount)
\t\texternal
\t\tview
\t\tvirtual
\t\toverride
\t\treturns (uint256)
\t{
\t\treturn amount.mul(_rebaseFeeRateNum) / _rebaseFeeRateDen;
\t}

\tfunction isExempt(address account)
\t\texternal
\t\tview
\t\tvirtual
\t\toverride
\t\treturns (bool)
\t{
\t\treturn _exempts.contains(account);
\t}

\tfunction isRebaseExempt(address account)
\t\texternal
\t\tview
\t\tvirtual
\t\toverride
\t\treturns (bool)
\t{
\t\treturn _rebaseExempts.contains(account);
\t}

\tfunction rebaseExemptsAt(uint256 index)
\t\texternal
\t\tview
\t\tvirtual
\t\toverride
\t\treturns (address)
\t{
\t\treturn _rebaseExempts.at(index);
\t}

\tfunction rebaseExemptsLength()
\t\texternal
\t\tview
\t\tvirtual
\t\toverride
\t\treturns (uint256)
\t{
\t\treturn _rebaseExempts.length();
\t}

\tfunction rebaseFeeRate()
\t\texternal
\t\tview
\t\tvirtual
\t\toverride
\t\treturns (uint128 numerator, uint128 denominator)
\t{
\t\tnumerator = _rebaseFeeRateNum;
\t\tdenominator = _rebaseFeeRateDen;
\t}

\tfunction rebaseInterval() external view virtual override returns (uint256) {
\t\treturn _rebaseInterval;
\t}

\tfunction recipient() external view virtual override returns (address) {
\t\treturn _recipient;
\t}

\tfunction undoFee(
\t\taddress sender,
\t\taddress, /* recipient_ */
\t\tuint256 amount
\t) external view virtual override returns (uint256) {
\t\tif (_exempts.contains(sender)) {
\t\t\treturn amount;
\t\t}
\t\treturn amount.mul(_feeRateDen) / (_feeRateDen - _feeRateNum);
\t}

\tfunction undoRebaseFee(uint256 amount)
\t\texternal
\t\tview
\t\tvirtual
\t\toverride
\t\treturns (uint256)
\t{
\t\treturn
\t\t\tamount.mul(_rebaseFeeRateDen) / (_rebaseFeeRateDen - _rebaseFeeRateNum);
\t}

\t/* External Mutators */

\tfunction notify(
\t\tuint256 /* amount */
\t) external virtual override {
\t\treturn;
\t}

\tfunction setExempt(address account, bool isExempt_)
\t\tpublic
\t\tvirtual
\t\toverride
\t\tonlyOwner
\t{
\t\tif (isExempt_) {
\t\t\tif (_exempts.add(account)) {
\t\t\t\temit ExemptAdded(_msgSender(), account);
\t\t\t}
\t\t\treturn;
\t\t}
\t\tif (_exempts.remove(account)) {
\t\t\temit ExemptRemoved(_msgSender(), account);
\t\t}
\t}

\tfunction setExemptBatch(ExemptData[] memory batch)
\t\tpublic
\t\tvirtual
\t\toverride
\t\tonlyOwner
\t{
\t\tfor (uint256 i = 0; i < batch.length; i++) {
\t\t\tsetExempt(batch[i].account, batch[i].isExempt);
\t\t}
\t}

\tfunction setFeeRate(uint128 numerator, uint128 denominator)
\t\texternal
\t\tvirtual
\t\toverride
\t\tonlyOwner
\t{
\t\t// Also guarantees that the denominator cannot be zero.
\t\trequire(denominator > numerator, \"FeeLogic: feeRate is gte to 1\");
\t\t_feeRateNum = numerator;
\t\t_feeRateDen = denominator;
\t\temit FeeRateSet(_msgSender(), numerator, denominator);
\t}

\tfunction setRebaseExempt(address account, bool isExempt_)
\t\tpublic
\t\tvirtual
\t\toverride
\t\tonlyOwner
\t{
\t\tif (isExempt_) {
\t\t\tif (_rebaseExempts.add(account)) {
\t\t\t\temit RebaseExemptAdded(_msgSender(), account);
\t\t\t}
\t\t\treturn;
\t\t}
\t\tif (_rebaseExempts.remove(account)) {
\t\t\temit RebaseExemptRemoved(_msgSender(), account);
\t\t}
\t}

\tfunction setRebaseExemptBatch(ExemptData[] memory batch)
\t\tpublic
\t\tvirtual
\t\toverride
\t\tonlyOwner
\t{
\t\tfor (uint256 i = 0; i < batch.length; i++) {
\t\t\tsetRebaseExempt(batch[i].account, batch[i].isExempt);
\t\t}
\t}

\tfunction setRebaseFeeRate(uint128 numerator, uint128 denominator)
\t\texternal
\t\tvirtual
\t\toverride
\t\tonlyOwner
\t{
\t\t// Also guarantees that the denominator cannot be zero.
\t\trequire(denominator > numerator, \"FeeLogic: rebaseFeeRate is gte to 1\");
\t\t_rebaseFeeRateNum = numerator;
\t\t_rebaseFeeRateDen = denominator;
\t\temit RebaseFeeRateSet(_msgSender(), numerator, denominator);
\t}

\tfunction setRebaseInterval(uint256 interval)
\t\texternal
\t\tvirtual
\t\toverride
\t\tonlyOwner
\t{
\t\t_rebaseInterval = interval;
\t\temit RebaseIntervalSet(_msgSender(), interval);
\t}

\tfunction setRecipient(address account) external virtual override onlyOwner {
\t\trequire(account != address(0), \"FeeLogic: recipient is zero address\");
\t\t_recipient = account;
\t\temit RecipientSet(_msgSender(), account);
\t}
}
"
    
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IFeeLogic {
\t/* Types */

\tstruct ExemptData {
\t\taddress account;
\t\tbool isExempt;
\t}

\t/* Views */

\tfunction exemptsAt(uint256 index) external view returns (address);

\tfunction exemptsLength() external view returns (uint256);

\tfunction feeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction getFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction getRebaseFee(uint256 amount) external view returns (uint256);

\tfunction isExempt(address account) external view returns (bool);

\tfunction isRebaseExempt(address account) external view returns (bool);

\tfunction rebaseExemptsAt(uint256 index) external view returns (address);

\tfunction rebaseExemptsLength() external view returns (uint256);

\tfunction rebaseFeeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction rebaseInterval() external view returns (uint256);

\tfunction recipient() external view returns (address);

\tfunction undoFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction undoRebaseFee(uint256 amount) external view returns (uint256);

\t/* Mutators */

\tfunction notify(uint256 amount) external;

\tfunction setExempt(address account, bool isExempt_) external;

\tfunction setExemptBatch(ExemptData[] memory batch) external;

\tfunction setFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseExempt(address account, bool isExempt_) external;

\tfunction setRebaseExemptBatch(ExemptData[] memory batch) external;

\tfunction setRebaseFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseInterval(uint256 interval) external;

\tfunction setRecipient(address account) external;

\t/* Events */

\tevent ExemptAdded(address indexed author, address indexed account);
\tevent ExemptRemoved(address indexed author, address indexed account);
\tevent FeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseExemptAdded(address indexed author, address indexed account);
\tevent RebaseExemptRemoved(address indexed author, address indexed account);
\tevent RebaseFeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseIntervalSet(address indexed author, uint256 interval);
\tevent RecipientSet(address indexed author, address indexed account);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IFeeLogic {
\t/* Types */

\tstruct ExemptData {
\t\taddress account;
\t\tbool isExempt;
\t}

\t/* Views */

\tfunction exemptsAt(uint256 index) external view returns (address);

\tfunction exemptsLength() external view returns (uint256);

\tfunction feeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction getFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction getRebaseFee(uint256 amount) external view returns (uint256);

\tfunction isExempt(address account) external view returns (bool);

\tfunction isRebaseExempt(address account) external view returns (bool);

\tfunction rebaseExemptsAt(uint256 index) external view returns (address);

\tfunction rebaseExemptsLength() external view returns (uint256);

\tfunction rebaseFeeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction rebaseInterval() external view returns (uint256);

\tfunction recipient() external view returns (address);

\tfunction undoFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction undoRebaseFee(uint256 amount) external view returns (uint256);

\t/* Mutators */

\tfunction notify(uint256 amount) external;

\tfunction setExempt(address account, bool isExempt_) external;

\tfunction setExemptBatch(ExemptData[] memory batch) external;

\tfunction setFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseExempt(address account, bool isExempt_) external;

\tfunction setRebaseExemptBatch(ExemptData[] memory batch) external;

\tfunction setRebaseFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseInterval(uint256 interval) external;

\tfunction setRecipient(address account) external;

\t/* Events */

\tevent ExemptAdded(address indexed author, address indexed account);
\tevent ExemptRemoved(address indexed author, address indexed account);
\tevent FeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseExemptAdded(address indexed author, address indexed account);
\tevent RebaseExemptRemoved(address indexed author, address indexed account);
\tevent RebaseFeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseIntervalSet(address indexed author, uint256 interval);
\tevent RecipientSet(address indexed author, address indexed account);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IFeeLogic {
\t/* Types */

\tstruct ExemptData {
\t\taddress account;
\t\tbool isExempt;
\t}

\t/* Views */

\tfunction exemptsAt(uint256 index) external view returns (address);

\tfunction exemptsLength() external view returns (uint256);

\tfunction feeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction getFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction getRebaseFee(uint256 amount) external view returns (uint256);

\tfunction isExempt(address account) external view returns (bool);

\tfunction isRebaseExempt(address account) external view returns (bool);

\tfunction rebaseExemptsAt(uint256 index) external view returns (address);

\tfunction rebaseExemptsLength() external view returns (uint256);

\tfunction rebaseFeeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction rebaseInterval() external view returns (uint256);

\tfunction recipient() external view returns (address);

\tfunction undoFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction undoRebaseFee(uint256 amount) external view returns (uint256);

\t/* Mutators */

\tfunction notify(uint256 amount) external;

\tfunction setExempt(address account, bool isExempt_) external;

\tfunction setExemptBatch(ExemptData[] memory batch) external;

\tfunction setFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseExempt(address account, bool isExempt_) external;

\tfunction setRebaseExemptBatch(ExemptData[] memory batch) external;

\tfunction setRebaseFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseInterval(uint256 interval) external;

\tfunction setRecipient(address account) external;

\t/* Events */

\tevent ExemptAdded(address indexed author, address indexed account);
\tevent ExemptRemoved(address indexed author, address indexed account);
\tevent FeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseExemptAdded(address indexed author, address indexed account);
\tevent RebaseExemptRemoved(address indexed author, address indexed account);
\tevent RebaseFeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseIntervalSet(address indexed author, uint256 interval);
\tevent RecipientSet(address indexed author, address indexed account);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IFeeLogic {
\t/* Types */

\tstruct ExemptData {
\t\taddress account;
\t\tbool isExempt;
\t}

\t/* Views */

\tfunction exemptsAt(uint256 index) external view returns (address);

\tfunction exemptsLength() external view returns (uint256);

\tfunction feeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction getFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction getRebaseFee(uint256 amount) external view returns (uint256);

\tfunction isExempt(address account) external view returns (bool);

\tfunction isRebaseExempt(address account) external view returns (bool);

\tfunction rebaseExemptsAt(uint256 index) external view returns (address);

\tfunction rebaseExemptsLength() external view returns (uint256);

\tfunction rebaseFeeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction rebaseInterval() external view returns (uint256);

\tfunction recipient() external view returns (address);

\tfunction undoFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction undoRebaseFee(uint256 amount) external view returns (uint256);

\t/* Mutators */

\tfunction notify(uint256 amount) external;

\tfunction setExempt(address account, bool isExempt_) external;

\tfunction setExemptBatch(ExemptData[] memory batch) external;

\tfunction setFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseExempt(address account, bool isExempt_) external;

\tfunction setRebaseExemptBatch(ExemptData[] memory batch) external;

\tfunction setRebaseFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseInterval(uint256 interval) external;

\tfunction setRecipient(address account) external;

\t/* Events */

\tevent ExemptAdded(address indexed author, address indexed account);
\tevent ExemptRemoved(address indexed author, address indexed account);
\tevent FeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseExemptAdded(address indexed author, address indexed account);
\tevent RebaseExemptRemoved(address indexed author, address indexed account);
\tevent RebaseFeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseIntervalSet(address indexed author, uint256 interval);
\tevent RecipientSet(address indexed author, address indexed account);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IFeeLogic {
\t/* Types */

\tstruct ExemptData {
\t\taddress account;
\t\tbool isExempt;
\t}

\t/* Views */

\tfunction exemptsAt(uint256 index) external view returns (address);

\tfunction exemptsLength() external view returns (uint256);

\tfunction feeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction getFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction getRebaseFee(uint256 amount) external view returns (uint256);

\tfunction isExempt(address account) external view returns (bool);

\tfunction isRebaseExempt(address account) external view returns (bool);

\tfunction rebaseExemptsAt(uint256 index) external view returns (address);

\tfunction rebaseExemptsLength() external view returns (uint256);

\tfunction rebaseFeeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction rebaseInterval() external view returns (uint256);

\tfunction recipient() external view returns (address);

\tfunction undoFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction undoRebaseFee(uint256 amount) external view returns (uint256);

\t/* Mutators */

\tfunction notify(uint256 amount) external;

\tfunction setExempt(address account, bool isExempt_) external;

\tfunction setExemptBatch(ExemptData[] memory batch) external;

\tfunction setFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseExempt(address account, bool isExempt_) external;

\tfunction setRebaseExemptBatch(ExemptData[] memory batch) external;

\tfunction setRebaseFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseInterval(uint256 interval) external;

\tfunction setRecipient(address account) external;

\t/* Events */

\tevent ExemptAdded(address indexed author, address indexed account);
\tevent ExemptRemoved(address indexed author, address indexed account);
\tevent FeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseExemptAdded(address indexed author, address indexed account);
\tevent RebaseExemptRemoved(address indexed author, address indexed account);
\tevent RebaseFeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseIntervalSet(address indexed author, uint256 interval);
\tevent RecipientSet(address indexed author, address indexed account);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IFeeLogic {
\t/* Types */

\tstruct ExemptData {
\t\taddress account;
\t\tbool isExempt;
\t}

\t/* Views */

\tfunction exemptsAt(uint256 index) external view returns (address);

\tfunction exemptsLength() external view returns (uint256);

\tfunction feeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction getFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction getRebaseFee(uint256 amount) external view returns (uint256);

\tfunction isExempt(address account) external view returns (bool);

\tfunction isRebaseExempt(address account) external view returns (bool);

\tfunction rebaseExemptsAt(uint256 index) external view returns (address);

\tfunction rebaseExemptsLength() external view returns (uint256);

\tfunction rebaseFeeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction rebaseInterval() external view returns (uint256);

\tfunction recipient() external view returns (address);

\tfunction undoFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction undoRebaseFee(uint256 amount) external view returns (uint256);

\t/* Mutators */

\tfunction notify(uint256 amount) external;

\tfunction setExempt(address account, bool isExempt_) external;

\tfunction setExemptBatch(ExemptData[] memory batch) external;

\tfunction setFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseExempt(address account, bool isExempt_) external;

\tfunction setRebaseExemptBatch(ExemptData[] memory batch) external;

\tfunction setRebaseFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseInterval(uint256 interval) external;

\tfunction setRecipient(address account) external;

\t/* Events */

\tevent ExemptAdded(address indexed author, address indexed account);
\tevent ExemptRemoved(address indexed author, address indexed account);
\tevent FeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseExemptAdded(address indexed author, address indexed account);
\tevent RebaseExemptRemoved(address indexed author, address indexed account);
\tevent RebaseFeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseIntervalSet(address indexed author, uint256 interval);
\tevent RecipientSet(address indexed author, address indexed account);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IFeeLogic {
\t/* Types */

\tstruct ExemptData {
\t\taddress account;
\t\tbool isExempt;
\t}

\t/* Views */

\tfunction exemptsAt(uint256 index) external view returns (address);

\tfunction exemptsLength() external view returns (uint256);

\tfunction feeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction getFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction getRebaseFee(uint256 amount) external view returns (uint256);

\tfunction isExempt(address account) external view returns (bool);

\tfunction isRebaseExempt(address account) external view returns (bool);

\tfunction rebaseExemptsAt(uint256 index) external view returns (address);

\tfunction rebaseExemptsLength() external view returns (uint256);

\tfunction rebaseFeeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction rebaseInterval() external view returns (uint256);

\tfunction recipient() external view returns (address);

\tfunction undoFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction undoRebaseFee(uint256 amount) external view returns (uint256);

\t/* Mutators */

\tfunction notify(uint256 amount) external;

\tfunction setExempt(address account, bool isExempt_) external;

\tfunction setExemptBatch(ExemptData[] memory batch) external;

\tfunction setFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseExempt(address account, bool isExempt_) external;

\tfunction setRebaseExemptBatch(ExemptData[] memory batch) external;

\tfunction setRebaseFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseInterval(uint256 interval) external;

\tfunction setRecipient(address account) external;

\t/* Events */

\tevent ExemptAdded(address indexed author, address indexed account);
\tevent ExemptRemoved(address indexed author, address indexed account);
\tevent FeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseExemptAdded(address indexed author, address indexed account);
\tevent RebaseExemptRemoved(address indexed author, address indexed account);
\tevent RebaseFeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseIntervalSet(address indexed author, uint256 interval);
\tevent RecipientSet(address indexed author, address indexed account);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IFeeLogic {
\t/* Types */

\tstruct ExemptData {
\t\taddress account;
\t\tbool isExempt;
\t}

\t/* Views */

\tfunction exemptsAt(uint256 index) external view returns (address);

\tfunction exemptsLength() external view returns (uint256);

\tfunction feeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction getFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction getRebaseFee(uint256 amount) external view returns (uint256);

\tfunction isExempt(address account) external view returns (bool);

\tfunction isRebaseExempt(address account) external view returns (bool);

\tfunction rebaseExemptsAt(uint256 index) external view returns (address);

\tfunction rebaseExemptsLength() external view returns (uint256);

\tfunction rebaseFeeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction rebaseInterval() external view returns (uint256);

\tfunction recipient() external view returns (address);

\tfunction undoFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction undoRebaseFee(uint256 amount) external view returns (uint256);

\t/* Mutators */

\tfunction notify(uint256 amount) external;

\tfunction setExempt(address account, bool isExempt_) external;

\tfunction setExemptBatch(ExemptData[] memory batch) external;

\tfunction setFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseExempt(address account, bool isExempt_) external;

\tfunction setRebaseExemptBatch(ExemptData[] memory batch) external;

\tfunction setRebaseFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseInterval(uint256 interval) external;

\tfunction setRecipient(address account) external;

\t/* Events */

\tevent ExemptAdded(address indexed author, address indexed account);
\tevent ExemptRemoved(address indexed author, address indexed account);
\tevent FeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseExemptAdded(address indexed author, address indexed account);
\tevent RebaseExemptRemoved(address indexed author, address indexed account);
\tevent RebaseFeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseIntervalSet(address indexed author, uint256 interval);
\tevent RecipientSet(address indexed author, address indexed account);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IFeeLogic {
\t/* Types */

\tstruct ExemptData {
\t\taddress account;
\t\tbool isExempt;
\t}

\t/* Views */

\tfunction exemptsAt(uint256 index) external view returns (address);

\tfunction exemptsLength() external view returns (uint256);

\tfunction feeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction getFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction getRebaseFee(uint256 amount) external view returns (uint256);

\tfunction isExempt(address account) external view returns (bool);

\tfunction isRebaseExempt(address account) external view returns (bool);

\tfunction rebaseExemptsAt(uint256 index) external view returns (address);

\tfunction rebaseExemptsLength() external view returns (uint256);

\tfunction rebaseFeeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction rebaseInterval() external view returns (uint256);

\tfunction recipient() external view returns (address);

\tfunction undoFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction undoRebaseFee(uint256 amount) external view returns (uint256);

\t/* Mutators */

\tfunction notify(uint256 amount) external;

\tfunction setExempt(address account, bool isExempt_) external;

\tfunction setExemptBatch(ExemptData[] memory batch) external;

\tfunction setFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseExempt(address account, bool isExempt_) external;

\tfunction setRebaseExemptBatch(ExemptData[] memory batch) external;

\tfunction setRebaseFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseInterval(uint256 interval) external;

\tfunction setRecipient(address account) external;

\t/* Events */

\tevent ExemptAdded(address indexed author, address indexed account);
\tevent ExemptRemoved(address indexed author, address indexed account);
\tevent FeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseExemptAdded(address indexed author, address indexed account);
\tevent RebaseExemptRemoved(address indexed author, address indexed account);
\tevent RebaseFeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseIntervalSet(address indexed author, uint256 interval);
\tevent RecipientSet(address indexed author, address indexed account);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IFeeLogic {
\t/* Types */

\tstruct ExemptData {
\t\taddress account;
\t\tbool isExempt;
\t}

\t/* Views */

\tfunction exemptsAt(uint256 index) external view returns (address);

\tfunction exemptsLength() external view returns (uint256);

\tfunction feeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction getFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction getRebaseFee(uint256 amount) external view returns (uint256);

\tfunction isExempt(address account) external view returns (bool);

\tfunction isRebaseExempt(address account) external view returns (bool);

\tfunction rebaseExemptsAt(uint256 index) external view returns (address);

\tfunction rebaseExemptsLength() external view returns (uint256);

\tfunction rebaseFeeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction rebaseInterval() external view returns (uint256);

\tfunction recipient() external view returns (address);

\tfunction undoFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction undoRebaseFee(uint256 amount) external view returns (uint256);

\t/* Mutators */

\tfunction notify(uint256 amount) external;

\tfunction setExempt(address account, bool isExempt_) external;

\tfunction setExemptBatch(ExemptData[] memory batch) external;

\tfunction setFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseExempt(address account, bool isExempt_) external;

\tfunction setRebaseExemptBatch(ExemptData[] memory batch) external;

\tfunction setRebaseFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseInterval(uint256 interval) external;

\tfunction setRecipient(address account) external;

\t/* Events */

\tevent ExemptAdded(address indexed author, address indexed account);
\tevent ExemptRemoved(address indexed author, address indexed account);
\tevent FeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseExemptAdded(address indexed author, address indexed account);
\tevent RebaseExemptRemoved(address indexed author, address indexed account);
\tevent RebaseFeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseIntervalSet(address indexed author, uint256 interval);
\tevent RecipientSet(address indexed author, address indexed account);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IFeeLogic {
\t/* Types */

\tstruct ExemptData {
\t\taddress account;
\t\tbool isExempt;
\t}

\t/* Views */

\tfunction exemptsAt(uint256 index) external view returns (address);

\tfunction exemptsLength() external view returns (uint256);

\tfunction feeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction getFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction getRebaseFee(uint256 amount) external view returns (uint256);

\tfunction isExempt(address account) external view returns (bool);

\tfunction isRebaseExempt(address account) external view returns (bool);

\tfunction rebaseExemptsAt(uint256 index) external view returns (address);

\tfunction rebaseExemptsLength() external view returns (uint256);

\tfunction rebaseFeeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction rebaseInterval() external view returns (uint256);

\tfunction recipient() external view returns (address);

\tfunction undoFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction undoRebaseFee(uint256 amount) external view returns (uint256);

\t/* Mutators */

\tfunction notify(uint256 amount) external;

\tfunction setExempt(address account, bool isExempt_) external;

\tfunction setExemptBatch(ExemptData[] memory batch) external;

\tfunction setFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseExempt(address account, bool isExempt_) external;

\tfunction setRebaseExemptBatch(ExemptData[] memory batch) external;

\tfunction setRebaseFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseInterval(uint256 interval) external;

\tfunction setRecipient(address account) external;

\t/* Events */

\tevent ExemptAdded(address indexed author, address indexed account);
\tevent ExemptRemoved(address indexed author, address indexed account);
\tevent FeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseExemptAdded(address indexed author, address indexed account);
\tevent RebaseExemptRemoved(address indexed author, address indexed account);
\tevent RebaseFeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseIntervalSet(address indexed author, uint256 interval);
\tevent RecipientSet(address indexed author, address indexed account);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IFeeLogic {
\t/* Types */

\tstruct ExemptData {
\t\taddress account;
\t\tbool isExempt;
\t}

\t/* Views */

\tfunction exemptsAt(uint256 index) external view returns (address);

\tfunction exemptsLength() external view returns (uint256);

\tfunction feeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction getFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction getRebaseFee(uint256 amount) external view returns (uint256);

\tfunction isExempt(address account) external view returns (bool);

\tfunction isRebaseExempt(address account) external view returns (bool);

\tfunction rebaseExemptsAt(uint256 index) external view returns (address);

\tfunction rebaseExemptsLength() external view returns (uint256);

\tfunction rebaseFeeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction rebaseInterval() external view returns (uint256);

\tfunction recipient() external view returns (address);

\tfunction undoFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction undoRebaseFee(uint256 amount) external view returns (uint256);

\t/* Mutators */

\tfunction notify(uint256 amount) external;

\tfunction setExempt(address account, bool isExempt_) external;

\tfunction setExemptBatch(ExemptData[] memory batch) external;

\tfunction setFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseExempt(address account, bool isExempt_) external;

\tfunction setRebaseExemptBatch(ExemptData[] memory batch) external;

\tfunction setRebaseFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseInterval(uint256 interval) external;

\tfunction setRecipient(address account) external;

\t/* Events */

\tevent ExemptAdded(address indexed author, address indexed account);
\tevent ExemptRemoved(address indexed author, address indexed account);
\tevent FeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseExemptAdded(address indexed author, address indexed account);
\tevent RebaseExemptRemoved(address indexed author, address indexed account);
\tevent RebaseFeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseIntervalSet(address indexed author, uint256 interval);
\tevent RecipientSet(address indexed author, address indexed account);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IFeeLogic {
\t/* Types */

\tstruct ExemptData {
\t\taddress account;
\t\tbool isExempt;
\t}

\t/* Views */

\tfunction exemptsAt(uint256 index) external view returns (address);

\tfunction exemptsLength() external view returns (uint256);

\tfunction feeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction getFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction getRebaseFee(uint256 amount) external view returns (uint256);

\tfunction isExempt(address account) external view returns (bool);

\tfunction isRebaseExempt(address account) external view returns (bool);

\tfunction rebaseExemptsAt(uint256 index) external view returns (address);

\tfunction rebaseExemptsLength() external view returns (uint256);

\tfunction rebaseFeeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction rebaseInterval() external view returns (uint256);

\tfunction recipient() external view returns (address);

\tfunction undoFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction undoRebaseFee(uint256 amount) external view returns (uint256);

\t/* Mutators */

\tfunction notify(uint256 amount) external;

\tfunction setExempt(address account, bool isExempt_) external;

\tfunction setExemptBatch(ExemptData[] memory batch) external;

\tfunction setFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseExempt(address account, bool isExempt_) external;

\tfunction setRebaseExemptBatch(ExemptData[] memory batch) external;

\tfunction setRebaseFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseInterval(uint256 interval) external;

\tfunction setRecipient(address account) external;

\t/* Events */

\tevent ExemptAdded(address indexed author, address indexed account);
\tevent ExemptRemoved(address indexed author, address indexed account);
\tevent FeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseExemptAdded(address indexed author, address indexed account);
\tevent RebaseExemptRemoved(address indexed author, address indexed account);
\tevent RebaseFeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseIntervalSet(address indexed author, uint256 interval);
\tevent RecipientSet(address indexed author, address indexed account);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IFeeLogic {
\t/* Types */

\tstruct ExemptData {
\t\taddress account;
\t\tbool isExempt;
\t}

\t/* Views */

\tfunction exemptsAt(uint256 index) external view returns (address);

\tfunction exemptsLength() external view returns (uint256);

\tfunction feeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction getFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction getRebaseFee(uint256 amount) external view returns (uint256);

\tfunction isExempt(address account) external view returns (bool);

\tfunction isRebaseExempt(address account) external view returns (bool);

\tfunction rebaseExemptsAt(uint256 index) external view returns (address);

\tfunction rebaseExemptsLength() external view returns (uint256);

\tfunction rebaseFeeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction rebaseInterval() external view returns (uint256);

\tfunction recipient() external view returns (address);

\tfunction undoFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction undoRebaseFee(uint256 amount) external view returns (uint256);

\t/* Mutators */

\tfunction notify(uint256 amount) external;

\tfunction setExempt(address account, bool isExempt_) external;

\tfunction setExemptBatch(ExemptData[] memory batch) external;

\tfunction setFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseExempt(address account, bool isExempt_) external;

\tfunction setRebaseExemptBatch(ExemptData[] memory batch) external;

\tfunction setRebaseFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseInterval(uint256 interval) external;

\tfunction setRecipient(address account) external;

\t/* Events */

\tevent ExemptAdded(address indexed author, address indexed account);
\tevent ExemptRemoved(address indexed author, address indexed account);
\tevent FeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseExemptAdded(address indexed author, address indexed account);
\tevent RebaseExemptRemoved(address indexed author, address indexed account);
\tevent RebaseFeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseIntervalSet(address indexed author, uint256 interval);
\tevent RecipientSet(address indexed author, address indexed account);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IFeeLogic {
\t/* Types */

\tstruct ExemptData {
\t\taddress account;
\t\tbool isExempt;
\t}

\t/* Views */

\tfunction exemptsAt(uint256 index) external view returns (address);

\tfunction exemptsLength() external view returns (uint256);

\tfunction feeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction getFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction getRebaseFee(uint256 amount) external view returns (uint256);

\tfunction isExempt(address account) external view returns (bool);

\tfunction isRebaseExempt(address account) external view returns (bool);

\tfunction rebaseExemptsAt(uint256 index) external view returns (address);

\tfunction rebaseExemptsLength() external view returns (uint256);

\tfunction rebaseFeeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction rebaseInterval() external view returns (uint256);

\tfunction recipient() external view returns (address);

\tfunction undoFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction undoRebaseFee(uint256 amount) external view returns (uint256);

\t/* Mutators */

\tfunction notify(uint256 amount) external;

\tfunction setExempt(address account, bool isExempt_) external;

\tfunction setExemptBatch(ExemptData[] memory batch) external;

\tfunction setFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseExempt(address account, bool isExempt_) external;

\tfunction setRebaseExemptBatch(ExemptData[] memory batch) external;

\tfunction setRebaseFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseInterval(uint256 interval) external;

\tfunction setRecipient(address account) external;

\t/* Events */

\tevent ExemptAdded(address indexed author, address indexed account);
\tevent ExemptRemoved(address indexed author, address indexed account);
\tevent FeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseExemptAdded(address indexed author, address indexed account);
\tevent RebaseExemptRemoved(address indexed author, address indexed account);
\tevent RebaseFeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseIntervalSet(address indexed author, uint256 interval);
\tevent RecipientSet(address indexed author, address indexed account);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IFeeLogic {
\t/* Types */

\tstruct ExemptData {
\t\taddress account;
\t\tbool isExempt;
\t}

\t/* Views */

\tfunction exemptsAt(uint256 index) external view returns (address);

\tfunction exemptsLength() external view returns (uint256);

\tfunction feeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction getFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction getRebaseFee(uint256 amount) external view returns (uint256);

\tfunction isExempt(address account) external view returns (bool);

\tfunction isRebaseExempt(address account) external view returns (bool);

\tfunction rebaseExemptsAt(uint256 index) external view returns (address);

\tfunction rebaseExemptsLength() external view returns (uint256);

\tfunction rebaseFeeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction rebaseInterval() external view returns (uint256);

\tfunction recipient() external view returns (address);

\tfunction undoFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction undoRebaseFee(uint256 amount) external view returns (uint256);

\t/* Mutators */

\tfunction notify(uint256 amount) external;

\tfunction setExempt(address account, bool isExempt_) external;

\tfunction setExemptBatch(ExemptData[] memory batch) external;

\tfunction setFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseExempt(address account, bool isExempt_) external;

\tfunction setRebaseExemptBatch(ExemptData[] memory batch) external;

\tfunction setRebaseFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseInterval(uint256 interval) external;

\tfunction setRecipient(address account) external;

\t/* Events */

\tevent ExemptAdded(address indexed author, address indexed account);
\tevent ExemptRemoved(address indexed author, address indexed account);
\tevent FeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseExemptAdded(address indexed author, address indexed account);
\tevent RebaseExemptRemoved(address indexed author, address indexed account);
\tevent RebaseFeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseIntervalSet(address indexed author, uint256 interval);
\tevent RecipientSet(address indexed author, address indexed account);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IFeeLogic {
\t/* Types */

\tstruct ExemptData {
\t\taddress account;
\t\tbool isExempt;
\t}

\t/* Views */

\tfunction exemptsAt(uint256 index) external view returns (address);

\tfunction exemptsLength() external view returns (uint256);

\tfunction feeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction getFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction getRebaseFee(uint256 amount) external view returns (uint256);

\tfunction isExempt(address account) external view returns (bool);

\tfunction isRebaseExempt(address account) external view returns (bool);

\tfunction rebaseExemptsAt(uint256 index) external view returns (address);

\tfunction rebaseExemptsLength() external view returns (uint256);

\tfunction rebaseFeeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction rebaseInterval() external view returns (uint256);

\tfunction recipient() external view returns (address);

\tfunction undoFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction undoRebaseFee(uint256 amount) external view returns (uint256);

\t/* Mutators */

\tfunction notify(uint256 amount) external;

\tfunction setExempt(address account, bool isExempt_) external;

\tfunction setExemptBatch(ExemptData[] memory batch) external;

\tfunction setFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseExempt(address account, bool isExempt_) external;

\tfunction setRebaseExemptBatch(ExemptData[] memory batch) external;

\tfunction setRebaseFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseInterval(uint256 interval) external;

\tfunction setRecipient(address account) external;

\t/* Events */

\tevent ExemptAdded(address indexed author, address indexed account);
\tevent ExemptRemoved(address indexed author, address indexed account);
\tevent FeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseExemptAdded(address indexed author, address indexed account);
\tevent RebaseExemptRemoved(address indexed author, address indexed account);
\tevent RebaseFeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseIntervalSet(address indexed author, uint256 interval);
\tevent RecipientSet(address indexed author, address indexed account);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IFeeLogic {
\t/* Types */

\tstruct ExemptData {
\t\taddress account;
\t\tbool isExempt;
\t}

\t/* Views */

\tfunction exemptsAt(uint256 index) external view returns (address);

\tfunction exemptsLength() external view returns (uint256);

\tfunction feeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction getFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction getRebaseFee(uint256 amount) external view returns (uint256);

\tfunction isExempt(address account) external view returns (bool);

\tfunction isRebaseExempt(address account) external view returns (bool);

\tfunction rebaseExemptsAt(uint256 index) external view returns (address);

\tfunction rebaseExemptsLength() external view returns (uint256);

\tfunction rebaseFeeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction rebaseInterval() external view returns (uint256);

\tfunction recipient() external view returns (address);

\tfunction undoFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction undoRebaseFee(uint256 amount) external view returns (uint256);

\t/* Mutators */

\tfunction notify(uint256 amount) external;

\tfunction setExempt(address account, bool isExempt_) external;

\tfunction setExemptBatch(ExemptData[] memory batch) external;

\tfunction setFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseExempt(address account, bool isExempt_) external;

\tfunction setRebaseExemptBatch(ExemptData[] memory batch) external;

\tfunction setRebaseFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseInterval(uint256 interval) external;

\tfunction setRecipient(address account) external;

\t/* Events */

\tevent ExemptAdded(address indexed author, address indexed account);
\tevent ExemptRemoved(address indexed author, address indexed account);
\tevent FeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseExemptAdded(address indexed author, address indexed account);
\tevent RebaseExemptRemoved(address indexed author, address indexed account);
\tevent RebaseFeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseIntervalSet(address indexed author, uint256 interval);
\tevent RecipientSet(address indexed author, address indexed account);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IFeeLogic {
\t/* Types */

\tstruct ExemptData {
\t\taddress account;
\t\tbool isExempt;
\t}

\t/* Views */

\tfunction exemptsAt(uint256 index) external view returns (address);

\tfunction exemptsLength() external view returns (uint256);

\tfunction feeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction getFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction getRebaseFee(uint256 amount) external view returns (uint256);

\tfunction isExempt(address account) external view returns (bool);

\tfunction isRebaseExempt(address account) external view returns (bool);

\tfunction rebaseExemptsAt(uint256 index) external view returns (address);

\tfunction rebaseExemptsLength() external view returns (uint256);

\tfunction rebaseFeeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction rebaseInterval() external view returns (uint256);

\tfunction recipient() external view returns (address);

\tfunction undoFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction undoRebaseFee(uint256 amount) external view returns (uint256);

\t/* Mutators */

\tfunction notify(uint256 amount) external;

\tfunction setExempt(address account, bool isExempt_) external;

\tfunction setExemptBatch(ExemptData[] memory batch) external;

\tfunction setFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseExempt(address account, bool isExempt_) external;

\tfunction setRebaseExemptBatch(ExemptData[] memory batch) external;

\tfunction setRebaseFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseInterval(uint256 interval) external;

\tfunction setRecipient(address account) external;

\t/* Events */

\tevent ExemptAdded(address indexed author, address indexed account);
\tevent ExemptRemoved(address indexed author, address indexed account);
\tevent FeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseExemptAdded(address indexed author, address indexed account);
\tevent RebaseExemptRemoved(address indexed author, address indexed account);
\tevent RebaseFeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseIntervalSet(address indexed author, uint256 interval);
\tevent RecipientSet(address indexed author, address indexed account);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IFeeLogic {
\t/* Types */

\tstruct ExemptData {
\t\taddress account;
\t\tbool isExempt;
\t}

\t/* Views */

\tfunction exemptsAt(uint256 index) external view returns (address);

\tfunction exemptsLength() external view returns (uint256);

\tfunction feeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction getFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction getRebaseFee(uint256 amount) external view returns (uint256);

\tfunction isExempt(address account) external view returns (bool);

\tfunction isRebaseExempt(address account) external view returns (bool);

\tfunction rebaseExemptsAt(uint256 index) external view returns (address);

\tfunction rebaseExemptsLength() external view returns (uint256);

\tfunction rebaseFeeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction rebaseInterval() external view returns (uint256);

\tfunction recipient() external view returns (address);

\tfunction undoFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction undoRebaseFee(uint256 amount) external view returns (uint256);

\t/* Mutators */

\tfunction notify(uint256 amount) external;

\tfunction setExempt(address account, bool isExempt_) external;

\tfunction setExemptBatch(ExemptData[] memory batch) external;

\tfunction setFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseExempt(address account, bool isExempt_) external;

\tfunction setRebaseExemptBatch(ExemptData[] memory batch) external;

\tfunction setRebaseFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseInterval(uint256 interval) external;

\tfunction setRecipient(address account) external;

\t/* Events */

\tevent ExemptAdded(address indexed author, address indexed account);
\tevent ExemptRemoved(address indexed author, address indexed account);
\tevent FeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseExemptAdded(address indexed author, address indexed account);
\tevent RebaseExemptRemoved(address indexed author, address indexed account);
\tevent RebaseFeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseIntervalSet(address indexed author, uint256 interval);
\tevent RecipientSet(address indexed author, address indexed account);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IFeeLogic {
\t/* Types */

\tstruct ExemptData {
\t\taddress account;
\t\tbool isExempt;
\t}

\t/* Views */

\tfunction exemptsAt(uint256 index) external view returns (address);

\tfunction exemptsLength() external view returns (uint256);

\tfunction feeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction getFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction getRebaseFee(uint256 amount) external view returns (uint256);

\tfunction isExempt(address account) external view returns (bool);

\tfunction isRebaseExempt(address account) external view returns (bool);

\tfunction rebaseExemptsAt(uint256 index) external view returns (address);

\tfunction rebaseExemptsLength() external view returns (uint256);

\tfunction rebaseFeeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction rebaseInterval() external view returns (uint256);

\tfunction recipient() external view returns (address);

\tfunction undoFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction undoRebaseFee(uint256 amount) external view returns (uint256);

\t/* Mutators */

\tfunction notify(uint256 amount) external;

\tfunction setExempt(address account, bool isExempt_) external;

\tfunction setExemptBatch(ExemptData[] memory batch) external;

\tfunction setFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseExempt(address account, bool isExempt_) external;

\tfunction setRebaseExemptBatch(ExemptData[] memory batch) external;

\tfunction setRebaseFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseInterval(uint256 interval) external;

\tfunction setRecipient(address account) external;

\t/* Events */

\tevent ExemptAdded(address indexed author, address indexed account);
\tevent ExemptRemoved(address indexed author, address indexed account);
\tevent FeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseExemptAdded(address indexed author, address indexed account);
\tevent RebaseExemptRemoved(address indexed author, address indexed account);
\tevent RebaseFeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseIntervalSet(address indexed author, uint256 interval);
\tevent RecipientSet(address indexed author, address indexed account);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IFeeLogic {
\t/* Types */

\tstruct ExemptData {
\t\taddress account;
\t\tbool isExempt;
\t}

\t/* Views */

\tfunction exemptsAt(uint256 index) external view returns (address);

\tfunction exemptsLength() external view returns (uint256);

\tfunction feeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction getFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction getRebaseFee(uint256 amount) external view returns (uint256);

\tfunction isExempt(address account) external view returns (bool);

\tfunction isRebaseExempt(address account) external view returns (bool);

\tfunction rebaseExemptsAt(uint256 index) external view returns (address);

\tfunction rebaseExemptsLength() external view returns (uint256);

\tfunction rebaseFeeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction rebaseInterval() external view returns (uint256);

\tfunction recipient() external view returns (address);

\tfunction undoFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction undoRebaseFee(uint256 amount) external view returns (uint256);

\t/* Mutators */

\tfunction notify(uint256 amount) external;

\tfunction setExempt(address account, bool isExempt_) external;

\tfunction setExemptBatch(ExemptData[] memory batch) external;

\tfunction setFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseExempt(address account, bool isExempt_) external;

\tfunction setRebaseExemptBatch(ExemptData[] memory batch) external;

\tfunction setRebaseFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseInterval(uint256 interval) external;

\tfunction setRecipient(address account) external;

\t/* Events */

\tevent ExemptAdded(address indexed author, address indexed account);
\tevent ExemptRemoved(address indexed author, address indexed account);
\tevent FeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseExemptAdded(address indexed author, address indexed account);
\tevent RebaseExemptRemoved(address indexed author, address indexed account);
\tevent RebaseFeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseIntervalSet(address indexed author, uint256 interval);
\tevent RecipientSet(address indexed author, address indexed account);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IFeeLogic {
\t/* Types */

\tstruct ExemptData {
\t\taddress account;
\t\tbool isExempt;
\t}

\t/* Views */

\tfunction exemptsAt(uint256 index) external view returns (address);

\tfunction exemptsLength() external view returns (uint256);

\tfunction feeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction getFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction getRebaseFee(uint256 amount) external view returns (uint256);

\tfunction isExempt(address account) external view returns (bool);

\tfunction isRebaseExempt(address account) external view returns (bool);

\tfunction rebaseExemptsAt(uint256 index) external view returns (address);

\tfunction rebaseExemptsLength() external view returns (uint256);

\tfunction rebaseFeeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction rebaseInterval() external view returns (uint256);

\tfunction recipient() external view returns (address);

\tfunction undoFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction undoRebaseFee(uint256 amount) external view returns (uint256);

\t/* Mutators */

\tfunction notify(uint256 amount) external;

\tfunction setExempt(address account, bool isExempt_) external;

\tfunction setExemptBatch(ExemptData[] memory batch) external;

\tfunction setFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseExempt(address account, bool isExempt_) external;

\tfunction setRebaseExemptBatch(ExemptData[] memory batch) external;

\tfunction setRebaseFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseInterval(uint256 interval) external;

\tfunction setRecipient(address account) external;

\t/* Events */

\tevent ExemptAdded(address indexed author, address indexed account);
\tevent ExemptRemoved(address indexed author, address indexed account);
\tevent FeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseExemptAdded(address indexed author, address indexed account);
\tevent RebaseExemptRemoved(address indexed author, address indexed account);
\tevent RebaseFeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseIntervalSet(address indexed author, uint256 interval);
\tevent RecipientSet(address indexed author, address indexed account);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IFeeLogic {
\t/* Types */

\tstruct ExemptData {
\t\taddress account;
\t\tbool isExempt;
\t}

\t/* Views */

\tfunction exemptsAt(uint256 index) external view returns (address);

\tfunction exemptsLength() external view returns (uint256);

\tfunction feeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction getFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction getRebaseFee(uint256 amount) external view returns (uint256);

\tfunction isExempt(address account) external view returns (bool);

\tfunction isRebaseExempt(address account) external view returns (bool);

\tfunction rebaseExemptsAt(uint256 index) external view returns (address);

\tfunction rebaseExemptsLength() external view returns (uint256);

\tfunction rebaseFeeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction rebaseInterval() external view returns (uint256);

\tfunction recipient() external view returns (address);

\tfunction undoFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction undoRebaseFee(uint256 amount) external view returns (uint256);

\t/* Mutators */

\tfunction notify(uint256 amount) external;

\tfunction setExempt(address account, bool isExempt_) external;

\tfunction setExemptBatch(ExemptData[] memory batch) external;

\tfunction setFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseExempt(address account, bool isExempt_) external;

\tfunction setRebaseExemptBatch(ExemptData[] memory batch) external;

\tfunction setRebaseFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseInterval(uint256 interval) external;

\tfunction setRecipient(address account) external;

\t/* Events */

\tevent ExemptAdded(address indexed author, address indexed account);
\tevent ExemptRemoved(address indexed author, address indexed account);
\tevent FeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseExemptAdded(address indexed author, address indexed account);
\tevent RebaseExemptRemoved(address indexed author, address indexed account);
\tevent RebaseFeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseIntervalSet(address indexed author, uint256 interval);
\tevent RecipientSet(address indexed author, address indexed account);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IFeeLogic {
\t/* Types */

\tstruct ExemptData {
\t\taddress account;
\t\tbool isExempt;
\t}

\t/* Views */

\tfunction exemptsAt(uint256 index) external view returns (address);

\tfunction exemptsLength() external view returns (uint256);

\tfunction feeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction getFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction getRebaseFee(uint256 amount) external view returns (uint256);

\tfunction isExempt(address account) external view returns (bool);

\tfunction isRebaseExempt(address account) external view returns (bool);

\tfunction rebaseExemptsAt(uint256 index) external view returns (address);

\tfunction rebaseExemptsLength() external view returns (uint256);

\tfunction rebaseFeeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction rebaseInterval() external view returns (uint256);

\tfunction recipient() external view returns (address);

\tfunction undoFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction undoRebaseFee(uint256 amount) external view returns (uint256);

\t/* Mutators */

\tfunction notify(uint256 amount) external;

\tfunction setExempt(address account, bool isExempt_) external;

\tfunction setExemptBatch(ExemptData[] memory batch) external;

\tfunction setFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseExempt(address account, bool isExempt_) external;

\tfunction setRebaseExemptBatch(ExemptData[] memory batch) external;

\tfunction setRebaseFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseInterval(uint256 interval) external;

\tfunction setRecipient(address account) external;

\t/* Events */

\tevent ExemptAdded(address indexed author, address indexed account);
\tevent ExemptRemoved(address indexed author, address indexed account);
\tevent FeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseExemptAdded(address indexed author, address indexed account);
\tevent RebaseExemptRemoved(address indexed author, address indexed account);
\tevent RebaseFeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseIntervalSet(address indexed author, uint256 interval);
\tevent RecipientSet(address indexed author, address indexed account);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IFeeLogic {
\t/* Types */

\tstruct ExemptData {
\t\taddress account;
\t\tbool isExempt;
\t}

\t/* Views */

\tfunction exemptsAt(uint256 index) external view returns (address);

\tfunction exemptsLength() external view returns (uint256);

\tfunction feeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction getFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction getRebaseFee(uint256 amount) external view returns (uint256);

\tfunction isExempt(address account) external view returns (bool);

\tfunction isRebaseExempt(address account) external view returns (bool);

\tfunction rebaseExemptsAt(uint256 index) external view returns (address);

\tfunction rebaseExemptsLength() external view returns (uint256);

\tfunction rebaseFeeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction rebaseInterval() external view returns (uint256);

\tfunction recipient() external view returns (address);

\tfunction undoFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction undoRebaseFee(uint256 amount) external view returns (uint256);

\t/* Mutators */

\tfunction notify(uint256 amount) external;

\tfunction setExempt(address account, bool isExempt_) external;

\tfunction setExemptBatch(ExemptData[] memory batch) external;

\tfunction setFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseExempt(address account, bool isExempt_) external;

\tfunction setRebaseExemptBatch(ExemptData[] memory batch) external;

\tfunction setRebaseFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseInterval(uint256 interval) external;

\tfunction setRecipient(address account) external;

\t/* Events */

\tevent ExemptAdded(address indexed author, address indexed account);
\tevent ExemptRemoved(address indexed author, address indexed account);
\tevent FeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseExemptAdded(address indexed author, address indexed account);
\tevent RebaseExemptRemoved(address indexed author, address indexed account);
\tevent RebaseFeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseIntervalSet(address indexed author, uint256 interval);
\tevent RecipientSet(address indexed author, address indexed account);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IFeeLogic {
\t/* Types */

\tstruct ExemptData {
\t\taddress account;
\t\tbool isExempt;
\t}

\t/* Views */

\tfunction exemptsAt(uint256 index) external view returns (address);

\tfunction exemptsLength() external view returns (uint256);

\tfunction feeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction getFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction getRebaseFee(uint256 amount) external view returns (uint256);

\tfunction isExempt(address account) external view returns (bool);

\tfunction isRebaseExempt(address account) external view returns (bool);

\tfunction rebaseExemptsAt(uint256 index) external view returns (address);

\tfunction rebaseExemptsLength() external view returns (uint256);

\tfunction rebaseFeeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction rebaseInterval() external view returns (uint256);

\tfunction recipient() external view returns (address);

\tfunction undoFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction undoRebaseFee(uint256 amount) external view returns (uint256);

\t/* Mutators */

\tfunction notify(uint256 amount) external;

\tfunction setExempt(address account, bool isExempt_) external;

\tfunction setExemptBatch(ExemptData[] memory batch) external;

\tfunction setFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseExempt(address account, bool isExempt_) external;

\tfunction setRebaseExemptBatch(ExemptData[] memory batch) external;

\tfunction setRebaseFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseInterval(uint256 interval) external;

\tfunction setRecipient(address account) external;

\t/* Events */

\tevent ExemptAdded(address indexed author, address indexed account);
\tevent ExemptRemoved(address indexed author, address indexed account);
\tevent FeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseExemptAdded(address indexed author, address indexed account);
\tevent RebaseExemptRemoved(address indexed author, address indexed account);
\tevent RebaseFeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseIntervalSet(address indexed author, uint256 interval);
\tevent RecipientSet(address indexed author, address indexed account);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IFeeLogic {
\t/* Types */

\tstruct ExemptData {
\t\taddress account;
\t\tbool isExempt;
\t}

\t/* Views */

\tfunction exemptsAt(uint256 index) external view returns (address);

\tfunction exemptsLength() external view returns (uint256);

\tfunction feeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction getFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction getRebaseFee(uint256 amount) external view returns (uint256);

\tfunction isExempt(address account) external view returns (bool);

\tfunction isRebaseExempt(address account) external view returns (bool);

\tfunction rebaseExemptsAt(uint256 index) external view returns (address);

\tfunction rebaseExemptsLength() external view returns (uint256);

\tfunction rebaseFeeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction rebaseInterval() external view returns (uint256);

\tfunction recipient() external view returns (address);

\tfunction undoFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction undoRebaseFee(uint256 amount) external view returns (uint256);

\t/* Mutators */

\tfunction notify(uint256 amount) external;

\tfunction setExempt(address account, bool isExempt_) external;

\tfunction setExemptBatch(ExemptData[] memory batch) external;

\tfunction setFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseExempt(address account, bool isExempt_) external;

\tfunction setRebaseExemptBatch(ExemptData[] memory batch) external;

\tfunction setRebaseFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseInterval(uint256 interval) external;

\tfunction setRecipient(address account) external;

\t/* Events */

\tevent ExemptAdded(address indexed author, address indexed account);
\tevent ExemptRemoved(address indexed author, address indexed account);
\tevent FeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseExemptAdded(address indexed author, address indexed account);
\tevent RebaseExemptRemoved(address indexed author, address indexed account);
\tevent RebaseFeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseIntervalSet(address indexed author, uint256 interval);
\tevent RecipientSet(address indexed author, address indexed account);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IFeeLogic {
\t/* Types */

\tstruct ExemptData {
\t\taddress account;
\t\tbool isExempt;
\t}

\t/* Views */

\tfunction exemptsAt(uint256 index) external view returns (address);

\tfunction exemptsLength() external view returns (uint256);

\tfunction feeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction getFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction getRebaseFee(uint256 amount) external view returns (uint256);

\tfunction isExempt(address account) external view returns (bool);

\tfunction isRebaseExempt(address account) external view returns (bool);

\tfunction rebaseExemptsAt(uint256 index) external view returns (address);

\tfunction rebaseExemptsLength() external view returns (uint256);

\tfunction rebaseFeeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction rebaseInterval() external view returns (uint256);

\tfunction recipient() external view returns (address);

\tfunction undoFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction undoRebaseFee(uint256 amount) external view returns (uint256);

\t/* Mutators */

\tfunction notify(uint256 amount) external;

\tfunction setExempt(address account, bool isExempt_) external;

\tfunction setExemptBatch(ExemptData[] memory batch) external;

\tfunction setFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseExempt(address account, bool isExempt_) external;

\tfunction setRebaseExemptBatch(ExemptData[] memory batch) external;

\tfunction setRebaseFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseInterval(uint256 interval) external;

\tfunction setRecipient(address account) external;

\t/* Events */

\tevent ExemptAdded(address indexed author, address indexed account);
\tevent ExemptRemoved(address indexed author, address indexed account);
\tevent FeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseExemptAdded(address indexed author, address indexed account);
\tevent RebaseExemptRemoved(address indexed author, address indexed account);
\tevent RebaseFeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseIntervalSet(address indexed author, uint256 interval);
\tevent RecipientSet(address indexed author, address indexed account);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IFeeLogic {
\t/* Types */

\tstruct ExemptData {
\t\taddress account;
\t\tbool isExempt;
\t}

\t/* Views */

\tfunction exemptsAt(uint256 index) external view returns (address);

\tfunction exemptsLength() external view returns (uint256);

\tfunction feeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction getFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction getRebaseFee(uint256 amount) external view returns (uint256);

\tfunction isExempt(address account) external view returns (bool);

\tfunction isRebaseExempt(address account) external view returns (bool);

\tfunction rebaseExemptsAt(uint256 index) external view returns (address);

\tfunction rebaseExemptsLength() external view returns (uint256);

\tfunction rebaseFeeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction rebaseInterval() external view returns (uint256);

\tfunction recipient() external view returns (address);

\tfunction undoFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction undoRebaseFee(uint256 amount) external view returns (uint256);

\t/* Mutators */

\tfunction notify(uint256 amount) external;

\tfunction setExempt(address account, bool isExempt_) external;

\tfunction setExemptBatch(ExemptData[] memory batch) external;

\tfunction setFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseExempt(address account, bool isExempt_) external;

\tfunction setRebaseExemptBatch(ExemptData[] memory batch) external;

\tfunction setRebaseFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseInterval(uint256 interval) external;

\tfunction setRecipient(address account) external;

\t/* Events */

\tevent ExemptAdded(address indexed author, address indexed account);
\tevent ExemptRemoved(address indexed author, address indexed account);
\tevent FeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseExemptAdded(address indexed author, address indexed account);
\tevent RebaseExemptRemoved(address indexed author, address indexed account);
\tevent RebaseFeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseIntervalSet(address indexed author, uint256 interval);
\tevent RecipientSet(address indexed author, address indexed account);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IFeeLogic {
\t/* Types */

\tstruct ExemptData {
\t\taddress account;
\t\tbool isExempt;
\t}

\t/* Views */

\tfunction exemptsAt(uint256 index) external view returns (address);

\tfunction exemptsLength() external view returns (uint256);

\tfunction feeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction getFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction getRebaseFee(uint256 amount) external view returns (uint256);

\tfunction isExempt(address account) external view returns (bool);

\tfunction isRebaseExempt(address account) external view returns (bool);

\tfunction rebaseExemptsAt(uint256 index) external view returns (address);

\tfunction rebaseExemptsLength() external view returns (uint256);

\tfunction rebaseFeeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction rebaseInterval() external view returns (uint256);

\tfunction recipient() external view returns (address);

\tfunction undoFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction undoRebaseFee(uint256 amount) external view returns (uint256);

\t/* Mutators */

\tfunction notify(uint256 amount) external;

\tfunction setExempt(address account, bool isExempt_) external;

\tfunction setExemptBatch(ExemptData[] memory batch) external;

\tfunction setFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseExempt(address account, bool isExempt_) external;

\tfunction setRebaseExemptBatch(ExemptData[] memory batch) external;

\tfunction setRebaseFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseInterval(uint256 interval) external;

\tfunction setRecipient(address account) external;

\t/* Events */

\tevent ExemptAdded(address indexed author, address indexed account);
\tevent ExemptRemoved(address indexed author, address indexed account);
\tevent FeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseExemptAdded(address indexed author, address indexed account);
\tevent RebaseExemptRemoved(address indexed author, address indexed account);
\tevent RebaseFeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseIntervalSet(address indexed author, uint256 interval);
\tevent RecipientSet(address indexed author, address indexed account);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IFeeLogic {
\t/* Types */

\tstruct ExemptData {
\t\taddress account;
\t\tbool isExempt;
\t}

\t/* Views */

\tfunction exemptsAt(uint256 index) external view returns (address);

\tfunction exemptsLength() external view returns (uint256);

\tfunction feeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction getFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction getRebaseFee(uint256 amount) external view returns (uint256);

\tfunction isExempt(address account) external view returns (bool);

\tfunction isRebaseExempt(address account) external view returns (bool);

\tfunction rebaseExemptsAt(uint256 index) external view returns (address);

\tfunction rebaseExemptsLength() external view returns (uint256);

\tfunction rebaseFeeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction rebaseInterval() external view returns (uint256);

\tfunction recipient() external view returns (address);

\tfunction undoFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction undoRebaseFee(uint256 amount) external view returns (uint256);

\t/* Mutators */

\tfunction notify(uint256 amount) external;

\tfunction setExempt(address account, bool isExempt_) external;

\tfunction setExemptBatch(ExemptData[] memory batch) external;

\tfunction setFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseExempt(address account, bool isExempt_) external;

\tfunction setRebaseExemptBatch(ExemptData[] memory batch) external;

\tfunction setRebaseFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseInterval(uint256 interval) external;

\tfunction setRecipient(address account) external;

\t/* Events */

\tevent ExemptAdded(address indexed author, address indexed account);
\tevent ExemptRemoved(address indexed author, address indexed account);
\tevent FeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseExemptAdded(address indexed author, address indexed account);
\tevent RebaseExemptRemoved(address indexed author, address indexed account);
\tevent RebaseFeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseIntervalSet(address indexed author, uint256 interval);
\tevent RecipientSet(address indexed author, address indexed account);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IFeeLogic {
\t/* Types */

\tstruct ExemptData {
\t\taddress account;
\t\tbool isExempt;
\t}

\t/* Views */

\tfunction exemptsAt(uint256 index) external view returns (address);

\tfunction exemptsLength() external view returns (uint256);

\tfunction feeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction getFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction getRebaseFee(uint256 amount) external view returns (uint256);

\tfunction isExempt(address account) external view returns (bool);

\tfunction isRebaseExempt(address account) external view returns (bool);

\tfunction rebaseExemptsAt(uint256 index) external view returns (address);

\tfunction rebaseExemptsLength() external view returns (uint256);

\tfunction rebaseFeeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction rebaseInterval() external view returns (uint256);

\tfunction recipient() external view returns (address);

\tfunction undoFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction undoRebaseFee(uint256 amount) external view returns (uint256);

\t/* Mutators */

\tfunction notify(uint256 amount) external;

\tfunction setExempt(address account, bool isExempt_) external;

\tfunction setExemptBatch(ExemptData[] memory batch) external;

\tfunction setFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseExempt(address account, bool isExempt_) external;

\tfunction setRebaseExemptBatch(ExemptData[] memory batch) external;

\tfunction setRebaseFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseInterval(uint256 interval) external;

\tfunction setRecipient(address account) external;

\t/* Events */

\tevent ExemptAdded(address indexed author, address indexed account);
\tevent ExemptRemoved(address indexed author, address indexed account);
\tevent FeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseExemptAdded(address indexed author, address indexed account);
\tevent RebaseExemptRemoved(address indexed author, address indexed account);
\tevent RebaseFeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseIntervalSet(address indexed author, uint256 interval);
\tevent RecipientSet(address indexed author, address indexed account);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IFeeLogic {
\t/* Types */

\tstruct ExemptData {
\t\taddress account;
\t\tbool isExempt;
\t}

\t/* Views */

\tfunction exemptsAt(uint256 index) external view returns (address);

\tfunction exemptsLength() external view returns (uint256);

\tfunction feeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction getFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction getRebaseFee(uint256 amount) external view returns (uint256);

\tfunction isExempt(address account) external view returns (bool);

\tfunction isRebaseExempt(address account) external view returns (bool);

\tfunction rebaseExemptsAt(uint256 index) external view returns (address);

\tfunction rebaseExemptsLength() external view returns (uint256);

\tfunction rebaseFeeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction rebaseInterval() external view returns (uint256);

\tfunction recipient() external view returns (address);

\tfunction undoFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction undoRebaseFee(uint256 amount) external view returns (uint256);

\t/* Mutators */

\tfunction notify(uint256 amount) external;

\tfunction setExempt(address account, bool isExempt_) external;

\tfunction setExemptBatch(ExemptData[] memory batch) external;

\tfunction setFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseExempt(address account, bool isExempt_) external;

\tfunction setRebaseExemptBatch(ExemptData[] memory batch) external;

\tfunction setRebaseFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseInterval(uint256 interval) external;

\tfunction setRecipient(address account) external;

\t/* Events */

\tevent ExemptAdded(address indexed author, address indexed account);
\tevent ExemptRemoved(address indexed author, address indexed account);
\tevent FeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseExemptAdded(address indexed author, address indexed account);
\tevent RebaseExemptRemoved(address indexed author, address indexed account);
\tevent RebaseFeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseIntervalSet(address indexed author, uint256 interval);
\tevent RecipientSet(address indexed author, address indexed account);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IFeeLogic {
\t/* Types */

\tstruct ExemptData {
\t\taddress account;
\t\tbool isExempt;
\t}

\t/* Views */

\tfunction exemptsAt(uint256 index) external view returns (address);

\tfunction exemptsLength() external view returns (uint256);

\tfunction feeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction getFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction getRebaseFee(uint256 amount) external view returns (uint256);

\tfunction isExempt(address account) external view returns (bool);

\tfunction isRebaseExempt(address account) external view returns (bool);

\tfunction rebaseExemptsAt(uint256 index) external view returns (address);

\tfunction rebaseExemptsLength() external view returns (uint256);

\tfunction rebaseFeeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction rebaseInterval() external view returns (uint256);

\tfunction recipient() external view returns (address);

\tfunction undoFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction undoRebaseFee(uint256 amount) external view returns (uint256);

\t/* Mutators */

\tfunction notify(uint256 amount) external;

\tfunction setExempt(address account, bool isExempt_) external;

\tfunction setExemptBatch(ExemptData[] memory batch) external;

\tfunction setFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseExempt(address account, bool isExempt_) external;

\tfunction setRebaseExemptBatch(ExemptData[] memory batch) external;

\tfunction setRebaseFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseInterval(uint256 interval) external;

\tfunction setRecipient(address account) external;

\t/* Events */

\tevent ExemptAdded(address indexed author, address indexed account);
\tevent ExemptRemoved(address indexed author, address indexed account);
\tevent FeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseExemptAdded(address indexed author, address indexed account);
\tevent RebaseExemptRemoved(address indexed author, address indexed account);
\tevent RebaseFeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseIntervalSet(address indexed author, uint256 interval);
\tevent RecipientSet(address indexed author, address indexed account);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IFeeLogic {
\t/* Types */

\tstruct ExemptData {
\t\taddress account;
\t\tbool isExempt;
\t}

\t/* Views */

\tfunction exemptsAt(uint256 index) external view returns (address);

\tfunction exemptsLength() external view returns (uint256);

\tfunction feeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction getFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction getRebaseFee(uint256 amount) external view returns (uint256);

\tfunction isExempt(address account) external view returns (bool);

\tfunction isRebaseExempt(address account) external view returns (bool);

\tfunction rebaseExemptsAt(uint256 index) external view returns (address);

\tfunction rebaseExemptsLength() external view returns (uint256);

\tfunction rebaseFeeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction rebaseInterval() external view returns (uint256);

\tfunction recipient() external view returns (address);

\tfunction undoFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction undoRebaseFee(uint256 amount) external view returns (uint256);

\t/* Mutators */

\tfunction notify(uint256 amount) external;

\tfunction setExempt(address account, bool isExempt_) external;

\tfunction setExemptBatch(ExemptData[] memory batch) external;

\tfunction setFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseExempt(address account, bool isExempt_) external;

\tfunction setRebaseExemptBatch(ExemptData[] memory batch) external;

\tfunction setRebaseFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseInterval(uint256 interval) external;

\tfunction setRecipient(address account) external;

\t/* Events */

\tevent ExemptAdded(address indexed author, address indexed account);
\tevent ExemptRemoved(address indexed author, address indexed account);
\tevent FeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseExemptAdded(address indexed author, address indexed account);
\tevent RebaseExemptRemoved(address indexed author, address indexed account);
\tevent RebaseFeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseIntervalSet(address indexed author, uint256 interval);
\tevent RecipientSet(address indexed author, address indexed account);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IFeeLogic {
\t/* Types */

\tstruct ExemptData {
\t\taddress account;
\t\tbool isExempt;
\t}

\t/* Views */

\tfunction exemptsAt(uint256 index) external view returns (address);

\tfunction exemptsLength() external view returns (uint256);

\tfunction feeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction getFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction getRebaseFee(uint256 amount) external view returns (uint256);

\tfunction isExempt(address account) external view returns (bool);

\tfunction isRebaseExempt(address account) external view returns (bool);

\tfunction rebaseExemptsAt(uint256 index) external view returns (address);

\tfunction rebaseExemptsLength() external view returns (uint256);

\tfunction rebaseFeeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction rebaseInterval() external view returns (uint256);

\tfunction recipient() external view returns (address);

\tfunction undoFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction undoRebaseFee(uint256 amount) external view returns (uint256);

\t/* Mutators */

\tfunction notify(uint256 amount) external;

\tfunction setExempt(address account, bool isExempt_) external;

\tfunction setExemptBatch(ExemptData[] memory batch) external;

\tfunction setFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseExempt(address account, bool isExempt_) external;

\tfunction setRebaseExemptBatch(ExemptData[] memory batch) external;

\tfunction setRebaseFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseInterval(uint256 interval) external;

\tfunction setRecipient(address account) external;

\t/* Events */

\tevent ExemptAdded(address indexed author, address indexed account);
\tevent ExemptRemoved(address indexed author, address indexed account);
\tevent FeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseExemptAdded(address indexed author, address indexed account);
\tevent RebaseExemptRemoved(address indexed author, address indexed account);
\tevent RebaseFeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseIntervalSet(address indexed author, uint256 interval);
\tevent RecipientSet(address indexed author, address indexed account);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IFeeLogic {
\t/* Types */

\tstruct ExemptData {
\t\taddress account;
\t\tbool isExempt;
\t}

\t/* Views */

\tfunction exemptsAt(uint256 index) external view returns (address);

\tfunction exemptsLength() external view returns (uint256);

\tfunction feeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction getFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction getRebaseFee(uint256 amount) external view returns (uint256);

\tfunction isExempt(address account) external view returns (bool);

\tfunction isRebaseExempt(address account) external view returns (bool);

\tfunction rebaseExemptsAt(uint256 index) external view returns (address);

\tfunction rebaseExemptsLength() external view returns (uint256);

\tfunction rebaseFeeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction rebaseInterval() external view returns (uint256);

\tfunction recipient() external view returns (address);

\tfunction undoFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction undoRebaseFee(uint256 amount) external view returns (uint256);

\t/* Mutators */

\tfunction notify(uint256 amount) external;

\tfunction setExempt(address account, bool isExempt_) external;

\tfunction setExemptBatch(ExemptData[] memory batch) external;

\tfunction setFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseExempt(address account, bool isExempt_) external;

\tfunction setRebaseExemptBatch(ExemptData[] memory batch) external;

\tfunction setRebaseFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseInterval(uint256 interval) external;

\tfunction setRecipient(address account) external;

\t/* Events */

\tevent ExemptAdded(address indexed author, address indexed account);
\tevent ExemptRemoved(address indexed author, address indexed account);
\tevent FeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseExemptAdded(address indexed author, address indexed account);
\tevent RebaseExemptRemoved(address indexed author, address indexed account);
\tevent RebaseFeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseIntervalSet(address indexed author, uint256 interval);
\tevent RecipientSet(address indexed author, address indexed account);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IFeeLogic {
\t/* Types */

\tstruct ExemptData {
\t\taddress account;
\t\tbool isExempt;
\t}

\t/* Views */

\tfunction exemptsAt(uint256 index) external view returns (address);

\tfunction exemptsLength() external view returns (uint256);

\tfunction feeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction getFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction getRebaseFee(uint256 amount) external view returns (uint256);

\tfunction isExempt(address account) external view returns (bool);

\tfunction isRebaseExempt(address account) external view returns (bool);

\tfunction rebaseExemptsAt(uint256 index) external view returns (address);

\tfunction rebaseExemptsLength() external view returns (uint256);

\tfunction rebaseFeeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction rebaseInterval() external view returns (uint256);

\tfunction recipient() external view returns (address);

\tfunction undoFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction undoRebaseFee(uint256 amount) external view returns (uint256);

\t/* Mutators */

\tfunction notify(uint256 amount) external;

\tfunction setExempt(address account, bool isExempt_) external;

\tfunction setExemptBatch(ExemptData[] memory batch) external;

\tfunction setFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseExempt(address account, bool isExempt_) external;

\tfunction setRebaseExemptBatch(ExemptData[] memory batch) external;

\tfunction setRebaseFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseInterval(uint256 interval) external;

\tfunction setRecipient(address account) external;

\t/* Events */

\tevent ExemptAdded(address indexed author, address indexed account);
\tevent ExemptRemoved(address indexed author, address indexed account);
\tevent FeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseExemptAdded(address indexed author, address indexed account);
\tevent RebaseExemptRemoved(address indexed author, address indexed account);
\tevent RebaseFeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseIntervalSet(address indexed author, uint256 interval);
\tevent RecipientSet(address indexed author, address indexed account);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IFeeLogic {
\t/* Types */

\tstruct ExemptData {
\t\taddress account;
\t\tbool isExempt;
\t}

\t/* Views */

\tfunction exemptsAt(uint256 index) external view returns (address);

\tfunction exemptsLength() external view returns (uint256);

\tfunction feeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction getFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction getRebaseFee(uint256 amount) external view returns (uint256);

\tfunction isExempt(address account) external view returns (bool);

\tfunction isRebaseExempt(address account) external view returns (bool);

\tfunction rebaseExemptsAt(uint256 index) external view returns (address);

\tfunction rebaseExemptsLength() external view returns (uint256);

\tfunction rebaseFeeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction rebaseInterval() external view returns (uint256);

\tfunction recipient() external view returns (address);

\tfunction undoFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction undoRebaseFee(uint256 amount) external view returns (uint256);

\t/* Mutators */

\tfunction notify(uint256 amount) external;

\tfunction setExempt(address account, bool isExempt_) external;

\tfunction setExemptBatch(ExemptData[] memory batch) external;

\tfunction setFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseExempt(address account, bool isExempt_) external;

\tfunction setRebaseExemptBatch(ExemptData[] memory batch) external;

\tfunction setRebaseFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseInterval(uint256 interval) external;

\tfunction setRecipient(address account) external;

\t/* Events */

\tevent ExemptAdded(address indexed author, address indexed account);
\tevent ExemptRemoved(address indexed author, address indexed account);
\tevent FeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseExemptAdded(address indexed author, address indexed account);
\tevent RebaseExemptRemoved(address indexed author, address indexed account);
\tevent RebaseFeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseIntervalSet(address indexed author, uint256 interval);
\tevent RecipientSet(address indexed author, address indexed account);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IFeeLogic {
\t/* Types */

\tstruct ExemptData {
\t\taddress account;
\t\tbool isExempt;
\t}

\t/* Views */

\tfunction exemptsAt(uint256 index) external view returns (address);

\tfunction exemptsLength() external view returns (uint256);

\tfunction feeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction getFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction getRebaseFee(uint256 amount) external view returns (uint256);

\tfunction isExempt(address account) external view returns (bool);

\tfunction isRebaseExempt(address account) external view returns (bool);

\tfunction rebaseExemptsAt(uint256 index) external view returns (address);

\tfunction rebaseExemptsLength() external view returns (uint256);

\tfunction rebaseFeeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction rebaseInterval() external view returns (uint256);

\tfunction recipient() external view returns (address);

\tfunction undoFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction undoRebaseFee(uint256 amount) external view returns (uint256);

\t/* Mutators */

\tfunction notify(uint256 amount) external;

\tfunction setExempt(address account, bool isExempt_) external;

\tfunction setExemptBatch(ExemptData[] memory batch) external;

\tfunction setFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseExempt(address account, bool isExempt_) external;

\tfunction setRebaseExemptBatch(ExemptData[] memory batch) external;

\tfunction setRebaseFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseInterval(uint256 interval) external;

\tfunction setRecipient(address account) external;

\t/* Events */

\tevent ExemptAdded(address indexed author, address indexed account);
\tevent ExemptRemoved(address indexed author, address indexed account);
\tevent FeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseExemptAdded(address indexed author, address indexed account);
\tevent RebaseExemptRemoved(address indexed author, address indexed account);
\tevent RebaseFeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseIntervalSet(address indexed author, uint256 interval);
\tevent RecipientSet(address indexed author, address indexed account);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IFeeLogic {
\t/* Types */

\tstruct ExemptData {
\t\taddress account;
\t\tbool isExempt;
\t}

\t/* Views */

\tfunction exemptsAt(uint256 index) external view returns (address);

\tfunction exemptsLength() external view returns (uint256);

\tfunction feeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction getFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction getRebaseFee(uint256 amount) external view returns (uint256);

\tfunction isExempt(address account) external view returns (bool);

\tfunction isRebaseExempt(address account) external view returns (bool);

\tfunction rebaseExemptsAt(uint256 index) external view returns (address);

\tfunction rebaseExemptsLength() external view returns (uint256);

\tfunction rebaseFeeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction rebaseInterval() external view returns (uint256);

\tfunction recipient() external view returns (address);

\tfunction undoFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction undoRebaseFee(uint256 amount) external view returns (uint256);

\t/* Mutators */

\tfunction notify(uint256 amount) external;

\tfunction setExempt(address account, bool isExempt_) external;

\tfunction setExemptBatch(ExemptData[] memory batch) external;

\tfunction setFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseExempt(address account, bool isExempt_) external;

\tfunction setRebaseExemptBatch(ExemptData[] memory batch) external;

\tfunction setRebaseFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseInterval(uint256 interval) external;

\tfunction setRecipient(address account) external;

\t/* Events */

\tevent ExemptAdded(address indexed author, address indexed account);
\tevent ExemptRemoved(address indexed author, address indexed account);
\tevent FeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseExemptAdded(address indexed author, address indexed account);
\tevent RebaseExemptRemoved(address indexed author, address indexed account);
\tevent RebaseFeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseIntervalSet(address indexed author, uint256 interval);
\tevent RecipientSet(address indexed author, address indexed account);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IFeeLogic {
\t/* Types */

\tstruct ExemptData {
\t\taddress account;
\t\tbool isExempt;
\t}

\t/* Views */

\tfunction exemptsAt(uint256 index) external view returns (address);

\tfunction exemptsLength() external view returns (uint256);

\tfunction feeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction getFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction getRebaseFee(uint256 amount) external view returns (uint256);

\tfunction isExempt(address account) external view returns (bool);

\tfunction isRebaseExempt(address account) external view returns (bool);

\tfunction rebaseExemptsAt(uint256 index) external view returns (address);

\tfunction rebaseExemptsLength() external view returns (uint256);

\tfunction rebaseFeeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction rebaseInterval() external view returns (uint256);

\tfunction recipient() external view returns (address);

\tfunction undoFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction undoRebaseFee(uint256 amount) external view returns (uint256);

\t/* Mutators */

\tfunction notify(uint256 amount) external;

\tfunction setExempt(address account, bool isExempt_) external;

\tfunction setExemptBatch(ExemptData[] memory batch) external;

\tfunction setFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseExempt(address account, bool isExempt_) external;

\tfunction setRebaseExemptBatch(ExemptData[] memory batch) external;

\tfunction setRebaseFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseInterval(uint256 interval) external;

\tfunction setRecipient(address account) external;

\t/* Events */

\tevent ExemptAdded(address indexed author, address indexed account);
\tevent ExemptRemoved(address indexed author, address indexed account);
\tevent FeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseExemptAdded(address indexed author, address indexed account);
\tevent RebaseExemptRemoved(address indexed author, address indexed account);
\tevent RebaseFeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseIntervalSet(address indexed author, uint256 interval);
\tevent RecipientSet(address indexed author, address indexed account);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IFeeLogic {
\t/* Types */

\tstruct ExemptData {
\t\taddress account;
\t\tbool isExempt;
\t}

\t/* Views */

\tfunction exemptsAt(uint256 index) external view returns (address);

\tfunction exemptsLength() external view returns (uint256);

\tfunction feeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction getFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction getRebaseFee(uint256 amount) external view returns (uint256);

\tfunction isExempt(address account) external view returns (bool);

\tfunction isRebaseExempt(address account) external view returns (bool);

\tfunction rebaseExemptsAt(uint256 index) external view returns (address);

\tfunction rebaseExemptsLength() external view returns (uint256);

\tfunction rebaseFeeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction rebaseInterval() external view returns (uint256);

\tfunction recipient() external view returns (address);

\tfunction undoFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction undoRebaseFee(uint256 amount) external view returns (uint256);

\t/* Mutators */

\tfunction notify(uint256 amount) external;

\tfunction setExempt(address account, bool isExempt_) external;

\tfunction setExemptBatch(ExemptData[] memory batch) external;

\tfunction setFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseExempt(address account, bool isExempt_) external;

\tfunction setRebaseExemptBatch(ExemptData[] memory batch) external;

\tfunction setRebaseFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseInterval(uint256 interval) external;

\tfunction setRecipient(address account) external;

\t/* Events */

\tevent ExemptAdded(address indexed author, address indexed account);
\tevent ExemptRemoved(address indexed author, address indexed account);
\tevent FeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseExemptAdded(address indexed author, address indexed account);
\tevent RebaseExemptRemoved(address indexed author, address indexed account);
\tevent RebaseFeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseIntervalSet(address indexed author, uint256 interval);
\tevent RecipientSet(address indexed author, address indexed account);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IFeeLogic {
\t/* Types */

\tstruct ExemptData {
\t\taddress account;
\t\tbool isExempt;
\t}

\t/* Views */

\tfunction exemptsAt(uint256 index) external view returns (address);

\tfunction exemptsLength() external view returns (uint256);

\tfunction feeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction getFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction getRebaseFee(uint256 amount) external view returns (uint256);

\tfunction isExempt(address account) external view returns (bool);

\tfunction isRebaseExempt(address account) external view returns (bool);

\tfunction rebaseExemptsAt(uint256 index) external view returns (address);

\tfunction rebaseExemptsLength() external view returns (uint256);

\tfunction rebaseFeeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction rebaseInterval() external view returns (uint256);

\tfunction recipient() external view returns (address);

\tfunction undoFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction undoRebaseFee(uint256 amount) external view returns (uint256);

\t/* Mutators */

\tfunction notify(uint256 amount) external;

\tfunction setExempt(address account, bool isExempt_) external;

\tfunction setExemptBatch(ExemptData[] memory batch) external;

\tfunction setFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseExempt(address account, bool isExempt_) external;

\tfunction setRebaseExemptBatch(ExemptData[] memory batch) external;

\tfunction setRebaseFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseInterval(uint256 interval) external;

\tfunction setRecipient(address account) external;

\t/* Events */

\tevent ExemptAdded(address indexed author, address indexed account);
\tevent ExemptRemoved(address indexed author, address indexed account);
\tevent FeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseExemptAdded(address indexed author, address indexed account);
\tevent RebaseExemptRemoved(address indexed author, address indexed account);
\tevent RebaseFeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseIntervalSet(address indexed author, uint256 interval);
\tevent RecipientSet(address indexed author, address indexed account);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IFeeLogic {
\t/* Types */

\tstruct ExemptData {
\t\taddress account;
\t\tbool isExempt;
\t}

\t/* Views */

\tfunction exemptsAt(uint256 index) external view returns (address);

\tfunction exemptsLength() external view returns (uint256);

\tfunction feeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction getFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction getRebaseFee(uint256 amount) external view returns (uint256);

\tfunction isExempt(address account) external view returns (bool);

\tfunction isRebaseExempt(address account) external view returns (bool);

\tfunction rebaseExemptsAt(uint256 index) external view returns (address);

\tfunction rebaseExemptsLength() external view returns (uint256);

\tfunction rebaseFeeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction rebaseInterval() external view returns (uint256);

\tfunction recipient() external view returns (address);

\tfunction undoFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction undoRebaseFee(uint256 amount) external view returns (uint256);

\t/* Mutators */

\tfunction notify(uint256 amount) external;

\tfunction setExempt(address account, bool isExempt_) external;

\tfunction setExemptBatch(ExemptData[] memory batch) external;

\tfunction setFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseExempt(address account, bool isExempt_) external;

\tfunction setRebaseExemptBatch(ExemptData[] memory batch) external;

\tfunction setRebaseFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseInterval(uint256 interval) external;

\tfunction setRecipient(address account) external;

\t/* Events */

\tevent ExemptAdded(address indexed author, address indexed account);
\tevent ExemptRemoved(address indexed author, address indexed account);
\tevent FeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseExemptAdded(address indexed author, address indexed account);
\tevent RebaseExemptRemoved(address indexed author, address indexed account);
\tevent RebaseFeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseIntervalSet(address indexed author, uint256 interval);
\tevent RecipientSet(address indexed author, address indexed account);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IFeeLogic {
\t/* Types */

\tstruct ExemptData {
\t\taddress account;
\t\tbool isExempt;
\t}

\t/* Views */

\tfunction exemptsAt(uint256 index) external view returns (address);

\tfunction exemptsLength() external view returns (uint256);

\tfunction feeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction getFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction getRebaseFee(uint256 amount) external view returns (uint256);

\tfunction isExempt(address account) external view returns (bool);

\tfunction isRebaseExempt(address account) external view returns (bool);

\tfunction rebaseExemptsAt(uint256 index) external view returns (address);

\tfunction rebaseExemptsLength() external view returns (uint256);

\tfunction rebaseFeeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction rebaseInterval() external view returns (uint256);

\tfunction recipient() external view returns (address);

\tfunction undoFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction undoRebaseFee(uint256 amount) external view returns (uint256);

\t/* Mutators */

\tfunction notify(uint256 amount) external;

\tfunction setExempt(address account, bool isExempt_) external;

\tfunction setExemptBatch(ExemptData[] memory batch) external;

\tfunction setFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseExempt(address account, bool isExempt_) external;

\tfunction setRebaseExemptBatch(ExemptData[] memory batch) external;

\tfunction setRebaseFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseInterval(uint256 interval) external;

\tfunction setRecipient(address account) external;

\t/* Events */

\tevent ExemptAdded(address indexed author, address indexed account);
\tevent ExemptRemoved(address indexed author, address indexed account);
\tevent FeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseExemptAdded(address indexed author, address indexed account);
\tevent RebaseExemptRemoved(address indexed author, address indexed account);
\tevent RebaseFeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseIntervalSet(address indexed author, uint256 interval);
\tevent RecipientSet(address indexed author, address indexed account);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IFeeLogic {
\t/* Types */

\tstruct ExemptData {
\t\taddress account;
\t\tbool isExempt;
\t}

\t/* Views */

\tfunction exemptsAt(uint256 index) external view returns (address);

\tfunction exemptsLength() external view returns (uint256);

\tfunction feeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction getFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction getRebaseFee(uint256 amount) external view returns (uint256);

\tfunction isExempt(address account) external view returns (bool);

\tfunction isRebaseExempt(address account) external view returns (bool);

\tfunction rebaseExemptsAt(uint256 index) external view returns (address);

\tfunction rebaseExemptsLength() external view returns (uint256);

\tfunction rebaseFeeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction rebaseInterval() external view returns (uint256);

\tfunction recipient() external view returns (address);

\tfunction undoFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction undoRebaseFee(uint256 amount) external view returns (uint256);

\t/* Mutators */

\tfunction notify(uint256 amount) external;

\tfunction setExempt(address account, bool isExempt_) external;

\tfunction setExemptBatch(ExemptData[] memory batch) external;

\tfunction setFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseExempt(address account, bool isExempt_) external;

\tfunction setRebaseExemptBatch(ExemptData[] memory batch) external;

\tfunction setRebaseFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseInterval(uint256 interval) external;

\tfunction setRecipient(address account) external;

\t/* Events */

\tevent ExemptAdded(address indexed author, address indexed account);
\tevent ExemptRemoved(address indexed author, address indexed account);
\tevent FeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseExemptAdded(address indexed author, address indexed account);
\tevent RebaseExemptRemoved(address indexed author, address indexed account);
\tevent RebaseFeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseIntervalSet(address indexed author, uint256 interval);
\tevent RecipientSet(address indexed author, address indexed account);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IFeeLogic {
\t/* Types */

\tstruct ExemptData {
\t\taddress account;
\t\tbool isExempt;
\t}

\t/* Views */

\tfunction exemptsAt(uint256 index) external view returns (address);

\tfunction exemptsLength() external view returns (uint256);

\tfunction feeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction getFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction getRebaseFee(uint256 amount) external view returns (uint256);

\tfunction isExempt(address account) external view returns (bool);

\tfunction isRebaseExempt(address account) external view returns (bool);

\tfunction rebaseExemptsAt(uint256 index) external view returns (address);

\tfunction rebaseExemptsLength() external view returns (uint256);

\tfunction rebaseFeeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction rebaseInterval() external view returns (uint256);

\tfunction recipient() external view returns (address);

\tfunction undoFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction undoRebaseFee(uint256 amount) external view returns (uint256);

\t/* Mutators */

\tfunction notify(uint256 amount) external;

\tfunction setExempt(address account, bool isExempt_) external;

\tfunction setExemptBatch(ExemptData[] memory batch) external;

\tfunction setFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseExempt(address account, bool isExempt_) external;

\tfunction setRebaseExemptBatch(ExemptData[] memory batch) external;

\tfunction setRebaseFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseInterval(uint256 interval) external;

\tfunction setRecipient(address account) external;

\t/* Events */

\tevent ExemptAdded(address indexed author, address indexed account);
\tevent ExemptRemoved(address indexed author, address indexed account);
\tevent FeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseExemptAdded(address indexed author, address indexed account);
\tevent RebaseExemptRemoved(address indexed author, address indexed account);
\tevent RebaseFeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseIntervalSet(address indexed author, uint256 interval);
\tevent RecipientSet(address indexed author, address indexed account);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IFeeLogic {
\t/* Types */

\tstruct ExemptData {
\t\taddress account;
\t\tbool isExempt;
\t}

\t/* Views */

\tfunction exemptsAt(uint256 index) external view returns (address);

\tfunction exemptsLength() external view returns (uint256);

\tfunction feeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction getFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction getRebaseFee(uint256 amount) external view returns (uint256);

\tfunction isExempt(address account) external view returns (bool);

\tfunction isRebaseExempt(address account) external view returns (bool);

\tfunction rebaseExemptsAt(uint256 index) external view returns (address);

\tfunction rebaseExemptsLength() external view returns (uint256);

\tfunction rebaseFeeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction rebaseInterval() external view returns (uint256);

\tfunction recipient() external view returns (address);

\tfunction undoFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction undoRebaseFee(uint256 amount) external view returns (uint256);

\t/* Mutators */

\tfunction notify(uint256 amount) external;

\tfunction setExempt(address account, bool isExempt_) external;

\tfunction setExemptBatch(ExemptData[] memory batch) external;

\tfunction setFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseExempt(address account, bool isExempt_) external;

\tfunction setRebaseExemptBatch(ExemptData[] memory batch) external;

\tfunction setRebaseFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseInterval(uint256 interval) external;

\tfunction setRecipient(address account) external;

\t/* Events */

\tevent ExemptAdded(address indexed author, address indexed account);
\tevent ExemptRemoved(address indexed author, address indexed account);
\tevent FeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseExemptAdded(address indexed author, address indexed account);
\tevent RebaseExemptRemoved(address indexed author, address indexed account);
\tevent RebaseFeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseIntervalSet(address indexed author, uint256 interval);
\tevent RecipientSet(address indexed author, address indexed account);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IFeeLogic {
\t/* Types */

\tstruct ExemptData {
\t\taddress account;
\t\tbool isExempt;
\t}

\t/* Views */

\tfunction exemptsAt(uint256 index) external view returns (address);

\tfunction exemptsLength() external view returns (uint256);

\tfunction feeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction getFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction getRebaseFee(uint256 amount) external view returns (uint256);

\tfunction isExempt(address account) external view returns (bool);

\tfunction isRebaseExempt(address account) external view returns (bool);

\tfunction rebaseExemptsAt(uint256 index) external view returns (address);

\tfunction rebaseExemptsLength() external view returns (uint256);

\tfunction rebaseFeeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction rebaseInterval() external view returns (uint256);

\tfunction recipient() external view returns (address);

\tfunction undoFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction undoRebaseFee(uint256 amount) external view returns (uint256);

\t/* Mutators */

\tfunction notify(uint256 amount) external;

\tfunction setExempt(address account, bool isExempt_) external;

\tfunction setExemptBatch(ExemptData[] memory batch) external;

\tfunction setFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseExempt(address account, bool isExempt_) external;

\tfunction setRebaseExemptBatch(ExemptData[] memory batch) external;

\tfunction setRebaseFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseInterval(uint256 interval) external;

\tfunction setRecipient(address account) external;

\t/* Events */

\tevent ExemptAdded(address indexed author, address indexed account);
\tevent ExemptRemoved(address indexed author, address indexed account);
\tevent FeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseExemptAdded(address indexed author, address indexed account);
\tevent RebaseExemptRemoved(address indexed author, address indexed account);
\tevent RebaseFeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseIntervalSet(address indexed author, uint256 interval);
\tevent RecipientSet(address indexed author, address indexed account);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IFeeLogic {
\t/* Types */

\tstruct ExemptData {
\t\taddress account;
\t\tbool isExempt;
\t}

\t/* Views */

\tfunction exemptsAt(uint256 index) external view returns (address);

\tfunction exemptsLength() external view returns (uint256);

\tfunction feeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction getFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction getRebaseFee(uint256 amount) external view returns (uint256);

\tfunction isExempt(address account) external view returns (bool);

\tfunction isRebaseExempt(address account) external view returns (bool);

\tfunction rebaseExemptsAt(uint256 index) external view returns (address);

\tfunction rebaseExemptsLength() external view returns (uint256);

\tfunction rebaseFeeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction rebaseInterval() external view returns (uint256);

\tfunction recipient() external view returns (address);

\tfunction undoFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction undoRebaseFee(uint256 amount) external view returns (uint256);

\t/* Mutators */

\tfunction notify(uint256 amount) external;

\tfunction setExempt(address account, bool isExempt_) external;

\tfunction setExemptBatch(ExemptData[] memory batch) external;

\tfunction setFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseExempt(address account, bool isExempt_) external;

\tfunction setRebaseExemptBatch(ExemptData[] memory batch) external;

\tfunction setRebaseFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseInterval(uint256 interval) external;

\tfunction setRecipient(address account) external;

\t/* Events */

\tevent ExemptAdded(address indexed author, address indexed account);
\tevent ExemptRemoved(address indexed author, address indexed account);
\tevent FeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseExemptAdded(address indexed author, address indexed account);
\tevent RebaseExemptRemoved(address indexed author, address indexed account);
\tevent RebaseFeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseIntervalSet(address indexed author, uint256 interval);
\tevent RecipientSet(address indexed author, address indexed account);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IFeeLogic {
\t/* Types */

\tstruct ExemptData {
\t\taddress account;
\t\tbool isExempt;
\t}

\t/* Views */

\tfunction exemptsAt(uint256 index) external view returns (address);

\tfunction exemptsLength() external view returns (uint256);

\tfunction feeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction getFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction getRebaseFee(uint256 amount) external view returns (uint256);

\tfunction isExempt(address account) external view returns (bool);

\tfunction isRebaseExempt(address account) external view returns (bool);

\tfunction rebaseExemptsAt(uint256 index) external view returns (address);

\tfunction rebaseExemptsLength() external view returns (uint256);

\tfunction rebaseFeeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction rebaseInterval() external view returns (uint256);

\tfunction recipient() external view returns (address);

\tfunction undoFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction undoRebaseFee(uint256 amount) external view returns (uint256);

\t/* Mutators */

\tfunction notify(uint256 amount) external;

\tfunction setExempt(address account, bool isExempt_) external;

\tfunction setExemptBatch(ExemptData[] memory batch) external;

\tfunction setFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseExempt(address account, bool isExempt_) external;

\tfunction setRebaseExemptBatch(ExemptData[] memory batch) external;

\tfunction setRebaseFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseInterval(uint256 interval) external;

\tfunction setRecipient(address account) external;

\t/* Events */

\tevent ExemptAdded(address indexed author, address indexed account);
\tevent ExemptRemoved(address indexed author, address indexed account);
\tevent FeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseExemptAdded(address indexed author, address indexed account);
\tevent RebaseExemptRemoved(address indexed author, address indexed account);
\tevent RebaseFeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseIntervalSet(address indexed author, uint256 interval);
\tevent RecipientSet(address indexed author, address indexed account);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IFeeLogic {
\t/* Types */

\tstruct ExemptData {
\t\taddress account;
\t\tbool isExempt;
\t}

\t/* Views */

\tfunction exemptsAt(uint256 index) external view returns (address);

\tfunction exemptsLength() external view returns (uint256);

\tfunction feeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction getFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction getRebaseFee(uint256 amount) external view returns (uint256);

\tfunction isExempt(address account) external view returns (bool);

\tfunction isRebaseExempt(address account) external view returns (bool);

\tfunction rebaseExemptsAt(uint256 index) external view returns (address);

\tfunction rebaseExemptsLength() external view returns (uint256);

\tfunction rebaseFeeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction rebaseInterval() external view returns (uint256);

\tfunction recipient() external view returns (address);

\tfunction undoFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction undoRebaseFee(uint256 amount) external view returns (uint256);

\t/* Mutators */

\tfunction notify(uint256 amount) external;

\tfunction setExempt(address account, bool isExempt_) external;

\tfunction setExemptBatch(ExemptData[] memory batch) external;

\tfunction setFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseExempt(address account, bool isExempt_) external;

\tfunction setRebaseExemptBatch(ExemptData[] memory batch) external;

\tfunction setRebaseFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseInterval(uint256 interval) external;

\tfunction setRecipient(address account) external;

\t/* Events */

\tevent ExemptAdded(address indexed author, address indexed account);
\tevent ExemptRemoved(address indexed author, address indexed account);
\tevent FeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseExemptAdded(address indexed author, address indexed account);
\tevent RebaseExemptRemoved(address indexed author, address indexed account);
\tevent RebaseFeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseIntervalSet(address indexed author, uint256 interval);
\tevent RecipientSet(address indexed author, address indexed account);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IFeeLogic {
\t/* Types */

\tstruct ExemptData {
\t\taddress account;
\t\tbool isExempt;
\t}

\t/* Views */

\tfunction exemptsAt(uint256 index) external view returns (address);

\tfunction exemptsLength() external view returns (uint256);

\tfunction feeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction getFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction getRebaseFee(uint256 amount) external view returns (uint256);

\tfunction isExempt(address account) external view returns (bool);

\tfunction isRebaseExempt(address account) external view returns (bool);

\tfunction rebaseExemptsAt(uint256 index) external view returns (address);

\tfunction rebaseExemptsLength() external view returns (uint256);

\tfunction rebaseFeeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction rebaseInterval() external view returns (uint256);

\tfunction recipient() external view returns (address);

\tfunction undoFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction undoRebaseFee(uint256 amount) external view returns (uint256);

\t/* Mutators */

\tfunction notify(uint256 amount) external;

\tfunction setExempt(address account, bool isExempt_) external;

\tfunction setExemptBatch(ExemptData[] memory batch) external;

\tfunction setFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseExempt(address account, bool isExempt_) external;

\tfunction setRebaseExemptBatch(ExemptData[] memory batch) external;

\tfunction setRebaseFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseInterval(uint256 interval) external;

\tfunction setRecipient(address account) external;

\t/* Events */

\tevent ExemptAdded(address indexed author, address indexed account);
\tevent ExemptRemoved(address indexed author, address indexed account);
\tevent FeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseExemptAdded(address indexed author, address indexed account);
\tevent RebaseExemptRemoved(address indexed author, address indexed account);
\tevent RebaseFeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseIntervalSet(address indexed author, uint256 interval);
\tevent RecipientSet(address indexed author, address indexed account);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IFeeLogic {
\t/* Types */

\tstruct ExemptData {
\t\taddress account;
\t\tbool isExempt;
\t}

\t/* Views */

\tfunction exemptsAt(uint256 index) external view returns (address);

\tfunction exemptsLength() external view returns (uint256);

\tfunction feeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction getFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction getRebaseFee(uint256 amount) external view returns (uint256);

\tfunction isExempt(address account) external view returns (bool);

\tfunction isRebaseExempt(address account) external view returns (bool);

\tfunction rebaseExemptsAt(uint256 index) external view returns (address);

\tfunction rebaseExemptsLength() external view returns (uint256);

\tfunction rebaseFeeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction rebaseInterval() external view returns (uint256);

\tfunction recipient() external view returns (address);

\tfunction undoFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction undoRebaseFee(uint256 amount) external view returns (uint256);

\t/* Mutators */

\tfunction notify(uint256 amount) external;

\tfunction setExempt(address account, bool isExempt_) external;

\tfunction setExemptBatch(ExemptData[] memory batch) external;

\tfunction setFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseExempt(address account, bool isExempt_) external;

\tfunction setRebaseExemptBatch(ExemptData[] memory batch) external;

\tfunction setRebaseFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseInterval(uint256 interval) external;

\tfunction setRecipient(address account) external;

\t/* Events */

\tevent ExemptAdded(address indexed author, address indexed account);
\tevent ExemptRemoved(address indexed author, address indexed account);
\tevent FeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseExemptAdded(address indexed author, address indexed account);
\tevent RebaseExemptRemoved(address indexed author, address indexed account);
\tevent RebaseFeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseIntervalSet(address indexed author, uint256 interval);
\tevent RecipientSet(address indexed author, address indexed account);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IFeeLogic {
\t/* Types */

\tstruct ExemptData {
\t\taddress account;
\t\tbool isExempt;
\t}

\t/* Views */

\tfunction exemptsAt(uint256 index) external view returns (address);

\tfunction exemptsLength() external view returns (uint256);

\tfunction feeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction getFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction getRebaseFee(uint256 amount) external view returns (uint256);

\tfunction isExempt(address account) external view returns (bool);

\tfunction isRebaseExempt(address account) external view returns (bool);

\tfunction rebaseExemptsAt(uint256 index) external view returns (address);

\tfunction rebaseExemptsLength() external view returns (uint256);

\tfunction rebaseFeeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction rebaseInterval() external view returns (uint256);

\tfunction recipient() external view returns (address);

\tfunction undoFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction undoRebaseFee(uint256 amount) external view returns (uint256);

\t/* Mutators */

\tfunction notify(uint256 amount) external;

\tfunction setExempt(address account, bool isExempt_) external;

\tfunction setExemptBatch(ExemptData[] memory batch) external;

\tfunction setFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseExempt(address account, bool isExempt_) external;

\tfunction setRebaseExemptBatch(ExemptData[] memory batch) external;

\tfunction setRebaseFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseInterval(uint256 interval) external;

\tfunction setRecipient(address account) external;

\t/* Events */

\tevent ExemptAdded(address indexed author, address indexed account);
\tevent ExemptRemoved(address indexed author, address indexed account);
\tevent FeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseExemptAdded(address indexed author, address indexed account);
\tevent RebaseExemptRemoved(address indexed author, address indexed account);
\tevent RebaseFeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseIntervalSet(address indexed author, uint256 interval);
\tevent RecipientSet(address indexed author, address indexed account);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IFeeLogic {
\t/* Types */

\tstruct ExemptData {
\t\taddress account;
\t\tbool isExempt;
\t}

\t/* Views */

\tfunction exemptsAt(uint256 index) external view returns (address);

\tfunction exemptsLength() external view returns (uint256);

\tfunction feeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction getFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction getRebaseFee(uint256 amount) external view returns (uint256);

\tfunction isExempt(address account) external view returns (bool);

\tfunction isRebaseExempt(address account) external view returns (bool);

\tfunction rebaseExemptsAt(uint256 index) external view returns (address);

\tfunction rebaseExemptsLength() external view returns (uint256);

\tfunction rebaseFeeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction rebaseInterval() external view returns (uint256);

\tfunction recipient() external view returns (address);

\tfunction undoFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction undoRebaseFee(uint256 amount) external view returns (uint256);

\t/* Mutators */

\tfunction notify(uint256 amount) external;

\tfunction setExempt(address account, bool isExempt_) external;

\tfunction setExemptBatch(ExemptData[] memory batch) external;

\tfunction setFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseExempt(address account, bool isExempt_) external;

\tfunction setRebaseExemptBatch(ExemptData[] memory batch) external;

\tfunction setRebaseFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseInterval(uint256 interval) external;

\tfunction setRecipient(address account) external;

\t/* Events */

\tevent ExemptAdded(address indexed author, address indexed account);
\tevent ExemptRemoved(address indexed author, address indexed account);
\tevent FeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseExemptAdded(address indexed author, address indexed account);
\tevent RebaseExemptRemoved(address indexed author, address indexed account);
\tevent RebaseFeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseIntervalSet(address indexed author, uint256 interval);
\tevent RecipientSet(address indexed author, address indexed account);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IFeeLogic {
\t/* Types */

\tstruct ExemptData {
\t\taddress account;
\t\tbool isExempt;
\t}

\t/* Views */

\tfunction exemptsAt(uint256 index) external view returns (address);

\tfunction exemptsLength() external view returns (uint256);

\tfunction feeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction getFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction getRebaseFee(uint256 amount) external view returns (uint256);

\tfunction isExempt(address account) external view returns (bool);

\tfunction isRebaseExempt(address account) external view returns (bool);

\tfunction rebaseExemptsAt(uint256 index) external view returns (address);

\tfunction rebaseExemptsLength() external view returns (uint256);

\tfunction rebaseFeeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction rebaseInterval() external view returns (uint256);

\tfunction recipient() external view returns (address);

\tfunction undoFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction undoRebaseFee(uint256 amount) external view returns (uint256);

\t/* Mutators */

\tfunction notify(uint256 amount) external;

\tfunction setExempt(address account, bool isExempt_) external;

\tfunction setExemptBatch(ExemptData[] memory batch) external;

\tfunction setFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseExempt(address account, bool isExempt_) external;

\tfunction setRebaseExemptBatch(ExemptData[] memory batch) external;

\tfunction setRebaseFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseInterval(uint256 interval) external;

\tfunction setRecipient(address account) external;

\t/* Events */

\tevent ExemptAdded(address indexed author, address indexed account);
\tevent ExemptRemoved(address indexed author, address indexed account);
\tevent FeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseExemptAdded(address indexed author, address indexed account);
\tevent RebaseExemptRemoved(address indexed author, address indexed account);
\tevent RebaseFeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseIntervalSet(address indexed author, uint256 interval);
\tevent RecipientSet(address indexed author, address indexed account);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IFeeLogic {
\t/* Types */

\tstruct ExemptData {
\t\taddress account;
\t\tbool isExempt;
\t}

\t/* Views */

\tfunction exemptsAt(uint256 index) external view returns (address);

\tfunction exemptsLength() external view returns (uint256);

\tfunction feeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction getFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction getRebaseFee(uint256 amount) external view returns (uint256);

\tfunction isExempt(address account) external view returns (bool);

\tfunction isRebaseExempt(address account) external view returns (bool);

\tfunction rebaseExemptsAt(uint256 index) external view returns (address);

\tfunction rebaseExemptsLength() external view returns (uint256);

\tfunction rebaseFeeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction rebaseInterval() external view returns (uint256);

\tfunction recipient() external view returns (address);

\tfunction undoFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction undoRebaseFee(uint256 amount) external view returns (uint256);

\t/* Mutators */

\tfunction notify(uint256 amount) external;

\tfunction setExempt(address account, bool isExempt_) external;

\tfunction setExemptBatch(ExemptData[] memory batch) external;

\tfunction setFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseExempt(address account, bool isExempt_) external;

\tfunction setRebaseExemptBatch(ExemptData[] memory batch) external;

\tfunction setRebaseFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseInterval(uint256 interval) external;

\tfunction setRecipient(address account) external;

\t/* Events */

\tevent ExemptAdded(address indexed author, address indexed account);
\tevent ExemptRemoved(address indexed author, address indexed account);
\tevent FeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseExemptAdded(address indexed author, address indexed account);
\tevent RebaseExemptRemoved(address indexed author, address indexed account);
\tevent RebaseFeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseIntervalSet(address indexed author, uint256 interval);
\tevent RecipientSet(address indexed author, address indexed account);
}
"
    }
  }
}
