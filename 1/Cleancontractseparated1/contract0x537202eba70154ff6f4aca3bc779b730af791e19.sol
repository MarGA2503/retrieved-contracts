// SPDX-License-Identifier: UNLICENCED
pragma solidity \u003e=0.4.22 \u003c0.9.0;

import \u0027./IERC20.sol\u0027;

contract Exchange{
    // Define Struct
    struct Order{
        uint256 id;
        address from;
        uint256 amount;
    }
    // Define properties
    uint256 public length = 0;
    IERC20 token;
    Order[] public _orders;
    // mapping(address=\u003euint256) public _uncleared_balances;

    address public owner;

    // Define Modifiers
    modifier onlyAdmin(){
        require(msg.sender == owner,\"Exchange: Action not Alowed!\");
        _;
    }

    // Define Events
    event TransferIn(address indexed from,address indexed to, uint256 value, uint256 id);
    // event TransferOut(address indexed from,address indexed to, uint256 value);

    constructor(address _token){
        token = IERC20(_token);
        owner = msg.sender;
    }

    function setAdmin(address _new_admin) public virtual onlyAdmin{
        owner = _new_admin;
    }

    // A function is called to start Swaping Process 
    function transferIn(uint256 _value) public virtual{
        // Transfer Amount
        token.transferFrom(msg.sender, owner, _value);

        // Add to order
        Order memory _order;
        _order.id = length;
        _order.amount = _value;
        _order.from = msg.sender;
        _orders.push(_order);


        // Emit Event
        emit TransferIn(msg.sender, owner, _value, length);

        length +=1;
    }

    function checkStatus(uint256 _order_id) public virtual view returns(uint256 id,address from,uint256 amount){
        for(uint256 i = 0; i\u003e=length;i++){
            if(_order_id ==_orders[i].id){
                return (_orders[i].id,_orders[i].from,_orders[i].amount);
            }
        }
    }

    function getOrders() public virtual view returns(Order[] memory){
        return _orders;
    }
}


"},"IERC20.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

/**
 * @dev Interface of the ERC20 standard as defined in the EIP.
 */
interface IERC20 {
    /**
     * @dev Returns the amount of tokens in existence.
     */
    function totalSupply() external view returns (uint256);

    /**
     * @dev Returns the amount of tokens owned by `account`.
     */
    function balanceOf(address account) external view returns (uint256);

    /**
     * @dev Moves `amount` tokens from the caller\u0027s account to `recipient`.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transfer(address recipient, uint256 amount) external returns (bool);

    /**
     * @dev Returns the remaining number of tokens that `spender` will be
     * allowed to spend on behalf of `owner` through {transferFrom}. This is
     * zero by default.
     *
     * This value changes when {approve} or {transferFrom} are called.
     */
    function allowance(address owner, address spender) external view returns (uint256);

    /**
     * @dev Sets `amount` as the allowance of `spender` over the caller\u0027s tokens.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * IMPORTANT: Beware that changing an allowance with this method brings the risk
     * that someone may use both the old and the new allowance by unfortunate
     * transaction ordering. One possible solution to mitigate this race
     * condition is to first reduce the spender\u0027s allowance to 0 and set the
     * desired value afterwards:
     * https://github.com/ethereum/EIPs/issues/20#issuecomment-263524729
     *
     * Emits an {Approval} event.
     */
    function approve(address spender, uint256 amount) external returns (bool);

    /**
     * @dev Moves `amount` tokens from `sender` to `recipient` using the
     * allowance mechanism. `amount` is then deducted from the caller\u0027s
     * allowance.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transferFrom(
        address sender,
        address recipient,
        uint256 amount
    ) external returns (bool);

    /**
     * @dev Emitted when `value` tokens are moved from one account (`from`) to
     * another (`to`).
     *
     * Note that `value` may be zero.
     */
    event Transfer(address indexed from, address indexed to, uint256 value);

    /**
     * @dev Emitted when the allowance of a `spender` for an `owner` is set by
     * a call to {approve}. `value` is the new allowance.
     */
    event Approval(address indexed owner, address indexed spender, uint256 value);
}

