// SPDX-License-Identifier: UNLICENSED
pragma solidity 0.6.12;
pragma experimental ABIEncoderV2;

import \"./IERC20.sol\";

interface IHoldefi {

\tstruct Market {
\t\tuint256 totalSupply;

\t\tuint256 supplyIndex;
\t\tuint256 supplyIndexUpdateTime;

\t\tuint256 totalBorrow;

\t\tuint256 borrowIndex;
\t\tuint256 borrowIndexUpdateTime;

\t\tuint256 promotionReserveScaled;
\t\tuint256 promotionReserveLastUpdateTime;

\t\tuint256 promotionDebtScaled;
\t\tuint256 promotionDebtLastUpdateTime;
\t}


\tstruct Collateral {
\t\tuint256 totalCollateral;
\t\tuint256 totalLiquidatedCollateral;
\t}

\tfunction marketAssets(address market) external view returns(Market memory);
\tfunction collateralAssets(address collateral) external view returns(Collateral memory);

\tfunction getAccountSupply(address account, address market)
\t\texternal
\t\tview
\t\treturns (uint256 balance, uint256 interest, uint256 currentSupplyIndex);

\tfunction getAccountBorrow(address account, address market, address collateral)
\t\texternal
\t\tview
\t\treturns (uint256 balance, uint256 interest, uint256 currentBorrowIndex);

\tfunction getAccountCollateral(address account, address collateral)
\t\texternal
\t\tview
\t\treturns (
\t\t\tuint256 balance,
\t\t\tuint256 timeSinceLastActivity,
\t\t\tuint256 borrowPowerValue,
\t\t\tuint256 totalBorrowValue,
\t\t\tbool underCollateral
\t\t);

\tfunction getCurrentSupplyIndex (address market)
\t\texternal
\t\tview
\t\treturns (
\t\t\tuint256 supplyIndex,
\t\t\tuint256 supplyRate,
\t\t\tuint256 currentTime
\t\t);

\tfunction getCurrentBorrowIndex (address market)
\t\texternal
\t\tview
\t\treturns (
\t\t\tuint256 borrowIndex,
\t\t\tuint256 borrowRate,
\t\t\tuint256 currentTime
\t\t);

\tfunction marketDebt (address collateral, address market)
\t\texternal
\t\tview
\t\treturns(
\t\t\tuint256 debt
\t\t);

\tfunction isPaused(string memory operation) external view returns (bool res);
}

interface IHoldefiSettings {

\tstruct MarketSettings {
\t\tbool isExist;
\t\tbool isActive;      

\t\tuint256 borrowRate;
\t\tuint256 borrowRateUpdateTime;

\t\tuint256 suppliersShareRate;
\t\tuint256 suppliersShareRateUpdateTime;

\t\tuint256 promotionRate;
\t}

\tstruct CollateralSettings {
\t\tbool isExist;
\t\tbool isActive;    

\t\tuint256 valueToLoanRate; 
\t\tuint256 VTLUpdateTime;

\t\tuint256 penaltyRate;
\t\tuint256 penaltyUpdateTime;

\t\tuint256 bonusRate;
\t}

\tfunction marketAssets(address market) external view returns(MarketSettings memory);
\tfunction collateralAssets(address collateral) external view returns(CollateralSettings memory);
}


interface IHoldefiPrices {
\tfunction getPrice(address asset) external view returns (uint256 price, uint256 priceDecimals);
}


contract HoldefiRead {

\taddress constant public ethAddress = 0xEeeeeEeeeEeEeeEeEeEeeEEEeeeeEeeeeeeeEEeE;

\tstring[8] private operationsList = [
\t\t\"supply\",
\t\t\"withdrawSupply\",
\t\t\"collateralize\",
\t\t\"withdrawCollateral\",
\t\t\"borrow\",
\t\t\"repayBorrow\",
\t\t\"liquidateBorrowerCollateral\",
\t\t\"buyLiquidatedCollateral\"
\t];

\tstruct AccountMarketData {
\t\taddress market;
\t\tuint256 balance;
\t\tuint256 interest;
\t\tuint256 index;
\t}

\tstruct AccountCollateralData {
\t\taddress collateral;
\t\tuint256 balance;
\t\tuint256 timeSinceLastActivity;
\t\tuint256 borrowPowerValue;
\t\tuint256 totalBorrowValue;
\t\tbool underCollateral;

\t\tAccountMarketData[] userBorrows;
\t}

\tstruct AccountAssetData {
\t\taddress asset;
\t\tuint256 walletBalance;
\t\tuint256 allowance;
\t\tuint256 price;
\t\tuint256 priceDecimals;
\t}

\tstruct MarketData {
\t\taddress market;

\t\tbool isExist;
\t\tbool isActive;      

\t\tuint256 borrowRate;
\t\tuint256 supplyRate;

\t\tuint256 suppliersShareRate;
\t\tuint256 promotionRate;

\t\tuint256 totalSupply;
\t\tuint256 totalBorrow;

\t\tuint256 supplyIndex;
\t\tuint256 borrowIndex;

\t\tuint256 price;
\t\tuint256 priceDecimals;
\t}

\tstruct CollateralData {
\t\taddress collateral;

\t\tbool isExist;
\t\tbool isActive;    

\t\tuint256 valueToLoanRate; 
\t\tuint256 penaltyRate;
\t\tuint256 bonusRate;

\t\tuint256 totalCollateral;
\t\tuint256 totalLiquidatedCollateral;

\t\tMarketDebtData[] marketDebt;

\t\tuint256 price;
\t\tuint256 priceDecimals;
\t}

\tstruct OperationPauseStatus {
\t\tstring operation;
\t\tbool pauseStatus;
\t}

\tstruct MarketDebtData {
\t\taddress market;
\t\tuint256 debt;
\t}

\tIHoldefi public holdefi;

\tIHoldefiSettings public holdefiSettings;

\tIHoldefiPrices public holdefiPrices;


\tconstructor(
\t\tIHoldefi holdefiAddress,
\t\tIHoldefiSettings holdefiSettingsAddress,
\t\tIHoldefiPrices holdefiPricesAddress
\t)
\t\tpublic
\t{
\t\tholdefi = holdefiAddress;
\t\tholdefiSettings = holdefiSettingsAddress;
\t\tholdefiPrices = holdefiPricesAddress;
\t}

\tfunction getWalletBalance(address account, address asset) public view returns (uint256 res) {
\t\tif (asset == ethAddress) {
\t\t\tres = account.balance;
\t\t}
\t\telse {
\t\t\tIERC20 token = IERC20(asset);
\t\t\tres = token.balanceOf(account);
\t\t}
\t}

\tfunction getWalletAllowance(address account, address asset) public view returns (uint256 res) {
\t\tif (asset != ethAddress) {
\t\t\tIERC20 token = IERC20(asset);
\t\t\tres = token.allowance(account, address(holdefi));
\t\t}
\t}


\tfunction getUserData(address userAddress, address[] memory marketList, address[] memory collateralList)
\t\tpublic
\t\tview
\t\treturns(
\t\t\tAccountMarketData[] memory userSupplies,
\t\t\tAccountCollateralData[] memory userCollaterals,
\t\t\tAccountAssetData[] memory userAssets
\t\t)
\t{
\t\tuserSupplies = new AccountMarketData[](marketList.length);
\t\tuserCollaterals = new AccountCollateralData[](collateralList.length);
\t\taddress[] memory assets = new address[](marketList.length + collateralList.length);

\t\tbool isExist;
\t\tuint256 index;
\t\tuint256 i;
\t\tuint256 j;
\t\tfor (i = 0 ; i \u003c collateralList.length ; i++) {
\t\t\tisExist = false;
\t\t\tuserCollaterals[i].collateral = collateralList[i];
\t\t\t(
\t\t\t\tuserCollaterals[i].balance,
\t\t\t\tuserCollaterals[i].timeSinceLastActivity,
\t\t\t\tuserCollaterals[i].borrowPowerValue,
\t\t\t\tuserCollaterals[i].totalBorrowValue,
\t\t\t\tuserCollaterals[i].underCollateral
\t\t\t) = holdefi.getAccountCollateral(userAddress, collateralList[i]);
\t\t\tuserCollaterals[i].userBorrows = new AccountMarketData[](marketList.length);
\t\t\tfor (j = 0 ; j \u003c marketList.length ; j++) {
\t\t\t\tif (i == 0) {
\t\t\t\t\tuserSupplies[j].market = marketList[j];
\t\t\t\t\t(
\t\t\t\t\t\tuserSupplies[j].balance,
\t\t\t\t\t\tuserSupplies[j].interest,
\t\t\t\t\t\tuserSupplies[j].index
\t\t\t\t\t) = holdefi.getAccountSupply(userAddress, marketList[j]);

\t\t\t\t\tassets[j] = marketList[j];
\t\t\t\t\tindex = j + 1;
\t\t\t\t}
\t\t\t\tif (collateralList[i] == marketList[j]) {
\t\t\t\t\tisExist = true;
\t\t\t\t}

\t\t\t\tuserCollaterals[i].userBorrows[j].market = marketList[j];
\t\t\t\t(
\t\t\t\t\tuserCollaterals[i].userBorrows[j].balance,
\t\t\t\t\tuserCollaterals[i].userBorrows[j].interest,
\t\t\t\t\tuserCollaterals[i].userBorrows[j].index
\t\t\t\t) = holdefi.getAccountBorrow(userAddress, marketList[j], collateralList[i]);
\t\t\t\t
\t\t\t}

\t\t\tif (!isExist) {
\t\t\t\tassets[index] = collateralList[i];
\t\t\t\tindex = index + 1;
\t\t\t}
\t\t}

\t\tuserAssets = new AccountAssetData[](index);
\t\tfor (i = 0 ; i \u003c index ; i++) {
\t\t\tuserAssets[i].asset = assets[i];
\t\t\t(userAssets[i].price, userAssets[i].priceDecimals) = holdefiPrices.getPrice(assets[i]);
\t\t\tuserAssets[i].walletBalance = getWalletBalance(userAddress, assets[i]);
\t\t\tuserAssets[i].allowance = getWalletAllowance(userAddress, assets[i]);
\t\t}
\t}

\tfunction getProtocolData(address[] memory marketList, address[] memory collateralList)
\t\tpublic
\t\tview
\t\treturns(
\t\t\tMarketData[] memory markets,
\t\t\tCollateralData[] memory collaterals, 
\t\t\tOperationPauseStatus[8] memory operations
\t\t)
\t{

\t\tmarkets = new MarketData[](marketList.length);
\t\tcollaterals = new CollateralData[](collateralList.length);

\t\tuint256 i;
\t\tuint256 j;
\t\tfor (i = 0 ; i \u003c marketList.length ; i++) {
\t\t\tIHoldefi.Market memory holdefiMarket = holdefi.marketAssets(marketList[i]);

\t\t\tmarkets[i].market = marketList[i];
\t\t\tmarkets[i].totalSupply = holdefiMarket.totalSupply;
\t\t\tmarkets[i].totalBorrow = holdefiMarket.totalBorrow;

\t\t\t(
\t\t\t\tmarkets[i].supplyIndex,
\t\t\t\tmarkets[i].supplyRate,
\t\t\t) = holdefi.getCurrentSupplyIndex(marketList[i]);


\t\t\t(
\t\t\t\tmarkets[i].borrowIndex,
\t\t\t\tmarkets[i].borrowRate,
\t\t\t) = holdefi.getCurrentBorrowIndex(marketList[i]);


\t\t\tIHoldefiSettings.MarketSettings memory holdefiSettingsMarket = holdefiSettings.marketAssets(marketList[i]);
\t\t\tmarkets[i].isExist = holdefiSettingsMarket.isExist;
\t\t\tmarkets[i].isActive = holdefiSettingsMarket.isActive;
\t\t\tmarkets[i].suppliersShareRate = holdefiSettingsMarket.suppliersShareRate;
\t\t\tmarkets[i].promotionRate = holdefiSettingsMarket.promotionRate;

\t\t\t(markets[i].price, markets[i].priceDecimals) = holdefiPrices.getPrice(marketList[i]);
\t\t}

\t\tfor (i = 0 ; i \u003c collateralList.length ; i++) {
\t\t\tIHoldefi.Collateral memory holdefiCollateral = holdefi.collateralAssets(collateralList[i]);
\t\t\tcollaterals[i].collateral = collateralList[i];
\t\t\tcollaterals[i].totalCollateral = holdefiCollateral.totalCollateral;
\t\t\tcollaterals[i].totalLiquidatedCollateral = holdefiCollateral.totalLiquidatedCollateral;

\t\t\tIHoldefiSettings.CollateralSettings memory holdefiSettingsCollateral = holdefiSettings.collateralAssets(collateralList[i]);
\t\t\tcollaterals[i].isExist = holdefiSettingsCollateral.isExist;
\t\t\tcollaterals[i].isActive = holdefiSettingsCollateral.isActive;
\t\t\tcollaterals[i].valueToLoanRate = holdefiSettingsCollateral.valueToLoanRate;
\t\t\tcollaterals[i].penaltyRate = holdefiSettingsCollateral.penaltyRate;
\t\t\tcollaterals[i].bonusRate = holdefiSettingsCollateral.bonusRate;

\t\t\t(collaterals[i].price, collaterals[i].priceDecimals) = holdefiPrices.getPrice(collateralList[i]);

\t\t\tcollaterals[i].marketDebt = new MarketDebtData[](marketList.length);
\t\t\tfor (j = 0 ; j \u003c marketList.length ; j++) {
\t\t\t\tcollaterals[i].marketDebt[j].market = marketList[j];
\t\t\t\tcollaterals[i].marketDebt[j].debt = holdefi.marketDebt(collateralList[i], marketList[j]);
\t\t\t}
\t\t}

\t\tfor (i = 0 ; i \u003c operationsList.length ; i++) {
\t\t\toperations[i].operation = operationsList[i];
\t\t\toperations[i].pauseStatus = holdefi.isPaused(operationsList[i]);
\t\t}
\t}


\tfunction getUserProtocolData(address userAddress, address[] memory marketList, address[] memory collateralList)
\t\tpublic
\t\tview
\t\treturns(
\t\t\tAccountMarketData[] memory userSupplies,
\t\t\tAccountCollateralData[] memory userCollaterals,
\t\t\tAccountAssetData[] memory userAssets,
\t\t\tMarketData[] memory markets,
\t\t\tCollateralData[] memory collaterals,
\t\t\tOperationPauseStatus[8] memory operations
\t\t)
\t{
\t\t(userSupplies, userCollaterals, userAssets) = getUserData(userAddress, marketList, collateralList);
\t\t(markets, collaterals, operations) = getProtocolData(marketList, collateralList);
\t}


\tfunction getUserAssetsData(address userAddress, address[] memory marketList, address[] memory collateralList)
\t\tpublic
\t\tview
\t\treturns(
\t\t\tAccountAssetData[] memory userAssets
\t\t)
\t{
\t\taddress[] memory assets = new address[](marketList.length + collateralList.length);

\t\tbool isExist;
\t\tuint256 index;
\t\tuint256 i;
\t\tuint256 j;
\t\tfor (i = 0 ; i \u003c collateralList.length ; i++) {
\t\t\tisExist = false;
\t\t\tfor (j = 0 ; j \u003c marketList.length ; j++) {
\t\t\t\tif (i == 0) {
\t\t\t\t\tassets[j] = marketList[j];
\t\t\t\t\tindex = j + 1;
\t\t\t\t}

\t\t\t\tif (collateralList[i] == marketList[j]) {
\t\t\t\t\tisExist = true;
\t\t\t\t}
\t\t\t}

\t\t\tif (!isExist) {
\t\t\t\tassets[index] = collateralList[i];
\t\t\t\tindex = index + 1;
\t\t\t}
\t\t}

\t\tuserAssets = new AccountAssetData[](index);
\t\tfor (i = 0 ; i \u003c index ; i++) {
\t\t\tuserAssets[i].asset = assets[i];
\t\t\t(userAssets[i].price, userAssets[i].priceDecimals) = holdefiPrices.getPrice(assets[i]);
\t\t\tuserAssets[i].walletBalance = getWalletBalance(userAddress, assets[i]);
\t\t\tuserAssets[i].allowance = getWalletAllowance(userAddress, assets[i]);
\t\t}
\t}
}"},"IERC20.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity \u003e=0.6.0 \u003c0.8.0;

/**
 * @dev Interface of the ERC20 standard as defined in the EIP.
 */
interface IERC20 {
    /**
     * @dev Returns the amount of tokens in existence.
     */
    function totalSupply() external view returns (uint256);

    /**
     * @dev Returns the amount of tokens owned by `account`.
     */
    function balanceOf(address account) external view returns (uint256);

    /**
     * @dev Moves `amount` tokens from the caller\u0027s account to `recipient`.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transfer(address recipient, uint256 amount) external returns (bool);

    /**
     * @dev Returns the remaining number of tokens that `spender` will be
     * allowed to spend on behalf of `owner` through {transferFrom}. This is
     * zero by default.
     *
     * This value changes when {approve} or {transferFrom} are called.
     */
    function allowance(address owner, address spender) external view returns (uint256);

    /**
     * @dev Sets `amount` as the allowance of `spender` over the caller\u0027s tokens.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * IMPORTANT: Beware that changing an allowance with this method brings the risk
     * that someone may use both the old and the new allowance by unfortunate
     * transaction ordering. One possible solution to mitigate this race
     * condition is to first reduce the spender\u0027s allowance to 0 and set the
     * desired value afterwards:
     * https://github.com/ethereum/EIPs/issues/20#issuecomment-263524729
     *
     * Emits an {Approval} event.
     */
    function approve(address spender, uint256 amount) external returns (bool);

    /**
     * @dev Moves `amount` tokens from `sender` to `recipient` using the
     * allowance mechanism. `amount` is then deducted from the caller\u0027s
     * allowance.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transferFrom(address sender, address recipient, uint256 amount) external returns (bool);

    /**
     * @dev Emitted when `value` tokens are moved from one account (`from`) to
     * another (`to`).
     *
     * Note that `value` may be zero.
     */
    event Transfer(address indexed from, address indexed to, uint256 value);

    /**
     * @dev Emitted when the allowance of a `spender` for an `owner` is set by
     * a call to {approve}. `value` is the new allowance.
     */
    event Approval(address indexed owner, address indexed spender, uint256 value);
}

