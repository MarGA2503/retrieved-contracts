// SPDX-License-Identifier: MIT

pragma solidity \u003e=0.6.0 \u003c0.8.0;

/*
 * @dev Provides information about the current execution context, including the
 * sender of the transaction and its data. While these are generally available
 * via msg.sender and msg.data, they should not be accessed in such a direct
 * manner, since when dealing with GSN meta-transactions the account sending and
 * paying for execution may not be the actual sender (as far as an application
 * is concerned).
 *
 * This contract is only required for intermediate, library-like contracts.
 */
abstract contract Context {
    function _msgSender() internal view virtual returns (address payable) {
        return msg.sender;
    }

    function _msgData() internal view virtual returns (bytes memory) {
        this; // silence state mutability warning without generating bytecode - see https://github.com/ethereum/solidity/issues/2691
        return msg.data;
    }
}
"},"Ownable.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity \u003e=0.6.0 \u003c0.8.0;

import \"./Context.sol\";
/**
 * @dev Contract module which provides a basic access control mechanism, where
 * there is an account (an owner) that can be granted exclusive access to
 * specific functions.
 *
 * By default, the owner account will be the one that deploys the contract. This
 * can later be changed with {transferOwnership}.
 *
 * This module is used through inheritance. It will make available the modifier
 * `onlyOwner`, which can be applied to your functions to restrict their use to
 * the owner.
 */
abstract contract Ownable is Context {
    address private _owner;

    event OwnershipTransferred(address indexed previousOwner, address indexed newOwner);

    /**
     * @dev Initializes the contract setting the deployer as the initial owner.
     */
    constructor () internal {
        address msgSender = _msgSender();
        _owner = msgSender;
        emit OwnershipTransferred(address(0), msgSender);
    }

    /**
     * @dev Returns the address of the current owner.
     */
    function owner() public view virtual returns (address) {
        return _owner;
    }

    /**
     * @dev Throws if called by any account other than the owner.
     */
    modifier onlyOwner() {
        require(owner() == _msgSender(), \"Ownable: caller is not the owner\");
        _;
    }

    /**
     * @dev Leaves the contract without owner. It will not be possible to call
     * `onlyOwner` functions anymore. Can only be called by the current owner.
     *
     * NOTE: Renouncing ownership will leave the contract without an owner,
     * thereby removing any functionality that is only available to the owner.
     */
    function renounceOwnership() public virtual onlyOwner {
        emit OwnershipTransferred(_owner, address(0));
        _owner = address(0);
    }

    /**
     * @dev Transfers ownership of the contract to a new account (`newOwner`).
     * Can only be called by the current owner.
     */
    function transferOwnership(address newOwner) public virtual onlyOwner {
        require(newOwner != address(0), \"Ownable: new owner is the zero address\");
        emit OwnershipTransferred(_owner, newOwner);
        _owner = newOwner;
    }
}
"},"StitchedPunksShop.sol":{"content":"// SPDX-License-Identifier: MIT

// 2021-04-10 - Release Version

pragma solidity 0.7.6;

// @openzeppelin/contracts/utils/Context.sol introduces execution context to replace msg.sender with _msgSender()
// implement admin role
import \"./Ownable.sol\";

// interfaces for fetching ownership of common and wrapped Cryptopunks
interface ICryptoPunks {
\tfunction punkIndexToAddress(uint) external view returns(address);
}
interface IWrappedPunks {
\tfunction ownerOf(uint256) external view returns (address);
}

contract StitchedPunksShop is Ownable {
    // access to existing CryptoPunks and WrappedPunks contracts
\tICryptoPunks internal CryptoPunks = ICryptoPunks(0xb47e3cd837dDF8e4c57F05d70Ab865de6e193BBB);
\tIWrappedPunks internal WrappedPunks = IWrappedPunks(0xb7F7F6C52F2e2fdb1963Eab30438024864c313F6);

    // current price for submitting an order
    uint256 public currentOrderPrice = 1 ether;

    // order details
    struct OrderDetails {
        // punk ID this order refers to:
        uint16 punkId;
        // current status of order:
        // 0 = \"not created yet\"
        // 10 = \"order created and paid\"
        // 20 = \"crafting StitchedPunk\"
        // 30 = \"shipped\"
        // 40 = \"received and NFT redeemed\"
        uint8 status;
        // owner who submitted the order:
        address owner;
    }

    // order status for punk ID
    uint16[] public orderedPunkIds;
    mapping(uint16 =\u003e OrderDetails) public orderStatus;

    // events
    event OrderCreated(uint16 indexed punkId, address indexed owner);
    event OrderUpdated(uint16 indexed punkId, uint8 indexed newStatus);

    function withdraw() external onlyOwner() {
        payable(owner()).transfer(address(this).balance);
    }

    function setOrderPrice(uint256 newPrice) external onlyOwner() {
        currentOrderPrice = newPrice;
    }

    // returns current owner of a given CryptoPunk (if common CryptoPunk)
    function getOwnerForCryptoPunk(uint16 punkIndex) public view returns (address) {
        return CryptoPunks.punkIndexToAddress(punkIndex);
    }

    // returns current owner of a given WrappedPunk (if wrapped CryptoPunk)
    function getOwnerForWrappedPunk(uint16 punkIndex) public view returns (address) {
        try WrappedPunks.ownerOf(punkIndex) returns (address wrappedPunkOwner) {
            return wrappedPunkOwner;
        } catch Error(string memory) {
            // catches failing revert() and require()
            // ERC721: if token does not exist, require() fails in target contract
            return address(0);
        } catch (bytes memory) {
            // low-level: catches a failing assertion, etc.
            return address(0);
        }
    }

    // checks if wallet owns a given CryptoPunk
    function isOwnerOfPunk(address wallet, uint16 punkIndex) public view returns (bool) {
        return getOwnerForCryptoPunk(punkIndex) == wallet || getOwnerForWrappedPunk(punkIndex) == wallet;
    }

    function submitOrder(uint16 punkIndex) external payable {
        // currentOrderPrice has to be paid
        require(msg.value \u003e= currentOrderPrice, \"price is too low\");
        // sender has to be owner of the punk (common or wrapped)
        require(isOwnerOfPunk(_msgSender(), punkIndex), \"you need to own this punk\");
        // punk must not already be ordered
        require(orderStatus[punkIndex].status == 0, \"punk was already ordered\");

        // save order details
        orderStatus[punkIndex] = OrderDetails(punkIndex, 10, _msgSender());
        orderedPunkIds.push(punkIndex);

        emit OrderCreated(punkIndex, _msgSender());
    }

    function updateOrderStatus(uint16 punkIndex, uint8 newStatus) public onlyOwner() {
        // punk has to be ordered already
        require(orderStatus[punkIndex].status != 0, \"punk was not yet ordered\");

        // update order status
        orderStatus[punkIndex].status = newStatus;

        emit OrderUpdated(punkIndex, newStatus);
    }

    // StitchedPunksNFT contract address
    address public stitchedPunksNFTAddress = address(0);

    // will be called after the StitchedPunksNFT contract was deployed
    function setStitchedPunksNFTAddress(address newAddress) public onlyOwner() {
        stitchedPunksNFTAddress = newAddress;
    }

    // update order status when the NFT is redeemed/minted (must be called from the StitchedPunksNFT contract)
    function updateOrderRedeemNFT(uint16 punkIndex) external {
        require(stitchedPunksNFTAddress == _msgSender(), \"caller is not the StitchedPunksNFT contract\");

        // update order status: 40 = \"received and NFT redeemed\"
        uint8 newStatus = 40;

        // punk has to be ordered already
        require(orderStatus[punkIndex].status != 0, \"punk was not yet ordered\");

        // update order status
        orderStatus[punkIndex].status = newStatus;

        emit OrderUpdated(punkIndex, newStatus);
    }
}
