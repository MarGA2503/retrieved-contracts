



// SPDX-License-Identifier: MIT

// solhint-disable-next-line compiler-version
pragma solidity >=0.4.24 <0.8.0;

import \"../utils/AddressUpgradeable.sol\";

/**
 * @dev This is a base contract to aid in writing upgradeable contracts, or any kind of contract that will be deployed
 * behind a proxy. Since a proxied contract can't have a constructor, it's common to move constructor logic to an
 * external initializer function, usually called `initialize`. It then becomes necessary to protect this initializer
 * function so it can only be called once. The {initializer} modifier provided by this contract will have this effect.
 *
 * TIP: To avoid leaving the proxy in an uninitialized state, the initializer function should be called as early as
 * possible by providing the encoded function call as the `_data` argument to {UpgradeableProxy-constructor}.
 *
 * CAUTION: When used with inheritance, manual care must be taken to not invoke a parent initializer twice, or to ensure
 * that all initializers are idempotent. This is not verified automatically as constructors are by Solidity.
 */
abstract contract Initializable {

    /**
     * @dev Indicates that the contract has been initialized.
     */
    bool private _initialized;

    /**
     * @dev Indicates that the contract is in the process of being initialized.
     */
    bool private _initializing;

    /**
     * @dev Modifier to protect an initializer function from being invoked twice.
     */
    modifier initializer() {
        require(_initializing || _isConstructor() || !_initialized, \"Initializable: contract is already initialized\");

        bool isTopLevelCall = !_initializing;
        if (isTopLevelCall) {
            _initializing = true;
            _initialized = true;
        }

        _;

        if (isTopLevelCall) {
            _initializing = false;
        }
    }

    /// @dev Returns true if and only if the function is running in the constructor
    function _isConstructor() private view returns (bool) {
        return !AddressUpgradeable.isContract(address(this));
    }
}
"
    
// SPDX-License-Identifier: MIT

pragma solidity ^0.7.0;

/**
 * @dev Collection of functions related to the address type
 */
library AddressUpgradeable {
    /**
     * @dev Returns true if `account` is a contract.
     *
     * [IMPORTANT]
     * ====
     * It is unsafe to assume that an address for which this function returns
     * false is an externally-owned account (EOA) and not a contract.
     *
     * Among others, `isContract` will return false for the following
     * types of addresses:
     *
     *  - an externally-owned account
     *  - a contract in construction
     *  - an address where a contract will be created
     *  - an address where a contract lived, but was destroyed
     * ====
     */
    function isContract(address account) internal view returns (bool) {
        // This method relies on extcodesize, which returns 0 for contracts in
        // construction, since the code is only stored at the end of the
        // constructor execution.

        uint256 size;
        // solhint-disable-next-line no-inline-assembly
        assembly { size := extcodesize(account) }
        return size > 0;
    }

    /**
     * @dev Replacement for Solidity's `transfer`: sends `amount` wei to
     * `recipient`, forwarding all available gas and reverting on errors.
     *
     * https://eips.ethereum.org/EIPS/eip-1884[EIP1884] increases the gas cost
     * of certain opcodes, possibly making contracts go over the 2300 gas limit
     * imposed by `transfer`, making them unable to receive funds via
     * `transfer`. {sendValue} removes this limitation.
     *
     * https://diligence.consensys.net/posts/2019/09/stop-using-soliditys-transfer-now/[Learn more].
     *
     * IMPORTANT: because control is transferred to `recipient`, care must be
     * taken to not create reentrancy vulnerabilities. Consider using
     * {ReentrancyGuard} or the
     * https://solidity.readthedocs.io/en/v0.5.11/security-considerations.html#use-the-checks-effects-interactions-pattern[checks-effects-interactions pattern].
     */
    function sendValue(address payable recipient, uint256 amount) internal {
        require(address(this).balance >= amount, \"Address: insufficient balance\");

        // solhint-disable-next-line avoid-low-level-calls, avoid-call-value
        (bool success, ) = recipient.call{ value: amount }(\"\");
        require(success, \"Address: unable to send value, recipient may have reverted\");
    }

    /**
     * @dev Performs a Solidity function call using a low level `call`. A
     * plain`call` is an unsafe replacement for a function call: use this
     * function instead.
     *
     * If `target` reverts with a revert reason, it is bubbled up by this
     * function (like regular Solidity function calls).
     *
     * Returns the raw returned data. To convert to the expected return value,
     * use https://solidity.readthedocs.io/en/latest/units-and-global-variables.html?highlight=abi.decode#abi-encoding-and-decoding-functions[`abi.decode`].
     *
     * Requirements:
     *
     * - `target` must be a contract.
     * - calling `target` with `data` must not revert.
     *
     * _Available since v3.1._
     */
    function functionCall(address target, bytes memory data) internal returns (bytes memory) {
      return functionCall(target, data, \"Address: low-level call failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`], but with
     * `errorMessage` as a fallback revert reason when `target` reverts.
     *
     * _Available since v3.1._
     */
    function functionCall(address target, bytes memory data, string memory errorMessage) internal returns (bytes memory) {
        return functionCallWithValue(target, data, 0, errorMessage);
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`],
     * but also transferring `value` wei to `target`.
     *
     * Requirements:
     *
     * - the calling contract must have an ETH balance of at least `value`.
     * - the called Solidity function must be `payable`.
     *
     * _Available since v3.1._
     */
    function functionCallWithValue(address target, bytes memory data, uint256 value) internal returns (bytes memory) {
        return functionCallWithValue(target, data, value, \"Address: low-level call with value failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCallWithValue-address-bytes-uint256-}[`functionCallWithValue`], but
     * with `errorMessage` as a fallback revert reason when `target` reverts.
     *
     * _Available since v3.1._
     */
    function functionCallWithValue(address target, bytes memory data, uint256 value, string memory errorMessage) internal returns (bytes memory) {
        require(address(this).balance >= value, \"Address: insufficient balance for call\");
        require(isContract(target), \"Address: call to non-contract\");

        // solhint-disable-next-line avoid-low-level-calls
        (bool success, bytes memory returndata) = target.call{ value: value }(data);
        return _verifyCallResult(success, returndata, errorMessage);
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`],
     * but performing a static call.
     *
     * _Available since v3.3._
     */
    function functionStaticCall(address target, bytes memory data) internal view returns (bytes memory) {
        return functionStaticCall(target, data, \"Address: low-level static call failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-string-}[`functionCall`],
     * but performing a static call.
     *
     * _Available since v3.3._
     */
    function functionStaticCall(address target, bytes memory data, string memory errorMessage) internal view returns (bytes memory) {
        require(isContract(target), \"Address: static call to non-contract\");

        // solhint-disable-next-line avoid-low-level-calls
        (bool success, bytes memory returndata) = target.staticcall(data);
        return _verifyCallResult(success, returndata, errorMessage);
    }

    function _verifyCallResult(bool success, bytes memory returndata, string memory errorMessage) private pure returns(bytes memory) {
        if (success) {
            return returndata;
        } else {
            // Look for revert reason and bubble it up if present
            if (returndata.length > 0) {
                // The easiest way to bubble the revert reason is using memory via assembly

                // solhint-disable-next-line no-inline-assembly
                assembly {
                    let returndata_size := mload(returndata)
                    revert(add(32, returndata), returndata_size)
                }
            } else {
                revert(errorMessage);
            }
        }
    }
}
"
    
// SPDX-License-Identifier: MIT

pragma solidity >=0.6.0 <0.8.0;
import \"../proxy/Initializable.sol\";

/*
 * @dev Provides information about the current execution context, including the
 * sender of the transaction and its data. While these are generally available
 * via msg.sender and msg.data, they should not be accessed in such a direct
 * manner, since when dealing with GSN meta-transactions the account sending and
 * paying for execution may not be the actual sender (as far as an application
 * is concerned).
 *
 * This contract is only required for intermediate, library-like contracts.
 */
abstract contract ContextUpgradeable is Initializable {
    function __Context_init() internal initializer {
        __Context_init_unchained();
    }

    function __Context_init_unchained() internal initializer {
    }
    function _msgSender() internal view virtual returns (address payable) {
        return msg.sender;
    }

    function _msgData() internal view virtual returns (bytes memory) {
        this; // silence state mutability warning without generating bytecode - see https://github.com/ethereum/solidity/issues/2691
        return msg.data;
    }
    uint256[50] private __gap;
}
"
    
// SPDX-License-Identifier: MIT

pragma solidity ^0.7.0;

/**
 * @dev Wrappers over Solidity's arithmetic operations with added overflow
 * checks.
 *
 * Arithmetic operations in Solidity wrap on overflow. This can easily result
 * in bugs, because programmers usually assume that an overflow raises an
 * error, which is the standard behavior in high level programming languages.
 * `SafeMath` restores this intuition by reverting the transaction when an
 * operation overflows.
 *
 * Using this library instead of the unchecked operations eliminates an entire
 * class of bugs, so it's recommended to use it always.
 */
library SafeMath {
    /**
     * @dev Returns the addition of two unsigned integers, with an overflow flag.
     *
     * _Available since v3.4._
     */
    function tryAdd(uint256 a, uint256 b) internal pure returns (bool, uint256) {
        uint256 c = a + b;
        if (c < a) return (false, 0);
        return (true, c);
    }

    /**
     * @dev Returns the substraction of two unsigned integers, with an overflow flag.
     *
     * _Available since v3.4._
     */
    function trySub(uint256 a, uint256 b) internal pure returns (bool, uint256) {
        if (b > a) return (false, 0);
        return (true, a - b);
    }

    /**
     * @dev Returns the multiplication of two unsigned integers, with an overflow flag.
     *
     * _Available since v3.4._
     */
    function tryMul(uint256 a, uint256 b) internal pure returns (bool, uint256) {
        // Gas optimization: this is cheaper than requiring 'a' not being zero, but the
        // benefit is lost if 'b' is also tested.
        // See: https://github.com/OpenZeppelin/openzeppelin-contracts/pull/522
        if (a == 0) return (true, 0);
        uint256 c = a * b;
        if (c / a != b) return (false, 0);
        return (true, c);
    }

    /**
     * @dev Returns the division of two unsigned integers, with a division by zero flag.
     *
     * _Available since v3.4._
     */
    function tryDiv(uint256 a, uint256 b) internal pure returns (bool, uint256) {
        if (b == 0) return (false, 0);
        return (true, a / b);
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers, with a division by zero flag.
     *
     * _Available since v3.4._
     */
    function tryMod(uint256 a, uint256 b) internal pure returns (bool, uint256) {
        if (b == 0) return (false, 0);
        return (true, a % b);
    }

    /**
     * @dev Returns the addition of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity's `+` operator.
     *
     * Requirements:
     *
     * - Addition cannot overflow.
     */
    function add(uint256 a, uint256 b) internal pure returns (uint256) {
        uint256 c = a + b;
        require(c >= a, \"SafeMath: addition overflow\");
        return c;
    }

    /**
     * @dev Returns the subtraction of two unsigned integers, reverting on
     * overflow (when the result is negative).
     *
     * Counterpart to Solidity's `-` operator.
     *
     * Requirements:
     *
     * - Subtraction cannot overflow.
     */
    function sub(uint256 a, uint256 b) internal pure returns (uint256) {
        require(b <= a, \"SafeMath: subtraction overflow\");
        return a - b;
    }

    /**
     * @dev Returns the multiplication of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity's `*` operator.
     *
     * Requirements:
     *
     * - Multiplication cannot overflow.
     */
    function mul(uint256 a, uint256 b) internal pure returns (uint256) {
        if (a == 0) return 0;
        uint256 c = a * b;
        require(c / a == b, \"SafeMath: multiplication overflow\");
        return c;
    }

    /**
     * @dev Returns the integer division of two unsigned integers, reverting on
     * division by zero. The result is rounded towards zero.
     *
     * Counterpart to Solidity's `/` operator. Note: this function uses a
     * `revert` opcode (which leaves remaining gas untouched) while Solidity
     * uses an invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function div(uint256 a, uint256 b) internal pure returns (uint256) {
        require(b > 0, \"SafeMath: division by zero\");
        return a / b;
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
     * reverting when dividing by zero.
     *
     * Counterpart to Solidity's `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function mod(uint256 a, uint256 b) internal pure returns (uint256) {
        require(b > 0, \"SafeMath: modulo by zero\");
        return a % b;
    }

    /**
     * @dev Returns the subtraction of two unsigned integers, reverting with custom message on
     * overflow (when the result is negative).
     *
     * CAUTION: This function is deprecated because it requires allocating memory for the error
     * message unnecessarily. For custom revert reasons use {trySub}.
     *
     * Counterpart to Solidity's `-` operator.
     *
     * Requirements:
     *
     * - Subtraction cannot overflow.
     */
    function sub(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b <= a, errorMessage);
        return a - b;
    }

    /**
     * @dev Returns the integer division of two unsigned integers, reverting with custom message on
     * division by zero. The result is rounded towards zero.
     *
     * CAUTION: This function is deprecated because it requires allocating memory for the error
     * message unnecessarily. For custom revert reasons use {tryDiv}.
     *
     * Counterpart to Solidity's `/` operator. Note: this function uses a
     * `revert` opcode (which leaves remaining gas untouched) while Solidity
     * uses an invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function div(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b > 0, errorMessage);
        return a / b;
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
     * reverting with custom message when dividing by zero.
     *
     * CAUTION: This function is deprecated because it requires allocating memory for the error
     * message unnecessarily. For custom revert reasons use {tryMod}.
     *
     * Counterpart to Solidity's `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function mod(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b > 0, errorMessage);
        return a % b;
    }
}
"
    
// SPDX-License-Identifier: MIT

pragma solidity ^0.7.0;

/**
 * @dev Interface of the ERC20 standard as defined in the EIP.
 */
interface IERC20 {
    /**
     * @dev Returns the amount of tokens in existence.
     */
    function totalSupply() external view returns (uint256);

    /**
     * @dev Returns the amount of tokens owned by `account`.
     */
    function balanceOf(address account) external view returns (uint256);

    /**
     * @dev Moves `amount` tokens from the caller's account to `recipient`.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transfer(address recipient, uint256 amount) external returns (bool);

    /**
     * @dev Returns the remaining number of tokens that `spender` will be
     * allowed to spend on behalf of `owner` through {transferFrom}. This is
     * zero by default.
     *
     * This value changes when {approve} or {transferFrom} are called.
     */
    function allowance(address owner, address spender) external view returns (uint256);

    /**
     * @dev Sets `amount` as the allowance of `spender` over the caller's tokens.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * IMPORTANT: Beware that changing an allowance with this method brings the risk
     * that someone may use both the old and the new allowance by unfortunate
     * transaction ordering. One possible solution to mitigate this race
     * condition is to first reduce the spender's allowance to 0 and set the
     * desired value afterwards:
     * https://github.com/ethereum/EIPs/issues/20#issuecomment-263524729
     *
     * Emits an {Approval} event.
     */
    function approve(address spender, uint256 amount) external returns (bool);

    /**
     * @dev Moves `amount` tokens from `sender` to `recipient` using the
     * allowance mechanism. `amount` is then deducted from the caller's
     * allowance.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transferFrom(address sender, address recipient, uint256 amount) external returns (bool);

    /**
     * @dev Emitted when `value` tokens are moved from one account (`from`) to
     * another (`to`).
     *
     * Note that `value` may be zero.
     */
    event Transfer(address indexed from, address indexed to, uint256 value);

    /**
     * @dev Emitted when the allowance of a `spender` for an `owner` is set by
     * a call to {approve}. `value` is the new allowance.
     */
    event Approval(address indexed owner, address indexed spender, uint256 value);
}
"
    
// SPDX-License-Identifier: MIT

pragma solidity ^0.7.0;

import \"./IERC20.sol\";
import \"../../math/SafeMath.sol\";
import \"../../utils/Address.sol\";

/**
 * @title SafeERC20
 * @dev Wrappers around ERC20 operations that throw on failure (when the token
 * contract returns false). Tokens that return no value (and instead revert or
 * throw on failure) are also supported, non-reverting calls are assumed to be
 * successful.
 * To use this library you can add a `using SafeERC20 for IERC20;` statement to your contract,
 * which allows you to call the safe operations as `token.safeTransfer(...)`, etc.
 */
library SafeERC20 {
    using SafeMath for uint256;
    using Address for address;

    function safeTransfer(IERC20 token, address to, uint256 value) internal {
        _callOptionalReturn(token, abi.encodeWithSelector(token.transfer.selector, to, value));
    }

    function safeTransferFrom(IERC20 token, address from, address to, uint256 value) internal {
        _callOptionalReturn(token, abi.encodeWithSelector(token.transferFrom.selector, from, to, value));
    }

    /**
     * @dev Deprecated. This function has issues similar to the ones found in
     * {IERC20-approve}, and its usage is discouraged.
     *
     * Whenever possible, use {safeIncreaseAllowance} and
     * {safeDecreaseAllowance} instead.
     */
    function safeApprove(IERC20 token, address spender, uint256 value) internal {
        // safeApprove should only be called when setting an initial allowance,
        // or when resetting it to zero. To increase and decrease it, use
        // 'safeIncreaseAllowance' and 'safeDecreaseAllowance'
        // solhint-disable-next-line max-line-length
        require((value == 0) || (token.allowance(address(this), spender) == 0),
            \"SafeERC20: approve from non-zero to non-zero allowance\"
        );
        _callOptionalReturn(token, abi.encodeWithSelector(token.approve.selector, spender, value));
    }

    function safeIncreaseAllowance(IERC20 token, address spender, uint256 value) internal {
        uint256 newAllowance = token.allowance(address(this), spender).add(value);
        _callOptionalReturn(token, abi.encodeWithSelector(token.approve.selector, spender, newAllowance));
    }

    function safeDecreaseAllowance(IERC20 token, address spender, uint256 value) internal {
        uint256 newAllowance = token.allowance(address(this), spender).sub(value, \"SafeERC20: decreased allowance below zero\");
        _callOptionalReturn(token, abi.encodeWithSelector(token.approve.selector, spender, newAllowance));
    }

    /**
     * @dev Imitates a Solidity high-level call (i.e. a regular function call to a contract), relaxing the requirement
     * on the return value: the return value is optional (but if data is returned, it must not be false).
     * @param token The token targeted by the call.
     * @param data The call data (encoded using abi.encode or one of its variants).
     */
    function _callOptionalReturn(IERC20 token, bytes memory data) private {
        // We need to perform a low level call here, to bypass Solidity's return data size checking mechanism, since
        // we're implementing it ourselves. We use {Address.functionCall} to perform this call, which verifies that
        // the target address contains contract code and also asserts for success in the low-level call.

        bytes memory returndata = address(token).functionCall(data, \"SafeERC20: low-level call failed\");
        if (returndata.length > 0) { // Return data is optional
            // solhint-disable-next-line max-line-length
            require(abi.decode(returndata, (bool)), \"SafeERC20: ERC20 operation did not succeed\");
        }
    }
}
"
    
// SPDX-License-Identifier: MIT

pragma solidity ^0.7.0;

/**
 * @dev Collection of functions related to the address type
 */
library Address {
    /**
     * @dev Returns true if `account` is a contract.
     *
     * [IMPORTANT]
     * ====
     * It is unsafe to assume that an address for which this function returns
     * false is an externally-owned account (EOA) and not a contract.
     *
     * Among others, `isContract` will return false for the following
     * types of addresses:
     *
     *  - an externally-owned account
     *  - a contract in construction
     *  - an address where a contract will be created
     *  - an address where a contract lived, but was destroyed
     * ====
     */
    function isContract(address account) internal view returns (bool) {
        // This method relies on extcodesize, which returns 0 for contracts in
        // construction, since the code is only stored at the end of the
        // constructor execution.

        uint256 size;
        // solhint-disable-next-line no-inline-assembly
        assembly { size := extcodesize(account) }
        return size > 0;
    }

    /**
     * @dev Replacement for Solidity's `transfer`: sends `amount` wei to
     * `recipient`, forwarding all available gas and reverting on errors.
     *
     * https://eips.ethereum.org/EIPS/eip-1884[EIP1884] increases the gas cost
     * of certain opcodes, possibly making contracts go over the 2300 gas limit
     * imposed by `transfer`, making them unable to receive funds via
     * `transfer`. {sendValue} removes this limitation.
     *
     * https://diligence.consensys.net/posts/2019/09/stop-using-soliditys-transfer-now/[Learn more].
     *
     * IMPORTANT: because control is transferred to `recipient`, care must be
     * taken to not create reentrancy vulnerabilities. Consider using
     * {ReentrancyGuard} or the
     * https://solidity.readthedocs.io/en/v0.5.11/security-considerations.html#use-the-checks-effects-interactions-pattern[checks-effects-interactions pattern].
     */
    function sendValue(address payable recipient, uint256 amount) internal {
        require(address(this).balance >= amount, \"Address: insufficient balance\");

        // solhint-disable-next-line avoid-low-level-calls, avoid-call-value
        (bool success, ) = recipient.call{ value: amount }(\"\");
        require(success, \"Address: unable to send value, recipient may have reverted\");
    }

    /**
     * @dev Performs a Solidity function call using a low level `call`. A
     * plain`call` is an unsafe replacement for a function call: use this
     * function instead.
     *
     * If `target` reverts with a revert reason, it is bubbled up by this
     * function (like regular Solidity function calls).
     *
     * Returns the raw returned data. To convert to the expected return value,
     * use https://solidity.readthedocs.io/en/latest/units-and-global-variables.html?highlight=abi.decode#abi-encoding-and-decoding-functions[`abi.decode`].
     *
     * Requirements:
     *
     * - `target` must be a contract.
     * - calling `target` with `data` must not revert.
     *
     * _Available since v3.1._
     */
    function functionCall(address target, bytes memory data) internal returns (bytes memory) {
      return functionCall(target, data, \"Address: low-level call failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`], but with
     * `errorMessage` as a fallback revert reason when `target` reverts.
     *
     * _Available since v3.1._
     */
    function functionCall(address target, bytes memory data, string memory errorMessage) internal returns (bytes memory) {
        return functionCallWithValue(target, data, 0, errorMessage);
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`],
     * but also transferring `value` wei to `target`.
     *
     * Requirements:
     *
     * - the calling contract must have an ETH balance of at least `value`.
     * - the called Solidity function must be `payable`.
     *
     * _Available since v3.1._
     */
    function functionCallWithValue(address target, bytes memory data, uint256 value) internal returns (bytes memory) {
        return functionCallWithValue(target, data, value, \"Address: low-level call with value failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCallWithValue-address-bytes-uint256-}[`functionCallWithValue`], but
     * with `errorMessage` as a fallback revert reason when `target` reverts.
     *
     * _Available since v3.1._
     */
    function functionCallWithValue(address target, bytes memory data, uint256 value, string memory errorMessage) internal returns (bytes memory) {
        require(address(this).balance >= value, \"Address: insufficient balance for call\");
        require(isContract(target), \"Address: call to non-contract\");

        // solhint-disable-next-line avoid-low-level-calls
        (bool success, bytes memory returndata) = target.call{ value: value }(data);
        return _verifyCallResult(success, returndata, errorMessage);
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`],
     * but performing a static call.
     *
     * _Available since v3.3._
     */
    function functionStaticCall(address target, bytes memory data) internal view returns (bytes memory) {
        return functionStaticCall(target, data, \"Address: low-level static call failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-string-}[`functionCall`],
     * but performing a static call.
     *
     * _Available since v3.3._
     */
    function functionStaticCall(address target, bytes memory data, string memory errorMessage) internal view returns (bytes memory) {
        require(isContract(target), \"Address: static call to non-contract\");

        // solhint-disable-next-line avoid-low-level-calls
        (bool success, bytes memory returndata) = target.staticcall(data);
        return _verifyCallResult(success, returndata, errorMessage);
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`],
     * but performing a delegate call.
     *
     * _Available since v3.4._
     */
    function functionDelegateCall(address target, bytes memory data) internal returns (bytes memory) {
        return functionDelegateCall(target, data, \"Address: low-level delegate call failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-string-}[`functionCall`],
     * but performing a delegate call.
     *
     * _Available since v3.4._
     */
    function functionDelegateCall(address target, bytes memory data, string memory errorMessage) internal returns (bytes memory) {
        require(isContract(target), \"Address: delegate call to non-contract\");

        // solhint-disable-next-line avoid-low-level-calls
        (bool success, bytes memory returndata) = target.delegatecall(data);
        return _verifyCallResult(success, returndata, errorMessage);
    }

    function _verifyCallResult(bool success, bytes memory returndata, string memory errorMessage) private pure returns(bytes memory) {
        if (success) {
            return returndata;
        } else {
            // Look for revert reason and bubble it up if present
            if (returndata.length > 0) {
                // The easiest way to bubble the revert reason is using memory via assembly

                // solhint-disable-next-line no-inline-assembly
                assembly {
                    let returndata_size := mload(returndata)
                    revert(add(32, returndata), returndata_size)
                }
            } else {
                revert(errorMessage);
            }
        }
    }
}
"
    
// SPDX-License-Identifier: MIT

pragma solidity ^0.7.0;

/**
 * @dev Library for managing
 * https://en.wikipedia.org/wiki/Set_(abstract_data_type)[sets] of primitive
 * types.
 *
 * Sets have the following properties:
 *
 * - Elements are added, removed, and checked for existence in constant time
 * (O(1)).
 * - Elements are enumerated in O(n). No guarantees are made on the ordering.
 *
 * ```
 * contract Example {
 *     // Add the library methods
 *     using EnumerableSet for EnumerableSet.AddressSet;
 *
 *     // Declare a set state variable
 *     EnumerableSet.AddressSet private mySet;
 * }
 * ```
 *
 * As of v3.3.0, sets of type `bytes32` (`Bytes32Set`), `address` (`AddressSet`)
 * and `uint256` (`UintSet`) are supported.
 */
library EnumerableSet {
    // To implement this library for multiple types with as little code
    // repetition as possible, we write it in terms of a generic Set type with
    // bytes32 values.
    // The Set implementation uses private functions, and user-facing
    // implementations (such as AddressSet) are just wrappers around the
    // underlying Set.
    // This means that we can only create new EnumerableSets for types that fit
    // in bytes32.

    struct Set {
        // Storage of set values
        bytes32[] _values;

        // Position of the value in the `values` array, plus 1 because index 0
        // means a value is not in the set.
        mapping (bytes32 => uint256) _indexes;
    }

    /**
     * @dev Add a value to a set. O(1).
     *
     * Returns true if the value was added to the set, that is if it was not
     * already present.
     */
    function _add(Set storage set, bytes32 value) private returns (bool) {
        if (!_contains(set, value)) {
            set._values.push(value);
            // The value is stored at length-1, but we add 1 to all indexes
            // and use 0 as a sentinel value
            set._indexes[value] = set._values.length;
            return true;
        } else {
            return false;
        }
    }

    /**
     * @dev Removes a value from a set. O(1).
     *
     * Returns true if the value was removed from the set, that is if it was
     * present.
     */
    function _remove(Set storage set, bytes32 value) private returns (bool) {
        // We read and store the value's index to prevent multiple reads from the same storage slot
        uint256 valueIndex = set._indexes[value];

        if (valueIndex != 0) { // Equivalent to contains(set, value)
            // To delete an element from the _values array in O(1), we swap the element to delete with the last one in
            // the array, and then remove the last element (sometimes called as 'swap and pop').
            // This modifies the order of the array, as noted in {at}.

            uint256 toDeleteIndex = valueIndex - 1;
            uint256 lastIndex = set._values.length - 1;

            // When the value to delete is the last one, the swap operation is unnecessary. However, since this occurs
            // so rarely, we still do the swap anyway to avoid the gas cost of adding an 'if' statement.

            bytes32 lastvalue = set._values[lastIndex];

            // Move the last value to the index where the value to delete is
            set._values[toDeleteIndex] = lastvalue;
            // Update the index for the moved value
            set._indexes[lastvalue] = toDeleteIndex + 1; // All indexes are 1-based

            // Delete the slot where the moved value was stored
            set._values.pop();

            // Delete the index for the deleted slot
            delete set._indexes[value];

            return true;
        } else {
            return false;
        }
    }

    /**
     * @dev Returns true if the value is in the set. O(1).
     */
    function _contains(Set storage set, bytes32 value) private view returns (bool) {
        return set._indexes[value] != 0;
    }

    /**
     * @dev Returns the number of values on the set. O(1).
     */
    function _length(Set storage set) private view returns (uint256) {
        return set._values.length;
    }

   /**
    * @dev Returns the value stored at position `index` in the set. O(1).
    *
    * Note that there are no guarantees on the ordering of values inside the
    * array, and it may change when more values are added or removed.
    *
    * Requirements:
    *
    * - `index` must be strictly less than {length}.
    */
    function _at(Set storage set, uint256 index) private view returns (bytes32) {
        require(set._values.length > index, \"EnumerableSet: index out of bounds\");
        return set._values[index];
    }

    // Bytes32Set

    struct Bytes32Set {
        Set _inner;
    }

    /**
     * @dev Add a value to a set. O(1).
     *
     * Returns true if the value was added to the set, that is if it was not
     * already present.
     */
    function add(Bytes32Set storage set, bytes32 value) internal returns (bool) {
        return _add(set._inner, value);
    }

    /**
     * @dev Removes a value from a set. O(1).
     *
     * Returns true if the value was removed from the set, that is if it was
     * present.
     */
    function remove(Bytes32Set storage set, bytes32 value) internal returns (bool) {
        return _remove(set._inner, value);
    }

    /**
     * @dev Returns true if the value is in the set. O(1).
     */
    function contains(Bytes32Set storage set, bytes32 value) internal view returns (bool) {
        return _contains(set._inner, value);
    }

    /**
     * @dev Returns the number of values in the set. O(1).
     */
    function length(Bytes32Set storage set) internal view returns (uint256) {
        return _length(set._inner);
    }

   /**
    * @dev Returns the value stored at position `index` in the set. O(1).
    *
    * Note that there are no guarantees on the ordering of values inside the
    * array, and it may change when more values are added or removed.
    *
    * Requirements:
    *
    * - `index` must be strictly less than {length}.
    */
    function at(Bytes32Set storage set, uint256 index) internal view returns (bytes32) {
        return _at(set._inner, index);
    }

    // AddressSet

    struct AddressSet {
        Set _inner;
    }

    /**
     * @dev Add a value to a set. O(1).
     *
     * Returns true if the value was added to the set, that is if it was not
     * already present.
     */
    function add(AddressSet storage set, address value) internal returns (bool) {
        return _add(set._inner, bytes32(uint256(uint160(value))));
    }

    /**
     * @dev Removes a value from a set. O(1).
     *
     * Returns true if the value was removed from the set, that is if it was
     * present.
     */
    function remove(AddressSet storage set, address value) internal returns (bool) {
        return _remove(set._inner, bytes32(uint256(uint160(value))));
    }

    /**
     * @dev Returns true if the value is in the set. O(1).
     */
    function contains(AddressSet storage set, address value) internal view returns (bool) {
        return _contains(set._inner, bytes32(uint256(uint160(value))));
    }

    /**
     * @dev Returns the number of values in the set. O(1).
     */
    function length(AddressSet storage set) internal view returns (uint256) {
        return _length(set._inner);
    }

   /**
    * @dev Returns the value stored at position `index` in the set. O(1).
    *
    * Note that there are no guarantees on the ordering of values inside the
    * array, and it may change when more values are added or removed.
    *
    * Requirements:
    *
    * - `index` must be strictly less than {length}.
    */
    function at(AddressSet storage set, uint256 index) internal view returns (address) {
        return address(uint160(uint256(_at(set._inner, index))));
    }


    // UintSet

    struct UintSet {
        Set _inner;
    }

    /**
     * @dev Add a value to a set. O(1).
     *
     * Returns true if the value was added to the set, that is if it was not
     * already present.
     */
    function add(UintSet storage set, uint256 value) internal returns (bool) {
        return _add(set._inner, bytes32(value));
    }

    /**
     * @dev Removes a value from a set. O(1).
     *
     * Returns true if the value was removed from the set, that is if it was
     * present.
     */
    function remove(UintSet storage set, uint256 value) internal returns (bool) {
        return _remove(set._inner, bytes32(value));
    }

    /**
     * @dev Returns true if the value is in the set. O(1).
     */
    function contains(UintSet storage set, uint256 value) internal view returns (bool) {
        return _contains(set._inner, bytes32(value));
    }

    /**
     * @dev Returns the number of values on the set. O(1).
     */
    function length(UintSet storage set) internal view returns (uint256) {
        return _length(set._inner);
    }

   /**
    * @dev Returns the value stored at position `index` in the set. O(1).
    *
    * Note that there are no guarantees on the ordering of values inside the
    * array, and it may change when more values are added or removed.
    *
    * Requirements:
    *
    * - `index` must be strictly less than {length}.
    */
    function at(UintSet storage set, uint256 index) internal view returns (uint256) {
        return uint256(_at(set._inner, index));
    }
}
"
    
// SPDX-License-Identifier: MIT

pragma solidity ^0.7.0;


/**
 * @dev Wrappers over Solidity's uintXX/intXX casting operators with added overflow
 * checks.
 *
 * Downcasting from uint256/int256 in Solidity does not revert on overflow. This can
 * easily result in undesired exploitation or bugs, since developers usually
 * assume that overflows raise errors. `SafeCast` restores this intuition by
 * reverting the transaction when such an operation overflows.
 *
 * Using this library instead of the unchecked operations eliminates an entire
 * class of bugs, so it's recommended to use it always.
 *
 * Can be combined with {SafeMath} and {SignedSafeMath} to extend it to smaller types, by performing
 * all math on `uint256` and `int256` and then downcasting.
 */
library SafeCast {

    /**
     * @dev Returns the downcasted uint128 from uint256, reverting on
     * overflow (when the input is greater than largest uint128).
     *
     * Counterpart to Solidity's `uint128` operator.
     *
     * Requirements:
     *
     * - input must fit into 128 bits
     */
    function toUint128(uint256 value) internal pure returns (uint128) {
        require(value < 2**128, \"SafeCast: value doesn\\'t fit in 128 bits\");
        return uint128(value);
    }

    /**
     * @dev Returns the downcasted uint64 from uint256, reverting on
     * overflow (when the input is greater than largest uint64).
     *
     * Counterpart to Solidity's `uint64` operator.
     *
     * Requirements:
     *
     * - input must fit into 64 bits
     */
    function toUint64(uint256 value) internal pure returns (uint64) {
        require(value < 2**64, \"SafeCast: value doesn\\'t fit in 64 bits\");
        return uint64(value);
    }

    /**
     * @dev Returns the downcasted uint32 from uint256, reverting on
     * overflow (when the input is greater than largest uint32).
     *
     * Counterpart to Solidity's `uint32` operator.
     *
     * Requirements:
     *
     * - input must fit into 32 bits
     */
    function toUint32(uint256 value) internal pure returns (uint32) {
        require(value < 2**32, \"SafeCast: value doesn\\'t fit in 32 bits\");
        return uint32(value);
    }

    /**
     * @dev Returns the downcasted uint16 from uint256, reverting on
     * overflow (when the input is greater than largest uint16).
     *
     * Counterpart to Solidity's `uint16` operator.
     *
     * Requirements:
     *
     * - input must fit into 16 bits
     */
    function toUint16(uint256 value) internal pure returns (uint16) {
        require(value < 2**16, \"SafeCast: value doesn\\'t fit in 16 bits\");
        return uint16(value);
    }

    /**
     * @dev Returns the downcasted uint8 from uint256, reverting on
     * overflow (when the input is greater than largest uint8).
     *
     * Counterpart to Solidity's `uint8` operator.
     *
     * Requirements:
     *
     * - input must fit into 8 bits.
     */
    function toUint8(uint256 value) internal pure returns (uint8) {
        require(value < 2**8, \"SafeCast: value doesn\\'t fit in 8 bits\");
        return uint8(value);
    }

    /**
     * @dev Converts a signed int256 into an unsigned uint256.
     *
     * Requirements:
     *
     * - input must be greater than or equal to 0.
     */
    function toUint256(int256 value) internal pure returns (uint256) {
        require(value >= 0, \"SafeCast: value must be positive\");
        return uint256(value);
    }

    /**
     * @dev Returns the downcasted int128 from int256, reverting on
     * overflow (when the input is less than smallest int128 or
     * greater than largest int128).
     *
     * Counterpart to Solidity's `int128` operator.
     *
     * Requirements:
     *
     * - input must fit into 128 bits
     *
     * _Available since v3.1._
     */
    function toInt128(int256 value) internal pure returns (int128) {
        require(value >= -2**127 && value < 2**127, \"SafeCast: value doesn\\'t fit in 128 bits\");
        return int128(value);
    }

    /**
     * @dev Returns the downcasted int64 from int256, reverting on
     * overflow (when the input is less than smallest int64 or
     * greater than largest int64).
     *
     * Counterpart to Solidity's `int64` operator.
     *
     * Requirements:
     *
     * - input must fit into 64 bits
     *
     * _Available since v3.1._
     */
    function toInt64(int256 value) internal pure returns (int64) {
        require(value >= -2**63 && value < 2**63, \"SafeCast: value doesn\\'t fit in 64 bits\");
        return int64(value);
    }

    /**
     * @dev Returns the downcasted int32 from int256, reverting on
     * overflow (when the input is less than smallest int32 or
     * greater than largest int32).
     *
     * Counterpart to Solidity's `int32` operator.
     *
     * Requirements:
     *
     * - input must fit into 32 bits
     *
     * _Available since v3.1._
     */
    function toInt32(int256 value) internal pure returns (int32) {
        require(value >= -2**31 && value < 2**31, \"SafeCast: value doesn\\'t fit in 32 bits\");
        return int32(value);
    }

    /**
     * @dev Returns the downcasted int16 from int256, reverting on
     * overflow (when the input is less than smallest int16 or
     * greater than largest int16).
     *
     * Counterpart to Solidity's `int16` operator.
     *
     * Requirements:
     *
     * - input must fit into 16 bits
     *
     * _Available since v3.1._
     */
    function toInt16(int256 value) internal pure returns (int16) {
        require(value >= -2**15 && value < 2**15, \"SafeCast: value doesn\\'t fit in 16 bits\");
        return int16(value);
    }

    /**
     * @dev Returns the downcasted int8 from int256, reverting on
     * overflow (when the input is less than smallest int8 or
     * greater than largest int8).
     *
     * Counterpart to Solidity's `int8` operator.
     *
     * Requirements:
     *
     * - input must fit into 8 bits.
     *
     * _Available since v3.1._
     */
    function toInt8(int256 value) internal pure returns (int8) {
        require(value >= -2**7 && value < 2**7, \"SafeCast: value doesn\\'t fit in 8 bits\");
        return int8(value);
    }

    /**
     * @dev Converts an unsigned uint256 into a signed int256.
     *
     * Requirements:
     *
     * - input must be less than or equal to maxInt256.
     */
    function toInt256(uint256 value) internal pure returns (int256) {
        require(value < 2**255, \"SafeCast: value doesn't fit in an int256\");
        return int256(value);
    }
}
"
    
// SPDX-License-Identifier: MIT

/**
 * Based on https://github.com/OpenZeppelin/openzeppelin-contracts-upgradeable/blob/v3.4.0-solc-0.7/contracts/access/OwnableUpgradeable.sol
 *
 * Changes:
 * - Added owner argument to initializer
 * - Reformatted styling in line with this repository.
 */

/*
The MIT License (MIT)

Copyright (c) 2016-2020 zOS Global Limited

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
\"Software\"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

/* solhint-disable func-name-mixedcase */

pragma solidity 0.7.6;

import \"@openzeppelin/contracts-upgradeable/utils/ContextUpgradeable.sol\";
import \"@openzeppelin/contracts-upgradeable/proxy/Initializable.sol\";

/**
 * @dev Contract module which provides a basic access control mechanism, where
 * there is an account (an owner) that can be granted exclusive access to
 * specific functions.
 *
 * By default, the owner account will be the one that deploys the contract. This
 * can later be changed with {transferOwnership}.
 *
 * This module is used through inheritance. It will make available the modifier
 * `onlyOwner`, which can be applied to your functions to restrict their use to
 * the owner.
 */
abstract contract OwnableUpgradeable is Initializable, ContextUpgradeable {
\taddress private _owner;

\tevent OwnershipTransferred(
\t\taddress indexed previousOwner,
\t\taddress indexed newOwner
\t);

\t/**
\t * @dev Initializes the contract setting the deployer as the initial owner.
\t */
\tfunction __Ownable_init(address owner_) internal initializer {
\t\t__Context_init_unchained();
\t\t__Ownable_init_unchained(owner_);
\t}

\tfunction __Ownable_init_unchained(address owner_) internal initializer {
\t\t_owner = owner_;
\t\temit OwnershipTransferred(address(0), owner_);
\t}

\t/**
\t * @dev Returns the address of the current owner.
\t */
\tfunction owner() public view virtual returns (address) {
\t\treturn _owner;
\t}

\t/**
\t * @dev Throws if called by any account other than the owner.
\t */
\tmodifier onlyOwner() {
\t\trequire(owner() == _msgSender(), \"Ownable: caller is not the owner\");
\t\t_;
\t}

\t/**
\t * @dev Leaves the contract without owner. It will not be possible to call
\t * `onlyOwner` functions anymore. Can only be called by the current owner.
\t *
\t * NOTE: Renouncing ownership will leave the contract without an owner,
\t * thereby removing any functionality that is only available to the owner.
\t */
\tfunction renounceOwnership() public virtual onlyOwner {
\t\temit OwnershipTransferred(_owner, address(0));
\t\t_owner = address(0);
\t}

\t/**
\t * @dev Transfers ownership of the contract to a new account (`newOwner`).
\t * Can only be called by the current owner.
\t */
\tfunction transferOwnership(address newOwner) public virtual onlyOwner {
\t\trequire(newOwner != address(0), \"Ownable: new owner is the zero address\");
\t\temit OwnershipTransferred(_owner, newOwner);
\t\t_owner = newOwner;
\t}

\tuint256[49] private __gap;
}
"
    
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

interface IETHtxAMM {
\t/* Views */

\tfunction cRatio()
\t\texternal
\t\tview
\t\treturns (uint256 numerator, uint256 denominator);

\tfunction cRatioBelowTarget() external view returns (bool);

\tfunction ethNeeded() external view returns (uint256);

\tfunction ethtx() external view returns (address);

\tfunction exactEthToEthtx(uint256 amountEthIn)
\t\texternal
\t\tview
\t\treturns (uint256);

\tfunction ethToExactEthtx(uint256 amountEthtxOut)
\t\texternal
\t\tview
\t\treturns (uint256);

\tfunction exactEthtxToEth(uint256 amountEthtxIn)
\t\texternal
\t\tview
\t\treturns (uint256);

\tfunction ethtxToExactEth(uint256 amountEthOut)
\t\texternal
\t\tview
\t\treturns (uint256);

\tfunction ethSupply() external view returns (uint256);

\tfunction ethSupplyTarget() external view returns (uint256);

\tfunction ethtxAvailable() external view returns (uint256);

\tfunction ethtxOutstanding() external view returns (uint256);

\tfunction feeLogic() external view returns (address);

\tfunction gasOracle() external view returns (address);

\tfunction gasPerETHtx() external pure returns (uint256);

\tfunction gasPrice() external view returns (uint256);

\tfunction gasPriceAtRedemption() external view returns (uint256);

\tfunction maxGasPrice() external view returns (uint256);

\tfunction targetCRatio()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction weth() external view returns (address);

\t/* Mutators */

\tfunction swapEthForEthtx(uint256 deadline) external payable;

\tfunction swapWethForEthtx(uint256 amountIn, uint256 deadline) external;

\tfunction swapEthForExactEthtx(uint256 amountOut, uint256 deadline)
\t\texternal
\t\tpayable;

\tfunction swapWethForExactEthtx(
\t\tuint256 amountInMax,
\t\tuint256 amountOut,
\t\tuint256 deadline
\t) external;

\tfunction swapExactEthForEthtx(uint256 amountOutMin, uint256 deadline)
\t\texternal
\t\tpayable;

\tfunction swapExactWethForEthtx(
\t\tuint256 amountIn,
\t\tuint256 amountOutMin,
\t\tuint256 deadline
\t) external;

\tfunction swapEthtxForEth(
\t\tuint256 amountIn,
\t\tuint256 deadline,
\t\tbool asWETH
\t) external;

\tfunction swapEthtxForExactEth(
\t\tuint256 amountInMax,
\t\tuint256 amountOut,
\t\tuint256 deadline,
\t\tbool asWETH
\t) external;

\tfunction swapExactEthtxForEth(
\t\tuint256 amountIn,
\t\tuint256 amountOutMin,
\t\tuint256 deadline,
\t\tbool asWETH
\t) external;

\tfunction pause() external;

\tfunction recoverUnsupportedERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction setEthtx(address account) external;

\tfunction setGasOracle(address account) external;

\tfunction setTargetCRatio(uint128 numerator, uint128 denominator) external;

\tfunction setWETH(address account) external;

\tfunction unpause() external;

\t/* Events */

\tevent ETHtxSet(address indexed author, address indexed account);
\tevent GasOracleSet(address indexed author, address indexed account);
\tevent RecoveredUnsupported(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
\tevent TargetCRatioSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent WETHSet(address indexed author, address indexed account);
}
"
    
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

import \"@openzeppelin/contracts-upgradeable/utils/ContextUpgradeable.sol\";
import \"@openzeppelin/contracts-upgradeable/proxy/Initializable.sol\";

import \"./ETHtxRewardsManagerData.sol\";
import \"../../exchanges/interfaces/IETHtxAMM.sol\";
import \"../interfaces/IETHtxRewardsManager.sol\";
import \"../interfaces/IETHmxRewards.sol\";
import \"../interfaces/ILPRewards.sol\";
import \"../../access/OwnableUpgradeable.sol\";
import \"../RewardsManager/RewardsManager.sol\";

contract ETHtxRewardsManager is
\tInitializable,
\tContextUpgradeable,
\tOwnableUpgradeable,
\tRewardsManager,
\tETHtxRewardsManagerData,
\tIETHtxRewardsManager
{
\tusing EnumerableSet for EnumerableSet.AddressSet;
\tusing SafeERC20 for IERC20;
\tusing SafeMath for uint256;

\tstruct ETHtxRewardsManagerArgs {
\t\taddress defaultRecipient;
\t\taddress rewardsToken;
\t\taddress ethmxRewards;
\t\taddress ethtx;
\t\taddress ethtxAMM;
\t\taddress lpRewards;
\t\tShareData[] shares;
\t}

\t/* Constructor */

\tconstructor(address owner_) RewardsManager(owner_) {
\t\treturn;
\t}

\t/* Initializer */

\t// init inherited from RewardsManager

\tfunction ethtxRewardsManagerPostInit(ETHtxRewardsManagerArgs memory _args)
\t\texternal
\t\tvirtual
\t\tonlyOwner
\t{
\t\taddress sender = _msgSender();

\t\t_rewardsToken = _args.rewardsToken;
\t\temit RewardsTokenSet(sender, _args.rewardsToken);

\t\tsetDefaultRecipient(_args.defaultRecipient);

\t\t_ethmxRewards = _args.ethmxRewards;
\t\temit EthmxRewardsSet(sender, _args.ethmxRewards);

\t\t_ethtx = _args.ethtx;
\t\temit EthtxSet(sender, _args.ethtx);

\t\t_ethtxAMM = _args.ethtxAMM;
\t\temit EthtxAMMSet(sender, _args.ethtxAMM);

\t\t_lpRewards = _args.lpRewards;
\t\temit LPRewardsSet(sender, _args.lpRewards);

\t\tsetSharesBatch(_args.shares);
\t}

\t/* External Mutators */

\tfunction convertETHtx() public virtual override {
\t\tIERC20 ethtxHandle = IERC20(ethtx());
\t\tuint256 amount = ethtxHandle.balanceOf(address(this));
\t\tif (amount == 0) {
\t\t\treturn;
\t\t}

\t\taddress ethtxAMM_ = ethtxAMM(); // Gas savings
\t\tethtxHandle.safeIncreaseAllowance(ethtxAMM_, amount);

\t\t// solhint-disable-next-line not-rely-on-time
\t\tIETHtxAMM(ethtxAMM_).swapEthtxForEth(amount, block.timestamp, true);
\t}

\tfunction distributeRewards() external virtual override returns (uint256) {
\t\tconvertETHtx();
\t\tuint256 rewards = sendRewards();
\t\tif (rewards != 0) {
\t\t\tnotifyRecipients();
\t\t}
\t\treturn rewards;
\t}

\tfunction notifyRecipients() public virtual override {
\t\t_notifyEthmxRewards();
\t\t_notifyLpRewards();
\t}

\tfunction sendRewards() public virtual override returns (uint256) {
\t\tuint256 rewards = _currentRewardsBalance();
\t\tif (rewards == 0) {
\t\t\treturn 0;
\t\t}

\t\tuint256 totalShares_ = totalShares();

\t\tfor (uint256 i = 0; i < _recipients.length(); i++) {
\t\t\t_sendTo(_recipients.at(i), totalShares_, rewards);
\t\t}

\t\t_totalRewardsRedeemed += rewards;
\t\treturn rewards;
\t}

\tfunction setEthmxRewards(address account) public virtual override onlyOwner {
\t\t_ethmxRewards = account;
\t\temit EthmxRewardsSet(_msgSender(), account);
\t}

\tfunction setEthtx(address account) public virtual override onlyOwner {
\t\t_ethtx = account;
\t\temit EthtxSet(_msgSender(), account);
\t}

\tfunction setEthtxAMM(address account) public virtual override onlyOwner {
\t\t_ethtxAMM = account;
\t\temit EthtxAMMSet(_msgSender(), account);
\t}

\tfunction setLPRewards(address account) public virtual override onlyOwner {
\t\t_lpRewards = account;
\t\temit LPRewardsSet(_msgSender(), account);
\t}

\t/* Public Views */

\tfunction ethmxRewards() public view virtual override returns (address) {
\t\treturn _ethmxRewards;
\t}

\tfunction ethtx() public view virtual override returns (address) {
\t\treturn _ethtx;
\t}

\tfunction ethtxAMM() public view virtual override returns (address) {
\t\treturn _ethtxAMM;
\t}

\tfunction lpRewards() public view virtual override returns (address) {
\t\treturn _lpRewards;
\t}

\t/* Internal Mutators */

\tfunction _notifyEthmxRewards() internal virtual {
\t\tIETHmxRewards ethmxRewardsHandle = IETHmxRewards(ethmxRewards());
\t\tif (ethmxRewardsHandle.readyForUpdate()) {
\t\t\tethmxRewardsHandle.updateAccrual();
\t\t}
\t}

\tfunction _notifyLpRewards() internal virtual {
\t\tILPRewards(lpRewards()).updateAccrual();
\t}

\tfunction _sendTo(
\t\taddress account,
\t\tuint256 totalShares_,
\t\tuint256 totalRewards
\t) internal virtual {
\t\tShares storage s = _shares[account];
\t\tuint256 amount = totalRewards.mul(s.active).div(totalShares_);

\t\tIERC20(_rewardsToken).safeTransfer(account, amount);
\t}
}
"
    
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

abstract contract ETHtxRewardsManagerData {
\taddress internal _ethmxRewards;
\taddress internal _ethtx;
\taddress internal _ethtxAMM;
\taddress internal _lpRewards;

\tuint256[46] private __gap;
}
"
    
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

import \"@openzeppelin/contracts-upgradeable/utils/ContextUpgradeable.sol\";
import \"@openzeppelin/contracts/utils/EnumerableSet.sol\";
import \"@openzeppelin/contracts/token/ERC20/IERC20.sol\";
import \"@openzeppelin/contracts-upgradeable/proxy/Initializable.sol\";
import \"@openzeppelin/contracts/utils/SafeCast.sol\";
import \"@openzeppelin/contracts/token/ERC20/SafeERC20.sol\";
import \"@openzeppelin/contracts/math/SafeMath.sol\";

import \"./RewardsManagerData.sol\";
import \"../interfaces/IRewardsManager.sol\";
import \"../../access/OwnableUpgradeable.sol\";

contract RewardsManager is
\tInitializable,
\tContextUpgradeable,
\tOwnableUpgradeable,
\tRewardsManagerData,
\tIRewardsManager
{
\tusing EnumerableSet for EnumerableSet.AddressSet;
\tusing SafeCast for uint256;
\tusing SafeERC20 for IERC20;
\tusing SafeMath for uint256;
\tusing SafeMath for uint128;

\tstruct RewardsManagerArgs {
\t\taddress defaultRecipient;
\t\taddress rewardsToken;
\t\tShareData[] shares;
\t}

\t/* Constructor */

\tconstructor(address owner_) {
\t\tinit(owner_);
\t}

\t/* Initializers */

\tfunction init(address owner_) public virtual initializer {
\t\t__Context_init_unchained();
\t\t__Ownable_init_unchained(owner_);
\t}

\tfunction postInit(RewardsManagerArgs memory _args)
\t\texternal
\t\tvirtual
\t\tonlyOwner
\t{
\t\taddress sender = _msgSender();

\t\t_rewardsToken = _args.rewardsToken;
\t\temit RewardsTokenSet(sender, _args.rewardsToken);

\t\tsetDefaultRecipient(_args.defaultRecipient);

\t\tsetSharesBatch(_args.shares);
\t}

\t/* External Views */

\tfunction defaultRecipient()
\t\texternal
\t\tview
\t\tvirtual
\t\toverride
\t\treturns (address)
\t{
\t\treturn _defaultRecipient;
\t}

\tfunction rewardsToken() public view virtual override returns (address) {
\t\treturn _rewardsToken;
\t}

\tfunction sharesFor(address account)
\t\texternal
\t\tview
\t\tvirtual
\t\toverride
\t\treturns (uint128 active, uint128 total)
\t{
\t\tShares storage s = _shares[account];
\t\treturn (s.active, s.total);
\t}

\tfunction totalRewardsAccrued()
\t\texternal
\t\tview
\t\tvirtual
\t\toverride
\t\treturns (uint256)
\t{
\t\t// Overflow is OK
\t\treturn _currentRewardsBalance() + _totalRewardsRedeemed;
\t}

\tfunction totalRewardsRedeemed()
\t\texternal
\t\tview
\t\tvirtual
\t\toverride
\t\treturns (uint256)
\t{
\t\treturn _totalRewardsRedeemed;
\t}

\tfunction totalShares() public view virtual override returns (uint256 total) {
\t\tfor (uint256 i = 0; i < _recipients.length(); i++) {
\t\t\ttotal += _shares[_recipients.at(i)].total;
\t\t}
\t}

\t/* External Mutators */

\tfunction activateShares() external virtual override {
\t\t_activate(_msgSender());
\t}

\tfunction activateSharesFor(address account)
\t\texternal
\t\tvirtual
\t\toverride
\t\tonlyOwner
\t{
\t\t_activate(account);
\t}

\tfunction addShares(address account, uint128 amount)
\t\texternal
\t\tvirtual
\t\toverride
\t\tonlyOwner
\t{
\t\trequire(
\t\t\taccount != address(0),
\t\t\t\"RewardsManager: cannot add shares to zero address\"
\t\t);
\t\trequire(
\t\t\taccount != address(this),
\t\t\t\"RewardsManager: cannot add shares to this contract address\"
\t\t);
\t\trequire(amount != 0, \"RewardsManager: cannot add zero shares\");

\t\tShares storage s = _shares[account];
\t\tif (s.active == 0) {
\t\t\t// Add to inactive value
\t\t\tShares storage d = _shares[_defaultRecipient];
\t\t\td.active = d.active.add(amount).toUint128();
\t\t} else {
\t\t\ts.active = s.active.add(amount).toUint128();
\t\t}
\t\ts.total = s.total.add(amount).toUint128();
\t\t_recipients.add(account);
\t\temit SharesAdded(_msgSender(), account, amount);
\t}

\tfunction deactivateShares() external virtual override {
\t\t_deactivate(_msgSender());
\t}

\tfunction deactivateSharesFor(address account)
\t\texternal
\t\tvirtual
\t\toverride
\t\tonlyOwner
\t{
\t\t_deactivate(account);
\t}

\tfunction recoverUnsupportedERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external virtual override onlyOwner {
\t\trequire(
\t\t\ttoken != _rewardsToken,
\t\t\t\"RewardsManager: cannot recover rewards token\"
\t\t);
\t\tIERC20(token).safeTransfer(to, amount);
\t\temit RecoveredUnsupported(_msgSender(), token, to, amount);
\t}

\tfunction removeShares(address account, uint128 amount)
\t\texternal
\t\tvirtual
\t\toverride
\t\tonlyOwner
\t{
\t\trequire(amount != 0, \"RewardsManager: cannot remove zero shares\");

\t\tShares storage s = _shares[account];
\t\tif (s.active == 0) {
\t\t\t// Remove from inactive value
\t\t\tShares storage d = _shares[_defaultRecipient];
\t\t\td.active = d.active.sub(amount).toUint128();
\t\t} else {
\t\t\ts.active = s.active.sub(amount).toUint128();
\t\t}
\t\ts.total = s.total.sub(amount).toUint128();
\t\tif (s.total == 0) {
\t\t\t_recipients.remove(account);
\t\t}
\t\temit SharesRemoved(_msgSender(), account, amount);
\t}

\tfunction setDefaultRecipient(address account)
\t\tpublic
\t\tvirtual
\t\toverride
\t\tonlyOwner
\t{
\t\trequire(
\t\t\taccount != address(0),
\t\t\t\"RewardsManager: cannot set to zero address\"
\t\t);
\t\trequire(
\t\t\taccount != address(this),
\t\t\t\"RewardsManager: cannot set to this contract\"
\t\t);

\t\t// Activate
\t\t_activate(account);

\t\t// Move any inactive shares
\t\tShares storage original = _shares[_defaultRecipient];
\t\tif (original.active > original.total) {
\t\t\tuint128 inactive = original.active - original.total;
\t\t\toriginal.active -= inactive;

\t\t\tShares storage next = _shares[account];
\t\t\tnext.active = next.active.add(inactive).toUint128();
\t\t}

\t\tif (original.total == 0) {
\t\t\t_recipients.remove(_defaultRecipient);
\t\t}
\t\t_defaultRecipient = account;
\t\t_recipients.add(account);
\t\temit DefaultRecipientSet(_msgSender(), account);
\t}

\tfunction setRewardsToken(address token) public virtual override onlyOwner {
\t\t_rewardsToken = token;
\t\temit RewardsTokenSet(_msgSender(), token);
\t}

\tfunction setShares(
\t\taddress account,
\t\tuint128 value,
\t\tbool isActive
\t) public virtual override onlyOwner {
\t\trequire(
\t\t\taccount != address(0),
\t\t\t\"RewardsManager: cannot set shares for zero address\"
\t\t);
\t\trequire(
\t\t\taccount != address(this),
\t\t\t\"RewardsManager: cannot set shares for this contract address\"
\t\t);

\t\t// Gas savings
\t\taddress defaultRecipient_ = _defaultRecipient;
\t\tShares storage d = _shares[defaultRecipient_];

\t\tif (account == defaultRecipient_) {
\t\t\td.active = d.active.sub(d.total).add(value).toUint128();
\t\t\td.total = value;
\t\t\temit SharesSet(_msgSender(), account, value, isActive);
\t\t\treturn;
\t\t}

\t\tShares storage s = _shares[account];

\t\tif (s.total != 0 && s.active == 0) {
\t\t\t// Subtract old inactive value
\t\t\td.active = d.active.sub(s.total).toUint128();
\t\t}

\t\tif (!isActive) {
\t\t\ts.active = 0;
\t\t\t// Add new inactive value
\t\t\td.active = d.active.add(value).toUint128();
\t\t} else {
\t\t\ts.active = value;
\t\t}

\t\ts.total = value;
\t\tif (value != 0) {
\t\t\t_recipients.add(account);
\t\t} else {
\t\t\t_recipients.remove(account);
\t\t}
\t\temit SharesSet(_msgSender(), account, value, isActive);
\t}

\tfunction setSharesBatch(ShareData[] memory batch)
\t\tpublic
\t\tvirtual
\t\toverride
\t\tonlyOwner
\t{
\t\tfor (uint256 i = 0; i < batch.length; i++) {
\t\t\tsetShares(batch[i].account, batch[i].value, batch[i].isActive);
\t\t}
\t}

\t/* Internal Views */

\tfunction _currentRewardsBalance() internal view virtual returns (uint256) {
\t\treturn IERC20(_rewardsToken).balanceOf(address(this));
\t}

\t/* Internal Mutators */

\tfunction _activate(address account) internal virtual {
\t\tShares storage s = _shares[account];

\t\t// Do nothing if already active
\t\tif (s.total == 0 || s.active > 0) {
\t\t\treturn;
\t\t}

\t\tShares storage d = _shares[_defaultRecipient];

\t\ts.active = s.total;
\t\td.active = d.active.sub(s.total).toUint128();
\t\temit SharesActivated(_msgSender(), account);
\t}

\tfunction _deactivate(address account) internal virtual {
\t\t// Skip for the default recipient
\t\tif (account == _defaultRecipient) {
\t\t\treturn;
\t\t}

\t\tShares storage s = _shares[account];

\t\t// Do nothing if already deactivated
\t\tif (s.active == 0) {
\t\t\treturn;
\t\t}

\t\tShares storage d = _shares[_defaultRecipient];

\t\ts.active = 0;
\t\td.active = d.active.add(s.total).toUint128();
\t\temit SharesDeactivated(_msgSender(), account);
\t}
}
"
    
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

import \"@openzeppelin/contracts/utils/EnumerableSet.sol\";

abstract contract RewardsManagerData {
\tstruct Shares {
\t\tuint128 active;
\t\tuint128 total;
\t}

\taddress internal _rewardsToken;
\taddress internal _defaultRecipient;
\tuint256 internal _totalRewardsRedeemed;
\tEnumerableSet.AddressSet internal _recipients;
\tmapping(address => Shares) internal _shares;

\tuint256[45] private __gap;
}
"
    
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

interface IETHmxRewards {
\t/* Views */

\tfunction accrualUpdateInterval() external view returns (uint256);

\tfunction accruedRewardsPerToken() external view returns (uint256);

\tfunction accruedRewardsPerTokenLast(address account)
\t\texternal
\t\tview
\t\treturns (uint256);

\tfunction ethmx() external view returns (address);

\tfunction lastAccrualUpdate() external view returns (uint256);

\tfunction lastRewardsBalanceOf(address account)
\t\texternal
\t\tview
\t\treturns (uint256);

\tfunction lastStakedBalanceOf(address account)
\t\texternal
\t\tview
\t\treturns (uint256);

\tfunction lastTotalRewardsAccrued() external view returns (uint256);

\tfunction readyForUpdate() external view returns (bool);

\tfunction rewardsBalanceOf(address account) external view returns (uint256);

\tfunction stakedBalanceOf(address account) external view returns (uint256);

\tfunction totalRewardsAccrued() external view returns (uint256);

\tfunction totalRewardsRedeemed() external view returns (uint256);

\tfunction totalStaked() external view returns (uint256);

\tfunction unredeemableRewards() external view returns (uint256);

\tfunction weth() external view returns (address);

\t/* Mutators */

\tfunction exit(bool asWETH) external;

\tfunction pause() external;

\tfunction recoverUnredeemableRewards(address to, uint256 amount) external;

\tfunction recoverUnstaked(address to, uint256 amount) external;

\tfunction recoverUnsupportedERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction redeemAllRewards(bool asWETH) external;

\tfunction redeemReward(uint256 amount, bool asWETH) external;

\tfunction setAccrualUpdateInterval(uint256 interval) external;

\tfunction setEthmx(address account) external;

\tfunction setWeth(address account) external;

\tfunction stake(uint256 amount) external;

\tfunction unpause() external;

\tfunction unstake(uint256 amount) external;

\tfunction unstakeAll() external;

\tfunction updateAccrual() external;

\tfunction updateReward() external;

\t/* Events */

\tevent AccrualUpdated(address indexed author, uint256 accruedRewards);
\tevent AccrualUpdateIntervalSet(address indexed author, uint256 interval);
\tevent ETHmxSet(address indexed author, address indexed account);
\tevent RecoveredUnredeemableRewards(
\t\taddress indexed author,
\t\taddress indexed to,
\t\tuint256 amount
\t);
\tevent RecoveredUnstaked(
\t\taddress indexed author,
\t\taddress indexed to,
\t\tuint256 amount
\t);
\tevent RecoveredUnsupported(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
\tevent RewardPaid(address indexed to, uint256 amount);
\tevent Staked(address indexed account, uint256 amount);
\tevent Unstaked(address indexed account, uint256 amount);
\tevent WETHSet(address indexed author, address indexed account);
}
"
    
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

interface IETHtxRewardsManager {
\t/* Views */

\tfunction ethmxRewards() external view returns (address);

\tfunction ethtx() external view returns (address);

\tfunction ethtxAMM() external view returns (address);

\tfunction lpRewards() external view returns (address);

\t/* Mutators */

\tfunction convertETHtx() external;

\tfunction distributeRewards() external returns (uint256);

\tfunction notifyRecipients() external;

\tfunction sendRewards() external returns (uint256);

\tfunction setEthmxRewards(address account) external;

\tfunction setEthtx(address account) external;

\tfunction setEthtxAMM(address account) external;

\tfunction setLPRewards(address account) external;

\t/* Events */

\tevent EthmxRewardsSet(address indexed author, address indexed account);
\tevent EthtxSet(address indexed author, address indexed account);
\tevent EthtxAMMSet(address indexed author, address indexed account);
\tevent LPRewardsSet(address indexed author, address indexed account);
\tevent RewardsSent(
\t\taddress indexed author,
\t\taddress indexed to,
\t\tuint256 amount
\t);
}
"
    
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

interface ILPRewards {
\t/* Views */

\tfunction accruedRewardsPerTokenFor(address token)
\t\texternal
\t\tview
\t\treturns (uint256);

\tfunction accruedRewardsPerTokenLastFor(address account, address token)
\t\texternal
\t\tview
\t\treturns (uint256);

\tfunction lastRewardsBalanceOf(address account)
\t\texternal
\t\tview
\t\treturns (uint256);

\tfunction lastRewardsBalanceOfFor(address account, address token)
\t\texternal
\t\tview
\t\treturns (uint256);

\tfunction lastTotalRewardsAccrued() external view returns (uint256);

\tfunction lastTotalRewardsAccruedFor(address token)
\t\texternal
\t\tview
\t\treturns (uint256);

\tfunction numStakingTokens() external view returns (uint256);

\tfunction rewardsBalanceOf(address account) external view returns (uint256);

\tfunction rewardsBalanceOfFor(address account, address token)
\t\texternal
\t\tview
\t\treturns (uint256);

\tfunction rewardsForToken(address token) external view returns (uint256);

\tfunction rewardsToken() external view returns (address);

\tfunction sharesFor(address account, address token)
\t\texternal
\t\tview
\t\treturns (uint256);

\tfunction sharesPerToken(address token) external view returns (uint256);

\tfunction stakedBalanceOf(address account, address token)
\t\texternal
\t\tview
\t\treturns (uint256);

\tfunction stakingTokenAt(uint256 index) external view returns (address);

\tfunction supportsStakingToken(address token) external view returns (bool);

\tfunction totalRewardsAccrued() external view returns (uint256);

\tfunction totalRewardsAccruedFor(address token)
\t\texternal
\t\tview
\t\treturns (uint256);

\tfunction totalRewardsRedeemed() external view returns (uint256);

\tfunction totalRewardsRedeemedFor(address token)
\t\texternal
\t\tview
\t\treturns (uint256);

\tfunction totalShares() external view returns (uint256);

\tfunction totalSharesFor(address account) external view returns (uint256);

\tfunction totalSharesForToken(address token) external view returns (uint256);

\tfunction totalStaked(address token) external view returns (uint256);

\tfunction unredeemableRewards() external view returns (uint256);

\tfunction valuePerTokenImpl(address token) external view returns (address);

\t/* Mutators */

\tfunction addToken(address token, address tokenValueImpl) external;

\tfunction changeTokenValueImpl(address token, address tokenValueImpl)
\t\texternal;

\tfunction exit(bool asWETH) external;

\tfunction exitFrom(address token, bool asWETH) external;

\tfunction pause() external;

\tfunction recoverUnredeemableRewards(address to, uint256 amount) external;

\tfunction recoverUnstaked(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction redeemAllRewards(bool asWETH) external;

\tfunction redeemAllRewardsFrom(address token, bool asWETH) external;

\tfunction redeemReward(uint256 amount, bool asWETH) external;

\tfunction redeemRewardFrom(
\t\taddress token,
\t\tuint256 amount,
\t\tbool asWETH
\t) external;

\tfunction removeToken(address token) external;

\tfunction setRewardsToken(address token) external;

\tfunction stake(address token, uint256 amount) external;

\tfunction unpause() external;

\tfunction unstake(address token, uint256 amount) external;

\tfunction unstakeAll() external;

\tfunction unstakeAllFrom(address token) external;

\tfunction updateAccrual() external;

\tfunction updateReward() external;

\tfunction updateRewardFor(address token) external;

\t/* Events */

\tevent AccrualUpdated(address indexed author, uint256 accruedRewards);
\tevent RecoveredUnredeemableRewards(
\t\taddress indexed author,
\t\taddress indexed to,
\t\tuint256 amount
\t);
\tevent RecoveredUnstaked(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
\tevent RewardPaid(
\t\taddress indexed account,
\t\taddress indexed token,
\t\tuint256 amount
\t);
\tevent RewardsTokenSet(address indexed author, address indexed token);
\tevent Staked(address indexed account, address indexed token, uint256 amount);
\tevent TokenAdded(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed tokenValueImpl
\t);
\tevent TokenRemoved(address indexed author, address indexed token);
\tevent TokenValueImplChanged(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed tokenValueImpl
\t);
\tevent Unstaked(
\t\taddress indexed account,
\t\taddress indexed token,
\t\tuint256 amount
\t);
}
"
    
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IRewardsManager {
\t/* Types */

\tstruct ShareData {
\t\taddress account;
\t\tuint128 value;
\t\tbool isActive;
\t}

\t/* Views */

\tfunction defaultRecipient() external view returns (address);

\tfunction rewardsToken() external view returns (address);

\tfunction sharesFor(address account)
\t\texternal
\t\tview
\t\treturns (uint128 active, uint128 total);

\tfunction totalRewardsAccrued() external view returns (uint256);

\tfunction totalRewardsRedeemed() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction activateShares() external;

\tfunction activateSharesFor(address account) external;

\tfunction addShares(address account, uint128 amount) external;

\tfunction deactivateShares() external;

\tfunction deactivateSharesFor(address account) external;

\tfunction recoverUnsupportedERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction removeShares(address account, uint128 amount) external;

\tfunction setDefaultRecipient(address account) external;

\tfunction setRewardsToken(address token) external;

\tfunction setShares(
\t\taddress account,
\t\tuint128 value,
\t\tbool isActive
\t) external;

\tfunction setSharesBatch(ShareData[] memory batch) external;

\t/* Events */

\tevent DefaultRecipientSet(address indexed author, address indexed account);
\tevent RecoveredUnsupported(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
\tevent RewardsTokenSet(address indexed author, address indexed token);
\tevent SharesActivated(address indexed author, address indexed account);
\tevent SharesAdded(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesDeactivated(address indexed author, address indexed account);
\tevent SharesRemoved(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesSet(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 value,
\t\tbool isActive
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IRewardsManager {
\t/* Types */

\tstruct ShareData {
\t\taddress account;
\t\tuint128 value;
\t\tbool isActive;
\t}

\t/* Views */

\tfunction defaultRecipient() external view returns (address);

\tfunction rewardsToken() external view returns (address);

\tfunction sharesFor(address account)
\t\texternal
\t\tview
\t\treturns (uint128 active, uint128 total);

\tfunction totalRewardsAccrued() external view returns (uint256);

\tfunction totalRewardsRedeemed() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction activateShares() external;

\tfunction activateSharesFor(address account) external;

\tfunction addShares(address account, uint128 amount) external;

\tfunction deactivateShares() external;

\tfunction deactivateSharesFor(address account) external;

\tfunction recoverUnsupportedERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction removeShares(address account, uint128 amount) external;

\tfunction setDefaultRecipient(address account) external;

\tfunction setRewardsToken(address token) external;

\tfunction setShares(
\t\taddress account,
\t\tuint128 value,
\t\tbool isActive
\t) external;

\tfunction setSharesBatch(ShareData[] memory batch) external;

\t/* Events */

\tevent DefaultRecipientSet(address indexed author, address indexed account);
\tevent RecoveredUnsupported(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
\tevent RewardsTokenSet(address indexed author, address indexed token);
\tevent SharesActivated(address indexed author, address indexed account);
\tevent SharesAdded(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesDeactivated(address indexed author, address indexed account);
\tevent SharesRemoved(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesSet(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 value,
\t\tbool isActive
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IRewardsManager {
\t/* Types */

\tstruct ShareData {
\t\taddress account;
\t\tuint128 value;
\t\tbool isActive;
\t}

\t/* Views */

\tfunction defaultRecipient() external view returns (address);

\tfunction rewardsToken() external view returns (address);

\tfunction sharesFor(address account)
\t\texternal
\t\tview
\t\treturns (uint128 active, uint128 total);

\tfunction totalRewardsAccrued() external view returns (uint256);

\tfunction totalRewardsRedeemed() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction activateShares() external;

\tfunction activateSharesFor(address account) external;

\tfunction addShares(address account, uint128 amount) external;

\tfunction deactivateShares() external;

\tfunction deactivateSharesFor(address account) external;

\tfunction recoverUnsupportedERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction removeShares(address account, uint128 amount) external;

\tfunction setDefaultRecipient(address account) external;

\tfunction setRewardsToken(address token) external;

\tfunction setShares(
\t\taddress account,
\t\tuint128 value,
\t\tbool isActive
\t) external;

\tfunction setSharesBatch(ShareData[] memory batch) external;

\t/* Events */

\tevent DefaultRecipientSet(address indexed author, address indexed account);
\tevent RecoveredUnsupported(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
\tevent RewardsTokenSet(address indexed author, address indexed token);
\tevent SharesActivated(address indexed author, address indexed account);
\tevent SharesAdded(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesDeactivated(address indexed author, address indexed account);
\tevent SharesRemoved(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesSet(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 value,
\t\tbool isActive
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IRewardsManager {
\t/* Types */

\tstruct ShareData {
\t\taddress account;
\t\tuint128 value;
\t\tbool isActive;
\t}

\t/* Views */

\tfunction defaultRecipient() external view returns (address);

\tfunction rewardsToken() external view returns (address);

\tfunction sharesFor(address account)
\t\texternal
\t\tview
\t\treturns (uint128 active, uint128 total);

\tfunction totalRewardsAccrued() external view returns (uint256);

\tfunction totalRewardsRedeemed() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction activateShares() external;

\tfunction activateSharesFor(address account) external;

\tfunction addShares(address account, uint128 amount) external;

\tfunction deactivateShares() external;

\tfunction deactivateSharesFor(address account) external;

\tfunction recoverUnsupportedERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction removeShares(address account, uint128 amount) external;

\tfunction setDefaultRecipient(address account) external;

\tfunction setRewardsToken(address token) external;

\tfunction setShares(
\t\taddress account,
\t\tuint128 value,
\t\tbool isActive
\t) external;

\tfunction setSharesBatch(ShareData[] memory batch) external;

\t/* Events */

\tevent DefaultRecipientSet(address indexed author, address indexed account);
\tevent RecoveredUnsupported(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
\tevent RewardsTokenSet(address indexed author, address indexed token);
\tevent SharesActivated(address indexed author, address indexed account);
\tevent SharesAdded(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesDeactivated(address indexed author, address indexed account);
\tevent SharesRemoved(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesSet(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 value,
\t\tbool isActive
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IRewardsManager {
\t/* Types */

\tstruct ShareData {
\t\taddress account;
\t\tuint128 value;
\t\tbool isActive;
\t}

\t/* Views */

\tfunction defaultRecipient() external view returns (address);

\tfunction rewardsToken() external view returns (address);

\tfunction sharesFor(address account)
\t\texternal
\t\tview
\t\treturns (uint128 active, uint128 total);

\tfunction totalRewardsAccrued() external view returns (uint256);

\tfunction totalRewardsRedeemed() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction activateShares() external;

\tfunction activateSharesFor(address account) external;

\tfunction addShares(address account, uint128 amount) external;

\tfunction deactivateShares() external;

\tfunction deactivateSharesFor(address account) external;

\tfunction recoverUnsupportedERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction removeShares(address account, uint128 amount) external;

\tfunction setDefaultRecipient(address account) external;

\tfunction setRewardsToken(address token) external;

\tfunction setShares(
\t\taddress account,
\t\tuint128 value,
\t\tbool isActive
\t) external;

\tfunction setSharesBatch(ShareData[] memory batch) external;

\t/* Events */

\tevent DefaultRecipientSet(address indexed author, address indexed account);
\tevent RecoveredUnsupported(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
\tevent RewardsTokenSet(address indexed author, address indexed token);
\tevent SharesActivated(address indexed author, address indexed account);
\tevent SharesAdded(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesDeactivated(address indexed author, address indexed account);
\tevent SharesRemoved(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesSet(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 value,
\t\tbool isActive
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IRewardsManager {
\t/* Types */

\tstruct ShareData {
\t\taddress account;
\t\tuint128 value;
\t\tbool isActive;
\t}

\t/* Views */

\tfunction defaultRecipient() external view returns (address);

\tfunction rewardsToken() external view returns (address);

\tfunction sharesFor(address account)
\t\texternal
\t\tview
\t\treturns (uint128 active, uint128 total);

\tfunction totalRewardsAccrued() external view returns (uint256);

\tfunction totalRewardsRedeemed() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction activateShares() external;

\tfunction activateSharesFor(address account) external;

\tfunction addShares(address account, uint128 amount) external;

\tfunction deactivateShares() external;

\tfunction deactivateSharesFor(address account) external;

\tfunction recoverUnsupportedERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction removeShares(address account, uint128 amount) external;

\tfunction setDefaultRecipient(address account) external;

\tfunction setRewardsToken(address token) external;

\tfunction setShares(
\t\taddress account,
\t\tuint128 value,
\t\tbool isActive
\t) external;

\tfunction setSharesBatch(ShareData[] memory batch) external;

\t/* Events */

\tevent DefaultRecipientSet(address indexed author, address indexed account);
\tevent RecoveredUnsupported(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
\tevent RewardsTokenSet(address indexed author, address indexed token);
\tevent SharesActivated(address indexed author, address indexed account);
\tevent SharesAdded(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesDeactivated(address indexed author, address indexed account);
\tevent SharesRemoved(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesSet(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 value,
\t\tbool isActive
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IRewardsManager {
\t/* Types */

\tstruct ShareData {
\t\taddress account;
\t\tuint128 value;
\t\tbool isActive;
\t}

\t/* Views */

\tfunction defaultRecipient() external view returns (address);

\tfunction rewardsToken() external view returns (address);

\tfunction sharesFor(address account)
\t\texternal
\t\tview
\t\treturns (uint128 active, uint128 total);

\tfunction totalRewardsAccrued() external view returns (uint256);

\tfunction totalRewardsRedeemed() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction activateShares() external;

\tfunction activateSharesFor(address account) external;

\tfunction addShares(address account, uint128 amount) external;

\tfunction deactivateShares() external;

\tfunction deactivateSharesFor(address account) external;

\tfunction recoverUnsupportedERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction removeShares(address account, uint128 amount) external;

\tfunction setDefaultRecipient(address account) external;

\tfunction setRewardsToken(address token) external;

\tfunction setShares(
\t\taddress account,
\t\tuint128 value,
\t\tbool isActive
\t) external;

\tfunction setSharesBatch(ShareData[] memory batch) external;

\t/* Events */

\tevent DefaultRecipientSet(address indexed author, address indexed account);
\tevent RecoveredUnsupported(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
\tevent RewardsTokenSet(address indexed author, address indexed token);
\tevent SharesActivated(address indexed author, address indexed account);
\tevent SharesAdded(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesDeactivated(address indexed author, address indexed account);
\tevent SharesRemoved(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesSet(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 value,
\t\tbool isActive
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IRewardsManager {
\t/* Types */

\tstruct ShareData {
\t\taddress account;
\t\tuint128 value;
\t\tbool isActive;
\t}

\t/* Views */

\tfunction defaultRecipient() external view returns (address);

\tfunction rewardsToken() external view returns (address);

\tfunction sharesFor(address account)
\t\texternal
\t\tview
\t\treturns (uint128 active, uint128 total);

\tfunction totalRewardsAccrued() external view returns (uint256);

\tfunction totalRewardsRedeemed() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction activateShares() external;

\tfunction activateSharesFor(address account) external;

\tfunction addShares(address account, uint128 amount) external;

\tfunction deactivateShares() external;

\tfunction deactivateSharesFor(address account) external;

\tfunction recoverUnsupportedERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction removeShares(address account, uint128 amount) external;

\tfunction setDefaultRecipient(address account) external;

\tfunction setRewardsToken(address token) external;

\tfunction setShares(
\t\taddress account,
\t\tuint128 value,
\t\tbool isActive
\t) external;

\tfunction setSharesBatch(ShareData[] memory batch) external;

\t/* Events */

\tevent DefaultRecipientSet(address indexed author, address indexed account);
\tevent RecoveredUnsupported(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
\tevent RewardsTokenSet(address indexed author, address indexed token);
\tevent SharesActivated(address indexed author, address indexed account);
\tevent SharesAdded(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesDeactivated(address indexed author, address indexed account);
\tevent SharesRemoved(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesSet(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 value,
\t\tbool isActive
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IRewardsManager {
\t/* Types */

\tstruct ShareData {
\t\taddress account;
\t\tuint128 value;
\t\tbool isActive;
\t}

\t/* Views */

\tfunction defaultRecipient() external view returns (address);

\tfunction rewardsToken() external view returns (address);

\tfunction sharesFor(address account)
\t\texternal
\t\tview
\t\treturns (uint128 active, uint128 total);

\tfunction totalRewardsAccrued() external view returns (uint256);

\tfunction totalRewardsRedeemed() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction activateShares() external;

\tfunction activateSharesFor(address account) external;

\tfunction addShares(address account, uint128 amount) external;

\tfunction deactivateShares() external;

\tfunction deactivateSharesFor(address account) external;

\tfunction recoverUnsupportedERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction removeShares(address account, uint128 amount) external;

\tfunction setDefaultRecipient(address account) external;

\tfunction setRewardsToken(address token) external;

\tfunction setShares(
\t\taddress account,
\t\tuint128 value,
\t\tbool isActive
\t) external;

\tfunction setSharesBatch(ShareData[] memory batch) external;

\t/* Events */

\tevent DefaultRecipientSet(address indexed author, address indexed account);
\tevent RecoveredUnsupported(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
\tevent RewardsTokenSet(address indexed author, address indexed token);
\tevent SharesActivated(address indexed author, address indexed account);
\tevent SharesAdded(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesDeactivated(address indexed author, address indexed account);
\tevent SharesRemoved(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesSet(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 value,
\t\tbool isActive
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IRewardsManager {
\t/* Types */

\tstruct ShareData {
\t\taddress account;
\t\tuint128 value;
\t\tbool isActive;
\t}

\t/* Views */

\tfunction defaultRecipient() external view returns (address);

\tfunction rewardsToken() external view returns (address);

\tfunction sharesFor(address account)
\t\texternal
\t\tview
\t\treturns (uint128 active, uint128 total);

\tfunction totalRewardsAccrued() external view returns (uint256);

\tfunction totalRewardsRedeemed() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction activateShares() external;

\tfunction activateSharesFor(address account) external;

\tfunction addShares(address account, uint128 amount) external;

\tfunction deactivateShares() external;

\tfunction deactivateSharesFor(address account) external;

\tfunction recoverUnsupportedERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction removeShares(address account, uint128 amount) external;

\tfunction setDefaultRecipient(address account) external;

\tfunction setRewardsToken(address token) external;

\tfunction setShares(
\t\taddress account,
\t\tuint128 value,
\t\tbool isActive
\t) external;

\tfunction setSharesBatch(ShareData[] memory batch) external;

\t/* Events */

\tevent DefaultRecipientSet(address indexed author, address indexed account);
\tevent RecoveredUnsupported(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
\tevent RewardsTokenSet(address indexed author, address indexed token);
\tevent SharesActivated(address indexed author, address indexed account);
\tevent SharesAdded(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesDeactivated(address indexed author, address indexed account);
\tevent SharesRemoved(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesSet(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 value,
\t\tbool isActive
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IRewardsManager {
\t/* Types */

\tstruct ShareData {
\t\taddress account;
\t\tuint128 value;
\t\tbool isActive;
\t}

\t/* Views */

\tfunction defaultRecipient() external view returns (address);

\tfunction rewardsToken() external view returns (address);

\tfunction sharesFor(address account)
\t\texternal
\t\tview
\t\treturns (uint128 active, uint128 total);

\tfunction totalRewardsAccrued() external view returns (uint256);

\tfunction totalRewardsRedeemed() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction activateShares() external;

\tfunction activateSharesFor(address account) external;

\tfunction addShares(address account, uint128 amount) external;

\tfunction deactivateShares() external;

\tfunction deactivateSharesFor(address account) external;

\tfunction recoverUnsupportedERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction removeShares(address account, uint128 amount) external;

\tfunction setDefaultRecipient(address account) external;

\tfunction setRewardsToken(address token) external;

\tfunction setShares(
\t\taddress account,
\t\tuint128 value,
\t\tbool isActive
\t) external;

\tfunction setSharesBatch(ShareData[] memory batch) external;

\t/* Events */

\tevent DefaultRecipientSet(address indexed author, address indexed account);
\tevent RecoveredUnsupported(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
\tevent RewardsTokenSet(address indexed author, address indexed token);
\tevent SharesActivated(address indexed author, address indexed account);
\tevent SharesAdded(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesDeactivated(address indexed author, address indexed account);
\tevent SharesRemoved(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesSet(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 value,
\t\tbool isActive
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IRewardsManager {
\t/* Types */

\tstruct ShareData {
\t\taddress account;
\t\tuint128 value;
\t\tbool isActive;
\t}

\t/* Views */

\tfunction defaultRecipient() external view returns (address);

\tfunction rewardsToken() external view returns (address);

\tfunction sharesFor(address account)
\t\texternal
\t\tview
\t\treturns (uint128 active, uint128 total);

\tfunction totalRewardsAccrued() external view returns (uint256);

\tfunction totalRewardsRedeemed() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction activateShares() external;

\tfunction activateSharesFor(address account) external;

\tfunction addShares(address account, uint128 amount) external;

\tfunction deactivateShares() external;

\tfunction deactivateSharesFor(address account) external;

\tfunction recoverUnsupportedERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction removeShares(address account, uint128 amount) external;

\tfunction setDefaultRecipient(address account) external;

\tfunction setRewardsToken(address token) external;

\tfunction setShares(
\t\taddress account,
\t\tuint128 value,
\t\tbool isActive
\t) external;

\tfunction setSharesBatch(ShareData[] memory batch) external;

\t/* Events */

\tevent DefaultRecipientSet(address indexed author, address indexed account);
\tevent RecoveredUnsupported(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
\tevent RewardsTokenSet(address indexed author, address indexed token);
\tevent SharesActivated(address indexed author, address indexed account);
\tevent SharesAdded(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesDeactivated(address indexed author, address indexed account);
\tevent SharesRemoved(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesSet(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 value,
\t\tbool isActive
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IRewardsManager {
\t/* Types */

\tstruct ShareData {
\t\taddress account;
\t\tuint128 value;
\t\tbool isActive;
\t}

\t/* Views */

\tfunction defaultRecipient() external view returns (address);

\tfunction rewardsToken() external view returns (address);

\tfunction sharesFor(address account)
\t\texternal
\t\tview
\t\treturns (uint128 active, uint128 total);

\tfunction totalRewardsAccrued() external view returns (uint256);

\tfunction totalRewardsRedeemed() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction activateShares() external;

\tfunction activateSharesFor(address account) external;

\tfunction addShares(address account, uint128 amount) external;

\tfunction deactivateShares() external;

\tfunction deactivateSharesFor(address account) external;

\tfunction recoverUnsupportedERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction removeShares(address account, uint128 amount) external;

\tfunction setDefaultRecipient(address account) external;

\tfunction setRewardsToken(address token) external;

\tfunction setShares(
\t\taddress account,
\t\tuint128 value,
\t\tbool isActive
\t) external;

\tfunction setSharesBatch(ShareData[] memory batch) external;

\t/* Events */

\tevent DefaultRecipientSet(address indexed author, address indexed account);
\tevent RecoveredUnsupported(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
\tevent RewardsTokenSet(address indexed author, address indexed token);
\tevent SharesActivated(address indexed author, address indexed account);
\tevent SharesAdded(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesDeactivated(address indexed author, address indexed account);
\tevent SharesRemoved(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesSet(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 value,
\t\tbool isActive
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IRewardsManager {
\t/* Types */

\tstruct ShareData {
\t\taddress account;
\t\tuint128 value;
\t\tbool isActive;
\t}

\t/* Views */

\tfunction defaultRecipient() external view returns (address);

\tfunction rewardsToken() external view returns (address);

\tfunction sharesFor(address account)
\t\texternal
\t\tview
\t\treturns (uint128 active, uint128 total);

\tfunction totalRewardsAccrued() external view returns (uint256);

\tfunction totalRewardsRedeemed() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction activateShares() external;

\tfunction activateSharesFor(address account) external;

\tfunction addShares(address account, uint128 amount) external;

\tfunction deactivateShares() external;

\tfunction deactivateSharesFor(address account) external;

\tfunction recoverUnsupportedERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction removeShares(address account, uint128 amount) external;

\tfunction setDefaultRecipient(address account) external;

\tfunction setRewardsToken(address token) external;

\tfunction setShares(
\t\taddress account,
\t\tuint128 value,
\t\tbool isActive
\t) external;

\tfunction setSharesBatch(ShareData[] memory batch) external;

\t/* Events */

\tevent DefaultRecipientSet(address indexed author, address indexed account);
\tevent RecoveredUnsupported(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
\tevent RewardsTokenSet(address indexed author, address indexed token);
\tevent SharesActivated(address indexed author, address indexed account);
\tevent SharesAdded(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesDeactivated(address indexed author, address indexed account);
\tevent SharesRemoved(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesSet(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 value,
\t\tbool isActive
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IRewardsManager {
\t/* Types */

\tstruct ShareData {
\t\taddress account;
\t\tuint128 value;
\t\tbool isActive;
\t}

\t/* Views */

\tfunction defaultRecipient() external view returns (address);

\tfunction rewardsToken() external view returns (address);

\tfunction sharesFor(address account)
\t\texternal
\t\tview
\t\treturns (uint128 active, uint128 total);

\tfunction totalRewardsAccrued() external view returns (uint256);

\tfunction totalRewardsRedeemed() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction activateShares() external;

\tfunction activateSharesFor(address account) external;

\tfunction addShares(address account, uint128 amount) external;

\tfunction deactivateShares() external;

\tfunction deactivateSharesFor(address account) external;

\tfunction recoverUnsupportedERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction removeShares(address account, uint128 amount) external;

\tfunction setDefaultRecipient(address account) external;

\tfunction setRewardsToken(address token) external;

\tfunction setShares(
\t\taddress account,
\t\tuint128 value,
\t\tbool isActive
\t) external;

\tfunction setSharesBatch(ShareData[] memory batch) external;

\t/* Events */

\tevent DefaultRecipientSet(address indexed author, address indexed account);
\tevent RecoveredUnsupported(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
\tevent RewardsTokenSet(address indexed author, address indexed token);
\tevent SharesActivated(address indexed author, address indexed account);
\tevent SharesAdded(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesDeactivated(address indexed author, address indexed account);
\tevent SharesRemoved(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesSet(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 value,
\t\tbool isActive
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IRewardsManager {
\t/* Types */

\tstruct ShareData {
\t\taddress account;
\t\tuint128 value;
\t\tbool isActive;
\t}

\t/* Views */

\tfunction defaultRecipient() external view returns (address);

\tfunction rewardsToken() external view returns (address);

\tfunction sharesFor(address account)
\t\texternal
\t\tview
\t\treturns (uint128 active, uint128 total);

\tfunction totalRewardsAccrued() external view returns (uint256);

\tfunction totalRewardsRedeemed() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction activateShares() external;

\tfunction activateSharesFor(address account) external;

\tfunction addShares(address account, uint128 amount) external;

\tfunction deactivateShares() external;

\tfunction deactivateSharesFor(address account) external;

\tfunction recoverUnsupportedERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction removeShares(address account, uint128 amount) external;

\tfunction setDefaultRecipient(address account) external;

\tfunction setRewardsToken(address token) external;

\tfunction setShares(
\t\taddress account,
\t\tuint128 value,
\t\tbool isActive
\t) external;

\tfunction setSharesBatch(ShareData[] memory batch) external;

\t/* Events */

\tevent DefaultRecipientSet(address indexed author, address indexed account);
\tevent RecoveredUnsupported(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
\tevent RewardsTokenSet(address indexed author, address indexed token);
\tevent SharesActivated(address indexed author, address indexed account);
\tevent SharesAdded(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesDeactivated(address indexed author, address indexed account);
\tevent SharesRemoved(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesSet(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 value,
\t\tbool isActive
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IRewardsManager {
\t/* Types */

\tstruct ShareData {
\t\taddress account;
\t\tuint128 value;
\t\tbool isActive;
\t}

\t/* Views */

\tfunction defaultRecipient() external view returns (address);

\tfunction rewardsToken() external view returns (address);

\tfunction sharesFor(address account)
\t\texternal
\t\tview
\t\treturns (uint128 active, uint128 total);

\tfunction totalRewardsAccrued() external view returns (uint256);

\tfunction totalRewardsRedeemed() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction activateShares() external;

\tfunction activateSharesFor(address account) external;

\tfunction addShares(address account, uint128 amount) external;

\tfunction deactivateShares() external;

\tfunction deactivateSharesFor(address account) external;

\tfunction recoverUnsupportedERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction removeShares(address account, uint128 amount) external;

\tfunction setDefaultRecipient(address account) external;

\tfunction setRewardsToken(address token) external;

\tfunction setShares(
\t\taddress account,
\t\tuint128 value,
\t\tbool isActive
\t) external;

\tfunction setSharesBatch(ShareData[] memory batch) external;

\t/* Events */

\tevent DefaultRecipientSet(address indexed author, address indexed account);
\tevent RecoveredUnsupported(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
\tevent RewardsTokenSet(address indexed author, address indexed token);
\tevent SharesActivated(address indexed author, address indexed account);
\tevent SharesAdded(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesDeactivated(address indexed author, address indexed account);
\tevent SharesRemoved(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesSet(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 value,
\t\tbool isActive
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IRewardsManager {
\t/* Types */

\tstruct ShareData {
\t\taddress account;
\t\tuint128 value;
\t\tbool isActive;
\t}

\t/* Views */

\tfunction defaultRecipient() external view returns (address);

\tfunction rewardsToken() external view returns (address);

\tfunction sharesFor(address account)
\t\texternal
\t\tview
\t\treturns (uint128 active, uint128 total);

\tfunction totalRewardsAccrued() external view returns (uint256);

\tfunction totalRewardsRedeemed() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction activateShares() external;

\tfunction activateSharesFor(address account) external;

\tfunction addShares(address account, uint128 amount) external;

\tfunction deactivateShares() external;

\tfunction deactivateSharesFor(address account) external;

\tfunction recoverUnsupportedERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction removeShares(address account, uint128 amount) external;

\tfunction setDefaultRecipient(address account) external;

\tfunction setRewardsToken(address token) external;

\tfunction setShares(
\t\taddress account,
\t\tuint128 value,
\t\tbool isActive
\t) external;

\tfunction setSharesBatch(ShareData[] memory batch) external;

\t/* Events */

\tevent DefaultRecipientSet(address indexed author, address indexed account);
\tevent RecoveredUnsupported(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
\tevent RewardsTokenSet(address indexed author, address indexed token);
\tevent SharesActivated(address indexed author, address indexed account);
\tevent SharesAdded(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesDeactivated(address indexed author, address indexed account);
\tevent SharesRemoved(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesSet(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 value,
\t\tbool isActive
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IRewardsManager {
\t/* Types */

\tstruct ShareData {
\t\taddress account;
\t\tuint128 value;
\t\tbool isActive;
\t}

\t/* Views */

\tfunction defaultRecipient() external view returns (address);

\tfunction rewardsToken() external view returns (address);

\tfunction sharesFor(address account)
\t\texternal
\t\tview
\t\treturns (uint128 active, uint128 total);

\tfunction totalRewardsAccrued() external view returns (uint256);

\tfunction totalRewardsRedeemed() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction activateShares() external;

\tfunction activateSharesFor(address account) external;

\tfunction addShares(address account, uint128 amount) external;

\tfunction deactivateShares() external;

\tfunction deactivateSharesFor(address account) external;

\tfunction recoverUnsupportedERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction removeShares(address account, uint128 amount) external;

\tfunction setDefaultRecipient(address account) external;

\tfunction setRewardsToken(address token) external;

\tfunction setShares(
\t\taddress account,
\t\tuint128 value,
\t\tbool isActive
\t) external;

\tfunction setSharesBatch(ShareData[] memory batch) external;

\t/* Events */

\tevent DefaultRecipientSet(address indexed author, address indexed account);
\tevent RecoveredUnsupported(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
\tevent RewardsTokenSet(address indexed author, address indexed token);
\tevent SharesActivated(address indexed author, address indexed account);
\tevent SharesAdded(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesDeactivated(address indexed author, address indexed account);
\tevent SharesRemoved(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesSet(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 value,
\t\tbool isActive
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IRewardsManager {
\t/* Types */

\tstruct ShareData {
\t\taddress account;
\t\tuint128 value;
\t\tbool isActive;
\t}

\t/* Views */

\tfunction defaultRecipient() external view returns (address);

\tfunction rewardsToken() external view returns (address);

\tfunction sharesFor(address account)
\t\texternal
\t\tview
\t\treturns (uint128 active, uint128 total);

\tfunction totalRewardsAccrued() external view returns (uint256);

\tfunction totalRewardsRedeemed() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction activateShares() external;

\tfunction activateSharesFor(address account) external;

\tfunction addShares(address account, uint128 amount) external;

\tfunction deactivateShares() external;

\tfunction deactivateSharesFor(address account) external;

\tfunction recoverUnsupportedERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction removeShares(address account, uint128 amount) external;

\tfunction setDefaultRecipient(address account) external;

\tfunction setRewardsToken(address token) external;

\tfunction setShares(
\t\taddress account,
\t\tuint128 value,
\t\tbool isActive
\t) external;

\tfunction setSharesBatch(ShareData[] memory batch) external;

\t/* Events */

\tevent DefaultRecipientSet(address indexed author, address indexed account);
\tevent RecoveredUnsupported(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
\tevent RewardsTokenSet(address indexed author, address indexed token);
\tevent SharesActivated(address indexed author, address indexed account);
\tevent SharesAdded(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesDeactivated(address indexed author, address indexed account);
\tevent SharesRemoved(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesSet(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 value,
\t\tbool isActive
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IRewardsManager {
\t/* Types */

\tstruct ShareData {
\t\taddress account;
\t\tuint128 value;
\t\tbool isActive;
\t}

\t/* Views */

\tfunction defaultRecipient() external view returns (address);

\tfunction rewardsToken() external view returns (address);

\tfunction sharesFor(address account)
\t\texternal
\t\tview
\t\treturns (uint128 active, uint128 total);

\tfunction totalRewardsAccrued() external view returns (uint256);

\tfunction totalRewardsRedeemed() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction activateShares() external;

\tfunction activateSharesFor(address account) external;

\tfunction addShares(address account, uint128 amount) external;

\tfunction deactivateShares() external;

\tfunction deactivateSharesFor(address account) external;

\tfunction recoverUnsupportedERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction removeShares(address account, uint128 amount) external;

\tfunction setDefaultRecipient(address account) external;

\tfunction setRewardsToken(address token) external;

\tfunction setShares(
\t\taddress account,
\t\tuint128 value,
\t\tbool isActive
\t) external;

\tfunction setSharesBatch(ShareData[] memory batch) external;

\t/* Events */

\tevent DefaultRecipientSet(address indexed author, address indexed account);
\tevent RecoveredUnsupported(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
\tevent RewardsTokenSet(address indexed author, address indexed token);
\tevent SharesActivated(address indexed author, address indexed account);
\tevent SharesAdded(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesDeactivated(address indexed author, address indexed account);
\tevent SharesRemoved(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesSet(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 value,
\t\tbool isActive
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IRewardsManager {
\t/* Types */

\tstruct ShareData {
\t\taddress account;
\t\tuint128 value;
\t\tbool isActive;
\t}

\t/* Views */

\tfunction defaultRecipient() external view returns (address);

\tfunction rewardsToken() external view returns (address);

\tfunction sharesFor(address account)
\t\texternal
\t\tview
\t\treturns (uint128 active, uint128 total);

\tfunction totalRewardsAccrued() external view returns (uint256);

\tfunction totalRewardsRedeemed() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction activateShares() external;

\tfunction activateSharesFor(address account) external;

\tfunction addShares(address account, uint128 amount) external;

\tfunction deactivateShares() external;

\tfunction deactivateSharesFor(address account) external;

\tfunction recoverUnsupportedERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction removeShares(address account, uint128 amount) external;

\tfunction setDefaultRecipient(address account) external;

\tfunction setRewardsToken(address token) external;

\tfunction setShares(
\t\taddress account,
\t\tuint128 value,
\t\tbool isActive
\t) external;

\tfunction setSharesBatch(ShareData[] memory batch) external;

\t/* Events */

\tevent DefaultRecipientSet(address indexed author, address indexed account);
\tevent RecoveredUnsupported(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
\tevent RewardsTokenSet(address indexed author, address indexed token);
\tevent SharesActivated(address indexed author, address indexed account);
\tevent SharesAdded(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesDeactivated(address indexed author, address indexed account);
\tevent SharesRemoved(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesSet(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 value,
\t\tbool isActive
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IRewardsManager {
\t/* Types */

\tstruct ShareData {
\t\taddress account;
\t\tuint128 value;
\t\tbool isActive;
\t}

\t/* Views */

\tfunction defaultRecipient() external view returns (address);

\tfunction rewardsToken() external view returns (address);

\tfunction sharesFor(address account)
\t\texternal
\t\tview
\t\treturns (uint128 active, uint128 total);

\tfunction totalRewardsAccrued() external view returns (uint256);

\tfunction totalRewardsRedeemed() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction activateShares() external;

\tfunction activateSharesFor(address account) external;

\tfunction addShares(address account, uint128 amount) external;

\tfunction deactivateShares() external;

\tfunction deactivateSharesFor(address account) external;

\tfunction recoverUnsupportedERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction removeShares(address account, uint128 amount) external;

\tfunction setDefaultRecipient(address account) external;

\tfunction setRewardsToken(address token) external;

\tfunction setShares(
\t\taddress account,
\t\tuint128 value,
\t\tbool isActive
\t) external;

\tfunction setSharesBatch(ShareData[] memory batch) external;

\t/* Events */

\tevent DefaultRecipientSet(address indexed author, address indexed account);
\tevent RecoveredUnsupported(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
\tevent RewardsTokenSet(address indexed author, address indexed token);
\tevent SharesActivated(address indexed author, address indexed account);
\tevent SharesAdded(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesDeactivated(address indexed author, address indexed account);
\tevent SharesRemoved(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesSet(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 value,
\t\tbool isActive
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IRewardsManager {
\t/* Types */

\tstruct ShareData {
\t\taddress account;
\t\tuint128 value;
\t\tbool isActive;
\t}

\t/* Views */

\tfunction defaultRecipient() external view returns (address);

\tfunction rewardsToken() external view returns (address);

\tfunction sharesFor(address account)
\t\texternal
\t\tview
\t\treturns (uint128 active, uint128 total);

\tfunction totalRewardsAccrued() external view returns (uint256);

\tfunction totalRewardsRedeemed() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction activateShares() external;

\tfunction activateSharesFor(address account) external;

\tfunction addShares(address account, uint128 amount) external;

\tfunction deactivateShares() external;

\tfunction deactivateSharesFor(address account) external;

\tfunction recoverUnsupportedERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction removeShares(address account, uint128 amount) external;

\tfunction setDefaultRecipient(address account) external;

\tfunction setRewardsToken(address token) external;

\tfunction setShares(
\t\taddress account,
\t\tuint128 value,
\t\tbool isActive
\t) external;

\tfunction setSharesBatch(ShareData[] memory batch) external;

\t/* Events */

\tevent DefaultRecipientSet(address indexed author, address indexed account);
\tevent RecoveredUnsupported(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
\tevent RewardsTokenSet(address indexed author, address indexed token);
\tevent SharesActivated(address indexed author, address indexed account);
\tevent SharesAdded(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesDeactivated(address indexed author, address indexed account);
\tevent SharesRemoved(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesSet(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 value,
\t\tbool isActive
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IRewardsManager {
\t/* Types */

\tstruct ShareData {
\t\taddress account;
\t\tuint128 value;
\t\tbool isActive;
\t}

\t/* Views */

\tfunction defaultRecipient() external view returns (address);

\tfunction rewardsToken() external view returns (address);

\tfunction sharesFor(address account)
\t\texternal
\t\tview
\t\treturns (uint128 active, uint128 total);

\tfunction totalRewardsAccrued() external view returns (uint256);

\tfunction totalRewardsRedeemed() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction activateShares() external;

\tfunction activateSharesFor(address account) external;

\tfunction addShares(address account, uint128 amount) external;

\tfunction deactivateShares() external;

\tfunction deactivateSharesFor(address account) external;

\tfunction recoverUnsupportedERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction removeShares(address account, uint128 amount) external;

\tfunction setDefaultRecipient(address account) external;

\tfunction setRewardsToken(address token) external;

\tfunction setShares(
\t\taddress account,
\t\tuint128 value,
\t\tbool isActive
\t) external;

\tfunction setSharesBatch(ShareData[] memory batch) external;

\t/* Events */

\tevent DefaultRecipientSet(address indexed author, address indexed account);
\tevent RecoveredUnsupported(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
\tevent RewardsTokenSet(address indexed author, address indexed token);
\tevent SharesActivated(address indexed author, address indexed account);
\tevent SharesAdded(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesDeactivated(address indexed author, address indexed account);
\tevent SharesRemoved(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesSet(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 value,
\t\tbool isActive
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IRewardsManager {
\t/* Types */

\tstruct ShareData {
\t\taddress account;
\t\tuint128 value;
\t\tbool isActive;
\t}

\t/* Views */

\tfunction defaultRecipient() external view returns (address);

\tfunction rewardsToken() external view returns (address);

\tfunction sharesFor(address account)
\t\texternal
\t\tview
\t\treturns (uint128 active, uint128 total);

\tfunction totalRewardsAccrued() external view returns (uint256);

\tfunction totalRewardsRedeemed() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction activateShares() external;

\tfunction activateSharesFor(address account) external;

\tfunction addShares(address account, uint128 amount) external;

\tfunction deactivateShares() external;

\tfunction deactivateSharesFor(address account) external;

\tfunction recoverUnsupportedERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction removeShares(address account, uint128 amount) external;

\tfunction setDefaultRecipient(address account) external;

\tfunction setRewardsToken(address token) external;

\tfunction setShares(
\t\taddress account,
\t\tuint128 value,
\t\tbool isActive
\t) external;

\tfunction setSharesBatch(ShareData[] memory batch) external;

\t/* Events */

\tevent DefaultRecipientSet(address indexed author, address indexed account);
\tevent RecoveredUnsupported(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
\tevent RewardsTokenSet(address indexed author, address indexed token);
\tevent SharesActivated(address indexed author, address indexed account);
\tevent SharesAdded(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesDeactivated(address indexed author, address indexed account);
\tevent SharesRemoved(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesSet(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 value,
\t\tbool isActive
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IRewardsManager {
\t/* Types */

\tstruct ShareData {
\t\taddress account;
\t\tuint128 value;
\t\tbool isActive;
\t}

\t/* Views */

\tfunction defaultRecipient() external view returns (address);

\tfunction rewardsToken() external view returns (address);

\tfunction sharesFor(address account)
\t\texternal
\t\tview
\t\treturns (uint128 active, uint128 total);

\tfunction totalRewardsAccrued() external view returns (uint256);

\tfunction totalRewardsRedeemed() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction activateShares() external;

\tfunction activateSharesFor(address account) external;

\tfunction addShares(address account, uint128 amount) external;

\tfunction deactivateShares() external;

\tfunction deactivateSharesFor(address account) external;

\tfunction recoverUnsupportedERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction removeShares(address account, uint128 amount) external;

\tfunction setDefaultRecipient(address account) external;

\tfunction setRewardsToken(address token) external;

\tfunction setShares(
\t\taddress account,
\t\tuint128 value,
\t\tbool isActive
\t) external;

\tfunction setSharesBatch(ShareData[] memory batch) external;

\t/* Events */

\tevent DefaultRecipientSet(address indexed author, address indexed account);
\tevent RecoveredUnsupported(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
\tevent RewardsTokenSet(address indexed author, address indexed token);
\tevent SharesActivated(address indexed author, address indexed account);
\tevent SharesAdded(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesDeactivated(address indexed author, address indexed account);
\tevent SharesRemoved(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesSet(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 value,
\t\tbool isActive
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IRewardsManager {
\t/* Types */

\tstruct ShareData {
\t\taddress account;
\t\tuint128 value;
\t\tbool isActive;
\t}

\t/* Views */

\tfunction defaultRecipient() external view returns (address);

\tfunction rewardsToken() external view returns (address);

\tfunction sharesFor(address account)
\t\texternal
\t\tview
\t\treturns (uint128 active, uint128 total);

\tfunction totalRewardsAccrued() external view returns (uint256);

\tfunction totalRewardsRedeemed() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction activateShares() external;

\tfunction activateSharesFor(address account) external;

\tfunction addShares(address account, uint128 amount) external;

\tfunction deactivateShares() external;

\tfunction deactivateSharesFor(address account) external;

\tfunction recoverUnsupportedERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction removeShares(address account, uint128 amount) external;

\tfunction setDefaultRecipient(address account) external;

\tfunction setRewardsToken(address token) external;

\tfunction setShares(
\t\taddress account,
\t\tuint128 value,
\t\tbool isActive
\t) external;

\tfunction setSharesBatch(ShareData[] memory batch) external;

\t/* Events */

\tevent DefaultRecipientSet(address indexed author, address indexed account);
\tevent RecoveredUnsupported(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
\tevent RewardsTokenSet(address indexed author, address indexed token);
\tevent SharesActivated(address indexed author, address indexed account);
\tevent SharesAdded(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesDeactivated(address indexed author, address indexed account);
\tevent SharesRemoved(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesSet(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 value,
\t\tbool isActive
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IRewardsManager {
\t/* Types */

\tstruct ShareData {
\t\taddress account;
\t\tuint128 value;
\t\tbool isActive;
\t}

\t/* Views */

\tfunction defaultRecipient() external view returns (address);

\tfunction rewardsToken() external view returns (address);

\tfunction sharesFor(address account)
\t\texternal
\t\tview
\t\treturns (uint128 active, uint128 total);

\tfunction totalRewardsAccrued() external view returns (uint256);

\tfunction totalRewardsRedeemed() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction activateShares() external;

\tfunction activateSharesFor(address account) external;

\tfunction addShares(address account, uint128 amount) external;

\tfunction deactivateShares() external;

\tfunction deactivateSharesFor(address account) external;

\tfunction recoverUnsupportedERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction removeShares(address account, uint128 amount) external;

\tfunction setDefaultRecipient(address account) external;

\tfunction setRewardsToken(address token) external;

\tfunction setShares(
\t\taddress account,
\t\tuint128 value,
\t\tbool isActive
\t) external;

\tfunction setSharesBatch(ShareData[] memory batch) external;

\t/* Events */

\tevent DefaultRecipientSet(address indexed author, address indexed account);
\tevent RecoveredUnsupported(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
\tevent RewardsTokenSet(address indexed author, address indexed token);
\tevent SharesActivated(address indexed author, address indexed account);
\tevent SharesAdded(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesDeactivated(address indexed author, address indexed account);
\tevent SharesRemoved(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesSet(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 value,
\t\tbool isActive
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IRewardsManager {
\t/* Types */

\tstruct ShareData {
\t\taddress account;
\t\tuint128 value;
\t\tbool isActive;
\t}

\t/* Views */

\tfunction defaultRecipient() external view returns (address);

\tfunction rewardsToken() external view returns (address);

\tfunction sharesFor(address account)
\t\texternal
\t\tview
\t\treturns (uint128 active, uint128 total);

\tfunction totalRewardsAccrued() external view returns (uint256);

\tfunction totalRewardsRedeemed() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction activateShares() external;

\tfunction activateSharesFor(address account) external;

\tfunction addShares(address account, uint128 amount) external;

\tfunction deactivateShares() external;

\tfunction deactivateSharesFor(address account) external;

\tfunction recoverUnsupportedERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction removeShares(address account, uint128 amount) external;

\tfunction setDefaultRecipient(address account) external;

\tfunction setRewardsToken(address token) external;

\tfunction setShares(
\t\taddress account,
\t\tuint128 value,
\t\tbool isActive
\t) external;

\tfunction setSharesBatch(ShareData[] memory batch) external;

\t/* Events */

\tevent DefaultRecipientSet(address indexed author, address indexed account);
\tevent RecoveredUnsupported(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
\tevent RewardsTokenSet(address indexed author, address indexed token);
\tevent SharesActivated(address indexed author, address indexed account);
\tevent SharesAdded(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesDeactivated(address indexed author, address indexed account);
\tevent SharesRemoved(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesSet(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 value,
\t\tbool isActive
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IRewardsManager {
\t/* Types */

\tstruct ShareData {
\t\taddress account;
\t\tuint128 value;
\t\tbool isActive;
\t}

\t/* Views */

\tfunction defaultRecipient() external view returns (address);

\tfunction rewardsToken() external view returns (address);

\tfunction sharesFor(address account)
\t\texternal
\t\tview
\t\treturns (uint128 active, uint128 total);

\tfunction totalRewardsAccrued() external view returns (uint256);

\tfunction totalRewardsRedeemed() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction activateShares() external;

\tfunction activateSharesFor(address account) external;

\tfunction addShares(address account, uint128 amount) external;

\tfunction deactivateShares() external;

\tfunction deactivateSharesFor(address account) external;

\tfunction recoverUnsupportedERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction removeShares(address account, uint128 amount) external;

\tfunction setDefaultRecipient(address account) external;

\tfunction setRewardsToken(address token) external;

\tfunction setShares(
\t\taddress account,
\t\tuint128 value,
\t\tbool isActive
\t) external;

\tfunction setSharesBatch(ShareData[] memory batch) external;

\t/* Events */

\tevent DefaultRecipientSet(address indexed author, address indexed account);
\tevent RecoveredUnsupported(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
\tevent RewardsTokenSet(address indexed author, address indexed token);
\tevent SharesActivated(address indexed author, address indexed account);
\tevent SharesAdded(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesDeactivated(address indexed author, address indexed account);
\tevent SharesRemoved(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesSet(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 value,
\t\tbool isActive
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IRewardsManager {
\t/* Types */

\tstruct ShareData {
\t\taddress account;
\t\tuint128 value;
\t\tbool isActive;
\t}

\t/* Views */

\tfunction defaultRecipient() external view returns (address);

\tfunction rewardsToken() external view returns (address);

\tfunction sharesFor(address account)
\t\texternal
\t\tview
\t\treturns (uint128 active, uint128 total);

\tfunction totalRewardsAccrued() external view returns (uint256);

\tfunction totalRewardsRedeemed() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction activateShares() external;

\tfunction activateSharesFor(address account) external;

\tfunction addShares(address account, uint128 amount) external;

\tfunction deactivateShares() external;

\tfunction deactivateSharesFor(address account) external;

\tfunction recoverUnsupportedERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction removeShares(address account, uint128 amount) external;

\tfunction setDefaultRecipient(address account) external;

\tfunction setRewardsToken(address token) external;

\tfunction setShares(
\t\taddress account,
\t\tuint128 value,
\t\tbool isActive
\t) external;

\tfunction setSharesBatch(ShareData[] memory batch) external;

\t/* Events */

\tevent DefaultRecipientSet(address indexed author, address indexed account);
\tevent RecoveredUnsupported(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
\tevent RewardsTokenSet(address indexed author, address indexed token);
\tevent SharesActivated(address indexed author, address indexed account);
\tevent SharesAdded(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesDeactivated(address indexed author, address indexed account);
\tevent SharesRemoved(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesSet(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 value,
\t\tbool isActive
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IRewardsManager {
\t/* Types */

\tstruct ShareData {
\t\taddress account;
\t\tuint128 value;
\t\tbool isActive;
\t}

\t/* Views */

\tfunction defaultRecipient() external view returns (address);

\tfunction rewardsToken() external view returns (address);

\tfunction sharesFor(address account)
\t\texternal
\t\tview
\t\treturns (uint128 active, uint128 total);

\tfunction totalRewardsAccrued() external view returns (uint256);

\tfunction totalRewardsRedeemed() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction activateShares() external;

\tfunction activateSharesFor(address account) external;

\tfunction addShares(address account, uint128 amount) external;

\tfunction deactivateShares() external;

\tfunction deactivateSharesFor(address account) external;

\tfunction recoverUnsupportedERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction removeShares(address account, uint128 amount) external;

\tfunction setDefaultRecipient(address account) external;

\tfunction setRewardsToken(address token) external;

\tfunction setShares(
\t\taddress account,
\t\tuint128 value,
\t\tbool isActive
\t) external;

\tfunction setSharesBatch(ShareData[] memory batch) external;

\t/* Events */

\tevent DefaultRecipientSet(address indexed author, address indexed account);
\tevent RecoveredUnsupported(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
\tevent RewardsTokenSet(address indexed author, address indexed token);
\tevent SharesActivated(address indexed author, address indexed account);
\tevent SharesAdded(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesDeactivated(address indexed author, address indexed account);
\tevent SharesRemoved(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesSet(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 value,
\t\tbool isActive
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IRewardsManager {
\t/* Types */

\tstruct ShareData {
\t\taddress account;
\t\tuint128 value;
\t\tbool isActive;
\t}

\t/* Views */

\tfunction defaultRecipient() external view returns (address);

\tfunction rewardsToken() external view returns (address);

\tfunction sharesFor(address account)
\t\texternal
\t\tview
\t\treturns (uint128 active, uint128 total);

\tfunction totalRewardsAccrued() external view returns (uint256);

\tfunction totalRewardsRedeemed() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction activateShares() external;

\tfunction activateSharesFor(address account) external;

\tfunction addShares(address account, uint128 amount) external;

\tfunction deactivateShares() external;

\tfunction deactivateSharesFor(address account) external;

\tfunction recoverUnsupportedERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction removeShares(address account, uint128 amount) external;

\tfunction setDefaultRecipient(address account) external;

\tfunction setRewardsToken(address token) external;

\tfunction setShares(
\t\taddress account,
\t\tuint128 value,
\t\tbool isActive
\t) external;

\tfunction setSharesBatch(ShareData[] memory batch) external;

\t/* Events */

\tevent DefaultRecipientSet(address indexed author, address indexed account);
\tevent RecoveredUnsupported(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
\tevent RewardsTokenSet(address indexed author, address indexed token);
\tevent SharesActivated(address indexed author, address indexed account);
\tevent SharesAdded(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesDeactivated(address indexed author, address indexed account);
\tevent SharesRemoved(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesSet(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 value,
\t\tbool isActive
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IRewardsManager {
\t/* Types */

\tstruct ShareData {
\t\taddress account;
\t\tuint128 value;
\t\tbool isActive;
\t}

\t/* Views */

\tfunction defaultRecipient() external view returns (address);

\tfunction rewardsToken() external view returns (address);

\tfunction sharesFor(address account)
\t\texternal
\t\tview
\t\treturns (uint128 active, uint128 total);

\tfunction totalRewardsAccrued() external view returns (uint256);

\tfunction totalRewardsRedeemed() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction activateShares() external;

\tfunction activateSharesFor(address account) external;

\tfunction addShares(address account, uint128 amount) external;

\tfunction deactivateShares() external;

\tfunction deactivateSharesFor(address account) external;

\tfunction recoverUnsupportedERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction removeShares(address account, uint128 amount) external;

\tfunction setDefaultRecipient(address account) external;

\tfunction setRewardsToken(address token) external;

\tfunction setShares(
\t\taddress account,
\t\tuint128 value,
\t\tbool isActive
\t) external;

\tfunction setSharesBatch(ShareData[] memory batch) external;

\t/* Events */

\tevent DefaultRecipientSet(address indexed author, address indexed account);
\tevent RecoveredUnsupported(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
\tevent RewardsTokenSet(address indexed author, address indexed token);
\tevent SharesActivated(address indexed author, address indexed account);
\tevent SharesAdded(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesDeactivated(address indexed author, address indexed account);
\tevent SharesRemoved(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesSet(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 value,
\t\tbool isActive
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IRewardsManager {
\t/* Types */

\tstruct ShareData {
\t\taddress account;
\t\tuint128 value;
\t\tbool isActive;
\t}

\t/* Views */

\tfunction defaultRecipient() external view returns (address);

\tfunction rewardsToken() external view returns (address);

\tfunction sharesFor(address account)
\t\texternal
\t\tview
\t\treturns (uint128 active, uint128 total);

\tfunction totalRewardsAccrued() external view returns (uint256);

\tfunction totalRewardsRedeemed() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction activateShares() external;

\tfunction activateSharesFor(address account) external;

\tfunction addShares(address account, uint128 amount) external;

\tfunction deactivateShares() external;

\tfunction deactivateSharesFor(address account) external;

\tfunction recoverUnsupportedERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction removeShares(address account, uint128 amount) external;

\tfunction setDefaultRecipient(address account) external;

\tfunction setRewardsToken(address token) external;

\tfunction setShares(
\t\taddress account,
\t\tuint128 value,
\t\tbool isActive
\t) external;

\tfunction setSharesBatch(ShareData[] memory batch) external;

\t/* Events */

\tevent DefaultRecipientSet(address indexed author, address indexed account);
\tevent RecoveredUnsupported(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
\tevent RewardsTokenSet(address indexed author, address indexed token);
\tevent SharesActivated(address indexed author, address indexed account);
\tevent SharesAdded(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesDeactivated(address indexed author, address indexed account);
\tevent SharesRemoved(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesSet(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 value,
\t\tbool isActive
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IRewardsManager {
\t/* Types */

\tstruct ShareData {
\t\taddress account;
\t\tuint128 value;
\t\tbool isActive;
\t}

\t/* Views */

\tfunction defaultRecipient() external view returns (address);

\tfunction rewardsToken() external view returns (address);

\tfunction sharesFor(address account)
\t\texternal
\t\tview
\t\treturns (uint128 active, uint128 total);

\tfunction totalRewardsAccrued() external view returns (uint256);

\tfunction totalRewardsRedeemed() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction activateShares() external;

\tfunction activateSharesFor(address account) external;

\tfunction addShares(address account, uint128 amount) external;

\tfunction deactivateShares() external;

\tfunction deactivateSharesFor(address account) external;

\tfunction recoverUnsupportedERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction removeShares(address account, uint128 amount) external;

\tfunction setDefaultRecipient(address account) external;

\tfunction setRewardsToken(address token) external;

\tfunction setShares(
\t\taddress account,
\t\tuint128 value,
\t\tbool isActive
\t) external;

\tfunction setSharesBatch(ShareData[] memory batch) external;

\t/* Events */

\tevent DefaultRecipientSet(address indexed author, address indexed account);
\tevent RecoveredUnsupported(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
\tevent RewardsTokenSet(address indexed author, address indexed token);
\tevent SharesActivated(address indexed author, address indexed account);
\tevent SharesAdded(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesDeactivated(address indexed author, address indexed account);
\tevent SharesRemoved(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesSet(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 value,
\t\tbool isActive
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IRewardsManager {
\t/* Types */

\tstruct ShareData {
\t\taddress account;
\t\tuint128 value;
\t\tbool isActive;
\t}

\t/* Views */

\tfunction defaultRecipient() external view returns (address);

\tfunction rewardsToken() external view returns (address);

\tfunction sharesFor(address account)
\t\texternal
\t\tview
\t\treturns (uint128 active, uint128 total);

\tfunction totalRewardsAccrued() external view returns (uint256);

\tfunction totalRewardsRedeemed() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction activateShares() external;

\tfunction activateSharesFor(address account) external;

\tfunction addShares(address account, uint128 amount) external;

\tfunction deactivateShares() external;

\tfunction deactivateSharesFor(address account) external;

\tfunction recoverUnsupportedERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction removeShares(address account, uint128 amount) external;

\tfunction setDefaultRecipient(address account) external;

\tfunction setRewardsToken(address token) external;

\tfunction setShares(
\t\taddress account,
\t\tuint128 value,
\t\tbool isActive
\t) external;

\tfunction setSharesBatch(ShareData[] memory batch) external;

\t/* Events */

\tevent DefaultRecipientSet(address indexed author, address indexed account);
\tevent RecoveredUnsupported(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
\tevent RewardsTokenSet(address indexed author, address indexed token);
\tevent SharesActivated(address indexed author, address indexed account);
\tevent SharesAdded(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesDeactivated(address indexed author, address indexed account);
\tevent SharesRemoved(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesSet(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 value,
\t\tbool isActive
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IRewardsManager {
\t/* Types */

\tstruct ShareData {
\t\taddress account;
\t\tuint128 value;
\t\tbool isActive;
\t}

\t/* Views */

\tfunction defaultRecipient() external view returns (address);

\tfunction rewardsToken() external view returns (address);

\tfunction sharesFor(address account)
\t\texternal
\t\tview
\t\treturns (uint128 active, uint128 total);

\tfunction totalRewardsAccrued() external view returns (uint256);

\tfunction totalRewardsRedeemed() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction activateShares() external;

\tfunction activateSharesFor(address account) external;

\tfunction addShares(address account, uint128 amount) external;

\tfunction deactivateShares() external;

\tfunction deactivateSharesFor(address account) external;

\tfunction recoverUnsupportedERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction removeShares(address account, uint128 amount) external;

\tfunction setDefaultRecipient(address account) external;

\tfunction setRewardsToken(address token) external;

\tfunction setShares(
\t\taddress account,
\t\tuint128 value,
\t\tbool isActive
\t) external;

\tfunction setSharesBatch(ShareData[] memory batch) external;

\t/* Events */

\tevent DefaultRecipientSet(address indexed author, address indexed account);
\tevent RecoveredUnsupported(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
\tevent RewardsTokenSet(address indexed author, address indexed token);
\tevent SharesActivated(address indexed author, address indexed account);
\tevent SharesAdded(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesDeactivated(address indexed author, address indexed account);
\tevent SharesRemoved(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesSet(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 value,
\t\tbool isActive
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IRewardsManager {
\t/* Types */

\tstruct ShareData {
\t\taddress account;
\t\tuint128 value;
\t\tbool isActive;
\t}

\t/* Views */

\tfunction defaultRecipient() external view returns (address);

\tfunction rewardsToken() external view returns (address);

\tfunction sharesFor(address account)
\t\texternal
\t\tview
\t\treturns (uint128 active, uint128 total);

\tfunction totalRewardsAccrued() external view returns (uint256);

\tfunction totalRewardsRedeemed() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction activateShares() external;

\tfunction activateSharesFor(address account) external;

\tfunction addShares(address account, uint128 amount) external;

\tfunction deactivateShares() external;

\tfunction deactivateSharesFor(address account) external;

\tfunction recoverUnsupportedERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction removeShares(address account, uint128 amount) external;

\tfunction setDefaultRecipient(address account) external;

\tfunction setRewardsToken(address token) external;

\tfunction setShares(
\t\taddress account,
\t\tuint128 value,
\t\tbool isActive
\t) external;

\tfunction setSharesBatch(ShareData[] memory batch) external;

\t/* Events */

\tevent DefaultRecipientSet(address indexed author, address indexed account);
\tevent RecoveredUnsupported(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
\tevent RewardsTokenSet(address indexed author, address indexed token);
\tevent SharesActivated(address indexed author, address indexed account);
\tevent SharesAdded(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesDeactivated(address indexed author, address indexed account);
\tevent SharesRemoved(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesSet(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 value,
\t\tbool isActive
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IRewardsManager {
\t/* Types */

\tstruct ShareData {
\t\taddress account;
\t\tuint128 value;
\t\tbool isActive;
\t}

\t/* Views */

\tfunction defaultRecipient() external view returns (address);

\tfunction rewardsToken() external view returns (address);

\tfunction sharesFor(address account)
\t\texternal
\t\tview
\t\treturns (uint128 active, uint128 total);

\tfunction totalRewardsAccrued() external view returns (uint256);

\tfunction totalRewardsRedeemed() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction activateShares() external;

\tfunction activateSharesFor(address account) external;

\tfunction addShares(address account, uint128 amount) external;

\tfunction deactivateShares() external;

\tfunction deactivateSharesFor(address account) external;

\tfunction recoverUnsupportedERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction removeShares(address account, uint128 amount) external;

\tfunction setDefaultRecipient(address account) external;

\tfunction setRewardsToken(address token) external;

\tfunction setShares(
\t\taddress account,
\t\tuint128 value,
\t\tbool isActive
\t) external;

\tfunction setSharesBatch(ShareData[] memory batch) external;

\t/* Events */

\tevent DefaultRecipientSet(address indexed author, address indexed account);
\tevent RecoveredUnsupported(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
\tevent RewardsTokenSet(address indexed author, address indexed token);
\tevent SharesActivated(address indexed author, address indexed account);
\tevent SharesAdded(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesDeactivated(address indexed author, address indexed account);
\tevent SharesRemoved(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesSet(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 value,
\t\tbool isActive
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IRewardsManager {
\t/* Types */

\tstruct ShareData {
\t\taddress account;
\t\tuint128 value;
\t\tbool isActive;
\t}

\t/* Views */

\tfunction defaultRecipient() external view returns (address);

\tfunction rewardsToken() external view returns (address);

\tfunction sharesFor(address account)
\t\texternal
\t\tview
\t\treturns (uint128 active, uint128 total);

\tfunction totalRewardsAccrued() external view returns (uint256);

\tfunction totalRewardsRedeemed() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction activateShares() external;

\tfunction activateSharesFor(address account) external;

\tfunction addShares(address account, uint128 amount) external;

\tfunction deactivateShares() external;

\tfunction deactivateSharesFor(address account) external;

\tfunction recoverUnsupportedERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction removeShares(address account, uint128 amount) external;

\tfunction setDefaultRecipient(address account) external;

\tfunction setRewardsToken(address token) external;

\tfunction setShares(
\t\taddress account,
\t\tuint128 value,
\t\tbool isActive
\t) external;

\tfunction setSharesBatch(ShareData[] memory batch) external;

\t/* Events */

\tevent DefaultRecipientSet(address indexed author, address indexed account);
\tevent RecoveredUnsupported(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
\tevent RewardsTokenSet(address indexed author, address indexed token);
\tevent SharesActivated(address indexed author, address indexed account);
\tevent SharesAdded(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesDeactivated(address indexed author, address indexed account);
\tevent SharesRemoved(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesSet(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 value,
\t\tbool isActive
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IRewardsManager {
\t/* Types */

\tstruct ShareData {
\t\taddress account;
\t\tuint128 value;
\t\tbool isActive;
\t}

\t/* Views */

\tfunction defaultRecipient() external view returns (address);

\tfunction rewardsToken() external view returns (address);

\tfunction sharesFor(address account)
\t\texternal
\t\tview
\t\treturns (uint128 active, uint128 total);

\tfunction totalRewardsAccrued() external view returns (uint256);

\tfunction totalRewardsRedeemed() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction activateShares() external;

\tfunction activateSharesFor(address account) external;

\tfunction addShares(address account, uint128 amount) external;

\tfunction deactivateShares() external;

\tfunction deactivateSharesFor(address account) external;

\tfunction recoverUnsupportedERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction removeShares(address account, uint128 amount) external;

\tfunction setDefaultRecipient(address account) external;

\tfunction setRewardsToken(address token) external;

\tfunction setShares(
\t\taddress account,
\t\tuint128 value,
\t\tbool isActive
\t) external;

\tfunction setSharesBatch(ShareData[] memory batch) external;

\t/* Events */

\tevent DefaultRecipientSet(address indexed author, address indexed account);
\tevent RecoveredUnsupported(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
\tevent RewardsTokenSet(address indexed author, address indexed token);
\tevent SharesActivated(address indexed author, address indexed account);
\tevent SharesAdded(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesDeactivated(address indexed author, address indexed account);
\tevent SharesRemoved(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesSet(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 value,
\t\tbool isActive
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IRewardsManager {
\t/* Types */

\tstruct ShareData {
\t\taddress account;
\t\tuint128 value;
\t\tbool isActive;
\t}

\t/* Views */

\tfunction defaultRecipient() external view returns (address);

\tfunction rewardsToken() external view returns (address);

\tfunction sharesFor(address account)
\t\texternal
\t\tview
\t\treturns (uint128 active, uint128 total);

\tfunction totalRewardsAccrued() external view returns (uint256);

\tfunction totalRewardsRedeemed() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction activateShares() external;

\tfunction activateSharesFor(address account) external;

\tfunction addShares(address account, uint128 amount) external;

\tfunction deactivateShares() external;

\tfunction deactivateSharesFor(address account) external;

\tfunction recoverUnsupportedERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction removeShares(address account, uint128 amount) external;

\tfunction setDefaultRecipient(address account) external;

\tfunction setRewardsToken(address token) external;

\tfunction setShares(
\t\taddress account,
\t\tuint128 value,
\t\tbool isActive
\t) external;

\tfunction setSharesBatch(ShareData[] memory batch) external;

\t/* Events */

\tevent DefaultRecipientSet(address indexed author, address indexed account);
\tevent RecoveredUnsupported(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
\tevent RewardsTokenSet(address indexed author, address indexed token);
\tevent SharesActivated(address indexed author, address indexed account);
\tevent SharesAdded(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesDeactivated(address indexed author, address indexed account);
\tevent SharesRemoved(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesSet(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 value,
\t\tbool isActive
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IRewardsManager {
\t/* Types */

\tstruct ShareData {
\t\taddress account;
\t\tuint128 value;
\t\tbool isActive;
\t}

\t/* Views */

\tfunction defaultRecipient() external view returns (address);

\tfunction rewardsToken() external view returns (address);

\tfunction sharesFor(address account)
\t\texternal
\t\tview
\t\treturns (uint128 active, uint128 total);

\tfunction totalRewardsAccrued() external view returns (uint256);

\tfunction totalRewardsRedeemed() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction activateShares() external;

\tfunction activateSharesFor(address account) external;

\tfunction addShares(address account, uint128 amount) external;

\tfunction deactivateShares() external;

\tfunction deactivateSharesFor(address account) external;

\tfunction recoverUnsupportedERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction removeShares(address account, uint128 amount) external;

\tfunction setDefaultRecipient(address account) external;

\tfunction setRewardsToken(address token) external;

\tfunction setShares(
\t\taddress account,
\t\tuint128 value,
\t\tbool isActive
\t) external;

\tfunction setSharesBatch(ShareData[] memory batch) external;

\t/* Events */

\tevent DefaultRecipientSet(address indexed author, address indexed account);
\tevent RecoveredUnsupported(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
\tevent RewardsTokenSet(address indexed author, address indexed token);
\tevent SharesActivated(address indexed author, address indexed account);
\tevent SharesAdded(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesDeactivated(address indexed author, address indexed account);
\tevent SharesRemoved(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesSet(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 value,
\t\tbool isActive
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IRewardsManager {
\t/* Types */

\tstruct ShareData {
\t\taddress account;
\t\tuint128 value;
\t\tbool isActive;
\t}

\t/* Views */

\tfunction defaultRecipient() external view returns (address);

\tfunction rewardsToken() external view returns (address);

\tfunction sharesFor(address account)
\t\texternal
\t\tview
\t\treturns (uint128 active, uint128 total);

\tfunction totalRewardsAccrued() external view returns (uint256);

\tfunction totalRewardsRedeemed() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction activateShares() external;

\tfunction activateSharesFor(address account) external;

\tfunction addShares(address account, uint128 amount) external;

\tfunction deactivateShares() external;

\tfunction deactivateSharesFor(address account) external;

\tfunction recoverUnsupportedERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction removeShares(address account, uint128 amount) external;

\tfunction setDefaultRecipient(address account) external;

\tfunction setRewardsToken(address token) external;

\tfunction setShares(
\t\taddress account,
\t\tuint128 value,
\t\tbool isActive
\t) external;

\tfunction setSharesBatch(ShareData[] memory batch) external;

\t/* Events */

\tevent DefaultRecipientSet(address indexed author, address indexed account);
\tevent RecoveredUnsupported(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
\tevent RewardsTokenSet(address indexed author, address indexed token);
\tevent SharesActivated(address indexed author, address indexed account);
\tevent SharesAdded(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesDeactivated(address indexed author, address indexed account);
\tevent SharesRemoved(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesSet(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 value,
\t\tbool isActive
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IRewardsManager {
\t/* Types */

\tstruct ShareData {
\t\taddress account;
\t\tuint128 value;
\t\tbool isActive;
\t}

\t/* Views */

\tfunction defaultRecipient() external view returns (address);

\tfunction rewardsToken() external view returns (address);

\tfunction sharesFor(address account)
\t\texternal
\t\tview
\t\treturns (uint128 active, uint128 total);

\tfunction totalRewardsAccrued() external view returns (uint256);

\tfunction totalRewardsRedeemed() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction activateShares() external;

\tfunction activateSharesFor(address account) external;

\tfunction addShares(address account, uint128 amount) external;

\tfunction deactivateShares() external;

\tfunction deactivateSharesFor(address account) external;

\tfunction recoverUnsupportedERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction removeShares(address account, uint128 amount) external;

\tfunction setDefaultRecipient(address account) external;

\tfunction setRewardsToken(address token) external;

\tfunction setShares(
\t\taddress account,
\t\tuint128 value,
\t\tbool isActive
\t) external;

\tfunction setSharesBatch(ShareData[] memory batch) external;

\t/* Events */

\tevent DefaultRecipientSet(address indexed author, address indexed account);
\tevent RecoveredUnsupported(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
\tevent RewardsTokenSet(address indexed author, address indexed token);
\tevent SharesActivated(address indexed author, address indexed account);
\tevent SharesAdded(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesDeactivated(address indexed author, address indexed account);
\tevent SharesRemoved(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesSet(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 value,
\t\tbool isActive
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IRewardsManager {
\t/* Types */

\tstruct ShareData {
\t\taddress account;
\t\tuint128 value;
\t\tbool isActive;
\t}

\t/* Views */

\tfunction defaultRecipient() external view returns (address);

\tfunction rewardsToken() external view returns (address);

\tfunction sharesFor(address account)
\t\texternal
\t\tview
\t\treturns (uint128 active, uint128 total);

\tfunction totalRewardsAccrued() external view returns (uint256);

\tfunction totalRewardsRedeemed() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction activateShares() external;

\tfunction activateSharesFor(address account) external;

\tfunction addShares(address account, uint128 amount) external;

\tfunction deactivateShares() external;

\tfunction deactivateSharesFor(address account) external;

\tfunction recoverUnsupportedERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction removeShares(address account, uint128 amount) external;

\tfunction setDefaultRecipient(address account) external;

\tfunction setRewardsToken(address token) external;

\tfunction setShares(
\t\taddress account,
\t\tuint128 value,
\t\tbool isActive
\t) external;

\tfunction setSharesBatch(ShareData[] memory batch) external;

\t/* Events */

\tevent DefaultRecipientSet(address indexed author, address indexed account);
\tevent RecoveredUnsupported(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
\tevent RewardsTokenSet(address indexed author, address indexed token);
\tevent SharesActivated(address indexed author, address indexed account);
\tevent SharesAdded(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesDeactivated(address indexed author, address indexed account);
\tevent SharesRemoved(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesSet(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 value,
\t\tbool isActive
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IRewardsManager {
\t/* Types */

\tstruct ShareData {
\t\taddress account;
\t\tuint128 value;
\t\tbool isActive;
\t}

\t/* Views */

\tfunction defaultRecipient() external view returns (address);

\tfunction rewardsToken() external view returns (address);

\tfunction sharesFor(address account)
\t\texternal
\t\tview
\t\treturns (uint128 active, uint128 total);

\tfunction totalRewardsAccrued() external view returns (uint256);

\tfunction totalRewardsRedeemed() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction activateShares() external;

\tfunction activateSharesFor(address account) external;

\tfunction addShares(address account, uint128 amount) external;

\tfunction deactivateShares() external;

\tfunction deactivateSharesFor(address account) external;

\tfunction recoverUnsupportedERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction removeShares(address account, uint128 amount) external;

\tfunction setDefaultRecipient(address account) external;

\tfunction setRewardsToken(address token) external;

\tfunction setShares(
\t\taddress account,
\t\tuint128 value,
\t\tbool isActive
\t) external;

\tfunction setSharesBatch(ShareData[] memory batch) external;

\t/* Events */

\tevent DefaultRecipientSet(address indexed author, address indexed account);
\tevent RecoveredUnsupported(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
\tevent RewardsTokenSet(address indexed author, address indexed token);
\tevent SharesActivated(address indexed author, address indexed account);
\tevent SharesAdded(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesDeactivated(address indexed author, address indexed account);
\tevent SharesRemoved(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesSet(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 value,
\t\tbool isActive
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IRewardsManager {
\t/* Types */

\tstruct ShareData {
\t\taddress account;
\t\tuint128 value;
\t\tbool isActive;
\t}

\t/* Views */

\tfunction defaultRecipient() external view returns (address);

\tfunction rewardsToken() external view returns (address);

\tfunction sharesFor(address account)
\t\texternal
\t\tview
\t\treturns (uint128 active, uint128 total);

\tfunction totalRewardsAccrued() external view returns (uint256);

\tfunction totalRewardsRedeemed() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction activateShares() external;

\tfunction activateSharesFor(address account) external;

\tfunction addShares(address account, uint128 amount) external;

\tfunction deactivateShares() external;

\tfunction deactivateSharesFor(address account) external;

\tfunction recoverUnsupportedERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction removeShares(address account, uint128 amount) external;

\tfunction setDefaultRecipient(address account) external;

\tfunction setRewardsToken(address token) external;

\tfunction setShares(
\t\taddress account,
\t\tuint128 value,
\t\tbool isActive
\t) external;

\tfunction setSharesBatch(ShareData[] memory batch) external;

\t/* Events */

\tevent DefaultRecipientSet(address indexed author, address indexed account);
\tevent RecoveredUnsupported(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
\tevent RewardsTokenSet(address indexed author, address indexed token);
\tevent SharesActivated(address indexed author, address indexed account);
\tevent SharesAdded(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesDeactivated(address indexed author, address indexed account);
\tevent SharesRemoved(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesSet(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 value,
\t\tbool isActive
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IRewardsManager {
\t/* Types */

\tstruct ShareData {
\t\taddress account;
\t\tuint128 value;
\t\tbool isActive;
\t}

\t/* Views */

\tfunction defaultRecipient() external view returns (address);

\tfunction rewardsToken() external view returns (address);

\tfunction sharesFor(address account)
\t\texternal
\t\tview
\t\treturns (uint128 active, uint128 total);

\tfunction totalRewardsAccrued() external view returns (uint256);

\tfunction totalRewardsRedeemed() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction activateShares() external;

\tfunction activateSharesFor(address account) external;

\tfunction addShares(address account, uint128 amount) external;

\tfunction deactivateShares() external;

\tfunction deactivateSharesFor(address account) external;

\tfunction recoverUnsupportedERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction removeShares(address account, uint128 amount) external;

\tfunction setDefaultRecipient(address account) external;

\tfunction setRewardsToken(address token) external;

\tfunction setShares(
\t\taddress account,
\t\tuint128 value,
\t\tbool isActive
\t) external;

\tfunction setSharesBatch(ShareData[] memory batch) external;

\t/* Events */

\tevent DefaultRecipientSet(address indexed author, address indexed account);
\tevent RecoveredUnsupported(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
\tevent RewardsTokenSet(address indexed author, address indexed token);
\tevent SharesActivated(address indexed author, address indexed account);
\tevent SharesAdded(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesDeactivated(address indexed author, address indexed account);
\tevent SharesRemoved(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesSet(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 value,
\t\tbool isActive
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IRewardsManager {
\t/* Types */

\tstruct ShareData {
\t\taddress account;
\t\tuint128 value;
\t\tbool isActive;
\t}

\t/* Views */

\tfunction defaultRecipient() external view returns (address);

\tfunction rewardsToken() external view returns (address);

\tfunction sharesFor(address account)
\t\texternal
\t\tview
\t\treturns (uint128 active, uint128 total);

\tfunction totalRewardsAccrued() external view returns (uint256);

\tfunction totalRewardsRedeemed() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction activateShares() external;

\tfunction activateSharesFor(address account) external;

\tfunction addShares(address account, uint128 amount) external;

\tfunction deactivateShares() external;

\tfunction deactivateSharesFor(address account) external;

\tfunction recoverUnsupportedERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction removeShares(address account, uint128 amount) external;

\tfunction setDefaultRecipient(address account) external;

\tfunction setRewardsToken(address token) external;

\tfunction setShares(
\t\taddress account,
\t\tuint128 value,
\t\tbool isActive
\t) external;

\tfunction setSharesBatch(ShareData[] memory batch) external;

\t/* Events */

\tevent DefaultRecipientSet(address indexed author, address indexed account);
\tevent RecoveredUnsupported(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
\tevent RewardsTokenSet(address indexed author, address indexed token);
\tevent SharesActivated(address indexed author, address indexed account);
\tevent SharesAdded(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesDeactivated(address indexed author, address indexed account);
\tevent SharesRemoved(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesSet(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 value,
\t\tbool isActive
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IRewardsManager {
\t/* Types */

\tstruct ShareData {
\t\taddress account;
\t\tuint128 value;
\t\tbool isActive;
\t}

\t/* Views */

\tfunction defaultRecipient() external view returns (address);

\tfunction rewardsToken() external view returns (address);

\tfunction sharesFor(address account)
\t\texternal
\t\tview
\t\treturns (uint128 active, uint128 total);

\tfunction totalRewardsAccrued() external view returns (uint256);

\tfunction totalRewardsRedeemed() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction activateShares() external;

\tfunction activateSharesFor(address account) external;

\tfunction addShares(address account, uint128 amount) external;

\tfunction deactivateShares() external;

\tfunction deactivateSharesFor(address account) external;

\tfunction recoverUnsupportedERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction removeShares(address account, uint128 amount) external;

\tfunction setDefaultRecipient(address account) external;

\tfunction setRewardsToken(address token) external;

\tfunction setShares(
\t\taddress account,
\t\tuint128 value,
\t\tbool isActive
\t) external;

\tfunction setSharesBatch(ShareData[] memory batch) external;

\t/* Events */

\tevent DefaultRecipientSet(address indexed author, address indexed account);
\tevent RecoveredUnsupported(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
\tevent RewardsTokenSet(address indexed author, address indexed token);
\tevent SharesActivated(address indexed author, address indexed account);
\tevent SharesAdded(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesDeactivated(address indexed author, address indexed account);
\tevent SharesRemoved(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesSet(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 value,
\t\tbool isActive
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IRewardsManager {
\t/* Types */

\tstruct ShareData {
\t\taddress account;
\t\tuint128 value;
\t\tbool isActive;
\t}

\t/* Views */

\tfunction defaultRecipient() external view returns (address);

\tfunction rewardsToken() external view returns (address);

\tfunction sharesFor(address account)
\t\texternal
\t\tview
\t\treturns (uint128 active, uint128 total);

\tfunction totalRewardsAccrued() external view returns (uint256);

\tfunction totalRewardsRedeemed() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction activateShares() external;

\tfunction activateSharesFor(address account) external;

\tfunction addShares(address account, uint128 amount) external;

\tfunction deactivateShares() external;

\tfunction deactivateSharesFor(address account) external;

\tfunction recoverUnsupportedERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction removeShares(address account, uint128 amount) external;

\tfunction setDefaultRecipient(address account) external;

\tfunction setRewardsToken(address token) external;

\tfunction setShares(
\t\taddress account,
\t\tuint128 value,
\t\tbool isActive
\t) external;

\tfunction setSharesBatch(ShareData[] memory batch) external;

\t/* Events */

\tevent DefaultRecipientSet(address indexed author, address indexed account);
\tevent RecoveredUnsupported(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
\tevent RewardsTokenSet(address indexed author, address indexed token);
\tevent SharesActivated(address indexed author, address indexed account);
\tevent SharesAdded(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesDeactivated(address indexed author, address indexed account);
\tevent SharesRemoved(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesSet(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 value,
\t\tbool isActive
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IRewardsManager {
\t/* Types */

\tstruct ShareData {
\t\taddress account;
\t\tuint128 value;
\t\tbool isActive;
\t}

\t/* Views */

\tfunction defaultRecipient() external view returns (address);

\tfunction rewardsToken() external view returns (address);

\tfunction sharesFor(address account)
\t\texternal
\t\tview
\t\treturns (uint128 active, uint128 total);

\tfunction totalRewardsAccrued() external view returns (uint256);

\tfunction totalRewardsRedeemed() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction activateShares() external;

\tfunction activateSharesFor(address account) external;

\tfunction addShares(address account, uint128 amount) external;

\tfunction deactivateShares() external;

\tfunction deactivateSharesFor(address account) external;

\tfunction recoverUnsupportedERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction removeShares(address account, uint128 amount) external;

\tfunction setDefaultRecipient(address account) external;

\tfunction setRewardsToken(address token) external;

\tfunction setShares(
\t\taddress account,
\t\tuint128 value,
\t\tbool isActive
\t) external;

\tfunction setSharesBatch(ShareData[] memory batch) external;

\t/* Events */

\tevent DefaultRecipientSet(address indexed author, address indexed account);
\tevent RecoveredUnsupported(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
\tevent RewardsTokenSet(address indexed author, address indexed token);
\tevent SharesActivated(address indexed author, address indexed account);
\tevent SharesAdded(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesDeactivated(address indexed author, address indexed account);
\tevent SharesRemoved(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesSet(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 value,
\t\tbool isActive
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IRewardsManager {
\t/* Types */

\tstruct ShareData {
\t\taddress account;
\t\tuint128 value;
\t\tbool isActive;
\t}

\t/* Views */

\tfunction defaultRecipient() external view returns (address);

\tfunction rewardsToken() external view returns (address);

\tfunction sharesFor(address account)
\t\texternal
\t\tview
\t\treturns (uint128 active, uint128 total);

\tfunction totalRewardsAccrued() external view returns (uint256);

\tfunction totalRewardsRedeemed() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction activateShares() external;

\tfunction activateSharesFor(address account) external;

\tfunction addShares(address account, uint128 amount) external;

\tfunction deactivateShares() external;

\tfunction deactivateSharesFor(address account) external;

\tfunction recoverUnsupportedERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction removeShares(address account, uint128 amount) external;

\tfunction setDefaultRecipient(address account) external;

\tfunction setRewardsToken(address token) external;

\tfunction setShares(
\t\taddress account,
\t\tuint128 value,
\t\tbool isActive
\t) external;

\tfunction setSharesBatch(ShareData[] memory batch) external;

\t/* Events */

\tevent DefaultRecipientSet(address indexed author, address indexed account);
\tevent RecoveredUnsupported(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
\tevent RewardsTokenSet(address indexed author, address indexed token);
\tevent SharesActivated(address indexed author, address indexed account);
\tevent SharesAdded(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesDeactivated(address indexed author, address indexed account);
\tevent SharesRemoved(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesSet(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 value,
\t\tbool isActive
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IRewardsManager {
\t/* Types */

\tstruct ShareData {
\t\taddress account;
\t\tuint128 value;
\t\tbool isActive;
\t}

\t/* Views */

\tfunction defaultRecipient() external view returns (address);

\tfunction rewardsToken() external view returns (address);

\tfunction sharesFor(address account)
\t\texternal
\t\tview
\t\treturns (uint128 active, uint128 total);

\tfunction totalRewardsAccrued() external view returns (uint256);

\tfunction totalRewardsRedeemed() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction activateShares() external;

\tfunction activateSharesFor(address account) external;

\tfunction addShares(address account, uint128 amount) external;

\tfunction deactivateShares() external;

\tfunction deactivateSharesFor(address account) external;

\tfunction recoverUnsupportedERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction removeShares(address account, uint128 amount) external;

\tfunction setDefaultRecipient(address account) external;

\tfunction setRewardsToken(address token) external;

\tfunction setShares(
\t\taddress account,
\t\tuint128 value,
\t\tbool isActive
\t) external;

\tfunction setSharesBatch(ShareData[] memory batch) external;

\t/* Events */

\tevent DefaultRecipientSet(address indexed author, address indexed account);
\tevent RecoveredUnsupported(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
\tevent RewardsTokenSet(address indexed author, address indexed token);
\tevent SharesActivated(address indexed author, address indexed account);
\tevent SharesAdded(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesDeactivated(address indexed author, address indexed account);
\tevent SharesRemoved(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesSet(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 value,
\t\tbool isActive
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IRewardsManager {
\t/* Types */

\tstruct ShareData {
\t\taddress account;
\t\tuint128 value;
\t\tbool isActive;
\t}

\t/* Views */

\tfunction defaultRecipient() external view returns (address);

\tfunction rewardsToken() external view returns (address);

\tfunction sharesFor(address account)
\t\texternal
\t\tview
\t\treturns (uint128 active, uint128 total);

\tfunction totalRewardsAccrued() external view returns (uint256);

\tfunction totalRewardsRedeemed() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction activateShares() external;

\tfunction activateSharesFor(address account) external;

\tfunction addShares(address account, uint128 amount) external;

\tfunction deactivateShares() external;

\tfunction deactivateSharesFor(address account) external;

\tfunction recoverUnsupportedERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction removeShares(address account, uint128 amount) external;

\tfunction setDefaultRecipient(address account) external;

\tfunction setRewardsToken(address token) external;

\tfunction setShares(
\t\taddress account,
\t\tuint128 value,
\t\tbool isActive
\t) external;

\tfunction setSharesBatch(ShareData[] memory batch) external;

\t/* Events */

\tevent DefaultRecipientSet(address indexed author, address indexed account);
\tevent RecoveredUnsupported(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
\tevent RewardsTokenSet(address indexed author, address indexed token);
\tevent SharesActivated(address indexed author, address indexed account);
\tevent SharesAdded(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesDeactivated(address indexed author, address indexed account);
\tevent SharesRemoved(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesSet(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 value,
\t\tbool isActive
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IRewardsManager {
\t/* Types */

\tstruct ShareData {
\t\taddress account;
\t\tuint128 value;
\t\tbool isActive;
\t}

\t/* Views */

\tfunction defaultRecipient() external view returns (address);

\tfunction rewardsToken() external view returns (address);

\tfunction sharesFor(address account)
\t\texternal
\t\tview
\t\treturns (uint128 active, uint128 total);

\tfunction totalRewardsAccrued() external view returns (uint256);

\tfunction totalRewardsRedeemed() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction activateShares() external;

\tfunction activateSharesFor(address account) external;

\tfunction addShares(address account, uint128 amount) external;

\tfunction deactivateShares() external;

\tfunction deactivateSharesFor(address account) external;

\tfunction recoverUnsupportedERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction removeShares(address account, uint128 amount) external;

\tfunction setDefaultRecipient(address account) external;

\tfunction setRewardsToken(address token) external;

\tfunction setShares(
\t\taddress account,
\t\tuint128 value,
\t\tbool isActive
\t) external;

\tfunction setSharesBatch(ShareData[] memory batch) external;

\t/* Events */

\tevent DefaultRecipientSet(address indexed author, address indexed account);
\tevent RecoveredUnsupported(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
\tevent RewardsTokenSet(address indexed author, address indexed token);
\tevent SharesActivated(address indexed author, address indexed account);
\tevent SharesAdded(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesDeactivated(address indexed author, address indexed account);
\tevent SharesRemoved(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesSet(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 value,
\t\tbool isActive
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IRewardsManager {
\t/* Types */

\tstruct ShareData {
\t\taddress account;
\t\tuint128 value;
\t\tbool isActive;
\t}

\t/* Views */

\tfunction defaultRecipient() external view returns (address);

\tfunction rewardsToken() external view returns (address);

\tfunction sharesFor(address account)
\t\texternal
\t\tview
\t\treturns (uint128 active, uint128 total);

\tfunction totalRewardsAccrued() external view returns (uint256);

\tfunction totalRewardsRedeemed() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction activateShares() external;

\tfunction activateSharesFor(address account) external;

\tfunction addShares(address account, uint128 amount) external;

\tfunction deactivateShares() external;

\tfunction deactivateSharesFor(address account) external;

\tfunction recoverUnsupportedERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction removeShares(address account, uint128 amount) external;

\tfunction setDefaultRecipient(address account) external;

\tfunction setRewardsToken(address token) external;

\tfunction setShares(
\t\taddress account,
\t\tuint128 value,
\t\tbool isActive
\t) external;

\tfunction setSharesBatch(ShareData[] memory batch) external;

\t/* Events */

\tevent DefaultRecipientSet(address indexed author, address indexed account);
\tevent RecoveredUnsupported(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
\tevent RewardsTokenSet(address indexed author, address indexed token);
\tevent SharesActivated(address indexed author, address indexed account);
\tevent SharesAdded(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesDeactivated(address indexed author, address indexed account);
\tevent SharesRemoved(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesSet(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 value,
\t\tbool isActive
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IRewardsManager {
\t/* Types */

\tstruct ShareData {
\t\taddress account;
\t\tuint128 value;
\t\tbool isActive;
\t}

\t/* Views */

\tfunction defaultRecipient() external view returns (address);

\tfunction rewardsToken() external view returns (address);

\tfunction sharesFor(address account)
\t\texternal
\t\tview
\t\treturns (uint128 active, uint128 total);

\tfunction totalRewardsAccrued() external view returns (uint256);

\tfunction totalRewardsRedeemed() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction activateShares() external;

\tfunction activateSharesFor(address account) external;

\tfunction addShares(address account, uint128 amount) external;

\tfunction deactivateShares() external;

\tfunction deactivateSharesFor(address account) external;

\tfunction recoverUnsupportedERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction removeShares(address account, uint128 amount) external;

\tfunction setDefaultRecipient(address account) external;

\tfunction setRewardsToken(address token) external;

\tfunction setShares(
\t\taddress account,
\t\tuint128 value,
\t\tbool isActive
\t) external;

\tfunction setSharesBatch(ShareData[] memory batch) external;

\t/* Events */

\tevent DefaultRecipientSet(address indexed author, address indexed account);
\tevent RecoveredUnsupported(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
\tevent RewardsTokenSet(address indexed author, address indexed token);
\tevent SharesActivated(address indexed author, address indexed account);
\tevent SharesAdded(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesDeactivated(address indexed author, address indexed account);
\tevent SharesRemoved(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesSet(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 value,
\t\tbool isActive
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IRewardsManager {
\t/* Types */

\tstruct ShareData {
\t\taddress account;
\t\tuint128 value;
\t\tbool isActive;
\t}

\t/* Views */

\tfunction defaultRecipient() external view returns (address);

\tfunction rewardsToken() external view returns (address);

\tfunction sharesFor(address account)
\t\texternal
\t\tview
\t\treturns (uint128 active, uint128 total);

\tfunction totalRewardsAccrued() external view returns (uint256);

\tfunction totalRewardsRedeemed() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction activateShares() external;

\tfunction activateSharesFor(address account) external;

\tfunction addShares(address account, uint128 amount) external;

\tfunction deactivateShares() external;

\tfunction deactivateSharesFor(address account) external;

\tfunction recoverUnsupportedERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction removeShares(address account, uint128 amount) external;

\tfunction setDefaultRecipient(address account) external;

\tfunction setRewardsToken(address token) external;

\tfunction setShares(
\t\taddress account,
\t\tuint128 value,
\t\tbool isActive
\t) external;

\tfunction setSharesBatch(ShareData[] memory batch) external;

\t/* Events */

\tevent DefaultRecipientSet(address indexed author, address indexed account);
\tevent RecoveredUnsupported(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
\tevent RewardsTokenSet(address indexed author, address indexed token);
\tevent SharesActivated(address indexed author, address indexed account);
\tevent SharesAdded(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesDeactivated(address indexed author, address indexed account);
\tevent SharesRemoved(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesSet(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 value,
\t\tbool isActive
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IRewardsManager {
\t/* Types */

\tstruct ShareData {
\t\taddress account;
\t\tuint128 value;
\t\tbool isActive;
\t}

\t/* Views */

\tfunction defaultRecipient() external view returns (address);

\tfunction rewardsToken() external view returns (address);

\tfunction sharesFor(address account)
\t\texternal
\t\tview
\t\treturns (uint128 active, uint128 total);

\tfunction totalRewardsAccrued() external view returns (uint256);

\tfunction totalRewardsRedeemed() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction activateShares() external;

\tfunction activateSharesFor(address account) external;

\tfunction addShares(address account, uint128 amount) external;

\tfunction deactivateShares() external;

\tfunction deactivateSharesFor(address account) external;

\tfunction recoverUnsupportedERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction removeShares(address account, uint128 amount) external;

\tfunction setDefaultRecipient(address account) external;

\tfunction setRewardsToken(address token) external;

\tfunction setShares(
\t\taddress account,
\t\tuint128 value,
\t\tbool isActive
\t) external;

\tfunction setSharesBatch(ShareData[] memory batch) external;

\t/* Events */

\tevent DefaultRecipientSet(address indexed author, address indexed account);
\tevent RecoveredUnsupported(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
\tevent RewardsTokenSet(address indexed author, address indexed token);
\tevent SharesActivated(address indexed author, address indexed account);
\tevent SharesAdded(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesDeactivated(address indexed author, address indexed account);
\tevent SharesRemoved(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesSet(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 value,
\t\tbool isActive
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IRewardsManager {
\t/* Types */

\tstruct ShareData {
\t\taddress account;
\t\tuint128 value;
\t\tbool isActive;
\t}

\t/* Views */

\tfunction defaultRecipient() external view returns (address);

\tfunction rewardsToken() external view returns (address);

\tfunction sharesFor(address account)
\t\texternal
\t\tview
\t\treturns (uint128 active, uint128 total);

\tfunction totalRewardsAccrued() external view returns (uint256);

\tfunction totalRewardsRedeemed() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction activateShares() external;

\tfunction activateSharesFor(address account) external;

\tfunction addShares(address account, uint128 amount) external;

\tfunction deactivateShares() external;

\tfunction deactivateSharesFor(address account) external;

\tfunction recoverUnsupportedERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction removeShares(address account, uint128 amount) external;

\tfunction setDefaultRecipient(address account) external;

\tfunction setRewardsToken(address token) external;

\tfunction setShares(
\t\taddress account,
\t\tuint128 value,
\t\tbool isActive
\t) external;

\tfunction setSharesBatch(ShareData[] memory batch) external;

\t/* Events */

\tevent DefaultRecipientSet(address indexed author, address indexed account);
\tevent RecoveredUnsupported(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
\tevent RewardsTokenSet(address indexed author, address indexed token);
\tevent SharesActivated(address indexed author, address indexed account);
\tevent SharesAdded(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesDeactivated(address indexed author, address indexed account);
\tevent SharesRemoved(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesSet(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 value,
\t\tbool isActive
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IRewardsManager {
\t/* Types */

\tstruct ShareData {
\t\taddress account;
\t\tuint128 value;
\t\tbool isActive;
\t}

\t/* Views */

\tfunction defaultRecipient() external view returns (address);

\tfunction rewardsToken() external view returns (address);

\tfunction sharesFor(address account)
\t\texternal
\t\tview
\t\treturns (uint128 active, uint128 total);

\tfunction totalRewardsAccrued() external view returns (uint256);

\tfunction totalRewardsRedeemed() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction activateShares() external;

\tfunction activateSharesFor(address account) external;

\tfunction addShares(address account, uint128 amount) external;

\tfunction deactivateShares() external;

\tfunction deactivateSharesFor(address account) external;

\tfunction recoverUnsupportedERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction removeShares(address account, uint128 amount) external;

\tfunction setDefaultRecipient(address account) external;

\tfunction setRewardsToken(address token) external;

\tfunction setShares(
\t\taddress account,
\t\tuint128 value,
\t\tbool isActive
\t) external;

\tfunction setSharesBatch(ShareData[] memory batch) external;

\t/* Events */

\tevent DefaultRecipientSet(address indexed author, address indexed account);
\tevent RecoveredUnsupported(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
\tevent RewardsTokenSet(address indexed author, address indexed token);
\tevent SharesActivated(address indexed author, address indexed account);
\tevent SharesAdded(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesDeactivated(address indexed author, address indexed account);
\tevent SharesRemoved(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesSet(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 value,
\t\tbool isActive
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IRewardsManager {
\t/* Types */

\tstruct ShareData {
\t\taddress account;
\t\tuint128 value;
\t\tbool isActive;
\t}

\t/* Views */

\tfunction defaultRecipient() external view returns (address);

\tfunction rewardsToken() external view returns (address);

\tfunction sharesFor(address account)
\t\texternal
\t\tview
\t\treturns (uint128 active, uint128 total);

\tfunction totalRewardsAccrued() external view returns (uint256);

\tfunction totalRewardsRedeemed() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction activateShares() external;

\tfunction activateSharesFor(address account) external;

\tfunction addShares(address account, uint128 amount) external;

\tfunction deactivateShares() external;

\tfunction deactivateSharesFor(address account) external;

\tfunction recoverUnsupportedERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction removeShares(address account, uint128 amount) external;

\tfunction setDefaultRecipient(address account) external;

\tfunction setRewardsToken(address token) external;

\tfunction setShares(
\t\taddress account,
\t\tuint128 value,
\t\tbool isActive
\t) external;

\tfunction setSharesBatch(ShareData[] memory batch) external;

\t/* Events */

\tevent DefaultRecipientSet(address indexed author, address indexed account);
\tevent RecoveredUnsupported(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
\tevent RewardsTokenSet(address indexed author, address indexed token);
\tevent SharesActivated(address indexed author, address indexed account);
\tevent SharesAdded(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesDeactivated(address indexed author, address indexed account);
\tevent SharesRemoved(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 amount
\t);
\tevent SharesSet(
\t\taddress indexed author,
\t\taddress indexed account,
\t\tuint128 value,
\t\tbool isActive
\t);
}
"
    }
  }
}
