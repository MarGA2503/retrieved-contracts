pragma solidity \u003e=0.6.0 \u003c0.8.0;
pragma experimental ABIEncoderV2;

abstract contract Context {
\tfunction _msgSender() internal view virtual returns (address payable) {
\t\treturn msg.sender;
\t}

\tfunction _msgData() internal view virtual returns (bytes memory) {
\t\tthis; // silence state mutability warning without generating bytecode - see https://github.com/ethereum/solidity/issues/2691
\t\treturn msg.data;
\t}
}

library Base64 {
\tbytes internal constant TABLE = \"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/\";

\t/// @notice Encodes some bytes to the base64 representation
\tfunction encode(bytes memory data) internal pure returns (string memory) {
\t\tuint256 len = data.length;
\t\tif (len == 0) return \"\";

\t\t// multiply by 4/3 rounded up
\t\tuint256 encodedLen = 4 * ((len + 2) / 3);

\t\t// Add some extra buffer at the end
\t\tbytes memory result = new bytes(encodedLen + 32);

\t\tbytes memory table = TABLE;

\t\tassembly {
\t\t\tlet tablePtr := add(table, 1)
\t\t\tlet resultPtr := add(result, 32)

\t\t\tfor {
\t\t\t\tlet i := 0
\t\t\t} lt(i, len) {

\t\t\t} {
\t\t\t\ti := add(i, 3)
\t\t\t\tlet input := and(mload(add(data, i)), 0xffffff)

\t\t\t\tlet out := mload(add(tablePtr, and(shr(18, input), 0x3F)))
\t\t\t\tout := shl(8, out)
\t\t\t\tout := add(out, and(mload(add(tablePtr, and(shr(12, input), 0x3F))), 0xFF))
\t\t\t\tout := shl(8, out)
\t\t\t\tout := add(out, and(mload(add(tablePtr, and(shr(6, input), 0x3F))), 0xFF))
\t\t\t\tout := shl(8, out)
\t\t\t\tout := add(out, and(mload(add(tablePtr, and(input, 0x3F))), 0xFF))
\t\t\t\tout := shl(224, out)

\t\t\t\tmstore(resultPtr, out)

\t\t\t\tresultPtr := add(resultPtr, 4)
\t\t\t}

\t\t\tswitch mod(len, 3)
\t\t\tcase 1 {
\t\t\t\tmstore(sub(resultPtr, 2), shl(240, 0x3d3d))
\t\t\t}
\t\t\tcase 2 {
\t\t\t\tmstore(sub(resultPtr, 1), shl(248, 0x3d))
\t\t\t}

\t\t\tmstore(result, encodedLen)
\t\t}

\t\treturn string(result);
\t}
}

interface LootInterface {
\tfunction ownerOf(uint256 tokenId) external view returns (address owner);
}

abstract contract Ownable is Context {
\taddress private _owner;

\tevent OwnershipTransferred(address indexed previousOwner, address indexed newOwner);

\t/**
\t * @dev Initializes the contract setting the deployer as the initial owner.
\t */
\tconstructor () internal {
\t\t//address msgSender = _msgSender();
\t\t_owner = 0x3Facc82A985021e25D563cf458d49B9c86280D09;
\t\t//emit OwnershipTransferred(address(0), msgSender);
\t}

\t/**
\t * @dev Returns the address of the current owner.
\t */
\tfunction owner() public view virtual returns (address) {
\t\treturn _owner;
\t}

\t/**
\t * @dev Throws if called by any account other than the owner.
\t */
\tmodifier onlyOwner() {
\t\trequire(owner() == _msgSender(), \"Ownable: caller is not the owner\");
\t\t_;
\t}

\t/**
\t * @dev Leaves the contract without owner. It will not be possible to call
\t * `onlyOwner` functions anymore. Can only be called by the current owner.
\t *
\t * NOTE: Renouncing ownership will leave the contract without an owner,
\t * thereby removing any functionality that is only available to the owner.
\t */
\tfunction renounceOwnership() public virtual onlyOwner {
\t\temit OwnershipTransferred(_owner, address(0));
\t\t_owner = address(0);
\t}

\t/**
\t * @dev Transfers ownership of the contract to a new account (`newOwner`).
\t * Can only be called by the current owner.
\t */
\tfunction transferOwnership(address newOwner) public virtual onlyOwner {
\t\trequire(newOwner != address(0), \"Ownable: new owner is the zero address\");
\t\temit OwnershipTransferred(_owner, newOwner);
\t\t_owner = newOwner;
\t}
}

abstract contract ReentrancyGuard {
\t// Booleans are more expensive than uint256 or any type that takes up a full
\t// word because each write operation emits an extra SLOAD to first read the
\t// slot\u0027s contents, replace the bits taken up by the boolean, and then write
\t// back. This is the compiler\u0027s defense against contract upgrades and
\t// pointer aliasing, and it cannot be disabled.

\t// The values being non-zero value makes deployment a bit more expensive,
\t// but in exchange the refund on every call to nonReentrant will be lower in
\t// amount. Since refunds are capped to a percentage of the total
\t// transaction\u0027s gas, it is best to keep them low in cases like this one, to
\t// increase the likelihood of the full refund coming into effect.
\tuint256 private constant _NOT_ENTERED = 1;
\tuint256 private constant _ENTERED = 2;

\tuint256 private _status;

\tconstructor() {
\t\t_status = _NOT_ENTERED;
\t}

\t/**
\t * @dev Prevents a contract from calling itself, directly or indirectly.
\t * Calling a `nonReentrant` function from another `nonReentrant`
\t * function is not supported. It is possible to prevent this from happening
\t * by making the `nonReentrant` function external, and make it call a
\t * `private` function that does the actual work.
\t */
\tmodifier nonReentrant() {
\t\t// On the first call to nonReentrant, _notEntered will be true
\t\trequire(_status != _ENTERED, \"ReentrancyGuard: reentrant call\");

\t\t// Any calls to nonReentrant after this point will fail
\t\t_status = _ENTERED;

\t\t_;

\t\t// By storing the original value once again, a refund is triggered (see
\t\t// https://eips.ethereum.org/EIPS/eip-2200)
\t\t_status = _NOT_ENTERED;
\t}
}

interface IERC721Receiver {
\t/**
\t * @dev Whenever an {IERC721} `tokenId` token is transferred to this contract via {IERC721-safeTransferFrom}
\t * by `operator` from `from`, this function is called.
\t *
\t * It must return its Solidity selector to confirm the token transfer.
\t * If any other value is returned or the interface is not implemented by the recipient, the transfer will be reverted.
\t *
\t * The selector can be obtained in Solidity with `IERC721.onERC721Received.selector`.
\t */
\tfunction onERC721Received(address operator, address from, uint256 tokenId, bytes calldata data) external returns (bytes4);
}

library Strings {
\t/**
\t * @dev Converts a `uint256` to its ASCII `string` representation.
\t */
\tfunction toString(uint256 value) internal pure returns (string memory) {
\t\t// Inspired by OraclizeAPI\u0027s implementation - MIT licence
\t\t// https://github.com/oraclize/ethereum-api/blob/b42146b063c7d6ee1358846c198246239e9360e8/oraclizeAPI_0.4.25.sol

\t\tif (value == 0) {
\t\t\treturn \"0\";
\t\t}
\t\tuint256 temp = value;
\t\tuint256 digits;
\t\twhile (temp != 0) {
\t\t\tdigits++;
\t\t\ttemp /= 10;
\t\t}
\t\tbytes memory buffer = new bytes(digits);
\t\tuint256 index = digits - 1;
\t\ttemp = value;
\t\twhile (temp != 0) {
\t\t\tbuffer[index--] = bytes1(uint8(48 + temp % 10));
\t\t\ttemp /= 10;
\t\t}
\t\treturn string(buffer);
\t}
}

library EnumerableMap {
\t// To implement this library for multiple types with as little code
\t// repetition as possible, we write it in terms of a generic Map type with
\t// bytes32 keys and values.
\t// The Map implementation uses private functions, and user-facing
\t// implementations (such as Uint256ToAddressMap) are just wrappers around
\t// the underlying Map.
\t// This means that we can only create new EnumerableMaps for types that fit
\t// in bytes32.

\tstruct MapEntry {
\t\tbytes32 _key;
\t\tbytes32 _value;
\t}

\tstruct Map {
\t\t// Storage of map keys and values
\t\tMapEntry[] _entries;

\t\t// Position of the entry defined by a key in the `entries` array, plus 1
\t\t// because index 0 means a key is not in the map.
\t\tmapping (bytes32 =\u003e uint256) _indexes;
\t}

\t/**
\t * @dev Adds a key-value pair to a map, or updates the value for an existing
\t * key. O(1).
\t *
\t * Returns true if the key was added to the map, that is if it was not
\t * already present.
\t */
\tfunction _set(Map storage map, bytes32 key, bytes32 value) private returns (bool) {
\t\t// We read and store the key\u0027s index to prevent multiple reads from the same storage slot
\t\tuint256 keyIndex = map._indexes[key];

\t\tif (keyIndex == 0) { // Equivalent to !contains(map, key)
\t\t\tmap._entries.push(MapEntry({ _key: key, _value: value }));
\t\t\t// The entry is stored at length-1, but we add 1 to all indexes
\t\t\t// and use 0 as a sentinel value
\t\t\tmap._indexes[key] = map._entries.length;
\t\t\treturn true;
\t\t} else {
\t\t\tmap._entries[keyIndex - 1]._value = value;
\t\t\treturn false;
\t\t}
\t}

\t/**
\t * @dev Removes a key-value pair from a map. O(1).
\t *
\t * Returns true if the key was removed from the map, that is if it was present.
\t */
\tfunction _remove(Map storage map, bytes32 key) private returns (bool) {
\t\t// We read and store the key\u0027s index to prevent multiple reads from the same storage slot
\t\tuint256 keyIndex = map._indexes[key];

\t\tif (keyIndex != 0) { // Equivalent to contains(map, key)
\t\t\t// To delete a key-value pair from the _entries array in O(1), we swap the entry to delete with the last one
\t\t\t// in the array, and then remove the last entry (sometimes called as \u0027swap and pop\u0027).
\t\t\t// This modifies the order of the array, as noted in {at}.

\t\t\tuint256 toDeleteIndex = keyIndex - 1;
\t\t\tuint256 lastIndex = map._entries.length - 1;

\t\t\t// When the entry to delete is the last one, the swap operation is unnecessary. However, since this occurs
\t\t\t// so rarely, we still do the swap anyway to avoid the gas cost of adding an \u0027if\u0027 statement.

\t\t\tMapEntry storage lastEntry = map._entries[lastIndex];

\t\t\t// Move the last entry to the index where the entry to delete is
\t\t\tmap._entries[toDeleteIndex] = lastEntry;
\t\t\t// Update the index for the moved entry
\t\t\tmap._indexes[lastEntry._key] = toDeleteIndex + 1; // All indexes are 1-based

\t\t\t// Delete the slot where the moved entry was stored
\t\t\tmap._entries.pop();

\t\t\t// Delete the index for the deleted slot
\t\t\tdelete map._indexes[key];

\t\t\treturn true;
\t\t} else {
\t\t\treturn false;
\t\t}
\t}

\t/**
\t * @dev Returns true if the key is in the map. O(1).
\t */
\tfunction _contains(Map storage map, bytes32 key) private view returns (bool) {
\t\treturn map._indexes[key] != 0;
\t}

\t/**
\t * @dev Returns the number of key-value pairs in the map. O(1).
\t */
\tfunction _length(Map storage map) private view returns (uint256) {
\t\treturn map._entries.length;
\t}

\t/**
\t * @dev Returns the key-value pair stored at position `index` in the map. O(1).
\t *
\t * Note that there are no guarantees on the ordering of entries inside the
\t * array, and it may change when more entries are added or removed.
\t *
\t * Requirements:
\t *
\t * - `index` must be strictly less than {length}.
\t */
\tfunction _at(Map storage map, uint256 index) private view returns (bytes32, bytes32) {
\t\trequire(map._entries.length \u003e index, \"EnumerableMap: index out of bounds\");

\t\tMapEntry storage entry = map._entries[index];
\t\treturn (entry._key, entry._value);
\t}

\t/**
\t * @dev Tries to returns the value associated with `key`.  O(1).
\t * Does not revert if `key` is not in the map.
\t */
\tfunction _tryGet(Map storage map, bytes32 key) private view returns (bool, bytes32) {
\t\tuint256 keyIndex = map._indexes[key];
\t\tif (keyIndex == 0) return (false, 0); // Equivalent to contains(map, key)
\t\treturn (true, map._entries[keyIndex - 1]._value); // All indexes are 1-based
\t}

\t/**
\t * @dev Returns the value associated with `key`.  O(1).
\t *
\t * Requirements:
\t *
\t * - `key` must be in the map.
\t */
\tfunction _get(Map storage map, bytes32 key) private view returns (bytes32) {
\t\tuint256 keyIndex = map._indexes[key];
\t\trequire(keyIndex != 0, \"EnumerableMap: nonexistent key\"); // Equivalent to contains(map, key)
\t\treturn map._entries[keyIndex - 1]._value; // All indexes are 1-based
\t}

\t/**
\t * @dev Same as {_get}, with a custom error message when `key` is not in the map.
\t *
\t * CAUTION: This function is deprecated because it requires allocating memory for the error
\t * message unnecessarily. For custom revert reasons use {_tryGet}.
\t */
\tfunction _get(Map storage map, bytes32 key, string memory errorMessage) private view returns (bytes32) {
\t\tuint256 keyIndex = map._indexes[key];
\t\trequire(keyIndex != 0, errorMessage); // Equivalent to contains(map, key)
\t\treturn map._entries[keyIndex - 1]._value; // All indexes are 1-based
\t}

\t// UintToAddressMap

\tstruct UintToAddressMap {
\t\tMap _inner;
\t}

\t/**
\t * @dev Adds a key-value pair to a map, or updates the value for an existing
\t * key. O(1).
\t *
\t * Returns true if the key was added to the map, that is if it was not
\t * already present.
\t */
\tfunction set(UintToAddressMap storage map, uint256 key, address value) internal returns (bool) {
\t\treturn _set(map._inner, bytes32(key), bytes32(uint256(uint160(value))));
\t}

\t/**
\t * @dev Removes a value from a set. O(1).
\t *
\t * Returns true if the key was removed from the map, that is if it was present.
\t */
\tfunction remove(UintToAddressMap storage map, uint256 key) internal returns (bool) {
\t\treturn _remove(map._inner, bytes32(key));
\t}

\t/**
\t * @dev Returns true if the key is in the map. O(1).
\t */
\tfunction contains(UintToAddressMap storage map, uint256 key) internal view returns (bool) {
\t\treturn _contains(map._inner, bytes32(key));
\t}

\t/**
\t * @dev Returns the number of elements in the map. O(1).
\t */
\tfunction length(UintToAddressMap storage map) internal view returns (uint256) {
\t\treturn _length(map._inner);
\t}

\t/**
\t * @dev Returns the element stored at position `index` in the set. O(1).
\t * Note that there are no guarantees on the ordering of values inside the
\t * array, and it may change when more values are added or removed.
\t *
\t * Requirements:
\t *
\t * - `index` must be strictly less than {length}.
\t */
\tfunction at(UintToAddressMap storage map, uint256 index) internal view returns (uint256, address) {
\t\t(bytes32 key, bytes32 value) = _at(map._inner, index);
\t\treturn (uint256(key), address(uint160(uint256(value))));
\t}

\t/**
\t * @dev Tries to returns the value associated with `key`.  O(1).
\t * Does not revert if `key` is not in the map.
\t *
\t * _Available since v3.4._
\t */
\tfunction tryGet(UintToAddressMap storage map, uint256 key) internal view returns (bool, address) {
\t\t(bool success, bytes32 value) = _tryGet(map._inner, bytes32(key));
\t\treturn (success, address(uint160(uint256(value))));
\t}

\t/**
\t * @dev Returns the value associated with `key`.  O(1).
\t *
\t * Requirements:
\t *
\t * - `key` must be in the map.
\t */
\tfunction get(UintToAddressMap storage map, uint256 key) internal view returns (address) {
\t\treturn address(uint160(uint256(_get(map._inner, bytes32(key)))));
\t}

\t/**
\t * @dev Same as {get}, with a custom error message when `key` is not in the map.
\t *
\t * CAUTION: This function is deprecated because it requires allocating memory for the error
\t * message unnecessarily. For custom revert reasons use {tryGet}.
\t */
\tfunction get(UintToAddressMap storage map, uint256 key, string memory errorMessage) internal view returns (address) {
\t\treturn address(uint160(uint256(_get(map._inner, bytes32(key), errorMessage))));
\t}
}

library EnumerableSet {
\t// To implement this library for multiple types with as little code
\t// repetition as possible, we write it in terms of a generic Set type with
\t// bytes32 values.
\t// The Set implementation uses private functions, and user-facing
\t// implementations (such as AddressSet) are just wrappers around the
\t// underlying Set.
\t// This means that we can only create new EnumerableSets for types that fit
\t// in bytes32.

\tstruct Set {
\t\t// Storage of set values
\t\tbytes32[] _values;

\t\t// Position of the value in the `values` array, plus 1 because index 0
\t\t// means a value is not in the set.
\t\tmapping (bytes32 =\u003e uint256) _indexes;
\t}

\t/**
\t * @dev Add a value to a set. O(1).
\t *
\t * Returns true if the value was added to the set, that is if it was not
\t * already present.
\t */
\tfunction _add(Set storage set, bytes32 value) private returns (bool) {
\t\tif (!_contains(set, value)) {
\t\t\tset._values.push(value);
\t\t\t// The value is stored at length-1, but we add 1 to all indexes
\t\t\t// and use 0 as a sentinel value
\t\t\tset._indexes[value] = set._values.length;
\t\t\treturn true;
\t\t} else {
\t\t\treturn false;
\t\t}
\t}

\t/**
\t * @dev Removes a value from a set. O(1).
\t *
\t * Returns true if the value was removed from the set, that is if it was
\t * present.
\t */
\tfunction _remove(Set storage set, bytes32 value) private returns (bool) {
\t\t// We read and store the value\u0027s index to prevent multiple reads from the same storage slot
\t\tuint256 valueIndex = set._indexes[value];

\t\tif (valueIndex != 0) { // Equivalent to contains(set, value)
\t\t\t// To delete an element from the _values array in O(1), we swap the element to delete with the last one in
\t\t\t// the array, and then remove the last element (sometimes called as \u0027swap and pop\u0027).
\t\t\t// This modifies the order of the array, as noted in {at}.

\t\t\tuint256 toDeleteIndex = valueIndex - 1;
\t\t\tuint256 lastIndex = set._values.length - 1;

\t\t\t// When the value to delete is the last one, the swap operation is unnecessary. However, since this occurs
\t\t\t// so rarely, we still do the swap anyway to avoid the gas cost of adding an \u0027if\u0027 statement.

\t\t\tbytes32 lastvalue = set._values[lastIndex];

\t\t\t// Move the last value to the index where the value to delete is
\t\t\tset._values[toDeleteIndex] = lastvalue;
\t\t\t// Update the index for the moved value
\t\t\tset._indexes[lastvalue] = toDeleteIndex + 1; // All indexes are 1-based

\t\t\t// Delete the slot where the moved value was stored
\t\t\tset._values.pop();

\t\t\t// Delete the index for the deleted slot
\t\t\tdelete set._indexes[value];

\t\t\treturn true;
\t\t} else {
\t\t\treturn false;
\t\t}
\t}

\t/**
\t * @dev Returns true if the value is in the set. O(1).
\t */
\tfunction _contains(Set storage set, bytes32 value) private view returns (bool) {
\t\treturn set._indexes[value] != 0;
\t}

\t/**
\t * @dev Returns the number of values on the set. O(1).
\t */
\tfunction _length(Set storage set) private view returns (uint256) {
\t\treturn set._values.length;
\t}

\t/**
\t * @dev Returns the value stored at position `index` in the set. O(1).
\t *
\t * Note that there are no guarantees on the ordering of values inside the
\t * array, and it may change when more values are added or removed.
\t *
\t * Requirements:
\t *
\t * - `index` must be strictly less than {length}.
\t */
\tfunction _at(Set storage set, uint256 index) private view returns (bytes32) {
\t\trequire(set._values.length \u003e index, \"EnumerableSet: index out of bounds\");
\t\treturn set._values[index];
\t}

\t// Bytes32Set

\tstruct Bytes32Set {
\t\tSet _inner;
\t}

\t/**
\t * @dev Add a value to a set. O(1).
\t *
\t * Returns true if the value was added to the set, that is if it was not
\t * already present.
\t */
\tfunction add(Bytes32Set storage set, bytes32 value) internal returns (bool) {
\t\treturn _add(set._inner, value);
\t}

\t/**
\t * @dev Removes a value from a set. O(1).
\t *
\t * Returns true if the value was removed from the set, that is if it was
\t * present.
\t */
\tfunction remove(Bytes32Set storage set, bytes32 value) internal returns (bool) {
\t\treturn _remove(set._inner, value);
\t}

\t/**
\t * @dev Returns true if the value is in the set. O(1).
\t */
\tfunction contains(Bytes32Set storage set, bytes32 value) internal view returns (bool) {
\t\treturn _contains(set._inner, value);
\t}

\t/**
\t * @dev Returns the number of values in the set. O(1).
\t */
\tfunction length(Bytes32Set storage set) internal view returns (uint256) {
\t\treturn _length(set._inner);
\t}

\t/**
\t * @dev Returns the value stored at position `index` in the set. O(1).
\t *
\t * Note that there are no guarantees on the ordering of values inside the
\t * array, and it may change when more values are added or removed.
\t *
\t * Requirements:
\t *
\t * - `index` must be strictly less than {length}.
\t */
\tfunction at(Bytes32Set storage set, uint256 index) internal view returns (bytes32) {
\t\treturn _at(set._inner, index);
\t}

\t// AddressSet

\tstruct AddressSet {
\t\tSet _inner;
\t}

\t/**
\t * @dev Add a value to a set. O(1).
\t *
\t * Returns true if the value was added to the set, that is if it was not
\t * already present.
\t */
\tfunction add(AddressSet storage set, address value) internal returns (bool) {
\t\treturn _add(set._inner, bytes32(uint256(uint160(value))));
\t}

\t/**
\t * @dev Removes a value from a set. O(1).
\t *
\t * Returns true if the value was removed from the set, that is if it was
\t * present.
\t */
\tfunction remove(AddressSet storage set, address value) internal returns (bool) {
\t\treturn _remove(set._inner, bytes32(uint256(uint160(value))));
\t}

\t/**
\t * @dev Returns true if the value is in the set. O(1).
\t */
\tfunction contains(AddressSet storage set, address value) internal view returns (bool) {
\t\treturn _contains(set._inner, bytes32(uint256(uint160(value))));
\t}

\t/**
\t * @dev Returns the number of values in the set. O(1).
\t */
\tfunction length(AddressSet storage set) internal view returns (uint256) {
\t\treturn _length(set._inner);
\t}

\t/**
\t * @dev Returns the value stored at position `index` in the set. O(1).
\t *
\t * Note that there are no guarantees on the ordering of values inside the
\t * array, and it may change when more values are added or removed.
\t *
\t * Requirements:
\t *
\t * - `index` must be strictly less than {length}.
\t */
\tfunction at(AddressSet storage set, uint256 index) internal view returns (address) {
\t\treturn address(uint160(uint256(_at(set._inner, index))));
\t}


\t// UintSet

\tstruct UintSet {
\t\tSet _inner;
\t}

\t/**
\t * @dev Add a value to a set. O(1).
\t *
\t * Returns true if the value was added to the set, that is if it was not
\t * already present.
\t */
\tfunction add(UintSet storage set, uint256 value) internal returns (bool) {
\t\treturn _add(set._inner, bytes32(value));
\t}

\t/**
\t * @dev Removes a value from a set. O(1).
\t *
\t * Returns true if the value was removed from the set, that is if it was
\t * present.
\t */
\tfunction remove(UintSet storage set, uint256 value) internal returns (bool) {
\t\treturn _remove(set._inner, bytes32(value));
\t}

\t/**
\t * @dev Returns true if the value is in the set. O(1).
\t */
\tfunction contains(UintSet storage set, uint256 value) internal view returns (bool) {
\t\treturn _contains(set._inner, bytes32(value));
\t}

\t/**
\t * @dev Returns the number of values on the set. O(1).
\t */
\tfunction length(UintSet storage set) internal view returns (uint256) {
\t\treturn _length(set._inner);
\t}

\t/**
\t * @dev Returns the value stored at position `index` in the set. O(1).
\t *
\t * Note that there are no guarantees on the ordering of values inside the
\t * array, and it may change when more values are added or removed.
\t *
\t * Requirements:
\t *
\t * - `index` must be strictly less than {length}.
\t */
\tfunction at(UintSet storage set, uint256 index) internal view returns (uint256) {
\t\treturn uint256(_at(set._inner, index));
\t}
}

library Address {
\t/**
\t * @dev Returns true if `account` is a contract.
\t *
\t * [IMPORTANT]
\t * ====
\t * It is unsafe to assume that an address for which this function returns
\t * false is an externally-owned account (EOA) and not a contract.
\t *
\t * Among others, `isContract` will return false for the following
\t * types of addresses:
\t *
\t *  - an externally-owned account
\t *  - a contract in construction
\t *  - an address where a contract will be created
\t *  - an address where a contract lived, but was destroyed
\t * ====
\t */
\tfunction isContract(address account) internal view returns (bool) {
\t\t// This method relies on extcodesize, which returns 0 for contracts in
\t\t// construction, since the code is only stored at the end of the
\t\t// constructor execution.

\t\tuint256 size;
\t\t// solhint-disable-next-line no-inline-assembly
\t\tassembly { size := extcodesize(account) }
\t\treturn size \u003e 0;
\t}

\t/**
\t * @dev Replacement for Solidity\u0027s `transfer`: sends `amount` wei to
\t * `recipient`, forwarding all available gas and reverting on errors.
\t *
\t * https://eips.ethereum.org/EIPS/eip-1884[EIP1884] increases the gas cost
\t * of certain opcodes, possibly making contracts go over the 2300 gas limit
\t * imposed by `transfer`, making them unable to receive funds via
\t * `transfer`. {sendValue} removes this limitation.
\t *
\t * https://diligence.consensys.net/posts/2019/09/stop-using-soliditys-transfer-now/[Learn more].
\t *
\t * IMPORTANT: because control is transferred to `recipient`, care must be
\t * taken to not create reentrancy vulnerabilities. Consider using
\t * {ReentrancyGuard} or the
\t * https://solidity.readthedocs.io/en/v0.5.11/security-considerations.html#use-the-checks-effects-interactions-pattern[checks-effects-interactions pattern].
\t */
\tfunction sendValue(address payable recipient, uint256 amount) internal {
\t\trequire(address(this).balance \u003e= amount, \"Address: insufficient balance\");

\t\t// solhint-disable-next-line avoid-low-level-calls, avoid-call-value
\t\t(bool success, ) = recipient.call{ value: amount }(\"\");
\t\trequire(success, \"Address: unable to send value, recipient may have reverted\");
\t}

\t/**
\t * @dev Performs a Solidity function call using a low level `call`. A
\t * plain`call` is an unsafe replacement for a function call: use this
\t * function instead.
\t *
\t * If `target` reverts with a revert reason, it is bubbled up by this
\t * function (like regular Solidity function calls).
\t *
\t * Returns the raw returned data. To convert to the expected return value,
\t * use https://solidity.readthedocs.io/en/latest/units-and-global-variables.html?highlight=abi.decode#abi-encoding-and-decoding-functions[`abi.decode`].
\t *
\t * Requirements:
\t *
\t * - `target` must be a contract.
\t * - calling `target` with `data` must not revert.
\t *
\t * _Available since v3.1._
\t */
\tfunction functionCall(address target, bytes memory data) internal returns (bytes memory) {
\t\treturn functionCall(target, data, \"Address: low-level call failed\");
\t}

\t/**
\t * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`], but with
\t * `errorMessage` as a fallback revert reason when `target` reverts.
\t *
\t * _Available since v3.1._
\t */
\tfunction functionCall(address target, bytes memory data, string memory errorMessage) internal returns (bytes memory) {
\t\treturn functionCallWithValue(target, data, 0, errorMessage);
\t}

\t/**
\t * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`],
\t * but also transferring `value` wei to `target`.
\t *
\t * Requirements:
\t *
\t * - the calling contract must have an ETH balance of at least `value`.
\t * - the called Solidity function must be `payable`.
\t *
\t * _Available since v3.1._
\t */
\tfunction functionCallWithValue(address target, bytes memory data, uint256 value) internal returns (bytes memory) {
\t\treturn functionCallWithValue(target, data, value, \"Address: low-level call with value failed\");
\t}

\t/**
\t * @dev Same as {xref-Address-functionCallWithValue-address-bytes-uint256-}[`functionCallWithValue`], but
\t * with `errorMessage` as a fallback revert reason when `target` reverts.
\t *
\t * _Available since v3.1._
\t */
\tfunction functionCallWithValue(address target, bytes memory data, uint256 value, string memory errorMessage) internal returns (bytes memory) {
\t\trequire(address(this).balance \u003e= value, \"Address: insufficient balance for call\");
\t\trequire(isContract(target), \"Address: call to non-contract\");

\t\t// solhint-disable-next-line avoid-low-level-calls
\t\t(bool success, bytes memory returndata) = target.call{ value: value }(data);
\t\treturn _verifyCallResult(success, returndata, errorMessage);
\t}

\t/**
\t * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`],
\t * but performing a static call.
\t *
\t * _Available since v3.3._
\t */
\tfunction functionStaticCall(address target, bytes memory data) internal view returns (bytes memory) {
\t\treturn functionStaticCall(target, data, \"Address: low-level static call failed\");
\t}

\t/**
\t * @dev Same as {xref-Address-functionCall-address-bytes-string-}[`functionCall`],
\t * but performing a static call.
\t *
\t * _Available since v3.3._
\t */
\tfunction functionStaticCall(address target, bytes memory data, string memory errorMessage) internal view returns (bytes memory) {
\t\trequire(isContract(target), \"Address: static call to non-contract\");

\t\t// solhint-disable-next-line avoid-low-level-calls
\t\t(bool success, bytes memory returndata) = target.staticcall(data);
\t\treturn _verifyCallResult(success, returndata, errorMessage);
\t}

\t/**
\t * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`],
\t * but performing a delegate call.
\t *
\t * _Available since v3.4._
\t */
\tfunction functionDelegateCall(address target, bytes memory data) internal returns (bytes memory) {
\t\treturn functionDelegateCall(target, data, \"Address: low-level delegate call failed\");
\t}

\t/**
\t * @dev Same as {xref-Address-functionCall-address-bytes-string-}[`functionCall`],
\t * but performing a delegate call.
\t *
\t * _Available since v3.4._
\t */
\tfunction functionDelegateCall(address target, bytes memory data, string memory errorMessage) internal returns (bytes memory) {
\t\trequire(isContract(target), \"Address: delegate call to non-contract\");

\t\t// solhint-disable-next-line avoid-low-level-calls
\t\t(bool success, bytes memory returndata) = target.delegatecall(data);
\t\treturn _verifyCallResult(success, returndata, errorMessage);
\t}

\tfunction _verifyCallResult(bool success, bytes memory returndata, string memory errorMessage) private pure returns(bytes memory) {
\t\tif (success) {
\t\t\treturn returndata;
\t\t} else {
\t\t\t// Look for revert reason and bubble it up if present
\t\t\tif (returndata.length \u003e 0) {
\t\t\t\t// The easiest way to bubble the revert reason is using memory via assembly

\t\t\t\t// solhint-disable-next-line no-inline-assembly
\t\t\t\tassembly {
\t\t\t\t\tlet returndata_size := mload(returndata)
\t\t\t\t\trevert(add(32, returndata), returndata_size)
\t\t\t\t}
\t\t\t} else {
\t\t\t\trevert(errorMessage);
\t\t\t}
\t\t}
\t}
}

library SafeMath {
\t/**
\t * @dev Returns the addition of two unsigned integers, with an overflow flag.
\t *
\t * _Available since v3.4._
\t */
\tfunction tryAdd(uint256 a, uint256 b) internal pure returns (bool, uint256) {
\t\tuint256 c = a + b;
\t\tif (c \u003c a) return (false, 0);
\t\treturn (true, c);
\t}

\t/**
\t * @dev Returns the substraction of two unsigned integers, with an overflow flag.
\t *
\t * _Available since v3.4._
\t */
\tfunction trySub(uint256 a, uint256 b) internal pure returns (bool, uint256) {
\t\tif (b \u003e a) return (false, 0);
\t\treturn (true, a - b);
\t}

\t/**
\t * @dev Returns the multiplication of two unsigned integers, with an overflow flag.
\t *
\t * _Available since v3.4._
\t */
\tfunction tryMul(uint256 a, uint256 b) internal pure returns (bool, uint256) {
\t\t// Gas optimization: this is cheaper than requiring \u0027a\u0027 not being zero, but the
\t\t// benefit is lost if \u0027b\u0027 is also tested.
\t\t// See: https://github.com/OpenZeppelin/openzeppelin-contracts/pull/522
\t\tif (a == 0) return (true, 0);
\t\tuint256 c = a * b;
\t\tif (c / a != b) return (false, 0);
\t\treturn (true, c);
\t}

\t/**
\t * @dev Returns the division of two unsigned integers, with a division by zero flag.
\t *
\t * _Available since v3.4._
\t */
\tfunction tryDiv(uint256 a, uint256 b) internal pure returns (bool, uint256) {
\t\tif (b == 0) return (false, 0);
\t\treturn (true, a / b);
\t}

\t/**
\t * @dev Returns the remainder of dividing two unsigned integers, with a division by zero flag.
\t *
\t * _Available since v3.4._
\t */
\tfunction tryMod(uint256 a, uint256 b) internal pure returns (bool, uint256) {
\t\tif (b == 0) return (false, 0);
\t\treturn (true, a % b);
\t}

\t/**
\t * @dev Returns the addition of two unsigned integers, reverting on
\t * overflow.
\t *
\t * Counterpart to Solidity\u0027s `+` operator.
\t *
\t * Requirements:
\t *
\t * - Addition cannot overflow.
\t */
\tfunction add(uint256 a, uint256 b) internal pure returns (uint256) {
\t\tuint256 c = a + b;
\t\trequire(c \u003e= a, \"SafeMath: addition overflow\");
\t\treturn c;
\t}

\t/**
\t * @dev Returns the subtraction of two unsigned integers, reverting on
\t * overflow (when the result is negative).
\t *
\t * Counterpart to Solidity\u0027s `-` operator.
\t *
\t * Requirements:
\t *
\t * - Subtraction cannot overflow.
\t */
\tfunction sub(uint256 a, uint256 b) internal pure returns (uint256) {
\t\trequire(b \u003c= a, \"SafeMath: subtraction overflow\");
\t\treturn a - b;
\t}

\t/**
\t * @dev Returns the multiplication of two unsigned integers, reverting on
\t * overflow.
\t *
\t * Counterpart to Solidity\u0027s `*` operator.
\t *
\t * Requirements:
\t *
\t * - Multiplication cannot overflow.
\t */
\tfunction mul(uint256 a, uint256 b) internal pure returns (uint256) {
\t\tif (a == 0) return 0;
\t\tuint256 c = a * b;
\t\trequire(c / a == b, \"SafeMath: multiplication overflow\");
\t\treturn c;
\t}

\t/**
\t * @dev Returns the integer division of two unsigned integers, reverting on
\t * division by zero. The result is rounded towards zero.
\t *
\t * Counterpart to Solidity\u0027s `/` operator. Note: this function uses a
\t * `revert` opcode (which leaves remaining gas untouched) while Solidity
\t * uses an invalid opcode to revert (consuming all remaining gas).
\t *
\t * Requirements:
\t *
\t * - The divisor cannot be zero.
\t */
\tfunction div(uint256 a, uint256 b) internal pure returns (uint256) {
\t\trequire(b \u003e 0, \"SafeMath: division by zero\");
\t\treturn a / b;
\t}

\t/**
\t * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
\t * reverting when dividing by zero.
\t *
\t * Counterpart to Solidity\u0027s `%` operator. This function uses a `revert`
\t * opcode (which leaves remaining gas untouched) while Solidity uses an
\t * invalid opcode to revert (consuming all remaining gas).
\t *
\t * Requirements:
\t *
\t * - The divisor cannot be zero.
\t */
\tfunction mod(uint256 a, uint256 b) internal pure returns (uint256) {
\t\trequire(b \u003e 0, \"SafeMath: modulo by zero\");
\t\treturn a % b;
\t}

\t/**
\t * @dev Returns the subtraction of two unsigned integers, reverting with custom message on
\t * overflow (when the result is negative).
\t *
\t * CAUTION: This function is deprecated because it requires allocating memory for the error
\t * message unnecessarily. For custom revert reasons use {trySub}.
\t *
\t * Counterpart to Solidity\u0027s `-` operator.
\t *
\t * Requirements:
\t *
\t * - Subtraction cannot overflow.
\t */
\tfunction sub(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
\t\trequire(b \u003c= a, errorMessage);
\t\treturn a - b;
\t}

\t/**
\t * @dev Returns the integer division of two unsigned integers, reverting with custom message on
\t * division by zero. The result is rounded towards zero.
\t *
\t * CAUTION: This function is deprecated because it requires allocating memory for the error
\t * message unnecessarily. For custom revert reasons use {tryDiv}.
\t *
\t * Counterpart to Solidity\u0027s `/` operator. Note: this function uses a
\t * `revert` opcode (which leaves remaining gas untouched) while Solidity
\t * uses an invalid opcode to revert (consuming all remaining gas).
\t *
\t * Requirements:
\t *
\t * - The divisor cannot be zero.
\t */
\tfunction div(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
\t\trequire(b \u003e 0, errorMessage);
\t\treturn a / b;
\t}

\t/**
\t * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
\t * reverting with custom message when dividing by zero.
\t *
\t * CAUTION: This function is deprecated because it requires allocating memory for the error
\t * message unnecessarily. For custom revert reasons use {tryMod}.
\t *
\t * Counterpart to Solidity\u0027s `%` operator. This function uses a `revert`
\t * opcode (which leaves remaining gas untouched) while Solidity uses an
\t * invalid opcode to revert (consuming all remaining gas).
\t *
\t * Requirements:
\t *
\t * - The divisor cannot be zero.
\t */
\tfunction mod(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
\t\trequire(b \u003e 0, errorMessage);
\t\treturn a % b;
\t}
}

interface IERC165 {
\t/**
\t * @dev Returns true if this contract implements the interface defined by
\t * `interfaceId`. See the corresponding
\t * https://eips.ethereum.org/EIPS/eip-165#how-interfaces-are-identified[EIP section]
\t * to learn more about how these ids are created.
\t *
\t * This function call must use less than 30 000 gas.
\t */
\tfunction supportsInterface(bytes4 interfaceId) external view returns (bool);
}

abstract contract ERC165 is IERC165 {
\t/*
\t * bytes4(keccak256(\u0027supportsInterface(bytes4)\u0027)) == 0x01ffc9a7
\t */
\tbytes4 private constant _INTERFACE_ID_ERC165 = 0x01ffc9a7;

\t/**
\t * @dev Mapping of interface ids to whether or not it\u0027s supported.
\t */
\tmapping(bytes4 =\u003e bool) private _supportedInterfaces;

\tconstructor () internal {
\t\t// Derived contracts need only register support for their own interfaces,
\t\t// we register support for ERC165 itself here
\t\t_registerInterface(_INTERFACE_ID_ERC165);
\t}

\t/**
\t * @dev See {IERC165-supportsInterface}.
\t *
\t * Time complexity O(1), guaranteed to always use less than 30 000 gas.
\t */
\tfunction supportsInterface(bytes4 interfaceId) public view virtual override returns (bool) {
\t\treturn _supportedInterfaces[interfaceId];
\t}

\t/**
\t * @dev Registers the contract as an implementer of the interface defined by
\t * `interfaceId`. Support of the actual ERC165 interface is automatic and
\t * registering its interface id is not required.
\t *
\t * See {IERC165-supportsInterface}.
\t *
\t * Requirements:
\t *
\t * - `interfaceId` cannot be the ERC165 invalid interface (`0xffffffff`).
\t */
\tfunction _registerInterface(bytes4 interfaceId) internal virtual {
\t\trequire(interfaceId != 0xffffffff, \"ERC165: invalid interface id\");
\t\t_supportedInterfaces[interfaceId] = true;
\t}
}

interface IERC721 is IERC165 {
\t/**
\t * @dev Emitted when `tokenId` token is transferred from `from` to `to`.
\t */
\tevent Transfer(address indexed from, address indexed to, uint256 indexed tokenId);

\t/**
\t * @dev Emitted when `owner` enables `approved` to manage the `tokenId` token.
\t */
\tevent Approval(address indexed owner, address indexed approved, uint256 indexed tokenId);

\t/**
\t * @dev Emitted when `owner` enables or disables (`approved`) `operator` to manage all of its assets.
\t */
\tevent ApprovalForAll(address indexed owner, address indexed operator, bool approved);

\t/**
\t * @dev Returns the number of tokens in ``owner``\u0027s account.
\t */
\tfunction balanceOf(address owner) external view returns (uint256 balance);

\t/**
\t * @dev Returns the owner of the `tokenId` token.
\t *
\t * Requirements:
\t *
\t * - `tokenId` must exist.
\t */
\tfunction ownerOf(uint256 tokenId) external view returns (address owner);

\t/**
\t * @dev Safely transfers `tokenId` token from `from` to `to`, checking first that contract recipients
\t * are aware of the ERC721 protocol to prevent tokens from being forever locked.
\t *
\t * Requirements:
\t *
\t * - `from` cannot be the zero address.
\t * - `to` cannot be the zero address.
\t * - `tokenId` token must exist and be owned by `from`.
\t * - If the caller is not `from`, it must be have been allowed to move this token by either {approve} or {setApprovalForAll}.
\t * - If `to` refers to a smart contract, it must implement {IERC721Receiver-onERC721Received}, which is called upon a safe transfer.
\t *
\t * Emits a {Transfer} event.
\t */
\tfunction safeTransferFrom(address from, address to, uint256 tokenId) external;

\t/**
\t * @dev Transfers `tokenId` token from `from` to `to`.
\t *
\t * WARNING: Usage of this method is discouraged, use {safeTransferFrom} whenever possible.
\t *
\t * Requirements:
\t *
\t * - `from` cannot be the zero address.
\t * - `to` cannot be the zero address.
\t * - `tokenId` token must be owned by `from`.
\t * - If the caller is not `from`, it must be approved to move this token by either {approve} or {setApprovalForAll}.
\t *
\t * Emits a {Transfer} event.
\t */
\tfunction transferFrom(address from, address to, uint256 tokenId) external;

\t/**
\t * @dev Gives permission to `to` to transfer `tokenId` token to another account.
\t * The approval is cleared when the token is transferred.
\t *
\t * Only a single account can be approved at a time, so approving the zero address clears previous approvals.
\t *
\t * Requirements:
\t *
\t * - The caller must own the token or be an approved operator.
\t * - `tokenId` must exist.
\t *
\t * Emits an {Approval} event.
\t */
\tfunction approve(address to, uint256 tokenId) external;

\t/**
\t * @dev Returns the account approved for `tokenId` token.
\t *
\t * Requirements:
\t *
\t * - `tokenId` must exist.
\t */
\tfunction getApproved(uint256 tokenId) external view returns (address operator);

\t/**
\t * @dev Approve or remove `operator` as an operator for the caller.
\t * Operators can call {transferFrom} or {safeTransferFrom} for any token owned by the caller.
\t *
\t * Requirements:
\t *
\t * - The `operator` cannot be the caller.
\t *
\t * Emits an {ApprovalForAll} event.
\t */
\tfunction setApprovalForAll(address operator, bool _approved) external;

\t/**
\t * @dev Returns if the `operator` is allowed to manage all of the assets of `owner`.
\t *
\t * See {setApprovalForAll}
\t */
\tfunction isApprovedForAll(address owner, address operator) external view returns (bool);

\t/**
\t  * @dev Safely transfers `tokenId` token from `from` to `to`.
\t  *
\t  * Requirements:
\t  *
\t  * - `from` cannot be the zero address.
\t  * - `to` cannot be the zero address.
\t  * - `tokenId` token must exist and be owned by `from`.
\t  * - If the caller is not `from`, it must be approved to move this token by either {approve} or {setApprovalForAll}.
\t  * - If `to` refers to a smart contract, it must implement {IERC721Receiver-onERC721Received}, which is called upon a safe transfer.
\t  *
\t  * Emits a {Transfer} event.
\t  */
\tfunction safeTransferFrom(address from, address to, uint256 tokenId, bytes calldata data) external;
}

interface IERC721Enumerable is IERC721 {

\t/**
\t * @dev Returns the total amount of tokens stored by the contract.
\t */
\tfunction totalSupply() external view returns (uint256);

\t/**
\t * @dev Returns a token ID owned by `owner` at a given `index` of its token list.
\t * Use along with {balanceOf} to enumerate all of ``owner``\u0027s tokens.
\t */
\tfunction tokenOfOwnerByIndex(address owner, uint256 index) external view returns (uint256 tokenId);

\t/**
\t * @dev Returns a token ID at a given `index` of all the tokens stored by the contract.
\t * Use along with {totalSupply} to enumerate all tokens.
\t */
\tfunction tokenByIndex(uint256 index) external view returns (uint256);
}

interface IERC721Metadata is IERC721 {

\t/**
\t * @dev Returns the token collection name.
\t */
\tfunction name() external view returns (string memory);

\t/**
\t * @dev Returns the token collection symbol.
\t */
\tfunction symbol() external view returns (string memory);

\t/**
\t * @dev Returns the Uniform Resource Identifier (URI) for `tokenId` token.
\t */
\tfunction tokenURI(uint256 tokenId) external view returns (string memory);
}

contract ERC721 is Context, ERC165, IERC721, IERC721Metadata, IERC721Enumerable {
\tusing SafeMath for uint256;
\tusing Address for address;
\tusing EnumerableSet for EnumerableSet.UintSet;
\tusing EnumerableMap for EnumerableMap.UintToAddressMap;
\tusing Strings for uint256;

\t// Equals to `bytes4(keccak256(\"onERC721Received(address,address,uint256,bytes)\"))`
\t// which can be also obtained as `IERC721Receiver(0).onERC721Received.selector`
\tbytes4 private constant _ERC721_RECEIVED = 0x150b7a02;

\t// Mapping from holder address to their (enumerable) set of owned tokens
\tmapping (address =\u003e EnumerableSet.UintSet) private _holderTokens;

\t// Enumerable mapping from token ids to their owners
\tEnumerableMap.UintToAddressMap private _tokenOwners;

\t// Mapping from token ID to approved address
\tmapping (uint256 =\u003e address) private _tokenApprovals;

\t// Mapping from owner to operator approvals
\tmapping (address =\u003e mapping (address =\u003e bool)) private _operatorApprovals;

\t// Token name
\tstring private _name;

\t// Token symbol
\tstring private _symbol;

\t// Optional mapping for token URIs
\tmapping (uint256 =\u003e string) private _tokenURIs;

\t// Base URI
\tstring private _baseURI;

\t/*
\t *     bytes4(keccak256(\u0027balanceOf(address)\u0027)) == 0x70a08231
\t *     bytes4(keccak256(\u0027ownerOf(uint256)\u0027)) == 0x6352211e
\t *     bytes4(keccak256(\u0027approve(address,uint256)\u0027)) == 0x095ea7b3
\t *     bytes4(keccak256(\u0027getApproved(uint256)\u0027)) == 0x081812fc
\t *     bytes4(keccak256(\u0027setApprovalForAll(address,bool)\u0027)) == 0xa22cb465
\t *     bytes4(keccak256(\u0027isApprovedForAll(address,address)\u0027)) == 0xe985e9c5
\t *     bytes4(keccak256(\u0027transferFrom(address,address,uint256)\u0027)) == 0x23b872dd
\t *     bytes4(keccak256(\u0027safeTransferFrom(address,address,uint256)\u0027)) == 0x42842e0e
\t *     bytes4(keccak256(\u0027safeTransferFrom(address,address,uint256,bytes)\u0027)) == 0xb88d4fde
\t *
\t *     =\u003e 0x70a08231 ^ 0x6352211e ^ 0x095ea7b3 ^ 0x081812fc ^
\t *        0xa22cb465 ^ 0xe985e9c5 ^ 0x23b872dd ^ 0x42842e0e ^ 0xb88d4fde == 0x80ac58cd
\t */
\tbytes4 private constant _INTERFACE_ID_ERC721 = 0x80ac58cd;

\t/*
\t *     bytes4(keccak256(\u0027name()\u0027)) == 0x06fdde03
\t *     bytes4(keccak256(\u0027symbol()\u0027)) == 0x95d89b41
\t *     bytes4(keccak256(\u0027tokenURI(uint256)\u0027)) == 0xc87b56dd
\t *
\t *     =\u003e 0x06fdde03 ^ 0x95d89b41 ^ 0xc87b56dd == 0x5b5e139f
\t */
\tbytes4 private constant _INTERFACE_ID_ERC721_METADATA = 0x5b5e139f;

\t/*
\t *     bytes4(keccak256(\u0027totalSupply()\u0027)) == 0x18160ddd
\t *     bytes4(keccak256(\u0027tokenOfOwnerByIndex(address,uint256)\u0027)) == 0x2f745c59
\t *     bytes4(keccak256(\u0027tokenByIndex(uint256)\u0027)) == 0x4f6ccce7
\t *
\t *     =\u003e 0x18160ddd ^ 0x2f745c59 ^ 0x4f6ccce7 == 0x780e9d63
\t */
\tbytes4 private constant _INTERFACE_ID_ERC721_ENUMERABLE = 0x780e9d63;

\t/**
\t * @dev Initializes the contract by setting a `name` and a `symbol` to the token collection.
\t */
\tconstructor (string memory name_, string memory symbol_) public {
\t\t_name = name_;
\t\t_symbol = symbol_;

\t\t// register the supported interfaces to conform to ERC721 via ERC165
\t\t_registerInterface(_INTERFACE_ID_ERC721);
\t\t_registerInterface(_INTERFACE_ID_ERC721_METADATA);
\t\t_registerInterface(_INTERFACE_ID_ERC721_ENUMERABLE);
\t}

\t/**
\t * @dev See {IERC721-balanceOf}.
\t */
\tfunction balanceOf(address owner) public view virtual override returns (uint256) {
\t\trequire(owner != address(0), \"ERC721: balance query for the zero address\");
\t\treturn _holderTokens[owner].length();
\t}

\t/**
\t * @dev See {IERC721-ownerOf}.
\t */
\tfunction ownerOf(uint256 tokenId) public view virtual override returns (address) {
\t\treturn _tokenOwners.get(tokenId, \"ERC721: owner query for nonexistent token\");
\t}

\t/**
\t * @dev See {IERC721Metadata-name}.
\t */
\tfunction name() public view virtual override returns (string memory) {
\t\treturn _name;
\t}

\t/**
\t * @dev See {IERC721Metadata-symbol}.
\t */
\tfunction symbol() public view virtual override returns (string memory) {
\t\treturn _symbol;
\t}

\t/**
\t * @dev See {IERC721Metadata-tokenURI}.
\t */
\tfunction tokenURI(uint256 tokenId) public view virtual override returns (string memory) {
\t\trequire(_exists(tokenId), \"ERC721Metadata: URI query for nonexistent token\");

\t\tstring memory _tokenURI = _tokenURIs[tokenId];
\t\tstring memory base = baseURI();

\t\t// If there is no base URI, return the token URI.
\t\tif (bytes(base).length == 0) {
\t\t\treturn _tokenURI;
\t\t}
\t\t// If both are set, concatenate the baseURI and tokenURI (via abi.encodePacked).
\t\tif (bytes(_tokenURI).length \u003e 0) {
\t\t\treturn string(abi.encodePacked(base, _tokenURI));
\t\t}
\t\t// If there is a baseURI but no tokenURI, concatenate the tokenID to the baseURI.
\t\treturn string(abi.encodePacked(base, tokenId.toString()));
\t}

\t/**
\t* @dev Returns the base URI set via {_setBaseURI}. This will be
\t* automatically added as a prefix in {tokenURI} to each token\u0027s URI, or
\t* to the token ID if no specific URI is set for that token ID.
\t*/
\tfunction baseURI() public view virtual returns (string memory) {
\t\treturn _baseURI;
\t}

\t/**
\t * @dev See {IERC721Enumerable-tokenOfOwnerByIndex}.
\t */
\tfunction tokenOfOwnerByIndex(address owner, uint256 index) public view virtual override returns (uint256) {
\t\treturn _holderTokens[owner].at(index);
\t}

\t/**
\t * @dev See {IERC721Enumerable-totalSupply}.
\t */
\tfunction totalSupply() public view virtual override returns (uint256) {
\t\t// _tokenOwners are indexed by tokenIds, so .length() returns the number of tokenIds
\t\treturn _tokenOwners.length();
\t}

\t/**
\t * @dev See {IERC721Enumerable-tokenByIndex}.
\t */
\tfunction tokenByIndex(uint256 index) public view virtual override returns (uint256) {
\t\t(uint256 tokenId, ) = _tokenOwners.at(index);
\t\treturn tokenId;
\t}

\t/**
\t * @dev See {IERC721-approve}.
\t */
\tfunction approve(address to, uint256 tokenId) public virtual override {
\t\taddress owner = ERC721.ownerOf(tokenId);
\t\trequire(to != owner, \"ERC721: approval to current owner\");

\t\trequire(_msgSender() == owner || ERC721.isApprovedForAll(owner, _msgSender()),
\t\t\t\"ERC721: approve caller is not owner nor approved for all\"
\t\t);

\t\t_approve(to, tokenId);
\t}

\t/**
\t * @dev See {IERC721-getApproved}.
\t */
\tfunction getApproved(uint256 tokenId) public view virtual override returns (address) {
\t\trequire(_exists(tokenId), \"ERC721: approved query for nonexistent token\");

\t\treturn _tokenApprovals[tokenId];
\t}

\t/**
\t * @dev See {IERC721-setApprovalForAll}.
\t */
\tfunction setApprovalForAll(address operator, bool approved) public virtual override {
\t\trequire(operator != _msgSender(), \"ERC721: approve to caller\");

\t\t_operatorApprovals[_msgSender()][operator] = approved;
\t\temit ApprovalForAll(_msgSender(), operator, approved);
\t}

\t/**
\t * @dev See {IERC721-isApprovedForAll}.
\t */
\tfunction isApprovedForAll(address owner, address operator) public view virtual override returns (bool) {
\t\treturn _operatorApprovals[owner][operator];
\t}

\t/**
\t * @dev See {IERC721-transferFrom}.
\t */
\tfunction transferFrom(address from, address to, uint256 tokenId) public virtual override {
\t\t//solhint-disable-next-line max-line-length
\t\trequire(_isApprovedOrOwner(_msgSender(), tokenId), \"ERC721: transfer caller is not owner nor approved\");

\t\t_transfer(from, to, tokenId);
\t}

\t/**
\t * @dev See {IERC721-safeTransferFrom}.
\t */
\tfunction safeTransferFrom(address from, address to, uint256 tokenId) public virtual override {
\t\tsafeTransferFrom(from, to, tokenId, \"\");
\t}

\t/**
\t * @dev See {IERC721-safeTransferFrom}.
\t */
\tfunction safeTransferFrom(address from, address to, uint256 tokenId, bytes memory _data) public virtual override {
\t\trequire(_isApprovedOrOwner(_msgSender(), tokenId), \"ERC721: transfer caller is not owner nor approved\");
\t\t_safeTransfer(from, to, tokenId, _data);
\t}

\t/**
\t * @dev Safely transfers `tokenId` token from `from` to `to`, checking first that contract recipients
\t * are aware of the ERC721 protocol to prevent tokens from being forever locked.
\t *
\t * `_data` is additional data, it has no specified format and it is sent in call to `to`.
\t *
\t * This internal function is equivalent to {safeTransferFrom}, and can be used to e.g.
\t * implement alternative mechanisms to perform token transfer, such as signature-based.
\t *
\t * Requirements:
\t *
\t * - `from` cannot be the zero address.
\t * - `to` cannot be the zero address.
\t * - `tokenId` token must exist and be owned by `from`.
\t * - If `to` refers to a smart contract, it must implement {IERC721Receiver-onERC721Received}, which is called upon a safe transfer.
\t *
\t * Emits a {Transfer} event.
\t */
\tfunction _safeTransfer(address from, address to, uint256 tokenId, bytes memory _data) internal virtual {
\t\t_transfer(from, to, tokenId);
\t\trequire(_checkOnERC721Received(from, to, tokenId, _data), \"ERC721: transfer to non ERC721Receiver implementer\");
\t}

\t/**
\t * @dev Returns whether `tokenId` exists.
\t *
\t * Tokens can be managed by their owner or approved accounts via {approve} or {setApprovalForAll}.
\t *
\t * Tokens start existing when they are minted (`_mint`),
\t * and stop existing when they are burned (`_burn`).
\t */
\tfunction _exists(uint256 tokenId) internal view virtual returns (bool) {
\t\treturn _tokenOwners.contains(tokenId);
\t}

\t/**
\t * @dev Returns whether `spender` is allowed to manage `tokenId`.
\t *
\t * Requirements:
\t *
\t * - `tokenId` must exist.
\t */
\tfunction _isApprovedOrOwner(address spender, uint256 tokenId) internal view virtual returns (bool) {
\t\trequire(_exists(tokenId), \"ERC721: operator query for nonexistent token\");
\t\taddress owner = ERC721.ownerOf(tokenId);
\t\treturn (spender == owner || getApproved(tokenId) == spender || ERC721.isApprovedForAll(owner, spender));
\t}

\t/**
\t * @dev Safely mints `tokenId` and transfers it to `to`.
\t *
\t * Requirements:
\t d*
\t * - `tokenId` must not exist.
\t * - If `to` refers to a smart contract, it must implement {IERC721Receiver-onERC721Received}, which is called upon a safe transfer.
\t *
\t * Emits a {Transfer} event.
\t */
\tfunction _safeMint(address to, uint256 tokenId) internal virtual {
\t\t_safeMint(to, tokenId, \"\");
\t}

\t/**
\t * @dev Same as {xref-ERC721-_safeMint-address-uint256-}[`_safeMint`], with an additional `data` parameter which is
\t * forwarded in {IERC721Receiver-onERC721Received} to contract recipients.
\t */
\tfunction _safeMint(address to, uint256 tokenId, bytes memory _data) internal virtual {
\t\t_mint(to, tokenId);
\t\trequire(_checkOnERC721Received(address(0), to, tokenId, _data), \"ERC721: transfer to non ERC721Receiver implementer\");
\t}

\t/**
\t * @dev Mints `tokenId` and transfers it to `to`.
\t *
\t * WARNING: Usage of this method is discouraged, use {_safeMint} whenever possible
\t *
\t * Requirements:
\t *
\t * - `tokenId` must not exist.
\t * - `to` cannot be the zero address.
\t *
\t * Emits a {Transfer} event.
\t */
\tfunction _mint(address to, uint256 tokenId) internal virtual {
\t\trequire(to != address(0), \"ERC721: mint to the zero address\");
\t\trequire(!_exists(tokenId), \"ERC721: token already minted\");

\t\t_beforeTokenTransfer(address(0), to, tokenId);

\t\t_holderTokens[to].add(tokenId);

\t\t_tokenOwners.set(tokenId, to);

\t\temit Transfer(address(0), to, tokenId);
\t}

\t/**
\t * @dev Destroys `tokenId`.
\t * The approval is cleared when the token is burned.
\t *
\t * Requirements:
\t *
\t * - `tokenId` must exist.
\t *
\t * Emits a {Transfer} event.
\t */
\tfunction _burn(uint256 tokenId) internal virtual {
\t\taddress owner = ERC721.ownerOf(tokenId); // internal owner

\t\t_beforeTokenTransfer(owner, address(0), tokenId);

\t\t// Clear approvals
\t\t_approve(address(0), tokenId);

\t\t// Clear metadata (if any)
\t\tif (bytes(_tokenURIs[tokenId]).length != 0) {
\t\t\tdelete _tokenURIs[tokenId];
\t\t}

\t\t_holderTokens[owner].remove(tokenId);

\t\t_tokenOwners.remove(tokenId);

\t\temit Transfer(owner, address(0), tokenId);
\t}

\t/**
\t * @dev Transfers `tokenId` from `from` to `to`.
\t *  As opposed to {transferFrom}, this imposes no restrictions on msg.sender.
\t *
\t * Requirements:
\t *
\t * - `to` cannot be the zero address.
\t * - `tokenId` token must be owned by `from`.
\t *
\t * Emits a {Transfer} event.
\t */
\tfunction _transfer(address from, address to, uint256 tokenId) internal virtual {
\t\trequire(ERC721.ownerOf(tokenId) == from, \"ERC721: transfer of token that is not own\"); // internal owner
\t\trequire(to != address(0), \"ERC721: transfer to the zero address\");

\t\t_beforeTokenTransfer(from, to, tokenId);

\t\t// Clear approvals from the previous owner
\t\t_approve(address(0), tokenId);

\t\t_holderTokens[from].remove(tokenId);
\t\t_holderTokens[to].add(tokenId);

\t\t_tokenOwners.set(tokenId, to);

\t\temit Transfer(from, to, tokenId);
\t}

\t/**
\t * @dev Sets `_tokenURI` as the tokenURI of `tokenId`.
\t *
\t * Requirements:
\t *
\t * - `tokenId` must exist.
\t */
\tfunction _setTokenURI(uint256 tokenId, string memory _tokenURI) internal virtual {
\t\trequire(_exists(tokenId), \"ERC721Metadata: URI set of nonexistent token\");
\t\t_tokenURIs[tokenId] = _tokenURI;
\t}

\t/**
\t * @dev Internal function to set the base URI for all token IDs. It is
\t * automatically added as a prefix to the value returned in {tokenURI},
\t * or to the token ID if {tokenURI} is empty.
\t */
\tfunction _setBaseURI(string memory baseURI_) internal virtual {
\t\t_baseURI = baseURI_;
\t}

\t/**
\t * @dev Internal function to invoke {IERC721Receiver-onERC721Received} on a target address.
\t * The call is not executed if the target address is not a contract.
\t *
\t * @param from address representing the previous owner of the given token ID
\t * @param to target address that will receive the tokens
\t * @param tokenId uint256 ID of the token to be transferred
\t * @param _data bytes optional data to send along with the call
\t * @return bool whether the call correctly returned the expected magic value
\t */
\tfunction _checkOnERC721Received(address from, address to, uint256 tokenId, bytes memory _data)
\tprivate returns (bool)
\t{
\t\tif (!to.isContract()) {
\t\t\treturn true;
\t\t}
\t\tbytes memory returndata = to.functionCall(abi.encodeWithSelector(
\t\t\t\tIERC721Receiver(to).onERC721Received.selector,
\t\t\t\t_msgSender(),
\t\t\t\tfrom,
\t\t\t\ttokenId,
\t\t\t\t_data
\t\t\t), \"ERC721: transfer to non ERC721Receiver implementer\");
\t\tbytes4 retval = abi.decode(returndata, (bytes4));
\t\treturn (retval == _ERC721_RECEIVED);
\t}

\t/**
\t * @dev Approve `to` to operate on `tokenId`
\t *
\t * Emits an {Approval} event.
\t */
\tfunction _approve(address to, uint256 tokenId) internal virtual {
\t\t_tokenApprovals[tokenId] = to;
\t\temit Approval(ERC721.ownerOf(tokenId), to, tokenId); // internal owner
\t}

\t/**
\t * @dev Hook that is called before any token transfer. This includes minting
\t * and burning.
\t *
\t * Calling conditions:
\t *
\t * - When `from` and `to` are both non-zero, ``from``\u0027s `tokenId` will be
\t * transferred to `to`.
\t * - When `from` is zero, `tokenId` will be minted for `to`.
\t * - When `to` is zero, ``from``\u0027s `tokenId` will be burned.
\t * - `from` cannot be the zero address.
\t * - `to` cannot be the zero address.
\t *
\t * To learn more about hooks, head to xref:ROOT:extending-contracts.adoc#using-hooks[Using Hooks].
\t */
\tfunction _beforeTokenTransfer(address from, address to, uint256 tokenId) internal virtual { }
}"},"NFT.sol":{"content":"pragma solidity \u003e=0.6.0 \u003c0.8.0;
pragma experimental ABIEncoderV2;

//SPDX-License-Identifier: UNLICENSED

import \"./ERC721.sol\";

contract Pet is ERC721, ReentrancyGuard, Ownable {
    using SafeMath for uint256;

    constructor() ERC721(\"Pets (for Adventurers)\", \"PET\") {}
    address public lootAddress = 0xFF9C1b15B16263C61d017ee9F65C50e4AE0113D7;
    LootInterface lootContract = LootInterface(lootAddress);

    string public PROVENANCE = \"\";

    uint256 public maxSupply = 3900;
    uint256 public currentSupply = 0;


    uint256 public lootersPrice = 20000000000000000; // 0.02 ETH
    uint256 public publicPrice = 80000000000000000; // 0.08 ETH

    string[] private Type = [
    \"Goblin\", \"Goblin\", \"Goblin\", \"Goblin\", \"Goblin\", \"Goblin\",
    \"Python\", \"Python\", \"Python\", \"Python\",
    \"Werewolf\", \"Werewolf\",
    \"Lion\", \"Lion\",
    \"Minotaur\", \"Minotaur\",
    \"Phoenix\",
    \"Ghost\", \"Ghost\",
    \"Griffin\",
    \"Raven\", \"Raven\",
    \"Hydra\",
    \"Dragon\",
    \"Imp\", \"Imp\", \"Imp\", \"Imp\",
    \"Ghoul\", \"Ghoul\",
    \"Fairy\",
    \"Gnome\", \"Gnome\", \"Gnome\", \"Gnome\", \"Gnome\",
    \"Troll\", \"Troll\", \"Troll\", \"Troll\",
    \"Sea serpent\",
    \"Yeti\",
    \"Mermaid\", \"Mermaid\",
    \"Bat\", \"Bat\", \"Bat\",
    \"Ogre\", \"Ogre\",
    \"Spider\", \"Spider\", \"Spider\", \"Spider\", \"Spider\",
    \"Golem\",
    \"Turtle\", \"Turtle\",
    \"Cerberus\",
    \"Harpy\", \"Harpy\",
    \"Kraken\",
    \"Nian\",
    \"Owl\", \"Owl\", \"Owl\",
    \"Zouwu\",
    \"Ape\",
    \"Cow\", \"Cow\", \"Cow\", \"Cow\", \"Cow\", \"Cow\", \"Cow\",
    \"Donkey\", \"Donkey\", \"Donkey\", \"Donkey\", \"Donkey\",
    \"Crab\", \"Crab\", \"Crab\",
    \"Sea lion\", \"Sea lion\",
    \"Toad\", \"Toad\", \"Toad\", \"Toad\",
    \"Lizard\", \"Lizard\", \"Lizard\", \"Lizard\",
    \"Wisp\",
    \"Panda\", \"Panda\",
    \"Jellyfish\", \"Jellyfish\"
    ];

    string[] private rarities = [
    \"Mythical\",
    \"Legendary\", \"Legendary\",
    \"Epic\", \"Epic\", \"Epic\", \"Epic\",
    \"Rare\", \"Rare\", \"Rare\", \"Rare\", \"Rare\", \"Rare\", \"Rare\", \"Rare\",
    \"Common\", \"Common\", \"Common\", \"Common\", \"Common\", \"Common\", \"Common\", \"Common\", \"Common\", \"Common\", \"Common\", \"Common\", \"Common\", \"Common\", \"Common\", \"Common\", \"Common\", \"Common\", \"Common\", \"Common\", \"Common\", \"Common\", \"Common\"
    ];

    string[] private moods = [
    \"Gloomy\", \"Gloomy\", \"Gloomy\", \"Gloomy\",
    \"Bossy\",
    \"Curious\", \"Curious\",
    \"Whiny\", \"Whiny\", \"Whiny\", \"Whiny\", \"Whiny\", \"Whiny\",
    \"Impulsive\", \"Impulsive\",
    \"Thorough\", \"Thorough\", \"Thorough\",
    \"Liar\", \"Liar\",
    \"Shameless\",
    \"Imaginative\",
    \"Harsh\", \"Harsh\", \"Harsh\", \"Harsh\",
    \"Forgiving\", \"Forgiving\", \"Forgiving\", \"Forgiving\",
    \"Proud\",
    \"Listless\", \"Listless\", \"Listless\", \"Listless\", \"Listless\", \"Listless\",
    \"Humorous\", \"Humorous\",
    \"Sadistic\",
    \"Generous\", \"Generous\", \"Generous\", \"Generous\",
    \"Irreligious\",
    \"Solitary\", \"Solitary\", \"Solitary\", \"Solitary\",
    \"Aggressive\",
    \"Friendly\",
    \"Calm\"
    ];


    function getType(uint256 tokenId) public view returns (string memory) {
        if (tokenId == 1) {
            return \"Pegasus\";
        }
        if (tokenId == maxSupply - 1) {
            return \"Demon\";
        }
        return pluck(tokenId, \"Type\", Type);
    }

    function getRarity(uint256 tokenId) public view returns (string memory) {
        if (tokenId == 1) {
            return \"Godly\";
        }
        if (tokenId == maxSupply - 1) {
            return \"Hellish\";
        }
        return pluck(tokenId, \"Rarity\", rarities);
    }

    function getMood(uint256 tokenId) public view returns (string memory) {
        if (tokenId == 1 || tokenId == maxSupply - 1) {
            return \"Charismatic\";
        }
        return pluck(tokenId, \"Mood\", moods);
    }

    function random(string memory input) public pure returns (uint256) {
        return uint256(keccak256(abi.encodePacked(input))) % 31;
    }

    function pluck(
        uint256 tokenId,
        string memory keyPrefix,
        string[] memory sourceArray
    ) internal view returns (string memory) {
        uint256 rand = random(
            string(abi.encodePacked(keyPrefix, toString(tokenId)))
        );

        string memory output = sourceArray[rand % sourceArray.length];

        return output;
    }

    function withdraw() public onlyOwner {
        uint balance = address(this).balance;
        msg.sender.transfer(balance);
    }

    function deposit() public payable onlyOwner {}


    function setLootersPrice(uint256 newPrice) public onlyOwner {
        lootersPrice = newPrice;
    }

    function setPublicPrice(uint256 newPrice) public onlyOwner {
        publicPrice = newPrice;
    }

    function setBaseURI(string memory baseURI) public onlyOwner {
        _setBaseURI(baseURI);
    }

    function setProvenance(string memory prov) public onlyOwner {
        PROVENANCE = prov;
    }

    // Loot owners minting
    function mintWithLoot(uint lootId) public payable nonReentrant {
        require(lootContract.ownerOf(lootId) == msg.sender, \"This Loot is not owned by the minter\");
        require(lootersPrice \u003c= msg.value, \"Not enough Ether sent\");
        require(currentSupply \u003c maxSupply, \"All pets are minted\");
        _safeMint(msg.sender, currentSupply);
        currentSupply += 1;
    }

    // Public minting
    function mint() public payable nonReentrant {
        require(publicPrice \u003c= msg.value, \"Not enough Ether sent\");
        require(currentSupply \u003c maxSupply, \"All pets are minted\");

        _safeMint(msg.sender, currentSupply);
        currentSupply += 1;
    }

    function toString(uint256 value) internal pure returns (string memory) {
        if (value == 0) {
            return \"0\";
        }
        uint256 temp = value;
        uint256 digits;
        while (temp != 0) {
            digits++;
            temp /= 10;
        }
        bytes memory buffer = new bytes(digits);
        while (value != 0) {
            digits -= 1;
            buffer[digits] = bytes1(uint8(48 + uint256(value % 10)));
            value /= 10;
        }
        return string(buffer);
    }

    function tokenURI(uint256 tokenId) override public view returns (string memory) {
        string memory rarity = getRarity(tokenId);

        string memory info = \" font-family: serif; font-size: 14px; \";
        string[7] memory parts;
        parts[0] = string(abi.encodePacked(\u0027\u003csvg xmlns=\"http://www.w3.org/2000/svg\" preserveAspectRatio=\"xMinYMin meet\" viewBox=\"0 0 350 350\"\u003e\u003cstyle\u003e.Godly { fill: white;\u0027, info, \u0027}.Hellish { fill: white;\u0027, info, \u0027}.Mythical { fill: #e8b23d; \u0027, info, \u0027}.Legendary { fill: #e8b23d;\u0027, info, \u0027 }.Epic { fill: #b935d2;\u0027, info, abi.encodePacked(\u0027 }.Rare { fill: #6699cc;\u0027, info, \u0027}.Common { fill: #6a7b7e;\u0027, info, \u0027}\u0027, \u0027.base { fill: white;\u0027, info, \u0027},\u003c/style\u003e\u003crect width=\"100%\" height=\"100%\" fill=\"black\" /\u003e\u003ctext x=\"10\" y=\"20\" class=\"\u0027, rarity, \u0027\"\u003e\u0027)));

        parts[1] = rarity;

        parts[2] = \u0027\u003c/text\u003e\u003ctext x=\"10\" y=\"40\" class=\"base\"\u003e\u0027;

        parts[3] = getType(tokenId);

        parts[4] = \u0027\u003c/text\u003e\u003ctext x=\"10\" y=\"60\" class=\"base\"\u003e\u0027;

        parts[5] = getMood(tokenId);

        parts[6] = \u0027\u003c/text\u003e\u003c/svg\u003e\u0027;

        string memory output = string(abi.encodePacked(parts[0], parts[1], parts[2], parts[3], parts[4], parts[5], parts[6]));
        //output = string(abi.encodePacked(output, parts[9], parts[10], parts[11], parts[12], parts[13], parts[14], parts[15], parts[16]));

        string memory json = Base64.encode(bytes(string(abi.encodePacked(\u0027{\"name\": \"Pet #\u0027, toString(tokenId), \u0027\", \"description\": \"Pets are randomized adventurer gear generated and stored on chain. Stats, images, and other functionality are intentionally omitted for others to interpret. Feel free to use Pets in any way you want.\", \"image\": \"data:image/svg+xml;base64,\u0027, Base64.encode(bytes(output)), \u0027\",\"attributes\":[{\"trait_type\":\"Rarity\",\"value\":\"\u0027, parts[1], \u0027\"},{\"trait_type\":\"Type\",\"value\": \"\u0027, parts[3], \u0027\"},{\"trait_type\": \"Mood\",\"value\": \"\u0027, parts[5], \u0027\"}]}\u0027))));
        output = string(abi.encodePacked(\u0027data:application/json;base64,\u0027, json));

        return output;
    }
}
