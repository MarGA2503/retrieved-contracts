// SPDX-License-Identifier: GPL-3.0
pragma solidity \u003e0.6.99 \u003c0.8.0;

contract Wallet {

\taddress public architect;
\taddress public owner;
\taddress public relayer;
\tuint public unlockStartDate;
\tuint public unlockEndDate;
\tuint createdAt;
\tuint iterations;
\tuint latestETHClaim = 0;
\tuint latestTokenClaim = 0;

\tevent Received(address from, uint amount);
\tevent ClaimedETH(address to, uint amount);
\tevent ClaimedToken(address tokenContract, address to, uint amount);

\tmodifier onlyAllowed {
\t\trequire(msg.sender == owner || msg.sender == relayer, \"Not allowed.\");
\t\t_;
\t}

\tconstructor(
\t\taddress _architect,
\t\taddress _owner,
\t\taddress _relayer,
\t\tuint _iterations,
\t\tuint _unlockStartDate,
\t\tuint _unlockEndDate
\t)
\t\tpayable
  {
\t\trequire(_iterations \u003e 0 \u0026\u0026 _unlockStartDate \u003e= block.timestamp \u0026\u0026 _unlockEndDate \u003e= _unlockStartDate, \"Wrong parameters.\");
\t\tarchitect = _architect;
\t\towner = _owner;
\t\trelayer = _relayer;
\t\titerations = _iterations;
\t\tunlockStartDate = _unlockStartDate;
\t\tunlockEndDate = _unlockEndDate;
\t\tcreatedAt = block.timestamp;
\t}

\treceive ()
\t\texternal
\t\tpayable
\t{
    emit Received(msg.sender, msg.value);
  }

\tfunction info()
\t\tpublic
\t\tview
\t\treturns(address, address, uint, uint, uint, uint, uint, uint, uint, uint)
\t{
\t  return (architect, owner, createdAt, unlockStartDate, unlockEndDate, iterations, currentIteration(), latestTokenClaim, latestETHClaim, address(this).balance);
\t}

\tfunction currentIteration()
\t\tprivate
\t\tview
\t\treturns (uint)
\t{
\t\tif(block.timestamp \u003e= unlockEndDate) {
\t\t\treturn iterations;
\t\t} else if(block.timestamp \u003e= unlockStartDate) {
\t\t\tuint i = iterations * (block.timestamp - unlockStartDate) / (unlockEndDate - unlockStartDate) + 1;
\t\t\tif(i \u003e iterations) {
\t\t\t\treturn iterations;
\t\t\t} else {
\t\t\t\treturn i;
\t\t\t}
\t\t} else {
\t\t\treturn 0;
\t\t}
\t}

\tfunction claim(address _tokenContract) onlyAllowed public {
\t\trequire(block.timestamp \u003e= unlockStartDate, \"Asset cannot be unlocked yet.\");
\t\tif(address(0) == _tokenContract) {
\t\t\tclaimETH();
\t\t} else {
\t\t\tclaimToken(_tokenContract);
\t\t}
\t}

\tfunction claimETH() private {
\t\trequire(latestETHClaim \u003e= iterations || latestETHClaim \u003c currentIteration(), \"ETH cannot be unlocked yet.\");
\t\tuint amount = address(this).balance;
\t\tif(block.timestamp \u003c unlockEndDate \u0026\u0026 latestETHClaim \u003c iterations) {
\t\t\tamount = amount / (iterations - latestETHClaim);
\t\t\tlatestETHClaim++;
\t\t}
\t\tpayable(owner).transfer(amount);
    emit ClaimedETH(owner, amount);
  }

  function claimToken(address _tokenContract) private {
\t\trequire(latestTokenClaim \u003e= iterations || latestTokenClaim \u003c currentIteration(), \"Token cannot be unlocked yet.\");
\t\tIERC20 token = IERC20(_tokenContract);
\t\tuint amount = token.balanceOf(address(this));
\t\tif(block.timestamp \u003c unlockEndDate \u0026\u0026 latestTokenClaim \u003c iterations) {
\t\t\tamount = amount / (iterations - latestTokenClaim);
\t\t\tlatestTokenClaim++;
\t\t}
    token.transfer(owner, amount);
    emit ClaimedToken(_tokenContract, owner, amount);
  }
}

interface IERC20 {
  function totalSupply() external view returns (uint);
  function balanceOf(address account) external view returns (uint);
  function transfer(address recipient, uint amount) external returns (bool);
  function allowance(address owner, address spender) external view returns (uint);
  function approve(address spender, uint amount) external returns (bool);
  function transferFrom(address sender, address recipient, uint amount) external returns (bool);

  event Transfer(address indexed from, address indexed to, uint value);
  event Approval(address indexed owner, address indexed spender, uint value);
}
"},"WalletFactory.sol":{"content":"// SPDX-License-Identifier: GPL-3.0
pragma solidity \u003e0.6.99 \u003c0.8.0;
import \"./Wallet.sol\";
contract WalletFactory {

\tmapping(address =\u003e address[]) wallets;

\tevent Created(address wallet, address from, address to, uint iterations, uint unlockStartDate, uint unlockEndDate);

\tfunction getWallets(address _user)
  \tpublic
  \tview
  returns(address[] memory)
  {
  \treturn wallets[_user];
\t}

\tfunction newWallet(address _owner, address _relayer, uint _iterations, uint _unlockStartDate, uint _unlockEndDate)
\t\tpublic
\t\tpayable
\t{
\t\taddress wallet = address(new Wallet(msg.sender, _owner, _relayer, _iterations, _unlockStartDate, _unlockEndDate));
    wallets[msg.sender].push(wallet);

    if(msg.sender != _owner){
      wallets[_owner].push(wallet);
    }

\t\tpayable(wallet).transfer(msg.value);
\t\temit Created(wallet, msg.sender, _owner, _iterations, _unlockStartDate, _unlockEndDate);
\t}
}

