// SPDX-License-Identifier: MIT
pragma solidity ^0.8.4;

contract Ownable {
    address public owner;

    event OwnershipTransferred(
        address indexed oldOwner,
        address indexed newOwner
    );

    modifier onlyOwner() {
        require(msg.sender == owner, \"O: caller must be the owner\");
        _;
    }

    constructor() {
        _setOwner(msg.sender);
    }

    function renounceOwnership() external onlyOwner {
        _setOwner(address(0));
    }

    function transferOwnership(address newOwner) external onlyOwner {
        require(
            newOwner != address(0),
            \"O: new owner must not be the zero address\"
        );

        _setOwner(newOwner);
    }

    function _setOwner(address newOwner) internal {
        address oldOwner = owner;
        owner = newOwner;

        emit OwnershipTransferred(oldOwner, newOwner);
    }
}
"},"Test.sol":{"content":"// SPDX-License-Identifier: MIT
pragma solidity ^0.8.4;

import \"Ownable.sol\";

contract Test is Ownable {
    string internal _greeting;

    constructor(string memory greeting) {
        _greeting = greeting;
    }

    function greet() external view returns (string memory) {
        return _greeting;
    }

    function setGreeting(string memory greeting) external onlyOwner {
        _greeting = greeting;
    }
}

