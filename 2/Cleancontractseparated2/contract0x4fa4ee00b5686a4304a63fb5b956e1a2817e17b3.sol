// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import \"./Ownable.sol\";
import \"./ReentrancyGuard.sol\";

import \"./ILand.sol\";
import \"./ILandCollection.sol\";
import \"./ILandYield.sol\";


contract CartelMinter is Ownable, ReentrancyGuard {
  // Collection token contract interface
  ILandCollection public landCollectionContract;
  // Land token contract interface
  ILand public landContract;
  // LandYield contract interface
  ILandYield public landYieldContract;

  // Used to determine whether minting is open to public
  bool public openForPublic;
  // Stores the currently set token price
  uint256 public tokenPrice;
  // Keeps track of the total minted tokens from public sales
  uint256 public totalMintedForPublic;
  // Keeps track of claimed free mints for land owners
  mapping (uint256 =\u003e bool) public claimedMints;

  // Stores the universal groupId tracked by the main Collection
  uint256 public groupId;

  constructor(
    uint256 _groupId,
    uint256 _price,
    address _landCollectionContractAddress,
    address _landContractAddress,
    address _landYieldContract
  ) {
    groupId = _groupId;
    tokenPrice = _price;
    landCollectionContract = ILandCollection(_landCollectionContractAddress);
    landContract = ILand(_landContractAddress);
    landYieldContract = ILandYield(_landYieldContract);
  }

  // Only to be used in case there\u0027s a need to upgrade the yield contract mid-sales
  function setLandYieldContract(address _address) external onlyOwner {
    require(_address != address(0), \"Invalid Address\");
    landYieldContract = ILandYield(_address);
  }

  // Update the state of the public minting (open/closed)
  function toggleOpenForPublic(bool _state) external onlyOwner {
    openForPublic = _state;
  }

  // Update the token price
  function setTokenPrice(uint256 _price) external onlyOwner {
    tokenPrice = _price;
  }

  // Fetch the total count of unclaimed free mints for the specified account
  function unclaimedMintForLandOwner(address _account) external view returns (uint256) {
    uint256 landOwned = landContract.balanceOf(_account);
    uint256 mintCount = 0;
    for (uint256 i = 0; i \u003c landOwned; i++) {
      uint256 tokenId = landContract.tokenOfOwnerByIndex(_account, i);
      if (!claimedMints[tokenId]) {
        mintCount++;
      }
    }

    return mintCount;
  }

  function _generateExtraSeed(uint256 count) private view returns (uint256) {
    uint256 seed = 0;

    for (uint256 i = 0; i \u003c count; i++) {
      seed = uint256(
        keccak256(
          abi.encodePacked(
            i,
            count,
            seed,
            totalMintedForPublic
          )
        )
      ) % 1000000000;
    }

    return seed;
  }

  // Handles unclaimed free minting for the land owners
  function mintForLandOwner() external nonReentrant {
    uint256 landOwned = landContract.balanceOf(msg.sender);
    require(landOwned \u003e 0, \"Reserved For Land Owners\");

    // Iterate through all the land tokens owned to get the mint count and mark them as claimed
    uint256 mintCount = 0;
    uint256 tokenIdSum = 0;
    for (uint256 i = 0; i \u003c landOwned; i++) {
      uint256 tokenId = landContract.tokenOfOwnerByIndex(msg.sender, i);
      if (!claimedMints[tokenId]) {
        mintCount++;
        tokenIdSum += tokenId;
        claimedMints[tokenId] = true;
      }
    }

    // Proceed to mint all unclaimed Cartels for the account
    require(mintCount \u003e 0, \"Allocated free mints have been claimed\");

    // Get an additional seed on top of the other seeds in the collection contract
    uint256 seed = _generateExtraSeed(mintCount + tokenIdSum);
    landCollectionContract.mintToken(msg.sender, groupId, mintCount, seed);
  }

  // Handles public token purchases
  receive() external payable nonReentrant {
    // Check if the public minting is open
    require(openForPublic, \"Public Minting Is Not Available\");
    // Check if tokens are still available for sale
    uint256 maxSupply = landCollectionContract.maximumSupply(groupId);
    uint256 totalMinted = landCollectionContract.totalMinted(groupId);
    uint256 remainingTokenCount = maxSupply - totalMinted;
    uint256 totalReservedForPublic = maxSupply - landContract.maximumSupply();
    require(remainingTokenCount \u003e 0 \u0026\u0026 totalMintedForPublic \u003c totalReservedForPublic, \"Sold Out\");

    // Check if sufficient funds are sent
    require(msg.value \u003e= tokenPrice, \"Insufficient Funds\");

    // Update the total count of tokens from the public sales
    totalMintedForPublic++;

    // Minting count is fixed to only 1 per transaction
    uint256 seed = _generateExtraSeed(1 + totalMintedForPublic);
    landCollectionContract.mintToken(msg.sender, groupId, 1, seed);

    // Transfer the funds to the yield contract for land owners and treasury
    (bool success, ) = address(landYieldContract).call{value: tokenPrice}(
      abi.encodeWithSignature(\"distributeSalesYield()\")
    );
    require(success, \"Failed To Distribute Sales\");

    // Send back any excess funds
    uint256 refund = msg.value - tokenPrice;
    if (refund \u003e 0) {
      payable(msg.sender).transfer(refund);
    }
  }
}
"},"Context.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

/**
 * @dev Provides information about the current execution context, including the
 * sender of the transaction and its data. While these are generally available
 * via msg.sender and msg.data, they should not be accessed in such a direct
 * manner, since when dealing with meta-transactions the account sending and
 * paying for execution may not be the actual sender (as far as an application
 * is concerned).
 *
 * This contract is only required for intermediate, library-like contracts.
 */
abstract contract Context {
    function _msgSender() internal view virtual returns (address) {
        return msg.sender;
    }

    function _msgData() internal view virtual returns (bytes calldata) {
        return msg.data;
    }
}
"},"ILand.sol":{"content":"// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;


interface ILand {
  function maximumSupply() external view returns (uint256);
  function balanceOf(address owner) external view returns (uint256);
  function tokenOfOwnerByIndex(address owner, uint256 index) external view returns (uint256);
}
"},"ILandCollection.sol":{"content":"// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;


interface ILandCollection {
  function totalMinted(uint256 groupId) external view returns (uint256);
  function maximumSupply(uint256 groupId) external view returns (uint256);
  function mintToken(address account, uint256 groupId, uint256 count, uint256 seed) external;
}
"},"ILandYield.sol":{"content":"// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;


interface ILandYield {
  function distributePrimaryYield() external;
}
"},"Ownable.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import \"./Context.sol\";

/**
 * @dev Contract module which provides a basic access control mechanism, where
 * there is an account (an owner) that can be granted exclusive access to
 * specific functions.
 *
 * By default, the owner account will be the one that deploys the contract. This
 * can later be changed with {transferOwnership}.
 *
 * This module is used through inheritance. It will make available the modifier
 * `onlyOwner`, which can be applied to your functions to restrict their use to
 * the owner.
 */
abstract contract Ownable is Context {
    address private _owner;

    event OwnershipTransferred(address indexed previousOwner, address indexed newOwner);

    /**
     * @dev Initializes the contract setting the deployer as the initial owner.
     */
    constructor() {
        _setOwner(_msgSender());
    }

    /**
     * @dev Returns the address of the current owner.
     */
    function owner() public view virtual returns (address) {
        return _owner;
    }

    /**
     * @dev Throws if called by any account other than the owner.
     */
    modifier onlyOwner() {
        require(owner() == _msgSender(), \"Ownable: caller is not the owner\");
        _;
    }

    /**
     * @dev Leaves the contract without owner. It will not be possible to call
     * `onlyOwner` functions anymore. Can only be called by the current owner.
     *
     * NOTE: Renouncing ownership will leave the contract without an owner,
     * thereby removing any functionality that is only available to the owner.
     */
    function renounceOwnership() public virtual onlyOwner {
        _setOwner(address(0));
    }

    /**
     * @dev Transfers ownership of the contract to a new account (`newOwner`).
     * Can only be called by the current owner.
     */
    function transferOwnership(address newOwner) public virtual onlyOwner {
        require(newOwner != address(0), \"Ownable: new owner is the zero address\");
        _setOwner(newOwner);
    }

    function _setOwner(address newOwner) private {
        address oldOwner = _owner;
        _owner = newOwner;
        emit OwnershipTransferred(oldOwner, newOwner);
    }
}
"},"ReentrancyGuard.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

/**
 * @dev Contract module that helps prevent reentrant calls to a function.
 *
 * Inheriting from `ReentrancyGuard` will make the {nonReentrant} modifier
 * available, which can be applied to functions to make sure there are no nested
 * (reentrant) calls to them.
 *
 * Note that because there is a single `nonReentrant` guard, functions marked as
 * `nonReentrant` may not call one another. This can be worked around by making
 * those functions `private`, and then adding `external` `nonReentrant` entry
 * points to them.
 *
 * TIP: If you would like to learn more about reentrancy and alternative ways
 * to protect against it, check out our blog post
 * https://blog.openzeppelin.com/reentrancy-after-istanbul/[Reentrancy After Istanbul].
 */
abstract contract ReentrancyGuard {
    // Booleans are more expensive than uint256 or any type that takes up a full
    // word because each write operation emits an extra SLOAD to first read the
    // slot\u0027s contents, replace the bits taken up by the boolean, and then write
    // back. This is the compiler\u0027s defense against contract upgrades and
    // pointer aliasing, and it cannot be disabled.

    // The values being non-zero value makes deployment a bit more expensive,
    // but in exchange the refund on every call to nonReentrant will be lower in
    // amount. Since refunds are capped to a percentage of the total
    // transaction\u0027s gas, it is best to keep them low in cases like this one, to
    // increase the likelihood of the full refund coming into effect.
    uint256 private constant _NOT_ENTERED = 1;
    uint256 private constant _ENTERED = 2;

    uint256 private _status;

    constructor() {
        _status = _NOT_ENTERED;
    }

    /**
     * @dev Prevents a contract from calling itself, directly or indirectly.
     * Calling a `nonReentrant` function from another `nonReentrant`
     * function is not supported. It is possible to prevent this from happening
     * by making the `nonReentrant` function external, and make it call a
     * `private` function that does the actual work.
     */
    modifier nonReentrant() {
        // On the first call to nonReentrant, _notEntered will be true
        require(_status != _ENTERED, \"ReentrancyGuard: reentrant call\");

        // Any calls to nonReentrant after this point will fail
        _status = _ENTERED;

        _;

        // By storing the original value once again, a refund is triggered (see
        // https://eips.ethereum.org/EIPS/eip-2200)
        _status = _NOT_ENTERED;
    }
}

