pragma solidity \u003e=0.4.22 \u003c0.6.0;

// ----------------------------------------------------------------------------
// Safe maths
// ----------------------------------------------------------------------------
library SafeMath {
    function add(uint a, uint b) internal pure returns (uint c) {
        c = a + b;
        require(c \u003e= a);
    }
    function sub(uint a, uint b) internal pure returns (uint c) {
        require(b \u003c= a);
        c = a - b;
    }
    function mul(uint a, uint b) internal pure returns (uint c) {
        c = a * b;
        require(a == 0 || c / a == b);
    }
    function div(uint a, uint b) internal pure returns (uint c) {
        require(b \u003e 0);
        c = a / b;
    }
}


// ----------------------------------------------------------------------------
// ERC Token Standard #20 Interface
// https://github.com/ethereum/EIPs/blob/master/EIPS/eip-20-token-standard.md
// ----------------------------------------------------------------------------
contract ERC20Interface {
    function totalSupply() public constant returns (uint);
    function balanceOf(address tokenOwner) public constant returns (uint balance);
    function allowance(address tokenOwner, address spender) public constant returns (uint remaining);
    function transfer(address to, uint tokens) public returns (bool success);
    function approve(address spender, uint tokens) public returns (bool success);
    function transferFrom(address from, address to, uint tokens) public returns (bool success);

    event Transfer(address indexed from, address indexed to, uint tokens);
    event Approval(address indexed tokenOwner, address indexed spender, uint tokens);
}


// ----------------------------------------------------------------------------
// Contract function to receive approval and execute function in one call
// ----------------------------------------------------------------------------
contract ApproveAndCallFallBack {
    function receiveApproval(address from, uint256 tokens, address token, bytes data) public;
}


// ----------------------------------------------------------------------------
// Owned contract
// ----------------------------------------------------------------------------
contract Owned {
    address public owner;
    address public newOwner;

    event OwnershipTransferred(address indexed _from, address indexed _to);

    function Owned() public {
        owner = msg.sender;
    }

    modifier onlyOwner {
        require(msg.sender == owner);
        _;
    }

    function transferOwnership(address _newOwner) public onlyOwner {
        newOwner = _newOwner;
    }
    function acceptOwnership() public {
        require(msg.sender == newOwner);
        OwnershipTransferred(owner, newOwner);
        owner = newOwner;
        newOwner = address(0);
    }
}


// ----------------------------------------------------------------------------
// ERC20 Token, with the addition of symbol, name and decimals and an
// initial fixed supply
// ----------------------------------------------------------------------------
contract ERC20Token is ERC20Interface, Owned {
    using SafeMath for uint;

    string public symbol;
    string public  name;
    uint8 public decimals;
    uint public _totalSupply;

    mapping(address =\u003e uint) balances;
    mapping(address =\u003e mapping(address =\u003e uint)) allowed;

    // ------------------------------------------------------------------------
    // Constructor
    // ------------------------------------------------------------------------
    function ERC20Token() public {
  
    }

    // ------------------------------------------------------------------------
    // Total supply
    // ------------------------------------------------------------------------
    function totalSupply() public constant returns (uint) {
        return _totalSupply;
    }


    // ------------------------------------------------------------------------
    // Get the token balance for account `tokenOwner`
    // ------------------------------------------------------------------------
    function balanceOf(address tokenOwner) public constant returns (uint balance) {
        return balances[tokenOwner];
    }


    // ------------------------------------------------------------------------
    // Transfer the balance from token owner\u0027s account to `to` account
    // - Owner\u0027s account must have sufficient balance to transfer
    // - 0 value transfers are allowed
    // ------------------------------------------------------------------------
    function transfer(address to, uint tokens) public returns (bool success) {
        balances[msg.sender] = balances[msg.sender].sub(tokens);
        balances[to] = balances[to].add(tokens);
        Transfer(msg.sender, to, tokens);
        return true;
    }


    // ------------------------------------------------------------------------
    // Token owner can approve for `spender` to transferFrom(...) `tokens`
    // from the token owner\u0027s account
    //
    // https://github.com/ethereum/EIPs/blob/master/EIPS/eip-20-token-standard.md
    // recommends that there are no checks for the approval double-spend attack
    // as this should be implemented in user interfaces 
    // ------------------------------------------------------------------------
    function approve(address spender, uint tokens) public returns (bool success) {
        allowed[msg.sender][spender] = tokens;
        Approval(msg.sender, spender, tokens);
        return true;
    }


    // ------------------------------------------------------------------------
    // Transfer `tokens` from the `from` account to the `to` account
    // 
    // The calling account must already have sufficient tokens approve(...)-d
    // for spending from the `from` account and
    // - From account must have sufficient balance to transfer
    // - Spender must have sufficient allowance to transfer
    // - 0 value transfers are allowed
    // ------------------------------------------------------------------------
    function transferFrom(address from, address to, uint tokens) public returns (bool success) {
        balances[from] = balances[from].sub(tokens);
        allowed[from][msg.sender] = allowed[from][msg.sender].sub(tokens);
        balances[to] = balances[to].add(tokens);
        Transfer(from, to, tokens);
        return true;
    }


    // ------------------------------------------------------------------------
    // Returns the amount of tokens approved by the owner that can be
    // transferred to the spender\u0027s account
    // ------------------------------------------------------------------------
    function allowance(address tokenOwner, address spender) public constant returns (uint remaining) {
        return allowed[tokenOwner][spender];
    }


    // ------------------------------------------------------------------------
    // Token owner can approve for `spender` to transferFrom(...) `tokens`
    // from the token owner\u0027s account. The `spender` contract function
    // `receiveApproval(...)` is then executed
    // ------------------------------------------------------------------------
    function approveAndCall(address spender, uint tokens, bytes data) public returns (bool success) {
        allowed[msg.sender][spender] = tokens;
        Approval(msg.sender, spender, tokens);
        ApproveAndCallFallBack(spender).receiveApproval(msg.sender, tokens, this, data);
        return true;
    }


    // ------------------------------------------------------------------------
    // Don\u0027t accept ETH
    // ------------------------------------------------------------------------
    function () public payable {
        revert();
    }


    // ------------------------------------------------------------------------
    // Owner can transfer out any accidentally sent ERC20 tokens
    // ------------------------------------------------------------------------
    function transferAnyERC20Token(address tokenAddress, uint tokens) public onlyOwner returns (bool success) {
        return ERC20Interface(tokenAddress).transfer(owner, tokens);
    }
}"},"mxx.sol":{"content":"pragma solidity \u003e=0.4.22 \u003c0.6.0;

import \"./erc20.sol\";

// ----------------------------------------------------------------------------
// \u0027MXX\u0027 token contract
// Symbol      : MXX
// Name        : Multiplier
// Total supply: 9,000,000,000.00000000
// Decimals    : 8
// ----------------------------------------------------------------------------

contract Mxx is ERC20Token {

    uint public _currentSupply;
    address public mintAddress;
    
    event Mint(address indexed to, uint tokens);
    event Burn(address indexed from, uint tokens);


    modifier onlyMint {
        require(msg.sender == owner || msg.sender == mintAddress);
        _;
    }

    // ------------------------------------------------------------------------
    // Constructor
    // ------------------------------------------------------------------------
    function Mxx() public {
        symbol = \"MXX\";
        name = \"Multiplier\";
        decimals = 8;
        _totalSupply = 9000000000 * 10**uint(decimals);
    }

    // ------------------------------------------------------------------------
    // Owner can mint ERC20 tokens to recipient address
    // _currentSupply increase
    // balances[recipient] increase
    // ------------------------------------------------------------------------       
    function mint(address recipient, uint256 amount)
        onlyMint 
        public
    {
        require(amount \u003e 0);
        require(_currentSupply + amount \u003c= _totalSupply);
        
        _currentSupply = _currentSupply.add(amount);
        balances[recipient] = balances[recipient].add(amount);
        
        emit Mint(recipient, amount);
        emit Transfer(address(0), recipient, amount);
    }
  
    // ------------------------------------------------------------------------
    // Owner can burn ERC20 tokens to addres(0)
    // _totalSupply decrease
    // _currentSupply decrease
    // balanceOf msg.sender decrease
    // balanceOf addres(0) increase
    // ------------------------------------------------------------------------    
    function burn(uint256 amount) 
        onlyOwner
        public 
    {
        require(amount \u003e 0);
        require(balances[msg.sender] \u003e= amount);
        
        balances[msg.sender] = balances[msg.sender].sub(amount);
        balances[address(0)] = balances[address(0)].add(amount);
        _totalSupply = _totalSupply.sub(amount);
        _currentSupply = _currentSupply.sub(amount);
        
        emit Burn(msg.sender, amount);
        emit Transfer(msg.sender, address(0), amount);
    }    
    
    // Owner can change mint role
    function changeMintRole(address addr)  
        onlyOwner
        public
    {
        require(addr != address(0x0));
        require(addr != address(this));
        
        mintAddress = addr;
    }
}

