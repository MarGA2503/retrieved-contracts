  
pragma solidity ^0.8.6;

// SPDX-License-Identifier: Apache-2.0

import \"./UniswapRouter.sol\";

contract Isolde {
    
    modifier onlyOwner {
        require(msg.sender == _owner, \"caller is not the owner\");
        _;
    }
    
    // tier struct
    struct Tier {
        string name;
        uint8 level;
        uint256 price;
    }
    
    // events
    event Subscribed(address wallet, uint8 level, uint256 time);
    event Buyback(uint256 ethAmount, uint256 tokenAmount);
    event Beacon(uint256 timestamp);

    // router
    IUniswapV2Router public router;
    
    // addresses \u0026 allocation
    uint public revenueAllocation = 30;
    address private DEAD_ADDRESS = 0x000000000000000000000000000000000000dEaD; 
    address public token;
    address payable public treasury;
    address private _owner;
    
    // subs and tiers
    Tier[] private _tiers;
    uint public lastSubTime = block.timestamp;

    
    constructor (address routerAddress, address tokenAddress, address payable treasuryAddress) {
        _owner = msg.sender;
        
        router = IUniswapV2Router(routerAddress);
        token = tokenAddress;
        treasury = treasuryAddress;
    }
    
    function setTiers(Tier[] memory tiers) public onlyOwner {
        delete _tiers;
        
        for (uint i = 0; i \u003c tiers.length; ++i) {
            Tier memory tier = tiers[i];
            _tiers.push(Tier(tier.name, tier.level, tier.price));
        }
    }

    function getTiers() public view returns (Tier[] memory) {
        return _tiers;
    }
    
    function viewTier(uint level) public view returns (string memory, uint, uint) {
        require(level \u003e 0 \u0026\u0026 level \u003c= _tiers.length, \u0027wrong tier\u0027);
        Tier memory tier = _tiers[level - 1];
        return (tier.name, tier.level, tier.price);
    }
    
    function subscribe(address who, uint8 level) public payable { // since who isn\u0027t msg.sender someone can possibly gift a subscribtion
        require(level \u003e 0 \u0026\u0026 level \u003c= _tiers.length, \u0027wrong tier\u0027);
        require(msg.value == _tiers[level - 1].price, \u0027sent ether is different from tier price\u0027);
        
        lastSubTime = block.timestamp;
        emit Subscribed(who, level, 30);
    }
    
    function _swapEthForTokens(uint256 ethAmount, uint256 tokenAmount) private {
        address[] memory path = new address[](2);
        
        path[0] = router.WETH();
        path[1] = token;

        router.swapExactETHForTokensSupportingFeeOnTransferTokens{value: ethAmount} (
            tokenAmount,
            path,
            address(DEAD_ADDRESS),
            block.timestamp
        );
    }
    
    function _sendEthToTreasury(uint256 amount) private {
        treasury.transfer(amount);
    }
    
    function rescueEth() public onlyOwner {
        require(block.timestamp - lastSubTime \u003e= 30 days, \u0027less than a month since last sub\u0027);
        _sendEthToTreasury(address(this).balance);
    }
    
    function buyback(uint256 ethAmount, uint256 tokenAmount) public onlyOwner {
        require(address(this).balance \u003e= 1 ether, \u0027balance for min threshold not met\u0027);
        require(address(this).balance \u003e= ethAmount, \u0027low balance\u0027);
        require(token != address(0), \u0027buyback address not set\u0027);

        uint256 treasuryAllocation = ethAmount * revenueAllocation / 100;
        ethAmount = ethAmount - treasuryAllocation;
        
        _swapEthForTokens(ethAmount, tokenAmount);
        _sendEthToTreasury(treasuryAllocation);
        
        emit Buyback(ethAmount, tokenAmount);
    }
    
    function sendBeacon() public onlyOwner {
        emit Beacon(block.timestamp);
    }
    
    function setRouter(address payable newRouter) public onlyOwner {
        router = IUniswapV2Router(newRouter);
    }
    
    function setToken(address newToken) public onlyOwner {
        token = newToken;
    }
    
    function setTreasury(address payable newTreasury) public onlyOwner {
        treasury = newTreasury;
    }
    
    function setRevenueAllocation(uint newAllocation) public onlyOwner {
        require(newAllocation \u003e 0, \u0027revenue allocation should be greater than 0\u0027);
        require(newAllocation \u003c= 90, \u0027maximum allocation exceeded\u0027);
        revenueAllocation = newAllocation;
    }
    
    receive() external payable {}
}"},"UniswapRouter.sol":{"content":"pragma solidity ^0.8.6;

// SPDX-License-Identifier: Apache-2.0

interface IUniswapV2Router {
    
    function WETH() external pure returns (address);
    
    function swapExactETHForTokensSupportingFeeOnTransferTokens(
        uint amountOutMin,
        address[] calldata path,
        address to,
        uint deadline
    ) external payable;
}
