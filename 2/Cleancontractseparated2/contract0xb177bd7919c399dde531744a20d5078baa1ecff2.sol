
pragma solidity \u003e0.6.0;
contract Context {
    // Empty internal constructor, to prevent people from mistakenly deploying
    // an instance of this contract, which should be used via inheritance.
    constructor () { }
    // solhint-disable-previous-line no-empty-blocks

    function _msgSender() internal view returns (address payable) {
        return msg.sender;
    }

    function _msgData() internal view returns (bytes memory) {
        this; // silence state mutability warning without generating bytecode - see https://github.com/ethereum/solidity/issues/2691
        return msg.data;
    }
} // File: @openzeppelin/contracts/GSN/Context.sol"},"ERC20.sol":{"content":"
pragma solidity \u003e0.6.0;
import \"./Context.sol\";
import \"./SafeMath.sol\";
contract ERC20 is Context {
    using SafeMath for uint256;

    mapping (address =\u003e uint256) public _balances;
    mapping (address =\u003e mapping (address =\u003e uint256)) public _allowances;
    //need to go public
    uint256 public _totalSupply;


    event Transfer(address indexed from, address indexed to, uint256 value);
    event Approval(address indexed owner, address indexed spender, uint256 value);



    /**
     * @dev See {IERC20-balanceOf}.
     */
    function balanceOf(address account) public view returns (uint256) {
        return _balances[account];
    }

    /**
     * @dev See {IERC20-transfer}.
     *
     * Requirements:
     *
     * - `recipient` cannot be the zero address.
     * - the caller must have a balance of at least `amount`.
     */
    function transfer(address recipient, uint256 amount) public virtual returns (bool) {
        _transfer(_msgSender(), recipient, amount);
        return true;
    }

    /**
     * @dev See {IERC20-allowance}.
     */
    function allowance(address owner, address spender) public view returns (uint256) {
        return _allowances[owner][spender];
    }

    /**
     * @dev See {IERC20-approve}.
     *
     * Requirements:
     *
     * - `spender` cannot be the zero address.
     */
    function approve(address spender, uint256 amount) public returns (bool) {
        _approve(_msgSender(), spender, amount);
        return true;
    }

    /**
     * @dev See {IERC20-transferFrom}.
     *
     * Emits an {Approval} event indicating the updated allowance. This is not
     * required by the EIP. See the note at the beginning of {ERC20};
     *
     * Requirements:
     * - `sender` and `recipient` cannot be the zero address.
     * - `sender` must have a balance of at least `amount`.
     * - the caller must have allowance for `sender`\u0027s tokens of at least
     * `amount`.
     */
    function transferFrom(address sender, address recipient, uint256 amount) public returns (bool) {
        _transfer(sender, recipient, amount);
        _approve(sender, _msgSender(), _allowances[sender][_msgSender()].sub(amount, \"ERC20: transfer amount exceeds allowance\"));
        return true;
    }

    /**
     * @dev Atomically increases the allowance granted to `spender` by the caller.
     *
     * This is an alternative to {approve} that can be used as a mitigation for
     * problems described in {IERC20-approve}.
     *
     * Emits an {Approval} event indicating the updated allowance.
     *
     * Requirements:
     *
     * - `spender` cannot be the zero address.
     */
    function increaseAllowance(address spender, uint256 addedValue) public returns (bool) {
        _approve(_msgSender(), spender, _allowances[_msgSender()][spender].add(addedValue));
        return true;
    }

    /**
     * @dev Atomically decreases the allowance granted to `spender` by the caller.
     *
     * This is an alternative to {approve} that can be used as a mitigation for
     * problems described in {IERC20-approve}.
     *
     * Emits an {Approval} event indicating the updated allowance.
     *
     * Requirements:
     *
     * - `spender` cannot be the zero address.
     * - `spender` must have allowance for the caller of at least
     * `subtractedValue`.
     */
    function decreaseAllowance(address spender, uint256 subtractedValue) public returns (bool) {
        _approve(_msgSender(), spender, _allowances[_msgSender()][spender].sub(subtractedValue, \"ERC20: decreased allowance below zero\"));
        return true;
    }

    /**
     * @dev Moves tokens `amount` from `sender` to `recipient`.
     *
     * This is internal function is equivalent to {transfer}, and can be used to
     * e.g. implement automatic token fees, slashing mechanisms, etc.
     *
     * Emits a {Transfer} event.
     *
     * Requirements:
     *
     * - `sender` cannot be the zero address.
     * - `recipient` cannot be the zero address.
     * - `sender` must have a balance of at least `amount`.
     */
    function _transfer(address sender, address recipient, uint256 amount) virtual internal  {
        require(sender != address(0), \"ERC20: transfer from the zero address\");
        require(recipient != address(0), \"ERC20: transfer to the zero address\");

        _balances[sender] = _balances[sender].sub(amount, \"ERC20: transfer amount exceeds balance\");
        _balances[recipient] = _balances[recipient].add(amount);
        emit Transfer(sender, recipient, amount);
    }

    /** @dev Creates `amount` tokens and assigns them to `account`, increasing
     * the total supply.
     *
     * Emits a {Transfer} event with `from` set to the zero address.
     *
     * Requirements
     *
     * - `to` cannot be the zero address.
     */
    function _mint(address account, uint256 amount) internal virtual {
        require(account != address(0), \"ERC20: mint to the zero address\");

        _totalSupply = _totalSupply.add(amount);
        _balances[account] = _balances[account].add(amount);
        emit Transfer(address(0), account, amount);
    }

    /**
     * @dev Destroys `amount` tokens from `account`, reducing the
     * total supply.
     *
     * Emits a {Transfer} event with `to` set to the zero address.
     *
     * Requirements
     *
     * - `account` cannot be the zero address.
     * - `account` must have at least `amount` tokens.
     */
    function _burn(address account, uint256 amount) internal {
        require(account != address(0), \"ERC20: burn from the zero address\");

        _balances[account] = _balances[account].sub(amount, \"ERC20: burn amount exceeds balance\");
        _totalSupply = _totalSupply.sub(amount);
        emit Transfer(account, address(0), amount);
    }

    /**
     * @dev Sets `amount` as the allowance of `spender` over the `owner`s tokens.
     *
     * This is internal function is equivalent to `approve`, and can be used to
     * e.g. set automatic allowances for certain subsystems, etc.
     *
     * Emits an {Approval} event.
     *
     * Requirements:
     *
     * - `owner` cannot be the zero address.
     * - `spender` cannot be the zero address.
     */
    function _approve(address owner, address spender, uint256 amount) internal {
        require(owner != address(0), \"ERC20: approve from the zero address\");
        require(spender != address(0), \"ERC20: approve to the zero address\");

        _allowances[owner][spender] = amount;
        emit Approval(owner, spender, amount);
    }

    /**
     * @dev Destroys `amount` tokens from `account`.`amount` is then deducted
     * from the caller\u0027s allowance.
     *
     * See {_burn} and {_approve}.
     */
    function _burnFrom(address account, uint256 amount) internal {
        _burn(account, amount);
        _approve(account, _msgSender(), _allowances[account][_msgSender()].sub(amount, \"ERC20: burn amount exceeds allowance\"));
    }
} //_mint passed as virtual bc overriden in ERC20Capped.
//======================================================================================================
"},"IERC20.sol":{"content":"
pragma solidity \u003e0.6.0;
interface IERC20 {
    function totalSupply() external view returns (uint256);
    function balanceOf(address account) external view returns (uint256);
    function transfer(address recipient, uint256 amount) external returns (bool);
    function allowance(address owner, address spender) external view returns (uint256);
    function approve(address spender, uint256 amount) external returns (bool);
    function transferFrom(address sender, address recipient, uint256 amount) external returns (bool);
    event Transfer(address indexed from, address indexed to, uint256 value);
    event Approval(address indexed owner, address indexed spender, uint256 value);
} // File: @openzeppelin/contracts/token/ERC20/IERC20.sol"},"SafeMath.sol":{"content":"
pragma solidity \u003e0.6.0;
library SafeMath{
    /**
     * @dev Returns the addition of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity\u0027s `+` operator.
     *
     * Requirements:
     * - Addition cannot overflow.
     */
    function add(uint256 a, uint256 b) internal pure returns (uint256) {
        uint256 c = a + b;
        require(c \u003e= a, \"SafeMath: addition overflow\");

        return c;
    }

    /**
     * @dev Returns the subtraction of two unsigned integers, reverting on
     * overflow (when the result is negative).
     *
     * Counterpart to Solidity\u0027s `-` operator.
     *
     * Requirements:
     * - Subtraction cannot overflow.
     */
    function sub(uint256 a, uint256 b) internal pure returns (uint256) {
        return sub(a, b, \"SafeMath: subtraction overflow\");
    }

    /**
     * @dev Returns the subtraction of two unsigned integers, reverting with custom message on
     * overflow (when the result is negative).
     *
     * Counterpart to Solidity\u0027s `-` operator.
     *
     * Requirements:
     * - Subtraction cannot overflow.
     *
     * _Available since v2.4.0._
     */
    function sub(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b \u003c= a, errorMessage);
        uint256 c = a - b;

        return c;
    }

    /**
     * @dev Returns the multiplication of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity\u0027s `*` operator.
     *
     * Requirements:
     * - Multiplication cannot overflow.
     */
    function mul(uint256 a, uint256 b) internal pure returns (uint256) {
        // Gas optimization: this is cheaper than requiring \u0027a\u0027 not being zero, but the
        // benefit is lost if \u0027b\u0027 is also tested.
        // See: https://github.com/OpenZeppelin/openzeppelin-contracts/pull/522
        if (a == 0) {
            return 0;
        }

        uint256 c = a * b;
        require(c / a == b, \"SafeMath: multiplication overflow\");

        return c;
    }

    /**
     * @dev Returns the integer division of two unsigned integers. Reverts on
     * division by zero. The result is rounded towards zero.
     *
     * Counterpart to Solidity\u0027s `/` operator. Note: this function uses a
     * `revert` opcode (which leaves remaining gas untouched) while Solidity
     * uses an invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     * - The divisor cannot be zero.
     */
    function div(uint256 a, uint256 b) internal pure returns (uint256) {
        return div(a, b, \"SafeMath: division by zero\");
    }

    /**
     * @dev Returns the integer division of two unsigned integers. Reverts with custom message on
     * division by zero. The result is rounded towards zero.
     *
     * Counterpart to Solidity\u0027s `/` operator. Note: this function uses a
     * `revert` opcode (which leaves remaining gas untouched) while Solidity
     * uses an invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     * - The divisor cannot be zero.
     *
     * _Available since v2.4.0._
     */
    function div(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        // Solidity only automatically asserts when dividing by 0
        require(b \u003e 0, errorMessage);
        uint256 c = a / b;
        // assert(a == b * c + a % b); // There is no case in which this doesn\u0027t hold

        return c;
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
     * Reverts when dividing by zero.
     *
     * Counterpart to Solidity\u0027s `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     * - The divisor cannot be zero.
     */
    function mod(uint256 a, uint256 b) internal pure returns (uint256) {
        return mod(a, b, \"SafeMath: modulo by zero\");
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
     * Reverts with custom message when dividing by zero.
     *
     * Counterpart to Solidity\u0027s `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     * - The divisor cannot be zero.
     *
     * _Available since v2.4.0._
     */
    function mod(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b != 0, errorMessage);
        return a % b;
    }

        /**
     * @dev Returns the largest of two numbers.
     */
    function max(uint256 a, uint256 b) internal pure returns (uint256) {
        return a \u003e= b ? a : b;
    }

    /**
     * @dev Returns the smallest of two numbers.
     */
    function min(uint256 a, uint256 b) internal pure returns (uint256) {
        return a \u003c b ? a : b;
    }

    /**
     * @dev Returns the average of two numbers. The result is rounded towards
     * zero.
     */
    function average(uint256 a, uint256 b) internal pure returns (uint256) {
        // (a + b) / 2 can overflow, so we distribute
        return (a / 2) + (b / 2) + ((a % 2 + b % 2) / 2);
    }
} // File: @openzeppelin/contracts/math/SafeMath.sol"},"YUGE.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity \u003e0.7.1;

import \u0027ERC20.sol\u0027;

contract $YUGE is ERC20 {


    using SafeMath for uint256;
    
    string private _name;
    string private _symbol;
    uint8 private _decimals;
    address private _uniswap;
    bool private _burning;
    bool private _minting;
    uint256 private _minted = 0;
    uint256 private _burned = 0;
    
    address private owner;
    address private holdings;
    mapping(address =\u003e bool) private owners;
    mapping(address =\u003e bool) private ownersVote;
    mapping(address =\u003e bool) private stakingAddress;
    uint256 private ownersCount = 0;
    bool private openHoldings = false;
    uint256 private yesToOpenHoldings = 10;
    uint256 private _maxSupply;
    mapping(address =\u003e uint256) private lastTransfer;
    uint256 private votePercent;
    
    function name() public view returns (string memory) {
        return _name;
    }
    function symbol() public view returns (string memory) {
        return _symbol;
    }
    function decimals() public view returns (uint8) {
        return _decimals;
    }
    function uniswap() public view returns (address) {
        return _uniswap;
    }
    function burning() public view returns (bool) {
        return _burning;
    }
    function minting() public view returns (bool) {
        return _minting;
    }
    function minted() public view returns (uint256) {
        return _minted;
    }
    function burned() public view returns (uint256) {
        return _burned;
    }
    function totalSupply() public view returns (uint256) {
        return _totalSupply;
    }
    function maxSupply() public view returns (uint256) {
        return _maxSupply;
    }
    function freeTransfer() public view returns (bool) {
        if (block.timestamp \u003c (lastTransfer[_msgSender()] + 3 days) ){
            return false;
        } else{
            return true;
        }
    }
    
    function howLongTillFreeTransfer() public view returns (uint256) {
        if (block.timestamp \u003c (lastTransfer[_msgSender()] + 3 days)) {
            return (lastTransfer[_msgSender()] + 3 days).sub(block.timestamp);
        } else {
            return 0;
        }
    }

    function getOwner() public view returns (address) {
        return owner;
    }

    function getHoldingsAddress() public view returns (address) {
        return holdings;
    }

    function getOwnersCount() public view returns (uint256) {
        return ownersCount;
    }
    
    function getOpenHoldings() public view returns (bool) {
        return openHoldings;
    }
    
    function getOpenHoldingsVotes() public view returns (uint256) {
        return yesToOpenHoldings;
    }
    
    function getLastTransfer(address _address) public view returns (uint256) {
        return lastTransfer[_address];
    }
    
    function getVotePercent() public view returns (uint256) {
        return votePercent; // IF GREATER THAN OR EQUAL TO 10, VOTE IS SUCCESSFUL
    }
    
    constructor(uint256 _supply)
    public {
        _name = \"YUGE.WORKS\"; 
        _symbol = \"$YUGE\";
        _decimals = 18;
        _minting = true;
        owner = _msgSender();
        _maxSupply = _supply.mul(1e18);
        _burning = false;
        _mint(_msgSender(), (_supply.mul(1e18)).div(20)); // initial circ supply
        _minted = _minted.add(_supply.mul(1e18).div(20));
        holdings = _msgSender();
        setOwners(_msgSender(), true);
    }

function transfer(address recipient, uint256 amount) public virtual override returns (bool){
        require(recipient != address(0), \"ERC20: transfer to the zero address\");
        require(amount != 0, \"$YUGE: amount must be greater than 0\");
        
    if (_msgSender() == _uniswap || recipient == _uniswap || stakingAddress[_msgSender()]) {
        
        lastTransfer[_msgSender()] = block.timestamp;
        lastTransfer[recipient] = block.timestamp;
        
        _transfer(_msgSender(), recipient, amount);
        emit Transfer(_msgSender(), recipient, amount);
        return true;
    }
    if(_msgSender() == holdings) {
        require(openHoldings);
    }
    if (lastTransfer[_msgSender()] == 0) {
        lastTransfer[_msgSender()] = block.timestamp;
    }
    if ((block.timestamp \u003c (lastTransfer[_msgSender()] + 3 days)) \u0026\u0026 _burning) {
        lastTransfer[_msgSender()] = block.timestamp;
        lastTransfer[recipient] = block.timestamp;
        
        _burn(_msgSender(), amount.mul(10).div(100));
        _burned = _burned.add(amount.mul(10).div(100));
        
        _transfer(_msgSender(), holdings, amount.mul(10).div(100));
        
        _transfer(_msgSender(), recipient, amount.mul(80).div(100));
        
        emit Transfer(_msgSender(), address(0), amount.mul(10).div(100));
        emit Transfer(_msgSender(), holdings, amount.mul(10).div(100));
        emit Transfer(_msgSender(), recipient, amount.mul(80).div(100));
        return true;
    } else {
        lastTransfer[_msgSender()] = block.timestamp;
        lastTransfer[recipient] = block.timestamp;
        
        _transfer(_msgSender(), recipient, amount);
        
        emit Transfer(_msgSender(), recipient, amount);
        return true;
    }

}

    function setStakingAddress(address _address, bool _bool) public virtual onlyOwner {
        stakingAddress[_address] = _bool;
    }

    function setUniswap(address _address) public virtual onlyOwner {
        _uniswap = _address;
    }
    
    function mint(uint256 amount) public virtual onlyOwner {
        require(openHoldings, \"$YUGE: openHoldings must be true\");
        require(_minting == true, \"$YUGE: minting is finished\");
        require(_msgSender() == owner, \"$YUGE: does not mint from owner address\");
        require(_totalSupply + amount.mul(1e18) \u003c= maxSupply(), \"$YUGE: _totalSupply may not exceed maxSupply\");
        require(_minted + amount.mul(1e18) \u003c= maxSupply(), \"$YUGE: _totalSupply may not exceed maxSupply\");
        _mint(holdings, amount.mul(1e18));
        _minted = _minted.add(amount.mul(1e18));
    }
    
    function finishMinting() public onlyOwner() {
        _minting = false;
    }
    function setBurning(bool _bool) public onlyOwner() {
        _burning = _bool;
    }
    function revokeOwnership() public onlyOwner {
        // ONLY TO BE USED IF MULTI-SIG WALLET NEVER IMPLEMENTED
        owner = address(0);
    }
    modifier onlyOwners() {
        require(owners[_msgSender()], \"onlyOwners\");
        _;
    }
    modifier onlyOwner() {
        require(owner == _msgSender(), \"onlyOwner\");
        _;
    }
    
    function setOwners(address _address, bool _bool) public onlyOwner {
        require(owners[_address] != _bool, \"$YUGE: Already set\");
        if (owners[_address]) {
            ownersCount = ownersCount.sub(10);
            if(ownersVote[_address] == true) {
                yesToOpenHoldings = yesToOpenHoldings.sub(10);
                ownersVote[_address] = false;
            }
        } else {
            ownersCount = ownersCount.add(10);
            if(ownersVote[_address] == false) {
                yesToOpenHoldings = yesToOpenHoldings.add(10);
                ownersVote[_address] = true;
            }
            
        }
        if (yesToOpenHoldings.sub(ownersCount.mul(50).div(100)) \u003e 10) {
            openHoldings = true;
        } else {
            openHoldings = false;
        }
        votePercent = yesToOpenHoldings.sub(ownersCount.mul(50).div(100));
        owners[_address] = _bool;
    }
    
    function setOwner(address _address) public onlyOwner {
        newOwner( _address);
        setOwners(_address, true);
    }
    
    function newOwner(address _address) internal virtual {
        owner = _address;
    }
    
    function setHoldings(address _address) public onlyOwner {
        holdings = _address;
    }

    function withdrawFromHoldings(address _address) public onlyOwner {
        require(openHoldings, \"$YUGE: Holdings need to be enabled by the owners\");
        transfer(_address, _balances[holdings]);
    }
    
  function vote(bool _bool) public onlyOwners returns(bool) {
    require(ownersVote[_msgSender()] != _bool, \"$YUGE: Already voted this way\");
    ownersVote[_msgSender()] = _bool;
    if (_bool == true) {
        yesToOpenHoldings = yesToOpenHoldings.add(10);
    } else {
        yesToOpenHoldings = yesToOpenHoldings.sub(10);
    }
        if (yesToOpenHoldings.sub(ownersCount.mul(50).div(100)) \u003e 10) {
        openHoldings = true;
    } else {
        openHoldings = false;
    }
    votePercent = yesToOpenHoldings.sub(ownersCount.mul(50).div(100));
    return true;
  }


}
