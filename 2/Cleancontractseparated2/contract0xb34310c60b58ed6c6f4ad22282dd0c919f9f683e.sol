pragma solidity ^0.5.7;

library SafeMath {

    function mul(uint256 a, uint256 b) internal pure returns (uint256) {
        if (a == 0) {
            return 0;
        }

        uint256 c = a * b;
        require(c / a == b);

        return c;
    }

    function div(uint256 a, uint256 b) internal pure returns (uint256) {
        require(b \u003e 0);
        uint256 c = a / b;
        
\treturn c;
    }

    function sub(uint256 a, uint256 b) internal pure returns (uint256) {
        require(b \u003c= a);
        uint256 c = a - b;

        return c;
    }

    function add(uint256 a, uint256 b) internal pure returns (uint256) {
        uint256 c = a + b;
        require(c \u003e= a);

        return c;
    }

    function mod(uint256 a, uint256 b) internal pure returns (uint256) {
        require(b != 0);
        return a % b;
    }
}

contract ERC20Standard {
\tusing SafeMath for uint256;
\tuint public totalSupply;
\t
\tstring public name;
\tuint8 public decimals;
\tstring public symbol;
\tstring public version;
\t
\tmapping (address =\u003e uint256) balances;
\tmapping (address =\u003e mapping (address =\u003e uint)) allowed;

\t//Fix for short address attack against ERC20
\tmodifier onlyPayloadSize(uint size) {
\t\tassert(msg.data.length == size + 4);
\t\t_;
\t} 

\tfunction balanceOf(address _owner) public view returns (uint balance) {
\t\treturn balances[_owner];
\t}

\tfunction transfer(address _recipient, uint _value) public onlyPayloadSize(2*32) {
\t    require(balances[msg.sender] \u003e= _value \u0026\u0026 _value \u003e 0);
\t    balances[msg.sender] = balances[msg.sender].sub(_value);
\t    balances[_recipient] = balances[_recipient].add(_value);
\t    emit Transfer(msg.sender, _recipient, _value);        
        }

\tfunction transferFrom(address _from, address _to, uint _value) public {
\t    require(balances[_from] \u003e= _value \u0026\u0026 allowed[_from][msg.sender] \u003e= _value \u0026\u0026 _value \u003e 0);
            balances[_to] = balances[_to].add(_value);
            balances[_from] = balances[_from].sub(_value);
            allowed[_from][msg.sender] = allowed[_from][msg.sender].sub(_value);
            emit Transfer(_from, _to, _value);
        }

\tfunction  approve(address _spender, uint _value) public {
\t\tallowed[msg.sender][_spender] = _value;
\t\temit Approval(msg.sender, _spender, _value);
\t}

\tfunction allowance(address _spender, address _owner) public view returns (uint balance) {
\t\treturn allowed[_owner][_spender];
\t}

\t//Event which is triggered to log all transfers to this contract\u0027s event log
\tevent Transfer(
\t\taddress indexed _from,
\t\taddress indexed _to,
\t\tuint _value
\t\t);
\t\t
\t//Event which is triggered whenever an owner approves a new allowance for a spender.
\tevent Approval(
\t\taddress indexed _owner,
\t\taddress indexed _spender,
\t\tuint _value
\t\t);
}
"},"NewToken.sol":{"content":"pragma solidity ^0.5.7;

import \"./ERC20Standard.sol\";

contract NewToken is ERC20Standard {
\tconstructor() public {
\t\ttotalSupply = 100000000000;
\t\tname = \"Stable Blockchain Loan\";
\t\tdecimals = 2;
\t\tsymbol = \"SBL\";
\t\tversion = \"1.0\";
\t\tbalances[msg.sender] = totalSupply;
\t}
}

