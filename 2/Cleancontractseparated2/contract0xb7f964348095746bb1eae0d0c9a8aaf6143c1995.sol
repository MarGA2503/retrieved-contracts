// SPDX-License-Identifier: GPL-3.0
pragma solidity \u003e=0.8.0 \u003c0.9.0;

import \"./ICoinvestingDeFiPair.sol\";
import \"./SafeMath.sol\";

library CoinvestingDeFiLibrary {
    using SafeMath for uint;
    // Internal functions that are view
    function getAmountsIn(
        address factory,
        uint amountOut,
        address[] memory path
    )
    internal
    view
    returns (uint[] memory amounts)
    {
        require(path.length \u003e= 2, \u0027LIB: INV_P\u0027);
        amounts = new uint[](path.length);
        amounts[amounts.length - 1] = amountOut;
        for (uint i = path.length - 1; i \u003e 0; i--) {
            (uint reserveIn, uint reserveOut) = getReserves(
                factory, 
                path[i - 1], 
                path[i]
            );
            amounts[i - 1] = getAmountIn(
                amounts[i],
                reserveIn,
                reserveOut
            );
        }
    }

    function getAmountsOut(
        address factory,
        uint amountIn,
        address[] memory path
    )
    internal
    view
    returns (uint[] memory amounts)
    {
        require(path.length \u003e= 2, \u0027LIB: INV_P\u0027);
        amounts = new uint[](path.length);
        amounts[0] = amountIn;
        for (uint i; i \u003c path.length - 1; i++) {
            (uint reserveIn, uint reserveOut) = getReserves(
                factory, 
                path[i], 
                path[i + 1]
            );
            amounts[i + 1] = getAmountOut(
                amounts[i],
                reserveIn,
                reserveOut
            );
        }
    }

    function getReserves(
        address factory,
        address tokenA,
        address tokenB
    )
    internal
    view
    returns (
        uint reserveA,
        uint reserveB
    )
    {
        (address token0,) = sortTokens(tokenA, tokenB);
        (uint reserve0, uint reserve1,) = ICoinvestingDeFiPair(pairFor(
            factory,
            tokenA,
            tokenB
        )).getReserves();
        (reserveA, reserveB) = tokenA == token0 ? (reserve0, reserve1) : (reserve1, reserve0);
    }

    // Internal functions that are pure
    function getAmountIn(
        uint amountOut,
        uint reserveIn,
        uint reserveOut
    )
    internal
    pure
    returns (uint amountIn)
    {
        require(amountOut \u003e 0, \u0027LIB: INSUF_OUT_AMT\u0027);
        require(reserveIn \u003e 0 \u0026\u0026 reserveOut \u003e 0, \u0027LIB: INSUF_LIQ\u0027);
        uint numerator = reserveIn.mul(amountOut).mul(1000);
        uint denominator = reserveOut.sub(amountOut).mul(997);
        amountIn = (numerator / denominator).add(1);
    }
    
    function getAmountOut(
        uint amountIn,
        uint reserveIn,
        uint reserveOut
    )
    internal
    pure
    returns (uint amountOut)
    {
        require(amountIn \u003e 0, \u0027LIB: INSUF_IN_AMT\u0027);
        require(reserveIn \u003e 0 \u0026\u0026 reserveOut \u003e 0, \u0027LIB: INSUF_LIQ\u0027);
        uint amountInWithFee = amountIn.mul(997);
        uint numerator = amountInWithFee.mul(reserveOut);
        uint denominator = reserveIn.mul(1000).add(amountInWithFee);
        amountOut = numerator / denominator;
    }

    // calculates the CREATE2 address for a pair without making any external calls
    function pairFor(
        address factory,
        address tokenA,
        address tokenB
    )
    internal
    pure
    returns (address pair) 
    {
        (address token0, address token1) = sortTokens(tokenA, tokenB);
        pair = address(uint160(uint(keccak256(abi.encodePacked(
                hex\u0027ff\u0027,
                factory,
                keccak256(abi.encodePacked(token0, token1)),
                hex\u0027f414eaf687b005cd1d29be0b74430bb3d59f939715b3abbbb07af574ca27e22e\u0027 // init code hash
        )))));
    }

    function quote(
        uint amountA,
        uint reserveA,
        uint reserveB
    )
    internal
    pure
    returns (uint amountB)
    {
        require(amountA \u003e 0, \u0027LIB: INSUF_AMT\u0027);
        require(reserveA \u003e 0 \u0026\u0026 reserveB \u003e 0, \u0027LIB: INSUF_LIQ\u0027);
        amountB = amountA.mul(reserveB) / reserveA;
    }

    function sortTokens(
        address tokenA,
        address tokenB
    )
    internal
    pure
    returns (
        address token0,
        address token1
    )
    {
        require(tokenA != tokenB, \u0027LIB: IDT_ADDR\u0027);
        (token0, token1) = tokenA \u003c tokenB ? (tokenA, tokenB) : (tokenB, tokenA);
        require(token0 != address(0), \u0027LIB: ZERO_ADDR\u0027);
    }
}
"},"CoinvestingDeFiRouter.sol":{"content":"// SPDX-License-Identifier: GPL-3.0
pragma solidity \u003e=0.8.0 \u003c0.9.0;

import \"./ICoinvestingDeFiFactory.sol\";
import \"./IERC20.sol\";
import \u0027./SafeMath.sol\u0027;
import \"./ICoinvestingDeFiRouter.sol\";
import \"./IWETH.sol\";
import \"./CoinvestingDeFiLibrary.sol\";
import \"./TransferHelper.sol\";

contract CoinvestingDeFiRouter is ICoinvestingDeFiRouter {
    using SafeMath for uint;
    // Variables
    address public immutable override factory;
    address public immutable override WETH;
    
    // Modifiers
    modifier ensure(uint deadline) {
        require(deadline \u003e= block.timestamp, \"RTR: EXPD\");
        _;
    }

    // Constructor
    constructor(address _factory, address _WETH) {
        factory = _factory;
        WETH = _WETH;
    }

    // Receive function
    receive() external payable {
        // only accept ETH via fallback from the WETH contract
        assert(msg.sender == WETH); 
    }

    // External functions
    function addLiquidity(
        address tokenA,
        address tokenB,
        uint amountADesired,
        uint amountBDesired,
        uint amountAMin,
        uint amountBMin,
        address to,
        uint deadline
    )
    external
    virtual
    override
    ensure(deadline)
    returns (
        uint amountA,
        uint amountB,
        uint liquidity
    )
    {
        (amountA, amountB) = _addLiquidity(
            tokenA,
            tokenB,
            amountADesired,
            amountBDesired,
            amountAMin,
            amountBMin
        );

        address pair = CoinvestingDeFiLibrary.pairFor(
            factory,
            tokenA,
            tokenB
        );
        
        TransferHelper.safeTransferFrom(
            tokenA,
            msg.sender,
            pair,
            amountA
        );

        TransferHelper.safeTransferFrom(
            tokenB,
            msg.sender,
            pair,
            amountB
        );

        liquidity = ICoinvestingDeFiPair(pair).mint(to);
    }

    function addLiquidityETH(
        address token,
        uint amountTokenDesired,
        uint amountTokenMin,
        uint amountETHMin,
        address to,
        uint deadline
    )
    external
    virtual
    override
    payable
    ensure(deadline)
    returns (
        uint amountToken,
        uint amountETH,
        uint liquidity
    )
    {
        (amountToken, amountETH) = _addLiquidity(
            token,
            WETH,
            amountTokenDesired,
            msg.value,
            amountTokenMin,
            amountETHMin
        );

        address pair = CoinvestingDeFiLibrary.pairFor(
            factory,
            token,
            WETH
        );

        TransferHelper.safeTransferFrom(
            token,
            msg.sender,
            pair,
            amountToken
        );

        IWETH(WETH).deposit{value: amountETH}();
        assert(IWETH(WETH).transfer(
                pair,
                amountETH
            )
        );

        liquidity = ICoinvestingDeFiPair(pair).mint(to);        
        if (msg.value \u003e amountETH) {
            TransferHelper.safeTransferETH(
                msg.sender,
                msg.value - amountETH
            );
        }
    }

    function removeLiquidityETHWithPermit(
        address token,
        uint liquidity,
        uint amountTokenMin,
        uint amountETHMin,
        address to,
        uint deadline,
        bool approveMax,
        uint8 v,
        bytes32 r,
        bytes32 s
    )
    external
    virtual
    override
    returns (
        uint amountToken,
        uint amountETH
    )
    {
        address pair = CoinvestingDeFiLibrary.pairFor(
            factory,
            token,
            WETH
        );

        uint value = approveMax ? type(uint).max : liquidity;
        ICoinvestingDeFiPair(pair).permit(
            msg.sender,
            address(this),
            value,
            deadline,
            v,
            r,
            s
        );

        (amountToken, amountETH) = removeLiquidityETH(
            token,
            liquidity,
            amountTokenMin,
            amountETHMin,
            to,
            deadline
        );
    }

    function removeLiquidityETHWithPermitSupportingFeeOnTransferTokens(
        address token,
        uint liquidity,
        uint amountTokenMin,
        uint amountETHMin,
        address to,
        uint deadline,
        bool approveMax,
        uint8 v,
        bytes32 r,
        bytes32 s
    )
    external
    virtual
    override
    returns (uint amountETH)
    {
        address pair = CoinvestingDeFiLibrary.pairFor(
            factory,
            token,
            WETH
        );

        uint value = approveMax ? type(uint).max : liquidity;
        ICoinvestingDeFiPair(pair).permit(
            msg.sender,
            address(this),
            value,
            deadline,
            v,
            r,
            s
        );
        
        amountETH = removeLiquidityETHSupportingFeeOnTransferTokens(
            token,
            liquidity,
            amountTokenMin,
            amountETHMin,
            to,
            deadline
        );
    }

    function removeLiquidityWithPermit(
        address tokenA,
        address tokenB,
        uint liquidity,
        uint amountAMin,
        uint amountBMin,
        address to,
        uint deadline,
        bool approveMax,
        uint8 v,
        bytes32 r,
        bytes32 s
    )
    external
    virtual
    override
    returns (
        uint amountA,
        uint amountB
    )
    {
        address pair = CoinvestingDeFiLibrary.pairFor(
            factory,
            tokenA,
            tokenB
        );

        uint value = approveMax ? type(uint).max : liquidity;
        ICoinvestingDeFiPair(pair).permit(
            msg.sender,
            address(this),
            value,
            deadline,
            v,
            r,
            s
        );

        (amountA, amountB) = removeLiquidity(
            tokenA,
            tokenB,
            liquidity,
            amountAMin,
            amountBMin,
            to,
            deadline
        );
    }

    function swapETHForExactTokens(
        uint amountOut,
        address[] calldata path,
        address to,
        uint deadline
    )
    external
    virtual
    override
    payable
    ensure(deadline)
    returns (uint[] memory amounts)
    {
        require(path[0] == WETH, \"RTR: INV_P\");
        amounts = CoinvestingDeFiLibrary.getAmountsIn(
            factory,
            amountOut,
            path
        );

        require(amounts[0] \u003c= msg.value, \"RTR: XS_IN_AMT\");
        IWETH(WETH).deposit{value: amounts[0]}();
        assert(IWETH(WETH).transfer(
                CoinvestingDeFiLibrary.pairFor(
                    factory,
                    path[0],
                    path[1]
                ),
                amounts[0]
            )
        );

        _swap(
            amounts,
            path,
            to
        );

        if (msg.value \u003e amounts[0]) {
            TransferHelper.safeTransferETH(
                msg.sender,
                msg.value - amounts[0]
            );
        }            
    }

    function swapExactETHForTokens(
        uint amountOutMin,
        address[] calldata path,
        address to,
        uint deadline
    )
        external
        virtual
        override
        payable
        ensure(deadline)
        returns (uint[] memory amounts)
    {
        require(path[0] == WETH, \"RTR: INV_P\");
        amounts = CoinvestingDeFiLibrary.getAmountsOut(
            factory,
            msg.value,
            path
        );
        require(amounts[amounts.length - 1] \u003e= amountOutMin, \"RTR: INSUF_OUT_AMT\");
        
        IWETH(WETH).deposit{value: amounts[0]}();
        assert(IWETH(WETH).transfer(
                CoinvestingDeFiLibrary.pairFor(
                    factory,
                    path[0],
                    path[1]
                ),
                amounts[0]
            )
        );

        _swap(
            amounts,
            path,
            to
        );
    }

    function swapExactETHForTokensSupportingFeeOnTransferTokens(
        uint amountOutMin,
        address[] calldata path,
        address to,
        uint deadline
    )
    external
    virtual
    override
    payable
    ensure(deadline)
    {
        require(path[0] == WETH, \"RTR: INV_P\");
        uint amountIn = msg.value;
        IWETH(WETH).deposit{value: amountIn}();
        assert(IWETH(WETH).transfer(
                CoinvestingDeFiLibrary.pairFor(
                    factory,
                    path[0],
                    path[1]
                ), 
                amountIn
            )
        );
        
        uint balanceBefore = IERC20(path[path.length - 1]).balanceOf(to);
        _swapSupportingFeeOnTransferTokens(
            path,
            to
        );

        require(IERC20(path[path.length - 1]).balanceOf(to).sub(balanceBefore) \u003e= amountOutMin, \"RTR: INSUF_OUT_AMT\");
    }

    function swapExactTokensForETH(
        uint amountIn,
        uint amountOutMin,
        address[] calldata path,
        address to,
        uint deadline
    )
    external
    virtual
    override
    ensure(deadline)
    returns (uint[] memory amounts)
    {
        require(path[path.length - 1] == WETH, \"RTR: INV_P\");
        amounts = CoinvestingDeFiLibrary.getAmountsOut(
            factory,
            amountIn,
            path
        );

        require(amounts[amounts.length - 1] \u003e= amountOutMin, \"RTR: INSUF_OUT_AMT\");

        TransferHelper.safeTransferFrom(
            path[0],
            msg.sender,
            CoinvestingDeFiLibrary.pairFor(
                factory,
                path[0],
                path[1]
            ),
            amounts[0]
        );

        _swap(
            amounts,
            path,
            address(this)
        );

        IWETH(WETH).withdraw(amounts[amounts.length - 1]);
        TransferHelper.safeTransferETH(
            to,
            amounts[amounts.length - 1]
        );
    }
    
    function swapExactTokensForETHSupportingFeeOnTransferTokens(
        uint amountIn,
        uint amountOutMin,
        address[] calldata path,
        address to,
        uint deadline
    )
    external
    virtual
    override
    ensure(deadline)
    {
        require(path[path.length - 1] == WETH, \"RTR: INV_P\");
        TransferHelper.safeTransferFrom(
            path[0],
            msg.sender,
            CoinvestingDeFiLibrary.pairFor(
                factory,
                path[0],
                path[1]
            ),
            amountIn
        );

        _swapSupportingFeeOnTransferTokens(
            path,
            address(this)
        );
        
        uint amountOut = IERC20(WETH).balanceOf(address(this));
        require(amountOut \u003e= amountOutMin, \"RTR: INSUF_OUT_AMT\");

        IWETH(WETH).withdraw(amountOut);
        TransferHelper.safeTransferETH(
            to,
            amountOut
        );
    }

    function swapExactTokensForTokens(
        uint amountIn,
        uint amountOutMin,
        address[] calldata path,
        address to,
        uint deadline
    )
    external
    virtual
    override
    ensure(deadline)
    returns (uint[] memory amounts) 
    {
        amounts = CoinvestingDeFiLibrary.getAmountsOut(
            factory,
            amountIn,
            path
        );

        require(amounts[amounts.length - 1] \u003e= amountOutMin, \"RTR: INSUF_OUT_AMT\");
        
        TransferHelper.safeTransferFrom(
            path[0],
            msg.sender,
            CoinvestingDeFiLibrary.pairFor(
                factory,
                path[0],
                path[1]
            ), 
            amounts[0]
        );

        _swap(
            amounts,
            path,
            to
        );
    }

    function swapExactTokensForTokensSupportingFeeOnTransferTokens(
        uint amountIn,
        uint amountOutMin,
        address[] calldata path,
        address to,
        uint deadline
    )
    external
    virtual
    override
    ensure(deadline)
    {
        TransferHelper.safeTransferFrom(
            path[0],
            msg.sender,
            CoinvestingDeFiLibrary.pairFor(
                factory,
                path[0],
                path[1]
            ),
            amountIn
        );

        uint balanceBefore = IERC20(path[path.length - 1]).balanceOf(to);
        _swapSupportingFeeOnTransferTokens(
            path,
            to
        );

        require(IERC20(path[path.length - 1]).balanceOf(to).sub(balanceBefore) \u003e= amountOutMin, \"RTR: INSUF_OUT_AMT\");
    }

    function swapTokensForExactETH(
        uint amountOut,
        uint amountInMax,
        address[] calldata path,
        address to,
        uint deadline
    )
    external
    virtual
    override
    ensure(deadline)
    returns (uint[] memory amounts)
    {
        require(path[path.length - 1] == WETH, \"RTR: INV_P\");
        amounts = CoinvestingDeFiLibrary.getAmountsIn(
            factory,
            amountOut,
            path
        );

        require(amounts[0] \u003c= amountInMax, \"RTR: XS_IN_AMT\");
        TransferHelper.safeTransferFrom(
            path[0],
            msg.sender,
            CoinvestingDeFiLibrary.pairFor(
                factory,
                path[0],
                path[1]
            ),
            amounts[0]
        );

        _swap(
            amounts,
            path,
            address(this)
        );

        IWETH(WETH).withdraw(amounts[amounts.length - 1]);
        TransferHelper.safeTransferETH(
            to,
            amounts[amounts.length - 1]
        );
    }

    function swapTokensForExactTokens(
        uint amountOut,
        uint amountInMax,
        address[] calldata path,
        address to,
        uint deadline
    )
    external
    virtual
    override
    ensure(deadline)
    returns (uint[] memory amounts)
    {
        amounts = CoinvestingDeFiLibrary.getAmountsIn(
            factory,
            amountOut,
            path
        );

        require(amounts[0] \u003c= amountInMax, \"RTR: XS_IN_AMT\");
        TransferHelper.safeTransferFrom(
            path[0],
            msg.sender,
            CoinvestingDeFiLibrary.pairFor(
                factory,
                path[0],
                path[1]
            ),
            amounts[0]
        );

        _swap(
            amounts,
            path,
            to
        );
    }

    // Public functions
    function removeLiquidity(
        address tokenA,
        address tokenB,
        uint liquidity,
        uint amountAMin,
        uint amountBMin,
        address to,
        uint deadline
    )
    public
    virtual
    override
    ensure(deadline)
    returns (
        uint amountA,
        uint amountB
    )
    {
        address pair = CoinvestingDeFiLibrary.pairFor(
            factory,
            tokenA,
            tokenB
        );

        ICoinvestingDeFiPair(pair).transferFrom(
            msg.sender,
            pair,
            liquidity
        );

        (uint amount0, uint amount1) = ICoinvestingDeFiPair(pair).burn(to);
        (address token0,) = CoinvestingDeFiLibrary.sortTokens(
            tokenA,
            tokenB
        );

        (amountA, amountB) = tokenA == token0 ? (amount0, amount1) : (amount1, amount0);
        require(amountA \u003e= amountAMin, \"RTR: INSUF_A_AMT\");
        require(amountB \u003e= amountBMin, \"RTR: INSUF_B_AMT\");
    }

    function removeLiquidityETH(
        address token,
        uint liquidity,
        uint amountTokenMin,
        uint amountETHMin,
        address to,
        uint deadline
    )
    public
    virtual
    override
    ensure(deadline)
    returns (
        uint amountToken,
        uint amountETH
    ) 
    {
        (amountToken, amountETH) = removeLiquidity(
            token,
            WETH,
            liquidity,
            amountTokenMin,
            amountETHMin,
            address(this),
            deadline
        );

        TransferHelper.safeTransfer(
            token,
            to,
            amountToken
        );

        IWETH(WETH).withdraw(amountETH);
        TransferHelper.safeTransferETH(
            to,
            amountETH
        );
    }

    function removeLiquidityETHSupportingFeeOnTransferTokens(
        address token,
        uint liquidity,
        uint amountTokenMin,
        uint amountETHMin,
        address to,
        uint deadline
    )
    public
    virtual
    override
    ensure(deadline)
    returns (uint amountETH)
    {
        (, amountETH) = removeLiquidity(
            token,
            WETH,
            liquidity,
            amountTokenMin,
            amountETHMin,
            address(this),
            deadline
        );

        TransferHelper.safeTransfer(
            token,
            to,
            IERC20(token).balanceOf(address(this))
        );

        IWETH(WETH).withdraw(amountETH);
        TransferHelper.safeTransferETH(
            to,
            amountETH
        );
    }

    // Internal functions
    function _addLiquidity(
        address tokenA,
        address tokenB,
        uint amountADesired,
        uint amountBDesired,
        uint amountAMin,
        uint amountBMin
    )
    internal
    virtual
    returns (
        uint amountA,
        uint amountB
    )
    {
        if (ICoinvestingDeFiFactory(factory).getPair(tokenA, tokenB) == address(0)) {
            ICoinvestingDeFiFactory(factory).createPair(tokenA, tokenB);
        }
            
        (uint reserveA, uint reserveB) = CoinvestingDeFiLibrary.getReserves(
            factory,
            tokenA,
            tokenB
        );

        if (reserveA == 0 \u0026\u0026 reserveB == 0) {
            (amountA, amountB) = (amountADesired, amountBDesired);
        } else {
            uint amountBOptimal = CoinvestingDeFiLibrary.quote(
                amountADesired,
                reserveA,
                reserveB
            );
            if (amountBOptimal \u003c= amountBDesired) {
                require(amountBOptimal \u003e= amountBMin, \"RTR: INSUF_B_AMT\");
                (amountA, amountB) = (amountADesired, amountBOptimal);
            } else {
                uint amountAOptimal = CoinvestingDeFiLibrary.quote(
                    amountBDesired,
                    reserveB,
                    reserveA
                );
                assert(amountAOptimal \u003c= amountADesired);
                require(amountAOptimal \u003e= amountAMin, \"RTR: INSUF_A_AMT\");
                (amountA, amountB) = (amountAOptimal, amountBDesired);
            }
        }
    }

    function _swap(
        uint[] memory amounts,
        address[] memory path,
        address _to
    )
    internal
    virtual
    {
        for (uint i; i \u003c path.length - 1; i++) {
            (address input, address output) = (path[i], path[i + 1]);
            (address token0,) = CoinvestingDeFiLibrary.sortTokens(input, output);
            uint amountOut = amounts[i + 1];
            (uint amount0Out, uint amount1Out) = 
                input == token0 ? (uint(0), amountOut) : (amountOut, uint(0));

            address to = 
                i \u003c path.length - 2 ? 
                CoinvestingDeFiLibrary.pairFor(factory, output, path[i + 2]) : _to;

            ICoinvestingDeFiPair(CoinvestingDeFiLibrary.pairFor(
                    factory,
                    input,
                    output
                )
            ).swap(
                amount0Out,
                amount1Out,
                to,
                new bytes(0)
            );
        }
    }

    function _swapSupportingFeeOnTransferTokens(
        address[] memory path,
        address _to
    )
    internal
    virtual
    {
        for (uint i; i \u003c path.length - 1; i++) {
            (address input, address output) = (path[i], path[i + 1]);
            (address token0,) = CoinvestingDeFiLibrary.sortTokens(input, output);
            ICoinvestingDeFiPair pair = ICoinvestingDeFiPair(CoinvestingDeFiLibrary.pairFor(
                    factory,
                    input,
                    output
                )
            );

            uint amountInput;
            uint amountOutput;
            {
                (uint reserve0, uint reserve1,) = pair.getReserves();
                (uint reserveInput, uint reserveOutput) = 
                    input == token0 ? (reserve0, reserve1) : (reserve1, reserve0);
                
                amountInput = IERC20(input).balanceOf(address(pair)).sub(reserveInput);
                amountOutput = CoinvestingDeFiLibrary.getAmountOut(
                    amountInput,
                    reserveInput,
                    reserveOutput
                );
            }

            (uint amount0Out, uint amount1Out) = 
                input == token0 ? (uint(0), amountOutput) : (amountOutput, uint(0));
            
            address to = 
                i \u003c path.length - 2 ? 
                CoinvestingDeFiLibrary.pairFor(factory, output, path[i + 2]) : _to;
            
            pair.swap(
                amount0Out,
                amount1Out,
                to,
                new bytes(0)
            );
        }
    }

    // Public functions that are view
    function getAmountsIn(
        uint amountOut,
        address[] memory path
    )
    public
    view
    virtual
    override
    returns (uint[] memory amounts)
    {
        return CoinvestingDeFiLibrary.getAmountsIn(
            factory,
            amountOut,
            path
        );
    }

    function getAmountsOut(
        uint amountIn,
        address[] memory path
    )
    public 
    view
    virtual
    override
    returns (uint[] memory amounts)
    {
        return CoinvestingDeFiLibrary.getAmountsOut(
            factory,
            amountIn,
            path
        );
    }

    // Public functions that are pure
    function getAmountIn(
        uint amountOut,
        uint reserveIn,
        uint reserveOut
    )
    public
    pure
    virtual
    override
    returns (uint amountIn)
    {
        return CoinvestingDeFiLibrary.getAmountIn(
            amountOut,
            reserveIn,
            reserveOut
        );
    }

    function getAmountOut(
        uint amountIn,
        uint reserveIn,
        uint reserveOut
    ) 
    public
    pure
    virtual
    override
    returns (uint amountOut)
    {
        return CoinvestingDeFiLibrary.getAmountOut(
            amountIn,
            reserveIn,
            reserveOut
        );
    }

    function quote(
        uint amountA,
        uint reserveA,
        uint reserveB
    )
    public
    pure
    virtual
    override
    returns (uint amountB)
    {
        return CoinvestingDeFiLibrary.quote(
            amountA,
            reserveA,
            reserveB
        );
    }
}
"},"ICoinvestingDeFiERC20.sol":{"content":"// SPDX-License-Identifier: GPL-3.0
pragma solidity \u003e=0.8.0 \u003c0.9.0;

interface ICoinvestingDeFiERC20 {
    // Events
    event Approval(
        address indexed owner,
        address indexed spender,
        uint value
    );

    event Transfer(
        address indexed from,
        address indexed to,
        uint value
    );

    // External functions
    function approve(
        address spender,
        uint value
    )
        external 
        returns (bool);

    function permit(
        address owner,
        address spender,
        uint value,
        uint deadline,
        uint8 v,
        bytes32 r,
        bytes32 s
    )
        external;
    
    function transfer(
        address to,
        uint value
    )
        external
        returns (bool);

    function transferFrom(
        address from,
        address to,
        uint value
    )
        external
        returns (bool);
    
    // External functions that are view        
    function allowance(
        address owner,
        address spender
    )
        external 
        view 
        returns (uint);

    function balanceOf(address owner) external view returns (uint);
    function DOMAIN_SEPARATOR() external view returns (bytes32);
    function nonces(address owner) external view returns (uint);
    function totalSupply() external view returns (uint);

    // External functions that are pure
    function decimals() external pure returns (uint8);
    function name() external pure returns (string memory);
    function PERMIT_TYPEHASH() external pure returns (bytes32);
    function symbol() external pure returns (string memory);
}
"},"ICoinvestingDeFiFactory.sol":{"content":"// SPDX-License-Identifier: GPL-3.0
pragma solidity \u003e=0.8.0 \u003c0.9.0;

interface ICoinvestingDeFiFactory {
    //Events
    event PairCreated(
        address indexed token0,
        address indexed token1,
        address pair,
        uint
    );

    //External functions
    function createPair(
        address tokenA,
        address tokenB
    )
    external
    returns (address pair);

    function setFeeTo(address) external;
    function setFeeToSetter(address) external;

    // External functions that are view
    function allPairs(uint) external view returns (address pair);
    function allPairsLength() external view returns (uint);
    function feeTo() external view returns (address);
    function feeToSetter() external view returns (address);
    function getPair(
        address tokenA,
        address tokenB
    )
    external
    view
    returns (address pair);
}
"},"ICoinvestingDeFiPair.sol":{"content":"// SPDX-License-Identifier: GPL-3.0
pragma solidity \u003e=0.8.0 \u003c0.9.0;

import \"./ICoinvestingDeFiERC20.sol\";

interface ICoinvestingDeFiPair is ICoinvestingDeFiERC20 {
    // Events
    event Burn(
        address indexed sender,
        uint amount0,
        uint amount1,
        address indexed to
    );

    event Mint(
        address indexed sender,
        uint amount0,
        uint amount1
    );

    event Swap(
        address indexed sender,
        uint amount0In,
        uint amount1In,
        uint amount0Out,
        uint amount1Out,
        address indexed to
    );

    event Sync(
        uint112 reserve0, 
        uint112 reserve1
    );

    // External functions
    function burn(address to) external returns (
        uint amount0,
        uint amount1
    );

    function initialize(
        address,
        address
    ) external;

    function mint(address to) external returns (uint liquidity);
    function skim(address to) external;
    function swap(
        uint amount0Out,
        uint amount1Out,
        address to,
        bytes calldata data
    ) external;

    function sync() external;

    // External functions that are view
    function factory() external view returns (address);
    function getReserves() external view returns (
        uint112 reserve0,
        uint112 reserve1,
        uint32 blockTimestampLast
    );

    function kLast() external view returns (uint);    
    function price0CumulativeLast() external view returns (uint);
    function price1CumulativeLast() external view returns (uint);
    function token0() external view returns (address);
    function token1() external view returns (address);
    

    // External functions that are pure
    function MINIMUM_LIQUIDITY() external pure returns (uint);
}
"},"ICoinvestingDeFiRouter.sol":{"content":"// SPDX-License-Identifier: GPL-3.0
pragma solidity \u003e=0.8.0 \u003c0.9.0;

interface ICoinvestingDeFiRouter {
    // External functions
    function addLiquidity(
        address tokenA,
        address tokenB,
        uint amountADesired,
        uint amountBDesired,
        uint amountAMin,
        uint amountBMin,
        address to,
        uint deadline
    )
    external
    returns (
        uint amountA,
        uint amountB,
        uint liquidity
    );

    function addLiquidityETH(
        address token,
        uint amountTokenDesired,
        uint amountTokenMin,
        uint amountETHMin,
        address to,
        uint deadline
    )
    external
    payable
    returns (
        uint amountToken,
        uint amountETH,
        uint liquidity
    );

    function removeLiquidity(
        address tokenA,
        address tokenB,
        uint liquidity,
        uint amountAMin,
        uint amountBMin,
        address to,
        uint deadline
    )
    external
    returns (
        uint amountA,
        uint amountB
    );

    function removeLiquidityETH(
        address token,
        uint liquidity,
        uint amountTokenMin,
        uint amountETHMin,
        address to,
        uint deadline
    )
    external
    returns (
        uint amountToken,
        uint amountETH
    );

    function removeLiquidityETHSupportingFeeOnTransferTokens(
        address token,
        uint liquidity,
        uint amountTokenMin,
        uint amountETHMin,
        address to,
        uint deadline
    )
    external
    returns (uint amountETH);

    function removeLiquidityETHWithPermit(
        address token,
        uint liquidity,
        uint amountTokenMin,
        uint amountETHMin,
        address to,
        uint deadline,
        bool approveMax,
        uint8 v,
        bytes32 r,
        bytes32 s
    )
    external
    returns (
        uint amountToken,
        uint amountETH
    );

    function removeLiquidityETHWithPermitSupportingFeeOnTransferTokens(
        address token,
        uint liquidity,
        uint amountTokenMin,
        uint amountETHMin,
        address to,
        uint deadline,
        bool approveMax,
        uint8 v,
        bytes32 r,
        bytes32 s
    )
    external
    returns (uint amountETH);

    function removeLiquidityWithPermit(
        address tokenA,
        address tokenB,
        uint liquidity,
        uint amountAMin,
        uint amountBMin,
        address to,
        uint deadline,
        bool approveMax,
        uint8 v,
        bytes32 r,
        bytes32 s
    )
    external
    returns (
        uint amountA,
        uint amountB
    );

    function swapETHForExactTokens(
        uint amountOut,
        address[] calldata path,
        address to,
        uint deadline
    )
    external
    payable
    returns (uint[] memory amounts);

    function swapExactETHForTokens(
        uint amountOutMin, 
        address[] calldata path, 
        address to, 
        uint deadline
    )
    external
    payable
    returns (uint[] memory amounts);

    function swapExactETHForTokensSupportingFeeOnTransferTokens(
        uint amountOutMin,
        address[] calldata path,
        address to,
        uint deadline
    ) 
    external 
    payable;

    function swapExactTokensForETH(
        uint amountIn,
        uint amountOutMin,
        address[] calldata path,
        address to,
        uint deadline
    )
    external
    returns (uint[] memory amounts);

    function swapExactTokensForETHSupportingFeeOnTransferTokens(
        uint amountIn,
        uint amountOutMin,
        address[] calldata path,
        address to,
        uint deadline
    ) external;

    function swapExactTokensForTokens(
        uint amountIn,
        uint amountOutMin,
        address[] calldata path,
        address to,
        uint deadline
    ) 
    external
    returns (uint[] memory amounts);

    function swapExactTokensForTokensSupportingFeeOnTransferTokens(
        uint amountIn,
        uint amountOutMin,
        address[] calldata path,
        address to,
        uint deadline
    ) external;

    function swapTokensForExactETH(
        uint amountOut,
        uint amountInMax,
        address[] calldata path,
        address to,
        uint deadline
    )
    external
    returns (uint[] memory amounts);

    function swapTokensForExactTokens(
        uint amountOut,
        uint amountInMax,
        address[] calldata path,
        address to,
        uint deadline
    )
    external
    returns (uint[] memory amounts);

    // External functions that are view
    function factory() external view returns (address);

    function getAmountsIn(
        uint amountOut,
        address[] calldata path
    )
    external
    view
    returns (uint[] memory amounts);

    function getAmountsOut(
        uint amountIn,
        address[] calldata path
    )
    external 
    view 
    returns (uint[] memory amounts);

    function WETH() external view returns (address);

    // External functions that are pure
    function getAmountIn(
        uint amountOut,
        uint reserveIn,
        uint reserveOut
    )
    external
    pure
    returns (uint amountIn);

    function getAmountOut(
        uint amountIn,
        uint reserveIn,
        uint reserveOut
    ) 
    external
    pure 
    returns (uint amountOut);

    function quote(
        uint amountA,
        uint reserveA,
        uint reserveB
    )
    external
    pure
    returns (uint amountB);
}
"},"IERC20.sol":{"content":"// SPDX-License-Identifier: GPL-3.0
pragma solidity \u003e=0.8.0 \u003c0.9.0;

interface IERC20 {
    //Events
    event Approval(
        address indexed owner,
        address indexed spender,
        uint value
    );

    event Transfer(
        address indexed from,
        address indexed to,
        uint value
    );

    //External functions
    function approve(
        address spender,
        uint value
    )
        external 
        returns (bool);
    
    function transfer(
        address to,
        uint value
    )
        external
        returns (bool);

    function transferFrom(
        address from,
        address to,
        uint value
    )
        external
        returns (bool);
    
    // External functions that are view        
    function allowance(
        address owner,
        address spender
    )
        external 
        view 
        returns (uint);

    function balanceOf(address owner) external view returns (uint);
    function decimals() external view returns (uint8);
    function name() external view returns (string memory);
    function symbol() external view returns (string memory);
    function totalSupply() external view returns (uint);
}
"},"IWETH.sol":{"content":"// SPDX-License-Identifier: GPL-3.0
pragma solidity \u003e=0.8.0 \u003c0.9.0;

interface IWETH {
    // External functions
    function deposit() external payable;
    function transfer(
        address to,
        uint value
    )
        external
        returns (bool);

    function withdraw(uint) external;
}
"},"SafeMath.sol":{"content":"// SPDX-License-Identifier: GPL-3.0
pragma solidity \u003e=0.8.0 \u003c0.9.0;

// CAUTION
// This version of SafeMath should only be used with Solidity 0.8 or later,
// because it relies on the compiler\u0027s built in overflow checks.

/**
 * @dev Wrappers over Solidity\u0027s arithmetic operations.
 *
 * NOTE: `SafeMath` is no longer needed starting with Solidity 0.8. The compiler
 * now has built in overflow checking.
 */
library SafeMath {
    /**
     * @dev Returns the addition of two unsigned integers, with an overflow flag.
     *
     * _Available since v3.4._
     */
    function tryAdd(uint256 a, uint256 b) internal pure returns (bool, uint256) {
        unchecked {
            uint256 c = a + b;
            if (c \u003c a) return (false, 0);
            return (true, c);
        }
    }

    /**
     * @dev Returns the substraction of two unsigned integers, with an overflow flag.
     *
     * _Available since v3.4._
     */
    function trySub(uint256 a, uint256 b) internal pure returns (bool, uint256) {
        unchecked {
            if (b \u003e a) return (false, 0);
            return (true, a - b);
        }
    }

    /**
     * @dev Returns the multiplication of two unsigned integers, with an overflow flag.
     *
     * _Available since v3.4._
     */
    function tryMul(uint256 a, uint256 b) internal pure returns (bool, uint256) {
        unchecked {
            // Gas optimization: this is cheaper than requiring \u0027a\u0027 not being zero, but the
            // benefit is lost if \u0027b\u0027 is also tested.
            // See: https://github.com/OpenZeppelin/openzeppelin-contracts/pull/522
            if (a == 0) return (true, 0);
            uint256 c = a * b;
            if (c / a != b) return (false, 0);
            return (true, c);
        }
    }

    /**
     * @dev Returns the division of two unsigned integers, with a division by zero flag.
     *
     * _Available since v3.4._
     */
    function tryDiv(uint256 a, uint256 b) internal pure returns (bool, uint256) {
        unchecked {
            if (b == 0) return (false, 0);
            return (true, a / b);
        }
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers, with a division by zero flag.
     *
     * _Available since v3.4._
     */
    function tryMod(uint256 a, uint256 b) internal pure returns (bool, uint256) {
        unchecked {
            if (b == 0) return (false, 0);
            return (true, a % b);
        }
    }

    /**
     * @dev Returns the addition of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity\u0027s `+` operator.
     *
     * Requirements:
     *
     * - Addition cannot overflow.
     */
    function add(uint256 a, uint256 b) internal pure returns (uint256) {
        return a + b;
    }

    /**
     * @dev Returns the subtraction of two unsigned integers, reverting on
     * overflow (when the result is negative).
     *
     * Counterpart to Solidity\u0027s `-` operator.
     *
     * Requirements:
     *
     * - Subtraction cannot overflow.
     */
    function sub(uint256 a, uint256 b) internal pure returns (uint256) {
        return a - b;
    }

    /**
     * @dev Returns the multiplication of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity\u0027s `*` operator.
     *
     * Requirements:
     *
     * - Multiplication cannot overflow.
     */
    function mul(uint256 a, uint256 b) internal pure returns (uint256) {
        return a * b;
    }

    /**
     * @dev Returns the integer division of two unsigned integers, reverting on
     * division by zero. The result is rounded towards zero.
     *
     * Counterpart to Solidity\u0027s `/` operator.
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function div(uint256 a, uint256 b) internal pure returns (uint256) {
        return a / b;
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
     * reverting when dividing by zero.
     *
     * Counterpart to Solidity\u0027s `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function mod(uint256 a, uint256 b) internal pure returns (uint256) {
        return a % b;
    }

    /**
     * @dev Returns the subtraction of two unsigned integers, reverting with custom message on
     * overflow (when the result is negative).
     *
     * CAUTION: This function is deprecated because it requires allocating memory for the error
     * message unnecessarily. For custom revert reasons use {trySub}.
     *
     * Counterpart to Solidity\u0027s `-` operator.
     *
     * Requirements:
     *
     * - Subtraction cannot overflow.
     */
    function sub(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        unchecked {
            require(b \u003c= a, errorMessage);
            return a - b;
        }
    }

    /**
     * @dev Returns the integer division of two unsigned integers, reverting with custom message on
     * division by zero. The result is rounded towards zero.
     *
     * Counterpart to Solidity\u0027s `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     *
     * Counterpart to Solidity\u0027s `/` operator. Note: this function uses a
     * `revert` opcode (which leaves remaining gas untouched) while Solidity
     * uses an invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function div(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        unchecked {
            require(b \u003e 0, errorMessage);
            return a / b;
        }
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
     * reverting with custom message when dividing by zero.
     *
     * CAUTION: This function is deprecated because it requires allocating memory for the error
     * message unnecessarily. For custom revert reasons use {tryMod}.
     *
     * Counterpart to Solidity\u0027s `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function mod(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        unchecked {
            require(b \u003e 0, errorMessage);
            return a % b;
        }
    }
}
"},"TransferHelper.sol":{"content":"// SPDX-License-Identifier: GPL-3.0
pragma solidity \u003e=0.8.0 \u003c0.9.0;

/// helper methods for interacting with ERC20 tokens and 
/// sending ETH that do not consistently return true/false
library TransferHelper {
    function safeApprove(
        address token,
        address to,
        uint256 value
    )
    internal
    {
        // bytes4(keccak256(bytes(\u0027approve(address,uint256)\u0027)));
        (bool success, bytes memory data) = token.call(
            abi.encodeWithSelector(0x095ea7b3, to, value)
        );
        require(success \u0026\u0026 (data.length == 0 || abi.decode(data, (bool))));
    }

    function safeTransfer(
        address token,
        address to,
        uint256 value
    )
    internal
    {
        // bytes4(keccak256(bytes(\u0027transfer(address,uint256)\u0027)));
        (bool success, bytes memory data) = token.call(
            abi.encodeWithSelector(0xa9059cbb, to, value)
        );
        require(success \u0026\u0026 (data.length == 0 || abi.decode(data, (bool))));
    }

    function safeTransferETH(
        address to,
        uint256 value
    )
    internal
    {
        (bool success, ) = to.call{value: value}(new bytes(0));
        require(success);
    }

    function safeTransferFrom(
        address token,
        address from,
        address to,
        uint256 value
    )
    internal
    {
        // bytes4(keccak256(bytes(\u0027transferFrom(address,address,uint256)\u0027)));
        (bool success, bytes memory data) = token.call(
            abi.encodeWithSelector(0x23b872dd, from, to, value)
        );
        require(success \u0026\u0026 (data.length == 0 || abi.decode(data, (bool))));
    }
}

