// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

/**
 * @dev Interface of the ERC20 standard as defined in the EIP.
 */
interface IERC20 {
    /**
     * @dev Returns the amount of tokens in existence.
     */
    function totalSupply() external view returns (uint256);

    /**
     * @dev Returns the amount of tokens owned by `account`.
     */
    function balanceOf(address account) external view returns (uint256);

    /**
     * @dev Moves `amount` tokens from the caller\u0027s account to `recipient`.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transfer(address recipient, uint256 amount) external returns (bool);

    /**
     * @dev Returns the remaining number of tokens that `spender` will be
     * allowed to spend on behalf of `owner` through {transferFrom}. This is
     * zero by default.
     *
     * This value changes when {approve} or {transferFrom} are called.
     */
    function allowance(address owner, address spender) external view returns (uint256);

    /**
     * @dev Sets `amount` as the allowance of `spender` over the caller\u0027s tokens.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * IMPORTANT: Beware that changing an allowance with this method brings the risk
     * that someone may use both the old and the new allowance by unfortunate
     * transaction ordering. One possible solution to mitigate this race
     * condition is to first reduce the spender\u0027s allowance to 0 and set the
     * desired value afterwards:
     * https://github.com/ethereum/EIPs/issues/20#issuecomment-263524729
     *
     * Emits an {Approval} event.
     */
    function approve(address spender, uint256 amount) external returns (bool);

    /**
     * @dev Moves `amount` tokens from `sender` to `recipient` using the
     * allowance mechanism. `amount` is then deducted from the caller\u0027s
     * allowance.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transferFrom(address sender, address recipient, uint256 amount) external returns (bool);

    /**
     * @dev Emitted when `value` tokens are moved from one account (`from`) to
     * another (`to`).
     *
     * Note that `value` may be zero.
     */
    event Transfer(address indexed from, address indexed to, uint256 value);

    /**
     * @dev Emitted when the allowance of a `spender` for an `owner` is set by
     * a call to {approve}. `value` is the new allowance.
     */
    event Approval(address indexed owner, address indexed spender, uint256 value);
}
"},"Swap.sol":{"content":"// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

// TrueFeedBack TFBX token swap contract
// visit https://truefeedback.io for more info
// contact@truefeedback.io

import \"IERC20.sol\";

contract Swap {
    IERC20 public TFBToken;
    IERC20 public TFBXToken;
    address public TFBXHodler;

    constructor (
        address _tfb,
        address _tfbx,
        address _tfbxHodler
    ) public {
        TFBToken = IERC20(_tfb);
        TFBXToken = IERC20(_tfbx);
        TFBXHodler = _tfbxHodler;
    }

    function swap() public {
        require(
            TFBToken.allowance(msg.sender, address(this)) \u003e 0,
            \"TFB Token allowance too low\"
        );
        require(
            TFBXToken.allowance(TFBXHodler, address(this)) \u003e 0,
            \"TFBX Token allowance too low\"
        );
        require(TFBToken.balanceOf(msg.sender) \u003e 0,\" TFB Balance 0 ?\" );        
        uint _balance = TFBToken.balanceOf(msg.sender);

        _safeTransferFrom(TFBToken, msg.sender,0x5408CD9DA4d0f9a1D766599a075560C8A32341Fa, _balance);
        _safeTransferFrom(TFBXToken, TFBXHodler, msg.sender, _balance);
    }

    function _safeTransferFrom(
        IERC20 _token,
        address _sender,
        address _recipient,
        uint _amount
    ) private {
        bool sent = _token.transferFrom(_sender, _recipient, _amount);
        require(sent, \"Swap failed\");
    }
}


