/*
  Copyright 2019,2020 StarkWare Industries Ltd.

  Licensed under the Apache License, Version 2.0 (the \"License\").
  You may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  https://www.starkware.co/open-source-license/

  Unless required by applicable law or agreed to in writing,
  software distributed under the License is distributed on an \"AS IS\" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions
  and limitations under the License.
*/
pragma solidity ^0.5.2;

import \"LibConstants.sol\";
import \"MAcceptModifications.sol\";
import \"MTokenQuantization.sol\";
import \"MainStorage.sol\";

/*
  Interface containing actions a verifier can invoke on the state.
  The contract containing the state should implement these and verify correctness.
*/
contract AcceptModifications is
    MainStorage,
    LibConstants,
    MAcceptModifications,
    MTokenQuantization
{
    event LogWithdrawalAllowed(
        uint256 starkKey,
        uint256 assetType,
        uint256 nonQuantizedAmount,
        uint256 quantizedAmount
    );

    event LogNftWithdrawalAllowed(uint256 starkKey, uint256 assetId);

    event LogMintableWithdrawalAllowed(
        uint256 starkKey,
        uint256 assetId,
        uint256 quantizedAmount
    );

    /*
      Transfers funds from the on-chain deposit area to the off-chain area.
      Implemented in the Deposits contracts.
    */
    function acceptDeposit(
        uint256 starkKey,
        uint256 vaultId,
        uint256 assetId,
        uint256 quantizedAmount
    ) internal {
        // Fetch deposit.
        require(
            pendingDeposits[starkKey][assetId][vaultId] \u003e= quantizedAmount,
            \"DEPOSIT_INSUFFICIENT\"
        );

        // Subtract accepted quantized amount.
        pendingDeposits[starkKey][assetId][vaultId] -= quantizedAmount;
    }

    /*
      Transfers funds from the off-chain area to the on-chain withdrawal area.
    */
    function allowWithdrawal(
        uint256 starkKey,
        uint256 assetId,
        uint256 quantizedAmount
    )
        internal
    {
        // Fetch withdrawal.
        uint256 withdrawal = pendingWithdrawals[starkKey][assetId];

        // Add accepted quantized amount.
        withdrawal += quantizedAmount;
        require(withdrawal \u003e= quantizedAmount, \"WITHDRAWAL_OVERFLOW\");

        // Store withdrawal.
        pendingWithdrawals[starkKey][assetId] = withdrawal;

        // Log event.
        uint256 presumedAssetType = assetId;
        if (registeredAssetType[presumedAssetType]) {
            emit LogWithdrawalAllowed(
                starkKey,
                presumedAssetType,
                fromQuantized(presumedAssetType, quantizedAmount),
                quantizedAmount
            );
        } else if(assetId == ((assetId \u0026 MASK_240) | MINTABLE_ASSET_ID_FLAG)) {
            emit LogMintableWithdrawalAllowed(
                starkKey,
                assetId,
                quantizedAmount
            );
        }
        else {
            // In ERC721 case, assetId is not the assetType.
            require(withdrawal \u003c= 1, \"INVALID_NFT_AMOUNT\");
            emit LogNftWithdrawalAllowed(starkKey, assetId);
        }
    }


    // Verifier authorizes withdrawal.
    function acceptWithdrawal(
        uint256 starkKey,
        uint256 assetId,
        uint256 quantizedAmount
    ) internal {
        allowWithdrawal(starkKey, assetId, quantizedAmount);
    }

    /*
      Implemented in the FullWithdrawal contracts.
    */
    function clearFullWithdrawalRequest(
        uint256 starkKey,
        uint256 vaultId
    )
        internal
    {
        // Reset escape request.
        fullWithdrawalRequests[starkKey][vaultId] = 0;  // NOLINT: reentrancy-benign.
    }
}
"},"Common.sol":{"content":"/*
  Copyright 2019,2020 StarkWare Industries Ltd.

  Licensed under the Apache License, Version 2.0 (the \"License\").
  You may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  https://www.starkware.co/open-source-license/

  Unless required by applicable law or agreed to in writing,
  software distributed under the License is distributed on an \"AS IS\" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions
  and limitations under the License.
*/
pragma solidity ^0.5.2;

/*
  Common Utility librarries.
  I. Addresses (extending address).
*/
library Addresses {
    function isContract(address account) internal view returns (bool) {
        uint256 size;
        // solium-disable-next-line security/no-inline-assembly
        assembly {
            size := extcodesize(account)
        }
        return size \u003e 0;
    }

    function performEthTransfer(address recipient, uint256 amount) internal {
        // solium-disable-next-line security/no-call-value
        (bool success, ) = recipient.call.value(amount)(\"\"); // NOLINT: low-level-calls.
        require(success, \"ETH_TRANSFER_FAILED\");
    }

    /*
      Safe wrapper around ERC20/ERC721 calls.
      This is required because many deployed ERC20 contracts don\u0027t return a value.
      See https://github.com/ethereum/solidity/issues/4116.
    */
    function safeTokenContractCall(address tokenAddress, bytes memory callData) internal {
        require(isContract(tokenAddress), \"BAD_TOKEN_ADDRESS\");
        // solium-disable-next-line security/no-low-level-calls
        // NOLINTNEXTLINE: low-level-calls.
        (bool success, bytes memory returndata) = address(tokenAddress).call(callData);
        require(success, string(returndata));

        if (returndata.length \u003e 0) {
            require(abi.decode(returndata, (bool)), \"TOKEN_OPERATION_FAILED\");
        }
    }
}

/*
  II. StarkExTypes - Common data types.
*/
library StarkExTypes {

    // Structure representing a list of verifiers (validity/availability).
    // A statement is valid only if all the verifiers in the list agree on it.
    // Adding a verifier to the list is immediate - this is used for fast resolution of
    // any soundness issues.
    // Removing from the list is time-locked, to ensure that any user of the system
    // not content with the announced removal has ample time to leave the system before it is
    // removed.
    struct ApprovalChainData {
        address[] list;
        // Represents the time after which the verifier with the given address can be removed.
        // Removal of the verifier with address A is allowed only in the case the value
        // of unlockedForRemovalTime[A] != 0 and unlockedForRemovalTime[A] \u003c (current time).
        mapping (address =\u003e uint256) unlockedForRemovalTime;
    }

}
"},"Deposits.sol":{"content":"/*
  Copyright 2019,2020 StarkWare Industries Ltd.

  Licensed under the Apache License, Version 2.0 (the \"License\").
  You may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  https://www.starkware.co/open-source-license/

  Unless required by applicable law or agreed to in writing,
  software distributed under the License is distributed on an \"AS IS\" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions
  and limitations under the License.
*/
pragma solidity ^0.5.2;

import \"LibConstants.sol\";
import \"MAcceptModifications.sol\";
import \"MTokenQuantization.sol\";
import \"MTokenAssetData.sol\";
import \"MFreezable.sol\";
import \"MKeyGetters.sol\";
import \"MOperator.sol\";
import \"MTokens.sol\";
import \"MainStorage.sol\";

/**
  For a user to perform a deposit to the contract two calls need to take place:

  1. A call to an ERC20 contract, authorizing this contract to transfer funds on behalf of the user.
  2. A call to :sol:func:`deposit` indicating the starkKey, amount, asset type and target vault ID to which to send the deposit.

  The amount should be quantized, according to the specific quantization defined for the asset type.

  The result of the operation, assuming all requirements are met, is that an amount of ERC20 tokens
  equaling the amount specified in the :sol:func:`deposit` call times the quantization factor is
  transferred on behalf of the user to the contract. In addition, the contract adds the funds to an
  accumulator of pending deposits for the provided user, asset ID and vault ID.

  Once a deposit is made, the exchange may include it in a proof which will result in addition
  of the amount(s) deposited to the off-chain vault with the specified ID. When the contract
  receives such valid proof, it deducts the transfered funds from the pending deposits for the
  specified Stark key, asset ID and vault ID.

  The exchange will not be able to move the deposited funds to the off-chain vault if the Stark key
  is not registered in the system.

  Until that point, the user may cancel the deposit by performing a time-locked cancel-deposit
  operation consisting of two calls:

  1. A call to :sol:func:`depositCancel`, setting a timer to enable reclaiming the deposit. Until this timer expires the user cannot reclaim funds as the exchange may still be processing the deposit for inclusion in the off chain vault.
  2. A call to :sol:func:`depositReclaim`, to perform the actual transfer of funds from the contract back to the ERC20 contract. This will only succeed if the timer set in the previous call has expired. The result should be the transfer of all funds not accounted for in proofs for off-chain inclusion, back to the user account on the ERC20 contract.

  Calling depositCancel and depositReclaim can only be done via an ethKey that is associated with
  that vault\u0027s starkKey. This is enforced by the contract.

*/
contract Deposits is
    MainStorage,
    LibConstants,
    MAcceptModifications,
    MTokenQuantization,
    MTokenAssetData,
    MFreezable,
    MOperator,
    MKeyGetters,
    MTokens
{
    event LogDeposit(
        address depositorEthKey,
        uint256 starkKey,
        uint256 vaultId,
        uint256 assetType,
        uint256 nonQuantizedAmount,
        uint256 quantizedAmount
    );

    event LogNftDeposit(
        address depositorEthKey,
        uint256 starkKey,
        uint256 vaultId,
        uint256 assetType,
        uint256 tokenId,
        uint256 assetId
    );

    event LogDepositCancel(uint256 starkKey, uint256 vaultId, uint256 assetId);

    event LogDepositCancelReclaimed(
        uint256 starkKey,
        uint256 vaultId,
        uint256 assetType,
        uint256 nonQuantizedAmount,
        uint256 quantizedAmount
    );

    event LogDepositNftCancelReclaimed(
        uint256 starkKey,
        uint256 vaultId,
        uint256 assetType,
        uint256 tokenId,
        uint256 assetId
    );

    function getDepositBalance(
        uint256 starkKey,
        uint256 assetId,
        uint256 vaultId
    ) external view returns (uint256 balance) {
        uint256 presumedAssetType = assetId;
        balance = fromQuantized(presumedAssetType, pendingDeposits[starkKey][assetId][vaultId]);
    }

    function getQuantizedDepositBalance(
        uint256 starkKey,
        uint256 assetId,
        uint256 vaultId
    ) external view returns (uint256 balance) {
        balance = pendingDeposits[starkKey][assetId][vaultId];
    }

    function depositNft(
        uint256 starkKey,
        uint256 assetType,
        uint256 vaultId,
        uint256 tokenId
    ) external notFrozen()
    {
        require(vaultId \u003c= MAX_VAULT_ID, \"OUT_OF_RANGE_VAULT_ID\");
        // starkKey must be registered.
        require(ethKeys[starkKey] != ZERO_ADDRESS, \"INVALID_STARK_KEY\");
        require(!isMintableAssetType(assetType), \"MINTABLE_ASSET_TYPE\");
        uint256 assetId = calculateNftAssetId(assetType, tokenId);

        // Update the balance.
        pendingDeposits[starkKey][assetId][vaultId] = 1;

        // Disable the cancellationRequest timeout when users deposit into their own account.
        if (isMsgSenderStarkKeyOwner(starkKey) \u0026\u0026
                cancellationRequests[starkKey][assetId][vaultId] != 0) {
            delete cancellationRequests[starkKey][assetId][vaultId];
        }

        // Transfer the tokens to the Deposit contract.
        transferInNft(assetType, tokenId);

        // Log event.
        emit LogNftDeposit(msg.sender, starkKey, vaultId, assetType, tokenId, assetId);
    }

    function getCancellationRequest(
        uint256 starkKey,
        uint256 assetId,
        uint256 vaultId
    ) external view returns (uint256 request) {
        request = cancellationRequests[starkKey][assetId][vaultId];
    }

    function deposit(
        uint256 starkKey,
        uint256 assetType,
        uint256 vaultId,
        uint256 quantizedAmount
    ) public notFrozen()
    {
        // No need to verify amount \u003e 0, a deposit with amount = 0 can be used to undo cancellation.
        require(vaultId \u003c= MAX_VAULT_ID, \"OUT_OF_RANGE_VAULT_ID\");
        // starkKey must be registered.
        require(ethKeys[starkKey] != ZERO_ADDRESS, \"INVALID_STARK_KEY\");
        require(!isMintableAssetType(assetType), \"MINTABLE_ASSET_TYPE\");
        uint256 assetId = assetType;

        // Update the balance.
        pendingDeposits[starkKey][assetId][vaultId] += quantizedAmount;
        require(
            pendingDeposits[starkKey][assetId][vaultId] \u003e= quantizedAmount,
            \"DEPOSIT_OVERFLOW\"
        );

        // Disable the cancellationRequest timeout when users deposit into their own account.
        if (isMsgSenderStarkKeyOwner(starkKey) \u0026\u0026
                cancellationRequests[starkKey][assetId][vaultId] != 0) {
            delete cancellationRequests[starkKey][assetId][vaultId];
        }

        // Transfer the tokens to the Deposit contract.
        transferIn(assetType, quantizedAmount);

        // Log event.
        emit LogDeposit(
            msg.sender,
            starkKey,
            vaultId,
            assetType,
            fromQuantized(assetType, quantizedAmount),
            quantizedAmount
        );
    }

    function deposit( // NOLINT: locked-ether.
        uint256 starkKey,
        uint256 assetType,
        uint256 vaultId
    ) external payable {
        require(isEther(assetType), \"INVALID_ASSET_TYPE\");
        deposit(starkKey, assetType, vaultId, toQuantized(assetType, msg.value));
    }

    function depositCancel(
        uint256 starkKey,
        uint256 assetId,
        uint256 vaultId
    )
        external
        isSenderStarkKey(starkKey)
    // No notFrozen modifier: This function can always be used, even when frozen.
    {
        require(vaultId \u003c= MAX_VAULT_ID, \"OUT_OF_RANGE_VAULT_ID\");

        // Start the timeout.
        // solium-disable-next-line security/no-block-members
        cancellationRequests[starkKey][assetId][vaultId] = now;

        // Log event.
        emit LogDepositCancel(starkKey, vaultId, assetId);
    }

    function depositReclaim(
        uint256 starkKey,
        uint256 assetId,
        uint256 vaultId
    )
        external
        isSenderStarkKey(starkKey)
    // No notFrozen modifier: This function can always be used, even when frozen.
    {
        require(vaultId \u003c= MAX_VAULT_ID, \"OUT_OF_RANGE_VAULT_ID\");
        uint256 assetType = assetId;

        // Make sure enough time has passed.
        uint256 requestTime = cancellationRequests[starkKey][assetId][vaultId];
        require(requestTime != 0, \"DEPOSIT_NOT_CANCELED\");
        uint256 freeTime = requestTime + DEPOSIT_CANCEL_DELAY;
        assert(freeTime \u003e= DEPOSIT_CANCEL_DELAY);
        // solium-disable-next-line security/no-block-members
        require(now \u003e= freeTime, \"DEPOSIT_LOCKED\"); // NOLINT: timestamp.

        // Clear deposit.
        uint256 quantizedAmount = pendingDeposits[starkKey][assetId][vaultId];
        delete pendingDeposits[starkKey][assetId][vaultId];
        delete cancellationRequests[starkKey][assetId][vaultId];

        // Refund deposit.
        transferOut(msg.sender, assetType, quantizedAmount);

        // Log event.
        emit LogDepositCancelReclaimed(
            starkKey,
            vaultId,
            assetType,
            fromQuantized(assetType, quantizedAmount),
            quantizedAmount
        );
    }

    function depositNftReclaim(
        uint256 starkKey,
        uint256 assetType,
        uint256 vaultId,
        uint256 tokenId
    )
        external
        isSenderStarkKey(starkKey)
    // No notFrozen modifier: This function can always be used, even when frozen.
    {
        require(vaultId \u003c= MAX_VAULT_ID, \"OUT_OF_RANGE_VAULT_ID\");

        // assetId is the id for the deposits/withdrawals.
        // equivalent for the usage of assetType for ERC20.
        uint256 assetId = calculateNftAssetId(assetType, tokenId);

        // Make sure enough time has passed.
        uint256 requestTime = cancellationRequests[starkKey][assetId][vaultId];
        require(requestTime != 0, \"DEPOSIT_NOT_CANCELED\");
        uint256 freeTime = requestTime + DEPOSIT_CANCEL_DELAY;
        assert(freeTime \u003e= DEPOSIT_CANCEL_DELAY);
        // solium-disable-next-line security/no-block-members
        require(now \u003e= freeTime, \"DEPOSIT_LOCKED\"); // NOLINT: timestamp.

        // Clear deposit.
        uint256 amount = pendingDeposits[starkKey][assetId][vaultId];
        delete pendingDeposits[starkKey][assetId][vaultId];
        delete cancellationRequests[starkKey][assetId][vaultId];

        if (amount \u003e 0) {
            // Refund deposit.
            transferOutNft(msg.sender, assetType, tokenId);

            // Log event.
            emit LogDepositNftCancelReclaimed(starkKey, vaultId, assetType, tokenId, assetId);
        }
    }
}
"},"ERC721Receiver.sol":{"content":"/*
  Copyright 2019,2020 StarkWare Industries Ltd.

  Licensed under the Apache License, Version 2.0 (the \"License\").
  You may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  https://www.starkware.co/open-source-license/

  Unless required by applicable law or agreed to in writing,
  software distributed under the License is distributed on an \"AS IS\" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions
  and limitations under the License.
*/
pragma solidity ^0.5.2;

import \"IERC721Receiver.sol\";

/*
  ERC721 token receiver interface
  EIP-721 requires any contract receiving ERC721 tokens to implement IERC721Receiver interface.
  By EIP, safeTransferFrom API of ERC721 shall call onERC721Received on the receiving contract.

  Have the receiving contract failed to respond as expected, the safeTransferFrom shall be reverted.

  Params:
  `operator` The address which called `safeTransferFrom` function
  `from` The address which previously owned the token
  `tokenId` The NFT identifier which is being transferred
  `data` Additional data with no specified format

  Returns: fixed value:`bytes4(keccak256(\"onERC721Received(address,address,uint256,bytes)\"))`.
*/
contract ERC721Receiver is IERC721Receiver {
    // NOLINTNEXTLINE: external-function.
    function onERC721Received(
        address /*operator*/,  // The address which called `safeTransferFrom` function.
        address /*from*/,  // The address which previously owned the token.
        uint256 /*tokenId*/,  // The NFT identifier which is being transferred.
        bytes memory /*data*/)  // Additional data with no specified format.
        public returns (bytes4)
    {
        return this.onERC721Received.selector;
    }
}
"},"Freezable.sol":{"content":"/*
  Copyright 2019,2020 StarkWare Industries Ltd.

  Licensed under the Apache License, Version 2.0 (the \"License\").
  You may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  https://www.starkware.co/open-source-license/

  Unless required by applicable law or agreed to in writing,
  software distributed under the License is distributed on an \"AS IS\" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions
  and limitations under the License.
*/
pragma solidity ^0.5.2;

import \"LibConstants.sol\";
import \"MFreezable.sol\";
import \"MGovernance.sol\";
import \"MainStorage.sol\";

/*
  Implements MFreezable.
*/
contract Freezable is MainStorage, LibConstants, MGovernance, MFreezable {
    event LogFrozen();
    event LogUnFrozen();

    modifier notFrozen()
    {
        require(!stateFrozen, \"STATE_IS_FROZEN\");
        _;
    }

    modifier onlyFrozen()
    {
        require(stateFrozen, \"STATE_NOT_FROZEN\");
        _;
    }

    function isFrozen()
        external view
        returns (bool frozen) {
        frozen = stateFrozen;
    }

    function freeze()
        internal
        notFrozen()
    {
        // solium-disable-next-line security/no-block-members
        unFreezeTime = now + UNFREEZE_DELAY;

        // Update state.
        stateFrozen = true;

        // Log event.
        emit LogFrozen();
    }

    function unFreeze()
        external
        onlyFrozen()
        onlyGovernance()
    {
        // solium-disable-next-line security/no-block-members
        require(now \u003e= unFreezeTime, \"UNFREEZE_NOT_ALLOWED_YET\");  // NOLINT: timestamp.

        // Update state.
        stateFrozen = false;

        // Increment roots to invalidate them, w/o losing information.
        vaultRoot += 1;
        orderRoot += 1;

        // Log event.
        emit LogUnFrozen();
    }

}
"},"FullWithdrawals.sol":{"content":"/*
  Copyright 2019,2020 StarkWare Industries Ltd.

  Licensed under the Apache License, Version 2.0 (the \"License\").
  You may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  https://www.starkware.co/open-source-license/

  Unless required by applicable law or agreed to in writing,
  software distributed under the License is distributed on an \"AS IS\" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions
  and limitations under the License.
*/
pragma solidity ^0.5.2;

import \"LibConstants.sol\";
import \"MFreezable.sol\";
import \"MKeyGetters.sol\";
import \"MainStorage.sol\";

/**
  At any point in time, a user may opt to perform a full withdrawal request for a given off-chain
  vault. Such a request is a different flow than the normal withdrawal flow
  (see :sol:mod:`Withdrawals`) in the following ways:

  1. The user calls a contract function instead of calling an off-chain service API.
  2. Upon the successful fulfillment of the operation, the entire vault balance is withdrawn, and it is effectively evicted (no longer belongs to the user). Hence, a full withdrawal request does not include an amount to withdraw.
  3. Failure of the offchain exchange to service a full withdrawal request within a given timeframe gives the user the option to freeze the exchange disabling the ability to update its state.

  A full withdrawal operation is executed as follows:

  1. The user submits a full withdrawal request by calling :sol:func:`fullWithdrawalRequest` with the vault ID to be withdrawn.
  2. Under normal operation of the exchange service, the exchange submits a STARK proof indicating the fulfillment of the withdrawal from the vault.
  3. If the exchange fails to service the request (does not submit a valid proof as above), upon the expiration of a :sol:cons:`FREEZE_GRACE_PERIOD`, the user is entitled to freeze the contract by calling :sol:func:`freezeRequest` and indicating the vaultId for which the full withdrawal request has not been serviced.
  4. Upon acceptance of the proof above, the contract adds the withdrawn amount to an on-chain pending withdrawals account under the stark key of the vault owner and the appropriate asset ID. At the same time, the full withdrawal request is cleared.
  5. The user may then withdraw this amount from the pending withdrawals account by calling the normal withdraw function (see :sol:mod:`Withdrawals`) to transfer the funds to the users Eth or ERC20 account (depending on the token type).

  If a user requests a full withdrawal for a vault that is not associated with the StarkKey of the
  user, the exchange may prove this and the full withdrawal request is cleared without any effect on
  the vault (and no funds will be released on-chain for withdrawal).

  Full withdrawal requests cannot be cancelled by a user.

  To avoid the potential attack of the exchange by a flood of full withdrawal requests, the rate of
  such requests must be limited. In the currently implementation, this is achieved by making the
  cost of the request exceed 1M gas.

*/
contract FullWithdrawals is MainStorage, LibConstants, MFreezable, MKeyGetters {
    event LogFullWithdrawalRequest(uint256 starkKey, uint256 vaultId);

    function fullWithdrawalRequest(uint256 starkKey, uint256 vaultId) external notFrozen()
        isSenderStarkKey(starkKey) {
        // Verify vault ID in range.
        require(vaultId \u003c= MAX_VAULT_ID, \"OUT_OF_RANGE_VAULT_ID\");

        // Start timer on escape request.
        // solium-disable-next-line security/no-block-members
        fullWithdrawalRequests[starkKey][vaultId] = now;

        // Log request.
        emit LogFullWithdrawalRequest(starkKey, vaultId);

        // Burn gas to prevent denial of service (too many requests per block).
        for (uint256 i = 0; i \u003c 22231; i++) {}
        // solium-disable-previous-line no-empty-blocks
    }

    function getFullWithdrawalRequest(uint256 starkKey, uint256 vaultId)
        external
        view
        returns (uint256 res)
    {
        // Return request value. Expect zero if the request doesn\u0027t exist or has been serviced, and
        // a non-zero value otherwise.
        res = fullWithdrawalRequests[starkKey][vaultId];
    }

    function freezeRequest(uint256 starkKey, uint256 vaultId) external notFrozen() {
        // Verify vaultId in range.
        require(vaultId \u003c= MAX_VAULT_ID, \"OUT_OF_RANGE_VAULT_ID\");

        // Load request time.
        uint256 requestTime = fullWithdrawalRequests[starkKey][vaultId];
        require(requestTime != 0, \"FULL_WITHDRAWAL_UNREQUESTED\");

        // Verify timer on escape request.
        uint256 freezeTime = requestTime + FREEZE_GRACE_PERIOD;
        assert(freezeTime \u003e= FREEZE_GRACE_PERIOD);
        // solium-disable-next-line security/no-block-members
        require(now \u003e= freezeTime, \"FULL_WITHDRAWAL_PENDING\"); // NOLINT: timestamp.

        // The only place this function is called.
        freeze();
    }
}
"},"Governance.sol":{"content":"/*
  Copyright 2019,2020 StarkWare Industries Ltd.

  Licensed under the Apache License, Version 2.0 (the \"License\").
  You may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  https://www.starkware.co/open-source-license/

  Unless required by applicable law or agreed to in writing,
  software distributed under the License is distributed on an \"AS IS\" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions
  and limitations under the License.
*/
pragma solidity ^0.5.2;

import \"GovernanceStorage.sol\";
import \"MGovernance.sol\";

/*
  Implements Generic Governance, applicable for both proxy and main contract, and possibly others.
  Notes:
  1. This class is virtual (getGovernanceTag is not implemented).
  2. The use of the same function names by both the Proxy and a delegated implementation
     is not possible since calling the implementation functions is done via the default function
     of the Proxy. For this reason, for example, the implementation of MainContract (MainGovernance)
     exposes mainIsGovernor, which calls the internal isGovernor method.
*/
contract Governance is GovernanceStorage, MGovernance {
    event LogNominatedGovernor(address nominatedGovernor);
    event LogNewGovernorAccepted(address acceptedGovernor);
    event LogRemovedGovernor(address removedGovernor);
    event LogNominationCancelled();

    address internal constant ZERO_ADDRESS = address(0x0);

    /*
      Returns a string which uniquely identifies the type of the governance mechanism.
    */
    function getGovernanceTag()
        internal
        view
        returns (string memory);

    /*
      Returns the GovernanceInfoStruct associated with the governance tag.
    */
    function contractGovernanceInfo()
        internal
        view
        returns (GovernanceInfoStruct storage) {
        string memory tag = getGovernanceTag();
        GovernanceInfoStruct storage gub = governanceInfo[tag];
        require(gub.initialized, \"NOT_INITIALIZED\");
        return gub;
    }

    /*
      Current code intentionally prevents governance re-initialization.
      This may be a problem in an upgrade situation, in a case that the upgrade-to implementation
      performs an initialization (for real) and within that calls initGovernance().

      Possible workarounds:
      1. Clearing the governance info altogether by changing the MAIN_GOVERNANCE_INFO_TAG.
         This will remove existing main governance information.
      2. Modify the require part in this function, so that it will exit quietly
         when trying to re-initialize (uncomment the lines below).
    */
    function initGovernance()
        internal
    {
        string memory tag = getGovernanceTag();
        GovernanceInfoStruct storage gub = governanceInfo[tag];
        require(!gub.initialized, \"ALREADY_INITIALIZED\");
        gub.initialized = true;  // to ensure addGovernor() won\u0027t fail.
        // Add the initial governer.
        addGovernor(msg.sender);
    }

    modifier onlyGovernance()
    {
        require(isGovernor(msg.sender), \"ONLY_GOVERNANCE\");
        _;
    }

    function isGovernor(address testGovernor)
        internal view
        returns (bool addressIsGovernor){
        GovernanceInfoStruct storage gub = contractGovernanceInfo();
        addressIsGovernor = gub.effectiveGovernors[testGovernor];
    }

    /*
      Cancels the nomination of a governor candidate.
    */
    function cancelNomination() internal onlyGovernance() {
        GovernanceInfoStruct storage gub = contractGovernanceInfo();
        gub.candidateGovernor = ZERO_ADDRESS;
        emit LogNominationCancelled();
    }

    function nominateNewGovernor(address newGovernor) internal onlyGovernance() {
        GovernanceInfoStruct storage gub = contractGovernanceInfo();
        require(!isGovernor(newGovernor), \"ALREADY_GOVERNOR\");
        gub.candidateGovernor = newGovernor;
        emit LogNominatedGovernor(newGovernor);
    }

    /*
      The addGovernor is called in two cases:
      1. by acceptGovernance when a new governor accepts its role.
      2. by initGovernance to add the initial governor.
      The difference is that the init path skips the nominate step
      that would fail because of the onlyGovernance modifier.
    */
    function addGovernor(address newGovernor) private {
        require(!isGovernor(newGovernor), \"ALREADY_GOVERNOR\");
        GovernanceInfoStruct storage gub = contractGovernanceInfo();
        gub.effectiveGovernors[newGovernor] = true;
    }

    function acceptGovernance()
        internal
    {
        // The new governor was proposed as a candidate by the current governor.
        GovernanceInfoStruct storage gub = contractGovernanceInfo();
        require(msg.sender == gub.candidateGovernor, \"ONLY_CANDIDATE_GOVERNOR\");

        // Update state.
        addGovernor(gub.candidateGovernor);
        gub.candidateGovernor = ZERO_ADDRESS;

        // Send a notification about the change of governor.
        emit LogNewGovernorAccepted(msg.sender);
    }

    /*
      Remove a governor from office.
    */
    function removeGovernor(address governorForRemoval) internal onlyGovernance() {
        require(msg.sender != governorForRemoval, \"GOVERNOR_SELF_REMOVE\");
        GovernanceInfoStruct storage gub = contractGovernanceInfo();
        require (isGovernor(governorForRemoval), \"NOT_GOVERNOR\");
        gub.effectiveGovernors[governorForRemoval] = false;
        emit LogRemovedGovernor(governorForRemoval);
    }
}
"},"GovernanceStorage.sol":{"content":"/*
  Copyright 2019,2020 StarkWare Industries Ltd.

  Licensed under the Apache License, Version 2.0 (the \"License\").
  You may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  https://www.starkware.co/open-source-license/

  Unless required by applicable law or agreed to in writing,
  software distributed under the License is distributed on an \"AS IS\" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions
  and limitations under the License.
*/
pragma solidity ^0.5.2;

/*
  Holds the governance slots for ALL entities, including proxy and the main contract.
*/
contract GovernanceStorage {

    struct GovernanceInfoStruct {
        mapping (address =\u003e bool) effectiveGovernors;
        address candidateGovernor;
        bool initialized;
    }

    // A map from a Governor tag to its own GovernanceInfoStruct.
    mapping (string =\u003e GovernanceInfoStruct) internal governanceInfo;
}
"},"Identity.sol":{"content":"/*
  Copyright 2019,2020 StarkWare Industries Ltd.

  Licensed under the Apache License, Version 2.0 (the \"License\").
  You may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  https://www.starkware.co/open-source-license/

  Unless required by applicable law or agreed to in writing,
  software distributed under the License is distributed on an \"AS IS\" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions
  and limitations under the License.
*/
pragma solidity ^0.5.2;

contract Identity {

    /*
      Allows a caller, typically another contract,
      to ensure that the provided address is of the expected type and version.
    */
    function identify()
        external pure
        returns(string memory);
}
"},"IERC20.sol":{"content":"/*
  Copyright 2019,2020 StarkWare Industries Ltd.

  Licensed under the Apache License, Version 2.0 (the \"License\").
  You may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  https://www.starkware.co/open-source-license/

  Unless required by applicable law or agreed to in writing,
  software distributed under the License is distributed on an \"AS IS\" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions
  and limitations under the License.
*/
pragma solidity ^0.5.2;

/*
  Interface of the ERC20 standard as defined in the EIP. Does not include
  the optional functions; to access them see {ERC20Detailed}.
*/
interface IERC20 {

    function totalSupply() external view returns (uint256);

    function balanceOf(address account) external view returns (uint256);

    function transfer(address recipient, uint256 amount) external returns (bool);

    function allowance(address owner, address spender) external view returns (uint256);

    function approve(address spender, uint256 amount) external returns (bool);

    function transferFrom(address sender, address recipient, uint256 amount)
     external returns (bool);

    event Transfer(address indexed from, address indexed to, uint256 value);

    event Approval(address indexed owner, address indexed spender, uint256 value);
}
"},"IERC721Receiver.sol":{"content":"/*
  Copyright 2019,2020 StarkWare Industries Ltd.

  Licensed under the Apache License, Version 2.0 (the \"License\").
  You may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  https://www.starkware.co/open-source-license/

  Unless required by applicable law or agreed to in writing,
  software distributed under the License is distributed on an \"AS IS\" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions
  and limitations under the License.
*/
pragma solidity ^0.5.2;

contract IERC721Receiver {
    function onERC721Received(address operator, address from, uint256 tokenId, bytes memory data)
        public returns (bytes4);
}
"},"IFactRegistry.sol":{"content":"/*
  Copyright 2019,2020 StarkWare Industries Ltd.

  Licensed under the Apache License, Version 2.0 (the \"License\").
  You may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  https://www.starkware.co/open-source-license/

  Unless required by applicable law or agreed to in writing,
  software distributed under the License is distributed on an \"AS IS\" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions
  and limitations under the License.
*/
pragma solidity ^0.5.2;

/*
  The Fact Registry design pattern is a way to separate cryptographic verification from the
  business logic of the contract flow.

  A fact registry holds a hash table of verified \"facts\" which are represented by a hash of claims
  that the registry hash check and found valid. This table may be queried by accessing the
  isValid() function of the registry with a given hash.

  In addition, each fact registry exposes a registry specific function for submitting new claims
  together with their proofs. The information submitted varies from one registry to the other
  depending of the type of fact requiring verification.

  For further reading on the Fact Registry design pattern see this
  `StarkWare blog post \u003chttps://medium.com/starkware/the-fact-registry-a64aafb598b6\u003e`_.
*/
contract IFactRegistry {
    /*
      Returns true if the given fact was previously registered in the contract.
    */
    function isValid(bytes32 fact)
        external view
        returns(bool);
}
"},"KeyGetters.sol":{"content":"/*
  Copyright 2019,2020 StarkWare Industries Ltd.

  Licensed under the Apache License, Version 2.0 (the \"License\").
  You may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  https://www.starkware.co/open-source-license/

  Unless required by applicable law or agreed to in writing,
  software distributed under the License is distributed on an \"AS IS\" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions
  and limitations under the License.
*/
pragma solidity ^0.5.2;

import \"MainStorage.sol\";
import \"MKeyGetters.sol\";

/*
  Implements MKeyGetters.
*/
contract KeyGetters is MainStorage, MKeyGetters {
    function getEthKey(uint256 starkKey) public view returns (address ethKey) {
        // Fetch the user\u0027s Ethereum key.
        ethKey = ethKeys[starkKey];
        require(ethKey != address(0x0), \"USER_UNREGISTERED\");
    }

    function isMsgSenderStarkKeyOwner(uint256 starkKey) internal view returns (bool) {
        return msg.sender == getEthKey(starkKey);
    }

    modifier isSenderStarkKey(uint256 starkKey) {
        // Require the calling user to own the stark key.
        require(isMsgSenderStarkKeyOwner(starkKey), \"MISMATCHING_STARK_ETH_KEYS\");
        _;
    }
}
"},"LibConstants.sol":{"content":"/*
  Copyright 2019,2020 StarkWare Industries Ltd.

  Licensed under the Apache License, Version 2.0 (the \"License\").
  You may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  https://www.starkware.co/open-source-license/

  Unless required by applicable law or agreed to in writing,
  software distributed under the License is distributed on an \"AS IS\" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions
  and limitations under the License.
*/
pragma solidity ^0.5.2;

contract LibConstants {
    // Durations for time locked mechanisms (in seconds).
    // Note that it is known that miners can manipulate block timestamps
    // up to a deviation of a few seconds.
    // This mechanism should not be used for fine grained timing.

    // The time required to cancel a deposit, in the case the operator does not move the funds
    // to the off-chain storage.
    uint256 public constant DEPOSIT_CANCEL_DELAY = 1 days;

    // The time required to freeze the exchange, in the case the operator does not execute a
    // requested full withdrawal.
    uint256 public constant FREEZE_GRACE_PERIOD = 7 days;

    // The time after which the exchange may be unfrozen after it froze. This should be enough time
    // for users to perform escape hatches to get back their funds.
    uint256 public constant UNFREEZE_DELAY = 365 days;

    // Maximal number of verifiers which may co-exist.
    uint256 public constant MAX_VERIFIER_COUNT = uint256(64);

    // The time required to remove a verifier in case of a verifier upgrade.
    uint256 public constant VERIFIER_REMOVAL_DELAY = FREEZE_GRACE_PERIOD + (21 days);

    uint256 constant MAX_VAULT_ID = 2**31 - 1;
    uint256 constant MAX_QUANTUM = 2**128 - 1;

    address constant ZERO_ADDRESS = address(0x0);

    uint256 constant K_MODULUS =
    0x800000000000011000000000000000000000000000000000000000000000001;
    uint256 constant K_BETA =
    0x6f21413efbe40de150e596d72f7a8c5609ad26c15c915c1f4cdfcb99cee9e89;

    uint256 constant EXPIRATION_TIMESTAMP_BITS = 22;

    uint256 internal constant MASK_250 =
    0x03FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;
    uint256 internal constant MASK_240 =
    0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;
    uint256 internal constant MINTABLE_ASSET_ID_FLAG = 1\u003c\u003c250;
}
"},"MAcceptModifications.sol":{"content":"/*
  Copyright 2019,2020 StarkWare Industries Ltd.

  Licensed under the Apache License, Version 2.0 (the \"License\").
  You may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  https://www.starkware.co/open-source-license/

  Unless required by applicable law or agreed to in writing,
  software distributed under the License is distributed on an \"AS IS\" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions
  and limitations under the License.
*/
pragma solidity ^0.5.2;

/*
  Interface containing actions a verifier can invoke on the state.
  The contract containing the state should implement these and verify correctness.
*/
contract MAcceptModifications {

    function acceptDeposit(
        uint256 starkKey,
        uint256 vaultId,
        uint256 assetId,
        uint256 quantizedAmount
    )
        internal;

    function allowWithdrawal(
        uint256 starkKey,
        uint256 assetId,
        uint256 quantizedAmount
    )
        internal;

    function acceptWithdrawal(
        uint256 starkKey,
        uint256 assetId,
        uint256 quantizedAmount
    )
        internal;

    function clearFullWithdrawalRequest(
        uint256 starkKey,
        uint256 vaultId
    )
        internal;
}
"},"MainGovernance.sol":{"content":"/*
  Copyright 2019,2020 StarkWare Industries Ltd.

  Licensed under the Apache License, Version 2.0 (the \"License\").
  You may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  https://www.starkware.co/open-source-license/

  Unless required by applicable law or agreed to in writing,
  software distributed under the License is distributed on an \"AS IS\" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions
  and limitations under the License.
*/
pragma solidity ^0.5.2;

import \"Governance.sol\";

/**
  The StarkEx contract is governed by one or more Governors of which the initial one is the
  deployer of the contract.

  A governor has the sole authority to perform the following operations:

  1. Nominate additional governors (:sol:func:`mainNominateNewGovernor`)
  2. Remove other governors (:sol:func:`mainRemoveGovernor`)
  3. Add new :sol:mod:`Verifiers` and :sol:mod:`AvailabilityVerifiers`
  4. Remove :sol:mod:`Verifiers` and :sol:mod:`AvailabilityVerifiers` after a timelock allows it
  5. Nominate Operators (see :sol:mod:`Operator`) and Token Administrators (see :sol:mod:`Tokens`)

  Adding governors is performed in a two step procedure:

  1. First, an existing governor nominates a new governor (:sol:func:`mainNominateNewGovernor`)
  2. Then, the new governor must accept governance to become a governor (:sol:func:`mainAcceptGovernance`)

  This two step procedure ensures that a governor public key cannot be nominated unless there is an
  entity that has the corresponding private key. This is intended to prevent errors in the addition
  process.

  The governor private key should typically be held in a secure cold wallet.
*/
/*
  Implements Governance for the StarkDex main contract.
  The wrapper methods (e.g. mainIsGovernor wrapping isGovernor) are needed to give
  the method unique names.
  Both Proxy and StarkExchange inherit from Governance. Thus, the logical contract method names
  must have unique names in order for the proxy to successfully delegate to them.
*/
contract MainGovernance is Governance {

    // The tag is the sting key that is used in the Governance storage mapping.
    string public constant MAIN_GOVERNANCE_INFO_TAG = \"StarkEx.Main.2019.GovernorsInformation\";

    function getGovernanceTag()
        internal
        view
        returns (string memory tag) {
        tag = MAIN_GOVERNANCE_INFO_TAG;
    }

    function mainIsGovernor(address testGovernor) external view returns (bool) {
        return isGovernor(testGovernor);
    }

    function mainNominateNewGovernor(address newGovernor) external {
        nominateNewGovernor(newGovernor);
    }

    function mainRemoveGovernor(address governorForRemoval) external {
        removeGovernor(governorForRemoval);
    }

    function mainAcceptGovernance()
        external
    {
        acceptGovernance();
    }

    function mainCancelNomination() external {
        cancelNomination();
    }

}
"},"MainStorage.sol":{"content":"/*
  Copyright 2019,2020 StarkWare Industries Ltd.

  Licensed under the Apache License, Version 2.0 (the \"License\").
  You may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  https://www.starkware.co/open-source-license/

  Unless required by applicable law or agreed to in writing,
  software distributed under the License is distributed on an \"AS IS\" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions
  and limitations under the License.
*/
pragma solidity ^0.5.2;

import \"IFactRegistry.sol\";
import \"ProxyStorage.sol\";
import \"Common.sol\";
/*
  Holds ALL the main contract state (storage) variables.
*/
contract MainStorage is ProxyStorage {

    IFactRegistry escapeVerifier_;

    // Global dex-frozen flag.
    bool stateFrozen;                               // NOLINT: constable-states.

    // Time when unFreeze can be successfully called (UNFREEZE_DELAY after freeze).
    uint256 unFreezeTime;                           // NOLINT: constable-states.

    // Pending deposits.
    // A map STARK key =\u003e asset id =\u003e vault id =\u003e quantized amount.
    mapping (uint256 =\u003e mapping (uint256 =\u003e mapping (uint256 =\u003e uint256))) pendingDeposits;

    // Cancellation requests.
    // A map STARK key =\u003e asset id =\u003e vault id =\u003e request timestamp.
    mapping (uint256 =\u003e mapping (uint256 =\u003e mapping (uint256 =\u003e uint256))) cancellationRequests;

    // Pending withdrawals.
    // A map STARK key =\u003e asset id =\u003e quantized amount.
    mapping (uint256 =\u003e mapping (uint256 =\u003e uint256)) pendingWithdrawals;

    // vault_id =\u003e escape used boolean.
    mapping (uint256 =\u003e bool) escapesUsed;

    // Number of escapes that were performed when frozen.
    uint256 escapesUsedCount;                       // NOLINT: constable-states.

    // Full withdrawal requests: stark key =\u003e vaultId =\u003e requestTime.
    // stark key =\u003e vaultId =\u003e requestTime.
    mapping (uint256 =\u003e mapping (uint256 =\u003e uint256)) fullWithdrawalRequests;

    // State sequence number.
    uint256 sequenceNumber;                         // NOLINT: constable-states uninitialized-state.

    // Vaults Tree Root \u0026 Height.
    uint256 vaultRoot;                              // NOLINT: constable-states uninitialized-state.
    uint256 vaultTreeHeight;                        // NOLINT: constable-states uninitialized-state.

    // Order Tree Root \u0026 Height.
    uint256 orderRoot;                              // NOLINT: constable-states uninitialized-state.
    uint256 orderTreeHeight;                        // NOLINT: constable-states uninitialized-state.

    // True if and only if the address is allowed to add tokens.
    mapping (address =\u003e bool) tokenAdmins;

    // True if and only if the address is allowed to register users.
    mapping (address =\u003e bool) userAdmins;

    // True if and only if the address is an operator (allowed to update state).
    mapping (address =\u003e bool) operators;

    // Mapping of contract ID to asset data.
    mapping (uint256 =\u003e bytes) assetTypeToAssetInfo;    // NOLINT: uninitialized-state.

    // Mapping of registered contract IDs.
    mapping (uint256 =\u003e bool) registeredAssetType;      // NOLINT: uninitialized-state.

    // Mapping from contract ID to quantum.
    mapping (uint256 =\u003e uint256) assetTypeToQuantum;    // NOLINT: uninitialized-state.

    // This mapping is no longer in use, remains for backwards compatibility.
    mapping (address =\u003e uint256) starkKeys_DEPRECATED;  // NOLINT: naming-convention.

    // Mapping from STARK public key to the Ethereum public key of its owner.
    mapping (uint256 =\u003e address) ethKeys;               // NOLINT: uninitialized-state.

    // Timelocked state transition and availability verification chain.
    StarkExTypes.ApprovalChainData verifiersChain;
    StarkExTypes.ApprovalChainData availabilityVerifiersChain;

    // Batch id of last accepted proof.
    uint256 lastBatchId;                            // NOLINT: constable-states uninitialized-state.

    // Mapping between sub-contract index to sub-contract address.
    mapping(uint256 =\u003e address) subContracts;       // NOLINT: uninitialized-state.
}
"},"MFreezable.sol":{"content":"/*
  Copyright 2019,2020 StarkWare Industries Ltd.

  Licensed under the Apache License, Version 2.0 (the \"License\").
  You may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  https://www.starkware.co/open-source-license/

  Unless required by applicable law or agreed to in writing,
  software distributed under the License is distributed on an \"AS IS\" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions
  and limitations under the License.
*/
pragma solidity ^0.5.2;

contract MFreezable {
    /*
      Forbids calling the function if the exchange is frozen.
    */
    modifier notFrozen()
    {
        // Pure modifier declarations are not supported. Instead we provide
        // a dummy definition.
        revert(\"UNIMPLEMENTED\");
        _;
    }

    /*
      Allows calling the function only if the exchange is frozen.
    */
    modifier onlyFrozen()
    {
        // Pure modifier declarations are not supported. Instead we provide
        // a dummy definition.
        revert(\"UNIMPLEMENTED\");
        _;
    }

    /*
      Freezes the exchange.
    */
    function freeze()
        internal;

    /*
      Returns true if the exchange is frozen.
    */
    function isFrozen()
        external view
        returns (bool);

}
"},"MGovernance.sol":{"content":"/*
  Copyright 2019,2020 StarkWare Industries Ltd.

  Licensed under the Apache License, Version 2.0 (the \"License\").
  You may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  https://www.starkware.co/open-source-license/

  Unless required by applicable law or agreed to in writing,
  software distributed under the License is distributed on an \"AS IS\" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions
  and limitations under the License.
*/
pragma solidity ^0.5.2;

contract MGovernance {
    /*
      Allows calling the function only by a Governor.
    */
    modifier onlyGovernance()
    {
        // Pure modifier declarations are not supported. Instead we provide
        // a dummy definition.
        revert(\"UNIMPLEMENTED\");
        _;
    }
}
"},"MKeyGetters.sol":{"content":"/*
  Copyright 2019,2020 StarkWare Industries Ltd.

  Licensed under the Apache License, Version 2.0 (the \"License\").
  You may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  https://www.starkware.co/open-source-license/

  Unless required by applicable law or agreed to in writing,
  software distributed under the License is distributed on an \"AS IS\" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions
  and limitations under the License.
*/
pragma solidity ^0.5.2;

contract MKeyGetters {
    // NOLINTNEXTLINE: external-function.
    function getEthKey(uint256 starkKey) public view returns (address ethKey);

    function isMsgSenderStarkKeyOwner(uint256 starkKey) internal view returns (bool);

    /*
      Allows calling the function only if starkKey is registered to msg.sender.
    */
    modifier isSenderStarkKey(uint256 starkKey)
    {
        // Pure modifier declarations are not supported. Instead we provide
        // a dummy definition.
        revert(\"UNIMPLEMENTED\");
        _;
    }
}
"},"MOperator.sol":{"content":"/*
  Copyright 2019,2020 StarkWare Industries Ltd.

  Licensed under the Apache License, Version 2.0 (the \"License\").
  You may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  https://www.starkware.co/open-source-license/

  Unless required by applicable law or agreed to in writing,
  software distributed under the License is distributed on an \"AS IS\" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions
  and limitations under the License.
*/
pragma solidity ^0.5.2;

contract MOperator {

    modifier onlyOperator()
    {
        // Pure modifier declarations are not supported. Instead we provide
        // a dummy definition.
        revert(\"UNIMPLEMENTED\");
        _;
    }

    function registerOperator(address newOperator)
        external;

    function unregisterOperator(address removedOperator)
        external;

}
"},"MTokenAssetData.sol":{"content":"/*
  Copyright 2019,2020 StarkWare Industries Ltd.

  Licensed under the Apache License, Version 2.0 (the \"License\").
  You may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  https://www.starkware.co/open-source-license/

  Unless required by applicable law or agreed to in writing,
  software distributed under the License is distributed on an \"AS IS\" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions
  and limitations under the License.
*/
pragma solidity ^0.5.2;

contract MTokenAssetData {

    // NOLINTNEXTLINE: external-function.
    function getAssetInfo(uint256 assetType)
        public
        view
        returns (bytes memory assetInfo);

    function extractTokenSelector(bytes memory assetInfo)
        internal
        pure
        returns (bytes4 selector);

    function isEther(uint256 assetType)
        internal
        view
        returns (bool);

    function isMintableAssetType(uint256 assetType)
        internal
        view
        returns (bool);

    function extractContractAddress(bytes memory assetInfo)
        internal
        pure
        returns (address _contract);

    function calculateNftAssetId(uint256 assetType, uint256 tokenId)
        internal
        pure
        returns(uint256 assetId);

    function calculateMintableAssetId(uint256 assetType, bytes memory mintingBlob)
        internal
        pure
        returns(uint256 assetId);

}
"},"MTokenQuantization.sol":{"content":"/*
  Copyright 2019,2020 StarkWare Industries Ltd.

  Licensed under the Apache License, Version 2.0 (the \"License\").
  You may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  https://www.starkware.co/open-source-license/

  Unless required by applicable law or agreed to in writing,
  software distributed under the License is distributed on an \"AS IS\" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions
  and limitations under the License.
*/
pragma solidity ^0.5.2;

contract MTokenQuantization {
    function fromQuantized(uint256 presumedAssetType, uint256 quantizedAmount)
        internal
        view
        returns (uint256 amount);

    // NOLINTNEXTLINE: external-function.
    function getQuantum(uint256 presumedAssetType)
        public
        view
        returns (uint256 quantum);

    function toQuantized(uint256 presumedAssetType, uint256 amount)
        internal
        view
        returns (uint256 quantizedAmount);
}
"},"MTokens.sol":{"content":"/*
  Copyright 2019,2020 StarkWare Industries Ltd.

  Licensed under the Apache License, Version 2.0 (the \"License\").
  You may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  https://www.starkware.co/open-source-license/

  Unless required by applicable law or agreed to in writing,
  software distributed under the License is distributed on an \"AS IS\" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions
  and limitations under the License.
*/
pragma solidity ^0.5.2;

contract MTokens {
    function transferIn(uint256 assetType, uint256 quantizedAmount) internal;

    function transferInNft(uint256 assetType, uint256 tokenId) internal;

    function transferOut(address payable recipient, uint256 assetType, uint256 quantizedAmount)
        internal;

    function transferOutNft(address recipient, uint256 assetType, uint256 tokenId) internal;

    function transferOutMint(
        uint256 assetType, uint256 quantizedAmount, bytes memory mintingBlob) internal;
}
"},"Operator.sol":{"content":"/*
  Copyright 2019,2020 StarkWare Industries Ltd.

  Licensed under the Apache License, Version 2.0 (the \"License\").
  You may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  https://www.starkware.co/open-source-license/

  Unless required by applicable law or agreed to in writing,
  software distributed under the License is distributed on an \"AS IS\" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions
  and limitations under the License.
*/
pragma solidity ^0.5.2;

import \"MOperator.sol\";
import \"MGovernance.sol\";
import \"MainStorage.sol\";

/**
  The Operator of the contract is the entity entitled to submit state update requests
  by calling :sol:func:`updateState`.

  An Operator may be instantly appointed or removed by the contract Governor
  (see :sol:mod:`MainGovernance`). Typically, the Operator is the hot wallet of the StarkEx service
  submitting proofs for state updates.
*/
contract Operator is MainStorage, MGovernance, MOperator {
    event LogOperatorAdded(address operator);
    event LogOperatorRemoved(address operator);

    function initialize()
        internal
    {
        operators[msg.sender] = true;
        emit LogOperatorAdded(msg.sender);
    }

    modifier onlyOperator()
    {
        require(operators[msg.sender], \"ONLY_OPERATOR\");
        _;
    }

    function registerOperator(address newOperator)
        external
        onlyGovernance
    {
        operators[newOperator] = true;
        emit LogOperatorAdded(newOperator);
    }

    function unregisterOperator(address removedOperator)
        external
        onlyGovernance
    {
        operators[removedOperator] = false;
        emit LogOperatorRemoved(removedOperator);
    }

    function isOperator(address testedOperator) external view returns (bool) {
        return operators[testedOperator];
    }
}
"},"ProxyStorage.sol":{"content":"/*
  Copyright 2019,2020 StarkWare Industries Ltd.

  Licensed under the Apache License, Version 2.0 (the \"License\").
  You may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  https://www.starkware.co/open-source-license/

  Unless required by applicable law or agreed to in writing,
  software distributed under the License is distributed on an \"AS IS\" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions
  and limitations under the License.
*/
pragma solidity ^0.5.2;

import \"GovernanceStorage.sol\";

/*
  Holds the Proxy-specific state variables.
  This contract is inherited by the GovernanceStorage (and indirectly by MainStorage)
  to prevent collision hazard.
*/
contract ProxyStorage is GovernanceStorage {

    // Stores the hash of the initialization vector of the added implementation.
    // Upon upgradeTo the implementation, the initialization vector is verified
    // to be identical to the one submitted when adding the implementation.
    mapping (address =\u003e bytes32) internal initializationHash;

    // The time after which we can switch to the implementation.
    mapping (address =\u003e uint256) internal enabledTime;

    // A central storage of the flags whether implementation has been initialized.
    // Note - it can be used flexibly enough to accommodate multiple levels of initialization
    // (i.e. using different key salting schemes for different initialization levels).
    mapping (bytes32 =\u003e bool) internal initialized;
}
"},"SubContractor.sol":{"content":"/*
  Copyright 2019,2020 StarkWare Industries Ltd.

  Licensed under the Apache License, Version 2.0 (the \"License\").
  You may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  https://www.starkware.co/open-source-license/

  Unless required by applicable law or agreed to in writing,
  software distributed under the License is distributed on an \"AS IS\" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions
  and limitations under the License.
*/
pragma solidity ^0.5.2;

import \"Identity.sol\";

contract SubContractor is Identity {

    function initialize(bytes calldata data)
        external;

    function initializerSize()
        external view
        returns(uint256);

}
"},"TokenAssetData.sol":{"content":"/*
  Copyright 2019,2020 StarkWare Industries Ltd.

  Licensed under the Apache License, Version 2.0 (the \"License\").
  You may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  https://www.starkware.co/open-source-license/

  Unless required by applicable law or agreed to in writing,
  software distributed under the License is distributed on an \"AS IS\" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions
  and limitations under the License.
*/
pragma solidity ^0.5.2;

import \"MainStorage.sol\";
import \"MTokenAssetData.sol\";
import \"LibConstants.sol\";

contract TokenAssetData is MainStorage, LibConstants, MTokenAssetData {
    bytes4 internal constant ERC20_SELECTOR = bytes4(keccak256(\"ERC20Token(address)\"));
    bytes4 internal constant ETH_SELECTOR = bytes4(keccak256(\"ETH()\"));
    bytes4 internal constant ERC721_SELECTOR = bytes4(keccak256(\"ERC721Token(address,uint256)\"));
    bytes4 internal constant MINTABLE_ERC20_SELECTOR =
    bytes4(keccak256(\"MintableERC20Token(address)\"));
    bytes4 internal constant MINTABLE_ERC721_SELECTOR =
    bytes4(keccak256(\"MintableERC721Token(address,uint256)\"));

    // The selector follows the 0x20 bytes assetInfo.length field.
    uint256 internal constant SELECTOR_OFFSET = 0x20;
    uint256 internal constant SELECTOR_SIZE = 4;
    uint256 internal constant TOKEN_CONTRACT_ADDRESS_OFFSET = SELECTOR_OFFSET + SELECTOR_SIZE;
    string internal constant NFT_ASSET_ID_PREFIX = \"NFT:\";
    string internal constant MINTABLE_PREFIX = \"MINTABLE:\";

    /*
      Extract the tokenSelector from assetInfo.

      Works like bytes4 tokenSelector = abi.decode(assetInfo, (bytes4))
      but does not revert when assetInfo.length \u003c SELECTOR_OFFSET.
    */
    function extractTokenSelector(bytes memory assetInfo) internal pure
        returns (bytes4 selector) {
        // solium-disable-next-line security/no-inline-assembly
        assembly {
            selector := and(
                0xffffffff00000000000000000000000000000000000000000000000000000000,
                mload(add(assetInfo, SELECTOR_OFFSET))
            )
        }
    }

    function getAssetInfo(uint256 assetType) public view returns (bytes memory assetInfo) {
        // Verify that the registration is set and valid.
        require(registeredAssetType[assetType], \"ASSET_TYPE_NOT_REGISTERED\");

        // Retrieve registration.
        assetInfo = assetTypeToAssetInfo[assetType];
    }

    function isEther(uint256 assetType) internal view returns (bool) {
        return extractTokenSelector(getAssetInfo(assetType)) == ETH_SELECTOR;
    }

    function isMintableAssetType(uint256 assetType) internal view returns (bool) {
        bytes4 tokenSelector = extractTokenSelector(getAssetInfo(assetType));
        return
            tokenSelector == MINTABLE_ERC20_SELECTOR ||
            tokenSelector == MINTABLE_ERC721_SELECTOR;
    }

    function extractContractAddress(bytes memory assetInfo)
        internal pure returns (address _contract) {
        uint256 offset = TOKEN_CONTRACT_ADDRESS_OFFSET;
        uint256 res;
        // solium-disable-next-line security/no-inline-assembly
        assembly {
            res := mload(add(assetInfo, offset))
        }
        _contract = address(res);
    }

    function calculateNftAssetId(uint256 assetType, uint256 tokenId)
        internal
        pure
        returns(uint256 assetId) {
        assetId = uint256(keccak256(abi.encodePacked(NFT_ASSET_ID_PREFIX, assetType, tokenId))) \u0026
            MASK_250;
    }

    function calculateMintableAssetId(uint256 assetType, bytes memory mintingBlob)
        internal
        pure
        returns(uint256 assetId) {
        uint256 blobHash = uint256(keccak256(mintingBlob));
        assetId = (uint256(keccak256(abi.encodePacked(MINTABLE_PREFIX ,assetType, blobHash)))
            \u0026 MASK_240) | MINTABLE_ASSET_ID_FLAG;
    }

}
"},"TokenQuantization.sol":{"content":"/*
  Copyright 2019,2020 StarkWare Industries Ltd.

  Licensed under the Apache License, Version 2.0 (the \"License\").
  You may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  https://www.starkware.co/open-source-license/

  Unless required by applicable law or agreed to in writing,
  software distributed under the License is distributed on an \"AS IS\" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions
  and limitations under the License.
*/
pragma solidity ^0.5.2;

import \"MainStorage.sol\";
import \"MTokenQuantization.sol\";


contract TokenQuantization is MainStorage, MTokenQuantization {

    function fromQuantized(uint256 presumedAssetType, uint256 quantizedAmount)
        internal view returns (uint256 amount) {
        uint256 quantum = getQuantum(presumedAssetType);
        amount = quantizedAmount * quantum;
        require(amount / quantum == quantizedAmount, \"DEQUANTIZATION_OVERFLOW\");
    }

    function getQuantum(uint256 presumedAssetType) public view returns (uint256 quantum) {
        if (!registeredAssetType[presumedAssetType]) {
            // Default quantization, for NFTs etc.
            quantum = 1;
        } else {
            // Retrieve registration.
            quantum = assetTypeToQuantum[presumedAssetType];
        }
    }

    function toQuantized(uint256 presumedAssetType, uint256 amount)
        internal view returns (uint256 quantizedAmount) {
        uint256 quantum = getQuantum(presumedAssetType);
        require(amount % quantum == 0, \"INVALID_AMOUNT\");
        quantizedAmount = amount / quantum;
    }
}
"},"Tokens.sol":{"content":"/*
  Copyright 2019,2020 StarkWare Industries Ltd.

  Licensed under the Apache License, Version 2.0 (the \"License\").
  You may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  https://www.starkware.co/open-source-license/

  Unless required by applicable law or agreed to in writing,
  software distributed under the License is distributed on an \"AS IS\" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions
  and limitations under the License.
*/
pragma solidity ^0.5.2;

import \"Common.sol\";
import \"LibConstants.sol\";
import \"MGovernance.sol\";
import \"MTokens.sol\";
import \"TokenAssetData.sol\";
import \"TokenQuantization.sol\";
import \"IERC20.sol\";
import \"MainStorage.sol\";

/**
  Registration of a new token (:sol:func:`registerToken`) entails defining a new asset type within
  the system, and associating it with an `assetInfo` array of
  bytes and a quantization factor (`quantum`).

  The `assetInfo` is a byte array, with a size depending on the token.
  For ETH, assetInfo is 4 bytes long. For ERC20 tokens, it is 36 bytes long.

  For each token type, the following constant 4-byte hash is defined, called the `selector`:

   | `ETH_SELECTOR = bytes4(keccak256(\"ETH()\"));`
   | `ERC20_SELECTOR = bytes4(keccak256(\"ERC20Token(address)\"));`
   | `ERC721_SELECTOR = bytes4(keccak256(\"ERC721Token(address,uint256)\"));`
   | `MINTABLE_ERC20_SELECTOR = bytes4(keccak256(\"MintableERC20Token(address)\"));`
   | `MINTABLE_ERC721_SELECTOR = bytes4(keccak256(\"MintableERC721Token(address,uint256)\"));`

  For each token type, `assetInfo` is defined as follows:


  The `quantum` quantization factor defines the multiplicative transformation from the native token
  denomination as a 256b unsigned integer to a 63b unsigned integer representation as used by the
  Stark exchange. Only amounts in the native representation that represent an integer number of
  quanta are allowed in the system.

  The asset type is restricted to be the result of a hash of the `assetInfo` and the
  `quantum` masked to 250 bits (to be less than the prime used) according to the following formula:

  | ``uint256 assetType = uint256(keccak256(abi.encodePacked(assetInfo, quantum))) \u0026``
  | ``0x03FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;``

  Once registered, tokens cannot be removed from the system, as their IDs may be used by off-chain
  accounts.

  New tokens may only be registered by a Token Administrator. A Token Administrator may be instantly
  appointed or removed by the contract Governor (see :sol:mod:`MainGovernance`). Typically, the
  Token Administrator\u0027s private key should be kept in a cold wallet.
*/
contract Tokens is
    MainStorage,
    LibConstants,
    MGovernance,
    TokenQuantization,
    TokenAssetData,
    MTokens
{
    event LogTokenRegistered(uint256 assetType, bytes assetInfo);
    event LogTokenAdminAdded(address tokenAdmin);
    event LogTokenAdminRemoved(address tokenAdmin);

    using Addresses for address;
    using Addresses for address payable;

    modifier onlyTokensAdmin() {
        require(tokenAdmins[msg.sender], \"ONLY_TOKENS_ADMIN\");
        _;
    }

    function registerTokenAdmin(address newAdmin) external onlyGovernance() {
        tokenAdmins[newAdmin] = true;
        emit LogTokenAdminAdded(newAdmin);
    }

    function unregisterTokenAdmin(address oldAdmin) external onlyGovernance() {
        tokenAdmins[oldAdmin] = false;
        emit LogTokenAdminRemoved(oldAdmin);
    }

    function isTokenAdmin(address testedAdmin) external view returns (bool) {
        return tokenAdmins[testedAdmin];
    }


    function registerToken(uint256 assetType, bytes calldata assetInfo) external {
        registerToken(assetType, assetInfo, 1);
    }

    /*
      Registers a new asset to the system.
      Once added, it can not be removed and there is a limited number
      of slots available.
    */
    function registerToken(
        uint256 assetType,
        bytes memory assetInfo,
        uint256 quantum
    ) public onlyTokensAdmin() {
        // Make sure it is not invalid or already registered.
        require(!registeredAssetType[assetType], \"ASSET_ALREADY_REGISTERED\");
        require(assetType \u003c K_MODULUS, \"INVALID_ASSET_TYPE\");
        require(quantum \u003e 0, \"INVALID_QUANTUM\");
        require(quantum \u003c= MAX_QUANTUM, \"INVALID_QUANTUM\");
        require(assetInfo.length \u003e= SELECTOR_SIZE, \"INVALID_ASSET_STRING\");

        // Require that the assetType is the hash of the assetInfo and quantum truncated to 250 bits.
        uint256 enforcedId = uint256(keccak256(abi.encodePacked(assetInfo, quantum))) \u0026 MASK_250;
        require(assetType == enforcedId, \"INVALID_ASSET_TYPE\");

        // Add token to the in-storage structures.
        registeredAssetType[assetType] = true;
        assetTypeToAssetInfo[assetType] = assetInfo;
        assetTypeToQuantum[assetType] = quantum;

        bytes4 tokenSelector = extractTokenSelector(assetInfo);

        // Ensure the selector is of an asset type we know.
        require(
            tokenSelector == ETH_SELECTOR ||
            tokenSelector == ERC20_SELECTOR ||
            tokenSelector == ERC721_SELECTOR ||
            tokenSelector == MINTABLE_ERC20_SELECTOR ||
            tokenSelector == MINTABLE_ERC721_SELECTOR,
            \"UNSUPPORTED_TOKEN_TYPE\"
        );

        if (tokenSelector == ETH_SELECTOR) {
            // Assset info for ETH assetType is only a selector, i.e. 4 bytes length.
            require(assetInfo.length == 4, \"INVALID_ASSET_STRING\");
        } else {
            // Assset info for other asset types are a selector + uint256 concatanation.
            // We pass the address as a uint256 (zero padded),
            // thus its length is 0x04 + 0x20 = 0x24.
            require(assetInfo.length == 0x24, \"INVALID_ASSET_STRING\");
            address tokenAddress = extractContractAddress(assetInfo);
            require(tokenAddress.isContract(), \"BAD_TOKEN_ADDRESS\");
            if (tokenSelector == ERC721_SELECTOR || tokenSelector == MINTABLE_ERC721_SELECTOR) {
                require(quantum == 1, \"INVALID_NFT_QUANTUM\");
            }
        }

        // Log the registration of a new token.
        emit LogTokenRegistered(assetType, assetInfo);
    }

    /*
      Transfers funds from msg.sender to the exchange.
    */
    function transferIn(uint256 assetType, uint256 quantizedAmount) internal {
        bytes memory assetInfo = getAssetInfo(assetType);
        uint256 amount = fromQuantized(assetType, quantizedAmount);

        bytes4 tokenSelector = extractTokenSelector(assetInfo);
        if (tokenSelector == ERC20_SELECTOR) {
            address tokenAddress = extractContractAddress(assetInfo);
            IERC20 token = IERC20(tokenAddress);
            uint256 exchangeBalanceBefore = token.balanceOf(address(this));
            token.transferFrom(msg.sender, address(this), amount); // NOLINT: unused-return.
            uint256 exchangeBalanceAfter = token.balanceOf(address(this));
            require(exchangeBalanceAfter \u003e= exchangeBalanceBefore, \"OVERFLOW\");
            // NOLINTNEXTLINE(incorrect-equality): strict equality needed.
            require(
                exchangeBalanceAfter == exchangeBalanceBefore + amount,
                \"INCORRECT_AMOUNT_TRANSFERRED\");
        } else if (tokenSelector == ETH_SELECTOR) {
            require(msg.value == amount, \"INCORRECT_DEPOSIT_AMOUNT\");
        } else {
            revert(\"UNSUPPORTED_TOKEN_TYPE\");
        }
    }

    function transferInNft(uint256 assetType, uint256 tokenId) internal {
        bytes memory assetInfo = getAssetInfo(assetType);

        bytes4 tokenSelector = extractTokenSelector(assetInfo);
        require(tokenSelector == ERC721_SELECTOR, \"NOT_ERC721_TOKEN\");
        address tokenAddress = extractContractAddress(assetInfo);
        tokenAddress.safeTokenContractCall(
            abi.encodeWithSignature(
                \"safeTransferFrom(address,address,uint256)\",
                msg.sender,
                address(this),
                tokenId
            )
        );
    }

    /*
      Transfers funds from the exchange to recipient.
    */
    function transferOut(
        address payable recipient,
        uint256 assetType,
        uint256 quantizedAmount
    ) internal {
        bytes memory assetInfo = getAssetInfo(assetType);
        uint256 amount = fromQuantized(assetType, quantizedAmount);

        bytes4 tokenSelector = extractTokenSelector(assetInfo);
        if (tokenSelector == ERC20_SELECTOR) {
            address tokenAddress = extractContractAddress(assetInfo);
            IERC20 token = IERC20(tokenAddress);
            uint256 exchangeBalanceBefore = token.balanceOf(address(this));
            token.transfer(recipient, amount); // NOLINT: unused-return.
            uint256 exchangeBalanceAfter = token.balanceOf(address(this));
            require(exchangeBalanceAfter \u003c= exchangeBalanceBefore, \"UNDERFLOW\");
            // NOLINTNEXTLINE(incorrect-equality): strict equality needed.
            require(
                exchangeBalanceAfter == exchangeBalanceBefore - amount,
                \"INCORRECT_AMOUNT_TRANSFERRED\");
        } else if (tokenSelector == ETH_SELECTOR) {
            recipient.performEthTransfer(amount);
        } else {
            revert(\"UNSUPPORTED_TOKEN_TYPE\");
        }
    }

    /*
      Transfers NFT from the exchange to recipient.
    */
    function transferOutNft(address recipient, uint256 assetType, uint256 tokenId) internal {
        bytes memory assetInfo = getAssetInfo(assetType);
        bytes4 tokenSelector = extractTokenSelector(assetInfo);
        require(tokenSelector == ERC721_SELECTOR, \"NOT_ERC721_TOKEN\");
        address tokenAddress = extractContractAddress(assetInfo);
        tokenAddress.safeTokenContractCall(
            abi.encodeWithSignature(
                \"safeTransferFrom(address,address,uint256)\",
                address(this),
                recipient,
                tokenId
            )
        );
    }

    function transferOutMint(
        uint256 assetType,
        uint256 quantizedAmount,
        bytes memory mintingBlob) internal {
        uint256 amount = fromQuantized(assetType, quantizedAmount);
        address tokenAddress = extractContractAddress(getAssetInfo(assetType));
        tokenAddress.safeTokenContractCall(
            abi.encodeWithSignature(
                \"mintFor(address,uint256,bytes)\",
                msg.sender, amount, mintingBlob)
        );
    }
}
"},"TokensAndRamping.sol":{"content":"/*
  Copyright 2019,2020 StarkWare Industries Ltd.

  Licensed under the Apache License, Version 2.0 (the \"License\").
  You may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  https://www.starkware.co/open-source-license/

  Unless required by applicable law or agreed to in writing,
  software distributed under the License is distributed on an \"AS IS\" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions
  and limitations under the License.
*/
pragma solidity ^0.5.2;

import \"ERC721Receiver.sol\";
import \"Freezable.sol\";
import \"KeyGetters.sol\";
import \"Operator.sol\";
import \"Tokens.sol\";
import \"Users.sol\";
import \"MainGovernance.sol\";
import \"AcceptModifications.sol\";
import \"Deposits.sol\";
import \"FullWithdrawals.sol\";
import \"Withdrawals.sol\";
import \"SubContractor.sol\";

contract TokensAndRamping is
    ERC721Receiver,
    SubContractor,
    Operator,
    Freezable,
    MainGovernance,
    AcceptModifications,
    Tokens,
    KeyGetters,
    Users,
    Deposits,
    Withdrawals,
    FullWithdrawals
{
    function initialize(bytes calldata /* data */)
        external {
        revert(\"NOT_IMPLEMENTED\");
    }

    function initializerSize()
        external view
        returns(uint256){
        return 0;
    }

    function identify()
        external pure
        returns(string memory){
        return \"StarkWare_TokensAndRamping_2020_1\";
    }
}
"},"Users.sol":{"content":"/*
  Copyright 2019,2020 StarkWare Industries Ltd.

  Licensed under the Apache License, Version 2.0 (the \"License\").
  You may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  https://www.starkware.co/open-source-license/

  Unless required by applicable law or agreed to in writing,
  software distributed under the License is distributed on an \"AS IS\" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions
  and limitations under the License.
*/
pragma solidity ^0.5.2;

import \"MainStorage.sol\";
import \"LibConstants.sol\";
import \"MGovernance.sol\";
import \"MKeyGetters.sol\";

/**
  Users of the Stark Exchange are identified within the exchange by their Stark Key which is a
  public key defined over a Stark-friendly elliptic curve that is different from the standard
  Ethereum elliptic curve. These keys may be generated using the same private key used by the user
  on Ethereum.

  The Stark-friendly elliptic curve used is defined as follows:

  .. math:: y^2 = (x^3 + \\alpha \\cdot x + \\beta) \\% p

  where:

  .. math:: \\alpha = 1
  .. math:: \\beta = 3141592653589793238462643383279502884197169399375105820974944592307816406665
  .. math:: p = 3618502788666131213697322783095070105623107215331596699973092056135872020481

  In order to associate exchange users with Ethereum account addresses, an Ethereum address must be
  registered with the Stark Key on the exchange contract before any other user operation can take
  place.
  User registration is performed by calling :sol:func:`registerUser` with the selected Stark Key,
  representing an `x` coordinate on the Stark-friendly elliptic curve, and the `y` coordinate of
  the key on the curve (due to the nature of the curve, only two such possible `y` coordinates
  exist).

  The registration is accepted if the following holds:

  1. The key registered is not zero and has not been registered in the past by the user or anyone else.
  2. The key provided represents a valid point on the Stark-friendly elliptic curve.
  3. The linkage between the provided Ethereum key and the selected Stark Key is signed by the User Admin (typically the exchange operator).

  If the above holds, the Stark Key is registered by the contract, mapping it to the Ethereum key.
  This mapping is later used to ensure that withdrawals from accounts mapped to the Stark Keys can
  only be performed by users authenticated with the associated Ethereum public keys (see :sol:mod:`Withdrawals`).
*/
contract Users is MainStorage, LibConstants, MGovernance, MKeyGetters {
    event LogUserRegistered(address ethKey, uint256 starkKey, address sender);
    event LogUserAdminAdded(address userAdmin);
    event LogUserAdminRemoved(address userAdmin);

    function isOnCurve(uint256 starkKey) private view returns (bool) {
        uint256 xCubed = mulmod(mulmod(starkKey, starkKey, K_MODULUS), starkKey, K_MODULUS);
        return isQuadraticResidue(addmod(addmod(xCubed, starkKey, K_MODULUS), K_BETA, K_MODULUS));
    }

    function registerUserAdmin(address newAdmin) external onlyGovernance() {
        userAdmins[newAdmin] = true;
        emit LogUserAdminAdded(newAdmin);
    }

    function unregisterUserAdmin(address oldAdmin) external onlyGovernance() {
        userAdmins[oldAdmin] = false;
        emit LogUserAdminRemoved(oldAdmin);
    }

    function isUserAdmin(address testedAdmin) public view returns (bool) {
        return userAdmins[testedAdmin];
    }

    function registerUser(address ethKey, uint256 starkKey, bytes calldata signature) external {
        // Validate keys and availability.
        require(starkKey != 0, \"INVALID_STARK_KEY\");
        require(starkKey \u003c K_MODULUS, \"INVALID_STARK_KEY\");
        require(ethKey != ZERO_ADDRESS, \"INVALID_ETH_ADDRESS\");
        require(ethKeys[starkKey] == ZERO_ADDRESS, \"STARK_KEY_UNAVAILABLE\");
        require(isOnCurve(starkKey), \"INVALID_STARK_KEY\");
        require(signature.length == 65, \"INVALID_SIGNATURE\");

        bytes32 signedData = keccak256(abi.encodePacked(\"UserRegistration:\", ethKey, starkKey));

        bytes memory sig = signature;
        uint8 v = uint8(sig[64]);
        bytes32 r;
        bytes32 s;

        // solium-disable-next-line security/no-inline-assembly
        assembly {
            r := mload(add(sig, 32))
            s := mload(add(sig, 64))
        }

        address signer = ecrecover(signedData, v, r, s);
        require(isUserAdmin(signer), \"INVALID_SIGNATURE\");

        // Update state.
        ethKeys[starkKey] = ethKey;

        // Log new user.
        emit LogUserRegistered(ethKey, starkKey, msg.sender);
    }

    function fieldPow(uint256 base, uint256 exponent) internal view returns (uint256) {
        // solium-disable-next-line security/no-low-level-calls
        // NOLINTNEXTLINE: low-level-calls reentrancy-events reentrancy-no-eth.
        (bool success, bytes memory returndata) = address(5).staticcall(
            abi.encode(0x20, 0x20, 0x20, base, exponent, K_MODULUS)
        );
        require(success, string(returndata));
        return abi.decode(returndata, (uint256));
    }

    function isQuadraticResidue(uint256 fieldElement) private view returns (bool) {
        return 1 == fieldPow(fieldElement, ((K_MODULUS - 1) / 2));
    }
}
"},"Withdrawals.sol":{"content":"/*
  Copyright 2019,2020 StarkWare Industries Ltd.

  Licensed under the Apache License, Version 2.0 (the \"License\").
  You may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  https://www.starkware.co/open-source-license/

  Unless required by applicable law or agreed to in writing,
  software distributed under the License is distributed on an \"AS IS\" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions
  and limitations under the License.
*/
pragma solidity ^0.5.2;

import \"MAcceptModifications.sol\";
import \"MTokenQuantization.sol\";
import \"MTokenAssetData.sol\";
import \"MFreezable.sol\";
import \"MOperator.sol\";
import \"MKeyGetters.sol\";
import \"MTokens.sol\";
import \"MainStorage.sol\";

/**
  For a user to perform a withdrawal operation from the Stark Exchange during normal operation
  two calls are required:

  1. A call to an offchain exchange API, requesting a withdrawal from a user account (vault).
  2. A call to the on-chain :sol:func:`withdraw` function to perform the actual withdrawal of funds transferring them to the users Eth or ERC20 account (depending on the token type).

  For simplicity, hereafter it is assumed that all tokens are ERC20 tokens but the text below
  applies to Eth in the same manner.

  In the first call mentioned above, anyone can call the API to request the withdrawal of an
  amount from a given vault. Following the request, the exchange may include the withdrawal in a
  STARK proof. The submission of a proof then results in the addition of the amount(s) withdrawn to
  an on-chain pending withdrawals account under the stark key of the vault owner and the appropriate
  asset ID. At the same time, this also implies that this amount is deducted from the off-chain
  vault.

  Once the amount to be withdrawn has been transfered to the on-chain pending withdrawals account,
  the user may perform the second call mentioned above to complete the transfer of funds from the
  Stark Exchange contract to the appropriate ERC20 account. Only a user holding the Eth key
  corresponding to the Stark Key of a pending withdrawals account may perform this operation.

  It is possible that for multiple withdrawal calls to the API, a single withdrawal call to the
  contract may retrieve all funds, as long as they are all for the same asset ID.

  The result of the operation, assuming all requirements are met, is that an amount of ERC20 tokens
  in the pending withdrawal account times the quantization factor is transferred to the ERC20
  account of the user.

  A withdrawal request cannot be cancelled. Once funds reach the pending withdrawals account
  on-chain, they cannot be moved back into an off-chain vault before completion of the withdrawal
  to the ERC20 account of the user.

  In the event that the exchange reaches a frozen state the user may perform a withdrawal operation
  via an alternative flow, known as the \"Escape\" flow. In this flow, the API call above is replaced
  with an :sol:func:`escape` call to the on-chain contract (see :sol:mod:`Escapes`) proving the
  ownership of off-chain funds. If such proof is accepted, the user may proceed as above with
  the :sol:func:`withdraw` call to the contract to complete the operation.
*/
contract Withdrawals is MainStorage, MAcceptModifications, MTokenQuantization, MTokenAssetData,
                        MFreezable, MOperator, MKeyGetters, MTokens  {
    event LogWithdrawalPerformed(
        uint256 starkKey,
        uint256 assetType,
        uint256 nonQuantizedAmount,
        uint256 quantizedAmount,
        address recipient
    );

    event LogNftWithdrawalPerformed(
        uint256 starkKey,
        uint256 assetType,
        uint256 tokenId,
        uint256 assetId,
        address recipient
    );

    event LogMintWithdrawalPerformed(
        uint256 starkKey,
        uint256 tokenId,
        uint256 nonQuantizedAmount,
        uint256 quantizedAmount,
        uint256 assetId
    );

    function getWithdrawalBalance(
        uint256 starkKey,
        uint256 assetId
    )
        external
        view
        returns (uint256 balance)
    {
        uint256 presumedAssetType = assetId;
        balance = fromQuantized(presumedAssetType, pendingWithdrawals[starkKey][assetId]);
    }

    /*
      Allows a user to withdraw accepted funds to a recipient\u0027s account.
      This function can be called normally while frozen.
    */
    function withdrawTo(uint256 starkKey, uint256 assetType, address payable recipient)
        public
        isSenderStarkKey(starkKey)
    // No notFrozen modifier: This function can always be used, even when frozen.
    {
        require(!isMintableAssetType(assetType), \"MINTABLE_ASSET_TYPE\");
        uint256 assetId = assetType;
        // Fetch and clear quantized amount.
        uint256 quantizedAmount = pendingWithdrawals[starkKey][assetId];
        pendingWithdrawals[starkKey][assetId] = 0;

        // Transfer funds.
        transferOut(recipient, assetType, quantizedAmount);
        emit LogWithdrawalPerformed(
            starkKey,
            assetType,
            fromQuantized(assetType, quantizedAmount),
            quantizedAmount,
            recipient
        );
    }

    /*
      Allows a user to withdraw accepted funds to its own account.
      This function can be called normally while frozen.
    */
    function withdraw(uint256 starkKey, uint256 assetType)
        external
    // No notFrozen modifier: This function can always be used, even when frozen.
    {
        withdrawTo(starkKey, assetType, msg.sender);
    }

    /*
      Allows a user to withdraw an accepted NFT to a recipient\u0027s account.
      This function can be called normally while frozen.
    */
    function withdrawNftTo(
        uint256 starkKey,
        uint256 assetType,
        uint256 tokenId,
        address recipient
    )
        public
        isSenderStarkKey(starkKey)
    // No notFrozen modifier: This function can always be used, even when frozen.
    {
        // Calculate assetId.
        uint256 assetId = calculateNftAssetId(assetType, tokenId);
        require(!isMintableAssetType(assetType), \"MINTABLE_ASSET_TYPE\");
        if (pendingWithdrawals[starkKey][assetId] \u003e 0) {
            require(pendingWithdrawals[starkKey][assetId] == 1, \"ILLEGAL_NFT_BALANCE\");
            pendingWithdrawals[starkKey][assetId] = 0;

            // Transfer funds.
            transferOutNft(recipient, assetType, tokenId);
            emit LogNftWithdrawalPerformed(starkKey, assetType, tokenId, assetId, recipient);
        }
    }

    /*
      Allows a user to withdraw an accepted NFT to its own account.
      This function can be called normally while frozen.
    */
    function withdrawNft(
        uint256 starkKey,
        uint256 assetType,
        uint256 tokenId
    )
        external
    // No notFrozen modifier: This function can always be used, even when frozen.
    {
        withdrawNftTo(starkKey, assetType, tokenId, msg.sender);
    }

    function withdrawAndMint(
        uint256 starkKey,
        uint256 assetType,
        bytes calldata mintingBlob
    ) external isSenderStarkKey(starkKey) {
        require(registeredAssetType[assetType], \"INVALID_ASSET_TYPE\");
        require(isMintableAssetType(assetType), \"NON_MINTABLE_ASSET_TYPE\");
        uint256 assetId = calculateMintableAssetId(assetType, mintingBlob);
        if (pendingWithdrawals[starkKey][assetId] \u003e 0) {
            uint256 quantizedAmount = pendingWithdrawals[starkKey][assetId];
            pendingWithdrawals[starkKey][assetId] = 0;
            // Transfer funds.
            transferOutMint(assetType, quantizedAmount, mintingBlob);
            emit LogMintWithdrawalPerformed(
                starkKey, assetType, fromQuantized(assetType, quantizedAmount), quantizedAmount,
                assetId);
        }
    }
}

