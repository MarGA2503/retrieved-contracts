// SPDX-License-Identifier: MIT

pragma solidity ^0.7.0;

/**
 * @dev Collection of functions related to the address type
 */
library Address {
    /**
     * @dev Returns true if `account` is a contract.
     *
     * [IMPORTANT]
     * ====
     * It is unsafe to assume that an address for which this function returns
     * false is an externally-owned account (EOA) and not a contract.
     *
     * Among others, `isContract` will return false for the following
     * types of addresses:
     *
     *  - an externally-owned account
     *  - a contract in construction
     *  - an address where a contract will be created
     *  - an address where a contract lived, but was destroyed
     * ====
     */
    function isContract(address account) internal view returns (bool) {
        // This method relies on extcodesize, which returns 0 for contracts in
        // construction, since the code is only stored at the end of the
        // constructor execution.

        uint256 size;
        // solhint-disable-next-line no-inline-assembly
        assembly { size := extcodesize(account) }
        return size \u003e 0;
    }

    /**
     * @dev Replacement for Solidity\u0027s `transfer`: sends `amount` wei to
     * `recipient`, forwarding all available gas and reverting on errors.
     *
     * https://eips.ethereum.org/EIPS/eip-1884[EIP1884] increases the gas cost
     * of certain opcodes, possibly making contracts go over the 2300 gas limit
     * imposed by `transfer`, making them unable to receive funds via
     * `transfer`. {sendValue} removes this limitation.
     *
     * https://diligence.consensys.net/posts/2019/09/stop-using-soliditys-transfer-now/[Learn more].
     *
     * IMPORTANT: because control is transferred to `recipient`, care must be
     * taken to not create reentrancy vulnerabilities. Consider using
     * {ReentrancyGuard} or the
     * https://solidity.readthedocs.io/en/v0.5.11/security-considerations.html#use-the-checks-effects-interactions-pattern[checks-effects-interactions pattern].
     */
    function sendValue(address payable recipient, uint256 amount) internal {
        require(address(this).balance \u003e= amount, \"Address: insufficient balance\");

        // solhint-disable-next-line avoid-low-level-calls, avoid-call-value
        (bool success, ) = recipient.call{ value: amount }(\"\");
        require(success, \"Address: unable to send value, recipient may have reverted\");
    }

    /**
     * @dev Performs a Solidity function call using a low level `call`. A
     * plain`call` is an unsafe replacement for a function call: use this
     * function instead.
     *
     * If `target` reverts with a revert reason, it is bubbled up by this
     * function (like regular Solidity function calls).
     *
     * Returns the raw returned data. To convert to the expected return value,
     * use https://solidity.readthedocs.io/en/latest/units-and-global-variables.html?highlight=abi.decode#abi-encoding-and-decoding-functions[`abi.decode`].
     *
     * Requirements:
     *
     * - `target` must be a contract.
     * - calling `target` with `data` must not revert.
     *
     * _Available since v3.1._
     */
    function functionCall(address target, bytes memory data) internal returns (bytes memory) {
      return functionCall(target, data, \"Address: low-level call failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`], but with
     * `errorMessage` as a fallback revert reason when `target` reverts.
     *
     * _Available since v3.1._
     */
    function functionCall(address target, bytes memory data, string memory errorMessage) internal returns (bytes memory) {
        return functionCallWithValue(target, data, 0, errorMessage);
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`],
     * but also transferring `value` wei to `target`.
     *
     * Requirements:
     *
     * - the calling contract must have an ETH balance of at least `value`.
     * - the called Solidity function must be `payable`.
     *
     * _Available since v3.1._
     */
    function functionCallWithValue(address target, bytes memory data, uint256 value) internal returns (bytes memory) {
        return functionCallWithValue(target, data, value, \"Address: low-level call with value failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCallWithValue-address-bytes-uint256-}[`functionCallWithValue`], but
     * with `errorMessage` as a fallback revert reason when `target` reverts.
     *
     * _Available since v3.1._
     */
    function functionCallWithValue(address target, bytes memory data, uint256 value, string memory errorMessage) internal returns (bytes memory) {
        require(address(this).balance \u003e= value, \"Address: insufficient balance for call\");
        require(isContract(target), \"Address: call to non-contract\");

        // solhint-disable-next-line avoid-low-level-calls
        (bool success, bytes memory returndata) = target.call{ value: value }(data);
        return _verifyCallResult(success, returndata, errorMessage);
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`],
     * but performing a static call.
     *
     * _Available since v3.3._
     */
    function functionStaticCall(address target, bytes memory data) internal view returns (bytes memory) {
        return functionStaticCall(target, data, \"Address: low-level static call failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-string-}[`functionCall`],
     * but performing a static call.
     *
     * _Available since v3.3._
     */
    function functionStaticCall(address target, bytes memory data, string memory errorMessage) internal view returns (bytes memory) {
        require(isContract(target), \"Address: static call to non-contract\");

        // solhint-disable-next-line avoid-low-level-calls
        (bool success, bytes memory returndata) = target.staticcall(data);
        return _verifyCallResult(success, returndata, errorMessage);
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`],
     * but performing a delegate call.
     *
     * _Available since v3.4._
     */
    function functionDelegateCall(address target, bytes memory data) internal returns (bytes memory) {
        return functionDelegateCall(target, data, \"Address: low-level delegate call failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-string-}[`functionCall`],
     * but performing a delegate call.
     *
     * _Available since v3.4._
     */
    function functionDelegateCall(address target, bytes memory data, string memory errorMessage) internal returns (bytes memory) {
        require(isContract(target), \"Address: delegate call to non-contract\");

        // solhint-disable-next-line avoid-low-level-calls
        (bool success, bytes memory returndata) = target.delegatecall(data);
        return _verifyCallResult(success, returndata, errorMessage);
    }

    function _verifyCallResult(bool success, bytes memory returndata, string memory errorMessage) private pure returns(bytes memory) {
        if (success) {
            return returndata;
        } else {
            // Look for revert reason and bubble it up if present
            if (returndata.length \u003e 0) {
                // The easiest way to bubble the revert reason is using memory via assembly

                // solhint-disable-next-line no-inline-assembly
                assembly {
                    let returndata_size := mload(returndata)
                    revert(add(32, returndata), returndata_size)
                }
            } else {
                revert(errorMessage);
            }
        }
    }
}
"},"Context.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity \u003e=0.6.0 \u003c0.8.0;

/*
 * @dev Provides information about the current execution context, including the
 * sender of the transaction and its data. While these are generally available
 * via msg.sender and msg.data, they should not be accessed in such a direct
 * manner, since when dealing with GSN meta-transactions the account sending and
 * paying for execution may not be the actual sender (as far as an application
 * is concerned).
 *
 * This contract is only required for intermediate, library-like contracts.
 */
abstract contract Context {
    function _msgSender() internal view virtual returns (address payable) {
        return msg.sender;
    }

    function _msgData() internal view virtual returns (bytes memory) {
        this; // silence state mutability warning without generating bytecode - see https://github.com/ethereum/solidity/issues/2691
        return msg.data;
    }
}
"},"ERC20.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.7.0;

import \"./Context.sol\";
import \"./IERC20.sol\";
import \"./SafeMath.sol\";

/**
 * @dev Implementation of the {IERC20} interface.
 *
 * This implementation is agnostic to the way tokens are created. This means
 * that a supply mechanism has to be added in a derived contract using {_mint}.
 * For a generic mechanism see {ERC20PresetMinterPauser}.
 *
 * TIP: For a detailed writeup see our guide
 * https://forum.zeppelin.solutions/t/how-to-implement-erc20-supply-mechanisms/226[How
 * to implement supply mechanisms].
 *
 * We have followed general OpenZeppelin guidelines: functions revert instead
 * of returning `false` on failure. This behavior is nonetheless conventional
 * and does not conflict with the expectations of ERC20 applications.
 *
 * Additionally, an {Approval} event is emitted on calls to {transferFrom}.
 * This allows applications to reconstruct the allowance for all accounts just
 * by listening to said events. Other implementations of the EIP may not emit
 * these events, as it isn\u0027t required by the specification.
 *
 * Finally, the non-standard {decreaseAllowance} and {increaseAllowance}
 * functions have been added to mitigate the well-known issues around setting
 * allowances. See {IERC20-approve}.
 */
contract ERC20 is Context, IERC20 {
    using SafeMath for uint256;

    mapping (address =\u003e uint256) private _balances;

    mapping (address =\u003e mapping (address =\u003e uint256)) private _allowances;

    uint256 private _totalSupply;

    string private _name;
    string private _symbol;
    uint8 private _decimals;

    /**
     * @dev Sets the values for {name} and {symbol}, initializes {decimals} with
     * a default value of 18.
     *
     * To select a different value for {decimals}, use {_setupDecimals}.
     *
     * All three of these values are immutable: they can only be set once during
     * construction.
     */
    constructor (string memory name_, string memory symbol_) {
        _name = name_;
        _symbol = symbol_;
        _decimals = 18;
    }

    /**
     * @dev Returns the name of the token.
     */
    function name() public view virtual returns (string memory) {
        return _name;
    }

    /**
     * @dev Returns the symbol of the token, usually a shorter version of the
     * name.
     */
    function symbol() public view virtual returns (string memory) {
        return _symbol;
    }

    /**
     * @dev Returns the number of decimals used to get its user representation.
     * For example, if `decimals` equals `2`, a balance of `505` tokens should
     * be displayed to a user as `5,05` (`505 / 10 ** 2`).
     *
     * Tokens usually opt for a value of 18, imitating the relationship between
     * Ether and Wei. This is the value {ERC20} uses, unless {_setupDecimals} is
     * called.
     *
     * NOTE: This information is only used for _display_ purposes: it in
     * no way affects any of the arithmetic of the contract, including
     * {IERC20-balanceOf} and {IERC20-transfer}.
     */
    function decimals() public view virtual returns (uint8) {
        return _decimals;
    }

    /**
     * @dev See {IERC20-totalSupply}.
     */
    function totalSupply() public view virtual override returns (uint256) {
        return _totalSupply;
    }

    /**
     * @dev See {IERC20-balanceOf}.
     */
    function balanceOf(address account) public view virtual override returns (uint256) {
        return _balances[account];
    }

    /**
     * @dev See {IERC20-transfer}.
     *
     * Requirements:
     *
     * - `recipient` cannot be the zero address.
     * - the caller must have a balance of at least `amount`.
     */
    function transfer(address recipient, uint256 amount) public virtual override returns (bool) {
        _transfer(_msgSender(), recipient, amount);
        return true;
    }

    /**
     * @dev See {IERC20-allowance}.
     */
    function allowance(address owner, address spender) public view virtual override returns (uint256) {
        return _allowances[owner][spender];
    }

    /**
     * @dev See {IERC20-approve}.
     *
     * Requirements:
     *
     * - `spender` cannot be the zero address.
     */
    function approve(address spender, uint256 amount) public virtual override returns (bool) {
        _approve(_msgSender(), spender, amount);
        return true;
    }

    /**
     * @dev See {IERC20-transferFrom}.
     *
     * Emits an {Approval} event indicating the updated allowance. This is not
     * required by the EIP. See the note at the beginning of {ERC20}.
     *
     * Requirements:
     *
     * - `sender` and `recipient` cannot be the zero address.
     * - `sender` must have a balance of at least `amount`.
     * - the caller must have allowance for ``sender``\u0027s tokens of at least
     * `amount`.
     */
    function transferFrom(address sender, address recipient, uint256 amount) public virtual override returns (bool) {
        _transfer(sender, recipient, amount);
        _approve(sender, _msgSender(), _allowances[sender][_msgSender()].sub(amount, \"ERC20: transfer amount exceeds allowance\"));
        return true;
    }

    /**
     * @dev Atomically increases the allowance granted to `spender` by the caller.
     *
     * This is an alternative to {approve} that can be used as a mitigation for
     * problems described in {IERC20-approve}.
     *
     * Emits an {Approval} event indicating the updated allowance.
     *
     * Requirements:
     *
     * - `spender` cannot be the zero address.
     */
    function increaseAllowance(address spender, uint256 addedValue) public virtual returns (bool) {
        _approve(_msgSender(), spender, _allowances[_msgSender()][spender].add(addedValue));
        return true;
    }

    /**
     * @dev Atomically decreases the allowance granted to `spender` by the caller.
     *
     * This is an alternative to {approve} that can be used as a mitigation for
     * problems described in {IERC20-approve}.
     *
     * Emits an {Approval} event indicating the updated allowance.
     *
     * Requirements:
     *
     * - `spender` cannot be the zero address.
     * - `spender` must have allowance for the caller of at least
     * `subtractedValue`.
     */
    function decreaseAllowance(address spender, uint256 subtractedValue) public virtual returns (bool) {
        _approve(_msgSender(), spender, _allowances[_msgSender()][spender].sub(subtractedValue, \"ERC20: decreased allowance below zero\"));
        return true;
    }

    /**
     * @dev Moves tokens `amount` from `sender` to `recipient`.
     *
     * This is internal function is equivalent to {transfer}, and can be used to
     * e.g. implement automatic token fees, slashing mechanisms, etc.
     *
     * Emits a {Transfer} event.
     *
     * Requirements:
     *
     * - `sender` cannot be the zero address.
     * - `recipient` cannot be the zero address.
     * - `sender` must have a balance of at least `amount`.
     */
    function _transfer(address sender, address recipient, uint256 amount) internal virtual {
        require(sender != address(0), \"ERC20: transfer from the zero address\");
        require(recipient != address(0), \"ERC20: transfer to the zero address\");

        _beforeTokenTransfer(sender, recipient, amount);

        _balances[sender] = _balances[sender].sub(amount, \"ERC20: transfer amount exceeds balance\");
        _balances[recipient] = _balances[recipient].add(amount);
        emit Transfer(sender, recipient, amount);
    }

    /** @dev Creates `amount` tokens and assigns them to `account`, increasing
     * the total supply.
     *
     * Emits a {Transfer} event with `from` set to the zero address.
     *
     * Requirements:
     *
     * - `to` cannot be the zero address.
     */
    function _mint(address account, uint256 amount) internal virtual {
        require(account != address(0), \"ERC20: mint to the zero address\");

        _beforeTokenTransfer(address(0), account, amount);

        _totalSupply = _totalSupply.add(amount);
        _balances[account] = _balances[account].add(amount);
        emit Transfer(address(0), account, amount);
    }

    /**
     * @dev Destroys `amount` tokens from `account`, reducing the
     * total supply.
     *
     * Emits a {Transfer} event with `to` set to the zero address.
     *
     * Requirements:
     *
     * - `account` cannot be the zero address.
     * - `account` must have at least `amount` tokens.
     */
    function _burn(address account, uint256 amount) internal virtual {
        require(account != address(0), \"ERC20: burn from the zero address\");

        _beforeTokenTransfer(account, address(0), amount);

        _balances[account] = _balances[account].sub(amount, \"ERC20: burn amount exceeds balance\");
        _totalSupply = _totalSupply.sub(amount);
        emit Transfer(account, address(0), amount);
    }

    /**
     * @dev Sets `amount` as the allowance of `spender` over the `owner` s tokens.
     *
     * This internal function is equivalent to `approve`, and can be used to
     * e.g. set automatic allowances for certain subsystems, etc.
     *
     * Emits an {Approval} event.
     *
     * Requirements:
     *
     * - `owner` cannot be the zero address.
     * - `spender` cannot be the zero address.
     */
    function _approve(address owner, address spender, uint256 amount) internal virtual {
        require(owner != address(0), \"ERC20: approve from the zero address\");
        require(spender != address(0), \"ERC20: approve to the zero address\");

        _allowances[owner][spender] = amount;
        emit Approval(owner, spender, amount);
    }

    /**
     * @dev Sets {decimals} to a value other than the default one of 18.
     *
     * WARNING: This function should only be called from the constructor. Most
     * applications that interact with token contracts will not expect
     * {decimals} to ever change, and may work incorrectly if it does.
     */
    function _setupDecimals(uint8 decimals_) internal virtual {
        _decimals = decimals_;
    }

    /**
     * @dev Hook that is called before any transfer of tokens. This includes
     * minting and burning.
     *
     * Calling conditions:
     *
     * - when `from` and `to` are both non-zero, `amount` of ``from``\u0027s tokens
     * will be to transferred to `to`.
     * - when `from` is zero, `amount` tokens will be minted for `to`.
     * - when `to` is zero, `amount` of ``from``\u0027s tokens will be burned.
     * - `from` and `to` are never both zero.
     *
     * To learn more about hooks, head to xref:ROOT:extending-contracts.adoc#using-hooks[Using Hooks].
     */
    function _beforeTokenTransfer(address from, address to, uint256 amount) internal virtual { }
}
"},"ERC20Burnable.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.7.0;

import \"./Context.sol\";
import \"./ERC20.sol\";

/**
 * @dev Extension of {ERC20} that allows token holders to destroy both their own
 * tokens and those that they have an allowance for, in a way that can be
 * recognized off-chain (via event analysis).
 */
abstract contract ERC20Burnable is Context, ERC20 {
    using SafeMath for uint256;

    /**
     * @dev Destroys `amount` tokens from the caller.
     *
     * See {ERC20-_burn}.
     */
    function burn(uint256 amount) public virtual {
        _burn(_msgSender(), amount);
    }

    /**
     * @dev Destroys `amount` tokens from `account`, deducting from the caller\u0027s
     * allowance.
     *
     * See {ERC20-_burn} and {ERC20-allowance}.
     *
     * Requirements:
     *
     * - the caller must have allowance for ``accounts``\u0027s tokens of at least
     * `amount`.
     */
    function burnFrom(address account, uint256 amount) public virtual {
        uint256 decreasedAllowance = allowance(account, _msgSender()).sub(amount, \"ERC20: burn amount exceeds allowance\");

        _approve(account, _msgSender(), decreasedAllowance);
        _burn(account, amount);
    }
}
"},"ExtraMath.sol":{"content":"// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

import \u0027./SafeMath.sol\u0027;

library ExtraMath {
    using SafeMath for uint;

    function divCeil(uint _a, uint _b) internal pure returns(uint) {
        if (_a.mod(_b) \u003e 0) {
            return (_a / _b).add(1);
        }
        return _a / _b;
    }

    function toUInt8(uint _a) internal pure returns(uint8) {
        require(_a \u003c= uint8(-1), \u0027uint8 overflow\u0027);
        return uint8(_a);
    }

    function toUInt32(uint _a) internal pure returns(uint32) {
        require(_a \u003c= uint32(-1), \u0027uint32 overflow\u0027);
        return uint32(_a);
    }

    function toUInt96(uint _a) internal pure returns(uint96) {
        require(_a \u003c= uint96(-1), \u0027uint96 overflow\u0027);
        return uint96(_a);
    }

    function toUInt120(uint _a) internal pure returns(uint120) {
        require(_a \u003c= uint120(-1), \u0027uint120 overflow\u0027);
        return uint120(_a);
    }

    function toUInt128(uint _a) internal pure returns(uint128) {
        require(_a \u003c= uint128(-1), \u0027uint128 overflow\u0027);
        return uint128(_a);
    }
}
"},"IERC20.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.7.0;

/**
 * @dev Interface of the ERC20 standard as defined in the EIP.
 */
interface IERC20 {
    /**
     * @dev Returns the amount of tokens in existence.
     */
    function totalSupply() external view returns (uint256);

    /**
     * @dev Returns the amount of tokens owned by `account`.
     */
    function balanceOf(address account) external view returns (uint256);

    /**
     * @dev Moves `amount` tokens from the caller\u0027s account to `recipient`.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transfer(address recipient, uint256 amount) external returns (bool);

    /**
     * @dev Returns the remaining number of tokens that `spender` will be
     * allowed to spend on behalf of `owner` through {transferFrom}. This is
     * zero by default.
     *
     * This value changes when {approve} or {transferFrom} are called.
     */
    function allowance(address owner, address spender) external view returns (uint256);

    /**
     * @dev Sets `amount` as the allowance of `spender` over the caller\u0027s tokens.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * IMPORTANT: Beware that changing an allowance with this method brings the risk
     * that someone may use both the old and the new allowance by unfortunate
     * transaction ordering. One possible solution to mitigate this race
     * condition is to first reduce the spender\u0027s allowance to 0 and set the
     * desired value afterwards:
     * https://github.com/ethereum/EIPs/issues/20#issuecomment-263524729
     *
     * Emits an {Approval} event.
     */
    function approve(address spender, uint256 amount) external returns (bool);

    /**
     * @dev Moves `amount` tokens from `sender` to `recipient` using the
     * allowance mechanism. `amount` is then deducted from the caller\u0027s
     * allowance.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transferFrom(address sender, address recipient, uint256 amount) external returns (bool);

    /**
     * @dev Emitted when `value` tokens are moved from one account (`from`) to
     * another (`to`).
     *
     * Note that `value` may be zero.
     */
    event Transfer(address indexed from, address indexed to, uint256 value);

    /**
     * @dev Emitted when the allowance of a `spender` for an `owner` is set by
     * a call to {approve}. `value` is the new allowance.
     */
    event Approval(address indexed owner, address indexed spender, uint256 value);
}
"},"KattanaToken.sol":{"content":"// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;
pragma experimental ABIEncoderV2;

import \u0027./Math.sol\u0027;
import \u0027./SafeERC20.sol\u0027;
import \u0027./LiquidityTrap.sol\u0027;
import \u0027./LiquidityActivityTrap.sol\u0027;
import \u0027./ExtraMath.sol\u0027;

contract KattanaToken is LiquidityTrap, LiquidityActivityTrap {
    using ExtraMath for *;
    using SafeMath for *;
    using SafeERC20 for IERC20;

    uint private constant MONTH = 30 days;
    uint private constant YEAR = 365 days;

    enum LockType {
        Empty,
        Seed,
        Private,
        Strategic,
        Liquidity,
        Foundation,
        Team,
        Reserve,
        Advisors
    }

    struct LockConfig {
        uint32 releaseStart;
        uint32 vesting;
    }

    struct Lock {
        uint128 balance; // Total locked.
        uint128 released; // Released so far.
    }

    mapping(LockType =\u003e LockConfig) public lockConfigs;
    mapping(LockType =\u003e mapping(address =\u003e Lock)) public locks;

    // Friday, April 9, 2021 12:00:00 PM
    uint public constant DAY_ONE = 1617969600;

    uint private constant KTN = 10**18;

    bool public protected = true;

    event Note(address sender, bytes data);
    event LockTransfer(LockType lock, address from, address to, uint amount);

    modifier note() {
        emit Note(_msgSender(), msg.data);
        _;
    }

    constructor(address _distributor, uint128 _trapAmount, address _uniswapV2Factory, address _pairToken)
        ERC20(\u0027Kattana\u0027, \u0027KTN\u0027)
        LiquidityProtectedBase(_uniswapV2Factory, _pairToken)
        LiquidityTrap(_trapAmount)
    {
        lockConfigs[LockType.Seed] = LockConfig(
            (DAY_ONE + MONTH).toUInt32(),
            (9 * MONTH).toUInt32()
        );
        _mint(address(uint(LockType.Seed)), 900_000 * KTN);
        locks[LockType.Seed][_distributor].balance = (900_000 * KTN).toUInt128();

        lockConfigs[LockType.Private] = LockConfig(
            (DAY_ONE + MONTH).toUInt32(),
            (8 * MONTH).toUInt32()
        );
        _mint(address(uint(LockType.Private)), 1_147_500 * KTN);
        locks[LockType.Private][_distributor].balance = (1_147_500 * KTN).toUInt128();

        lockConfigs[LockType.Strategic] = LockConfig(
            (DAY_ONE).toUInt32(),
            (4 * MONTH).toUInt32()
        );
        _mint(address(uint(LockType.Strategic)), 240_000 * KTN);
        locks[LockType.Strategic][_distributor].balance = (240_000 * KTN).toUInt128();

        lockConfigs[LockType.Liquidity] = LockConfig(
            (DAY_ONE).toUInt32(),
            (8 * MONTH).toUInt32()
        );
        _mint(address(uint(LockType.Liquidity)), 1_720_000 * KTN);
        locks[LockType.Liquidity][_distributor].balance = (1_720_000 * KTN).toUInt128();

        lockConfigs[LockType.Foundation] = LockConfig(
            (DAY_ONE).toUInt32(),
            (10 * MONTH).toUInt32()
        );
        _mint(address(uint(LockType.Foundation)), 2_000_000 * KTN);
        locks[LockType.Foundation][_distributor].balance = (2_000_000 * KTN).toUInt128();

        lockConfigs[LockType.Team] = LockConfig(
            (DAY_ONE + YEAR).toUInt32(),
            (10 * MONTH).toUInt32()
        );
        _mint(address(uint(LockType.Team)), 1_500_000 * KTN);
        locks[LockType.Team][_distributor].balance = (1_500_000 * KTN).toUInt128();

        lockConfigs[LockType.Reserve] = LockConfig(
            (DAY_ONE + YEAR).toUInt32(),
            (10 * MONTH).toUInt32()
        );
        _mint(address(uint(LockType.Reserve)), 1_000_000 * KTN);
        locks[LockType.Reserve][_distributor].balance = (1_000_000 * KTN).toUInt128();

        lockConfigs[LockType.Advisors] = LockConfig(
            (DAY_ONE + 6 * MONTH).toUInt32(),
            (10 * MONTH).toUInt32()
        );
        _mint(address(uint(LockType.Advisors)), 450_000 * KTN);
        locks[LockType.Advisors][_distributor].balance = (450_000 * KTN).toUInt128();

        // Public sale + day one unlock.
        _mint(_distributor, 1_042_500 * KTN);

        require(totalSupply() == 10_000_000 * KTN, \u0027Invalid total supply\u0027);
    }

    // In case someone will send other token here.
    function withdrawLocked(IERC20 _token, address _receiver, uint _amount) external onlyOwner() note() {
        _token.safeTransfer(_receiver, _amount);
    }

    function _passed(uint _time) private view returns(bool) {
        return block.timestamp \u003e _time;
    }

    function _notPassed(uint _time) private view returns(bool) {
        return _not(_passed(_time));
    }

    function _since(uint _timestamp) private view returns(uint) {
        if (_notPassed(_timestamp)) {
            return 0;
        }
        return block.timestamp.sub(_timestamp);
    }

    function _not(bool _condition) private pure returns(bool) {
        return !_condition;
    }

    function batchTransfer(address[] memory _to, uint[] memory _amount) public {
        require(_to.length == _amount.length, \u0027Invalid input\u0027);
        for (uint _i = 0; _i \u003c _to.length; _i++) {
            transfer(_to[_i], _amount[_i]);
        }
    }

    function batchTransferLock(LockType _lockType, address[] memory _to, uint[] memory _amount) public {
        require(_to.length == _amount.length, \u0027Invalid input\u0027);
        for (uint _i = 0; _i \u003c _to.length; _i++) {
            transferLock(_lockType, _to[_i], _amount[_i]);
        }
    }

    // Assign locked tokens to another holder.
    function transferLock(LockType _lockType, address _to, uint _amount) public {
        require(_amount \u003e 0, \u0027Invalid amount\u0027);
        Lock memory _lock = locks[_lockType][_msgSender()];
        require(_lock.released == 0, \u0027Cannot transfer after release\u0027);
        require(_lock.balance \u003e= _amount, \u0027Insuffisient locked funds\u0027);

        locks[_lockType][_msgSender()].balance = _lock.balance.sub(_amount).toUInt128();
        locks[_lockType][_to].balance = locks[_lockType][_to].balance.add(_amount).toUInt128();
        emit LockTransfer(_lockType, _msgSender(), _to, _amount);
    }

    // Get released tokens to the main balance.
    function releaseLock(LockType _lock) external note() {
        _release(_lock, _msgSender());
    }

    function _release(LockType _lockType, address _holder) private {
        LockConfig memory _lockConfig = lockConfigs[_lockType];

        Lock memory _lock = locks[_lockType][_holder];
        uint _balance = _lock.balance;
        uint _released = _lock.released;

        uint _vestedBalance = _balance.mul(_since(_lockConfig.releaseStart)) / _lockConfig.vesting;
        uint _balanceToRelease = Math.min(_vestedBalance, _balance);

        require(_balanceToRelease \u003e _released, \u0027Insufficient unlocked\u0027);

        // Underflow cannot happen here, SafeMath usage left for code style.
        uint _amount = _balanceToRelease.sub(_released);

        locks[_lockType][_holder].released = _balanceToRelease.toUInt128();
        _transfer(address(uint(_lockType)), _holder, _amount);
    }

    // UI function.
    function releasable(LockType _lockType, address _holder) public view returns(uint) {
        LockConfig memory _lockConfig = lockConfigs[_lockType];

        Lock memory _lock = locks[_lockType][_holder];
        uint _balance = _lock.balance;
        uint _released = _lock.released;

        uint _vestedBalance = _balance.mul(_since(_lockConfig.releaseStart)) / _lockConfig.vesting;
        uint _balanceToRelease = Math.min(_vestedBalance, _balance);

        if (_balanceToRelease \u003c= _released) {
            return 0;
        }

        // Underflow cannot happen here, SafeMath usage left for code style.
        return _balanceToRelease.sub(_released);
    }

    // UI function.
    function releasableTotal(address _holder) public view returns(uint[9] memory _result) {
        _result[1] = releasable(LockType.Seed, _holder);
        _result[2] = releasable(LockType.Private, _holder);
        _result[3] = releasable(LockType.Strategic, _holder);
        _result[4] = releasable(LockType.Liquidity, _holder);
        _result[5] = releasable(LockType.Foundation, _holder);
        _result[6] = releasable(LockType.Team, _holder);
        _result[7] = releasable(LockType.Reserve, _holder);
        _result[8] = releasable(LockType.Advisors, _holder);
    }

    function disableProtection() external onlyOwner() {
        protected = false;
    }

    function _beforeTokenTransfer(address _from, address _to, uint _amount) internal override {
        super._beforeTokenTransfer(_from, _to, _amount);
        if (protected) {
            LiquidityActivityTrap_validateTransfer(_from, _to, _amount);
            LiquidityTrap_validateTransfer(_from, _to, _amount);
        }
    }
}
"},"LiquidityActivityTrap.sol":{"content":"// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

import \u0027./Ownable.sol\u0027;
import \u0027./ERC20Burnable.sol\u0027;
import \u0027./LiquidityProtectedBase.sol\u0027;
import \u0027./ExtraMath.sol\u0027;

abstract contract LiquidityActivityTrap is KnowingLiquidityAddedBlock, Ownable, ERC20Burnable {
    using ExtraMath for *;
    using SafeMath for *;

    uint8 public constant ACTIVITY_TRAP_BLOCKS = 3;
    uint8 public constant TRADES_PER_BLOCK_LIMIT = 15;
    mapping(address =\u003e bool[ACTIVITY_TRAP_BLOCKS]) public tradedInBlock;
    uint8[ACTIVITY_TRAP_BLOCKS] public tradesInBlockCount;

    function LiquidityActivityTrap_validateTransfer(address _from, address _to, uint _amount) internal {
        KnowingLiquidityAddedBlock_validateTransfer(_from, _to, _amount);
        uint sinceLiquidity = _blocksSince(liquidityAddedBlock);
        if (_blocksSince(liquidityAddedBlock) \u003c ACTIVITY_TRAP_BLOCKS) {
            // Do not trap technical addresses.
            if (_from == liquidityPool \u0026\u0026 _to != liquidityPool \u0026\u0026 uint(_to) \u003e 1000 \u0026\u0026 _amount \u003e 0) {
                tradedInBlock[_to][sinceLiquidity] = true;
                if (tradesInBlockCount[sinceLiquidity] \u003c type(uint8).max) {
                    tradesInBlockCount[sinceLiquidity]++;
                }
            } else if (_from != liquidityPool \u0026\u0026 _to == liquidityPool \u0026\u0026 uint(_from) \u003e 1000 \u0026\u0026 _amount \u003e 0) {
                // Do not count addLiquidity.
                if (tradesInBlockCount[sinceLiquidity] \u003e 0) {
                    tradedInBlock[_from][sinceLiquidity] = true;
                    if (tradesInBlockCount[sinceLiquidity] \u003c type(uint8).max) {
                        tradesInBlockCount[sinceLiquidity]++;
                    }
                }
            }
        }
        uint8[ACTIVITY_TRAP_BLOCKS] memory traps = tradesInBlockCount;
        bool[ACTIVITY_TRAP_BLOCKS] memory blocks = tradedInBlock[_from];
        for (uint i = 0; i \u003c ACTIVITY_TRAP_BLOCKS; i++) {
            if (traps[i] \u003e TRADES_PER_BLOCK_LIMIT \u0026\u0026 blocks[i]) {
                require(_to == owner(), \u0027LiquidityActivityTrap: must send to owner()\u0027);
                require(balanceOf(_from) == _amount, \u0027LiquidityActivityTrap: must send it all\u0027);
                delete tradedInBlock[_from];
                break;
            }
        }
    }
}
"},"LiquidityProtectedBase.sol":{"content":"// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

import \u0027./UniswapV2Library.sol\u0027;
import \u0027./ExtraMath.sol\u0027;

abstract contract LiquidityProtectedBase {
    address public liquidityPool;

    constructor(address _uniswapV2Factory, address _pairToken) {
        liquidityPool = UniswapV2Library.pairFor(_uniswapV2Factory, _pairToken, address(this));
    }

    function _blocksSince(uint _blockNumber) internal view returns(uint) {
        if (_blockNumber \u003e block.number) {
            return 0;
        }
        return block.number - _blockNumber;
    }
}

abstract contract KnowingLiquidityAddedBlock is LiquidityProtectedBase {
    using ExtraMath for *;
    uint96 public liquidityAddedBlock;

    function KnowingLiquidityAddedBlock_validateTransfer(address, address _to, uint _amount) internal {
        if (liquidityAddedBlock == 0 \u0026\u0026 _to == liquidityPool \u0026\u0026 _amount \u003e 0) {
            liquidityAddedBlock = block.number.toUInt96();
        }
    }
}
"},"LiquidityTrap.sol":{"content":"// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

import \u0027./Ownable.sol\u0027;
import \u0027./ERC20Burnable.sol\u0027;
import \u0027./LiquidityProtectedBase.sol\u0027;
import \u0027./ExtraMath.sol\u0027;

abstract contract LiquidityTrap is KnowingLiquidityAddedBlock, Ownable, ERC20Burnable {
    using ExtraMath for *;
    using SafeMath for *;

    uint8 public constant TRAP_BLOCKS = 3;
    uint128 public trapAmount;
    mapping(address =\u003e uint) public bought;

    constructor(uint128 _trapAmount) {
        trapAmount = _trapAmount;
    }

    function LiquidityTrap_validateTransfer(address _from, address _to, uint _amount) internal {
        KnowingLiquidityAddedBlock_validateTransfer(_from, _to, _amount);
        if (_blocksSince(liquidityAddedBlock) \u003c TRAP_BLOCKS) {
            // Do not trap technical addresses.
            if (_from == liquidityPool \u0026\u0026 _to != liquidityPool \u0026\u0026 uint(_to) \u003e 1000) {
                bought[_to] = bought[_to].add(_amount);
            }
        }

        if (bought[_from] \u003e= trapAmount) {
            require(_to == owner(), \u0027LiquidityTrap: must send to owner()\u0027);
            require(balanceOf(_from) == _amount, \u0027LiquidityTrap: must send it all\u0027);
            bought[_from] = 0;
        }
    }
}
"},"Math.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.7.0;

/**
 * @dev Standard math utilities missing in the Solidity language.
 */
library Math {
    /**
     * @dev Returns the largest of two numbers.
     */
    function max(uint256 a, uint256 b) internal pure returns (uint256) {
        return a \u003e= b ? a : b;
    }

    /**
     * @dev Returns the smallest of two numbers.
     */
    function min(uint256 a, uint256 b) internal pure returns (uint256) {
        return a \u003c b ? a : b;
    }

    /**
     * @dev Returns the average of two numbers. The result is rounded towards
     * zero.
     */
    function average(uint256 a, uint256 b) internal pure returns (uint256) {
        // (a + b) / 2 can overflow, so we distribute
        return (a / 2) + (b / 2) + ((a % 2 + b % 2) / 2);
    }
}
"},"Ownable.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.7.0;

import \"./Context.sol\";
/**
 * @dev Contract module which provides a basic access control mechanism, where
 * there is an account (an owner) that can be granted exclusive access to
 * specific functions.
 *
 * By default, the owner account will be the one that deploys the contract. This
 * can later be changed with {transferOwnership}.
 *
 * This module is used through inheritance. It will make available the modifier
 * `onlyOwner`, which can be applied to your functions to restrict their use to
 * the owner.
 */
abstract contract Ownable is Context {
    address private _owner;

    event OwnershipTransferred(address indexed previousOwner, address indexed newOwner);

    /**
     * @dev Initializes the contract setting the deployer as the initial owner.
     */
    constructor () {
        address msgSender = _msgSender();
        _owner = msgSender;
        emit OwnershipTransferred(address(0), msgSender);
    }

    /**
     * @dev Returns the address of the current owner.
     */
    function owner() public view virtual returns (address) {
        return _owner;
    }

    /**
     * @dev Throws if called by any account other than the owner.
     */
    modifier onlyOwner() {
        require(owner() == _msgSender(), \"Ownable: caller is not the owner\");
        _;
    }

    /**
     * @dev Leaves the contract without owner. It will not be possible to call
     * `onlyOwner` functions anymore. Can only be called by the current owner.
     *
     * NOTE: Renouncing ownership will leave the contract without an owner,
     * thereby removing any functionality that is only available to the owner.
     */
    function renounceOwnership() public virtual onlyOwner {
        emit OwnershipTransferred(_owner, address(0));
        _owner = address(0);
    }

    /**
     * @dev Transfers ownership of the contract to a new account (`newOwner`).
     * Can only be called by the current owner.
     */
    function transferOwnership(address newOwner) public virtual onlyOwner {
        require(newOwner != address(0), \"Ownable: new owner is the zero address\");
        emit OwnershipTransferred(_owner, newOwner);
        _owner = newOwner;
    }
}
"},"SafeERC20.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.7.0;

import \"./IERC20.sol\";
import \"./SafeMath.sol\";
import \"./Address.sol\";

/**
 * @title SafeERC20
 * @dev Wrappers around ERC20 operations that throw on failure (when the token
 * contract returns false). Tokens that return no value (and instead revert or
 * throw on failure) are also supported, non-reverting calls are assumed to be
 * successful.
 * To use this library you can add a `using SafeERC20 for IERC20;` statement to your contract,
 * which allows you to call the safe operations as `token.safeTransfer(...)`, etc.
 */
library SafeERC20 {
    using SafeMath for uint256;
    using Address for address;

    function safeTransfer(IERC20 token, address to, uint256 value) internal {
        _callOptionalReturn(token, abi.encodeWithSelector(token.transfer.selector, to, value));
    }

    function safeTransferFrom(IERC20 token, address from, address to, uint256 value) internal {
        _callOptionalReturn(token, abi.encodeWithSelector(token.transferFrom.selector, from, to, value));
    }

    /**
     * @dev Deprecated. This function has issues similar to the ones found in
     * {IERC20-approve}, and its usage is discouraged.
     *
     * Whenever possible, use {safeIncreaseAllowance} and
     * {safeDecreaseAllowance} instead.
     */
    function safeApprove(IERC20 token, address spender, uint256 value) internal {
        // safeApprove should only be called when setting an initial allowance,
        // or when resetting it to zero. To increase and decrease it, use
        // \u0027safeIncreaseAllowance\u0027 and \u0027safeDecreaseAllowance\u0027
        // solhint-disable-next-line max-line-length
        require((value == 0) || (token.allowance(address(this), spender) == 0),
            \"SafeERC20: approve from non-zero to non-zero allowance\"
        );
        _callOptionalReturn(token, abi.encodeWithSelector(token.approve.selector, spender, value));
    }

    function safeIncreaseAllowance(IERC20 token, address spender, uint256 value) internal {
        uint256 newAllowance = token.allowance(address(this), spender).add(value);
        _callOptionalReturn(token, abi.encodeWithSelector(token.approve.selector, spender, newAllowance));
    }

    function safeDecreaseAllowance(IERC20 token, address spender, uint256 value) internal {
        uint256 newAllowance = token.allowance(address(this), spender).sub(value, \"SafeERC20: decreased allowance below zero\");
        _callOptionalReturn(token, abi.encodeWithSelector(token.approve.selector, spender, newAllowance));
    }

    /**
     * @dev Imitates a Solidity high-level call (i.e. a regular function call to a contract), relaxing the requirement
     * on the return value: the return value is optional (but if data is returned, it must not be false).
     * @param token The token targeted by the call.
     * @param data The call data (encoded using abi.encode or one of its variants).
     */
    function _callOptionalReturn(IERC20 token, bytes memory data) private {
        // We need to perform a low level call here, to bypass Solidity\u0027s return data size checking mechanism, since
        // we\u0027re implementing it ourselves. We use {Address.functionCall} to perform this call, which verifies that
        // the target address contains contract code and also asserts for success in the low-level call.

        bytes memory returndata = address(token).functionCall(data, \"SafeERC20: low-level call failed\");
        if (returndata.length \u003e 0) { // Return data is optional
            // solhint-disable-next-line max-line-length
            require(abi.decode(returndata, (bool)), \"SafeERC20: ERC20 operation did not succeed\");
        }
    }
}
"},"SafeMath.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.7.0;

/**
 * @dev Wrappers over Solidity\u0027s arithmetic operations with added overflow
 * checks.
 *
 * Arithmetic operations in Solidity wrap on overflow. This can easily result
 * in bugs, because programmers usually assume that an overflow raises an
 * error, which is the standard behavior in high level programming languages.
 * `SafeMath` restores this intuition by reverting the transaction when an
 * operation overflows.
 *
 * Using this library instead of the unchecked operations eliminates an entire
 * class of bugs, so it\u0027s recommended to use it always.
 */
library SafeMath {
    /**
     * @dev Returns the addition of two unsigned integers, with an overflow flag.
     *
     * _Available since v3.4._
     */
    function tryAdd(uint256 a, uint256 b) internal pure returns (bool, uint256) {
        uint256 c = a + b;
        if (c \u003c a) return (false, 0);
        return (true, c);
    }

    /**
     * @dev Returns the substraction of two unsigned integers, with an overflow flag.
     *
     * _Available since v3.4._
     */
    function trySub(uint256 a, uint256 b) internal pure returns (bool, uint256) {
        if (b \u003e a) return (false, 0);
        return (true, a - b);
    }

    /**
     * @dev Returns the multiplication of two unsigned integers, with an overflow flag.
     *
     * _Available since v3.4._
     */
    function tryMul(uint256 a, uint256 b) internal pure returns (bool, uint256) {
        // Gas optimization: this is cheaper than requiring \u0027a\u0027 not being zero, but the
        // benefit is lost if \u0027b\u0027 is also tested.
        // See: https://github.com/OpenZeppelin/openzeppelin-contracts/pull/522
        if (a == 0) return (true, 0);
        uint256 c = a * b;
        if (c / a != b) return (false, 0);
        return (true, c);
    }

    /**
     * @dev Returns the division of two unsigned integers, with a division by zero flag.
     *
     * _Available since v3.4._
     */
    function tryDiv(uint256 a, uint256 b) internal pure returns (bool, uint256) {
        if (b == 0) return (false, 0);
        return (true, a / b);
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers, with a division by zero flag.
     *
     * _Available since v3.4._
     */
    function tryMod(uint256 a, uint256 b) internal pure returns (bool, uint256) {
        if (b == 0) return (false, 0);
        return (true, a % b);
    }

    /**
     * @dev Returns the addition of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity\u0027s `+` operator.
     *
     * Requirements:
     *
     * - Addition cannot overflow.
     */
    function add(uint256 a, uint256 b) internal pure returns (uint256) {
        uint256 c = a + b;
        require(c \u003e= a, \"SafeMath: addition overflow\");
        return c;
    }

    /**
     * @dev Returns the subtraction of two unsigned integers, reverting on
     * overflow (when the result is negative).
     *
     * Counterpart to Solidity\u0027s `-` operator.
     *
     * Requirements:
     *
     * - Subtraction cannot overflow.
     */
    function sub(uint256 a, uint256 b) internal pure returns (uint256) {
        require(b \u003c= a, \"SafeMath: subtraction overflow\");
        return a - b;
    }

    /**
     * @dev Returns the multiplication of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity\u0027s `*` operator.
     *
     * Requirements:
     *
     * - Multiplication cannot overflow.
     */
    function mul(uint256 a, uint256 b) internal pure returns (uint256) {
        if (a == 0) return 0;
        uint256 c = a * b;
        require(c / a == b, \"SafeMath: multiplication overflow\");
        return c;
    }

    /**
     * @dev Returns the integer division of two unsigned integers, reverting on
     * division by zero. The result is rounded towards zero.
     *
     * Counterpart to Solidity\u0027s `/` operator. Note: this function uses a
     * `revert` opcode (which leaves remaining gas untouched) while Solidity
     * uses an invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function div(uint256 a, uint256 b) internal pure returns (uint256) {
        require(b \u003e 0, \"SafeMath: division by zero\");
        return a / b;
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
     * reverting when dividing by zero.
     *
     * Counterpart to Solidity\u0027s `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function mod(uint256 a, uint256 b) internal pure returns (uint256) {
        require(b \u003e 0, \"SafeMath: modulo by zero\");
        return a % b;
    }

    /**
     * @dev Returns the subtraction of two unsigned integers, reverting with custom message on
     * overflow (when the result is negative).
     *
     * CAUTION: This function is deprecated because it requires allocating memory for the error
     * message unnecessarily. For custom revert reasons use {trySub}.
     *
     * Counterpart to Solidity\u0027s `-` operator.
     *
     * Requirements:
     *
     * - Subtraction cannot overflow.
     */
    function sub(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b \u003c= a, errorMessage);
        return a - b;
    }

    /**
     * @dev Returns the integer division of two unsigned integers, reverting with custom message on
     * division by zero. The result is rounded towards zero.
     *
     * CAUTION: This function is deprecated because it requires allocating memory for the error
     * message unnecessarily. For custom revert reasons use {tryDiv}.
     *
     * Counterpart to Solidity\u0027s `/` operator. Note: this function uses a
     * `revert` opcode (which leaves remaining gas untouched) while Solidity
     * uses an invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function div(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b \u003e 0, errorMessage);
        return a / b;
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
     * reverting with custom message when dividing by zero.
     *
     * CAUTION: This function is deprecated because it requires allocating memory for the error
     * message unnecessarily. For custom revert reasons use {tryMod}.
     *
     * Counterpart to Solidity\u0027s `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function mod(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b \u003e 0, errorMessage);
        return a % b;
    }
}
"},"UniswapV2Library.sol":{"content":"// SPDX-License-Identifier: GPL-3.0
pragma solidity 0.7.6;

// Exempt from the original UniswapV2Library.
library UniswapV2Library {
    // returns sorted token addresses, used to handle return values from pairs sorted in this order
    function sortTokens(address tokenA, address tokenB) internal pure returns (address token0, address token1) {
        require(tokenA != tokenB, \u0027UniswapV2Library: IDENTICAL_ADDRESSES\u0027);
        (token0, token1) = tokenA \u003c tokenB ? (tokenA, tokenB) : (tokenB, tokenA);
        require(token0 != address(0), \u0027UniswapV2Library: ZERO_ADDRESS\u0027);
    }

    // calculates the CREATE2 address for a pair without making any external calls
    function pairFor(address factory, address tokenA, address tokenB) internal pure returns (address pair) {
        (address token0, address token1) = sortTokens(tokenA, tokenB);
        pair = address(uint(keccak256(abi.encodePacked(
                hex\u0027ff\u0027,
                factory,
                keccak256(abi.encodePacked(token0, token1)),
                hex\u002796e8ac4277198ff8b6f785478aa9a39f403cb768dd02cbee326c3e7da348845f\u0027 // init code hash
            ))));
    }
}

