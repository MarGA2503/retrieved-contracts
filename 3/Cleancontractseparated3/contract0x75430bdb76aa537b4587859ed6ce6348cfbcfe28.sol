// SPDX-License-Identifier: MIT

pragma solidity 0.7.6;

/**
 * @dev Elliptic Curve Digital Signature Algorithm (ECDSA) operations.
 *
 * These functions can be used to verify that a message was signed by the holder
 * of the private keys of a given address.
 */
library ECDSA {
    enum RecoverError {
        NoError,
        InvalidSignature,
        InvalidSignatureLength,
        InvalidSignatureS,
        InvalidSignatureV
    }

    function _throwError(RecoverError error) private pure {
        if (error == RecoverError.NoError) {
            return; // no error: do nothing
        } else if (error == RecoverError.InvalidSignature) {
            revert(\"ECDSA: invalid signature\");
        } else if (error == RecoverError.InvalidSignatureLength) {
            revert(\"ECDSA: invalid signature length\");
        } else if (error == RecoverError.InvalidSignatureS) {
            revert(\"ECDSA: invalid signature \u0027s\u0027 value\");
        } else if (error == RecoverError.InvalidSignatureV) {
            revert(\"ECDSA: invalid signature \u0027v\u0027 value\");
        }
    }

    /**
     * @dev Returns the address that signed a hashed message (`hash`) with
     * `signature` or error string. This address can then be used for verification purposes.
     *
     * The `ecrecover` EVM opcode allows for malleable (non-unique) signatures:
     * this function rejects them by requiring the `s` value to be in the lower
     * half order, and the `v` value to be either 27 or 28.
     *
     * IMPORTANT: `hash` _must_ be the result of a hash operation for the
     * verification to be secure: it is possible to craft signatures that
     * recover to arbitrary addresses for non-hashed data. A safe way to ensure
     * this is by receiving a hash of the original message (which may otherwise
     * be too long), and then calling {toEthSignedMessageHash} on it.
     *
     * Documentation for signature generation:
     * - with https://web3js.readthedocs.io/en/v1.3.4/web3-eth-accounts.html#sign[Web3.js]
     * - with https://docs.ethers.io/v5/api/signer/#Signer-signMessage[ethers]
     *
     * _Available since v4.3._
     */
    function tryRecover(bytes32 hash, bytes memory signature) internal pure returns (address, RecoverError) {
        // Check the signature length
        // - case 65: r,s,v signature (standard)
        // - case 64: r,vs signature (cf https://eips.ethereum.org/EIPS/eip-2098) _Available since v4.1._
        if (signature.length == 65) {
            bytes32 r;
            bytes32 s;
            uint8 v;
            // ecrecover takes the signature parameters, and the only way to get them
            // currently is to use assembly.
            assembly {
                r := mload(add(signature, 0x20))
                s := mload(add(signature, 0x40))
                v := byte(0, mload(add(signature, 0x60)))
            }
            return tryRecover(hash, v, r, s);
        } else if (signature.length == 64) {
            bytes32 r;
            bytes32 vs;
            // ecrecover takes the signature parameters, and the only way to get them
            // currently is to use assembly.
            assembly {
                r := mload(add(signature, 0x20))
                vs := mload(add(signature, 0x40))
            }
            return tryRecover(hash, r, vs);
        } else {
            return (address(0), RecoverError.InvalidSignatureLength);
        }
    }

    /**
     * @dev Returns the address that signed a hashed message (`hash`) with
     * `signature`. This address can then be used for verification purposes.
     *
     * The `ecrecover` EVM opcode allows for malleable (non-unique) signatures:
     * this function rejects them by requiring the `s` value to be in the lower
     * half order, and the `v` value to be either 27 or 28.
     *
     * IMPORTANT: `hash` _must_ be the result of a hash operation for the
     * verification to be secure: it is possible to craft signatures that
     * recover to arbitrary addresses for non-hashed data. A safe way to ensure
     * this is by receiving a hash of the original message (which may otherwise
     * be too long), and then calling {toEthSignedMessageHash} on it.
     */
    function recover(bytes32 hash, bytes memory signature) internal pure returns (address) {
        (address recovered, RecoverError error) = tryRecover(hash, signature);
        _throwError(error);
        return recovered;
    }

    /**
     * @dev Overload of {ECDSA-tryRecover} that receives the `r` and `vs` short-signature fields separately.
     *
     * See https://eips.ethereum.org/EIPS/eip-2098[EIP-2098 short signatures]
     *
     * _Available since v4.3._
     */
    function tryRecover(
        bytes32 hash,
        bytes32 r,
        bytes32 vs
    ) internal pure returns (address, RecoverError) {
        bytes32 s;
        uint8 v;
        assembly {
            s := and(vs, 0x7fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff)
            v := add(shr(255, vs), 27)
        }
        return tryRecover(hash, v, r, s);
    }

    /**
     * @dev Overload of {ECDSA-recover} that receives the `r and `vs` short-signature fields separately.
     *
     * _Available since v4.2._
     */
    function recover(
        bytes32 hash,
        bytes32 r,
        bytes32 vs
    ) internal pure returns (address) {
        (address recovered, RecoverError error) = tryRecover(hash, r, vs);
        _throwError(error);
        return recovered;
    }

    /**
     * @dev Overload of {ECDSA-tryRecover} that receives the `v`,
     * `r` and `s` signature fields separately.
     *
     * _Available since v4.3._
     */
    function tryRecover(
        bytes32 hash,
        uint8 v,
        bytes32 r,
        bytes32 s
    ) internal pure returns (address, RecoverError) {
        // EIP-2 still allows signature malleability for ecrecover(). Remove this possibility and make the signature
        // unique. Appendix F in the Ethereum Yellow paper (https://ethereum.github.io/yellowpaper/paper.pdf), defines
        // the valid range for s in (301): 0 \u003c s \u003c secp256k1n ÷ 2 + 1, and for v in (302): v ∈ {27, 28}. Most
        // signatures from current libraries generate a unique signature with an s-value in the lower half order.
        //
        // If your library generates malleable signatures, such as s-values in the upper range, calculate a new s-value
        // with 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEBAAEDCE6AF48A03BBFD25E8CD0364141 - s1 and flip v from 27 to 28 or
        // vice versa. If your library also generates signatures with 0/1 for v instead 27/28, add 27 to v to accept
        // these malleable signatures as well.
        if (uint256(s) \u003e 0x7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF5D576E7357A4501DDFE92F46681B20A0) {
            return (address(0), RecoverError.InvalidSignatureS);
        }
        if (v != 27 \u0026\u0026 v != 28) {
            return (address(0), RecoverError.InvalidSignatureV);
        }

        // If the signature is valid (and not malleable), return the signer address
        address signer = ecrecover(hash, v, r, s);
        if (signer == address(0)) {
            return (address(0), RecoverError.InvalidSignature);
        }

        return (signer, RecoverError.NoError);
    }

    /**
     * @dev Overload of {ECDSA-recover} that receives the `v`,
     * `r` and `s` signature fields separately.
     */
    function recover(
        bytes32 hash,
        uint8 v,
        bytes32 r,
        bytes32 s
    ) internal pure returns (address) {
        (address recovered, RecoverError error) = tryRecover(hash, v, r, s);
        _throwError(error);
        return recovered;
    }

    /**
     * @dev Returns an Ethereum Signed Message, created from a `hash`. This
     * produces hash corresponding to the one signed with the
     * https://eth.wiki/json-rpc/API#eth_sign[`eth_sign`]
     * JSON-RPC method as part of EIP-191.
     *
     * See {recover}.
     */
    function toEthSignedMessageHash(bytes32 hash) public pure returns (bytes32) {
        // 32 is the length in bytes of hash,
        // enforced by the type signature above
        return keccak256(abi.encodePacked(\"\\x19Ethereum Signed Message:\
32\", hash));
    }
    

    /**
     * @dev Returns an Ethereum Signed Typed Data, created from a
     * `domainSeparator` and a `structHash`. This produces hash corresponding
     * to the one signed with the
     * https://eips.ethereum.org/EIPS/eip-712[`eth_signTypedData`]
     * JSON-RPC method as part of EIP-712.
     *
     * See {recover}.
     */
    function toTypedDataHash(bytes32 domainSeparator, bytes32 structHash) internal pure returns (bytes32) {
        return keccak256(abi.encodePacked(\"\\x19\\x01\", domainSeparator, structHash));
    }
}"},"IERC20.sol":{"content":"// SPDX-License-Identifier: UNLICENSED

pragma solidity 0.7.6;

interface IERC20 {
    event Approval(address indexed owner, address indexed spender, uint value);
    event Transfer(address indexed from, address indexed to, uint value);

    function name() external view returns (string memory);
    function symbol() external view returns (string memory);
    function decimals() external view returns (uint8);
    function totalSupply() external view returns (uint);
    function balanceOf(address owner) external view returns (uint);
    function allowance(address owner, address spender) external view returns (uint);

    function approve(address spender, uint value) external returns (bool);
    function transfer(address to, uint value) external returns (bool);
    function transferFrom(address from, address to, uint value) external returns (bool);
}"},"OBFC.sol":{"content":"// SPDX-License-Identifier: UNLICENSED

pragma solidity 0.7.6;

import \"./IERC20.sol\";

import \"./Ownable.sol\";
import \"./SafeMath.sol\";
import \"./ECDSA.sol\";

import \"./SignatureCheck.sol\";

contract OBFC is Ownable {
    using SafeMath for uint256;

    mapping(address =\u003e mapping(bytes12 =\u003e bool)) public authorizedSenders;
    mapping(address =\u003e bool) public authorizedAccounts;
    mapping(address =\u003e bool) public authorizedTokens;
    mapping(uint256 =\u003e bool) public authorizedRecipients;

    uint256 oneUnit = 10**18;
    uint256 public fee = 0;
    uint256 public min = 0;
    address public router;
    SignatureCheck public signatureCheck;

    event Transmit(
        address indexed transactor,
        bytes12 account,
        uint256 recipient,
        address token,
        uint256 amount,
        uint256 amountFee
    );

    event SetSenderAuthorization(
        address indexed transactor,
        bytes12 account,
        address indexed sender,
        bool isAuthorized
    );

    event ActivateAccount(
        address indexed transactor,
        bytes12 account,
        bytes signature
    );

    event AddRecipient(address indexed wallet, uint256 obfc, bytes signature);

    constructor(address _router, address _signerAddress) Ownable() {
        router = _router;
        signatureCheck = new SignatureCheck(_signerAddress);
    }

    function transmit(
        bytes12 _account,
        address _token,
        uint256 _amount,
        uint256 _recipientOBFC
    ) public returns (bool) {
        address from;
        require(_amount \u003e= min);
        require(authorizedRecipients[_recipientOBFC] == true);
        if (authorizedSenders[msg.sender][_account] == true) {
            from = msg.sender;
        } else if (authorizedSenders[tx.origin][_account] == true) {
            from = tx.origin;
        }
        require(from != address(0));

        uint256 amountAfterFee = _amount.sub(fee);
        IERC20 token = IERC20(_token);
        token.transferFrom(msg.sender, address(this), _amount);
        token.transfer(router, amountAfterFee);
        emit Transmit(from, _account, _recipientOBFC, _token, _amount, fee);
        return true;
    }

    function setSenderAuthorization(
        bytes12 _account,
        address _sender,
        bool _isAuthorized
    ) public returns (bool) {
        require(authorizedSenders[msg.sender][_account] == true);
        authorizedSenders[_sender][_account] = _isAuthorized;
        emit SetSenderAuthorization(
            msg.sender,
            _account,
            _sender,
            _isAuthorized
        );
        return true;
    }

    function activateAccount(
        bytes12 _account,
        uint256 _signatureTimestamp,
        bytes memory _signature
    ) public returns (bool) {
        signatureCheck.activateAccount(
            msg.sender,
            _account,
            _signatureTimestamp,
            _signature
        );
        authorizedSenders[msg.sender][_account] = true;
        emit ActivateAccount(msg.sender, _account, _signature);
        return true;
    }

    function addRecipient(
        bytes12 _account,
        uint256 _recipientOBFC,
        uint256 _signatureTimestamp,
        bytes memory _signature
    ) public returns (bool) {
        signatureCheck.addRecipient(
            msg.sender,
            _account,
            _recipientOBFC,
            _signatureTimestamp,
            _signature
        );
        require(authorizedSenders[msg.sender][_account] == true);
        authorizedRecipients[_recipientOBFC] = true;
        emit AddRecipient(msg.sender, _recipientOBFC, _signature);
        return true;
    }

    function setSignerAddress(address _signerAddress)
        public
        onlyOwner
        returns (bool)
    {
        signatureCheck.setSignerAddress(_signerAddress);
        return true;
    }

    function setRouterAddress(address _router) public onlyOwner returns (bool) {
        router = _router;
        return true;
    }

    function setAuthorizedToken(address _token, bool _isAuthorized)
        public
        onlyOwner
        returns (bool)
    {
        authorizedTokens[_token] = _isAuthorized;
        return true;
    }

    function setFee(uint256 _fee) public onlyOwner returns (bool) {
        fee = _fee;
        return true;
    }

    function setMin(uint256 _min) public onlyOwner returns (bool) {
        fee = _min;
        return true;
    }

    function adminWithdraw(
        address _token,
        uint256 _amount,
        address _recipient
    ) public onlyOwner returns (bool) {
        IERC20 t = IERC20(_token);
        t.transfer(_recipient, _amount);
        return true;
    }
}
"},"Ownable.sol":{"content":"// SPDX-License-Identifier: UNLICENSED

pragma solidity 0.7.6;

/**
 * @dev Contract module which provides a basic access control mechanism, where
 * there is an account (an owner) that can be granted exclusive access to
 * specific functions.
 *
 * By default, the owner account will be the one that deploys the contract. This
 * can later be changed with {transferOwnership}.
 *
 * This module is used through inheritance. It will make available the modifier
 * `onlyOwner`, which can be applied to your functions to restrict their use to
 * the owner.
 */

contract Ownable {
    address private _owner;

    event OwnershipTransferred(
        address indexed previousOwner,
        address indexed newOwner
    );

    /**
     * @dev Initializes the contract setting the deployer as the initial owner.
     */
    constructor() {
        _owner = msg.sender;
        emit OwnershipTransferred(address(0), _owner);
    }

    /**
     * @dev Returns the address of the current owner.
     */
    function owner() public view returns (address) {
        return _owner;
    }

    /**
     * @dev Throws if called by any account other than the owner.
     */
    modifier onlyOwner() {
        require(msg.sender == _owner, \"Ownable: caller is not the owner\");
        _;
    }

    /**
     * @dev Leaves the contract without owner. It will not be possible to call
     * `onlyOwner` functions anymore. Can only be called by the current owner.
     *
     * NOTE: Renouncing ownership will leave the contract without an owner,
     * thereby removing any functionality that is only available to the owner.
     */
    function renounceOwnership() public virtual onlyOwner {
        emit OwnershipTransferred(_owner, address(0));
        _owner = address(0);
    }

    /**
     * @dev Transfers ownership of the contract to a new account (`newOwner`).
     * Can only be called by the current owner.
     */
    function transferOwnership(address newOwner) public virtual onlyOwner {
        require(
            newOwner != address(0),
            \"Ownable: new owner is the zero address\"
        );
        emit OwnershipTransferred(_owner, newOwner);
        _owner = newOwner;
    }
}
"},"SafeMath.sol":{"content":"// SPDX-License-Identifier: UNLICENSED

pragma solidity 0.7.6;

// a library for performing overflow-safe math, courtesy of DappHub (https://github.com/dapphub/ds-math)

library SafeMath {
    function mul(uint256 a, uint256 b) internal pure returns (uint256) {
        if (a == 0) {
            return 0;
        }

        uint256 c = a * b;
        require(c / a == b);

        return c;
    }

    function div(uint256 a, uint256 b) internal pure returns (uint256) {
        // Solidity only automatically asserts when dividing by 0
        require(b \u003e 0);
        uint256 c = a / b;
        // assert(a == b * c + a % b); // There is no case in which this doesn\u0027t hold

        return c;
    }

    function sub(uint256 a, uint256 b) internal pure returns (uint256) {
        require(b \u003c= a);
        uint256 c = a - b;

        return c;
    }

    function add(uint256 a, uint256 b) internal pure returns (uint256) {
        uint256 c = a + b;
        require(c \u003e= a);

        return c;
    }
}"},"SignatureCheck.sol":{"content":"// SPDX-License-Identifier: UNLICENSED

pragma solidity 0.7.6;

import \"./ECDSA.sol\";
import \"./Ownable.sol\";

contract SignatureCheck is Ownable {
    using ECDSA for bytes32;
    address signerAddress;
    mapping(bytes =\u003e bool) signatures;

    constructor(address _signerAddress) Ownable() {
        signerAddress = _signerAddress;
    }

    function activateAccount(
        address transactor,
        bytes12 _account,
        uint256 _timestamp,
        bytes memory _signature
    ) public onlyOwner returns (bool) {
        require(signatures[_signature] == false);
        require(
            signerAddress ==
                verifySig(abi.encode(transactor, _account,_timestamp), _signature)
        );
        signatures[_signature] = true;
        return true;
    }

    function addRecipient(
        address transactor,
        bytes12 _account,
        uint256 _recipientOBFC,
        uint256 _timestamp,
        bytes memory _signature
    ) public onlyOwner returns (bool) {
        require(signatures[_signature] == false);
        require(
            signerAddress ==
                verifySig(
                    abi.encode(transactor, _account, _recipientOBFC, _timestamp),
                    _signature
                )
        );
        signatures[_signature] = true;
        return true;
    }

    function verifySig(bytes memory _params, bytes memory _signature)
        public
        pure
        returns (address)
    {
        return keccak256(_params).toEthSignedMessageHash().recover(_signature);
    }

    function setSignerAddress(address _signerAddress)
        public
        onlyOwner
        returns (bool)
    {
        signerAddress = _signerAddress;
        return true;
    }
}

