// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

/**
 * @dev Collection of functions related to the address type
 */
library Address {
    /**
     * @dev Returns true if `account` is a contract.
     *
     * [IMPORTANT]
     * ====
     * It is unsafe to assume that an address for which this function returns
     * false is an externally-owned account (EOA) and not a contract.
     *
     * Among others, `isContract` will return false for the following
     * types of addresses:
     *
     *  - an externally-owned account
     *  - a contract in construction
     *  - an address where a contract will be created
     *  - an address where a contract lived, but was destroyed
     * ====
     */
    function isContract(address account) internal view returns (bool) {
        // This method relies on extcodesize, which returns 0 for contracts in
        // construction, since the code is only stored at the end of the
        // constructor execution.

        uint256 size;
        assembly {
            size := extcodesize(account)
        }
        return size \u003e 0;
    }

    /**
     * @dev Replacement for Solidity\u0027s `transfer`: sends `amount` wei to
     * `recipient`, forwarding all available gas and reverting on errors.
     *
     * https://eips.ethereum.org/EIPS/eip-1884[EIP1884] increases the gas cost
     * of certain opcodes, possibly making contracts go over the 2300 gas limit
     * imposed by `transfer`, making them unable to receive funds via
     * `transfer`. {sendValue} removes this limitation.
     *
     * https://diligence.consensys.net/posts/2019/09/stop-using-soliditys-transfer-now/[Learn more].
     *
     * IMPORTANT: because control is transferred to `recipient`, care must be
     * taken to not create reentrancy vulnerabilities. Consider using
     * {ReentrancyGuard} or the
     * https://solidity.readthedocs.io/en/v0.5.11/security-considerations.html#use-the-checks-effects-interactions-pattern[checks-effects-interactions pattern].
     */
    function sendValue(address payable recipient, uint256 amount) internal {
        require(address(this).balance \u003e= amount, \"Address: insufficient balance\");

        (bool success, ) = recipient.call{value: amount}(\"\");
        require(success, \"Address: unable to send value, recipient may have reverted\");
    }

    /**
     * @dev Performs a Solidity function call using a low level `call`. A
     * plain `call` is an unsafe replacement for a function call: use this
     * function instead.
     *
     * If `target` reverts with a revert reason, it is bubbled up by this
     * function (like regular Solidity function calls).
     *
     * Returns the raw returned data. To convert to the expected return value,
     * use https://solidity.readthedocs.io/en/latest/units-and-global-variables.html?highlight=abi.decode#abi-encoding-and-decoding-functions[`abi.decode`].
     *
     * Requirements:
     *
     * - `target` must be a contract.
     * - calling `target` with `data` must not revert.
     *
     * _Available since v3.1._
     */
    function functionCall(address target, bytes memory data) internal returns (bytes memory) {
        return functionCall(target, data, \"Address: low-level call failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`], but with
     * `errorMessage` as a fallback revert reason when `target` reverts.
     *
     * _Available since v3.1._
     */
    function functionCall(
        address target,
        bytes memory data,
        string memory errorMessage
    ) internal returns (bytes memory) {
        return functionCallWithValue(target, data, 0, errorMessage);
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`],
     * but also transferring `value` wei to `target`.
     *
     * Requirements:
     *
     * - the calling contract must have an ETH balance of at least `value`.
     * - the called Solidity function must be `payable`.
     *
     * _Available since v3.1._
     */
    function functionCallWithValue(
        address target,
        bytes memory data,
        uint256 value
    ) internal returns (bytes memory) {
        return functionCallWithValue(target, data, value, \"Address: low-level call with value failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCallWithValue-address-bytes-uint256-}[`functionCallWithValue`], but
     * with `errorMessage` as a fallback revert reason when `target` reverts.
     *
     * _Available since v3.1._
     */
    function functionCallWithValue(
        address target,
        bytes memory data,
        uint256 value,
        string memory errorMessage
    ) internal returns (bytes memory) {
        require(address(this).balance \u003e= value, \"Address: insufficient balance for call\");
        require(isContract(target), \"Address: call to non-contract\");

        (bool success, bytes memory returndata) = target.call{value: value}(data);
        return verifyCallResult(success, returndata, errorMessage);
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`],
     * but performing a static call.
     *
     * _Available since v3.3._
     */
    function functionStaticCall(address target, bytes memory data) internal view returns (bytes memory) {
        return functionStaticCall(target, data, \"Address: low-level static call failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-string-}[`functionCall`],
     * but performing a static call.
     *
     * _Available since v3.3._
     */
    function functionStaticCall(
        address target,
        bytes memory data,
        string memory errorMessage
    ) internal view returns (bytes memory) {
        require(isContract(target), \"Address: static call to non-contract\");

        (bool success, bytes memory returndata) = target.staticcall(data);
        return verifyCallResult(success, returndata, errorMessage);
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`],
     * but performing a delegate call.
     *
     * _Available since v3.4._
     */
    function functionDelegateCall(address target, bytes memory data) internal returns (bytes memory) {
        return functionDelegateCall(target, data, \"Address: low-level delegate call failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-string-}[`functionCall`],
     * but performing a delegate call.
     *
     * _Available since v3.4._
     */
    function functionDelegateCall(
        address target,
        bytes memory data,
        string memory errorMessage
    ) internal returns (bytes memory) {
        require(isContract(target), \"Address: delegate call to non-contract\");

        (bool success, bytes memory returndata) = target.delegatecall(data);
        return verifyCallResult(success, returndata, errorMessage);
    }

    /**
     * @dev Tool to verifies that a low level call was successful, and revert if it wasn\u0027t, either by bubbling the
     * revert reason using the provided one.
     *
     * _Available since v4.3._
     */
    function verifyCallResult(
        bool success,
        bytes memory returndata,
        string memory errorMessage
    ) internal pure returns (bytes memory) {
        if (success) {
            return returndata;
        } else {
            // Look for revert reason and bubble it up if present
            if (returndata.length \u003e 0) {
                // The easiest way to bubble the revert reason is using memory via assembly

                assembly {
                    let returndata_size := mload(returndata)
                    revert(add(32, returndata), returndata_size)
                }
            } else {
                revert(errorMessage);
            }
        }
    }
}
"},"Context.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

/**
 * @dev Provides information about the current execution context, including the
 * sender of the transaction and its data. While these are generally available
 * via msg.sender and msg.data, they should not be accessed in such a direct
 * manner, since when dealing with meta-transactions the account sending and
 * paying for execution may not be the actual sender (as far as an application
 * is concerned).
 *
 * This contract is only required for intermediate, library-like contracts.
 */
abstract contract Context {
    function _msgSender() internal view virtual returns (address) {
        return msg.sender;
    }

    function _msgData() internal view virtual returns (bytes calldata) {
        return msg.data;
    }
}
"},"FundManagement.sol":{"content":"// SPDX-License-Identifier: GPL-3.0-only
pragma solidity 0.8.4;

import \"./SafeERC20.sol\";
import \"./Ownable.sol\";

/**
 * Contract to allow Tracer DAO to delegate funds to managers to spend as they see fit. 
 * Managers can request to withdraw funds assigned to them. The DAO may take back the funds at any time.
 */
contract FundManagement is Ownable {
    using SafeERC20 for IERC20;

    struct Fund {
        uint256 totalAmount; // Total amount of tokens assigned to the manager
        address asset;
        uint256 requestedWithdrawTime; // Timestamp which the manager may withdraw the pendingWithdrawAmount
        uint256 pendingWithdrawAmount; // Total amount of tokens the manager is pending to withdraw
    }

    /* ========== STATE VARIABLES ========== */

    uint256 public requestWindow = 2 days; // default period needed to wait after requesting that one can withdraw funds
    mapping(address =\u003e mapping(uint256 =\u003e Fund)) public funds; // user -\u003e fundId -\u003e fund
    mapping(address =\u003e uint256) public numberOfFunds; // user -\u003e number of funds owned
    mapping(address =\u003e uint256) public locked; // asset -\u003e amount locked up

    constructor() {}

    /* ========== VIEWS ========== */

    /**
     * @notice Checks a users fund if they passed the withdraw window and amount they\u0027re able to claim.
     */
    function checkClaimableAmount(address account, uint256 fundNumber) external view returns (bool claimable, uint256 amount) {
        Fund memory fund = funds[account][fundNumber];
        return (block.timestamp \u003e= fund.requestedWithdrawTime, fund.pendingWithdrawAmount);
    }

    /* ========== MUTATIVE FUNCTIONS ========== */

    /**
     * @notice User requests funds that are allocated to them. After the request window if no clawback, they may claim those requested funds.
     * Note: If this function is called while there is already a pending request, it will add to the pending withdrawable amount and reset the request window.
     */
    function requestFunds(uint256 fundNumber, uint256 amount) external {
        require(
            fundNumber \u003c numberOfFunds[msg.sender],
            \"Fund number does not exist\"
        );
        Fund storage fund = funds[msg.sender][fundNumber];
        uint256 totalWithdrawableAmount = fund.pendingWithdrawAmount + amount;
        require(
            totalWithdrawableAmount \u003c= fund.totalAmount, 
            \"Amount \u003e total allocated funds\"
        );
        
        fund.requestedWithdrawTime = block.timestamp + requestWindow;
        fund.pendingWithdrawAmount = totalWithdrawableAmount;

        emit RequestFunds(msg.sender, fundNumber, amount);
    }

    /**
     * @notice User claims the funds they requested. Only claimable after request window has passed with no clawbacks.
     */
    function claim(uint256 fundNumber) external {
        require(
            fundNumber \u003c numberOfFunds[msg.sender],
            \"Fund number does not exist\"
        );
        Fund storage fund = funds[msg.sender][fundNumber];
        uint256 pendingAmount = fund.pendingWithdrawAmount;

        require(
            pendingAmount \u003e 0,
            \"No withdrawable funds\"
        );
        require(
            pendingAmount \u003c= fund.totalAmount, 
            \"Amount \u003e total allocated funds\"
        );
        require(
            block.timestamp \u003e= fund.requestedWithdrawTime,
            \"Not withdrawable yet\"
        );

        locked[fund.asset] = locked[fund.asset] - pendingAmount;
        fund.totalAmount = fund.totalAmount - pendingAmount;
        fund.pendingWithdrawAmount = 0;

        IERC20(fund.asset).safeTransfer(msg.sender, pendingAmount);

        emit Claim(msg.sender, fundNumber, pendingAmount);
    }

    /* ========== RESTRICTED FUNCTIONS ========== */

    /**
     * @notice Owner of the contract may create a fund for a user/fundmanager. The fund will allow the user/fundmanager to request funds and claim it after the request window.
     */
    function createFund(address account, uint256 amount, address asset) external onlyOwner returns (uint256 fundNumber) {
        require(
            account != address(0) \u0026\u0026 asset != address(0),
            \"Account or asset cannot be null\"
        );
        require(
            amount \u003e 0,
            \"Invalid amount\"
        );
        uint256 currentLocked = locked[asset];
        require(
            IERC20(asset).balanceOf(address(this)) \u003e= currentLocked + amount,
            \"Not enough tokens\"
        );

        fundNumber = numberOfFunds[account];
        funds[account][fundNumber] = Fund(
            {
                totalAmount: amount,
                asset: asset,
                requestedWithdrawTime: 0,
                pendingWithdrawAmount: 0
            }
        );

        numberOfFunds[account] = fundNumber + 1;
        locked[asset] = currentLocked + amount;

        emit CreateFund(account, fundNumber, asset, amount);
    }

    /**
     * @notice Stops a fund from being claimed by deallocating their amount to 0. Those deallocated funds are unlocked for the owner to withdraw or reallocate.
     */
    function clawbackFunds(address account, uint256 fundNumber) external onlyOwner {
        require(
            account != address(0),
            \"Account cannot be null\"
        );
        require(
            fundNumber \u003c numberOfFunds[account],
            \"Fund number does not exist\"
        );
        Fund storage fund = funds[account][fundNumber];

        locked[fund.asset] = locked[fund.asset] - fund.totalAmount;
        fund.totalAmount = 0;
        fund.pendingWithdrawAmount = 0;

        emit Clawback(account, fundNumber);
    }

    /**
     * @notice Add more tokens to a fund.
     */
    function addToFund(address account, uint256 amount, uint256 fundNumber) external onlyOwner {
        require(
            account != address(0),
            \"Account cannot be null\"
        );
        require(
            fundNumber \u003c numberOfFunds[account],
            \"Fund number does not exist\"
        );
        Fund storage fund = funds[account][fundNumber];

        uint256 currentLocked = locked[fund.asset];
        require(
            IERC20(fund.asset).balanceOf(address(this)) - currentLocked \u003e= amount,
            \"Not enough unlocked tokens\"
        );

        fund.totalAmount = fund.totalAmount + amount;
        locked[fund.asset] = currentLocked + amount;

        emit AddToFund(account, fundNumber, amount);
    }

    /**
     * @notice Withdraws an asset only if it\u0027s unlocked/deallocated. If you want to withdraw locked/allocated assets, clawback it first.
     */
    function withdrawUnlockedAssets(uint256 amount, address asset) external onlyOwner {
        IERC20 token = IERC20(asset);
        require(
            token.balanceOf(address(this)) - locked[asset] \u003e= amount,
            \"Not enough unlocked tokens\"
        );
        token.safeTransfer(owner(), amount);
    }

    /**
     * @notice Change the duration of time a fund manager needs to wait to withdraw funds they request.
     * @param duration of time in seconds.
     */
    function setRequestWindow(uint256 duration) external onlyOwner {
        requestWindow = duration;
        emit ChangeRequestWindow(duration);
    }

    /* ========== EVENTS ========== */

    event AddToFund(address indexed account, uint256 fundNumber, uint256 amount);
    event ChangeRequestWindow(uint256 duration);
    event Claim(address indexed to, uint256 fundNumber, uint256 amount);
    event Clawback(address indexed account, uint256 fundNumber);
    event CreateFund(address indexed manager, uint256 fundNumber, address indexed asset, uint256 amount);
    event RequestFunds(address indexed to, uint256 fundNumber, uint256 amount);
}"},"IERC20.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

/**
 * @dev Interface of the ERC20 standard as defined in the EIP.
 */
interface IERC20 {
    /**
     * @dev Returns the amount of tokens in existence.
     */
    function totalSupply() external view returns (uint256);

    /**
     * @dev Returns the amount of tokens owned by `account`.
     */
    function balanceOf(address account) external view returns (uint256);

    /**
     * @dev Moves `amount` tokens from the caller\u0027s account to `recipient`.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transfer(address recipient, uint256 amount) external returns (bool);

    /**
     * @dev Returns the remaining number of tokens that `spender` will be
     * allowed to spend on behalf of `owner` through {transferFrom}. This is
     * zero by default.
     *
     * This value changes when {approve} or {transferFrom} are called.
     */
    function allowance(address owner, address spender) external view returns (uint256);

    /**
     * @dev Sets `amount` as the allowance of `spender` over the caller\u0027s tokens.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * IMPORTANT: Beware that changing an allowance with this method brings the risk
     * that someone may use both the old and the new allowance by unfortunate
     * transaction ordering. One possible solution to mitigate this race
     * condition is to first reduce the spender\u0027s allowance to 0 and set the
     * desired value afterwards:
     * https://github.com/ethereum/EIPs/issues/20#issuecomment-263524729
     *
     * Emits an {Approval} event.
     */
    function approve(address spender, uint256 amount) external returns (bool);

    /**
     * @dev Moves `amount` tokens from `sender` to `recipient` using the
     * allowance mechanism. `amount` is then deducted from the caller\u0027s
     * allowance.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transferFrom(
        address sender,
        address recipient,
        uint256 amount
    ) external returns (bool);

    /**
     * @dev Emitted when `value` tokens are moved from one account (`from`) to
     * another (`to`).
     *
     * Note that `value` may be zero.
     */
    event Transfer(address indexed from, address indexed to, uint256 value);

    /**
     * @dev Emitted when the allowance of a `spender` for an `owner` is set by
     * a call to {approve}. `value` is the new allowance.
     */
    event Approval(address indexed owner, address indexed spender, uint256 value);
}
"},"Ownable.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import \"./Context.sol\";

/**
 * @dev Contract module which provides a basic access control mechanism, where
 * there is an account (an owner) that can be granted exclusive access to
 * specific functions.
 *
 * By default, the owner account will be the one that deploys the contract. This
 * can later be changed with {transferOwnership}.
 *
 * This module is used through inheritance. It will make available the modifier
 * `onlyOwner`, which can be applied to your functions to restrict their use to
 * the owner.
 */
abstract contract Ownable is Context {
    address private _owner;

    event OwnershipTransferred(address indexed previousOwner, address indexed newOwner);

    /**
     * @dev Initializes the contract setting the deployer as the initial owner.
     */
    constructor() {
        _setOwner(_msgSender());
    }

    /**
     * @dev Returns the address of the current owner.
     */
    function owner() public view virtual returns (address) {
        return _owner;
    }

    /**
     * @dev Throws if called by any account other than the owner.
     */
    modifier onlyOwner() {
        require(owner() == _msgSender(), \"Ownable: caller is not the owner\");
        _;
    }

    /**
     * @dev Leaves the contract without owner. It will not be possible to call
     * `onlyOwner` functions anymore. Can only be called by the current owner.
     *
     * NOTE: Renouncing ownership will leave the contract without an owner,
     * thereby removing any functionality that is only available to the owner.
     */
    function renounceOwnership() public virtual onlyOwner {
        _setOwner(address(0));
    }

    /**
     * @dev Transfers ownership of the contract to a new account (`newOwner`).
     * Can only be called by the current owner.
     */
    function transferOwnership(address newOwner) public virtual onlyOwner {
        require(newOwner != address(0), \"Ownable: new owner is the zero address\");
        _setOwner(newOwner);
    }

    function _setOwner(address newOwner) private {
        address oldOwner = _owner;
        _owner = newOwner;
        emit OwnershipTransferred(oldOwner, newOwner);
    }
}
"},"SafeERC20.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import \"./IERC20.sol\";
import \"./Address.sol\";

/**
 * @title SafeERC20
 * @dev Wrappers around ERC20 operations that throw on failure (when the token
 * contract returns false). Tokens that return no value (and instead revert or
 * throw on failure) are also supported, non-reverting calls are assumed to be
 * successful.
 * To use this library you can add a `using SafeERC20 for IERC20;` statement to your contract,
 * which allows you to call the safe operations as `token.safeTransfer(...)`, etc.
 */
library SafeERC20 {
    using Address for address;

    function safeTransfer(
        IERC20 token,
        address to,
        uint256 value
    ) internal {
        _callOptionalReturn(token, abi.encodeWithSelector(token.transfer.selector, to, value));
    }

    function safeTransferFrom(
        IERC20 token,
        address from,
        address to,
        uint256 value
    ) internal {
        _callOptionalReturn(token, abi.encodeWithSelector(token.transferFrom.selector, from, to, value));
    }

    /**
     * @dev Deprecated. This function has issues similar to the ones found in
     * {IERC20-approve}, and its usage is discouraged.
     *
     * Whenever possible, use {safeIncreaseAllowance} and
     * {safeDecreaseAllowance} instead.
     */
    function safeApprove(
        IERC20 token,
        address spender,
        uint256 value
    ) internal {
        // safeApprove should only be called when setting an initial allowance,
        // or when resetting it to zero. To increase and decrease it, use
        // \u0027safeIncreaseAllowance\u0027 and \u0027safeDecreaseAllowance\u0027
        require(
            (value == 0) || (token.allowance(address(this), spender) == 0),
            \"SafeERC20: approve from non-zero to non-zero allowance\"
        );
        _callOptionalReturn(token, abi.encodeWithSelector(token.approve.selector, spender, value));
    }

    function safeIncreaseAllowance(
        IERC20 token,
        address spender,
        uint256 value
    ) internal {
        uint256 newAllowance = token.allowance(address(this), spender) + value;
        _callOptionalReturn(token, abi.encodeWithSelector(token.approve.selector, spender, newAllowance));
    }

    function safeDecreaseAllowance(
        IERC20 token,
        address spender,
        uint256 value
    ) internal {
        unchecked {
            uint256 oldAllowance = token.allowance(address(this), spender);
            require(oldAllowance \u003e= value, \"SafeERC20: decreased allowance below zero\");
            uint256 newAllowance = oldAllowance - value;
            _callOptionalReturn(token, abi.encodeWithSelector(token.approve.selector, spender, newAllowance));
        }
    }

    /**
     * @dev Imitates a Solidity high-level call (i.e. a regular function call to a contract), relaxing the requirement
     * on the return value: the return value is optional (but if data is returned, it must not be false).
     * @param token The token targeted by the call.
     * @param data The call data (encoded using abi.encode or one of its variants).
     */
    function _callOptionalReturn(IERC20 token, bytes memory data) private {
        // We need to perform a low level call here, to bypass Solidity\u0027s return data size checking mechanism, since
        // we\u0027re implementing it ourselves. We use {Address.functionCall} to perform this call, which verifies that
        // the target address contains contract code and also asserts for success in the low-level call.

        bytes memory returndata = address(token).functionCall(data, \"SafeERC20: low-level call failed\");
        if (returndata.length \u003e 0) {
            // Return data is optional
            require(abi.decode(returndata, (bool)), \"SafeERC20: ERC20 operation did not succeed\");
        }
    }
}

