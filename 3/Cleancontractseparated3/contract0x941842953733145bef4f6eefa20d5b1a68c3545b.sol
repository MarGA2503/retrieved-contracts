pragma solidity 0.6.8;

library BlockVerifier {
\tfunction extractStateRootAndTimestamp(bytes memory rlpBytes, bytes32 blockHash) internal view returns (bytes32 stateRoot, uint256 blockTimestamp, uint256 blockNumber) {
\t\tassembly {
\t\t\tfunction revertWithReason(message, length) {
\t\t\t\tmstore(0, 0x08c379a000000000000000000000000000000000000000000000000000000000)
\t\t\t\tmstore(4, 0x20)
\t\t\t\tmstore(0x24, length)
\t\t\t\tmstore(0x44, message)
\t\t\t\trevert(0, add(0x44, length))
\t\t\t}

\t\t\tfunction readDynamic(prefixPointer) -> dataPointer, dataLength {
\t\t\t\tlet value := byte(0, mload(prefixPointer))
\t\t\t\tswitch lt(value, 0x80)
\t\t\t\tcase 1 {
\t\t\t\t\tdataPointer := prefixPointer
\t\t\t\t\tdataLength := 1
\t\t\t\t}
\t\t\t\tcase 0 {
\t\t\t\t\tdataPointer := add(prefixPointer, 1)
\t\t\t\t\tdataLength := sub(value, 0x80)
\t\t\t\t}
\t\t\t}

\t\t\t// get the length of the data
\t\t\tlet rlpLength := mload(rlpBytes)
\t\t\t// move pointer forward, ahead of length
\t\t\trlpBytes := add(rlpBytes, 0x20)

\t\t\t// we know the length of the block will be between 483 bytes and 709 bytes, which means it will have 2 length bytes after the prefix byte, so we can skip 3 bytes in
\t\t\t// CONSIDER: we could save a trivial amount of gas by compressing most of this into a single add instruction
\t\t\tlet parentHashPrefixPointer := add(rlpBytes, 3)
\t\t\tlet parentHashPointer := add(parentHashPrefixPointer, 1)
\t\t\tlet uncleHashPrefixPointer := add(parentHashPointer, 32)
\t\t\tlet uncleHashPointer := add(uncleHashPrefixPointer, 1)
\t\t\tlet minerAddressPrefixPointer := add(uncleHashPointer, 32)
\t\t\tlet minerAddressPointer := add(minerAddressPrefixPointer, 1)
\t\t\tlet stateRootPrefixPointer := add(minerAddressPointer, 20)
\t\t\tlet stateRootPointer := add(stateRootPrefixPointer, 1)
\t\t\tlet transactionRootPrefixPointer := add(stateRootPointer, 32)
\t\t\tlet transactionRootPointer := add(transactionRootPrefixPointer, 1)
\t\t\tlet receiptsRootPrefixPointer := add(transactionRootPointer, 32)
\t\t\tlet receiptsRootPointer := add(receiptsRootPrefixPointer, 1)
\t\t\tlet logsBloomPrefixPointer := add(receiptsRootPointer, 32)
\t\t\tlet logsBloomPointer := add(logsBloomPrefixPointer, 3)
\t\t\tlet difficultyPrefixPointer := add(logsBloomPointer, 256)
\t\t\tlet difficultyPointer, difficultyLength := readDynamic(difficultyPrefixPointer)
\t\t\tlet blockNumberPrefixPointer := add(difficultyPointer, difficultyLength)
\t\t\tlet blockNumberPointer, blockNumberLength := readDynamic(blockNumberPrefixPointer)
\t\t\tlet gasLimitPrefixPointer := add(blockNumberPointer, blockNumberLength)
\t\t\tlet gasLimitPointer, gasLimitLength := readDynamic(gasLimitPrefixPointer)
\t\t\tlet gasUsedPrefixPointer := add(gasLimitPointer, gasLimitLength)
\t\t\tlet gasUsedPointer, gasUsedLength := readDynamic(gasUsedPrefixPointer)
\t\t\tlet timestampPrefixPointer := add(gasUsedPointer, gasUsedLength)
\t\t\tlet timestampPointer, timestampLength := readDynamic(timestampPrefixPointer)

\t\t\tblockNumber := shr(sub(256, mul(blockNumberLength, 8)), mload(blockNumberPointer))
\t\t\tlet rlpHash := keccak256(rlpBytes, rlpLength)
\t\t\tif iszero(eq(blockHash, rlpHash)) { revertWithReason(\"blockHash != rlpHash\", 20) }

\t\t\tstateRoot := mload(stateRootPointer)
\t\t\tblockTimestamp := shr(sub(256, mul(timestampLength, 8)), mload(timestampPointer))
\t\t}
\t}
}"
    
pragma solidity 0.6.8;
pragma experimental ABIEncoderV2;

import { BlockVerifier } from \"./BlockVerifier.sol\";
import { MerklePatriciaVerifier } from \"./MerklePatriciaVerifier.sol\";
import { Rlp } from \"./Rlp.sol\";
import { IUniswapV2Pair } from \"./IUniswapV2Pair.sol\";
import { UQ112x112 } from \"./UQ112x112.sol\";

contract SushiOracle {
\tusing UQ112x112 for uint224;

\tbytes32 public constant reserveTimestampSlotHash = keccak256(abi.encodePacked(uint256(8)));
\tbytes32 public constant token0Slot = keccak256(abi.encodePacked(uint256(9)));
\tbytes32 public constant token1Slot = keccak256(abi.encodePacked(uint256(10)));

\tstruct ProofData {
\t\tbytes block;
\t\tbytes accountProofNodesRlp;
\t\tbytes reserveAndTimestampProofNodesRlp;
\t\tbytes priceAccumulatorProofNodesRlp;
\t}

\tfunction getAccountStorageRoot(address uniswapV2Pair, ProofData memory proofData, bytes32 blockHash) public view returns (bytes32 storageRootHash, uint256 blockNumber, uint256 blockTimestamp) {
\t\tbytes32 stateRoot;
\t\t(stateRoot, blockTimestamp, blockNumber) = BlockVerifier.extractStateRootAndTimestamp(proofData.block, blockHash);
\t\tbytes memory accountDetailsBytes = MerklePatriciaVerifier.getValueFromProof(stateRoot, keccak256(abi.encodePacked(uniswapV2Pair)), proofData.accountProofNodesRlp);
\t\tRlp.Item[] memory accountDetails = Rlp.toList(Rlp.toItem(accountDetailsBytes));
\t\treturn (Rlp.toBytes32(accountDetails[2]), blockNumber, blockTimestamp);
\t}

\t// This function verifies the full block is old enough (MIN_BLOCK_COUNT), not too old (or blockhash will return 0x0) and return the proof values for the two storage slots we care about
\tfunction verifyBlockAndExtractReserveData(IUniswapV2Pair uniswapV2Pair, bytes32 slotHash, ProofData memory proofData, bytes32 blockHash) public view returns
\t(uint256 blockTimestamp, uint256 blockNumber, uint256 priceCumulativeLast, uint112 reserve0, uint112 reserve1, uint256 reserveTimestamp) {
\t\tbytes32 storageRootHash;
\t\t(storageRootHash, blockNumber, blockTimestamp) = getAccountStorageRoot(address(uniswapV2Pair), proofData, blockHash);
\t\t// require (blockNumber <= block.number - minBlocksBack, \"Proof does not span enough blocks\");
\t\t// require (blockNumber >= block.number - maxBlocksBack, \"Proof spans too many blocks\");

\t\tpriceCumulativeLast = Rlp.rlpBytesToUint256(MerklePatriciaVerifier.getValueFromProof(storageRootHash, slotHash, proofData.priceAccumulatorProofNodesRlp));
\t\tuint256 reserve0Reserve1TimestampPacked = Rlp.rlpBytesToUint256(MerklePatriciaVerifier.getValueFromProof(storageRootHash, reserveTimestampSlotHash, proofData.reserveAndTimestampProofNodesRlp));
\t\treserveTimestamp = reserve0Reserve1TimestampPacked >> (112 + 112);
\t\treserve1 = uint112((reserve0Reserve1TimestampPacked >> 112) & (2**112 - 1));
\t\treserve0 = uint112(reserve0Reserve1TimestampPacked & (2**112 - 1));
\t}

\tfunction getPrice(IUniswapV2Pair uniswapV2Pair, address denominationToken, uint256 blockNum, ProofData memory proofData) public view returns (uint256 price, uint256 blockNumber) {
\t\t// exchange = the ExchangeV2Pair. check denomination token (USE create2 check?!) check gas cost
\t\tbool denominationTokenIs0 = true;
\t\tif (uniswapV2Pair.token0() == denominationToken) {
\t\t\tdenominationTokenIs0 = true;
\t\t} else if (uniswapV2Pair.token1() == denominationToken) {
\t\t\tdenominationTokenIs0 = false;
\t\t} else {
\t\t\trevert(\"denominationToken invalid\");
\t\t}
\t\treturn getPriceRaw(uniswapV2Pair, denominationTokenIs0, proofData, blockhash(blockNum));
\t}

\tfunction getPriceRaw(IUniswapV2Pair uniswapV2Pair, bool denominationTokenIs0, ProofData memory proofData, bytes32 blockHash) public view returns (uint256 price, uint256 blockNumber) {
\t\tuint256 historicBlockTimestamp;
\t\tuint256 historicPriceCumulativeLast;
\t\t{
\t\t\t// Stack-too-deep workaround, manual scope
\t\t\t// Side-note: wtf Solidity?
\t\t\tuint112 reserve0;
\t\t\tuint112 reserve1;
\t\t\tuint256 reserveTimestamp;
\t\t\t(historicBlockTimestamp, blockNumber, historicPriceCumulativeLast, reserve0, reserve1, reserveTimestamp) = verifyBlockAndExtractReserveData(uniswapV2Pair, denominationTokenIs0 ? token1Slot : token0Slot, proofData, blockHash);
\t\t\tuint256 secondsBetweenReserveUpdateAndHistoricBlock = historicBlockTimestamp - reserveTimestamp;
\t\t\t// bring old record up-to-date, in case there was no cumulative update in provided historic block itself
\t\t\tif (secondsBetweenReserveUpdateAndHistoricBlock > 0) {
\t\t\t\thistoricPriceCumulativeLast += secondsBetweenReserveUpdateAndHistoricBlock * uint(UQ112x112
\t\t\t\t\t.encode(denominationTokenIs0 ? reserve0 : reserve1)
\t\t\t\t\t.uqdiv(denominationTokenIs0 ? reserve1 : reserve0)
\t\t\t\t);
\t\t\t}
\t\t}
\t\tuint256 secondsBetweenProvidedBlockAndNow = block.timestamp - historicBlockTimestamp;
\t\tprice = (getCurrentPriceCumulativeLast(uniswapV2Pair, denominationTokenIs0) - historicPriceCumulativeLast) / secondsBetweenProvidedBlockAndNow;
\t\treturn (price, blockNumber);
\t}

\tfunction getCurrentPriceCumulativeLast(IUniswapV2Pair uniswapV2Pair, bool denominationTokenIs0) public view returns (uint256 priceCumulativeLast) {
\t\t(uint112 reserve0, uint112 reserve1, uint32 blockTimestampLast) = uniswapV2Pair.getReserves();
\t\tpriceCumulativeLast = denominationTokenIs0 ? uniswapV2Pair.price1CumulativeLast() : uniswapV2Pair.price0CumulativeLast();
\t\tuint256 timeElapsed = block.timestamp - blockTimestampLast;
\t\tpriceCumulativeLast += timeElapsed * uint(UQ112x112
\t\t\t.encode(denominationTokenIs0 ? reserve0 : reserve1)
\t\t\t.uqdiv(denominationTokenIs0 ? reserve1 : reserve0)
\t\t);
\t}
}
"
    
pragma solidity 0.6.8;

import { Rlp } from \"./Rlp.sol\";

library MerklePatriciaVerifier {
\t/*
\t * @dev Extracts the value from a merkle proof
\t * @param expectedRoot The expected hash of the root node of the trie.
\t * @param path The path in the trie leading to value.
\t * @param proofNodesRlp RLP encoded array of proof nodes.
\t * @return The value proven to exist in the merkle patricia tree whose root is `expectedRoot` at the path `path`
\t *
\t * WARNING: Does not currently support validation of unset/0 values!
\t */
\tfunction getValueFromProof(bytes32 expectedRoot, bytes32 path, bytes memory proofNodesRlp) internal pure returns (bytes memory) {
\t\tRlp.Item memory rlpParentNodes = Rlp.toItem(proofNodesRlp);
\t\tRlp.Item[] memory parentNodes = Rlp.toList(rlpParentNodes);

\t\tbytes memory currentNode;
\t\tRlp.Item[] memory currentNodeList;

\t\tbytes32 nodeKey = expectedRoot;
\t\tuint pathPtr = 0;

\t\t// our input is a 32-byte path, but we have to prepend a single 0 byte to that and pass it along as a 33 byte memory array since that is what getNibbleArray wants
\t\tbytes memory nibblePath = new bytes(33);
\t\tassembly { mstore(add(nibblePath, 33), path) }
\t\tnibblePath = _getNibbleArray(nibblePath);

\t\trequire(path.length != 0, \"empty path provided\");

\t\tcurrentNode = Rlp.toBytes(parentNodes[0]);

\t\tfor (uint i=0; i<parentNodes.length; i++) {
\t\t\trequire(pathPtr <= nibblePath.length, \"Path overflow\");

\t\t\tcurrentNode = Rlp.toBytes(parentNodes[i]);
\t\t\trequire(nodeKey == keccak256(currentNode), \"node doesn't match key\");
\t\t\tcurrentNodeList = Rlp.toList(parentNodes[i]);

\t\t\tif(currentNodeList.length == 17) {
\t\t\t\tif(pathPtr == nibblePath.length) {
\t\t\t\t\treturn Rlp.toData(currentNodeList[16]);
\t\t\t\t}

\t\t\t\tuint8 nextPathNibble = uint8(nibblePath[pathPtr]);
\t\t\t\trequire(nextPathNibble <= 16, \"nibble too long\");
\t\t\t\tnodeKey = Rlp.toBytes32(currentNodeList[nextPathNibble]);
\t\t\t\tpathPtr += 1;
\t\t\t} else if(currentNodeList.length == 2) {
\t\t\t\tpathPtr += _nibblesToTraverse(Rlp.toData(currentNodeList[0]), nibblePath, pathPtr);
\t\t\t\t// leaf node
\t\t\t\tif(pathPtr == nibblePath.length) {
\t\t\t\t\treturn Rlp.toData(currentNodeList[1]);
\t\t\t\t}
\t\t\t\t//extension node
\t\t\t\trequire(_nibblesToTraverse(Rlp.toData(currentNodeList[0]), nibblePath, pathPtr) != 0, \"invalid extension node\");

\t\t\t\tnodeKey = Rlp.toBytes32(currentNodeList[1]);
\t\t\t} else {
\t\t\t\trequire(false, \"unexpected length array\");
\t\t\t}
\t\t}
\t\trequire(false, \"not enough proof nodes\");
\t}

\tfunction _nibblesToTraverse(bytes memory encodedPartialPath, bytes memory path, uint pathPtr) private pure returns (uint) {
\t\tuint len;
\t\t// encodedPartialPath has elements that are each two hex characters (1 byte), but partialPath
\t\t// and slicedPath have elements that are each one hex character (1 nibble)
\t\tbytes memory partialPath = _getNibbleArray(encodedPartialPath);
\t\tbytes memory slicedPath = new bytes(partialPath.length);

\t\t// pathPtr counts nibbles in path
\t\t// partialPath.length is a number of nibbles
\t\tfor(uint i=pathPtr; i<pathPtr+partialPath.length; i++) {
\t\t\tbyte pathNibble = path[i];
\t\t\tslicedPath[i-pathPtr] = pathNibble;
\t\t}

\t\tif(keccak256(partialPath) == keccak256(slicedPath)) {
\t\t\tlen = partialPath.length;
\t\t} else {
\t\t\tlen = 0;
\t\t}
\t\treturn len;
\t}

\t// bytes byteArray must be hp encoded
\tfunction _getNibbleArray(bytes memory byteArray) private pure returns (bytes memory) {
\t\tbytes memory nibbleArray;
\t\tif (byteArray.length == 0) return nibbleArray;

\t\tuint8 offset;
\t\tuint8 hpNibble = uint8(_getNthNibbleOfBytes(0,byteArray));
\t\tif(hpNibble == 1 || hpNibble == 3) {
\t\t\tnibbleArray = new bytes(byteArray.length*2-1);
\t\t\tbyte oddNibble = _getNthNibbleOfBytes(1,byteArray);
\t\t\tnibbleArray[0] = oddNibble;
\t\t\toffset = 1;
\t\t} else {
\t\t\tnibbleArray = new bytes(byteArray.length*2-2);
\t\t\toffset = 0;
\t\t}

\t\tfor(uint i=offset; i<nibbleArray.length; i++) {
\t\t\tnibbleArray[i] = _getNthNibbleOfBytes(i-offset+2,byteArray);
\t\t}
\t\treturn nibbleArray;
\t}

\tfunction _getNthNibbleOfBytes(uint n, bytes memory str) private pure returns (byte) {
\t\treturn byte(n%2==0 ? uint8(str[n/2])/0x10 : uint8(str[n/2])%0x10);
\t}
}"
    
pragma solidity 0.6.8;

library Rlp {
\tuint constant DATA_SHORT_START = 0x80;
\tuint constant DATA_LONG_START = 0xB8;
\tuint constant LIST_SHORT_START = 0xC0;
\tuint constant LIST_LONG_START = 0xF8;

\tuint constant DATA_LONG_OFFSET = 0xB7;
\tuint constant LIST_LONG_OFFSET = 0xF7;


\tstruct Item {
\t\tuint _unsafe_memPtr;    // Pointer to the RLP-encoded bytes.
\t\tuint _unsafe_length;    // Number of bytes. This is the full length of the string.
\t}

\tstruct Iterator {
\t\tItem _unsafe_item;   // Item that's being iterated over.
\t\tuint _unsafe_nextPtr;   // Position of the next item in the list.
\t}

\t/* Iterator */

\tfunction next(Iterator memory self) internal pure returns (Item memory subItem) {
\t\trequire(hasNext(self), \"Rlp.sol:Rlp:next:1\");
\t\tuint256 ptr = self._unsafe_nextPtr;
\t\tuint256 itemLength = _itemLength(ptr);
\t\tsubItem._unsafe_memPtr = ptr;
\t\tsubItem._unsafe_length = itemLength;
\t\tself._unsafe_nextPtr = ptr + itemLength;
\t}

\tfunction next(Iterator memory self, bool strict) internal pure returns (Item memory subItem) {
\t\tsubItem = next(self);
\t\trequire(!strict || _validate(subItem), \"Rlp.sol:Rlp:next:2\");
\t}

\tfunction hasNext(Iterator memory self) internal pure returns (bool) {
\t\tRlp.Item memory item = self._unsafe_item;
\t\treturn self._unsafe_nextPtr < item._unsafe_memPtr + item._unsafe_length;
\t}

\t/* Item */

\t/// @dev Creates an Item from an array of RLP encoded bytes.
\t/// @param self The RLP encoded bytes.
\t/// @return An Item
\tfunction toItem(bytes memory self) internal pure returns (Item memory) {
\t\tuint len = self.length;
\t\tif (len == 0) {
\t\t\treturn Item(0, 0);
\t\t}
\t\tuint memPtr;
\t\tassembly {
\t\t\tmemPtr := add(self, 0x20)
\t\t}
\t\treturn Item(memPtr, len);
\t}

\t/// @dev Creates an Item from an array of RLP encoded bytes.
\t/// @param self The RLP encoded bytes.
\t/// @param strict Will throw if the data is not RLP encoded.
\t/// @return An Item
\tfunction toItem(bytes memory self, bool strict) internal pure returns (Item memory) {
\t\tRlp.Item memory item = toItem(self);
\t\tif(strict) {
\t\t\tuint len = self.length;
\t\t\trequire(_payloadOffset(item) <= len, \"Rlp.sol:Rlp:toItem4\");
\t\t\trequire(_itemLength(item._unsafe_memPtr) == len, \"Rlp.sol:Rlp:toItem:5\");
\t\t\trequire(_validate(item), \"Rlp.sol:Rlp:toItem:6\");
\t\t}
\t\treturn item;
\t}

\t/// @dev Check if the Item is null.
\t/// @param self The Item.
\t/// @return 'true' if the item is null.
\tfunction isNull(Item memory self) internal pure returns (bool) {
\t\treturn self._unsafe_length == 0;
\t}

\t/// @dev Check if the Item is a list.
\t/// @param self The Item.
\t/// @return 'true' if the item is a list.
\tfunction isList(Item memory self) internal pure returns (bool) {
\t\tif (self._unsafe_length == 0)
\t\t\treturn false;
\t\tuint memPtr = self._unsafe_memPtr;
\t\tbool result;
\t\tassembly {
\t\t\tresult := iszero(lt(byte(0, mload(memPtr)), 0xC0))
\t\t}
\t\treturn result;
\t}

\t/// @dev Check if the Item is data.
\t/// @param self The Item.
\t/// @return 'true' if the item is data.
\tfunction isData(Item memory self) internal pure returns (bool) {
\t\tif (self._unsafe_length == 0)
\t\t\treturn false;
\t\tuint memPtr = self._unsafe_memPtr;
\t\tbool result;
\t\tassembly {
\t\t\tresult := lt(byte(0, mload(memPtr)), 0xC0)
\t\t}
\t\treturn result;
\t}

\t/// @dev Check if the Item is empty (string or list).
\t/// @param self The Item.
\t/// @return result 'true' if the item is null.
\tfunction isEmpty(Item memory self) internal pure returns (bool) {
\t\tif(isNull(self))
\t\t\treturn false;
\t\tuint b0;
\t\tuint memPtr = self._unsafe_memPtr;
\t\tassembly {
\t\t\tb0 := byte(0, mload(memPtr))
\t\t}
\t\treturn (b0 == DATA_SHORT_START || b0 == LIST_SHORT_START);
\t}

\t/// @dev Get the number of items in an RLP encoded list.
\t/// @param self The Item.
\t/// @return The number of items.
\tfunction items(Item memory self) internal pure returns (uint) {
\t\tif (!isList(self))
\t\t\treturn 0;
\t\tuint b0;
\t\tuint memPtr = self._unsafe_memPtr;
\t\tassembly {
\t\t\tb0 := byte(0, mload(memPtr))
\t\t}
\t\tuint pos = memPtr + _payloadOffset(self);
\t\tuint last = memPtr + self._unsafe_length - 1;
\t\tuint itms;
\t\twhile(pos <= last) {
\t\t\tpos += _itemLength(pos);
\t\t\titms++;
\t\t}
\t\treturn itms;
\t}

\t/// @dev Create an iterator.
\t/// @param self The Item.
\t/// @return An 'Iterator' over the item.
\tfunction iterator(Item memory self) internal pure returns (Iterator memory) {
\t\trequire(isList(self), \"Rlp.sol:Rlp:iterator:1\");
\t\tuint ptr = self._unsafe_memPtr + _payloadOffset(self);
\t\tIterator memory it;
\t\tit._unsafe_item = self;
\t\tit._unsafe_nextPtr = ptr;
\t\treturn it;
\t}

\t/// @dev Return the RLP encoded bytes.
\t/// @param self The Item.
\t/// @return The bytes.
\tfunction toBytes(Item memory self) internal pure returns (bytes memory) {
\t\tuint256 len = self._unsafe_length;
\t\trequire(len != 0, \"Rlp.sol:Rlp:toBytes:2\");
\t\tbytes memory bts;
\t\tbts = new bytes(len);
\t\t_copyToBytes(self._unsafe_memPtr, bts, len);
\t\treturn bts;
\t}

\t/// @dev Decode an Item into bytes. This will not work if the
\t/// Item is a list.
\t/// @param self The Item.
\t/// @return The decoded string.
\tfunction toData(Item memory self) internal pure returns (bytes memory) {
\t\trequire(isData(self));
\t\t(uint256 rStartPos, uint256 len) = _decode(self);
\t\tbytes memory bts;
\t\tbts = new bytes(len);
\t\t_copyToBytes(rStartPos, bts, len);
\t\treturn bts;
\t}

\t/// @dev Get the list of sub-items from an RLP encoded list.
\t/// Warning: This is inefficient, as it requires that the list is read twice.
\t/// @param self The Item.
\t/// @return Array of Items.
\tfunction toList(Item memory self) internal pure returns (Item[] memory) {
\t\trequire(isList(self), \"Rlp.sol:Rlp:toList:1\");
\t\tuint256 numItems = items(self);
\t\tItem[] memory list = new Item[](numItems);
\t\tRlp.Iterator memory it = iterator(self);
\t\tuint idx;
\t\twhile(hasNext(it)) {
\t\t\tlist[idx] = next(it);
\t\t\tidx++;
\t\t}
\t\treturn list;
\t}

\t/// @dev Decode an Item into an ascii string. This will not work if the
\t/// Item is a list.
\t/// @param self The Item.
\t/// @return The decoded string.
\tfunction toAscii(Item memory self) internal pure returns (string memory) {
\t\trequire(isData(self), \"Rlp.sol:Rlp:toAscii:1\");
\t\t(uint256 rStartPos, uint256 len) = _decode(self);
\t\tbytes memory bts = new bytes(len);
\t\t_copyToBytes(rStartPos, bts, len);
\t\tstring memory str = string(bts);
\t\treturn str;
\t}

\t/// @dev Decode an Item into a uint. This will not work if the
\t/// Item is a list.
\t/// @param self The Item.
\t/// @return The decoded string.
\tfunction toUint(Item memory self) internal pure returns (uint) {
\t\trequire(isData(self), \"Rlp.sol:Rlp:toUint:1\");
\t\t(uint256 rStartPos, uint256 len) = _decode(self);
\t\trequire(len <= 32, \"Rlp.sol:Rlp:toUint:3\");
\t\trequire(len != 0, \"Rlp.sol:Rlp:toUint:4\");
\t\tuint data;
\t\tassembly {
\t\t\tdata := div(mload(rStartPos), exp(256, sub(32, len)))
\t\t}
\t\treturn data;
\t}

\t/// @dev Decode an Item into a boolean. This will not work if the
\t/// Item is a list.
\t/// @param self The Item.
\t/// @return The decoded string.
\tfunction toBool(Item memory self) internal pure returns (bool) {
\t\trequire(isData(self), \"Rlp.sol:Rlp:toBool:1\");
\t\t(uint256 rStartPos, uint256 len) = _decode(self);
\t\trequire(len == 1, \"Rlp.sol:Rlp:toBool:3\");
\t\tuint temp;
\t\tassembly {
\t\t\ttemp := byte(0, mload(rStartPos))
\t\t}
\t\trequire(temp <= 1, \"Rlp.sol:Rlp:toBool:8\");
\t\treturn temp == 1 ? true : false;
\t}

\t/// @dev Decode an Item into a byte. This will not work if the
\t/// Item is a list.
\t/// @param self The Item.
\t/// @return The decoded string.
\tfunction toByte(Item memory self) internal pure returns (byte) {
\t\trequire(isData(self), \"Rlp.sol:Rlp:toByte:1\");
\t\t(uint256 rStartPos, uint256 len) = _decode(self);
\t\trequire(len == 1, \"Rlp.sol:Rlp:toByte:3\");
\t\tbyte temp;
\t\tassembly {
\t\t\ttemp := byte(0, mload(rStartPos))
\t\t}
\t\treturn byte(temp);
\t}

\t/// @dev Decode an Item into an int. This will not work if the
\t/// Item is a list.
\t/// @param self The Item.
\t/// @return The decoded string.
\tfunction toInt(Item memory self) internal pure returns (int) {
\t\treturn int(toUint(self));
\t}

\t/// @dev Decode an Item into a bytes32. This will not work if the
\t/// Item is a list.
\t/// @param self The Item.
\t/// @return The decoded string.
\tfunction toBytes32(Item memory self) internal pure returns (bytes32) {
\t\treturn bytes32(toUint(self));
\t}

\t/// @dev Decode an Item into an address. This will not work if the
\t/// Item is a list.
\t/// @param self The Item.
\t/// @return The decoded string.
\tfunction toAddress(Item memory self) internal pure returns (address) {
\t\trequire(isData(self), \"Rlp.sol:Rlp:toAddress:1\");
\t\t(uint256 rStartPos, uint256 len) = _decode(self);
\t\trequire(len == 20, \"Rlp.sol:Rlp:toAddress:3\");
\t\taddress data;
\t\tassembly {
\t\t\tdata := div(mload(rStartPos), exp(256, 12))
\t\t}
\t\treturn data;
\t}

\t// Get the payload offset.
\tfunction _payloadOffset(Item memory self) private pure returns (uint) {
\t\tif(self._unsafe_length == 0)
\t\t\treturn 0;
\t\tuint b0;
\t\tuint memPtr = self._unsafe_memPtr;
\t\tassembly {
\t\t\tb0 := byte(0, mload(memPtr))
\t\t}
\t\tif(b0 < DATA_SHORT_START)
\t\t\treturn 0;
\t\tif(b0 < DATA_LONG_START || (b0 >= LIST_SHORT_START && b0 < LIST_LONG_START))
\t\t\treturn 1;
\t\tif(b0 < LIST_SHORT_START)
\t\t\treturn b0 - DATA_LONG_OFFSET + 1;
\t\treturn b0 - LIST_LONG_OFFSET + 1;
\t}

\t// Get the full length of an Item.
\tfunction _itemLength(uint memPtr) private pure returns (uint len) {
\t\tuint b0;
\t\tassembly {
\t\t\tb0 := byte(0, mload(memPtr))
\t\t}
\t\tif (b0 < DATA_SHORT_START)
\t\t\tlen = 1;
\t\telse if (b0 < DATA_LONG_START)
\t\t\tlen = b0 - DATA_SHORT_START + 1;
\t\telse if (b0 < LIST_SHORT_START) {
\t\t\tassembly {
\t\t\t\tlet bLen := sub(b0, 0xB7) // bytes length (DATA_LONG_OFFSET)
\t\t\t\tlet dLen := div(mload(add(memPtr, 1)), exp(256, sub(32, bLen))) // data length
\t\t\t\tlen := add(1, add(bLen, dLen)) // total length
\t\t\t}
\t\t}
\t\telse if (b0 < LIST_LONG_START)
\t\t\tlen = b0 - LIST_SHORT_START + 1;
\t\telse {
\t\t\tassembly {
\t\t\t\tlet bLen := sub(b0, 0xF7) // bytes length (LIST_LONG_OFFSET)
\t\t\t\tlet dLen := div(mload(add(memPtr, 1)), exp(256, sub(32, bLen))) // data length
\t\t\t\tlen := add(1, add(bLen, dLen)) // total length
\t\t\t}
\t\t}
\t}

\t// Get start position and length of the data.
\tfunction _decode(Item memory self) private pure returns (uint memPtr, uint len) {
\t\trequire(isData(self), \"Rlp.sol:Rlp:_decode:1\");
\t\tuint b0;
\t\tuint start = self._unsafe_memPtr;
\t\tassembly {
\t\t\tb0 := byte(0, mload(start))
\t\t}
\t\tif (b0 < DATA_SHORT_START) {
\t\t\tmemPtr = start;
\t\t\tlen = 1;
\t\t\treturn (memPtr, len);
\t\t}
\t\tif (b0 < DATA_LONG_START) {
\t\t\tlen = self._unsafe_length - 1;
\t\t\tmemPtr = start + 1;
\t\t} else {
\t\t\tuint bLen;
\t\t\tassembly {
\t\t\t\tbLen := sub(b0, 0xB7) // DATA_LONG_OFFSET
\t\t\t}
\t\t\tlen = self._unsafe_length - 1 - bLen;
\t\t\tmemPtr = start + bLen + 1;
\t\t}
\t\treturn (memPtr, len);
\t}

\t// Assumes that enough memory has been allocated to store in target.
\tfunction _copyToBytes(uint sourceBytes, bytes memory destinationBytes, uint btsLen) internal pure {
\t\t// Exploiting the fact that 'tgt' was the last thing to be allocated,
\t\t// we can write entire words, and just overwrite any excess.
\t\tassembly {
\t\t\tlet words := div(add(btsLen, 31), 32)
\t\t\tlet sourcePointer := sourceBytes
\t\t\tlet destinationPointer := add(destinationBytes, 32)
\t\t\tfor { let i := 0 } lt(i, words) { i := add(i, 1) }
\t\t\t{
\t\t\t\tlet offset := mul(i, 32)
\t\t\t\tmstore(add(destinationPointer, offset), mload(add(sourcePointer, offset)))
\t\t\t}
\t\t\tmstore(add(destinationBytes, add(32, mload(destinationBytes))), 0)
\t\t}
\t}

\t// Check that an Item is valid.
\tfunction _validate(Item memory self) private pure returns (bool ret) {
\t\t// Check that RLP is well-formed.
\t\tuint b0;
\t\tuint b1;
\t\tuint memPtr = self._unsafe_memPtr;
\t\tassembly {
\t\t\tb0 := byte(0, mload(memPtr))
\t\t\tb1 := byte(1, mload(memPtr))
\t\t}
\t\tif(b0 == DATA_SHORT_START + 1 && b1 < DATA_SHORT_START)
\t\t\treturn false;
\t\treturn true;
\t}

\tfunction rlpBytesToUint256(bytes memory source) internal pure returns (uint256 result) {
\t\treturn Rlp.toUint(Rlp.toItem(source));
\t}
}
"
    
pragma solidity 0.6.8;

interface IUniswapV2Pair {
    function token0() external view returns (address);
    function token1() external view returns (address);

    function price0CumulativeLast() external view returns (uint256);
    function price1CumulativeLast() external view returns (uint256);

\tfunction getReserves() external view returns (uint112 _reserve0, uint112 _reserve1, uint32 _blockTimestampLast);
}
"
    
pragma solidity 0.6.8;

// https://raw.githubusercontent.com/Uniswap/uniswap-v2-core/master/contracts/libraries/UQ112x112.sol
// a library for handling binary fixed point numbers (https://en.wikipedia.org/wiki/Q_(number_format))

// range: [0, 2**112 - 1]
// resolution: 1 / 2**112

library UQ112x112 {
\tuint224 constant Q112 = 2**112;

\t// encode a uint112 as a UQ112x112
\tfunction encode(uint112 y) internal pure returns (uint224 z) {
\t\tz = uint224(y) * Q112; // never overflows
\t}

\t// divide a UQ112x112 by a uint112, returning a UQ112x112
\tfunction uqdiv(uint224 x, uint112 y) internal pure returns (uint224 z) {
\t\tz = x / uint224(y);
\t}
}
"
    }
  
