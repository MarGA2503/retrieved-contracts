// SPDX-License-Identifier: UNLICENSED
pragma solidity 0.6.12;

/// @title HoldefiOwnable
/// @author Holdefi Team
/// @notice Taking ideas from Open Zeppelin\u0027s Ownable contract
/// @dev Contract module which provides a basic access control mechanism, where
/// there is an account (an owner) that can be granted exclusive access to
/// specific functions.
///
/// By default, the owner account will be the one that deploys the contract. This
/// can later be changed with {transferOwnership}.
///
/// This module is used through inheritance. It will make available the modifier
/// `onlyOwner`, which can be applied to your functions to restrict their use to
/// the owner.
/// @dev Error codes description: 
///     OE01: Sender should be the owner
///     OE02: New owner can not be zero address
///     OE03: Pending owner is empty
///     OE04: Pending owner is not same as the `msg.sender`
contract HoldefiOwnable {
    address public owner;
    address public pendingOwner;

    /// @notice Event emitted when an ownership transfer request is recieved
    event OwnershipTransferRequested(address newPendingOwner);

    /// @notice Event emitted when an ownership transfer request is accepted by the pending owner
    event OwnershipTransferred(address newOwner, address oldOwner);

    /// @notice Initializes the contract owner
    constructor () public {
        owner = msg.sender;
        emit OwnershipTransferred(owner, address(0));
    }

    /// @notice Throws if called by any account other than the owner
    modifier onlyOwner() {
        require(msg.sender == owner, \"OE01\");
        _;
    }

    /// @notice Transfers ownership of the contract to a new owner
    /// @dev Can only be called by the current owner
    /// @param newOwner Address of new owner
    function transferOwnership(address newOwner) external onlyOwner {
        require(newOwner != address(0), \"OE02\");
        pendingOwner = newOwner;

        emit OwnershipTransferRequested(newOwner);
    }

    /// @notice Pending owner accepts ownership of the contract
    /// @dev Only Pending owner can call this function
    function acceptTransferOwnership () external {
        require (pendingOwner != address(0), \"OE03\");
        require (pendingOwner == msg.sender, \"OE04\");
        
        emit OwnershipTransferred(pendingOwner, owner);
        owner = pendingOwner;
        pendingOwner = address(0);
    }
}"},"HoldefiSettings.sol":{"content":"// SPDX-License-Identifier: UNLICENSED
pragma solidity 0.6.12;
pragma experimental ABIEncoderV2;

import \"./SafeMath.sol\";
import \"./HoldefiOwnable.sol\";

/// @notice File: contracts/Holdefi.sol
interface HoldefiInterface {
\tstruct Market {
\t\tuint256 totalSupply;
\t\tuint256 supplyIndex;
\t\tuint256 supplyIndexUpdateTime;

\t\tuint256 totalBorrow;
\t\tuint256 borrowIndex;
\t\tuint256 borrowIndexUpdateTime;

\t\tuint256 promotionReserveScaled;
\t\tuint256 promotionReserveLastUpdateTime;
\t\tuint256 promotionDebtScaled;
\t\tuint256 promotionDebtLastUpdateTime;
\t}

\tfunction marketAssets(address market) external view returns (Market memory);
\tfunction holdefiSettings() external view returns (address contractAddress);
\tfunction beforeChangeSupplyRate (address market) external;
\tfunction beforeChangeBorrowRate (address market) external;
\tfunction reserveSettlement (address market) external;
}

/// @title HoldefiSettings contract
/// @author Holdefi Team
/// @notice This contract is for Holdefi settings implementation
/// @dev Error codes description: 
/// \tSE01: Market is not exist
/// \tSE02: Collateral is not exist
/// \tSE03: Conflict with Holdefi contract
/// \tSE04: The contract should be set once
/// \tSE05: Rate should be in the allowed range
/// \tSE06: Sender should be Holdefi contract
/// \tSE07: Collateral is exist
/// \tSE08: Market is exist
/// \tSE09: Market list is full
/// \tSE10: Total borrow is not zero
/// \tSE11: Changing rate is not allowed at this time
/// \tSE12: Changing rate should be less than Max allowed
contract HoldefiSettings is HoldefiOwnable {

\tusing SafeMath for uint256;

\t/// @notice Markets Features
\tstruct MarketSettings {
\t\tbool isExist;\t\t// Market is exist or not
\t\tbool isActive;\t\t// Market is open for deposit or not

\t\tuint256 borrowRate;
\t\tuint256 borrowRateUpdateTime;

\t\tuint256 suppliersShareRate;
\t\tuint256 suppliersShareRateUpdateTime;

\t\tuint256 promotionRate;
\t}

\t/// @notice Collateral Features
\tstruct CollateralSettings {
\t\tbool isExist;\t\t// Collateral is exist or not
\t\tbool isActive;\t\t// Collateral is open for deposit or not

\t\tuint256 valueToLoanRate;
\t\tuint256 VTLUpdateTime;

\t\tuint256 penaltyRate;
\t\tuint256 penaltyUpdateTime;

\t\tuint256 bonusRate;
\t}

\tuint256 constant private rateDecimals = 10 ** 4;

\taddress constant private ethAddress = 0xEeeeeEeeeEeEeeEeEeEeeEEEeeeeEeeeeeeeEEeE;

\tuint256 constant private periodBetweenUpdates = 604800;      \t// seconds per week

\tuint256 constant private maxBorrowRate = 4000;      \t\t\t// 40%

\tuint256 constant private borrowRateMaxIncrease = 500;      \t\t// 5%

\tuint256 constant private minSuppliersShareRate = 5000;      \t// 50%

\tuint256 constant private suppliersShareRateMaxDecrease = 500;\t// 5%

\tuint256 constant private maxValueToLoanRate = 20000;      \t\t// 200%

\tuint256 constant private valueToLoanRateMaxIncrease = 500;      // 5%

\tuint256 constant private maxPenaltyRate = 13000;      \t\t\t// 130%

\tuint256 constant private penaltyRateMaxIncrease = 500;      \t// 5%

\tuint256 constant private maxPromotionRate = 10000;\t\t\t\t// 100%

\tuint256 constant private maxListsLength = 50;

\t/// @dev Used for calculating liquidation threshold 
\t/// There is 5% gap between value to loan rate and liquidation rate
\tuint256 constant private fivePercentLiquidationGap = 500;

\tmapping (address =\u003e MarketSettings) public marketAssets;
\taddress[] public marketsList;

\tmapping (address =\u003e CollateralSettings) public collateralAssets;

\tHoldefiInterface public holdefiContract;

\t/// @notice Event emitted when market activation status is changed
\tevent MarketActivationChanged(address indexed market, bool status);

\t/// @notice Event emitted when collateral activation status is changed
\tevent CollateralActivationChanged(address indexed collateral, bool status);

\t/// @notice Event emitted when market existence status is changed
\tevent MarketExistenceChanged(address indexed market, bool status);

\t/// @notice Event emitted when collateral existence status is changed
\tevent CollateralExistenceChanged(address indexed collateral, bool status);

\t/// @notice Event emitted when market borrow rate is changed
\tevent BorrowRateChanged(address indexed market, uint256 newRate, uint256 oldRate);

\t/// @notice Event emitted when market suppliers share rate is changed
\tevent SuppliersShareRateChanged(address indexed market, uint256 newRate, uint256 oldRate);

\t/// @notice Event emitted when market promotion rate is changed
\tevent PromotionRateChanged(address indexed market, uint256 newRate, uint256 oldRate);

\t/// @notice Event emitted when collateral value to loan rate is changed
\tevent ValueToLoanRateChanged(address indexed collateral, uint256 newRate, uint256 oldRate);

\t/// @notice Event emitted when collateral penalty rate is changed
\tevent PenaltyRateChanged(address indexed collateral, uint256 newRate, uint256 oldRate);

\t/// @notice Event emitted when collateral bonus rate is changed
\tevent BonusRateChanged(address indexed collateral, uint256 newRate, uint256 oldRate);


\t/// @dev Modifier to make a function callable only when the market is exist
\t/// @param market Address of the given market
    modifier marketIsExist(address market) {
        require (marketAssets[market].isExist, \"SE01\");
        _;
    }

\t/// @dev Modifier to make a function callable only when the collateral is exist
\t/// @param collateral Address of the given collateral
    modifier collateralIsExist(address collateral) {
        require (collateralAssets[collateral].isExist, \"SE02\");
        _;
    }


\t/// @notice you cannot send ETH to this contract
    receive() external payable {
        revert();
    }

 \t/// @notice Activate a market asset
\t/// @dev Can only be called by the owner
\t/// @param market Address of the given market
\tfunction activateMarket (address market) external onlyOwner marketIsExist(market) {
\t\tactivateMarketInternal(market);
\t}

\t/// @notice Deactivate a market asset
\t/// @dev Can only be called by the owner
\t/// @param market Address of the given market
\tfunction deactivateMarket (address market) external onlyOwner marketIsExist(market) {
\t\tmarketAssets[market].isActive = false;
\t\temit MarketActivationChanged(market, false);
\t}

\t/// @notice Activate a collateral asset
\t/// @dev Can only be called by the owner
\t/// @param collateral Address the given collateral
\tfunction activateCollateral (address collateral) external onlyOwner collateralIsExist(collateral) {
\t\tactivateCollateralInternal(collateral);
\t}

\t/// @notice Deactivate a collateral asset
\t/// @dev Can only be called by the owner
\t/// @param collateral Address of the given collateral
\tfunction deactivateCollateral (address collateral) external onlyOwner collateralIsExist(collateral) {
\t\tcollateralAssets[collateral].isActive = false;
\t\temit CollateralActivationChanged(collateral, false);
\t}

\t/// @notice Returns the list of markets
\t/// @return res List of markets
\tfunction getMarketsList() external view returns (address[] memory res) {
\t\tres = marketsList;
\t}

\t/// @notice Disposable function to interact with Holdefi contract
\t/// @dev Can only be called by the owner
\t/// @param holdefiContractAddress Address of the Holdefi contract
\tfunction setHoldefiContract(HoldefiInterface holdefiContractAddress) external onlyOwner {
\t\trequire (holdefiContractAddress.holdefiSettings() == address(this), \"SE03\");
\t\trequire (address(holdefiContract) == address(0), \"SE04\");
\t\tholdefiContract = holdefiContractAddress;
\t}

\t/// @notice Returns supply, borrow and promotion rate of the given market
\t/// @dev supplyRate = (totalBorrow * borrowRate) * suppliersShareRate / totalSupply
\t/// @param market Address of the given market
\t/// @return borrowRate Borrow rate of the given market
\t/// @return supplyRateBase Supply rate base of the given market
\t/// @return promotionRate Promotion rate of the given market
\tfunction getInterests (address market)
\t\texternal
\t\tview
\t\treturns (uint256 borrowRate, uint256 supplyRateBase, uint256 promotionRate)
\t{
\t\tuint256 totalBorrow = holdefiContract.marketAssets(market).totalBorrow;
\t\tuint256 totalSupply = holdefiContract.marketAssets(market).totalSupply;
\t\tborrowRate = marketAssets[market].borrowRate;

\t\tif (totalSupply == 0) {
\t\t\tsupplyRateBase = 0;
\t\t}
\t\telse {
\t\t\tuint256 totalInterestFromBorrow = totalBorrow.mul(borrowRate);
\t\t\tuint256 suppliersShare = totalInterestFromBorrow.mul(marketAssets[market].suppliersShareRate).div(rateDecimals);
\t\t\tsupplyRateBase = suppliersShare.div(totalSupply);
\t\t}
\t\tpromotionRate = marketAssets[market].promotionRate;
\t}


\t/// @notice Set promotion rate for a market
\t/// @dev Can only be called by the owner
\t/// @param market Address of the given market
\t/// @param newPromotionRate New promotion rate
\tfunction setPromotionRate (address market, uint256 newPromotionRate) external onlyOwner {
\t\trequire (newPromotionRate \u003c= maxPromotionRate, \"SE05\");

\t\tholdefiContract.reserveSettlement(market);

\t\temit PromotionRateChanged(market, newPromotionRate, marketAssets[market].promotionRate);
\t\tmarketAssets[market].promotionRate = newPromotionRate;
\t}

\t/// @notice Reset promotion rate of the market to zero
\t/// @dev Can only be called by holdefi contract
\t/// @param market Address of the given market
\tfunction resetPromotionRate (address market) external {
\t\trequire (msg.sender == address(holdefiContract), \"SE06\");

\t\temit PromotionRateChanged(market, 0, marketAssets[market].promotionRate);
\t\tmarketAssets[market].promotionRate = 0;
\t}

\t/// @notice Set borrow rate for a market
\t/// @dev Can only be called by the owner
\t/// @param market Address of the given market
\t/// @param newBorrowRate New borrow rate
\tfunction setBorrowRate (address market, uint256 newBorrowRate)
\t\texternal 
\t\tonlyOwner
\t\tmarketIsExist(market)
\t{
\t\tsetBorrowRateInternal(market, newBorrowRate);
\t}

\t/// @notice Set suppliers share rate for a market
\t/// @dev Can only be called by the owner
\t/// @param market Address of the given market
\t/// @param newSuppliersShareRate New suppliers share rate
\tfunction setSuppliersShareRate (address market, uint256 newSuppliersShareRate)
\t\texternal
\t\tonlyOwner
\t\tmarketIsExist(market)
\t{
\t\tsetSuppliersShareRateInternal(market, newSuppliersShareRate);
\t}

\t/// @notice Set value to loan rate for a collateral
\t/// @dev Can only be called by the owner
\t/// @param collateral Address of the given collateral
\t/// @param newValueToLoanRate New value to loan rate
\tfunction setValueToLoanRate (address collateral, uint256 newValueToLoanRate)
\t\texternal
\t\tonlyOwner
\t\tcollateralIsExist(collateral)
\t{
\t\tsetValueToLoanRateInternal(collateral, newValueToLoanRate);
\t}

\t/// @notice Set penalty rate for a collateral
\t/// @dev Can only be called by the owner
\t/// @param collateral Address of the given collateral
\t/// @param newPenaltyRate New penalty rate
\tfunction setPenaltyRate (address collateral, uint256 newPenaltyRate)
\t\texternal
\t\tonlyOwner
\t\tcollateralIsExist(collateral)
\t{
\t\tsetPenaltyRateInternal(collateral, newPenaltyRate);
\t}

\t/// @notice Set bonus rate for a collateral
\t/// @dev Can only be called by the owner
\t/// @param collateral Address of the given collateral
\t/// @param newBonusRate New bonus rate
\tfunction setBonusRate (address collateral, uint256 newBonusRate)
\t\texternal
\t\tonlyOwner
\t\tcollateralIsExist(collateral)
\t{
\t\tsetBonusRateInternal(collateral, newBonusRate); 
\t}

\t/// @notice Add a new asset as a market
\t/// @dev Can only be called by the owner
\t/// @param market Address of the new market
\t/// @param borrowRate BorrowRate of the new market
\t/// @param suppliersShareRate SuppliersShareRate of the new market
\tfunction addMarket (address market, uint256 borrowRate, uint256 suppliersShareRate)
\t\texternal
\t\tonlyOwner
\t{
\t\trequire (!marketAssets[market].isExist, \"SE08\");
\t\trequire (marketsList.length \u003c maxListsLength, \"SE09\");

\t\tmarketsList.push(market);
\t\tmarketAssets[market].isExist = true;
\t\temit MarketExistenceChanged(market, true);

\t\tsetBorrowRateInternal(market, borrowRate);
\t\tsetSuppliersShareRateInternal(market, suppliersShareRate);
\t\t
\t\tactivateMarketInternal(market);\t\t
\t}

\t/// @notice Remove a market asset
\t/// @dev Can only be called by the owner
\t/// @param market Address of the given market
\tfunction removeMarket (address market) external onlyOwner marketIsExist(market) {
\t\trequire (holdefiContract.marketAssets(market).totalBorrow == 0, \"SE10\");
\t\t
\t\tholdefiContract.beforeChangeBorrowRate(market);

\t\tuint256 index;
\t\tuint256 marketListLength = marketsList.length;
\t\tfor (uint256 i = 0 ; i \u003c marketListLength ; i++) {
\t\t\tif (marketsList[i] == market) {
\t\t\t\tindex = i;
\t\t\t}
\t\t}

\t\tmarketsList[index] = marketsList[marketListLength-1];
\t\tmarketsList.pop();
\t\tdelete marketAssets[market];
\t\temit MarketExistenceChanged(market, false);
\t}

\t/// @notice Add a new asset as a collateral
\t/// @dev Can only be called by the owner
\t/// @param collateral Address of the new collateral
\t/// @param valueToLoanRate ValueToLoanRate of the new collateral
\t/// @param penaltyRate PenaltyRate of the new collateral
\t/// @param bonusRate BonusRate of the new collateral
\tfunction addCollateral (
\t\taddress collateral,
\t\tuint256 valueToLoanRate,
\t\tuint256 penaltyRate,
\t\tuint256 bonusRate
\t)
\t\texternal
\t\tonlyOwner
\t{
\t\trequire (!collateralAssets[collateral].isExist, \"SE07\");

\t\tcollateralAssets[collateral].isExist = true;
\t\temit CollateralExistenceChanged(collateral, true);

\t\tsetValueToLoanRateInternal(collateral, valueToLoanRate);
\t\tsetPenaltyRateInternal(collateral, penaltyRate);
\t\tsetBonusRateInternal(collateral, bonusRate);

\t\tactivateCollateralInternal(collateral);
\t}

\t/// @notice Activate the market
\tfunction activateMarketInternal (address market) internal {
\t\tmarketAssets[market].isActive = true;
\t\temit MarketActivationChanged(market, true);
\t}

\t/// @notice Activate the collateral
\tfunction activateCollateralInternal (address collateral) internal {
\t\tcollateralAssets[collateral].isActive = true;
\t\temit CollateralActivationChanged(collateral, true);
\t}

\t/// @notice Set borrow rate operation
\tfunction setBorrowRateInternal (address market, uint256 newBorrowRate) internal {
\t\trequire (newBorrowRate \u003c= maxBorrowRate, \"SE05\");
\t\tuint256 currentTime = block.timestamp;

\t\tif (marketAssets[market].borrowRateUpdateTime != 0) {
\t\t\tif (newBorrowRate \u003e marketAssets[market].borrowRate) {
\t\t\t\tuint256 deltaTime = currentTime.sub(marketAssets[market].borrowRateUpdateTime);
\t\t\t\trequire (deltaTime \u003e= periodBetweenUpdates, \"SE11\");

\t\t\t\tuint256 maxIncrease = marketAssets[market].borrowRate.add(borrowRateMaxIncrease);
\t\t\t\trequire (newBorrowRate \u003c= maxIncrease, \"SE12\");
\t\t\t}

\t\t\tholdefiContract.beforeChangeBorrowRate(market);
\t\t}

\t\temit BorrowRateChanged(market, newBorrowRate, marketAssets[market].borrowRate);

\t\tmarketAssets[market].borrowRate = newBorrowRate;
\t\tmarketAssets[market].borrowRateUpdateTime = currentTime;
\t}

\t/// @notice Set suppliers share rate operation
\tfunction setSuppliersShareRateInternal (address market, uint256 newSuppliersShareRate) internal {
\t\trequire (
\t\t\tnewSuppliersShareRate \u003e= minSuppliersShareRate \u0026\u0026 newSuppliersShareRate \u003c= rateDecimals,
\t\t\t\"SE05\"
\t\t);
\t\tuint256 currentTime = block.timestamp;

\t\tif (marketAssets[market].suppliersShareRateUpdateTime != 0) {
\t\t\tif (newSuppliersShareRate \u003c marketAssets[market].suppliersShareRate) {
\t\t\t\tuint256 deltaTime = currentTime.sub(marketAssets[market].suppliersShareRateUpdateTime);
\t\t\t\trequire (deltaTime \u003e= periodBetweenUpdates, \"SE11\");

\t\t\t\tuint256 decreasedAllowed = newSuppliersShareRate.add(suppliersShareRateMaxDecrease);
\t\t\t\trequire (
\t\t\t\t\tmarketAssets[market].suppliersShareRate \u003c= decreasedAllowed,
\t\t\t\t\t\"SE12\"
\t\t\t\t);
\t\t\t}

\t\t\tholdefiContract.beforeChangeSupplyRate(market);
\t\t}

\t\temit SuppliersShareRateChanged(
\t\t\tmarket,
\t\t\tnewSuppliersShareRate,
\t\t\tmarketAssets[market].suppliersShareRate
\t\t);

\t\tmarketAssets[market].suppliersShareRate = newSuppliersShareRate;
\t\tmarketAssets[market].suppliersShareRateUpdateTime = currentTime;
\t}

\t/// @notice Set value to loan rate operation
\tfunction setValueToLoanRateInternal (address collateral, uint256 newValueToLoanRate) internal {
\t\trequire (
\t\t\tnewValueToLoanRate \u003c= maxValueToLoanRate \u0026\u0026
\t\t\tcollateralAssets[collateral].penaltyRate.add(fivePercentLiquidationGap) \u003c= newValueToLoanRate,
\t\t\t\"SE05\"
\t\t);
\t\t
\t\tuint256 currentTime = block.timestamp;
\t\tif (
\t\t\tcollateralAssets[collateral].VTLUpdateTime != 0 \u0026\u0026
\t\t\tnewValueToLoanRate \u003e collateralAssets[collateral].valueToLoanRate
\t\t) {
\t\t\tuint256 deltaTime = currentTime.sub(collateralAssets[collateral].VTLUpdateTime);
\t\t\trequire (deltaTime \u003e= periodBetweenUpdates,\"SE11\");
\t\t\tuint256 maxIncrease = collateralAssets[collateral].valueToLoanRate.add(
\t\t\t\tvalueToLoanRateMaxIncrease
\t\t\t);
\t\t\trequire (newValueToLoanRate \u003c= maxIncrease,\"SE12\");
\t\t}
\t\temit ValueToLoanRateChanged(
\t\t\tcollateral,
\t\t\tnewValueToLoanRate,
\t\t\tcollateralAssets[collateral].valueToLoanRate
\t\t);

\t    collateralAssets[collateral].valueToLoanRate = newValueToLoanRate;
\t    collateralAssets[collateral].VTLUpdateTime = currentTime;
\t}

\t/// @notice Set penalty rate operation
\tfunction setPenaltyRateInternal (address collateral, uint256 newPenaltyRate) internal {
\t\trequire (
\t\t\tnewPenaltyRate \u003c= maxPenaltyRate \u0026\u0026
\t\t\tnewPenaltyRate \u003c= collateralAssets[collateral].valueToLoanRate.sub(fivePercentLiquidationGap) \u0026\u0026
\t\t\tcollateralAssets[collateral].bonusRate \u003c= newPenaltyRate,
\t\t\t\"SE05\"
\t\t);

\t\tuint256 currentTime = block.timestamp;
\t\tif (
\t\t\tcollateralAssets[collateral].penaltyUpdateTime != 0 \u0026\u0026
\t\t\tnewPenaltyRate \u003e collateralAssets[collateral].penaltyRate
\t\t) {
\t\t\tuint256 deltaTime = currentTime.sub(collateralAssets[collateral].penaltyUpdateTime);
\t\t\trequire (deltaTime \u003e= periodBetweenUpdates, \"SE11\");
\t\t\tuint256 maxIncrease = collateralAssets[collateral].penaltyRate.add(penaltyRateMaxIncrease);
\t\t\trequire (newPenaltyRate \u003c= maxIncrease, \"SE12\");
\t\t}

\t\temit PenaltyRateChanged(collateral, newPenaltyRate, collateralAssets[collateral].penaltyRate);

\t    collateralAssets[collateral].penaltyRate  = newPenaltyRate;
\t    collateralAssets[collateral].penaltyUpdateTime = currentTime;
\t}

\t/// @notice Set Bonus rate operation
\tfunction setBonusRateInternal (address collateral, uint256 newBonusRate) internal {
\t\trequire (
\t\t\tnewBonusRate \u003c= collateralAssets[collateral].penaltyRate \u0026\u0026 newBonusRate \u003e= rateDecimals,
\t\t\t\"SE05\"
\t\t);
\t\t
\t\temit BonusRateChanged(collateral, newBonusRate, collateralAssets[collateral].bonusRate);
\t    collateralAssets[collateral].bonusRate = newBonusRate;    
\t}
}
"},"SafeMath.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity \u003e=0.6.0 \u003c0.8.0;

/**
 * @dev Wrappers over Solidity\u0027s arithmetic operations with added overflow
 * checks.
 *
 * Arithmetic operations in Solidity wrap on overflow. This can easily result
 * in bugs, because programmers usually assume that an overflow raises an
 * error, which is the standard behavior in high level programming languages.
 * `SafeMath` restores this intuition by reverting the transaction when an
 * operation overflows.
 *
 * Using this library instead of the unchecked operations eliminates an entire
 * class of bugs, so it\u0027s recommended to use it always.
 */
library SafeMath {
    /**
     * @dev Returns the addition of two unsigned integers, with an overflow flag.
     *
     * _Available since v3.4._
     */
    function tryAdd(uint256 a, uint256 b) internal pure returns (bool, uint256) {
        uint256 c = a + b;
        if (c \u003c a) return (false, 0);
        return (true, c);
    }

    /**
     * @dev Returns the substraction of two unsigned integers, with an overflow flag.
     *
     * _Available since v3.4._
     */
    function trySub(uint256 a, uint256 b) internal pure returns (bool, uint256) {
        if (b \u003e a) return (false, 0);
        return (true, a - b);
    }

    /**
     * @dev Returns the multiplication of two unsigned integers, with an overflow flag.
     *
     * _Available since v3.4._
     */
    function tryMul(uint256 a, uint256 b) internal pure returns (bool, uint256) {
        // Gas optimization: this is cheaper than requiring \u0027a\u0027 not being zero, but the
        // benefit is lost if \u0027b\u0027 is also tested.
        // See: https://github.com/OpenZeppelin/openzeppelin-contracts/pull/522
        if (a == 0) return (true, 0);
        uint256 c = a * b;
        if (c / a != b) return (false, 0);
        return (true, c);
    }

    /**
     * @dev Returns the division of two unsigned integers, with a division by zero flag.
     *
     * _Available since v3.4._
     */
    function tryDiv(uint256 a, uint256 b) internal pure returns (bool, uint256) {
        if (b == 0) return (false, 0);
        return (true, a / b);
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers, with a division by zero flag.
     *
     * _Available since v3.4._
     */
    function tryMod(uint256 a, uint256 b) internal pure returns (bool, uint256) {
        if (b == 0) return (false, 0);
        return (true, a % b);
    }

    /**
     * @dev Returns the addition of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity\u0027s `+` operator.
     *
     * Requirements:
     *
     * - Addition cannot overflow.
     */
    function add(uint256 a, uint256 b) internal pure returns (uint256) {
        uint256 c = a + b;
        require(c \u003e= a, \"SafeMath: addition overflow\");
        return c;
    }

    /**
     * @dev Returns the subtraction of two unsigned integers, reverting on
     * overflow (when the result is negative).
     *
     * Counterpart to Solidity\u0027s `-` operator.
     *
     * Requirements:
     *
     * - Subtraction cannot overflow.
     */
    function sub(uint256 a, uint256 b) internal pure returns (uint256) {
        require(b \u003c= a, \"SafeMath: subtraction overflow\");
        return a - b;
    }

    /**
     * @dev Returns the multiplication of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity\u0027s `*` operator.
     *
     * Requirements:
     *
     * - Multiplication cannot overflow.
     */
    function mul(uint256 a, uint256 b) internal pure returns (uint256) {
        if (a == 0) return 0;
        uint256 c = a * b;
        require(c / a == b, \"SafeMath: multiplication overflow\");
        return c;
    }

    /**
     * @dev Returns the integer division of two unsigned integers, reverting on
     * division by zero. The result is rounded towards zero.
     *
     * Counterpart to Solidity\u0027s `/` operator. Note: this function uses a
     * `revert` opcode (which leaves remaining gas untouched) while Solidity
     * uses an invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function div(uint256 a, uint256 b) internal pure returns (uint256) {
        require(b \u003e 0, \"SafeMath: division by zero\");
        return a / b;
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
     * reverting when dividing by zero.
     *
     * Counterpart to Solidity\u0027s `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function mod(uint256 a, uint256 b) internal pure returns (uint256) {
        require(b \u003e 0, \"SafeMath: modulo by zero\");
        return a % b;
    }

    /**
     * @dev Returns the subtraction of two unsigned integers, reverting with custom message on
     * overflow (when the result is negative).
     *
     * CAUTION: This function is deprecated because it requires allocating memory for the error
     * message unnecessarily. For custom revert reasons use {trySub}.
     *
     * Counterpart to Solidity\u0027s `-` operator.
     *
     * Requirements:
     *
     * - Subtraction cannot overflow.
     */
    function sub(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b \u003c= a, errorMessage);
        return a - b;
    }

    /**
     * @dev Returns the integer division of two unsigned integers, reverting with custom message on
     * division by zero. The result is rounded towards zero.
     *
     * CAUTION: This function is deprecated because it requires allocating memory for the error
     * message unnecessarily. For custom revert reasons use {tryDiv}.
     *
     * Counterpart to Solidity\u0027s `/` operator. Note: this function uses a
     * `revert` opcode (which leaves remaining gas untouched) while Solidity
     * uses an invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function div(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b \u003e 0, errorMessage);
        return a / b;
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
     * reverting with custom message when dividing by zero.
     *
     * CAUTION: This function is deprecated because it requires allocating memory for the error
     * message unnecessarily. For custom revert reasons use {tryMod}.
     *
     * Counterpart to Solidity\u0027s `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function mod(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b \u003e 0, errorMessage);
        return a % b;
    }
}

