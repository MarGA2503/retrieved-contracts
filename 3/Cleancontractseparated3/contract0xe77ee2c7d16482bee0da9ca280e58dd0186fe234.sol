pragma solidity ^0.4.20;

import \u0027./owned.sol\u0027;
import \u0027./erc20.sol\u0027;

contract BorToken is ERC20,Owned {
    
    mapping(address =\u003e bool) public frozenAccount;
    
    event AddSupply(uint amount);
    event FrozenFunds(address target,bool frozen);
    event Burn(address target,uint amount);
    
    
    function mine(address _target,uint _amount) public onlyOwner {
        require(_amount \u003e 0);
        totalSupply += _amount;
        balanceOf[_target] += _amount;
        
        emit AddSupply(_amount);
        emit Transfer(0,_target,_amount);
    }
    
    
    function freezeAccount(address _target,bool _freeze) public onlyOwner {
        frozenAccount[_target]=_freeze;
        emit FrozenFunds(_target,_freeze);
    }
    
    function burn(address _from,uint256 _value) public onlyOwner returns (bool success) {
        require(_value \u003e 0);
        require(balanceOf[_from] \u003e= _value);
        
        totalSupply -= _value;
        balanceOf[_from] -= _value;
        
        emit Burn(_from,_value);
        return true;
    }
    
    
    function transfer(address _to,uint256 _value) public returns (bool success){
        success= _transfer(msg.sender,_to,_value);
    }
    
    function transferFrom(address _from,address _to,uint256 _value) public returns (bool success){
        require(allowed[_from][msg.sender] \u003e= _value);
        success= _transfer(_from,_to,_value);
        allowed[_from][msg.sender] -= _value;
    }
    
    function _transfer(address _from,address _to,uint256 _value) internal returns (bool success){
        require(_value \u003e 0);
        require(_to!=address(0));
        require(!frozenAccount[_from]);
        
        require(balanceOf[_from] \u003e= _value);
        require(balanceOf[_to] + _value \u003e= balanceOf[_to]);
        
        balanceOf[_from] -= _value;
        balanceOf[_to] += _value;
        
        emit Transfer(_from,_to,_value);
        
        return true;
    }
    
    
}"},"erc20.sol":{"content":"pragma solidity ^0.4.20;

import \u0027./erc20Interface.sol\u0027;

contract ERC20 is ERC20Interface{
    
    mapping(address =\u003e uint256) public balanceOf;
    mapping(address =\u003e mapping(address=\u003euint256)) allowed;
    
    constructor() public {
        name=\"BorCoin\";
        symbol=\"BORC\";
        decimals=6;
        totalSupply=100000000000000;
        balanceOf[msg.sender] = totalSupply;
    }
    
    function transfer(address _to,uint256 _value) public returns (bool success){
        require(_to!=address(0));
        require(balanceOf[msg.sender] \u003e= _value);
        require(balanceOf[_to] + _value \u003e= balanceOf[_to]);
        
        balanceOf[msg.sender] -= _value;
        balanceOf[_to] += _value;
        
        emit Transfer(msg.sender,_to,_value);
        
        return true;
    }
    
    function transferFrom(address _from,address _to,uint256 _value) public returns (bool success){
        require(_to!=address(0));
        require(allowed[_from][msg.sender] \u003e= _value);
        require(balanceOf[_from] \u003e= _value);
        require(balanceOf[_to] + _value \u003e= balanceOf[_to]);
        
        balanceOf[_from] -= _value;
        balanceOf[_to] += _value;
        
        allowed[_from][msg.sender] -=_value;
        
        emit Transfer(_from,_to,_value);
        
        return true;
    }
    
    function approve(address _spender,uint256 _value) public returns (bool success){
        allowed[msg.sender][_spender] = _value;
        
        emit Approval(msg.sender,_spender,_value);
        
        return true;
    }
    
    function allowance(address _owner,address _spender) view public returns(uint256 remaining){
        return allowed[_owner][_spender];
    }
    
}"},"erc20Interface.sol":{"content":"pragma solidity ^0.4.20;

contract ERC20Interface{
    
    string public name;
    string public symbol;
    uint8 public decimals;
    uint public totalSupply;
    
    function transfer(address _to,uint256 _value) returns (bool success);
    function transferFrom(address _from,address _to,uint256 _value) returns (bool success);
    
    function approve(address _spender,uint256 _value) returns (bool success);
    function allowance(address _owner,address _spender) view returns(uint256 remaining);
    
    event Transfer(address indexed _from,address indexed _to,uint256 _value);
    event Approval(address indexed _owner, address indexed _spender,uint256 _value);
}"},"owned.sol":{"content":"pragma solidity ^0.4.20;

contract Owned{
    address public owned;
    
    constructor () public {
        owned = msg.sender;
    }
    
    modifier onlyOwner {
        require(msg.sender == owned);
        _;
    }
    
    function transferOwnerShip(address newOwer) public onlyOwner {
        owned = newOwer;
    }
}
