// SPDX-License-Identifier: UNLICENSED
pragma solidity \u003e=0.4.22 \u003c0.8.0;

interface ISmartRightsCertify {
    function certifyHash(address _owner, bytes32 _hash) external;
    function certifyHash(bytes32 _hash) external;
    function getHashOwner(bytes32 _hash) external view returns(address);
    function addToWhitelist(address user) external;
}"},"SmartRightsCertify.sol":{"content":"pragma solidity \u003e=0.4.22 \u003c0.8.0;

import \"./ISmartRightsCertify.sol\";

contract SmartRightsCertify is ISmartRightsCertify {

    address owner;
    mapping(bytes32 =\u003e address) userCertifications;
    mapping(address =\u003e bool) whitelist;

    constructor() public {
        owner = msg.sender;
    }

    modifier onlyWhitelist() {
        require(whitelist[msg.sender] == true, \"NotInWhitelist\");
        _;
    }

    modifier onlyOwner() {
        require(msg.sender == owner, \"OnlyOwner\");
        _;
    }

    function certifyHash(address _owner, bytes32 _hash) external onlyOwner {
        require(userCertifications[_hash] == address(0), \"DuplicateData\");
        userCertifications[_hash] = _owner;
    }

    function certifyHash(bytes32 _hash) external onlyWhitelist {
        require(userCertifications[_hash] == address(0), \"DuplicateData\");
        userCertifications[_hash] = msg.sender;
    }

    function getHashOwner(bytes32 _hash) external view returns(address) {
        return userCertifications[_hash];
    }

    function addToWhitelist(address user) external onlyOwner {
        whitelist[user] = true;
    }
}
