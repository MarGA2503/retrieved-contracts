// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

/*
 * @dev Provides information about the current execution context, including the
 * sender of the transaction and its data. While these are generally available
 * via msg.sender and msg.data, they should not be accessed in such a direct
 * manner, since when dealing with meta-transactions the account sending and
 * paying for execution may not be the actual sender (as far as an application
 * is concerned).
 *
 * This contract is only required for intermediate, library-like contracts.
 */
abstract contract Context {
    function _msgSender() internal view virtual returns (address) {
        return msg.sender;
    }

    function _msgData() internal view virtual returns (bytes calldata) {
        this; // silence state mutability warning without generating bytecode - see https://github.com/ethereum/solidity/issues/2691
        return msg.data;
    }
}"},"HashBridge.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import \"./Ownable.sol\";
import \"./TransferHelper.sol\";

contract HashBridge is Ownable {
\taddress public signer;
\tuint public reservationTime;
\tuint public constant RATE_DECIMALS = 18;
\t
\tstruct Offer {
\t\taddress token;
\t\tuint amount;
\t\taddress payToken;
\t\tuint rate;
\t\taddress ownerAddress;
\t\taddress payAddress;
\t\tuint minPurchase;
\t\tbool active;
\t}
\tstruct Order {
\t\tuint offerId;
\t\tuint rate;
\t\taddress ownerAddress;
\t\taddress withdrawAddress;
\t\tuint amount;
\t\tuint payAmount;
\t\taddress payAddress;
\t\tuint reservedUntil;
\t\tbool complete;
\t}
\tstruct Payment {
\t\tuint orderId;
\t\tuint payAmount;
\t\taddress payToken;
\t\taddress payAddress;
\t}
\t
\tOffer[] offers;
\tOrder[] orders;
\tPayment[] payments;
\t
\tevent OfferAdd(uint indexed offerId, address indexed token, address indexed payToken, address ownerAddress, address payAddress, uint amount, uint rate, uint minPurchase, bool active);
\tevent OfferUpdate(uint indexed offerId, address payAddress, uint amount, uint rate, uint minPurchase, bool active);
\tevent OrderAdd(uint indexed orderId, uint indexed offerId, address indexed ownerAddress, uint rate, address withdrawAddress, uint amount, uint payAmount, address payAddress, uint reservedUntil);
\tevent OrderPay(uint indexed paymentId, uint indexed orderId, uint payAmount, address payToken, address payAddress);
\tevent OrderComplete(uint indexed orderId, uint offerAmount);
\t
\tconstructor(address _signer, uint _reservationTime) {
        signer = _signer;
\t\treservationTime = _reservationTime;
    }
\t
\tfunction changeSigner(address _signer) public onlyOwner {
        signer = _signer;
    }
\t
\tfunction changeReservationTime(uint _reservationTime) public onlyOwner {
\t\treservationTime = _reservationTime;
\t}
\t
\tfunction addOffer(address _token, uint _amount, address _payToken, uint _rate, address _payAddress, uint _minPurchase) public {
\t\trequire(_amount \u003e 0, \"Amount must be greater than 0\");
\t\trequire(_amount \u003e= _minPurchase, \"Amount must not be less than the minimum purchase\");
\t\trequire(_rate \u003e 0, \"Rate must be greater than 0\");
\t\tTransferHelper.safeTransferFrom(_token, msg.sender, address(this), _amount);
\t\tuint offerId = offers.length;
\t\toffers.push(Offer(_token, _amount, _payToken, _rate, msg.sender, _payAddress, _minPurchase, true));
\t\temit OfferAdd(offerId, _token, _payToken, msg.sender, _payAddress, _amount, _rate, _minPurchase, true);
\t}
\t
\tfunction updateOffer(uint _offerId, uint _amount, uint _rate, address _payAddress, uint _minPurchase) public {
\t\t_checkOfferAccess(_offerId);
\t\trequire(_rate \u003e 0, \"Rate must be greater than 0\");
\t\tuint blockedAmount = _getBlockedAmount(_offerId);
\t\trequire(_amount \u003e= blockedAmount, \"You can not withdraw tokens ordered by customers\");
\t\tif (_amount \u003e offers[_offerId].amount) {
\t\t\tTransferHelper.safeTransferFrom(offers[_offerId].token, msg.sender, address(this), _amount - offers[_offerId].amount);
\t\t} else {
\t\t\tTransferHelper.safeTransfer(offers[_offerId].token, msg.sender, offers[_offerId].amount - _amount);
\t\t}
\t\toffers[_offerId].amount = _amount;
\t\toffers[_offerId].rate = _rate;
\t\toffers[_offerId].payAddress = _payAddress;
\t\toffers[_offerId].minPurchase = _minPurchase;
\t\temit OfferUpdate(_offerId, _payAddress, _amount, _rate, _minPurchase, offers[_offerId].active);
\t}
\t
\tfunction activateOffer(uint _offerId) public {
\t\t_checkOfferAccess(_offerId);
\t\trequire(offers[_offerId].active == false, \"Offer is already active\");
\t\toffers[_offerId].active = true;
\t\temit OfferUpdate(_offerId, offers[_offerId].payAddress, offers[_offerId].amount, offers[_offerId].rate, offers[_offerId].minPurchase, true);
\t}
\t
\tfunction deactivateOffer(uint _offerId) public {
\t\t_checkOfferAccess(_offerId);
\t\trequire(offers[_offerId].active == true, \"Offer is already inactive\");
\t\toffers[_offerId].active = false;
\t\temit OfferUpdate(_offerId, offers[_offerId].payAddress, offers[_offerId].amount, offers[_offerId].rate, offers[_offerId].minPurchase, false);
\t}
\t
\tfunction addOrder(uint _offerId, address _withdrawAddress, uint _amount, uint _payAmount) public {
\t\trequire(_offerId \u003c offers.length, \"Incorrect offerId\");
\t\trequire(offers[_offerId].active == true, \"Offer is inactive\");
\t\trequire(_amount \u003e 0 || _payAmount \u003e 0, \"Amount must be greater than 0\");
\t\tuint rate = offers[_offerId].rate;
\t\tif (_amount \u003e 0) {
\t\t\t_payAmount = _amount * rate / (10 ** RATE_DECIMALS);
\t\t} else {
\t\t\t_amount = _payAmount * (10 ** RATE_DECIMALS) / rate;
\t\t}
\t\trequire(_amount \u003e= offers[_offerId].minPurchase, \"Amount is less than the minimum purchase\");
\t\tuint blockedAmount = _getBlockedAmount(_offerId);
\t\trequire(_amount \u003c= offers[_offerId].amount - blockedAmount, \"Not enough tokens in the offer\");
\t\taddress _payAddress = offers[_offerId].payAddress;
\t\tuint reservedUntil = block.timestamp + reservationTime;
\t\tuint orderId = orders.length;
\t\torders.push(Order(_offerId, rate, msg.sender, _withdrawAddress, _amount, _payAmount, _payAddress, reservedUntil, false));
\t\temit OrderAdd(orderId, _offerId, msg.sender, rate, _withdrawAddress, _amount, _payAmount, _payAddress, reservedUntil);
\t}
\t
\tfunction payOrder(uint _orderId, uint _payAmount, address _payToken, address _payAddress) public {
\t\trequire(_payAmount \u003e 0, \"Amount must be greater than 0\");
\t\tTransferHelper.safeTransferFrom(_payToken, msg.sender, _payAddress, _payAmount);
\t\tuint paymentId = payments.length;
\t\tpayments.push(Payment(_orderId, _payAmount, _payToken, _payAddress));
\t\temit OrderPay(paymentId, _orderId, _payAmount, _payToken, _payAddress);
\t}
\t
\tfunction withdrawTokens(uint _orderId, bytes calldata _sign) public {
\t\trequire(_orderId \u003c orders.length, \"Incorrect orderId\");
\t\trequire(orders[_orderId].complete == false, \"Tokens already withdrawn\");
\t\tuint offerId = orders[_orderId].offerId;
\t\tuint amount = orders[_orderId].amount;
\t\tuint payAmount = orders[_orderId].payAmount;
\t\taddress payToken = offers[offerId].payToken;
\t\taddress payAddress = orders[_orderId].payAddress;
\t\trequire(orders[_orderId].reservedUntil \u003e= block.timestamp || offers[offerId].amount - _getBlockedAmount(offerId) \u003e= amount, \"Not enough tokens in the offer\");
\t\tbytes32 data = keccak256(abi.encodePacked(_orderId, payAmount, payToken, payAddress));
\t\trequire(_verifySign(data, _sign), \"Incorrect signature\");
\t\tTransferHelper.safeTransfer(offers[offerId].token, orders[_orderId].withdrawAddress, amount);
\t\torders[_orderId].complete = true;
\t\toffers[offerId].amount -= amount;
\t\temit OrderComplete(_orderId, offers[offerId].amount);
\t}
\t
\tfunction _checkOfferAccess(uint _offerId) private view {
\t\trequire(_offerId \u003c offers.length, \"Incorrect offerId\");
\t\trequire(offers[_offerId].ownerAddress == msg.sender, \"Forbidden\");
\t}
\t
\tfunction _getBlockedAmount(uint _offerId) private view returns(uint blockedAmount) {
\t\tblockedAmount = 0;
\t\tfor (uint i = 0; i \u003c orders.length; i++) {
\t\t\tif (orders[i].offerId == _offerId \u0026\u0026 orders[i].complete == false \u0026\u0026 orders[i].reservedUntil \u003e= block.timestamp) {
\t\t\t\tblockedAmount += orders[i].amount;
\t\t\t}
\t\t}
\t}
\t
\tfunction _verifySign(bytes32 _data, bytes memory _sign) private view returns (bool) {
        bytes32 hash = _toEthBytes32SignedMessageHash(_data);
        address[] memory signList = _recoverAddresses(hash, _sign);
        return signList[0] == signer;
    }
\t
\tfunction _toEthBytes32SignedMessageHash (bytes32 _data) pure private returns (bytes32 signHash) {
        signHash = keccak256(abi.encodePacked(\"\\x19Ethereum Signed Message:\
32\", _data));
    }
\t
\tfunction _recoverAddresses(bytes32 _hash, bytes memory _signatures) pure private returns (address[] memory addresses) {
        uint8 v;
        bytes32 r;
        bytes32 s;
        uint count = _countSignatures(_signatures);
        addresses = new address[](count);
        for (uint i = 0; i \u003c count; i++) {
            (v, r, s) = _parseSignature(_signatures, i);
            addresses[i] = ecrecover(_hash, v, r, s);
        }
    }
\t
\tfunction _parseSignature(bytes memory _signatures, uint _pos) pure private returns (uint8 v, bytes32 r, bytes32 s) {
        uint offset = _pos * 65;
        assembly {
            r := mload(add(_signatures, add(32, offset)))
            s := mload(add(_signatures, add(64, offset)))
            v := and(mload(add(_signatures, add(65, offset))), 0xff)
        }
        if (v \u003c 27) v += 27;
        require(v == 27 || v == 28);
    }
    
    function _countSignatures(bytes memory _signatures) pure private returns (uint) {
        return _signatures.length % 65 == 0 ? _signatures.length / 65 : 0;
    }
}"},"Ownable.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import \"./Context.sol\";
/**
 * @dev Contract module which provides a basic access control mechanism, where
 * there is an account (an owner) that can be granted exclusive access to
 * specific functions.
 *
 * By default, the owner account will be the one that deploys the contract. This
 * can later be changed with {transferOwnership}.
 *
 * This module is used through inheritance. It will make available the modifier
 * `onlyOwner`, which can be applied to your functions to restrict their use to
 * the owner.
 */
abstract contract Ownable is Context {
    address private _owner;

    event OwnershipTransferred(address indexed previousOwner, address indexed newOwner);

    /**
     * @dev Initializes the contract setting the deployer as the initial owner.
     */
    constructor () {
        address msgSender = _msgSender();
        _owner = msgSender;
        emit OwnershipTransferred(address(0), msgSender);
    }

    /**
     * @dev Returns the address of the current owner.
     */
    function owner() public view virtual returns (address) {
        return _owner;
    }

    /**
     * @dev Throws if called by any account other than the owner.
     */
    modifier onlyOwner() {
        require(owner() == _msgSender(), \"Ownable: caller is not the owner\");
        _;
    }

    /**
     * @dev Leaves the contract without owner. It will not be possible to call
     * `onlyOwner` functions anymore. Can only be called by the current owner.
     *
     * NOTE: Renouncing ownership will leave the contract without an owner,
     * thereby removing any functionality that is only available to the owner.
     */
    function renounceOwnership() public virtual onlyOwner {
        emit OwnershipTransferred(_owner, address(0));
        _owner = address(0);
    }

    /**
     * @dev Transfers ownership of the contract to a new account (`newOwner`).
     * Can only be called by the current owner.
     */
    function transferOwnership(address newOwner) public virtual onlyOwner {
        require(newOwner != address(0), \"Ownable: new owner is the zero address\");
        emit OwnershipTransferred(_owner, newOwner);
        _owner = newOwner;
    }
}"},"TransferHelper.sol":{"content":"// SPDX-License-Identifier: GPL-3.0-or-later

pragma solidity \u003e=0.6.0;

// helper methods for interacting with ERC20 tokens and sending ETH that do not consistently return true/false
library TransferHelper {
    function safeApprove(
        address token,
        address to,
        uint256 value
    ) internal {
        // bytes4(keccak256(bytes(\u0027approve(address,uint256)\u0027)));
        (bool success, bytes memory data) = token.call(abi.encodeWithSelector(0x095ea7b3, to, value));
        require(
            success \u0026\u0026 (data.length == 0 || abi.decode(data, (bool))),
            \u0027TransferHelper::safeApprove: approve failed\u0027
        );
    }

    function safeTransfer(
        address token,
        address to,
        uint256 value
    ) internal {
        // bytes4(keccak256(bytes(\u0027transfer(address,uint256)\u0027)));
        (bool success, bytes memory data) = token.call(abi.encodeWithSelector(0xa9059cbb, to, value));
        require(
            success \u0026\u0026 (data.length == 0 || abi.decode(data, (bool))),
            \u0027TransferHelper::safeTransfer: transfer failed\u0027
        );
    }

    function safeTransferFrom(
        address token,
        address from,
        address to,
        uint256 value
    ) internal {
        // bytes4(keccak256(bytes(\u0027transferFrom(address,address,uint256)\u0027)));
        (bool success, bytes memory data) = token.call(abi.encodeWithSelector(0x23b872dd, from, to, value));
        require(
            success \u0026\u0026 (data.length == 0 || abi.decode(data, (bool))),
            \u0027TransferHelper::transferFrom: transferFrom failed\u0027
        );
    }

    function safeTransferETH(address to, uint256 value) internal {
        (bool success, ) = to.call{value: value}(new bytes(0));
        require(success, \u0027TransferHelper::safeTransferETH: ETH transfer failed\u0027);
    }
}
