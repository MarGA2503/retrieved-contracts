



pragma solidity ^0.6.0;
import \"../Initializable.sol\";

/*
 * @dev Provides information about the current execution context, including the
 * sender of the transaction and its data. While these are generally available
 * via msg.sender and msg.data, they should not be accessed in such a direct
 * manner, since when dealing with GSN meta-transactions the account sending and
 * paying for execution may not be the actual sender (as far as an application
 * is concerned).
 *
 * This contract is only required for intermediate, library-like contracts.
 */
contract ContextUpgradeSafe is Initializable {
    // Empty internal constructor, to prevent people from mistakenly deploying
    // an instance of this contract, which should be used via inheritance.

    function __Context_init() internal initializer {
        __Context_init_unchained();
    }

    function __Context_init_unchained() internal initializer {


    }


    function _msgSender() internal view virtual returns (address payable) {
        return msg.sender;
    }

    function _msgData() internal view virtual returns (bytes memory) {
        this; // silence state mutability warning without generating bytecode - see https://github.com/ethereum/solidity/issues/2691
        return msg.data;
    }

    uint256[50] private __gap;
}
",
      "keccak256": "0xe81686511d62f18b2d9c693c2c94c0a789c690de63aa90e15451ebf65c5bfd3e"
    
pragma solidity >=0.4.24 <0.7.0;


/**
 * @title Initializable
 *
 * @dev Helper contract to support initializer functions. To use it, replace
 * the constructor with a function that has the `initializer` modifier.
 * WARNING: Unlike constructors, initializer functions must be manually
 * invoked. This applies both to deploying an Initializable contract, as well
 * as extending an Initializable contract via inheritance.
 * WARNING: When used with inheritance, manual care must be taken to not invoke
 * a parent initializer twice, or ensure that all initializers are idempotent,
 * because this is not dealt with automatically as with constructors.
 */
contract Initializable {

  /**
   * @dev Indicates that the contract has been initialized.
   */
  bool private initialized;

  /**
   * @dev Indicates that the contract is in the process of being initialized.
   */
  bool private initializing;

  /**
   * @dev Modifier to use in the initializer function of a contract.
   */
  modifier initializer() {
    require(initializing || isConstructor() || !initialized, \"Contract instance has already been initialized\");

    bool isTopLevelCall = !initializing;
    if (isTopLevelCall) {
      initializing = true;
      initialized = true;
    }

    _;

    if (isTopLevelCall) {
      initializing = false;
    }
  }

  /// @dev Returns true if and only if the function is running in the constructor
  function isConstructor() private view returns (bool) {
    // extcodesize checks the size of the code stored in an address, and
    // address returns the current address. Since the code is still not
    // deployed when running a constructor, any checks on its code size will
    // yield zero, making it an effective way to detect if a contract is
    // under construction or not.
    address self = address(this);
    uint256 cs;
    assembly { cs := extcodesize(self) }
    return cs == 0;
  }

  // Reserved storage space to allow for layout changes in the future.
  uint256[50] private ______gap;
}
",
      "keccak256": "0x9bfec92e36234ecc99b5d37230acb6cd1f99560233753162204104a4897e8721"
    
pragma solidity ^0.6.0;

import \"../GSN/Context.sol\";
import \"../Initializable.sol\";
/**
 * @dev Contract module which provides a basic access control mechanism, where
 * there is an account (an owner) that can be granted exclusive access to
 * specific functions.
 *
 * By default, the owner account will be the one that deploys the contract. This
 * can later be changed with {transferOwnership}.
 *
 * This module is used through inheritance. It will make available the modifier
 * `onlyOwner`, which can be applied to your functions to restrict their use to
 * the owner.
 */
contract OwnableUpgradeSafe is Initializable, ContextUpgradeSafe {
    address private _owner;

    event OwnershipTransferred(address indexed previousOwner, address indexed newOwner);

    /**
     * @dev Initializes the contract setting the deployer as the initial owner.
     */

    function __Ownable_init() internal initializer {
        __Context_init_unchained();
        __Ownable_init_unchained();
    }

    function __Ownable_init_unchained() internal initializer {


        address msgSender = _msgSender();
        _owner = msgSender;
        emit OwnershipTransferred(address(0), msgSender);

    }


    /**
     * @dev Returns the address of the current owner.
     */
    function owner() public view returns (address) {
        return _owner;
    }

    /**
     * @dev Throws if called by any account other than the owner.
     */
    modifier onlyOwner() {
        require(_owner == _msgSender(), \"Ownable: caller is not the owner\");
        _;
    }

    /**
     * @dev Leaves the contract without owner. It will not be possible to call
     * `onlyOwner` functions anymore. Can only be called by the current owner.
     *
     * NOTE: Renouncing ownership will leave the contract without an owner,
     * thereby removing any functionality that is only available to the owner.
     */
    function renounceOwnership() public virtual onlyOwner {
        emit OwnershipTransferred(_owner, address(0));
        _owner = address(0);
    }

    /**
     * @dev Transfers ownership of the contract to a new account (`newOwner`).
     * Can only be called by the current owner.
     */
    function transferOwnership(address newOwner) public virtual onlyOwner {
        require(newOwner != address(0), \"Ownable: new owner is the zero address\");
        emit OwnershipTransferred(_owner, newOwner);
        _owner = newOwner;
    }

    uint256[49] private __gap;
}
",
      "keccak256": "0x04a69a78363214b4e3055db8e620bed222349f0c81e9d1cbe769eb849b69b73f"
    
pragma solidity ^0.6.0;

/**
 * @dev Interface for an ERC1820 implementer, as defined in the
 * https://eips.ethereum.org/EIPS/eip-1820#interface-implementation-erc1820implementerinterface[EIP].
 * Used by contracts that will be registered as implementers in the
 * {IERC1820Registry}.
 */
interface IERC1820Implementer {
    /**
     * @dev Returns a special value (`ERC1820_ACCEPT_MAGIC`) if this contract
     * implements `interfaceHash` for `account`.
     *
     * See {IERC1820Registry-setInterfaceImplementer}.
     */
    function canImplementInterfaceForAddress(bytes32 interfaceHash, address account) external view returns (bytes32);
}
",
      "keccak256": "0xd61eddb6efd8690bb0d1074a00e818e29009ffc6e36cd6dced4b6c6c27e24689"
    
pragma solidity ^0.6.0;

/**
 * @dev Interface of the global ERC1820 Registry, as defined in the
 * https://eips.ethereum.org/EIPS/eip-1820[EIP]. Accounts may register
 * implementers for interfaces in this registry, as well as query support.
 *
 * Implementers may be shared by multiple accounts, and can also implement more
 * than a single interface for each account. Contracts can implement interfaces
 * for themselves, but externally-owned accounts (EOA) must delegate this to a
 * contract.
 *
 * {IERC165} interfaces can also be queried via the registry.
 *
 * For an in-depth explanation and source code analysis, see the EIP text.
 */
interface IERC1820Registry {
    /**
     * @dev Sets `newManager` as the manager for `account`. A manager of an
     * account is able to set interface implementers for it.
     *
     * By default, each account is its own manager. Passing a value of `0x0` in
     * `newManager` will reset the manager to this initial state.
     *
     * Emits a {ManagerChanged} event.
     *
     * Requirements:
     *
     * - the caller must be the current manager for `account`.
     */
    function setManager(address account, address newManager) external;

    /**
     * @dev Returns the manager for `account`.
     *
     * See {setManager}.
     */
    function getManager(address account) external view returns (address);

    /**
     * @dev Sets the `implementer` contract as ``account``'s implementer for
     * `interfaceHash`.
     *
     * `account` being the zero address is an alias for the caller's address.
     * The zero address can also be used in `implementer` to remove an old one.
     *
     * See {interfaceHash} to learn how these are created.
     *
     * Emits an {InterfaceImplementerSet} event.
     *
     * Requirements:
     *
     * - the caller must be the current manager for `account`.
     * - `interfaceHash` must not be an {IERC165} interface id (i.e. it must not
     * end in 28 zeroes).
     * - `implementer` must implement {IERC1820Implementer} and return true when
     * queried for support, unless `implementer` is the caller. See
     * {IERC1820Implementer-canImplementInterfaceForAddress}.
     */
    function setInterfaceImplementer(address account, bytes32 interfaceHash, address implementer) external;

    /**
     * @dev Returns the implementer of `interfaceHash` for `account`. If no such
     * implementer is registered, returns the zero address.
     *
     * If `interfaceHash` is an {IERC165} interface id (i.e. it ends with 28
     * zeroes), `account` will be queried for support of it.
     *
     * `account` being the zero address is an alias for the caller's address.
     */
    function getInterfaceImplementer(address account, bytes32 interfaceHash) external view returns (address);

    /**
     * @dev Returns the interface hash for an `interfaceName`, as defined in the
     * corresponding
     * https://eips.ethereum.org/EIPS/eip-1820#interface-name[section of the EIP].
     */
    function interfaceHash(string calldata interfaceName) external pure returns (bytes32);

    /**
     *  @notice Updates the cache with whether the contract implements an ERC165 interface or not.
     *  @param account Address of the contract for which to update the cache.
     *  @param interfaceId ERC165 interface for which to update the cache.
     */
    function updateERC165Cache(address account, bytes4 interfaceId) external;

    /**
     *  @notice Checks whether a contract implements an ERC165 interface or not.
     *  If the result is not cached a direct lookup on the contract address is performed.
     *  If the result is not cached or the cached value is out-of-date, the cache MUST be updated manually by calling
     *  {updateERC165Cache} with the contract address.
     *  @param account Address of the contract to check.
     *  @param interfaceId ERC165 interface to check.
     *  @return True if `account` implements `interfaceId`, false otherwise.
     */
    function implementsERC165Interface(address account, bytes4 interfaceId) external view returns (bool);

    /**
     *  @notice Checks whether a contract implements an ERC165 interface or not without using nor updating the cache.
     *  @param account Address of the contract to check.
     *  @param interfaceId ERC165 interface to check.
     *  @return True if `account` implements `interfaceId`, false otherwise.
     */
    function implementsERC165InterfaceNoCache(address account, bytes4 interfaceId) external view returns (bool);

    event InterfaceImplementerSet(address indexed account, bytes32 indexed interfaceHash, address indexed implementer);

    event ManagerChanged(address indexed account, address indexed newManager);
}
",
      "keccak256": "0xf96602898a507855ea4a0b3a2576e787433a9bba9ca555fadf16ffc03d15a6fe"
    
pragma solidity ^0.6.0;

/**
 * @dev Interface of the ERC20 standard as defined in the EIP.
 */
interface IERC20 {
    /**
     * @dev Returns the amount of tokens in existence.
     */
    function totalSupply() external view returns (uint256);

    /**
     * @dev Returns the amount of tokens owned by `account`.
     */
    function balanceOf(address account) external view returns (uint256);

    /**
     * @dev Moves `amount` tokens from the caller's account to `recipient`.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transfer(address recipient, uint256 amount) external returns (bool);

    /**
     * @dev Returns the remaining number of tokens that `spender` will be
     * allowed to spend on behalf of `owner` through {transferFrom}. This is
     * zero by default.
     *
     * This value changes when {approve} or {transferFrom} are called.
     */
    function allowance(address owner, address spender) external view returns (uint256);

    /**
     * @dev Sets `amount` as the allowance of `spender` over the caller's tokens.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * IMPORTANT: Beware that changing an allowance with this method brings the risk
     * that someone may use both the old and the new allowance by unfortunate
     * transaction ordering. One possible solution to mitigate this race
     * condition is to first reduce the spender's allowance to 0 and set the
     * desired value afterwards:
     * https://github.com/ethereum/EIPs/issues/20#issuecomment-263524729
     *
     * Emits an {Approval} event.
     */
    function approve(address spender, uint256 amount) external returns (bool);

    /**
     * @dev Moves `amount` tokens from `sender` to `recipient` using the
     * allowance mechanism. `amount` is then deducted from the caller's
     * allowance.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transferFrom(address sender, address recipient, uint256 amount) external returns (bool);

    /**
     * @dev Emitted when `value` tokens are moved from one account (`from`) to
     * another (`to`).
     *
     * Note that `value` may be zero.
     */
    event Transfer(address indexed from, address indexed to, uint256 value);

    /**
     * @dev Emitted when the allowance of a `spender` for an `owner` is set by
     * a call to {approve}. `value` is the new allowance.
     */
    event Approval(address indexed owner, address indexed spender, uint256 value);
}
",
      "keccak256": "0x6cc1cb934a3ac2137a7dcaed018af9e235392236ceecfd3687259702b9c767ad"
    
pragma solidity ^0.6.0;

/**
 * @dev Interface of the ERC777Token standard as defined in the EIP.
 *
 * This contract uses the
 * https://eips.ethereum.org/EIPS/eip-1820[ERC1820 registry standard] to let
 * token holders and recipients react to token movements by using setting implementers
 * for the associated interfaces in said registry. See {IERC1820Registry} and
 * {ERC1820Implementer}.
 */
interface IERC777 {
    /**
     * @dev Returns the name of the token.
     */
    function name() external view returns (string memory);

    /**
     * @dev Returns the symbol of the token, usually a shorter version of the
     * name.
     */
    function symbol() external view returns (string memory);

    /**
     * @dev Returns the smallest part of the token that is not divisible. This
     * means all token operations (creation, movement and destruction) must have
     * amounts that are a multiple of this number.
     *
     * For most token contracts, this value will equal 1.
     */
    function granularity() external view returns (uint256);

    /**
     * @dev Returns the amount of tokens in existence.
     */
    function totalSupply() external view returns (uint256);

    /**
     * @dev Returns the amount of tokens owned by an account (`owner`).
     */
    function balanceOf(address owner) external view returns (uint256);

    /**
     * @dev Moves `amount` tokens from the caller's account to `recipient`.
     *
     * If send or receive hooks are registered for the caller and `recipient`,
     * the corresponding functions will be called with `data` and empty
     * `operatorData`. See {IERC777Sender} and {IERC777Recipient}.
     *
     * Emits a {Sent} event.
     *
     * Requirements
     *
     * - the caller must have at least `amount` tokens.
     * - `recipient` cannot be the zero address.
     * - if `recipient` is a contract, it must implement the {IERC777Recipient}
     * interface.
     */
    function send(address recipient, uint256 amount, bytes calldata data) external;

    /**
     * @dev Destroys `amount` tokens from the caller's account, reducing the
     * total supply.
     *
     * If a send hook is registered for the caller, the corresponding function
     * will be called with `data` and empty `operatorData`. See {IERC777Sender}.
     *
     * Emits a {Burned} event.
     *
     * Requirements
     *
     * - the caller must have at least `amount` tokens.
     */
    function burn(uint256 amount, bytes calldata data) external;

    /**
     * @dev Returns true if an account is an operator of `tokenHolder`.
     * Operators can send and burn tokens on behalf of their owners. All
     * accounts are their own operator.
     *
     * See {operatorSend} and {operatorBurn}.
     */
    function isOperatorFor(address operator, address tokenHolder) external view returns (bool);

    /**
     * @dev Make an account an operator of the caller.
     *
     * See {isOperatorFor}.
     *
     * Emits an {AuthorizedOperator} event.
     *
     * Requirements
     *
     * - `operator` cannot be calling address.
     */
    function authorizeOperator(address operator) external;

    /**
     * @dev Revoke an account's operator status for the caller.
     *
     * See {isOperatorFor} and {defaultOperators}.
     *
     * Emits a {RevokedOperator} event.
     *
     * Requirements
     *
     * - `operator` cannot be calling address.
     */
    function revokeOperator(address operator) external;

    /**
     * @dev Returns the list of default operators. These accounts are operators
     * for all token holders, even if {authorizeOperator} was never called on
     * them.
     *
     * This list is immutable, but individual holders may revoke these via
     * {revokeOperator}, in which case {isOperatorFor} will return false.
     */
    function defaultOperators() external view returns (address[] memory);

    /**
     * @dev Moves `amount` tokens from `sender` to `recipient`. The caller must
     * be an operator of `sender`.
     *
     * If send or receive hooks are registered for `sender` and `recipient`,
     * the corresponding functions will be called with `data` and
     * `operatorData`. See {IERC777Sender} and {IERC777Recipient}.
     *
     * Emits a {Sent} event.
     *
     * Requirements
     *
     * - `sender` cannot be the zero address.
     * - `sender` must have at least `amount` tokens.
     * - the caller must be an operator for `sender`.
     * - `recipient` cannot be the zero address.
     * - if `recipient` is a contract, it must implement the {IERC777Recipient}
     * interface.
     */
    function operatorSend(
        address sender,
        address recipient,
        uint256 amount,
        bytes calldata data,
        bytes calldata operatorData
    ) external;

    /**
     * @dev Destroys `amount` tokens from `account`, reducing the total supply.
     * The caller must be an operator of `account`.
     *
     * If a send hook is registered for `account`, the corresponding function
     * will be called with `data` and `operatorData`. See {IERC777Sender}.
     *
     * Emits a {Burned} event.
     *
     * Requirements
     *
     * - `account` cannot be the zero address.
     * - `account` must have at least `amount` tokens.
     * - the caller must be an operator for `account`.
     */
    function operatorBurn(
        address account,
        uint256 amount,
        bytes calldata data,
        bytes calldata operatorData
    ) external;

    event Sent(
        address indexed operator,
        address indexed from,
        address indexed to,
        uint256 amount,
        bytes data,
        bytes operatorData
    );

    event Minted(address indexed operator, address indexed to, uint256 amount, bytes data, bytes operatorData);

    event Burned(address indexed operator, address indexed from, uint256 amount, bytes data, bytes operatorData);

    event AuthorizedOperator(address indexed operator, address indexed tokenHolder);

    event RevokedOperator(address indexed operator, address indexed tokenHolder);
}
",
      "keccak256": "0x5d9e02a26cd8ae92627495d65becc3e122e6eef07ed5911db3cdbf168c3f59fa"
    
pragma solidity ^0.6.0;

/**
 * @dev Interface of the ERC777TokensRecipient standard as defined in the EIP.
 *
 * Accounts can be notified of {IERC777} tokens being sent to them by having a
 * contract implement this interface (contract holders can be their own
 * implementer) and registering it on the
 * https://eips.ethereum.org/EIPS/eip-1820[ERC1820 global registry].
 *
 * See {IERC1820Registry} and {ERC1820Implementer}.
 */
interface IERC777Recipient {
    /**
     * @dev Called by an {IERC777} token contract whenever tokens are being
     * moved or created into a registered account (`to`). The type of operation
     * is conveyed by `from` being the zero address or not.
     *
     * This call occurs _after_ the token contract's state is updated, so
     * {IERC777-balanceOf}, etc., can be used to query the post-operation state.
     *
     * This function may revert to prevent the operation from being executed.
     */
    function tokensReceived(
        address operator,
        address from,
        address to,
        uint256 amount,
        bytes calldata userData,
        bytes calldata operatorData
    ) external;
}
",
      "keccak256": "0x55a013725aefc9de0c0f6fca871e7edef78ff30481175461f4e06cdcd20bf72a"
    
pragma solidity ^0.6.0;

/**
 * @dev Interface of the ERC777TokensSender standard as defined in the EIP.
 *
 * {IERC777} Token holders can be notified of operations performed on their
 * tokens by having a contract implement this interface (contract holders can be
 *  their own implementer) and registering it on the
 * https://eips.ethereum.org/EIPS/eip-1820[ERC1820 global registry].
 *
 * See {IERC1820Registry} and {ERC1820Implementer}.
 */
interface IERC777Sender {
    /**
     * @dev Called by an {IERC777} token contract whenever a registered holder's
     * (`from`) tokens are about to be moved or destroyed. The type of operation
     * is conveyed by `to` being the zero address or not.
     *
     * This call occurs _before_ the token contract's state is updated, so
     * {IERC777-balanceOf}, etc., can be used to query the pre-operation state.
     *
     * This function may revert to prevent the operation from being executed.
     */
    function tokensToSend(
        address operator,
        address from,
        address to,
        uint256 amount,
        bytes calldata userData,
        bytes calldata operatorData
    ) external;
}
",
      "keccak256": "0x32a1be516e63f5926d5cea799c2e1b8619b11d10ebf8909d4f3bb1cce5ab0eec"
    
pragma solidity ^0.6.12;

import \"./external/MixedPodInterface.sol\";
import \"@openzeppelin/contracts-ethereum-package/contracts/access/Ownable.sol\";
import \"@openzeppelin/contracts-ethereum-package/contracts/token/ERC20/IERC20.sol\";
import \"@openzeppelin/contracts-ethereum-package/contracts/token/ERC777/IERC777.sol\";
import \"@openzeppelin/contracts-ethereum-package/contracts/token/ERC777/IERC777Recipient.sol\";
import \"@openzeppelin/contracts-ethereum-package/contracts/token/ERC777/IERC777Sender.sol\";
import \"@openzeppelin/contracts-ethereum-package/contracts/introspection/IERC1820Registry.sol\";
import \"@openzeppelin/contracts-ethereum-package/contracts/introspection/IERC1820Implementer.sol\";

contract MigrateV2ToV3 is OwnableUpgradeSafe, IERC777Recipient, IERC1820Implementer {

  IERC1820Registry constant internal ERC1820_REGISTRY = IERC1820Registry(0x1820a4B7618BdE71Dce8cdc73aAB6C95905faD24);

  bytes32 constant private _ERC1820_ACCEPT_MAGIC = keccak256(abi.encodePacked(\"ERC1820_ACCEPT_MAGIC\"));

  // keccak256(\"ERC777TokensRecipient\")
  bytes32 constant internal TOKENS_RECIPIENT_INTERFACE_HASH =
  0xb281fc8c12954d22544db45de3159a39272895b169a852b314f9cc762e44c53b;

  event ReceivedTokens(address token, address from, uint256 amount);

  IERC777 public poolDaiToken;
  IERC777 public poolUsdcToken;
  MixedPodInterface public poolDaiPod;
  MixedPodInterface public poolUsdcPod;
  IERC20 public v3Token;

  constructor (
    IERC777 _poolDaiToken,
    IERC777 _poolUsdcToken,
    MixedPodInterface _poolDaiPod,
    MixedPodInterface _poolUsdcPod,
    IERC20 _v3Token
  ) public {
    poolDaiToken = _poolDaiToken;
    poolUsdcToken = _poolUsdcToken;
    poolDaiPod = _poolDaiPod;
    poolUsdcPod = _poolUsdcPod;
    v3Token = _v3Token;

    // register interfaces
    ERC1820_REGISTRY.setInterfaceImplementer(address(this), TOKENS_RECIPIENT_INTERFACE_HASH, address(this));

    __Ownable_init();
  }

  function tokensReceived(
    address,
    address from,
    address to,
    uint256 amount,
    bytes calldata,
    bytes calldata
  ) external override {
    require(to == address(this), \"MigrateV2ToV3/only-tokens\");

    if (msg.sender == address(poolDaiToken)) {
      v3Token.transfer(from, amount);
    } else if (msg.sender == address(poolUsdcToken)) {
      v3Token.transfer(from, amount * 1e12);
    } else if (msg.sender == address(poolDaiPod)) {
      uint256 collateral = poolDaiPod.tokenToCollateralValue(amount);
      v3Token.transfer(from, collateral);
    } else if (msg.sender == address(poolUsdcPod)) {
      uint256 collateral = poolUsdcPod.tokenToCollateralValue(amount);
      v3Token.transfer(from, collateral * 1e12);
    } else {
      revert(\"MigrateV2ToV3/unknown-token\");
    }

    emit ReceivedTokens(msg.sender, from, amount);
  }

  function withdrawERC777(IERC777 token) external onlyOwner {
    uint256 amount = token.balanceOf(address(this));
    token.send(msg.sender, amount, \"\");
  }

  function withdrawERC20(IERC20 token) external onlyOwner {
    uint256 amount = token.balanceOf(address(this));
    token.transfer(msg.sender, amount);
  }

  function canImplementInterfaceForAddress(bytes32 interfaceHash, address account) external override view returns (bytes32) {
    if (account == address(this) && interfaceHash == TOKENS_RECIPIENT_INTERFACE_HASH) {
      return _ERC1820_ACCEPT_MAGIC;
    } else {
      return bytes32(0x00);
    }
  }
}
",
      "keccak256": "0x6037ec10c5f236a25879dd1aaa624385b91017c3f24e97252558ef614e75c763"
    
pragma solidity ^0.6.12;

import \"@openzeppelin/contracts-ethereum-package/contracts/token/ERC777/IERC777.sol\";
import \"./PodInterface.sol\";

interface MixedPodInterface is IERC777, PodInterface {
}",
      "keccak256": "0xefc31590b7f7cab3c00abfa1821f0377c1416ddd502a71fb4ffce39cd0a2a8aa"
    
pragma solidity ^0.6.12;

interface PodInterface {
  function tokenToCollateralValue(uint256 tokens) external view returns (uint256);
  function balanceOfUnderlying(address user) external view returns (uint256);
}",
      "keccak256": "0x8f00a2c1e39e2a2dd749a6e05beb743158379d6899e99cb2981c3f5ab8a0e2f7"
    }
  }
}
pragma solidity ^0.6.12;

interface PodInterface {
  function tokenToCollateralValue(uint256 tokens) external view returns (uint256);
  function balanceOfUnderlying(address user) external view returns (uint256);
}",
      "keccak256": "0x8f00a2c1e39e2a2dd749a6e05beb743158379d6899e99cb2981c3f5ab8a0e2f7"
    }
  }
}
pragma solidity ^0.6.12;

interface PodInterface {
  function tokenToCollateralValue(uint256 tokens) external view returns (uint256);
  function balanceOfUnderlying(address user) external view returns (uint256);
}",
      "keccak256": "0x8f00a2c1e39e2a2dd749a6e05beb743158379d6899e99cb2981c3f5ab8a0e2f7"
    }
  }
}
pragma solidity ^0.6.12;

interface PodInterface {
  function tokenToCollateralValue(uint256 tokens) external view returns (uint256);
  function balanceOfUnderlying(address user) external view returns (uint256);
}",
      "keccak256": "0x8f00a2c1e39e2a2dd749a6e05beb743158379d6899e99cb2981c3f5ab8a0e2f7"
    }
  }
}
pragma solidity ^0.6.12;

interface PodInterface {
  function tokenToCollateralValue(uint256 tokens) external view returns (uint256);
  function balanceOfUnderlying(address user) external view returns (uint256);
}",
      "keccak256": "0x8f00a2c1e39e2a2dd749a6e05beb743158379d6899e99cb2981c3f5ab8a0e2f7"
    }
  }
}
pragma solidity ^0.6.12;

interface PodInterface {
  function tokenToCollateralValue(uint256 tokens) external view returns (uint256);
  function balanceOfUnderlying(address user) external view returns (uint256);
}",
      "keccak256": "0x8f00a2c1e39e2a2dd749a6e05beb743158379d6899e99cb2981c3f5ab8a0e2f7"
    }
  }
}
pragma solidity ^0.6.12;

interface PodInterface {
  function tokenToCollateralValue(uint256 tokens) external view returns (uint256);
  function balanceOfUnderlying(address user) external view returns (uint256);
}",
      "keccak256": "0x8f00a2c1e39e2a2dd749a6e05beb743158379d6899e99cb2981c3f5ab8a0e2f7"
    }
  }
}
pragma solidity ^0.6.12;

interface PodInterface {
  function tokenToCollateralValue(uint256 tokens) external view returns (uint256);
  function balanceOfUnderlying(address user) external view returns (uint256);
}",
      "keccak256": "0x8f00a2c1e39e2a2dd749a6e05beb743158379d6899e99cb2981c3f5ab8a0e2f7"
    }
  }
}
pragma solidity ^0.6.12;

interface PodInterface {
  function tokenToCollateralValue(uint256 tokens) external view returns (uint256);
  function balanceOfUnderlying(address user) external view returns (uint256);
}",
      "keccak256": "0x8f00a2c1e39e2a2dd749a6e05beb743158379d6899e99cb2981c3f5ab8a0e2f7"
    }
  }
}
pragma solidity ^0.6.12;

interface PodInterface {
  function tokenToCollateralValue(uint256 tokens) external view returns (uint256);
  function balanceOfUnderlying(address user) external view returns (uint256);
}",
      "keccak256": "0x8f00a2c1e39e2a2dd749a6e05beb743158379d6899e99cb2981c3f5ab8a0e2f7"
    }
  }
}
pragma solidity ^0.6.12;

interface PodInterface {
  function tokenToCollateralValue(uint256 tokens) external view returns (uint256);
  function balanceOfUnderlying(address user) external view returns (uint256);
}",
      "keccak256": "0x8f00a2c1e39e2a2dd749a6e05beb743158379d6899e99cb2981c3f5ab8a0e2f7"
    }
  }
}
pragma solidity ^0.6.12;

interface PodInterface {
  function tokenToCollateralValue(uint256 tokens) external view returns (uint256);
  function balanceOfUnderlying(address user) external view returns (uint256);
}",
      "keccak256": "0x8f00a2c1e39e2a2dd749a6e05beb743158379d6899e99cb2981c3f5ab8a0e2f7"
    }
  }
}
pragma solidity ^0.6.12;

interface PodInterface {
  function tokenToCollateralValue(uint256 tokens) external view returns (uint256);
  function balanceOfUnderlying(address user) external view returns (uint256);
}",
      "keccak256": "0x8f00a2c1e39e2a2dd749a6e05beb743158379d6899e99cb2981c3f5ab8a0e2f7"
    }
  }
}
pragma solidity ^0.6.12;

interface PodInterface {
  function tokenToCollateralValue(uint256 tokens) external view returns (uint256);
  function balanceOfUnderlying(address user) external view returns (uint256);
}",
      "keccak256": "0x8f00a2c1e39e2a2dd749a6e05beb743158379d6899e99cb2981c3f5ab8a0e2f7"
    }
  }
}
pragma solidity ^0.6.12;

interface PodInterface {
  function tokenToCollateralValue(uint256 tokens) external view returns (uint256);
  function balanceOfUnderlying(address user) external view returns (uint256);
}",
      "keccak256": "0x8f00a2c1e39e2a2dd749a6e05beb743158379d6899e99cb2981c3f5ab8a0e2f7"
    }
  }
}
pragma solidity ^0.6.12;

interface PodInterface {
  function tokenToCollateralValue(uint256 tokens) external view returns (uint256);
  function balanceOfUnderlying(address user) external view returns (uint256);
}",
      "keccak256": "0x8f00a2c1e39e2a2dd749a6e05beb743158379d6899e99cb2981c3f5ab8a0e2f7"
    }
  }
}
pragma solidity ^0.6.12;

interface PodInterface {
  function tokenToCollateralValue(uint256 tokens) external view returns (uint256);
  function balanceOfUnderlying(address user) external view returns (uint256);
}",
      "keccak256": "0x8f00a2c1e39e2a2dd749a6e05beb743158379d6899e99cb2981c3f5ab8a0e2f7"
    }
  }
}
pragma solidity ^0.6.12;

interface PodInterface {
  function tokenToCollateralValue(uint256 tokens) external view returns (uint256);
  function balanceOfUnderlying(address user) external view returns (uint256);
}",
      "keccak256": "0x8f00a2c1e39e2a2dd749a6e05beb743158379d6899e99cb2981c3f5ab8a0e2f7"
    }
  }
}
pragma solidity ^0.6.12;

interface PodInterface {
  function tokenToCollateralValue(uint256 tokens) external view returns (uint256);
  function balanceOfUnderlying(address user) external view returns (uint256);
}",
      "keccak256": "0x8f00a2c1e39e2a2dd749a6e05beb743158379d6899e99cb2981c3f5ab8a0e2f7"
    }
  }
}
pragma solidity ^0.6.12;

interface PodInterface {
  function tokenToCollateralValue(uint256 tokens) external view returns (uint256);
  function balanceOfUnderlying(address user) external view returns (uint256);
}",
      "keccak256": "0x8f00a2c1e39e2a2dd749a6e05beb743158379d6899e99cb2981c3f5ab8a0e2f7"
    }
  }
}
pragma solidity ^0.6.12;

interface PodInterface {
  function tokenToCollateralValue(uint256 tokens) external view returns (uint256);
  function balanceOfUnderlying(address user) external view returns (uint256);
}",
      "keccak256": "0x8f00a2c1e39e2a2dd749a6e05beb743158379d6899e99cb2981c3f5ab8a0e2f7"
    }
  }
}
pragma solidity ^0.6.12;

interface PodInterface {
  function tokenToCollateralValue(uint256 tokens) external view returns (uint256);
  function balanceOfUnderlying(address user) external view returns (uint256);
}",
      "keccak256": "0x8f00a2c1e39e2a2dd749a6e05beb743158379d6899e99cb2981c3f5ab8a0e2f7"
    }
  }
}
pragma solidity ^0.6.12;

interface PodInterface {
  function tokenToCollateralValue(uint256 tokens) external view returns (uint256);
  function balanceOfUnderlying(address user) external view returns (uint256);
}",
      "keccak256": "0x8f00a2c1e39e2a2dd749a6e05beb743158379d6899e99cb2981c3f5ab8a0e2f7"
    }
  }
}
pragma solidity ^0.6.12;

interface PodInterface {
  function tokenToCollateralValue(uint256 tokens) external view returns (uint256);
  function balanceOfUnderlying(address user) external view returns (uint256);
}",
      "keccak256": "0x8f00a2c1e39e2a2dd749a6e05beb743158379d6899e99cb2981c3f5ab8a0e2f7"
    }
  }
}
pragma solidity ^0.6.12;

interface PodInterface {
  function tokenToCollateralValue(uint256 tokens) external view returns (uint256);
  function balanceOfUnderlying(address user) external view returns (uint256);
}",
      "keccak256": "0x8f00a2c1e39e2a2dd749a6e05beb743158379d6899e99cb2981c3f5ab8a0e2f7"
    }
  }
}
pragma solidity ^0.6.12;

interface PodInterface {
  function tokenToCollateralValue(uint256 tokens) external view returns (uint256);
  function balanceOfUnderlying(address user) external view returns (uint256);
}",
      "keccak256": "0x8f00a2c1e39e2a2dd749a6e05beb743158379d6899e99cb2981c3f5ab8a0e2f7"
    }
  }
}
pragma solidity ^0.6.12;

interface PodInterface {
  function tokenToCollateralValue(uint256 tokens) external view returns (uint256);
  function balanceOfUnderlying(address user) external view returns (uint256);
}",
      "keccak256": "0x8f00a2c1e39e2a2dd749a6e05beb743158379d6899e99cb2981c3f5ab8a0e2f7"
    }
  }
}
pragma solidity ^0.6.12;

interface PodInterface {
  function tokenToCollateralValue(uint256 tokens) external view returns (uint256);
  function balanceOfUnderlying(address user) external view returns (uint256);
}",
      "keccak256": "0x8f00a2c1e39e2a2dd749a6e05beb743158379d6899e99cb2981c3f5ab8a0e2f7"
    }
  }
}
pragma solidity ^0.6.12;

interface PodInterface {
  function tokenToCollateralValue(uint256 tokens) external view returns (uint256);
  function balanceOfUnderlying(address user) external view returns (uint256);
}",
      "keccak256": "0x8f00a2c1e39e2a2dd749a6e05beb743158379d6899e99cb2981c3f5ab8a0e2f7"
    }
  }
}
pragma solidity ^0.6.12;

interface PodInterface {
  function tokenToCollateralValue(uint256 tokens) external view returns (uint256);
  function balanceOfUnderlying(address user) external view returns (uint256);
}",
      "keccak256": "0x8f00a2c1e39e2a2dd749a6e05beb743158379d6899e99cb2981c3f5ab8a0e2f7"
    }
  }
}
pragma solidity ^0.6.12;

interface PodInterface {
  function tokenToCollateralValue(uint256 tokens) external view returns (uint256);
  function balanceOfUnderlying(address user) external view returns (uint256);
}",
      "keccak256": "0x8f00a2c1e39e2a2dd749a6e05beb743158379d6899e99cb2981c3f5ab8a0e2f7"
    }
  }
}
pragma solidity ^0.6.12;

interface PodInterface {
  function tokenToCollateralValue(uint256 tokens) external view returns (uint256);
  function balanceOfUnderlying(address user) external view returns (uint256);
}",
      "keccak256": "0x8f00a2c1e39e2a2dd749a6e05beb743158379d6899e99cb2981c3f5ab8a0e2f7"
    }
  }
}
pragma solidity ^0.6.12;

interface PodInterface {
  function tokenToCollateralValue(uint256 tokens) external view returns (uint256);
  function balanceOfUnderlying(address user) external view returns (uint256);
}",
      "keccak256": "0x8f00a2c1e39e2a2dd749a6e05beb743158379d6899e99cb2981c3f5ab8a0e2f7"
    }
  }
}
pragma solidity ^0.6.12;

interface PodInterface {
  function tokenToCollateralValue(uint256 tokens) external view returns (uint256);
  function balanceOfUnderlying(address user) external view returns (uint256);
}",
      "keccak256": "0x8f00a2c1e39e2a2dd749a6e05beb743158379d6899e99cb2981c3f5ab8a0e2f7"
    }
  }
}
pragma solidity ^0.6.12;

interface PodInterface {
  function tokenToCollateralValue(uint256 tokens) external view returns (uint256);
  function balanceOfUnderlying(address user) external view returns (uint256);
}",
      "keccak256": "0x8f00a2c1e39e2a2dd749a6e05beb743158379d6899e99cb2981c3f5ab8a0e2f7"
    }
  }
}
pragma solidity ^0.6.12;

interface PodInterface {
  function tokenToCollateralValue(uint256 tokens) external view returns (uint256);
  function balanceOfUnderlying(address user) external view returns (uint256);
}",
      "keccak256": "0x8f00a2c1e39e2a2dd749a6e05beb743158379d6899e99cb2981c3f5ab8a0e2f7"
    }
  }
}
pragma solidity ^0.6.12;

interface PodInterface {
  function tokenToCollateralValue(uint256 tokens) external view returns (uint256);
  function balanceOfUnderlying(address user) external view returns (uint256);
}",
      "keccak256": "0x8f00a2c1e39e2a2dd749a6e05beb743158379d6899e99cb2981c3f5ab8a0e2f7"
    }
  }
}
pragma solidity ^0.6.12;

interface PodInterface {
  function tokenToCollateralValue(uint256 tokens) external view returns (uint256);
  function balanceOfUnderlying(address user) external view returns (uint256);
}",
      "keccak256": "0x8f00a2c1e39e2a2dd749a6e05beb743158379d6899e99cb2981c3f5ab8a0e2f7"
    }
  }
}
pragma solidity ^0.6.12;

interface PodInterface {
  function tokenToCollateralValue(uint256 tokens) external view returns (uint256);
  function balanceOfUnderlying(address user) external view returns (uint256);
}",
      "keccak256": "0x8f00a2c1e39e2a2dd749a6e05beb743158379d6899e99cb2981c3f5ab8a0e2f7"
    }
  }
}
pragma solidity ^0.6.12;

interface PodInterface {
  function tokenToCollateralValue(uint256 tokens) external view returns (uint256);
  function balanceOfUnderlying(address user) external view returns (uint256);
}",
      "keccak256": "0x8f00a2c1e39e2a2dd749a6e05beb743158379d6899e99cb2981c3f5ab8a0e2f7"
    }
  }
}
pragma solidity ^0.6.12;

interface PodInterface {
  function tokenToCollateralValue(uint256 tokens) external view returns (uint256);
  function balanceOfUnderlying(address user) external view returns (uint256);
}",
      "keccak256": "0x8f00a2c1e39e2a2dd749a6e05beb743158379d6899e99cb2981c3f5ab8a0e2f7"
    }
  }
}
pragma solidity ^0.6.12;

interface PodInterface {
  function tokenToCollateralValue(uint256 tokens) external view returns (uint256);
  function balanceOfUnderlying(address user) external view returns (uint256);
}",
      "keccak256": "0x8f00a2c1e39e2a2dd749a6e05beb743158379d6899e99cb2981c3f5ab8a0e2f7"
    }
  }
}
pragma solidity ^0.6.12;

interface PodInterface {
  function tokenToCollateralValue(uint256 tokens) external view returns (uint256);
  function balanceOfUnderlying(address user) external view returns (uint256);
}",
      "keccak256": "0x8f00a2c1e39e2a2dd749a6e05beb743158379d6899e99cb2981c3f5ab8a0e2f7"
    }
  }
}
pragma solidity ^0.6.12;

interface PodInterface {
  function tokenToCollateralValue(uint256 tokens) external view returns (uint256);
  function balanceOfUnderlying(address user) external view returns (uint256);
}",
      "keccak256": "0x8f00a2c1e39e2a2dd749a6e05beb743158379d6899e99cb2981c3f5ab8a0e2f7"
    }
  }
}
pragma solidity ^0.6.12;

interface PodInterface {
  function tokenToCollateralValue(uint256 tokens) external view returns (uint256);
  function balanceOfUnderlying(address user) external view returns (uint256);
}",
      "keccak256": "0x8f00a2c1e39e2a2dd749a6e05beb743158379d6899e99cb2981c3f5ab8a0e2f7"
    }
  }
}
pragma solidity ^0.6.12;

interface PodInterface {
  function tokenToCollateralValue(uint256 tokens) external view returns (uint256);
  function balanceOfUnderlying(address user) external view returns (uint256);
}",
      "keccak256": "0x8f00a2c1e39e2a2dd749a6e05beb743158379d6899e99cb2981c3f5ab8a0e2f7"
    }
  }
}
pragma solidity ^0.6.12;

interface PodInterface {
  function tokenToCollateralValue(uint256 tokens) external view returns (uint256);
  function balanceOfUnderlying(address user) external view returns (uint256);
}",
      "keccak256": "0x8f00a2c1e39e2a2dd749a6e05beb743158379d6899e99cb2981c3f5ab8a0e2f7"
    }
  }
}
pragma solidity ^0.6.12;

interface PodInterface {
  function tokenToCollateralValue(uint256 tokens) external view returns (uint256);
  function balanceOfUnderlying(address user) external view returns (uint256);
}",
      "keccak256": "0x8f00a2c1e39e2a2dd749a6e05beb743158379d6899e99cb2981c3f5ab8a0e2f7"
    }
  }
}
pragma solidity ^0.6.12;

interface PodInterface {
  function tokenToCollateralValue(uint256 tokens) external view returns (uint256);
  function balanceOfUnderlying(address user) external view returns (uint256);
}",
      "keccak256": "0x8f00a2c1e39e2a2dd749a6e05beb743158379d6899e99cb2981c3f5ab8a0e2f7"
    }
  }
}
pragma solidity ^0.6.12;

interface PodInterface {
  function tokenToCollateralValue(uint256 tokens) external view returns (uint256);
  function balanceOfUnderlying(address user) external view returns (uint256);
}",
      "keccak256": "0x8f00a2c1e39e2a2dd749a6e05beb743158379d6899e99cb2981c3f5ab8a0e2f7"
    }
  }
}
pragma solidity ^0.6.12;

interface PodInterface {
  function tokenToCollateralValue(uint256 tokens) external view returns (uint256);
  function balanceOfUnderlying(address user) external view returns (uint256);
}",
      "keccak256": "0x8f00a2c1e39e2a2dd749a6e05beb743158379d6899e99cb2981c3f5ab8a0e2f7"
    }
  }
}
pragma solidity ^0.6.12;

interface PodInterface {
  function tokenToCollateralValue(uint256 tokens) external view returns (uint256);
  function balanceOfUnderlying(address user) external view returns (uint256);
}",
      "keccak256": "0x8f00a2c1e39e2a2dd749a6e05beb743158379d6899e99cb2981c3f5ab8a0e2f7"
    }
  }
}
pragma solidity ^0.6.12;

interface PodInterface {
  function tokenToCollateralValue(uint256 tokens) external view returns (uint256);
  function balanceOfUnderlying(address user) external view returns (uint256);
}",
      "keccak256": "0x8f00a2c1e39e2a2dd749a6e05beb743158379d6899e99cb2981c3f5ab8a0e2f7"
    }
  }
}
