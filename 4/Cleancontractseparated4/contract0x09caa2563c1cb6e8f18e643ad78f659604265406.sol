// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

/**
 * @dev Collection of functions related to the address type
 */
library Address {
    /**
     * @dev Returns true if `account` is a contract.
     *
     * [IMPORTANT]
     * ====
     * It is unsafe to assume that an address for which this function returns
     * false is an externally-owned account (EOA) and not a contract.
     *
     * Among others, `isContract` will return false for the following
     * types of addresses:
     *
     *  - an externally-owned account
     *  - a contract in construction
     *  - an address where a contract will be created
     *  - an address where a contract lived, but was destroyed
     * ====
     */
    function isContract(address account) internal view returns (bool) {
        // This method relies on extcodesize, which returns 0 for contracts in
        // construction, since the code is only stored at the end of the
        // constructor execution.

        uint256 size;
        // solhint-disable-next-line no-inline-assembly
        assembly { size := extcodesize(account) }
        return size > 0;
    }

    /**
     * @dev Replacement for Solidity's `transfer`: sends `amount` wei to
     * `recipient`, forwarding all available gas and reverting on errors.
     *
     * https://eips.ethereum.org/EIPS/eip-1884[EIP1884] increases the gas cost
     * of certain opcodes, possibly making contracts go over the 2300 gas limit
     * imposed by `transfer`, making them unable to receive funds via
     * `transfer`. {sendValue} removes this limitation.
     *
     * https://diligence.consensys.net/posts/2019/09/stop-using-soliditys-transfer-now/[Learn more].
     *
     * IMPORTANT: because control is transferred to `recipient`, care must be
     * taken to not create reentrancy vulnerabilities. Consider using
     * {ReentrancyGuard} or the
     * https://solidity.readthedocs.io/en/v0.5.11/security-considerations.html#use-the-checks-effects-interactions-pattern[checks-effects-interactions pattern].
     */
    function sendValue(address payable recipient, uint256 amount) internal {
        require(address(this).balance >= amount, \"Address: insufficient balance\");

        // solhint-disable-next-line avoid-low-level-calls, avoid-call-value
        (bool success, ) = recipient.call{ value: amount }(\"\");
        require(success, \"Address: unable to send value, recipient may have reverted\");
    }

    /**
     * @dev Performs a Solidity function call using a low level `call`. A
     * plain`call` is an unsafe replacement for a function call: use this
     * function instead.
     *
     * If `target` reverts with a revert reason, it is bubbled up by this
     * function (like regular Solidity function calls).
     *
     * Returns the raw returned data. To convert to the expected return value,
     * use https://solidity.readthedocs.io/en/latest/units-and-global-variables.html?highlight=abi.decode#abi-encoding-and-decoding-functions[`abi.decode`].
     *
     * Requirements:
     *
     * - `target` must be a contract.
     * - calling `target` with `data` must not revert.
     *
     * _Available since v3.1._
     */
    function functionCall(address target, bytes memory data) internal returns (bytes memory) {
      return functionCall(target, data, \"Address: low-level call failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`], but with
     * `errorMessage` as a fallback revert reason when `target` reverts.
     *
     * _Available since v3.1._
     */
    function functionCall(address target, bytes memory data, string memory errorMessage) internal returns (bytes memory) {
        return functionCallWithValue(target, data, 0, errorMessage);
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`],
     * but also transferring `value` wei to `target`.
     *
     * Requirements:
     *
     * - the calling contract must have an ETH balance of at least `value`.
     * - the called Solidity function must be `payable`.
     *
     * _Available since v3.1._
     */
    function functionCallWithValue(address target, bytes memory data, uint256 value) internal returns (bytes memory) {
        return functionCallWithValue(target, data, value, \"Address: low-level call with value failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCallWithValue-address-bytes-uint256-}[`functionCallWithValue`], but
     * with `errorMessage` as a fallback revert reason when `target` reverts.
     *
     * _Available since v3.1._
     */
    function functionCallWithValue(address target, bytes memory data, uint256 value, string memory errorMessage) internal returns (bytes memory) {
        require(address(this).balance >= value, \"Address: insufficient balance for call\");
        require(isContract(target), \"Address: call to non-contract\");

        // solhint-disable-next-line avoid-low-level-calls
        (bool success, bytes memory returndata) = target.call{ value: value }(data);
        return _verifyCallResult(success, returndata, errorMessage);
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`],
     * but performing a static call.
     *
     * _Available since v3.3._
     */
    function functionStaticCall(address target, bytes memory data) internal view returns (bytes memory) {
        return functionStaticCall(target, data, \"Address: low-level static call failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-string-}[`functionCall`],
     * but performing a static call.
     *
     * _Available since v3.3._
     */
    function functionStaticCall(address target, bytes memory data, string memory errorMessage) internal view returns (bytes memory) {
        require(isContract(target), \"Address: static call to non-contract\");

        // solhint-disable-next-line avoid-low-level-calls
        (bool success, bytes memory returndata) = target.staticcall(data);
        return _verifyCallResult(success, returndata, errorMessage);
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`],
     * but performing a delegate call.
     *
     * _Available since v3.4._
     */
    function functionDelegateCall(address target, bytes memory data) internal returns (bytes memory) {
        return functionDelegateCall(target, data, \"Address: low-level delegate call failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-string-}[`functionCall`],
     * but performing a delegate call.
     *
     * _Available since v3.4._
     */
    function functionDelegateCall(address target, bytes memory data, string memory errorMessage) internal returns (bytes memory) {
        require(isContract(target), \"Address: delegate call to non-contract\");

        // solhint-disable-next-line avoid-low-level-calls
        (bool success, bytes memory returndata) = target.delegatecall(data);
        return _verifyCallResult(success, returndata, errorMessage);
    }

    function _verifyCallResult(bool success, bytes memory returndata, string memory errorMessage) private pure returns(bytes memory) {
        if (success) {
            return returndata;
        } else {
            // Look for revert reason and bubble it up if present
            if (returndata.length > 0) {
                // The easiest way to bubble the revert reason is using memory via assembly

                // solhint-disable-next-line no-inline-assembly
                assembly {
                    let returndata_size := mload(returndata)
                    revert(add(32, returndata), returndata_size)
                }
            } else {
                revert(errorMessage);
            }
        }
    }
}
"
    
// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import \"./Context.sol\";
/**
 * @dev Contract module which provides a basic access control mechanism, where
 * there is an account (an owner) that can be granted exclusive access to
 * specific functions.
 *
 * By default, the owner account will be the one that deploys the contract. This
 * can later be changed with {transferOwnership}.
 *
 * This module is used through inheritance. It will make available the modifier
 * `onlyOwner`, which can be applied to your functions to restrict their use to
 * the owner.
 */
abstract contract Ownable is Context {
    address private _owner;

    event OwnershipTransferred(address indexed previousOwner, address indexed newOwner);

    /**
     * @dev Initializes the contract setting the deployer as the initial owner.
     */
    constructor () {
        address msgSender = _msgSender();
        _owner = msgSender;
        emit OwnershipTransferred(address(0), msgSender);
    }

    /**
     * @dev Returns the address of the current owner.
     */
    function owner() public view virtual returns (address) {
        return _owner;
    }

    /**
     * @dev Throws if called by any account other than the owner.
     */
    modifier onlyOwner() {
        require(owner() == _msgSender(), \"Ownable: caller is not the owner\");
        _;
    }

    /**
     * @dev Leaves the contract without owner. It will not be possible to call
     * `onlyOwner` functions anymore. Can only be called by the current owner.
     *
     * NOTE: Renouncing ownership will leave the contract without an owner,
     * thereby removing any functionality that is only available to the owner.
     */
    function renounceOwnership() public virtual onlyOwner {
        emit OwnershipTransferred(_owner, address(0));
        _owner = address(0);
    }

    /**
     * @dev Transfers ownership of the contract to a new account (`newOwner`).
     * Can only be called by the current owner.
     */
    function transferOwnership(address newOwner) public virtual onlyOwner {
        require(newOwner != address(0), \"Ownable: new owner is the zero address\");
        emit OwnershipTransferred(_owner, newOwner);
        _owner = newOwner;
    }
}
"
    
// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

/*
 * @dev Provides information about the current execution context, including the
 * sender of the transaction and its data. While these are generally available
 * via msg.sender and msg.data, they should not be accessed in such a direct
 * manner, since when dealing with meta-transactions the account sending and
 * paying for execution may not be the actual sender (as far as an application
 * is concerned).
 *
 * This contract is only required for intermediate, library-like contracts.
 */
abstract contract Context {
    function _msgSender() internal view virtual returns (address) {
        return msg.sender;
    }

    function _msgData() internal view virtual returns (bytes calldata) {
        this; // silence state mutability warning without generating bytecode - see https://github.com/ethereum/solidity/issues/2691
        return msg.data;
    }
}
"
    
// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import \"./IERC721.sol\";

/**
 * @title ERC-721 Non-Fungible Token Standard, optional enumeration extension
 * @dev See https://eips.ethereum.org/EIPS/eip-721
 */
interface IERC721Enumerable is IERC721 {

    /**
     * @dev Returns the total amount of tokens stored by the contract.
     */
    function totalSupply() external view returns (uint256);

    /**
     * @dev Returns a token ID owned by `owner` at a given `index` of its token list.
     * Use along with {balanceOf} to enumerate all of ``owner``'s tokens.
     */
    function tokenOfOwnerByIndex(address owner, uint256 index) external view returns (uint256 tokenId);

    /**
     * @dev Returns a token ID at a given `index` of all the tokens stored by the contract.
     * Use along with {totalSupply} to enumerate all tokens.
     */
    function tokenByIndex(uint256 index) external view returns (uint256);
}
"
    
// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import \"./IERC165.sol\";

/**
 * @dev Required interface of an ERC721 compliant contract.
 */
interface IERC721 is IERC165 {
    /**
     * @dev Emitted when `tokenId` token is transferred from `from` to `to`.
     */
    event Transfer(address indexed from, address indexed to, uint256 indexed tokenId);

    /**
     * @dev Emitted when `owner` enables `approved` to manage the `tokenId` token.
     */
    event Approval(address indexed owner, address indexed approved, uint256 indexed tokenId);

    /**
     * @dev Emitted when `owner` enables or disables (`approved`) `operator` to manage all of its assets.
     */
    event ApprovalForAll(address indexed owner, address indexed operator, bool approved);

    /**
     * @dev Returns the number of tokens in ``owner``'s account.
     */
    function balanceOf(address owner) external view returns (uint256 balance);

    /**
     * @dev Returns the owner of the `tokenId` token.
     *
     * Requirements:
     *
     * - `tokenId` must exist.
     */
    function ownerOf(uint256 tokenId) external view returns (address owner);

    /**
     * @dev Safely transfers `tokenId` token from `from` to `to`, checking first that contract recipients
     * are aware of the ERC721 protocol to prevent tokens from being forever locked.
     *
     * Requirements:
     *
     * - `from` cannot be the zero address.
     * - `to` cannot be the zero address.
     * - `tokenId` token must exist and be owned by `from`.
     * - If the caller is not `from`, it must be have been allowed to move this token by either {approve} or {setApprovalForAll}.
     * - If `to` refers to a smart contract, it must implement {IERC721Receiver-onERC721Received}, which is called upon a safe transfer.
     *
     * Emits a {Transfer} event.
     */
    function safeTransferFrom(address from, address to, uint256 tokenId) external;

    /**
     * @dev Transfers `tokenId` token from `from` to `to`.
     *
     * WARNING: Usage of this method is discouraged, use {safeTransferFrom} whenever possible.
     *
     * Requirements:
     *
     * - `from` cannot be the zero address.
     * - `to` cannot be the zero address.
     * - `tokenId` token must be owned by `from`.
     * - If the caller is not `from`, it must be approved to move this token by either {approve} or {setApprovalForAll}.
     *
     * Emits a {Transfer} event.
     */
    function transferFrom(address from, address to, uint256 tokenId) external;

    /**
     * @dev Gives permission to `to` to transfer `tokenId` token to another account.
     * The approval is cleared when the token is transferred.
     *
     * Only a single account can be approved at a time, so approving the zero address clears previous approvals.
     *
     * Requirements:
     *
     * - The caller must own the token or be an approved operator.
     * - `tokenId` must exist.
     *
     * Emits an {Approval} event.
     */
    function approve(address to, uint256 tokenId) external;

    /**
     * @dev Returns the account approved for `tokenId` token.
     *
     * Requirements:
     *
     * - `tokenId` must exist.
     */
    function getApproved(uint256 tokenId) external view returns (address operator);

    /**
     * @dev Approve or remove `operator` as an operator for the caller.
     * Operators can call {transferFrom} or {safeTransferFrom} for any token owned by the caller.
     *
     * Requirements:
     *
     * - The `operator` cannot be the caller.
     *
     * Emits an {ApprovalForAll} event.
     */
    function setApprovalForAll(address operator, bool _approved) external;

    /**
     * @dev Returns if the `operator` is allowed to manage all of the assets of `owner`.
     *
     * See {setApprovalForAll}
     */
    function isApprovedForAll(address owner, address operator) external view returns (bool);

    /**
      * @dev Safely transfers `tokenId` token from `from` to `to`.
      *
      * Requirements:
      *
      * - `from` cannot be the zero address.
      * - `to` cannot be the zero address.
      * - `tokenId` token must exist and be owned by `from`.
      * - If the caller is not `from`, it must be approved to move this token by either {approve} or {setApprovalForAll}.
      * - If `to` refers to a smart contract, it must implement {IERC721Receiver-onERC721Received}, which is called upon a safe transfer.
      *
      * Emits a {Transfer} event.
      */
    function safeTransferFrom(address from, address to, uint256 tokenId, bytes calldata data) external;
}
"
    
// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

/**
 * @dev Interface of the ERC165 standard, as defined in the
 * https://eips.ethereum.org/EIPS/eip-165[EIP].
 *
 * Implementers can declare support of contract interfaces, which can then be
 * queried by others ({ERC165Checker}).
 *
 * For an implementation, see {ERC165}.
 */
interface IERC165 {
    /**
     * @dev Returns true if this contract implements the interface defined by
     * `interfaceId`. See the corresponding
     * https://eips.ethereum.org/EIPS/eip-165#how-interfaces-are-identified[EIP section]
     * to learn more about how these ids are created.
     *
     * This function call must use less than 30 000 gas.
     */
    function supportsInterface(bytes4 interfaceId) external view returns (bool);
}
"
    
// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

/**
 * @title ERC721 token receiver interface
 * @dev Interface for any contract that wants to support safeTransfers
 * from ERC721 asset contracts.
 */
interface IERC721Receiver {
    /**
     * @dev Whenever an {IERC721} `tokenId` token is transferred to this contract via {IERC721-safeTransferFrom}
     * by `operator` from `from`, this function is called.
     *
     * It must return its Solidity selector to confirm the token transfer.
     * If any other value is returned or the interface is not implemented by the recipient, the transfer will be reverted.
     *
     * The selector can be obtained in Solidity with `IERC721.onERC721Received.selector`.
     */
    function onERC721Received(address operator, address from, uint256 tokenId, bytes calldata data) external returns (bytes4);
}
"
    
// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import \"./IERC165.sol\";

/**
 * @dev Implementation of the {IERC165} interface.
 *
 * Contracts that want to implement ERC165 should inherit from this contract and override {supportsInterface} to check
 * for the additional interface id that will be supported. For example:
 *
 * ```solidity
 * function supportsInterface(bytes4 interfaceId) public view virtual override returns (bool) {
 *     return interfaceId == type(MyInterface).interfaceId || super.supportsInterface(interfaceId);
 * }
 * ```
 *
 * Alternatively, {ERC165Storage} provides an easier to use but more expensive implementation.
 */
abstract contract ERC165 is IERC165 {
    /**
     * @dev See {IERC165-supportsInterface}.
     */
    function supportsInterface(bytes4 interfaceId) public view virtual override returns (bool) {
        return interfaceId == type(IERC165).interfaceId;
    }
}
"
    
// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

// CAUTION
// This version of SafeMath should only be used with Solidity 0.8 or later,
// because it relies on the compiler's built in overflow checks.

/**
 * @dev Wrappers over Solidity's arithmetic operations.
 *
 * NOTE: `SafeMath` is no longer needed starting with Solidity 0.8. The compiler
 * now has built in overflow checking.
 */
library SafeMath {
    /**
     * @dev Returns the addition of two unsigned integers, with an overflow flag.
     *
     * _Available since v3.4._
     */
    function tryAdd(uint256 a, uint256 b) internal pure returns (bool, uint256) {
        unchecked {
            uint256 c = a + b;
            if (c < a) return (false, 0);
            return (true, c);
        }
    }

    /**
     * @dev Returns the substraction of two unsigned integers, with an overflow flag.
     *
     * _Available since v3.4._
     */
    function trySub(uint256 a, uint256 b) internal pure returns (bool, uint256) {
        unchecked {
            if (b > a) return (false, 0);
            return (true, a - b);
        }
    }

    /**
     * @dev Returns the multiplication of two unsigned integers, with an overflow flag.
     *
     * _Available since v3.4._
     */
    function tryMul(uint256 a, uint256 b) internal pure returns (bool, uint256) {
        unchecked {
            // Gas optimization: this is cheaper than requiring 'a' not being zero, but the
            // benefit is lost if 'b' is also tested.
            // See: https://github.com/OpenZeppelin/openzeppelin-contracts/pull/522
            if (a == 0) return (true, 0);
            uint256 c = a * b;
            if (c / a != b) return (false, 0);
            return (true, c);
        }
    }

    /**
     * @dev Returns the division of two unsigned integers, with a division by zero flag.
     *
     * _Available since v3.4._
     */
    function tryDiv(uint256 a, uint256 b) internal pure returns (bool, uint256) {
        unchecked {
            if (b == 0) return (false, 0);
            return (true, a / b);
        }
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers, with a division by zero flag.
     *
     * _Available since v3.4._
     */
    function tryMod(uint256 a, uint256 b) internal pure returns (bool, uint256) {
        unchecked {
            if (b == 0) return (false, 0);
            return (true, a % b);
        }
    }

    /**
     * @dev Returns the addition of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity's `+` operator.
     *
     * Requirements:
     *
     * - Addition cannot overflow.
     */
    function add(uint256 a, uint256 b) internal pure returns (uint256) {
        return a + b;
    }

    /**
     * @dev Returns the subtraction of two unsigned integers, reverting on
     * overflow (when the result is negative).
     *
     * Counterpart to Solidity's `-` operator.
     *
     * Requirements:
     *
     * - Subtraction cannot overflow.
     */
    function sub(uint256 a, uint256 b) internal pure returns (uint256) {
        return a - b;
    }

    /**
     * @dev Returns the multiplication of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity's `*` operator.
     *
     * Requirements:
     *
     * - Multiplication cannot overflow.
     */
    function mul(uint256 a, uint256 b) internal pure returns (uint256) {
        return a * b;
    }

    /**
     * @dev Returns the integer division of two unsigned integers, reverting on
     * division by zero. The result is rounded towards zero.
     *
     * Counterpart to Solidity's `/` operator.
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function div(uint256 a, uint256 b) internal pure returns (uint256) {
        return a / b;
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
     * reverting when dividing by zero.
     *
     * Counterpart to Solidity's `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function mod(uint256 a, uint256 b) internal pure returns (uint256) {
        return a % b;
    }

    /**
     * @dev Returns the subtraction of two unsigned integers, reverting with custom message on
     * overflow (when the result is negative).
     *
     * CAUTION: This function is deprecated because it requires allocating memory for the error
     * message unnecessarily. For custom revert reasons use {trySub}.
     *
     * Counterpart to Solidity's `-` operator.
     *
     * Requirements:
     *
     * - Subtraction cannot overflow.
     */
    function sub(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        unchecked {
            require(b <= a, errorMessage);
            return a - b;
        }
    }

    /**
     * @dev Returns the integer division of two unsigned integers, reverting with custom message on
     * division by zero. The result is rounded towards zero.
     *
     * Counterpart to Solidity's `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     *
     * Counterpart to Solidity's `/` operator. Note: this function uses a
     * `revert` opcode (which leaves remaining gas untouched) while Solidity
     * uses an invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function div(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        unchecked {
            require(b > 0, errorMessage);
            return a / b;
        }
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
     * reverting with custom message when dividing by zero.
     *
     * CAUTION: This function is deprecated because it requires allocating memory for the error
     * message unnecessarily. For custom revert reasons use {tryMod}.
     *
     * Counterpart to Solidity's `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function mod(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        unchecked {
            require(b > 0, errorMessage);
            return a % b;
        }
    }
}
"
    
// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

/**
 * @dev String operations.
 */
library Strings {
    bytes16 private constant alphabet = \"0123456789abcdef\";

    /**
     * @dev Converts a `uint256` to its ASCII `string` decimal representation.
     */
    function toString(uint256 value) internal pure returns (string memory) {
        // Inspired by OraclizeAPI's implementation - MIT licence
        // https://github.com/oraclize/ethereum-api/blob/b42146b063c7d6ee1358846c198246239e9360e8/oraclizeAPI_0.4.25.sol

        if (value == 0) {
            return \"0\";
        }
        uint256 temp = value;
        uint256 digits;
        while (temp != 0) {
            digits++;
            temp /= 10;
        }
        bytes memory buffer = new bytes(digits);
        while (value != 0) {
            digits -= 1;
            buffer[digits] = bytes1(uint8(48 + uint256(value % 10)));
            value /= 10;
        }
        return string(buffer);
    }

    /**
     * @dev Converts a `uint256` to its ASCII `string` hexadecimal representation.
     */
    function toHexString(uint256 value) internal pure returns (string memory) {
        if (value == 0) {
            return \"0x00\";
        }
        uint256 temp = value;
        uint256 length = 0;
        while (temp != 0) {
            length++;
            temp >>= 8;
        }
        return toHexString(value, length);
    }

    /**
     * @dev Converts a `uint256` to its ASCII `string` hexadecimal representation with fixed length.
     */
    function toHexString(uint256 value, uint256 length) internal pure returns (string memory) {
        bytes memory buffer = new bytes(2 * length + 2);
        buffer[0] = \"0\";
        buffer[1] = \"x\";
        for (uint256 i = 2 * length + 1; i > 1; --i) {
            buffer[i] = alphabet[value & 0xf];
            value >>= 4;
        }
        require(value == 0, \"Strings: hex length insufficient\");
        return string(buffer);
    }

}
"
    
// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import \"./IERC721.sol\";

/**
 * @title ERC-721 Non-Fungible Token Standard, optional metadata extension
 * @dev See https://eips.ethereum.org/EIPS/eip-721
 */
interface IERC721Metadata is IERC721 {

    /**
     * @dev Returns the token collection name.
     */
    function name() external view returns (string memory);

    /**
     * @dev Returns the token collection symbol.
     */
    function symbol() external view returns (string memory);

    /**
     * @dev Returns the Uniform Resource Identifier (URI) for `tokenId` token.
     */
    function tokenURI(uint256 tokenId) external view returns (string memory);
}
"
    
// SPDX-License-Identifier: MIT

pragma solidity ^ 0.8.0;

import \"./SafeMath.sol\";
import \"./Context.sol\";
import \"./Ownable.sol\";
import \"./Strings.sol\";
import \"./Address.sol\";
import \"./IERC165.sol\";
import \"./ERC165.sol\";
import \"./IERC721.sol\";
import \"./IERC721Enumerable.sol\";
import \"./IERC721Metadata.sol\";
import \"./IERC721Receiver.sol\";

/**
 * @dev Implementation of https://eips.ethereum.org/EIPS/eip-721[ERC721] Non-Fungible Token Standard, including
 * the Metadata extension, but not including the Enumerable extension, which is available separately as
 * {ERC721Enumerable}.
 */
contract ERC721 is Context, ERC165, IERC721, IERC721Metadata {
\tusing Address for address;
\tusing Strings for uint256;

\t// Token name
\tstring private _name;

\t// Token symbol
\tstring private _symbol;

\t// Mapping from token ID to owner address
\tmapping(uint256 => address) private _owners;

\t// Mapping owner address to token count
\tmapping(address => uint256) private _balances;

\t// Mapping from token ID to approved address
\tmapping(uint256 => address) private _tokenApprovals;

\t// Mapping from owner to operator approvals
\tmapping(address => mapping(address => bool)) private _operatorApprovals;

\t// Optional mapping for token URIs
\tmapping(uint256 => string) private _tokenURIs;

\t// Base URI
\tstring private _baseURI;

\t/**
\t * @dev Initializes the contract by setting a `name` and a `symbol` to the token collection.
\t */
\tconstructor(string memory name_, string memory symbol_) {
\t\t_name = name_;
\t\t_symbol = symbol_;
\t}

\t/**
\t * @dev See {IERC165-supportsInterface}.
\t */
\tfunction supportsInterface(bytes4 interfaceId) public view virtual override(ERC165, IERC165) returns(bool) {
\t\treturn interfaceId == type(IERC721).interfaceId
\t\t\t|| interfaceId == type(IERC721Metadata).interfaceId
\t\t\t|| super.supportsInterface(interfaceId);
\t}

\t/**
\t * @dev See {IERC721-balanceOf}.
\t */
\tfunction balanceOf(address owner) public view virtual override returns(uint256) {
\t\trequire(owner != address(0), \"ERC721: balance query for the zero address\");
\t\treturn _balances[owner];
\t}

\t/**
\t * @dev See {IERC721-ownerOf}.
\t */
\tfunction ownerOf(uint256 tokenId) public view virtual override returns(address) {
\t\taddress owner = _owners[tokenId];
\t\trequire(owner != address(0), \"ERC721: owner query for nonexistent token\");
\t\treturn owner;
\t}

\t/**
\t * @dev See {IERC721Metadata-name}.
\t */
\tfunction name() public view virtual override returns(string memory) {
\t\treturn _name;
\t}

\t/**
\t * @dev See {IERC721Metadata-symbol}.
\t */
\tfunction symbol() public view virtual override returns(string memory) {
\t\treturn _symbol;
\t}

\t/**
\t * @dev See {IERC721Metadata-tokenURI}.
\t */
\tfunction tokenURI(uint256 tokenId) public view virtual override returns(string memory) {
\t\trequire(_exists(tokenId), \"ERC721Metadata: URI query for nonexistent token\");

\t\tstring memory _tokenURI = _tokenURIs[tokenId];
\t\tstring memory base = baseURI();

\t\t// If there is no base URI, return the token URI.
\t\tif (bytes(base).length == 0) {
\t\t\treturn _tokenURI;
\t\t}
\t\t// If both are set, concatenate the baseURI and tokenURI (via abi.encodePacked).
\t\tif (bytes(_tokenURI).length > 0) {
\t\t\treturn string(abi.encodePacked(base, _tokenURI));
\t\t}
\t\t// If there is a baseURI but no tokenURI, concatenate the tokenID to the baseURI.
\t\treturn string(abi.encodePacked(base, tokenId.toString()));
\t}

\t/**
\t * @dev Base URI for computing {tokenURI}. Empty by default, can be overriden
\t * in child contracts.
\t */
\tfunction baseURI() internal view virtual returns(string memory) {
\t\treturn _baseURI;
\t}

\t/**
\t * @dev See {IERC721-approve}.
\t */
\tfunction approve(address to, uint256 tokenId) public virtual override {
\t\taddress owner = ERC721.ownerOf(tokenId);
\t\trequire(to != owner, \"ERC721: approval to current owner\");

\t\trequire(_msgSender() == owner || ERC721.isApprovedForAll(owner, _msgSender()),
\t\t\t\"ERC721: approve caller is not owner nor approved for all\"
\t\t);

\t\t_approve(to, tokenId);
\t}

\t/**
\t * @dev See {IERC721-getApproved}.
\t */
\tfunction getApproved(uint256 tokenId) public view virtual override returns(address) {
\t\trequire(_exists(tokenId), \"ERC721: approved query for nonexistent token\");

\t\treturn _tokenApprovals[tokenId];
\t}

\t/**
\t * @dev See {IERC721-setApprovalForAll}.
\t */
\tfunction setApprovalForAll(address operator, bool approved) public virtual override {
\t\trequire(operator != _msgSender(), \"ERC721: approve to caller\");

\t\t_operatorApprovals[_msgSender()][operator] = approved;
\t\temit ApprovalForAll(_msgSender(), operator, approved);
\t}

\t/**
\t * @dev See {IERC721-isApprovedForAll}.
\t */
\tfunction isApprovedForAll(address owner, address operator) public view virtual override returns(bool) {
\t\treturn _operatorApprovals[owner][operator];
\t}

\t/**
\t * @dev See {IERC721-transferFrom}.
\t */
\tfunction transferFrom(address from, address to, uint256 tokenId) public virtual override {
\t\t//solhint-disable-next-line max-line-length
\t\trequire(_isApprovedOrOwner(_msgSender(), tokenId), \"ERC721: transfer caller is not owner nor approved\");

\t\t_transfer(from, to, tokenId);
\t}

\t/**
\t * @dev See {IERC721-safeTransferFrom}.
\t */
\tfunction safeTransferFrom(address from, address to, uint256 tokenId) public virtual override {
\t\tsafeTransferFrom(from, to, tokenId, \"\");
\t}

\t/**
\t * @dev See {IERC721-safeTransferFrom}.
\t */
\tfunction safeTransferFrom(address from, address to, uint256 tokenId, bytes memory _data) public virtual override {
\t\trequire(_isApprovedOrOwner(_msgSender(), tokenId), \"ERC721: transfer caller is not owner nor approved\");
\t\t_safeTransfer(from, to, tokenId, _data);
\t}

\t/**
\t * @dev Safely transfers `tokenId` token from `from` to `to`, checking first that contract recipients
\t * are aware of the ERC721 protocol to prevent tokens from being forever locked.
\t *
\t * `_data` is additional data, it has no specified format and it is sent in call to `to`.
\t *
\t * This internal function is equivalent to {safeTransferFrom}, and can be used to e.g.
\t * implement alternative mechanisms to perform token transfer, such as signature-based.
\t *
\t * Requirements:
\t *
\t * - `from` cannot be the zero address.
\t * - `to` cannot be the zero address.
\t * - `tokenId` token must exist and be owned by `from`.
\t * - If `to` refers to a smart contract, it must implement {IERC721Receiver-onERC721Received}, which is called upon a safe transfer.
\t *
\t * Emits a {Transfer} event.
\t */
\tfunction _safeTransfer(address from, address to, uint256 tokenId, bytes memory _data) internal virtual {
\t\t_transfer(from, to, tokenId);
\t\trequire(_checkOnERC721Received(from, to, tokenId, _data), \"ERC721: transfer to non ERC721Receiver implementer\");
\t}

\t/**
\t * @dev Returns whether `tokenId` exists.
\t *
\t * Tokens can be managed by their owner or approved accounts via {approve} or {setApprovalForAll}.
\t *
\t * Tokens start existing when they are minted (`_mint`),
\t * and stop existing when they are burned (`_burn`).
\t */
\tfunction _exists(uint256 tokenId) internal view virtual returns(bool) {
\t\treturn _owners[tokenId] != address(0);
\t}

\t/**
\t * @dev Returns whether `spender` is allowed to manage `tokenId`.
\t *
\t * Requirements:
\t *
\t * - `tokenId` must exist.
\t */
\tfunction _isApprovedOrOwner(address spender, uint256 tokenId) internal view virtual returns(bool) {
\t\trequire(_exists(tokenId), \"ERC721: operator query for nonexistent token\");
\t\taddress owner = ERC721.ownerOf(tokenId);
\t\treturn (spender == owner || getApproved(tokenId) == spender || ERC721.isApprovedForAll(owner, spender));
\t}

\t/**
\t * @dev Safely mints `tokenId` and transfers it to `to`.
\t *
\t * Requirements:
\t *
\t * - `tokenId` must not exist.
\t * - If `to` refers to a smart contract, it must implement {IERC721Receiver-onERC721Received}, which is called upon a safe transfer.
\t *
\t * Emits a {Transfer} event.
\t */
\tfunction _safeMint(address to, uint256 tokenId) internal virtual {
\t\t_safeMint(to, tokenId, \"\");
\t}

\t/**
\t * @dev Same as {xref-ERC721-_safeMint-address-uint256-}[`_safeMint`], with an additional `data` parameter which is
\t * forwarded in {IERC721Receiver-onERC721Received} to contract recipients.
\t */
\tfunction _safeMint(address to, uint256 tokenId, bytes memory _data) internal virtual {
\t\t_mint(to, tokenId);
\t\trequire(_checkOnERC721Received(address(0), to, tokenId, _data), \"ERC721: transfer to non ERC721Receiver implementer\");
\t}

\t/**
\t * @dev Mints `tokenId` and transfers it to `to`.
\t *
\t * WARNING: Usage of this method is discouraged, use {_safeMint} whenever possible
\t *
\t * Requirements:
\t *
\t * - `tokenId` must not exist.
\t * - `to` cannot be the zero address.
\t *
\t * Emits a {Transfer} event.
\t */
\tfunction _mint(address to, uint256 tokenId) internal virtual {
\t\trequire(to != address(0), \"ERC721: mint to the zero address\");
\t\trequire(!_exists(tokenId), \"ERC721: token already minted\");

\t\t_beforeTokenTransfer(address(0), to, tokenId);

\t\t_balances[to] += 1;
\t\t_owners[tokenId] = to;

\t\temit Transfer(address(0), to, tokenId);
\t}

\t/**
\t * @dev Destroys `tokenId`.
\t * The approval is cleared when the token is burned.
\t *
\t * Requirements:
\t *
\t * - `tokenId` must exist.
\t *
\t * Emits a {Transfer} event.
\t */
\tfunction _burn(uint256 tokenId) internal virtual {
\t\taddress owner = ERC721.ownerOf(tokenId);

\t\t_beforeTokenTransfer(owner, address(0), tokenId);

\t\t// Clear approvals
\t\t_approve(address(0), tokenId);

\t\t_balances[owner] -= 1;
\t\tdelete _owners[tokenId];

\t\temit Transfer(owner, address(0), tokenId);
\t}

\t/**
\t * @dev Transfers `tokenId` from `from` to `to`.
\t *  As opposed to {transferFrom}, this imposes no restrictions on msg.sender.
\t *
\t * Requirements:
\t *
\t * - `to` cannot be the zero address.
\t * - `tokenId` token must be owned by `from`.
\t *
\t * Emits a {Transfer} event.
\t */
\tfunction _transfer(address from, address to, uint256 tokenId) internal virtual {
\t\trequire(ERC721.ownerOf(tokenId) == from, \"ERC721: transfer of token that is not own\");
\t\trequire(to != address(0), \"ERC721: transfer to the zero address\");

\t\t_beforeTokenTransfer(from, to, tokenId);

\t\t// Clear approvals from the previous owner
\t\t_approve(address(0), tokenId);

\t\t_balances[from] -= 1;
\t\t_balances[to] += 1;
\t\t_owners[tokenId] = to;

\t\temit Transfer(from, to, tokenId);
\t}

\t/**
\t * @dev Sets `_tokenURI` as the tokenURI of `tokenId`.
\t *
\t * Requirements:
\t *
\t * - `tokenId` must exist.
\t */
\tfunction _setTokenURI(uint256 tokenId, string memory _tokenURI) internal virtual {
\t\trequire(_exists(tokenId), \"ERC721Metadata: URI set of nonexistent token\");
\t\t_tokenURIs[tokenId] = _tokenURI;
\t}

\t/**
\t * @dev Internal function to set the base URI for all token IDs. It is
\t * automatically added as a prefix to the value returned in {tokenURI},
\t * or to the token ID if {tokenURI} is empty.
\t */
\tfunction _setBaseURI(string memory baseURI_) internal virtual {
\t\t_baseURI = baseURI_;
\t}

\t/**
\t * @dev Approve `to` to operate on `tokenId`
\t *
\t * Emits a {Approval} event.
\t */
\tfunction _approve(address to, uint256 tokenId) internal virtual {
\t\t_tokenApprovals[tokenId] = to;
\t\temit Approval(ERC721.ownerOf(tokenId), to, tokenId);
\t}

\t/**
\t * @dev Internal function to invoke {IERC721Receiver-onERC721Received} on a target address.
\t * The call is not executed if the target address is not a contract.
\t *
\t * @param from address representing the previous owner of the given token ID
\t * @param to target address that will receive the tokens
\t * @param tokenId uint256 ID of the token to be transferred
\t * @param _data bytes optional data to send along with the call
\t * @return bool whether the call correctly returned the expected magic value
\t */
\tfunction _checkOnERC721Received(address from, address to, uint256 tokenId, bytes memory _data)
        private returns(bool)
\t{
\t\tif (to.isContract()) {
\t\t\ttry IERC721Receiver(to).onERC721Received(_msgSender(), from, tokenId, _data) returns(bytes4 retval) {
\t\t\t\treturn retval == IERC721Receiver(to).onERC721Received.selector;
\t\t\t} catch (bytes memory reason) {
\t\t\t\tif (reason.length == 0) {
\t\t\t\t\trevert(\"ERC721: transfer to non ERC721Receiver implementer\");
\t\t\t\t} else {
\t\t\t\t\t// solhint-disable-next-line no-inline-assembly
\t\t\t\t\tassembly {
\t\t\t\t\t\trevert(add(32, reason), mload(reason))
\t\t\t\t\t}
\t\t\t\t}
\t\t\t}
\t\t} else {
\t\t\treturn true;
\t\t}
\t}

\t/**
\t * @dev Hook that is called before any token transfer. This includes minting
\t * and burning.
\t *
\t * Calling conditions:
\t *
\t * - When `from` and `to` are both non-zero, ``from``'s `tokenId` will be
\t * transferred to `to`.
\t * - When `from` is zero, `tokenId` will be minted for `to`.
\t * - When `to` is zero, ``from``'s `tokenId` will be burned.
\t * - `from` cannot be the zero address.
\t * - `to` cannot be the zero address.
\t *
\t * To learn more about hooks, head to xref:ROOT:extending-contracts.adoc#using-hooks[Using Hooks].
\t */
\tfunction _beforeTokenTransfer(address from, address to, uint256 tokenId) internal virtual { }
}

/**
 * @dev This implements an optional extension of {ERC721} defined in the EIP that adds
 * enumerability of all the token ids in the contract as well as all token ids owned by each
 * account.
 */
abstract contract ERC721Enumerable is ERC721, IERC721Enumerable {
\t// Mapping from owner to list of owned token IDs
\tmapping(address => mapping(uint256 => uint256)) private _ownedTokens;

\t// Mapping from token ID to index of the owner tokens list
\tmapping(uint256 => uint256) private _ownedTokensIndex;

\t// Array with all token ids, used for enumeration
\tuint256[] private _allTokens;

\t// Mapping from token id to position in the allTokens array
\tmapping(uint256 => uint256) private _allTokensIndex;

\t/**
\t * @dev See {IERC165-supportsInterface}.
\t */
\tfunction supportsInterface(bytes4 interfaceId) public view virtual override(IERC165, ERC721) returns(bool) {
\t\treturn interfaceId == type(IERC721Enumerable).interfaceId
\t\t\t|| super.supportsInterface(interfaceId);
\t}

\t/**
\t * @dev See {IERC721Enumerable-tokenOfOwnerByIndex}.
\t */
\tfunction tokenOfOwnerByIndex(address owner, uint256 index) public view virtual override returns(uint256) {
\t\trequire(index < ERC721.balanceOf(owner), \"ERC721Enumerable: owner index out of bounds\");
\t\treturn _ownedTokens[owner][index];
\t}

\t/**
\t * @dev See {IERC721Enumerable-totalSupply}.
\t */
\tfunction totalSupply() public view virtual override returns(uint256) {
\t\treturn _allTokens.length;
\t}

\t/**
\t * @dev See {IERC721Enumerable-tokenByIndex}.
\t */
\tfunction tokenByIndex(uint256 index) public view virtual override returns(uint256) {
\t\trequire(index < ERC721Enumerable.totalSupply(), \"ERC721Enumerable: global index out of bounds\");
\t\treturn _allTokens[index];
\t}

\t/**
\t * @dev Hook that is called before any token transfer. This includes minting
\t * and burning.
\t *
\t * Calling conditions:
\t *
\t * - When `from` and `to` are both non-zero, ``from``'s `tokenId` will be
\t * transferred to `to`.
\t * - When `from` is zero, `tokenId` will be minted for `to`.
\t * - When `to` is zero, ``from``'s `tokenId` will be burned.
\t * - `from` cannot be the zero address.
\t * - `to` cannot be the zero address.
\t *
\t * To learn more about hooks, head to xref:ROOT:extending-contracts.adoc#using-hooks[Using Hooks].
\t */
\tfunction _beforeTokenTransfer(address from, address to, uint256 tokenId) internal virtual override {
\t\tsuper._beforeTokenTransfer(from, to, tokenId);

\t\tif (from == address(0)) {
\t\t\t_addTokenToAllTokensEnumeration(tokenId);
\t\t} else if (from != to) {
\t\t\t_removeTokenFromOwnerEnumeration(from, tokenId);
\t\t}
\t\tif (to == address(0)) {
\t\t\t_removeTokenFromAllTokensEnumeration(tokenId);
\t\t} else if (to != from) {
\t\t\t_addTokenToOwnerEnumeration(to, tokenId);
\t\t}
\t}

\t/**
\t * @dev Private function to add a token to this extension's ownership-tracking data structures.
\t * @param to address representing the new owner of the given token ID
\t * @param tokenId uint256 ID of the token to be added to the tokens list of the given address
\t */
\tfunction _addTokenToOwnerEnumeration(address to, uint256 tokenId) private {
\t\tuint256 length = ERC721.balanceOf(to);
\t\t_ownedTokens[to][length] = tokenId;
\t\t_ownedTokensIndex[tokenId] = length;
\t}

\t/**
\t * @dev Private function to add a token to this extension's token tracking data structures.
\t * @param tokenId uint256 ID of the token to be added to the tokens list
\t */
\tfunction _addTokenToAllTokensEnumeration(uint256 tokenId) private {
\t\t_allTokensIndex[tokenId] = _allTokens.length;
\t\t_allTokens.push(tokenId);
\t}

\t/**
\t * @dev Private function to remove a token from this extension's ownership-tracking data structures. Note that
\t * while the token is not assigned a new owner, the `_ownedTokensIndex` mapping is _not_ updated: this allows for
\t * gas optimizations e.g. when performing a transfer operation (avoiding double writes).
\t * This has O(1) time complexity, but alters the order of the _ownedTokens array.
\t * @param from address representing the previous owner of the given token ID
\t * @param tokenId uint256 ID of the token to be removed from the tokens list of the given address
\t */
\tfunction _removeTokenFromOwnerEnumeration(address from, uint256 tokenId) private {
\t\t// To prevent a gap in from's tokens array, we store the last token in the index of the token to delete, and
\t\t// then delete the last slot (swap and pop).

\t\tuint256 lastTokenIndex = ERC721.balanceOf(from) - 1;
\t\tuint256 tokenIndex = _ownedTokensIndex[tokenId];

\t\t// When the token to delete is the last token, the swap operation is unnecessary
\t\tif (tokenIndex != lastTokenIndex) {
\t\t\tuint256 lastTokenId = _ownedTokens[from][lastTokenIndex];

\t\t\t_ownedTokens[from][tokenIndex] = lastTokenId; // Move the last token to the slot of the to-delete token
\t\t\t_ownedTokensIndex[lastTokenId] = tokenIndex; // Update the moved token's index
\t\t}

\t\t// This also deletes the contents at the last position of the array
\t\tdelete _ownedTokensIndex[tokenId];
\t\tdelete _ownedTokens[from][lastTokenIndex];
\t}

\t/**
\t * @dev Private function to remove a token from this extension's token tracking data structures.
\t * This has O(1) time complexity, but alters the order of the _allTokens array.
\t * @param tokenId uint256 ID of the token to be removed from the tokens list
\t */
\tfunction _removeTokenFromAllTokensEnumeration(uint256 tokenId) private {
\t\t// To prevent a gap in the tokens array, we store the last token in the index of the token to delete, and
\t\t// then delete the last slot (swap and pop).

\t\tuint256 lastTokenIndex = _allTokens.length - 1;
\t\tuint256 tokenIndex = _allTokensIndex[tokenId];

\t\t// When the token to delete is the last token, the swap operation is unnecessary. However, since this occurs so
\t\t// rarely (when the last minted token is burnt) that we still do the swap here to avoid the gas cost of adding
\t\t// an 'if' statement (like in _removeTokenFromOwnerEnumeration)
\t\tuint256 lastTokenId = _allTokens[lastTokenIndex];

\t\t_allTokens[tokenIndex] = lastTokenId; // Move the last token to the slot of the to-delete token
\t\t_allTokensIndex[lastTokenId] = tokenIndex; // Update the moved token's index

\t\t// This also deletes the contents at the last position of the array
\t\tdelete _allTokensIndex[tokenId];
\t\t_allTokens.pop();
\t}
}

contract LaserCats is ERC721Enumerable, Ownable {
\tusing SafeMath for uint256;
\tuint public constant maxTokenSupply = 25440;
\tuint public presalePriceCap;
\tuint public startPrice;
\tuint public priceFactor;
\tuint public maxTxQty;
\tbool public mintingAllowed;

\tconstructor() ERC721(\"LaserCats\", \"LaserCats\") { }

\tfunction getTokensOfOwner(address _owner) external view returns(uint256[] memory) {
\t\tuint256 tokenCount = balanceOf(_owner);
\t\tif (tokenCount == 0) {
\t\t\treturn new uint256[](0);
\t\t} else {
\t\t\tuint256[] memory result = new uint256[](tokenCount);
\t\t\tuint256 index;
\t\t\tfor (index = 0; index < tokenCount; index++) {
\t\t\t\tresult[index] = tokenOfOwnerByIndex(_owner, index);
\t\t\t}
\t\t\treturn result;
\t\t}
\t}

\tfunction getPresaleQty() public view returns(uint256) {
\t\trequire(mintingAllowed == true, \"Minting not allowed yet.\");
\t\trequire(totalSupply() < maxTokenSupply, \"All tokens have been minted.\");
\t\treturn maxTxQty;
\t}

\tfunction getPresalePrice() public view returns(uint256) {
\t\trequire(mintingAllowed == true, \"Minting not allowed yet.\");
\t\trequire(totalSupply() < maxTokenSupply, \"All tokens have been minted.\");
\t\treturn startPrice.mul(10 ** 16) + totalSupply().div(100).mul(priceFactor);
\t}

\tfunction mintToken(uint256 qty) public payable {
\t\trequire(qty > 0 && qty <= getPresaleQty(), \"Quantity exceeds allowed.\");
\t\trequire(totalSupply().add(qty) <= maxTokenSupply, \"Quantity exceeds max supply.\");
\t\trequire(msg.value >= getPresalePrice().mul(qty), \"Ether value to send is insufficient.\");

\t\tfor (uint i = 0; i < qty; i++) {
\t\t\tuint mintIndex = totalSupply();
\t\t\t_safeMint(msg.sender, mintIndex);
\t\t}
\t}

\t// reserves tokens 0-99 for giveaways
\tfunction mintPromoToken(uint256 qty) public onlyOwner {
\t\trequire(totalSupply().add(qty) <= 100, \"Quantity exceeds allowed.\");

\t\tfor (uint i = 0; i < qty; i++) {
\t\t\tuint mintIndex = totalSupply();
\t\t\t_safeMint(msg.sender, mintIndex);
\t\t}
\t}

\tfunction setPresaleAmounts(uint start, uint cap, uint qty) public onlyOwner {
\t\tstartPrice = start;
\t\tpresalePriceCap = cap;
\t\tmaxTxQty = qty;
\t\tpriceFactor = ((cap - start) * (10 ** 5) / (maxTokenSupply.div(100))) * (10 ** 11);
\t}

\tfunction setBaseURI(string memory baseURI) public onlyOwner {
\t\t_setBaseURI(baseURI);
\t}

\tfunction beginPresale() public onlyOwner {
\t\tmintingAllowed = true;
\t}

\tfunction pausePresale() public onlyOwner {
\t\tmintingAllowed = false;
\t}

\tfunction withdraw() public payable onlyOwner {
\t\trequire(payable(msg.sender).send(address(this).balance));
\t}
}

"
    }
  
