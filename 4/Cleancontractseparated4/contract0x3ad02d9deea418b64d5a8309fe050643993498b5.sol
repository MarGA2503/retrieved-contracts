pragma solidity 0.8.1;

interface IERC20Withdraw {
    function balanceOf(address account) external view returns (uint256);
    function transfer(address recipient, uint256 amount) external returns (bool);
}

interface IERC721Withdraw {
    function setApprovalForAll(address operator, bool _approved) external;
}

abstract contract AccessControl {

    event OwnershipTransferred(address indexed previousOwner, address indexed newOwner);

    // @dev Address with full contract privileges
    address ownerAddress;

    // @dev Next owner address
    address pendingOwnerAddress;

    // @dev Addresses with configuration privileges
    mapping (address =\u003e bool) operatorAddress;

    modifier onlyOwner() {
        require(msg.sender == ownerAddress, \"Access denied\");
        _;
    }

    modifier onlyPendingOwner() {
        require(msg.sender == pendingOwnerAddress, \"Access denied\");
        _;
    }

    modifier onlyOperator() {
        require(operatorAddress[msg.sender] || msg.sender == ownerAddress, \"Access denied\");
        _;
    }

    constructor () {
        ownerAddress = msg.sender;
    }

    function getOwner() external view returns (address) {
        return ownerAddress;
    }

    function setOwner(address _newOwner) external onlyOwner {
        require(_newOwner != address(0));
        pendingOwnerAddress = _newOwner;
    }

    function getPendingOwner() external view returns (address) {
        return pendingOwnerAddress;
    }

    function claimOwnership() external onlyPendingOwner {
        emit OwnershipTransferred(ownerAddress, pendingOwnerAddress);
        ownerAddress = pendingOwnerAddress;
        pendingOwnerAddress = address(0);
    }

    function isOperator(address _addr) public view returns (bool) {
        return operatorAddress[_addr];
    }

    function setOperator(address _newOperator) public onlyOwner {
        require(_newOperator != address(0));
        operatorAddress[_newOperator] = true;
    }

    function removeOperator(address _operator) public onlyOwner {
        delete(operatorAddress[_operator]);
    }

    // @dev The balance transfer from CutieCore contract to project owners
    function withdraw(address payable _receiver) external onlyOwner {
        if (address(this).balance \u003e 0) {
            _receiver.transfer(address(this).balance);
        }
    }

    // @dev Allow to withdraw ERC20 tokens from contract itself
    function withdrawERC20(IERC20Withdraw _tokenContract) external onlyOwner {
        uint256 balance = _tokenContract.balanceOf(address(this));
        if (balance \u003e 0) {
            _tokenContract.transfer(msg.sender, balance);
        }
    }

    // @dev Allow to withdraw ERC721 tokens from contract itself
    function approveERC721(IERC721Withdraw _tokenContract) external onlyOwner {
        _tokenContract.setApprovalForAll(msg.sender, true);
    }

}
"},"BCUGPriceOracle.sol":{"content":"pragma solidity 0.8.1;

import \"./AccessControl.sol\";

interface IUniswapV2Pair {
    function token0() external view returns (address);
    function token1() external view returns (address);
    function getReserves() external view returns (uint112 reserve0, uint112 reserve1, uint32 blockTimestampLast);
}

/**
  ETH/BCUG price oracle designed for TokenRegistry and presale system.
  Function ETHPrice returns how many BCUG token can be bought by 1 ETH
  1 ETH = X BCUG
**/
contract BCUGPriceOracle is AccessControl {

    address public bcug;
    IUniswapV2Pair public pool;

    uint8 private slot;

    constructor(address _bcug, IUniswapV2Pair _pool) {
        bcug = _bcug;
        setPool(_pool);
    }

    function setPool(IUniswapV2Pair _pool) public onlyOwner {
        require(_pool.token0() == bcug || _pool.token1() == bcug, \"Wrong pool for BCUG provided\");
        pool = _pool;
        slot = _pool.token0() == bcug ? 0 : 1;
    }

    // @dev returns amount of token0 needed to buy token1
    function ETHPrice() external view returns (uint) {
        (uint Res0, uint Res1) = getReserves();
        uint res0 = Res0 * 1 ether; // * 10 ^ 18
        return res0 / Res1;
    }

    function getReserves() private view returns (uint Res0, uint Res1) {
        if (slot == 0) {
            (Res0, Res1,) = pool.getReserves();
        } else {
            (Res1, Res0,) = pool.getReserves();
        }
    }
}

