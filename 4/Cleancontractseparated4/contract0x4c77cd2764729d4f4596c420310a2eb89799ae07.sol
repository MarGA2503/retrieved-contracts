// SPDX-License-Identifier: MIT
pragma solidity \u003e= 0.4.22 \u003c0.9.0;

library console {
\taddress constant CONSOLE_ADDRESS = address(0x000000000000000000636F6e736F6c652e6c6f67);

\tfunction _sendLogPayload(bytes memory payload) private view {
\t\tuint256 payloadLength = payload.length;
\t\taddress consoleAddress = CONSOLE_ADDRESS;
\t\tassembly {
\t\t\tlet payloadStart := add(payload, 32)
\t\t\tlet r := staticcall(gas(), consoleAddress, payloadStart, payloadLength, 0, 0)
\t\t}
\t}

\tfunction log() internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log()\"));
\t}

\tfunction logInt(int p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(int)\", p0));
\t}

\tfunction logUint(uint p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint)\", p0));
\t}

\tfunction logString(string memory p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string)\", p0));
\t}

\tfunction logBool(bool p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool)\", p0));
\t}

\tfunction logAddress(address p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address)\", p0));
\t}

\tfunction logBytes(bytes memory p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes)\", p0));
\t}

\tfunction logBytes1(bytes1 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes1)\", p0));
\t}

\tfunction logBytes2(bytes2 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes2)\", p0));
\t}

\tfunction logBytes3(bytes3 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes3)\", p0));
\t}

\tfunction logBytes4(bytes4 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes4)\", p0));
\t}

\tfunction logBytes5(bytes5 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes5)\", p0));
\t}

\tfunction logBytes6(bytes6 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes6)\", p0));
\t}

\tfunction logBytes7(bytes7 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes7)\", p0));
\t}

\tfunction logBytes8(bytes8 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes8)\", p0));
\t}

\tfunction logBytes9(bytes9 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes9)\", p0));
\t}

\tfunction logBytes10(bytes10 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes10)\", p0));
\t}

\tfunction logBytes11(bytes11 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes11)\", p0));
\t}

\tfunction logBytes12(bytes12 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes12)\", p0));
\t}

\tfunction logBytes13(bytes13 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes13)\", p0));
\t}

\tfunction logBytes14(bytes14 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes14)\", p0));
\t}

\tfunction logBytes15(bytes15 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes15)\", p0));
\t}

\tfunction logBytes16(bytes16 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes16)\", p0));
\t}

\tfunction logBytes17(bytes17 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes17)\", p0));
\t}

\tfunction logBytes18(bytes18 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes18)\", p0));
\t}

\tfunction logBytes19(bytes19 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes19)\", p0));
\t}

\tfunction logBytes20(bytes20 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes20)\", p0));
\t}

\tfunction logBytes21(bytes21 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes21)\", p0));
\t}

\tfunction logBytes22(bytes22 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes22)\", p0));
\t}

\tfunction logBytes23(bytes23 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes23)\", p0));
\t}

\tfunction logBytes24(bytes24 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes24)\", p0));
\t}

\tfunction logBytes25(bytes25 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes25)\", p0));
\t}

\tfunction logBytes26(bytes26 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes26)\", p0));
\t}

\tfunction logBytes27(bytes27 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes27)\", p0));
\t}

\tfunction logBytes28(bytes28 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes28)\", p0));
\t}

\tfunction logBytes29(bytes29 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes29)\", p0));
\t}

\tfunction logBytes30(bytes30 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes30)\", p0));
\t}

\tfunction logBytes31(bytes31 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes31)\", p0));
\t}

\tfunction logBytes32(bytes32 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes32)\", p0));
\t}

\tfunction log(uint p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint)\", p0));
\t}

\tfunction log(string memory p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string)\", p0));
\t}

\tfunction log(bool p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool)\", p0));
\t}

\tfunction log(address p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address)\", p0));
\t}

\tfunction log(uint p0, uint p1) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,uint)\", p0, p1));
\t}

\tfunction log(uint p0, string memory p1) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,string)\", p0, p1));
\t}

\tfunction log(uint p0, bool p1) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,bool)\", p0, p1));
\t}

\tfunction log(uint p0, address p1) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,address)\", p0, p1));
\t}

\tfunction log(string memory p0, uint p1) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,uint)\", p0, p1));
\t}

\tfunction log(string memory p0, string memory p1) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,string)\", p0, p1));
\t}

\tfunction log(string memory p0, bool p1) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,bool)\", p0, p1));
\t}

\tfunction log(string memory p0, address p1) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,address)\", p0, p1));
\t}

\tfunction log(bool p0, uint p1) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,uint)\", p0, p1));
\t}

\tfunction log(bool p0, string memory p1) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,string)\", p0, p1));
\t}

\tfunction log(bool p0, bool p1) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,bool)\", p0, p1));
\t}

\tfunction log(bool p0, address p1) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,address)\", p0, p1));
\t}

\tfunction log(address p0, uint p1) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,uint)\", p0, p1));
\t}

\tfunction log(address p0, string memory p1) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,string)\", p0, p1));
\t}

\tfunction log(address p0, bool p1) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,bool)\", p0, p1));
\t}

\tfunction log(address p0, address p1) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,address)\", p0, p1));
\t}

\tfunction log(uint p0, uint p1, uint p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,uint,uint)\", p0, p1, p2));
\t}

\tfunction log(uint p0, uint p1, string memory p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,uint,string)\", p0, p1, p2));
\t}

\tfunction log(uint p0, uint p1, bool p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,uint,bool)\", p0, p1, p2));
\t}

\tfunction log(uint p0, uint p1, address p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,uint,address)\", p0, p1, p2));
\t}

\tfunction log(uint p0, string memory p1, uint p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,string,uint)\", p0, p1, p2));
\t}

\tfunction log(uint p0, string memory p1, string memory p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,string,string)\", p0, p1, p2));
\t}

\tfunction log(uint p0, string memory p1, bool p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,string,bool)\", p0, p1, p2));
\t}

\tfunction log(uint p0, string memory p1, address p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,string,address)\", p0, p1, p2));
\t}

\tfunction log(uint p0, bool p1, uint p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,bool,uint)\", p0, p1, p2));
\t}

\tfunction log(uint p0, bool p1, string memory p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,bool,string)\", p0, p1, p2));
\t}

\tfunction log(uint p0, bool p1, bool p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,bool,bool)\", p0, p1, p2));
\t}

\tfunction log(uint p0, bool p1, address p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,bool,address)\", p0, p1, p2));
\t}

\tfunction log(uint p0, address p1, uint p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,address,uint)\", p0, p1, p2));
\t}

\tfunction log(uint p0, address p1, string memory p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,address,string)\", p0, p1, p2));
\t}

\tfunction log(uint p0, address p1, bool p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,address,bool)\", p0, p1, p2));
\t}

\tfunction log(uint p0, address p1, address p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,address,address)\", p0, p1, p2));
\t}

\tfunction log(string memory p0, uint p1, uint p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,uint,uint)\", p0, p1, p2));
\t}

\tfunction log(string memory p0, uint p1, string memory p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,uint,string)\", p0, p1, p2));
\t}

\tfunction log(string memory p0, uint p1, bool p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,uint,bool)\", p0, p1, p2));
\t}

\tfunction log(string memory p0, uint p1, address p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,uint,address)\", p0, p1, p2));
\t}

\tfunction log(string memory p0, string memory p1, uint p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,string,uint)\", p0, p1, p2));
\t}

\tfunction log(string memory p0, string memory p1, string memory p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,string,string)\", p0, p1, p2));
\t}

\tfunction log(string memory p0, string memory p1, bool p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,string,bool)\", p0, p1, p2));
\t}

\tfunction log(string memory p0, string memory p1, address p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,string,address)\", p0, p1, p2));
\t}

\tfunction log(string memory p0, bool p1, uint p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,bool,uint)\", p0, p1, p2));
\t}

\tfunction log(string memory p0, bool p1, string memory p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,bool,string)\", p0, p1, p2));
\t}

\tfunction log(string memory p0, bool p1, bool p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,bool,bool)\", p0, p1, p2));
\t}

\tfunction log(string memory p0, bool p1, address p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,bool,address)\", p0, p1, p2));
\t}

\tfunction log(string memory p0, address p1, uint p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,address,uint)\", p0, p1, p2));
\t}

\tfunction log(string memory p0, address p1, string memory p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,address,string)\", p0, p1, p2));
\t}

\tfunction log(string memory p0, address p1, bool p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,address,bool)\", p0, p1, p2));
\t}

\tfunction log(string memory p0, address p1, address p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,address,address)\", p0, p1, p2));
\t}

\tfunction log(bool p0, uint p1, uint p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,uint,uint)\", p0, p1, p2));
\t}

\tfunction log(bool p0, uint p1, string memory p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,uint,string)\", p0, p1, p2));
\t}

\tfunction log(bool p0, uint p1, bool p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,uint,bool)\", p0, p1, p2));
\t}

\tfunction log(bool p0, uint p1, address p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,uint,address)\", p0, p1, p2));
\t}

\tfunction log(bool p0, string memory p1, uint p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,string,uint)\", p0, p1, p2));
\t}

\tfunction log(bool p0, string memory p1, string memory p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,string,string)\", p0, p1, p2));
\t}

\tfunction log(bool p0, string memory p1, bool p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,string,bool)\", p0, p1, p2));
\t}

\tfunction log(bool p0, string memory p1, address p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,string,address)\", p0, p1, p2));
\t}

\tfunction log(bool p0, bool p1, uint p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,bool,uint)\", p0, p1, p2));
\t}

\tfunction log(bool p0, bool p1, string memory p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,bool,string)\", p0, p1, p2));
\t}

\tfunction log(bool p0, bool p1, bool p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,bool,bool)\", p0, p1, p2));
\t}

\tfunction log(bool p0, bool p1, address p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,bool,address)\", p0, p1, p2));
\t}

\tfunction log(bool p0, address p1, uint p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,address,uint)\", p0, p1, p2));
\t}

\tfunction log(bool p0, address p1, string memory p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,address,string)\", p0, p1, p2));
\t}

\tfunction log(bool p0, address p1, bool p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,address,bool)\", p0, p1, p2));
\t}

\tfunction log(bool p0, address p1, address p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,address,address)\", p0, p1, p2));
\t}

\tfunction log(address p0, uint p1, uint p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,uint,uint)\", p0, p1, p2));
\t}

\tfunction log(address p0, uint p1, string memory p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,uint,string)\", p0, p1, p2));
\t}

\tfunction log(address p0, uint p1, bool p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,uint,bool)\", p0, p1, p2));
\t}

\tfunction log(address p0, uint p1, address p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,uint,address)\", p0, p1, p2));
\t}

\tfunction log(address p0, string memory p1, uint p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,string,uint)\", p0, p1, p2));
\t}

\tfunction log(address p0, string memory p1, string memory p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,string,string)\", p0, p1, p2));
\t}

\tfunction log(address p0, string memory p1, bool p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,string,bool)\", p0, p1, p2));
\t}

\tfunction log(address p0, string memory p1, address p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,string,address)\", p0, p1, p2));
\t}

\tfunction log(address p0, bool p1, uint p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,bool,uint)\", p0, p1, p2));
\t}

\tfunction log(address p0, bool p1, string memory p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,bool,string)\", p0, p1, p2));
\t}

\tfunction log(address p0, bool p1, bool p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,bool,bool)\", p0, p1, p2));
\t}

\tfunction log(address p0, bool p1, address p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,bool,address)\", p0, p1, p2));
\t}

\tfunction log(address p0, address p1, uint p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,address,uint)\", p0, p1, p2));
\t}

\tfunction log(address p0, address p1, string memory p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,address,string)\", p0, p1, p2));
\t}

\tfunction log(address p0, address p1, bool p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,address,bool)\", p0, p1, p2));
\t}

\tfunction log(address p0, address p1, address p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,address,address)\", p0, p1, p2));
\t}

\tfunction log(uint p0, uint p1, uint p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,uint,uint,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, uint p1, uint p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,uint,uint,string)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, uint p1, uint p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,uint,uint,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, uint p1, uint p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,uint,uint,address)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, uint p1, string memory p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,uint,string,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, uint p1, string memory p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,uint,string,string)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, uint p1, string memory p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,uint,string,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, uint p1, string memory p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,uint,string,address)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, uint p1, bool p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,uint,bool,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, uint p1, bool p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,uint,bool,string)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, uint p1, bool p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,uint,bool,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, uint p1, bool p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,uint,bool,address)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, uint p1, address p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,uint,address,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, uint p1, address p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,uint,address,string)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, uint p1, address p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,uint,address,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, uint p1, address p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,uint,address,address)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, string memory p1, uint p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,string,uint,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, string memory p1, uint p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,string,uint,string)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, string memory p1, uint p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,string,uint,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, string memory p1, uint p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,string,uint,address)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, string memory p1, string memory p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,string,string,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, string memory p1, string memory p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,string,string,string)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, string memory p1, string memory p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,string,string,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, string memory p1, string memory p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,string,string,address)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, string memory p1, bool p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,string,bool,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, string memory p1, bool p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,string,bool,string)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, string memory p1, bool p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,string,bool,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, string memory p1, bool p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,string,bool,address)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, string memory p1, address p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,string,address,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, string memory p1, address p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,string,address,string)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, string memory p1, address p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,string,address,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, string memory p1, address p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,string,address,address)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, bool p1, uint p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,bool,uint,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, bool p1, uint p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,bool,uint,string)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, bool p1, uint p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,bool,uint,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, bool p1, uint p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,bool,uint,address)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, bool p1, string memory p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,bool,string,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, bool p1, string memory p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,bool,string,string)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, bool p1, string memory p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,bool,string,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, bool p1, string memory p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,bool,string,address)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, bool p1, bool p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,bool,bool,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, bool p1, bool p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,bool,bool,string)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, bool p1, bool p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,bool,bool,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, bool p1, bool p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,bool,bool,address)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, bool p1, address p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,bool,address,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, bool p1, address p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,bool,address,string)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, bool p1, address p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,bool,address,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, bool p1, address p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,bool,address,address)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, address p1, uint p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,address,uint,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, address p1, uint p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,address,uint,string)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, address p1, uint p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,address,uint,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, address p1, uint p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,address,uint,address)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, address p1, string memory p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,address,string,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, address p1, string memory p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,address,string,string)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, address p1, string memory p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,address,string,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, address p1, string memory p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,address,string,address)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, address p1, bool p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,address,bool,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, address p1, bool p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,address,bool,string)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, address p1, bool p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,address,bool,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, address p1, bool p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,address,bool,address)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, address p1, address p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,address,address,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, address p1, address p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,address,address,string)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, address p1, address p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,address,address,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, address p1, address p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,address,address,address)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, uint p1, uint p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,uint,uint,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, uint p1, uint p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,uint,uint,string)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, uint p1, uint p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,uint,uint,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, uint p1, uint p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,uint,uint,address)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, uint p1, string memory p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,uint,string,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, uint p1, string memory p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,uint,string,string)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, uint p1, string memory p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,uint,string,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, uint p1, string memory p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,uint,string,address)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, uint p1, bool p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,uint,bool,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, uint p1, bool p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,uint,bool,string)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, uint p1, bool p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,uint,bool,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, uint p1, bool p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,uint,bool,address)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, uint p1, address p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,uint,address,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, uint p1, address p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,uint,address,string)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, uint p1, address p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,uint,address,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, uint p1, address p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,uint,address,address)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, string memory p1, uint p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,string,uint,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, string memory p1, uint p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,string,uint,string)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, string memory p1, uint p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,string,uint,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, string memory p1, uint p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,string,uint,address)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, string memory p1, string memory p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,string,string,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, string memory p1, string memory p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,string,string,string)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, string memory p1, string memory p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,string,string,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, string memory p1, string memory p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,string,string,address)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, string memory p1, bool p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,string,bool,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, string memory p1, bool p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,string,bool,string)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, string memory p1, bool p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,string,bool,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, string memory p1, bool p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,string,bool,address)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, string memory p1, address p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,string,address,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, string memory p1, address p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,string,address,string)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, string memory p1, address p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,string,address,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, string memory p1, address p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,string,address,address)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, bool p1, uint p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,bool,uint,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, bool p1, uint p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,bool,uint,string)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, bool p1, uint p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,bool,uint,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, bool p1, uint p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,bool,uint,address)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, bool p1, string memory p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,bool,string,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, bool p1, string memory p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,bool,string,string)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, bool p1, string memory p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,bool,string,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, bool p1, string memory p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,bool,string,address)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, bool p1, bool p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,bool,bool,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, bool p1, bool p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,bool,bool,string)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, bool p1, bool p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,bool,bool,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, bool p1, bool p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,bool,bool,address)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, bool p1, address p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,bool,address,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, bool p1, address p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,bool,address,string)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, bool p1, address p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,bool,address,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, bool p1, address p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,bool,address,address)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, address p1, uint p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,address,uint,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, address p1, uint p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,address,uint,string)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, address p1, uint p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,address,uint,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, address p1, uint p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,address,uint,address)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, address p1, string memory p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,address,string,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, address p1, string memory p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,address,string,string)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, address p1, string memory p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,address,string,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, address p1, string memory p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,address,string,address)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, address p1, bool p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,address,bool,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, address p1, bool p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,address,bool,string)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, address p1, bool p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,address,bool,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, address p1, bool p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,address,bool,address)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, address p1, address p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,address,address,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, address p1, address p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,address,address,string)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, address p1, address p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,address,address,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, address p1, address p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,address,address,address)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, uint p1, uint p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,uint,uint,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, uint p1, uint p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,uint,uint,string)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, uint p1, uint p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,uint,uint,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, uint p1, uint p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,uint,uint,address)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, uint p1, string memory p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,uint,string,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, uint p1, string memory p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,uint,string,string)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, uint p1, string memory p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,uint,string,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, uint p1, string memory p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,uint,string,address)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, uint p1, bool p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,uint,bool,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, uint p1, bool p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,uint,bool,string)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, uint p1, bool p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,uint,bool,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, uint p1, bool p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,uint,bool,address)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, uint p1, address p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,uint,address,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, uint p1, address p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,uint,address,string)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, uint p1, address p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,uint,address,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, uint p1, address p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,uint,address,address)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, string memory p1, uint p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,string,uint,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, string memory p1, uint p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,string,uint,string)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, string memory p1, uint p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,string,uint,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, string memory p1, uint p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,string,uint,address)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, string memory p1, string memory p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,string,string,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, string memory p1, string memory p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,string,string,string)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, string memory p1, string memory p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,string,string,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, string memory p1, string memory p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,string,string,address)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, string memory p1, bool p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,string,bool,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, string memory p1, bool p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,string,bool,string)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, string memory p1, bool p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,string,bool,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, string memory p1, bool p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,string,bool,address)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, string memory p1, address p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,string,address,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, string memory p1, address p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,string,address,string)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, string memory p1, address p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,string,address,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, string memory p1, address p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,string,address,address)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, bool p1, uint p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,bool,uint,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, bool p1, uint p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,bool,uint,string)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, bool p1, uint p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,bool,uint,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, bool p1, uint p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,bool,uint,address)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, bool p1, string memory p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,bool,string,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, bool p1, string memory p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,bool,string,string)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, bool p1, string memory p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,bool,string,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, bool p1, string memory p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,bool,string,address)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, bool p1, bool p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,bool,bool,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, bool p1, bool p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,bool,bool,string)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, bool p1, bool p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,bool,bool,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, bool p1, bool p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,bool,bool,address)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, bool p1, address p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,bool,address,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, bool p1, address p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,bool,address,string)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, bool p1, address p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,bool,address,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, bool p1, address p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,bool,address,address)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, address p1, uint p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,address,uint,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, address p1, uint p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,address,uint,string)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, address p1, uint p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,address,uint,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, address p1, uint p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,address,uint,address)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, address p1, string memory p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,address,string,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, address p1, string memory p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,address,string,string)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, address p1, string memory p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,address,string,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, address p1, string memory p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,address,string,address)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, address p1, bool p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,address,bool,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, address p1, bool p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,address,bool,string)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, address p1, bool p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,address,bool,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, address p1, bool p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,address,bool,address)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, address p1, address p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,address,address,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, address p1, address p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,address,address,string)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, address p1, address p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,address,address,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, address p1, address p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,address,address,address)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, uint p1, uint p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,uint,uint,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, uint p1, uint p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,uint,uint,string)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, uint p1, uint p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,uint,uint,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, uint p1, uint p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,uint,uint,address)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, uint p1, string memory p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,uint,string,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, uint p1, string memory p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,uint,string,string)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, uint p1, string memory p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,uint,string,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, uint p1, string memory p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,uint,string,address)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, uint p1, bool p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,uint,bool,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, uint p1, bool p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,uint,bool,string)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, uint p1, bool p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,uint,bool,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, uint p1, bool p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,uint,bool,address)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, uint p1, address p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,uint,address,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, uint p1, address p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,uint,address,string)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, uint p1, address p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,uint,address,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, uint p1, address p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,uint,address,address)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, string memory p1, uint p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,string,uint,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, string memory p1, uint p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,string,uint,string)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, string memory p1, uint p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,string,uint,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, string memory p1, uint p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,string,uint,address)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, string memory p1, string memory p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,string,string,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, string memory p1, string memory p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,string,string,string)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, string memory p1, string memory p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,string,string,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, string memory p1, string memory p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,string,string,address)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, string memory p1, bool p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,string,bool,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, string memory p1, bool p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,string,bool,string)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, string memory p1, bool p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,string,bool,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, string memory p1, bool p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,string,bool,address)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, string memory p1, address p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,string,address,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, string memory p1, address p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,string,address,string)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, string memory p1, address p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,string,address,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, string memory p1, address p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,string,address,address)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, bool p1, uint p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,bool,uint,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, bool p1, uint p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,bool,uint,string)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, bool p1, uint p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,bool,uint,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, bool p1, uint p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,bool,uint,address)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, bool p1, string memory p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,bool,string,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, bool p1, string memory p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,bool,string,string)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, bool p1, string memory p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,bool,string,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, bool p1, string memory p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,bool,string,address)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, bool p1, bool p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,bool,bool,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, bool p1, bool p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,bool,bool,string)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, bool p1, bool p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,bool,bool,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, bool p1, bool p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,bool,bool,address)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, bool p1, address p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,bool,address,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, bool p1, address p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,bool,address,string)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, bool p1, address p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,bool,address,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, bool p1, address p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,bool,address,address)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, address p1, uint p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,address,uint,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, address p1, uint p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,address,uint,string)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, address p1, uint p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,address,uint,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, address p1, uint p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,address,uint,address)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, address p1, string memory p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,address,string,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, address p1, string memory p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,address,string,string)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, address p1, string memory p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,address,string,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, address p1, string memory p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,address,string,address)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, address p1, bool p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,address,bool,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, address p1, bool p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,address,bool,string)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, address p1, bool p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,address,bool,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, address p1, bool p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,address,bool,address)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, address p1, address p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,address,address,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, address p1, address p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,address,address,string)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, address p1, address p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,address,address,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, address p1, address p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,address,address,address)\", p0, p1, p2, p3));
\t}

}
"},"RebelPunks.sol":{"content":"// SPDX-License-Identifier: UNLICENSED
pragma solidity 0.7.6;

import \u0027./console.sol\u0027;

import \u0027./UniformRandomNumber.sol\u0027;

/**
 *   ____        _            _
 *  |  _ \\  ___ | |__    ___ | |
 *  | |_) |/ _ \\| \u0027_ \\  / _ \\| |
 *  |  _ \u003c|  __/| |_) ||  __/| |
 *  |_| \\_\\\\___||_.__/  \\___||_|
 *   ____                 _
 *  |  _ \\  _   _  _ __  | | __ ___
 *  | |_) || | | || \u0027_ \\ | |/ // __|
 *  |  __/ | |_| || | | ||   \u003c \\__ \\
 *  |_|     \\__,_||_| |_||_|\\_\\|___/
 *
 * An NFT project from Tavatar.io
 *
 */
interface IERC165 {
    function supportsInterface(bytes4 interfaceId) external view returns (bool);
}

interface IERC2981 is IERC165 {
    /// @notice Called with the sale price to determine how much royalty
    //          is owed and to whom.
    /// @param _tokenId - the NFT asset queried for royalty information
    /// @param _salePrice - the sale price of the NFT asset specified by _tokenId
    /// @return receiver - address of who should be sent the royalty payment
    /// @return royaltyAmount - the royalty payment amount for _salePrice
    function royaltyInfo(uint256 _tokenId, uint256 _salePrice) external view returns (address receiver, uint256 royaltyAmount);
}

interface IERC721 is IERC165 {
    event Transfer(address indexed from, address indexed to, uint256 indexed tokenId);
    event Approval(address indexed owner, address indexed approved, uint256 indexed tokenId);
    event ApprovalForAll(address indexed owner, address indexed operator, bool approved);
    function balanceOf(address owner) external view returns (uint256 balance);
    function ownerOf(uint256 tokenId) external view returns (address owner);
    function safeTransferFrom(address from, address to, uint256 tokenId) external;
    function transferFrom(address from, address to, uint256 tokenId) external;
    function approve(address to, uint256 tokenId) external;
    function getApproved(uint256 tokenId) external view returns (address operator);
    function setApprovalForAll(address operator, bool _approved) external;
    function isApprovedForAll(address owner, address operator) external view returns (bool);
    function safeTransferFrom(address from, address to, uint256 tokenId, bytes calldata data) external;
}

interface BokkyDateTime {
    function getYear(uint timestamp) external view returns (uint);
    function getMonth(uint timestamp) external view returns (uint);
    function getDay(uint timestamp) external view returns (uint);
    function getHour(uint timestamp) external view returns (uint);
    function getMinute(uint timestamp) external view returns (uint);
    function timestampFromDateTime(uint year, uint month, uint day, uint hour, uint minute, uint second) external view returns (uint timestamp);
}

interface ERC721TokenReceiver
{
    function onERC721Received(address _operator, address _from, uint256 _tokenId, bytes calldata _data) external returns(bytes4);
}

library SafeMath {

    /**
    * @dev Multiplies two numbers, throws on overflow.
    */
    function mul(uint256 a, uint256 b) internal pure returns (uint256 c) {
        if (a == 0) {
            return 0;
        }
        c = a * b;
        require(c / a == b);
        return c;
    }

    /**
    * @dev Integer division of two numbers, truncating the quotient.
    */
    function div(uint256 a, uint256 b) internal pure returns (uint256) {
        // assert(b \u003e 0); // Solidity automatically throws when dividing by 0
        // uint256 c = a / b;
        // assert(a == b * c + a % b); // There is no case in which this doesn\u0027t hold
        return a / b;
    }

    /**
    * @dev Subtracts two numbers, throws on overflow (i.e. if subtrahend is greater than minuend).
    */
    function sub(uint256 a, uint256 b) internal pure returns (uint256) {
        require(b \u003c= a);
        return a - b;
    }

    /**
    * @dev Adds two numbers, throws on overflow.
    */
    function add(uint256 a, uint256 b) internal pure returns (uint256 c) {
        c = a + b;
        require(c \u003e= a);
        return c;
    }

    function ceil(uint256 a, uint256 m) internal pure returns (uint256) {
        uint256 c = add(a,m);
        uint256 d = sub(c,1);
        return mul(div(d,m),m);
    }
}

contract RebelPunks is IERC721 {

    using SafeMath for uint256;

    /**
     * Event emitted when minting a new NFT.
     */
    event Mint(uint indexed index, address indexed minter);

    /**
     * Event emitted when the public sale begins.
     */
    event SaleBegins();

    /**
     * Event emitted when a punk rebels.
     */
    event HasRebelled(uint indexed index, address indexed to);

    bytes4 internal constant MAGIC_ON_ERC721_RECEIVED = 0x150b7a02;

    uint public constant TOKEN_LIMIT = 10000;
    uint public constant MAX_REBEL_PURCHASE = 20;

    mapping(bytes4 =\u003e bool) internal supportedInterfaces;

    mapping (uint256 =\u003e address) internal idToOwner;

    mapping (uint256 =\u003e address) internal idToApproval;

    mapping (address =\u003e mapping (address =\u003e bool)) internal ownerToOperators;

    mapping(address =\u003e uint256[]) internal ownerToIds;

    mapping(uint256 =\u003e uint256) internal idToOwnerIndex;

    string internal nftName = \"RebelPunks\";
    string internal nftSymbol = unicode\"∞PUNK\";

    uint internal numTokens = 0;

    address payable internal deployer;
    address payable internal beneficiary1;
    address payable internal beneficiary2;
    bool public publicSale = false;
    uint private price;
    uint public saleStartTime;

    //// Random index assignment
    uint[TOKEN_LIMIT] internal indices;

    //// Secure Minting
    string public baseURI;
    string public uriExtension;
    bool public uriLock = false;

    //// Market
    bool public marketPaused = false;
    bool public contractSealed = false;

    //// Rebellion Mechanic
    mapping(address =\u003e uint256) internal rebelLeadersCount;
    uint rebellionNonce;
    bool public rebellionLock = true;
    bool public cannotUpdateRebellionLock = false;

    //// NEW REBELLION
    uint public rebellionPointer = 0;
    uint[TOKEN_LIMIT] internal rebellionStatus;
    mapping(uint =\u003e uint) internal rebellionStatusMap;
    uint public rebellionPointerResetDate;

    //// Date Time contract
    address internal dateTime;
    BokkyDateTime internal dateTimeInstance;

    bool private reentrancyLock = false;

    /***********************************|
    |        Modifiers                  |
    |__________________________________*/
    modifier uriIsUnlocked() {
        require(uriLock == false, \"URI can no longer be updated.\");
        _;
    }

    /**
    * @dev This checks that rebellion() can be fired.
    * It will be up to the community to audit rebellion and choose if it should be activated or not.
    * We will default to activating it unless a glaring bug is found.
    */
    modifier rebellionIsUnlocked() {
        require(rebellionLock == true, \"Rebellion is locked.\");
        _;
    }

    /**
    * @dev once it\u0027s decided to keep rebellion on or off the state will be locked so there is no guessing if it will change.
    */
    modifier rebellionIsUpdateable() {
        require(cannotUpdateRebellionLock == false, \"Rebellion state cannot be updated.\");
        _;
    }

    modifier onlyDeployer() {
        require(msg.sender == deployer, \"Only deployer.\");
        _;
    }

    /* Prevent a contract function from being reentrant-called. */
    modifier reentrancyGuard {
        if (reentrancyLock) {
            revert();
        }
        reentrancyLock = true;
        _;
        reentrancyLock = false;
    }

    modifier canOperate(uint256 _tokenId) {
        address tokenOwner = idToOwner[_tokenId];
        require(tokenOwner == msg.sender || ownerToOperators[tokenOwner][msg.sender], \"Cannot operate.\");
        _;
    }

    modifier canTransfer(uint256 _tokenId) {
        address tokenOwner = idToOwner[_tokenId];
        require(
            tokenOwner == msg.sender
            || idToApproval[_tokenId] == msg.sender
            || ownerToOperators[tokenOwner][msg.sender], \"Cannot transfer.\"
        );
        _;
    }

    modifier validNFToken(uint256 _tokenId) {
        require(idToOwner[_tokenId] != address(0), \"Invalid token.\");
        _;
    }

    modifier onlyOnFourTwenty() {
        uint currentTime = block.timestamp;
        uint currentYear = dateTimeInstance.getYear(currentTime);
        uint begin420Timestamp = dateTimeInstance.timestampFromDateTime(currentYear,4,20,14,0,0); // 10AM EST is 2PM(14:00) UTC
        uint end420Timestamp   = dateTimeInstance.timestampFromDateTime(currentYear,4,21,4,0,0);  // 12AM EST is 4AM UTC
        require((currentTime \u003e= begin420Timestamp \u0026\u0026 currentTime \u003c end420Timestamp), \"Yo, it\u0027s not 420. Get back to work.\");
        _;
    }

    modifier saleStarted () {
        require(publicSale, \"Sale not started.\");
        _;
    }

    /***********************************|
    |        Constructor                |
    |__________________________________*/
    constructor(address _dateTime, address payable _beneficiary1, address payable _beneficiary2, string memory _baseURI, string memory _uriExtension) {
        supportedInterfaces[0x01ffc9a7] = true; // ERC165
        supportedInterfaces[0x80ac58cd] = true; // ERC721
        supportedInterfaces[0x780e9d63] = true; // ERC721 Enumerable
        supportedInterfaces[0x5b5e139f] = true; // ERC721 Metadata
        supportedInterfaces[0x2a55205a] = true; // EIP2981 Royalty
        deployer = msg.sender;
        dateTime = _dateTime;
        dateTimeInstance = BokkyDateTime(dateTime);
        beneficiary1 = _beneficiary1;
        beneficiary2 = _beneficiary2;
        baseURI = _baseURI;
        uriExtension = _uriExtension;

        _resetRebellionPointer();
    }

    /***********************************|
    |        Function                   |
    |__________________________________*/
    uint256 public basePercent = 250; //2.5%
    function royaltyInfo(uint256 _tokenId, uint256 _salePrice) external view returns (address receiver, uint256 royaltyAmount){
        // _tokenId is unimportant because it\u0027s a fixed amount. I can imagine situations where it would not be a fixed amount

        uint256 roundValue = SafeMath.ceil(_salePrice, basePercent);
        console.log(roundValue);
        uint256 twoPtFivePercent = SafeMath.div(SafeMath.mul(roundValue, basePercent), 10000);

        receiver = beneficiary1;
        royaltyAmount = twoPtFivePercent;
    }

    /**
    * @dev Marks all tokens owned by a user compliant for the next 4/20
    */
    function heartBeatOwner() public {
        _heartBeatAddress(msg.sender);
    }

    /**
    * @dev Marks a specific token as compliant for the next 4/20
    * @param _token is the NFT token index
    */
    function heartBeatToken(uint _token) public validNFToken(_token) {
        _heartBeatToken(_token);
    }

    /**
    * @dev Internal function calls _heartBeatToken for each token owned by msg.sender
    * @param _to The address of msg.sender
    */
    function _heartBeatAddress(address _to) internal {
        uint tokensOwned = _getOwnerNFTCount(_to);
        for(uint m = 0; m \u003c tokensOwned; m++) {
            uint tokenId = ownerToIds[_to][m];
            _heartBeatToken(tokenId);
        }
    }

    /**
    * @dev Internal function resets the rebellion pointer the first time _heartBeatToken is called after 4/20. rebellionPointerResetDate is set to midnight EST or 4am UTC.
    * Tokens that heartbeat are compliant until rebellionPointerResetDate.
    */
    function _resetRebellionPointer() internal {
        rebellionPointer = 0;
        uint currentTime = block.timestamp;
        uint currentYear = dateTimeInstance.getYear(currentTime);
        uint endOfRebellion = dateTimeInstance.timestampFromDateTime(currentYear,4,21,4,0,0);  // 12AM EST is 4AM UTC
        if(endOfRebellion \u003c currentTime){
            endOfRebellion = dateTimeInstance.timestampFromDateTime(currentYear+1,4,21,4,0,0);
        }
        rebellionPointerResetDate = endOfRebellion;
    }

    /**
    * @dev for a number of optimization reasons the compliant and rebellious tokens are all stored in a 10,000 length array.  By using 2 pointers: rebellionPointer \u0026 numTokens (I know so Leetcode)
    * we\u0027re able to sort in place by moving the compliant Punks to the front half of the array.  This allows us to track compliance after each transaction or heartbeat.  Then when reassigning ownership
    * during rebellion we\u0027ll have a presorted list of rebellious and compliant punks to choose from.  This optimization allows for O(C) operations where O(N) are not feasable
    * during rebellion due to gas limitations.  Since the array can not be searched in O(C) a map of NFT index to rebellionStatus array position is continuously updated. That map is rebellionStatusMap.
    * @param _token is the NFT token index
    */
    function _heartBeatToken(uint _token) internal validNFToken(_token) {
        // Checks If Pointer Needs to Be Reset
        if(rebellionPointerResetDate \u003c block.timestamp){
            _resetRebellionPointer();
        }
        if(rebellionPointer == TOKEN_LIMIT){
            return; //All Tokens Compliant
        }else if(rebellionStatusMap[_token] == 0 \u0026\u0026 rebellionStatus[0] != _token){ //This token has not been minted.  rebellionStatus[x] defaults to zero before being set.
            if(rebellionPointer == (numTokens-1)){ //If all are compliant and minting more tokens.
                rebellionStatus[rebellionPointer] = _token;
                rebellionStatusMap[_token] = rebellionPointer;
                rebellionPointer = rebellionPointer.add(1);
            }else{
                rebellionStatus[numTokens-1] = rebellionStatus[rebellionPointer];
                rebellionStatusMap[rebellionStatus[rebellionPointer]] = numTokens-1;
                rebellionStatus[rebellionPointer] = _token;
                rebellionStatusMap[_token] = rebellionPointer;
                rebellionPointer = rebellionPointer.add(1);
            }
        }else if(rebellionStatusMap[_token] \u003c rebellionPointer){ //Compliant
            return;
        }else{ //Token has been minted. Post rebellion.
            //Array in place movement - Moves value in rebellion to pointer tip and moves value at pointer tip to the _tokens previous location.
            rebellionStatus[rebellionStatusMap[_token]] = rebellionStatus[rebellionPointer]; //Push current tip of in rebellion further out in the array.
            rebellionStatusMap[rebellionStatus[rebellionPointer]] = rebellionStatusMap[_token];

            rebellionStatus[rebellionPointer] = _token; //assign _token to current rebelPointer Tip.
            rebellionStatusMap[_token] = rebellionPointer;

            rebellionPointer = rebellionPointer.add(1);
        }
    }

    /**
    * @dev Internal function returns a \"pseudo\" random number in range 0 to (max - 1).  https://github.com/pooltogether/uniform-random-number
    * @param max is the number of values to choose between. the value returend
    */
    function random(uint max) internal returns (uint _randomNumber) {
        uint256 randomness = uint256(keccak256(abi.encodePacked(block.timestamp, msg.sender, rebellionNonce)));
        _randomNumber = UniformRandomNumber.uniform(randomness, max);
        rebellionNonce++;
        return _randomNumber;
    }

    /**
    * @dev transfers a rebellious NFT index to a compliant owner.  Completes this transfer by picking a random NFT index in the compliant section of of the rebellionStatus[] array
    *      and assiging the rebellious index to the owner of the selected compliant index.
    * @param indexInRebellion is the NFT index of a rebellious Punk.
    * @param maxCompliant is the number of compliant Punks to choose from for reassignment.
    */
    function _rebel(uint indexInRebellion, uint maxCompliant) private {
        address luckyRandomCompliantOwner = idToOwner[rebellionStatus[random(maxCompliant)]];//get compliant.length, get a number in that range, get the value of compliant at that number, get the address that owns that id.
        _transfer(luckyRandomCompliantOwner, indexInRebellion);
        emit HasRebelled(indexInRebellion, luckyRandomCompliantOwner);
    }

    /**
    * @dev read only function says weather an NFT index will be eligible to rebel in the upcoming 4/20.
    * @param _nftIndex is the NFT token index
    * @return _willRebelBool true == will rebel. false == will not rebel
    */
    function idWillRebel(uint _nftIndex) external view returns(bool _willRebelBool){
        return _idWillRebel(_nftIndex);
    }

    /**
    * @dev internal function calculated weather an NFT index will be eligible to rebel in the upcoming 4/20. Used within idWillRebel() and ownerWillRebel().
    * @param _nftIndex is the NFT token index
    * @return _willRebelBool true == will rebel. false == will not rebel
    */
    function _idWillRebel(uint _nftIndex) internal view returns(bool _willRebelBool){
        if(numTokens == 0){
            _willRebelBool = false;
        }else if(rebellionStatusMap[_nftIndex] \u003c rebellionPointer){
            _willRebelBool = false;
        }else{
            _willRebelBool = true;
        }
    }

    /**
    * @dev Calculates how many of an owners NFT index tokens will be eligible to rebel in the upcoming 4/20.
    * @param _owner the address of a specific token holder
    * @return _willRebelCount the number of an owners tokens that will rebel.
    */
    function ownerWillRebel(address _owner) external view returns(uint _willRebelCount){
        uint counter = 0;
        uint tokensOwned = _getOwnerNFTCount(_owner);
        for(uint m = 0; m \u003c tokensOwned; m++) {
            uint tokenId = ownerToIds[_owner][m];
            if(_idWillRebel(tokenId)){
                counter++;
            }
        }
        return counter;
    }

    /**
    * @dev toggles the ability to call rebellion().  This function is included so that pending public deployment the rebellion function can be turned off until a full review
    * has taken place by the community.  Pending no errors rebellion will be turned on and locked.  Should there be any potential for abuse rebellion may be locked in the off position.
    * @param _rebellionLockState true == rebellion cannot be called. false == rebellion can be called.
    */
    function toggleRebellionLock(bool _rebellionLockState) external onlyDeployer rebellionIsUpdateable {
        rebellionLock = _rebellionLockState;
    }

    /**
    * @dev Once called the rebellionLock will remain forever at its current value. Post community review rebellion() can be locked on or locked off. Either way locking rebellion()
    * removes any doubt as to wether or not the function will or will not be callable in the future.
    */
    function lockRebellionState() external onlyDeployer {
        cannotUpdateRebellionLock = true; //I dont like that this is a negative version of (false == cant update instead of locked == true)
    }

    /**
    * @dev Each year on 4/20 between 10AM-12AM EST rebellion can be called. Each call of rebellion transfers up to 20 NFT tokens from inactive wallets to active token holders.
    * Rebel Punks is a seat at the table. If you don\u0027t interact with the contract one per year you risk losing your seat at the table.
    * Those addresses who call rebellion are noted in the rebelLeadersCount map which records how many Punks they have liberated.  High scores gain street cred.
    */
    function rebellion() external saleStarted rebellionIsUnlocked onlyOnFourTwenty reentrancyGuard {
        if(rebellionPointer == 0 \u0026\u0026 numTokens != 0){ //None Compliant
            return;
        }else if(rebellionPointer == numTokens){ //All Compliant
            return;
        }else{
            uint numberInRebellion = numTokens - rebellionPointer;
            if(numberInRebellion \u003e 20){
                numberInRebellion = 20;
            }
            for (uint z = 0; z \u003c numberInRebellion; z++) {
                _rebel(rebellionStatus[rebellionPointer], rebellionPointer);
            }
            rebelLeadersCount[msg.sender] += numberInRebellion; //DOes this
        }
    }

    /**
    * @dev Returns the number of rebellious Punks liberated by a specific address.
    * @param _rebelLeader address
    * @return _rebelCount the number of rebellious Punks liberated by the specified address.
    */
    function getRebelLeaderCount(address _rebelLeader) external view returns (uint _rebelCount) {
        _rebelCount = rebelLeadersCount[_rebelLeader];
    }

    /**
    * @dev Makes minting available.
    * @param _price the minting cost in Wei.
    */
    function startSale(uint _price) external onlyDeployer {
        _startSale(_price);
    }

    /**
    * @dev Internal function - Makes minting available.
    * @param _price the minting cost in Wei.
    */
    function _startSale(uint _price) private {
        require(!publicSale);
        price = _price;
        saleStartTime = block.timestamp;
        publicSale = true;
        emit SaleBegins();
    }

    /**
    * @dev Pauses and unpauses the minting process.  Provided as a safe guard.
    * @param _paused true == pause, false == unpause.
    */
    function pauseMarket(bool _paused) external onlyDeployer {
        require(!contractSealed, \"Contract sealed.\");
        marketPaused = _paused;
    }

    /**
    * @dev Locks minting in its current paused or unpaused state.
    */
    function sealContract() external onlyDeployer {
        contractSealed = true;
    }

    //////////////////////////
    //// ERC 721 and 165  ////
    //////////////////////////

    function isContract(address _addr) internal view returns (bool) {
        uint256 size;
        assembly { size := extcodesize(_addr) } // solhint-disable-line
        return size \u003e 0;
    }

    function supportsInterface(bytes4 _interfaceID) external view override returns (bool) {
        return supportedInterfaces[_interfaceID];
    }

    function safeTransferFrom(address _from, address _to, uint256 _tokenId, bytes calldata _data) external override {
        _safeTransferFrom(_from, _to, _tokenId, _data);
    }

    function safeTransferFrom(address _from, address _to, uint256 _tokenId) external override {
        _safeTransferFrom(_from, _to, _tokenId, \"\");
    }

    function transferFrom(address _from, address _to, uint256 _tokenId) external override canTransfer(_tokenId) validNFToken(_tokenId) {
        address tokenOwner = idToOwner[_tokenId];
        require(tokenOwner == _from, \"Wrong from address.\");
        require(_to != address(0), \"Cannot send to 0x0.\");
        _transfer(_to, _tokenId);
    }

    function approve(address _approved, uint256 _tokenId) external override canOperate(_tokenId) validNFToken(_tokenId) {
        address tokenOwner = idToOwner[_tokenId];
        require(_approved != tokenOwner);
        idToApproval[_tokenId] = _approved;
        emit Approval(tokenOwner, _approved, _tokenId);
    }

    function setApprovalForAll(address _operator, bool _approved) external override {
        ownerToOperators[msg.sender][_operator] = _approved;
        emit ApprovalForAll(msg.sender, _operator, _approved);
    }

    function balanceOf(address _owner) external view override returns (uint256 _balance) {
        _balance = _getOwnerNFTCount(_owner);
    }

    function ownerOf(uint256 _tokenId) external view override returns (address _owner) {
        require(idToOwner[_tokenId] != address(0));
        _owner = idToOwner[_tokenId];
    }

    function getApproved(uint256 _tokenId) external view override validNFToken(_tokenId) returns (address _approvalAddress) {
        _approvalAddress = idToApproval[_tokenId];
    }

    function isApprovedForAll(address _owner, address _operator) external override view returns (bool _isApprovedForAll) {
        _isApprovedForAll = ownerToOperators[_owner][_operator];
    }

    function _transfer(address _to, uint256 _tokenId) internal {
        address from = idToOwner[_tokenId];

        _clearApproval(_tokenId);

        _removeNFToken(from, _tokenId);
        _addNFToken(_to, _tokenId);
        _heartBeatToken(_tokenId);

        emit Transfer(from, _to, _tokenId);
    }

    function randomIndex() internal returns (uint) {
        uint totalSize = TOKEN_LIMIT - numTokens;
        uint index = random(totalSize);
        uint value = 0;
        if (indices[index] != 0) {
            value = indices[index];
        } else {
            value = index;
        }

        // Move last value to selected position
        if (indices[totalSize - 1] == 0) {
            // Array position not initialized, so use position
            indices[index] = totalSize - 1;
        } else {
            // Array position holds a value so use that
            indices[index] = indices[totalSize - 1];
        }
        // Don\u0027t allow a zero index, start counting at 1
        return value.add(1);
    }

    function getPrice() public view saleStarted returns (uint _price) {
        return price;
    }

    function mintsRemaining() external view returns (uint _remaining) {
        _remaining = TOKEN_LIMIT.sub(numTokens);
    }

    /**
    * @dev Public sale minting.
    */
    function mint(uint numberOfTokens) external payable saleStarted reentrancyGuard {
        require(numberOfTokens \u003c= MAX_REBEL_PURCHASE, \"Can only mint 20 tokens at a time.\");
        require(numberOfTokens \u003e 0, \"Must mint at least 1 rebel.\");
        require(!marketPaused, \"Rebels have paused all operations.\");
        require(numTokens.add(numberOfTokens) \u003c= TOKEN_LIMIT, \"Purchase would exceed max supply of Rebels.\");
        require(price.mul(numberOfTokens) \u003c= msg.value, \"Ether value sent is not correct.\");
        if (msg.value \u003e price.mul(numberOfTokens)) {
            msg.sender.transfer(msg.value.sub(price.mul(numberOfTokens))); //Refund Excess
        }
        beneficiary1.transfer(numberOfTokens.mul(price.mul(70)).div(100));
        beneficiary2.transfer(numberOfTokens.mul(price.mul(30)).div(100));
        for(uint i = 0; i \u003c numberOfTokens; i++) {
            if (numTokens \u003c TOKEN_LIMIT) {
                _mint(msg.sender);
            }
        }
    }

    function _mint(address _to) internal {
        require(_to != address(0), \"Cannot mint to 0x0.\");
        require(numTokens \u003c TOKEN_LIMIT, \"Token limit reached.\");

        uint id = randomIndex();

        numTokens = numTokens + 1;
        _addNFToken(_to, id);
        _heartBeatToken(id);

        emit Mint(id, _to);
        emit Transfer(address(0), _to, id);
    }

    function _addNFToken(address _to, uint256 _tokenId) internal {
        require(idToOwner[_tokenId] == address(0), \"Cannot add, already owned.\");
        idToOwner[_tokenId] = _to;

        ownerToIds[_to].push(_tokenId);
        idToOwnerIndex[_tokenId] = ownerToIds[_to].length.sub(1);
    }

    function _removeNFToken(address _from, uint256 _tokenId) internal {
        require(idToOwner[_tokenId] == _from, \"Incorrect owner.\");
        delete idToOwner[_tokenId];

        uint256 tokenToRemoveIndex = idToOwnerIndex[_tokenId];
        uint256 lastTokenIndex = ownerToIds[_from].length.sub(1);

        if (lastTokenIndex != tokenToRemoveIndex) {
            uint256 lastToken = ownerToIds[_from][lastTokenIndex];
            ownerToIds[_from][tokenToRemoveIndex] = lastToken;
            idToOwnerIndex[lastToken] = tokenToRemoveIndex;
        }

        ownerToIds[_from].pop();
    }

    function _getOwnerNFTCount(address _owner) internal view returns (uint256 _count) {
        _count = ownerToIds[_owner].length;
    }

    function _safeTransferFrom(address _from,  address _to,  uint256 _tokenId,  bytes memory _data) private canTransfer(_tokenId) validNFToken(_tokenId) {
        address tokenOwner = idToOwner[_tokenId];
        require(tokenOwner == _from, \"Incorrect owner.\");
        require(_to != address(0));

        _transfer(_to, _tokenId);

        if (isContract(_to)) {
            bytes4 retval = ERC721TokenReceiver(_to).onERC721Received(msg.sender, _from, _tokenId, _data);
            require(retval == MAGIC_ON_ERC721_RECEIVED);
        }
    }

    function _clearApproval(uint256 _tokenId) private {
        if (idToApproval[_tokenId] != address(0)) {
            delete idToApproval[_tokenId];
        }
    }

    //// Enumerable

    function totalSupply() public view returns (uint256 _total) {
        _total = numTokens;
    }

    function tokenByIndex(uint256 index) public pure returns (uint256 _token) {
        require(index \u003e= 0 \u0026\u0026 index \u003c TOKEN_LIMIT);
        _token = index + 1;
    }

    function tokenOfOwnerByIndex(address _owner, uint256 _index) external view returns (uint256 _token) {
        require(_index \u003c ownerToIds[_owner].length);
        _token = ownerToIds[_owner][_index];
    }

    //// Metadata

    /**
      * @dev Converts a `uint256` to its ASCII `string` representation.
      */
    function toString(uint256 value) internal pure returns (string memory) {
        if (value == 0) {
            return \"0\";
        }
        uint256 temp = value;
        uint256 digits;
        while (temp != 0) {
            digits++;
            temp /= 10;
        }
        bytes memory buffer = new bytes(digits);
        uint256 index = digits - 1;
        temp = value;
        while (temp != 0) {
            buffer[index--] = bytes1(uint8(48 + temp % 10));
            temp /= 10;
        }
        return string(buffer);
    }

    /**
      * @dev Returns a descriptive name for a collection of NFTokens.
      * @return _name Representing name.
      */
    function name() external view returns (string memory _name) {
        _name = nftName;
    }

    /**
     * @dev Returns an abbreviated name for NFTokens.
     * @return _symbol Representing symbol.
     */
    function symbol() external view returns (string memory _symbol) {
        _symbol = nftSymbol;
    }

    /**
     * @dev A distinct URI (RFC 3986) for a given NFT.
     * @param _tokenId Id for which we want uri.
     * @return _tokenURI URI of _tokenId.
     */
    function tokenURI(uint256 _tokenId) external view validNFToken(_tokenId) returns (string memory _tokenURI) {
        _tokenURI = string(abi.encodePacked(baseURI, toString(_tokenId), uriExtension));
    }

    /**
     * @dev For public minting uri will be a localized server, after minting uri will be updated to IFPS and locked. Note: all images are already on IFPS
     * @param _newBaseURI is the new base URI, presumable a new IPFS hash to be updated post minting.
     */
    function updateURI(string memory _newBaseURI, string memory _newUriExtension) external onlyDeployer uriIsUnlocked {
        baseURI = _newBaseURI;
        uriExtension = _newUriExtension;
    }

    /**
    * @dev Locks the URI at it\u0027s current value. Will be called post minting so that the URI perminently points to IPFS without the possibility of changing.
    */
    function lockURI() external onlyDeployer {
        uriLock = true;
    }
}
"},"UniformRandomNumber.sol":{"content":"pragma solidity ^0.7.6;

///**
// * @author Brendan Asselstine
// * @notice A library that uses entropy to select a random number within a bound.  Compensates for modulo bias.
// * @dev Thanks to https://medium.com/hownetworks/dont-waste-cycles-with-modulo-bias-35b6fdafcf94
// */
//library UniformRandomNumber {
//  /// @notice Select a random number without modulo bias using a random seed and upper bound
//  /// @param _entropy The seed for randomness
//  /// @param _upperBound The upper bound of the desired number
//  /// @return A random number less than the _upperBound
//  function uniform(uint256 _entropy, uint256 _upperBound) internal pure returns (uint256) {
//    require(_upperBound \u003e 0, \"UniformRand/min-bound\");
//    uint256 negation = _upperBound \u0026 (~_upperBound + 1);
//    uint256 min = negation % _upperBound;
//    uint256 random = _entropy;
//    while (true) {
//      if (random \u003e= min) {
//        break;
//      }
//      random = uint256(keccak256(abi.encodePacked(random)));
//    }
//    return random % _upperBound;
//  }
//}


library UniformRandomNumber {
  /// @notice Select a random number without modulo bias using a random seed and upper bound
  /// @param _entropy The seed for randomness
  /// @param _upperBound The upper bound of the desired number
  /// @return A random number less than the _upperBound
  function uniform(uint256 _entropy, uint256 _upperBound) internal pure returns (uint256) {
    require(_upperBound \u003e 0, \"UniformRand/min-bound\");
    uint256 min = -_upperBound % _upperBound;
    uint256 random = _entropy;
    while (true) {
      if (random \u003e= min) {
        break;
      }
      random = uint256(keccak256(abi.encodePacked(random)));
    }
    return random % _upperBound;
  }
}

