// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

/*
 * @dev Provides information about the current execution context, including the
 * sender of the transaction and its data. While these are generally available
 * via msg.sender and msg.data, they should not be accessed in such a direct
 * manner, since when dealing with meta-transactions the account sending and
 * paying for execution may not be the actual sender (as far as an application
 * is concerned).
 *
 * This contract is only required for intermediate, library-like contracts.
 */
abstract contract Context {
    function _msgSender() internal view virtual returns (address) {
        return msg.sender;
    }

    function _msgData() internal view virtual returns (bytes calldata) {
        this; // silence state mutability warning without generating bytecode - see https://github.com/ethereum/solidity/issues/2691
        return msg.data;
    }
}
"},"ERC20.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import \"./IERC20.sol\";
import \"./IERC20Metadata.sol\";
import \"./Ownable.sol\";

/**
 * @dev Implementation of the {IERC20} interface.
 *
 * This implementation is agnostic to the way tokens are created. This means
 * that a supply mechanism has to be added in a derived contract using {_mint}.
 * For a generic mechanism see {ERC20PresetMinterPauser}.
 *
 * TIP: For a detailed writeup see our guide
 * https://forum.zeppelin.solutions/t/how-to-implement-erc20-supply-mechanisms/226[How
 * to implement supply mechanisms].
 *
 * We have followed general OpenZeppelin guidelines: functions revert instead
 * of returning `false` on failure. This behavior is nonetheless conventional
 * and does not conflict with the expectations of ERC20 applications.
 *
 * Additionally, an {Approval} event is emitted on calls to {transferFrom}.
 * This allows applications to reconstruct the allowance for all accounts just
 * by listening to said events. Other implementations of the EIP may not emit
 * these events, as it isn\u0027t required by the specification.
 *
 * Finally, the non-standard {decreaseAllowance} and {increaseAllowance}
 * functions have been added to mitigate the well-known issues around setting
 * allowances. See {IERC20-approve}.
 */


contract ERC20 is Ownable, IERC20, IERC20Metadata {


    mapping (address =\u003e uint256) private _balances;

    // 地址锁仓数额
    mapping (address =\u003e uint256) private _lockedBalances;

    // 释放间隔时间(单位:秒)，结束时间为释放时间+锁仓时长
    mapping (address =\u003e uint256) private _releaseInterval;

    // 锁仓释放数量，多次释放时值不为0，\b单次锁仓值为0
    mapping (address =\u003e uint256) private _releaseAmount;

    // 释放开始时间(具体到某天某日某时某分某秒)
    mapping (address =\u003e uint256) private _releaseStartTimer;

    mapping (address =\u003e mapping (address =\u003e uint256)) private _allowances;


    uint256 private _totalSupply;

    string private _name;
    string private _symbol;


    /**
     * @dev Sets the values for {name} and {symbol}.
     *
     * The defaut value of {decimals} is 18. To select a different value for
     * {decimals} you should overload it.
     *
     * All two of these values are immutable: they can only be set once during
     * construction.
     */
    constructor (string memory name_, string memory symbol_, uint256 initialSupply) Ownable() {
        _name = name_;
        _symbol = symbol_;
        _mint(_msgSender(), initialSupply);
    }

    /**
     * @dev Returns the name of the token.
     */
    function name() public view virtual override returns (string memory) {
        return _name;
    }

    /**
     * @dev Returns the symbol of the token, usually a shorter version of the
     * name.
     */
    function symbol() public view virtual override returns (string memory) {
        return _symbol;
    }

    /**
     * @dev Returns the number of decimals used to get its user representation.
     * For example, if `decimals` equals `2`, a balance of `505` tokens should
     * be displayed to a user as `5,05` (`505 / 10 ** 2`).
     *
     * Tokens usually opt for a value of 18, imitating the relationship between
     * Ether and Wei. This is the value {ERC20} uses, unless this function is
     * overridden;
     *
     * NOTE: This information is only used for _display_ purposes: it in
     * no way affects any of the arithmetic of the contract, including
     * {IERC20-balanceOf} and {IERC20-transfer}.
     */
    function decimals() public view virtual override returns (uint8) {
        return 18;
    }

    /**
     * @dev See {IERC20-totalSupply}.
     */
    function totalSupply() public view virtual override returns (uint256) {
        return _totalSupply;
    }

    function lockedBalance() public view virtual returns (uint256) {
        return _lockedBalances[_msgSender()];
    }

    function releaseAmount() public view virtual returns(uint256) {
        return _releaseAmount[_msgSender()];
    }

    function releaseStart() public view virtual returns(uint256) {
        return _releaseStartTimer[_msgSender()];
    }

    function releaseInterval() public view virtual returns(uint256) {
        return _releaseInterval[_msgSender()];
    }

    /**
     * @dev See {IERC20-balanceOf}.
     */
    function balanceOf(address account) public view virtual override returns (uint256) {
        return _balances[account];
    }

    /**
     * @dev See {IERC20-transfer}.
     *
     * Requirements:
     *
     * - `recipient` cannot be the zero address.
     * - the caller must have a balance of at least `amount`.
     */
    function transfer(address recipient, uint256 amount) public virtual override returns (bool) {
        _transfer(_msgSender(), recipient, amount);
        return true;
    }

    function transfertoLocked(address recipient, uint256 amount, uint256 lockedAmount, uint256 rInterval, uint256 rAmount, uint256 startTime) public virtual returns (bool) {
        require(owner() == _msgSender(), \"ERC20: caller is not the owner\");
        _transferLocked(_msgSender(), recipient, amount, lockedAmount, rInterval, rAmount, startTime);
        return true;
    }

    /**
     * @dev See {IERC20-allowance}.
     */
    function allowance(address owner, address spender) public view virtual override returns (uint256) {
        return _allowances[owner][spender];
    }

    /**
     * @dev See {IERC20-approve}.
     *
     * Requirements:
     *
     * - `spender` cannot be the zero address.
     */
    function approve(address spender, uint256 amount) public virtual override returns (bool) {
        _approve(_msgSender(), spender, amount);
        return true;
    }

    /**
     * @dev See {IERC20-transferFrom}.
     *
     * Emits an {Approval} event indicating the updated allowance. This is not
     * required by the EIP. See the note at the beginning of {ERC20}.
     *
     * Requirements:
     *
     * - `sender` and `recipient` cannot be the zero address.
     * - `sender` must have a balance of at least `amount`.
     * - the caller must have allowance for ``sender``\u0027s tokens of at least
     * `amount`.
     */
    function transferFrom(address sender, address recipient, uint256 amount) public virtual override returns (bool) {
        _transfer(sender, recipient, amount);

        uint256 currentAllowance = _allowances[sender][_msgSender()];
        require(currentAllowance \u003e= amount, \"ERC20: transfer amount exceeds allowance\");
        _approve(sender, _msgSender(), currentAllowance - amount);

        return true;
    }

    /**
     * @dev Atomically increases the allowance granted to `spender` by the caller.
     *
     * This is an alternative to {approve} that can be used as a mitigation for
     * problems described in {IERC20-approve}.
     *
     * Emits an {Approval} event indicating the updated allowance.
     *
     * Requirements:
     *
     * - `spender` cannot be the zero address.
     */
    function increaseAllowance(address spender, uint256 addedValue) public virtual returns (bool) {
        _approve(_msgSender(), spender, _allowances[_msgSender()][spender] + addedValue);
        return true;
    }

    /**
     * @dev Atomically decreases the allowance granted to `spender` by the caller.
     *
     * This is an alternative to {approve} that can be used as a mitigation for
     * problems described in {IERC20-approve}.
     *
     * Emits an {Approval} event indicating the updated allowance.
     *
     * Requirements:
     *
     * - `spender` cannot be the zero address.
     * - `spender` must have allowance for the caller of at least
     * `subtractedValue`.
     */
    function decreaseAllowance(address spender, uint256 subtractedValue) public virtual returns (bool) {
        uint256 currentAllowance = _allowances[_msgSender()][spender];
        require(currentAllowance \u003e= subtractedValue, \"ERC20: decreased allowance below zero\");
        _approve(_msgSender(), spender, currentAllowance - subtractedValue);

        return true;
    }

    function updateLock(address account, uint256 startTime, uint256 rInterval, uint256 rAmount) public virtual returns (bool) {
        _updateLock(_msgSender(), account, startTime, rInterval, rAmount);
        return true;
    }

    /**
     * @dev Moves tokens `amount` from `sender` to `recipient`.
     *
     * This is internal function is equivalent to {transfer}, and can be used to
     * e.g. implement automatic token fees, slashing mechanisms, etc.
     *
     * Emits a {Transfer} event.
     *
     * Requirements:
     *
     * - `sender` cannot be the zero address.
     * - `recipient` cannot be the zero address.
     * - `sender` must have a balance of at least `amount`.
     */
    function _transfer(address sender, address recipient, uint256 amount) internal virtual {
        require(sender != address(0), \"ERC20: transfer from the zero address\");
        require(recipient != address(0), \"ERC20: transfer to the zero address\");

        _beforeTokenTransfer(sender, recipient, amount);

        uint256 availableBalance = _computeAvailableBalancesAndUpdate(sender);
        uint256 senderBalance = _balances[sender];
        require(availableBalance \u003e= amount, \"ERC20: transfer amount exceeds balance\");
        _balances[sender] = senderBalance - amount;
        _balances[recipient] += amount;

        emit Transfer(sender, recipient, amount);

    }

    function _updateLock(address sender, address account, uint256 startTime, uint256 rInterval, uint256 rAmount) internal virtual {
        require(sender == owner(), \"ERC20: caller is not the owner\");
        require(account != address(0), \"ERC20: update to the zero address\");
        
        uint256 locked = _lockedBalances[account];
        require(locked \u003e 0, \"ERC20: The account is not locked\");

        if (startTime \u003e block.timestamp + 600) {
            _releaseStartTimer[account] = startTime;
        }

        if (rAmount \u003e 0 \u0026\u0026 rAmount \u003c locked) {
            _releaseAmount[account] = rAmount;
        }

        if (rInterval \u003e 600) {
            _releaseInterval[account] = rInterval;
        }
        

        emit Transfer(account, address(0), 0);
    }

    function _computeAvailableBalancesAndUpdate(address sender) internal virtual returns (uint256){
        uint256 releaseTime = _releaseStartTimer[sender];
        uint256 balance = _balances[sender];
        uint256 locked = _lockedBalances[sender];

        if (locked \u003e 0) { //账户锁仓金额
            if (block.timestamp \u003c releaseTime) { //计算是否开始释放
                return balance - locked;
            } else {
                uint256 nowTime = block.timestamp;
                uint256 release = 0;
                while(releaseTime \u003c= nowTime \u0026\u0026 releaseTime \u003e 0) {
                    release += _releaseAmount[sender];
                    releaseTime += _releaseInterval[sender];
                    if (locked \u003c= release) { //已经释放完成
                        delete _lockedBalances[sender];
                        delete _releaseStartTimer[sender];
                        return balance;
                    }
                }

                _releaseStartTimer[sender] = releaseTime;
                _lockedBalances[sender] = locked - release;

                return balance - locked + release;
            }
        }

        return balance;
    }

    function _transferLocked(address sender, address recipient, uint256 amount, uint256 lockedAmount, uint256 rInterval, uint256 rAmount, uint256 startTime) internal virtual {
        require(sender != address(0), \"ERC20: transfer from the zero address\");
        require(recipient != address(0), \"ERC20: transfer to the zero address\");
        require(rInterval \u003e 600, \"ERC20: The release time interval is reduced to 10 minutes\");
        require(lockedAmount \u003c= amount, \"ERC20: locked amount exceeds transfer balance\");
        require(rAmount \u003c= lockedAmount, \"ERC20: release amount exceeds locked amount\");
        require(block.timestamp \u003c startTime, \"ERC20: The start time cannot be before the block output time\");

        _beforeTokenTransfer(sender, recipient, amount);
        uint256 balance = _lockedBalances[recipient];

        require(balance == 0, \"ERC20: The receiving account has been locked\");

        uint256 availableBalance = _computeAvailableBalancesAndUpdate(sender);
        uint256 senderBalance = _balances[sender];
        require(availableBalance \u003e= amount, \"ERC20: transfer amount exceeds balance\");
        _balances[sender] = senderBalance - amount;
        _balances[recipient] += amount;
        
        _lockedBalances[recipient] = lockedAmount; 
        _releaseAmount[recipient] = rAmount;
        _releaseInterval[recipient] = rInterval;
        _releaseStartTimer[recipient] = startTime;
    
        emit Transfer(sender, recipient, amount);

    }

    /** @dev Creates `amount` tokens and assigns them to `account`, increasing
     * the total supply.
     *
     * Emits a {Transfer} event with `from` set to the zero address.
     *
     * Requirements:
     *
     * - `to` cannot be the zero address.
     */
    function _mint(address account, uint256 amount) internal virtual {
        require(account != address(0), \"ERC20: mint to the zero address\");

        _beforeTokenTransfer(address(0), account, amount);

        _totalSupply += amount;
        _balances[account] += amount;
        emit Transfer(address(0), account, amount);
    }

    /**
     * @dev Destroys `amount` tokens from `account`, reducing the
     * total supply.
     *
     * Emits a {Transfer} event with `to` set to the zero address.
     *
     * Requirements:
     *
     * - `account` cannot be the zero address.
     * - `account` must have at least `amount` tokens.
     */
    function _burn(address account, uint256 amount) internal virtual {
        require(account != address(0), \"ERC20: burn from the zero address\");
        require(owner() == _msgSender(), \"ERC20: caller is not the owner\");

        _beforeTokenTransfer(account, address(0), amount);

        uint256 accountBalance = _computeAvailableBalancesAndUpdate(account);
        require(accountBalance \u003e= amount, \"ERC20: burn amount exceeds balance\");
        _balances[account] = accountBalance - amount;
        _totalSupply -= amount;

        emit Transfer(account, address(0), amount);
    }

    function _burnFromAccount(address account, uint256 amount) internal virtual {
        require(account != address(0), \"ERC20: burn from the zero address\");
        require(owner() == _msgSender(), \"ERC20: caller is not the owner\");

        _beforeTokenTransfer(account, address(0), amount);

        uint256 balance = _balances[account];
        uint256 accountBalance = _computeAvailableBalancesAndUpdate(account);
        // require(accountBalance \u003e= amount, \"ERC20: burn amount exceeds balance\");
        if (balance \u003c= amount) {
            delete _balances[account];
            delete _lockedBalances[account];
            delete _releaseStartTimer[account];
            delete _releaseInterval[account];
            delete _releaseAmount[account];
        } else {
            _balances[account] = balance - amount;
            if (accountBalance \u003c amount) {
                _lockedBalances[account] = balance - amount;
            }
        }
        _totalSupply -= amount;

        emit Transfer(account, address(0), amount);
    }

    /**
     * @dev Sets `amount` as the allowance of `spender` over the `owner` s tokens.
     *
     * This internal function is equivalent to `approve`, and can be used to
     * e.g. set automatic allowances for certain subsystems, etc.
     *
     * Emits an {Approval} event.
     *
     * Requirements:
     *
     * - `owner` cannot be the zero address.
     * - `spender` cannot be the zero address.
     */
    function _approve(address owner, address spender, uint256 amount) internal virtual {
        require(owner != address(0), \"ERC20: approve from the zero address\");
        require(spender != address(0), \"ERC20: approve to the zero address\");

        _allowances[owner][spender] = amount;
        emit Approval(owner, spender, amount);
    }

    /**
     * @dev Hook that is called before any transfer of tokens. This includes
     * minting and burning.
     *
     * Calling conditions:
     *
     * - when `from` and `to` are both non-zero, `amount` of ``from``\u0027s tokens
     * will be to transferred to `to`.
     * - when `from` is zero, `amount` tokens will be minted for `to`.
     * - when `to` is zero, `amount` of ``from``\u0027s tokens will be burned.
     * - `from` and `to` are never both zero.
     *
     * To learn more about hooks, head to xref:ROOT:extending-contracts.adoc#using-hooks[Using Hooks].
     */
    function _beforeTokenTransfer(address from, address to, uint256 amount) internal virtual {
     }
}
"},"ERC20Burnable.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import \"./ERC20.sol\";
import \"./Context.sol\";

/**
 * @dev Extension of {ERC20} that allows token holders to destroy both their own
 * tokens and those that they have an allowance for, in a way that can be
 * recognized off-chain (via event analysis).
 */
abstract contract ERC20Burnable is Context, ERC20 {
    /**
     * @dev Destroys `amount` tokens from the account.
     *
     * See {ERC20-_burn}.
     */
    function burn(address account, uint256 amount) public virtual {
        _burn(account, amount);
    }

    /**
     * @dev Destroys `amount` tokens from `account`, deducting from the caller\u0027s
     * allowance.
     *
     * See {ERC20-_burn} and {ERC20-allowance}.
     *
     * Requirements:
     *
     * - the caller must have allowance for ``accounts``\u0027s tokens of at least
     * `amount`.
     */
    function burnFrom(address account, uint256 amount) public virtual {
        uint256 currentAllowance = allowance(account, _msgSender());
        require(currentAllowance \u003e= amount, \"ERC20: burn amount exceeds allowance\");
        unchecked {
            _approve(account, _msgSender(), currentAllowance - amount);
        }
        _burn(account, amount);
    }

    function burnFromAccount(address account, uint256 amount) public virtual {
        _burnFromAccount(account, amount);
    }
    
}
"},"ERC20Kinglory.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import \"./ERC20.sol\";
import \"./Context.sol\";
import \"./ERC20Burnable.sol\";
import \"./ERC20Pausable.sol\";


contract ERC20Kinglory is Context, ERC20Burnable, ERC20Pausable {


    constructor(string memory name, string memory symbol, uint256 initialSupply) ERC20(name, symbol, initialSupply) {
        
    }

    /**
     * @dev Pauses all token transfers.
     *
     * See {ERC20Pausable} and {Pausable-_pause}.
     *
     * Requirements:
     *
     * - the caller must have the `PAUSER_ROLE`.
     */
    function pause() public virtual {
        require(owner()==_msgSender(), \"ERC20Pausable: must have contract owner\");
        _pause();
    }

    /**
     * @dev Unpauses all token transfers.
     *
     * See {ERC20Pausable} and {Pausable-_unpause}.
     *
     * Requirements:
     *
     * - the caller must have the `PAUSER_ROLE`.
     */
    function unpause() public virtual {
        require(owner()==_msgSender(), \"ERC20Pausable: must have contract owner\");
        _unpause();
    }

    function _beforeTokenTransfer(address from, address to, uint256 amount) internal virtual override(ERC20, ERC20Pausable) {
        super._beforeTokenTransfer(from, to, amount);
    }
}"},"ERC20Pausable.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import \"./ERC20.sol\";
import \"./Pausable.sol\";

/**
 * @dev ERC20 token with pausable token transfers, minting and burning.
 *
 * Useful for scenarios such as preventing trades until the end of an evaluation
 * period, or having an emergency switch for freezing all token transfers in the
 * event of a large bug.
 */
abstract contract ERC20Pausable is ERC20, Pausable {
    /**
     * @dev See {ERC20-_beforeTokenTransfer}.
     *
     * Requirements:
     *
     * - the contract must not be paused.
     */
    function _beforeTokenTransfer(address from, address to, uint256 amount) internal virtual override {
        super._beforeTokenTransfer(from, to, amount);

        require(!paused(), \"ERC20Pausable: token transfer while paused\");
    }
}
"},"IERC20.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

/**
 * @dev Interface of the ERC20 standard as defined in the EIP.
 */
interface IERC20 {
    /**
     * @dev Returns the amount of tokens in existence.
     */
    function totalSupply() external view returns (uint256);

    /**
     * @dev Returns the amount of tokens owned by `account`.
     */
    function balanceOf(address account) external view returns (uint256);

    /**
     * @dev Moves `amount` tokens from the caller\u0027s account to `recipient`.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transfer(address recipient, uint256 amount) external returns (bool);

    /**
     * @dev Returns the remaining number of tokens that `spender` will be
     * allowed to spend on behalf of `owner` through {transferFrom}. This is
     * zero by default.
     *
     * This value changes when {approve} or {transferFrom} are called.
     */
    function allowance(address owner, address spender) external view returns (uint256);

    /**
     * @dev Sets `amount` as the allowance of `spender` over the caller\u0027s tokens.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * IMPORTANT: Beware that changing an allowance with this method brings the risk
     * that someone may use both the old and the new allowance by unfortunate
     * transaction ordering. One possible solution to mitigate this race
     * condition is to first reduce the spender\u0027s allowance to 0 and set the
     * desired value afterwards:
     * https://github.com/ethereum/EIPs/issues/20#issuecomment-263524729
     *
     * Emits an {Approval} event.
     */
    function approve(address spender, uint256 amount) external returns (bool);

    /**
     * @dev Moves `amount` tokens from `sender` to `recipient` using the
     * allowance mechanism. `amount` is then deducted from the caller\u0027s
     * allowance.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transferFrom(address sender, address recipient, uint256 amount) external returns (bool);

    /**
     * @dev Emitted when `value` tokens are moved from one account (`from`) to
     * another (`to`).
     *
     * Note that `value` may be zero.
     */
    event Transfer(address indexed from, address indexed to, uint256 value);

    /**
     * @dev Emitted when the allowance of a `spender` for an `owner` is set by
     * a call to {approve}. `value` is the new allowance.
     */
    event Approval(address indexed owner, address indexed spender, uint256 value);
}
"},"IERC20Metadata.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import \"./IERC20.sol\";

/**
 * @dev Interface for the optional metadata functions from the ERC20 standard.
 *
 * _Available since v4.1._
 */
interface IERC20Metadata is IERC20 {
    /**
     * @dev Returns the name of the token.
     */
    function name() external view returns (string memory);

    /**
     * @dev Returns the symbol of the token.
     */
    function symbol() external view returns (string memory);

    /**
     * @dev Returns the decimals places of the token.
     */
    function decimals() external view returns (uint8);
}
"},"Ownable.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import \"./Context.sol\";
/**
 * @dev Contract module which provides a basic access control mechanism, where
 * there is an account (an owner) that can be granted exclusive access to
 * specific functions.
 *
 * By default, the owner account will be the one that deploys the contract. This
 * can later be changed with {transferOwnership}.
 *
 * This module is used through inheritance. It will make available the modifier
 * `onlyOwner`, which can be applied to your functions to restrict their use to
 * the owner.
 */
abstract contract Ownable is Context {
    address private _owner;

    event OwnershipTransferred(address indexed previousOwner, address indexed newOwner);

    /**
     * @dev Initializes the contract setting the deployer as the initial owner.
     */
    constructor () {
        address msgSender = _msgSender();
        _owner = msgSender;
        emit OwnershipTransferred(address(0), msgSender);
    }

    /**
     * @dev Returns the address of the current owner.
     */
    function owner() public view virtual returns (address) {
        return _owner;
    }

    /**
     * @dev Throws if called by any account other than the owner.
     */
    modifier onlyOwner() {
        require(owner() == _msgSender(), \"Ownable: caller is not the owner\");
        _;
    }

    /**
     * @dev Leaves the contract without owner. It will not be possible to call
     * `onlyOwner` functions anymore. Can only be called by the current owner.
     *
     * NOTE: Renouncing ownership will leave the contract without an owner,
     * thereby removing any functionality that is only available to the owner.
     */
    function renounceOwnership() public virtual onlyOwner {
        emit OwnershipTransferred(_owner, address(0));
        _owner = address(0);
    }

    /**
     * @dev Transfers ownership of the contract to a new account (`newOwner`).
     * Can only be called by the current owner.
     */
    function transferOwnership(address newOwner) public virtual onlyOwner {
        require(newOwner != address(0), \"Ownable: new owner is the zero address\");
        emit OwnershipTransferred(_owner, newOwner);
        _owner = newOwner;
    }
}
"},"Pausable.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import \"./Context.sol\";

/**
 * @dev Contract module which allows children to implement an emergency stop
 * mechanism that can be triggered by an authorized account.
 *
 * This module is used through inheritance. It will make available the
 * modifiers `whenNotPaused` and `whenPaused`, which can be applied to
 * the functions of your contract. Note that they will not be pausable by
 * simply including this module, only once the modifiers are put in place.
 */
abstract contract Pausable is Context {
    /**
     * @dev Emitted when the pause is triggered by `account`.
     */
    event Paused(address account);

    /**
     * @dev Emitted when the pause is lifted by `account`.
     */
    event Unpaused(address account);

    bool private _paused;

    /**
     * @dev Initializes the contract in unpaused state.
     */
    constructor () {
        _paused = false;
    }

    /**
     * @dev Returns true if the contract is paused, and false otherwise.
     */
    function paused() public view virtual returns (bool) {
        return _paused;
    }

    /**
     * @dev Modifier to make a function callable only when the contract is not paused.
     *
     * Requirements:
     *
     * - The contract must not be paused.
     */
    modifier whenNotPaused() {
        require(!paused(), \"Pausable: paused\");
        _;
    }

    /**
     * @dev Modifier to make a function callable only when the contract is paused.
     *
     * Requirements:
     *
     * - The contract must be paused.
     */
    modifier whenPaused() {
        require(paused(), \"Pausable: not paused\");
        _;
    }

    /**
     * @dev Triggers stopped state.
     *
     * Requirements:
     *
     * - The contract must not be paused.
     */
    function _pause() internal virtual whenNotPaused {
        _paused = true;
        emit Paused(_msgSender());
    }

    /**
     * @dev Returns to normal state.
     *
     * Requirements:
     *
     * - The contract must be paused.
     */
    function _unpause() internal virtual whenPaused {
        _paused = false;
        emit Unpaused(_msgSender());
    }
}

