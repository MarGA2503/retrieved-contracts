// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

/**
 * @dev Provides information about the current execution context, including the
 * sender of the transaction and its data. While these are generally available
 * via msg.sender and msg.data, they should not be accessed in such a direct
 * manner, since when dealing with meta-transactions the account sending and
 * paying for execution may not be the actual sender (as far as an application
 * is concerned).
 *
 * This contract is only required for intermediate, library-like contracts.
 */
abstract contract Context {
    function _msgSender() internal view virtual returns (address) {
        return msg.sender;
    }

    function _msgData() internal view virtual returns (bytes calldata) {
        return msg.data;
    }
}
"},"ILandCollection.sol":{"content":"// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;


interface ILandCollection {
  function totalMinted(uint256 groupId) external view returns (uint256);
  function maximumSupply(uint256 groupId) external view returns (uint256);
  function mintToken(address account, uint256 groupId, uint256 count, uint256 seed) external;
  function balanceOf(address owner) external view returns (uint256);
  function tokenOfOwnerByIndex(address owner, uint256 index) external view returns (uint256);
  function ownerOf(uint256 tokenId) external view returns (address);
}
"},"IOreClaim.sol":{"content":"// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;


interface IOreClaim {
  function lastClaimedWeekByTokenId(uint256 _tokenId) external view returns (uint256);
  function initialClaimTimestampByGroupId(uint256 _groupId) external view returns (uint256);
  function finalClaimTimestamp() external view returns (uint256);
}
"},"OreClaimHelper.sol":{"content":"// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import \"./Ownable.sol\";

import \"./ILandCollection.sol\";
import \"./IOreClaim.sol\";


// Contains getter methods used by the webapp for ore claiming
contract OreClaimHelper is Ownable {
  // Collection token contract interface
  ILandCollection public collection;
  // Ore token contract interface
  IOreClaim public oreClaim;

  constructor(address _oreClaim, address _collection) {
    oreClaim = IOreClaim(_oreClaim);
    collection = ILandCollection(_collection);
  }

  function claimWeek(uint256 _groupId) public view returns (uint256) {
    // Calculate and return the number of weeks elapsed since the initial claim timestamp for the groupId 
    uint256 initial = oreClaim.initialClaimTimestampByGroupId(_groupId);
    // In the case of the claiming being paused for the current contract due to contract upgrade
    // make sure that the maximum claimable week is within the set final timestamp
    uint256 finalClaimTimestamp = oreClaim.finalClaimTimestamp();
    uint256 timestamp = (finalClaimTimestamp \u003e 0 \u0026\u0026 block.timestamp \u003e finalClaimTimestamp ? finalClaimTimestamp : block.timestamp);

    if (initial == 0 || timestamp \u003c initial) {
      return 0;
    }

    uint256 elapsed = timestamp - initial;
    return (elapsed / 60 / 60 / 24 / 7) + 1;
  }

  // Returns the list of tokenIds (with elapsed weeks for each) eligible for claiming owned by the specified address
  function unclaimedTokenIds(address _address) external view returns (uint256[] memory, uint256[] memory) {
    uint256 owned = collection.balanceOf(_address);
    uint256 count = 0;

    // Count the total number of eligible tokens
    for (uint256 i = 0; i \u003c owned; i++) {
      uint256 tokenId = collection.tokenOfOwnerByIndex(_address, i);
      uint256 groupId = tokenId / 100000;
      
      uint256 currentWeek = claimWeek(groupId);
      uint256 lastClaimedWeek = oreClaim.lastClaimedWeekByTokenId(tokenId);

      if (currentWeek \u003e lastClaimedWeek) {
        count++;
      }
    }

    // Fill the array to be returned containing the eligible tokenIds along with the elapsed weeks
    uint256[] memory tokenIds = new uint256[](count);
    uint256[] memory elapsedWeeks = new uint256[](count);
    uint256 j = 0;
    for (uint256 i = 0; i \u003c owned; i++) {
      uint256 tokenId = collection.tokenOfOwnerByIndex(_address, i);
      uint256 groupId = tokenId / 100000;
      uint256 currentWeek = claimWeek(groupId);
      uint256 lastClaimedWeek = oreClaim.lastClaimedWeekByTokenId(tokenId);

      if (currentWeek \u003e lastClaimedWeek) {
        tokenIds[j] = tokenId;
        elapsedWeeks[j++] = currentWeek - lastClaimedWeek;
      }
    }

    return (tokenIds, elapsedWeeks);
  }
}
"},"Ownable.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import \"./Context.sol\";

/**
 * @dev Contract module which provides a basic access control mechanism, where
 * there is an account (an owner) that can be granted exclusive access to
 * specific functions.
 *
 * By default, the owner account will be the one that deploys the contract. This
 * can later be changed with {transferOwnership}.
 *
 * This module is used through inheritance. It will make available the modifier
 * `onlyOwner`, which can be applied to your functions to restrict their use to
 * the owner.
 */
abstract contract Ownable is Context {
    address private _owner;

    event OwnershipTransferred(address indexed previousOwner, address indexed newOwner);

    /**
     * @dev Initializes the contract setting the deployer as the initial owner.
     */
    constructor() {
        _setOwner(_msgSender());
    }

    /**
     * @dev Returns the address of the current owner.
     */
    function owner() public view virtual returns (address) {
        return _owner;
    }

    /**
     * @dev Throws if called by any account other than the owner.
     */
    modifier onlyOwner() {
        require(owner() == _msgSender(), \"Ownable: caller is not the owner\");
        _;
    }

    /**
     * @dev Leaves the contract without owner. It will not be possible to call
     * `onlyOwner` functions anymore. Can only be called by the current owner.
     *
     * NOTE: Renouncing ownership will leave the contract without an owner,
     * thereby removing any functionality that is only available to the owner.
     */
    function renounceOwnership() public virtual onlyOwner {
        _setOwner(address(0));
    }

    /**
     * @dev Transfers ownership of the contract to a new account (`newOwner`).
     * Can only be called by the current owner.
     */
    function transferOwnership(address newOwner) public virtual onlyOwner {
        require(newOwner != address(0), \"Ownable: new owner is the zero address\");
        _setOwner(newOwner);
    }

    function _setOwner(address newOwner) private {
        address oldOwner = _owner;
        _owner = newOwner;
        emit OwnershipTransferred(oldOwner, newOwner);
    }
}

