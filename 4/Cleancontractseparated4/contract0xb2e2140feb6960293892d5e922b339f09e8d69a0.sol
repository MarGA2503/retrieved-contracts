// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

/*
 * @dev Provides information about the current execution context, including the
 * sender of the transaction and its data. While these are generally available
 * via msg.sender and msg.data, they should not be accessed in such a direct
 * manner, since when dealing with meta-transactions the account sending and
 * paying for execution may not be the actual sender (as far as an application
 * is concerned).
 *
 * This contract is only required for intermediate, library-like contracts.
 */
abstract contract Context {
    function _msgSender() internal view virtual returns (address) {
        return msg.sender;
    }

    function _msgData() internal view virtual returns (bytes calldata) {
        this; // silence state mutability warning without generating bytecode - see https://github.com/ethereum/solidity/issues/2691
        return msg.data;
    }
}"},"HashSale.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import \"./Ownable.sol\";
import \"./TransferHelper.sol\";

contract HashSale is Ownable {
\taddress public hashTokenAddress;
\taddress payable paymentAddress;
\taddress kycSigner;
\tuint public endTime;
\tuint public stageAmount;
\tuint public minPayAmount;
\tuint public tokensSold = 0;
\tuint public tokensWithdrawn = 0;
\tuint public discountThreshold;
\tuint8 public discount = 25;
\tuint8 public currentStage = 0;
\tbool isFinished = false;
\tuint public constant HASH_TOKEN_DECIMALS = 18;
\tstring public constant AGREEMENT = \"I confirm I am not a citizen, national, resident (tax or otherwise) or holder of a green card of the USA and have never been a citizen, national, resident (tax or otherwise) or holder of a green card of the USA in the past.\";
\tstring constant AGREEMENT_LENGTH = \"223\";

    struct Stage {
        uint tokenPrice;
        uint tokensSold;
    }
\tstruct Sale {
\t    address customerAddress;
\t\tuint payAmount;
\t\tuint tokenAmount;
\t\tbytes agreementSignature;
\t\tuint16 referral;
\t\tbool tokensWithdrawn;
\t}

\tStage[] public stages;
\tSale[] public sales;

\tevent NewSale(uint indexed saleId, address indexed customerAddress, uint16 indexed referral, uint payAmount, uint tokenAmount, uint8 stage, uint stageSales, bool isSaleFinished);
\tevent TokensWithdrawn(address indexed customerAddress, uint tokenAmount);
\tevent StageSwitch(uint8 previousStage, uint8 newStage, bool isSaleFinished);

\tconstructor(
\t    address _hashTokenAddress, address payable _paymentAddress, address _kycSigner, uint _endTime,
\t    uint _stageAmount, uint _minPayAmount, uint _discountThreshold,
\t    uint _tokenPrice1, uint _tokenPrice2, uint _tokenPrice3, uint _tokenPrice4, uint _tokenPrice5
    ) {
        hashTokenAddress = _hashTokenAddress;
        paymentAddress = _paymentAddress;
        endTime = _endTime;
        stageAmount = _stageAmount * (10 ** HASH_TOKEN_DECIMALS);
        minPayAmount = _minPayAmount;
        kycSigner = _kycSigner;
        discountThreshold = _discountThreshold;
        stages.push(Stage(_tokenPrice1, 0));
        stages.push(Stage(_tokenPrice2, 0));
\t\tstages.push(Stage(_tokenPrice3, 0));
\t\tstages.push(Stage(_tokenPrice4, 0));
\t\tstages.push(Stage(_tokenPrice5, 0));
    }

    function changeKycSigner(address _kycSigner) public onlyOwner {
        require(_kycSigner != address(0), \"Incorrect address\");
        kycSigner = _kycSigner;
    }

    function changeMinPayAmount(uint _minPayAmount) public onlyOwner {
        minPayAmount = _minPayAmount;
    }

    function changeDiscount(uint8 _discount) public onlyOwner {
        discount = _discount;
    }

    function changeDiscountThreshold(uint _discountThreshold) public onlyOwner {
        discountThreshold = _discountThreshold;
    }

    function switchStage(uint8 _stage) public onlyOwner {
        require(!isSaleFinished(), \"The sale is over\");
        uint8 previousStage = currentStage;
        _switchStage(_stage);
        emit StageSwitch(previousStage, currentStage, isFinished);
    }

    function withdrawRemainingTokens(uint _tokenAmount) public onlyOwner {
        require(_tokenAmount \u003e 0, \"Nothing to withdraw\");
        require(_tokenAmount \u003c= _getHashBalance(), \"Not enough HASH tokens to withdraw\");
        TransferHelper.safeTransfer(hashTokenAddress, msg.sender, _tokenAmount);
    }

\tfunction buyTokens(bytes calldata _agreementSignature, uint16 _referral) payable public {
\t    require(!isSaleFinished(), \"The sale is over\");
\t    require(msg.value \u003e= minPayAmount, \"Amount must be greater than the minimal value\");
\t\trequire (_verifySign(_agreementSignature, msg.sender), \"Incorrect agreement signature\");
\t\tuint etherAmount = msg.value;
\t\tif (etherAmount \u003e= discountThreshold) {
\t\t    etherAmount = etherAmount * 100 / (100 - discount);
\t\t}
\t\tuint tokenAmount = 0;
\t\tfor (uint8 i = currentStage; i \u003c stages.length \u0026\u0026 etherAmount \u003e 0; i++) {
\t\t    uint buyAmount = _calculateTokenAmount(etherAmount, i);
\t\t    uint thisStageAmount = stageAmount - stages[i].tokensSold;
\t\t    if (buyAmount \u003e= thisStageAmount) {
\t\t        tokenAmount += thisStageAmount;
\t\t        etherAmount -= _calculateEtherAmount(thisStageAmount, i);
\t\t        stages[i].tokensSold = stageAmount;
\t\t        _switchStage(currentStage + 1);
\t\t    } else {
\t\t        tokenAmount += buyAmount;
\t\t        etherAmount = 0;
\t\t        stages[i].tokensSold += buyAmount;
\t\t    }
\t\t}
\t\trequire(etherAmount == 0, \"Not enough HASH tokens to buy\");
\t\trequire(tokenAmount \u003e 0, \"Amount must be greater than 0\");
\t\ttokensSold += tokenAmount;
\t\tpaymentAddress.transfer(msg.value);
\t\tuint saleId = sales.length;
\t\tsales.push(Sale(msg.sender, msg.value, tokenAmount, _agreementSignature, _referral, false));
\t\temit NewSale(saleId, msg.sender, _referral, msg.value, tokenAmount, currentStage, stages[currentStage].tokensSold, isFinished);
\t}

\tfunction getUnwithdrawnTokenAmount(address _customerAddress) public view returns (uint tokenAmount) {
\t    tokenAmount = 0;
\t    for (uint i = 0; i \u003c sales.length; i++) {
\t\t\tif (sales[i].customerAddress == _customerAddress \u0026\u0026 sales[i].tokensWithdrawn == false) {
\t\t\t\ttokenAmount += sales[i].tokenAmount;
\t\t\t}
\t\t}
\t}

\tfunction withdrawTokens(bytes calldata _kycSignature) public {
\t    require(isSaleFinished(), \"The withdrawal of HASH tokens is not yet available\");
\t    require(_verifyKycSign(_kycSignature, msg.sender), \"Incorrect KYC signature\");
\t    uint tokenAmount = 0;
\t    for (uint i = 0; i \u003c sales.length; i++) {
\t\t\tif (sales[i].customerAddress == msg.sender \u0026\u0026 sales[i].tokensWithdrawn == false) {
\t\t\t\ttokenAmount += sales[i].tokenAmount;
\t\t\t\tsales[i].tokensWithdrawn = true;
\t\t\t}
\t\t}
\t\trequire(tokenAmount \u003e 0, \"You have nothing to withdraw\");
\t\ttokensWithdrawn += tokenAmount;
        TransferHelper.safeTransfer(hashTokenAddress, msg.sender, tokenAmount);
\t\temit TokensWithdrawn(msg.sender, tokenAmount);
\t}

\tfunction isSaleFinished() public view returns (bool) {
\t    return isFinished || block.timestamp \u003e endTime;
\t}

\tfunction calculateTokenAmount(uint etherAmount) public view returns(uint tokenAmount) {
\t    require(!isSaleFinished(), \"The sale is over\");
\t    require(etherAmount \u003e= minPayAmount, \"Amount must be greater than the minimal value\");
\t    if (etherAmount \u003e= discountThreshold) {
            etherAmount = etherAmount * 100 / (100 - discount);
        }
\t    for (uint8 i = currentStage; i \u003c stages.length \u0026\u0026 etherAmount \u003e 0; i++) {
\t\t    uint buyAmount = _calculateTokenAmount(etherAmount, i);
\t\t    uint thisStageAmount = stageAmount - stages[i].tokensSold;
\t\t    if (buyAmount \u003e= thisStageAmount) {
\t\t        tokenAmount += thisStageAmount;
\t\t        etherAmount -= _calculateEtherAmount(thisStageAmount, i);
\t\t    } else {
\t\t        tokenAmount += buyAmount;
\t\t        etherAmount = 0;
\t\t    }
\t\t}
\t\trequire(etherAmount == 0, \"Not enough HASH tokens to buy\");
\t}

\tfunction getMinAndMaxPayAmounts() public view returns(uint, uint) {
\t    uint maxPayAmount = 0;
\t    for (uint8 i = currentStage; i \u003c stages.length \u0026\u0026 !isSaleFinished(); i++) {
\t\t    uint thisStageAmount = stageAmount - stages[i].tokensSold;
\t\t    maxPayAmount += _calculateEtherAmount(thisStageAmount, i);
\t\t}
\t\tif (maxPayAmount \u003e= discountThreshold) {
\t\t    uint maxPayAmountWithDiscount = maxPayAmount * (100 - discount) / 100;
\t\t    if (maxPayAmountWithDiscount \u003e= discountThreshold) {
\t\t        maxPayAmount = maxPayAmountWithDiscount;
\t\t    } else {
\t\t        maxPayAmount = discountThreshold - 1;
\t\t    }
\t\t}
\t\treturn (minPayAmount, maxPayAmount);
\t}

\tfunction _switchStage(uint8 _stage) private {
\t    require(_stage \u003e currentStage, \"The next stage value must be more than the current one\");
\t    if (_stage \u003e= stages.length) {
\t        isFinished = true;
        } else {
            currentStage = _stage;
        }
\t}

\tfunction _calculateTokenAmount(uint etherAmount, uint8 stage) private view returns(uint tokenAmount) {
\t    tokenAmount = etherAmount * (10 ** HASH_TOKEN_DECIMALS) / stages[stage].tokenPrice;
\t}

\tfunction _calculateEtherAmount(uint tokenAmount, uint8 stage) private view returns(uint etherAmount) {
\t    etherAmount = tokenAmount * stages[stage].tokenPrice / (10 ** HASH_TOKEN_DECIMALS);
\t}

    function _getHashBalance() private returns(uint balance) {
        (bool success, bytes memory data) = hashTokenAddress.call(
            abi.encodeWithSelector(bytes4(keccak256(bytes(\u0027balanceOf(address)\u0027))), address(this))
        );
        require(success, \"Getting HASH balance failed\");
        balance = abi.decode(data, (uint));
    }

\tfunction _verifySign(bytes memory _sign, address _signer) pure private returns (bool) {
        bytes32 hash = keccak256(abi.encodePacked(\"\\x19Ethereum Signed Message:\
\", AGREEMENT_LENGTH, AGREEMENT));
        address[] memory signList = _recoverAddresses(hash, _sign);
        return signList[0] == _signer;
    }

    function _verifyKycSign(bytes memory _sign, address _customerAddress) view private returns (bool) {
        bytes32 hash = keccak256(abi.encodePacked(\"\\x19Ethereum Signed Message:\
20\", _customerAddress));
        address[] memory signList = _recoverAddresses(hash, _sign);
        return signList[0] == kycSigner;
    }

\tfunction _recoverAddresses(bytes32 _hash, bytes memory _signatures) pure private returns (address[] memory addresses) {
        uint8 v;
        bytes32 r;
        bytes32 s;
        uint count = _countSignatures(_signatures);
        addresses = new address[](count);
        for (uint i = 0; i \u003c count; i++) {
            (v, r, s) = _parseSignature(_signatures, i);
            addresses[i] = ecrecover(_hash, v, r, s);
        }
    }

\tfunction _parseSignature(bytes memory _signatures, uint _pos) pure private returns (uint8 v, bytes32 r, bytes32 s) {
        uint offset = _pos * 65;
        assembly {
            r := mload(add(_signatures, add(32, offset)))
            s := mload(add(_signatures, add(64, offset)))
            v := and(mload(add(_signatures, add(65, offset))), 0xff)
        }
        if (v \u003c 27) v += 27;
        require(v == 27 || v == 28);
    }

    function _countSignatures(bytes memory _signatures) pure private returns (uint) {
        return _signatures.length % 65 == 0 ? _signatures.length / 65 : 0;
    }
}"},"Ownable.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import \"./Context.sol\";
/**
 * @dev Contract module which provides a basic access control mechanism, where
 * there is an account (an owner) that can be granted exclusive access to
 * specific functions.
 *
 * By default, the owner account will be the one that deploys the contract. This
 * can later be changed with {transferOwnership}.
 *
 * This module is used through inheritance. It will make available the modifier
 * `onlyOwner`, which can be applied to your functions to restrict their use to
 * the owner.
 */
abstract contract Ownable is Context {
    address private _owner;

    event OwnershipTransferred(address indexed previousOwner, address indexed newOwner);

    /**
     * @dev Initializes the contract setting the deployer as the initial owner.
     */
    constructor () {
        address msgSender = _msgSender();
        _owner = msgSender;
        emit OwnershipTransferred(address(0), msgSender);
    }

    /**
     * @dev Returns the address of the current owner.
     */
    function owner() public view virtual returns (address) {
        return _owner;
    }

    /**
     * @dev Throws if called by any account other than the owner.
     */
    modifier onlyOwner() {
        require(owner() == _msgSender(), \"Ownable: caller is not the owner\");
        _;
    }

    /**
     * @dev Leaves the contract without owner. It will not be possible to call
     * `onlyOwner` functions anymore. Can only be called by the current owner.
     *
     * NOTE: Renouncing ownership will leave the contract without an owner,
     * thereby removing any functionality that is only available to the owner.
     */
    function renounceOwnership() public virtual onlyOwner {
        emit OwnershipTransferred(_owner, address(0));
        _owner = address(0);
    }

    /**
     * @dev Transfers ownership of the contract to a new account (`newOwner`).
     * Can only be called by the current owner.
     */
    function transferOwnership(address newOwner) public virtual onlyOwner {
        require(newOwner != address(0), \"Ownable: new owner is the zero address\");
        emit OwnershipTransferred(_owner, newOwner);
        _owner = newOwner;
    }
}"},"TransferHelper.sol":{"content":"// SPDX-License-Identifier: GPL-3.0-or-later

pragma solidity \u003e=0.6.0;

// helper methods for interacting with ERC20 tokens and sending ETH that do not consistently return true/false
library TransferHelper {
    function safeApprove(
        address token,
        address to,
        uint256 value
    ) internal {
        // bytes4(keccak256(bytes(\u0027approve(address,uint256)\u0027)));
        (bool success, bytes memory data) = token.call(abi.encodeWithSelector(0x095ea7b3, to, value));
        require(
            success \u0026\u0026 (data.length == 0 || abi.decode(data, (bool))),
            \u0027TransferHelper::safeApprove: approve failed\u0027
        );
    }

    function safeTransfer(
        address token,
        address to,
        uint256 value
    ) internal {
        // bytes4(keccak256(bytes(\u0027transfer(address,uint256)\u0027)));
        (bool success, bytes memory data) = token.call(abi.encodeWithSelector(0xa9059cbb, to, value));
        require(
            success \u0026\u0026 (data.length == 0 || abi.decode(data, (bool))),
            \u0027TransferHelper::safeTransfer: transfer failed\u0027
        );
    }

    function safeTransferFrom(
        address token,
        address from,
        address to,
        uint256 value
    ) internal {
        // bytes4(keccak256(bytes(\u0027transferFrom(address,address,uint256)\u0027)));
        (bool success, bytes memory data) = token.call(abi.encodeWithSelector(0x23b872dd, from, to, value));
        require(
            success \u0026\u0026 (data.length == 0 || abi.decode(data, (bool))),
            \u0027TransferHelper::transferFrom: transferFrom failed\u0027
        );
    }

    function safeTransferETH(address to, uint256 value) internal {
        (bool success, ) = to.call{value: value}(new bytes(0));
        require(success, \u0027TransferHelper::safeTransferETH: ETH transfer failed\u0027);
    }
}
