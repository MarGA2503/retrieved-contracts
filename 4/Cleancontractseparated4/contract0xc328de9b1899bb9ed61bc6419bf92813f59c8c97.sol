



// SPDX-License-Identifier: MIT

pragma solidity >=0.6.0 <0.8.0;

/*
 * @dev Provides information about the current execution context, including the
 * sender of the transaction and its data. While these are generally available
 * via msg.sender and msg.data, they should not be accessed in such a direct
 * manner, since when dealing with GSN meta-transactions the account sending and
 * paying for execution may not be the actual sender (as far as an application
 * is concerned).
 *
 * This contract is only required for intermediate, library-like contracts.
 */
abstract contract Context {
    function _msgSender() internal view virtual returns (address payable) {
        return msg.sender;
    }

    function _msgData() internal view virtual returns (bytes memory) {
        this; // silence state mutability warning without generating bytecode - see https://github.com/ethereum/solidity/issues/2691
        return msg.data;
    }
}
"
    
// SPDX-License-Identifier: MIT

/**
 * From https://github.com/OpenZeppelin/openzeppelin-contracts/blob/1488d4f6782f76f74f3652e44da9b9e241146ccb/contracts/access/AccessControl.sol
 *
 * Changes:
 * - Compiled for 0.7.6
 * - Removed ERC165 Introspection
 * - Removed _checkRole
 * - Reformatted styling in line with this repository.
 */

/*
The MIT License (MIT)

Copyright (c) 2016-2020 zOS Global Limited

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
\"Software\"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

pragma solidity ^0.7.6;

import \"@openzeppelin/contracts/utils/Context.sol\";

import \"../interfaces/IAccessControl.sol\";

abstract contract AccessControl is Context, IAccessControl {
\tstruct RoleData {
\t\tmapping(address => bool) members;
\t\tbytes32 adminRole;
\t}

\tmapping(bytes32 => RoleData) private _roles;

\tbytes32 public constant DEFAULT_ADMIN_ROLE = 0x00;

\t/* Modifiers */

\tmodifier onlyRole(bytes32 role) {
\t\trequire(hasRole(role, _msgSender()), \"AccessControl: access denied\");
\t\t_;
\t}

\t/* External Views */

\t/**
\t * @dev Returns `true` if `account` has been granted `role`.
\t */
\tfunction hasRole(bytes32 role, address account)
\t\tpublic
\t\tview
\t\toverride
\t\treturns (bool)
\t{
\t\treturn _roles[role].members[account];
\t}

\t/**
\t * @dev Returns the admin role that controls `role`. See {grantRole} and
\t * {revokeRole}.
\t *
\t * To change a role's admin, use {_setRoleAdmin}.
\t */
\tfunction getRoleAdmin(bytes32 role) public view override returns (bytes32) {
\t\treturn _roles[role].adminRole;
\t}

\t/* External Mutators */

\t/**
\t * @dev Grants `role` to `account`.
\t *
\t * If `account` had not been already granted `role`, emits a {RoleGranted}
\t * event.
\t *
\t * Requirements:
\t *
\t * - the caller must have ``role``'s admin role.
\t */
\tfunction grantRole(bytes32 role, address account)
\t\tpublic
\t\tvirtual
\t\toverride
\t\tonlyRole(getRoleAdmin(role))
\t{
\t\t_grantRole(role, account);
\t}

\t/**
\t * @dev Revokes `role` from `account`.
\t *
\t * If `account` had been granted `role`, emits a {RoleRevoked} event.
\t *
\t * Requirements:
\t *
\t * - the caller must have ``role``'s admin role.
\t */
\tfunction revokeRole(bytes32 role, address account)
\t\tpublic
\t\tvirtual
\t\toverride
\t\tonlyRole(getRoleAdmin(role))
\t{
\t\t_revokeRole(role, account);
\t}

\t/**
\t * @dev Revokes `role` from the calling account.
\t *
\t * Roles are often managed via {grantRole} and {revokeRole}: this function's
\t * purpose is to provide a mechanism for accounts to lose their privileges
\t * if they are compromised (such as when a trusted device is misplaced).
\t *
\t * If the calling account had been granted `role`, emits a {RoleRevoked}
\t * event.
\t *
\t * Requirements:
\t *
\t * - the caller must be `account`.
\t */
\tfunction renounceRole(bytes32 role, address account)
\t\tpublic
\t\tvirtual
\t\toverride
\t{
\t\trequire(
\t\t\taccount == _msgSender(),
\t\t\t\"AccessControl: can only renounce roles for self\"
\t\t);

\t\t_revokeRole(role, account);
\t}

\t/* Internal Mutators */

\t/**
\t * @dev Grants `role` to `account`.
\t *
\t * If `account` had not been already granted `role`, emits a {RoleGranted}
\t * event. Note that unlike {grantRole}, this function doesn't perform any
\t * checks on the calling account.
\t *
\t * [WARNING]
\t * ====
\t * This function should only be called from the constructor when setting
\t * up the initial roles for the system.
\t *
\t * Using this function in any other way is effectively circumventing the admin
\t * system imposed by {AccessControl}.
\t * ====
\t */
\tfunction _setupRole(bytes32 role, address account) internal virtual {
\t\t_grantRole(role, account);
\t}

\t/**
\t * @dev Sets `adminRole` as ``role``'s admin role.
\t *
\t * Emits a {RoleAdminChanged} event.
\t */
\tfunction _setRoleAdmin(bytes32 role, bytes32 adminRole) internal virtual {
\t\temit RoleAdminChanged(role, getRoleAdmin(role), adminRole);
\t\t_roles[role].adminRole = adminRole;
\t}

\tfunction _grantRole(bytes32 role, address account) private {
\t\tif (!hasRole(role, account)) {
\t\t\t_roles[role].members[account] = true;
\t\t\temit RoleGranted(role, account, _msgSender());
\t\t}
\t}

\tfunction _revokeRole(bytes32 role, address account) private {
\t\tif (hasRole(role, account)) {
\t\t\t_roles[role].members[account] = false;
\t\t\temit RoleRevoked(role, account, _msgSender());
\t\t}
\t}
}
"
    
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

interface IAccessControl {
\t/* Views */

\tfunction getRoleAdmin(bytes32 role) external view returns (bytes32);

\tfunction hasRole(bytes32 role, address account) external view returns (bool);

\t/* Mutators */

\tfunction grantRole(bytes32 role, address account) external;

\tfunction revokeRole(bytes32 role, address account) external;

\tfunction renounceRole(bytes32 role, address account) external;

\t/* Events */
\tevent RoleAdminChanged(
\t\tbytes32 indexed role,
\t\tbytes32 indexed previousAdminRole,
\t\tbytes32 indexed newAdminRole
\t);
\tevent RoleGranted(
\t\tbytes32 indexed role,
\t\taddress indexed account,
\t\taddress indexed sender
\t);
\tevent RoleRevoked(
\t\tbytes32 indexed role,
\t\taddress indexed account,
\t\taddress indexed sender
\t);
}
"
    
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

import \"../access/AccessControl/AccessControl.sol\";

interface IGasPrice {
\tfunction setGasPrice(uint256) external;
}

interface IETHtx {
\tfunction rebase() external;
}

contract Policy is AccessControl {
\tbytes32 public constant POLICY_ROLE = keccak256(\"POLICY_ROLE\");

\taddress public immutable ethtx;
\taddress public immutable gasOracle;

\tconstructor(
\t\taddress admin,
\t\taddress policyMaker,
\t\taddress gasOracle_,
\t\taddress ethtx_
\t) {
\t\t_setupRole(DEFAULT_ADMIN_ROLE, admin);
\t\t_setupRole(POLICY_ROLE, policyMaker);
\t\tethtx = ethtx_;
\t\tgasOracle = gasOracle_;
\t}

\tfunction update(uint256 gasPrice) external onlyRole(POLICY_ROLE) {
\t\tIGasPrice(gasOracle).setGasPrice(gasPrice);
\t\tIETHtx(ethtx).rebase();
\t}
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

import \"../access/AccessControl/AccessControl.sol\";

interface IGasPrice {
\tfunction setGasPrice(uint256) external;
}

interface IETHtx {
\tfunction rebase() external;
}

contract Policy is AccessControl {
\tbytes32 public constant POLICY_ROLE = keccak256(\"POLICY_ROLE\");

\taddress public immutable ethtx;
\taddress public immutable gasOracle;

\tconstructor(
\t\taddress admin,
\t\taddress policyMaker,
\t\taddress gasOracle_,
\t\taddress ethtx_
\t) {
\t\t_setupRole(DEFAULT_ADMIN_ROLE, admin);
\t\t_setupRole(POLICY_ROLE, policyMaker);
\t\tethtx = ethtx_;
\t\tgasOracle = gasOracle_;
\t}

\tfunction update(uint256 gasPrice) external onlyRole(POLICY_ROLE) {
\t\tIGasPrice(gasOracle).setGasPrice(gasPrice);
\t\tIETHtx(ethtx).rebase();
\t}
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

import \"../access/AccessControl/AccessControl.sol\";

interface IGasPrice {
\tfunction setGasPrice(uint256) external;
}

interface IETHtx {
\tfunction rebase() external;
}

contract Policy is AccessControl {
\tbytes32 public constant POLICY_ROLE = keccak256(\"POLICY_ROLE\");

\taddress public immutable ethtx;
\taddress public immutable gasOracle;

\tconstructor(
\t\taddress admin,
\t\taddress policyMaker,
\t\taddress gasOracle_,
\t\taddress ethtx_
\t) {
\t\t_setupRole(DEFAULT_ADMIN_ROLE, admin);
\t\t_setupRole(POLICY_ROLE, policyMaker);
\t\tethtx = ethtx_;
\t\tgasOracle = gasOracle_;
\t}

\tfunction update(uint256 gasPrice) external onlyRole(POLICY_ROLE) {
\t\tIGasPrice(gasOracle).setGasPrice(gasPrice);
\t\tIETHtx(ethtx).rebase();
\t}
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

import \"../access/AccessControl/AccessControl.sol\";

interface IGasPrice {
\tfunction setGasPrice(uint256) external;
}

interface IETHtx {
\tfunction rebase() external;
}

contract Policy is AccessControl {
\tbytes32 public constant POLICY_ROLE = keccak256(\"POLICY_ROLE\");

\taddress public immutable ethtx;
\taddress public immutable gasOracle;

\tconstructor(
\t\taddress admin,
\t\taddress policyMaker,
\t\taddress gasOracle_,
\t\taddress ethtx_
\t) {
\t\t_setupRole(DEFAULT_ADMIN_ROLE, admin);
\t\t_setupRole(POLICY_ROLE, policyMaker);
\t\tethtx = ethtx_;
\t\tgasOracle = gasOracle_;
\t}

\tfunction update(uint256 gasPrice) external onlyRole(POLICY_ROLE) {
\t\tIGasPrice(gasOracle).setGasPrice(gasPrice);
\t\tIETHtx(ethtx).rebase();
\t}
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

import \"../access/AccessControl/AccessControl.sol\";

interface IGasPrice {
\tfunction setGasPrice(uint256) external;
}

interface IETHtx {
\tfunction rebase() external;
}

contract Policy is AccessControl {
\tbytes32 public constant POLICY_ROLE = keccak256(\"POLICY_ROLE\");

\taddress public immutable ethtx;
\taddress public immutable gasOracle;

\tconstructor(
\t\taddress admin,
\t\taddress policyMaker,
\t\taddress gasOracle_,
\t\taddress ethtx_
\t) {
\t\t_setupRole(DEFAULT_ADMIN_ROLE, admin);
\t\t_setupRole(POLICY_ROLE, policyMaker);
\t\tethtx = ethtx_;
\t\tgasOracle = gasOracle_;
\t}

\tfunction update(uint256 gasPrice) external onlyRole(POLICY_ROLE) {
\t\tIGasPrice(gasOracle).setGasPrice(gasPrice);
\t\tIETHtx(ethtx).rebase();
\t}
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

import \"../access/AccessControl/AccessControl.sol\";

interface IGasPrice {
\tfunction setGasPrice(uint256) external;
}

interface IETHtx {
\tfunction rebase() external;
}

contract Policy is AccessControl {
\tbytes32 public constant POLICY_ROLE = keccak256(\"POLICY_ROLE\");

\taddress public immutable ethtx;
\taddress public immutable gasOracle;

\tconstructor(
\t\taddress admin,
\t\taddress policyMaker,
\t\taddress gasOracle_,
\t\taddress ethtx_
\t) {
\t\t_setupRole(DEFAULT_ADMIN_ROLE, admin);
\t\t_setupRole(POLICY_ROLE, policyMaker);
\t\tethtx = ethtx_;
\t\tgasOracle = gasOracle_;
\t}

\tfunction update(uint256 gasPrice) external onlyRole(POLICY_ROLE) {
\t\tIGasPrice(gasOracle).setGasPrice(gasPrice);
\t\tIETHtx(ethtx).rebase();
\t}
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

import \"../access/AccessControl/AccessControl.sol\";

interface IGasPrice {
\tfunction setGasPrice(uint256) external;
}

interface IETHtx {
\tfunction rebase() external;
}

contract Policy is AccessControl {
\tbytes32 public constant POLICY_ROLE = keccak256(\"POLICY_ROLE\");

\taddress public immutable ethtx;
\taddress public immutable gasOracle;

\tconstructor(
\t\taddress admin,
\t\taddress policyMaker,
\t\taddress gasOracle_,
\t\taddress ethtx_
\t) {
\t\t_setupRole(DEFAULT_ADMIN_ROLE, admin);
\t\t_setupRole(POLICY_ROLE, policyMaker);
\t\tethtx = ethtx_;
\t\tgasOracle = gasOracle_;
\t}

\tfunction update(uint256 gasPrice) external onlyRole(POLICY_ROLE) {
\t\tIGasPrice(gasOracle).setGasPrice(gasPrice);
\t\tIETHtx(ethtx).rebase();
\t}
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

import \"../access/AccessControl/AccessControl.sol\";

interface IGasPrice {
\tfunction setGasPrice(uint256) external;
}

interface IETHtx {
\tfunction rebase() external;
}

contract Policy is AccessControl {
\tbytes32 public constant POLICY_ROLE = keccak256(\"POLICY_ROLE\");

\taddress public immutable ethtx;
\taddress public immutable gasOracle;

\tconstructor(
\t\taddress admin,
\t\taddress policyMaker,
\t\taddress gasOracle_,
\t\taddress ethtx_
\t) {
\t\t_setupRole(DEFAULT_ADMIN_ROLE, admin);
\t\t_setupRole(POLICY_ROLE, policyMaker);
\t\tethtx = ethtx_;
\t\tgasOracle = gasOracle_;
\t}

\tfunction update(uint256 gasPrice) external onlyRole(POLICY_ROLE) {
\t\tIGasPrice(gasOracle).setGasPrice(gasPrice);
\t\tIETHtx(ethtx).rebase();
\t}
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

import \"../access/AccessControl/AccessControl.sol\";

interface IGasPrice {
\tfunction setGasPrice(uint256) external;
}

interface IETHtx {
\tfunction rebase() external;
}

contract Policy is AccessControl {
\tbytes32 public constant POLICY_ROLE = keccak256(\"POLICY_ROLE\");

\taddress public immutable ethtx;
\taddress public immutable gasOracle;

\tconstructor(
\t\taddress admin,
\t\taddress policyMaker,
\t\taddress gasOracle_,
\t\taddress ethtx_
\t) {
\t\t_setupRole(DEFAULT_ADMIN_ROLE, admin);
\t\t_setupRole(POLICY_ROLE, policyMaker);
\t\tethtx = ethtx_;
\t\tgasOracle = gasOracle_;
\t}

\tfunction update(uint256 gasPrice) external onlyRole(POLICY_ROLE) {
\t\tIGasPrice(gasOracle).setGasPrice(gasPrice);
\t\tIETHtx(ethtx).rebase();
\t}
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

import \"../access/AccessControl/AccessControl.sol\";

interface IGasPrice {
\tfunction setGasPrice(uint256) external;
}

interface IETHtx {
\tfunction rebase() external;
}

contract Policy is AccessControl {
\tbytes32 public constant POLICY_ROLE = keccak256(\"POLICY_ROLE\");

\taddress public immutable ethtx;
\taddress public immutable gasOracle;

\tconstructor(
\t\taddress admin,
\t\taddress policyMaker,
\t\taddress gasOracle_,
\t\taddress ethtx_
\t) {
\t\t_setupRole(DEFAULT_ADMIN_ROLE, admin);
\t\t_setupRole(POLICY_ROLE, policyMaker);
\t\tethtx = ethtx_;
\t\tgasOracle = gasOracle_;
\t}

\tfunction update(uint256 gasPrice) external onlyRole(POLICY_ROLE) {
\t\tIGasPrice(gasOracle).setGasPrice(gasPrice);
\t\tIETHtx(ethtx).rebase();
\t}
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

import \"../access/AccessControl/AccessControl.sol\";

interface IGasPrice {
\tfunction setGasPrice(uint256) external;
}

interface IETHtx {
\tfunction rebase() external;
}

contract Policy is AccessControl {
\tbytes32 public constant POLICY_ROLE = keccak256(\"POLICY_ROLE\");

\taddress public immutable ethtx;
\taddress public immutable gasOracle;

\tconstructor(
\t\taddress admin,
\t\taddress policyMaker,
\t\taddress gasOracle_,
\t\taddress ethtx_
\t) {
\t\t_setupRole(DEFAULT_ADMIN_ROLE, admin);
\t\t_setupRole(POLICY_ROLE, policyMaker);
\t\tethtx = ethtx_;
\t\tgasOracle = gasOracle_;
\t}

\tfunction update(uint256 gasPrice) external onlyRole(POLICY_ROLE) {
\t\tIGasPrice(gasOracle).setGasPrice(gasPrice);
\t\tIETHtx(ethtx).rebase();
\t}
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

import \"../access/AccessControl/AccessControl.sol\";

interface IGasPrice {
\tfunction setGasPrice(uint256) external;
}

interface IETHtx {
\tfunction rebase() external;
}

contract Policy is AccessControl {
\tbytes32 public constant POLICY_ROLE = keccak256(\"POLICY_ROLE\");

\taddress public immutable ethtx;
\taddress public immutable gasOracle;

\tconstructor(
\t\taddress admin,
\t\taddress policyMaker,
\t\taddress gasOracle_,
\t\taddress ethtx_
\t) {
\t\t_setupRole(DEFAULT_ADMIN_ROLE, admin);
\t\t_setupRole(POLICY_ROLE, policyMaker);
\t\tethtx = ethtx_;
\t\tgasOracle = gasOracle_;
\t}

\tfunction update(uint256 gasPrice) external onlyRole(POLICY_ROLE) {
\t\tIGasPrice(gasOracle).setGasPrice(gasPrice);
\t\tIETHtx(ethtx).rebase();
\t}
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

import \"../access/AccessControl/AccessControl.sol\";

interface IGasPrice {
\tfunction setGasPrice(uint256) external;
}

interface IETHtx {
\tfunction rebase() external;
}

contract Policy is AccessControl {
\tbytes32 public constant POLICY_ROLE = keccak256(\"POLICY_ROLE\");

\taddress public immutable ethtx;
\taddress public immutable gasOracle;

\tconstructor(
\t\taddress admin,
\t\taddress policyMaker,
\t\taddress gasOracle_,
\t\taddress ethtx_
\t) {
\t\t_setupRole(DEFAULT_ADMIN_ROLE, admin);
\t\t_setupRole(POLICY_ROLE, policyMaker);
\t\tethtx = ethtx_;
\t\tgasOracle = gasOracle_;
\t}

\tfunction update(uint256 gasPrice) external onlyRole(POLICY_ROLE) {
\t\tIGasPrice(gasOracle).setGasPrice(gasPrice);
\t\tIETHtx(ethtx).rebase();
\t}
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

import \"../access/AccessControl/AccessControl.sol\";

interface IGasPrice {
\tfunction setGasPrice(uint256) external;
}

interface IETHtx {
\tfunction rebase() external;
}

contract Policy is AccessControl {
\tbytes32 public constant POLICY_ROLE = keccak256(\"POLICY_ROLE\");

\taddress public immutable ethtx;
\taddress public immutable gasOracle;

\tconstructor(
\t\taddress admin,
\t\taddress policyMaker,
\t\taddress gasOracle_,
\t\taddress ethtx_
\t) {
\t\t_setupRole(DEFAULT_ADMIN_ROLE, admin);
\t\t_setupRole(POLICY_ROLE, policyMaker);
\t\tethtx = ethtx_;
\t\tgasOracle = gasOracle_;
\t}

\tfunction update(uint256 gasPrice) external onlyRole(POLICY_ROLE) {
\t\tIGasPrice(gasOracle).setGasPrice(gasPrice);
\t\tIETHtx(ethtx).rebase();
\t}
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

import \"../access/AccessControl/AccessControl.sol\";

interface IGasPrice {
\tfunction setGasPrice(uint256) external;
}

interface IETHtx {
\tfunction rebase() external;
}

contract Policy is AccessControl {
\tbytes32 public constant POLICY_ROLE = keccak256(\"POLICY_ROLE\");

\taddress public immutable ethtx;
\taddress public immutable gasOracle;

\tconstructor(
\t\taddress admin,
\t\taddress policyMaker,
\t\taddress gasOracle_,
\t\taddress ethtx_
\t) {
\t\t_setupRole(DEFAULT_ADMIN_ROLE, admin);
\t\t_setupRole(POLICY_ROLE, policyMaker);
\t\tethtx = ethtx_;
\t\tgasOracle = gasOracle_;
\t}

\tfunction update(uint256 gasPrice) external onlyRole(POLICY_ROLE) {
\t\tIGasPrice(gasOracle).setGasPrice(gasPrice);
\t\tIETHtx(ethtx).rebase();
\t}
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

import \"../access/AccessControl/AccessControl.sol\";

interface IGasPrice {
\tfunction setGasPrice(uint256) external;
}

interface IETHtx {
\tfunction rebase() external;
}

contract Policy is AccessControl {
\tbytes32 public constant POLICY_ROLE = keccak256(\"POLICY_ROLE\");

\taddress public immutable ethtx;
\taddress public immutable gasOracle;

\tconstructor(
\t\taddress admin,
\t\taddress policyMaker,
\t\taddress gasOracle_,
\t\taddress ethtx_
\t) {
\t\t_setupRole(DEFAULT_ADMIN_ROLE, admin);
\t\t_setupRole(POLICY_ROLE, policyMaker);
\t\tethtx = ethtx_;
\t\tgasOracle = gasOracle_;
\t}

\tfunction update(uint256 gasPrice) external onlyRole(POLICY_ROLE) {
\t\tIGasPrice(gasOracle).setGasPrice(gasPrice);
\t\tIETHtx(ethtx).rebase();
\t}
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

import \"../access/AccessControl/AccessControl.sol\";

interface IGasPrice {
\tfunction setGasPrice(uint256) external;
}

interface IETHtx {
\tfunction rebase() external;
}

contract Policy is AccessControl {
\tbytes32 public constant POLICY_ROLE = keccak256(\"POLICY_ROLE\");

\taddress public immutable ethtx;
\taddress public immutable gasOracle;

\tconstructor(
\t\taddress admin,
\t\taddress policyMaker,
\t\taddress gasOracle_,
\t\taddress ethtx_
\t) {
\t\t_setupRole(DEFAULT_ADMIN_ROLE, admin);
\t\t_setupRole(POLICY_ROLE, policyMaker);
\t\tethtx = ethtx_;
\t\tgasOracle = gasOracle_;
\t}

\tfunction update(uint256 gasPrice) external onlyRole(POLICY_ROLE) {
\t\tIGasPrice(gasOracle).setGasPrice(gasPrice);
\t\tIETHtx(ethtx).rebase();
\t}
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

import \"../access/AccessControl/AccessControl.sol\";

interface IGasPrice {
\tfunction setGasPrice(uint256) external;
}

interface IETHtx {
\tfunction rebase() external;
}

contract Policy is AccessControl {
\tbytes32 public constant POLICY_ROLE = keccak256(\"POLICY_ROLE\");

\taddress public immutable ethtx;
\taddress public immutable gasOracle;

\tconstructor(
\t\taddress admin,
\t\taddress policyMaker,
\t\taddress gasOracle_,
\t\taddress ethtx_
\t) {
\t\t_setupRole(DEFAULT_ADMIN_ROLE, admin);
\t\t_setupRole(POLICY_ROLE, policyMaker);
\t\tethtx = ethtx_;
\t\tgasOracle = gasOracle_;
\t}

\tfunction update(uint256 gasPrice) external onlyRole(POLICY_ROLE) {
\t\tIGasPrice(gasOracle).setGasPrice(gasPrice);
\t\tIETHtx(ethtx).rebase();
\t}
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

import \"../access/AccessControl/AccessControl.sol\";

interface IGasPrice {
\tfunction setGasPrice(uint256) external;
}

interface IETHtx {
\tfunction rebase() external;
}

contract Policy is AccessControl {
\tbytes32 public constant POLICY_ROLE = keccak256(\"POLICY_ROLE\");

\taddress public immutable ethtx;
\taddress public immutable gasOracle;

\tconstructor(
\t\taddress admin,
\t\taddress policyMaker,
\t\taddress gasOracle_,
\t\taddress ethtx_
\t) {
\t\t_setupRole(DEFAULT_ADMIN_ROLE, admin);
\t\t_setupRole(POLICY_ROLE, policyMaker);
\t\tethtx = ethtx_;
\t\tgasOracle = gasOracle_;
\t}

\tfunction update(uint256 gasPrice) external onlyRole(POLICY_ROLE) {
\t\tIGasPrice(gasOracle).setGasPrice(gasPrice);
\t\tIETHtx(ethtx).rebase();
\t}
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

import \"../access/AccessControl/AccessControl.sol\";

interface IGasPrice {
\tfunction setGasPrice(uint256) external;
}

interface IETHtx {
\tfunction rebase() external;
}

contract Policy is AccessControl {
\tbytes32 public constant POLICY_ROLE = keccak256(\"POLICY_ROLE\");

\taddress public immutable ethtx;
\taddress public immutable gasOracle;

\tconstructor(
\t\taddress admin,
\t\taddress policyMaker,
\t\taddress gasOracle_,
\t\taddress ethtx_
\t) {
\t\t_setupRole(DEFAULT_ADMIN_ROLE, admin);
\t\t_setupRole(POLICY_ROLE, policyMaker);
\t\tethtx = ethtx_;
\t\tgasOracle = gasOracle_;
\t}

\tfunction update(uint256 gasPrice) external onlyRole(POLICY_ROLE) {
\t\tIGasPrice(gasOracle).setGasPrice(gasPrice);
\t\tIETHtx(ethtx).rebase();
\t}
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

import \"../access/AccessControl/AccessControl.sol\";

interface IGasPrice {
\tfunction setGasPrice(uint256) external;
}

interface IETHtx {
\tfunction rebase() external;
}

contract Policy is AccessControl {
\tbytes32 public constant POLICY_ROLE = keccak256(\"POLICY_ROLE\");

\taddress public immutable ethtx;
\taddress public immutable gasOracle;

\tconstructor(
\t\taddress admin,
\t\taddress policyMaker,
\t\taddress gasOracle_,
\t\taddress ethtx_
\t) {
\t\t_setupRole(DEFAULT_ADMIN_ROLE, admin);
\t\t_setupRole(POLICY_ROLE, policyMaker);
\t\tethtx = ethtx_;
\t\tgasOracle = gasOracle_;
\t}

\tfunction update(uint256 gasPrice) external onlyRole(POLICY_ROLE) {
\t\tIGasPrice(gasOracle).setGasPrice(gasPrice);
\t\tIETHtx(ethtx).rebase();
\t}
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

import \"../access/AccessControl/AccessControl.sol\";

interface IGasPrice {
\tfunction setGasPrice(uint256) external;
}

interface IETHtx {
\tfunction rebase() external;
}

contract Policy is AccessControl {
\tbytes32 public constant POLICY_ROLE = keccak256(\"POLICY_ROLE\");

\taddress public immutable ethtx;
\taddress public immutable gasOracle;

\tconstructor(
\t\taddress admin,
\t\taddress policyMaker,
\t\taddress gasOracle_,
\t\taddress ethtx_
\t) {
\t\t_setupRole(DEFAULT_ADMIN_ROLE, admin);
\t\t_setupRole(POLICY_ROLE, policyMaker);
\t\tethtx = ethtx_;
\t\tgasOracle = gasOracle_;
\t}

\tfunction update(uint256 gasPrice) external onlyRole(POLICY_ROLE) {
\t\tIGasPrice(gasOracle).setGasPrice(gasPrice);
\t\tIETHtx(ethtx).rebase();
\t}
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

import \"../access/AccessControl/AccessControl.sol\";

interface IGasPrice {
\tfunction setGasPrice(uint256) external;
}

interface IETHtx {
\tfunction rebase() external;
}

contract Policy is AccessControl {
\tbytes32 public constant POLICY_ROLE = keccak256(\"POLICY_ROLE\");

\taddress public immutable ethtx;
\taddress public immutable gasOracle;

\tconstructor(
\t\taddress admin,
\t\taddress policyMaker,
\t\taddress gasOracle_,
\t\taddress ethtx_
\t) {
\t\t_setupRole(DEFAULT_ADMIN_ROLE, admin);
\t\t_setupRole(POLICY_ROLE, policyMaker);
\t\tethtx = ethtx_;
\t\tgasOracle = gasOracle_;
\t}

\tfunction update(uint256 gasPrice) external onlyRole(POLICY_ROLE) {
\t\tIGasPrice(gasOracle).setGasPrice(gasPrice);
\t\tIETHtx(ethtx).rebase();
\t}
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

import \"../access/AccessControl/AccessControl.sol\";

interface IGasPrice {
\tfunction setGasPrice(uint256) external;
}

interface IETHtx {
\tfunction rebase() external;
}

contract Policy is AccessControl {
\tbytes32 public constant POLICY_ROLE = keccak256(\"POLICY_ROLE\");

\taddress public immutable ethtx;
\taddress public immutable gasOracle;

\tconstructor(
\t\taddress admin,
\t\taddress policyMaker,
\t\taddress gasOracle_,
\t\taddress ethtx_
\t) {
\t\t_setupRole(DEFAULT_ADMIN_ROLE, admin);
\t\t_setupRole(POLICY_ROLE, policyMaker);
\t\tethtx = ethtx_;
\t\tgasOracle = gasOracle_;
\t}

\tfunction update(uint256 gasPrice) external onlyRole(POLICY_ROLE) {
\t\tIGasPrice(gasOracle).setGasPrice(gasPrice);
\t\tIETHtx(ethtx).rebase();
\t}
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

import \"../access/AccessControl/AccessControl.sol\";

interface IGasPrice {
\tfunction setGasPrice(uint256) external;
}

interface IETHtx {
\tfunction rebase() external;
}

contract Policy is AccessControl {
\tbytes32 public constant POLICY_ROLE = keccak256(\"POLICY_ROLE\");

\taddress public immutable ethtx;
\taddress public immutable gasOracle;

\tconstructor(
\t\taddress admin,
\t\taddress policyMaker,
\t\taddress gasOracle_,
\t\taddress ethtx_
\t) {
\t\t_setupRole(DEFAULT_ADMIN_ROLE, admin);
\t\t_setupRole(POLICY_ROLE, policyMaker);
\t\tethtx = ethtx_;
\t\tgasOracle = gasOracle_;
\t}

\tfunction update(uint256 gasPrice) external onlyRole(POLICY_ROLE) {
\t\tIGasPrice(gasOracle).setGasPrice(gasPrice);
\t\tIETHtx(ethtx).rebase();
\t}
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

import \"../access/AccessControl/AccessControl.sol\";

interface IGasPrice {
\tfunction setGasPrice(uint256) external;
}

interface IETHtx {
\tfunction rebase() external;
}

contract Policy is AccessControl {
\tbytes32 public constant POLICY_ROLE = keccak256(\"POLICY_ROLE\");

\taddress public immutable ethtx;
\taddress public immutable gasOracle;

\tconstructor(
\t\taddress admin,
\t\taddress policyMaker,
\t\taddress gasOracle_,
\t\taddress ethtx_
\t) {
\t\t_setupRole(DEFAULT_ADMIN_ROLE, admin);
\t\t_setupRole(POLICY_ROLE, policyMaker);
\t\tethtx = ethtx_;
\t\tgasOracle = gasOracle_;
\t}

\tfunction update(uint256 gasPrice) external onlyRole(POLICY_ROLE) {
\t\tIGasPrice(gasOracle).setGasPrice(gasPrice);
\t\tIETHtx(ethtx).rebase();
\t}
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

import \"../access/AccessControl/AccessControl.sol\";

interface IGasPrice {
\tfunction setGasPrice(uint256) external;
}

interface IETHtx {
\tfunction rebase() external;
}

contract Policy is AccessControl {
\tbytes32 public constant POLICY_ROLE = keccak256(\"POLICY_ROLE\");

\taddress public immutable ethtx;
\taddress public immutable gasOracle;

\tconstructor(
\t\taddress admin,
\t\taddress policyMaker,
\t\taddress gasOracle_,
\t\taddress ethtx_
\t) {
\t\t_setupRole(DEFAULT_ADMIN_ROLE, admin);
\t\t_setupRole(POLICY_ROLE, policyMaker);
\t\tethtx = ethtx_;
\t\tgasOracle = gasOracle_;
\t}

\tfunction update(uint256 gasPrice) external onlyRole(POLICY_ROLE) {
\t\tIGasPrice(gasOracle).setGasPrice(gasPrice);
\t\tIETHtx(ethtx).rebase();
\t}
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

import \"../access/AccessControl/AccessControl.sol\";

interface IGasPrice {
\tfunction setGasPrice(uint256) external;
}

interface IETHtx {
\tfunction rebase() external;
}

contract Policy is AccessControl {
\tbytes32 public constant POLICY_ROLE = keccak256(\"POLICY_ROLE\");

\taddress public immutable ethtx;
\taddress public immutable gasOracle;

\tconstructor(
\t\taddress admin,
\t\taddress policyMaker,
\t\taddress gasOracle_,
\t\taddress ethtx_
\t) {
\t\t_setupRole(DEFAULT_ADMIN_ROLE, admin);
\t\t_setupRole(POLICY_ROLE, policyMaker);
\t\tethtx = ethtx_;
\t\tgasOracle = gasOracle_;
\t}

\tfunction update(uint256 gasPrice) external onlyRole(POLICY_ROLE) {
\t\tIGasPrice(gasOracle).setGasPrice(gasPrice);
\t\tIETHtx(ethtx).rebase();
\t}
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

import \"../access/AccessControl/AccessControl.sol\";

interface IGasPrice {
\tfunction setGasPrice(uint256) external;
}

interface IETHtx {
\tfunction rebase() external;
}

contract Policy is AccessControl {
\tbytes32 public constant POLICY_ROLE = keccak256(\"POLICY_ROLE\");

\taddress public immutable ethtx;
\taddress public immutable gasOracle;

\tconstructor(
\t\taddress admin,
\t\taddress policyMaker,
\t\taddress gasOracle_,
\t\taddress ethtx_
\t) {
\t\t_setupRole(DEFAULT_ADMIN_ROLE, admin);
\t\t_setupRole(POLICY_ROLE, policyMaker);
\t\tethtx = ethtx_;
\t\tgasOracle = gasOracle_;
\t}

\tfunction update(uint256 gasPrice) external onlyRole(POLICY_ROLE) {
\t\tIGasPrice(gasOracle).setGasPrice(gasPrice);
\t\tIETHtx(ethtx).rebase();
\t}
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

import \"../access/AccessControl/AccessControl.sol\";

interface IGasPrice {
\tfunction setGasPrice(uint256) external;
}

interface IETHtx {
\tfunction rebase() external;
}

contract Policy is AccessControl {
\tbytes32 public constant POLICY_ROLE = keccak256(\"POLICY_ROLE\");

\taddress public immutable ethtx;
\taddress public immutable gasOracle;

\tconstructor(
\t\taddress admin,
\t\taddress policyMaker,
\t\taddress gasOracle_,
\t\taddress ethtx_
\t) {
\t\t_setupRole(DEFAULT_ADMIN_ROLE, admin);
\t\t_setupRole(POLICY_ROLE, policyMaker);
\t\tethtx = ethtx_;
\t\tgasOracle = gasOracle_;
\t}

\tfunction update(uint256 gasPrice) external onlyRole(POLICY_ROLE) {
\t\tIGasPrice(gasOracle).setGasPrice(gasPrice);
\t\tIETHtx(ethtx).rebase();
\t}
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

import \"../access/AccessControl/AccessControl.sol\";

interface IGasPrice {
\tfunction setGasPrice(uint256) external;
}

interface IETHtx {
\tfunction rebase() external;
}

contract Policy is AccessControl {
\tbytes32 public constant POLICY_ROLE = keccak256(\"POLICY_ROLE\");

\taddress public immutable ethtx;
\taddress public immutable gasOracle;

\tconstructor(
\t\taddress admin,
\t\taddress policyMaker,
\t\taddress gasOracle_,
\t\taddress ethtx_
\t) {
\t\t_setupRole(DEFAULT_ADMIN_ROLE, admin);
\t\t_setupRole(POLICY_ROLE, policyMaker);
\t\tethtx = ethtx_;
\t\tgasOracle = gasOracle_;
\t}

\tfunction update(uint256 gasPrice) external onlyRole(POLICY_ROLE) {
\t\tIGasPrice(gasOracle).setGasPrice(gasPrice);
\t\tIETHtx(ethtx).rebase();
\t}
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

import \"../access/AccessControl/AccessControl.sol\";

interface IGasPrice {
\tfunction setGasPrice(uint256) external;
}

interface IETHtx {
\tfunction rebase() external;
}

contract Policy is AccessControl {
\tbytes32 public constant POLICY_ROLE = keccak256(\"POLICY_ROLE\");

\taddress public immutable ethtx;
\taddress public immutable gasOracle;

\tconstructor(
\t\taddress admin,
\t\taddress policyMaker,
\t\taddress gasOracle_,
\t\taddress ethtx_
\t) {
\t\t_setupRole(DEFAULT_ADMIN_ROLE, admin);
\t\t_setupRole(POLICY_ROLE, policyMaker);
\t\tethtx = ethtx_;
\t\tgasOracle = gasOracle_;
\t}

\tfunction update(uint256 gasPrice) external onlyRole(POLICY_ROLE) {
\t\tIGasPrice(gasOracle).setGasPrice(gasPrice);
\t\tIETHtx(ethtx).rebase();
\t}
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

import \"../access/AccessControl/AccessControl.sol\";

interface IGasPrice {
\tfunction setGasPrice(uint256) external;
}

interface IETHtx {
\tfunction rebase() external;
}

contract Policy is AccessControl {
\tbytes32 public constant POLICY_ROLE = keccak256(\"POLICY_ROLE\");

\taddress public immutable ethtx;
\taddress public immutable gasOracle;

\tconstructor(
\t\taddress admin,
\t\taddress policyMaker,
\t\taddress gasOracle_,
\t\taddress ethtx_
\t) {
\t\t_setupRole(DEFAULT_ADMIN_ROLE, admin);
\t\t_setupRole(POLICY_ROLE, policyMaker);
\t\tethtx = ethtx_;
\t\tgasOracle = gasOracle_;
\t}

\tfunction update(uint256 gasPrice) external onlyRole(POLICY_ROLE) {
\t\tIGasPrice(gasOracle).setGasPrice(gasPrice);
\t\tIETHtx(ethtx).rebase();
\t}
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

import \"../access/AccessControl/AccessControl.sol\";

interface IGasPrice {
\tfunction setGasPrice(uint256) external;
}

interface IETHtx {
\tfunction rebase() external;
}

contract Policy is AccessControl {
\tbytes32 public constant POLICY_ROLE = keccak256(\"POLICY_ROLE\");

\taddress public immutable ethtx;
\taddress public immutable gasOracle;

\tconstructor(
\t\taddress admin,
\t\taddress policyMaker,
\t\taddress gasOracle_,
\t\taddress ethtx_
\t) {
\t\t_setupRole(DEFAULT_ADMIN_ROLE, admin);
\t\t_setupRole(POLICY_ROLE, policyMaker);
\t\tethtx = ethtx_;
\t\tgasOracle = gasOracle_;
\t}

\tfunction update(uint256 gasPrice) external onlyRole(POLICY_ROLE) {
\t\tIGasPrice(gasOracle).setGasPrice(gasPrice);
\t\tIETHtx(ethtx).rebase();
\t}
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

import \"../access/AccessControl/AccessControl.sol\";

interface IGasPrice {
\tfunction setGasPrice(uint256) external;
}

interface IETHtx {
\tfunction rebase() external;
}

contract Policy is AccessControl {
\tbytes32 public constant POLICY_ROLE = keccak256(\"POLICY_ROLE\");

\taddress public immutable ethtx;
\taddress public immutable gasOracle;

\tconstructor(
\t\taddress admin,
\t\taddress policyMaker,
\t\taddress gasOracle_,
\t\taddress ethtx_
\t) {
\t\t_setupRole(DEFAULT_ADMIN_ROLE, admin);
\t\t_setupRole(POLICY_ROLE, policyMaker);
\t\tethtx = ethtx_;
\t\tgasOracle = gasOracle_;
\t}

\tfunction update(uint256 gasPrice) external onlyRole(POLICY_ROLE) {
\t\tIGasPrice(gasOracle).setGasPrice(gasPrice);
\t\tIETHtx(ethtx).rebase();
\t}
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

import \"../access/AccessControl/AccessControl.sol\";

interface IGasPrice {
\tfunction setGasPrice(uint256) external;
}

interface IETHtx {
\tfunction rebase() external;
}

contract Policy is AccessControl {
\tbytes32 public constant POLICY_ROLE = keccak256(\"POLICY_ROLE\");

\taddress public immutable ethtx;
\taddress public immutable gasOracle;

\tconstructor(
\t\taddress admin,
\t\taddress policyMaker,
\t\taddress gasOracle_,
\t\taddress ethtx_
\t) {
\t\t_setupRole(DEFAULT_ADMIN_ROLE, admin);
\t\t_setupRole(POLICY_ROLE, policyMaker);
\t\tethtx = ethtx_;
\t\tgasOracle = gasOracle_;
\t}

\tfunction update(uint256 gasPrice) external onlyRole(POLICY_ROLE) {
\t\tIGasPrice(gasOracle).setGasPrice(gasPrice);
\t\tIETHtx(ethtx).rebase();
\t}
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

import \"../access/AccessControl/AccessControl.sol\";

interface IGasPrice {
\tfunction setGasPrice(uint256) external;
}

interface IETHtx {
\tfunction rebase() external;
}

contract Policy is AccessControl {
\tbytes32 public constant POLICY_ROLE = keccak256(\"POLICY_ROLE\");

\taddress public immutable ethtx;
\taddress public immutable gasOracle;

\tconstructor(
\t\taddress admin,
\t\taddress policyMaker,
\t\taddress gasOracle_,
\t\taddress ethtx_
\t) {
\t\t_setupRole(DEFAULT_ADMIN_ROLE, admin);
\t\t_setupRole(POLICY_ROLE, policyMaker);
\t\tethtx = ethtx_;
\t\tgasOracle = gasOracle_;
\t}

\tfunction update(uint256 gasPrice) external onlyRole(POLICY_ROLE) {
\t\tIGasPrice(gasOracle).setGasPrice(gasPrice);
\t\tIETHtx(ethtx).rebase();
\t}
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

import \"../access/AccessControl/AccessControl.sol\";

interface IGasPrice {
\tfunction setGasPrice(uint256) external;
}

interface IETHtx {
\tfunction rebase() external;
}

contract Policy is AccessControl {
\tbytes32 public constant POLICY_ROLE = keccak256(\"POLICY_ROLE\");

\taddress public immutable ethtx;
\taddress public immutable gasOracle;

\tconstructor(
\t\taddress admin,
\t\taddress policyMaker,
\t\taddress gasOracle_,
\t\taddress ethtx_
\t) {
\t\t_setupRole(DEFAULT_ADMIN_ROLE, admin);
\t\t_setupRole(POLICY_ROLE, policyMaker);
\t\tethtx = ethtx_;
\t\tgasOracle = gasOracle_;
\t}

\tfunction update(uint256 gasPrice) external onlyRole(POLICY_ROLE) {
\t\tIGasPrice(gasOracle).setGasPrice(gasPrice);
\t\tIETHtx(ethtx).rebase();
\t}
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

import \"../access/AccessControl/AccessControl.sol\";

interface IGasPrice {
\tfunction setGasPrice(uint256) external;
}

interface IETHtx {
\tfunction rebase() external;
}

contract Policy is AccessControl {
\tbytes32 public constant POLICY_ROLE = keccak256(\"POLICY_ROLE\");

\taddress public immutable ethtx;
\taddress public immutable gasOracle;

\tconstructor(
\t\taddress admin,
\t\taddress policyMaker,
\t\taddress gasOracle_,
\t\taddress ethtx_
\t) {
\t\t_setupRole(DEFAULT_ADMIN_ROLE, admin);
\t\t_setupRole(POLICY_ROLE, policyMaker);
\t\tethtx = ethtx_;
\t\tgasOracle = gasOracle_;
\t}

\tfunction update(uint256 gasPrice) external onlyRole(POLICY_ROLE) {
\t\tIGasPrice(gasOracle).setGasPrice(gasPrice);
\t\tIETHtx(ethtx).rebase();
\t}
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

import \"../access/AccessControl/AccessControl.sol\";

interface IGasPrice {
\tfunction setGasPrice(uint256) external;
}

interface IETHtx {
\tfunction rebase() external;
}

contract Policy is AccessControl {
\tbytes32 public constant POLICY_ROLE = keccak256(\"POLICY_ROLE\");

\taddress public immutable ethtx;
\taddress public immutable gasOracle;

\tconstructor(
\t\taddress admin,
\t\taddress policyMaker,
\t\taddress gasOracle_,
\t\taddress ethtx_
\t) {
\t\t_setupRole(DEFAULT_ADMIN_ROLE, admin);
\t\t_setupRole(POLICY_ROLE, policyMaker);
\t\tethtx = ethtx_;
\t\tgasOracle = gasOracle_;
\t}

\tfunction update(uint256 gasPrice) external onlyRole(POLICY_ROLE) {
\t\tIGasPrice(gasOracle).setGasPrice(gasPrice);
\t\tIETHtx(ethtx).rebase();
\t}
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

import \"../access/AccessControl/AccessControl.sol\";

interface IGasPrice {
\tfunction setGasPrice(uint256) external;
}

interface IETHtx {
\tfunction rebase() external;
}

contract Policy is AccessControl {
\tbytes32 public constant POLICY_ROLE = keccak256(\"POLICY_ROLE\");

\taddress public immutable ethtx;
\taddress public immutable gasOracle;

\tconstructor(
\t\taddress admin,
\t\taddress policyMaker,
\t\taddress gasOracle_,
\t\taddress ethtx_
\t) {
\t\t_setupRole(DEFAULT_ADMIN_ROLE, admin);
\t\t_setupRole(POLICY_ROLE, policyMaker);
\t\tethtx = ethtx_;
\t\tgasOracle = gasOracle_;
\t}

\tfunction update(uint256 gasPrice) external onlyRole(POLICY_ROLE) {
\t\tIGasPrice(gasOracle).setGasPrice(gasPrice);
\t\tIETHtx(ethtx).rebase();
\t}
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

import \"../access/AccessControl/AccessControl.sol\";

interface IGasPrice {
\tfunction setGasPrice(uint256) external;
}

interface IETHtx {
\tfunction rebase() external;
}

contract Policy is AccessControl {
\tbytes32 public constant POLICY_ROLE = keccak256(\"POLICY_ROLE\");

\taddress public immutable ethtx;
\taddress public immutable gasOracle;

\tconstructor(
\t\taddress admin,
\t\taddress policyMaker,
\t\taddress gasOracle_,
\t\taddress ethtx_
\t) {
\t\t_setupRole(DEFAULT_ADMIN_ROLE, admin);
\t\t_setupRole(POLICY_ROLE, policyMaker);
\t\tethtx = ethtx_;
\t\tgasOracle = gasOracle_;
\t}

\tfunction update(uint256 gasPrice) external onlyRole(POLICY_ROLE) {
\t\tIGasPrice(gasOracle).setGasPrice(gasPrice);
\t\tIETHtx(ethtx).rebase();
\t}
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

import \"../access/AccessControl/AccessControl.sol\";

interface IGasPrice {
\tfunction setGasPrice(uint256) external;
}

interface IETHtx {
\tfunction rebase() external;
}

contract Policy is AccessControl {
\tbytes32 public constant POLICY_ROLE = keccak256(\"POLICY_ROLE\");

\taddress public immutable ethtx;
\taddress public immutable gasOracle;

\tconstructor(
\t\taddress admin,
\t\taddress policyMaker,
\t\taddress gasOracle_,
\t\taddress ethtx_
\t) {
\t\t_setupRole(DEFAULT_ADMIN_ROLE, admin);
\t\t_setupRole(POLICY_ROLE, policyMaker);
\t\tethtx = ethtx_;
\t\tgasOracle = gasOracle_;
\t}

\tfunction update(uint256 gasPrice) external onlyRole(POLICY_ROLE) {
\t\tIGasPrice(gasOracle).setGasPrice(gasPrice);
\t\tIETHtx(ethtx).rebase();
\t}
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

import \"../access/AccessControl/AccessControl.sol\";

interface IGasPrice {
\tfunction setGasPrice(uint256) external;
}

interface IETHtx {
\tfunction rebase() external;
}

contract Policy is AccessControl {
\tbytes32 public constant POLICY_ROLE = keccak256(\"POLICY_ROLE\");

\taddress public immutable ethtx;
\taddress public immutable gasOracle;

\tconstructor(
\t\taddress admin,
\t\taddress policyMaker,
\t\taddress gasOracle_,
\t\taddress ethtx_
\t) {
\t\t_setupRole(DEFAULT_ADMIN_ROLE, admin);
\t\t_setupRole(POLICY_ROLE, policyMaker);
\t\tethtx = ethtx_;
\t\tgasOracle = gasOracle_;
\t}

\tfunction update(uint256 gasPrice) external onlyRole(POLICY_ROLE) {
\t\tIGasPrice(gasOracle).setGasPrice(gasPrice);
\t\tIETHtx(ethtx).rebase();
\t}
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

import \"../access/AccessControl/AccessControl.sol\";

interface IGasPrice {
\tfunction setGasPrice(uint256) external;
}

interface IETHtx {
\tfunction rebase() external;
}

contract Policy is AccessControl {
\tbytes32 public constant POLICY_ROLE = keccak256(\"POLICY_ROLE\");

\taddress public immutable ethtx;
\taddress public immutable gasOracle;

\tconstructor(
\t\taddress admin,
\t\taddress policyMaker,
\t\taddress gasOracle_,
\t\taddress ethtx_
\t) {
\t\t_setupRole(DEFAULT_ADMIN_ROLE, admin);
\t\t_setupRole(POLICY_ROLE, policyMaker);
\t\tethtx = ethtx_;
\t\tgasOracle = gasOracle_;
\t}

\tfunction update(uint256 gasPrice) external onlyRole(POLICY_ROLE) {
\t\tIGasPrice(gasOracle).setGasPrice(gasPrice);
\t\tIETHtx(ethtx).rebase();
\t}
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

import \"../access/AccessControl/AccessControl.sol\";

interface IGasPrice {
\tfunction setGasPrice(uint256) external;
}

interface IETHtx {
\tfunction rebase() external;
}

contract Policy is AccessControl {
\tbytes32 public constant POLICY_ROLE = keccak256(\"POLICY_ROLE\");

\taddress public immutable ethtx;
\taddress public immutable gasOracle;

\tconstructor(
\t\taddress admin,
\t\taddress policyMaker,
\t\taddress gasOracle_,
\t\taddress ethtx_
\t) {
\t\t_setupRole(DEFAULT_ADMIN_ROLE, admin);
\t\t_setupRole(POLICY_ROLE, policyMaker);
\t\tethtx = ethtx_;
\t\tgasOracle = gasOracle_;
\t}

\tfunction update(uint256 gasPrice) external onlyRole(POLICY_ROLE) {
\t\tIGasPrice(gasOracle).setGasPrice(gasPrice);
\t\tIETHtx(ethtx).rebase();
\t}
}
"
    }
  }
}
