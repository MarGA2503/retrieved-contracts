// SPDX-License-Identifier: MIT

pragma solidity 0.6.8;

/**
 * @dev Collection of functions related to the address type
 */
library Address {
    /**
     * @dev Returns true if `account` is a contract.
     *
     * [IMPORTANT]
     * ====
     * It is unsafe to assume that an address for which this function returns
     * false is an externally-owned account (EOA) and not a contract.
     *
     * Among others, `isContract` will return false for the following
     * types of addresses:
     *
     *  - an externally-owned account
     *  - a contract in construction
     *  - an address where a contract will be created
     *  - an address where a contract lived, but was destroyed
     * ====
     */
    function isContract(address account) internal view returns (bool) {
        // This method relies in extcodesize, which returns 0 for contracts in
        // construction, since the code is only stored at the end of the
        // constructor execution.

        uint256 size;
        // solhint-disable-next-line no-inline-assembly
        assembly {
            size := extcodesize(account)
        }
        return size \u003e 0;
    }

    /**
     * @dev Replacement for Solidity\u0027s `transfer`: sends `amount` wei to
     * `recipient`, forwarding all available gas and reverting on errors.
     *
     * https://eips.ethereum.org/EIPS/eip-1884[EIP1884] increases the gas cost
     * of certain opcodes, possibly making contracts go over the 2300 gas limit
     * imposed by `transfer`, making them unable to receive vaults via
     * `transfer`. {sendValue} removes this limitation.
     *
     * https://diligence.consensys.net/posts/2019/09/stop-using-soliditys-transfer-now/[Learn more].
     *
     * IMPORTANT: because control is transferred to `recipient`, care must be
     * taken to not create reentrancy vulnerabilities. Consider using
     * {ReentrancyGuard} or the
     * https://solidity.readthedocs.io/en/v0.5.11/security-considerations.html#use-the-checks-effects-interactions-pattern[checks-effects-interactions pattern].
     */
    function sendValue(address payable recipient, uint256 amount) internal {
        require(
            address(this).balance \u003e= amount,
            \"Address: insufficient balance\"
        );

        // solhint-disable-next-line avoid-low-level-calls, avoid-call-value
        (bool success, ) = recipient.call{value: amount}(\"\");
        require(
            success,
            \"Address: unable to send value, recipient may have reverted\"
        );
    }

    /**
     * @dev Performs a Solidity function call using a low level `call`. A
     * plain`call` is an unsafe replacement for a function call: use this
     * function instead.
     *
     * If `target` reverts with a revert reason, it is bubbled up by this
     * function (like regular Solidity function calls).
     *
     * Returns the raw returned data. To convert to the expected return value,
     * use https://solidity.readthedocs.io/en/latest/units-and-global-variables.html?highlight=abi.decode#abi-encoding-and-decoding-functions[`abi.decode`].
     *
     * Requirements:
     *
     * - `target` must be a contract.
     * - calling `target` with `data` must not revert.
     *
     * _Available since v3.1._
     */
    function functionCall(address target, bytes memory data)
        internal
        returns (bytes memory)
    {
        return functionCall(target, data, \"Address: low-level call failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`], but with
     * `errorMessage` as a fallback revert reason when `target` reverts.
     *
     * _Available since v3.1._
     */
    function functionCall(
        address target,
        bytes memory data,
        string memory errorMessage
    ) internal returns (bytes memory) {
        return _functionCallWithValue(target, data, 0, errorMessage);
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`],
     * but also transferring `value` wei to `target`.
     *
     * Requirements:
     *
     * - the calling contract must have an ETH balance of at least `value`.
     * - the called Solidity function must be `payable`.
     *
     * _Available since v3.1._
     */
    function functionCallWithValue(
        address target,
        bytes memory data,
        uint256 value
    ) internal returns (bytes memory) {
        return
            functionCallWithValue(
                target,
                data,
                value,
                \"Address: low-level call with value failed\"
            );
    }

    /**
     * @dev Same as {xref-Address-functionCallWithValue-address-bytes-uint256-}[`functionCallWithValue`], but
     * with `errorMessage` as a fallback revert reason when `target` reverts.
     *
     * _Available since v3.1._
     */
    function functionCallWithValue(
        address target,
        bytes memory data,
        uint256 value,
        string memory errorMessage
    ) internal returns (bytes memory) {
        require(
            address(this).balance \u003e= value,
            \"Address: insufficient balance for call\"
        );
        return _functionCallWithValue(target, data, value, errorMessage);
    }

    function _functionCallWithValue(
        address target,
        bytes memory data,
        uint256 weiValue,
        string memory errorMessage
    ) private returns (bytes memory) {
        require(isContract(target), \"Address: call to non-contract\");

        // solhint-disable-next-line avoid-low-level-calls
        (bool success, bytes memory returndata) = target.call{value: weiValue}(
            data
        );
        if (success) {
            return returndata;
        } else {
            // Look for revert reason and bubble it up if present
            if (returndata.length \u003e 0) {
                // The easiest way to bubble the revert reason is using memory via assembly

                // solhint-disable-next-line no-inline-assembly
                assembly {
                    let returndata_size := mload(returndata)
                    revert(add(32, returndata), returndata_size)
                }
            } else {
                revert(errorMessage);
            }
        }
    }
}
"},"ClonesUpgradeable.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity \u003e=0.6.0 \u003c0.8.0;

/**
 * @dev https://eips.ethereum.org/EIPS/eip-1167[EIP 1167] is a standard for
 * deploying minimal proxy contracts, also known as \"clones\".
 *
 * \u003e To simply and cheaply clone contract functionality in an immutable way, this standard specifies
 * \u003e a minimal bytecode implementation that delegates all calls to a known, fixed address.
 *
 * The library includes functions to deploy a proxy using either `create` (traditional deployment) or `create2`
 * (salted deterministic deployment). It also includes functions to predict the addresses of clones deployed using the
 * deterministic method.
 *
 * _Available since v3.4._
 */
library ClonesUpgradeable {
    /**
     * @dev Deploys and returns the address of a clone that mimics the behaviour of `master`.
     *
     * This function uses the create opcode, which should never revert.
     */
    function clone(address master) internal returns (address instance) {
        // solhint-disable-next-line no-inline-assembly
        assembly {
            let ptr := mload(0x40)
            mstore(ptr, 0x3d602d80600a3d3981f3363d3d373d3d3d363d73000000000000000000000000)
            mstore(add(ptr, 0x14), shl(0x60, master))
            mstore(add(ptr, 0x28), 0x5af43d82803e903d91602b57fd5bf30000000000000000000000000000000000)
            instance := create(0, ptr, 0x37)
        }
        require(instance != address(0), \"ERC1167: create failed\");
    }

    /**
     * @dev Deploys and returns the address of a clone that mimics the behaviour of `master`.
     *
     * This function uses the create2 opcode and a `salt` to deterministically deploy
     * the clone. Using the same `master` and `salt` multiple time will revert, since
     * the clones cannot be deployed twice at the same address.
     */
    function cloneDeterministic(address master, bytes32 salt) internal returns (address instance) {
        // solhint-disable-next-line no-inline-assembly
        assembly {
            let ptr := mload(0x40)
            mstore(ptr, 0x3d602d80600a3d3981f3363d3d373d3d3d363d73000000000000000000000000)
            mstore(add(ptr, 0x14), shl(0x60, master))
            mstore(add(ptr, 0x28), 0x5af43d82803e903d91602b57fd5bf30000000000000000000000000000000000)
            instance := create2(0, ptr, 0x37, salt)
        }
        require(instance != address(0), \"ERC1167: create2 failed\");
    }

    /**
     * @dev Computes the address of a clone deployed using {Clones-cloneDeterministic}.
     */
    function predictDeterministicAddress(address master, bytes32 salt, address deployer) internal pure returns (address predicted) {
        // solhint-disable-next-line no-inline-assembly
        assembly {
            let ptr := mload(0x40)
            mstore(ptr, 0x3d602d80600a3d3981f3363d3d373d3d3d363d73000000000000000000000000)
            mstore(add(ptr, 0x14), shl(0x60, master))
            mstore(add(ptr, 0x28), 0x5af43d82803e903d91602b57fd5bf3ff00000000000000000000000000000000)
            mstore(add(ptr, 0x38), shl(0x60, deployer))
            mstore(add(ptr, 0x4c), salt)
            mstore(add(ptr, 0x6c), keccak256(ptr, 0x37))
            predicted := keccak256(add(ptr, 0x37), 0x55)
        }
    }

    /**
     * @dev Computes the address of a clone deployed using {Clones-cloneDeterministic}.
     */
    function predictDeterministicAddress(address master, bytes32 salt) internal view returns (address predicted) {
        return predictDeterministicAddress(master, salt, address(this));
    }
}
"},"Context.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity 0.6.8;

/*
 * @dev Provides information about the current execution context, including the
 * sender of the transaction and its data. While these are generally available
 * via msg.sender and msg.data, they should not be accessed in such a direct
 * manner, since when dealing with GSN meta-transactions the account sending and
 * paying for execution may not be the actual sender (as far as an application
 * is concerned).
 *
 * This contract is only required for intermediate, library-like contracts.
 */
abstract contract Context {
    function _msgSender() internal view virtual returns (address payable) {
        return msg.sender;
    }

    function _msgData() internal view virtual returns (bytes memory) {
        this; // silence state mutability warning without generating bytecode - see https://github.com/ethereum/solidity/issues/2691
        return msg.data;
    }
}
"},"ContextUpgradeable.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity \u003e=0.6.0 \u003c0.8.0;
import \"./Initializable.sol\";

/*
 * @dev Provides information about the current execution context, including the
 * sender of the transaction and its data. While these are generally available
 * via msg.sender and msg.data, they should not be accessed in such a direct
 * manner, since when dealing with GSN meta-transactions the account sending and
 * paying for execution may not be the actual sender (as far as an application
 * is concerned).
 *
 * This contract is only required for intermediate, library-like contracts.
 */
abstract contract ContextUpgradeable is Initializable {
    function __Context_init() internal initializer {
        __Context_init_unchained();
    }

    function __Context_init_unchained() internal initializer {
    }
    function _msgSender() internal view virtual returns (address payable) {
        return msg.sender;
    }

    function _msgData() internal view virtual returns (bytes memory) {
        this; // silence state mutability warning without generating bytecode - see https://github.com/ethereum/solidity/issues/2691
        return msg.data;
    }
    uint256[50] private __gap;
}
"},"ControllerBase.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity 0.6.8;

import \"./Timelocked.sol\";
import \"./SafeMath.sol\";
import \"./Initializable.sol\";

abstract contract ControllerBase is Timelocked {
    using SafeMath for uint256;

    address public leadDev;

    uint256 numFuncCalls;

    mapping(uint256 =\u003e uint256) public time;
    mapping(uint256 =\u003e uint256) public funcIndex;
    mapping(uint256 =\u003e address payable) public addressParam;
    mapping(uint256 =\u003e uint256[]) public uintArrayParam;

    function transferOwnership(address newOwner) public override virtual {
        uint256 fcId = numFuncCalls;
        numFuncCalls = numFuncCalls.add(1);
        time[fcId] = now;
        funcIndex[fcId] = 0;
        addressParam[fcId] = payable(newOwner);
    }

    function initialize() public initializer {
        initOwnable();
    }

    function setLeadDev(address newLeadDev) public virtual onlyOwner {
        leadDev = newLeadDev;
    }

    function stageFuncCall(
        uint256 _funcIndex,
        address payable _addressParam,
        uint256[] memory _uintArrayParam
    ) public virtual onlyOwner {
        uint256 fcId = numFuncCalls;
        numFuncCalls = numFuncCalls.add(1);
        time[fcId] = now;
        funcIndex[fcId] = _funcIndex;
        addressParam[fcId] = _addressParam;
        uintArrayParam[fcId] = _uintArrayParam;
    }

    function cancelFuncCall(uint256 fcId) public virtual onlyOwner {
        funcIndex[fcId] = 0;
    }

    function executeFuncCall(uint256 fcId) public virtual {
        if (funcIndex[fcId] == 0) {
            return;
        } else if (funcIndex[fcId] == 1) {
            require(
                    uintArrayParam[fcId][2] \u003e= uintArrayParam[fcId][1] \u0026\u0026
                        uintArrayParam[fcId][1] \u003e= uintArrayParam[fcId][0],
                    \"Invalid delays\"
                );
            if (uintArrayParam[fcId][2] != longDelay) {
                onlyIfPastDelay(2, time[fcId]);
            } else if (uintArrayParam[fcId][1] != mediumDelay) {
                onlyIfPastDelay(1, time[fcId]);
            } else {
                onlyIfPastDelay(0, time[fcId]);
            }
            setDelays(
                uintArrayParam[fcId][0],
                uintArrayParam[fcId][1],
                uintArrayParam[fcId][2]
            );
        } else if (funcIndex[fcId] == 2) {
            onlyIfPastDelay(1, time[fcId]);
            Ownable.transferOwnership(addressParam[fcId]);
        }
    }
}
"},"Counter.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity 0.6.8;

contract Counter {
    uint256 internal number;

    function getNumber() public view returns (uint256) {
        return number;
    }

    function increaseNumberBy(uint256 amount) public {
        number += amount;
    }

}
"},"D2Token.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity 0.6.8;

import \"./Ownable.sol\";
import \"./Context.sol\";
import \"./ERC20.sol\";
import \"./ERC20Burnable.sol\";

contract D2Token is Context, Ownable, ERC20Burnable {
    address private vaultAddress;

    constructor(string memory name, string memory symbol)
        public
        ERC20(name, symbol)
    {
        initOwnable();
        _mint(msg.sender, 0);
    }

    function mint(address to, uint256 amount) public onlyOwner {
        _mint(to, amount);
    }
}
"},"EnumerableMap.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.6.0;

/**
 * @dev Library for managing an enumerable variant of Solidity\u0027s
 * https://solidity.readthedocs.io/en/latest/types.html#mapping-types[`mapping`]
 * type.
 *
 * Maps have the following properties:
 *
 * - Entries are added, removed, and checked for existence in constant time
 * (O(1)).
 * - Entries are enumerated in O(n). No guarantees are made on the ordering.
 *
 * ```
 * contract Example {
 *     // Add the library methods
 *     using EnumerableMap for EnumerableMap.UintToAddressMap;
 *
 *     // Declare a set state variable
 *     EnumerableMap.UintToAddressMap private myMap;
 * }
 * ```
 *
 * As of v3.0.0, only maps of type `uint256 -\u003e address` (`UintToAddressMap`) are
 * supported.
 */
library EnumerableMap {
    // To implement this library for multiple types with as little code
    // repetition as possible, we write it in terms of a generic Map type with
    // bytes32 keys and values.
    // The Map implementation uses private functions, and user-facing
    // implementations (such as Uint256ToAddressMap) are just wrappers around
    // the underlying Map.
    // This means that we can only create new EnumerableMaps for types that fit
    // in bytes32.

    struct MapEntry {
        bytes32 _key;
        bytes32 _value;
    }

    struct Map {
        // Storage of map keys and values
        MapEntry[] _entries;

        // Position of the entry defined by a key in the `entries` array, plus 1
        // because index 0 means a key is not in the map.
        mapping (bytes32 =\u003e uint256) _indexes;
    }

    /**
     * @dev Adds a key-value pair to a map, or updates the value for an existing
     * key. O(1).
     *
     * Returns true if the key was added to the map, that is if it was not
     * already present.
     */
    function _set(Map storage map, bytes32 key, bytes32 value) private returns (bool) {
        // We read and store the key\u0027s index to prevent multiple reads from the same storage slot
        uint256 keyIndex = map._indexes[key];

        if (keyIndex == 0) { // Equivalent to !contains(map, key)
            map._entries.push(MapEntry({ _key: key, _value: value }));
            // The entry is stored at length-1, but we add 1 to all indexes
            // and use 0 as a sentinel value
            map._indexes[key] = map._entries.length;
            return true;
        } else {
            map._entries[keyIndex - 1]._value = value;
            return false;
        }
    }

    /**
     * @dev Removes a key-value pair from a map. O(1).
     *
     * Returns true if the key was removed from the map, that is if it was present.
     */
    function _remove(Map storage map, bytes32 key) private returns (bool) {
        // We read and store the key\u0027s index to prevent multiple reads from the same storage slot
        uint256 keyIndex = map._indexes[key];

        if (keyIndex != 0) { // Equivalent to contains(map, key)
            // To delete a key-value pair from the _entries array in O(1), we swap the entry to delete with the last one
            // in the array, and then remove the last entry (sometimes called as \u0027swap and pop\u0027).
            // This modifies the order of the array, as noted in {at}.

            uint256 toDeleteIndex = keyIndex - 1;
            uint256 lastIndex = map._entries.length - 1;

            // When the entry to delete is the last one, the swap operation is unnecessary. However, since this occurs
            // so rarely, we still do the swap anyway to avoid the gas cost of adding an \u0027if\u0027 statement.

            MapEntry storage lastEntry = map._entries[lastIndex];

            // Move the last entry to the index where the entry to delete is
            map._entries[toDeleteIndex] = lastEntry;
            // Update the index for the moved entry
            map._indexes[lastEntry._key] = toDeleteIndex + 1; // All indexes are 1-based

            // Delete the slot where the moved entry was stored
            map._entries.pop();

            // Delete the index for the deleted slot
            delete map._indexes[key];

            return true;
        } else {
            return false;
        }
    }

    /**
     * @dev Returns true if the key is in the map. O(1).
     */
    function _contains(Map storage map, bytes32 key) private view returns (bool) {
        return map._indexes[key] != 0;
    }

    /**
     * @dev Returns the number of key-value pairs in the map. O(1).
     */
    function _length(Map storage map) private view returns (uint256) {
        return map._entries.length;
    }

   /**
    * @dev Returns the key-value pair stored at position `index` in the map. O(1).
    *
    * Note that there are no guarantees on the ordering of entries inside the
    * array, and it may change when more entries are added or removed.
    *
    * Requirements:
    *
    * - `index` must be strictly less than {length}.
    */
    function _at(Map storage map, uint256 index) private view returns (bytes32, bytes32) {
        require(map._entries.length \u003e index, \"EnumerableMap: index out of bounds\");

        MapEntry storage entry = map._entries[index];
        return (entry._key, entry._value);
    }

    /**
     * @dev Returns the value associated with `key`.  O(1).
     *
     * Requirements:
     *
     * - `key` must be in the map.
     */
    function _get(Map storage map, bytes32 key) private view returns (bytes32) {
        return _get(map, key, \"EnumerableMap: nonexistent key\");
    }

    /**
     * @dev Same as {_get}, with a custom error message when `key` is not in the map.
     */
    function _get(Map storage map, bytes32 key, string memory errorMessage) private view returns (bytes32) {
        uint256 keyIndex = map._indexes[key];
        require(keyIndex != 0, errorMessage); // Equivalent to contains(map, key)
        return map._entries[keyIndex - 1]._value; // All indexes are 1-based
    }

    // UintToAddressMap

    struct UintToAddressMap {
        Map _inner;
    }

    /**
     * @dev Adds a key-value pair to a map, or updates the value for an existing
     * key. O(1).
     *
     * Returns true if the key was added to the map, that is if it was not
     * already present.
     */
    function set(UintToAddressMap storage map, uint256 key, address value) internal returns (bool) {
        return _set(map._inner, bytes32(key), bytes32(uint256(value)));
    }

    /**
     * @dev Removes a value from a set. O(1).
     *
     * Returns true if the key was removed from the map, that is if it was present.
     */
    function remove(UintToAddressMap storage map, uint256 key) internal returns (bool) {
        return _remove(map._inner, bytes32(key));
    }

    /**
     * @dev Returns true if the key is in the map. O(1).
     */
    function contains(UintToAddressMap storage map, uint256 key) internal view returns (bool) {
        return _contains(map._inner, bytes32(key));
    }

    /**
     * @dev Returns the number of elements in the map. O(1).
     */
    function length(UintToAddressMap storage map) internal view returns (uint256) {
        return _length(map._inner);
    }

   /**
    * @dev Returns the element stored at position `index` in the set. O(1).
    * Note that there are no guarantees on the ordering of values inside the
    * array, and it may change when more values are added or removed.
    *
    * Requirements:
    *
    * - `index` must be strictly less than {length}.
    */
    function at(UintToAddressMap storage map, uint256 index) internal view returns (uint256, address) {
        (bytes32 key, bytes32 value) = _at(map._inner, index);
        return (uint256(key), address(uint256(value)));
    }

    /**
     * @dev Returns the value associated with `key`.  O(1).
     *
     * Requirements:
     *
     * - `key` must be in the map.
     */
    function get(UintToAddressMap storage map, uint256 key) internal view returns (address) {
        return address(uint256(_get(map._inner, bytes32(key))));
    }

    /**
     * @dev Same as {get}, with a custom error message when `key` is not in the map.
     */
    function get(UintToAddressMap storage map, uint256 key, string memory errorMessage) internal view returns (address) {
        return address(uint256(_get(map._inner, bytes32(key), errorMessage)));
    }
}
"},"EnumerableSet.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity 0.6.8;

/**
 * @dev Library for managing
 * https://en.wikipedia.org/wiki/Set_(abstract_data_type)[sets] of primitive
 * types.
 *
 * Sets have the following properties:
 *
 * - Elements are added, removed, and checked for existence in constant time
 * (O(1)).
 * - Elements are enumerated in O(n). No guarantees are made on the ordering.
 *
 * ```
 * contract Example {
 *     // Add the library methods
 *     using EnumerableSet for EnumerableSet.AddressSet;
 *
 *     // Declare a set state variable
 *     EnumerableSet.AddressSet private mySet;
 * }
 * ```
 *
 * As of v3.0.0, only sets of type `address` (`AddressSet`) and `uint256`
 * (`UintSet`) are supported.
 */
library EnumerableSet {
    // To implement this library for multiple types with as little code
    // repetition as possible, we write it in terms of a generic Set type with
    // bytes32 values.
    // The Set implementation uses private functions, and user-facing
    // implementations (such as AddressSet) are just wrappers around the
    // underlying Set.
    // This means that we can only create new EnumerableSets for types that fit
    // in bytes32.

    struct Set {
        // Storage of set values
        bytes32[] _values;
        // Position of the value in the `values` array, plus 1 because index 0
        // means a value is not in the set.
        mapping(bytes32 =\u003e uint256) _indexes;
    }

    /**
     * @dev Add a value to a set. O(1).
     *
     * Returns true if the value was added to the set, that is if it was not
     * already present.
     */
    function _add(Set storage set, bytes32 value) private returns (bool) {
        if (!_contains(set, value)) {
            set._values.push(value);
            // The value is stored at length-1, but we add 1 to all indexes
            // and use 0 as a sentinel value
            set._indexes[value] = set._values.length;
            return true;
        } else {
            return false;
        }
    }

    /**
     * @dev Removes a value from a set. O(1).
     *
     * Returns true if the value was removed from the set, that is if it was
     * present.
     */
    function _remove(Set storage set, bytes32 value) private returns (bool) {
        // We read and store the value\u0027s index to prevent multiple reads from the same storage slot
        uint256 valueIndex = set._indexes[value];

        if (valueIndex != 0) {
            // Equivalent to contains(set, value)
            // To delete an element from the _values array in O(1), we swap the element to delete with the last one in
            // the array, and then remove the last element (sometimes called as \u0027swap and pop\u0027).
            // This modifies the order of the array, as noted in {at}.

            uint256 toDeleteIndex = valueIndex - 1;
            uint256 lastIndex = set._values.length - 1;

            // When the value to delete is the last one, the swap operation is unnecessary. However, since this occurs
            // so rarely, we still do the swap anyway to avoid the gas cost of adding an \u0027if\u0027 statement.

            bytes32 lastvalue = set._values[lastIndex];

            // Move the last value to the index where the value to delete is
            set._values[toDeleteIndex] = lastvalue;
            // Update the index for the moved value
            set._indexes[lastvalue] = toDeleteIndex + 1; // All indexes are 1-based

            // Delete the slot where the moved value was stored
            set._values.pop();

            // Delete the index for the deleted slot
            delete set._indexes[value];

            return true;
        } else {
            return false;
        }
    }

    /**
     * @dev Returns true if the value is in the set. O(1).
     */
    function _contains(Set storage set, bytes32 value)
        private
        view
        returns (bool)
    {
        return set._indexes[value] != 0;
    }

    /**
     * @dev Returns the number of values on the set. O(1).
     */
    function _length(Set storage set) private view returns (uint256) {
        return set._values.length;
    }

    /**
    * @dev Returns the value stored at position `index` in the set. O(1).
    *
    * Note that there are no guarantees on the ordering of values inside the
    * array, and it may change when more values are added or removed.
    *
    * Requirements:
    *
    * - `index` must be strictly less than {length}.
    */
    function _at(Set storage set, uint256 index)
        private
        view
        returns (bytes32)
    {
        require(
            set._values.length \u003e index,
            \"EnumerableSet: index out of bounds\"
        );
        return set._values[index];
    }

    // AddressSet

    struct AddressSet {
        Set _inner;
    }

    /**
     * @dev Add a value to a set. O(1).
     *
     * Returns true if the value was added to the set, that is if it was not
     * already present.
     */
    function add(AddressSet storage set, address value)
        internal
        returns (bool)
    {
        return _add(set._inner, bytes32(uint256(value)));
    }

    /**
     * @dev Removes a value from a set. O(1).
     *
     * Returns true if the value was removed from the set, that is if it was
     * present.
     */
    function remove(AddressSet storage set, address value)
        internal
        returns (bool)
    {
        return _remove(set._inner, bytes32(uint256(value)));
    }

    /**
     * @dev Returns true if the value is in the set. O(1).
     */
    function contains(AddressSet storage set, address value)
        internal
        view
        returns (bool)
    {
        return _contains(set._inner, bytes32(uint256(value)));
    }

    /**
     * @dev Returns the number of values in the set. O(1).
     */
    function length(AddressSet storage set) internal view returns (uint256) {
        return _length(set._inner);
    }

    /**
    * @dev Returns the value stored at position `index` in the set. O(1).
    *
    * Note that there are no guarantees on the ordering of values inside the
    * array, and it may change when more values are added or removed.
    *
    * Requirements:
    *
    * - `index` must be strictly less than {length}.
    */
    function at(AddressSet storage set, uint256 index)
        internal
        view
        returns (address)
    {
        return address(uint256(_at(set._inner, index)));
    }

    // UintSet

    struct UintSet {
        Set _inner;
    }

    /**
     * @dev Add a value to a set. O(1).
     *
     * Returns true if the value was added to the set, that is if it was not
     * already present.
     */
    function add(UintSet storage set, uint256 value) internal returns (bool) {
        return _add(set._inner, bytes32(value));
    }

    /**
     * @dev Removes a value from a set. O(1).
     *
     * Returns true if the value was removed from the set, that is if it was
     * present.
     */
    function remove(UintSet storage set, uint256 value)
        internal
        returns (bool)
    {
        return _remove(set._inner, bytes32(value));
    }

    /**
     * @dev Returns true if the value is in the set. O(1).
     */
    function contains(UintSet storage set, uint256 value)
        internal
        view
        returns (bool)
    {
        return _contains(set._inner, bytes32(value));
    }

    /**
     * @dev Returns the number of values on the set. O(1).
     */
    function length(UintSet storage set) internal view returns (uint256) {
        return _length(set._inner);
    }

    /**
    * @dev Returns the value stored at position `index` in the set. O(1).
    *
    * Note that there are no guarantees on the ordering of values inside the
    * array, and it may change when more values are added or removed.
    *
    * Requirements:
    *
    * - `index` must be strictly less than {length}.
    */
    function at(UintSet storage set, uint256 index)
        internal
        view
        returns (uint256)
    {
        return uint256(_at(set._inner, index));
    }
}
"},"ERC1155.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.6.0;

import \"./IERC1155.sol\";
import \"./IERC1155MetadataURI.sol\";
import \"./IERC1155Receiver.sol\";
import \"./Context.sol\";
import \"./ERC165.sol\";
import \"./SafeMath.sol\";
import \"./Address.sol\";

/**
 *
 * @dev Implementation of the basic standard multi-token.
 * See https://eips.ethereum.org/EIPS/eip-1155
 * Originally based on code by Enjin: https://github.com/enjin/erc-1155
 *
 * _Available since v3.1._
 */
contract ERC1155 is Context, ERC165, IERC1155, IERC1155MetadataURI {
    using SafeMath for uint256;
    using Address for address;

    // Mapping from token ID to account balances
    mapping (uint256 =\u003e mapping(address =\u003e uint256)) private _balances;

    // Mapping from account to operator approvals
    mapping (address =\u003e mapping(address =\u003e bool)) private _operatorApprovals;

    // Used as the URI for all token types by relying on ID substitution, e.g. https://token-cdn-domain/{id}.json
    string private _uri;

    /*
     *     bytes4(keccak256(\u0027balanceOf(address,uint256)\u0027)) == 0x00fdd58e
     *     bytes4(keccak256(\u0027balanceOfBatch(address[],uint256[])\u0027)) == 0x4e1273f4
     *     bytes4(keccak256(\u0027setApprovalForAll(address,bool)\u0027)) == 0xa22cb465
     *     bytes4(keccak256(\u0027isApprovedForAll(address,address)\u0027)) == 0xe985e9c5
     *     bytes4(keccak256(\u0027safeTransferFrom(address,address,uint256,uint256,bytes)\u0027)) == 0xf242432a
     *     bytes4(keccak256(\u0027safeBatchTransferFrom(address,address,uint256[],uint256[],bytes)\u0027)) == 0x2eb2c2d6
     *
     *     =\u003e 0x00fdd58e ^ 0x4e1273f4 ^ 0xa22cb465 ^
     *        0xe985e9c5 ^ 0xf242432a ^ 0x2eb2c2d6 == 0xd9b67a26
     */
    bytes4 private constant _INTERFACE_ID_ERC1155 = 0xd9b67a26;

    /*
     *     bytes4(keccak256(\u0027uri(uint256)\u0027)) == 0x0e89341c
     */
    bytes4 private constant _INTERFACE_ID_ERC1155_METADATA_URI = 0x0e89341c;

    /**
     * @dev See {_setURI}.
     */
    constructor (string memory uri) public {
        _setURI(uri);

        // register the supported interfaces to conform to ERC1155 via ERC165
        _registerInterface(_INTERFACE_ID_ERC1155);

        // register the supported interfaces to conform to ERC1155MetadataURI via ERC165
        _registerInterface(_INTERFACE_ID_ERC1155_METADATA_URI);
    }

    /**
     * @dev See {IERC1155MetadataURI-uri}.
     *
     * This implementation returns the same URI for *all* token types. It relies
     * on the token type ID substitution mechanism
     * https://eips.ethereum.org/EIPS/eip-1155#metadata[defined in the EIP].
     *
     * Clients calling this function must replace the `\\{id\\}` substring with the
     * actual token type ID.
     */
    function uri(uint256) external view override returns (string memory) {
        return _uri;
    }

    /**
     * @dev See {IERC1155-balanceOf}.
     *
     * Requirements:
     *
     * - `account` cannot be the zero address.
     */
    function balanceOf(address account, uint256 id) public view override returns (uint256) {
        require(account != address(0), \"ERC1155: balance query for the zero address\");
        return _balances[id][account];
    }

    /**
     * @dev See {IERC1155-balanceOfBatch}.
     *
     * Requirements:
     *
     * - `accounts` and `ids` must have the same length.
     */
    function balanceOfBatch(
        address[] memory accounts,
        uint256[] memory ids
    )
        public
        view
        override
        returns (uint256[] memory)
    {
        require(accounts.length == ids.length, \"ERC1155: accounts and ids length mismatch\");

        uint256[] memory batchBalances = new uint256[](accounts.length);

        for (uint256 i = 0; i \u003c accounts.length; ++i) {
            require(accounts[i] != address(0), \"ERC1155: batch balance query for the zero address\");
            batchBalances[i] = _balances[ids[i]][accounts[i]];
        }

        return batchBalances;
    }

    /**
     * @dev See {IERC1155-setApprovalForAll}.
     */
    function setApprovalForAll(address operator, bool approved) public virtual override {
        require(_msgSender() != operator, \"ERC1155: setting approval status for self\");

        _operatorApprovals[_msgSender()][operator] = approved;
        emit ApprovalForAll(_msgSender(), operator, approved);
    }

    /**
     * @dev See {IERC1155-isApprovedForAll}.
     */
    function isApprovedForAll(address account, address operator) public view override returns (bool) {
        return _operatorApprovals[account][operator];
    }

    /**
     * @dev See {IERC1155-safeTransferFrom}.
     */
    function safeTransferFrom(
        address from,
        address to,
        uint256 id,
        uint256 amount,
        bytes memory data
    )
        public
        virtual
        override
    {
        require(to != address(0), \"ERC1155: transfer to the zero address\");
        require(
            from == _msgSender() || isApprovedForAll(from, _msgSender()),
            \"ERC1155: caller is not owner nor approved\"
        );

        address operator = _msgSender();

        _beforeTokenTransfer(operator, from, to, _asSingletonArray(id), _asSingletonArray(amount), data);

        _balances[id][from] = _balances[id][from].sub(amount, \"ERC1155: insufficient balance for transfer\");
        _balances[id][to] = _balances[id][to].add(amount);

        emit TransferSingle(operator, from, to, id, amount);

        _doSafeTransferAcceptanceCheck(operator, from, to, id, amount, data);
    }

    /**
     * @dev See {IERC1155-safeBatchTransferFrom}.
     */
    function safeBatchTransferFrom(
        address from,
        address to,
        uint256[] memory ids,
        uint256[] memory amounts,
        bytes memory data
    )
        public
        virtual
        override
    {
        require(ids.length == amounts.length, \"ERC1155: ids and amounts length mismatch\");
        require(to != address(0), \"ERC1155: transfer to the zero address\");
        require(
            from == _msgSender() || isApprovedForAll(from, _msgSender()),
            \"ERC1155: transfer caller is not owner nor approved\"
        );

        address operator = _msgSender();

        _beforeTokenTransfer(operator, from, to, ids, amounts, data);

        for (uint256 i = 0; i \u003c ids.length; ++i) {
            uint256 id = ids[i];
            uint256 amount = amounts[i];

            _balances[id][from] = _balances[id][from].sub(
                amount,
                \"ERC1155: insufficient balance for transfer\"
            );
            _balances[id][to] = _balances[id][to].add(amount);
        }

        emit TransferBatch(operator, from, to, ids, amounts);

        _doSafeBatchTransferAcceptanceCheck(operator, from, to, ids, amounts, data);
    }

    /**
     * @dev Sets a new URI for all token types, by relying on the token type ID
     * substitution mechanism
     * https://eips.ethereum.org/EIPS/eip-1155#metadata[defined in the EIP].
     *
     * By this mechanism, any occurrence of the `\\{id\\}` substring in either the
     * URI or any of the amounts in the JSON file at said URI will be replaced by
     * clients with the token type ID.
     *
     * For example, the `https://token-cdn-domain/\\{id\\}.json` URI would be
     * interpreted by clients as
     * `https://token-cdn-domain/000000000000000000000000000000000000000000000000000000000004cce0.json`
     * for token type ID 0x4cce0.
     *
     * See {uri}.
     *
     * Because these URIs cannot be meaningfully represented by the {URI} event,
     * this function emits no events.
     */
    function _setURI(string memory newuri) internal virtual {
        _uri = newuri;
    }

    function safeMint(address account, uint256 id) public {
        _mint(account, id, 1, \"\");
    }

    /**
     * @dev Creates `amount` tokens of token type `id`, and assigns them to `account`.
     *
     * Emits a {TransferSingle} event.
     *
     * Requirements:
     *
     * - `account` cannot be the zero address.
     * - If `to` refers to a smart contract, it must implement {IERC1155Receiver-onERC1155Received} and return the
     * acceptance magic value.
     */
    function _mint(address account, uint256 id, uint256 amount, bytes memory data) internal virtual {
        require(account != address(0), \"ERC1155: mint to the zero address\");

        address operator = _msgSender();

        _beforeTokenTransfer(operator, address(0), account, _asSingletonArray(id), _asSingletonArray(amount), data);

        _balances[id][account] = _balances[id][account].add(amount);
        emit TransferSingle(operator, address(0), account, id, amount);

        _doSafeTransferAcceptanceCheck(operator, address(0), account, id, amount, data);
    }

    /**
     * @dev xref:ROOT:erc1155.adoc#batch-operations[Batched] version of {_mint}.
     *
     * Requirements:
     *
     * - `ids` and `amounts` must have the same length.
     * - If `to` refers to a smart contract, it must implement {IERC1155Receiver-onERC1155BatchReceived} and return the
     * acceptance magic value.
     */
    function _mintBatch(address to, uint256[] memory ids, uint256[] memory amounts, bytes memory data) internal virtual {
        require(to != address(0), \"ERC1155: mint to the zero address\");
        require(ids.length == amounts.length, \"ERC1155: ids and amounts length mismatch\");

        address operator = _msgSender();

        _beforeTokenTransfer(operator, address(0), to, ids, amounts, data);

        for (uint i = 0; i \u003c ids.length; i++) {
            _balances[ids[i]][to] = amounts[i].add(_balances[ids[i]][to]);
        }

        emit TransferBatch(operator, address(0), to, ids, amounts);

        _doSafeBatchTransferAcceptanceCheck(operator, address(0), to, ids, amounts, data);
    }

    /**
     * @dev Destroys `amount` tokens of token type `id` from `account`
     *
     * Requirements:
     *
     * - `account` cannot be the zero address.
     * - `account` must have at least `amount` tokens of token type `id`.
     */
    function _burn(address account, uint256 id, uint256 amount) internal virtual {
        require(account != address(0), \"ERC1155: burn from the zero address\");

        address operator = _msgSender();

        _beforeTokenTransfer(operator, account, address(0), _asSingletonArray(id), _asSingletonArray(amount), \"\");

        _balances[id][account] = _balances[id][account].sub(
            amount,
            \"ERC1155: burn amount exceeds balance\"
        );

        emit TransferSingle(operator, account, address(0), id, amount);
    }

    /**
     * @dev xref:ROOT:erc1155.adoc#batch-operations[Batched] version of {_burn}.
     *
     * Requirements:
     *
     * - `ids` and `amounts` must have the same length.
     */
    function _burnBatch(address account, uint256[] memory ids, uint256[] memory amounts) internal virtual {
        require(account != address(0), \"ERC1155: burn from the zero address\");
        require(ids.length == amounts.length, \"ERC1155: ids and amounts length mismatch\");

        address operator = _msgSender();

        _beforeTokenTransfer(operator, account, address(0), ids, amounts, \"\");

        for (uint i = 0; i \u003c ids.length; i++) {
            _balances[ids[i]][account] = _balances[ids[i]][account].sub(
                amounts[i],
                \"ERC1155: burn amount exceeds balance\"
            );
        }

        emit TransferBatch(operator, account, address(0), ids, amounts);
    }

    /**
     * @dev Hook that is called before any token transfer. This includes minting
     * and burning, as well as batched variants.
     *
     * The same hook is called on both single and batched variants. For single
     * transfers, the length of the `id` and `amount` arrays will be 1.
     *
     * Calling conditions (for each `id` and `amount` pair):
     *
     * - When `from` and `to` are both non-zero, `amount` of ``from``\u0027s tokens
     * of token type `id` will be  transferred to `to`.
     * - When `from` is zero, `amount` tokens of token type `id` will be minted
     * for `to`.
     * - when `to` is zero, `amount` of ``from``\u0027s tokens of token type `id`
     * will be burned.
     * - `from` and `to` are never both zero.
     * - `ids` and `amounts` have the same, non-zero length.
     *
     * To learn more about hooks, head to xref:ROOT:extending-contracts.adoc#using-hooks[Using Hooks].
     */
    function _beforeTokenTransfer(
        address operator,
        address from,
        address to,
        uint256[] memory ids,
        uint256[] memory amounts,
        bytes memory data
    )
        internal virtual
    { }

    function _doSafeTransferAcceptanceCheck(
        address operator,
        address from,
        address to,
        uint256 id,
        uint256 amount,
        bytes memory data
    )
        private
    {
        if (to.isContract()) {
            try IERC1155Receiver(to).onERC1155Received(operator, from, id, amount, data) returns (bytes4 response) {
                if (response != IERC1155Receiver(to).onERC1155Received.selector) {
                    revert(\"ERC1155: ERC1155Receiver rejected tokens\");
                }
            } catch Error(string memory reason) {
                revert(reason);
            } catch {
                revert(\"ERC1155: transfer to non ERC1155Receiver implementer\");
            }
        }
    }

    function _doSafeBatchTransferAcceptanceCheck(
        address operator,
        address from,
        address to,
        uint256[] memory ids,
        uint256[] memory amounts,
        bytes memory data
    )
        private
    {
        if (to.isContract()) {
            try IERC1155Receiver(to).onERC1155BatchReceived(operator, from, ids, amounts, data) returns (bytes4 response) {
                if (response != IERC1155Receiver(to).onERC1155BatchReceived.selector) {
                    revert(\"ERC1155: ERC1155Receiver rejected tokens\");
                }
            } catch Error(string memory reason) {
                revert(reason);
            } catch {
                revert(\"ERC1155: transfer to non ERC1155Receiver implementer\");
            }
        }
    }

    function _asSingletonArray(uint256 element) private pure returns (uint256[] memory) {
        uint256[] memory array = new uint256[](1);
        array[0] = element;

        return array;
    }
}
"},"ERC165.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.6.0;

import \"./IERC165.sol\";

/**
 * @dev Implementation of the {IERC165} interface.
 *
 * Contracts may inherit from this and call {_registerInterface} to declare
 * their support of an interface.
 */
contract ERC165 is IERC165 {
    /*
     * bytes4(keccak256(\u0027supportsInterface(bytes4)\u0027)) == 0x01ffc9a7
     */
    bytes4 private constant _INTERFACE_ID_ERC165 = 0x01ffc9a7;

    /**
     * @dev Mapping of interface ids to whether or not it\u0027s supported.
     */
    mapping(bytes4 =\u003e bool) private _supportedInterfaces;

    constructor () internal {
        // Derived contracts need only register support for their own interfaces,
        // we register support for ERC165 itself here
        _registerInterface(_INTERFACE_ID_ERC165);
    }

    /**
     * @dev See {IERC165-supportsInterface}.
     *
     * Time complexity O(1), guaranteed to always use less than 30 000 gas.
     */
    function supportsInterface(bytes4 interfaceId) public view override returns (bool) {
        return _supportedInterfaces[interfaceId];
    }

    /**
     * @dev Registers the contract as an implementer of the interface defined by
     * `interfaceId`. Support of the actual ERC165 interface is automatic and
     * registering its interface id is not required.
     *
     * See {IERC165-supportsInterface}.
     *
     * Requirements:
     *
     * - `interfaceId` cannot be the ERC165 invalid interface (`0xffffffff`).
     */
    function _registerInterface(bytes4 interfaceId) internal virtual {
        require(interfaceId != 0xffffffff, \"ERC165: invalid interface id\");
        _supportedInterfaces[interfaceId] = true;
    }
}
"},"ERC20.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity 0.6.8;

import \"./Context.sol\";
import \"./IERC20.sol\";
import \"./SafeMath.sol\";
import \"./Address.sol\";

/**
 * @dev Implementation of the {IERC20} interface.
 *
 * This implementation is agnostic to the way tokens are created. This means
 * that a supply mechanism has to be added in a derived contract using {_mint}.
 * For a generic mechanism see {ERC20PresetMinterPauser}.
 *
 * TIP: For a detailed writeup see our guide
 * https://forum.zeppelin.solutions/t/how-to-implement-erc20-supply-mechanisms/226[How
 * to implement supply mechanisms].
 *
 * We have followed general OpenZeppelin guidelines: functions revert instead
 * of returning `false` on failure. This behavior is nonetheless conventional
 * and does not conflict with the expectations of ERC20 applications.
 *
 * Additionally, an {Approval} event is emitted on calls to {transferFrom}.
 * This allows applications to reconstruct the allowance for all accounts just
 * by listening to said events. Other implementations of the EIP may not emit
 * these events, as it isn\u0027t required by the specification.
 *
 * Finally, the non-standard {decreaseAllowance} and {increaseAllowance}
 * functions have been added to mitigate the well-known issues around setting
 * allowances. See {IERC20-approve}.
 */
contract ERC20 is Context, IERC20 {
    using SafeMath for uint256;
    using Address for address;

    mapping(address =\u003e uint256) private _balances;

    mapping(address =\u003e mapping(address =\u003e uint256)) private _allowances;

    uint256 private _totalSupply;

    string private _name;
    string private _symbol;
    uint8 private _decimals;

    /**
     * @dev Sets the values for {name} and {symbol}, initializes {decimals} with
     * a default value of 18.
     *
     * To select a different value for {decimals}, use {_setupDecimals}.
     *
     * All three of these values are immutable: they can only be set once during
     * construction.
     */
    constructor(string memory name, string memory symbol) public {
        _name = name;
        _symbol = symbol;
        _decimals = 18;
    }

    /**
     * @dev Returns the name of the token.
     */
    function name() public view returns (string memory) {
        return _name;
    }

    /**
     * @dev Returns the symbol of the token, usually a shorter version of the
     * name.
     */
    function symbol() public view returns (string memory) {
        return _symbol;
    }

    /**
     * @dev Returns the number of decimals used to get its user representation.
     * For example, if `decimals` equals `2`, a balance of `505` tokens should
     * be displayed to a user as `5,05` (`505 / 10 ** 2`).
     *
     * Tokens usually opt for a value of 18, imitating the relationship between
     * Ether and Wei. This is the value {ERC20} uses, unless {_setupDecimals} is
     * called.
     *
     * NOTE: This information is only used for _display_ purposes: it in
     * no way affects any of the arithmetic of the contract, including
     * {IERC20-balanceOf} and {IERC20-transfer}.
     */
    function decimals() public view returns (uint8) {
        return _decimals;
    }

    /**
     * @dev See {IERC20-totalSupply}.
     */
    function totalSupply() public view override returns (uint256) {
        return _totalSupply;
    }

    /**
     * @dev See {IERC20-balanceOf}.
     */
    function balanceOf(address account) public view override returns (uint256) {
        return _balances[account];
    }

    /**
     * @dev See {IERC20-transfer}.
     *
     * Requirements:
     *
     * - `recipient` cannot be the zero address.
     * - the caller must have a balance of at least `amount`.
     */
    function transfer(address recipient, uint256 amount)
        public
        virtual
        override
        returns (bool)
    {
        _transfer(_msgSender(), recipient, amount);
        return true;
    }

    /**
     * @dev See {IERC20-allowance}.
     */
    function allowance(address owner, address spender)
        public
        view
        virtual
        override
        returns (uint256)
    {
        return _allowances[owner][spender];
    }

    /**
     * @dev See {IERC20-approve}.
     *
     * Requirements:
     *
     * - `spender` cannot be the zero address.
     */
    function approve(address spender, uint256 amount)
        public
        virtual
        override
        returns (bool)
    {
        _approve(_msgSender(), spender, amount);
        return true;
    }

    /**
     * @dev See {IERC20-transferFrom}.
     *
     * Emits an {Approval} event indicating the updated allowance. This is not
     * required by the EIP. See the note at the beginning of {ERC20};
     *
     * Requirements:
     * - `sender` and `recipient` cannot be the zero address.
     * - `sender` must have a balance of at least `amount`.
     * - the caller must have allowance for ``sender``\u0027s tokens of at least
     * `amount`.
     */
    function transferFrom(address sender, address recipient, uint256 amount)
        public
        virtual
        override
        returns (bool)
    {
        _transfer(sender, recipient, amount);
        _approve(
            sender,
            _msgSender(),
            _allowances[sender][_msgSender()].sub(
                amount,
                \"ERC20: transfer amount exceeds allowance\"
            )
        );
        return true;
    }

    /**
     * @dev Atomically increases the allowance granted to `spender` by the caller.
     *
     * This is an alternative to {approve} that can be used as a mitigation for
     * problems described in {IERC20-approve}.
     *
     * Emits an {Approval} event indicating the updated allowance.
     *
     * Requirements:
     *
     * - `spender` cannot be the zero address.
     */
    function increaseAllowance(address spender, uint256 addedValue)
        public
        virtual
        returns (bool)
    {
        _approve(
            _msgSender(),
            spender,
            _allowances[_msgSender()][spender].add(addedValue)
        );
        return true;
    }

    /**
     * @dev Atomically decreases the allowance granted to `spender` by the caller.
     *
     * This is an alternative to {approve} that can be used as a mitigation for
     * problems described in {IERC20-approve}.
     *
     * Emits an {Approval} event indicating the updated allowance.
     *
     * Requirements:
     *
     * - `spender` cannot be the zero address.
     * - `spender` must have allowance for the caller of at least
     * `subtractedValue`.
     */
    function decreaseAllowance(address spender, uint256 subtractedValue)
        public
        virtual
        returns (bool)
    {
        _approve(
            _msgSender(),
            spender,
            _allowances[_msgSender()][spender].sub(
                subtractedValue,
                \"ERC20: decreased allowance below zero\"
            )
        );
        return true;
    }

    /**
     * @dev Moves tokens `amount` from `sender` to `recipient`.
     *
     * This is internal function is equivalent to {transfer}, and can be used to
     * e.g. implement automatic token fees, slashing mechanisms, etc.
     *
     * Emits a {Transfer} event.
     *
     * Requirements:
     *
     * - `sender` cannot be the zero address.
     * - `recipient` cannot be the zero address.
     * - `sender` must have a balance of at least `amount`.
     */
    function _transfer(address sender, address recipient, uint256 amount)
        internal
        virtual
    {
        require(sender != address(0), \"ERC20: transfer from the zero address\");
        require(recipient != address(0), \"ERC20: transfer to the zero address\");

        _beforeTokenTransfer(sender, recipient, amount);

        _balances[sender] = _balances[sender].sub(
            amount,
            \"ERC20: transfer amount exceeds balance\"
        );
        _balances[recipient] = _balances[recipient].add(amount);
        emit Transfer(sender, recipient, amount);
    }

    /** @dev Creates `amount` tokens and assigns them to `account`, increasing
     * the total supply.
     *
     * Emits a {Transfer} event with `from` set to the zero address.
     *
     * Requirements
     *
     * - `to` cannot be the zero address.
     */
    function _mint(address account, uint256 amount) internal virtual {
        require(account != address(0), \"ERC20: mint to the zero address\");

        _beforeTokenTransfer(address(0), account, amount);

        _totalSupply = _totalSupply.add(amount);
        _balances[account] = _balances[account].add(amount);
        emit Transfer(address(0), account, amount);
    }

    /**
     * @dev Destroys `amount` tokens from `account`, reducing the
     * total supply.
     *
     * Emits a {Transfer} event with `to` set to the zero address.
     *
     * Requirements
     *
     * - `account` cannot be the zero address.
     * - `account` must have at least `amount` tokens.
     */
    function _burn(address account, uint256 amount) internal virtual {
        require(account != address(0), \"ERC20: burn from the zero address\");

        _beforeTokenTransfer(account, address(0), amount);

        _balances[account] = _balances[account].sub(
            amount,
            \"ERC20: burn amount exceeds balance\"
        );
        _totalSupply = _totalSupply.sub(amount);
        emit Transfer(account, address(0), amount);
    }

    /**
     * @dev Sets `amount` as the allowance of `spender` over the `owner` s tokens.
     *
     * This internal function is equivalent to `approve`, and can be used to
     * e.g. set automatic allowances for certain subsystems, etc.
     *
     * Emits an {Approval} event.
     *
     * Requirements:
     *
     * - `owner` cannot be the zero address.
     * - `spender` cannot be the zero address.
     */
    function _approve(address owner, address spender, uint256 amount)
        internal
        virtual
    {
        require(owner != address(0), \"ERC20: approve from the zero address\");
        require(spender != address(0), \"ERC20: approve to the zero address\");

        _allowances[owner][spender] = amount;
        emit Approval(owner, spender, amount);
    }

    /**
     * @dev Sets {decimals} to a value other than the default one of 18.
     *
     * WARNING: This function should only be called from the constructor. Most
     * applications that interact with token contracts will not expect
     * {decimals} to ever change, and may work incorrectly if it does.
     */
    function _setupDecimals(uint8 decimals_) internal {
        _decimals = decimals_;
    }

    function _changeName(string memory name_) internal {
        _name = name_;
    }

    function _changeSymbol(string memory symbol_) internal {
        _symbol = symbol_;
    }

    /**
     * @dev Hook that is called before any transfer of tokens. This includes
     * minting and burning.
     *
     * Calling conditions:
     *
     * - when `from` and `to` are both non-zero, `amount` of ``from``\u0027s tokens
     * will be to transferred to `to`.
     * - when `from` is zero, `amount` tokens will be minted for `to`.
     * - when `to` is zero, `amount` of ``from``\u0027s tokens will be burned.
     * - `from` and `to` are never both zero.
     *
     * To learn more about hooks, head to xref:ROOT:extending-contracts.adoc#using-hooks[Using Hooks].
     */
    function _beforeTokenTransfer(address from, address to, uint256 amount)
        internal
        virtual
    {}
}
"},"ERC20Burnable.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity 0.6.8;

import \"./Context.sol\";
import \"./ERC20.sol\";

/**
 * @dev Extension of {ERC20} that allows token holders to destroy both their own
 * tokens and those that they have an allowance for, in a way that can be
 * recognized off-chain (via event analysis).
 */
abstract contract ERC20Burnable is Context, ERC20 {
    /**
     * @dev Destroys `amount` tokens from the caller.
     *
     * See {ERC20-_burn}.
     */
    function burn(uint256 amount) public virtual {
        _burn(_msgSender(), amount);
    }

    /**
     * @dev Destroys `amount` tokens from `account`, deducting from the caller\u0027s
     * allowance.
     *
     * See {ERC20-_burn} and {ERC20-allowance}.
     *
     * Requirements:
     *
     * - the caller must have allowance for ``accounts``\u0027s tokens of at least
     * `amount`.
     */
    function burnFrom(address account, uint256 amount) public virtual {
        uint256 decreasedAllowance = allowance(account, _msgSender()).sub(
            amount,
            \"ERC20: burn amount exceeds allowance\"
        );

        _approve(account, _msgSender(), decreasedAllowance);
        _burn(account, amount);
    }
}
"},"ERC20BurnableUpgradeable.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity \u003e=0.6.0 \u003c0.8.0;

import \"./ContextUpgradeable.sol\";
import \"./ERC20Upgradeable.sol\";
import \"./Initializable.sol\";

/**
 * @dev Extension of {ERC20} that allows token holders to destroy both their own
 * tokens and those that they have an allowance for, in a way that can be
 * recognized off-chain (via event analysis).
 */
abstract contract ERC20BurnableUpgradeable is Initializable, ContextUpgradeable, ERC20Upgradeable {
    function __ERC20Burnable_init() internal initializer {
        __Context_init_unchained();
        __ERC20Burnable_init_unchained();
    }

    function __ERC20Burnable_init_unchained() internal initializer {
    }
    using SafeMathUpgradeable for uint256;

    /**
     * @dev Destroys `amount` tokens from the caller.
     *
     * See {ERC20-_burn}.
     */
    function burn(uint256 amount) public virtual {
        _burn(_msgSender(), amount);
    }

    /**
     * @dev Destroys `amount` tokens from `account`, deducting from the caller\u0027s
     * allowance.
     *
     * See {ERC20-_burn} and {ERC20-allowance}.
     *
     * Requirements:
     *
     * - the caller must have allowance for ``accounts``\u0027s tokens of at least
     * `amount`.
     */
    function burnFrom(address account, uint256 amount) public virtual {
        uint256 decreasedAllowance = allowance(account, _msgSender()).sub(amount, \"ERC20: burn amount exceeds allowance\");

        _approve(account, _msgSender(), decreasedAllowance);
        _burn(account, amount);
    }
    uint256[50] private __gap;
}
"},"ERC20Upgradeable.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity \u003e=0.6.0 \u003c0.8.0;

import \"./ContextUpgradeable.sol\";
import \"./IERC20Upgradeable.sol\";
import \"./SafeMathUpgradeable.sol\";
import \"./Initializable.sol\";

/**
 * @dev Implementation of the {IERC20} interface.
 *
 * This implementation is agnostic to the way tokens are created. This means
 * that a supply mechanism has to be added in a derived contract using {_mint}.
 * For a generic mechanism see {ERC20PresetMinterPauser}.
 *
 * TIP: For a detailed writeup see our guide
 * https://forum.zeppelin.solutions/t/how-to-implement-erc20-supply-mechanisms/226[How
 * to implement supply mechanisms].
 *
 * We have followed general OpenZeppelin guidelines: functions revert instead
 * of returning `false` on failure. This behavior is nonetheless conventional
 * and does not conflict with the expectations of ERC20 applications.
 *
 * Additionally, an {Approval} event is emitted on calls to {transferFrom}.
 * This allows applications to reconstruct the allowance for all accounts just
 * by listening to said events. Other implementations of the EIP may not emit
 * these events, as it isn\u0027t required by the specification.
 *
 * Finally, the non-standard {decreaseAllowance} and {increaseAllowance}
 * functions have been added to mitigate the well-known issues around setting
 * allowances. See {IERC20-approve}.
 */
contract ERC20Upgradeable is
    Initializable,
    ContextUpgradeable,
    IERC20Upgradeable
{
    using SafeMathUpgradeable for uint256;

    mapping(address =\u003e uint256) private _balances;

    mapping(address =\u003e mapping(address =\u003e uint256)) private _allowances;

    uint256 private _totalSupply;

    string private _name;
    string private _symbol;
    uint8 private _decimals;

    /**
     * @dev Sets the values for {name} and {symbol}, initializes {decimals} with
     * a default value of 18.
     *
     * To select a different value for {decimals}, use {_setupDecimals}.
     *
     * All three of these values are immutable: they can only be set once during
     * construction.
     */
    function __ERC20_init(string memory name_, string memory symbol_)
        internal
        initializer
    {
        __Context_init_unchained();
        __ERC20_init_unchained(name_, symbol_);
    }

    function __ERC20_init_unchained(string memory name_, string memory symbol_)
        internal
        initializer
    {
        _name = name_;
        _symbol = symbol_;
        _decimals = 18;
    }

    /**
     * @dev Returns the name of the token.
     */
    function name() public view virtual returns (string memory) {
        return _name;
    }

    /**
     * @dev Returns the symbol of the token, usually a shorter version of the
     * name.
     */
    function symbol() public view virtual returns (string memory) {
        return _symbol;
    }

    /**
     * @dev Returns the number of decimals used to get its user representation.
     * For example, if `decimals` equals `2`, a balance of `505` tokens should
     * be displayed to a user as `5,05` (`505 / 10 ** 2`).
     *
     * Tokens usually opt for a value of 18, imitating the relationship between
     * Ether and Wei. This is the value {ERC20} uses, unless {_setupDecimals} is
     * called.
     *
     * NOTE: This information is only used for _display_ purposes: it in
     * no way affects any of the arithmetic of the contract, including
     * {IERC20-balanceOf} and {IERC20-transfer}.
     */
    function decimals() public view virtual returns (uint8) {
        return _decimals;
    }

    /**
     * @dev See {IERC20-totalSupply}.
     */
    function totalSupply() public view virtual override returns (uint256) {
        return _totalSupply;
    }

    /**
     * @dev See {IERC20-balanceOf}.
     */
    function balanceOf(address account)
        public
        view
        virtual
        override
        returns (uint256)
    {
        return _balances[account];
    }

    /**
     * @dev See {IERC20-transfer}.
     *
     * Requirements:
     *
     * - `recipient` cannot be the zero address.
     * - the caller must have a balance of at least `amount`.
     */
    function transfer(address recipient, uint256 amount)
        public
        virtual
        override
        returns (bool)
    {
        _transfer(_msgSender(), recipient, amount);
        return true;
    }

    /**
     * @dev See {IERC20-allowance}.
     */
    function allowance(address owner, address spender)
        public
        view
        virtual
        override
        returns (uint256)
    {
        return _allowances[owner][spender];
    }

    /**
     * @dev See {IERC20-approve}.
     *
     * Requirements:
     *
     * - `spender` cannot be the zero address.
     */
    function approve(address spender, uint256 amount)
        public
        virtual
        override
        returns (bool)
    {
        _approve(_msgSender(), spender, amount);
        return true;
    }

    /**
     * @dev See {IERC20-transferFrom}.
     *
     * Emits an {Approval} event indicating the updated allowance. This is not
     * required by the EIP. See the note at the beginning of {ERC20}.
     *
     * Requirements:
     *
     * - `sender` and `recipient` cannot be the zero address.
     * - `sender` must have a balance of at least `amount`.
     * - the caller must have allowance for ``sender``\u0027s tokens of at least
     * `amount`.
     */
    function transferFrom(address sender, address recipient, uint256 amount)
        public
        virtual
        override
        returns (bool)
    {
        _transfer(sender, recipient, amount);
        _approve(
            sender,
            _msgSender(),
            _allowances[sender][_msgSender()].sub(
                amount,
                \"ERC20: transfer amount exceeds allowance\"
            )
        );
        return true;
    }

    /**
     * @dev Atomically increases the allowance granted to `spender` by the caller.
     *
     * This is an alternative to {approve} that can be used as a mitigation for
     * problems described in {IERC20-approve}.
     *
     * Emits an {Approval} event indicating the updated allowance.
     *
     * Requirements:
     *
     * - `spender` cannot be the zero address.
     */
    function increaseAllowance(address spender, uint256 addedValue)
        public
        virtual
        returns (bool)
    {
        _approve(
            _msgSender(),
            spender,
            _allowances[_msgSender()][spender].add(addedValue)
        );
        return true;
    }

    /**
     * @dev Atomically decreases the allowance granted to `spender` by the caller.
     *
     * This is an alternative to {approve} that can be used as a mitigation for
     * problems described in {IERC20-approve}.
     *
     * Emits an {Approval} event indicating the updated allowance.
     *
     * Requirements:
     *
     * - `spender` cannot be the zero address.
     * - `spender` must have allowance for the caller of at least
     * `subtractedValue`.
     */
    function decreaseAllowance(address spender, uint256 subtractedValue)
        public
        virtual
        returns (bool)
    {
        _approve(
            _msgSender(),
            spender,
            _allowances[_msgSender()][spender].sub(
                subtractedValue,
                \"ERC20: decreased allowance below zero\"
            )
        );
        return true;
    }

    /**
     * @dev Moves tokens `amount` from `sender` to `recipient`.
     *
     * This is internal function is equivalent to {transfer}, and can be used to
     * e.g. implement automatic token fees, slashing mechanisms, etc.
     *
     * Emits a {Transfer} event.
     *
     * Requirements:
     *
     * - `sender` cannot be the zero address.
     * - `recipient` cannot be the zero address.
     * - `sender` must have a balance of at least `amount`.
     */
    function _transfer(address sender, address recipient, uint256 amount)
        internal
        virtual
    {
        require(sender != address(0), \"ERC20: transfer from the zero address\");
        require(recipient != address(0), \"ERC20: transfer to the zero address\");

        _beforeTokenTransfer(sender, recipient, amount);

        _balances[sender] = _balances[sender].sub(
            amount,
            \"ERC20: transfer amount exceeds balance\"
        );
        _balances[recipient] = _balances[recipient].add(amount);
        emit Transfer(sender, recipient, amount);
    }

    /** @dev Creates `amount` tokens and assigns them to `account`, increasing
     * the total supply.
     *
     * Emits a {Transfer} event with `from` set to the zero address.
     *
     * Requirements:
     *
     * - `to` cannot be the zero address.
     */
    function _mint(address account, uint256 amount) internal virtual {
        require(account != address(0), \"ERC20: mint to the zero address\");

        _beforeTokenTransfer(address(0), account, amount);

        _totalSupply = _totalSupply.add(amount);
        _balances[account] = _balances[account].add(amount);
        emit Transfer(address(0), account, amount);
    }

    /**
     * @dev Destroys `amount` tokens from `account`, reducing the
     * total supply.
     *
     * Emits a {Transfer} event with `to` set to the zero address.
     *
     * Requirements:
     *
     * - `account` cannot be the zero address.
     * - `account` must have at least `amount` tokens.
     */
    function _burn(address account, uint256 amount) internal virtual {
        require(account != address(0), \"ERC20: burn from the zero address\");

        _beforeTokenTransfer(account, address(0), amount);

        _balances[account] = _balances[account].sub(
            amount,
            \"ERC20: burn amount exceeds balance\"
        );
        _totalSupply = _totalSupply.sub(amount);
        emit Transfer(account, address(0), amount);
    }

    /**
     * @dev Sets `amount` as the allowance of `spender` over the `owner` s tokens.
     *
     * This internal function is equivalent to `approve`, and can be used to
     * e.g. set automatic allowances for certain subsystems, etc.
     *
     * Emits an {Approval} event.
     *
     * Requirements:
     *
     * - `owner` cannot be the zero address.
     * - `spender` cannot be the zero address.
     */
    function _approve(address owner, address spender, uint256 amount)
        internal
        virtual
    {
        require(owner != address(0), \"ERC20: approve from the zero address\");
        require(spender != address(0), \"ERC20: approve to the zero address\");

        _allowances[owner][spender] = amount;
        emit Approval(owner, spender, amount);
    }

    /**
     * @dev Sets {decimals} to a value other than the default one of 18.
     *
     * WARNING: This function should only be called from the constructor. Most
     * applications that interact with token contracts will not expect
     * {decimals} to ever change, and may work incorrectly if it does.
     */
    function _setupDecimals(uint8 decimals_) internal virtual {
        _decimals = decimals_;
    }

    /**
     * @dev Hook that is called before any transfer of tokens. This includes
     * minting and burning.
     *
     * Calling conditions:
     *
     * - when `from` and `to` are both non-zero, `amount` of ``from``\u0027s tokens
     * will be to transferred to `to`.
     * - when `from` is zero, `amount` tokens will be minted for `to`.
     * - when `to` is zero, `amount` of ``from``\u0027s tokens will be burned.
     * - `from` and `to` are never both zero.
     *
     * To learn more about hooks, head to xref:ROOT:extending-contracts.adoc#using-hooks[Using Hooks].
     */
    function _beforeTokenTransfer(address from, address to, uint256 amount)
        internal
        virtual
    {}
    uint256[44] private __gap;
}
"},"ERC721.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.6.0;

import \"./Context.sol\";
import \"./IERC721.sol\";
import \"./IERC721Metadata.sol\";
import \"./IERC721Enumerable.sol\";
import \"./IERC721Receiver.sol\";
import \"./ERC165.sol\";
import \"./SafeMath.sol\";
import \"./Address.sol\";
import \"./EnumerableSet.sol\";
import \"./EnumerableMap.sol\";
import \"./Strings.sol\";

/**
 * @title ERC721 Non-Fungible Token Standard basic implementation
 * @dev see https://eips.ethereum.org/EIPS/eip-721
 */
contract ERC721 is
    Context,
    ERC165,
    IERC721,
    IERC721Metadata,
    IERC721Enumerable
{
    using SafeMath for uint256;
    using Address for address;
    using EnumerableSet for EnumerableSet.UintSet;
    using EnumerableMap for EnumerableMap.UintToAddressMap;
    using Strings for uint256;

    // Equals to `bytes4(keccak256(\"onERC721Received(address,address,uint256,bytes)\"))`
    // which can be also obtained as `IERC721Receiver(0).onERC721Received.selector`
    bytes4 private constant _ERC721_RECEIVED = 0x150b7a02;

    // Mapping from holder address to their (enumerable) set of owned tokens
    mapping(address =\u003e EnumerableSet.UintSet) private _holderTokens;

    // Enumerable mapping from token ids to their owners
    EnumerableMap.UintToAddressMap private _tokenOwners;

    // Mapping from token ID to approved address
    mapping(uint256 =\u003e address) private _tokenApprovals;

    // Mapping from owner to operator approvals
    mapping(address =\u003e mapping(address =\u003e bool)) private _operatorApprovals;

    // Token name
    string private _name;

    // Token symbol
    string private _symbol;

    // Optional mapping for token URIs
    mapping(uint256 =\u003e string) private _tokenURIs;

    // Base URI
    string private _baseURI;

    /*
     *     bytes4(keccak256(\u0027balanceOf(address)\u0027)) == 0x70a08231
     *     bytes4(keccak256(\u0027ownerOf(uint256)\u0027)) == 0x6352211e
     *     bytes4(keccak256(\u0027approve(address,uint256)\u0027)) == 0x095ea7b3
     *     bytes4(keccak256(\u0027getApproved(uint256)\u0027)) == 0x081812fc
     *     bytes4(keccak256(\u0027setApprovalForAll(address,bool)\u0027)) == 0xa22cb465
     *     bytes4(keccak256(\u0027isApprovedForAll(address,address)\u0027)) == 0xe985e9c5
     *     bytes4(keccak256(\u0027transferFrom(address,address,uint256)\u0027)) == 0x23b872dd
     *     bytes4(keccak256(\u0027safeTransferFrom(address,address,uint256)\u0027)) == 0x42842e0e
     *     bytes4(keccak256(\u0027safeTransferFrom(address,address,uint256,bytes)\u0027)) == 0xb88d4fde
     *
     *     =\u003e 0x70a08231 ^ 0x6352211e ^ 0x095ea7b3 ^ 0x081812fc ^
     *        0xa22cb465 ^ 0xe985e9c5 ^ 0x23b872dd ^ 0x42842e0e ^ 0xb88d4fde == 0x80ac58cd
     */
    bytes4 private constant _INTERFACE_ID_ERC721 = 0x80ac58cd;

    /*
     *     bytes4(keccak256(\u0027name()\u0027)) == 0x06fdde03
     *     bytes4(keccak256(\u0027symbol()\u0027)) == 0x95d89b41
     *     bytes4(keccak256(\u0027tokenURI(uint256)\u0027)) == 0xc87b56dd
     *
     *     =\u003e 0x06fdde03 ^ 0x95d89b41 ^ 0xc87b56dd == 0x5b5e139f
     */
    bytes4 private constant _INTERFACE_ID_ERC721_METADATA = 0x5b5e139f;

    /*
     *     bytes4(keccak256(\u0027totalSupply()\u0027)) == 0x18160ddd
     *     bytes4(keccak256(\u0027tokenOfOwnerByIndex(address,uint256)\u0027)) == 0x2f745c59
     *     bytes4(keccak256(\u0027tokenByIndex(uint256)\u0027)) == 0x4f6ccce7
     *
     *     =\u003e 0x18160ddd ^ 0x2f745c59 ^ 0x4f6ccce7 == 0x780e9d63
     */
    bytes4 private constant _INTERFACE_ID_ERC721_ENUMERABLE = 0x780e9d63;

    /**
     * @dev Initializes the contract by setting a `name` and a `symbol` to the token collection.
     */
    constructor(string memory name, string memory symbol) public {
        _name = name;
        _symbol = symbol;

        // register the supported interfaces to conform to ERC721 via ERC165
        _registerInterface(_INTERFACE_ID_ERC721);
        _registerInterface(_INTERFACE_ID_ERC721_METADATA);
        _registerInterface(_INTERFACE_ID_ERC721_ENUMERABLE);
    }

    /**
     * @dev See {IERC721-balanceOf}.
     */
    function balanceOf(address owner) public view override returns (uint256) {
        require(
            owner != address(0),
            \"ERC721: balance query for the zero address\"
        );

        return _holderTokens[owner].length();
    }

    /**
     * @dev See {IERC721-ownerOf}.
     */
    function ownerOf(uint256 tokenId) public view override returns (address) {
        return
            _tokenOwners.get(
                tokenId,
                \"ERC721: owner query for nonexistent token\"
            );
    }

    /**
     * @dev See {IERC721Metadata-name}.
     */
    function name() public view override returns (string memory) {
        return _name;
    }

    /**
     * @dev See {IERC721Metadata-symbol}.
     */
    function symbol() public view override returns (string memory) {
        return _symbol;
    }

    /**
     * @dev See {IERC721Metadata-tokenURI}.
     */
    function tokenURI(uint256 tokenId)
        public
        view
        override
        returns (string memory)
    {
        require(
            _exists(tokenId),
            \"ERC721Metadata: URI query for nonexistent token\"
        );

        string memory _tokenURI = _tokenURIs[tokenId];

        // If there is no base URI, return the token URI.
        if (bytes(_baseURI).length == 0) {
            return _tokenURI;
        }
        // If both are set, concatenate the baseURI and tokenURI (via abi.encodePacked).
        if (bytes(_tokenURI).length \u003e 0) {
            return string(abi.encodePacked(_baseURI, _tokenURI));
        }
        // If there is a baseURI but no tokenURI, concatenate the tokenID to the baseURI.
        return string(abi.encodePacked(_baseURI, tokenId.toString()));
    }

    /**
     * @dev Returns the base URI set via {_setBaseURI}. This will be
     * automatically added as a prefix in {tokenURI} to each token\u0027s URI, or
     * to the token ID if no specific URI is set for that token ID.
     */
    function baseURI() public view returns (string memory) {
        return _baseURI;
    }

    /**
     * @dev See {IERC721Enumerable-tokenOfOwnerByIndex}.
     */
    function tokenOfOwnerByIndex(address owner, uint256 index)
        public
        view
        override
        returns (uint256)
    {
        return _holderTokens[owner].at(index);
    }

    /**
     * @dev See {IERC721Enumerable-totalSupply}.
     */
    function totalSupply() public view override returns (uint256) {
        // _tokenOwners are indexed by tokenIds, so .length() returns the number of tokenIds
        return _tokenOwners.length();
    }

    /**
     * @dev See {IERC721Enumerable-tokenByIndex}.
     */
    function tokenByIndex(uint256 index)
        public
        view
        override
        returns (uint256)
    {
        (uint256 tokenId, ) = _tokenOwners.at(index);
        return tokenId;
    }

    /**
     * @dev See {IERC721-approve}.
     */
    function approve(address to, uint256 tokenId) public virtual override {
        address owner = ownerOf(tokenId);
        require(to != owner, \"ERC721: approval to current owner\");

        require(
            _msgSender() == owner || isApprovedForAll(owner, _msgSender()),
            \"ERC721: approve caller is not owner nor approved for all\"
        );

        _approve(to, tokenId);
    }

    /**
     * @dev See {IERC721-getApproved}.
     */
    function getApproved(uint256 tokenId)
        public
        view
        override
        returns (address)
    {
        require(
            _exists(tokenId),
            \"ERC721: approved query for nonexistent token\"
        );

        return _tokenApprovals[tokenId];
    }

    /**
     * @dev See {IERC721-setApprovalForAll}.
     */
    function setApprovalForAll(address operator, bool approved)
        public
        virtual
        override
    {
        require(operator != _msgSender(), \"ERC721: approve to caller\");

        _operatorApprovals[_msgSender()][operator] = approved;
        emit ApprovalForAll(_msgSender(), operator, approved);
    }

    /**
     * @dev See {IERC721-isApprovedForAll}.
     */
    function isApprovedForAll(address owner, address operator)
        public
        view
        override
        returns (bool)
    {
        return _operatorApprovals[owner][operator];
    }

    /**
     * @dev See {IERC721-transferFrom}.
     */
    function transferFrom(address from, address to, uint256 tokenId)
        public
        virtual
        override
    {
        //solhint-disable-next-line max-line-length
        require(
            _isApprovedOrOwner(_msgSender(), tokenId),
            \"ERC721: transfer caller is not owner nor approved\"
        );

        _transfer(from, to, tokenId);
    }

    /**
     * @dev See {IERC721-safeTransferFrom}.
     */
    function safeTransferFrom(address from, address to, uint256 tokenId)
        public
        virtual
        override
    {
        safeTransferFrom(from, to, tokenId, \"\");
    }

    /**
     * @dev See {IERC721-safeTransferFrom}.
     */
    function safeTransferFrom(
        address from,
        address to,
        uint256 tokenId,
        bytes memory _data
    ) public virtual override {
        require(
            _isApprovedOrOwner(_msgSender(), tokenId),
            \"ERC721: transfer caller is not owner nor approved\"
        );
        _safeTransfer(from, to, tokenId, _data);
    }

    /**
     * @dev Safely transfers `tokenId` token from `from` to `to`, checking first that contract recipients
     * are aware of the ERC721 protocol to prevent tokens from being forever locked.
     *
     * `_data` is additional data, it has no specified format and it is sent in call to `to`.
     *
     * This internal function is equivalent to {safeTransferFrom}, and can be used to e.g.
     * implement alternative mechanisms to perform token transfer, such as signature-based.
     *
     * Requirements:
     *
     * - `from` cannot be the zero address.
     * - `to` cannot be the zero address.
     * - `tokenId` token must exist and be owned by `from`.
     * - If `to` refers to a smart contract, it must implement {IERC721Receiver-onERC721Received}, which is called upon a safe transfer.
     *
     * Emits a {Transfer} event.
     */
    function _safeTransfer(
        address from,
        address to,
        uint256 tokenId,
        bytes memory _data
    ) internal virtual {
        _transfer(from, to, tokenId);
        require(
            _checkOnERC721Received(from, to, tokenId, _data),
            \"ERC721: transfer to non ERC721Receiver implementer\"
        );
    }

    /**
     * @dev Returns whether `tokenId` exists.
     *
     * Tokens can be managed by their owner or approved accounts via {approve} or {setApprovalForAll}.
     *
     * Tokens start existing when they are minted (`_mint`),
     * and stop existing when they are burned (`_burn`).
     */
    function _exists(uint256 tokenId) internal view returns (bool) {
        return _tokenOwners.contains(tokenId);
    }

    /**
     * @dev Returns whether `spender` is allowed to manage `tokenId`.
     *
     * Requirements:
     *
     * - `tokenId` must exist.
     */
    function _isApprovedOrOwner(address spender, uint256 tokenId)
        internal
        view
        returns (bool)
    {
        require(
            _exists(tokenId),
            \"ERC721: operator query for nonexistent token\"
        );
        address owner = ownerOf(tokenId);
        return (spender == owner ||
            getApproved(tokenId) == spender ||
            isApprovedForAll(owner, spender));
    }

    /**
     * @dev Safely mints `tokenId` and transfers it to `to`.
     *
     * Requirements:
     d*
     * - `tokenId` must not exist.
     * - If `to` refers to a smart contract, it must implement {IERC721Receiver-onERC721Received}, which is called upon a safe transfer.
     *
     * Emits a {Transfer} event.
     */
    function _safeMint(address to, uint256 tokenId) internal virtual {
        _safeMint(to, tokenId, \"\");
    }

    // For testing
    function safeMint(address to, uint256 tokenId) public virtual {
        _safeMint(to, tokenId, \"\");
    }

    /**
     * @dev Same as {xref-ERC721-_safeMint-address-uint256-}[`_safeMint`], with an additional `data` parameter which is
     * forwarded in {IERC721Receiver-onERC721Received} to contract recipients.
     */
    function _safeMint(address to, uint256 tokenId, bytes memory _data)
        internal
        virtual
    {
        _mint(to, tokenId);
        require(
            _checkOnERC721Received(address(0), to, tokenId, _data),
            \"ERC721: transfer to non ERC721Receiver implementer\"
        );
    }

    /**
     * @dev Mints `tokenId` and transfers it to `to`.
     *
     * WARNING: Usage of this method is discouraged, use {_safeMint} whenever possible
     *
     * Requirements:
     *
     * - `tokenId` must not exist.
     * - `to` cannot be the zero address.
     *
     * Emits a {Transfer} event.
     */
    function _mint(address to, uint256 tokenId) internal virtual {
        require(to != address(0), \"ERC721: mint to the zero address\");
        require(!_exists(tokenId), \"ERC721: token already minted\");

        _beforeTokenTransfer(address(0), to, tokenId);

        _holderTokens[to].add(tokenId);

        _tokenOwners.set(tokenId, to);

        emit Transfer(address(0), to, tokenId);
    }

    /**
     * @dev Destroys `tokenId`.
     * The approval is cleared when the token is burned.
     *
     * Requirements:
     *
     * - `tokenId` must exist.
     *
     * Emits a {Transfer} event.
     */
    function _burn(uint256 tokenId) internal virtual {
        address owner = ownerOf(tokenId);

        _beforeTokenTransfer(owner, address(0), tokenId);

        // Clear approvals
        _approve(address(0), tokenId);

        // Clear metadata (if any)
        if (bytes(_tokenURIs[tokenId]).length != 0) {
            delete _tokenURIs[tokenId];
        }

        _holderTokens[owner].remove(tokenId);

        _tokenOwners.remove(tokenId);

        emit Transfer(owner, address(0), tokenId);
    }

    /**
     * @dev Transfers `tokenId` from `from` to `to`.
     *  As opposed to {transferFrom}, this imposes no restrictions on msg.sender.
     *
     * Requirements:
     *
     * - `to` cannot be the zero address.
     * - `tokenId` token must be owned by `from`.
     *
     * Emits a {Transfer} event.
     */
    function _transfer(address from, address to, uint256 tokenId)
        internal
        virtual
    {
        require(
            ownerOf(tokenId) == from,
            \"ERC721: transfer of token that is not own\"
        );
        require(to != address(0), \"ERC721: transfer to the zero address\");

        _beforeTokenTransfer(from, to, tokenId);

        // Clear approvals from the previous owner
        _approve(address(0), tokenId);

        _holderTokens[from].remove(tokenId);
        _holderTokens[to].add(tokenId);

        _tokenOwners.set(tokenId, to);

        emit Transfer(from, to, tokenId);
    }

    /**
     * @dev Sets `_tokenURI` as the tokenURI of `tokenId`.
     *
     * Requirements:
     *
     * - `tokenId` must exist.
     */
    function _setTokenURI(uint256 tokenId, string memory _tokenURI)
        internal
        virtual
    {
        require(
            _exists(tokenId),
            \"ERC721Metadata: URI set of nonexistent token\"
        );
        _tokenURIs[tokenId] = _tokenURI;
    }

    /**
     * @dev Internal function to set the base URI for all token IDs. It is
     * automatically added as a prefix to the value returned in {tokenURI},
     * or to the token ID if {tokenURI} is empty.
     */
    function _setBaseURI(string memory baseURI_) internal virtual {
        _baseURI = baseURI_;
    }

    /**
     * @dev Internal function to invoke {IERC721Receiver-onERC721Received} on a target address.
     * The call is not executed if the target address is not a contract.
     *
     * @param from address representing the previous owner of the given token ID
     * @param to target address that will receive the tokens
     * @param tokenId uint256 ID of the token to be transferred
     * @param _data bytes optional data to send along with the call
     * @return bool whether the call correctly returned the expected magic value
     */
    function _checkOnERC721Received(
        address from,
        address to,
        uint256 tokenId,
        bytes memory _data
    ) private returns (bool) {
        if (!to.isContract()) {
            return true;
        }
        bytes memory returndata = to.functionCall(
            abi.encodeWithSelector(
                IERC721Receiver(to).onERC721Received.selector,
                _msgSender(),
                from,
                tokenId,
                _data
            ),
            \"ERC721: transfer to non ERC721Receiver implementer\"
        );
        bytes4 retval = abi.decode(returndata, (bytes4));
        return (retval == _ERC721_RECEIVED);
    }

    function _approve(address to, uint256 tokenId) private {
        _tokenApprovals[tokenId] = to;
        emit Approval(ownerOf(tokenId), to, tokenId);
    }

    /**
     * @dev Hook that is called before any token transfer. This includes minting
     * and burning.
     *
     * Calling conditions:
     *
     * - When `from` and `to` are both non-zero, ``from``\u0027s `tokenId` will be
     * transferred to `to`.
     * - When `from` is zero, `tokenId` will be minted for `to`.
     * - When `to` is zero, ``from``\u0027s `tokenId` will be burned.
     * - `from` cannot be the zero address.
     * - `to` cannot be the zero address.
     *
     * To learn more about hooks, head to xref:ROOT:extending-contracts.adoc#using-hooks[Using Hooks].
     */
    function _beforeTokenTransfer(address from, address to, uint256 tokenId)
        internal
        virtual
    {}
}
"},"ERC721Holder.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.6.0;

import \"./IERC721Receiver.sol\";

/**
 * @dev Implementation of the {IERC721Receiver} interface.
 *
 * Accepts all token transfers.
 * Make sure the contract is able to use its token with {IERC721-safeTransferFrom}, {IERC721-approve} or {IERC721-setApprovalForAll}.
 */
contract ERC721Holder is IERC721Receiver {
    /**
     * @dev See {IERC721Receiver-onERC721Received}.
     *
     * Always returns `IERC721Receiver.onERC721Received.selector`.
     */
    function onERC721Received(
        address,
        address,
        uint256,
        bytes memory
    ) public virtual override returns (bytes4) {
        return this.onERC721Received.selector;
    }
}
"},"ERC721Public.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity 0.6.8;

import \"./Context.sol\";
import \"./ERC721.sol\";

contract ERC721Public is Context, ERC721 {
    uint256 public minTokenId;
    uint256 public maxTokenId;

    constructor(
        string memory name,
        string memory symbol,
        uint256 _minTokenId,
        uint256 _maxTokenId
    ) public ERC721(name, symbol) {
        minTokenId = _minTokenId;
        maxTokenId = _maxTokenId;
    }

    function mint(uint256 tokenId, address recipient) public {
        require(tokenId \u003e= minTokenId, \"tokenId \u003c minTokenId\");
        require(tokenId \u003c= maxTokenId, \"tokenId \u003e maxTokenId\");
        _mint(recipient, tokenId);
    }

}
"},"IERC1155.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.6.2;

import \"./IERC165.sol\";

/**
 * @dev Required interface of an ERC1155 compliant contract, as defined in the
 * https://eips.ethereum.org/EIPS/eip-1155[EIP].
 *
 * _Available since v3.1._
 */
interface IERC1155 is IERC165 {
    /**
     * @dev Emitted when `value` tokens of token type `id` are transferred from `from` to `to` by `operator`.
     */
    event TransferSingle(address indexed operator, address indexed from, address indexed to, uint256 id, uint256 value);

    /**
     * @dev Equivalent to multiple {TransferSingle} events, where `operator`, `from` and `to` are the same for all
     * transfers.
     */
    event TransferBatch(address indexed operator, address indexed from, address indexed to, uint256[] ids, uint256[] values);

    /**
     * @dev Emitted when `account` grants or revokes permission to `operator` to transfer their tokens, according to
     * `approved`.
     */
    event ApprovalForAll(address indexed account, address indexed operator, bool approved);

    /**
     * @dev Emitted when the URI for token type `id` changes to `value`, if it is a non-programmatic URI.
     *
     * If an {URI} event was emitted for `id`, the standard
     * https://eips.ethereum.org/EIPS/eip-1155#metadata-extensions[guarantees] that `value` will equal the value
     * returned by {IERC1155MetadataURI-uri}.
     */
    event URI(string value, uint256 indexed id);

    /**
     * @dev Returns the amount of tokens of token type `id` owned by `account`.
     *
     * Requirements:
     *
     * - `account` cannot be the zero address.
     */
    function balanceOf(address account, uint256 id) external view returns (uint256);

    /**
     * @dev xref:ROOT:erc1155.adoc#batch-operations[Batched] version of {balanceOf}.
     *
     * Requirements:
     *
     * - `accounts` and `ids` must have the same length.
     */
    function balanceOfBatch(address[] calldata accounts, uint256[] calldata ids) external view returns (uint256[] memory);

    /**
     * @dev Grants or revokes permission to `operator` to transfer the caller\u0027s tokens, according to `approved`,
     *
     * Emits an {ApprovalForAll} event.
     *
     * Requirements:
     *
     * - `operator` cannot be the caller.
     */
    function setApprovalForAll(address operator, bool approved) external;

    /**
     * @dev Returns true if `operator` is approved to transfer ``account``\u0027s tokens.
     *
     * See {setApprovalForAll}.
     */
    function isApprovedForAll(address account, address operator) external view returns (bool);

    /**
     * @dev Transfers `amount` tokens of token type `id` from `from` to `to`.
     *
     * Emits a {TransferSingle} event.
     *
     * Requirements:
     *
     * - `to` cannot be the zero address.
     * - If the caller is not `from`, it must be have been approved to spend ``from``\u0027s tokens via {setApprovalForAll}.
     * - `from` must have a balance of tokens of type `id` of at least `amount`.
     * - If `to` refers to a smart contract, it must implement {IERC1155Receiver-onERC1155Received} and return the
     * acceptance magic value.
     */
    function safeTransferFrom(address from, address to, uint256 id, uint256 amount, bytes calldata data) external;

    /**
     * @dev xref:ROOT:erc1155.adoc#batch-operations[Batched] version of {safeTransferFrom}.
     *
     * Emits a {TransferBatch} event.
     *
     * Requirements:
     *
     * - `ids` and `amounts` must have the same length.
     * - If `to` refers to a smart contract, it must implement {IERC1155Receiver-onERC1155BatchReceived} and return the
     * acceptance magic value.
     */
    function safeBatchTransferFrom(address from, address to, uint256[] calldata ids, uint256[] calldata amounts, bytes calldata data) external;
}
"},"IERC1155MetadataURI.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.6.2;

import \"./IERC1155.sol\";

/**
 * @dev Interface of the optional ERC1155MetadataExtension interface, as defined
 * in the https://eips.ethereum.org/EIPS/eip-1155#metadata-extensions[EIP].
 *
 * _Available since v3.1._
 */
interface IERC1155MetadataURI is IERC1155 {
    /**
     * @dev Returns the URI for token type `id`.
     *
     * If the `\\{id\\}` substring is present in the URI, it must be replaced by
     * clients with the actual token type ID.
     */
    function uri(uint256 id) external view returns (string memory);
}
"},"IERC1155Receiver.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.6.0;

import \"./IERC165.sol\";

/**
 * _Available since v3.1._
 */
interface IERC1155Receiver is IERC165 {

    /**
        @dev Handles the receipt of a single ERC1155 token type. This function is
        called at the end of a `safeTransferFrom` after the balance has been updated.
        To accept the transfer, this must return
        `bytes4(keccak256(\"onERC1155Received(address,address,uint256,uint256,bytes)\"))`
        (i.e. 0xf23a6e61, or its own function selector).
        @param operator The address which initiated the transfer (i.e. msg.sender)
        @param from The address which previously owned the token
        @param id The ID of the token being transferred
        @param value The amount of tokens being transferred
        @param data Additional data with no specified format
        @return `bytes4(keccak256(\"onERC1155Received(address,address,uint256,uint256,bytes)\"))` if transfer is allowed
    */
    function onERC1155Received(
        address operator,
        address from,
        uint256 id,
        uint256 value,
        bytes calldata data
    )
        external
        returns(bytes4);

    /**
        @dev Handles the receipt of a multiple ERC1155 token types. This function
        is called at the end of a `safeBatchTransferFrom` after the balances have
        been updated. To accept the transfer(s), this must return
        `bytes4(keccak256(\"onERC1155BatchReceived(address,address,uint256[],uint256[],bytes)\"))`
        (i.e. 0xbc197c81, or its own function selector).
        @param operator The address which initiated the batch transfer (i.e. msg.sender)
        @param from The address which previously owned the token
        @param ids An array containing ids of each token being transferred (order and length must match values array)
        @param values An array containing amounts of each token being transferred (order and length must match ids array)
        @param data Additional data with no specified format
        @return `bytes4(keccak256(\"onERC1155BatchReceived(address,address,uint256[],uint256[],bytes)\"))` if transfer is allowed
    */
    function onERC1155BatchReceived(
        address operator,
        address from,
        uint256[] calldata ids,
        uint256[] calldata values,
        bytes calldata data
    )
        external
        returns(bytes4);
}
"},"IERC165.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.6.0;

/**
 * @dev Interface of the ERC165 standard, as defined in the
 * https://eips.ethereum.org/EIPS/eip-165[EIP].
 *
 * Implementers can declare support of contract interfaces, which can then be
 * queried by others ({ERC165Checker}).
 *
 * For an implementation, see {ERC165}.
 */
interface IERC165 {
    /**
     * @dev Returns true if this contract implements the interface defined by
     * `interfaceId`. See the corresponding
     * https://eips.ethereum.org/EIPS/eip-165#how-interfaces-are-identified[EIP section]
     * to learn more about how these ids are created.
     *
     * This function call must use less than 30 000 gas.
     */
    function supportsInterface(bytes4 interfaceId) external view returns (bool);
}
"},"IERC20.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity 0.6.8;

/**
 * @dev Interface of the ERC20 standard as defined in the EIP.
 */
interface IERC20 {
    /**
     * @dev Returns the amount of tokens in existence.
     */
    function totalSupply() external view returns (uint256);

    /**
     * @dev Returns the amount of tokens owned by `account`.
     */
    function balanceOf(address account) external view returns (uint256);

    /**
     * @dev Moves `amount` tokens from the caller\u0027s account to `recipient`.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transfer(address recipient, uint256 amount)
        external
        returns (bool);

    /**
     * @dev Returns the remaining number of tokens that `spender` will be
     * allowed to spend on behalf of `owner` through {transferFrom}. This is
     * zero by default.
     *
     * This value changes when {approve} or {transferFrom} are called.
     */
    function allowance(address owner, address spender)
        external
        view
        returns (uint256);

    /**
     * @dev Sets `amount` as the allowance of `spender` over the caller\u0027s tokens.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * IMPORTANT: Beware that changing an allowance with this method brings the risk
     * that someone may use both the old and the new allowance by unfortunate
     * transaction ordering. One possible solution to mitigate this race
     * condition is to first reduce the spender\u0027s allowance to 0 and set the
     * desired value afterwards:
     * https://github.com/ethereum/EIPs/issues/20#issuecomment-263524729
     *
     * Emits an {Approval} event.
     */
    function approve(address spender, uint256 amount) external returns (bool);

    /**
     * @dev Moves `amount` tokens from `sender` to `recipient` using the
     * allowance mechanism. `amount` is then deducted from the caller\u0027s
     * allowance.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transferFrom(address sender, address recipient, uint256 amount)
        external
        returns (bool);

    /**
     * @dev Emitted when `value` tokens are moved from one account (`from`) to
     * another (`to`).
     *
     * Note that `value` may be zero.
     */
    event Transfer(address indexed from, address indexed to, uint256 value);

    /**
     * @dev Emitted when the allowance of a `spender` for an `owner` is set by
     * a call to {approve}. `value` is the new allowance.
     */
    event Approval(
        address indexed owner,
        address indexed spender,
        uint256 value
    );
}
"},"IERC20Upgradeable.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity \u003e=0.6.0 \u003c0.8.0;

/**
 * @dev Interface of the ERC20 standard as defined in the EIP.
 */
interface IERC20Upgradeable {
    /**
     * @dev Returns the amount of tokens in existence.
     */
    function totalSupply() external view returns (uint256);

    /**
     * @dev Returns the amount of tokens owned by `account`.
     */
    function balanceOf(address account) external view returns (uint256);

    /**
     * @dev Moves `amount` tokens from the caller\u0027s account to `recipient`.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transfer(address recipient, uint256 amount) external returns (bool);

    /**
     * @dev Returns the remaining number of tokens that `spender` will be
     * allowed to spend on behalf of `owner` through {transferFrom}. This is
     * zero by default.
     *
     * This value changes when {approve} or {transferFrom} are called.
     */
    function allowance(address owner, address spender) external view returns (uint256);

    /**
     * @dev Sets `amount` as the allowance of `spender` over the caller\u0027s tokens.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * IMPORTANT: Beware that changing an allowance with this method brings the risk
     * that someone may use both the old and the new allowance by unfortunate
     * transaction ordering. One possible solution to mitigate this race
     * condition is to first reduce the spender\u0027s allowance to 0 and set the
     * desired value afterwards:
     * https://github.com/ethereum/EIPs/issues/20#issuecomment-263524729
     *
     * Emits an {Approval} event.
     */
    function approve(address spender, uint256 amount) external returns (bool);

    /**
     * @dev Moves `amount` tokens from `sender` to `recipient` using the
     * allowance mechanism. `amount` is then deducted from the caller\u0027s
     * allowance.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transferFrom(address sender, address recipient, uint256 amount) external returns (bool);

    /**
     * @dev Emitted when `value` tokens are moved from one account (`from`) to
     * another (`to`).
     *
     * Note that `value` may be zero.
     */
    event Transfer(address indexed from, address indexed to, uint256 value);

    /**
     * @dev Emitted when the allowance of a `spender` for an `owner` is set by
     * a call to {approve}. `value` is the new allowance.
     */
    event Approval(address indexed owner, address indexed spender, uint256 value);
}
"},"IERC721.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.6.2;

import \"./IERC165.sol\";

/**
 * @dev Required interface of an ERC721 compliant contract.
 */
interface IERC721 is IERC165 {
    /**
     * @dev Emitted when `tokenId` token is transferred from `from` to `to`.
     */
    event Transfer(
        address indexed from,
        address indexed to,
        uint256 indexed tokenId
    );

    /**
     * @dev Emitted when `owner` enables `approved` to manage the `tokenId` token.
     */
    event Approval(
        address indexed owner,
        address indexed approved,
        uint256 indexed tokenId
    );

    /**
     * @dev Emitted when `owner` enables or disables (`approved`) `operator` to manage all of its assets.
     */
    event ApprovalForAll(
        address indexed owner,
        address indexed operator,
        bool approved
    );

    /**
     * @dev Returns the number of tokens in ``owner``\u0027s account.
     */
    function balanceOf(address owner) external view returns (uint256 balance);

    /**
     * @dev Returns the owner of the `tokenId` token.
     *
     * Requirements:
     *
     * - `tokenId` must exist.
     */
    function ownerOf(uint256 tokenId) external view returns (address owner);

    /**
     * @dev Safely transfers `tokenId` token from `from` to `to`, checking first that contract recipients
     * are aware of the ERC721 protocol to prevent tokens from being forever locked.
     *
     * Requirements:
     *
     * - `from` cannot be the zero address.
     * - `to` cannot be the zero address.
     * - `tokenId` token must exist and be owned by `from`.
     * - If the caller is not `from`, it must be have been allowed to move this token by either {approve} or {setApprovalForAll}.
     * - If `to` refers to a smart contract, it must implement {IERC721Receiver-onERC721Received}, which is called upon a safe transfer.
     *
     * Emits a {Transfer} event.
     */
    function safeTransferFrom(
        address from,
        address to,
        uint256 tokenId
    ) external;

    /**
     * @dev Transfers `tokenId` token from `from` to `to`.
     *
     * WARNING: Usage of this method is discouraged, use {safeTransferFrom} whenever possible.
     *
     * Requirements:
     *
     * - `from` cannot be the zero address.
     * - `to` cannot be the zero address.
     * - `tokenId` token must be owned by `from`.
     * - If the caller is not `from`, it must be approved to move this token by either {approve} or {setApprovalForAll}.
     *
     * Emits a {Transfer} event.
     */
    function transferFrom(
        address from,
        address to,
        uint256 tokenId
    ) external;

    /**
     * @dev Gives permission to `to` to transfer `tokenId` token to another account.
     * The approval is cleared when the token is transferred.
     *
     * Only a single account can be approved at a time, so approving the zero address clears previous approvals.
     *
     * Requirements:
     *
     * - The caller must own the token or be an approved operator.
     * - `tokenId` must exist.
     *
     * Emits an {Approval} event.
     */
    function approve(address to, uint256 tokenId) external;

    /**
     * @dev Returns the account approved for `tokenId` token.
     *
     * Requirements:
     *
     * - `tokenId` must exist.
     */
    function getApproved(uint256 tokenId)
        external
        view
        returns (address operator);

    /**
     * @dev Approve or remove `operator` as an operator for the caller.
     * Operators can call {transferFrom} or {safeTransferFrom} for any token owned by the caller.
     *
     * Requirements:
     *
     * - The `operator` cannot be the caller.
     *
     * Emits an {ApprovalForAll} event.
     */
    function setApprovalForAll(address operator, bool _approved) external;

    /**
     * @dev Returns if the `operator` is allowed to manage all of the assets of `owner`.
     *
     * See {setApprovalForAll}
     */
    function isApprovedForAll(address owner, address operator)
        external
        view
        returns (bool);

    /**
     * @dev Safely transfers `tokenId` token from `from` to `to`.
     *
     * Requirements:
     *
     * - `from` cannot be the zero address.
     * - `to` cannot be the zero address.
     * - `tokenId` token must exist and be owned by `from`.
     * - If the caller is not `from`, it must be approved to move this token by either {approve} or {setApprovalForAll}.
     * - If `to` refers to a smart contract, it must implement {IERC721Receiver-onERC721Received}, which is called upon a safe transfer.
     *
     * Emits a {Transfer} event.
     */
    function safeTransferFrom(
        address from,
        address to,
        uint256 tokenId,
        bytes calldata data
    ) external;
}
"},"IERC721Enumerable.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.6.2;

import \"./IERC721.sol\";

/**
 * @title ERC-721 Non-Fungible Token Standard, optional enumeration extension
 * @dev See https://eips.ethereum.org/EIPS/eip-721
 */
interface IERC721Enumerable is IERC721 {

    /**
     * @dev Returns the total amount of tokens stored by the contract.
     */
    function totalSupply() external view returns (uint256);

    /**
     * @dev Returns a token ID owned by `owner` at a given `index` of its token list.
     * Use along with {balanceOf} to enumerate all of ``owner``\u0027s tokens.
     */
    function tokenOfOwnerByIndex(address owner, uint256 index) external view returns (uint256 tokenId);

    /**
     * @dev Returns a token ID at a given `index` of all the tokens stored by the contract.
     * Use along with {totalSupply} to enumerate all tokens.
     */
    function tokenByIndex(uint256 index) external view returns (uint256);
}
"},"IERC721Metadata.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.6.2;

import \"./IERC721.sol\";

/**
 * @title ERC-721 Non-Fungible Token Standard, optional metadata extension
 * @dev See https://eips.ethereum.org/EIPS/eip-721
 */
interface IERC721Metadata is IERC721 {

    /**
     * @dev Returns the token collection name.
     */
    function name() external view returns (string memory);

    /**
     * @dev Returns the token collection symbol.
     */
    function symbol() external view returns (string memory);

    /**
     * @dev Returns the Uniform Resource Identifier (URI) for `tokenId` token.
     */
    function tokenURI(uint256 tokenId) external view returns (string memory);
}
"},"IERC721Plus.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.6.2;

import \"./IERC721.sol\";

interface IERC721Plus is IERC721 {
    function name() external view returns (string memory);
    function symbol() external view returns (string memory);
}
"},"IERC721Receiver.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.6.0;

/**
 * @title ERC721 token receiver interface
 * @dev Interface for any contract that wants to support safeTransfers
 * from ERC721 asset contracts.
 */
interface IERC721Receiver {
    /**
     * @dev Whenever an {IERC721} `tokenId` token is transferred to this contract via {IERC721-safeTransferFrom}
     * by `operator` from `from`, this function is called.
     *
     * It must return its Solidity selector to confirm the token transfer.
     * If any other value is returned or the interface is not implemented by the recipient, the transfer will be reverted.
     *
     * The selector can be obtained in Solidity with `IERC721.onERC721Received.selector`.
     */
    function onERC721Received(
        address operator,
        address from,
        uint256 tokenId,
        bytes calldata data
    ) external returns (bytes4);
}
"},"INFTX.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity 0.6.8;

import \"./Pausable.sol\";
import \"./IXToken.sol\";
import \"./IERC721.sol\";
import \"./EnumerableSet.sol\";
import \"./ReentrancyGuard.sol\";
import \"./SafeMath.sol\";

interface INFTX {
    event NFTsDeposited(uint256 vaultId, uint256[] nftIds, address from);
    event NFTsRedeemed(uint256 vaultId, uint256[] nftIds, address to);
    event TokensMinted(uint256 vaultId, uint256 amount, address to);
    event TokensBurned(uint256 vaultId, uint256 amount, address from);

    event EligibilitySet(uint256 vaultId, uint256[] nftIds, bool _boolean);
    event ReservesIncreased(uint256 vaultId, uint256 nftId);
    event ReservesDecreased(uint256 vaultId, uint256 nftId);

    function store() external returns (address);

    function transferOwnership(address newOwner) external;

    function vaultSize(uint256 vaultId) external view returns (uint256);

    function isEligible(uint256 vaultId, uint256 nftId)
        external
        view
        returns (bool);

    function createVault(address _erc20Address, address _nftAddress)
        external
        returns (uint256);

    function depositETH(uint256 vaultId) external payable;

    function setIsEligible(
        uint256 vaultId,
        uint256[] calldata nftIds,
        bool _boolean
    ) external;

    function setNegateEligibility(uint256 vaultId, bool shouldNegate) external;

    function setShouldReserve(
        uint256 vaultId,
        uint256[] calldata nftIds,
        bool _boolean
    ) external;

    function setIsReserved(
        uint256 vaultId,
        uint256[] calldata nftIds,
        bool _boolean
    ) external;

    function setExtension(address contractAddress, bool _boolean) external;

    function directRedeem(uint256 vaultId, uint256[] calldata nftIds)
        external
        payable;

    function mint(uint256 vaultId, uint256[] calldata nftIds, uint256 d2Amount)
        external
        payable;

    function redeem(uint256 vaultId, uint256 numNFTs) external payable;

    function mintAndRedeem(uint256 vaultId, uint256[] calldata nftIds)
        external
        payable;

    function changeTokenName(uint256 vaultId, string calldata newName) external;

    function changeTokenSymbol(uint256 vaultId, string calldata newSymbol)
        external;

    function setManager(uint256 vaultId, address newManager) external;

    function finalizeVault(uint256 vaultId) external;

    function closeVault(uint256 vaultId) external;

    function setMintFees(uint256 vaultId, uint256 _ethBase, uint256 _ethStep)
        external;

    function setBurnFees(uint256 vaultId, uint256 _ethBase, uint256 _ethStep)
        external;

    function setDualFees(uint256 vaultId, uint256 _ethBase, uint256 _ethStep)
        external;

    function setSupplierBounty(uint256 vaultId, uint256 ethMax, uint256 length)
        external;
}
"},"Initializable.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity \u003e=0.4.24 \u003c0.7.0;

/**
 * @title Initializable
 *
 * @dev Helper contract to support initializer functions. To use it, replace
 * the constructor with a function that has the `initializer` modifier.
 * WARNING: Unlike constructors, initializer functions must be manually
 * invoked. This applies both to deploying an Initializable contract, as well
 * as extending an Initializable contract via inheritance.
 * WARNING: When used with inheritance, manual care must be taken to not invoke
 * a parent initializer twice, or ensure that all initializers are idempotent,
 * because this is not dealt with automatically as with constructors.
 */
contract Initializable {
    /**
   * @dev Indicates that the contract has been initialized.
   */
    bool private initialized;

    /**
   * @dev Indicates that the contract is in the process of being initialized.
   */
    bool private initializing;

    /**
   * @dev Modifier to use in the initializer function of a contract.
   */
    modifier initializer() {
        require(
            initializing || isConstructor() || !initialized,
            \"Contract instance has already been initialized\"
        );

        bool isTopLevelCall = !initializing;
        if (isTopLevelCall) {
            initializing = true;
            initialized = true;
        }

        _;

        if (isTopLevelCall) {
            initializing = false;
        }
    }

    /// @dev Returns true if and only if the function is running in the constructor
    function isConstructor() private view returns (bool) {
        // extcodesize checks the size of the code stored in an address, and
        // address returns the current address. Since the code is still not
        // deployed when running a constructor, any checks on its code size will
        // yield zero, making it an effective way to detect if a contract is
        // under construction or not.
        address self = address(this);
        uint256 cs;
        assembly {
            cs := extcodesize(self)
        }
        return cs == 0;
    }

    // Reserved storage space to allow for layout changes in the future.
    uint256[50] private ______gap;
}
"},"ITokenManager.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity 0.6.8;

interface ITokenManager {
    function mint(address _receiver, uint256 _amount) external;
    function issue(uint256 _amount) external;
    function assign(address _receiver, uint256 _amount) external;
    function burn(address _holder, uint256 _amount) external;
    function assignVested(
        address _receiver,
        uint256 _amount,
        uint64 _start,
        uint64 _cliff,
        uint64 _vested,
        bool _revokable
    ) external returns (uint256);
    function revokeVesting(address _holder, uint256 _vestingId) external;
}
"},"ITransparentUpgradeableProxy.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.6.0;

interface ITransparentUpgradeableProxy {
    function admin() external returns (address);

    function implementation() external returns (address);

    function changeAdmin(address newAdmin) external;

    function upgradeTo(address newImplementation) external;

    function upgradeToAndCall(address newImplementation, bytes calldata data)
        external
        payable;
}
"},"IXStore.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity 0.6.8;

import \"./EnumerableSet.sol\";
import \"./Ownable.sol\";
import \"./SafeMath.sol\";
import \"./IXToken.sol\";
import \"./IERC721.sol\";
import \"./EnumerableSet.sol\";

interface IXStore {
    struct FeeParams {
        uint256 ethBase;
        uint256 ethStep;
    }

    struct BountyParams {
        uint256 ethMax;
        uint256 length;
    }

    struct Vault {
        address xTokenAddress;
        address nftAddress;
        address manager;
        IXToken xToken;
        IERC721 nft;
        EnumerableSet.UintSet holdings;
        EnumerableSet.UintSet reserves;
        mapping(uint256 =\u003e address) requester;
        mapping(uint256 =\u003e bool) isEligible;
        mapping(uint256 =\u003e bool) shouldReserve;
        bool allowMintRequests;
        bool flipEligOnRedeem;
        bool negateEligibility;
        bool isFinalized;
        bool isClosed;
        FeeParams mintFees;
        FeeParams burnFees;
        FeeParams dualFees;
        BountyParams supplierBounty;
        uint256 ethBalance;
        uint256 tokenBalance;
        bool isD2Vault;
        address d2AssetAddress;
        IERC20 d2Asset;
        uint256 d2Holdings;
    }

    function isExtension(address addr) external view returns (bool);

    function randNonce() external view returns (uint256);

    function vaultsLength() external view returns (uint256);

    function xTokenAddress(uint256 vaultId) external view returns (address);

    function nftAddress(uint256 vaultId) external view returns (address);

    function manager(uint256 vaultId) external view returns (address);

    function xToken(uint256 vaultId) external view returns (IXToken);

    function nft(uint256 vaultId) external view returns (IERC721);

    function holdingsLength(uint256 vaultId) external view returns (uint256);

    function holdingsContains(uint256 vaultId, uint256 elem)
        external
        view
        returns (bool);

    function holdingsAt(uint256 vaultId, uint256 index)
        external
        view
        returns (uint256);

    function reservesLength(uint256 vaultId) external view returns (uint256);

    function reservesContains(uint256 vaultId, uint256 elem)
        external
        view
        returns (bool);

    function reservesAt(uint256 vaultId, uint256 index)
        external
        view
        returns (uint256);

    function requester(uint256 vaultId, uint256 id)
        external
        view
        returns (address);

    function isEligible(uint256 vaultId, uint256 id)
        external
        view
        returns (bool);

    function shouldReserve(uint256 vaultId, uint256 id)
        external
        view
        returns (bool);

    function allowMintRequests(uint256 vaultId) external view returns (bool);

    function flipEligOnRedeem(uint256 vaultId) external view returns (bool);

    function negateEligibility(uint256 vaultId) external view returns (bool);

    function isFinalized(uint256 vaultId) external view returns (bool);

    function isClosed(uint256 vaultId) external view returns (bool);

    function mintFees(uint256 vaultId) external view returns (uint256, uint256);

    function burnFees(uint256 vaultId) external view returns (uint256, uint256);

    function dualFees(uint256 vaultId) external view returns (uint256, uint256);

    function supplierBounty(uint256 vaultId)
        external
        view
        returns (uint256, uint256);

    function ethBalance(uint256 vaultId) external view returns (uint256);

    function tokenBalance(uint256 vaultId) external view returns (uint256);

    function isD2Vault(uint256 vaultId) external view returns (bool);

    function d2AssetAddress(uint256 vaultId) external view returns (address);

    function d2Asset(uint256 vaultId) external view returns (IERC20);

    function d2Holdings(uint256 vaultId) external view returns (uint256);

    function setXTokenAddress(uint256 vaultId, address _xTokenAddress) external;

    function setNftAddress(uint256 vaultId, address _assetAddress) external;

    function setManager(uint256 vaultId, address _manager) external;

    function setXToken(uint256 vaultId) external;

    function setNft(uint256 vaultId) external;

    function holdingsAdd(uint256 vaultId, uint256 elem) external;

    function holdingsRemove(uint256 vaultId, uint256 elem) external;

    function reservesAdd(uint256 vaultId, uint256 elem) external;

    function reservesRemove(uint256 vaultId, uint256 elem) external;

    function setRequester(uint256 vaultId, uint256 id, address _requester)
        external;

    function setIsEligible(uint256 vaultId, uint256 id, bool _bool) external;

    function setShouldReserve(uint256 vaultId, uint256 id, bool _shouldReserve)
        external;

    function setAllowMintRequests(uint256 vaultId, bool isAllowed) external;

    function setFlipEligOnRedeem(uint256 vaultId, bool flipElig) external;

    function setNegateEligibility(uint256 vaultId, bool negateElig) external;

    function setIsFinalized(uint256 vaultId, bool _isFinalized) external;

    function setIsClosed(uint256 vaultId, bool _isClosed) external;

    function setMintFees(uint256 vaultId, uint256 ethBase, uint256 ethStep)
        external;

    function setBurnFees(uint256 vaultId, uint256 ethBase, uint256 ethStep)
        external;

    function setDualFees(uint256 vaultId, uint256 ethBase, uint256 ethStep)
        external;

    function setSupplierBounty(uint256 vaultId, uint256 ethMax, uint256 length)
        external;

    function setEthBalance(uint256 vaultId, uint256 _ethBalance) external;

    function setTokenBalance(uint256 vaultId, uint256 _tokenBalance) external;

    function setIsD2Vault(uint256 vaultId, bool _isD2Vault) external;

    function setD2AssetAddress(uint256 vaultId, address _assetAddress) external;

    function setD2Asset(uint256 vaultId) external;

    function setD2Holdings(uint256 vaultId, uint256 _d2Holdings) external;

    ////////////////////////////////////////////////////////////

    function setIsExtension(address addr, bool _isExtension) external;

    function setRandNonce(uint256 _randNonce) external;

    function addNewVault() external returns (uint256);
}
"},"IXToken.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity 0.6.8;

import \"./IERC20.sol\";

interface IXToken is IERC20 {
    function owner() external returns (address);

    function burn(uint256 amount) external;

    function burnFrom(address account, uint256 amount) external;

    function mint(address to, uint256 amount) external;

    function changeName(string calldata name) external;

    function changeSymbol(string calldata symbol) external;

    function setVaultAddress(address vaultAddress) external;

    function transferOwnership(address newOwner) external;
}
"},"IXTokenClonable.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity 0.6.8;

import \"./IERC20.sol\";

interface IXTokenClonable is IERC20 {

    function initialize(string calldata name, string calldata symbol, address _owner) external;

    function owner() external returns (address);

    function burn(uint256 amount) external;

    function burnFrom(address account, uint256 amount) external;

    function mint(address to, uint256 amount) external;

    function changeName(string calldata name) external;

    function changeSymbol(string calldata symbol) external;

    function transferOwnership(address newOwner) external;
}"},"IXTokenFactory.sol":{"content":"// SPDX-License-Identifier: MIT
pragma solidity 0.6.8;

interface IXTokenFactory {

  function createXToken(string calldata name, string calldata symbol) external returns (address);

  event NewXToken(address _xTokenAddress);
}
"},"KittyTest.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity 0.6.8;

import \"./IERC721.sol\";
import \"./ERC721Holder.sol\";

contract KittyTest is ERC721Holder {
    address public kittyCoreAddress = 0x06012c8cf97BEaD5deAe237070F9587f8E7A266d;
    KittyCore kittyCore;
    IERC721 kittyNft;

    constructor() public {
        kittyCore = KittyCore(kittyCoreAddress);
        kittyNft = IERC721(kittyCoreAddress);
    }

    function testA1(uint256 tokenId, address toAddress) public {
        kittyNft.transferFrom(msg.sender, toAddress, tokenId);
    }

    function testA2(uint256 tokenId, address toAddress) public {
        kittyCore.transferFrom(msg.sender, toAddress, tokenId);
    }

    function testB1(uint256 tokenId, address toAddress) public {
        kittyNft.transferFrom(msg.sender, toAddress, tokenId);
    }

    function testB2(uint256 tokenId, address toAddress) public {
        kittyCore.transferFrom(msg.sender, toAddress, tokenId);
    }

    function depositA(uint256 tokenId) public {
        kittyNft.transferFrom(msg.sender, address(this), tokenId);
    }

    function depositB(uint256 tokenId) public {
        kittyCore.transferFrom(msg.sender, address(this), tokenId);
    }

    function withdrawA(uint256 tokenId) public {
        kittyNft.transferFrom(address(this), msg.sender, tokenId);
    }

    function withdrawB(uint256 tokenId) public {
        kittyCore.transferFrom(address(this), msg.sender, tokenId);
    }
}

interface KittyCore {
    function ownerOf(uint256 _tokenId) external view returns (address owner);
    function transferFrom(address _from, address _to, uint256 _tokenId)
        external;
    function transfer(address _to, uint256 _tokenId) external;
    function getKitty(uint256 _id)
        external
        view
        returns (
            bool,
            bool,
            uint256 _cooldownIndex,
            uint256,
            uint256,
            uint256,
            uint256,
            uint256,
            uint256 _generation,
            uint256
        );
    function kittyIndexToApproved(uint256 index)
        external
        view
        returns (address approved);
}
"},"NFTX.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity 0.6.8;

import \"./Pausable.sol\";
import \"./IXToken.sol\";
import \"./IERC721.sol\";
import \"./ReentrancyGuard.sol\";
import \"./ERC721Holder.sol\";
import \"./IXStore.sol\";
import \"./Initializable.sol\";
import \"./SafeERC20.sol\";

contract NFTX is Pausable, ReentrancyGuard, ERC721Holder {
    using SafeMath for uint256;
    using SafeERC20 for IERC20;

    event NewVault(uint256 indexed vaultId, address sender);
    event Mint(
        uint256 indexed vaultId,
        uint256[] nftIds,
        uint256 d2Amount,
        address sender
    );
    event Redeem(
        uint256 indexed vaultId,
        uint256[] nftIds,
        uint256 d2Amount,
        address sender
    );
    event MintRequested(
        uint256 indexed vaultId,
        uint256[] nftIds,
        address sender
    );

    IXStore public store;

    function initialize(address storeAddress) public initializer {
        initOwnable();
        initReentrancyGuard();
        store = IXStore(storeAddress);
    }

    /* function onlyManager(uint256 vaultId) internal view {
        
    } */

    function onlyPrivileged(uint256 vaultId) internal view {
        if (store.isFinalized(vaultId)) {
            require(msg.sender == owner(), \"Not owner\");
        } else {
            require(msg.sender == store.manager(vaultId), \"Not manager\");
        }
    }

    function isEligible(uint256 vaultId, uint256 nftId)
        public
        view
        virtual
        returns (bool)
    {
        return
            store.negateEligibility(vaultId)
                ? !store.isEligible(vaultId, nftId)
                : store.isEligible(vaultId, nftId);
    }

    function vaultSize(uint256 vaultId) public view virtual returns (uint256) {
        return
            store.isD2Vault(vaultId)
                ? store.d2Holdings(vaultId)
                : store.holdingsLength(vaultId).add(
                    store.reservesLength(vaultId)
                );
    }

    function _getPseudoRand(uint256 modulus)
        internal
        virtual
        returns (uint256)
    {
        store.setRandNonce(store.randNonce().add(1));
        return
            uint256(
                keccak256(abi.encodePacked(now, msg.sender, store.randNonce()))
            ) %
            modulus;
    }

    function _calcFee(
        uint256 amount,
        uint256 ethBase,
        uint256 ethStep,
        bool isD2
    ) internal pure virtual returns (uint256) {
        if (amount == 0) {
            return 0;
        } else if (isD2) {
            return ethBase.add(ethStep.mul(amount.sub(10**18)).div(10**18));
        } else {
            uint256 n = amount;
            uint256 nSub1 = amount \u003e= 1 ? n.sub(1) : 0;
            return ethBase.add(ethStep.mul(nSub1));
        }
    }

    function _calcBounty(uint256 vaultId, uint256 numTokens, bool isBurn)
        public
        view
        virtual
        returns (uint256)
    {
        (, uint256 length) = store.supplierBounty(vaultId);
        if (length == 0) return 0;
        uint256 ethBounty = 0;
        for (uint256 i = 0; i \u003c numTokens; i = i.add(1)) {
            uint256 _vaultSize = isBurn
                ? vaultSize(vaultId).sub(i.add(1))
                : vaultSize(vaultId).add(i);
            uint256 _ethBounty = _calcBountyHelper(vaultId, _vaultSize);
            ethBounty = ethBounty.add(_ethBounty);
        }
        return ethBounty;
    }

    function _calcBountyD2(uint256 vaultId, uint256 amount, bool isBurn)
        public
        view
        virtual
        returns (uint256)
    {
        (uint256 ethMax, uint256 length) = store.supplierBounty(vaultId);
        if (length == 0) return 0;
        uint256 prevSize = vaultSize(vaultId);
        uint256 prevDepth = prevSize \u003e length ? 0 : length.sub(prevSize);
        uint256 prevReward = _calcBountyD2Helper(ethMax, length, prevSize);
        uint256 newSize = isBurn
            ? vaultSize(vaultId).sub(amount)
            : vaultSize(vaultId).add(amount);
        uint256 newDepth = newSize \u003e length ? 0 : length.sub(newSize);
        uint256 newReward = _calcBountyD2Helper(ethMax, length, newSize);
        uint256 prevTriangle = prevDepth.mul(prevReward).div(2).div(10**18);
        uint256 newTriangle = newDepth.mul(newReward).div(2).div(10**18);
        return
            isBurn
                ? newTriangle.sub(prevTriangle)
                : prevTriangle.sub(newTriangle);
    }

    function _calcBountyD2Helper(uint256 ethMax, uint256 length, uint256 size)
        internal
        pure
        returns (uint256)
    {
        if (size \u003e= length) return 0;
        return ethMax.sub(ethMax.mul(size).div(length));
    }

    function _calcBountyHelper(uint256 vaultId, uint256 _vaultSize)
        internal
        view
        virtual
        returns (uint256)
    {
        (uint256 ethMax, uint256 length) = store.supplierBounty(vaultId);
        if (_vaultSize \u003e= length) return 0;
        uint256 depth = length.sub(_vaultSize);
        return ethMax.mul(depth).div(length);
    }

    function createVault(
        address _xTokenAddress,
        address _assetAddress,
        bool _isD2Vault
    ) public virtual nonReentrant returns (uint256) {
        onlyOwnerIfPaused(0);
        IXToken xToken = IXToken(_xTokenAddress);
        require(xToken.owner() == address(this), \"Wrong owner\");
        uint256 vaultId = store.addNewVault();
        store.setXTokenAddress(vaultId, _xTokenAddress);

        store.setXToken(vaultId);
        if (!_isD2Vault) {
            store.setNftAddress(vaultId, _assetAddress);
            store.setNft(vaultId);
            store.setNegateEligibility(vaultId, true);
        } else {
            store.setD2AssetAddress(vaultId, _assetAddress);
            store.setD2Asset(vaultId);
            store.setIsD2Vault(vaultId, true);
        }
        store.setManager(vaultId, msg.sender);
        emit NewVault(vaultId, msg.sender);
        return vaultId;
    }

    function depositETH(uint256 vaultId) public payable virtual {
        store.setEthBalance(vaultId, store.ethBalance(vaultId).add(msg.value));
    }

    function _payEthFromVault(
        uint256 vaultId,
        uint256 amount,
        address payable to
    ) internal virtual {
        uint256 ethBalance = store.ethBalance(vaultId);
        uint256 amountToSend = ethBalance \u003c amount ? ethBalance : amount;
        if (amountToSend \u003e 0) {
            store.setEthBalance(vaultId, ethBalance.sub(amountToSend));
            to.transfer(amountToSend);
        }
    }

    function _receiveEthToVault(
        uint256 vaultId,
        uint256 amountRequested,
        uint256 amountSent
    ) internal virtual {
        require(amountSent \u003e= amountRequested, \"Value too low\");
        store.setEthBalance(
            vaultId,
            store.ethBalance(vaultId).add(amountRequested)
        );
        if (amountSent \u003e amountRequested) {
            msg.sender.transfer(amountSent.sub(amountRequested));
        }
    }

    function requestMint(uint256 vaultId, uint256[] memory nftIds)
        public
        payable
        virtual
        nonReentrant
    {
        onlyOwnerIfPaused(1);
        require(store.allowMintRequests(vaultId), \"Not allowed\");
        // TODO: implement bounty + fees
        for (uint256 i = 0; i \u003c nftIds.length; i = i.add(1)) {
            require(
                store.nft(vaultId).ownerOf(nftIds[i]) != address(this),
                \"Already owner\"
            );
            store.nft(vaultId).safeTransferFrom(
                msg.sender,
                address(this),
                nftIds[i]
            );
            require(
                store.nft(vaultId).ownerOf(nftIds[i]) == address(this),
                \"Not received\"
            );
            store.setRequester(vaultId, nftIds[i], msg.sender);
        }
        emit MintRequested(vaultId, nftIds, msg.sender);
    }

    function revokeMintRequests(uint256 vaultId, uint256[] memory nftIds)
        public
        virtual
        nonReentrant
    {
        for (uint256 i = 0; i \u003c nftIds.length; i = i.add(1)) {
            require(
                store.requester(vaultId, nftIds[i]) == msg.sender,
                \"Not requester\"
            );
            store.setRequester(vaultId, nftIds[i], address(0));
            store.nft(vaultId).safeTransferFrom(
                address(this),
                msg.sender,
                nftIds[i]
            );
        }
    }

    function approveMintRequest(uint256 vaultId, uint256[] memory nftIds)
        public
        virtual
    {
        onlyPrivileged(vaultId);
        for (uint256 i = 0; i \u003c nftIds.length; i = i.add(1)) {
            address requester = store.requester(vaultId, nftIds[i]);
            require(requester != address(0), \"No request\");
            require(
                store.nft(vaultId).ownerOf(nftIds[i]) == address(this),
                \"Not owner\"
            );
            store.setRequester(vaultId, nftIds[i], address(0));
            store.setIsEligible(vaultId, nftIds[i], true);
            if (store.shouldReserve(vaultId, nftIds[i])) {
                store.reservesAdd(vaultId, nftIds[i]);
            } else {
                store.holdingsAdd(vaultId, nftIds[i]);
            }
            store.xToken(vaultId).mint(requester, 10**18);
        }
    }

    function _mint(uint256 vaultId, uint256[] memory nftIds, bool isDualOp)
        internal
        virtual
    {
        for (uint256 i = 0; i \u003c nftIds.length; i = i.add(1)) {
            uint256 nftId = nftIds[i];
            require(isEligible(vaultId, nftId), \"Not eligible\");
            require(
                store.nft(vaultId).ownerOf(nftId) != address(this),
                \"Already owner\"
            );
            store.nft(vaultId).safeTransferFrom(
                msg.sender,
                address(this),
                nftId
            );
            require(
                store.nft(vaultId).ownerOf(nftId) == address(this),
                \"Not received\"
            );
            if (store.shouldReserve(vaultId, nftId)) {
                store.reservesAdd(vaultId, nftId);
            } else {
                store.holdingsAdd(vaultId, nftId);
            }
        }
        if (!isDualOp) {
            uint256 amount = nftIds.length.mul(10**18);
            store.xToken(vaultId).mint(msg.sender, amount);
        }
    }

    function _mintD2(uint256 vaultId, uint256 amount) internal virtual {
        store.d2Asset(vaultId).safeTransferFrom(
            msg.sender,
            address(this),
            amount
        );
        store.xToken(vaultId).mint(msg.sender, amount);
        store.setD2Holdings(vaultId, store.d2Holdings(vaultId).add(amount));
    }

    function _redeem(uint256 vaultId, uint256 numNFTs, bool isDualOp)
        internal
        virtual
    {
        for (uint256 i = 0; i \u003c numNFTs; i = i.add(1)) {
            uint256[] memory nftIds = new uint256[](1);
            if (store.holdingsLength(vaultId) \u003e 0) {
                uint256 rand = _getPseudoRand(store.holdingsLength(vaultId));
                nftIds[0] = store.holdingsAt(vaultId, rand);
            } else {
                uint256 rand = _getPseudoRand(store.reservesLength(vaultId));
                nftIds[0] = store.reservesAt(vaultId, rand);
            }
            _redeemHelper(vaultId, nftIds, isDualOp);
            emit Redeem(vaultId, nftIds, 0, msg.sender);
        }
    }

    function _redeemD2(uint256 vaultId, uint256 amount) internal virtual {
        store.xToken(vaultId).burnFrom(msg.sender, amount);
        store.d2Asset(vaultId).safeTransfer(msg.sender, amount);
        store.setD2Holdings(vaultId, store.d2Holdings(vaultId).sub(amount));
        uint256[] memory nftIds = new uint256[](0);
        emit Redeem(vaultId, nftIds, amount, msg.sender);
    }

    function _redeemHelper(
        uint256 vaultId,
        uint256[] memory nftIds,
        bool isDualOp
    ) internal virtual {
        if (!isDualOp) {
            store.xToken(vaultId).burnFrom(
                msg.sender,
                nftIds.length.mul(10**18)
            );
        }
        for (uint256 i = 0; i \u003c nftIds.length; i = i.add(1)) {
            uint256 nftId = nftIds[i];
            require(
                store.holdingsContains(vaultId, nftId) ||
                    store.reservesContains(vaultId, nftId),
                \"NFT not in vault\"
            );
            if (store.holdingsContains(vaultId, nftId)) {
                store.holdingsRemove(vaultId, nftId);
            } else {
                store.reservesRemove(vaultId, nftId);
            }
            if (store.flipEligOnRedeem(vaultId)) {
                bool isElig = store.isEligible(vaultId, nftId);
                store.setIsEligible(vaultId, nftId, !isElig);
            }
            store.nft(vaultId).safeTransferFrom(
                address(this),
                msg.sender,
                nftId
            );
        }
    }

    function mint(uint256 vaultId, uint256[] memory nftIds, uint256 d2Amount)
        public
        payable
        virtual
        nonReentrant
    {
        onlyOwnerIfPaused(1);
        uint256 amount = store.isD2Vault(vaultId) ? d2Amount : nftIds.length;
        uint256 ethBounty = store.isD2Vault(vaultId)
            ? _calcBountyD2(vaultId, d2Amount, false)
            : _calcBounty(vaultId, amount, false);
        (uint256 ethBase, uint256 ethStep) = store.mintFees(vaultId);
        uint256 ethFee = _calcFee(
            amount,
            ethBase,
            ethStep,
            store.isD2Vault(vaultId)
        );
        if (ethFee \u003e ethBounty) {
            _receiveEthToVault(vaultId, ethFee.sub(ethBounty), msg.value);
        }
        if (store.isD2Vault(vaultId)) {
            _mintD2(vaultId, d2Amount);
        } else {
            _mint(vaultId, nftIds, false);
        }
        if (ethBounty \u003e ethFee) {
            _payEthFromVault(vaultId, ethBounty.sub(ethFee), msg.sender);
        }
        emit Mint(vaultId, nftIds, d2Amount, msg.sender);
    }

    function redeem(uint256 vaultId, uint256 amount)
        public
        payable
        virtual
        nonReentrant
    {
        onlyOwnerIfPaused(2);
        if (!store.isClosed(vaultId)) {
            uint256 ethBounty = store.isD2Vault(vaultId)
                ? _calcBountyD2(vaultId, amount, true)
                : _calcBounty(vaultId, amount, true);
            (uint256 ethBase, uint256 ethStep) = store.burnFees(vaultId);
            uint256 ethFee = _calcFee(
                amount,
                ethBase,
                ethStep,
                store.isD2Vault(vaultId)
            );
            if (ethBounty.add(ethFee) \u003e 0) {
                _receiveEthToVault(vaultId, ethBounty.add(ethFee), msg.value);
            }
        }
        if (!store.isD2Vault(vaultId)) {
            _redeem(vaultId, amount, false);
        } else {
            _redeemD2(vaultId, amount);
        }

    }

    /* function mintAndRedeem(uint256 vaultId, uint256[] memory nftIds)
        public
        payable
        virtual
        nonReentrant
    {
        onlyOwnerIfPaused(3);
        require(!store.isD2Vault(vaultId), \"Is D2 vault\");
        require(!store.isClosed(vaultId), \"Vault is closed\");
        (uint256 ethBase, uint256 ethStep) = store.dualFees(vaultId);
        uint256 ethFee = _calcFee(
            nftIds.length,
            ethBase,
            ethStep,
            store.isD2Vault(vaultId)
        );
        if (ethFee \u003e 0) {
            _receiveEthToVault(vaultId, ethFee, msg.value);
        }
        _mint(vaultId, nftIds, true);
        _redeem(vaultId, nftIds.length, true);
    } */

    function setIsEligible(
        uint256 vaultId,
        uint256[] memory nftIds,
        bool _boolean
    ) public virtual {
        onlyPrivileged(vaultId);
        for (uint256 i = 0; i \u003c nftIds.length; i = i.add(1)) {
            store.setIsEligible(vaultId, nftIds[i], _boolean);
        }
    }

    function setAllowMintRequests(uint256 vaultId, bool isAllowed)
        public
        virtual
    {
        onlyPrivileged(vaultId);
        store.setAllowMintRequests(vaultId, isAllowed);
    }

    function setFlipEligOnRedeem(uint256 vaultId, bool flipElig)
        public
        virtual
    {
        onlyPrivileged(vaultId);
        store.setFlipEligOnRedeem(vaultId, flipElig);
    }

    function setNegateEligibility(uint256 vaultId, bool shouldNegate)
        public
        virtual
    {
        onlyPrivileged(vaultId);
        require(
            store
                .holdingsLength(vaultId)
                .add(store.reservesLength(vaultId))
                .add(store.d2Holdings(vaultId)) ==
                0,
            \"Vault not empty\"
        );
        store.setNegateEligibility(vaultId, shouldNegate);
    }

    /* function setShouldReserve(
        uint256 vaultId,
        uint256[] memory nftIds,
        bool _boolean
    ) public virtual {
        onlyPrivileged(vaultId);
        for (uint256 i = 0; i \u003c nftIds.length; i.add(1)) {
            store.setShouldReserve(vaultId, nftIds[i], _boolean);
        }
    } */

    /* function setIsReserved(
        uint256 vaultId,
        uint256[] memory nftIds,
        bool _boolean
    ) public virtual {
        onlyPrivileged(vaultId);
        for (uint256 i = 0; i \u003c nftIds.length; i.add(1)) {
            uint256 nftId = nftIds[i];
            if (_boolean) {
                require(
                    store.holdingsContains(vaultId, nftId),
                    \"Invalid nftId\"
                );
                store.holdingsRemove(vaultId, nftId);
                store.reservesAdd(vaultId, nftId);
            } else {
                require(
                    store.reservesContains(vaultId, nftId),
                    \"Invalid nftId\"
                );
                store.reservesRemove(vaultId, nftId);
                store.holdingsAdd(vaultId, nftId);
            }
        }
    } */

    function changeTokenName(uint256 vaultId, string memory newName)
        public
        virtual
    {
        onlyPrivileged(vaultId);
        store.xToken(vaultId).changeName(newName);
    }

    function changeTokenSymbol(uint256 vaultId, string memory newSymbol)
        public
        virtual
    {
        onlyPrivileged(vaultId);
        store.xToken(vaultId).changeSymbol(newSymbol);
    }

    function setManager(uint256 vaultId, address newManager) public virtual {
        onlyPrivileged(vaultId);
        store.setManager(vaultId, newManager);
    }

    function finalizeVault(uint256 vaultId) public virtual {
        onlyPrivileged(vaultId);
        if (!store.isFinalized(vaultId)) {
            store.setIsFinalized(vaultId, true);
        }
    }

    function closeVault(uint256 vaultId) public virtual {
        onlyPrivileged(vaultId);
        if (!store.isFinalized(vaultId)) {
            store.setIsFinalized(vaultId, true);
        }
        store.setIsClosed(vaultId, true);
    }

    function setMintFees(uint256 vaultId, uint256 _ethBase, uint256 _ethStep)
        public
        virtual
    {
        onlyPrivileged(vaultId);
        store.setMintFees(vaultId, _ethBase, _ethStep);
    }

    function setBurnFees(uint256 vaultId, uint256 _ethBase, uint256 _ethStep)
        public
        virtual
    {
        onlyPrivileged(vaultId);
        store.setBurnFees(vaultId, _ethBase, _ethStep);
    }

    /* function setDualFees(uint256 vaultId, uint256 _ethBase, uint256 _ethStep)
        public
        virtual
    {
        onlyPrivileged(vaultId);
        store.setDualFees(vaultId, _ethBase, _ethStep);
    } */

    function setSupplierBounty(uint256 vaultId, uint256 ethMax, uint256 length)
        public
        virtual
    {
        onlyPrivileged(vaultId);
        store.setSupplierBounty(vaultId, ethMax, length);
    }

}
"},"NFTXv2.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity 0.6.8;

import \"./NFTX.sol\";

contract NFTXv2 is NFTX {
    /* function transferERC721(uint256 vaultId, uint256 tokenId, address to)
        public
        virtual
        onlyOwner
    {
        store.nft(vaultId).transferFrom(address(this), to, tokenId);
    }

    function createVault(
        address _xTokenAddress,
        address _assetAddress,
        bool _isD2Vault
    ) public virtual override nonReentrant returns (uint256) {
        if (_xTokenAddress != _assetAddress \u0026\u0026 _isD2Vault) {
            return 0;
        }
        return 0;
    } */

    function _mint(uint256 vaultId, uint256[] memory nftIds, bool isDualOp)
        internal
        virtual
        override
    {
        for (uint256 i = 0; i \u003c nftIds.length; i = i.add(1)) {
            uint256 nftId = nftIds[i];
            require(isEligible(vaultId, nftId), \"Not eligible\");
            require(
                store.nft(vaultId).ownerOf(nftId) != address(this),
                \"Already owner\"
            );
            store.nft(vaultId).transferFrom(msg.sender, address(this), nftId);
            require(
                store.nft(vaultId).ownerOf(nftId) == address(this),
                \"Not received\"
            );
            if (store.shouldReserve(vaultId, nftId)) {
                store.reservesAdd(vaultId, nftId);
            } else {
                store.holdingsAdd(vaultId, nftId);
            }
        }
        if (!isDualOp) {
            uint256 amount = nftIds.length.mul(10**18);
            store.xToken(vaultId).mint(msg.sender, amount);
        }
    }
}
"},"NFTXv3.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity 0.6.8;

import \"./NFTXv2.sol\";

contract NFTXv3 is NFTXv2 {
    function requestMint(uint256 vaultId, uint256[] memory nftIds)
        public
        payable
        virtual
        override
        nonReentrant
    {
        onlyOwnerIfPaused(1);
        require(store.allowMintRequests(vaultId), \"Not allowed\");
        // TODO: implement bounty + fees
        for (uint256 i = 0; i \u003c nftIds.length; i = i.add(1)) {
            require(
                store.nft(vaultId).ownerOf(nftIds[i]) != address(this),
                \"Already owner\"
            );
            store.nft(vaultId).transferFrom(
                msg.sender,
                address(this),
                nftIds[i]
            );
            require(
                store.nft(vaultId).ownerOf(nftIds[i]) == address(this),
                \"Not received\"
            );
            store.setRequester(vaultId, nftIds[i], msg.sender);
        }
        emit MintRequested(vaultId, nftIds, msg.sender);
    }
}
"},"NFTXv4.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity 0.6.8;

import \"./NFTXv3.sol\";

contract NFTXv4 is NFTXv3 {
    function _mintD2(uint256 vaultId, uint256 amount)
        internal
        virtual
        override
    {
        store.d2Asset(vaultId).safeTransferFrom(
            msg.sender,
            address(this),
            amount.mul(1000)
        );
        store.xToken(vaultId).mint(msg.sender, amount);
        store.setD2Holdings(
            vaultId,
            store.d2Holdings(vaultId).add(amount.mul(1000))
        );
    }

    function _redeemD2(uint256 vaultId, uint256 amount)
        internal
        virtual
        override
    {
        store.xToken(vaultId).burnFrom(msg.sender, amount);
        store.d2Asset(vaultId).safeTransfer(msg.sender, amount.mul(1000));
        store.setD2Holdings(
            vaultId,
            store.d2Holdings(vaultId).sub(amount.mul(1000))
        );
    }
}
"},"NFTXv5.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity 0.6.8;

import \"./NFTXv4.sol\";

contract NFTXv5 is NFTXv4 {
    function _calcFee(
        uint256 amount,
        uint256 ethBase,
        uint256 ethStep,
        bool isD2
    ) internal pure virtual override returns (uint256) {
        if (amount == 0) {
            return 0;
        } else if (isD2) {
            return 0; // this line was causing a bug when \u003c 1.0 of a D2 token was minted
            // probably won\u0027t be using fees much for this version of smart contracts anyway
        } else {
            uint256 n = amount;
            uint256 nSub1 = amount \u003e= 1 ? n.sub(1) : 0;
            return ethBase.add(ethStep.mul(nSub1));
        }
    }
}
"},"NFTXv6.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity 0.6.8;

import \"./NFTXv5.sol\";
import \"./IXTokenFactory.sol\";

contract NFTXv6 is NFTXv5 {
    function changeTokenName(uint256 vaultId, string memory newName)
        public
        virtual
        override
    {}

    function changeTokenSymbol(uint256 vaultId, string memory newSymbol)
        public
        virtual
        override
    {}

    /* function setSupplierBounty(uint256 vaultId, uint256 ethMax, uint256 length)
        public
        virtual
        override
    {} */

    /* IXTokenFactory public xTokenFactory; */

    /* function setXTokenFactoryAddress(address a) public onlyOwner {
      xTokenFactory = IXTokenFactory(a);
    } */

    function createVault(
        address _xTokenAddress,
        address _assetAddress,
        bool _isD2Vault
    ) public virtual override nonReentrant returns (uint256) {
        return 99999;
    }

    function createVault(
        string memory name,
        string memory symbol,
        address _assetAddress,
        bool _isD2Vault
    ) public virtual nonReentrant returns (uint256) {
        onlyOwnerIfPaused(0);
        IXTokenFactory xTokenFactory = IXTokenFactory(
            0xE7ac17cE2550f3a0B4fE3616515975eb093CEfea
        );
        address xTokenAddress = xTokenFactory.createXToken(name, symbol);
        uint256 vaultId = store.addNewVault();
        store.setXTokenAddress(vaultId, xTokenAddress);
        store.setXToken(vaultId);
        if (!_isD2Vault) {
            store.setNftAddress(vaultId, _assetAddress);
            store.setNft(vaultId);
            store.setNegateEligibility(vaultId, true);
        } else {
            store.setD2AssetAddress(vaultId, _assetAddress);
            store.setD2Asset(vaultId);
            store.setIsD2Vault(vaultId, true);
        }
        store.setManager(vaultId, msg.sender);
        emit NewVault(vaultId, msg.sender);
        return vaultId;
    }

    /* function redeemD1For(
        uint256 vaultId,
        uint256 amount,
        uint256[] memory nftIds,
        address recipient
    ) public payable virtual nonReentrant {
        onlyOwnerIfPaused(2);
        _redeemHelperFor(vaultId, nftIds, false, recipient);
        emit Redeem(vaultId, nftIds, 0, msg.sender);
    }

    function _redeemHelper(
        uint256 vaultId,
        uint256[] memory nftIds,
        bool isDualOp
    ) internal virtual override {
        _redeemHelperFor(vaultId, nftIds, isDualOp, msg.sender);
    }

    function _redeemHelperFor(
        uint256 vaultId,
        uint256[] memory nftIds,
        bool isDualOp,
        address recipient
    ) internal virtual {
        if (!isDualOp) {
            store.xToken(vaultId).burnFrom(
                msg.sender,
                nftIds.length.mul(10**18)
            );
        }
        for (uint256 i = 0; i \u003c nftIds.length; i = i.add(1)) {
            uint256 nftId = nftIds[i];
            require(
                store.holdingsContains(vaultId, nftId) ||
                    store.reservesContains(vaultId, nftId),
                \"NFT not in vault\"
            );
            if (store.holdingsContains(vaultId, nftId)) {
                store.holdingsRemove(vaultId, nftId);
            } else {
                store.reservesRemove(vaultId, nftId);
            }
            if (store.flipEligOnRedeem(vaultId)) {
                bool isElig = store.isEligible(vaultId, nftId);
                store.setIsEligible(vaultId, nftId, !isElig);
            }
            store.nft(vaultId).safeTransferFrom(
                address(this),
                recipient,
                nftId
            );
        }
    } */

}
"},"NFTXv6Rinkeby.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity 0.6.8;

import \"./NFTXv5.sol\";
import \"./IXTokenFactory.sol\";

contract NFTXv6Rinkeby is NFTXv5 {
    function changeTokenName(uint256 vaultId, string memory newName)
        public
        virtual
        override
    {}

    function changeTokenSymbol(uint256 vaultId, string memory newSymbol)
        public
        virtual
        override
    {}

    /* function setSupplierBounty(uint256 vaultId, uint256 ethMax, uint256 length)
        public
        virtual
        override
    {} */

    /* IXTokenFactory public xTokenFactory; */

    /* function setXTokenFactoryAddress(address a) public onlyOwner {
      xTokenFactory = IXTokenFactory(a);
    } */

    function createVault(
        address _xTokenAddress,
        address _assetAddress,
        bool _isD2Vault
    ) public virtual override nonReentrant returns (uint256) {
        return 99999;
    }

    function createVault(
        string memory name,
        string memory symbol,
        address _assetAddress,
        bool _isD2Vault
    ) public virtual nonReentrant returns (uint256) {
        onlyOwnerIfPaused(0);
        IXTokenFactory xTokenFactory = IXTokenFactory(
            0x8414cfbb676dC02d0eEe06756aF3383090C9388f
        );
        address xTokenAddress = xTokenFactory.createXToken(name, symbol);
        uint256 vaultId = store.addNewVault();
        store.setXTokenAddress(vaultId, xTokenAddress);
        store.setXToken(vaultId);
        if (!_isD2Vault) {
            store.setNftAddress(vaultId, _assetAddress);
            store.setNft(vaultId);
            store.setNegateEligibility(vaultId, true);
        } else {
            store.setD2AssetAddress(vaultId, _assetAddress);
            store.setD2Asset(vaultId);
            store.setIsD2Vault(vaultId, true);
        }
        store.setManager(vaultId, msg.sender);
        emit NewVault(vaultId, msg.sender);
        return vaultId;
    }

    /* function redeemD1For(
        uint256 vaultId,
        uint256 amount,
        uint256[] memory nftIds,
        address recipient
    ) public payable virtual nonReentrant {
        onlyOwnerIfPaused(2);
        _redeemHelperFor(vaultId, nftIds, false, recipient);
        emit Redeem(vaultId, nftIds, 0, msg.sender);
    }

    function _redeemHelper(
        uint256 vaultId,
        uint256[] memory nftIds,
        bool isDualOp
    ) internal virtual override {
        _redeemHelperFor(vaultId, nftIds, isDualOp, msg.sender);
    }

    function _redeemHelperFor(
        uint256 vaultId,
        uint256[] memory nftIds,
        bool isDualOp,
        address recipient
    ) internal virtual {
        if (!isDualOp) {
            store.xToken(vaultId).burnFrom(
                msg.sender,
                nftIds.length.mul(10**18)
            );
        }
        for (uint256 i = 0; i \u003c nftIds.length; i = i.add(1)) {
            uint256 nftId = nftIds[i];
            require(
                store.holdingsContains(vaultId, nftId) ||
                    store.reservesContains(vaultId, nftId),
                \"NFT not in vault\"
            );
            if (store.holdingsContains(vaultId, nftId)) {
                store.holdingsRemove(vaultId, nftId);
            } else {
                store.reservesRemove(vaultId, nftId);
            }
            if (store.flipEligOnRedeem(vaultId)) {
                bool isElig = store.isEligible(vaultId, nftId);
                store.setIsEligible(vaultId, nftId, !isElig);
            }
            store.nft(vaultId).safeTransferFrom(
                address(this),
                recipient,
                nftId
            );
        }
    } */

}
"},"NFTXv6_testing.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity 0.6.8;

import \"./NFTXv5.sol\";
import \"./IXTokenFactory.sol\";

contract NFTXv6 is NFTXv5 {
    function changeTokenName(uint256 vaultId, string memory newName)
        public
        virtual
        override
    {}

    function changeTokenSymbol(uint256 vaultId, string memory newSymbol)
        public
        virtual
        override
    {}

    /* function setSupplierBounty(uint256 vaultId, uint256 ethMax, uint256 length)
        public
        virtual
        override
    {} */

    /* IXTokenFactory public xTokenFactory; */

    /* function setXTokenFactoryAddress(address a) public onlyOwner {
      xTokenFactory = IXTokenFactory(a);
    } */

    // function createVault(
    //     address _xTokenAddress,
    //     address _assetAddress,
    //     bool _isD2Vault
    // ) public virtual override nonReentrant returns (uint256) {
    //     return 99999;
    // }

    // function createVault(
    //     string memory name,
    //     string memory symbol,
    //     address _assetAddress,
    //     bool _isD2Vault
    // ) public virtual nonReentrant returns (uint256) {
    //     onlyOwnerIfPaused(0);
    //     IXTokenFactory xTokenFactory = IXTokenFactory(
    //         0xE7ac17cE2550f3a0B4fE3616515975eb093CEfea
    //     );
    //     address xTokenAddress = xTokenFactory.createXToken(name, symbol);
    //     uint256 vaultId = store.addNewVault();
    //     store.setXTokenAddress(vaultId, xTokenAddress);
    //     store.setXToken(vaultId);
    //     if (!_isD2Vault) {
    //         store.setNftAddress(vaultId, _assetAddress);
    //         store.setNft(vaultId);
    //         store.setNegateEligibility(vaultId, true);
    //     } else {
    //         store.setD2AssetAddress(vaultId, _assetAddress);
    //         store.setD2Asset(vaultId);
    //         store.setIsD2Vault(vaultId, true);
    //     }
    //     store.setManager(vaultId, msg.sender);
    //     emit NewVault(vaultId, msg.sender);
    //     return vaultId;
    // }

    /* function redeemD1For(
        uint256 vaultId,
        uint256 amount,
        uint256[] memory nftIds,
        address recipient
    ) public payable virtual nonReentrant {
        onlyOwnerIfPaused(2);
        _redeemHelperFor(vaultId, nftIds, false, recipient);
        emit Redeem(vaultId, nftIds, 0, msg.sender);
    }

    function _redeemHelper(
        uint256 vaultId,
        uint256[] memory nftIds,
        bool isDualOp
    ) internal virtual override {
        _redeemHelperFor(vaultId, nftIds, isDualOp, msg.sender);
    }

    function _redeemHelperFor(
        uint256 vaultId,
        uint256[] memory nftIds,
        bool isDualOp,
        address recipient
    ) internal virtual {
        if (!isDualOp) {
            store.xToken(vaultId).burnFrom(
                msg.sender,
                nftIds.length.mul(10**18)
            );
        }
        for (uint256 i = 0; i \u003c nftIds.length; i = i.add(1)) {
            uint256 nftId = nftIds[i];
            require(
                store.holdingsContains(vaultId, nftId) ||
                    store.reservesContains(vaultId, nftId),
                \"NFT not in vault\"
            );
            if (store.holdingsContains(vaultId, nftId)) {
                store.holdingsRemove(vaultId, nftId);
            } else {
                store.reservesRemove(vaultId, nftId);
            }
            if (store.flipEligOnRedeem(vaultId)) {
                bool isElig = store.isEligible(vaultId, nftId);
                store.setIsEligible(vaultId, nftId, !isElig);
            }
            store.nft(vaultId).safeTransferFrom(
                address(this),
                recipient,
                nftId
            );
        }
    } */

}
"},"NFTXv7.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity 0.6.8;

import \"./NFTXv6.sol\";
import \"./IERC20.sol\";
import \"./IERC1155.sol\";
import \"./IERC1155Receiver.sol\";

contract NFTXv7 is NFTXv6, IERC1155Receiver {

    mapping(uint256 =\u003e bool) public isVault1155;

    function setIs1155(
        uint256 vaultId,
        bool _boolean
    ) public virtual {
        onlyPrivileged(vaultId);
        isVault1155[vaultId] = _boolean;
    }

    function _mint(uint256 vaultId, uint256[] memory nftIds, bool isDualOp)
        internal
        virtual
        override
    {
        for (uint256 i = 0; i \u003c nftIds.length; i = i.add(1)) {
            uint256 nftId = nftIds[i];
            require(isEligible(vaultId, nftId), \"1\");
            
            if (isVault1155[vaultId]) {
                IERC1155 nft = IERC1155(store.nftAddress(vaultId));
                nft.safeTransferFrom(msg.sender, address(this), nftId, 1, \"\");
            } else {
                require(
                    store.nft(vaultId).ownerOf(nftId) != address(this),
                    \"2\"
                );
                store.nft(vaultId).transferFrom(msg.sender, address(this), nftId);
                require(
                    store.nft(vaultId).ownerOf(nftId) == address(this),
                    \"3\"
                );
            }
            
            store.holdingsAdd(vaultId, nftId);
        }
        store.xToken(vaultId).mint(msg.sender, nftIds.length.mul(10**18));
    }

    function _redeemHelper(
        uint256 vaultId,
        uint256[] memory nftIds,
        bool isDualOp
    ) internal virtual override {
        store.xToken(vaultId).burnFrom(msg.sender, nftIds.length.mul(10**18));
        for (uint256 i = 0; i \u003c nftIds.length; i = i.add(1)) {
            uint256 nftId = nftIds[i];
            require(
                store.holdingsContains(vaultId, nftId),
                \"1\"
            );
            if (store.holdingsContains(vaultId, nftId)) {
                store.holdingsRemove(vaultId, nftId);
            }
            if (store.flipEligOnRedeem(vaultId)) {
                bool isElig = store.isEligible(vaultId, nftId);
                store.setIsEligible(vaultId, nftId, !isElig);
            }
            if (isVault1155[vaultId]) {
                IERC1155 nft = IERC1155(store.nftAddress(vaultId));
                nft.safeTransferFrom(address(this), msg.sender, nftId, 1, \"\");
            } else {
                store.nft(vaultId).safeTransferFrom(
                    address(this),
                    msg.sender,
                    nftId
                );
            }
            
        }
    }

    function requestMint(uint256 vaultId, uint256[] memory nftIds)
        public
        payable
        virtual
        override
        nonReentrant
    {
        onlyOwnerIfPaused(1);
        require(store.allowMintRequests(vaultId), \"1\");
        // TODO: implement bounty + fees
        for (uint256 i = 0; i \u003c nftIds.length; i = i.add(1)) {
            require(
                store.nft(vaultId).ownerOf(nftIds[i]) != address(this),
                \"2\"
            );
            store.nft(vaultId).safeTransferFrom(
                msg.sender,
                address(this),
                nftIds[i]
            );
            require(
                store.nft(vaultId).ownerOf(nftIds[i]) == address(this),
                \"3\"
            );
            store.setRequester(vaultId, nftIds[i], msg.sender);
        }
        emit MintRequested(vaultId, nftIds, msg.sender);
    }

    function setNegateEligibility(uint256 vaultId, bool shouldNegate)
        public
        virtual
        override
    {
        onlyPrivileged(vaultId);
        require(
            store
                .holdingsLength(vaultId)
                .add(store.d2Holdings(vaultId)) ==
                0,
            \"1\"
        );
        store.setNegateEligibility(vaultId, shouldNegate);
    }

    function mint(uint256 vaultId, uint256[] memory nftIds, uint256 d2Amount)
        public
        payable
        virtual
        override
        nonReentrant
    {
        onlyOwnerIfPaused(1);
        // uint256 amount = store.isD2Vault(vaultId) ? d2Amount : nftIds.length;
        // uint256 ethBounty = store.isD2Vault(vaultId)
        //     ? _calcBountyD2(vaultId, d2Amount, false)
        //     : _calcBounty(vaultId, amount, false);
        // (uint256 ethBase, uint256 ethStep) = store.mintFees(vaultId);
        // uint256 ethFee = _calcFee(
        //     amount,
        //     ethBase,
        //     ethStep,
        //     store.isD2Vault(vaultId)
        // );
        // if (ethFee \u003e ethBounty) {
        //     _receiveEthToVault(vaultId, ethFee.sub(ethBounty), msg.value);
        // }
        if (store.isD2Vault(vaultId)) {
            _mintD2(vaultId, d2Amount);
        } else {
            _mint(vaultId, nftIds, false);
        }
        // if (ethBounty \u003e ethFee) {
        //     _payEthFromVault(vaultId, ethBounty.sub(ethFee), msg.sender);
        // }
        emit Mint(vaultId, nftIds, d2Amount, msg.sender);
    }

    function onERC1155Received(
        address,
        address,
        uint256,
        uint256,
        bytes memory
    ) public virtual override returns (bytes4) {
        return this.onERC1155Received.selector;
    }

    function onERC1155BatchReceived(
        address,
        address,
        uint256[] memory,
        uint256[] memory,
        bytes memory
    ) public virtual override returns (bytes4) {
        return this.onERC1155BatchReceived.selector;
    }

    function supportsInterface(bytes4 interfaceId)
        external
        override
        view
        returns (bool)
    {}

    function createVault(
        address _xTokenAddress,
        address _assetAddress,
        bool _isD2Vault
    ) public virtual override nonReentrant returns (uint256) {
        revert();
    }

    function setMintFees(uint256 vaultId, uint256 _ethBase, uint256 _ethStep)
        public
        virtual
        override
    {
        revert();
    }

    function setBurnFees(uint256 vaultId, uint256 _ethBase, uint256 _ethStep)
        public
        virtual
        override
    {
        revert();
    }

    function setSupplierBounty(uint256 vaultId, uint256 ethMax, uint256 length)
        public
        virtual
        override
    {
        revert();
    }

    mapping(uint256 =\u003e uint256) public rangeStart;
    mapping(uint256 =\u003e uint256) public rangeEnd;

    function setRange(
        uint256 vaultId,
        uint256 start,
        uint256 end
    ) public virtual {
        onlyPrivileged(vaultId);
        rangeStart[vaultId] = start;
        rangeEnd[vaultId] = end;
    }

    function isEligible(uint256 vaultId, uint256 nftId)
        public
        view
        virtual
        override
        returns (bool)
    {
        if (rangeEnd[vaultId] \u003e 0) {
            if (nftId \u003e= rangeStart[vaultId] \u0026\u0026 nftId \u003c= rangeEnd[vaultId]) {
                return true;
            }
        }
        return
            store.negateEligibility(vaultId)
                ? !store.isEligible(vaultId, nftId)
                : store.isEligible(vaultId, nftId);
        
    }
}
"},"NFTXv7Rinkeby.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity 0.6.8;

import \"./NFTXv6Rinkeby.sol\";
import \"./IERC20.sol\";
import \"./IERC1155.sol\";
import \"./IERC1155Receiver.sol\";

contract NFTXv7Rinkeby is NFTXv6Rinkeby, IERC1155Receiver {

    mapping(uint256 =\u003e bool) public isVault1155;

    function setIs1155(
        uint256 vaultId,
        bool _boolean
    ) public virtual {
        onlyPrivileged(vaultId);
        isVault1155[vaultId] = _boolean;
    }

    function _mint(uint256 vaultId, uint256[] memory nftIds, bool isDualOp)
        internal
        virtual
        override
    {
        for (uint256 i = 0; i \u003c nftIds.length; i = i.add(1)) {
            uint256 nftId = nftIds[i];
            require(isEligible(vaultId, nftId), \"1\");
            
            if (isVault1155[vaultId]) {
                IERC1155 nft = IERC1155(store.nftAddress(vaultId));
                nft.safeTransferFrom(msg.sender, address(this), nftId, 1, \"\");
            } else {
                require(
                    store.nft(vaultId).ownerOf(nftId) != address(this),
                    \"2\"
                );
                store.nft(vaultId).transferFrom(msg.sender, address(this), nftId);
                require(
                    store.nft(vaultId).ownerOf(nftId) == address(this),
                    \"3\"
                );
            }
            
            store.holdingsAdd(vaultId, nftId);
        }
        store.xToken(vaultId).mint(msg.sender, nftIds.length.mul(10**18));
    }

    function _redeemHelper(
        uint256 vaultId,
        uint256[] memory nftIds,
        bool isDualOp
    ) internal virtual override {
        store.xToken(vaultId).burnFrom(msg.sender, nftIds.length.mul(10**18));
        for (uint256 i = 0; i \u003c nftIds.length; i = i.add(1)) {
            uint256 nftId = nftIds[i];
            require(
                store.holdingsContains(vaultId, nftId),
                \"1\"
            );
            if (store.holdingsContains(vaultId, nftId)) {
                store.holdingsRemove(vaultId, nftId);
            }
            if (store.flipEligOnRedeem(vaultId)) {
                bool isElig = store.isEligible(vaultId, nftId);
                store.setIsEligible(vaultId, nftId, !isElig);
            }
            if (isVault1155[vaultId]) {
                IERC1155 nft = IERC1155(store.nftAddress(vaultId));
                nft.safeTransferFrom(address(this), msg.sender, nftId, 1, \"\");
            } else {
                store.nft(vaultId).safeTransferFrom(
                    address(this),
                    msg.sender,
                    nftId
                );
            }
            
        }
    }

    function requestMint(uint256 vaultId, uint256[] memory nftIds)
        public
        payable
        virtual
        override
        nonReentrant
    {
        onlyOwnerIfPaused(1);
        require(store.allowMintRequests(vaultId), \"1\");
        // TODO: implement bounty + fees
        for (uint256 i = 0; i \u003c nftIds.length; i = i.add(1)) {
            require(
                store.nft(vaultId).ownerOf(nftIds[i]) != address(this),
                \"2\"
            );
            store.nft(vaultId).safeTransferFrom(
                msg.sender,
                address(this),
                nftIds[i]
            );
            require(
                store.nft(vaultId).ownerOf(nftIds[i]) == address(this),
                \"3\"
            );
            store.setRequester(vaultId, nftIds[i], msg.sender);
        }
        emit MintRequested(vaultId, nftIds, msg.sender);
    }

    function setNegateEligibility(uint256 vaultId, bool shouldNegate)
        public
        virtual
        override
    {
        onlyPrivileged(vaultId);
        require(
            store
                .holdingsLength(vaultId)
                .add(store.d2Holdings(vaultId)) ==
                0,
            \"1\"
        );
        store.setNegateEligibility(vaultId, shouldNegate);
    }

    function mint(uint256 vaultId, uint256[] memory nftIds, uint256 d2Amount)
        public
        payable
        virtual
        override
        nonReentrant
    {
        onlyOwnerIfPaused(1);
        // uint256 amount = store.isD2Vault(vaultId) ? d2Amount : nftIds.length;
        // uint256 ethBounty = store.isD2Vault(vaultId)
        //     ? _calcBountyD2(vaultId, d2Amount, false)
        //     : _calcBounty(vaultId, amount, false);
        // (uint256 ethBase, uint256 ethStep) = store.mintFees(vaultId);
        // uint256 ethFee = _calcFee(
        //     amount,
        //     ethBase,
        //     ethStep,
        //     store.isD2Vault(vaultId)
        // );
        // if (ethFee \u003e ethBounty) {
        //     _receiveEthToVault(vaultId, ethFee.sub(ethBounty), msg.value);
        // }
        if (store.isD2Vault(vaultId)) {
            _mintD2(vaultId, d2Amount);
        } else {
            _mint(vaultId, nftIds, false);
        }
        // if (ethBounty \u003e ethFee) {
        //     _payEthFromVault(vaultId, ethBounty.sub(ethFee), msg.sender);
        // }
        emit Mint(vaultId, nftIds, d2Amount, msg.sender);
    }

    function onERC1155Received(
        address,
        address,
        uint256,
        uint256,
        bytes memory
    ) public virtual override returns (bytes4) {
        return this.onERC1155Received.selector;
    }

    function onERC1155BatchReceived(
        address,
        address,
        uint256[] memory,
        uint256[] memory,
        bytes memory
    ) public virtual override returns (bytes4) {
        return this.onERC1155BatchReceived.selector;
    }

    function supportsInterface(bytes4 interfaceId)
        external
        override
        view
        returns (bool)
    {}

    function createVault(
        address _xTokenAddress,
        address _assetAddress,
        bool _isD2Vault
    ) public virtual override nonReentrant returns (uint256) {
        revert();
    }

    function setMintFees(uint256 vaultId, uint256 _ethBase, uint256 _ethStep)
        public
        virtual
        override
    {
        revert();
    }

    function setBurnFees(uint256 vaultId, uint256 _ethBase, uint256 _ethStep)
        public
        virtual
        override
    {
        revert();
    }

    function setSupplierBounty(uint256 vaultId, uint256 ethMax, uint256 length)
        public
        virtual
        override
    {
        revert();
    }

    mapping(uint256 =\u003e uint256) public rangeStart;
    mapping(uint256 =\u003e uint256) public rangeEnd;

    function setRange(
        uint256 vaultId,
        uint256 start,
        uint256 end
    ) public virtual {
        onlyPrivileged(vaultId);
        rangeStart[vaultId] = start;
        rangeEnd[vaultId] = end;
    }

    function isEligible(uint256 vaultId, uint256 nftId)
        public
        view
        virtual
        override
        returns (bool)
    {
        if (rangeEnd[vaultId] \u003e 0) {
            if (nftId \u003e= rangeStart[vaultId] \u0026\u0026 nftId \u003c= rangeEnd[vaultId]) {
                return true;
            }
        }
        return
            store.negateEligibility(vaultId)
                ? !store.isEligible(vaultId, nftId)
                : store.isEligible(vaultId, nftId);
        
    }
}
"},"NFTXv8.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity 0.6.8;

import \"./NFTXv7.sol\";

contract NFTXv8 is NFTXv7 {
    function _redeemHelper(
        uint256 vaultId,
        uint256[] memory nftIds,
        bool isDualOp
    ) internal virtual override {
        store.xToken(vaultId).burnFrom(msg.sender, nftIds.length.mul(10**18));
        for (uint256 i = 0; i \u003c nftIds.length; i = i.add(1)) {
            uint256 nftId = nftIds[i];
            require(store.holdingsContains(vaultId, nftId), \"1\");
            if (store.holdingsContains(vaultId, nftId)) {
                store.holdingsRemove(vaultId, nftId);
            }
            if (store.flipEligOnRedeem(vaultId)) {
                bool isElig = store.isEligible(vaultId, nftId);
                store.setIsEligible(vaultId, nftId, !isElig);
            }
            if (isVault1155[vaultId]) {
                IERC1155 nft = IERC1155(store.nftAddress(vaultId));
                nft.safeTransferFrom(address(this), msg.sender, nftId, 1, \"\");
            } else if (vaultId \u003e 6 \u0026\u0026 vaultId \u003c 10) {
                store.nft(vaultId).transferFrom(
                    address(this),
                    msg.sender,
                    nftId
                );
            } else {
                store.nft(vaultId).safeTransferFrom(
                    address(this),
                    msg.sender,
                    nftId
                );
            }
        }
    }
}
"},"NFTXv9.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity 0.6.8;

import \"./NFTXv8.sol\";

interface KittyCore {
    function transfer(address _to, uint256 _tokenId) external;
}

contract NFTXv9 is NFTXv8 {
    function _redeemHelper(
        uint256 vaultId,
        uint256[] memory nftIds,
        bool isDualOp
    ) internal virtual override {
        store.xToken(vaultId).burnFrom(msg.sender, nftIds.length.mul(10**18));
        for (uint256 i = 0; i \u003c nftIds.length; i = i.add(1)) {
            uint256 nftId = nftIds[i];
            require(store.holdingsContains(vaultId, nftId), \"1\");
            if (store.holdingsContains(vaultId, nftId)) {
                store.holdingsRemove(vaultId, nftId);
            }
            if (store.flipEligOnRedeem(vaultId)) {
                bool isElig = store.isEligible(vaultId, nftId);
                store.setIsEligible(vaultId, nftId, !isElig);
            }
            if (isVault1155[vaultId]) {
                IERC1155 nft = IERC1155(store.nftAddress(vaultId));
                nft.safeTransferFrom(address(this), msg.sender, nftId, 1, \"\");
            } else if (vaultId \u003e 6 \u0026\u0026 vaultId \u003c 10) {
                KittyCore kittyCore = KittyCore(store.nftAddress(vaultId));
                kittyCore.transfer(msg.sender, nftId);
            } else {
                store.nft(vaultId).safeTransferFrom(
                    address(this),
                    msg.sender,
                    nftId
                );
            }
        }
    }
}
"},"Ownable.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity 0.6.8;

import \"./Context.sol\";
import \"./Initializable.sol\";

/**
 * @dev Contract module which provides a basic access control mechanism, where
 * there is an account (an owner) that can be granted exclusive access to
 * specific functions.
 *
 * By default, the owner account will be the one that deploys the contract. This
 * can later be changed with {transferOwnership}.
 *
 * This module is used through inheritance. It will make available the modifier
 * `onlyOwner`, which can be applied to your functions to restrict their use to
 * the owner.
 */
contract Ownable is Context, Initializable {
    address private _owner;

    event OwnershipTransferred(
        address indexed previousOwner,
        address indexed newOwner
    );

    /**
     * @dev Initializes the contract setting the deployer as the initial owner.
     */
    function initOwnable() internal virtual initializer {
        address msgSender = _msgSender();
        _owner = msgSender;
        emit OwnershipTransferred(address(0), msgSender);
    }

    /**
     * @dev Returns the address of the current owner.
     */
    function owner() public view returns (address) {
        return _owner;
    }

    /**
     * @dev Throws if called by any account other than the owner.
     */
    modifier onlyOwner() {
        require(_owner == _msgSender(), \"Ownable: caller is not the owner\");
        _;
    }

    /**
     * @dev Leaves the contract without owner. It will not be possible to call
     * `onlyOwner` functions anymore. Can only be called by the current owner.
     *
     * NOTE: Renouncing ownership will leave the contract without an owner,
     * thereby removing any functionality that is only available to the owner.
     */
    function renounceOwnership() public virtual onlyOwner {
        emit OwnershipTransferred(_owner, address(0));
        _owner = address(0);
    }

    /**
     * @dev Transfers ownership of the contract to a new account (`newOwner`).
     * Can only be called by the current owner.
     */
    function transferOwnership(address newOwner) public virtual onlyOwner {
        require(
            newOwner != address(0),
            \"Ownable: new owner is the zero address\"
        );
        emit OwnershipTransferred(_owner, newOwner);
        _owner = newOwner;
    }
}
"},"OwnableRegular.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.6.0;

import \"./Context.sol\";
/**
 * @dev Contract module which provides a basic access control mechanism, where
 * there is an account (an owner) that can be granted exclusive access to
 * specific functions.
 *
 * By default, the owner account will be the one that deploys the contract. This
 * can later be changed with {transferOwnership}.
 *
 * This module is used through inheritance. It will make available the modifier
 * `onlyOwner`, which can be applied to your functions to restrict their use to
 * the owner.
 */
contract Ownable is Context {
    address private _owner;

    event OwnershipTransferred(
        address indexed previousOwner,
        address indexed newOwner
    );

    /**
     * @dev Initializes the contract setting the deployer as the initial owner.
     */
    constructor() internal {
        address msgSender = _msgSender();
        _owner = msgSender;
        emit OwnershipTransferred(address(0), msgSender);
    }

    /**
     * @dev Returns the address of the current owner.
     */
    function owner() public view returns (address) {
        return _owner;
    }

    /**
     * @dev Throws if called by any account other than the owner.
     */
    modifier onlyOwner() {
        require(_owner == _msgSender(), \"Ownable: caller is not the owner\");
        _;
    }

    /**
     * @dev Leaves the contract without owner. It will not be possible to call
     * `onlyOwner` functions anymore. Can only be called by the current owner.
     *
     * NOTE: Renouncing ownership will leave the contract without an owner,
     * thereby removing any functionality that is only available to the owner.
     */
    function renounceOwnership() public virtual onlyOwner {
        emit OwnershipTransferred(_owner, address(0));
        _owner = address(0);
    }

    /**
     * @dev Transfers ownership of the contract to a new account (`newOwner`).
     * Can only be called by the current owner.
     */
    function transferOwnership(address newOwner) public virtual onlyOwner {
        require(
            newOwner != address(0),
            \"Ownable: new owner is the zero address\"
        );
        emit OwnershipTransferred(_owner, newOwner);
        _owner = newOwner;
    }
}
"},"OwnableUpgradeable.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity \u003e=0.6.0 \u003c0.8.0;

// import \"../utils/ContextUpgradeable.sol\";
import \"./ContextUpgradeable.sol\";
import \"./Initializable.sol\";
/**
 * @dev Contract module which provides a basic access control mechanism, where
 * there is an account (an owner) that can be granted exclusive access to
 * specific functions.
 *
 * By default, the owner account will be the one that deploys the contract. This
 * can later be changed with {transferOwnership}.
 *
 * This module is used through inheritance. It will make available the modifier
 * `onlyOwner`, which can be applied to your functions to restrict their use to
 * the owner.
 */
abstract contract OwnableUpgradeable is Initializable, ContextUpgradeable {
    address private _owner;

    event OwnershipTransferred(address indexed previousOwner, address indexed newOwner);

    /**
     * @dev Initializes the contract setting the deployer as the initial owner.
     */
    function __Ownable_init() internal initializer {
        __Context_init_unchained();
        __Ownable_init_unchained();
    }

    function __Ownable_init_unchained() internal initializer {
        address msgSender = _msgSender();
        _owner = msgSender;
        emit OwnershipTransferred(address(0), msgSender);
    }

    /**
     * @dev Returns the address of the current owner.
     */
    function owner() public view virtual returns (address) {
        return _owner;
    }

    /**
     * @dev Throws if called by any account other than the owner.
     */
    modifier onlyOwner() {
        require(owner() == _msgSender(), \"Ownable: caller is not the owner\");
        _;
    }

    /**
     * @dev Leaves the contract without owner. It will not be possible to call
     * `onlyOwner` functions anymore. Can only be called by the current owner.
     *
     * NOTE: Renouncing ownership will leave the contract without an owner,
     * thereby removing any functionality that is only available to the owner.
     */
    function renounceOwnership() public virtual onlyOwner {
        emit OwnershipTransferred(_owner, address(0));
        _owner = address(0);
    }

    /**
     * @dev Transfers ownership of the contract to a new account (`newOwner`).
     * Can only be called by the current owner.
     */
    function transferOwnership(address newOwner) public virtual onlyOwner {
        require(newOwner != address(0), \"Ownable: new owner is the zero address\");
        emit OwnershipTransferred(_owner, newOwner);
        _owner = newOwner;
    }
    uint256[49] private __gap;
}
"},"Pausable.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity 0.6.8;

import \"./Ownable.sol\";
import \"./SafeMath.sol\";

contract Pausable is Ownable {
    mapping(uint256 =\u003e bool) isPaused;
    // 0 : createVault
    // 1 : mint
    // 2 : redeem
    // 3 : mintAndRedeem

    function onlyOwnerIfPaused(uint256 pauserId) public view virtual {
        require(!isPaused[pauserId] || msg.sender == owner(), \"Paused\");
    }

    function setPaused(uint256 pauserId, bool _isPaused)
        public
        virtual
        onlyOwner
    {
        isPaused[pauserId] = _isPaused;
    }
}
"},"ProxyController.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity 0.6.8;

import \"./Ownable.sol\";
import \"./SafeMath.sol\";
import \"./ITransparentUpgradeableProxy.sol\";

contract ProxyController is Ownable {
    using SafeMath for uint256;

    ITransparentUpgradeableProxy private nftxProxy;
    address public implAddress;

    constructor(address nftx) public {
        initOwnable();
        nftxProxy = ITransparentUpgradeableProxy(nftx);
    }

    function getAdmin() public returns (address) {
        return nftxProxy.admin();
    }

    function fetchImplAddress() public {
        implAddress = nftxProxy.implementation();
    }

    function changeProxyAdmin(address newAdmin) public onlyOwner {
        nftxProxy.changeAdmin(newAdmin);
    }

    function upgradeProxyTo(address newImpl) public onlyOwner {
        nftxProxy.upgradeTo(newImpl);
    }
}
"},"ReentrancyGuard.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity 0.6.8;

import \"./Initializable.sol\";

/**
 * @dev Contract module that helps prevent reentrant calls to a function.
 *
 * Inheriting from `ReentrancyGuard` will make the {nonReentrant} modifier
 * available, which can be applied to functions to make sure there are no nested
 * (reentrant) calls to them.
 *
 * Note that because there is a single `nonReentrant` guard, functions marked as
 * `nonReentrant` may not call one another. This can be worked around by making
 * those functions `private`, and then adding `external` `nonReentrant` entry
 * points to them.
 *
 * TIP: If you would like to learn more about reentrancy and alternative ways
 * to protect against it, check out our blog post
 * https://blog.openzeppelin.com/reentrancy-after-istanbul/[Reentrancy After Istanbul].
 */
contract ReentrancyGuard is Initializable {
    // Booleans are more expensive than uint256 or any type that takes up a full
    // word because each write operation emits an extra SLOAD to first read the
    // slot\u0027s contents, replace the bits taken up by the boolean, and then write
    // back. This is the compiler\u0027s defense against contract upgrades and
    // pointer aliasing, and it cannot be disabled.

    // The values being non-zero value makes deployment a bit more expensive,
    // but in exchange the revault on every call to nonReentrant will be lower in
    // amount. Since revaults are capped to a percentage of the total
    // transaction\u0027s gas, it is best to keep them low in cases like this one, to
    // increase the likelihood of the full revault coming into effect.
    uint256 private constant _NOT_ENTERED = 1;
    uint256 private constant _ENTERED = 2;

    uint256 private _status;

    function initReentrancyGuard() internal {
        _status = _NOT_ENTERED;
    }

    /**
     * @dev Prevents a contract from calling itself, directly or indirectly.
     * Calling a `nonReentrant` function from another `nonReentrant`
     * function is not supported. It is possible to prevent this from happening
     * by making the `nonReentrant` function external, and make it call a
     * `private` function that does the actual work.
     */
    modifier nonReentrant() {
        // On the first call to nonReentrant, _notEntered will be true
        require(_status != _ENTERED, \"ReentrancyGuard: reentrant call\");

        // Any calls to nonReentrant after this point will fail
        _status = _ENTERED;

        _;

        // By storing the original value once again, a revault is triggered (see
        // https://eips.ethereum.org/EIPS/eip-2200)
        _status = _NOT_ENTERED;
    }
}
"},"SafeERC20.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity 0.6.8;

import \"./IERC20.sol\";
import \"./SafeMath.sol\";
import \"./Address.sol\";

/**
 * @title SafeERC20
 * @dev Wrappers around ERC20 operations that throw on failure (when the token
 * contract returns false). Tokens that return no value (and instead revert or
 * throw on failure) are also supported, non-reverting calls are assumed to be
 * successful.
 * To use this library you can add a `using SafeERC20 for IERC20;` statement to your contract,
 * which allows you to call the safe operations as `token.safeTransfer(...)`, etc.
 */
library SafeERC20 {
    using SafeMath for uint256;
    using Address for address;

    function safeTransfer(IERC20 token, address to, uint256 value) internal {
        _callOptionalReturn(
            token,
            abi.encodeWithSelector(token.transfer.selector, to, value)
        );
    }

    function safeTransferFrom(
        IERC20 token,
        address from,
        address to,
        uint256 value
    ) internal {
        _callOptionalReturn(
            token,
            abi.encodeWithSelector(token.transferFrom.selector, from, to, value)
        );
    }

    /**
     * @dev Deprecated. This function has issues similar to the ones found in
     * {IERC20-approve}, and its usage is discouraged.
     *
     * Whenever possible, use {safeIncreaseAllowance} and
     * {safeDecreaseAllowance} instead.
     */
    function safeApprove(IERC20 token, address spender, uint256 value)
        internal
    {
        // safeApprove should only be called when setting an initial allowance,
        // or when resetting it to zero. To increase and decrease it, use
        // \u0027safeIncreaseAllowance\u0027 and \u0027safeDecreaseAllowance\u0027
        // solhint-disable-next-line max-line-length
        require(
            (value == 0) || (token.allowance(address(this), spender) == 0),
            \"SafeERC20: approve from non-zero to non-zero allowance\"
        );
        _callOptionalReturn(
            token,
            abi.encodeWithSelector(token.approve.selector, spender, value)
        );
    }

    function safeIncreaseAllowance(IERC20 token, address spender, uint256 value)
        internal
    {
        uint256 newAllowance = token.allowance(address(this), spender).add(
            value
        );
        _callOptionalReturn(
            token,
            abi.encodeWithSelector(
                token.approve.selector,
                spender,
                newAllowance
            )
        );
    }

    function safeDecreaseAllowance(IERC20 token, address spender, uint256 value)
        internal
    {
        uint256 newAllowance = token.allowance(address(this), spender).sub(
            value,
            \"SafeERC20: decreased allowance below zero\"
        );
        _callOptionalReturn(
            token,
            abi.encodeWithSelector(
                token.approve.selector,
                spender,
                newAllowance
            )
        );
    }

    /**
     * @dev Imitates a Solidity high-level call (i.e. a regular function call to a contract), relaxing the requirement
     * on the return value: the return value is optional (but if data is returned, it must not be false).
     * @param token The token targeted by the call.
     * @param data The call data (encoded using abi.encode or one of its variants).
     */
    function _callOptionalReturn(IERC20 token, bytes memory data) private {
        // We need to perform a low level call here, to bypass Solidity\u0027s return data size checking mechanism, since
        // we\u0027re implementing it ourselves. We use {Address.functionCall} to perform this call, which verifies that
        // the target address contains contract code and also asserts for success in the low-level call.

        bytes memory returndata = address(token).functionCall(
            data,
            \"SafeERC20: low-level call failed\"
        );
        if (returndata.length \u003e 0) {
            // Return data is optional
            // solhint-disable-next-line max-line-length
            require(
                abi.decode(returndata, (bool)),
                \"SafeERC20: ERC20 operation did not succeed\"
            );
        }
    }
}
"},"SafeMath.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity 0.6.8;

/**
 * @dev Wrappers over Solidity\u0027s arithmetic operations with added overflow
 * checks.
 *
 * Arithmetic operations in Solidity wrap on overflow. This can easily result
 * in bugs, because programmers usually assume that an overflow raises an
 * error, which is the standard behavior in high level programming languages.
 * `SafeMath` restores this intuition by reverting the transaction when an
 * operation overflows.
 *
 * Using this library instead of the unchecked operations eliminates an entire
 * class of bugs, so it\u0027s recommended to use it always.
 */
library SafeMath {
    /**
     * @dev Returns the addition of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity\u0027s `+` operator.
     *
     * Requirements:
     *
     * - Addition cannot overflow.
     */
    function add(uint256 a, uint256 b) internal pure returns (uint256) {
        uint256 c = a + b;
        require(c \u003e= a, \"SafeMath: addition overflow\");

        return c;
    }

    /**
     * @dev Returns the subtraction of two unsigned integers, reverting on
     * overflow (when the result is negative).
     *
     * Counterpart to Solidity\u0027s `-` operator.
     *
     * Requirements:
     *
     * - Subtraction cannot overflow.
     */
    function sub(uint256 a, uint256 b) internal pure returns (uint256) {
        return sub(a, b, \"SafeMath: subtraction overflow\");
    }

    /**
     * @dev Returns the subtraction of two unsigned integers, reverting with custom message on
     * overflow (when the result is negative).
     *
     * Counterpart to Solidity\u0027s `-` operator.
     *
     * Requirements:
     *
     * - Subtraction cannot overflow.
     */
    function sub(uint256 a, uint256 b, string memory errorMessage)
        internal
        pure
        returns (uint256)
    {
        require(b \u003c= a, errorMessage);
        uint256 c = a - b;

        return c;
    }

    /**
     * @dev Returns the multiplication of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity\u0027s `*` operator.
     *
     * Requirements:
     *
     * - Multiplication cannot overflow.
     */
    function mul(uint256 a, uint256 b) internal pure returns (uint256) {
        // Gas optimization: this is cheaper than requiring \u0027a\u0027 not being zero, but the
        // benefit is lost if \u0027b\u0027 is also tested.
        // See: https://github.com/OpenZeppelin/openzeppelin-contracts/pull/522
        if (a == 0) {
            return 0;
        }

        uint256 c = a * b;
        require(c / a == b, \"SafeMath: multiplication overflow\");

        return c;
    }

    /**
     * @dev Returns the integer division of two unsigned integers. Reverts on
     * division by zero. The result is rounded towards zero.
     *
     * Counterpart to Solidity\u0027s `/` operator. Note: this function uses a
     * `revert` opcode (which leaves remaining gas untouched) while Solidity
     * uses an invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function div(uint256 a, uint256 b) internal pure returns (uint256) {
        return div(a, b, \"SafeMath: division by zero\");
    }

    /**
     * @dev Returns the integer division of two unsigned integers. Reverts with custom message on
     * division by zero. The result is rounded towards zero.
     *
     * Counterpart to Solidity\u0027s `/` operator. Note: this function uses a
     * `revert` opcode (which leaves remaining gas untouched) while Solidity
     * uses an invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function div(uint256 a, uint256 b, string memory errorMessage)
        internal
        pure
        returns (uint256)
    {
        require(b \u003e 0, errorMessage);
        uint256 c = a / b;
        // assert(a == b * c + a % b); // There is no case in which this doesn\u0027t hold

        return c;
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
     * Reverts when dividing by zero.
     *
     * Counterpart to Solidity\u0027s `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function mod(uint256 a, uint256 b) internal pure returns (uint256) {
        return mod(a, b, \"SafeMath: modulo by zero\");
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
     * Reverts with custom message when dividing by zero.
     *
     * Counterpart to Solidity\u0027s `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function mod(uint256 a, uint256 b, string memory errorMessage)
        internal
        pure
        returns (uint256)
    {
        require(b != 0, errorMessage);
        return a % b;
    }
}
"},"SafeMathUpgradeable.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity \u003e=0.6.0 \u003c0.8.0;

/**
 * @dev Wrappers over Solidity\u0027s arithmetic operations with added overflow
 * checks.
 *
 * Arithmetic operations in Solidity wrap on overflow. This can easily result
 * in bugs, because programmers usually assume that an overflow raises an
 * error, which is the standard behavior in high level programming languages.
 * `SafeMath` restores this intuition by reverting the transaction when an
 * operation overflows.
 *
 * Using this library instead of the unchecked operations eliminates an entire
 * class of bugs, so it\u0027s recommended to use it always.
 */
library SafeMathUpgradeable {
    /**
     * @dev Returns the addition of two unsigned integers, with an overflow flag.
     *
     * _Available since v3.4._
     */
    function tryAdd(uint256 a, uint256 b) internal pure returns (bool, uint256) {
        uint256 c = a + b;
        if (c \u003c a) return (false, 0);
        return (true, c);
    }

    /**
     * @dev Returns the substraction of two unsigned integers, with an overflow flag.
     *
     * _Available since v3.4._
     */
    function trySub(uint256 a, uint256 b) internal pure returns (bool, uint256) {
        if (b \u003e a) return (false, 0);
        return (true, a - b);
    }

    /**
     * @dev Returns the multiplication of two unsigned integers, with an overflow flag.
     *
     * _Available since v3.4._
     */
    function tryMul(uint256 a, uint256 b) internal pure returns (bool, uint256) {
        // Gas optimization: this is cheaper than requiring \u0027a\u0027 not being zero, but the
        // benefit is lost if \u0027b\u0027 is also tested.
        // See: https://github.com/OpenZeppelin/openzeppelin-contracts/pull/522
        if (a == 0) return (true, 0);
        uint256 c = a * b;
        if (c / a != b) return (false, 0);
        return (true, c);
    }

    /**
     * @dev Returns the division of two unsigned integers, with a division by zero flag.
     *
     * _Available since v3.4._
     */
    function tryDiv(uint256 a, uint256 b) internal pure returns (bool, uint256) {
        if (b == 0) return (false, 0);
        return (true, a / b);
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers, with a division by zero flag.
     *
     * _Available since v3.4._
     */
    function tryMod(uint256 a, uint256 b) internal pure returns (bool, uint256) {
        if (b == 0) return (false, 0);
        return (true, a % b);
    }

    /**
     * @dev Returns the addition of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity\u0027s `+` operator.
     *
     * Requirements:
     *
     * - Addition cannot overflow.
     */
    function add(uint256 a, uint256 b) internal pure returns (uint256) {
        uint256 c = a + b;
        require(c \u003e= a, \"SafeMath: addition overflow\");
        return c;
    }

    /**
     * @dev Returns the subtraction of two unsigned integers, reverting on
     * overflow (when the result is negative).
     *
     * Counterpart to Solidity\u0027s `-` operator.
     *
     * Requirements:
     *
     * - Subtraction cannot overflow.
     */
    function sub(uint256 a, uint256 b) internal pure returns (uint256) {
        require(b \u003c= a, \"SafeMath: subtraction overflow\");
        return a - b;
    }

    /**
     * @dev Returns the multiplication of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity\u0027s `*` operator.
     *
     * Requirements:
     *
     * - Multiplication cannot overflow.
     */
    function mul(uint256 a, uint256 b) internal pure returns (uint256) {
        if (a == 0) return 0;
        uint256 c = a * b;
        require(c / a == b, \"SafeMath: multiplication overflow\");
        return c;
    }

    /**
     * @dev Returns the integer division of two unsigned integers, reverting on
     * division by zero. The result is rounded towards zero.
     *
     * Counterpart to Solidity\u0027s `/` operator. Note: this function uses a
     * `revert` opcode (which leaves remaining gas untouched) while Solidity
     * uses an invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function div(uint256 a, uint256 b) internal pure returns (uint256) {
        require(b \u003e 0, \"SafeMath: division by zero\");
        return a / b;
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
     * reverting when dividing by zero.
     *
     * Counterpart to Solidity\u0027s `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function mod(uint256 a, uint256 b) internal pure returns (uint256) {
        require(b \u003e 0, \"SafeMath: modulo by zero\");
        return a % b;
    }

    /**
     * @dev Returns the subtraction of two unsigned integers, reverting with custom message on
     * overflow (when the result is negative).
     *
     * CAUTION: This function is deprecated because it requires allocating memory for the error
     * message unnecessarily. For custom revert reasons use {trySub}.
     *
     * Counterpart to Solidity\u0027s `-` operator.
     *
     * Requirements:
     *
     * - Subtraction cannot overflow.
     */
    function sub(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b \u003c= a, errorMessage);
        return a - b;
    }

    /**
     * @dev Returns the integer division of two unsigned integers, reverting with custom message on
     * division by zero. The result is rounded towards zero.
     *
     * CAUTION: This function is deprecated because it requires allocating memory for the error
     * message unnecessarily. For custom revert reasons use {tryDiv}.
     *
     * Counterpart to Solidity\u0027s `/` operator. Note: this function uses a
     * `revert` opcode (which leaves remaining gas untouched) while Solidity
     * uses an invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function div(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b \u003e 0, errorMessage);
        return a / b;
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
     * reverting with custom message when dividing by zero.
     *
     * CAUTION: This function is deprecated because it requires allocating memory for the error
     * message unnecessarily. For custom revert reasons use {tryMod}.
     *
     * Counterpart to Solidity\u0027s `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function mod(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b \u003e 0, errorMessage);
        return a % b;
    }
}
"},"Strings.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.6.0;

/**
 * @dev String operations.
 */
library Strings {
    /**
     * @dev Converts a `uint256` to its ASCII `string` representation.
     */
    function toString(uint256 value) internal pure returns (string memory) {
        // Inspired by OraclizeAPI\u0027s implementation - MIT licence
        // https://github.com/oraclize/ethereum-api/blob/b42146b063c7d6ee1358846c198246239e9360e8/oraclizeAPI_0.4.25.sol

        if (value == 0) {
            return \"0\";
        }
        uint256 temp = value;
        uint256 digits;
        while (temp != 0) {
            digits++;
            temp /= 10;
        }
        bytes memory buffer = new bytes(digits);
        uint256 index = digits - 1;
        temp = value;
        while (temp != 0) {
            buffer[index--] = byte(uint8(48 + temp % 10));
            temp /= 10;
        }
        return string(buffer);
    }
}
"},"TimeDelay.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity 0.6.8;

import \"./Ownable.sol\";
import \"./SafeMath.sol\";

contract TimeDelay is Ownable {
    using SafeMath for uint256;

    uint256 public shortDelay;
    uint256 public mediumDelay;
    uint256 public longDelay;

    function setDelays(
        uint256 _shortDelay,
        uint256 _mediumDelay,
        uint256 _longDelay
    ) internal virtual {
        shortDelay = _shortDelay;
        mediumDelay = _mediumDelay;
        longDelay = _longDelay;
    }

    function timeInDays(uint256 num) internal pure returns (uint256) {
        return num * 60 * 60 * 24;
    }

    function getDelay(uint256 delayIndex) public view returns (uint256) {
        if (delayIndex == 0) {
            return shortDelay;
        } else if (delayIndex == 1) {
            return mediumDelay;
        } else if (delayIndex == 2) {
            return longDelay;
        }
    }

    function onlyIfPastDelay(uint256 delayIndex, uint256 startTime)
        internal
        view
    {
        require(1 \u003e= startTime.add(getDelay(delayIndex)), \"Delay not over\");
    }
}
"},"Timelocked.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity 0.6.8;

import \"./Ownable.sol\";
import \"./SafeMath.sol\";

contract Timelocked is Ownable {
    using SafeMath for uint256;

    uint256 public shortDelay;
    uint256 public mediumDelay;
    uint256 public longDelay;

    function setDelays(
        uint256 _shortDelay,
        uint256 _mediumDelay,
        uint256 _longDelay
    ) internal virtual {
        shortDelay = _shortDelay;
        mediumDelay = _mediumDelay;
        longDelay = _longDelay;
    }

    function timeInDays(uint256 num) internal pure returns (uint256) {
        return num * 60 * 60 * 24;
    }

    function getDelay(uint256 delayIndex) public view returns (uint256) {
        if (delayIndex == 0) {
            return shortDelay;
        } else if (delayIndex == 1) {
            return mediumDelay;
        } else if (delayIndex == 2) {
            return longDelay;
        }
    }

    function onlyIfPastDelay(uint256 delayIndex, uint256 startTime)
        internal
        view
    {
        require(1 \u003e= startTime.add(getDelay(delayIndex)), \"Delay not over\");
    }
}
"},"TokenAppController.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity 0.6.8;

import \"./Ownable.sol\";
import \"./ITokenManager.sol\";

contract TokenAppController is Ownable {
    ITokenManager public tokenManager;
    address public tokenManagerAddr;

    function initTAC() internal {
        initOwnable();
    }

    function setTokenManager(address tokenManagerAddress) internal onlyOwner {
        tokenManagerAddr = tokenManagerAddress;
        tokenManager = ITokenManager(tokenManagerAddr);
    }

    function callMint(address _receiver, uint256 _amount) internal onlyOwner {
        tokenManager.mint(_receiver, _amount);
    }

    function callIssue(uint256 _amount) internal onlyOwner {
        tokenManager.issue(_amount);
    }

    function callAssign(address _receiver, uint256 _amount) internal onlyOwner {
        tokenManager.assign(_receiver, _amount);
    }

    function callBurn(address _holder, uint256 _amount) internal onlyOwner {
        tokenManager.burn(_holder, _amount);
    }

    function callAssignVested(
        address _receiver,
        uint256 _amount,
        uint64 _start,
        uint64 _cliff,
        uint64 _vested,
        bool _revokable
    ) internal returns (uint256) {
        return
            tokenManager.assignVested(
                _receiver,
                _amount,
                _start,
                _cliff,
                _vested,
                _revokable
            );
    }

    function callRevokeVesting(address _holder, uint256 _vestingId)
        internal
        onlyOwner
    {
        tokenManager.revokeVesting(_holder, _vestingId);
    }

}
"},"TokenMultiCall.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity 0.6.8;

import \"./ERC20.sol\";
import \"./ERC721.sol\";

contract TokenMultiCall {
    function getErc20Data(address addr)
        public
        view
        returns (string memory name, string memory symbol, uint256 totalSupply)
    {
        ERC20 erc20 = ERC20(addr);
        string memory _name = erc20.name();
        string memory _symbol = erc20.symbol();
        uint256 _totalSupply = erc20.totalSupply();

        return (_name, _symbol, _totalSupply);
    }

    function getErc721Data(address addr)
        public
        view
        returns (string memory name, string memory symbol)
    {
        ERC721 erc721 = ERC721(addr);
        string memory _name = erc721.name();
        string memory _symbol = erc721.symbol();

        return (_name, _symbol);
    }

    function getErc20And721Data(address erc20Addr, address erc721Addr)
        public
        view
        returns (
            string memory erc20Name,
            string memory erc20Symbol,
            uint256 erc20TotalSupply,
            string memory erc721name,
            string memory erc721symbol
        )
    {
        (string memory _erc20Name, string memory _erc20Symbol, uint256 _erc20TotalSupply) = getErc20Data(
            erc20Addr
        );
        (string memory _erc721Name, string memory _erc721Symbol) = getErc721Data(
            erc721Addr
        );

        return (
            _erc20Name,
            _erc20Symbol,
            _erc20TotalSupply,
            _erc721Name,
            _erc721Symbol
        );
    }

    function getDoubleErc20Data(address addr1, address addr2)
        public
        view
        returns (
            string memory name1,
            string memory symbol1,
            uint256 totalSupply1,
            string memory name2,
            string memory symbol2,
            uint256 totalSupply2
        )
    {
        (string memory _name1, string memory _symbol1, uint256 _totalSupply1) = getErc20Data(
            addr1
        );
        (string memory _name2, string memory _symbol2, uint256 _totalSupply2) = getErc20Data(
            addr2
        );

        return (
            _name1,
            _symbol1,
            _totalSupply1,
            _name2,
            _symbol2,
            _totalSupply2
        );
    }

}
"},"UpgradeController.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity 0.6.8;

import \"./ITransparentUpgradeableProxy.sol\";
import \"./ControllerBase.sol\";

contract UpgradeController is ControllerBase {
    using SafeMath for uint256;

    
    ITransparentUpgradeableProxy private nftxProxy;
    ITransparentUpgradeableProxy private xControllerProxy;

    constructor(address nftx, address xController) public {
        ControllerBase.initialize();
        nftxProxy = ITransparentUpgradeableProxy(nftx);
        xControllerProxy = ITransparentUpgradeableProxy(xController);
    }

    function executeFuncCall(uint256 fcId) public override onlyOwner {
        super.executeFuncCall(fcId);
        if (funcIndex[fcId] == 3) {
            nftxProxy.changeAdmin(addressParam[fcId]);
        } else if (funcIndex[fcId] == 4) {
            nftxProxy.upgradeTo(addressParam[fcId]);
        } else if (funcIndex[fcId] == 5) {
            xControllerProxy.changeAdmin(addressParam[fcId]);
        } else if (funcIndex[fcId] == 6) {
            xControllerProxy.upgradeTo(addressParam[fcId]);
        }
    }

}
"},"XBouties.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity 0.6.8;

import \"./TokenAppController.sol\";
import \"./IERC20.sol\";
import \"./IERC721.sol\";
import \"./IXStore.sol\";
import \"./SafeMath.sol\";
import \"./SafeERC20.sol\";
import \"./ReentrancyGuard.sol\";

contract XBounties is TokenAppController, ReentrancyGuard {
    using SafeMath for uint256;

    using SafeERC20 for IERC20;

    uint256 public constant BASE = 10**18;
    uint256 public interval = 15 * 60; // 15 minutes
    uint256 public start = 1608580800; // Mon, Dec 21 2020, 12pm PST
    uint64 public vestedUntil = 1609876800; // Tue, Jan 5 2021, 12pm PST

    IERC20 public nftxToken;
    address payable public daoMultiSig;

    struct Bounty {
        address tokenContract;
        uint256 nftxPrice;
        uint256 paidOut;
        uint256 payoutCap;
    }

    event NewBountyAdded(uint256 bountyId);
    event BountyFilled(
        uint256 bountyId,
        uint256 nftxAmount,
        uint256 assetAmount,
        address sender,
        uint64 start,
        uint64 cliff,
        uint64 vested
    );
    event NftxPriceSet(uint256 bountyId, uint256 newNftxPrice);
    event PayoutCapSet(uint256 bountyId, uint256 newCap);
    event BountyClosed(uint256 bountyId);
    event EthWithdrawn(uint256 amount);
    event Erc20Withdrawn(address tokenContract, uint256 amount);
    event Erc721Withdrawn(address nftContract, uint256 tokenId);

    Bounty[] internal bounties;

    constructor(
        address _tokenManager,
        address payable _daoMultiSig,
        address _nftxToken,
        address _xStore
    ) public {
        initTAC();
        setTokenManager(_tokenManager);
        daoMultiSig = _daoMultiSig;
        nftxToken = IERC20(_nftxToken);

        IXStore xStore = IXStore(_xStore);

        createEthBounty(130 * BASE, 65000 * BASE);
        createEthBounty(65 * BASE, 65000 * BASE);
        createEthBounty(BASE.mul(130).div(3), 65000 * BASE);
        createBounty(
            xStore.xTokenAddress(0), // PUNK-BASIC
            390 * BASE,
            31200 * BASE
        );
        createBounty(
            xStore.xTokenAddress(1), // PUNK-ATTR-4
            585 * BASE,
            14625 * BASE
        );
        createBounty(
            xStore.xTokenAddress(2), // PUNK-ATTR-5
            1950 * BASE,
            15600 * BASE
        );
        createBounty(
            xStore.xTokenAddress(3), // PUNK-ZOMBIE
            8450 * BASE,
            16900 * BASE
        );
        createBounty(
            xStore.xTokenAddress(4), // AXIE-ORIGIN
            130 * BASE,
            7800 * BASE
        );
        createBounty(
            xStore.xTokenAddress(5), // AXIE-MYSTIC-1
            780 * BASE,
            7800 * BASE
        );
        createBounty(
            xStore.xTokenAddress(6), // AXIE-MYSTIC-2
            3900 * BASE,
            7800 * BASE
        );
        createBounty(
            xStore.xTokenAddress(7), // KITTY-GEN-0
            26 * BASE,
            5850 * BASE
        );
        createBounty(
            xStore.xTokenAddress(8), // KITTY-GEN-0-F
            39 * BASE,
            5850 * BASE
        );
        createBounty(
            xStore.xTokenAddress(9), // KITTY-FOUNDER
            6175 * BASE,
            6175 * BASE
        );
        createBounty(
            xStore.xTokenAddress(10), // AVASTR-BASIC
            20 * BASE,
            6175 * BASE
        );
        createBounty(
            xStore.xTokenAddress(11), // AVASTR-RANK-30
            26 * BASE,
            6175 * BASE
        );
        createBounty(
            xStore.xTokenAddress(12), // AVASTR-RANK-60
            195 * BASE,
            6175 * BASE
        );
        createBounty(
            xStore.xTokenAddress(13), // GLYPH
            1300 * BASE,
            26000 * BASE
        );
        createBounty(
            xStore.xTokenAddress(14), // JOY
            455 * BASE,
            10010 * BASE
        );
    }

    function setStart(uint256 newStart) public onlyOwner {
        start = newStart;
    }

    function setInterval(uint256 newInterval) public onlyOwner {
        interval = newInterval;
    }

    function setVestedUntil(uint64 newTime) public onlyOwner {
        vestedUntil = newTime;
    }

    function getBountyInfo(uint256 bountyId)
        public
        view
        returns (address, uint256, uint256, uint256)
    {
        require(bountyId \u003c bounties.length, \"Invalid bountyId\");
        return (
            bounties[bountyId].tokenContract,
            bounties[bountyId].nftxPrice,
            bounties[bountyId].paidOut,
            bounties[bountyId].payoutCap
        );
    }

    function getMaxPayout() public view returns (uint256) {
        uint256 tMinus4 = start.sub(interval.mul(4));
        uint256 tMinus3 = start.sub(interval.mul(3));
        uint256 tMinus2 = start.sub(interval.mul(2));
        uint256 tMinus1 = start.sub(interval.mul(1));
        uint256 tm4Max = 0;
        uint256 tm3Max = 50 * BASE;
        uint256 tm2Max = 500 * BASE;
        uint256 tm1Max = 5000 * BASE;
        uint256 tm0Max = 50000 * BASE;
        if (now \u003c tMinus4) {
            return 0;
        } else if (now \u003c tMinus3) {
            uint256 progressBigNum = now.sub(tMinus4).mul(BASE).div(interval);
            uint256 addedPayout = tm3Max.sub(tm4Max).mul(progressBigNum).div(
                BASE
            );
            return tm4Max.add(addedPayout);
        } else if (now \u003c tMinus2) {
            uint256 progressBigNum = now.sub(tMinus3).mul(BASE).div(interval);
            uint256 addedPayout = tm2Max.sub(tm3Max).mul(progressBigNum).div(
                BASE
            );
            return tm3Max.add(addedPayout);
        } else if (now \u003c tMinus1) {
            uint256 progressBigNum = now.sub(tMinus2).mul(BASE).div(interval);
            uint256 addedPayout = tm1Max.sub(tm2Max).mul(progressBigNum).div(
                BASE
            );
            return tm2Max.add(addedPayout);
        } else if (now \u003c start) {
            uint256 progressBigNum = now.sub(tMinus1).mul(BASE).div(interval);
            uint256 addedPayout = tm0Max.sub(tm1Max).mul(progressBigNum).div(
                BASE
            );
            return tm1Max.add(addedPayout);
        } else {
            return tm0Max;
        }
    }

    function getBountiesLength() public view returns (uint256) {
        return bounties.length;
    }

    function getIsEth(uint256 bountyId) public view returns (bool) {
        require(bountyId \u003c bounties.length, \"Invalid bountyId\");
        return bounties[bountyId].tokenContract == address(0);
    }

    function getTokenContract(uint256 bountyId) public view returns (address) {
        require(bountyId \u003c bounties.length, \"Invalid bountyId\");
        return bounties[bountyId].tokenContract;
    }

    function getNftxPrice(uint256 bountyId) public view returns (uint256) {
        require(bountyId \u003c bounties.length, \"Invalid bountyId\");
        return bounties[bountyId].nftxPrice;
    }

    function getPayoutCap(uint256 bountyId) public view returns (uint256) {
        require(bountyId \u003c bounties.length, \"Invalid bountyId\");
        return bounties[bountyId].payoutCap;
    }

    function getPaidOut(uint256 bountyId) public view returns (uint256) {
        require(bountyId \u003c bounties.length, \"Invalid bountyId\");
        return bounties[bountyId].paidOut;
    }

    function setNftxPrice(uint256 bountyId, uint256 newPrice) public onlyOwner {
        require(bountyId \u003c bounties.length, \"Invalid bountyId\");
        bounties[bountyId].nftxPrice = newPrice;
        emit NftxPriceSet(bountyId, newPrice);
    }

    function setPayoutCap(uint256 bountyId, uint256 newCap) public onlyOwner {
        require(bountyId \u003c bounties.length, \"Invalid bountyId\");
        bounties[bountyId].payoutCap = newCap;
        emit PayoutCapSet(bountyId, newCap);
    }

    function createEthBounty(uint256 nftxPricePerEth, uint256 amountOfEth)
        public
        onlyOwner
    {
        createBounty(address(0), nftxPricePerEth, amountOfEth);
    }

    function createBounty(address token, uint256 nftxPrice, uint256 payoutCap)
        public
        onlyOwner
    {
        Bounty memory newBounty;
        newBounty.tokenContract = token;
        newBounty.nftxPrice = nftxPrice;
        newBounty.payoutCap = payoutCap;
        bounties.push(newBounty);
        uint256 bountyId = bounties.length.sub(1);
        emit NewBountyAdded(bountyId);
    }

    function closeBounty(uint256 bountyId) public onlyOwner {
        require(bountyId \u003c bounties.length, \"Invalid bountyId\");
        bounties[bountyId].payoutCap = bounties[bountyId].paidOut;
        emit BountyClosed(bountyId);
    }

    function fillBounty(uint256 bountyId, uint256 amountBeingSent)
        public
        payable
        nonReentrant
    {
        _fillBountyCustom(
            bountyId,
            amountBeingSent,
            vestedUntil - 2,
            vestedUntil - 1,
            vestedUntil
        );
    }

    /* function fillBountyCustom(
        uint256 bountyId,
        uint256 donationSize,
        uint64 _start,
        uint64 cliff,
        uint64 vested
    ) public payable nonReentrant {
        _fillBountyCustom(bountyId, donationSize, _start, cliff, vested);
    } */

    function _fillBountyCustom(
        uint256 bountyId,
        uint256 donationSize,
        uint64 _start,
        uint64 cliff,
        uint64 vested
    ) internal {
        require(cliff \u003e= vestedUntil - 1 \u0026\u0026 vested \u003e= vestedUntil, \"Not valid\");
        require(bountyId \u003c bounties.length, \"Invalid bountyId\");
        Bounty storage bounty = bounties[bountyId];
        uint256 rewardCap = getMaxPayout();
        require(rewardCap \u003e 0, \"Must wait for cap to be lifted\");
        uint256 remainingNftx = bounty.payoutCap.sub(bounty.paidOut);
        require(remainingNftx \u003e 0, \"Bounty is already finished\");
        uint256 requestedNftx = donationSize.mul(bounty.nftxPrice).div(BASE);
        uint256 willGive = remainingNftx \u003c requestedNftx
            ? remainingNftx
            : rewardCap \u003c requestedNftx
            ? rewardCap
            : requestedNftx;
        uint256 willTake = donationSize.mul(willGive).div(requestedNftx);
        if (getIsEth(bountyId)) {
            require(msg.value \u003e= willTake, \"Value sent is insufficient\");
            if (msg.value \u003e willTake) {
                address payable _sender = msg.sender;
                _sender.transfer(msg.value.sub(willTake));
            }
            daoMultiSig.transfer(willTake);
        } else {
            IERC20 fundToken = IERC20(bounty.tokenContract);
            fundToken.safeTransferFrom(msg.sender, daoMultiSig, willTake);
        }
        if (now \u003e vested) {
            nftxToken.safeTransfer(msg.sender, willGive);
        } else {
            nftxToken.safeTransfer(tokenManagerAddr, willGive);
            callAssignVested(
                msg.sender,
                willGive,
                _start,
                cliff,
                vested,
                false
            );
        }
        bounty.paidOut = bounty.paidOut.add(willGive);
        emit BountyFilled(
            bountyId,
            willGive,
            willTake,
            msg.sender,
            _start,
            cliff,
            vested
        );
    }

    function withdrawEth(uint256 amount) public onlyOwner {
        address payable sender = msg.sender;
        sender.transfer(amount);
        emit EthWithdrawn(amount);
    }

    function withdrawErc20(address tokenContract, uint256 amount)
        public
        onlyOwner
    {
        IERC20 token = IERC20(tokenContract);
        token.safeTransfer(msg.sender, amount);
        emit Erc20Withdrawn(tokenContract, amount);
    }

    function withdrawErc721(address nftContract, uint256 tokenId)
        public
        onlyOwner
    {
        IERC721 nft = IERC721(nftContract);
        nft.safeTransferFrom(address(this), msg.sender, tokenId);
        emit Erc721Withdrawn(nftContract, tokenId);
    }
}
"},"XController.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity 0.6.8;

import \"./ControllerBase.sol\";
import \"./INFTX.sol\";
import \"./IXStore.sol\";
import \"./Initializable.sol\";

contract XController is ControllerBase {
    INFTX private nftx;
    IXStore store;

    /* uint256 numFuncCalls;

    mapping(uint256 =\u003e uint256) public time;
    mapping(uint256 =\u003e uint256) public funcIndex;
    mapping(uint256 =\u003e address payable) public addressParam;
    mapping(uint256 =\u003e uint256[]) public uintArrayParam; */
    mapping(uint256 =\u003e uint256) public uintParam;
    mapping(uint256 =\u003e string) public stringParam;
    mapping(uint256 =\u003e bool) public boolParam;

    mapping(uint256 =\u003e uint256) public pendingEligAdditions;

    function initXController(address nftxAddress) public initializer {
        initOwnable();
        nftx = INFTX(nftxAddress);
    }

    function onlyOwnerOrLeadDev(uint256 funcIndex) public view virtual {
        if (funcIndex \u003e 3) {
            require(
                _msgSender() == leadDev || _msgSender() == owner(),
                \"Not owner or leadDev\"
            );
        } else {
            require(_msgSender() == owner(), \"Not owner\");
        }
    }

    function stageFuncCall(
        uint256 _funcIndex,
        address payable _addressParam,
        uint256 _uintParam,
        string memory _stringParam,
        uint256[] memory _uintArrayParam,
        bool _boolParam
    ) public virtual {
        onlyOwnerOrLeadDev(_funcIndex);
        uint256 fcId = numFuncCalls;
        numFuncCalls = numFuncCalls.add(1);
        time[fcId] = 1;
        funcIndex[fcId] = _funcIndex;
        addressParam[fcId] = _addressParam;
        uintParam[fcId] = _uintParam;
        stringParam[fcId] = _stringParam;
        uintArrayParam[fcId] = _uintArrayParam;
        boolParam[fcId] = _boolParam;
        if (
            funcIndex[fcId] == 4 \u0026\u0026
            store.negateEligibility(uintParam[fcId]) != !boolParam[fcId]
        ) {
            pendingEligAdditions[uintParam[fcId]] = pendingEligAdditions[uintParam[fcId]]
                .add(uintArrayParam[fcId].length);
        }
    }

    function cancelFuncCall(uint256 fcId) public override virtual {
        onlyOwnerOrLeadDev(funcIndex[fcId]);
        require(funcIndex[fcId] != 0, \"Already cancelled\");
        funcIndex[fcId] = 0;
        if (
            funcIndex[fcId] == 3 \u0026\u0026
            store.negateEligibility(uintParam[fcId]) != !boolParam[fcId]
        ) {
            pendingEligAdditions[uintParam[fcId]] = pendingEligAdditions[uintParam[fcId]]
                .sub(uintArrayParam[fcId].length);
        }
    }

    function executeFuncCall(uint256 fcId) public override virtual {
        super.executeFuncCall(fcId);
        if (funcIndex[fcId] == 3) {
            onlyIfPastDelay(2, time[fcId]);
            nftx.transferOwnership(addressParam[fcId]);
        } else if (funcIndex[fcId] == 4) {
            uint256 percentInc = pendingEligAdditions[uintParam[fcId]]
                .mul(100)
                .div(nftx.vaultSize(uintParam[fcId]));
            if (percentInc \u003e 10) {
                onlyIfPastDelay(2, time[fcId]);
            } else if (percentInc \u003e 1) {
                onlyIfPastDelay(1, time[fcId]);
            } else {
                onlyIfPastDelay(0, time[fcId]);
            }
            nftx.setIsEligible(
                uintParam[fcId],
                uintArrayParam[fcId],
                boolParam[fcId]
            );
            pendingEligAdditions[uintParam[fcId]] = pendingEligAdditions[uintParam[fcId]]
                .sub(uintArrayParam[fcId].length);
        } else if (funcIndex[fcId] == 5) {
            onlyIfPastDelay(0, time[fcId]); // vault must be empty
            nftx.setNegateEligibility(funcIndex[fcId], boolParam[fcId]);
        } else if (funcIndex[fcId] == 6) {
            onlyIfPastDelay(0, time[fcId]);
            nftx.setShouldReserve(
                uintParam[fcId],
                uintArrayParam[fcId],
                boolParam[fcId]
            );
        } else if (funcIndex[fcId] == 7) {
            onlyIfPastDelay(0, time[fcId]);
            nftx.setIsReserved(
                uintParam[fcId],
                uintArrayParam[fcId],
                boolParam[fcId]
            );
        } else if (funcIndex[fcId] == 8) {
            onlyIfPastDelay(1, time[fcId]);
            nftx.changeTokenName(uintParam[fcId], stringParam[fcId]);
        } else if (funcIndex[fcId] == 9) {
            onlyIfPastDelay(1, time[fcId]);
            nftx.changeTokenSymbol(uintParam[fcId], stringParam[fcId]);
        } else if (funcIndex[fcId] == 10) {
            onlyIfPastDelay(0, time[fcId]);
            nftx.closeVault(uintParam[fcId]);
        } else if (funcIndex[fcId] == 11) {
            onlyIfPastDelay(0, time[fcId]);
            nftx.setMintFees(
                uintArrayParam[fcId][0],
                uintArrayParam[fcId][1],
                uintArrayParam[fcId][2]
            );
        } else if (funcIndex[fcId] == 12) {
            (uint256 ethBase, uint256 ethStep) = store.burnFees(
                uintArrayParam[fcId][0]
            );
            uint256 ethBasePercentInc = uintArrayParam[fcId][1].mul(100).div(
                ethBase
            );
            uint256 ethStepPercentInc = uintArrayParam[fcId][2].mul(100).div(
                ethStep
            );
            if (ethBasePercentInc.add(ethStepPercentInc) \u003e 15) {
                onlyIfPastDelay(2, time[fcId]);
            } else if (ethBasePercentInc.add(ethStepPercentInc) \u003e 5) {
                onlyIfPastDelay(1, time[fcId]);
            } else {
                onlyIfPastDelay(0, time[fcId]);
            }
            nftx.setBurnFees(
                uintArrayParam[fcId][0],
                uintArrayParam[fcId][1],
                uintArrayParam[fcId][2]
            );
        } else if (funcIndex[fcId] == 13) {
            onlyIfPastDelay(0, time[fcId]);
            nftx.setDualFees(
                uintArrayParam[fcId][0],
                uintArrayParam[fcId][1],
                uintArrayParam[fcId][2]
            );
        } else if (funcIndex[fcId] == 14) {
            (uint256 ethMax, uint256 length) = store.supplierBounty(
                uintArrayParam[fcId][0]
            );
            uint256 ethMaxPercentInc = uintArrayParam[fcId][1].mul(100).div(
                ethMax
            );
            uint256 lengthPercentInc = uintArrayParam[fcId][2].mul(100).div(
                length
            );
            if (ethMaxPercentInc.add(lengthPercentInc) \u003e 20) {
                onlyIfPastDelay(2, time[fcId]);
            } else if (ethMaxPercentInc.add(lengthPercentInc) \u003e 5) {
                onlyIfPastDelay(1, time[fcId]);
            } else {
                onlyIfPastDelay(0, time[fcId]);
            }
            nftx.setSupplierBounty(
                uintArrayParam[fcId][0],
                uintArrayParam[fcId][1],
                uintArrayParam[fcId][2]
            );
        }
    }

}
"},"XSale.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity 0.6.8;

import \"./SafeMath.sol\";
import \"./Pausable.sol\";
import \"./INFTX.sol\";
import \"./IXStore.sol\";
import \"./IERC721.sol\";
import \"./ITokenManager.sol\";
import \"./Context.sol\";
import \"./ReentrancyGuard.sol\";

contract XSale is Pausable, ReentrancyGuard {
    using SafeMath for uint256;
    using EnumerableSet for EnumerableSet.UintSet;

    INFTX public nftx;
    IXStore public xStore;
    IERC20 public nftxToken;
    ITokenManager public tokenManager;

    uint64 public constant vestedUntil = 1610697600000; // Fri Jan 15 2021 00:00:00 GMT-0800

    // Bounty[] public ethBounties;
    mapping(uint256 =\u003e Bounty[]) public xBounties;

    struct Bounty {
        uint256 reward;
        uint256 request;
    }

    constructor(address _nftx, address _nftxToken, address _tokenManager)
        public
    {
        initOwnable();
        nftx = INFTX(_nftx);
        xStore = IXStore(nftx.store());
        nftxToken = IERC20(_nftxToken);
        tokenManager = ITokenManager(_tokenManager);
    }

    function addXBounty(uint256 vaultId, uint256 reward, uint256 request)
        public
        onlyOwner
    {
        Bounty memory newXBounty;
        newXBounty.reward = reward;
        newXBounty.request = request;
        xBounties[vaultId].push(newXBounty);
    }

    function setXBounty(
        uint256 vaultId,
        uint256 xBountyIndex,
        uint256 newReward,
        uint256 newRequest
    ) public onlyOwner {
        Bounty storage xBounty = xBounties[vaultId][xBountyIndex];
        xBounty.reward = newReward;
        xBounty.request = newRequest;
    }

    function withdrawNFTX(address to, uint256 amount) public onlyOwner {
        nftxToken.transfer(to, amount);
    }

    function withdrawXToken(uint256 vaultId, address to, uint256 amount)
        public
        onlyOwner
    {
        xStore.xToken(vaultId).transfer(to, amount);
    }

    function withdrawETH(address payable to, uint256 amount) public onlyOwner {
        to.transfer(amount);
    }

    function fillXBounty(uint256 vaultId, uint256 xBountyIndex, uint256 amount)
        public
        nonReentrant
    {
        Bounty storage xBounty = xBounties[vaultId][xBountyIndex];
        require(amount \u003c= xBounty.request, \"Amount \u003e bounty\");
        require(
            amount \u003c= nftxToken.balanceOf(address(nftx)),
            \"Amount \u003e balance\"
        );
        xStore.xToken(vaultId).transferFrom(
            _msgSender(),
            address(nftx),
            amount
        );
        uint256 reward = xBounty.reward.mul(amount).div(xBounty.request);
        xBounty.request = xBounty.request.sub(amount);
        xBounty.reward = xBounty.reward.sub(reward);
        nftxToken.transfer(address(tokenManager), reward);
        tokenManager.assignVested(
            _msgSender(),
            reward,
            vestedUntil,
            vestedUntil,
            vestedUntil,
            false
        );
    }
}
"},"XStore.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity 0.6.8;

import \"./EnumerableSet.sol\";
import \"./Ownable.sol\";
import \"./SafeMath.sol\";
import \"./IXToken.sol\";
import \"./IERC721.sol\";
import \"./SafeERC20.sol\";

contract XStore is Ownable {
    using SafeMath for uint256;
    using SafeERC20 for IERC20;
    using EnumerableSet for EnumerableSet.UintSet;

    struct FeeParams {
        uint256 ethBase;
        uint256 ethStep;
    }

    struct BountyParams {
        uint256 ethMax;
        uint256 length;
    }

    struct Vault {
        address xTokenAddress;
        address nftAddress;
        address manager;
        IXToken xToken;
        IERC721 nft;
        EnumerableSet.UintSet holdings;
        EnumerableSet.UintSet reserves;
        mapping(uint256 =\u003e address) requester;
        mapping(uint256 =\u003e bool) isEligible;
        mapping(uint256 =\u003e bool) shouldReserve;
        bool allowMintRequests;
        bool flipEligOnRedeem;
        bool negateEligibility;
        bool isFinalized;
        bool isClosed;
        FeeParams mintFees;
        FeeParams burnFees;
        FeeParams dualFees;
        BountyParams supplierBounty;
        uint256 ethBalance;
        uint256 tokenBalance;
        bool isD2Vault;
        address d2AssetAddress;
        IERC20 d2Asset;
        uint256 d2Holdings;
    }

    event XTokenAddressSet(uint256 indexed vaultId, address token);
    event NftAddressSet(uint256 indexed vaultId, address asset);
    event ManagerSet(uint256 indexed vaultId, address manager);
    event XTokenSet(uint256 indexed vaultId);
    event NftSet(uint256 indexed vaultId);
    event HoldingsAdded(uint256 indexed vaultId, uint256 id);
    event HoldingsRemoved(uint256 indexed vaultId, uint256 id);
    event ReservesAdded(uint256 indexed vaultId, uint256 id);
    event ReservesRemoved(uint256 indexed vaultId, uint256 id);
    event RequesterSet(uint256 indexed vaultId, uint256 id, address requester);
    event IsEligibleSet(uint256 indexed vaultId, uint256 id, bool _bool);
    event ShouldReserveSet(uint256 indexed vaultId, uint256 id, bool _bool);
    event AllowMintRequestsSet(uint256 indexed vaultId, bool isAllowed);
    event FlipEligOnRedeemSet(uint256 indexed vaultId, bool _bool);
    event NegateEligibilitySet(uint256 indexed vaultId, bool _bool);
    event IsFinalizedSet(uint256 indexed vaultId, bool _isFinalized);
    event IsClosedSet(uint256 indexed vaultId, bool _isClosed);
    event MintFeesSet(
        uint256 indexed vaultId,
        uint256 ethBase,
        uint256 ethStep
    );
    event BurnFeesSet(
        uint256 indexed vaultId,
        uint256 ethBase,
        uint256 ethStep
    );
    event DualFeesSet(
        uint256 indexed vaultId,
        uint256 ethBase,
        uint256 ethStep
    );
    event SupplierBountySet(
        uint256 indexed vaultId,
        uint256 ethMax,
        uint256 length
    );
    event EthBalanceSet(uint256 indexed vaultId, uint256 _ethBalance);
    event TokenBalanceSet(uint256 indexed vaultId, uint256 _tokenBalance);
    event IsD2VaultSet(uint256 indexed vaultId, bool _isD2Vault);
    event D2AssetAddressSet(uint256 indexed vaultId, address _d2Asset);
    event D2AssetSet(uint256 indexed vaultId);
    event D2HoldingsSet(uint256 indexed vaultId, uint256 _d2Holdings);
    event NewVaultAdded(uint256 indexed vaultId);
    event IsExtensionSet(address addr, bool _isExtension);
    event RandNonceSet(uint256 _randNonce);

    Vault[] internal vaults;

    mapping(address =\u003e bool) public isExtension;
    uint256 public randNonce;

    constructor() public {
        initOwnable();
    }

    function _getVault(uint256 vaultId) internal view returns (Vault storage) {
        require(vaultId \u003c vaults.length, \"Invalid vaultId\");
        return vaults[vaultId];
    }

    function vaultsLength() public view returns (uint256) {
        return vaults.length;
    }

    function xTokenAddress(uint256 vaultId) public view returns (address) {
        Vault storage vault = _getVault(vaultId);
        return vault.xTokenAddress;
    }

    function nftAddress(uint256 vaultId) public view returns (address) {
        Vault storage vault = _getVault(vaultId);
        return vault.nftAddress;
    }

    function manager(uint256 vaultId) public view returns (address) {
        Vault storage vault = _getVault(vaultId);
        return vault.manager;
    }

    function xToken(uint256 vaultId) public view returns (IXToken) {
        Vault storage vault = _getVault(vaultId);
        return vault.xToken;
    }

    function nft(uint256 vaultId) public view returns (IERC721) {
        Vault storage vault = _getVault(vaultId);
        return vault.nft;
    }

    function holdingsLength(uint256 vaultId) public view returns (uint256) {
        Vault storage vault = _getVault(vaultId);
        return vault.holdings.length();
    }

    function holdingsContains(uint256 vaultId, uint256 elem)
        public
        view
        returns (bool)
    {
        Vault storage vault = _getVault(vaultId);
        return vault.holdings.contains(elem);
    }

    function holdingsAt(uint256 vaultId, uint256 index)
        public
        view
        returns (uint256)
    {
        Vault storage vault = _getVault(vaultId);
        return vault.holdings.at(index);
    }

    function reservesLength(uint256 vaultId) public view returns (uint256) {
        Vault storage vault = _getVault(vaultId);
        return vault.holdings.length();
    }

    function reservesContains(uint256 vaultId, uint256 elem)
        public
        view
        returns (bool)
    {
        Vault storage vault = _getVault(vaultId);
        return vault.holdings.contains(elem);
    }

    function reservesAt(uint256 vaultId, uint256 index)
        public
        view
        returns (uint256)
    {
        Vault storage vault = _getVault(vaultId);
        return vault.holdings.at(index);
    }

    function requester(uint256 vaultId, uint256 id)
        public
        view
        returns (address)
    {
        Vault storage vault = _getVault(vaultId);
        return vault.requester[id];
    }

    function isEligible(uint256 vaultId, uint256 id)
        public
        view
        returns (bool)
    {
        Vault storage vault = _getVault(vaultId);
        return vault.isEligible[id];
    }

    function shouldReserve(uint256 vaultId, uint256 id)
        public
        view
        returns (bool)
    {
        Vault storage vault = _getVault(vaultId);
        return vault.shouldReserve[id];
    }

    function allowMintRequests(uint256 vaultId) public view returns (bool) {
        Vault storage vault = _getVault(vaultId);
        return vault.allowMintRequests;
    }

    function flipEligOnRedeem(uint256 vaultId) public view returns (bool) {
        Vault storage vault = _getVault(vaultId);
        return vault.flipEligOnRedeem;
    }

    function negateEligibility(uint256 vaultId) public view returns (bool) {
        Vault storage vault = _getVault(vaultId);
        return vault.negateEligibility;
    }

    function isFinalized(uint256 vaultId) public view returns (bool) {
        Vault storage vault = _getVault(vaultId);
        return vault.isFinalized;
    }

    function isClosed(uint256 vaultId) public view returns (bool) {
        Vault storage vault = _getVault(vaultId);
        return vault.isClosed;
    }

    function mintFees(uint256 vaultId) public view returns (uint256, uint256) {
        Vault storage vault = _getVault(vaultId);
        return (vault.mintFees.ethBase, vault.mintFees.ethStep);
    }

    function burnFees(uint256 vaultId) public view returns (uint256, uint256) {
        Vault storage vault = _getVault(vaultId);
        return (vault.burnFees.ethBase, vault.burnFees.ethStep);
    }

    function dualFees(uint256 vaultId) public view returns (uint256, uint256) {
        Vault storage vault = _getVault(vaultId);
        return (vault.dualFees.ethBase, vault.dualFees.ethStep);
    }

    function supplierBounty(uint256 vaultId)
        public
        view
        returns (uint256, uint256)
    {
        Vault storage vault = _getVault(vaultId);
        return (vault.supplierBounty.ethMax, vault.supplierBounty.length);
    }

    function ethBalance(uint256 vaultId) public view returns (uint256) {
        Vault storage vault = _getVault(vaultId);
        return vault.ethBalance;
    }

    function tokenBalance(uint256 vaultId) public view returns (uint256) {
        Vault storage vault = _getVault(vaultId);
        return vault.tokenBalance;
    }

    function isD2Vault(uint256 vaultId) public view returns (bool) {
        Vault storage vault = _getVault(vaultId);
        return vault.isD2Vault;
    }

    function d2AssetAddress(uint256 vaultId) public view returns (address) {
        Vault storage vault = _getVault(vaultId);
        return vault.d2AssetAddress;
    }

    function d2Asset(uint256 vaultId) public view returns (IERC20) {
        Vault storage vault = _getVault(vaultId);
        return vault.d2Asset;
    }

    function d2Holdings(uint256 vaultId) public view returns (uint256) {
        Vault storage vault = _getVault(vaultId);
        return vault.d2Holdings;
    }

    function setXTokenAddress(uint256 vaultId, address _xTokenAddress)
        public
        onlyOwner
    {
        Vault storage vault = _getVault(vaultId);
        vault.xTokenAddress = _xTokenAddress;
        emit XTokenAddressSet(vaultId, _xTokenAddress);
    }

    function setNftAddress(uint256 vaultId, address _nft) public onlyOwner {
        Vault storage vault = _getVault(vaultId);
        vault.nftAddress = _nft;
        emit NftAddressSet(vaultId, _nft);
    }

    function setManager(uint256 vaultId, address _manager) public onlyOwner {
        Vault storage vault = _getVault(vaultId);
        vault.manager = _manager;
        emit ManagerSet(vaultId, _manager);
    }

    function setXToken(uint256 vaultId) public onlyOwner {
        Vault storage vault = _getVault(vaultId);
        vault.xToken = IXToken(vault.xTokenAddress);
        emit XTokenSet(vaultId);
    }

    function setNft(uint256 vaultId) public onlyOwner {
        Vault storage vault = _getVault(vaultId);
        vault.nft = IERC721(vault.nftAddress);
        emit NftSet(vaultId);
    }

    function holdingsAdd(uint256 vaultId, uint256 elem) public onlyOwner {
        Vault storage vault = _getVault(vaultId);
        vault.holdings.add(elem);
        emit HoldingsAdded(vaultId, elem);
    }

    function holdingsRemove(uint256 vaultId, uint256 elem) public onlyOwner {
        Vault storage vault = _getVault(vaultId);
        vault.holdings.remove(elem);
        emit HoldingsRemoved(vaultId, elem);
    }

    function reservesAdd(uint256 vaultId, uint256 elem) public onlyOwner {
        Vault storage vault = _getVault(vaultId);
        vault.reserves.add(elem);
        emit ReservesAdded(vaultId, elem);
    }

    function reservesRemove(uint256 vaultId, uint256 elem) public onlyOwner {
        Vault storage vault = _getVault(vaultId);
        vault.reserves.remove(elem);
        emit ReservesRemoved(vaultId, elem);
    }

    function setRequester(uint256 vaultId, uint256 id, address _requester)
        public
        onlyOwner
    {
        Vault storage vault = _getVault(vaultId);
        vault.requester[id] = _requester;
        emit RequesterSet(vaultId, id, _requester);
    }

    function setIsEligible(uint256 vaultId, uint256 id, bool _bool)
        public
        onlyOwner
    {
        Vault storage vault = _getVault(vaultId);
        vault.isEligible[id] = _bool;
        emit IsEligibleSet(vaultId, id, _bool);
    }

    function setShouldReserve(uint256 vaultId, uint256 id, bool _shouldReserve)
        public
        onlyOwner
    {
        Vault storage vault = _getVault(vaultId);
        vault.shouldReserve[id] = _shouldReserve;
        emit ShouldReserveSet(vaultId, id, _shouldReserve);
    }

    function setAllowMintRequests(uint256 vaultId, bool isAllowed)
        public
        onlyOwner
    {
        Vault storage vault = _getVault(vaultId);
        vault.allowMintRequests = isAllowed;
        emit AllowMintRequestsSet(vaultId, isAllowed);
    }

    function setFlipEligOnRedeem(uint256 vaultId, bool flipElig)
        public
        onlyOwner
    {
        Vault storage vault = _getVault(vaultId);
        vault.flipEligOnRedeem = flipElig;
        emit FlipEligOnRedeemSet(vaultId, flipElig);
    }

    function setNegateEligibility(uint256 vaultId, bool negateElig)
        public
        onlyOwner
    {
        Vault storage vault = _getVault(vaultId);
        vault.negateEligibility = negateElig;
        emit NegateEligibilitySet(vaultId, negateElig);
    }

    function setIsFinalized(uint256 vaultId, bool _isFinalized)
        public
        onlyOwner
    {
        Vault storage vault = _getVault(vaultId);
        vault.isFinalized = _isFinalized;
        emit IsFinalizedSet(vaultId, _isFinalized);
    }

    function setIsClosed(uint256 vaultId, bool _isClosed) public onlyOwner {
        Vault storage vault = _getVault(vaultId);
        vault.isClosed = _isClosed;
        emit IsClosedSet(vaultId, _isClosed);
    }

    function setMintFees(uint256 vaultId, uint256 ethBase, uint256 ethStep)
        public
        onlyOwner
    {
        Vault storage vault = _getVault(vaultId);
        vault.mintFees = FeeParams(ethBase, ethStep);
        emit MintFeesSet(vaultId, ethBase, ethStep);
    }

    function setBurnFees(uint256 vaultId, uint256 ethBase, uint256 ethStep)
        public
        onlyOwner
    {
        Vault storage vault = _getVault(vaultId);
        vault.burnFees = FeeParams(ethBase, ethStep);
        emit BurnFeesSet(vaultId, ethBase, ethStep);
    }

    function setDualFees(uint256 vaultId, uint256 ethBase, uint256 ethStep)
        public
        onlyOwner
    {
        Vault storage vault = _getVault(vaultId);
        vault.dualFees = FeeParams(ethBase, ethStep);
        emit DualFeesSet(vaultId, ethBase, ethStep);
    }

    function setSupplierBounty(uint256 vaultId, uint256 ethMax, uint256 length)
        public
        onlyOwner
    {
        Vault storage vault = _getVault(vaultId);
        vault.supplierBounty = BountyParams(ethMax, length);
        emit SupplierBountySet(vaultId, ethMax, length);
    }

    function setEthBalance(uint256 vaultId, uint256 _ethBalance)
        public
        onlyOwner
    {
        Vault storage vault = _getVault(vaultId);
        vault.ethBalance = _ethBalance;
        emit EthBalanceSet(vaultId, _ethBalance);
    }

    function setTokenBalance(uint256 vaultId, uint256 _tokenBalance)
        public
        onlyOwner
    {
        Vault storage vault = _getVault(vaultId);
        vault.tokenBalance = _tokenBalance;
        emit TokenBalanceSet(vaultId, _tokenBalance);
    }

    function setIsD2Vault(uint256 vaultId, bool _isD2Vault) public onlyOwner {
        Vault storage vault = _getVault(vaultId);
        vault.isD2Vault = _isD2Vault;
        emit IsD2VaultSet(vaultId, _isD2Vault);
    }

    function setD2AssetAddress(uint256 vaultId, address _d2Asset)
        public
        onlyOwner
    {
        Vault storage vault = _getVault(vaultId);
        vault.d2AssetAddress = _d2Asset;
        emit D2AssetAddressSet(vaultId, _d2Asset);
    }

    function setD2Asset(uint256 vaultId) public onlyOwner {
        Vault storage vault = _getVault(vaultId);
        vault.d2Asset = IERC20(vault.d2AssetAddress);
        emit D2AssetSet(vaultId);
    }

    function setD2Holdings(uint256 vaultId, uint256 _d2Holdings)
        public
        onlyOwner
    {
        Vault storage vault = _getVault(vaultId);
        vault.d2Holdings = _d2Holdings;
        emit D2HoldingsSet(vaultId, _d2Holdings);
    }

    ////////////////////////////////////////////////////////////

    function addNewVault() public onlyOwner returns (uint256) {
        Vault memory newVault;
        vaults.push(newVault);
        uint256 vaultId = vaults.length.sub(1);
        emit NewVaultAdded(vaultId);
        return vaultId;
    }

    function setIsExtension(address addr, bool _isExtension) public onlyOwner {
        isExtension[addr] = _isExtension;
        emit IsExtensionSet(addr, _isExtension);
    }

    function setRandNonce(uint256 _randNonce) public onlyOwner {
        randNonce = _randNonce;
        emit RandNonceSet(_randNonce);
    }
}
"},"XStoreMultiCall.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity 0.6.8;

import \"./IXStore.sol\";

contract XStoreMultiCall {
    IXStore public xStore;

    constructor() public {
        xStore = IXStore(0xBe54738723cea167a76ad5421b50cAa49692E7B7);
    }

    function getVaultDataA(uint256 vaultId)
        public
        view
        returns (
            address xTokenAddress,
            address nftAddress,
            address manager,
            bool isClosed,
            bool isD2Vault,
            address d2AssetAddress
        )
    {
        address _xTokenAddress = xStore.xTokenAddress(vaultId);
        address _nftAddress = xStore.nftAddress(vaultId);
        address _manager = xStore.manager(vaultId);
        bool _isClosed = xStore.isClosed(vaultId);
        bool _isD2Vault = xStore.isD2Vault(vaultId);
        address _d2AssetAddress = xStore.d2AssetAddress(vaultId);

        return (
            _xTokenAddress,
            _nftAddress,
            _manager,
            _isClosed,
            _isD2Vault,
            _d2AssetAddress
        );
    }

    function getVaultDataB(uint256 vaultId)
        public
        view
        returns (
            bool allowMintRequests,
            bool flipEligOnRedeem,
            bool negateEligibility,
            bool isFinalized
        )
    {
        bool _allowMintRequests = xStore.allowMintRequests(vaultId);
        bool _flipEligOnRedeem = xStore.flipEligOnRedeem(vaultId);
        bool _negateEligibility = xStore.negateEligibility(vaultId);
        bool _isFinalized = xStore.isFinalized(vaultId);

        return (
            _allowMintRequests,
            _flipEligOnRedeem,
            _negateEligibility,
            _isFinalized
        );
    }
}
"},"XToken.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity 0.6.8;

import \"./Ownable.sol\";
import \"./Context.sol\";
import \"./ERC20.sol\";
import \"./ERC20Burnable.sol\";

contract XToken is Context, Ownable, ERC20Burnable {
    constructor(string memory name, string memory symbol, address _owner)
        public
        ERC20(name, symbol)
    {
        initOwnable();
        transferOwnership(_owner);
        _mint(msg.sender, 0);
    }

    function mint(address to, uint256 amount) public onlyOwner {
        _mint(to, amount);
    }

    function changeName(string memory name) public onlyOwner {
        _changeName(name);
    }

    function changeSymbol(string memory symbol) public onlyOwner {
        _changeSymbol(symbol);
    }
}
"},"XTokenClonable.sol":{"content":"// SPDX-License-Identifier: MIT
pragma solidity 0.6.8;

// import \"@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol\";
import \"./OwnableUpgradeable.sol\";
// import \"@openzeppelin/contracts-upgradeable/token/ERC20/ERC20BurnableUpgradeable.sol\";
import \"./ERC20BurnableUpgradeable.sol\";

contract XTokenClonable is OwnableUpgradeable, ERC20BurnableUpgradeable {
    function initialize(string memory name, string memory symbol)
        public
        initializer
    {
        __Ownable_init();
        __ERC20_init(name, symbol);
        __ERC20Burnable_init_unchained();
    }

    function mint(address to, uint256 amount) public onlyOwner {
        _mint(to, amount);
    }

    function changeName(string memory name) public onlyOwner {}

    function changeSymbol(string memory symbol) public onlyOwner {}
}
"},"XTokenFactory.sol":{"content":"// SPDX-License-Identifier: MIT
pragma solidity 0.6.8;

// import \"@openzeppelin/contracts/access/Ownable.sol\";
import \"./OwnableRegular.sol\";
// import {ClonesUpgradeable} from \"@openzeppelin/contracts-upgradeable/proxy/ClonesUpgradeable.sol\";
import {ClonesUpgradeable} from \"./ClonesUpgradeable.sol\";
import \"./XTokenClonable.sol\";

contract XTokenFactory is Ownable {
    address public template;

    event NewXToken(address _xTokenAddress);

    constructor(address _template) public {
        template = _template;
    }

    function createXToken(string calldata name, string calldata symbol)
        external
        returns (address)
    {
        XTokenClonable x = XTokenClonable(ClonesUpgradeable.clone(template));
        x.initialize(name, symbol);
        x.transferOwnership(owner());
        address xAddress = address(x);
        emit NewXToken(xAddress);
        return xAddress;
    }
}

