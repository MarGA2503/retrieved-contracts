// SPDX-License-Identifier: MIT

pragma solidity ^0.7.0;

/**
 * @dev Collection of functions related to the address type
 */
library Address {
    /**
     * @dev Returns true if `account` is a contract.
     *
     * [IMPORTANT]
     * ====
     * It is unsafe to assume that an address for which this function returns
     * false is an externally-owned account (EOA) and not a contract.
     *
     * Among others, `isContract` will return false for the following
     * types of addresses:
     *
     *  - an externally-owned account
     *  - a contract in construction
     *  - an address where a contract will be created
     *  - an address where a contract lived, but was destroyed
     * ====
     */
    function isContract(address account) internal view returns (bool) {
        // According to EIP-1052, 0x0 is the value returned for not-yet created accounts
        // and 0xc5d2460186f7233c927e7db2dcc703c0e500b653ca82273b7bfad8045d85a470 is returned
        // for accounts without code, i.e. `keccak256(\u0027\u0027)`
        bytes32 codehash;
        bytes32 accountHash = 0xc5d2460186f7233c927e7db2dcc703c0e500b653ca82273b7bfad8045d85a470;
        // solhint-disable-next-line no-inline-assembly
        assembly { codehash := extcodehash(account) }
        return (codehash != accountHash \u0026\u0026 codehash != 0x0);
    }

    /**
     * @dev Replacement for Solidity\u0027s `transfer`: sends `amount` wei to
     * `recipient`, forwarding all available gas and reverting on errors.
     *
     * https://eips.ethereum.org/EIPS/eip-1884[EIP1884] increases the gas cost
     * of certain opcodes, possibly making contracts go over the 2300 gas limit
     * imposed by `transfer`, making them unable to receive funds via
     * `transfer`. {sendValue} removes this limitation.
     *
     * https://diligence.consensys.net/posts/2019/09/stop-using-soliditys-transfer-now/[Learn more].
     *
     * IMPORTANT: because control is transferred to `recipient`, care must be
     * taken to not create reentrancy vulnerabilities. Consider using
     * {ReentrancyGuard} or the
     * https://solidity.readthedocs.io/en/v0.5.11/security-considerations.html#use-the-checks-effects-interactions-pattern[checks-effects-interactions pattern].
     */
    function sendValue(address payable recipient, uint256 amount) internal {
        require(address(this).balance \u003e= amount, \"Address: insufficient balance\");

        // solhint-disable-next-line avoid-low-level-calls, avoid-call-value
        (bool success, ) = recipient.call{ value: amount }(\"\");
        require(success, \"Address: unable to send value, recipient may have reverted\");
    }

    /**
     * @dev Performs a Solidity function call using a low level `call`. A
     * plain`call` is an unsafe replacement for a function call: use this
     * function instead.
     *
     * If `target` reverts with a revert reason, it is bubbled up by this
     * function (like regular Solidity function calls).
     *
     * Returns the raw returned data. To convert to the expected return value,
     * use https://solidity.readthedocs.io/en/latest/units-and-global-variables.html?highlight=abi.decode#abi-encoding-and-decoding-functions[`abi.decode`].
     *
     * Requirements:
     *
     * - `target` must be a contract.
     * - calling `target` with `data` must not revert.
     *
     * _Available since v3.1._
     */
    function functionCall(address target, bytes memory data) internal returns (bytes memory) {
      return functionCall(target, data, \"Address: low-level call failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`], but with
     * `errorMessage` as a fallback revert reason when `target` reverts.
     *
     * _Available since v3.1._
     */
    function functionCall(address target, bytes memory data, string memory errorMessage) internal returns (bytes memory) {
        return _functionCallWithValue(target, data, 0, errorMessage);
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`],
     * but also transferring `value` wei to `target`.
     *
     * Requirements:
     *
     * - the calling contract must have an ETH balance of at least `value`.
     * - the called Solidity function must be `payable`.
     *
     * _Available since v3.1._
     */
    function functionCallWithValue(address target, bytes memory data, uint256 value) internal returns (bytes memory) {
        return functionCallWithValue(target, data, value, \"Address: low-level call with value failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCallWithValue-address-bytes-uint256-}[`functionCallWithValue`], but
     * with `errorMessage` as a fallback revert reason when `target` reverts.
     *
     * _Available since v3.1._
     */
    function functionCallWithValue(address target, bytes memory data, uint256 value, string memory errorMessage) internal returns (bytes memory) {
        require(address(this).balance \u003e= value, \"Address: insufficient balance for call\");
        return _functionCallWithValue(target, data, value, errorMessage);
    }

    function _functionCallWithValue(address target, bytes memory data, uint256 weiValue, string memory errorMessage) private returns (bytes memory) {
        require(isContract(target), \"Address: call to non-contract\");

        // solhint-disable-next-line avoid-low-level-calls
        (bool success, bytes memory returndata) = target.call{ value: weiValue }(data);
        if (success) {
            return returndata;
        } else {
            // Look for revert reason and bubble it up if present
            if (returndata.length \u003e 0) {
                // The easiest way to bubble the revert reason is using memory via assembly

                // solhint-disable-next-line no-inline-assembly
                assembly {
                    let returndata_size := mload(returndata)
                    revert(add(32, returndata), returndata_size)
                }
            } else {
                revert(errorMessage);
            }
        }
    }
}
"},"CappedTimedCrowdsale.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.7.0;

import \"./SafeMath.sol\";
import \"./TimedCrowdsale.sol\";

/**
 * @title CappedCrowdsale
 * @dev Crowdsale with a limit for total contributions based on time.
 */
abstract contract CappedTimedCrowdsale is TimedCrowdsale {
    using SafeMath for uint256;

    uint256 private _cap;

    /**
     * @dev Constructor, takes maximum amount of wei accepted in the crowdsale.
     * @param capReceived Max amount of wei to be contributed
     */
    constructor (uint256 capReceived) {
        require(capReceived \u003e 0, \"CappedCrowdsale: cap is 0\");
        _cap = capReceived;
    }

    /**
     * @return the cap of the crowdsale.
     */
    function cap() public view returns (uint256) {
        return _cap;
    }

    /*
    ** Updates cap
    */
    function changeCap(uint256 newCap) internal {
        _cap = newCap;
    }

    /**
     * @dev Checks whether the period in which the crowdsale is open has already elapsed or if the cap has been reached.
     */
    function hasClosed() public view override virtual returns (bool) {
        // solhint-disable-next-line not-rely-on-time
        return capReached() || super.hasClosed();
    }

    /**
     * @dev Checks whether the cap has been reached.
     * @return Whether the cap was reached
     */
    function capReached() public view returns (bool) {
        return weiRaised() \u003e= _cap;
    }

    /**
     * @dev Extend parent behavior requiring purchase to respect the funding cap.
     * @param beneficiary Token purchaser
     * @param weiAmount Amount of wei contributed
     */
    function _preValidatePurchase(address beneficiary, uint256 weiAmount) internal override virtual view {
        super._preValidatePurchase(beneficiary, weiAmount);
        require(weiRaised().add(weiAmount) \u003c= _cap, \"CappedCrowdsale: cap exceeded\");
    }
}"},"ConditionalEscrow.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.7.0;

import \"./Escrow.sol\";

/**
 * @title ConditionalEscrow
 * @dev Base abstract escrow to only allow withdrawal if a condition is met.
 * @dev Intended usage: See {Escrow}. Same usage guidelines apply here.
 */
abstract contract ConditionalEscrow is Escrow {
    /**
     * @dev Returns whether an address is allowed to withdraw their funds. To be
     * implemented by derived contracts.
     * @param payee The destination address of the funds.
     */
    function withdrawalAllowed(address payee) public view virtual returns (bool);

    function withdraw(address payable payee) public virtual override {
        require(withdrawalAllowed(payee), \"ConditionalEscrow: payee is not allowed to withdraw\");
        super.withdraw(payee);
    }
}
"},"Context.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.7.0;

/*
 * @dev Provides information about the current execution context, including the
 * sender of the transaction and its data. While these are generally available
 * via msg.sender and msg.data, they should not be accessed in such a direct
 * manner, since when dealing with GSN meta-transactions the account sending and
 * paying for execution may not be the actual sender (as far as an application
 * is concerned).
 *
 * This contract is only required for intermediate, library-like contracts.
 */
abstract contract Context {
    function _msgSender() internal view virtual returns (address payable) {
        return msg.sender;
    }

    function _msgData() internal view virtual returns (bytes memory) {
        this; // silence state mutability warning without generating bytecode - see https://github.com/ethereum/solidity/issues/2691
        return msg.data;
    }
}
"},"CrowdsaleMint.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.7.0;

import \"./Context.sol\";
import \"./IERC20.sol\";
import \"./SafeERC20.sol\";
import \"./SafeMath.sol\";
import \"./LBCToken.sol\";
import \"./ReentrancyGuard.sol\";

/**
  * Based on OpenZeppelin\u0027s Crowdsale legacy contract
 */
contract CrowdsaleMint is Context, ReentrancyGuard {
    using SafeMath for uint256;
    using SafeERC20 for IERC20;

    // The token being sold
    LBCToken private _token;

    // Address where funds are collected
    address payable private _wallet;

    // How many token units a buyer gets per wei.
    // The rate is the conversion between wei and the smallest and indivisible token unit.
    // So, if you are using a rate of 1 with a ERC20Detailed token with 3 decimals called TOK
    // 1 wei will give you 1 unit, or 0.001 TOK.
    uint256 private _rate;

    // Amount of wei raised
    uint256 private _weiRaised;

    /**
     * Event for token purchase logging
     * @param purchaser who paid for the tokens
     * @param beneficiary who got the tokens
     * @param value weis paid for purchase
     * @param amount amount of tokens purchased
     */
    event TokensPurchased(address indexed purchaser, address indexed beneficiary, uint256 value, uint256 amount);

    /**
     * @param rateReceived Number of token units a buyer gets per wei
     * @dev The rate is the conversion between wei and the smallest and indivisible
     * token unit. So, if you are using a rate of 1 with a ERC20Detailed token
     * with 3 decimals called TOK, 1 wei will give you 1 unit, or 0.001 TOK.
     * @param walletReceived Address where collected funds will be forwarded to
     * @param tokenReceived Address of the token being sold
     */
    constructor (uint256 rateReceived, address payable walletReceived, LBCToken tokenReceived) {
        require(rateReceived \u003e 0, \"Crowdsale: rate is 0\");
        require(walletReceived != address(0), \"Crowdsale: wallet is the zero address\");
        require(address(tokenReceived) != address(0), \"Crowdsale: token is the zero address\");

        _rate = rateReceived;
        _wallet = walletReceived;
        _token = tokenReceived;
    }

    /**
     * @dev fallback function ***DO NOT OVERRIDE***
     * Note that other contracts will transfer funds with a base gas stipend
     * of 2300, which is not enough to call buyTokens. Consider calling
     * buyTokens directly when purchasing tokens from a contract.
     */
    receive() external payable {
        buyTokens(_msgSender());
    }

    fallback() external payable {
        buyTokens(_msgSender());
    }

    /**
     * @return the token being sold.
     */
    function token() public view returns (LBCToken) {
        return _token;
    }

    function _changeToken(LBCToken newToken) internal {
        _token = newToken;
    }

    /**
     * @return the address where funds are collected.
     */
    function wallet() public view returns (address payable) {
        return _wallet;
    }

    /**
     * @return the number of token units a buyer gets per wei.
     */
    function rate() public view virtual returns (uint256) {
        return _rate;
    }

    /**
     * @return the amount of wei raised.
     */
    function weiRaised() public view returns (uint256) {
        return _weiRaised;
    }

    /**
     * @dev low level token purchase ***DO NOT OVERRIDE***
     * This function has a non-reentrancy guard, so it shouldn\u0027t be called by
     * another `nonReentrant` function.
     * @param beneficiary Recipient of the token purchase
     */
    function buyTokens(address beneficiary) public nonReentrant payable {
        uint256 weiAmount = msg.value;
        _preValidatePurchase(beneficiary, weiAmount);

        // calculate token amount to be created
        uint256 tokens = _getTokenAmount(weiAmount);

        // update state
        _weiRaised = _weiRaised.add(weiAmount);


        _processPurchase(beneficiary, tokens);
        emit TokensPurchased(_msgSender(), beneficiary, weiAmount, tokens);

        _updatePurchasingState(beneficiary, weiAmount);

        _forwardFunds();
        _postValidatePurchase(beneficiary, weiAmount);
    }

    /**
     * @dev Validation of an incoming purchase. Use require statements to revert state when conditions are not met.
     * Use `super` in contracts that inherit from Crowdsale to extend their validations.
     * Example from CappedCrowdsale.sol\u0027s _preValidatePurchase method:
     *     super._preValidatePurchase(beneficiary, weiAmount);
     *     require(weiRaised().add(weiAmount) \u003c= cap);
     * @param beneficiary Address performing the token purchase
     * @param weiAmount Value in wei involved in the purchase
     */
    function _preValidatePurchase(address beneficiary, uint256 weiAmount) internal virtual view {
        require(beneficiary != address(0), \"Crowdsale: beneficiary is the zero address\");
        require(weiAmount != 0, \"Crowdsale: weiAmount is 0\");
        this; // silence state mutability warning without generating bytecode - see https://github.com/ethereum/solidity/issues/2691
    }

    /**
     * @dev Validation of an executed purchase. Observe state and use revert statements to undo rollback when valid
     * conditions are not met.
     * @param beneficiary Address performing the token purchase
     * @param weiAmount Value in wei involved in the purchase
     */
    function _postValidatePurchase(address beneficiary, uint256 weiAmount) internal virtual {
        // solhint-disable-previous-line no-empty-blocks
    }

    /**
     * @dev Source of tokens. Override this method to modify the way in which the crowdsale ultimately gets and sends
     * its tokens.
     * @param beneficiary Address performing the token purchase
     * @param tokenAmount Number of tokens to be emitted
     */
    function _deliverTokens(address beneficiary, uint256 tokenAmount) internal {
        require( _token.mint(beneficiary, tokenAmount) == true, \"Crowdsale: minting failed\");
    }

    /**
     * @dev Executed when a purchase has been validated and is ready to be executed. Doesn\u0027t necessarily emit/send
     * tokens.
     * @param beneficiary Address receiving the tokens
     * @param tokenAmount Number of tokens to be purchased
     */
    function _processPurchase(address beneficiary, uint256 tokenAmount) internal virtual {
        _deliverTokens(beneficiary, tokenAmount);
    }

    /**
     * @dev Override for extensions that require an internal state to check for validity (current user contributions,
     * etc.)
     * @param beneficiary Address receiving the tokens
     * @param weiAmount Value in wei involved in the purchase
     */
    function _updatePurchasingState(address beneficiary, uint256 weiAmount) internal {
        // solhint-disable-previous-line no-empty-blocks
    }

    /**
     * @dev Override to extend the way in which ether is converted to tokens.
     * @param weiAmount Value in wei to be converted into tokens
     * @return Number of tokens that can be purchased with the specified _weiAmount
     */
    function _getTokenAmount(uint256 weiAmount) internal virtual returns (uint256) {
        return weiAmount.mul(_rate);
    }

    /**
     * @dev Determines how ETH is stored/forwarded on purchases.
     */
    function _forwardFunds() virtual internal {
        _wallet.transfer(msg.value);
    }
}"},"ERC20CappedUnburnable.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.7.0;

import \"./ERC20PausableUnburnable.sol\";

/**
 * @dev Extension of {ERC20} that adds a cap to the supply of tokens.
 */
abstract contract ERC20CappedUnburnable is ERC20PausableUnburnable {
    using SafeMath for uint256;

    uint256 private _cap;

    /**
     * @dev Sets the value of the `cap`. This value is immutable, it can only be
     * set once during construction.
     */
    constructor (uint256 capGiven) {
        require(capGiven \u003e 0, \"ERC20Capped: cap is 0\");
        _cap = capGiven;
    }

    /**
     * @dev Returns the cap on the token\u0027s total supply.
     */
    function cap() public view returns (uint256) {
        return _cap;
    }

    /**
     * @dev See {ERC20-_beforeTokenTransfer}.
     *
     * Requirements:
     *
     * - minted tokens must not cause the total supply to go over the cap.
     */
    function _beforeTokenTransfer(address from, address to, uint256 amount) internal virtual override {
        super._beforeTokenTransfer(from, to, amount);

        if (from == address(0)) { // When minting tokens
            require(totalSupply().add(amount) \u003c= _cap, \"ERC20Capped: cap exceeded\");
        }
    }
}
"},"ERC20PausableUnburnable.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.7.0;

import \"./ERC20Unburnable.sol\";
import \"./Pausable.sol\";

/**
 * OpenZeppelin ERC20Pausable based on ERC20Unbernable
 */
abstract contract ERC20PausableUnburnable is ERC20Unburnable, Pausable {

    function _beforeTokenTransfer(address from, address to, uint256 amount) internal virtual override {
        super._beforeTokenTransfer(from, to, amount);

        require(!paused(), \"ERC20PausableUnburnable: token transfer while paused\");
    }
}"},"ERC20Unburnable.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.7.0;

import \"./Context.sol\";
import \"./IERC20.sol\";
import \"./SafeMath.sol\";
import \"./Address.sol\";

/**
  * OpenZeppelin Erc20 implementation without _burn private method and with cap mechanism
 */
contract ERC20Unburnable is Context, IERC20 {
    using SafeMath for uint256;
    using Address for address;


    mapping (address =\u003e uint256) private _balances;

    mapping (address =\u003e mapping (address =\u003e uint256)) private _allowances;

    uint256 private _totalSupply;

    string private _name;
    string private _symbol;
    uint8 private _decimals;

    constructor (string memory nameGiven, string memory symbolGiven) {
        _name = nameGiven;
        _symbol = symbolGiven;
        _decimals = 18;
    }

    function name() public view returns (string memory) {
        return _name;
    }

    function symbol() public view returns (string memory) {
        return _symbol;
    }

    function decimals() public view returns (uint8) {
        return _decimals;
    }

    function totalSupply() public view override returns (uint256) {
        return _totalSupply;
    }

    function balanceOf(address account) public view override returns (uint256) {
        return _balances[account];
    }

    function transfer(address recipient, uint256 amount) public virtual override returns (bool) {
        _transfer(_msgSender(), recipient, amount);
        return true;
    }

    function allowance(address owner, address spender) public view virtual override returns (uint256) {
        return _allowances[owner][spender];
    }

    function approve(address spender, uint256 amount) public virtual override returns (bool) {
        _approve(_msgSender(), spender, amount);
        return true;
    }

    function transferFrom(address sender, address recipient, uint256 amount) public virtual override returns (bool) {
        _transfer(sender, recipient, amount);
        _approve(sender, _msgSender(), _allowances[sender][_msgSender()].sub(amount, \"ERC20Unburnable: transfer amount exceeds allowance\"));
        return true;
    }

    function increaseAllowance(address spender, uint256 addedValue) public virtual returns (bool) {
        _approve(_msgSender(), spender, _allowances[_msgSender()][spender].add(addedValue));
        return true;
    }

    function decreaseAllowance(address spender, uint256 subtractedValue) public virtual returns (bool) {
        _approve(_msgSender(), spender, _allowances[_msgSender()][spender].sub(subtractedValue, \"ERC20Unburnable: decreased allowance below zero\"));
        return true;
    }

    function _transfer(address sender, address recipient, uint256 amount) internal virtual {
        require(sender != address(0), \"ERC20Unburnable: transfer from the zero address\");
        require(recipient != address(0), \"ERC20Unburnable: transfer to the zero address\");

        _beforeTokenTransfer(sender, recipient, amount);

        _balances[sender] = _balances[sender].sub(amount, \"ERC20Unburnable: transfer amount exceeds balance\");
        _balances[recipient] = _balances[recipient].add(amount);
        emit Transfer(sender, recipient, amount);
    }

    function _mint(address account, uint256 amount) internal virtual {
        require(account != address(0), \"ERC20Unburnable: mint to the zero address\");

        _beforeTokenTransfer(address(0), account, amount);

        _totalSupply = _totalSupply.add(amount);
        _balances[account] = _balances[account].add(amount);
        emit Transfer(address(0), account, amount);
    }

    function _approve(address owner, address spender, uint256 amount) internal virtual {
        require(owner != address(0), \"ERC20Unburnable: approve from the zero address\");
        require(spender != address(0), \"ERC20Unburnable: approve to the zero address\");

        _allowances[owner][spender] = amount;
        emit Approval(owner, spender, amount);
    }

    function _setupDecimals(uint8 decimals_) internal {
        _decimals = decimals_;
    }

    function _beforeTokenTransfer(address from, address to, uint256 amount) internal virtual {
     }
}
"},"Escrow.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.7.0;

import \"./SafeMath.sol\";
import \"./Ownable.sol\";
import \"./Address.sol\";

 /**
  * @title Escrow
  * @dev Base escrow contract, holds funds designated for a payee until they
  * withdraw them.
  *
  * Intended usage: This contract (and derived escrow contracts) should be a
  * standalone contract, that only interacts with the contract that instantiated
  * it. That way, it is guaranteed that all Ether will be handled according to
  * the `Escrow` rules, and there is no need to check for payable functions or
  * transfers in the inheritance tree. The contract that uses the escrow as its
  * payment method should be its owner, and provide public methods redirecting
  * to the escrow\u0027s deposit and withdraw.
  */
contract Escrow is Ownable {
    using SafeMath for uint256;
    using Address for address payable;

    event Deposited(address indexed payee, uint256 weiAmount);
    event Withdrawn(address indexed payee, uint256 weiAmount);

    mapping(address =\u003e uint256) private _deposits;

    function depositsOf(address payee) public view returns (uint256) {
        return _deposits[payee];
    }

    /**
     * @dev Stores the sent amount as credit to be withdrawn.
     * @param payee The destination address of the funds.
     */
    function deposit(address payee) public virtual payable onlyOwner {
        uint256 amount = msg.value;
        _deposits[payee] = _deposits[payee].add(amount);

        emit Deposited(payee, amount);
    }

    /**
     * @dev Withdraw accumulated balance for a payee, forwarding all gas to the
     * recipient.
     *
     * WARNING: Forwarding all gas opens the door to reentrancy vulnerabilities.
     * Make sure you trust the recipient, or are either following the
     * checks-effects-interactions pattern or using {ReentrancyGuard}.
     *
     * @param payee The address whose funds will be withdrawn and transferred to.
     */
    function withdraw(address payable payee) public virtual onlyOwner {
        uint256 payment = _deposits[payee];

        _deposits[payee] = 0;

        payee.sendValue(payment);

        emit Withdrawn(payee, payment);
    }
}
"},"FinalizableCrowdsale.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.7.0;

import \"./SafeMath.sol\";
import \"./TimedCrowdsale.sol\";

/**
 * @title FinalizableCrowdsale
 * @dev Extension of TimedCrowdsale with a one-off finalization action, where one
 * can do extra work after finishing.
 */
abstract contract FinalizableCrowdsale is TimedCrowdsale {
    using SafeMath for uint256;

    bool private _finalized;

    event CrowdsaleFinalized();

    constructor () {
        _finalized = false;
    }

    /**
     * @return true if the crowdsale is finalized, false otherwise.
     */
    function finalized() public view returns (bool) {
        return _finalized;
    }

    /**
     * @dev Must be called after crowdsale ends, to do some extra finalization
     * work. Calls the contract\u0027s finalization function.
     */
    function finalize() public {
        require(!_finalized, \"FinalizableCrowdsale: already finalized\");
        require(hasClosed(), \"FinalizableCrowdsale: not closed\");

        _finalized = true;

        _finalization();
        emit CrowdsaleFinalized();
    }

    /**
     * @dev Can be overridden to add finalization logic. The overriding function
     * should call super._finalization() to ensure the chain of finalization is
     * executed entirely.
     */
    function _finalization() internal virtual {
        // solhint-disable-previous-line no-empty-blocks
    }
}"},"HLBICO.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.7.0;

import \"./SafeMath.sol\";
import \"./CappedTimedCrowdsale.sol\";
import \"./RefundPostdevCrowdsale.sol\";


/**
**  ICO Contract for the LBC crowdsale
*/
contract HLBICO is CappedTimedCrowdsale, RefundablePostDeliveryCrowdsale {
    using SafeMath for uint256;

    /*
    ** Global State
    */
    bool public initialized; // default : false

    /*
    ** Addresses
    */
    address public _deployingAddress; // should remain the same as deployer\u0027s address
    address public _whitelistingAddress; // should be oracle
    address public _reserveAddress; // should be deployer then humble reserve

    /*
    ** Events
    */
    event InitializedContract(address indexed changerAddress, address indexed whitelistingAddress);
    event ChangedWhitelisterAddress(address indexed whitelisterAddress, address indexed changerAddress);
    event ChangedReserveAddress(address indexed reserveAddress, address indexed changerAddress);
    event ChangedDeployerAddress(address indexed deployerAddress, address indexed changerAddress);
    event BlacklistedAdded(address indexed account);
    event BlacklistedRemoved(address indexed account);
    event UpdatedCaps(uint256 newGoal, uint256 newCap, uint256 newTranche, uint256 newMaxInvest, uint256 newRate, uint256 newRateCoef);

    /*
    ** Attrs
    */
    uint256 private _currentRate;
    uint256 private _rateCoef;
    mapping(address =\u003e bool) private _blacklistedAddrs;
    mapping(address =\u003e uint256) private _investmentAddrs;
    uint256 private _weiMaxInvest;
    uint256 private _etherTranche;
    uint256 private _currentWeiTranche; // Holds the current invested value for a tranche
    uint256 private _deliverToReserve;
    uint256 private _minimumInvest;

    /*
    * initialRateReceived : Number of token units a buyer gets per wei for the first investment slice. Should be 5000 (diving by 1000 for 3 decimals).
    * walletReceived : Wallet that will get the invested eth at the end of the crowdsale
    * tokenReceived : Address of the LBC token being sold
    * openingTimeReceived : Starting date of the ICO
    * closingtimeReceived : Ending date of the ICO
    * capReceived : Max amount of wei to be contributed
    * goalReceived : Funding goal
    * etherMaxInvestReceived : Maximum ether that can be invested
    */
    constructor(uint256 initialRateReceived,
        uint256 rateCoefficientReceived,
        address payable walletReceived,
        LBCToken tokenReceived,
        uint256 openingTimeReceived,
        uint256 closingTimeReceived,
        uint256 capReceived,
        uint256 goalReceived)
        CrowdsaleMint(initialRateReceived, walletReceived, tokenReceived)
        TimedCrowdsale(openingTimeReceived, closingTimeReceived)
        CappedTimedCrowdsale(capReceived)
        RefundableCrowdsale(goalReceived) {
        _deployingAddress = msg.sender;
        _etherTranche = 250000000000000000000; // 300000€; For eth = 1200 €
        _weiMaxInvest = 8340000000000000000; // 10008€; for eth = 1200 €
        _currentRate = initialRateReceived;
        _rateCoef = rateCoefficientReceived;
        _currentWeiTranche = 0;
        _deliverToReserve = 0;
        _minimumInvest = 1000000000000000; // 1.20€; for eth = 1200€
    }

    /*
    ** Initializes the contract address and affects addresses to their roles.
    */
    function init(
        address whitelistingAddress,
        address reserveAddress
    )
    public
    isNotInitialized
    onlyDeployingAddress
    {
        require(whitelistingAddress != address(0), \"HLBICO: whitelistingAddress cannot be 0x\");
        require(reserveAddress != address(0), \"HLBICO: reserveAddress cannot be 0x\");

        _whitelistingAddress = whitelistingAddress;
        _reserveAddress = reserveAddress;
        initialized = true;

        emit InitializedContract(_msgSender(), whitelistingAddress);
    }

    /**
     * @dev Returns the rate of tokens per wei at the present time and computes rate depending on tranche.
     * @param weiAmount The value in wei to be converted into tokens
     * @return The number of tokens a buyer gets per wei for a given tranche
     */
    function _getCustomAmount(uint256 weiAmount) internal returns (uint256) {
        if (!isOpen()) {
            return 0;
        }

        uint256 calculatedAmount = 0;

        _currentWeiTranche = _currentWeiTranche.add(weiAmount);

        if (_currentWeiTranche \u003e _etherTranche) {
            _currentWeiTranche = _currentWeiTranche.sub(_etherTranche);

            //If we updated the tranche manually to a smaller one
            uint256 manualSkew = weiAmount.sub(_currentWeiTranche);

            if (manualSkew \u003e= 0) {
                calculatedAmount = calculatedAmount.add(weiAmount.sub(_currentWeiTranche).mul(rate()));
                _currentRate -= _rateCoef; // coefficient for 35 tokens reduction for each tranche
                calculatedAmount = calculatedAmount.add(_currentWeiTranche.mul(rate()));
            }
            //If there is a skew between invested wei and calculated wei for a tranche
            else {
                _currentRate -= _rateCoef; // coefficient for 35 tokens reduction for each tranche
                calculatedAmount = calculatedAmount.add(weiAmount.mul(rate()));
            }
        }
        else
            calculatedAmount = calculatedAmount.add(weiAmount.mul(rate()));

        uint256 participationAmount = calculatedAmount.mul(5).div(100);

        calculatedAmount = calculatedAmount.sub(participationAmount);
        _deliverToReserve = _deliverToReserve.add(participationAmount);

        return calculatedAmount;
    }

    /*
    ** Adjusts all parameters influenced by Ether value based on a percentage coefficient
    ** coef is based on 4 digits for decimal representation with 1 precision
    ** i.e : 934 -\u003e 93.4%; 1278 -\u003e 127.8%
    */
    function adjustEtherValue(uint256 coef)
    public
    onlyDeployingAddress {
        require(coef \u003e 0 \u0026\u0026 coef \u003c 10000, \"HLBICO: coef isn\u0027t within range of authorized values\");

        uint256 baseCoef = 1000;

        changeGoal(goal().mul(coef).div(1000));
        changeCap(cap().mul(coef).div(1000));
        _etherTranche = _etherTranche.mul(coef).div(1000);
        _weiMaxInvest = _weiMaxInvest.mul(coef).div(1000);
        
        if (coef \u003e 1000) {
            coef = coef.sub(1000);
            _currentRate = _currentRate.sub(_currentRate.mul(coef).div(1000));
            _rateCoef = _rateCoef.sub(_rateCoef.mul(coef).div(1000));
        } else {
            coef = baseCoef.sub(coef);
            _currentRate = _currentRate.add(_currentRate.mul(coef).div(1000));
            _rateCoef = _rateCoef.add(_rateCoef.mul(coef).div(1000));
        }

        emit UpdatedCaps(goal(), cap(), _etherTranche, _weiMaxInvest, _currentRate, _rateCoef);
    }

    function rate() public view override returns (uint256) {
       return _currentRate;
    }

    function getNextRate() public view returns (uint256) {
        return _currentRate.sub(_rateCoef);
    }

    /*
    ** Changes the address of the token contract. Must only be callable by deployer
    */
    function changeToken(LBCToken newToken)
    public
    onlyDeployingAddress
    {
        _changeToken(newToken);
    }

    /*
    ** Changes the address with whitelisting role and can only be called by deployer
    */
    function changeWhitelister(address newWhitelisterAddress)
    public
    onlyDeployingAddress
    {
        _whitelistingAddress = newWhitelisterAddress;
        emit ChangedWhitelisterAddress(newWhitelisterAddress, _msgSender());
    }
    
    /*
    ** Changes the address with deployer role and can only be called by deployer
    */
    function changeDeployer(address newDeployerAddress)
    public
    onlyDeployingAddress
    {
        _deployingAddress = newDeployerAddress;
        emit ChangedDeployerAddress(_deployingAddress, _msgSender());
    }

    /*
    ** Changes the address with pause role and can only be called by deployer
    */
    function changeReserveAddress(address newReserveAddress)
    public
    onlyDeployingAddress
    {
        _reserveAddress = newReserveAddress;
        emit ChangedReserveAddress(newReserveAddress, _msgSender());
    }

    /**
     * @dev Escrow finalization task, called when finalize() is called.
     */
    function _finalization() override virtual internal {
        // Mints the 5% participation and sends it to humblereserve
        if (goalReached()) {
            _deliverTokens(_reserveAddress, _deliverToReserve);
        }

        super._finalization();
    }

    /*
    ** Checks if an adress has been blacklisted before letting them withdraw their funds
    */
    function withdrawTokens(address beneficiary) override virtual public {
        require(!isBlacklisted(beneficiary), \"HLBICO: account is blacklisted\");

        super.withdrawTokens(beneficiary);
    }

    /**
     * @dev Overrides parent method taking into account variable rate.
     * @param weiAmount The value in wei to be converted into tokens
     * @return The number of tokens _weiAmount wei will buy at present time
     */
    function _getTokenAmount(uint256 weiAmount) internal override returns (uint256) {
       return _getCustomAmount(weiAmount);
    }

    function _forwardFunds() internal override(CrowdsaleMint, RefundablePostDeliveryCrowdsale) {
        RefundablePostDeliveryCrowdsale._forwardFunds();
    }

    function _preValidatePurchase(address beneficiary, uint256 weiAmount) internal override(TimedCrowdsale, CappedTimedCrowdsale) view {
        require(weiAmount \u003e= _minimumInvest, \"HLBICO: Investment must be greater than or equal to 0.001 eth\");
        _dontExceedAmount(beneficiary, weiAmount);
        CappedTimedCrowdsale._preValidatePurchase(beneficiary, weiAmount);
    }

    function _postValidatePurchase(address beneficiary, uint256 weiAmount) internal override {
        require(beneficiary != address(0), \"HLBICO: _postValidatePurchase benificiary is the zero address\");

        _investmentAddrs[beneficiary] = _investmentAddrs[beneficiary].add(weiAmount);        
    }

    function _processPurchase(address beneficiary, uint256 tokenAmount) internal override(CrowdsaleMint, RefundablePostDeliveryCrowdsale) {
        RefundablePostDeliveryCrowdsale._processPurchase(beneficiary, tokenAmount);
    }

    function hasClosed() public view override(TimedCrowdsale, CappedTimedCrowdsale) returns (bool) {
        // solhint-disable-next-line not-rely-on-time
        return CappedTimedCrowdsale.hasClosed();
    }

    function etherTranche() public view returns (uint256) {
        return _etherTranche;
    }

    function maxInvest() public view returns (uint256) {
        return _weiMaxInvest;
    }

    function addBlacklisted(address account) public onlyWhitelistingAddress {
        _addBlacklisted(account);
    }

    function removeBlacklisted(address account) public onlyWhitelistingAddress {
        _removeBlacklisted(account);
    }

    function isBlacklisted(address account) public view returns (bool) {
        require(account != address(0), \"HLBICO: account is zero address\");
        return _blacklistedAddrs[account];
    }

    function _addBlacklisted(address account) internal {
        require(!isBlacklisted(account), \"HLBICO: account already blacklisted\");
        _blacklistedAddrs[account] = true;
        emit BlacklistedAdded(account);
    }

    function _removeBlacklisted(address account) internal {
        require(isBlacklisted(account), \"HLBICO: account is not blacklisted\");
        _blacklistedAddrs[account] = true;
        emit BlacklistedRemoved(account);
    }

    function _dontExceedAmount(address beneficiary, uint256 weiAmount) internal view {
        require(_investmentAddrs[beneficiary].add(weiAmount) \u003c= _weiMaxInvest,
          \"HLBICO: Cannot invest more than KYC limit.\");
    }

    modifier onlyWhitelistingAddress() {
        require(_msgSender() == _whitelistingAddress, \"HLBICO: caller does not have the Whitelisted role\");
        _;
    }

    /*
    ** Checks if the contract hasn\u0027t already been initialized
    */
    modifier isNotInitialized() {
        require(initialized == false, \"HLBICO: contract is already initialized.\");
        _;
    }

    /*
    ** Checks if the sender is the minter controller address
    */
    modifier onlyDeployingAddress() {
        require(msg.sender == _deployingAddress, \"HLBICO: only the deploying address can call this method.\");
        _;
    }

}"},"IERC20.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.7.0;

/**
 * @dev Interface of the ERC20 standard as defined in the EIP.
 */
interface IERC20 {
    /**
     * @dev Returns the amount of tokens in existence.
     */
    function totalSupply() external view returns (uint256);

    /**
     * @dev Returns the amount of tokens owned by `account`.
     */
    function balanceOf(address account) external view returns (uint256);

    /**
     * @dev Moves `amount` tokens from the caller\u0027s account to `recipient`.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transfer(address recipient, uint256 amount) external returns (bool);

    /**
     * @dev Returns the remaining number of tokens that `spender` will be
     * allowed to spend on behalf of `owner` through {transferFrom}. This is
     * zero by default.
     *
     * This value changes when {approve} or {transferFrom} are called.
     */
    function allowance(address owner, address spender) external view returns (uint256);

    /**
     * @dev Sets `amount` as the allowance of `spender` over the caller\u0027s tokens.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * IMPORTANT: Beware that changing an allowance with this method brings the risk
     * that someone may use both the old and the new allowance by unfortunate
     * transaction ordering. One possible solution to mitigate this race
     * condition is to first reduce the spender\u0027s allowance to 0 and set the
     * desired value afterwards:
     * https://github.com/ethereum/EIPs/issues/20#issuecomment-263524729
     *
     * Emits an {Approval} event.
     */
    function approve(address spender, uint256 amount) external returns (bool);

    /**
     * @dev Moves `amount` tokens from `sender` to `recipient` using the
     * allowance mechanism. `amount` is then deducted from the caller\u0027s
     * allowance.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transferFrom(address sender, address recipient, uint256 amount) external returns (bool);

    /**
     * @dev Emitted when `value` tokens are moved from one account (`from`) to
     * another (`to`).
     *
     * Note that `value` may be zero.
     */
    event Transfer(address indexed from, address indexed to, uint256 value);

    /**
     * @dev Emitted when the allowance of a `spender` for an `owner` is set by
     * a call to {approve}. `value` is the new allowance.
     */
    event Approval(address indexed owner, address indexed spender, uint256 value);
}
"},"LBCToken.sol":{"content":"// SPDX-License-Identifier: MIT


pragma solidity ^0.7.0;

import \"./Context.sol\";
import \"./ERC20CappedUnburnable.sol\";
import \"./ERC20Unburnable.sol\";
import \"./ERC20PausableUnburnable.sol\";

contract LBCToken is Context, ERC20CappedUnburnable {

    /*
    ** Global State
    */
    bool public initialized; // default : false

    /*
    ** Addresses
    */
    address public _deployingAddress; // should be changed to multisig contract address
    address public _pauserAddress; // should be deployer\u0027s
    address public _minterAddress; // should be ico\u0027s address then poe\u0027s
    address public _reserveAddress; // should be multisig then humble reserve

    /*
    ** Events
    */
    event InitializedContract(address indexed reserveAddress);
    event ChangedMinterAddress(address indexed minterAddress, address indexed changerAddress);
    event ChangedPauserAddress(address indexed pauserAddress, address indexed changerAddress);
    event ChangedReserveAddress(address indexed reserveAddress, address indexed changerAddress);
    event ChangedDeployerAddress(address indexed deployerAddress, address indexed changerAddress);


    constructor(
        string memory name,
        string memory symbol
    )
    ERC20Unburnable(name, symbol)
    ERC20CappedUnburnable(300000000000000000000000000)
    {
        _deployingAddress = msg.sender;
    }

    /*
    ** Initializes the contract address and affects addresses to their roles.
    */
    function init(
        address minterAddress,
        address pauserAddress,
        address reserveAddress
    )
    public
    isNotInitialized
    onlyDeployingAddress
    {
        require(minterAddress != address(0), \"_minterAddress cannot be 0x\");
        require(pauserAddress != address(0), \"_pauserAddress cannot be 0x\");
        require(reserveAddress != address(0), \"_reserveAddress cannot be 0x\");

        _minterAddress = minterAddress;
        _pauserAddress = pauserAddress;
        _reserveAddress = reserveAddress;

        initialized = true;

        emit InitializedContract(reserveAddress);
    }

    /*
    ** Mint function that can only be called by minter address and mints a specified amount and sends it to an address
    */
    function mint(address to, uint256 amount)
    public
    onlyMinterAddress
    virtual
    returns (bool) {
       _mint(to, amount);
       return true;
    }

    /*
    ** Freeze function that stops transactions and can only be called by pauser address
    */
    function pause()
    public
    onlyPauserAddress
    virtual {
        _pause();
    }

    /*
    ** Unfreeze function that resumes transactions and can only be called by pauser address
    */
    function unpause()
    public
    onlyPauserAddress
    virtual {
        _unpause();
    }

    /*
    ** Changes the address with pause role and can only be called by previous pauser address
    */
    function changePauser(address newPauserAddress)
    public
    onlyDeployingAddress
    whenNotPaused
    {
        _pauserAddress = newPauserAddress;
        emit ChangedPauserAddress(newPauserAddress, _msgSender());
    }

    /*
    ** Changes the address with minter role and can only be called by previous minter address
    */
    function changeMinter(address newMinterAddress)
    public
    onlyDeployingAddress
    whenNotPaused
    {
        _minterAddress = newMinterAddress;
        emit ChangedMinterAddress(newMinterAddress, _msgSender());
    }

    /*
    ** Changes the address with deployer role and can only be called by deployer
    */
    function changeDeployer(address newDeployerAddress)
    public
    onlyDeployingAddress
    {
        _deployingAddress = newDeployerAddress;
        emit ChangedDeployerAddress(_deployingAddress, _msgSender());
    }

    /*
    ** Checks if the sender is the minter controller address
    */
    modifier onlyDeployingAddress() {
        require(msg.sender == _deployingAddress, \"Only the deploying address can call this method.\");
        _;
    }

    /*
    ** Checks if the sender is the minter controller address
    */
    modifier onlyMinterAddress() {
        require(msg.sender == _minterAddress, \"Only the minter address can call this method.\");
        _;
    }

    /*
    ** Checks if the sender is the pauser controller address
    */
    modifier onlyPauserAddress() {
        require(msg.sender == _pauserAddress, \"Only the pauser address can call this method.\");
        _;
    }

    /*
    ** Checks if the contract hasn\u0027t already been initialized
    */
    modifier isNotInitialized() {
        require(initialized == false, \"Contract is already initialized.\");
        _;
    }

    function _beforeTokenTransfer(address from, address to, uint256 amount) internal virtual override(ERC20CappedUnburnable) {
        super._beforeTokenTransfer(from, to, amount);
    }
}"},"Ownable.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.7.0;

import \"./Context.sol\";
/**
 * @dev Contract module which provides a basic access control mechanism, where
 * there is an account (an owner) that can be granted exclusive access to
 * specific functions.
 *
 * By default, the owner account will be the one that deploys the contract. This
 * can later be changed with {transferOwnership}.
 *
 * This module is used through inheritance. It will make available the modifier
 * `onlyOwner`, which can be applied to your functions to restrict their use to
 * the owner.
 */
contract Ownable is Context {
    address private _owner;

    event OwnershipTransferred(address indexed previousOwner, address indexed newOwner);

    /**
     * @dev Initializes the contract setting the deployer as the initial owner.
     */
    constructor () {
        address msgSender = _msgSender();
        _owner = msgSender;
        emit OwnershipTransferred(address(0), msgSender);
    }

    /**
     * @dev Returns the address of the current owner.
     */
    function owner() public view returns (address) {
        return _owner;
    }

    /**
     * @dev Throws if called by any account other than the owner.
     */
    modifier onlyOwner() {
        require(_owner == _msgSender(), \"Ownable: caller is not the owner\");
        _;
    }

    /**
     * @dev Leaves the contract without owner. It will not be possible to call
     * `onlyOwner` functions anymore. Can only be called by the current owner.
     *
     * NOTE: Renouncing ownership will leave the contract without an owner,
     * thereby removing any functionality that is only available to the owner.
     */
    function renounceOwnership() public virtual onlyOwner {
        emit OwnershipTransferred(_owner, address(0));
        _owner = address(0);
    }

    /**
     * @dev Transfers ownership of the contract to a new account (`newOwner`).
     * Can only be called by the current owner.
     */
    function transferOwnership(address newOwner) public virtual onlyOwner {
        require(newOwner != address(0), \"Ownable: new owner is the zero address\");
        emit OwnershipTransferred(_owner, newOwner);
        _owner = newOwner;
    }
}
"},"Pausable.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.7.0;

import \"./Context.sol\";

/**
 * @dev Contract module which allows children to implement an emergency stop
 * mechanism that can be triggered by an authorized account.
 *
 * This module is used through inheritance. It will make available the
 * modifiers `whenNotPaused` and `whenPaused`, which can be applied to
 * the functions of your contract. Note that they will not be pausable by
 * simply including this module, only once the modifiers are put in place.
 */
contract Pausable is Context {
    /**
     * @dev Emitted when the pause is triggered by `account`.
     */
    event Paused(address account);

    /**
     * @dev Emitted when the pause is lifted by `account`.
     */
    event Unpaused(address account);

    bool private _paused;

    /**
     * @dev Initializes the contract in unpaused state.
     */
    constructor () {
        _paused = false;
    }

    /**
     * @dev Returns true if the contract is paused, and false otherwise.
     */
    function paused() public view returns (bool) {
        return _paused;
    }

    /**
     * @dev Modifier to make a function callable only when the contract is not paused.
     *
     * Requirements:
     *
     * - The contract must not be paused.
     */
    modifier whenNotPaused() {
        require(!_paused, \"Pausable: paused\");
        _;
    }

    /**
     * @dev Modifier to make a function callable only when the contract is paused.
     *
     * Requirements:
     *
     * - The contract must be paused.
     */
    modifier whenPaused() {
        require(_paused, \"Pausable: not paused\");
        _;
    }

    /**
     * @dev Triggers stopped state.
     *
     * Requirements:
     *
     * - The contract must not be paused.
     */
    function _pause() internal virtual whenNotPaused {
        _paused = true;
        emit Paused(_msgSender());
    }

    /**
     * @dev Returns to normal state.
     *
     * Requirements:
     *
     * - The contract must be paused.
     */
    function _unpause() internal virtual whenPaused {
        _paused = false;
        emit Unpaused(_msgSender());
    }
}
"},"PostDeliveryCrowdsale.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.7.0;

import \"./TimedCrowdsale.sol\";
import \"./SafeMath.sol\";
import \"./Secondary.sol\";
import \"./IERC20.sol\";

/**
 * @title PostDeliveryCrowdsale
 * @dev Crowdsale that locks tokens from withdrawal until it ends.
 */
abstract contract PostDeliveryCrowdsale is TimedCrowdsale {
    using SafeMath for uint256;

    mapping(address =\u003e uint256) private _balances;
    __unstable__TokenVault private _vault;

    constructor() {
        _vault = new __unstable__TokenVault();
    }

    /**
     * @dev Withdraw tokens only after crowdsale ends.
     * @param beneficiary Whose tokens will be withdrawn.
     */
    function withdrawTokens(address beneficiary) public virtual {
        require(hasClosed(), \"PostDeliveryCrowdsale: not closed\");
        uint256 amount = _balances[beneficiary];
        require(amount \u003e 0, \"PostDeliveryCrowdsale: beneficiary is not due any tokens\");

        _balances[beneficiary] = 0;
        _vault.transfer(token(), beneficiary, amount);
    }

    /**
     * @return the balance of an account.
     */
    function balanceOf(address account) public view returns (uint256) {
        return _balances[account];
    }

    /**
     * @dev Overrides parent by storing due balances, and delivering tokens to the vault instead of the end user. This
     * ensures that the tokens will be available by the time they are withdrawn (which may not be the case if
     * `_deliverTokens` was called later).
     * @param beneficiary Token purchaser
     * @param tokenAmount Amount of tokens purchased
     */
    function _processPurchase(address beneficiary, uint256 tokenAmount) override virtual internal {
        _balances[beneficiary] = _balances[beneficiary].add(tokenAmount);
        _deliverTokens(address(_vault), tokenAmount);
    }
}

/**
 * @title __unstable__TokenVault
 * @dev Similar to an Escrow for tokens, this contract allows its primary account to spend its tokens as it sees fit.
 * This contract is an internal helper for PostDeliveryCrowdsale, and should not be used outside of this context.
 */
// solhint-disable-next-line contract-name-camelcase
contract __unstable__TokenVault is Secondary {
    function transfer(IERC20 token, address to, uint256 amount) public onlyPrimary {
        token.transfer(to, amount);
    }
}"},"ReentrancyGuard.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.7.0;

/**
 * @dev Contract module that helps prevent reentrant calls to a function.
 *
 * Inheriting from `ReentrancyGuard` will make the {nonReentrant} modifier
 * available, which can be applied to functions to make sure there are no nested
 * (reentrant) calls to them.
 *
 * Note that because there is a single `nonReentrant` guard, functions marked as
 * `nonReentrant` may not call one another. This can be worked around by making
 * those functions `private`, and then adding `external` `nonReentrant` entry
 * points to them.
 *
 * TIP: If you would like to learn more about reentrancy and alternative ways
 * to protect against it, check out our blog post
 * https://blog.openzeppelin.com/reentrancy-after-istanbul/[Reentrancy After Istanbul].
 */
contract ReentrancyGuard {
    // Booleans are more expensive than uint256 or any type that takes up a full
    // word because each write operation emits an extra SLOAD to first read the
    // slot\u0027s contents, replace the bits taken up by the boolean, and then write
    // back. This is the compiler\u0027s defense against contract upgrades and
    // pointer aliasing, and it cannot be disabled.

    // The values being non-zero value makes deployment a bit more expensive,
    // but in exchange the refund on every call to nonReentrant will be lower in
    // amount. Since refunds are capped to a percentage of the total
    // transaction\u0027s gas, it is best to keep them low in cases like this one, to
    // increase the likelihood of the full refund coming into effect.
    uint256 private constant _NOT_ENTERED = 1;
    uint256 private constant _ENTERED = 2;

    uint256 private _status;

    constructor () {
        _status = _NOT_ENTERED;
    }

    /**
     * @dev Prevents a contract from calling itself, directly or indirectly.
     * Calling a `nonReentrant` function from another `nonReentrant`
     * function is not supported. It is possible to prevent this from happening
     * by making the `nonReentrant` function external, and make it call a
     * `private` function that does the actual work.
     */
    modifier nonReentrant() {
        // On the first call to nonReentrant, _notEntered will be true
        require(_status != _ENTERED, \"ReentrancyGuard: reentrant call\");

        // Any calls to nonReentrant after this point will fail
        _status = _ENTERED;

        _;

        // By storing the original value once again, a refund is triggered (see
        // https://eips.ethereum.org/EIPS/eip-2200)
        _status = _NOT_ENTERED;
    }
}
"},"RefundableCrowdsale.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.7.0;

import \"./Context.sol\";
import \"./SafeERC20.sol\";
import \"./FinalizableCrowdsale.sol\";
import \"./RefundEscrow.sol\";

/**
 * @title RefundableCrowdsale
 * @dev Extension of `FinalizableCrowdsale` contract that adds a funding goal, and the possibility of users
 * getting a refund if goal is not met.
 *
 * Deprecated, use `RefundablePostDeliveryCrowdsale` instead. Note that if you allow tokens to be traded before the goal
 * is met, then an attack is possible in which the attacker purchases tokens from the crowdsale and when they sees that
 * the goal is unlikely to be met, they sell their tokens (possibly at a discount). The attacker will be refunded when
 * the crowdsale is finalized, and the users that purchased from them will be left with worthless tokens.
 */
abstract contract RefundableCrowdsale is Context, FinalizableCrowdsale {
    using SafeMath for uint256;

    // minimum amount of funds to be raised in weis
    uint256 private _goal;

    // refund escrow used to hold funds while crowdsale is running
    RefundEscrow private _escrow;

    /**
     * @dev Constructor, creates RefundEscrow.
     * @param goalReceived Funding goal
     */
    constructor (uint256 goalReceived) {
        require(goalReceived \u003e 0, \"RefundableCrowdsale: goal is 0\");
        _escrow = new RefundEscrow(wallet());
        _goal = goalReceived;
    }

    /**
     * @return minimum amount of funds to be raised in wei.
     */
    function goal() public view returns (uint256) {
        return _goal;
    }

    /**
    ** Updates goal
    */
    function changeGoal(uint256 newGoal) internal {
        _goal = newGoal;
    }

    /**
     * @dev Investors can claim refunds here if crowdsale is unsuccessful.
     * @param refundee Whose refund will be claimed.
     */
    function claimRefund(address payable refundee) public {
        require(finalized(), \"RefundableCrowdsale: not finalized\");
        require(!goalReached(), \"RefundableCrowdsale: goal reached\");

        _escrow.withdraw(refundee);
    }

    /**
     * @dev Checks whether funding goal was reached.
     * @return Whether funding goal was reached
     */
    function goalReached() public view returns (bool) {
        return weiRaised() \u003e= _goal;
    }

    /**
     * @dev Escrow finalization task, called when finalize() is called.
     */
    function _finalization() override virtual internal {
        if (goalReached()) {
            _escrow.close();
            _escrow.beneficiaryWithdraw();
        } else {
            _escrow.enableRefunds();
        }

        super._finalization();
    }

    /**
     * @dev Overrides Crowdsale fund forwarding, sending funds to escrow.
     */
    function _forwardFunds() override virtual internal {
        _escrow.deposit{value : msg.value}(_msgSender());
    }
}"},"RefundEscrow.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.7.0;

import \"./ConditionalEscrow.sol\";

/**
 * @title RefundEscrow
 * @dev Escrow that holds funds for a beneficiary, deposited from multiple
 * parties.
 * @dev Intended usage: See {Escrow}. Same usage guidelines apply here.
 * @dev The owner account (that is, the contract that instantiates this
 * contract) may deposit, close the deposit period, and allow for either
 * withdrawal by the beneficiary, or refunds to the depositors. All interactions
 * with `RefundEscrow` will be made through the owner contract.
 */
contract RefundEscrow is ConditionalEscrow {
    enum State { Active, Refunding, Closed }

    event RefundsClosed();
    event RefundsEnabled();

    State private _state;
    address payable private _beneficiary;

    /**
     * @dev Constructor.
     * @param beneficiaryReceived The beneficiary of the deposits.
     */
    constructor (address payable beneficiaryReceived) {
        require(beneficiaryReceived != address(0), \"RefundEscrow: beneficiary is the zero address\");
        _beneficiary = beneficiaryReceived;
        _state = State.Active;
    }

    /**
     * @return The current state of the escrow.
     */
    function state() public view returns (State) {
        return _state;
    }

    /**
     * @return The beneficiary of the escrow.
     */
    function beneficiary() public view returns (address) {
        return _beneficiary;
    }

    /**
     * @dev Stores funds that may later be refunded.
     * @param refundee The address funds will be sent to if a refund occurs.
     */
    function deposit(address refundee) public payable virtual override {
        require(_state == State.Active, \"RefundEscrow: can only deposit while active\");
        super.deposit(refundee);
    }

    /**
     * @dev Allows for the beneficiary to withdraw their funds, rejecting
     * further deposits.
     */
    function close() public onlyOwner virtual {
        require(_state == State.Active, \"RefundEscrow: can only close while active\");
        _state = State.Closed;
        emit RefundsClosed();
    }

    /**
     * @dev Allows for refunds to take place, rejecting further deposits.
     */
    function enableRefunds() public onlyOwner virtual {
        require(_state == State.Active, \"RefundEscrow: can only enable refunds while active\");
        _state = State.Refunding;
        emit RefundsEnabled();
    }

    /**
     * @dev Withdraws the beneficiary\u0027s funds.
     */
    function beneficiaryWithdraw() public virtual {
        require(_state == State.Closed, \"RefundEscrow: beneficiary can only withdraw while closed\");
        _beneficiary.transfer(address(this).balance);
    }

    /**
     * @dev Returns whether refundees can withdraw their deposits (be refunded). The overridden function receives a
     * \u0027payee\u0027 argument, but we ignore it here since the condition is global, not per-payee.
     */
    function withdrawalAllowed(address) public view override returns (bool) {
        return _state == State.Refunding;
    }
}
"},"RefundPostdevCrowdsale.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.7.0;
import \"./RefundableCrowdsale.sol\";
import \"./PostDeliveryCrowdsale.sol\";


/**
 * @title RefundablePostDeliveryCrowdsale
 * @dev Extension of RefundableCrowdsale contract that only delivers the tokens
 * once the crowdsale has closed and the goal met, preventing refunds to be issued
 * to token holders.
 */
abstract contract RefundablePostDeliveryCrowdsale is RefundableCrowdsale, PostDeliveryCrowdsale {

    function _forwardFunds() internal override(CrowdsaleMint,RefundableCrowdsale) virtual {
        RefundableCrowdsale._forwardFunds();
    }

    function _processPurchase(address beneficiary, uint256 tokenAmount) internal override(CrowdsaleMint, PostDeliveryCrowdsale) virtual {
        PostDeliveryCrowdsale._processPurchase(beneficiary, tokenAmount);
    }

    function withdrawTokens(address beneficiary) override virtual public {
        require(finalized(), \"RefundablePostDeliveryCrowdsale: not finalized\");
        require(goalReached(), \"RefundablePostDeliveryCrowdsale: goal not reached\");

        super.withdrawTokens(beneficiary);
    }
}"},"SafeERC20.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.7.0;

import \"./IERC20.sol\";
import \"./SafeMath.sol\";
import \"./Address.sol\";

/**
 * @title SafeERC20
 * @dev Wrappers around ERC20 operations that throw on failure (when the token
 * contract returns false). Tokens that return no value (and instead revert or
 * throw on failure) are also supported, non-reverting calls are assumed to be
 * successful.
 * To use this library you can add a `using SafeERC20 for IERC20;` statement to your contract,
 * which allows you to call the safe operations as `token.safeTransfer(...)`, etc.
 */
library SafeERC20 {
    using SafeMath for uint256;
    using Address for address;

    function safeTransfer(IERC20 token, address to, uint256 value) internal {
        _callOptionalReturn(token, abi.encodeWithSelector(token.transfer.selector, to, value));
    }

    function safeTransferFrom(IERC20 token, address from, address to, uint256 value) internal {
        _callOptionalReturn(token, abi.encodeWithSelector(token.transferFrom.selector, from, to, value));
    }

    /**
     * @dev Deprecated. This function has issues similar to the ones found in
     * {IERC20-approve}, and its usage is discouraged.
     *
     * Whenever possible, use {safeIncreaseAllowance} and
     * {safeDecreaseAllowance} instead.
     */
    function safeApprove(IERC20 token, address spender, uint256 value) internal {
        // safeApprove should only be called when setting an initial allowance,
        // or when resetting it to zero. To increase and decrease it, use
        // \u0027safeIncreaseAllowance\u0027 and \u0027safeDecreaseAllowance\u0027
        // solhint-disable-next-line max-line-length
        require((value == 0) || (token.allowance(address(this), spender) == 0),
            \"SafeERC20: approve from non-zero to non-zero allowance\"
        );
        _callOptionalReturn(token, abi.encodeWithSelector(token.approve.selector, spender, value));
    }

    function safeIncreaseAllowance(IERC20 token, address spender, uint256 value) internal {
        uint256 newAllowance = token.allowance(address(this), spender).add(value);
        _callOptionalReturn(token, abi.encodeWithSelector(token.approve.selector, spender, newAllowance));
    }

    function safeDecreaseAllowance(IERC20 token, address spender, uint256 value) internal {
        uint256 newAllowance = token.allowance(address(this), spender).sub(value, \"SafeERC20: decreased allowance below zero\");
        _callOptionalReturn(token, abi.encodeWithSelector(token.approve.selector, spender, newAllowance));
    }

    /**
     * @dev Imitates a Solidity high-level call (i.e. a regular function call to a contract), relaxing the requirement
     * on the return value: the return value is optional (but if data is returned, it must not be false).
     * @param token The token targeted by the call.
     * @param data The call data (encoded using abi.encode or one of its variants).
     */
    function _callOptionalReturn(IERC20 token, bytes memory data) private {
        // We need to perform a low level call here, to bypass Solidity\u0027s return data size checking mechanism, since
        // we\u0027re implementing it ourselves. We use {Address.functionCall} to perform this call, which verifies that
        // the target address contains contract code and also asserts for success in the low-level call.

        bytes memory returndata = address(token).functionCall(data, \"SafeERC20: low-level call failed\");
        if (returndata.length \u003e 0) { // Return data is optional
            // solhint-disable-next-line max-line-length
            require(abi.decode(returndata, (bool)), \"SafeERC20: ERC20 operation did not succeed\");
        }
    }
}
"},"SafeMath.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.7.0;

/**
 * @dev Wrappers over Solidity\u0027s arithmetic operations with added overflow
 * checks.
 *
 * Arithmetic operations in Solidity wrap on overflow. This can easily result
 * in bugs, because programmers usually assume that an overflow raises an
 * error, which is the standard behavior in high level programming languages.
 * `SafeMath` restores this intuition by reverting the transaction when an
 * operation overflows.
 *
 * Using this library instead of the unchecked operations eliminates an entire
 * class of bugs, so it\u0027s recommended to use it always.
 */
library SafeMath {
    /**
     * @dev Returns the addition of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity\u0027s `+` operator.
     *
     * Requirements:
     *
     * - Addition cannot overflow.
     */
    function add(uint256 a, uint256 b) internal pure returns (uint256) {
        uint256 c = a + b;
        require(c \u003e= a, \"SafeMath: addition overflow\");

        return c;
    }

    /**
     * @dev Returns the subtraction of two unsigned integers, reverting on
     * overflow (when the result is negative).
     *
     * Counterpart to Solidity\u0027s `-` operator.
     *
     * Requirements:
     *
     * - Subtraction cannot overflow.
     */
    function sub(uint256 a, uint256 b) internal pure returns (uint256) {
        return sub(a, b, \"SafeMath: subtraction overflow\");
    }

    /**
     * @dev Returns the subtraction of two unsigned integers, reverting with custom message on
     * overflow (when the result is negative).
     *
     * Counterpart to Solidity\u0027s `-` operator.
     *
     * Requirements:
     *
     * - Subtraction cannot overflow.
     */
    function sub(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b \u003c= a, errorMessage);
        uint256 c = a - b;

        return c;
    }

    /**
     * @dev Returns the multiplication of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity\u0027s `*` operator.
     *
     * Requirements:
     *
     * - Multiplication cannot overflow.
     */
    function mul(uint256 a, uint256 b) internal pure returns (uint256) {
        // Gas optimization: this is cheaper than requiring \u0027a\u0027 not being zero, but the
        // benefit is lost if \u0027b\u0027 is also tested.
        // See: https://github.com/OpenZeppelin/openzeppelin-contracts/pull/522
        if (a == 0) {
            return 0;
        }

        uint256 c = a * b;
        require(c / a == b, \"SafeMath: multiplication overflow\");

        return c;
    }

    /**
     * @dev Returns the integer division of two unsigned integers. Reverts on
     * division by zero. The result is rounded towards zero.
     *
     * Counterpart to Solidity\u0027s `/` operator. Note: this function uses a
     * `revert` opcode (which leaves remaining gas untouched) while Solidity
     * uses an invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function div(uint256 a, uint256 b) internal pure returns (uint256) {
        return div(a, b, \"SafeMath: division by zero\");
    }

    /**
     * @dev Returns the integer division of two unsigned integers. Reverts with custom message on
     * division by zero. The result is rounded towards zero.
     *
     * Counterpart to Solidity\u0027s `/` operator. Note: this function uses a
     * `revert` opcode (which leaves remaining gas untouched) while Solidity
     * uses an invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function div(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b \u003e 0, errorMessage);
        uint256 c = a / b;
        // assert(a == b * c + a % b); // There is no case in which this doesn\u0027t hold

        return c;
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
     * Reverts when dividing by zero.
     *
     * Counterpart to Solidity\u0027s `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function mod(uint256 a, uint256 b) internal pure returns (uint256) {
        return mod(a, b, \"SafeMath: modulo by zero\");
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
     * Reverts with custom message when dividing by zero.
     *
     * Counterpart to Solidity\u0027s `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function mod(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b != 0, errorMessage);
        return a % b;
    }
}
"},"Secondary.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.7.0;

import \"./Context.sol\";
/**
 * @dev A Secondary contract can only be used by its primary account (the one that created it).
 */
abstract contract Secondary is Context {
    address private _primary;

    /**
     * @dev Emitted when the primary contract changes.
     */
    event PrimaryTransferred(
        address recipient
    );

    /**
     * @dev Sets the primary account to the one that is creating the Secondary contract.
     */
    constructor () {
        address msgSender = _msgSender();
        _primary = msgSender;
        emit PrimaryTransferred(msgSender);
    }

    /**
     * @dev Reverts if called from any account other than the primary.
     */
    modifier onlyPrimary() {
        require(_msgSender() == _primary, \"Secondary: caller is not the primary account\");
        _;
    }

    /**
     * @return the address of the primary.
     */
    function primary() public view returns (address) {
        return _primary;
    }

    /**
     * @dev Transfers contract to a new primary.
     * @param recipient The address of new primary.
     */
    function transferPrimary(address recipient) public onlyPrimary {
        require(recipient != address(0), \"Secondary: new primary is the zero address\");
        _primary = recipient;
        emit PrimaryTransferred(recipient);
    }
}"},"TimedCrowdsale.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.7.0;

import \"./SafeMath.sol\";
import \"./CrowdsaleMint.sol\";

/**
  * Based on OpenZeppelin\u0027s TimedCrowdsale legacy contract
 */
abstract contract TimedCrowdsale is CrowdsaleMint {
    using SafeMath for uint256;

    uint256 private _openingTime;
    uint256 private _closingTime;

    /**
     * Event for crowdsale extending
     * @param newClosingTime new closing time
     * @param prevClosingTime old closing time
     */
    event TimedCrowdsaleExtended(uint256 prevClosingTime, uint256 newClosingTime);

    /**
     * @dev Reverts if not in crowdsale time range.
     */
    modifier onlyWhileOpen {
        require(isOpen(), \"TimedCrowdsale: not open\");
        _;
    }

    /**
     * @dev Constructor, takes crowdsale opening and closing times.
     * @param openingTimeReceived Crowdsale opening time
     * @param closingTimeReceived Crowdsale closing time
     */
    constructor (uint256 openingTimeReceived, uint256 closingTimeReceived) {
        // solhint-disable-next-line not-rely-on-time
        require(openingTimeReceived \u003e= block.timestamp, \"TimedCrowdsale: opening time is before current time\");
        // solhint-disable-next-line max-line-length
        require(closingTimeReceived \u003e openingTimeReceived, \"TimedCrowdsale: opening time is not before closing time\");

        _openingTime = openingTimeReceived;
        _closingTime = closingTimeReceived;
    }

    /**
     * @return the crowdsale opening time.
     */
    function openingTime() public view returns (uint256) {
        return _openingTime;
    }

    /**
     * @return the crowdsale closing time.
     */
    function closingTime() public view returns (uint256) {
        return _closingTime;
    }

    /**
     * @return true if the crowdsale is open, false otherwise.
     */
    function isOpen() public view returns (bool) {
        // solhint-disable-next-line not-rely-on-time
        return block.timestamp \u003e= _openingTime \u0026\u0026 block.timestamp \u003c= _closingTime;
    }

    /**
     * @dev Checks whether the period in which the crowdsale is open has already elapsed.
     * @return Whether crowdsale period has elapsed
     */
    function hasClosed() public view virtual returns (bool) {
        // solhint-disable-next-line not-rely-on-time
        return block.timestamp \u003e _closingTime;
    }

    /**
     * @dev Extend parent behavior requiring to be within contributing period.
     * @param beneficiary Token purchaser
     * @param weiAmount Amount of wei contributed
     */
    function _preValidatePurchase(address beneficiary, uint256 weiAmount) internal override virtual onlyWhileOpen view {
        super._preValidatePurchase(beneficiary, weiAmount);
    }

    /**
     * @dev Extend crowdsale.
     * @param newClosingTime Crowdsale closing time
     */
    function _extendTime(uint256 newClosingTime) internal {
        require(!hasClosed(), \"TimedCrowdsale: already closed\");
        // solhint-disable-next-line max-line-length
        require(newClosingTime \u003e _closingTime, \"TimedCrowdsale: new closing time is before current closing time\");

        emit TimedCrowdsaleExtended(_closingTime, newClosingTime);
        _closingTime = newClosingTime;
    }
}
