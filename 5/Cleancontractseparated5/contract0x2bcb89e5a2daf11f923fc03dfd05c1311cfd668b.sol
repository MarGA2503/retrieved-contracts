// SPDX-License-Identifier: MIT
pragma solidity \u003e=0.6.0;

interface AggregatorV3Interface {

  function decimals() external view returns (uint8);
  function description() external view returns (string memory);
  function version() external view returns (uint256);

  // getRoundData and latestRoundData should both raise \"No data present\"
  // if they do not have data to report, instead of returning unset values
  // which could be misinterpreted as actual reported values.
  function getRoundData(uint80 _roundId)
    external
    view
    returns (
      uint80 roundId,
      int256 answer,
      uint256 startedAt,
      uint256 updatedAt,
      uint80 answeredInRound
    );
  function latestRoundData()
    external
    view
    returns (
      uint80 roundId,
      int256 answer,
      uint256 startedAt,
      uint256 updatedAt,
      uint80 answeredInRound
    );

}
"},"BuyROTHS.sol":{"content":"// SPDX-License-Identifier: MIT
pragma solidity ^0.6.12;

import \"./Context.sol\";
import \"./IERC20.sol\";
import \"./SafeMath.sol\";

import \"./AggregatorV3Interface.sol\";

interface Oracle{
    function getPrice() external view returns(uint256);
}


contract buying is Context{
    using SafeMath for uint256;
    
    AggregatorV3Interface internal _priceETH;
    AggregatorV3Interface internal _priceLINK;
    
    address public _USDT=0xdAC17F958D2ee523a2206206994597C13D831ec7;
    address public _USDC=0xA0b86991c6218b36c1d19D4a2e9Eb0cE3606eB48;
    address public _LINK=0x514910771AF9Ca656af840dff83E8264EcF986CA;
    address public _ROTS;
    address payable public _admin;
    uint256 _balance=105e23;
    bool public _started;
    uint256 public _startTime;
    address public oracle;
    
    constructor(address _rotsAddress) public {
        _ROTS=_rotsAddress;
        _admin=msg.sender;
        _priceETH = AggregatorV3Interface(0x5f4eC3Df9cbd43714FE2740f5E3616155c5b8419);
        _priceLINK = AggregatorV3Interface(0x2c1d072e956AFFC0D435Cb7AC38EF18d24d9127c);
    }
    
    
    
    function remainingROTS() public view returns (uint256) {
        return _balance;
    }
    
    function getUSDPrice(uint256 _amount) public view returns(uint256){
        return _amount.mul(_price()).div(10e18);
    }
    
    function getLINKPrice(uint256 _amount) public view returns(uint256){
        return _amount.mul(_price()).mul(getLatestPrice(_priceLINK)).div(10e11);
    }
    
    function getETHPrice(uint256 _amount) public view returns(uint256){
        return _amount.mul(_price()).mul(getLatestPrice(_priceETH)).div(10e11);
    }
    
    
    function setOracle(address _oracle) public virtual returns (bool){
        require(_msgSender()==_admin,\"You don\u0027t have permissions to perfrom the selected task.\");
        oracle=_oracle;
    }
    
    function start() public virtual returns (bool){
        require(_msgSender()==_admin,\"You don\u0027t have permissions to perfrom the selected task.\");
        require(_started==false,\"Already started.\");
        _started=true;
        _startTime=now;
    }
     
    function buyUSDT(uint256 _amount) public virtual returns(bool){
        require(_started==true,\"Buying not yet started.\");
        uint256 _rotsAmount=_amount.mul(_price()).div(10e18);
        _buy(_USDT,_amount,_rotsAmount);
        return true;
    }
    
    function buyUSDC(uint256 _amount) public virtual returns(bool){
        require(_started==true,\"Buying not yet started.\");
        uint256 _rotsAmount=_amount.mul(_price()).div(10e18);
        _buy(_USDC,_amount,_rotsAmount);
        return true;
    }
    
    function buyLINK(uint256 _amount) public virtual returns(bool){
        require(_started==true,\"Buying not yet started.\");
        uint256 _rotsAmount=_amount.mul(_price()).mul(getLatestPrice(_priceLINK)).div(10e11);
        _buy(_LINK,_amount,_rotsAmount);
        return true;
    }
    
    function buyETH() public payable returns(bool){
        require(_started==true,\"Buying not yet started.\");
        uint256 _amount=msg.value;
        _admin.transfer(_amount);
        uint256 _rotsAmount=_amount.mul(_price()).mul(getLatestPrice(_priceETH)).div(10e11);
        require(_rotsAmount\u003c=_balance,\"Not enough ROTS in contract.\");
        IERC20(_ROTS).transfer(_msgSender(),_rotsAmount);
        _balance=_balance.sub(_rotsAmount);
        return true;
        
    }
    
    function _buy(address _tokenAddress,uint256 _amount,uint256 _rotsAmount) internal virtual{
        IERC20(_tokenAddress).transferFrom(_msgSender(),_admin,_amount);
        require(_rotsAmount\u003c=_balance,\"Not enough ROTS in contract.\");
        IERC20(_ROTS).transfer(_msgSender(),_rotsAmount);
        _balance=_balance.sub(_rotsAmount);
    }
    
    function _price() public view returns(uint256){
        uint256 price_;
        if (oracle!=address(0)){
            price_=Oracle(oracle).getPrice();
        }
        else {
            price_=1e19;
        }
        
        
        return price_;
    }
    
    function getLatestPrice(AggregatorV3Interface priceFeed) internal view returns (uint256) {
        (,int price,,,) = priceFeed.latestRoundData();
        return uint256(price);
    }
}"},"Context.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.6.12;

/*
 * @dev Provides information about the current execution context, including the
 * sender of the transaction and its data. While these are generally available
 * via msg.sender and msg.data, they should not be accessed in such a direct
 * manner, since when dealing with meta-transactions the account sending and
 * paying for execution may not be the actual sender (as far as an application
 * is concerned).
 *
 * This contract is only required for intermediate, library-like contracts.
 */
abstract contract Context {
    function _msgSender() internal view virtual returns (address) {
        return msg.sender;
    }

    function _msgData() internal view virtual returns (bytes calldata) {
        this; // silence state mutability warning without generating bytecode - see https://github.com/ethereum/solidity/issues/2691
        return msg.data;
    }
}
"},"IERC20.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.6.12;

/**
 * @dev Interface of the ERC20 standard as defined in the EIP.
 */
interface IERC20 {
    /**
     * @dev Returns the amount of tokens in existence.
     */
    function totalSupply() external view returns (uint256);

    /**
     * @dev Returns the amount of tokens owned by `account`.
     */
    function balanceOf(address account) external view returns (uint256);

    /**
     * @dev Moves `amount` tokens from the caller\u0027s account to `recipient`.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transfer(address recipient, uint256 amount) external returns (bool);

    /**
     * @dev Returns the remaining number of tokens that `spender` will be
     * allowed to spend on behalf of `owner` through {transferFrom}. This is
     * zero by default.
     *
     * This value changes when {approve} or {transferFrom} are called.
     */
    function allowance(address owner, address spender) external view returns (uint256);

    /**
     * @dev Sets `amount` as the allowance of `spender` over the caller\u0027s tokens.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * IMPORTANT: Beware that changing an allowance with this method brings the risk
     * that someone may use both the old and the new allowance by unfortunate
     * transaction ordering. One possible solution to mitigate this race
     * condition is to first reduce the spender\u0027s allowance to 0 and set the
     * desired value afterwards:
     * https://github.com/ethereum/EIPs/issues/20#issuecomment-263524729
     *
     * Emits an {Approval} event.
     */
    function approve(address spender, uint256 amount) external returns (bool);

    /**
     * @dev Moves `amount` tokens from `sender` to `recipient` using the
     * allowance mechanism. `amount` is then deducted from the caller\u0027s
     * allowance.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transferFrom(address sender, address recipient, uint256 amount) external returns (bool);

    /**
     * @dev Emitted when `value` tokens are moved from one account (`from`) to
     * another (`to`).
     *
     * Note that `value` may be zero.
     */
    event Transfer(address indexed from, address indexed to, uint256 value);

    /**
     * @dev Emitted when the allowance of a `spender` for an `owner` is set by
     * a call to {approve}. `value` is the new allowance.
     */
    event Approval(address indexed owner, address indexed spender, uint256 value);
}
"},"SafeMath.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity \u003e=0.6.0 \u003c0.8.0;

/**
 * @dev Wrappers over Solidity\u0027s arithmetic operations with added overflow
 * checks.
 *
 * Arithmetic operations in Solidity wrap on overflow. This can easily result
 * in bugs, because programmers usually assume that an overflow raises an
 * error, which is the standard behavior in high level programming languages.
 * `SafeMath` restores this intuition by reverting the transaction when an
 * operation overflows.
 *
 * Using this library instead of the unchecked operations eliminates an entire
 * class of bugs, so it\u0027s recommended to use it always.
 */
library SafeMath {
    /**
     * @dev Returns the addition of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity\u0027s `+` operator.
     *
     * Requirements:
     *
     * - Addition cannot overflow.
     */
    function add(uint256 a, uint256 b) internal pure returns (uint256) {
        uint256 c = a + b;
        require(c \u003e= a, \"SafeMath: addition overflow\");

        return c;
    }

    /**
     * @dev Returns the subtraction of two unsigned integers, reverting on
     * overflow (when the result is negative).
     *
     * Counterpart to Solidity\u0027s `-` operator.
     *
     * Requirements:
     *
     * - Subtraction cannot overflow.
     */
    function sub(uint256 a, uint256 b) internal pure returns (uint256) {
        return sub(a, b, \"SafeMath: subtraction overflow\");
    }

    /**
     * @dev Returns the subtraction of two unsigned integers, reverting with custom message on
     * overflow (when the result is negative).
     *
     * Counterpart to Solidity\u0027s `-` operator.
     *
     * Requirements:
     *
     * - Subtraction cannot overflow.
     */
    function sub(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b \u003c= a, errorMessage);
        uint256 c = a - b;

        return c;
    }

    /**
     * @dev Returns the multiplication of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity\u0027s `*` operator.
     *
     * Requirements:
     *
     * - Multiplication cannot overflow.
     */
    function mul(uint256 a, uint256 b) internal pure returns (uint256) {
        // Gas optimization: this is cheaper than requiring \u0027a\u0027 not being zero, but the
        // benefit is lost if \u0027b\u0027 is also tested.
        // See: https://github.com/OpenZeppelin/openzeppelin-contracts/pull/522
        if (a == 0) {
            return 0;
        }

        uint256 c = a * b;
        require(c / a == b, \"SafeMath: multiplication overflow\");

        return c;
    }

    /**
     * @dev Returns the integer division of two unsigned integers. Reverts on
     * division by zero. The result is rounded towards zero.
     *
     * Counterpart to Solidity\u0027s `/` operator. Note: this function uses a
     * `revert` opcode (which leaves remaining gas untouched) while Solidity
     * uses an invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function div(uint256 a, uint256 b) internal pure returns (uint256) {
        return div(a, b, \"SafeMath: division by zero\");
    }

    /**
     * @dev Returns the integer division of two unsigned integers. Reverts with custom message on
     * division by zero. The result is rounded towards zero.
     *
     * Counterpart to Solidity\u0027s `/` operator. Note: this function uses a
     * `revert` opcode (which leaves remaining gas untouched) while Solidity
     * uses an invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function div(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b \u003e 0, errorMessage);
        uint256 c = a / b;
        // assert(a == b * c + a % b); // There is no case in which this doesn\u0027t hold

        return c;
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
     * Reverts when dividing by zero.
     *
     * Counterpart to Solidity\u0027s `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function mod(uint256 a, uint256 b) internal pure returns (uint256) {
        return mod(a, b, \"SafeMath: modulo by zero\");
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
     * Reverts with custom message when dividing by zero.
     *
     * Counterpart to Solidity\u0027s `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function mod(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b != 0, errorMessage);
        return a % b;
    }
}
