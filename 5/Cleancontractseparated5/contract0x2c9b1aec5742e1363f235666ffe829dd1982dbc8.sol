// SPDX-License-Identifier: MIT

pragma solidity ^0.6.0;

/*
 * @dev Provides information about the current execution context, including the
 * sender of the transaction and its data. While these are generally available
 * via msg.sender and msg.data, they should not be accessed in such a direct
 * manner, since when dealing with GSN meta-transactions the account sending and
 * paying for execution may not be the actual sender (as far as an application
 * is concerned).
 *
 * This contract is only required for intermediate, library-like contracts.
 */
abstract contract Context {
    function _msgSender() internal view virtual returns (address payable) {
        return msg.sender;
    }

    function _msgData() internal view virtual returns (bytes memory) {
        this; // silence state mutability warning without generating bytecode - see https://github.com/ethereum/solidity/issues/2691
        return msg.data;
    }
}
"},"ContractRegistryAccessor.sol":{"content":"// SPDX-License-Identifier: UNLICENSED

pragma solidity 0.6.12;

import \"./IContractRegistry.sol\";
import \"./WithClaimableRegistryManagement.sol\";
import \"./Initializable.sol\";

contract ContractRegistryAccessor is WithClaimableRegistryManagement, Initializable {

    IContractRegistry private contractRegistry;

    constructor(IContractRegistry _contractRegistry, address _registryAdmin) public {
        require(address(_contractRegistry) != address(0), \"_contractRegistry cannot be 0\");
        setContractRegistry(_contractRegistry);
        _transferRegistryManagement(_registryAdmin);
    }

    modifier onlyAdmin {
        require(isAdmin(), \"sender is not an admin (registryManger or initializationAdmin)\");

        _;
    }

    function isManager(string memory role) internal view returns (bool) {
        IContractRegistry _contractRegistry = contractRegistry;
        return isAdmin() || _contractRegistry != IContractRegistry(0) \u0026\u0026 contractRegistry.getManager(role) == msg.sender;
    }

    function isAdmin() internal view returns (bool) {
        return msg.sender == registryAdmin() || msg.sender == initializationAdmin() || msg.sender == address(contractRegistry);
    }

    function getProtocolContract() internal view returns (address) {
        return contractRegistry.getContract(\"protocol\");
    }

    function getStakingRewardsContract() internal view returns (address) {
        return contractRegistry.getContract(\"stakingRewards\");
    }

    function getFeesAndBootstrapRewardsContract() internal view returns (address) {
        return contractRegistry.getContract(\"feesAndBootstrapRewards\");
    }

    function getCommitteeContract() internal view returns (address) {
        return contractRegistry.getContract(\"committee\");
    }

    function getElectionsContract() internal view returns (address) {
        return contractRegistry.getContract(\"elections\");
    }

    function getDelegationsContract() internal view returns (address) {
        return contractRegistry.getContract(\"delegations\");
    }

    function getGuardiansRegistrationContract() internal view returns (address) {
        return contractRegistry.getContract(\"guardiansRegistration\");
    }

    function getCertificationContract() internal view returns (address) {
        return contractRegistry.getContract(\"certification\");
    }

    function getStakingContract() internal view returns (address) {
        return contractRegistry.getContract(\"staking\");
    }

    function getSubscriptionsContract() internal view returns (address) {
        return contractRegistry.getContract(\"subscriptions\");
    }

    function getStakingRewardsWallet() internal view returns (address) {
        return contractRegistry.getContract(\"stakingRewardsWallet\");
    }

    function getBootstrapRewardsWallet() internal view returns (address) {
        return contractRegistry.getContract(\"bootstrapRewardsWallet\");
    }

    function getGeneralFeesWallet() internal view returns (address) {
        return contractRegistry.getContract(\"generalFeesWallet\");
    }

    function getCertifiedFeesWallet() internal view returns (address) {
        return contractRegistry.getContract(\"certifiedFeesWallet\");
    }

    function getStakingContractHandler() internal view returns (address) {
        return contractRegistry.getContract(\"stakingContractHandler\");
    }

    /*
    * Governance functions
    */

    event ContractRegistryAddressUpdated(address addr);

    function setContractRegistry(IContractRegistry newContractRegistry) public onlyAdmin {
        require(newContractRegistry.getPreviousContractRegistry() == address(contractRegistry), \"new contract registry must provide the previous contract registry\");
        contractRegistry = newContractRegistry;
        emit ContractRegistryAddressUpdated(address(newContractRegistry));
    }

    function getContractRegistry() public view returns (IContractRegistry) {
        return contractRegistry;
    }

}
"},"Elections.sol":{"content":"// SPDX-License-Identifier: UNLICENSED

pragma solidity 0.6.12;

import \"./SafeMath.sol\";

import \"./IElections.sol\";
import \"./IDelegation.sol\";
import \"./IGuardiansRegistration.sol\";
import \"./ICommittee.sol\";
import \"./ICertification.sol\";
import \"./ManagedContract.sol\";

contract Elections is IElections, ManagedContract {
\tusing SafeMath for uint256;

\tuint32 constant PERCENT_MILLIE_BASE = 100000;

\tmapping(address =\u003e mapping(address =\u003e uint256)) voteUnreadyVotes; // by =\u003e to =\u003e expiration
\tmapping(address =\u003e uint256) public votersStake;
\tmapping(address =\u003e address) voteOutVotes; // by =\u003e to
\tmapping(address =\u003e uint256) accumulatedStakesForVoteOut; // addr =\u003e total stake
\tmapping(address =\u003e bool) votedOutGuardians;

\tstruct Settings {
\t\tuint32 minSelfStakePercentMille;
\t\tuint32 voteUnreadyPercentMilleThreshold;
\t\tuint32 voteOutPercentMilleThreshold;
\t}
\tSettings settings;

\tconstructor(IContractRegistry _contractRegistry, address _registryAdmin, uint32 minSelfStakePercentMille, uint32 voteUnreadyPercentMilleThreshold, uint32 voteOutPercentMilleThreshold) ManagedContract(_contractRegistry, _registryAdmin) public {
\t\tsetMinSelfStakePercentMille(minSelfStakePercentMille);
\t\tsetVoteOutPercentMilleThreshold(voteOutPercentMilleThreshold);
\t\tsetVoteUnreadyPercentMilleThreshold(voteUnreadyPercentMilleThreshold);
\t}

\tmodifier onlyDelegationsContract() {
\t\trequire(msg.sender == address(delegationsContract), \"caller is not the delegations contract\");

\t\t_;
\t}

\tmodifier onlyGuardiansRegistrationContract() {
\t\trequire(msg.sender == address(guardianRegistrationContract), \"caller is not the guardian registrations contract\");

\t\t_;
\t}

\t/*
\t * External functions
\t */

\tfunction readyToSync() external override onlyWhenActive {
\t\taddress guardian = guardianRegistrationContract.resolveGuardianAddress(msg.sender); // this validates registration
\t\trequire(!isVotedOut(guardian), \"caller is voted-out\");

\t\temit GuardianStatusUpdated(guardian, true, false);

\t\tcommitteeContract.removeMember(guardian);
\t}

\tfunction readyForCommittee() external override onlyWhenActive {
\t\t_readyForCommittee(msg.sender);
\t}

\tfunction canJoinCommittee(address guardian) external view override returns (bool) {
\t\tguardian = guardianRegistrationContract.resolveGuardianAddress(guardian); // this validates registration

\t\tif (isVotedOut(guardian)) {
\t\t\treturn false;
\t\t}

\t\t(, uint256 effectiveStake, ) = getGuardianStakeInfo(guardian, settings);
\t\treturn committeeContract.checkAddMember(guardian, effectiveStake);
\t}

\tfunction getEffectiveStake(address guardian) external override view returns (uint effectiveStake) {
\t\t(, effectiveStake, ) = getGuardianStakeInfo(guardian, settings);
\t}

\t/// @dev returns the current committee
\tfunction getCommittee() external override view returns (address[] memory committee, uint256[] memory weights, address[] memory orbsAddrs, bool[] memory certification, bytes4[] memory ips) {
\t\tIGuardiansRegistration _guardianRegistrationContract = guardianRegistrationContract;
\t\t(committee, weights, certification) = committeeContract.getCommittee();
\t\torbsAddrs = _guardianRegistrationContract.getGuardiansOrbsAddress(committee);
\t\tips = _guardianRegistrationContract.getGuardianIps(committee);
\t}

\t// Vote-unready

\tfunction voteUnready(address subject, uint voteExpiration) external override onlyWhenActive {
\t\trequire(voteExpiration \u003e= block.timestamp, \"vote expiration time must not be in the past\");

\t\taddress voter = guardianRegistrationContract.resolveGuardianAddress(msg.sender);
\t\tvoteUnreadyVotes[voter][subject] = voteExpiration;
\t\temit VoteUnreadyCasted(voter, subject, voteExpiration);

\t\t(address[] memory generalCommittee, uint256[] memory generalWeights, bool[] memory certification) = committeeContract.getCommittee();

\t\tbool votedUnready = isCommitteeVoteUnreadyThresholdReached(generalCommittee, generalWeights, certification, subject);
\t\tif (votedUnready) {
\t\t\tclearCommitteeUnreadyVotes(generalCommittee, subject);
\t\t\temit GuardianVotedUnready(subject);

\t\t\temit GuardianStatusUpdated(subject, false, false);
\t\t\tcommitteeContract.removeMember(subject);
\t\t}
\t}

\tfunction getVoteUnreadyVote(address voter, address subject) public override view returns (bool valid, uint256 expiration) {
\t\texpiration = voteUnreadyVotes[voter][subject];
\t\tvalid = expiration != 0 \u0026\u0026 block.timestamp \u003c expiration;
\t}

\tfunction getVoteUnreadyStatus(address subject) external override view returns (address[] memory committee, uint256[] memory weights, bool[] memory certification, bool[] memory votes, bool subjectInCommittee, bool subjectInCertifiedCommittee) {
\t\t(committee, weights, certification) = committeeContract.getCommittee();

\t\tvotes = new bool[](committee.length);
\t\tfor (uint i = 0; i \u003c committee.length; i++) {
\t\t\taddress memberAddr = committee[i];
\t\t\tif (block.timestamp \u003c voteUnreadyVotes[memberAddr][subject]) {
\t\t\t\tvotes[i] = true;
\t\t\t}

\t\t\tif (memberAddr == subject) {
\t\t\t\tsubjectInCommittee = true;
\t\t\t\tsubjectInCertifiedCommittee = certification[i];
\t\t\t}
\t\t}
\t}

\t// Vote-out

\tfunction voteOut(address subject) external override onlyWhenActive {
\t\tSettings memory _settings = settings;

\t\taddress voter = msg.sender;
\t\taddress prevSubject = voteOutVotes[voter];

\t\tvoteOutVotes[voter] = subject;
\t\temit VoteOutCasted(voter, subject);

\t\tuint256 voterStake = delegationsContract.getDelegatedStake(voter);

\t\tif (prevSubject == address(0)) {
\t\t\tvotersStake[voter] = voterStake;
\t\t}

\t\tif (subject == address(0)) {
\t\t\tdelete votersStake[voter];
\t\t}

\t\tuint totalStake = delegationsContract.getTotalDelegatedStake();

\t\tif (prevSubject != address(0) \u0026\u0026 prevSubject != subject) {
\t\t\tapplyVoteOutVotesFor(prevSubject, 0, voterStake, totalStake, _settings);
\t\t}

\t\tif (subject != address(0)) {
\t\t\tuint voteStakeAdded = prevSubject != subject ? voterStake : 0;
\t\t\tapplyVoteOutVotesFor(subject, voteStakeAdded, 0, totalStake, _settings); // recheck also if not new
\t\t}
\t}

\tfunction getVoteOutVote(address voter) external override view returns (address) {
\t\treturn voteOutVotes[voter];
\t}

\tfunction getVoteOutStatus(address subject) external override view returns (bool votedOut, uint votedStake, uint totalDelegatedStake) {
\t\tvotedOut = isVotedOut(subject);
\t\tvotedStake = accumulatedStakesForVoteOut[subject];
\t\ttotalDelegatedStake = delegationsContract.getTotalDelegatedStake();
\t}

\t/*
\t * Notification functions from other PoS contracts
\t */

\tfunction delegatedStakeChange(address delegate, uint256 selfStake, uint256 delegatedStake, uint256 totalDelegatedStake) external override onlyDelegationsContract onlyWhenActive {
\t\tSettings memory _settings = settings;

\t\tuint effectiveStake = calcEffectiveStake(selfStake, delegatedStake, _settings);
\t\temit StakeChanged(delegate, selfStake, delegatedStake, effectiveStake);

\t\tcommitteeContract.memberWeightChange(delegate, effectiveStake);

\t\tapplyStakesToVoteOutBy(delegate, delegatedStake, totalDelegatedStake, _settings);
\t}

\t/// @dev Called by: guardian registration contract
\t/// Notifies a new guardian was unregistered
\tfunction guardianUnregistered(address guardian) external override onlyGuardiansRegistrationContract onlyWhenActive {
\t\temit GuardianStatusUpdated(guardian, false, false);
\t\tcommitteeContract.removeMember(guardian);
\t}

\t/// @dev Called by: guardian registration contract
\t/// Notifies on a guardian certification change
\tfunction guardianCertificationChanged(address guardian, bool isCertified) external override onlyWhenActive {
\t\tcommitteeContract.memberCertificationChange(guardian, isCertified);
\t}

\t/*
     * Governance functions
\t */

\tfunction setMinSelfStakePercentMille(uint32 minSelfStakePercentMille) public override onlyFunctionalManager {
\t\trequire(minSelfStakePercentMille \u003c= PERCENT_MILLIE_BASE, \"minSelfStakePercentMille must be 100000 at most\");
\t\temit MinSelfStakePercentMilleChanged(minSelfStakePercentMille, settings.minSelfStakePercentMille);
\t\tsettings.minSelfStakePercentMille = minSelfStakePercentMille;
\t}

\tfunction getMinSelfStakePercentMille() external override view returns (uint32) {
\t\treturn settings.minSelfStakePercentMille;
\t}

\tfunction setVoteOutPercentMilleThreshold(uint32 voteOutPercentMilleThreshold) public override onlyFunctionalManager {
\t\trequire(voteOutPercentMilleThreshold \u003c= PERCENT_MILLIE_BASE, \"voteOutPercentMilleThreshold must not be larger than 100000\");
\t\temit VoteOutPercentMilleThresholdChanged(voteOutPercentMilleThreshold, settings.voteOutPercentMilleThreshold);
\t\tsettings.voteOutPercentMilleThreshold = voteOutPercentMilleThreshold;
\t}

\tfunction getVoteOutPercentMilleThreshold() external override view returns (uint32) {
\t\treturn settings.voteOutPercentMilleThreshold;
\t}

\tfunction setVoteUnreadyPercentMilleThreshold(uint32 voteUnreadyPercentMilleThreshold) public override onlyFunctionalManager {
\t\trequire(voteUnreadyPercentMilleThreshold \u003c= PERCENT_MILLIE_BASE, \"voteUnreadyPercentMilleThreshold must not be larger than 100000\");
\t\temit VoteUnreadyPercentMilleThresholdChanged(voteUnreadyPercentMilleThreshold, settings.voteUnreadyPercentMilleThreshold);
\t\tsettings.voteUnreadyPercentMilleThreshold = voteUnreadyPercentMilleThreshold;
\t}

\tfunction getVoteUnreadyPercentMilleThreshold() external override view returns (uint32) {
\t\treturn settings.voteUnreadyPercentMilleThreshold;
\t}

\tfunction getSettings() external override view returns (
\t\tuint32 minSelfStakePercentMille,
\t\tuint32 voteUnreadyPercentMilleThreshold,
\t\tuint32 voteOutPercentMilleThreshold
\t) {
\t\tSettings memory _settings = settings;
\t\tminSelfStakePercentMille = _settings.minSelfStakePercentMille;
\t\tvoteUnreadyPercentMilleThreshold = _settings.voteUnreadyPercentMilleThreshold;
\t\tvoteOutPercentMilleThreshold = _settings.voteOutPercentMilleThreshold;
\t}

\tfunction initReadyForCommittee(address[] calldata guardians) external override onlyInitializationAdmin {
\t\tfor (uint i = 0; i \u003c guardians.length; i++) {
\t\t\t_readyForCommittee(guardians[i]);
\t\t}
\t}

\t/*
     * Private functions
\t */

\tfunction _readyForCommittee(address guardian) private {
\t\tguardian = guardianRegistrationContract.resolveGuardianAddress(guardian); // this validates registration
\t\trequire(!isVotedOut(guardian), \"caller is voted-out\");

\t\temit GuardianStatusUpdated(guardian, true, true);

\t\t(, uint256 effectiveStake, ) = getGuardianStakeInfo(guardian, settings);
\t\tcommitteeContract.addMember(guardian, effectiveStake, certificationContract.isGuardianCertified(guardian));
\t}

\tfunction calcEffectiveStake(uint256 selfStake, uint256 delegatedStake, Settings memory _settings) private pure returns (uint256) {
\t\tif (selfStake.mul(PERCENT_MILLIE_BASE) \u003e= delegatedStake.mul(_settings.minSelfStakePercentMille)) {
\t\t\treturn delegatedStake;
\t\t}
\t\treturn selfStake.mul(PERCENT_MILLIE_BASE).div(_settings.minSelfStakePercentMille); // never overflows or divides by zero
\t}

\tfunction getGuardianStakeInfo(address guardian, Settings memory _settings) private view returns (uint256 selfStake, uint256 effectiveStake, uint256 delegatedStake) {
\t\tIDelegations _delegationsContract = delegationsContract;
\t\t(,selfStake) = _delegationsContract.getDelegationInfo(guardian);
\t\tdelegatedStake = _delegationsContract.getDelegatedStake(guardian);
\t\teffectiveStake = calcEffectiveStake(selfStake, delegatedStake, _settings);
\t}

\t// Vote-unready

\tfunction isCommitteeVoteUnreadyThresholdReached(address[] memory committee, uint256[] memory weights, bool[] memory certification, address subject) private returns (bool) {
\t\tSettings memory _settings = settings;

\t\tuint256 totalCommitteeStake = 0;
\t\tuint256 totalVoteUnreadyStake = 0;
\t\tuint256 totalCertifiedStake = 0;
\t\tuint256 totalCertifiedVoteUnreadyStake = 0;

\t\taddress member;
\t\tuint256 memberStake;
\t\tbool isSubjectCertified;
\t\tfor (uint i = 0; i \u003c committee.length; i++) {
\t\t\tmember = committee[i];
\t\t\tmemberStake = weights[i];

\t\t\tif (member == subject \u0026\u0026 certification[i]) {
\t\t\t\tisSubjectCertified = true;
\t\t\t}

\t\t\ttotalCommitteeStake = totalCommitteeStake.add(memberStake);
\t\t\tif (certification[i]) {
\t\t\t\ttotalCertifiedStake = totalCertifiedStake.add(memberStake);
\t\t\t}

\t\t\t(bool valid, uint256 expiration) = getVoteUnreadyVote(member, subject);
\t\t\tif (valid) {
\t\t\t\ttotalVoteUnreadyStake = totalVoteUnreadyStake.add(memberStake);
\t\t\t\tif (certification[i]) {
\t\t\t\t\ttotalCertifiedVoteUnreadyStake = totalCertifiedVoteUnreadyStake.add(memberStake);
\t\t\t\t}
\t\t\t} else if (expiration != 0) {
\t\t\t\t// Vote is stale, delete from state
\t\t\t\tdelete voteUnreadyVotes[member][subject];
\t\t\t}
\t\t}

\t\treturn (
\t\t\ttotalCommitteeStake \u003e 0 \u0026\u0026
\t\t\ttotalVoteUnreadyStake.mul(PERCENT_MILLIE_BASE) \u003e= uint256(_settings.voteUnreadyPercentMilleThreshold).mul(totalCommitteeStake)
\t\t) || (
\t\t\tisSubjectCertified \u0026\u0026
\t\t\ttotalCertifiedStake \u003e 0 \u0026\u0026
\t\t\ttotalCertifiedVoteUnreadyStake.mul(PERCENT_MILLIE_BASE) \u003e= uint256(_settings.voteUnreadyPercentMilleThreshold).mul(totalCertifiedStake)
\t\t);
\t}

\tfunction clearCommitteeUnreadyVotes(address[] memory committee, address subject) private {
\t\tfor (uint i = 0; i \u003c committee.length; i++) {
\t\t\tvoteUnreadyVotes[committee[i]][subject] = 0; // clear vote-outs
\t\t}
\t}

\t// Vote-out

\tfunction applyStakesToVoteOutBy(address voter, uint256 currentVoterStake, uint256 totalGovernanceStake, Settings memory _settings) private {
\t\taddress subject = voteOutVotes[voter];
\t\tif (subject == address(0)) return;

\t\tuint256 prevVoterStake = votersStake[voter];
\t\tvotersStake[voter] = currentVoterStake;

\t\tapplyVoteOutVotesFor(subject, currentVoterStake, prevVoterStake, totalGovernanceStake, _settings);
\t}

    function applyVoteOutVotesFor(address subject, uint256 voteOutStakeAdded, uint256 voteOutStakeRemoved, uint256 totalGovernanceStake, Settings memory _settings) private {
\t\tif (isVotedOut(subject)) {
\t\t\treturn;
\t\t}

\t\tuint256 accumulated = accumulatedStakesForVoteOut[subject].
\t\t\tsub(voteOutStakeRemoved).
\t\t\tadd(voteOutStakeAdded);

\t\tbool shouldBeVotedOut = totalGovernanceStake \u003e 0 \u0026\u0026 accumulated.mul(PERCENT_MILLIE_BASE) \u003e= uint256(_settings.voteOutPercentMilleThreshold).mul(totalGovernanceStake);
\t\tif (shouldBeVotedOut) {
\t\t\tvotedOutGuardians[subject] = true;
\t\t\temit GuardianVotedOut(subject);

\t\t\temit GuardianStatusUpdated(subject, false, false);
\t\t\tcommitteeContract.removeMember(subject);
\t\t}

\t\taccumulatedStakesForVoteOut[subject] = accumulated;
\t}

\tfunction isVotedOut(address guardian) private view returns (bool) {
\t\treturn votedOutGuardians[guardian];
\t}

\t/*
     * Contracts topology / registry interface
     */

\tICommittee committeeContract;
\tIDelegations delegationsContract;
\tIGuardiansRegistration guardianRegistrationContract;
\tICertification certificationContract;
\tfunction refreshContracts() external override {
\t\tcommitteeContract = ICommittee(getCommitteeContract());
\t\tdelegationsContract = IDelegations(getDelegationsContract());
\t\tguardianRegistrationContract = IGuardiansRegistration(getGuardiansRegistrationContract());
\t\tcertificationContract = ICertification(getCertificationContract());
\t}

}
"},"ICertification.sol":{"content":"// SPDX-License-Identifier: UNLICENSED

pragma solidity 0.6.12;

/// @title Elections contract interface
interface ICertification /* is Ownable */ {
\tevent GuardianCertificationUpdate(address indexed guardian, bool isCertified);

\t/*
     * External methods
     */

\t/// @dev Returns the certification status of a guardian
\tfunction isGuardianCertified(address guardian) external view returns (bool isCertified);

\t/// @dev Sets the guardian certification status
\tfunction setGuardianCertification(address guardian, bool isCertified) external /* Owner only */ ;
}
"},"ICommittee.sol":{"content":"// SPDX-License-Identifier: UNLICENSED

pragma solidity 0.6.12;

/// @title Committee contract interface
interface ICommittee {
\tevent CommitteeChange(address indexed addr, uint256 weight, bool certification, bool inCommittee);
\tevent CommitteeSnapshot(address[] addrs, uint256[] weights, bool[] certification);

\t// No external functions

\t/*
     * External functions
     */

\t/// @dev Called by: Elections contract
\t/// Notifies a weight change of certification change of a member
\tfunction memberWeightChange(address addr, uint256 weight) external /* onlyElectionsContract onlyWhenActive */;

\tfunction memberCertificationChange(address addr, bool isCertified) external /* onlyElectionsContract onlyWhenActive */;

\t/// @dev Called by: Elections contract
\t/// Notifies a a member removal for example due to voteOut / voteUnready
\tfunction removeMember(address addr) external returns (bool memberRemoved, uint removedMemberEffectiveStake, bool removedMemberCertified)/* onlyElectionContract */;

\t/// @dev Called by: Elections contract
\t/// Notifies a new member applicable for committee (due to registration, unbanning, certification change)
\tfunction addMember(address addr, uint256 weight, bool isCertified) external returns (bool memberAdded)  /* onlyElectionsContract */;

\t/// @dev Called by: Elections contract
\t/// Checks if addMember() would add a the member to the committee
\tfunction checkAddMember(address addr, uint256 weight) external view returns (bool wouldAddMember);

\t/// @dev Called by: Elections contract
\t/// Returns the committee members and their weights
\tfunction getCommittee() external view returns (address[] memory addrs, uint256[] memory weights, bool[] memory certification);

\tfunction getCommitteeStats() external view returns (uint generalCommitteeSize, uint certifiedCommitteeSize, uint totalStake);

\tfunction getMemberInfo(address addr) external view returns (bool inCommittee, uint weight, bool isCertified, uint totalCommitteeWeight);

\tfunction emitCommitteeSnapshot() external;

\t/*
\t * Governance functions
\t */

\tevent MaxCommitteeSizeChanged(uint8 newValue, uint8 oldValue);

\tfunction setMaxCommitteeSize(uint8 maxCommitteeSize) external /* onlyFunctionalManager onlyWhenActive */;

\tfunction getMaxCommitteeSize() external view returns (uint8);
}
"},"IContractRegistry.sol":{"content":"// SPDX-License-Identifier: UNLICENSED

pragma solidity 0.6.12;

interface IContractRegistry {

\tevent ContractAddressUpdated(string contractName, address addr, bool managedContract);
\tevent ManagerChanged(string role, address newManager);
\tevent ContractRegistryUpdated(address newContractRegistry);

\t/*
\t* External functions
\t*/

\t/// @dev updates the contracts address and emits a corresponding event
\t/// managedContract indicates whether the contract is managed by the registry and notified on changes
\tfunction setContract(string calldata contractName, address addr, bool managedContract) external /* onlyAdmin */;

\t/// @dev returns the current address of the given contracts
\tfunction getContract(string calldata contractName) external view returns (address);

\t/// @dev returns the list of contract addresses managed by the registry
\tfunction getManagedContracts() external view returns (address[] memory);

\tfunction setManager(string calldata role, address manager) external /* onlyAdmin */;

\tfunction getManager(string calldata role) external view returns (address);

\tfunction lockContracts() external /* onlyAdmin */;

\tfunction unlockContracts() external /* onlyAdmin */;

\tfunction setNewContractRegistry(IContractRegistry newRegistry) external /* onlyAdmin */;

\tfunction getPreviousContractRegistry() external view returns (address);

}
"},"IDelegation.sol":{"content":"// SPDX-License-Identifier: UNLICENSED

pragma solidity 0.6.12;

/// @title Delegations contract interface
interface IDelegations /* is IStakeChangeNotifier */ {

    // Delegation state change events
\tevent DelegatedStakeChanged(address indexed addr, uint256 selfDelegatedStake, uint256 delegatedStake, address indexed delegator, uint256 delegatorContributedStake);

    // Function calls
\tevent Delegated(address indexed from, address indexed to);

\t/*
     * External functions
     */

\t/// @dev Stake delegation
\tfunction delegate(address to) external /* onlyWhenActive */;

\tfunction refreshStake(address addr) external /* onlyWhenActive */;

\tfunction getDelegatedStake(address addr) external view returns (uint256);

\tfunction getDelegation(address addr) external view returns (address);

\tfunction getDelegationInfo(address addr) external view returns (address delegation, uint256 delegatorStake);

\tfunction getTotalDelegatedStake() external view returns (uint256) ;

\t/*
\t * Governance functions
\t */

\tevent DelegationsImported(address[] from, address indexed to);

\tevent DelegationInitialized(address indexed from, address indexed to);

\tfunction importDelegations(address[] calldata from, address to) external /* onlyMigrationManager onlyDuringDelegationImport */;

\tfunction initDelegation(address from, address to) external /* onlyInitializationAdmin */;
}
"},"IElections.sol":{"content":"// SPDX-License-Identifier: UNLICENSED

pragma solidity 0.6.12;

/// @title Elections contract interface
interface IElections {
\t
\t// Election state change events
\tevent StakeChanged(address indexed addr, uint256 selfStake, uint256 delegatedStake, uint256 effectiveStake);
\tevent GuardianStatusUpdated(address indexed guardian, bool readyToSync, bool readyForCommittee);

\t// Vote out / Vote unready
\tevent GuardianVotedUnready(address indexed guardian);
\tevent VoteUnreadyCasted(address indexed voter, address indexed subject, uint256 expiration);
\tevent GuardianVotedOut(address indexed guardian);
\tevent VoteOutCasted(address indexed voter, address indexed subject);

\t/*
\t * External functions
\t */

\t/// @dev Called by a guardian when ready to start syncing with other nodes
\tfunction readyToSync() external;

\t/// @dev Called by a guardian when ready to join the committee, typically after syncing is complete or after being voted out
\tfunction readyForCommittee() external;

\t/// @dev Called to test if a guardian calling readyForCommittee() will lead to joining the committee
\tfunction canJoinCommittee(address guardian) external view returns (bool);

\t/// @dev Returns an address effective stake
\tfunction getEffectiveStake(address guardian) external view returns (uint effectiveStake);

\t/// @dev returns the current committee
\t/// used also by the rewards and fees contracts
\tfunction getCommittee() external view returns (address[] memory committee, uint256[] memory weights, address[] memory orbsAddrs, bool[] memory certification, bytes4[] memory ips);

\t// Vote-unready

\t/// @dev Called by a guardian as part of the automatic vote-unready flow
\tfunction voteUnready(address subject, uint expiration) external;

\tfunction getVoteUnreadyVote(address voter, address subject) external view returns (bool valid, uint256 expiration);

\t/// @dev Returns the current vote-unready status of a subject guardian.
\t/// votes indicates wether the specific committee member voted the guardian unready
\tfunction getVoteUnreadyStatus(address subject) external view returns (
\t\taddress[] memory committee,
\t\tuint256[] memory weights,
\t\tbool[] memory certification,
\t\tbool[] memory votes,
\t\tbool subjectInCommittee,
\t\tbool subjectInCertifiedCommittee
\t);

\t// Vote-out

\t/// @dev Casts a voteOut vote by the sender to the given address
\tfunction voteOut(address subject) external;

\t/// @dev Returns the subject address the addr has voted-out against
\tfunction getVoteOutVote(address voter) external view returns (address);

\t/// @dev Returns the governance voteOut status of a guardian.
\t/// A guardian is voted out if votedStake / totalDelegatedStake (in percent mille) \u003e threshold
\tfunction getVoteOutStatus(address subject) external view returns (bool votedOut, uint votedStake, uint totalDelegatedStake);

\t/*
\t * Notification functions from other PoS contracts
\t */

\t/// @dev Called by: delegation contract
\t/// Notifies a delegated stake change event
\t/// total_delegated_stake = 0 if addr delegates to another guardian
\tfunction delegatedStakeChange(address delegate, uint256 selfStake, uint256 delegatedStake, uint256 totalDelegatedStake) external /* onlyDelegationsContract onlyWhenActive */;

\t/// @dev Called by: guardian registration contract
\t/// Notifies a new guardian was unregistered
\tfunction guardianUnregistered(address guardian) external /* onlyGuardiansRegistrationContract */;

\t/// @dev Called by: guardian registration contract
\t/// Notifies on a guardian certification change
\tfunction guardianCertificationChanged(address guardian, bool isCertified) external /* onlyCertificationContract */;


\t/*
     * Governance functions
\t */

\tevent VoteUnreadyTimeoutSecondsChanged(uint32 newValue, uint32 oldValue);
\tevent VoteOutPercentMilleThresholdChanged(uint32 newValue, uint32 oldValue);
\tevent VoteUnreadyPercentMilleThresholdChanged(uint32 newValue, uint32 oldValue);
\tevent MinSelfStakePercentMilleChanged(uint32 newValue, uint32 oldValue);

\t/// @dev Sets the minimum self-stake required for the effective stake
\t/// minSelfStakePercentMille - the minimum self stake in percent-mille (0-100,000)
\tfunction setMinSelfStakePercentMille(uint32 minSelfStakePercentMille) external /* onlyFunctionalManager onlyWhenActive */;

\t/// @dev Returns the minimum self-stake required for the effective stake
\tfunction getMinSelfStakePercentMille() external view returns (uint32);

\t/// @dev Sets the vote-out threshold
\t/// voteOutPercentMilleThreshold - the minimum threshold in percent-mille (0-100,000)
\tfunction setVoteOutPercentMilleThreshold(uint32 voteUnreadyPercentMilleThreshold) external /* onlyFunctionalManager onlyWhenActive */;

\t/// @dev Returns the vote-out threshold
\tfunction getVoteOutPercentMilleThreshold() external view returns (uint32);

\t/// @dev Sets the vote-unready threshold
\t/// voteUnreadyPercentMilleThreshold - the minimum threshold in percent-mille (0-100,000)
\tfunction setVoteUnreadyPercentMilleThreshold(uint32 voteUnreadyPercentMilleThreshold) external /* onlyFunctionalManager onlyWhenActive */;

\t/// @dev Returns the vote-unready threshold
\tfunction getVoteUnreadyPercentMilleThreshold() external view returns (uint32);

\t/// @dev Returns the contract\u0027s settings 
\tfunction getSettings() external view returns (
\t\tuint32 minSelfStakePercentMille,
\t\tuint32 voteUnreadyPercentMilleThreshold,
\t\tuint32 voteOutPercentMilleThreshold
\t);

\tfunction initReadyForCommittee(address[] calldata guardians) external /* onlyInitializationAdmin */;

}

"},"IGuardiansRegistration.sol":{"content":"// SPDX-License-Identifier: UNLICENSED

pragma solidity 0.6.12;

/// @title Guardian registration contract interface
interface IGuardiansRegistration {
\tevent GuardianRegistered(address indexed guardian);
\tevent GuardianUnregistered(address indexed guardian);
\tevent GuardianDataUpdated(address indexed guardian, bool isRegistered, bytes4 ip, address orbsAddr, string name, string website);
\tevent GuardianMetadataChanged(address indexed guardian, string key, string newValue, string oldValue);

\t/*
     * External methods
     */

    /// @dev Called by a participant who wishes to register as a guardian
\tfunction registerGuardian(bytes4 ip, address orbsAddr, string calldata name, string calldata website) external;

    /// @dev Called by a participant who wishes to update its propertires
\tfunction updateGuardian(bytes4 ip, address orbsAddr, string calldata name, string calldata website) external;

\t/// @dev Called by a participant who wishes to update its IP address (can be call by both main and Orbs addresses)
\tfunction updateGuardianIp(bytes4 ip) external /* onlyWhenActive */;

    /// @dev Called by a participant to update additional guardian metadata properties.
    function setMetadata(string calldata key, string calldata value) external;

    /// @dev Called by a participant to get additional guardian metadata properties.
    function getMetadata(address guardian, string calldata key) external view returns (string memory);

    /// @dev Called by a participant who wishes to unregister
\tfunction unregisterGuardian() external;

    /// @dev Returns a guardian\u0027s data
\tfunction getGuardianData(address guardian) external view returns (bytes4 ip, address orbsAddr, string memory name, string memory website, uint registrationTime, uint lastUpdateTime);

\t/// @dev Returns the Orbs addresses of a list of guardians
\tfunction getGuardiansOrbsAddress(address[] calldata guardianAddrs) external view returns (address[] memory orbsAddrs);

\t/// @dev Returns a guardian\u0027s ip
\tfunction getGuardianIp(address guardian) external view returns (bytes4 ip);

\t/// @dev Returns guardian ips
\tfunction getGuardianIps(address[] calldata guardian) external view returns (bytes4[] memory ips);

\t/// @dev Returns true if the given address is of a registered guardian
\tfunction isRegistered(address guardian) external view returns (bool);

\t/// @dev Translates a list guardians Orbs addresses to guardian addresses
\tfunction getGuardianAddresses(address[] calldata orbsAddrs) external view returns (address[] memory guardianAddrs);

\t/// @dev Resolves the guardian address for a guardian, given a Guardian/Orbs address
\tfunction resolveGuardianAddress(address guardianOrOrbsAddress) external view returns (address guardianAddress);

\t/*
\t * Governance functions
\t */

\tfunction migrateGuardians(address[] calldata guardiansToMigrate, IGuardiansRegistration previousContract) external /* onlyInitializationAdmin */;

}
"},"ILockable.sol":{"content":"// SPDX-License-Identifier: UNLICENSED

pragma solidity 0.6.12;

interface ILockable {

    event Locked();
    event Unlocked();

    function lock() external /* onlyLockOwner */;
    function unlock() external /* onlyLockOwner */;
    function isLocked() view external returns (bool);

}
"},"Initializable.sol":{"content":"// SPDX-License-Identifier: UNLICENSED

pragma solidity 0.6.12;

contract Initializable {

    address private _initializationAdmin;

    event InitializationComplete();

    constructor() public{
        _initializationAdmin = msg.sender;
    }

    modifier onlyInitializationAdmin() {
        require(msg.sender == initializationAdmin(), \"sender is not the initialization admin\");

        _;
    }

    /*
    * External functions
    */

    function initializationAdmin() public view returns (address) {
        return _initializationAdmin;
    }

    function initializationComplete() external onlyInitializationAdmin {
        _initializationAdmin = address(0);
        emit InitializationComplete();
    }

    function isInitializationComplete() public view returns (bool) {
        return _initializationAdmin == address(0);
    }

}"},"Lockable.sol":{"content":"// SPDX-License-Identifier: UNLICENSED

pragma solidity 0.6.12;

import \"./ContractRegistryAccessor.sol\";
import \"./ILockable.sol\";

contract Lockable is ILockable, ContractRegistryAccessor {

    bool public locked;

    constructor(IContractRegistry _contractRegistry, address _registryAdmin) ContractRegistryAccessor(_contractRegistry, _registryAdmin) public {}

    modifier onlyLockOwner() {
        require(msg.sender == registryAdmin() || msg.sender == address(getContractRegistry()), \"caller is not a lock owner\");

        _;
    }

    function lock() external override onlyLockOwner {
        locked = true;
        emit Locked();
    }

    function unlock() external override onlyLockOwner {
        locked = false;
        emit Unlocked();
    }

    function isLocked() external override view returns (bool) {
        return locked;
    }

    modifier onlyWhenActive() {
        require(!locked, \"contract is locked for this operation\");

        _;
    }
}
"},"ManagedContract.sol":{"content":"// SPDX-License-Identifier: UNLICENSED

pragma solidity 0.6.12;

import \"./Lockable.sol\";

contract ManagedContract is Lockable {

    constructor(IContractRegistry _contractRegistry, address _registryAdmin) Lockable(_contractRegistry, _registryAdmin) public {}

    modifier onlyMigrationManager {
        require(isManager(\"migrationManager\"), \"sender is not the migration manager\");

        _;
    }

    modifier onlyFunctionalManager {
        require(isManager(\"functionalManager\"), \"sender is not the functional manager\");

        _;
    }

    function refreshContracts() virtual external {}

}"},"SafeMath.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.6.0;

/**
 * @dev Wrappers over Solidity\u0027s arithmetic operations with added overflow
 * checks.
 *
 * Arithmetic operations in Solidity wrap on overflow. This can easily result
 * in bugs, because programmers usually assume that an overflow raises an
 * error, which is the standard behavior in high level programming languages.
 * `SafeMath` restores this intuition by reverting the transaction when an
 * operation overflows.
 *
 * Using this library instead of the unchecked operations eliminates an entire
 * class of bugs, so it\u0027s recommended to use it always.
 */
library SafeMath {
    /**
     * @dev Returns the addition of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity\u0027s `+` operator.
     *
     * Requirements:
     *
     * - Addition cannot overflow.
     */
    function add(uint256 a, uint256 b) internal pure returns (uint256) {
        uint256 c = a + b;
        require(c \u003e= a, \"SafeMath: addition overflow\");

        return c;
    }

    /**
     * @dev Returns the subtraction of two unsigned integers, reverting on
     * overflow (when the result is negative).
     *
     * Counterpart to Solidity\u0027s `-` operator.
     *
     * Requirements:
     *
     * - Subtraction cannot overflow.
     */
    function sub(uint256 a, uint256 b) internal pure returns (uint256) {
        return sub(a, b, \"SafeMath: subtraction overflow\");
    }

    /**
     * @dev Returns the subtraction of two unsigned integers, reverting with custom message on
     * overflow (when the result is negative).
     *
     * Counterpart to Solidity\u0027s `-` operator.
     *
     * Requirements:
     *
     * - Subtraction cannot overflow.
     */
    function sub(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b \u003c= a, errorMessage);
        uint256 c = a - b;

        return c;
    }

    /**
     * @dev Returns the multiplication of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity\u0027s `*` operator.
     *
     * Requirements:
     *
     * - Multiplication cannot overflow.
     */
    function mul(uint256 a, uint256 b) internal pure returns (uint256) {
        // Gas optimization: this is cheaper than requiring \u0027a\u0027 not being zero, but the
        // benefit is lost if \u0027b\u0027 is also tested.
        // See: https://github.com/OpenZeppelin/openzeppelin-contracts/pull/522
        if (a == 0) {
            return 0;
        }

        uint256 c = a * b;
        require(c / a == b, \"SafeMath: multiplication overflow\");

        return c;
    }

    /**
     * @dev Returns the integer division of two unsigned integers. Reverts on
     * division by zero. The result is rounded towards zero.
     *
     * Counterpart to Solidity\u0027s `/` operator. Note: this function uses a
     * `revert` opcode (which leaves remaining gas untouched) while Solidity
     * uses an invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function div(uint256 a, uint256 b) internal pure returns (uint256) {
        return div(a, b, \"SafeMath: division by zero\");
    }

    /**
     * @dev Returns the integer division of two unsigned integers. Reverts with custom message on
     * division by zero. The result is rounded towards zero.
     *
     * Counterpart to Solidity\u0027s `/` operator. Note: this function uses a
     * `revert` opcode (which leaves remaining gas untouched) while Solidity
     * uses an invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function div(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b \u003e 0, errorMessage);
        uint256 c = a / b;
        // assert(a == b * c + a % b); // There is no case in which this doesn\u0027t hold

        return c;
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
     * Reverts when dividing by zero.
     *
     * Counterpart to Solidity\u0027s `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function mod(uint256 a, uint256 b) internal pure returns (uint256) {
        return mod(a, b, \"SafeMath: modulo by zero\");
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
     * Reverts with custom message when dividing by zero.
     *
     * Counterpart to Solidity\u0027s `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function mod(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b != 0, errorMessage);
        return a % b;
    }
}
"},"WithClaimableRegistryManagement.sol":{"content":"// SPDX-License-Identifier: UNLICENSED

pragma solidity 0.6.12;

import \"./Context.sol\";

/**
 * @title Claimable
 * @dev Extension for the Ownable contract, where the ownership needs to be claimed.
 * This allows the new owner to accept the transfer.
 */
contract WithClaimableRegistryManagement is Context {
    address private _registryAdmin;
    address private _pendingRegistryAdmin;

    event RegistryManagementTransferred(address indexed previousRegistryAdmin, address indexed newRegistryAdmin);

    /**
     * @dev Initializes the contract setting the deployer as the initial registryRegistryAdmin.
     */
    constructor () internal {
        address msgSender = _msgSender();
        _registryAdmin = msgSender;
        emit RegistryManagementTransferred(address(0), msgSender);
    }

    /**
     * @dev Returns the address of the current registryAdmin.
     */
    function registryAdmin() public view returns (address) {
        return _registryAdmin;
    }

    /**
     * @dev Throws if called by any account other than the registryAdmin.
     */
    modifier onlyRegistryAdmin() {
        require(isRegistryAdmin(), \"WithClaimableRegistryManagement: caller is not the registryAdmin\");
        _;
    }

    /**
     * @dev Returns true if the caller is the current registryAdmin.
     */
    function isRegistryAdmin() public view returns (bool) {
        return _msgSender() == _registryAdmin;
    }

    /**
     * @dev Leaves the contract without registryAdmin. It will not be possible to call
     * `onlyManager` functions anymore. Can only be called by the current registryAdmin.
     *
     * NOTE: Renouncing registryManagement will leave the contract without an registryAdmin,
     * thereby removing any functionality that is only available to the registryAdmin.
     */
    function renounceRegistryManagement() public onlyRegistryAdmin {
        emit RegistryManagementTransferred(_registryAdmin, address(0));
        _registryAdmin = address(0);
    }

    /**
     * @dev Transfers registryManagement of the contract to a new account (`newManager`).
     */
    function _transferRegistryManagement(address newRegistryAdmin) internal {
        require(newRegistryAdmin != address(0), \"RegistryAdmin: new registryAdmin is the zero address\");
        emit RegistryManagementTransferred(_registryAdmin, newRegistryAdmin);
        _registryAdmin = newRegistryAdmin;
    }

    /**
     * @dev Modifier throws if called by any account other than the pendingManager.
     */
    modifier onlyPendingRegistryAdmin() {
        require(msg.sender == _pendingRegistryAdmin, \"Caller is not the pending registryAdmin\");
        _;
    }
    /**
     * @dev Allows the current registryAdmin to set the pendingManager address.
     * @param newRegistryAdmin The address to transfer registryManagement to.
     */
    function transferRegistryManagement(address newRegistryAdmin) public onlyRegistryAdmin {
        _pendingRegistryAdmin = newRegistryAdmin;
    }

    /**
     * @dev Allows the _pendingRegistryAdmin address to finalize the transfer.
     */
    function claimRegistryManagement() external onlyPendingRegistryAdmin {
        _transferRegistryManagement(_pendingRegistryAdmin);
        _pendingRegistryAdmin = address(0);
    }

    /**
     * @dev Returns the current pendingRegistryAdmin
    */
    function pendingRegistryAdmin() public view returns (address) {
       return _pendingRegistryAdmin;  
    }
}

