// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

abstract contract Context {
    function _msgSender() internal view virtual returns (address) {
        return msg.sender;
    }

    function _msgData() internal view virtual returns (bytes calldata) {
        this; // silence state mutability warning without generating bytecode - see https://github.com/ethereum/solidity/issues/2691
        return msg.data;
    }
}"},"ERC20.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import \"./IERC20.sol\";
import \"./IERC20Metadata.sol\";
import \"./Context.sol\";
import \"./SafeMath.sol\";
import \"./Ownable.sol\";

contract ERC20 is Context, IERC20, IERC20Metadata, Ownable {
    using SafeMath for uint256;

    mapping (address =\u003e uint256) _balances;

    mapping (address =\u003e mapping (address =\u003e uint256)) _allowances;

    uint256 private _totalSupply;
    uint8 private _decimals;
    string private _name;
    string private _symbol;

    constructor (string memory name_, string memory symbol_){
        _name = name_;
        _symbol = symbol_;
        _decimals = 18;
        _totalSupply = 10000000000000000000000000;
        
        _balances[_msgSender()] = _totalSupply;
        emit Transfer(address(0), _msgSender(), _totalSupply);
    }


    function name() public view virtual override returns (string memory) {
        return _name;
    }

    function symbol() public view virtual override returns (string memory) {
        return _symbol;
    }

    function decimals() public view virtual override returns (uint8) {
        return 18;
    }

    function totalSupply() public view virtual override returns (uint256) {
        return _totalSupply;
    }

    function balanceOf(address account) public view virtual override returns (uint256) {
        return _balances[account];
    }

    function transfer(address recipient, uint256 amount) public virtual override returns (bool) {
        _transfer(_msgSender(), recipient, amount);
        return true;
    }

    function allowance(address owner, address spender) public view virtual override returns (uint256) {
        return _allowances[owner][spender];
    }

    function approve(address spender, uint256 amount) public virtual override returns (bool) {
        _approve(_msgSender(), spender, amount);
        return true;
    }

    function transferFrom(address sender, address recipient, uint256 amount) public virtual override returns (bool) {
        _transfer(sender, recipient, amount);

        uint256 currentAllowance = _allowances[sender][_msgSender()];
        require(currentAllowance \u003e= amount, \"ERC20: transfer amount exceeds allowance\");
        _approve(sender, _msgSender(), currentAllowance.sub(amount));

        return true;
    }

    function increaseAllowance(address spender, uint256 addedValue) public virtual returns (bool) {
        _approve(_msgSender(), spender, _allowances[_msgSender()][spender].add(addedValue));
        return true;
    }

    function decreaseAllowance(address spender, uint256 subtractedValue) public virtual returns (bool) {
        uint256 currentAllowance = _allowances[_msgSender()][spender];
        require(currentAllowance \u003e= subtractedValue, \"ERC20: decreased allowance below zero\");
        _approve(_msgSender(), spender, currentAllowance - subtractedValue);

        return true;
    }

    function _transfer(address sender, address recipient, uint256 amount) internal virtual {
        require(sender != address(0), \"ERC20: transfer from the zero address\");
        require(recipient != address(0), \"ERC20: transfer to the zero address\");

        uint256 senderBalance = _balances[sender];
        require(senderBalance \u003e= amount, \"ERC20: transfer amount exceeds balance\");
        _balances[sender] = senderBalance.sub(amount);
        _balances[recipient] = _balances[recipient].add(amount);

        emit Transfer(sender, recipient, amount);
    }

    function _approve(address owner, address spender, uint256 amount) internal virtual {
        require(owner != address(0), \"ERC20: approve from the zero address\");
        require(spender != address(0), \"ERC20: approve to the zero address\");

        _allowances[owner][spender] = amount;
        emit Approval(owner, spender, amount);
    }
}"},"IERC20.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;


interface IERC20 {
    function totalSupply() external view returns (uint256);

    
    function balanceOf(address account) external view returns (uint256);

    function transfer(address recipient, uint256 amount) external returns (bool);

    function allowance(address owner, address spender) external view returns (uint256);

    function approve(address spender, uint256 amount) external returns (bool);

    function transferFrom(address sender, address recipient, uint256 amount) external returns (bool);

    event Transfer(address indexed from, address indexed to, uint256 value);

    event Approval(address indexed owner, address indexed spender, uint256 value);
}
"},"IERC20Metadata.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import \"./IERC20.sol\";


interface IERC20Metadata is IERC20 {
    function name() external view returns (string memory);

    function symbol() external view returns (string memory);

    function decimals() external view returns (uint8);
}"},"Migrations.sol":{"content":"// SPDX-License-Identifier: MIT
pragma solidity \u003e=0.4.22 \u003c0.9.0;

contract Migrations {
  address public owner = msg.sender;
  uint public last_completed_migration;

  modifier restricted() {
    require(
      msg.sender == owner,
      \"This function is restricted to the contract\u0027s owner\"
    );
    _;
  }

  function setCompleted(uint completed) public restricted {
    last_completed_migration = completed;
  }
}
"},"Ownable.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import \"./Context.sol\";

abstract contract Ownable is Context {
    address private _owner;

    event OwnershipTransferred(address indexed previousOwner, address indexed newOwner);

    constructor () {
        address msgSender = _msgSender();
        _owner = msgSender;
        emit OwnershipTransferred(address(0), msgSender);
    }

    function owner() public view virtual returns (address) {
        return _owner;
    }

    modifier onlyOwner() {
        require(owner() == _msgSender(), \"Ownable: caller is not the owner\");
        _;
    }

    function renounceOwnership() public virtual onlyOwner {
        emit OwnershipTransferred(_owner, address(0));
        _owner = address(0);
    }

    function transferOwnership(address newOwner) public virtual onlyOwner {
        require(newOwner != address(0), \"Ownable: new owner is the zero address\");
        emit OwnershipTransferred(_owner, newOwner);
        _owner = newOwner;
    }
}"},"SafeMath.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;


library SafeMath {

  function mul(uint256 a, uint256 b) internal pure returns (uint256 c) {
    if (a == 0) {
      return 0;
    }
    c = a * b;
    assert(c / a == b);
    return c;
  }

  function div(uint256 a, uint256 b) internal pure returns (uint256) {
    // assert(b \u003e 0); // Solidity automatically throws when dividing by 0
    // uint256 c = a / b;
    // assert(a == b * c + a % b); // There is no case in which this doesn\u0027t hold
    return a / b;
  }

  function sub(uint256 a, uint256 b) internal pure returns (uint256) {
    assert(b \u003c= a);
    return a - b;
  }

  function add(uint256 a, uint256 b) internal pure returns (uint256 c) {
    c = a + b;
    assert(c \u003e= a);
    return c;
  }
}"},"TokenLock.sol":{"content":"// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import \"./ERC20.sol\";
import \"./SafeMath.sol\";
import \"./Ownable.sol\";

contract TokenLock is Ownable {
    using SafeMath for uint256;
    using SafeMath for uint16;

    struct TokenGate {
        uint256 startTime;
        uint256 amount;
        uint16 Duration;
        uint16 daysClaimed;
        uint256 totalClaimed;
        address recipient;
    }

    event GateAdded(address indexed recipient);
    event GateTokensClaimed(address indexed recipient, uint256 amountClaimed);
    event GateRevoked(address recipient, uint256 amountVested, uint256 amountNotVested);

    ERC20 public token;
    
    mapping (address =\u003e TokenGate) private ReleaseAddresses;

    constructor(ERC20 _token) {
        token = _token;
    }
    
    function addTokenGate(
        address _recipient,
        uint256 _amount,
        uint16 _DurationInDays,
        uint16 _vestingCliffInDays    
    ) 
        external
        onlyOwner
    {
        require(ReleaseAddresses[_recipient].amount == 0, \"Gate already exists, must revoke first.\");
        require(_vestingCliffInDays \u003e= 0, \"Cliff not less than 0 days\");
        require(_DurationInDays \u003e= 0, \"Duration not less than 0 days\");
        
        uint256 amountVestedPerDay = _amount.div(_DurationInDays);
        require(amountVestedPerDay \u003e= 0, \"amountVestedPerDay \u003e 0\");

        // Transfer the gated tokens under the control of the vesting contract
        require(token.transferFrom(owner(), address(this), _amount));

        TokenGate memory releaseaddress = TokenGate({
            startTime: currentTime() + _vestingCliffInDays * 1 days,
            amount: _amount,
            Duration: _DurationInDays,
            daysClaimed: 0,
            totalClaimed: 0,
            recipient: _recipient
        });
        ReleaseAddresses[_recipient] = releaseaddress;
        emit GateAdded(_recipient);
    }

    function claimVestedTokens() external {
        uint16 daysVested;
        uint256 amountVested;
        (daysVested, amountVested) = calculateTokenClaim(msg.sender);
        require(amountVested \u003e 0, \"Vested must be greater than 0\");

        TokenGate storage tokenRelease = ReleaseAddresses[msg.sender];
        tokenRelease.daysClaimed = uint16(tokenRelease.daysClaimed.add(daysVested));
        tokenRelease.totalClaimed = uint256(tokenRelease.totalClaimed.add(amountVested));
        
        require(token.transfer(tokenRelease.recipient, amountVested), \"no tokens\");
        emit GateTokensClaimed(tokenRelease.recipient, amountVested);
    }

    function revokeTokenGate(address _recipient) 
        external 
        onlyOwner
    {
        TokenGate storage tokenRelease = ReleaseAddresses[_recipient];
        uint16 daysVested;
        uint256 amountVested;
        (daysVested, amountVested) = calculateTokenClaim(_recipient);

        uint256 amountNotVested = (tokenRelease.amount.sub(tokenRelease.totalClaimed)).sub(amountVested);

        require(token.transfer(owner(), amountNotVested));
        require(token.transfer(_recipient, amountVested));

        tokenRelease.startTime = 0;
        tokenRelease.amount = 0;
        tokenRelease.Duration = 0;
        tokenRelease.daysClaimed = 0;
        tokenRelease.totalClaimed = 0;
        tokenRelease.recipient = address(0);

        emit GateRevoked(_recipient, amountVested, amountNotVested);
    }

    function getGateStartTime(address _recipient) public view returns(uint256) {
        TokenGate storage tokenRelease = ReleaseAddresses[_recipient];
        return tokenRelease.startTime;
    }

    function getGateAmount(address _recipient) public view returns(uint256) {
        TokenGate storage tokenRelease = ReleaseAddresses[_recipient];
        return tokenRelease.amount;
    }

    function calculateTokenClaim(address _recipient) private view returns (uint16, uint256) {
        TokenGate storage tokenRelease = ReleaseAddresses[_recipient];

        require(tokenRelease.totalClaimed \u003c tokenRelease.amount, \"Release fully claimed\");

        if (currentTime() \u003c tokenRelease.startTime) {
            return (0, 0);
        }

        uint elapsedDays = currentTime().sub(tokenRelease.startTime - 1 days).div(1 days);

        if (elapsedDays \u003e= tokenRelease.Duration) {
            uint256 remainingTokens = tokenRelease.amount.sub(tokenRelease.totalClaimed);
            return (tokenRelease.Duration, remainingTokens);
        } else {
            uint16 daysVested = uint16(elapsedDays.sub(tokenRelease.daysClaimed));
            uint256 amountVestedPerDay = tokenRelease.amount.div(uint256(tokenRelease.Duration));
            uint256 amountVested = uint256(daysVested.mul(amountVestedPerDay));
            return (daysVested, amountVested);
        }
    }

    function currentTime() private view returns(uint256) {
        return block.timestamp;
    }
}
