// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import \"./ERC20.sol\";
import \"./Ownable.sol\";
import \"./Math.sol\";

contract AYRA is ERC20, Ownable {

    using Math for uint256;

    address public walletOrigin = 0xE9fe09A55377f760128800e6813F2E2C07db60Ad;
    address public walletMarketProtection = 0x0bD042059368389fdC3968d671c40319dEb39F2c;
    address public walletFoundingPartners = 0x454d1252EC7c1Dc7E4D0A92A84A3Da2BD158b1D7;
    address public walletBlockedFoundingPartners = 0x8f7F2243A34169931741ba7eB257841C639Bc165;
    address public walletSocialPartners = 0xe307d66905D10e7e51B0BFb12E7e64C876a04215;
    address public walletProgrammersAndPartners = 0xc21713ef49a48396c1939233F3B24E1c4CCD09a4;
    address public walletPrivateInvestors = 0x252Fa9eD5F51e3A9CF1b1890f479775eFeaa653d;
    address public walletAidsAndDonations = 0x1EEffDA40C880a93E19ecAF031e529C723072e51;
    address public walletStakingAyra = 0xD55E7f6C6B8027Fa7FCdE6eFb4fD3f02d391130C;

    address public operatorAddress;

    uint256 public MAX_BURN_AMOUNT = 100_000_000_000_000 * (10 ** decimals());
    uint256 public BURN_AMOUNT = 5_000_000_000_000 * (10 ** decimals());
    uint256 public lastBurnDay = block.timestamp;
    uint256 public burnedAmount = 0;

    uint256 private _maxStakingAmount = 60_000_000_000_000 * (10 ** decimals());
    uint256 private _maxStakingAmountPerAccount = 100_000_000 * (10 ** decimals());
    uint256 private _totalStakingAmount = 0;
    uint256 private _stakingPeriod = block.timestamp + 730 days;
    uint256 private _stakingFirstPeriod = block.timestamp + 365 days;

    uint256 private _stakingFirstPeriodReward = 1644;
    uint256 private _stakingSecondPeriodReward = 822;
    
    uint256 public deployedTime = block.timestamp;

    uint256 public lastUnlockTime;
    uint256 public unlockAmountPerYear = 20_000_000_000_000 * (10 ** decimals());
    
    // Mapping owner address to staked token count
    mapping (address =\u003e uint) _stakedBalances;
    
    // Mapping from owner to last reward time
    mapping (address =\u003e uint) _rewardedLastTime;

    event StakingSucceed(address indexed account, uint256 totalStakedAmount);
    event WithdrawSucceed(address indexed account, uint256 remainedStakedAmount);

    /**
    * @dev modifier which requires that account must be operator
    */
    modifier onlyOperator() {
        require(_msgSender() == operatorAddress, \"operator: wut?\");
        _;
    }

    /**
    * @dev modifier which requires that walletAddress is not blocked address(walletMarketProtection),
    * until blocking period.
    */
    modifier onlyUnblock(address walletAddress) {
        require((walletAddress != walletMarketProtection \u0026\u0026 walletAddress != walletBlockedFoundingPartners)
                    || block.timestamp \u003e deployedTime + 1825 days, \"This wallet address is blocked for 5 years.\" );
        _;
    }

    /**
    * @dev Constructor: mint pre-defined amount of tokens to special wallets.
     */
    constructor() ERC20(\"AYRA\", \"AYRA\") {
        operatorAddress = _msgSender();
        //uint totalSupply = 1_000_000_000_000_000 * (10 ** decimals());

        // 40% of total supply to walletOrigin
        _mint(walletOrigin, 400_000_000_000_000 * (10 ** decimals()));

        // 10% of total supply to walletMarketProtection
        _mint(walletMarketProtection, 100_000_000_000_000 * (10 ** decimals()));

        // 9% of total supply to walletFoundingPartners
        _mint(walletFoundingPartners, 90_000_000_000_000 * (10 ** decimals()));

        // 1% of total supply to walletBlockedFoundingPartners
        _mint(walletBlockedFoundingPartners, 10_000_000_000_000 * (10 ** decimals()));

        // 10% of total supply to walletSocialPartners
        _mint(walletSocialPartners, 100_000_000_000_000 * (10 ** decimals()));

        // 18% of total supply to walletProgrammersAndPartners
        _mint(walletProgrammersAndPartners, 180_000_000_000_000 * (10 ** decimals()));

        // 7% of total supply to walletPrivateInvestors
        _mint(walletPrivateInvestors, 70_000_000_000_000 * (10 ** decimals()));

        // 5% of total supply to walletAidsAndDonations
        _mint(walletAidsAndDonations, 50_000_000_000_000 * (10 ** decimals()));
    }

    /**
    * @dev set operator address
    * callable by owner
    */
    function setOperator(address _operator) external onlyOwner {
        require(_operator != address(0), \"Cannot be zero address\");
        operatorAddress = _operator;
    }

    /**
     * @dev Destroys `amount` tokens from `walletOrigin`, reducing the
     * total supply.
     *
     * Requirements:
     *
     * - total burning amount can not exceed `_maxBurnAmount`
     * - burning moment have to be 90 days later from `lastBurnDay`
     */
    function burn() external onlyOperator {
        
        require(burnedAmount + BURN_AMOUNT \u003c= MAX_BURN_AMOUNT, \"Burning too much.\");
        require(lastBurnDay + 90 days \u003c= block.timestamp, \"It\u0027s not time to burn. 90 days aren\u0027t passed since last burn\");
        lastBurnDay = block.timestamp;

        _burn(walletOrigin, BURN_AMOUNT);
        burnedAmount += BURN_AMOUNT;
    }

    /**
     * @dev Stake `amount` tokens from `msg.sender` to `walletOrigin`, calculate reward upto now.
     *
     * Emits a {StakingSucceed} event with `account` and total staked balance of `account`
     *
     * Requirements:
     *
     * - `account` must have at least `amount` tokens
     * - staking moment have to be in staking period
     * - staked balance of each account can not exceed `_maxStakingAmountPerAccount`
     * - total staking amount can not exceed `_totalStakingAmount`
     */
    function stake(uint amount) external {
        
        address account = _msgSender();

        require(balanceOf(account) \u003e= amount, \"insufficient balance for staking.\");
        require(block.timestamp \u003c= _stakingPeriod, \"The time is over staking period.\");

        _updateReward(account);

        _stakedBalances[account] += amount;
        require(_stakedBalances[account] \u003c= _maxStakingAmountPerAccount, \"This account overflows staking amount\");
        
        _totalStakingAmount += amount;
        require(_totalStakingAmount \u003c= _maxStakingAmount, \"Total staking amount overflows its limit.\");
        
        _transfer(account, walletStakingAyra, amount);
        
        emit StakingSucceed(account, _stakedBalances[account]);
    }

    /**
     * @dev Returns the amount of tokens owned by `account`. Something different from ERC20 is
     * adding reward which is not yet appended to account wallet.
     */
    function balanceOf(address account) public view override returns (uint) {
        return ERC20.balanceOf(account) + getAvailableReward(account);
    }

    /**
     * @dev Get account\u0027s reward which is yielded after last rewarded time.
     *
     * @notice if getting moment is after stakingPeriod, the reward must be 0.
     * 
     * First `if` statement is in case of `lastTime` is before firstPeriod.
     *         `lastTime`  block.timestamp(if1)                   block.timestamp(if2)
     * ||----------|---------------|------------||------------------------|-----------||
     *              firstPeriod                             secondPeriod
     *
     * Second `if` statement is in case of block.timestamp is in secondPeriod.
     */
    function getAvailableReward(address account) public view returns (uint) {

        if (_rewardedLastTime[account] \u003e _stakingPeriod) return 0;
        
        uint reward = 0;
        if (_rewardedLastTime[account] \u003c= _stakingFirstPeriod) {
            uint rewardDays = _stakingFirstPeriod.min(block.timestamp) - _rewardedLastTime[account];
            rewardDays /= 1 days;
            reward = rewardDays * _stakedBalances[account] * _stakingFirstPeriodReward / 1000000;
        }

        if (block.timestamp \u003e _stakingFirstPeriod) {
            uint rewardDays = _stakingPeriod.min(block.timestamp) - _rewardedLastTime[account].max(_stakingFirstPeriod);
            rewardDays /= 1 days;
            reward += rewardDays * _stakedBalances[account] * _stakingSecondPeriodReward / 1000000;
        }
        
        return reward;
    }

    /**
     * @dev Withdraw `amount` tokens from stakingPool(`walletOrigin`) to `msg.sender` address, calculate reward upto now.
     *
     * Emits a {WithdrawSucceed} event with `account` and total staked balance of `account`
     *
     * Requirements:
     *
     * - staked balance of `msg.sender` must be at least `amount`.
     */
    function withdraw(uint amount) external {
        address account = _msgSender();
        require (_stakedBalances[account] \u003e= amount, \"Can\u0027t withdraw more than staked balance\");

        _updateReward(account);

        _stakedBalances[account] -= amount;
        _totalStakingAmount -= amount;
        _transfer(walletStakingAyra, account, amount);

        emit WithdrawSucceed(account, _stakedBalances[account]);
    } 

    /**
     * @dev Hook that is called before any transfer of tokens. 
     * Here, update from\u0027s balance by adding not-yet-appended reward.
     *
     * Requirements:
     *
     * - blocked wallet (walletMarketProtection) can\u0027t be tranferred or transfer any balance.
     */
    function _beforeTokenTransfer(address from, address to, uint256) internal override onlyUnblock(from) {
        if (from != address(0) \u0026\u0026 from != walletOrigin) {
            _updateReward(from);
        }
    }

    /**
     * @dev Get account\u0027s available reward which is yielded from last rewarded moment.
     * And append available reward to account\u0027s balance.
     */
    function _updateReward(address account) public {
        uint availableReward = getAvailableReward(account);
        _rewardedLastTime[account] = block.timestamp;
        _transfer(walletOrigin, account, availableReward);
    }

    /**
     * @dev Unlock `walletMarketProtection`, which means that transfer tokens from `walletMarketProtection`
     * to `walletOrigin`, so that it can be traded across users.ok
     */
    function unlockProtection() public onlyOperator {
        require (block.timestamp \u003e deployedTime + 5 * 365 days, \"Unlock is not allowed now\");
        require (block.timestamp \u003e lastUnlockTime + 365 days, \"Unlock must be 1 year later from previous unlock\");
        lastUnlockTime = block.timestamp;
        _transfer(walletMarketProtection, walletOrigin, unlockAmountPerYear);
    }
} "},"Context.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

/**
 * @dev Provides information about the current execution context, including the
 * sender of the transaction and its data. While these are generally available
 * via msg.sender and msg.data, they should not be accessed in such a direct
 * manner, since when dealing with meta-transactions the account sending and
 * paying for execution may not be the actual sender (as far as an application
 * is concerned).
 *
 * This contract is only required for intermediate, library-like contracts.
 */
abstract contract Context {
    function _msgSender() internal view virtual returns (address) {
        return msg.sender;
    }

    function _msgData() internal view virtual returns (bytes calldata) {
        return msg.data;
    }
}
"},"ERC20.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import \"./IERC20.sol\";
import \"./IERC20Metadata.sol\";
import \"./Context.sol\";

/**
 * @dev Implementation of the {IERC20} interface.
 *
 * This implementation is agnostic to the way tokens are created. This means
 * that a supply mechanism has to be added in a derived contract using {_mint}.
 * For a generic mechanism see {ERC20PresetMinterPauser}.
 *
 * TIP: For a detailed writeup see our guide
 * https://forum.zeppelin.solutions/t/how-to-implement-erc20-supply-mechanisms/226[How
 * to implement supply mechanisms].
 *
 * We have followed general OpenZeppelin Contracts guidelines: functions revert
 * instead returning `false` on failure. This behavior is nonetheless
 * conventional and does not conflict with the expectations of ERC20
 * applications.
 *
 * Additionally, an {Approval} event is emitted on calls to {transferFrom}.
 * This allows applications to reconstruct the allowance for all accounts just
 * by listening to said events. Other implementations of the EIP may not emit
 * these events, as it isn\u0027t required by the specification.
 *
 * Finally, the non-standard {decreaseAllowance} and {increaseAllowance}
 * functions have been added to mitigate the well-known issues around setting
 * allowances. See {IERC20-approve}.
 */
contract ERC20 is Context, IERC20, IERC20Metadata {
    mapping(address =\u003e uint256) internal _balances; // modified from private to internal

    mapping(address =\u003e mapping(address =\u003e uint256)) private _allowances;

    uint256 private _totalSupply;

    string private _name;
    string private _symbol;

    /**
     * @dev Sets the values for {name} and {symbol}.
     *
     * The default value of {decimals} is 18. To select a different value for
     * {decimals} you should overload it.
     *
     * All two of these values are immutable: they can only be set once during
     * construction.
     */
    constructor(string memory name_, string memory symbol_) {
        _name = name_;
        _symbol = symbol_;
    }

    /**
     * @dev Returns the name of the token.
     */
    function name() public view virtual override returns (string memory) {
        return _name;
    }

    /**
     * @dev Returns the symbol of the token, usually a shorter version of the
     * name.
     */
    function symbol() public view virtual override returns (string memory) {
        return _symbol;
    }

    /**
     * @dev Returns the number of decimals used to get its user representation.
     * For example, if `decimals` equals `2`, a balance of `505` tokens should
     * be displayed to a user as `5.05` (`505 / 10 ** 2`).
     *
     * Tokens usually opt for a value of 18, imitating the relationship between
     * Ether and Wei. This is the value {ERC20} uses, unless this function is
     * overridden;
     *
     * NOTE: This information is only used for _display_ purposes: it in
     * no way affects any of the arithmetic of the contract, including
     * {IERC20-balanceOf} and {IERC20-transfer}.
     */
    function decimals() public view virtual override returns (uint8) {
        return 18;
    }

    /**
     * @dev See {IERC20-totalSupply}.
     */
    function totalSupply() public view virtual override returns (uint256) {
        return _totalSupply;
    }

    /**
     * @dev See {IERC20-balanceOf}.
     */
    function balanceOf(address account) public view virtual override returns (uint256) {
        return _balances[account];
    }

    /**
     * @dev See {IERC20-transfer}.
     *
     * Requirements:
     *
     * - `recipient` cannot be the zero address.
     * - the caller must have a balance of at least `amount`.
     */
    function transfer(address recipient, uint256 amount) public virtual override returns (bool) {
        _transfer(_msgSender(), recipient, amount);
        return true;
    }

    /**
     * @dev See {IERC20-allowance}.
     */
    function allowance(address owner, address spender) public view virtual override returns (uint256) {
        return _allowances[owner][spender];
    }

    /**
     * @dev See {IERC20-approve}.
     *
     * Requirements:
     *
     * - `spender` cannot be the zero address.
     */
    function approve(address spender, uint256 amount) public virtual override returns (bool) {
        _approve(_msgSender(), spender, amount);
        return true;
    }

    /**
     * @dev See {IERC20-transferFrom}.
     *
     * Emits an {Approval} event indicating the updated allowance. This is not
     * required by the EIP. See the note at the beginning of {ERC20}.
     *
     * Requirements:
     *
     * - `sender` and `recipient` cannot be the zero address.
     * - `sender` must have a balance of at least `amount`.
     * - the caller must have allowance for ``sender``\u0027s tokens of at least
     * `amount`.
     */
    function transferFrom(
        address sender,
        address recipient,
        uint256 amount
    ) public virtual override returns (bool) {
        _transfer(sender, recipient, amount);

        uint256 currentAllowance = _allowances[sender][_msgSender()];
        require(currentAllowance \u003e= amount, \"ERC20: transfer amount exceeds allowance\");
        unchecked {
            _approve(sender, _msgSender(), currentAllowance - amount);
        }

        return true;
    }

    /**
     * @dev Atomically increases the allowance granted to `spender` by the caller.
     *
     * This is an alternative to {approve} that can be used as a mitigation for
     * problems described in {IERC20-approve}.
     *
     * Emits an {Approval} event indicating the updated allowance.
     *
     * Requirements:
     *
     * - `spender` cannot be the zero address.
     */
    function increaseAllowance(address spender, uint256 addedValue) public virtual returns (bool) {
        _approve(_msgSender(), spender, _allowances[_msgSender()][spender] + addedValue);
        return true;
    }

    /**
     * @dev Atomically decreases the allowance granted to `spender` by the caller.
     *
     * This is an alternative to {approve} that can be used as a mitigation for
     * problems described in {IERC20-approve}.
     *
     * Emits an {Approval} event indicating the updated allowance.
     *
     * Requirements:
     *
     * - `spender` cannot be the zero address.
     * - `spender` must have allowance for the caller of at least
     * `subtractedValue`.
     */
    function decreaseAllowance(address spender, uint256 subtractedValue) public virtual returns (bool) {
        uint256 currentAllowance = _allowances[_msgSender()][spender];
        require(currentAllowance \u003e= subtractedValue, \"ERC20: decreased allowance below zero\");
        unchecked {
            _approve(_msgSender(), spender, currentAllowance - subtractedValue);
        }

        return true;
    }

    /**
     * @dev Moves `amount` of tokens from `sender` to `recipient`.
     *
     * This internal function is equivalent to {transfer}, and can be used to
     * e.g. implement automatic token fees, slashing mechanisms, etc.
     *
     * Emits a {Transfer} event.
     *
     * Requirements:
     *
     * - `sender` cannot be the zero address.
     * - `recipient` cannot be the zero address.
     * - `sender` must have a balance of at least `amount`.
     */
    function _transfer(
        address sender,
        address recipient,
        uint256 amount
    ) internal virtual {
        require(sender != address(0), \"ERC20: transfer from the zero address\");
        require(recipient != address(0), \"ERC20: transfer to the zero address\");

        _beforeTokenTransfer(sender, recipient, amount);

        uint256 senderBalance = _balances[sender];
        require(senderBalance \u003e= amount, \"ERC20: transfer amount exceeds balance\");
        unchecked {
            _balances[sender] = senderBalance - amount;
        }
        _balances[recipient] += amount;

        emit Transfer(sender, recipient, amount);

        _afterTokenTransfer(sender, recipient, amount);
    }

    /** @dev Creates `amount` tokens and assigns them to `account`, increasing
     * the total supply.
     *
     * Emits a {Transfer} event with `from` set to the zero address.
     *
     * Requirements:
     *
     * - `account` cannot be the zero address.
     */
    function _mint(address account, uint256 amount) internal virtual {
        require(account != address(0), \"ERC20: mint to the zero address\");

        _beforeTokenTransfer(address(0), account, amount);

        _totalSupply += amount;
        _balances[account] += amount;
        emit Transfer(address(0), account, amount);

        _afterTokenTransfer(address(0), account, amount);
    }

    /**
     * @dev Destroys `amount` tokens from `account`, reducing the
     * total supply.
     *
     * Emits a {Transfer} event with `to` set to the zero address.
     *
     * Requirements:
     *
     * - `account` cannot be the zero address.
     * - `account` must have at least `amount` tokens.
     */
    function _burn(address account, uint256 amount) internal virtual {
        require(account != address(0), \"ERC20: burn from the zero address\");

        _beforeTokenTransfer(account, address(0), amount);

        uint256 accountBalance = _balances[account];
        require(accountBalance \u003e= amount, \"ERC20: burn amount exceeds balance\");
        unchecked {
            _balances[account] = accountBalance - amount;
        }
        _totalSupply -= amount;

        emit Transfer(account, address(0), amount);

        _afterTokenTransfer(account, address(0), amount);
    }

    /**
     * @dev Sets `amount` as the allowance of `spender` over the `owner` s tokens.
     *
     * This internal function is equivalent to `approve`, and can be used to
     * e.g. set automatic allowances for certain subsystems, etc.
     *
     * Emits an {Approval} event.
     *
     * Requirements:
     *
     * - `owner` cannot be the zero address.
     * - `spender` cannot be the zero address.
     */
    function _approve(
        address owner,
        address spender,
        uint256 amount
    ) internal virtual {
        require(owner != address(0), \"ERC20: approve from the zero address\");
        require(spender != address(0), \"ERC20: approve to the zero address\");

        _allowances[owner][spender] = amount;
        emit Approval(owner, spender, amount);
    }

    /**
     * @dev Hook that is called before any transfer of tokens. This includes
     * minting and burning.
     *
     * Calling conditions:
     *
     * - when `from` and `to` are both non-zero, `amount` of ``from``\u0027s tokens
     * will be transferred to `to`.
     * - when `from` is zero, `amount` tokens will be minted for `to`.
     * - when `to` is zero, `amount` of ``from``\u0027s tokens will be burned.
     * - `from` and `to` are never both zero.
     *
     * To learn more about hooks, head to xref:ROOT:extending-contracts.adoc#using-hooks[Using Hooks].
     */
    function _beforeTokenTransfer(
        address from,
        address to,
        uint256 amount
    ) internal virtual {}

    /**
     * @dev Hook that is called after any transfer of tokens. This includes
     * minting and burning.
     *
     * Calling conditions:
     *
     * - when `from` and `to` are both non-zero, `amount` of ``from``\u0027s tokens
     * has been transferred to `to`.
     * - when `from` is zero, `amount` tokens have been minted for `to`.
     * - when `to` is zero, `amount` of ``from``\u0027s tokens have been burned.
     * - `from` and `to` are never both zero.
     *
     * To learn more about hooks, head to xref:ROOT:extending-contracts.adoc#using-hooks[Using Hooks].
     */
    function _afterTokenTransfer(
        address from,
        address to,
        uint256 amount
    ) internal virtual {}
}
"},"IERC20.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

/**
 * @dev Interface of the ERC20 standard as defined in the EIP.
 */
interface IERC20 {
    /**
     * @dev Returns the amount of tokens in existence.
     */
    function totalSupply() external view returns (uint256);

    /**
     * @dev Returns the amount of tokens owned by `account`.
     */
    function balanceOf(address account) external view returns (uint256);

    /**
     * @dev Moves `amount` tokens from the caller\u0027s account to `recipient`.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transfer(address recipient, uint256 amount) external returns (bool);

    /**
     * @dev Returns the remaining number of tokens that `spender` will be
     * allowed to spend on behalf of `owner` through {transferFrom}. This is
     * zero by default.
     *
     * This value changes when {approve} or {transferFrom} are called.
     */
    function allowance(address owner, address spender) external view returns (uint256);

    /**
     * @dev Sets `amount` as the allowance of `spender` over the caller\u0027s tokens.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * IMPORTANT: Beware that changing an allowance with this method brings the risk
     * that someone may use both the old and the new allowance by unfortunate
     * transaction ordering. One possible solution to mitigate this race
     * condition is to first reduce the spender\u0027s allowance to 0 and set the
     * desired value afterwards:
     * https://github.com/ethereum/EIPs/issues/20#issuecomment-263524729
     *
     * Emits an {Approval} event.
     */
    function approve(address spender, uint256 amount) external returns (bool);

    /**
     * @dev Moves `amount` tokens from `sender` to `recipient` using the
     * allowance mechanism. `amount` is then deducted from the caller\u0027s
     * allowance.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transferFrom(
        address sender,
        address recipient,
        uint256 amount
    ) external returns (bool);

    /**
     * @dev Emitted when `value` tokens are moved from one account (`from`) to
     * another (`to`).
     *
     * Note that `value` may be zero.
     */
    event Transfer(address indexed from, address indexed to, uint256 value);

    /**
     * @dev Emitted when the allowance of a `spender` for an `owner` is set by
     * a call to {approve}. `value` is the new allowance.
     */
    event Approval(address indexed owner, address indexed spender, uint256 value);
}
"},"IERC20Metadata.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import \"./IERC20.sol\";

/**
 * @dev Interface for the optional metadata functions from the ERC20 standard.
 *
 * _Available since v4.1._
 */
interface IERC20Metadata is IERC20 {
    /**
     * @dev Returns the name of the token.
     */
    function name() external view returns (string memory);

    /**
     * @dev Returns the symbol of the token.
     */
    function symbol() external view returns (string memory);

    /**
     * @dev Returns the decimals places of the token.
     */
    function decimals() external view returns (uint8);
}
"},"Math.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

/**
 * @dev Standard math utilities missing in the Solidity language.
 */
library Math {
    /**
     * @dev Returns the largest of two numbers.
     */
    function max(uint256 a, uint256 b) internal pure returns (uint256) {
        return a \u003e= b ? a : b;
    }

    /**
     * @dev Returns the smallest of two numbers.
     */
    function min(uint256 a, uint256 b) internal pure returns (uint256) {
        return a \u003c b ? a : b;
    }

    /**
     * @dev Returns the average of two numbers. The result is rounded towards
     * zero.
     */
    function average(uint256 a, uint256 b) internal pure returns (uint256) {
        // (a + b) / 2 can overflow.
        return (a \u0026 b) + (a ^ b) / 2;
    }

    /**
     * @dev Returns the ceiling of the division of two numbers.
     *
     * This differs from standard division with `/` in that it rounds up instead
     * of rounding down.
     */
    function ceilDiv(uint256 a, uint256 b) internal pure returns (uint256) {
        // (a + b - 1) / b can overflow on addition, so we distribute.
        return a / b + (a % b == 0 ? 0 : 1);
    }
}
"},"Migrations.sol":{"content":"// SPDX-License-Identifier: MIT
pragma solidity \u003e=0.4.22 \u003c0.9.0;

contract Migrations {
  address public owner = msg.sender;
  uint public last_completed_migration;

  modifier restricted() {
    require(
      msg.sender == owner,
      \"This function is restricted to the contract\u0027s owner\"
    );
    _;
  }

  function setCompleted(uint completed) public restricted {
    last_completed_migration = completed;
  }
}
"},"Ownable.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import \"./Context.sol\";

/**
 * @dev Contract module which provides a basic access control mechanism, where
 * there is an account (an owner) that can be granted exclusive access to
 * specific functions.
 *
 * By default, the owner account will be the one that deploys the contract. This
 * can later be changed with {transferOwnership}.
 *
 * This module is used through inheritance. It will make available the modifier
 * `onlyOwner`, which can be applied to your functions to restrict their use to
 * the owner.
 */
abstract contract Ownable is Context {
    address private _owner;

    event OwnershipTransferred(address indexed previousOwner, address indexed newOwner);

    /**
     * @dev Initializes the contract setting the deployer as the initial owner.
     */
    constructor() {
        _setOwner(_msgSender());
    }

    /**
     * @dev Returns the address of the current owner.
     */
    function owner() public view virtual returns (address) {
        return _owner;
    }

    /**
     * @dev Throws if called by any account other than the owner.
     */
    modifier onlyOwner() {
        require(owner() == _msgSender(), \"Ownable: caller is not the owner\");
        _;
    }

    /**
     * @dev Leaves the contract without owner. It will not be possible to call
     * `onlyOwner` functions anymore. Can only be called by the current owner.
     *
     * NOTE: Renouncing ownership will leave the contract without an owner,
     * thereby removing any functionality that is only available to the owner.
     */
    function renounceOwnership() public virtual onlyOwner {
        _setOwner(address(0));
    }

    /**
     * @dev Transfers ownership of the contract to a new account (`newOwner`).
     * Can only be called by the current owner.
     */
    function transferOwnership(address newOwner) public virtual onlyOwner {
        require(newOwner != address(0), \"Ownable: new owner is the zero address\");
        _setOwner(newOwner);
    }

    function _setOwner(address newOwner) private {
        address oldOwner = _owner;
        _owner = newOwner;
        emit OwnershipTransferred(oldOwner, newOwner);
    }
}

