// SPDX-License-Identifier: MIT
pragma solidity ^0.8.7;

/**
 * @notice An implementation of Perlin Noise that uses 16 bit fixed point arithmetic.
 */
library PerlinNoise {

\t/**
\t * @notice Computes the noise value for a 2D point.
\t *
\t * @param x the x coordinate.
\t * @param y the y coordinate.
\t *
\t * @dev This function should be kept public. Inlining the bytecode for this function
\t *      into other functions could explode its compiled size because of how `ftable`
\t *      and `ptable` were written.
\t */
\tfunction noise2d(int256 x, int256 y) public pure returns (int256) {
\t\tint256 temp = ptable(x \u003e\u003e 16 \u0026 0xff /* Unit square X */);

\t\tint256 a = ptable((temp \u003e\u003e 8  ) + (y \u003e\u003e 16 \u0026 0xff /* Unit square Y */));
\t\tint256 b = ptable((temp \u0026 0xff) + (y \u003e\u003e 16 \u0026 0xff                    ));

\t\tx \u0026= 0xffff; // Square relative X
\t\ty \u0026= 0xffff; // Square relative Y

\t\tint256 u = fade(x);

\t\tint256 c = lerp(u, grad2(a \u003e\u003e 8  , x, y        ), grad2(b \u003e\u003e 8  , x-0x10000, y        ));
\t\tint256 d = lerp(u, grad2(a \u0026 0xff, x, y-0x10000), grad2(b \u0026 0xff, x-0x10000, y-0x10000));

\t\treturn lerp(fade(y), c, d);
\t}

\t/**
\t * @notice Computes the noise value for a 3D point.
\t *
\t * @param x the x coordinate.
\t * @param y the y coordinate.
\t * @param z the z coordinate.
\t *
\t * @dev This function should be kept public. Inlining the bytecode for this function
\t *      into other functions could explode its compiled size because of how `ftable`
\t *      and `ptable` were written.
\t */
\tfunction noise3d(int256 x, int256 y, int256 z) public pure returns (int256) {
\t\tint256[7] memory scratch = [
\t\t\tx \u003e\u003e 16 \u0026 0xff,  // Unit cube X
\t\t\ty \u003e\u003e 16 \u0026 0xff,  // Unit cube Y
\t\t\tz \u003e\u003e 16 \u0026 0xff,  // Unit cube Z
\t\t\t0, 0, 0, 0
\t\t];

\t\tx \u0026= 0xffff; // Cube relative X
\t\ty \u0026= 0xffff; // Cube relative Y
\t\tz \u0026= 0xffff; // Cube relative Z

\t\t// Temporary variables used for intermediate calculations.
\t\tint256 u;
\t\tint256 v;

\t\tv = ptable(scratch[0]);

\t\tu = ptable((v \u003e\u003e 8  ) + scratch[1]);
\t\tv = ptable((v \u0026 0xff) + scratch[1]);

\t\tscratch[3] = ptable((u \u003e\u003e 8  ) + scratch[2]);
\t\tscratch[4] = ptable((u \u0026 0xff) + scratch[2]);
\t\tscratch[5] = ptable((v \u003e\u003e 8  ) + scratch[2]);
\t\tscratch[6] = ptable((v \u0026 0xff) + scratch[2]);

\t\tint256 a;
\t\tint256 b;
\t\tint256 c;

\t\tu = fade(x);
\t\tv = fade(y);

\t\ta = lerp(u, grad3(scratch[3] \u003e\u003e 8, x, y        , z), grad3(scratch[5] \u003e\u003e 8, x-0x10000, y        , z));
\t\tb = lerp(u, grad3(scratch[4] \u003e\u003e 8, x, y-0x10000, z), grad3(scratch[6] \u003e\u003e 8, x-0x10000, y-0x10000, z));
\t\tc = lerp(v, a, b);

\t\ta = lerp(u, grad3(scratch[3] \u0026 0xff, x, y        , z-0x10000), grad3(scratch[5] \u0026 0xff, x-0x10000, y        , z-0x10000));
\t\tb = lerp(u, grad3(scratch[4] \u0026 0xff, x, y-0x10000, z-0x10000), grad3(scratch[6] \u0026 0xff, x-0x10000, y-0x10000, z-0x10000));

\t\treturn lerp(fade(z), c, lerp(v, a, b));
\t}

\t/**
\t * @notice Computes the linear interpolation between two values, `a` and `b`, using fixed point arithmetic.
\t *
\t * @param t the time value of the equation.
\t * @param a the lower point.
\t * @param b the upper point.
\t */
\tfunction lerp(int256 t, int256 a, int256 b) internal pure returns (int256) {
\t\treturn a + (t * (b - a) \u003e\u003e 12);
\t}

\t/**
\t * @notice Applies the fade function to a value.
\t *
\t * @param t the time value of the equation.
\t *
\t * @dev The polynomial for this function is: 6t^4-15t^4+10t^3.
\t */
\tfunction fade(int256 t) internal pure returns (int256) {
\t\tint256 n = ftable(t \u003e\u003e 8);

\t\t// Lerp between the two points grabbed from the fade table.
\t\t(int256 lower, int256 upper) = (n \u003e\u003e 12, n \u0026 0xfff);
\t\treturn lower + ((t \u0026 0xff) * (upper - lower) \u003e\u003e 8);
\t}

\t/**
\t  * @notice Computes the gradient value for a 2D point.
\t  *
\t  * @param h the hash value to use for picking the vector.
\t  * @param x the x coordinate of the point.
\t  * @param y the y coordinate of the point.
\t  */
\tfunction grad2(int256 h, int256 x, int256 y) internal pure returns (int256) {
\t\th \u0026= 3;

\t\tint256 u;
\t\tif (h \u0026 0x1 == 0) {
\t\t\tu = x;
\t\t} else {
\t\t\tu = -x;
\t\t}

\t\tint256 v;
\t\tif (h \u003c 2) {
\t\t\tv = y;
\t\t} else {
\t\t\tv = -y;
\t\t}

\t\treturn u + v;
\t}

\t/**
\t * @notice Computes the gradient value for a 3D point.
\t *
\t * @param h the hash value to use for picking the vector.
\t * @param x the x coordinate of the point.
\t * @param y the y coordinate of the point.
\t * @param z the z coordinate of the point.
\t */
\tfunction grad3(int256 h, int256 x, int256 y, int256 z) internal pure returns (int256) {
\t\th \u0026= 0xf;

\t\tint256 u;
\t\tif (h \u003c 8) {
\t\t\tu = x;
\t\t} else {
\t\t\tu = y;
\t\t}

\t\tint256 v;
\t\tif (h \u003c 4) {
\t\t\tv = y;
\t\t} else if (h == 12 || h == 14) {
\t\t\tv = x;
\t\t} else {
\t\t\tv = z;
\t\t}

\t\tif ((h \u0026 0x1) != 0) {
\t\t\tu = -u;
\t\t}

\t\tif ((h \u0026 0x2) != 0) {
\t\t\tv = -v;
\t\t}

\t\treturn u + v;
\t}

\t/**
\t * @notice Gets a subsequent values in the permutation table at an index. The values are encoded
\t *         into a single 24 bit integer with the  value at the specified index being the most
\t *         significant 12 bits and the subsequent value being the least significant 12 bits.
\t *
\t * @param i the index in the permutation table.
\t *
\t * @dev The values from the table are mapped out into a binary tree for faster lookups.
\t *      Looking up any value in the table in this implementation is is O(8), in
\t *      the implementation of sequential if statements it is O(255).
\t *
\t * @dev The body of this function is autogenerated. Check out the \u0027gen-ptable\u0027 script.
\t */
\tfunction ptable(int256 i) internal pure returns (int256) {
\t\ti \u0026= 0xff;

\t\tif (i \u003c= 127) {
\t\t\tif (i \u003c= 63) {
\t\t\t\tif (i \u003c= 31) {
\t\t\t\t\tif (i \u003c= 15) {
\t\t\t\t\t\tif (i \u003c= 7) {
\t\t\t\t\t\t\tif (i \u003c= 3) {
\t\t\t\t\t\t\t\tif (i \u003c= 1) {
\t\t\t\t\t\t\t\t\tif (i == 0) { return 38816; } else { return 41097; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 2) { return 35163; } else { return 23386; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\tif (i \u003c= 5) {
\t\t\t\t\t\t\t\t\tif (i == 4) { return 23055; } else { return 3971; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 6) { return 33549; } else { return 3529; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t}
\t\t\t\t\t\t} else {
\t\t\t\t\t\t\tif (i \u003c= 11) {
\t\t\t\t\t\t\t\tif (i \u003c= 9) {
\t\t\t\t\t\t\t\t\tif (i == 8) { return 51551; } else { return 24416; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 10) { return 24629; } else { return 13762; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\tif (i \u003c= 13) {
\t\t\t\t\t\t\t\t\tif (i == 12) { return 49897; } else { return 59655; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 14) { return 2017; } else { return 57740; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t}
\t\t\t\t\t\t}
\t\t\t\t\t} else {
\t\t\t\t\t\tif (i \u003c= 23) {
\t\t\t\t\t\t\tif (i \u003c= 19) {
\t\t\t\t\t\t\t\tif (i \u003c= 17) {
\t\t\t\t\t\t\t\t\tif (i == 16) { return 35876; } else { return 9319; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 18) { return 26398; } else { return 7749; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\tif (i \u003c= 21) {
\t\t\t\t\t\t\t\t\tif (i == 20) { return 17806; } else { return 36360; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 22) { return 2147; } else { return 25381; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t}
\t\t\t\t\t\t} else {
\t\t\t\t\t\t\tif (i \u003c= 27) {
\t\t\t\t\t\t\t\tif (i \u003c= 25) {
\t\t\t\t\t\t\t\t\tif (i == 24) { return 9712; } else { return 61461; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 26) { return 5386; } else { return 2583; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\tif (i \u003c= 29) {
\t\t\t\t\t\t\t\t\tif (i == 28) { return 6078; } else { return 48646; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 30) { return 1684; } else { return 38135; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t}
\t\t\t\t\t\t}
\t\t\t\t\t}
\t\t\t\t} else {
\t\t\t\t\tif (i \u003c= 47) {
\t\t\t\t\t\tif (i \u003c= 39) {
\t\t\t\t\t\t\tif (i \u003c= 35) {
\t\t\t\t\t\t\t\tif (i \u003c= 33) {
\t\t\t\t\t\t\t\t\tif (i == 32) { return 63352; } else { return 30954; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 34) { return 59979; } else { return 19200; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\tif (i \u003c= 37) {
\t\t\t\t\t\t\t\t\tif (i == 36) { return 26; } else { return 6853; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 38) { return 50494; } else { return 15966; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t}
\t\t\t\t\t\t} else {
\t\t\t\t\t\t\tif (i \u003c= 43) {
\t\t\t\t\t\t\t\tif (i \u003c= 41) {
\t\t\t\t\t\t\t\t\tif (i == 40) { return 24316; } else { return 64731; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 42) { return 56267; } else { return 52085; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\tif (i \u003c= 45) {
\t\t\t\t\t\t\t\t\tif (i == 44) { return 29987; } else { return 8971; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 46) { return 2848; } else { return 8249; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t}
\t\t\t\t\t\t}
\t\t\t\t\t} else {
\t\t\t\t\t\tif (i \u003c= 55) {
\t\t\t\t\t\t\tif (i \u003c= 51) {
\t\t\t\t\t\t\t\tif (i \u003c= 49) {
\t\t\t\t\t\t\t\t\tif (i == 48) { return 14769; } else { return 45345; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 50) { return 8536; } else { return 22765; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\tif (i \u003c= 53) {
\t\t\t\t\t\t\t\t\tif (i == 52) { return 60821; } else { return 38200; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 54) { return 14423; } else { return 22446; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t}
\t\t\t\t\t\t} else {
\t\t\t\t\t\t\tif (i \u003c= 59) {
\t\t\t\t\t\t\t\tif (i \u003c= 57) {
\t\t\t\t\t\t\t\t\tif (i == 56) { return 44564; } else { return 5245; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 58) { return 32136; } else { return 34987; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\tif (i \u003c= 61) {
\t\t\t\t\t\t\t\t\tif (i == 60) { return 43944; } else { return 43076; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 62) { return 17583; } else { return 44874; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t}
\t\t\t\t\t\t}
\t\t\t\t\t}
\t\t\t\t}
\t\t\t} else {
\t\t\t\tif (i \u003c= 95) {
\t\t\t\t\tif (i \u003c= 79) {
\t\t\t\t\t\tif (i \u003c= 71) {
\t\t\t\t\t\t\tif (i \u003c= 67) {
\t\t\t\t\t\t\t\tif (i \u003c= 65) {
\t\t\t\t\t\t\t\t\tif (i == 64) { return 19109; } else { return 42311; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 66) { return 18310; } else { return 34443; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\tif (i \u003c= 69) {
\t\t\t\t\t\t\t\t\tif (i == 68) { return 35632; } else { return 12315; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 70) { return 7078; } else { return 42573; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t}
\t\t\t\t\t\t} else {
\t\t\t\t\t\t\tif (i \u003c= 75) {
\t\t\t\t\t\t\t\tif (i \u003c= 73) {
\t\t\t\t\t\t\t\t\tif (i == 72) { return 19858; } else { return 37534; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 74) { return 40679; } else { return 59219; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\tif (i \u003c= 77) {
\t\t\t\t\t\t\t\t\tif (i == 76) { return 21359; } else { return 28645; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 78) { return 58746; } else { return 31292; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t}
\t\t\t\t\t\t}
\t\t\t\t\t} else {
\t\t\t\t\t\tif (i \u003c= 87) {
\t\t\t\t\t\t\tif (i \u003c= 83) {
\t\t\t\t\t\t\t\tif (i \u003c= 81) {
\t\t\t\t\t\t\t\t\tif (i == 80) { return 15571; } else { return 54149; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 82) { return 34278; } else { return 59100; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\tif (i \u003c= 85) {
\t\t\t\t\t\t\t\t\tif (i == 84) { return 56425; } else { return 26972; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 86) { return 23593; } else { return 10551; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t}
\t\t\t\t\t\t} else {
\t\t\t\t\t\t\tif (i \u003c= 91) {
\t\t\t\t\t\t\t\tif (i \u003c= 89) {
\t\t\t\t\t\t\t\t\tif (i == 88) { return 14126; } else { return 12021; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 90) { return 62760; } else { return 10484; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\tif (i \u003c= 93) {
\t\t\t\t\t\t\t\t\tif (i == 92) { return 62566; } else { return 26255; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 94) { return 36662; } else { return 13889; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t}
\t\t\t\t\t\t}
\t\t\t\t\t}
\t\t\t\t} else {
\t\t\t\t\tif (i \u003c= 111) {
\t\t\t\t\t\tif (i \u003c= 103) {
\t\t\t\t\t\t\tif (i \u003c= 99) {
\t\t\t\t\t\t\t\tif (i \u003c= 97) {
\t\t\t\t\t\t\t\t\tif (i == 96) { return 16665; } else { return 6463; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 98) { return 16289; } else { return 41217; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\tif (i \u003c= 101) {
\t\t\t\t\t\t\t\t\tif (i == 100) { return 472; } else { return 55376; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 102) { return 20553; } else { return 18897; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t}
\t\t\t\t\t\t} else {
\t\t\t\t\t\t\tif (i \u003c= 107) {
\t\t\t\t\t\t\t\tif (i \u003c= 105) {
\t\t\t\t\t\t\t\t\tif (i == 104) { return 53580; } else { return 19588; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 106) { return 33979; } else { return 48080; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\tif (i \u003c= 109) {
\t\t\t\t\t\t\t\t\tif (i == 108) { return 53337; } else { return 22802; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 110) { return 4777; } else { return 43464; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t}
\t\t\t\t\t\t}
\t\t\t\t\t} else {
\t\t\t\t\t\tif (i \u003c= 119) {
\t\t\t\t\t\t\tif (i \u003c= 115) {
\t\t\t\t\t\t\t\tif (i \u003c= 113) {
\t\t\t\t\t\t\t\t\tif (i == 112) { return 51396; } else { return 50311; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 114) { return 34690; } else { return 33396; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\tif (i \u003c= 117) {
\t\t\t\t\t\t\t\t\tif (i == 116) { return 29884; } else { return 48287; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 118) { return 40790; } else { return 22180; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t}
\t\t\t\t\t\t} else {
\t\t\t\t\t\t\tif (i \u003c= 123) {
\t\t\t\t\t\t\t\tif (i \u003c= 121) {
\t\t\t\t\t\t\t\t\tif (i == 120) { return 42084; } else { return 25709; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 122) { return 28102; } else { return 50861; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\tif (i \u003c= 125) {
\t\t\t\t\t\t\t\t\tif (i == 124) { return 44474; } else { return 47619; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 126) { return 832; } else { return 16436; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t}
\t\t\t\t\t\t}
\t\t\t\t\t}
\t\t\t\t}
\t\t\t}
\t\t} else {
\t\t\tif (i \u003c= 191) {
\t\t\t\tif (i \u003c= 159) {
\t\t\t\t\tif (i \u003c= 143) {
\t\t\t\t\t\tif (i \u003c= 135) {
\t\t\t\t\t\t\tif (i \u003c= 131) {
\t\t\t\t\t\t\t\tif (i \u003c= 129) {
\t\t\t\t\t\t\t\t\tif (i == 128) { return 13529; } else { return 55778; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 130) { return 58106; } else { return 64124; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\tif (i \u003c= 133) {
\t\t\t\t\t\t\t\t\tif (i == 132) { return 31867; } else { return 31493; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 134) { return 1482; } else { return 51750; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t}
\t\t\t\t\t\t} else {
\t\t\t\t\t\t\tif (i \u003c= 139) {
\t\t\t\t\t\t\t\tif (i \u003c= 137) {
\t\t\t\t\t\t\t\t\tif (i == 136) { return 9875; } else { return 37750; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 138) { return 30334; } else { return 32511; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\tif (i \u003c= 141) {
\t\t\t\t\t\t\t\t\tif (i == 140) { return 65362; } else { return 21077; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 142) { return 21972; } else { return 54479; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t}
\t\t\t\t\t\t}
\t\t\t\t\t} else {
\t\t\t\t\t\tif (i \u003c= 151) {
\t\t\t\t\t\t\tif (i \u003c= 147) {
\t\t\t\t\t\t\t\tif (i \u003c= 145) {
\t\t\t\t\t\t\t\t\tif (i == 144) { return 53198; } else { return 52795; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 146) { return 15331; } else { return 58159; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\tif (i \u003c= 149) {
\t\t\t\t\t\t\t\t\tif (i == 148) { return 12048; } else { return 4154; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 150) { return 14865; } else { return 4534; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t}
\t\t\t\t\t\t} else {
\t\t\t\t\t\t\tif (i \u003c= 155) {
\t\t\t\t\t\t\t\tif (i \u003c= 153) {
\t\t\t\t\t\t\t\t\tif (i == 152) { return 46781; } else { return 48412; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 154) { return 7210; } else { return 10975; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\tif (i \u003c= 157) {
\t\t\t\t\t\t\t\t\tif (i == 156) { return 57271; } else { return 47018; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 158) { return 43733; } else { return 54647; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t}
\t\t\t\t\t\t}
\t\t\t\t\t}
\t\t\t\t} else {
\t\t\t\t\tif (i \u003c= 175) {
\t\t\t\t\t\tif (i \u003c= 167) {
\t\t\t\t\t\t\tif (i \u003c= 163) {
\t\t\t\t\t\t\t\tif (i \u003c= 161) {
\t\t\t\t\t\t\t\t\tif (i == 160) { return 30712; } else { return 63640; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 162) { return 38914; } else { return 556; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\tif (i \u003c= 165) {
\t\t\t\t\t\t\t\t\tif (i == 164) { return 11418; } else { return 39587; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 166) { return 41798; } else { return 18141; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t}
\t\t\t\t\t\t} else {
\t\t\t\t\t\t\tif (i \u003c= 171) {
\t\t\t\t\t\t\t\tif (i \u003c= 169) {
\t\t\t\t\t\t\t\t\tif (i == 168) { return 56729; } else { return 39269; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 170) { return 26011; } else { return 39847; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\tif (i \u003c= 173) {
\t\t\t\t\t\t\t\t\tif (i == 172) { return 42795; } else { return 11180; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 174) { return 44041; } else { return 2433; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t}
\t\t\t\t\t\t}
\t\t\t\t\t} else {
\t\t\t\t\t\tif (i \u003c= 183) {
\t\t\t\t\t\t\tif (i \u003c= 179) {
\t\t\t\t\t\t\t\tif (i \u003c= 177) {
\t\t\t\t\t\t\t\t\tif (i == 176) { return 33046; } else { return 5671; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 178) { return 10237; } else { return 64787; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\tif (i \u003c= 181) {
\t\t\t\t\t\t\t\t\tif (i == 180) { return 4962; } else { return 25196; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 182) { return 27758; } else { return 28239; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t}
\t\t\t\t\t\t} else {
\t\t\t\t\t\t\tif (i \u003c= 187) {
\t\t\t\t\t\t\t\tif (i \u003c= 185) {
\t\t\t\t\t\t\t\t\tif (i == 184) { return 20337; } else { return 29152; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 186) { return 57576; } else { return 59570; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\tif (i \u003c= 189) {
\t\t\t\t\t\t\t\t\tif (i == 188) { return 45753; } else { return 47472; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 190) { return 28776; } else { return 26842; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t}
\t\t\t\t\t\t}
\t\t\t\t\t}
\t\t\t\t}
\t\t\t} else {
\t\t\t\tif (i \u003c= 223) {
\t\t\t\t\tif (i \u003c= 207) {
\t\t\t\t\t\tif (i \u003c= 199) {
\t\t\t\t\t\t\tif (i \u003c= 195) {
\t\t\t\t\t\t\t\tif (i \u003c= 193) {
\t\t\t\t\t\t\t\t\tif (i == 192) { return 56054; } else { return 63073; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 194) { return 25060; } else { return 58619; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\tif (i \u003c= 197) {
\t\t\t\t\t\t\t\t\tif (i == 196) { return 64290; } else { return 8946; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 198) { return 62145; } else { return 49646; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t}
\t\t\t\t\t\t} else {
\t\t\t\t\t\t\tif (i \u003c= 203) {
\t\t\t\t\t\t\t\tif (i \u003c= 201) {
\t\t\t\t\t\t\t\t\tif (i == 200) { return 61138; } else { return 53904; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 202) { return 36876; } else { return 3263; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\tif (i \u003c= 205) {
\t\t\t\t\t\t\t\t\tif (i == 204) { return 49075; } else { return 45986; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 206) { return 41713; } else { return 61777; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t}
\t\t\t\t\t\t}
\t\t\t\t\t} else {
\t\t\t\t\t\tif (i \u003c= 215) {
\t\t\t\t\t\t\tif (i \u003c= 211) {
\t\t\t\t\t\t\t\tif (i \u003c= 209) {
\t\t\t\t\t\t\t\t\tif (i == 208) { return 20787; } else { return 13201; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 210) { return 37355; } else { return 60409; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\tif (i \u003c= 213) {
\t\t\t\t\t\t\t\t\tif (i == 212) { return 63758; } else { return 3823; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 214) { return 61291; } else { return 27441; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t}
\t\t\t\t\t\t} else {
\t\t\t\t\t\t\tif (i \u003c= 219) {
\t\t\t\t\t\t\t\tif (i \u003c= 217) {
\t\t\t\t\t\t\t\t\tif (i == 216) { return 12736; } else { return 49366; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 218) { return 54815; } else { return 8117; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\tif (i \u003c= 221) {
\t\t\t\t\t\t\t\t\tif (i == 220) { return 46535; } else { return 51050; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 222) { return 27293; } else { return 40376; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t}
\t\t\t\t\t\t}
\t\t\t\t\t}
\t\t\t\t} else {
\t\t\t\t\tif (i \u003c= 239) {
\t\t\t\t\t\tif (i \u003c= 231) {
\t\t\t\t\t\t\tif (i \u003c= 227) {
\t\t\t\t\t\t\t\tif (i \u003c= 225) {
\t\t\t\t\t\t\t\t\tif (i == 224) { return 47188; } else { return 21708; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 226) { return 52400; } else { return 45171; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\tif (i \u003c= 229) {
\t\t\t\t\t\t\t\t\tif (i == 228) { return 29561; } else { return 31026; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 230) { return 12845; } else { return 11647; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t}
\t\t\t\t\t\t} else {
\t\t\t\t\t\t\tif (i \u003c= 235) {
\t\t\t\t\t\t\t\tif (i \u003c= 233) {
\t\t\t\t\t\t\t\t\tif (i == 232) { return 32516; } else { return 1174; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 234) { return 38654; } else { return 65162; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\tif (i \u003c= 237) {
\t\t\t\t\t\t\t\t\tif (i == 236) { return 35564; } else { return 60621; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 238) { return 52573; } else { return 24030; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t}
\t\t\t\t\t\t}
\t\t\t\t\t} else {
\t\t\t\t\t\tif (i \u003c= 247) {
\t\t\t\t\t\t\tif (i \u003c= 243) {
\t\t\t\t\t\t\t\tif (i \u003c= 241) {
\t\t\t\t\t\t\t\t\tif (i == 240) { return 56946; } else { return 29251; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 242) { return 17181; } else { return 7448; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\tif (i \u003c= 245) {
\t\t\t\t\t\t\t\t\tif (i == 244) { return 6216; } else { return 18675; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 246) { return 62349; } else { return 36224; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t}
\t\t\t\t\t\t} else {
\t\t\t\t\t\t\tif (i \u003c= 251) {
\t\t\t\t\t\t\t\tif (i \u003c= 249) {
\t\t\t\t\t\t\t\t\tif (i == 248) { return 32963; } else { return 49998; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 250) { return 20034; } else { return 17111; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\tif (i \u003c= 253) {
\t\t\t\t\t\t\t\t\tif (i == 252) { return 55101; } else { return 15772; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 254) { return 40116; } else { return 46231; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t}
\t\t\t\t\t\t}
\t\t\t\t\t}
\t\t\t\t}
\t\t\t}
\t\t}

\t}

\t/**
\t * @notice Gets subsequent values in the fade table at an index. The values are encoded
\t *         into a single 16 bit integer with the value at the specified index being the most
\t *         significant 8 bits and the subsequent value being the least significant 8 bits.
\t *
\t * @param i the index in the fade table.
\t *
\t * @dev The values from the table are mapped out into a binary tree for faster lookups.
\t *      Looking up any value in the table in this implementation is is O(8), in
\t *      the implementation of sequential if statements it is O(256).
\t *
\t * @dev The body of this function is autogenerated. Check out the \u0027gen-ftable\u0027 script.
\t */
\tfunction ftable(int256 i) internal pure returns (int256) {
\t\tif (i \u003c= 127) {
\t\t\tif (i \u003c= 63) {
\t\t\t\tif (i \u003c= 31) {
\t\t\t\t\tif (i \u003c= 15) {
\t\t\t\t\t\tif (i \u003c= 7) {
\t\t\t\t\t\t\tif (i \u003c= 3) {
\t\t\t\t\t\t\t\tif (i \u003c= 1) {
\t\t\t\t\t\t\t\t\tif (i == 0) { return 0; } else { return 0; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 2) { return 0; } else { return 0; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\tif (i \u003c= 5) {
\t\t\t\t\t\t\t\t\tif (i == 4) { return 0; } else { return 0; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 6) { return 0; } else { return 1; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t}
\t\t\t\t\t\t} else {
\t\t\t\t\t\t\tif (i \u003c= 11) {
\t\t\t\t\t\t\t\tif (i \u003c= 9) {
\t\t\t\t\t\t\t\t\tif (i == 8) { return 4097; } else { return 4098; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 10) { return 8195; } else { return 12291; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\tif (i \u003c= 13) {
\t\t\t\t\t\t\t\t\tif (i == 12) { return 12292; } else { return 16390; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 14) { return 24583; } else { return 28681; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t}
\t\t\t\t\t\t}
\t\t\t\t\t} else {
\t\t\t\t\t\tif (i \u003c= 23) {
\t\t\t\t\t\t\tif (i \u003c= 19) {
\t\t\t\t\t\t\t\tif (i \u003c= 17) {
\t\t\t\t\t\t\t\t\tif (i == 16) { return 36874; } else { return 40972; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 18) { return 49166; } else { return 57361; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\tif (i \u003c= 21) {
\t\t\t\t\t\t\t\t\tif (i == 20) { return 69651; } else { return 77846; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 22) { return 90137; } else { return 102429; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t}
\t\t\t\t\t\t} else {
\t\t\t\t\t\t\tif (i \u003c= 27) {
\t\t\t\t\t\t\t\tif (i \u003c= 25) {
\t\t\t\t\t\t\t\t\tif (i == 24) { return 118816; } else { return 131108; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 26) { return 147496; } else { return 163885; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\tif (i \u003c= 29) {
\t\t\t\t\t\t\t\t\tif (i == 28) { return 184369; } else { return 200758; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 30) { return 221244; } else { return 245825; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t}
\t\t\t\t\t\t}
\t\t\t\t\t}
\t\t\t\t} else {
\t\t\t\t\tif (i \u003c= 47) {
\t\t\t\t\t\tif (i \u003c= 39) {
\t\t\t\t\t\t\tif (i \u003c= 35) {
\t\t\t\t\t\t\t\tif (i \u003c= 33) {
\t\t\t\t\t\t\t\t\tif (i == 32) { return 266311; } else { return 290893; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 34) { return 315476; } else { return 344155; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\tif (i \u003c= 37) {
\t\t\t\t\t\t\t\t\tif (i == 36) { return 372834; } else { return 401513; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 38) { return 430193; } else { return 462969; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t}
\t\t\t\t\t\t} else {
\t\t\t\t\t\t\tif (i \u003c= 43) {
\t\t\t\t\t\t\t\tif (i \u003c= 41) {
\t\t\t\t\t\t\t\t\tif (i == 40) { return 495746; } else { return 532619; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 42) { return 569492; } else { return 606366; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\tif (i \u003c= 45) {
\t\t\t\t\t\t\t\t\tif (i == 44) { return 647335; } else { return 684210; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 46) { return 729276; } else { return 770247; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t}
\t\t\t\t\t\t}
\t\t\t\t\t} else {
\t\t\t\t\t\tif (i \u003c= 55) {
\t\t\t\t\t\t\tif (i \u003c= 51) {
\t\t\t\t\t\t\t\tif (i \u003c= 49) {
\t\t\t\t\t\t\t\t\tif (i == 48) { return 815315; } else { return 864478; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 50) { return 909546; } else { return 958711; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\tif (i \u003c= 53) {
\t\t\t\t\t\t\t\t\tif (i == 52) { return 1011971; } else { return 1061137; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 54) { return 1118494; } else { return 1171756; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t}
\t\t\t\t\t\t} else {
\t\t\t\t\t\t\tif (i \u003c= 59) {
\t\t\t\t\t\t\t\tif (i \u003c= 57) {
\t\t\t\t\t\t\t\t\tif (i == 56) { return 1229114; } else { return 1286473; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 58) { return 1347928; } else { return 1409383; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\tif (i \u003c= 61) {
\t\t\t\t\t\t\t\t\tif (i == 60) { return 1470838; } else { return 1532294; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 62) { return 1597847; } else { return 1667496; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t}
\t\t\t\t\t\t}
\t\t\t\t\t}
\t\t\t\t}
\t\t\t} else {
\t\t\t\tif (i \u003c= 95) {
\t\t\t\t\tif (i \u003c= 79) {
\t\t\t\t\t\tif (i \u003c= 71) {
\t\t\t\t\t\t\tif (i \u003c= 67) {
\t\t\t\t\t\t\t\tif (i \u003c= 65) {
\t\t\t\t\t\t\t\t\tif (i == 64) { return 1737145; } else { return 1806794; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 66) { return 1876444; } else { return 1950190; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\tif (i \u003c= 69) {
\t\t\t\t\t\t\t\t\tif (i == 68) { return 2023936; } else { return 2097683; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 70) { return 2175526; } else { return 2253370; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t}
\t\t\t\t\t\t} else {
\t\t\t\t\t\t\tif (i \u003c= 75) {
\t\t\t\t\t\t\t\tif (i \u003c= 73) {
\t\t\t\t\t\t\t\t\tif (i == 72) { return 2335309; } else { return 2413153; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 74) { return 2495094; } else { return 2581131; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\tif (i \u003c= 77) {
\t\t\t\t\t\t\t\t\tif (i == 76) { return 2667168; } else { return 2753205; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 78) { return 2839243; } else { return 2929377; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t}
\t\t\t\t\t\t}
\t\t\t\t\t} else {
\t\t\t\t\t\tif (i \u003c= 87) {
\t\t\t\t\t\t\tif (i \u003c= 83) {
\t\t\t\t\t\t\t\tif (i \u003c= 81) {
\t\t\t\t\t\t\t\t\tif (i == 80) { return 3019511; } else { return 3109646; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 82) { return 3203877; } else { return 3298108; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\tif (i \u003c= 85) {
\t\t\t\t\t\t\t\t\tif (i == 84) { return 3392339; } else { return 3486571; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 86) { return 3584899; } else { return 3683227; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t}
\t\t\t\t\t\t} else {
\t\t\t\t\t\t\tif (i \u003c= 91) {
\t\t\t\t\t\t\t\tif (i \u003c= 89) {
\t\t\t\t\t\t\t\t\tif (i == 88) { return 3781556; } else { return 3883981; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 90) { return 3986406; } else { return 4088831; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\tif (i \u003c= 93) {
\t\t\t\t\t\t\t\t\tif (i == 92) { return 4191257; } else { return 4297778; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 94) { return 4400204; } else { return 4506727; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t}
\t\t\t\t\t\t}
\t\t\t\t\t}
\t\t\t\t} else {
\t\t\t\t\tif (i \u003c= 111) {
\t\t\t\t\t\tif (i \u003c= 103) {
\t\t\t\t\t\t\tif (i \u003c= 99) {
\t\t\t\t\t\t\t\tif (i \u003c= 97) {
\t\t\t\t\t\t\t\t\tif (i == 96) { return 4617345; } else { return 4723868; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 98) { return 4834487; } else { return 4945106; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\tif (i \u003c= 101) {
\t\t\t\t\t\t\t\t\tif (i == 100) { return 5055725; } else { return 5166345; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 102) { return 5281060; } else { return 5391680; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t}
\t\t\t\t\t\t} else {
\t\t\t\t\t\t\tif (i \u003c= 107) {
\t\t\t\t\t\t\t\tif (i \u003c= 105) {
\t\t\t\t\t\t\t\t\tif (i == 104) { return 5506396; } else { return 5621112; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 106) { return 5735829; } else { return 5854641; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\tif (i \u003c= 109) {
\t\t\t\t\t\t\t\t\tif (i == 108) { return 5969358; } else { return 6088171; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 110) { return 6206983; } else { return 6321700; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t}
\t\t\t\t\t\t}
\t\t\t\t\t} else {
\t\t\t\t\t\tif (i \u003c= 119) {
\t\t\t\t\t\t\tif (i \u003c= 115) {
\t\t\t\t\t\t\t\tif (i \u003c= 113) {
\t\t\t\t\t\t\t\t\tif (i == 112) { return 6440514; } else { return 6563423; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 114) { return 6682236; } else { return 6801050; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\tif (i \u003c= 117) {
\t\t\t\t\t\t\t\t\tif (i == 116) { return 6923959; } else { return 7042773; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 118) { return 7165682; } else { return 7284496; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t}
\t\t\t\t\t\t} else {
\t\t\t\t\t\t\tif (i \u003c= 123) {
\t\t\t\t\t\t\t\tif (i \u003c= 121) {
\t\t\t\t\t\t\t\t\tif (i == 120) { return 7407406; } else { return 7530316; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 122) { return 7653226; } else { return 7776136; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\tif (i \u003c= 125) {
\t\t\t\t\t\t\t\t\tif (i == 124) { return 7899046; } else { return 8021956; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 126) { return 8144866; } else { return 8267776; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t}
\t\t\t\t\t\t}
\t\t\t\t\t}
\t\t\t\t}
\t\t\t}
\t\t} else {
\t\t\tif (i \u003c= 191) {
\t\t\t\tif (i \u003c= 159) {
\t\t\t\t\tif (i \u003c= 143) {
\t\t\t\t\t\tif (i \u003c= 135) {
\t\t\t\t\t\t\tif (i \u003c= 131) {
\t\t\t\t\t\t\t\tif (i \u003c= 129) {
\t\t\t\t\t\t\t\t\tif (i == 128) { return 8390685; } else { return 8509499; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 130) { return 8632409; } else { return 8755319; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\tif (i \u003c= 133) {
\t\t\t\t\t\t\t\t\tif (i == 132) { return 8878229; } else { return 9001139; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 134) { return 9124049; } else { return 9246959; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t}
\t\t\t\t\t\t} else {
\t\t\t\t\t\t\tif (i \u003c= 139) {
\t\t\t\t\t\t\t\tif (i \u003c= 137) {
\t\t\t\t\t\t\t\t\tif (i == 136) { return 9369869; } else { return 9492778; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 138) { return 9611592; } else { return 9734501; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\tif (i \u003c= 141) {
\t\t\t\t\t\t\t\t\tif (i == 140) { return 9853315; } else { return 9976224; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 142) { return 10095037; } else { return 10213851; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t}
\t\t\t\t\t\t}
\t\t\t\t\t} else {
\t\t\t\t\t\tif (i \u003c= 151) {
\t\t\t\t\t\t\tif (i \u003c= 147) {
\t\t\t\t\t\t\t\tif (i \u003c= 145) {
\t\t\t\t\t\t\t\t\tif (i == 144) { return 10336760; } else { return 10455572; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 146) { return 10570289; } else { return 10689102; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\tif (i \u003c= 149) {
\t\t\t\t\t\t\t\t\tif (i == 148) { return 10807914; } else { return 10922631; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 150) { return 11041443; } else { return 11156159; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t}
\t\t\t\t\t\t} else {
\t\t\t\t\t\t\tif (i \u003c= 155) {
\t\t\t\t\t\t\t\tif (i \u003c= 153) {
\t\t\t\t\t\t\t\t\tif (i == 152) { return 11270875; } else { return 11385590; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 154) { return 11496210; } else { return 11610925; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\tif (i \u003c= 157) {
\t\t\t\t\t\t\t\t\tif (i == 156) { return 11721544; } else { return 11832163; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 158) { return 11942782; } else { return 12053400; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t}
\t\t\t\t\t\t}
\t\t\t\t\t}
\t\t\t\t} else {
\t\t\t\t\tif (i \u003c= 175) {
\t\t\t\t\t\tif (i \u003c= 167) {
\t\t\t\t\t\t\tif (i \u003c= 163) {
\t\t\t\t\t\t\t\tif (i \u003c= 161) {
\t\t\t\t\t\t\t\t\tif (i == 160) { return 12159923; } else { return 12270541; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 162) { return 12377062; } else { return 12479488; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\tif (i \u003c= 165) {
\t\t\t\t\t\t\t\t\tif (i == 164) { return 12586009; } else { return 12688434; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 166) { return 12790859; } else { return 12893284; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t}
\t\t\t\t\t\t} else {
\t\t\t\t\t\t\tif (i \u003c= 171) {
\t\t\t\t\t\t\t\tif (i \u003c= 169) {
\t\t\t\t\t\t\t\t\tif (i == 168) { return 12995708; } else { return 13094036; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 170) { return 13192364; } else { return 13290691; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\tif (i \u003c= 173) {
\t\t\t\t\t\t\t\t\tif (i == 172) { return 13384922; } else { return 13479153; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 174) { return 13573384; } else { return 13667614; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t}
\t\t\t\t\t\t}
\t\t\t\t\t} else {
\t\t\t\t\t\tif (i \u003c= 183) {
\t\t\t\t\t\t\tif (i \u003c= 179) {
\t\t\t\t\t\t\t\tif (i \u003c= 177) {
\t\t\t\t\t\t\t\t\tif (i == 176) { return 13757748; } else { return 13847882; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 178) { return 13938015; } else { return 14024052; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\tif (i \u003c= 181) {
\t\t\t\t\t\t\t\t\tif (i == 180) { return 14110089; } else { return 14196126; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 182) { return 14282162; } else { return 14364101; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t}
\t\t\t\t\t\t} else {
\t\t\t\t\t\t\tif (i \u003c= 187) {
\t\t\t\t\t\t\t\tif (i \u003c= 185) {
\t\t\t\t\t\t\t\t\tif (i == 184) { return 14441945; } else { return 14523884; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 186) { return 14601727; } else { return 14679569; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\tif (i \u003c= 189) {
\t\t\t\t\t\t\t\t\tif (i == 188) { return 14753315; } else { return 14827061; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 190) { return 14900806; } else { return 14970456; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t}
\t\t\t\t\t\t}
\t\t\t\t\t}
\t\t\t\t}
\t\t\t} else {
\t\t\t\tif (i \u003c= 223) {
\t\t\t\t\tif (i \u003c= 207) {
\t\t\t\t\t\tif (i \u003c= 199) {
\t\t\t\t\t\t\tif (i \u003c= 195) {
\t\t\t\t\t\t\t\tif (i \u003c= 193) {
\t\t\t\t\t\t\t\t\tif (i == 192) { return 15044200; } else { return 15109753; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 194) { return 15179401; } else { return 15244952; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\tif (i \u003c= 197) {
\t\t\t\t\t\t\t\t\tif (i == 196) { return 15306407; } else { return 15367862; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 198) { return 15429317; } else { return 15490771; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t}
\t\t\t\t\t\t} else {
\t\t\t\t\t\t\tif (i \u003c= 203) {
\t\t\t\t\t\t\t\tif (i \u003c= 201) {
\t\t\t\t\t\t\t\t\tif (i == 200) { return 15548129; } else { return 15605486; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 202) { return 15658748; } else { return 15716104; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\tif (i \u003c= 205) {
\t\t\t\t\t\t\t\t\tif (i == 204) { return 15765269; } else { return 15818529; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 206) { return 15867692; } else { return 15912760; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t}
\t\t\t\t\t\t}
\t\t\t\t\t} else {
\t\t\t\t\t\tif (i \u003c= 215) {
\t\t\t\t\t\t\tif (i \u003c= 211) {
\t\t\t\t\t\t\t\tif (i \u003c= 209) {
\t\t\t\t\t\t\t\t\tif (i == 208) { return 15961923; } else { return 16006989; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 210) { return 16047960; } else { return 16093025; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\tif (i \u003c= 213) {
\t\t\t\t\t\t\t\t\tif (i == 212) { return 16129899; } else { return 16170868; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 214) { return 16207741; } else { return 16244614; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t}
\t\t\t\t\t\t} else {
\t\t\t\t\t\t\tif (i \u003c= 219) {
\t\t\t\t\t\t\t\tif (i \u003c= 217) {
\t\t\t\t\t\t\t\t\tif (i == 216) { return 16281486; } else { return 16314262; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 218) { return 16347037; } else { return 16375716; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\tif (i \u003c= 221) {
\t\t\t\t\t\t\t\t\tif (i == 220) { return 16404395; } else { return 16433074; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 222) { return 16461752; } else { return 16486334; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t}
\t\t\t\t\t\t}
\t\t\t\t\t}
\t\t\t\t} else {
\t\t\t\t\tif (i \u003c= 239) {
\t\t\t\t\t\tif (i \u003c= 231) {
\t\t\t\t\t\t\tif (i \u003c= 227) {
\t\t\t\t\t\t\t\tif (i \u003c= 225) {
\t\t\t\t\t\t\t\t\tif (i == 224) { return 16510915; } else { return 16531401; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 226) { return 16555982; } else { return 16576466; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\tif (i \u003c= 229) {
\t\t\t\t\t\t\t\t\tif (i == 228) { return 16592855; } else { return 16613339; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 230) { return 16629727; } else { return 16646114; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t}
\t\t\t\t\t\t} else {
\t\t\t\t\t\t\tif (i \u003c= 235) {
\t\t\t\t\t\t\t\tif (i \u003c= 233) {
\t\t\t\t\t\t\t\t\tif (i == 232) { return 16658406; } else { return 16674793; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 234) { return 16687084; } else { return 16699374; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\tif (i \u003c= 237) {
\t\t\t\t\t\t\t\t\tif (i == 236) { return 16707569; } else { return 16719859; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 238) { return 16728053; } else { return 16736246; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t}
\t\t\t\t\t\t}
\t\t\t\t\t} else {
\t\t\t\t\t\tif (i \u003c= 247) {
\t\t\t\t\t\t\tif (i \u003c= 243) {
\t\t\t\t\t\t\t\tif (i \u003c= 241) {
\t\t\t\t\t\t\t\t\tif (i == 240) { return 16740344; } else { return 16748537; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 242) { return 16752635; } else { return 16760828; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\tif (i \u003c= 245) {
\t\t\t\t\t\t\t\t\tif (i == 244) { return 16764924; } else { return 16764925; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 246) { return 16769022; } else { return 16773118; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t}
\t\t\t\t\t\t} else {
\t\t\t\t\t\t\tif (i \u003c= 251) {
\t\t\t\t\t\t\t\tif (i \u003c= 249) {
\t\t\t\t\t\t\t\t\tif (i == 248) { return 16773119; } else { return 16777215; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 250) { return 16777215; } else { return 16777215; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\tif (i \u003c= 253) {
\t\t\t\t\t\t\t\t\tif (i == 252) { return 16777215; } else { return 16777215; }
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tif (i == 254) { return 16777215; } else { return 16777215; }
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t}
\t\t\t\t\t\t}
\t\t\t\t\t}
\t\t\t\t}
\t\t\t}
\t\t}
\t}
}"},"ThreadGlyphs.sol":{"content":"// SPDX-License-Identifier: MIT
pragma solidity ^0.8.7;

/*
.___..             ..__ .      .     
  |  |_ ._. _  _. _|[ __|  .._ |_  __
  |  [ )[  (/,(_](_][_./|\\_|[_)[ )_) 
ThreadGlyphs             ._||        
Collect customizable line art NFTs, all completely decentralized and generated by the Ethereum blockchain.

Website: https://threadglyphs.com/
Created by sol_dev

*/

import \"./PerlinNoise.sol\";

interface Receiver {
\tfunction onERC721Received(address _operator, address _from, uint256 _tokenId, bytes calldata _data) external returns (bytes4);
}

contract Metadata {
\tstring public name = \"ThreadGlyphs\";
\tstring public symbol = unicode\"⩬\";
\tfunction contractURI() external pure returns (string memory) {
\t\treturn \"https://api.threadglyphs.com/metadata\";
\t}
\tfunction baseTokenURI() public pure returns (string memory) {
\t\treturn \"https://api.threadglyphs.com/glyph/metadata/\";
\t}
\tfunction tokenURI(uint256 _tokenId) external pure returns (string memory) {
\t\tbytes memory _base = bytes(baseTokenURI());
\t\tuint256 _digits = 1;
\t\tuint256 _n = _tokenId;
\t\twhile (_n \u003e 9) {
\t\t\t_n /= 10;
\t\t\t_digits++;
\t\t}
\t\tbytes memory _uri = new bytes(_base.length + _digits);
\t\tfor (uint256 i = 0; i \u003c _uri.length; i++) {
\t\t\tif (i \u003c _base.length) {
\t\t\t\t_uri[i] = _base[i];
\t\t\t} else {
\t\t\t\tuint256 _dec = (_tokenId / (10**(_uri.length - i - 1))) % 10;
\t\t\t\t_uri[i] = bytes1(uint8(_dec) + 48);
\t\t\t}
\t\t}
\t\treturn string(_uri);
\t}
}


contract ThreadGlyphs {

\tuint256 constant public MAX_SUPPLY = 512;
\tuint256 constant public MINT_COST = 0.25 ether;
\tuint256 constant private SIZE = 10000;
\tuint256 constant private STEP = 80;
\tuint256 constant private STROKE = 32;
\tuint256 constant private X_PADDING = 1000;
\tuint256 constant private Y_PADDING = 1600;
\tint256 constant private SCALAR = 2**16;

\tstruct User {
\t\tuint256 balance;
\t\tmapping(uint256 =\u003e uint256) list;
\t\tmapping(address =\u003e bool) approved;
\t\tmapping(uint256 =\u003e uint256) indexOf;
\t}

\tstruct Token {
\t\taddress owner;
\t\taddress approved;
\t\tbytes32 seed;
\t\tbytes3 color1;
\t\tbytes3 color2;
\t}

\tstruct Info {
\t\tuint256 totalSupply;
\t\tmapping(uint256 =\u003e Token) list;
\t\tmapping(address =\u003e User) users;
\t\tMetadata metadata;
\t\taddress owner;
\t}
\tInfo private info;

\tmapping(bytes4 =\u003e bool) public supportsInterface;

\tevent Transfer(address indexed from, address indexed to, uint256 indexed tokenId);
\tevent Approval(address indexed owner, address indexed approved, uint256 indexed tokenId);
\tevent ApprovalForAll(address indexed owner, address indexed operator, bool approved);

\tevent Mint(address indexed owner, uint256 indexed tokenId, bytes32 seed);
\tevent Recolor(address indexed owner, uint256 indexed tokenId, bytes3 color, bool isColor1);


\tmodifier _onlyOwner() {
\t\trequire(msg.sender == owner());
\t\t_;
\t}


\tconstructor() {
\t\tinfo.metadata = new Metadata();
\t\tinfo.owner = msg.sender;
\t\tsupportsInterface[0x01ffc9a7] = true; // ERC-165
\t\tsupportsInterface[0x80ac58cd] = true; // ERC-721
\t\tsupportsInterface[0x5b5e139f] = true; // Metadata
\t\tsupportsInterface[0x780e9d63] = true; // Enumerable

\t\tfor (uint256 i = 0; i \u003c 10; i++) {
\t\t\t_mint();
\t\t}
\t}

\tfunction setOwner(address _owner) external _onlyOwner {
\t\tinfo.owner = _owner;
\t}

\tfunction setMetadata(Metadata _metadata) external _onlyOwner {
\t\tinfo.metadata = _metadata;
\t}

\tfunction ownerWithdraw() external _onlyOwner {
\t\tuint256 _balance = address(this).balance;
\t\trequire(_balance \u003e 0);
\t\tpayable(msg.sender).transfer(_balance);
\t}

\t
\treceive() external payable {
\t\tmintMany(msg.value / MINT_COST);
\t}
\t
\tfunction mint() external payable {
\t\tmintMany(1);
\t}

\tfunction mintMany(uint256 _tokens) public payable {
\t\trequire(_tokens \u003e 0);
\t\tuint256 _cost = _tokens * MINT_COST;
\t\trequire(msg.value \u003e= _cost);
\t\tfor (uint256 i = 0; i \u003c _tokens; i++) {
\t\t\t_mint();
\t\t}
\t\tif (msg.value \u003e _cost) {
\t\t\tpayable(msg.sender).transfer(msg.value - _cost);
\t\t}
\t}
\t
\tfunction setColor1(uint256 _tokenId, bytes3 _color) external {
\t\trequire(msg.sender == ownerOf(_tokenId));
\t\tinfo.list[_tokenId].color1 = _color;
\t\temit Recolor(msg.sender, _tokenId, _color, true);
\t}
\t
\tfunction setColor2(uint256 _tokenId, bytes3 _color) external {
\t\trequire(msg.sender == ownerOf(_tokenId));
\t\tinfo.list[_tokenId].color2 = _color;
\t\temit Recolor(msg.sender, _tokenId, _color, false);
\t}
\t
\tfunction approve(address _approved, uint256 _tokenId) external {
\t\trequire(msg.sender == ownerOf(_tokenId));
\t\tinfo.list[_tokenId].approved = _approved;
\t\temit Approval(msg.sender, _approved, _tokenId);
\t}

\tfunction setApprovalForAll(address _operator, bool _approved) external {
\t\tinfo.users[msg.sender].approved[_operator] = _approved;
\t\temit ApprovalForAll(msg.sender, _operator, _approved);
\t}

\tfunction transferFrom(address _from, address _to, uint256 _tokenId) external {
\t\t_transfer(_from, _to, _tokenId);
\t}

\tfunction safeTransferFrom(address _from, address _to, uint256 _tokenId) external {
\t\tsafeTransferFrom(_from, _to, _tokenId, \"\");
\t}

\tfunction safeTransferFrom(address _from, address _to, uint256 _tokenId, bytes memory _data) public {
\t\t_transfer(_from, _to, _tokenId);
\t\tuint32 _size;
\t\tassembly {
\t\t\t_size := extcodesize(_to)
\t\t}
\t\tif (_size \u003e 0) {
\t\t\trequire(Receiver(_to).onERC721Received(msg.sender, _from, _tokenId, _data) == 0x150b7a02);
\t\t}
\t}


\tfunction name() external view returns (string memory) {
\t\treturn info.metadata.name();
\t}

\tfunction symbol() external view returns (string memory) {
\t\treturn info.metadata.symbol();
\t}

\tfunction contractURI() external view returns (string memory) {
\t\treturn info.metadata.contractURI();
\t}

\tfunction baseTokenURI() external view returns (string memory) {
\t\treturn info.metadata.baseTokenURI();
\t}

\tfunction tokenURI(uint256 _tokenId) external view returns (string memory) {
\t\treturn info.metadata.tokenURI(_tokenId);
\t}

\tfunction owner() public view returns (address) {
\t\treturn info.owner;
\t}

\tfunction totalSupply() public view returns (uint256) {
\t\treturn info.totalSupply;
\t}

\tfunction balanceOf(address _owner) public view returns (uint256) {
\t\treturn info.users[_owner].balance;
\t}

\tfunction ownerOf(uint256 _tokenId) public view returns (address) {
\t\trequire(_tokenId \u003c totalSupply());
\t\treturn info.list[_tokenId].owner;
\t}

\tfunction getApproved(uint256 _tokenId) public view returns (address) {
\t\trequire(_tokenId \u003c totalSupply());
\t\treturn info.list[_tokenId].approved;
\t}

\tfunction isApprovedForAll(address _owner, address _operator) public view returns (bool) {
\t\treturn info.users[_owner].approved[_operator];
\t}

\tfunction getSeed(uint256 _tokenId) public view returns (bytes32) {
\t\trequire(_tokenId \u003c totalSupply());
\t\treturn info.list[_tokenId].seed;
\t}

\tfunction getColor1(uint256 _tokenId) public view returns (bytes3) {
\t\trequire(_tokenId \u003c totalSupply());
\t\treturn info.list[_tokenId].color1;
\t}

\tfunction getColor2(uint256 _tokenId) public view returns (bytes3) {
\t\trequire(_tokenId \u003c totalSupply());
\t\treturn info.list[_tokenId].color2;
\t}

\tfunction tokenByIndex(uint256 _index) public view returns (uint256) {
\t\trequire(_index \u003c totalSupply());
\t\treturn _index;
\t}

\tfunction tokenOfOwnerByIndex(address _owner, uint256 _index) public view returns (uint256) {
\t\trequire(_index \u003c balanceOf(_owner));
\t\treturn info.users[_owner].list[_index];
\t}

\tfunction getSVG(uint256 _tokenId) external view returns (string memory svg) {
\t\tstring memory _size = _uint2str(SIZE);
\t\tsvg = string(abi.encodePacked(\"\u003csvg xmlns=\u0027http://www.w3.org/2000/svg\u0027 version=\u00271.1\u0027 width=\u0027\", _size, \"\u0027 height=\u0027\", _size, \"\u0027 viewBox=\u00270 0 \", _size, \" \", _size, \"\u0027\u003e\"));
\t\t(bool[2] memory _bools, uint256[6] memory _ints) = getSettingsCompressed(getSeed(_tokenId));
\t\tsvg = string(abi.encodePacked(svg, \"\u003crect width=\u0027100%\u0027 height=\u0027100%\u0027 fill=\u0027\", _bools[0] ? \"#040404\" : \"#fbfbfb\", \"\u0027 /\u003e\u003cg fill=\u0027none\u0027 stroke-width=\u0027\", _uint2str(STROKE), \"\u0027 stroke-linecap=\u0027round\u0027\u003e\"));
\t\tfor (uint256 i = 0; i \u003c _ints[4]; i++) {
\t\t\tstring memory _path = \"\u003cpath d=\u0027\";
\t\t\tfor (uint256 j = 0; j \u003c= (SIZE - 2 * X_PADDING) / STEP; j++) {
\t\t\t\tuint256 x = X_PADDING + j * STEP;
\t\t\t\tuint256 y = Y_PADDING + (SIZE - 2 * Y_PADDING) * i / (_ints[4] - 1);
\t\t\t\ty = uint256(int256(y) + PerlinNoise.noise2d(int256(_ints[0] + x * uint256(SCALAR) / _ints[2]), int256(_ints[1] + y * uint256(SCALAR) / _ints[3])) * int256(Y_PADDING) / SCALAR);
\t\t\t\t_path = string(abi.encodePacked(_path, j == 0 ? \"M\" : \"L\", _uint2str(_bools[1] ? y : x), \" \", _uint2str(_bools[1] ? x : y)));
\t\t\t}
\t\t\tbytes3 _col = getColor1(_tokenId);
\t\t\tif ((_ints[5] == 1 \u0026\u0026 i % 2 == 1) || (_ints[5] == 2 \u0026\u0026 i \u003e= _ints[4] / 2)) {
\t\t\t\t_col = getColor2(_tokenId);
\t\t\t} else if (_ints[5] == 3) {
\t\t\t\t_col = _collerp(_col, getColor2(_tokenId), i, _ints[4] - 1);
\t\t\t}
\t\t\t_path = string(abi.encodePacked(_path, \"\u0027 stroke=\u0027\", _col2str(_col), \"\u0027\u003e\u003c/path\u003e\"));
\t\t\tsvg = string(abi.encodePacked(svg, _path));
\t\t}
\t\tsvg = string(abi.encodePacked(svg, \"\u003c/g\u003e\u003c/svg\u003e\"));
\t}

\tfunction getTokenSettings(uint256 _tokenId) external view returns (bool inverted, bool vertical, uint256 xPos, uint256 yPos, uint256 xZoom, uint256 yZoom, uint256 lines, uint256 mode) {
\t\t(inverted, vertical, xPos, yPos, xZoom, yZoom, lines, mode) = getSettings(getSeed(_tokenId));
\t}

\tfunction getSettings(bytes32 _seed) public pure returns (bool inverted, bool vertical, uint256 xPos, uint256 yPos, uint256 xZoom, uint256 yZoom, uint256 lines, uint256 mode) {
\t\tbytes32 _rand = keccak256(abi.encodePacked(\"ThreadGlyph:\", _seed));
\t\tinverted = uint256(_rand) % 5 == 0;
\t\t_rand = keccak256(abi.encodePacked(_rand));
\t\tvertical = uint256(_rand) % 25 == 0;
\t\t_rand = keccak256(abi.encodePacked(_rand));
\t\txPos = uint256(_rand) % 2**16;
\t\t_rand = keccak256(abi.encodePacked(_rand));
\t\tyPos = uint256(_rand) % 2**16;
\t\t_rand = keccak256(abi.encodePacked(_rand));
\t\txZoom = 3200 + uint256(_rand) % 1800;
\t\t_rand = keccak256(abi.encodePacked(_rand));
\t\tyZoom = xZoom - 500 + uint256(_rand) % 500;
\t\t_rand = keccak256(abi.encodePacked(_rand));
\t\tlines = 12 + 2 * (10 - _sqrt(uint256(_rand) % 101));
\t\t_rand = keccak256(abi.encodePacked(_rand));
\t\tif (uint256(_rand) % 8 == 0) {
\t\t\t_rand = keccak256(abi.encodePacked(_rand));
\t\t\tif (uint256(_rand) % 4 == 0) {
\t\t\t\t_rand = keccak256(abi.encodePacked(_rand));
\t\t\t\tif (uint256(_rand) % 4 == 0) {
\t\t\t\t\tmode = 3;
\t\t\t\t} else {
\t\t\t\t\tmode = 2;
\t\t\t\t}
\t\t\t} else {
\t\t\t\tmode = 1;
\t\t\t}
\t\t} else {
\t\t\tmode = 0;
\t\t}
\t}

\tfunction getSettingsCompressed(bytes32 _seed) public pure returns (bool[2] memory bools, uint256[6] memory ints) {
\t\t(bools[0], bools[1], ints[0], ints[1], ints[2], ints[3], ints[4], ints[5]) = getSettings(_seed);
\t}

\tfunction getGlyph(uint256 _tokenId) public view returns (address tokenOwner, address approved, bytes32 seed, bytes3 color1, bytes3 color2) {
\t\treturn (ownerOf(_tokenId), getApproved(_tokenId), getSeed(_tokenId), getColor1(_tokenId), getColor2(_tokenId));
\t}

\tfunction getGlyphs(uint256[] memory _tokenIds) public view returns (address[] memory owners, address[] memory approveds, bytes32[] memory seeds, bytes3[] memory color1s, bytes3[] memory color2s) {
\t\tuint256 _length = _tokenIds.length;
\t\towners = new address[](_length);
\t\tapproveds = new address[](_length);
\t\tseeds = new bytes32[](_length);
\t\tcolor1s = new bytes3[](_length);
\t\tcolor2s = new bytes3[](_length);
\t\tfor (uint256 i = 0; i \u003c _length; i++) {
\t\t\t(owners[i], approveds[i], seeds[i], color1s[i], color2s[i]) = getGlyph(_tokenIds[i]);
\t\t}
\t}

\tfunction getGlyphsTable(uint256 _limit, uint256 _page, bool _isAsc) public view returns (uint256[] memory tokenIds, address[] memory owners, address[] memory approveds, bytes32[] memory seeds, bytes3[] memory color1s, bytes3[] memory color2s, uint256 totalGlyphs, uint256 totalPages) {
\t\trequire(_limit \u003e 0);
\t\ttotalGlyphs = totalSupply();

\t\tif (totalGlyphs \u003e 0) {
\t\t\ttotalPages = (totalGlyphs / _limit) + (totalGlyphs % _limit == 0 ? 0 : 1);
\t\t\trequire(_page \u003c totalPages);

\t\t\tuint256 _offset = _limit * _page;
\t\t\tif (_page == totalPages - 1 \u0026\u0026 totalGlyphs % _limit != 0) {
\t\t\t\t_limit = totalGlyphs % _limit;
\t\t\t}

\t\t\ttokenIds = new uint256[](_limit);
\t\t\tfor (uint256 i = 0; i \u003c _limit; i++) {
\t\t\t\ttokenIds[i] = tokenByIndex(_isAsc ? _offset + i : totalGlyphs - _offset - i - 1);
\t\t\t}
\t\t} else {
\t\t\ttotalPages = 0;
\t\t\ttokenIds = new uint256[](0);
\t\t}
\t\t(owners, approveds, seeds, color1s, color2s) = getGlyphs(tokenIds);
\t}

\tfunction getOwnerGlyphsTable(address _owner, uint256 _limit, uint256 _page, bool _isAsc) public view returns (uint256[] memory tokenIds, address[] memory approveds, bytes32[] memory seeds, bytes3[] memory color1s, bytes3[] memory color2s, uint256 totalGlyphs, uint256 totalPages) {
\t\trequire(_limit \u003e 0);
\t\ttotalGlyphs = balanceOf(_owner);

\t\tif (totalGlyphs \u003e 0) {
\t\t\ttotalPages = (totalGlyphs / _limit) + (totalGlyphs % _limit == 0 ? 0 : 1);
\t\t\trequire(_page \u003c totalPages);

\t\t\tuint256 _offset = _limit * _page;
\t\t\tif (_page == totalPages - 1 \u0026\u0026 totalGlyphs % _limit != 0) {
\t\t\t\t_limit = totalGlyphs % _limit;
\t\t\t}

\t\t\ttokenIds = new uint256[](_limit);
\t\t\tfor (uint256 i = 0; i \u003c _limit; i++) {
\t\t\t\ttokenIds[i] = tokenOfOwnerByIndex(_owner, _isAsc ? _offset + i : totalGlyphs - _offset - i - 1);
\t\t\t}
\t\t} else {
\t\t\ttotalPages = 0;
\t\t\ttokenIds = new uint256[](0);
\t\t}
\t\t( , approveds, seeds, color1s, color2s) = getGlyphs(tokenIds);
\t}

\tfunction allInfoFor(address _owner) external view returns (uint256 supply, uint256 ownerBalance) {
\t\treturn (totalSupply(), balanceOf(_owner));
\t}


\tfunction _mint() internal {
\t\trequire(msg.sender == tx.origin);
\t\trequire(totalSupply() \u003c MAX_SUPPLY);
\t\tuint256 _tokenId = info.totalSupply++;
\t\tToken storage _newToken = info.list[_tokenId];
\t\t_newToken.owner = msg.sender;
\t\tbytes32 _seed = keccak256(abi.encodePacked(_tokenId, msg.sender, blockhash(block.number - 1), gasleft()));
\t\t_newToken.seed = _seed;
\t\t(bool _inverted, , , , , , , ) = getSettings(_seed);
\t\t_newToken.color1 = _inverted ? bytes3(0xffffff) : bytes3(0x000000);
\t\t_newToken.color2 = _inverted ? bytes3(0x66dd66) : bytes3(0xdd4444);
\t\tuint256 _index = info.users[msg.sender].balance++;
\t\tinfo.users[msg.sender].indexOf[_tokenId] = _index + 1;
\t\tinfo.users[msg.sender].list[_index] = _tokenId;
\t\temit Transfer(address(0x0), msg.sender, _tokenId);
\t\temit Mint(msg.sender, _tokenId, _seed);
\t}
\t
\tfunction _transfer(address _from, address _to, uint256 _tokenId) internal {
\t\taddress _owner = ownerOf(_tokenId);
\t\taddress _approved = getApproved(_tokenId);
\t\trequire(_from == _owner);
\t\trequire(msg.sender == _owner || msg.sender == _approved || isApprovedForAll(_owner, msg.sender));

\t\tinfo.list[_tokenId].owner = _to;
\t\tif (_approved != address(0x0)) {
\t\t\tinfo.list[_tokenId].approved = address(0x0);
\t\t\temit Approval(address(0x0), address(0x0), _tokenId);
\t\t}

\t\tuint256 _index = info.users[_from].indexOf[_tokenId] - 1;
\t\tuint256 _moved = info.users[_from].list[info.users[_from].balance - 1];
\t\tinfo.users[_from].list[_index] = _moved;
\t\tinfo.users[_from].indexOf[_moved] = _index + 1;
\t\tinfo.users[_from].balance--;
\t\tdelete info.users[_from].indexOf[_tokenId];
\t\tuint256 _newIndex = info.users[_to].balance++;
\t\tinfo.users[_to].indexOf[_tokenId] = _newIndex + 1;
\t\tinfo.users[_to].list[_newIndex] = _tokenId;
\t\temit Transfer(_from, _to, _tokenId);
\t}


\tfunction _uint2str(uint256 _value) internal pure returns (string memory) {
\t\tuint256 _digits = 1;
\t\tuint256 _n = _value;
\t\twhile (_n \u003e 9) {
\t\t\t_n /= 10;
\t\t\t_digits++;
\t\t}
\t\tbytes memory _out = new bytes(_digits);
\t\tfor (uint256 i = 0; i \u003c _out.length; i++) {
\t\t\tuint256 _dec = (_value / (10**(_out.length - i - 1))) % 10;
\t\t\t_out[i] = bytes1(uint8(_dec) + 48);
\t\t}
\t\treturn string(_out);
\t}

\tfunction _col2str(bytes3 _color) internal pure returns (string memory) {
\t\tbytes memory _out = new bytes(7);
\t\tfor (uint256 i = 0; i \u003c _out.length; i++) {
\t\t\tif (i == 0) {
\t\t\t\t_out[i] = bytes1(uint8(35));
\t\t\t} else {
\t\t\t\tuint8 _hex = uint8(uint24(_color \u003e\u003e (4 * (_out.length - i - 1))) \u0026 15);
\t\t\t\t_out[i] = bytes1(_hex + (_hex \u003e 9 ? 87 : 48));
\t\t\t}
\t\t}
\t\treturn string(_out);
\t}

\tfunction _collerp(bytes3 _color1, bytes3 _color2, uint256 _current, uint256 _limit) public pure returns (bytes3 col) {
\t\trequire(_current \u003c= _limit);
\t\tfor (uint256 i = 0; i \u003c 3; i++) {
\t\t\tuint8 _c1 = uint8(uint24(_color1) \u003e\u003e (8 * i));
\t\t\tuint8 _c2 = uint8(uint24(_color2) \u003e\u003e (8 * i));
\t\t\tuint8 _diff;
\t\t\tuint8 _new;
\t\t\tif (_c2 == _c1) {
\t\t\t\t_new = _c1;
\t\t\t} else if (_c2 \u003e _c1) {
\t\t\t\t_diff = _c2 - _c1;
\t\t\t\t_new = uint8(_c1 + _diff * _current / _limit);
\t\t\t} else {
\t\t\t\t_diff = _c1 - _c2;
\t\t\t\t_new = uint8(_c2 + _diff * (_limit - _current) / _limit);
\t\t\t}
\t\t\tcol |= bytes3(uint24(_new) \u003c\u003c uint24(8 * i));
\t\t}
\t}

\tfunction _sqrt(uint256 _n) internal pure returns (uint256 result) {
\t\tuint256 _tmp = (_n + 1) / 2;
\t\tresult = _n;
\t\twhile (_tmp \u003c result) {
\t\t\tresult = _tmp;
\t\t\t_tmp = (_n / _tmp + _tmp) / 2;
\t\t}
\t}
}

