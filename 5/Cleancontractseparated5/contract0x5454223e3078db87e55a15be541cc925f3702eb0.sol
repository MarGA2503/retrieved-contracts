// SPDX-License-Identifier: MIT

pragma solidity ^0.6.0;

/*
 * @dev Provides information about the current execution context, including the
 * sender of the transaction and its data. While these are generally available
 * via msg.sender and msg.data, they should not be accessed in such a direct
 * manner, since when dealing with GSN meta-transactions the account sending and
 * paying for execution may not be the actual sender (as far as an application
 * is concerned).
 *
 * This contract is only required for intermediate, library-like contracts.
 */
abstract contract Context {
    function _msgSender() internal view virtual returns (address payable) {
        return msg.sender;
    }

    function _msgData() internal view virtual returns (bytes memory) {
        this; // silence state mutability warning without generating bytecode - see https://github.com/ethereum/solidity/issues/2691
        return msg.data;
    }
}
"},"ContractRegistry.sol":{"content":"// SPDX-License-Identifier: UNLICENSED

pragma solidity 0.6.12;
import \"./IContractRegistry.sol\";
import \"./ILockable.sol\";
import \"./IContractRegistryListener.sol\";
import \"./WithClaimableRegistryManagement.sol\";
import \"./Initializable.sol\";

contract ContractRegistry is IContractRegistry, Initializable, WithClaimableRegistryManagement {

\taddress previousContractRegistry;
\tmapping(string =\u003e address) contracts;
\taddress[] managedContractAddresses;
\tmapping(string =\u003e address) managers;

\tconstructor(address _previousContractRegistry, address registryAdmin) public {
\t\tpreviousContractRegistry = _previousContractRegistry;
\t\t_transferRegistryManagement(registryAdmin);
\t}

\tmodifier onlyAdmin {
\t\trequire(msg.sender == registryAdmin() || msg.sender == initializationAdmin(), \"sender is not an admin (registryAdmin or initializationAdmin when initialization in progress)\");

\t\t_;
\t}

\tmodifier onlyAdminOrMigrationManager {
\t\trequire(msg.sender == registryAdmin() || msg.sender == initializationAdmin() || msg.sender == managers[\"migrationManager\"], \"sender is not an admin (registryAdmin or initializationAdmin when initialization in progress) and not the migration manager\");

\t\t_;
\t}

\t/*
\t* External functions
\t*/

\tfunction setContract(string calldata contractName, address addr, bool managedContract) external override onlyAdminOrMigrationManager {
\t\trequire(!managedContract || addr != address(0), \"managed contract may not have address(0)\");
\t\tremoveManagedContract(contracts[contractName]);
\t\tcontracts[contractName] = addr;
\t\tif (managedContract) {
\t\t\taddManagedContract(addr);
\t\t}
\t\temit ContractAddressUpdated(contractName, addr, managedContract);
\t\tnotifyOnContractsChange();
\t}

\tfunction getContract(string calldata contractName) external override view returns (address) {
\t\treturn contracts[contractName];
\t}

\tfunction lockContracts() external override onlyAdminOrMigrationManager {
\t\tfor (uint i = 0; i \u003c managedContractAddresses.length; i++) {
\t\t\tILockable(managedContractAddresses[i]).lock();
\t\t}
\t}

\tfunction unlockContracts() external override onlyAdminOrMigrationManager {
\t\tfor (uint i = 0; i \u003c managedContractAddresses.length; i++) {
\t\t\tILockable(managedContractAddresses[i]).unlock();
\t\t}
\t}

\tfunction getManagedContracts() external override view returns (address[] memory) {
\t\treturn managedContractAddresses;
\t}

\tfunction setManager(string calldata role, address manager) external override onlyAdmin {
\t\tmanagers[role] = manager;
\t\temit ManagerChanged(role, manager);
\t}

\tfunction getManager(string calldata role) external override view returns (address) {
\t\treturn managers[role];
\t}

\tfunction setNewContractRegistry(IContractRegistry newRegistry) external override onlyAdmin {
\t\tfor (uint i = 0; i \u003c managedContractAddresses.length; i++) {
\t\t\tIContractRegistryListener(managedContractAddresses[i]).setContractRegistry(newRegistry);
\t\t\tIContractRegistryListener(managedContractAddresses[i]).refreshContracts();
\t\t}
\t\temit ContractRegistryUpdated(address(newRegistry));
\t}

\tfunction getPreviousContractRegistry() external override view returns (address) {
\t\treturn previousContractRegistry;
\t}

\t/*
\t* Private methods
\t*/

\tfunction notifyOnContractsChange() private {
\t\tfor (uint i = 0; i \u003c managedContractAddresses.length; i++) {
\t\t\tIContractRegistryListener(managedContractAddresses[i]).refreshContracts();
\t\t}
\t}

\tfunction addManagedContract(address addr) private {
\t\tmanagedContractAddresses.push(addr);
\t}

\tfunction removeManagedContract(address addr) private {
\t\tuint length = managedContractAddresses.length;
\t\tfor (uint i = 0; i \u003c length; i++) {
\t\t\tif (managedContractAddresses[i] == addr) {
\t\t\t\tif (i != length - 1) {
\t\t\t\t\tmanagedContractAddresses[i] = managedContractAddresses[length-1];
\t\t\t\t}
\t\t\t\tmanagedContractAddresses.pop();
\t\t\t\tlength--;
\t\t\t}
\t\t}
\t}

}
"},"IContractRegistry.sol":{"content":"// SPDX-License-Identifier: UNLICENSED

pragma solidity 0.6.12;

interface IContractRegistry {

\tevent ContractAddressUpdated(string contractName, address addr, bool managedContract);
\tevent ManagerChanged(string role, address newManager);
\tevent ContractRegistryUpdated(address newContractRegistry);

\t/*
\t* External functions
\t*/

\t/// @dev updates the contracts address and emits a corresponding event
\t/// managedContract indicates whether the contract is managed by the registry and notified on changes
\tfunction setContract(string calldata contractName, address addr, bool managedContract) external /* onlyAdmin */;

\t/// @dev returns the current address of the given contracts
\tfunction getContract(string calldata contractName) external view returns (address);

\t/// @dev returns the list of contract addresses managed by the registry
\tfunction getManagedContracts() external view returns (address[] memory);

\tfunction setManager(string calldata role, address manager) external /* onlyAdmin */;

\tfunction getManager(string calldata role) external view returns (address);

\tfunction lockContracts() external /* onlyAdmin */;

\tfunction unlockContracts() external /* onlyAdmin */;

\tfunction setNewContractRegistry(IContractRegistry newRegistry) external /* onlyAdmin */;

\tfunction getPreviousContractRegistry() external view returns (address);

}
"},"IContractRegistryListener.sol":{"content":"// SPDX-License-Identifier: UNLICENSED

pragma solidity 0.6.12;

import \"./IContractRegistry.sol\";

interface IContractRegistryListener {

    function refreshContracts() external;

    function setContractRegistry(IContractRegistry newRegistry) external;

}
"},"ILockable.sol":{"content":"// SPDX-License-Identifier: UNLICENSED

pragma solidity 0.6.12;

interface ILockable {

    event Locked();
    event Unlocked();

    function lock() external /* onlyLockOwner */;
    function unlock() external /* onlyLockOwner */;
    function isLocked() view external returns (bool);

}
"},"Initializable.sol":{"content":"// SPDX-License-Identifier: UNLICENSED

pragma solidity 0.6.12;

contract Initializable {

    address private _initializationAdmin;

    event InitializationComplete();

    constructor() public{
        _initializationAdmin = msg.sender;
    }

    modifier onlyInitializationAdmin() {
        require(msg.sender == initializationAdmin(), \"sender is not the initialization admin\");

        _;
    }

    /*
    * External functions
    */

    function initializationAdmin() public view returns (address) {
        return _initializationAdmin;
    }

    function initializationComplete() external onlyInitializationAdmin {
        _initializationAdmin = address(0);
        emit InitializationComplete();
    }

    function isInitializationComplete() public view returns (bool) {
        return _initializationAdmin == address(0);
    }

}"},"WithClaimableRegistryManagement.sol":{"content":"// SPDX-License-Identifier: UNLICENSED

pragma solidity 0.6.12;

import \"./Context.sol\";

/**
 * @title Claimable
 * @dev Extension for the Ownable contract, where the ownership needs to be claimed.
 * This allows the new owner to accept the transfer.
 */
contract WithClaimableRegistryManagement is Context {
    address private _registryAdmin;
    address private _pendingRegistryAdmin;

    event RegistryManagementTransferred(address indexed previousRegistryAdmin, address indexed newRegistryAdmin);

    /**
     * @dev Initializes the contract setting the deployer as the initial registryRegistryAdmin.
     */
    constructor () internal {
        address msgSender = _msgSender();
        _registryAdmin = msgSender;
        emit RegistryManagementTransferred(address(0), msgSender);
    }

    /**
     * @dev Returns the address of the current registryAdmin.
     */
    function registryAdmin() public view returns (address) {
        return _registryAdmin;
    }

    /**
     * @dev Throws if called by any account other than the registryAdmin.
     */
    modifier onlyRegistryAdmin() {
        require(isRegistryAdmin(), \"WithClaimableRegistryManagement: caller is not the registryAdmin\");
        _;
    }

    /**
     * @dev Returns true if the caller is the current registryAdmin.
     */
    function isRegistryAdmin() public view returns (bool) {
        return _msgSender() == _registryAdmin;
    }

    /**
     * @dev Leaves the contract without registryAdmin. It will not be possible to call
     * `onlyManager` functions anymore. Can only be called by the current registryAdmin.
     *
     * NOTE: Renouncing registryManagement will leave the contract without an registryAdmin,
     * thereby removing any functionality that is only available to the registryAdmin.
     */
    function renounceRegistryManagement() public onlyRegistryAdmin {
        emit RegistryManagementTransferred(_registryAdmin, address(0));
        _registryAdmin = address(0);
    }

    /**
     * @dev Transfers registryManagement of the contract to a new account (`newManager`).
     */
    function _transferRegistryManagement(address newRegistryAdmin) internal {
        require(newRegistryAdmin != address(0), \"RegistryAdmin: new registryAdmin is the zero address\");
        emit RegistryManagementTransferred(_registryAdmin, newRegistryAdmin);
        _registryAdmin = newRegistryAdmin;
    }

    /**
     * @dev Modifier throws if called by any account other than the pendingManager.
     */
    modifier onlyPendingRegistryAdmin() {
        require(msg.sender == _pendingRegistryAdmin, \"Caller is not the pending registryAdmin\");
        _;
    }
    /**
     * @dev Allows the current registryAdmin to set the pendingManager address.
     * @param newRegistryAdmin The address to transfer registryManagement to.
     */
    function transferRegistryManagement(address newRegistryAdmin) public onlyRegistryAdmin {
        _pendingRegistryAdmin = newRegistryAdmin;
    }

    /**
     * @dev Allows the _pendingRegistryAdmin address to finalize the transfer.
     */
    function claimRegistryManagement() external onlyPendingRegistryAdmin {
        _transferRegistryManagement(_pendingRegistryAdmin);
        _pendingRegistryAdmin = address(0);
    }

    /**
     * @dev Returns the current pendingRegistryAdmin
    */
    function pendingRegistryAdmin() public view returns (address) {
       return _pendingRegistryAdmin;  
    }
}

