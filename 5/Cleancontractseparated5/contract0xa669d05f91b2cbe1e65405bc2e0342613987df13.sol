



// SPDX-License-Identifier: MIT

// solhint-disable-next-line compiler-version
pragma solidity >=0.4.24 <0.8.0;

import \"../utils/AddressUpgradeable.sol\";

/**
 * @dev This is a base contract to aid in writing upgradeable contracts, or any kind of contract that will be deployed
 * behind a proxy. Since a proxied contract can't have a constructor, it's common to move constructor logic to an
 * external initializer function, usually called `initialize`. It then becomes necessary to protect this initializer
 * function so it can only be called once. The {initializer} modifier provided by this contract will have this effect.
 *
 * TIP: To avoid leaving the proxy in an uninitialized state, the initializer function should be called as early as
 * possible by providing the encoded function call as the `_data` argument to {UpgradeableProxy-constructor}.
 *
 * CAUTION: When used with inheritance, manual care must be taken to not invoke a parent initializer twice, or to ensure
 * that all initializers are idempotent. This is not verified automatically as constructors are by Solidity.
 */
abstract contract Initializable {

    /**
     * @dev Indicates that the contract has been initialized.
     */
    bool private _initialized;

    /**
     * @dev Indicates that the contract is in the process of being initialized.
     */
    bool private _initializing;

    /**
     * @dev Modifier to protect an initializer function from being invoked twice.
     */
    modifier initializer() {
        require(_initializing || _isConstructor() || !_initialized, \"Initializable: contract is already initialized\");

        bool isTopLevelCall = !_initializing;
        if (isTopLevelCall) {
            _initializing = true;
            _initialized = true;
        }

        _;

        if (isTopLevelCall) {
            _initializing = false;
        }
    }

    /// @dev Returns true if and only if the function is running in the constructor
    function _isConstructor() private view returns (bool) {
        return !AddressUpgradeable.isContract(address(this));
    }
}
"
    
// SPDX-License-Identifier: MIT

pragma solidity ^0.7.0;

/**
 * @dev Collection of functions related to the address type
 */
library AddressUpgradeable {
    /**
     * @dev Returns true if `account` is a contract.
     *
     * [IMPORTANT]
     * ====
     * It is unsafe to assume that an address for which this function returns
     * false is an externally-owned account (EOA) and not a contract.
     *
     * Among others, `isContract` will return false for the following
     * types of addresses:
     *
     *  - an externally-owned account
     *  - a contract in construction
     *  - an address where a contract will be created
     *  - an address where a contract lived, but was destroyed
     * ====
     */
    function isContract(address account) internal view returns (bool) {
        // This method relies on extcodesize, which returns 0 for contracts in
        // construction, since the code is only stored at the end of the
        // constructor execution.

        uint256 size;
        // solhint-disable-next-line no-inline-assembly
        assembly { size := extcodesize(account) }
        return size > 0;
    }

    /**
     * @dev Replacement for Solidity's `transfer`: sends `amount` wei to
     * `recipient`, forwarding all available gas and reverting on errors.
     *
     * https://eips.ethereum.org/EIPS/eip-1884[EIP1884] increases the gas cost
     * of certain opcodes, possibly making contracts go over the 2300 gas limit
     * imposed by `transfer`, making them unable to receive funds via
     * `transfer`. {sendValue} removes this limitation.
     *
     * https://diligence.consensys.net/posts/2019/09/stop-using-soliditys-transfer-now/[Learn more].
     *
     * IMPORTANT: because control is transferred to `recipient`, care must be
     * taken to not create reentrancy vulnerabilities. Consider using
     * {ReentrancyGuard} or the
     * https://solidity.readthedocs.io/en/v0.5.11/security-considerations.html#use-the-checks-effects-interactions-pattern[checks-effects-interactions pattern].
     */
    function sendValue(address payable recipient, uint256 amount) internal {
        require(address(this).balance >= amount, \"Address: insufficient balance\");

        // solhint-disable-next-line avoid-low-level-calls, avoid-call-value
        (bool success, ) = recipient.call{ value: amount }(\"\");
        require(success, \"Address: unable to send value, recipient may have reverted\");
    }

    /**
     * @dev Performs a Solidity function call using a low level `call`. A
     * plain`call` is an unsafe replacement for a function call: use this
     * function instead.
     *
     * If `target` reverts with a revert reason, it is bubbled up by this
     * function (like regular Solidity function calls).
     *
     * Returns the raw returned data. To convert to the expected return value,
     * use https://solidity.readthedocs.io/en/latest/units-and-global-variables.html?highlight=abi.decode#abi-encoding-and-decoding-functions[`abi.decode`].
     *
     * Requirements:
     *
     * - `target` must be a contract.
     * - calling `target` with `data` must not revert.
     *
     * _Available since v3.1._
     */
    function functionCall(address target, bytes memory data) internal returns (bytes memory) {
      return functionCall(target, data, \"Address: low-level call failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`], but with
     * `errorMessage` as a fallback revert reason when `target` reverts.
     *
     * _Available since v3.1._
     */
    function functionCall(address target, bytes memory data, string memory errorMessage) internal returns (bytes memory) {
        return functionCallWithValue(target, data, 0, errorMessage);
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`],
     * but also transferring `value` wei to `target`.
     *
     * Requirements:
     *
     * - the calling contract must have an ETH balance of at least `value`.
     * - the called Solidity function must be `payable`.
     *
     * _Available since v3.1._
     */
    function functionCallWithValue(address target, bytes memory data, uint256 value) internal returns (bytes memory) {
        return functionCallWithValue(target, data, value, \"Address: low-level call with value failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCallWithValue-address-bytes-uint256-}[`functionCallWithValue`], but
     * with `errorMessage` as a fallback revert reason when `target` reverts.
     *
     * _Available since v3.1._
     */
    function functionCallWithValue(address target, bytes memory data, uint256 value, string memory errorMessage) internal returns (bytes memory) {
        require(address(this).balance >= value, \"Address: insufficient balance for call\");
        require(isContract(target), \"Address: call to non-contract\");

        // solhint-disable-next-line avoid-low-level-calls
        (bool success, bytes memory returndata) = target.call{ value: value }(data);
        return _verifyCallResult(success, returndata, errorMessage);
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`],
     * but performing a static call.
     *
     * _Available since v3.3._
     */
    function functionStaticCall(address target, bytes memory data) internal view returns (bytes memory) {
        return functionStaticCall(target, data, \"Address: low-level static call failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-string-}[`functionCall`],
     * but performing a static call.
     *
     * _Available since v3.3._
     */
    function functionStaticCall(address target, bytes memory data, string memory errorMessage) internal view returns (bytes memory) {
        require(isContract(target), \"Address: static call to non-contract\");

        // solhint-disable-next-line avoid-low-level-calls
        (bool success, bytes memory returndata) = target.staticcall(data);
        return _verifyCallResult(success, returndata, errorMessage);
    }

    function _verifyCallResult(bool success, bytes memory returndata, string memory errorMessage) private pure returns(bytes memory) {
        if (success) {
            return returndata;
        } else {
            // Look for revert reason and bubble it up if present
            if (returndata.length > 0) {
                // The easiest way to bubble the revert reason is using memory via assembly

                // solhint-disable-next-line no-inline-assembly
                assembly {
                    let returndata_size := mload(returndata)
                    revert(add(32, returndata), returndata_size)
                }
            } else {
                revert(errorMessage);
            }
        }
    }
}
"
    
// SPDX-License-Identifier: MIT

pragma solidity >=0.6.0 <0.8.0;
import \"../proxy/Initializable.sol\";

/*
 * @dev Provides information about the current execution context, including the
 * sender of the transaction and its data. While these are generally available
 * via msg.sender and msg.data, they should not be accessed in such a direct
 * manner, since when dealing with GSN meta-transactions the account sending and
 * paying for execution may not be the actual sender (as far as an application
 * is concerned).
 *
 * This contract is only required for intermediate, library-like contracts.
 */
abstract contract ContextUpgradeable is Initializable {
    function __Context_init() internal initializer {
        __Context_init_unchained();
    }

    function __Context_init_unchained() internal initializer {
    }
    function _msgSender() internal view virtual returns (address payable) {
        return msg.sender;
    }

    function _msgData() internal view virtual returns (bytes memory) {
        this; // silence state mutability warning without generating bytecode - see https://github.com/ethereum/solidity/issues/2691
        return msg.data;
    }
    uint256[50] private __gap;
}
"
    
// SPDX-License-Identifier: MIT

pragma solidity ^0.7.0;

import \"./ContextUpgradeable.sol\";
import \"../proxy/Initializable.sol\";

/**
 * @dev Contract module which allows children to implement an emergency stop
 * mechanism that can be triggered by an authorized account.
 *
 * This module is used through inheritance. It will make available the
 * modifiers `whenNotPaused` and `whenPaused`, which can be applied to
 * the functions of your contract. Note that they will not be pausable by
 * simply including this module, only once the modifiers are put in place.
 */
abstract contract PausableUpgradeable is Initializable, ContextUpgradeable {
    /**
     * @dev Emitted when the pause is triggered by `account`.
     */
    event Paused(address account);

    /**
     * @dev Emitted when the pause is lifted by `account`.
     */
    event Unpaused(address account);

    bool private _paused;

    /**
     * @dev Initializes the contract in unpaused state.
     */
    function __Pausable_init() internal initializer {
        __Context_init_unchained();
        __Pausable_init_unchained();
    }

    function __Pausable_init_unchained() internal initializer {
        _paused = false;
    }

    /**
     * @dev Returns true if the contract is paused, and false otherwise.
     */
    function paused() public view virtual returns (bool) {
        return _paused;
    }

    /**
     * @dev Modifier to make a function callable only when the contract is not paused.
     *
     * Requirements:
     *
     * - The contract must not be paused.
     */
    modifier whenNotPaused() {
        require(!paused(), \"Pausable: paused\");
        _;
    }

    /**
     * @dev Modifier to make a function callable only when the contract is paused.
     *
     * Requirements:
     *
     * - The contract must be paused.
     */
    modifier whenPaused() {
        require(paused(), \"Pausable: not paused\");
        _;
    }

    /**
     * @dev Triggers stopped state.
     *
     * Requirements:
     *
     * - The contract must not be paused.
     */
    function _pause() internal virtual whenNotPaused {
        _paused = true;
        emit Paused(_msgSender());
    }

    /**
     * @dev Returns to normal state.
     *
     * Requirements:
     *
     * - The contract must be paused.
     */
    function _unpause() internal virtual whenPaused {
        _paused = false;
        emit Unpaused(_msgSender());
    }
    uint256[49] private __gap;
}
"
    
// SPDX-License-Identifier: MIT

pragma solidity ^0.7.0;

/**
 * @dev Wrappers over Solidity's arithmetic operations with added overflow
 * checks.
 *
 * Arithmetic operations in Solidity wrap on overflow. This can easily result
 * in bugs, because programmers usually assume that an overflow raises an
 * error, which is the standard behavior in high level programming languages.
 * `SafeMath` restores this intuition by reverting the transaction when an
 * operation overflows.
 *
 * Using this library instead of the unchecked operations eliminates an entire
 * class of bugs, so it's recommended to use it always.
 */
library SafeMath {
    /**
     * @dev Returns the addition of two unsigned integers, with an overflow flag.
     *
     * _Available since v3.4._
     */
    function tryAdd(uint256 a, uint256 b) internal pure returns (bool, uint256) {
        uint256 c = a + b;
        if (c < a) return (false, 0);
        return (true, c);
    }

    /**
     * @dev Returns the substraction of two unsigned integers, with an overflow flag.
     *
     * _Available since v3.4._
     */
    function trySub(uint256 a, uint256 b) internal pure returns (bool, uint256) {
        if (b > a) return (false, 0);
        return (true, a - b);
    }

    /**
     * @dev Returns the multiplication of two unsigned integers, with an overflow flag.
     *
     * _Available since v3.4._
     */
    function tryMul(uint256 a, uint256 b) internal pure returns (bool, uint256) {
        // Gas optimization: this is cheaper than requiring 'a' not being zero, but the
        // benefit is lost if 'b' is also tested.
        // See: https://github.com/OpenZeppelin/openzeppelin-contracts/pull/522
        if (a == 0) return (true, 0);
        uint256 c = a * b;
        if (c / a != b) return (false, 0);
        return (true, c);
    }

    /**
     * @dev Returns the division of two unsigned integers, with a division by zero flag.
     *
     * _Available since v3.4._
     */
    function tryDiv(uint256 a, uint256 b) internal pure returns (bool, uint256) {
        if (b == 0) return (false, 0);
        return (true, a / b);
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers, with a division by zero flag.
     *
     * _Available since v3.4._
     */
    function tryMod(uint256 a, uint256 b) internal pure returns (bool, uint256) {
        if (b == 0) return (false, 0);
        return (true, a % b);
    }

    /**
     * @dev Returns the addition of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity's `+` operator.
     *
     * Requirements:
     *
     * - Addition cannot overflow.
     */
    function add(uint256 a, uint256 b) internal pure returns (uint256) {
        uint256 c = a + b;
        require(c >= a, \"SafeMath: addition overflow\");
        return c;
    }

    /**
     * @dev Returns the subtraction of two unsigned integers, reverting on
     * overflow (when the result is negative).
     *
     * Counterpart to Solidity's `-` operator.
     *
     * Requirements:
     *
     * - Subtraction cannot overflow.
     */
    function sub(uint256 a, uint256 b) internal pure returns (uint256) {
        require(b <= a, \"SafeMath: subtraction overflow\");
        return a - b;
    }

    /**
     * @dev Returns the multiplication of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity's `*` operator.
     *
     * Requirements:
     *
     * - Multiplication cannot overflow.
     */
    function mul(uint256 a, uint256 b) internal pure returns (uint256) {
        if (a == 0) return 0;
        uint256 c = a * b;
        require(c / a == b, \"SafeMath: multiplication overflow\");
        return c;
    }

    /**
     * @dev Returns the integer division of two unsigned integers, reverting on
     * division by zero. The result is rounded towards zero.
     *
     * Counterpart to Solidity's `/` operator. Note: this function uses a
     * `revert` opcode (which leaves remaining gas untouched) while Solidity
     * uses an invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function div(uint256 a, uint256 b) internal pure returns (uint256) {
        require(b > 0, \"SafeMath: division by zero\");
        return a / b;
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
     * reverting when dividing by zero.
     *
     * Counterpart to Solidity's `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function mod(uint256 a, uint256 b) internal pure returns (uint256) {
        require(b > 0, \"SafeMath: modulo by zero\");
        return a % b;
    }

    /**
     * @dev Returns the subtraction of two unsigned integers, reverting with custom message on
     * overflow (when the result is negative).
     *
     * CAUTION: This function is deprecated because it requires allocating memory for the error
     * message unnecessarily. For custom revert reasons use {trySub}.
     *
     * Counterpart to Solidity's `-` operator.
     *
     * Requirements:
     *
     * - Subtraction cannot overflow.
     */
    function sub(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b <= a, errorMessage);
        return a - b;
    }

    /**
     * @dev Returns the integer division of two unsigned integers, reverting with custom message on
     * division by zero. The result is rounded towards zero.
     *
     * CAUTION: This function is deprecated because it requires allocating memory for the error
     * message unnecessarily. For custom revert reasons use {tryDiv}.
     *
     * Counterpart to Solidity's `/` operator. Note: this function uses a
     * `revert` opcode (which leaves remaining gas untouched) while Solidity
     * uses an invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function div(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b > 0, errorMessage);
        return a / b;
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
     * reverting with custom message when dividing by zero.
     *
     * CAUTION: This function is deprecated because it requires allocating memory for the error
     * message unnecessarily. For custom revert reasons use {tryMod}.
     *
     * Counterpart to Solidity's `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function mod(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b > 0, errorMessage);
        return a % b;
    }
}
"
    
// SPDX-License-Identifier: MIT

pragma solidity ^0.7.0;

/**
 * @dev Interface of the ERC20 standard as defined in the EIP.
 */
interface IERC20 {
    /**
     * @dev Returns the amount of tokens in existence.
     */
    function totalSupply() external view returns (uint256);

    /**
     * @dev Returns the amount of tokens owned by `account`.
     */
    function balanceOf(address account) external view returns (uint256);

    /**
     * @dev Moves `amount` tokens from the caller's account to `recipient`.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transfer(address recipient, uint256 amount) external returns (bool);

    /**
     * @dev Returns the remaining number of tokens that `spender` will be
     * allowed to spend on behalf of `owner` through {transferFrom}. This is
     * zero by default.
     *
     * This value changes when {approve} or {transferFrom} are called.
     */
    function allowance(address owner, address spender) external view returns (uint256);

    /**
     * @dev Sets `amount` as the allowance of `spender` over the caller's tokens.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * IMPORTANT: Beware that changing an allowance with this method brings the risk
     * that someone may use both the old and the new allowance by unfortunate
     * transaction ordering. One possible solution to mitigate this race
     * condition is to first reduce the spender's allowance to 0 and set the
     * desired value afterwards:
     * https://github.com/ethereum/EIPs/issues/20#issuecomment-263524729
     *
     * Emits an {Approval} event.
     */
    function approve(address spender, uint256 amount) external returns (bool);

    /**
     * @dev Moves `amount` tokens from `sender` to `recipient` using the
     * allowance mechanism. `amount` is then deducted from the caller's
     * allowance.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transferFrom(address sender, address recipient, uint256 amount) external returns (bool);

    /**
     * @dev Emitted when `value` tokens are moved from one account (`from`) to
     * another (`to`).
     *
     * Note that `value` may be zero.
     */
    event Transfer(address indexed from, address indexed to, uint256 value);

    /**
     * @dev Emitted when the allowance of a `spender` for an `owner` is set by
     * a call to {approve}. `value` is the new allowance.
     */
    event Approval(address indexed owner, address indexed spender, uint256 value);
}
"
    
// SPDX-License-Identifier: MIT

pragma solidity ^0.7.0;

import \"./IERC20.sol\";
import \"../../math/SafeMath.sol\";
import \"../../utils/Address.sol\";

/**
 * @title SafeERC20
 * @dev Wrappers around ERC20 operations that throw on failure (when the token
 * contract returns false). Tokens that return no value (and instead revert or
 * throw on failure) are also supported, non-reverting calls are assumed to be
 * successful.
 * To use this library you can add a `using SafeERC20 for IERC20;` statement to your contract,
 * which allows you to call the safe operations as `token.safeTransfer(...)`, etc.
 */
library SafeERC20 {
    using SafeMath for uint256;
    using Address for address;

    function safeTransfer(IERC20 token, address to, uint256 value) internal {
        _callOptionalReturn(token, abi.encodeWithSelector(token.transfer.selector, to, value));
    }

    function safeTransferFrom(IERC20 token, address from, address to, uint256 value) internal {
        _callOptionalReturn(token, abi.encodeWithSelector(token.transferFrom.selector, from, to, value));
    }

    /**
     * @dev Deprecated. This function has issues similar to the ones found in
     * {IERC20-approve}, and its usage is discouraged.
     *
     * Whenever possible, use {safeIncreaseAllowance} and
     * {safeDecreaseAllowance} instead.
     */
    function safeApprove(IERC20 token, address spender, uint256 value) internal {
        // safeApprove should only be called when setting an initial allowance,
        // or when resetting it to zero. To increase and decrease it, use
        // 'safeIncreaseAllowance' and 'safeDecreaseAllowance'
        // solhint-disable-next-line max-line-length
        require((value == 0) || (token.allowance(address(this), spender) == 0),
            \"SafeERC20: approve from non-zero to non-zero allowance\"
        );
        _callOptionalReturn(token, abi.encodeWithSelector(token.approve.selector, spender, value));
    }

    function safeIncreaseAllowance(IERC20 token, address spender, uint256 value) internal {
        uint256 newAllowance = token.allowance(address(this), spender).add(value);
        _callOptionalReturn(token, abi.encodeWithSelector(token.approve.selector, spender, newAllowance));
    }

    function safeDecreaseAllowance(IERC20 token, address spender, uint256 value) internal {
        uint256 newAllowance = token.allowance(address(this), spender).sub(value, \"SafeERC20: decreased allowance below zero\");
        _callOptionalReturn(token, abi.encodeWithSelector(token.approve.selector, spender, newAllowance));
    }

    /**
     * @dev Imitates a Solidity high-level call (i.e. a regular function call to a contract), relaxing the requirement
     * on the return value: the return value is optional (but if data is returned, it must not be false).
     * @param token The token targeted by the call.
     * @param data The call data (encoded using abi.encode or one of its variants).
     */
    function _callOptionalReturn(IERC20 token, bytes memory data) private {
        // We need to perform a low level call here, to bypass Solidity's return data size checking mechanism, since
        // we're implementing it ourselves. We use {Address.functionCall} to perform this call, which verifies that
        // the target address contains contract code and also asserts for success in the low-level call.

        bytes memory returndata = address(token).functionCall(data, \"SafeERC20: low-level call failed\");
        if (returndata.length > 0) { // Return data is optional
            // solhint-disable-next-line max-line-length
            require(abi.decode(returndata, (bool)), \"SafeERC20: ERC20 operation did not succeed\");
        }
    }
}
"
    
// SPDX-License-Identifier: MIT

pragma solidity ^0.7.0;

/**
 * @dev Collection of functions related to the address type
 */
library Address {
    /**
     * @dev Returns true if `account` is a contract.
     *
     * [IMPORTANT]
     * ====
     * It is unsafe to assume that an address for which this function returns
     * false is an externally-owned account (EOA) and not a contract.
     *
     * Among others, `isContract` will return false for the following
     * types of addresses:
     *
     *  - an externally-owned account
     *  - a contract in construction
     *  - an address where a contract will be created
     *  - an address where a contract lived, but was destroyed
     * ====
     */
    function isContract(address account) internal view returns (bool) {
        // This method relies on extcodesize, which returns 0 for contracts in
        // construction, since the code is only stored at the end of the
        // constructor execution.

        uint256 size;
        // solhint-disable-next-line no-inline-assembly
        assembly { size := extcodesize(account) }
        return size > 0;
    }

    /**
     * @dev Replacement for Solidity's `transfer`: sends `amount` wei to
     * `recipient`, forwarding all available gas and reverting on errors.
     *
     * https://eips.ethereum.org/EIPS/eip-1884[EIP1884] increases the gas cost
     * of certain opcodes, possibly making contracts go over the 2300 gas limit
     * imposed by `transfer`, making them unable to receive funds via
     * `transfer`. {sendValue} removes this limitation.
     *
     * https://diligence.consensys.net/posts/2019/09/stop-using-soliditys-transfer-now/[Learn more].
     *
     * IMPORTANT: because control is transferred to `recipient`, care must be
     * taken to not create reentrancy vulnerabilities. Consider using
     * {ReentrancyGuard} or the
     * https://solidity.readthedocs.io/en/v0.5.11/security-considerations.html#use-the-checks-effects-interactions-pattern[checks-effects-interactions pattern].
     */
    function sendValue(address payable recipient, uint256 amount) internal {
        require(address(this).balance >= amount, \"Address: insufficient balance\");

        // solhint-disable-next-line avoid-low-level-calls, avoid-call-value
        (bool success, ) = recipient.call{ value: amount }(\"\");
        require(success, \"Address: unable to send value, recipient may have reverted\");
    }

    /**
     * @dev Performs a Solidity function call using a low level `call`. A
     * plain`call` is an unsafe replacement for a function call: use this
     * function instead.
     *
     * If `target` reverts with a revert reason, it is bubbled up by this
     * function (like regular Solidity function calls).
     *
     * Returns the raw returned data. To convert to the expected return value,
     * use https://solidity.readthedocs.io/en/latest/units-and-global-variables.html?highlight=abi.decode#abi-encoding-and-decoding-functions[`abi.decode`].
     *
     * Requirements:
     *
     * - `target` must be a contract.
     * - calling `target` with `data` must not revert.
     *
     * _Available since v3.1._
     */
    function functionCall(address target, bytes memory data) internal returns (bytes memory) {
      return functionCall(target, data, \"Address: low-level call failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`], but with
     * `errorMessage` as a fallback revert reason when `target` reverts.
     *
     * _Available since v3.1._
     */
    function functionCall(address target, bytes memory data, string memory errorMessage) internal returns (bytes memory) {
        return functionCallWithValue(target, data, 0, errorMessage);
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`],
     * but also transferring `value` wei to `target`.
     *
     * Requirements:
     *
     * - the calling contract must have an ETH balance of at least `value`.
     * - the called Solidity function must be `payable`.
     *
     * _Available since v3.1._
     */
    function functionCallWithValue(address target, bytes memory data, uint256 value) internal returns (bytes memory) {
        return functionCallWithValue(target, data, value, \"Address: low-level call with value failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCallWithValue-address-bytes-uint256-}[`functionCallWithValue`], but
     * with `errorMessage` as a fallback revert reason when `target` reverts.
     *
     * _Available since v3.1._
     */
    function functionCallWithValue(address target, bytes memory data, uint256 value, string memory errorMessage) internal returns (bytes memory) {
        require(address(this).balance >= value, \"Address: insufficient balance for call\");
        require(isContract(target), \"Address: call to non-contract\");

        // solhint-disable-next-line avoid-low-level-calls
        (bool success, bytes memory returndata) = target.call{ value: value }(data);
        return _verifyCallResult(success, returndata, errorMessage);
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`],
     * but performing a static call.
     *
     * _Available since v3.3._
     */
    function functionStaticCall(address target, bytes memory data) internal view returns (bytes memory) {
        return functionStaticCall(target, data, \"Address: low-level static call failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-string-}[`functionCall`],
     * but performing a static call.
     *
     * _Available since v3.3._
     */
    function functionStaticCall(address target, bytes memory data, string memory errorMessage) internal view returns (bytes memory) {
        require(isContract(target), \"Address: static call to non-contract\");

        // solhint-disable-next-line avoid-low-level-calls
        (bool success, bytes memory returndata) = target.staticcall(data);
        return _verifyCallResult(success, returndata, errorMessage);
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`],
     * but performing a delegate call.
     *
     * _Available since v3.4._
     */
    function functionDelegateCall(address target, bytes memory data) internal returns (bytes memory) {
        return functionDelegateCall(target, data, \"Address: low-level delegate call failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-string-}[`functionCall`],
     * but performing a delegate call.
     *
     * _Available since v3.4._
     */
    function functionDelegateCall(address target, bytes memory data, string memory errorMessage) internal returns (bytes memory) {
        require(isContract(target), \"Address: delegate call to non-contract\");

        // solhint-disable-next-line avoid-low-level-calls
        (bool success, bytes memory returndata) = target.delegatecall(data);
        return _verifyCallResult(success, returndata, errorMessage);
    }

    function _verifyCallResult(bool success, bytes memory returndata, string memory errorMessage) private pure returns(bytes memory) {
        if (success) {
            return returndata;
        } else {
            // Look for revert reason and bubble it up if present
            if (returndata.length > 0) {
                // The easiest way to bubble the revert reason is using memory via assembly

                // solhint-disable-next-line no-inline-assembly
                assembly {
                    let returndata_size := mload(returndata)
                    revert(add(32, returndata), returndata_size)
                }
            } else {
                revert(errorMessage);
            }
        }
    }
}
"
    
// SPDX-License-Identifier: MIT

/**
 * From https://github.com/OpenZeppelin/openzeppelin-contracts-upgradeable/blob/aeb86bf4f438e0fedb5eecc3dd334fd6544ab1f6/contracts/access/AccessControlUpgradeable.sol
 *
 * Changes:
 * - Compiled for 0.7.6
 * - Removed ERC165 Introspection
 * - Moved state to RbacFromOwnableData
 * - Added _ownerDeprecated for upgrading from OwnableUpgradeable
 * - Removed _checkRole
 * - Reformatted styling in line with this repository.
 */

/*
The MIT License (MIT)

Copyright (c) 2016-2020 zOS Global Limited

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
\"Software\"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

pragma solidity 0.7.6;

import \"@openzeppelin/contracts-upgradeable/utils/ContextUpgradeable.sol\";
import \"@openzeppelin/contracts-upgradeable/proxy/Initializable.sol\";

import \"./RbacFromOwnableData.sol\";
import \"../interfaces/IAccessControl.sol\";

/* solhint-disable func-name-mixedcase */

abstract contract RbacFromOwnable is
\tInitializable,
\tContextUpgradeable,
\tRbacFromOwnableData,
\tIAccessControl
{
\tbytes32 public constant DEFAULT_ADMIN_ROLE = 0x00;

\t/* Initializer */

\tfunction __RbacFromOwnable_init() internal initializer {
\t\t__Context_init_unchained();
\t}

\tfunction __RbacFromOwnable_init_unchained() internal initializer {
\t\treturn;
\t}

\t/* Modifiers */

\tmodifier onlyRole(bytes32 role) {
\t\trequire(hasRole(role, _msgSender()), \"AccessControl: access denied\");
\t\t_;
\t}

\t/* External Views */

\tfunction hasRole(bytes32 role, address account)
\t\tpublic
\t\tview
\t\toverride
\t\treturns (bool)
\t{
\t\treturn _roles[role].members[account];
\t}

\tfunction getRoleAdmin(bytes32 role) public view override returns (bytes32) {
\t\treturn _roles[role].adminRole;
\t}

\t/* External Mutators */

\tfunction grantRole(bytes32 role, address account)
\t\tpublic
\t\tvirtual
\t\toverride
\t\tonlyRole(getRoleAdmin(role))
\t{
\t\t_grantRole(role, account);
\t}

\tfunction revokeRole(bytes32 role, address account)
\t\tpublic
\t\tvirtual
\t\toverride
\t\tonlyRole(getRoleAdmin(role))
\t{
\t\t_revokeRole(role, account);
\t}

\tfunction renounceRole(bytes32 role, address account)
\t\tpublic
\t\tvirtual
\t\toverride
\t{
\t\trequire(
\t\t\taccount == _msgSender(),
\t\t\t\"AccessControl: can only renounce roles for self\"
\t\t);

\t\t_revokeRole(role, account);
\t}

\t/* Internal Mutators */

\tfunction _setupRole(bytes32 role, address account) internal virtual {
\t\t_grantRole(role, account);
\t}

\tfunction _setRoleAdmin(bytes32 role, bytes32 adminRole) internal virtual {
\t\temit RoleAdminChanged(role, getRoleAdmin(role), adminRole);
\t\t_roles[role].adminRole = adminRole;
\t}

\tfunction _grantRole(bytes32 role, address account) private {
\t\tif (!hasRole(role, account)) {
\t\t\t_roles[role].members[account] = true;
\t\t\temit RoleGranted(role, account, _msgSender());
\t\t}
\t}

\tfunction _revokeRole(bytes32 role, address account) private {
\t\tif (hasRole(role, account)) {
\t\t\t_roles[role].members[account] = false;
\t\t\temit RoleRevoked(role, account, _msgSender());
\t\t}
\t}
}
"
    
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

abstract contract RbacFromOwnableData {
\tstruct RoleData {
\t\tmapping(address => bool) members;
\t\tbytes32 adminRole;
\t}

\taddress internal _ownerDeprecated;

\tmapping(bytes32 => RoleData) internal _roles;

\tuint256[48] private __gap;
}
"
    
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

interface IAccessControl {
\t/* Views */

\tfunction getRoleAdmin(bytes32 role) external view returns (bytes32);

\tfunction hasRole(bytes32 role, address account) external view returns (bool);

\t/* Mutators */

\tfunction grantRole(bytes32 role, address account) external;

\tfunction revokeRole(bytes32 role, address account) external;

\tfunction renounceRole(bytes32 role, address account) external;

\t/* Events */
\tevent RoleAdminChanged(
\t\tbytes32 indexed role,
\t\tbytes32 indexed previousAdminRole,
\t\tbytes32 indexed newAdminRole
\t);
\tevent RoleGranted(
\t\tbytes32 indexed role,
\t\taddress indexed account,
\t\taddress indexed sender
\t);
\tevent RoleRevoked(
\t\tbytes32 indexed role,
\t\taddress indexed account,
\t\taddress indexed sender
\t);
}
"
    
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IFeeLogic {
\t/* Types */

\tstruct ExemptData {
\t\taddress account;
\t\tbool isExempt;
\t}

\t/* Views */

\tfunction exemptsAt(uint256 index) external view returns (address);

\tfunction exemptsLength() external view returns (uint256);

\tfunction feeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction getFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction getRebaseFee(uint256 amount) external view returns (uint256);

\tfunction isExempt(address account) external view returns (bool);

\tfunction isRebaseExempt(address account) external view returns (bool);

\tfunction rebaseExemptsAt(uint256 index) external view returns (address);

\tfunction rebaseExemptsLength() external view returns (uint256);

\tfunction rebaseFeeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction rebaseInterval() external view returns (uint256);

\tfunction recipient() external view returns (address);

\tfunction undoFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction undoRebaseFee(uint256 amount) external view returns (uint256);

\t/* Mutators */

\tfunction notify(uint256 amount) external;

\tfunction setExempt(address account, bool isExempt_) external;

\tfunction setExemptBatch(ExemptData[] memory batch) external;

\tfunction setFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseExempt(address account, bool isExempt_) external;

\tfunction setRebaseExemptBatch(ExemptData[] memory batch) external;

\tfunction setRebaseFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRebaseInterval(uint256 interval) external;

\tfunction setRecipient(address account) external;

\t/* Events */

\tevent ExemptAdded(address indexed author, address indexed account);
\tevent ExemptRemoved(address indexed author, address indexed account);
\tevent FeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseExemptAdded(address indexed author, address indexed account);
\tevent RebaseExemptRemoved(address indexed author, address indexed account);
\tevent RebaseFeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RebaseIntervalSet(address indexed author, uint256 interval);
\tevent RecipientSet(address indexed author, address indexed account);
}
"
    
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

abstract contract ERC20Data {
\tmapping(address => uint256) internal _balances;
\tmapping(address => mapping(address => uint256)) internal _allowances;
\tuint256 internal _totalSupply;
\tuint256[47] private __gap;
}
"
    
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

abstract contract ERC20TxFeeData {
\taddress internal _feeLogic;
\tuint256[49] private __gap;
}
"
    
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

import \"@openzeppelin/contracts-upgradeable/utils/ContextUpgradeable.sol\";
import \"@openzeppelin/contracts/token/ERC20/IERC20.sol\";
import \"@openzeppelin/contracts-upgradeable/proxy/Initializable.sol\";
import \"@openzeppelin/contracts-upgradeable/utils/PausableUpgradeable.sol\";
import \"@openzeppelin/contracts/token/ERC20/SafeERC20.sol\";
import \"@openzeppelin/contracts/math/SafeMath.sol\";

import \"./ETHtxData.sol\";
import \"../ERC20/ERC20Data.sol\";
import \"../ERC20TxFee/ERC20TxFeeData.sol\";
import \"../interfaces/IERC20Metadata.sol\";
import \"../interfaces/IETHtx.sol\";
import \"../../rewards/interfaces/IFeeLogic.sol\";
import \"../../access/RbacFromOwnable/RbacFromOwnable.sol\";

/* solhint-disable not-rely-on-time */

contract ETHtx is
\tInitializable,
\tContextUpgradeable,
\tRbacFromOwnable,
\tPausableUpgradeable,
\tERC20Data,
\tERC20TxFeeData,
\tETHtxData,
\tIERC20,
\tIERC20Metadata,
\tIETHtx
{
\tusing SafeERC20 for IERC20;
\tusing SafeMath for uint256;

\tstruct ETHtxArgs {
\t\taddress feeLogic;
\t\taddress[] minters;
\t\taddress[] rebasers;
\t}

\tbytes32 public constant MINTER_ROLE = keccak256(\"MINTER_ROLE\");
\tbytes32 public constant REBASER_ROLE = keccak256(\"REBASER_ROLE\");

\tuint256 internal constant _SHARES_MULT = 1e18;

\t/* Constructor */

\tconstructor(address owner_) {
\t\tinit(owner_);
\t}

\t/* Initializer */

\tfunction init(address owner_) public virtual initializer {
\t\t__Context_init_unchained();
\t\t__Pausable_init_unchained();
\t\t_setupRole(DEFAULT_ADMIN_ROLE, owner_);
\t}

\tfunction postInit(ETHtxArgs memory _args)
\t\texternal
\t\tvirtual
\t\tonlyRole(DEFAULT_ADMIN_ROLE)
\t{
\t\taddress sender = _msgSender();

\t\t_feeLogic = _args.feeLogic;
\t\temit FeeLogicSet(sender, _args.feeLogic);

\t\tfor (uint256 i = 0; i < _args.minters.length; i++) {
\t\t\t_setupRole(MINTER_ROLE, _args.minters[i]);
\t\t}

\t\tfor (uint256 i = 0; i < _args.rebasers.length; i++) {
\t\t\t_setupRole(REBASER_ROLE, _args.rebasers[i]);
\t\t}

\t\t_sharesPerToken = _SHARES_MULT;
\t}

\tfunction postUpgrade(address feeLogic_, address[] memory rebasers)
\t\texternal
\t\tvirtual
\t{
\t\taddress sender = _msgSender();
\t\t// Can only be called once
\t\trequire(
\t\t\t_ownerDeprecated == sender,
\t\t\t\"ETHtx::postUpgrade: caller is not the owner\"
\t\t);

\t\t_feeLogic = feeLogic_;
\t\temit FeeLogicSet(sender, feeLogic_);

\t\t// Set sharesPerToken to 1:1
\t\t_totalShares = _totalSupply;
\t\t_sharesPerToken = _SHARES_MULT;

\t\t// Setup RBAC
\t\t_setupRole(DEFAULT_ADMIN_ROLE, sender);
\t\t_setupRole(MINTER_ROLE, _minterDeprecated);
\t\tfor (uint256 i = 0; i < rebasers.length; i++) {
\t\t\t_setupRole(REBASER_ROLE, rebasers[i]);
\t\t}

\t\t// Clear deprecated state
\t\t_minterDeprecated = address(0);
\t\t_ownerDeprecated = address(0);
\t}

\t/* External Mutators */

\tfunction transfer(address recipient, uint256 amount)
\t\tpublic
\t\tvirtual
\t\toverride
\t\treturns (bool)
\t{
\t\t_transfer(_msgSender(), recipient, amount);
\t\treturn true;
\t}

\tfunction transferFrom(
\t\taddress sender,
\t\taddress recipient,
\t\tuint256 amount
\t) public virtual override returns (bool) {
\t\t_transfer(sender, recipient, amount);
\t\t_approve(
\t\t\tsender,
\t\t\t_msgSender(),
\t\t\t_allowances[sender][_msgSender()].sub(
\t\t\t\tamount,
\t\t\t\t\"ETHtx::transferFrom: amount exceeds allowance\"
\t\t\t)
\t\t);
\t\treturn true;
\t}

\tfunction approve(address spender, uint256 amount)
\t\tpublic
\t\tvirtual
\t\toverride
\t\treturns (bool)
\t{
\t\t_approve(_msgSender(), spender, amount);
\t\treturn true;
\t}

\tfunction increaseAllowance(address spender, uint256 addedValue)
\t\tpublic
\t\tvirtual
\t\treturns (bool)
\t{
\t\t_approve(
\t\t\t_msgSender(),
\t\t\tspender,
\t\t\t_allowances[_msgSender()][spender].add(addedValue)
\t\t);
\t\treturn true;
\t}

\tfunction decreaseAllowance(address spender, uint256 subtractedValue)
\t\tpublic
\t\tvirtual
\t\treturns (bool)
\t{
\t\t_approve(
\t\t\t_msgSender(),
\t\t\tspender,
\t\t\t_allowances[_msgSender()][spender].sub(
\t\t\t\tsubtractedValue,
\t\t\t\t\"ETHtx::decreaseAllowance: below zero\"
\t\t\t)
\t\t);
\t\treturn true;
\t}

\tfunction burn(address account, uint256 amount)
\t\texternal
\t\tvirtual
\t\toverride
\t\tonlyRole(MINTER_ROLE)
\t\twhenNotPaused
\t{
\t\t_burn(account, amount);
\t}

\tfunction mint(address account, uint256 amount)
\t\texternal
\t\tvirtual
\t\toverride
\t\tonlyRole(MINTER_ROLE)
\t\twhenNotPaused
\t{
\t\t_mint(account, amount);
\t}

\tfunction pause()
\t\texternal
\t\tvirtual
\t\toverride
\t\tonlyRole(DEFAULT_ADMIN_ROLE)
\t\twhenNotPaused
\t{
\t\t_pause();
\t}

\tfunction rebase()
\t\texternal
\t\tvirtual
\t\toverride
\t\tonlyRole(REBASER_ROLE)
\t\twhenNotPaused
\t{
\t\t// Limit calls
\t\tuint256 timePassed =
\t\t\tblock.timestamp.sub(
\t\t\t\t_lastRebaseTime,
\t\t\t\t\"ETHtx::rebase: block is older than last rebase\"
\t\t\t);
\t\tIFeeLogic feeHandle = IFeeLogic(_feeLogic);
\t\trequire(
\t\t\ttimePassed >= feeHandle.rebaseInterval(),
\t\t\t\"ETHtx::rebase: too soon\"
\t\t);

\t\tuint256 initTotalShares = _totalShares;
\t\tif (initTotalShares == 0) {
\t\t\treturn;
\t\t}

\t\t(uint128 rebaseNum, uint128 rebaseDen) = feeHandle.rebaseFeeRate();

\t\t// WARN This will eventually overflow
\t\tuint256 ts = initTotalShares.mul(rebaseDen) / (rebaseDen - rebaseNum);
\t\tuint256 newShares = ts - initTotalShares;

\t\t// Send to exemptions to return them to their initial percentage
\t\tfor (uint256 i = 0; i < feeHandle.rebaseExemptsLength(); i++) {
\t\t\taddress exempt = feeHandle.rebaseExemptsAt(i);
\t\t\tuint256 balance = _balances[exempt];
\t\t\tif (balance != 0) {
\t\t\t\tuint256 newBalance = balance.mul(rebaseDen) / (rebaseDen - rebaseNum);
\t\t\t\tuint256 addedShares = newBalance - balance;
\t\t\t\t_balances[exempt] = newBalance;
\t\t\t\tnewShares -= addedShares;
\t\t\t}
\t\t}
\t\tassert(newShares < ts);

\t\t// Send the remainder to rewards
\t\taddress rewardsRecipient = feeHandle.recipient();
\t\t_balances[rewardsRecipient] = _balances[rewardsRecipient].add(newShares);

\t\t// Mint shares, reducing every holder's percentage
\t\t_totalShares = ts;
\t\t// WARN This will eventually overflow
\t\t_sharesPerToken = ts.mul(_SHARES_MULT).div(_totalSupply);

\t\t_lastRebaseTime = block.timestamp;

\t\temit Rebased(_msgSender(), ts);
\t}

\tfunction recoverERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external virtual override onlyRole(DEFAULT_ADMIN_ROLE) {
\t\tIERC20(token).safeTransfer(to, amount);
\t\temit Recovered(_msgSender(), token, to, amount);
\t}

\tfunction setFeeLogic(address account)
\t\texternal
\t\tvirtual
\t\toverride
\t\tonlyRole(DEFAULT_ADMIN_ROLE)
\t{
\t\trequire(account != address(0), \"ETHtx::setFeeLogic: zero address\");
\t\t_feeLogic = account;
\t\temit FeeLogicSet(_msgSender(), account);
\t}

\tfunction unpause()
\t\texternal
\t\tvirtual
\t\toverride
\t\tonlyRole(DEFAULT_ADMIN_ROLE)
\t\twhenPaused
\t{
\t\t_unpause();
\t}

\t/* External Views */

\tfunction allowance(address owner, address spender)
\t\tpublic
\t\tview
\t\tvirtual
\t\toverride
\t\treturns (uint256)
\t{
\t\treturn _allowances[owner][spender];
\t}

\tfunction balanceOf(address account)
\t\tpublic
\t\tview
\t\tvirtual
\t\toverride
\t\treturns (uint256)
\t{
\t\treturn _balances[account].mul(_SHARES_MULT).div(_sharesPerToken);
\t}

\tfunction sharesBalanceOf(address account)
\t\tpublic
\t\tview
\t\tvirtual
\t\toverride
\t\treturns (uint256)
\t{
\t\treturn _balances[account];
\t}

\tfunction name() public view virtual override returns (string memory) {
\t\treturn \"Ethereum Transaction\";
\t}

\tfunction symbol() public view virtual override returns (string memory) {
\t\treturn \"ETHtx\";
\t}

\tfunction decimals() public view virtual override returns (uint8) {
\t\treturn 18;
\t}

\tfunction feeLogic() public view virtual override returns (address) {
\t\treturn _feeLogic;
\t}

\tfunction lastRebaseTime() public view virtual override returns (uint256) {
\t\treturn _lastRebaseTime;
\t}

\tfunction sharesPerTokenX18()
\t\texternal
\t\tview
\t\tvirtual
\t\toverride
\t\treturns (uint256)
\t{
\t\treturn _sharesPerToken;
\t}

\tfunction totalShares() public view virtual override returns (uint256) {
\t\treturn _totalShares;
\t}

\tfunction totalSupply() public view virtual override returns (uint256) {
\t\treturn _totalSupply;
\t}

\t/* Internal Mutators */

\tfunction _approve(
\t\taddress owner,
\t\taddress spender,
\t\tuint256 amount
\t) internal virtual {
\t\trequire(owner != address(0), \"ETHtx::_approve: from the zero address\");
\t\trequire(spender != address(0), \"ETHtx::_approve: to the zero address\");

\t\t_allowances[owner][spender] = amount;
\t\temit Approval(owner, spender, amount);
\t}

\t/**
\t * @dev Implements an ERC20 transfer with a fee.
\t *
\t * Emits a {Transfer} event. Emits a second {Transfer} event for the fee.
\t *
\t * Requirements:
\t *
\t * - `sender` cannot be the zero address.
\t * - `recipient` cannot be the zero address.
\t * - `sender` must have a balance of at least `amount`.
\t * - `_feeLogic` implements {IFeeLogic}
\t */
\tfunction _transfer(
\t\taddress sender,
\t\taddress recipient,
\t\tuint256 amount
\t) internal virtual {
\t\trequire(sender != address(0), \"ETHtx::_transfer: from the zero address\");
\t\trequire(recipient != address(0), \"ETHtx::_transfer: to the zero address\");

\t\tuint256 spt = _sharesPerToken;
\t\trequire(spt != 0, \"ETHtx::_transfer: zero sharesPerToken\");

\t\t// Could round small values to zero
\t\tuint256 shares = amount.mul(spt).div(_SHARES_MULT);

\t\t_balances[sender] = _balances[sender].sub(
\t\t\tshares,
\t\t\t\"ETHtx::_transfer: amount exceeds balance\"
\t\t);

\t\tIFeeLogic feeHandler = IFeeLogic(_feeLogic);
\t\tuint256 fee = feeHandler.getFee(sender, recipient, shares);
\t\tuint256 sharesSubFee = shares.sub(fee);

\t\t_balances[recipient] = _balances[recipient].add(sharesSubFee);
\t\temit Transfer(sender, recipient, (sharesSubFee * _SHARES_MULT) / spt);

\t\tif (fee != 0) {
\t\t\taddress feeRecipient = feeHandler.recipient();
\t\t\t_balances[feeRecipient] = _balances[feeRecipient].add(fee);

\t\t\tuint256 feeInTokens = (fee * _SHARES_MULT) / spt;
\t\t\temit Transfer(sender, feeRecipient, feeInTokens);
\t\t\tfeeHandler.notify(feeInTokens);
\t\t}
\t}

\tfunction _burn(address account, uint256 amount) internal {
\t\t// Burn shares proportionately for constant _sharesPerToken
\t\tuint256 shares = amount.mul(_sharesPerToken) / _SHARES_MULT;
\t\t_balances[account] = _balances[account].sub(
\t\t\tshares,
\t\t\t\"ETHtx::_burn: amount exceeds balance\"
\t\t);
\t\t_totalShares = _totalShares.sub(shares);
\t\t// Burn tokens
\t\t_totalSupply = _totalSupply.sub(amount);

\t\temit Transfer(account, address(0), amount);
\t}

\tfunction _mint(address account, uint256 amount) internal {
\t\t// Mint shares proportionately for constant _sharesPerToken
\t\tuint256 shares = amount.mul(_sharesPerToken) / _SHARES_MULT;
\t\t_totalShares = _totalShares.add(shares);
\t\t_balances[account] = _balances[account].add(shares);
\t\t// Mint tokens
\t\t_totalSupply = _totalSupply.add(amount);

\t\temit Transfer(address(0), account, amount);
\t}
}
"
    
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

abstract contract ETHtxData {
\taddress internal _minterDeprecated;

\tuint256 internal _sharesPerToken;
\tuint256 internal _totalShares;

\tuint256 internal _lastRebaseTime;

\tuint256[46] private __gap;
}
"
    
// SPDX-License-Identifier: MIT

pragma solidity 0.7.6;

/**
 * @dev Interface for the optional metadata functions from the ERC20 standard.
 */
interface IERC20Metadata {
\t/**
\t * @dev Returns the name of the token.
\t */
\tfunction name() external view returns (string memory);

\t/**
\t * @dev Returns the symbol of the token.
\t */
\tfunction symbol() external view returns (string memory);

\t/**
\t * @dev Returns the decimals places of the token.
\t */
\tfunction decimals() external view returns (uint8);
}
"
    
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

interface IETHtx {
\t/* Views */

\tfunction feeLogic() external view returns (address);

\tfunction lastRebaseTime() external view returns (uint256);

\tfunction sharesBalanceOf(address account) external view returns (uint256);

\tfunction sharesPerTokenX18() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction burn(address account, uint256 amount) external;

\tfunction mint(address account, uint256 amount) external;

\tfunction pause() external;

\tfunction rebase() external;

\tfunction recoverERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction setFeeLogic(address account) external;

\tfunction unpause() external;

\t/* Events */

\tevent FeeLogicSet(address indexed author, address indexed account);
\tevent Rebased(address indexed author, uint256 totalShares);
\tevent Recovered(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

interface IETHtx {
\t/* Views */

\tfunction feeLogic() external view returns (address);

\tfunction lastRebaseTime() external view returns (uint256);

\tfunction sharesBalanceOf(address account) external view returns (uint256);

\tfunction sharesPerTokenX18() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction burn(address account, uint256 amount) external;

\tfunction mint(address account, uint256 amount) external;

\tfunction pause() external;

\tfunction rebase() external;

\tfunction recoverERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction setFeeLogic(address account) external;

\tfunction unpause() external;

\t/* Events */

\tevent FeeLogicSet(address indexed author, address indexed account);
\tevent Rebased(address indexed author, uint256 totalShares);
\tevent Recovered(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

interface IETHtx {
\t/* Views */

\tfunction feeLogic() external view returns (address);

\tfunction lastRebaseTime() external view returns (uint256);

\tfunction sharesBalanceOf(address account) external view returns (uint256);

\tfunction sharesPerTokenX18() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction burn(address account, uint256 amount) external;

\tfunction mint(address account, uint256 amount) external;

\tfunction pause() external;

\tfunction rebase() external;

\tfunction recoverERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction setFeeLogic(address account) external;

\tfunction unpause() external;

\t/* Events */

\tevent FeeLogicSet(address indexed author, address indexed account);
\tevent Rebased(address indexed author, uint256 totalShares);
\tevent Recovered(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

interface IETHtx {
\t/* Views */

\tfunction feeLogic() external view returns (address);

\tfunction lastRebaseTime() external view returns (uint256);

\tfunction sharesBalanceOf(address account) external view returns (uint256);

\tfunction sharesPerTokenX18() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction burn(address account, uint256 amount) external;

\tfunction mint(address account, uint256 amount) external;

\tfunction pause() external;

\tfunction rebase() external;

\tfunction recoverERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction setFeeLogic(address account) external;

\tfunction unpause() external;

\t/* Events */

\tevent FeeLogicSet(address indexed author, address indexed account);
\tevent Rebased(address indexed author, uint256 totalShares);
\tevent Recovered(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

interface IETHtx {
\t/* Views */

\tfunction feeLogic() external view returns (address);

\tfunction lastRebaseTime() external view returns (uint256);

\tfunction sharesBalanceOf(address account) external view returns (uint256);

\tfunction sharesPerTokenX18() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction burn(address account, uint256 amount) external;

\tfunction mint(address account, uint256 amount) external;

\tfunction pause() external;

\tfunction rebase() external;

\tfunction recoverERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction setFeeLogic(address account) external;

\tfunction unpause() external;

\t/* Events */

\tevent FeeLogicSet(address indexed author, address indexed account);
\tevent Rebased(address indexed author, uint256 totalShares);
\tevent Recovered(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

interface IETHtx {
\t/* Views */

\tfunction feeLogic() external view returns (address);

\tfunction lastRebaseTime() external view returns (uint256);

\tfunction sharesBalanceOf(address account) external view returns (uint256);

\tfunction sharesPerTokenX18() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction burn(address account, uint256 amount) external;

\tfunction mint(address account, uint256 amount) external;

\tfunction pause() external;

\tfunction rebase() external;

\tfunction recoverERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction setFeeLogic(address account) external;

\tfunction unpause() external;

\t/* Events */

\tevent FeeLogicSet(address indexed author, address indexed account);
\tevent Rebased(address indexed author, uint256 totalShares);
\tevent Recovered(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

interface IETHtx {
\t/* Views */

\tfunction feeLogic() external view returns (address);

\tfunction lastRebaseTime() external view returns (uint256);

\tfunction sharesBalanceOf(address account) external view returns (uint256);

\tfunction sharesPerTokenX18() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction burn(address account, uint256 amount) external;

\tfunction mint(address account, uint256 amount) external;

\tfunction pause() external;

\tfunction rebase() external;

\tfunction recoverERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction setFeeLogic(address account) external;

\tfunction unpause() external;

\t/* Events */

\tevent FeeLogicSet(address indexed author, address indexed account);
\tevent Rebased(address indexed author, uint256 totalShares);
\tevent Recovered(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

interface IETHtx {
\t/* Views */

\tfunction feeLogic() external view returns (address);

\tfunction lastRebaseTime() external view returns (uint256);

\tfunction sharesBalanceOf(address account) external view returns (uint256);

\tfunction sharesPerTokenX18() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction burn(address account, uint256 amount) external;

\tfunction mint(address account, uint256 amount) external;

\tfunction pause() external;

\tfunction rebase() external;

\tfunction recoverERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction setFeeLogic(address account) external;

\tfunction unpause() external;

\t/* Events */

\tevent FeeLogicSet(address indexed author, address indexed account);
\tevent Rebased(address indexed author, uint256 totalShares);
\tevent Recovered(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

interface IETHtx {
\t/* Views */

\tfunction feeLogic() external view returns (address);

\tfunction lastRebaseTime() external view returns (uint256);

\tfunction sharesBalanceOf(address account) external view returns (uint256);

\tfunction sharesPerTokenX18() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction burn(address account, uint256 amount) external;

\tfunction mint(address account, uint256 amount) external;

\tfunction pause() external;

\tfunction rebase() external;

\tfunction recoverERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction setFeeLogic(address account) external;

\tfunction unpause() external;

\t/* Events */

\tevent FeeLogicSet(address indexed author, address indexed account);
\tevent Rebased(address indexed author, uint256 totalShares);
\tevent Recovered(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

interface IETHtx {
\t/* Views */

\tfunction feeLogic() external view returns (address);

\tfunction lastRebaseTime() external view returns (uint256);

\tfunction sharesBalanceOf(address account) external view returns (uint256);

\tfunction sharesPerTokenX18() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction burn(address account, uint256 amount) external;

\tfunction mint(address account, uint256 amount) external;

\tfunction pause() external;

\tfunction rebase() external;

\tfunction recoverERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction setFeeLogic(address account) external;

\tfunction unpause() external;

\t/* Events */

\tevent FeeLogicSet(address indexed author, address indexed account);
\tevent Rebased(address indexed author, uint256 totalShares);
\tevent Recovered(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

interface IETHtx {
\t/* Views */

\tfunction feeLogic() external view returns (address);

\tfunction lastRebaseTime() external view returns (uint256);

\tfunction sharesBalanceOf(address account) external view returns (uint256);

\tfunction sharesPerTokenX18() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction burn(address account, uint256 amount) external;

\tfunction mint(address account, uint256 amount) external;

\tfunction pause() external;

\tfunction rebase() external;

\tfunction recoverERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction setFeeLogic(address account) external;

\tfunction unpause() external;

\t/* Events */

\tevent FeeLogicSet(address indexed author, address indexed account);
\tevent Rebased(address indexed author, uint256 totalShares);
\tevent Recovered(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

interface IETHtx {
\t/* Views */

\tfunction feeLogic() external view returns (address);

\tfunction lastRebaseTime() external view returns (uint256);

\tfunction sharesBalanceOf(address account) external view returns (uint256);

\tfunction sharesPerTokenX18() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction burn(address account, uint256 amount) external;

\tfunction mint(address account, uint256 amount) external;

\tfunction pause() external;

\tfunction rebase() external;

\tfunction recoverERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction setFeeLogic(address account) external;

\tfunction unpause() external;

\t/* Events */

\tevent FeeLogicSet(address indexed author, address indexed account);
\tevent Rebased(address indexed author, uint256 totalShares);
\tevent Recovered(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

interface IETHtx {
\t/* Views */

\tfunction feeLogic() external view returns (address);

\tfunction lastRebaseTime() external view returns (uint256);

\tfunction sharesBalanceOf(address account) external view returns (uint256);

\tfunction sharesPerTokenX18() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction burn(address account, uint256 amount) external;

\tfunction mint(address account, uint256 amount) external;

\tfunction pause() external;

\tfunction rebase() external;

\tfunction recoverERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction setFeeLogic(address account) external;

\tfunction unpause() external;

\t/* Events */

\tevent FeeLogicSet(address indexed author, address indexed account);
\tevent Rebased(address indexed author, uint256 totalShares);
\tevent Recovered(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

interface IETHtx {
\t/* Views */

\tfunction feeLogic() external view returns (address);

\tfunction lastRebaseTime() external view returns (uint256);

\tfunction sharesBalanceOf(address account) external view returns (uint256);

\tfunction sharesPerTokenX18() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction burn(address account, uint256 amount) external;

\tfunction mint(address account, uint256 amount) external;

\tfunction pause() external;

\tfunction rebase() external;

\tfunction recoverERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction setFeeLogic(address account) external;

\tfunction unpause() external;

\t/* Events */

\tevent FeeLogicSet(address indexed author, address indexed account);
\tevent Rebased(address indexed author, uint256 totalShares);
\tevent Recovered(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

interface IETHtx {
\t/* Views */

\tfunction feeLogic() external view returns (address);

\tfunction lastRebaseTime() external view returns (uint256);

\tfunction sharesBalanceOf(address account) external view returns (uint256);

\tfunction sharesPerTokenX18() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction burn(address account, uint256 amount) external;

\tfunction mint(address account, uint256 amount) external;

\tfunction pause() external;

\tfunction rebase() external;

\tfunction recoverERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction setFeeLogic(address account) external;

\tfunction unpause() external;

\t/* Events */

\tevent FeeLogicSet(address indexed author, address indexed account);
\tevent Rebased(address indexed author, uint256 totalShares);
\tevent Recovered(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

interface IETHtx {
\t/* Views */

\tfunction feeLogic() external view returns (address);

\tfunction lastRebaseTime() external view returns (uint256);

\tfunction sharesBalanceOf(address account) external view returns (uint256);

\tfunction sharesPerTokenX18() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction burn(address account, uint256 amount) external;

\tfunction mint(address account, uint256 amount) external;

\tfunction pause() external;

\tfunction rebase() external;

\tfunction recoverERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction setFeeLogic(address account) external;

\tfunction unpause() external;

\t/* Events */

\tevent FeeLogicSet(address indexed author, address indexed account);
\tevent Rebased(address indexed author, uint256 totalShares);
\tevent Recovered(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

interface IETHtx {
\t/* Views */

\tfunction feeLogic() external view returns (address);

\tfunction lastRebaseTime() external view returns (uint256);

\tfunction sharesBalanceOf(address account) external view returns (uint256);

\tfunction sharesPerTokenX18() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction burn(address account, uint256 amount) external;

\tfunction mint(address account, uint256 amount) external;

\tfunction pause() external;

\tfunction rebase() external;

\tfunction recoverERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction setFeeLogic(address account) external;

\tfunction unpause() external;

\t/* Events */

\tevent FeeLogicSet(address indexed author, address indexed account);
\tevent Rebased(address indexed author, uint256 totalShares);
\tevent Recovered(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

interface IETHtx {
\t/* Views */

\tfunction feeLogic() external view returns (address);

\tfunction lastRebaseTime() external view returns (uint256);

\tfunction sharesBalanceOf(address account) external view returns (uint256);

\tfunction sharesPerTokenX18() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction burn(address account, uint256 amount) external;

\tfunction mint(address account, uint256 amount) external;

\tfunction pause() external;

\tfunction rebase() external;

\tfunction recoverERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction setFeeLogic(address account) external;

\tfunction unpause() external;

\t/* Events */

\tevent FeeLogicSet(address indexed author, address indexed account);
\tevent Rebased(address indexed author, uint256 totalShares);
\tevent Recovered(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

interface IETHtx {
\t/* Views */

\tfunction feeLogic() external view returns (address);

\tfunction lastRebaseTime() external view returns (uint256);

\tfunction sharesBalanceOf(address account) external view returns (uint256);

\tfunction sharesPerTokenX18() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction burn(address account, uint256 amount) external;

\tfunction mint(address account, uint256 amount) external;

\tfunction pause() external;

\tfunction rebase() external;

\tfunction recoverERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction setFeeLogic(address account) external;

\tfunction unpause() external;

\t/* Events */

\tevent FeeLogicSet(address indexed author, address indexed account);
\tevent Rebased(address indexed author, uint256 totalShares);
\tevent Recovered(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

interface IETHtx {
\t/* Views */

\tfunction feeLogic() external view returns (address);

\tfunction lastRebaseTime() external view returns (uint256);

\tfunction sharesBalanceOf(address account) external view returns (uint256);

\tfunction sharesPerTokenX18() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction burn(address account, uint256 amount) external;

\tfunction mint(address account, uint256 amount) external;

\tfunction pause() external;

\tfunction rebase() external;

\tfunction recoverERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction setFeeLogic(address account) external;

\tfunction unpause() external;

\t/* Events */

\tevent FeeLogicSet(address indexed author, address indexed account);
\tevent Rebased(address indexed author, uint256 totalShares);
\tevent Recovered(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

interface IETHtx {
\t/* Views */

\tfunction feeLogic() external view returns (address);

\tfunction lastRebaseTime() external view returns (uint256);

\tfunction sharesBalanceOf(address account) external view returns (uint256);

\tfunction sharesPerTokenX18() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction burn(address account, uint256 amount) external;

\tfunction mint(address account, uint256 amount) external;

\tfunction pause() external;

\tfunction rebase() external;

\tfunction recoverERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction setFeeLogic(address account) external;

\tfunction unpause() external;

\t/* Events */

\tevent FeeLogicSet(address indexed author, address indexed account);
\tevent Rebased(address indexed author, uint256 totalShares);
\tevent Recovered(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

interface IETHtx {
\t/* Views */

\tfunction feeLogic() external view returns (address);

\tfunction lastRebaseTime() external view returns (uint256);

\tfunction sharesBalanceOf(address account) external view returns (uint256);

\tfunction sharesPerTokenX18() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction burn(address account, uint256 amount) external;

\tfunction mint(address account, uint256 amount) external;

\tfunction pause() external;

\tfunction rebase() external;

\tfunction recoverERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction setFeeLogic(address account) external;

\tfunction unpause() external;

\t/* Events */

\tevent FeeLogicSet(address indexed author, address indexed account);
\tevent Rebased(address indexed author, uint256 totalShares);
\tevent Recovered(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

interface IETHtx {
\t/* Views */

\tfunction feeLogic() external view returns (address);

\tfunction lastRebaseTime() external view returns (uint256);

\tfunction sharesBalanceOf(address account) external view returns (uint256);

\tfunction sharesPerTokenX18() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction burn(address account, uint256 amount) external;

\tfunction mint(address account, uint256 amount) external;

\tfunction pause() external;

\tfunction rebase() external;

\tfunction recoverERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction setFeeLogic(address account) external;

\tfunction unpause() external;

\t/* Events */

\tevent FeeLogicSet(address indexed author, address indexed account);
\tevent Rebased(address indexed author, uint256 totalShares);
\tevent Recovered(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

interface IETHtx {
\t/* Views */

\tfunction feeLogic() external view returns (address);

\tfunction lastRebaseTime() external view returns (uint256);

\tfunction sharesBalanceOf(address account) external view returns (uint256);

\tfunction sharesPerTokenX18() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction burn(address account, uint256 amount) external;

\tfunction mint(address account, uint256 amount) external;

\tfunction pause() external;

\tfunction rebase() external;

\tfunction recoverERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction setFeeLogic(address account) external;

\tfunction unpause() external;

\t/* Events */

\tevent FeeLogicSet(address indexed author, address indexed account);
\tevent Rebased(address indexed author, uint256 totalShares);
\tevent Recovered(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

interface IETHtx {
\t/* Views */

\tfunction feeLogic() external view returns (address);

\tfunction lastRebaseTime() external view returns (uint256);

\tfunction sharesBalanceOf(address account) external view returns (uint256);

\tfunction sharesPerTokenX18() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction burn(address account, uint256 amount) external;

\tfunction mint(address account, uint256 amount) external;

\tfunction pause() external;

\tfunction rebase() external;

\tfunction recoverERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction setFeeLogic(address account) external;

\tfunction unpause() external;

\t/* Events */

\tevent FeeLogicSet(address indexed author, address indexed account);
\tevent Rebased(address indexed author, uint256 totalShares);
\tevent Recovered(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

interface IETHtx {
\t/* Views */

\tfunction feeLogic() external view returns (address);

\tfunction lastRebaseTime() external view returns (uint256);

\tfunction sharesBalanceOf(address account) external view returns (uint256);

\tfunction sharesPerTokenX18() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction burn(address account, uint256 amount) external;

\tfunction mint(address account, uint256 amount) external;

\tfunction pause() external;

\tfunction rebase() external;

\tfunction recoverERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction setFeeLogic(address account) external;

\tfunction unpause() external;

\t/* Events */

\tevent FeeLogicSet(address indexed author, address indexed account);
\tevent Rebased(address indexed author, uint256 totalShares);
\tevent Recovered(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

interface IETHtx {
\t/* Views */

\tfunction feeLogic() external view returns (address);

\tfunction lastRebaseTime() external view returns (uint256);

\tfunction sharesBalanceOf(address account) external view returns (uint256);

\tfunction sharesPerTokenX18() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction burn(address account, uint256 amount) external;

\tfunction mint(address account, uint256 amount) external;

\tfunction pause() external;

\tfunction rebase() external;

\tfunction recoverERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction setFeeLogic(address account) external;

\tfunction unpause() external;

\t/* Events */

\tevent FeeLogicSet(address indexed author, address indexed account);
\tevent Rebased(address indexed author, uint256 totalShares);
\tevent Recovered(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

interface IETHtx {
\t/* Views */

\tfunction feeLogic() external view returns (address);

\tfunction lastRebaseTime() external view returns (uint256);

\tfunction sharesBalanceOf(address account) external view returns (uint256);

\tfunction sharesPerTokenX18() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction burn(address account, uint256 amount) external;

\tfunction mint(address account, uint256 amount) external;

\tfunction pause() external;

\tfunction rebase() external;

\tfunction recoverERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction setFeeLogic(address account) external;

\tfunction unpause() external;

\t/* Events */

\tevent FeeLogicSet(address indexed author, address indexed account);
\tevent Rebased(address indexed author, uint256 totalShares);
\tevent Recovered(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

interface IETHtx {
\t/* Views */

\tfunction feeLogic() external view returns (address);

\tfunction lastRebaseTime() external view returns (uint256);

\tfunction sharesBalanceOf(address account) external view returns (uint256);

\tfunction sharesPerTokenX18() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction burn(address account, uint256 amount) external;

\tfunction mint(address account, uint256 amount) external;

\tfunction pause() external;

\tfunction rebase() external;

\tfunction recoverERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction setFeeLogic(address account) external;

\tfunction unpause() external;

\t/* Events */

\tevent FeeLogicSet(address indexed author, address indexed account);
\tevent Rebased(address indexed author, uint256 totalShares);
\tevent Recovered(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

interface IETHtx {
\t/* Views */

\tfunction feeLogic() external view returns (address);

\tfunction lastRebaseTime() external view returns (uint256);

\tfunction sharesBalanceOf(address account) external view returns (uint256);

\tfunction sharesPerTokenX18() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction burn(address account, uint256 amount) external;

\tfunction mint(address account, uint256 amount) external;

\tfunction pause() external;

\tfunction rebase() external;

\tfunction recoverERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction setFeeLogic(address account) external;

\tfunction unpause() external;

\t/* Events */

\tevent FeeLogicSet(address indexed author, address indexed account);
\tevent Rebased(address indexed author, uint256 totalShares);
\tevent Recovered(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

interface IETHtx {
\t/* Views */

\tfunction feeLogic() external view returns (address);

\tfunction lastRebaseTime() external view returns (uint256);

\tfunction sharesBalanceOf(address account) external view returns (uint256);

\tfunction sharesPerTokenX18() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction burn(address account, uint256 amount) external;

\tfunction mint(address account, uint256 amount) external;

\tfunction pause() external;

\tfunction rebase() external;

\tfunction recoverERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction setFeeLogic(address account) external;

\tfunction unpause() external;

\t/* Events */

\tevent FeeLogicSet(address indexed author, address indexed account);
\tevent Rebased(address indexed author, uint256 totalShares);
\tevent Recovered(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

interface IETHtx {
\t/* Views */

\tfunction feeLogic() external view returns (address);

\tfunction lastRebaseTime() external view returns (uint256);

\tfunction sharesBalanceOf(address account) external view returns (uint256);

\tfunction sharesPerTokenX18() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction burn(address account, uint256 amount) external;

\tfunction mint(address account, uint256 amount) external;

\tfunction pause() external;

\tfunction rebase() external;

\tfunction recoverERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction setFeeLogic(address account) external;

\tfunction unpause() external;

\t/* Events */

\tevent FeeLogicSet(address indexed author, address indexed account);
\tevent Rebased(address indexed author, uint256 totalShares);
\tevent Recovered(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

interface IETHtx {
\t/* Views */

\tfunction feeLogic() external view returns (address);

\tfunction lastRebaseTime() external view returns (uint256);

\tfunction sharesBalanceOf(address account) external view returns (uint256);

\tfunction sharesPerTokenX18() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction burn(address account, uint256 amount) external;

\tfunction mint(address account, uint256 amount) external;

\tfunction pause() external;

\tfunction rebase() external;

\tfunction recoverERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction setFeeLogic(address account) external;

\tfunction unpause() external;

\t/* Events */

\tevent FeeLogicSet(address indexed author, address indexed account);
\tevent Rebased(address indexed author, uint256 totalShares);
\tevent Recovered(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

interface IETHtx {
\t/* Views */

\tfunction feeLogic() external view returns (address);

\tfunction lastRebaseTime() external view returns (uint256);

\tfunction sharesBalanceOf(address account) external view returns (uint256);

\tfunction sharesPerTokenX18() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction burn(address account, uint256 amount) external;

\tfunction mint(address account, uint256 amount) external;

\tfunction pause() external;

\tfunction rebase() external;

\tfunction recoverERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction setFeeLogic(address account) external;

\tfunction unpause() external;

\t/* Events */

\tevent FeeLogicSet(address indexed author, address indexed account);
\tevent Rebased(address indexed author, uint256 totalShares);
\tevent Recovered(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

interface IETHtx {
\t/* Views */

\tfunction feeLogic() external view returns (address);

\tfunction lastRebaseTime() external view returns (uint256);

\tfunction sharesBalanceOf(address account) external view returns (uint256);

\tfunction sharesPerTokenX18() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction burn(address account, uint256 amount) external;

\tfunction mint(address account, uint256 amount) external;

\tfunction pause() external;

\tfunction rebase() external;

\tfunction recoverERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction setFeeLogic(address account) external;

\tfunction unpause() external;

\t/* Events */

\tevent FeeLogicSet(address indexed author, address indexed account);
\tevent Rebased(address indexed author, uint256 totalShares);
\tevent Recovered(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

interface IETHtx {
\t/* Views */

\tfunction feeLogic() external view returns (address);

\tfunction lastRebaseTime() external view returns (uint256);

\tfunction sharesBalanceOf(address account) external view returns (uint256);

\tfunction sharesPerTokenX18() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction burn(address account, uint256 amount) external;

\tfunction mint(address account, uint256 amount) external;

\tfunction pause() external;

\tfunction rebase() external;

\tfunction recoverERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction setFeeLogic(address account) external;

\tfunction unpause() external;

\t/* Events */

\tevent FeeLogicSet(address indexed author, address indexed account);
\tevent Rebased(address indexed author, uint256 totalShares);
\tevent Recovered(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

interface IETHtx {
\t/* Views */

\tfunction feeLogic() external view returns (address);

\tfunction lastRebaseTime() external view returns (uint256);

\tfunction sharesBalanceOf(address account) external view returns (uint256);

\tfunction sharesPerTokenX18() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction burn(address account, uint256 amount) external;

\tfunction mint(address account, uint256 amount) external;

\tfunction pause() external;

\tfunction rebase() external;

\tfunction recoverERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction setFeeLogic(address account) external;

\tfunction unpause() external;

\t/* Events */

\tevent FeeLogicSet(address indexed author, address indexed account);
\tevent Rebased(address indexed author, uint256 totalShares);
\tevent Recovered(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

interface IETHtx {
\t/* Views */

\tfunction feeLogic() external view returns (address);

\tfunction lastRebaseTime() external view returns (uint256);

\tfunction sharesBalanceOf(address account) external view returns (uint256);

\tfunction sharesPerTokenX18() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction burn(address account, uint256 amount) external;

\tfunction mint(address account, uint256 amount) external;

\tfunction pause() external;

\tfunction rebase() external;

\tfunction recoverERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction setFeeLogic(address account) external;

\tfunction unpause() external;

\t/* Events */

\tevent FeeLogicSet(address indexed author, address indexed account);
\tevent Rebased(address indexed author, uint256 totalShares);
\tevent Recovered(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

interface IETHtx {
\t/* Views */

\tfunction feeLogic() external view returns (address);

\tfunction lastRebaseTime() external view returns (uint256);

\tfunction sharesBalanceOf(address account) external view returns (uint256);

\tfunction sharesPerTokenX18() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction burn(address account, uint256 amount) external;

\tfunction mint(address account, uint256 amount) external;

\tfunction pause() external;

\tfunction rebase() external;

\tfunction recoverERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction setFeeLogic(address account) external;

\tfunction unpause() external;

\t/* Events */

\tevent FeeLogicSet(address indexed author, address indexed account);
\tevent Rebased(address indexed author, uint256 totalShares);
\tevent Recovered(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

interface IETHtx {
\t/* Views */

\tfunction feeLogic() external view returns (address);

\tfunction lastRebaseTime() external view returns (uint256);

\tfunction sharesBalanceOf(address account) external view returns (uint256);

\tfunction sharesPerTokenX18() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction burn(address account, uint256 amount) external;

\tfunction mint(address account, uint256 amount) external;

\tfunction pause() external;

\tfunction rebase() external;

\tfunction recoverERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction setFeeLogic(address account) external;

\tfunction unpause() external;

\t/* Events */

\tevent FeeLogicSet(address indexed author, address indexed account);
\tevent Rebased(address indexed author, uint256 totalShares);
\tevent Recovered(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

interface IETHtx {
\t/* Views */

\tfunction feeLogic() external view returns (address);

\tfunction lastRebaseTime() external view returns (uint256);

\tfunction sharesBalanceOf(address account) external view returns (uint256);

\tfunction sharesPerTokenX18() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction burn(address account, uint256 amount) external;

\tfunction mint(address account, uint256 amount) external;

\tfunction pause() external;

\tfunction rebase() external;

\tfunction recoverERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction setFeeLogic(address account) external;

\tfunction unpause() external;

\t/* Events */

\tevent FeeLogicSet(address indexed author, address indexed account);
\tevent Rebased(address indexed author, uint256 totalShares);
\tevent Recovered(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

interface IETHtx {
\t/* Views */

\tfunction feeLogic() external view returns (address);

\tfunction lastRebaseTime() external view returns (uint256);

\tfunction sharesBalanceOf(address account) external view returns (uint256);

\tfunction sharesPerTokenX18() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction burn(address account, uint256 amount) external;

\tfunction mint(address account, uint256 amount) external;

\tfunction pause() external;

\tfunction rebase() external;

\tfunction recoverERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction setFeeLogic(address account) external;

\tfunction unpause() external;

\t/* Events */

\tevent FeeLogicSet(address indexed author, address indexed account);
\tevent Rebased(address indexed author, uint256 totalShares);
\tevent Recovered(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

interface IETHtx {
\t/* Views */

\tfunction feeLogic() external view returns (address);

\tfunction lastRebaseTime() external view returns (uint256);

\tfunction sharesBalanceOf(address account) external view returns (uint256);

\tfunction sharesPerTokenX18() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction burn(address account, uint256 amount) external;

\tfunction mint(address account, uint256 amount) external;

\tfunction pause() external;

\tfunction rebase() external;

\tfunction recoverERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction setFeeLogic(address account) external;

\tfunction unpause() external;

\t/* Events */

\tevent FeeLogicSet(address indexed author, address indexed account);
\tevent Rebased(address indexed author, uint256 totalShares);
\tevent Recovered(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

interface IETHtx {
\t/* Views */

\tfunction feeLogic() external view returns (address);

\tfunction lastRebaseTime() external view returns (uint256);

\tfunction sharesBalanceOf(address account) external view returns (uint256);

\tfunction sharesPerTokenX18() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction burn(address account, uint256 amount) external;

\tfunction mint(address account, uint256 amount) external;

\tfunction pause() external;

\tfunction rebase() external;

\tfunction recoverERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction setFeeLogic(address account) external;

\tfunction unpause() external;

\t/* Events */

\tevent FeeLogicSet(address indexed author, address indexed account);
\tevent Rebased(address indexed author, uint256 totalShares);
\tevent Recovered(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

interface IETHtx {
\t/* Views */

\tfunction feeLogic() external view returns (address);

\tfunction lastRebaseTime() external view returns (uint256);

\tfunction sharesBalanceOf(address account) external view returns (uint256);

\tfunction sharesPerTokenX18() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction burn(address account, uint256 amount) external;

\tfunction mint(address account, uint256 amount) external;

\tfunction pause() external;

\tfunction rebase() external;

\tfunction recoverERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction setFeeLogic(address account) external;

\tfunction unpause() external;

\t/* Events */

\tevent FeeLogicSet(address indexed author, address indexed account);
\tevent Rebased(address indexed author, uint256 totalShares);
\tevent Recovered(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

interface IETHtx {
\t/* Views */

\tfunction feeLogic() external view returns (address);

\tfunction lastRebaseTime() external view returns (uint256);

\tfunction sharesBalanceOf(address account) external view returns (uint256);

\tfunction sharesPerTokenX18() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction burn(address account, uint256 amount) external;

\tfunction mint(address account, uint256 amount) external;

\tfunction pause() external;

\tfunction rebase() external;

\tfunction recoverERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction setFeeLogic(address account) external;

\tfunction unpause() external;

\t/* Events */

\tevent FeeLogicSet(address indexed author, address indexed account);
\tevent Rebased(address indexed author, uint256 totalShares);
\tevent Recovered(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

interface IETHtx {
\t/* Views */

\tfunction feeLogic() external view returns (address);

\tfunction lastRebaseTime() external view returns (uint256);

\tfunction sharesBalanceOf(address account) external view returns (uint256);

\tfunction sharesPerTokenX18() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction burn(address account, uint256 amount) external;

\tfunction mint(address account, uint256 amount) external;

\tfunction pause() external;

\tfunction rebase() external;

\tfunction recoverERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction setFeeLogic(address account) external;

\tfunction unpause() external;

\t/* Events */

\tevent FeeLogicSet(address indexed author, address indexed account);
\tevent Rebased(address indexed author, uint256 totalShares);
\tevent Recovered(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

interface IETHtx {
\t/* Views */

\tfunction feeLogic() external view returns (address);

\tfunction lastRebaseTime() external view returns (uint256);

\tfunction sharesBalanceOf(address account) external view returns (uint256);

\tfunction sharesPerTokenX18() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction burn(address account, uint256 amount) external;

\tfunction mint(address account, uint256 amount) external;

\tfunction pause() external;

\tfunction rebase() external;

\tfunction recoverERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction setFeeLogic(address account) external;

\tfunction unpause() external;

\t/* Events */

\tevent FeeLogicSet(address indexed author, address indexed account);
\tevent Rebased(address indexed author, uint256 totalShares);
\tevent Recovered(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

interface IETHtx {
\t/* Views */

\tfunction feeLogic() external view returns (address);

\tfunction lastRebaseTime() external view returns (uint256);

\tfunction sharesBalanceOf(address account) external view returns (uint256);

\tfunction sharesPerTokenX18() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction burn(address account, uint256 amount) external;

\tfunction mint(address account, uint256 amount) external;

\tfunction pause() external;

\tfunction rebase() external;

\tfunction recoverERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction setFeeLogic(address account) external;

\tfunction unpause() external;

\t/* Events */

\tevent FeeLogicSet(address indexed author, address indexed account);
\tevent Rebased(address indexed author, uint256 totalShares);
\tevent Recovered(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

interface IETHtx {
\t/* Views */

\tfunction feeLogic() external view returns (address);

\tfunction lastRebaseTime() external view returns (uint256);

\tfunction sharesBalanceOf(address account) external view returns (uint256);

\tfunction sharesPerTokenX18() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction burn(address account, uint256 amount) external;

\tfunction mint(address account, uint256 amount) external;

\tfunction pause() external;

\tfunction rebase() external;

\tfunction recoverERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction setFeeLogic(address account) external;

\tfunction unpause() external;

\t/* Events */

\tevent FeeLogicSet(address indexed author, address indexed account);
\tevent Rebased(address indexed author, uint256 totalShares);
\tevent Recovered(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

interface IETHtx {
\t/* Views */

\tfunction feeLogic() external view returns (address);

\tfunction lastRebaseTime() external view returns (uint256);

\tfunction sharesBalanceOf(address account) external view returns (uint256);

\tfunction sharesPerTokenX18() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction burn(address account, uint256 amount) external;

\tfunction mint(address account, uint256 amount) external;

\tfunction pause() external;

\tfunction rebase() external;

\tfunction recoverERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction setFeeLogic(address account) external;

\tfunction unpause() external;

\t/* Events */

\tevent FeeLogicSet(address indexed author, address indexed account);
\tevent Rebased(address indexed author, uint256 totalShares);
\tevent Recovered(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

interface IETHtx {
\t/* Views */

\tfunction feeLogic() external view returns (address);

\tfunction lastRebaseTime() external view returns (uint256);

\tfunction sharesBalanceOf(address account) external view returns (uint256);

\tfunction sharesPerTokenX18() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction burn(address account, uint256 amount) external;

\tfunction mint(address account, uint256 amount) external;

\tfunction pause() external;

\tfunction rebase() external;

\tfunction recoverERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction setFeeLogic(address account) external;

\tfunction unpause() external;

\t/* Events */

\tevent FeeLogicSet(address indexed author, address indexed account);
\tevent Rebased(address indexed author, uint256 totalShares);
\tevent Recovered(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

interface IETHtx {
\t/* Views */

\tfunction feeLogic() external view returns (address);

\tfunction lastRebaseTime() external view returns (uint256);

\tfunction sharesBalanceOf(address account) external view returns (uint256);

\tfunction sharesPerTokenX18() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction burn(address account, uint256 amount) external;

\tfunction mint(address account, uint256 amount) external;

\tfunction pause() external;

\tfunction rebase() external;

\tfunction recoverERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction setFeeLogic(address account) external;

\tfunction unpause() external;

\t/* Events */

\tevent FeeLogicSet(address indexed author, address indexed account);
\tevent Rebased(address indexed author, uint256 totalShares);
\tevent Recovered(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

interface IETHtx {
\t/* Views */

\tfunction feeLogic() external view returns (address);

\tfunction lastRebaseTime() external view returns (uint256);

\tfunction sharesBalanceOf(address account) external view returns (uint256);

\tfunction sharesPerTokenX18() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction burn(address account, uint256 amount) external;

\tfunction mint(address account, uint256 amount) external;

\tfunction pause() external;

\tfunction rebase() external;

\tfunction recoverERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction setFeeLogic(address account) external;

\tfunction unpause() external;

\t/* Events */

\tevent FeeLogicSet(address indexed author, address indexed account);
\tevent Rebased(address indexed author, uint256 totalShares);
\tevent Recovered(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

interface IETHtx {
\t/* Views */

\tfunction feeLogic() external view returns (address);

\tfunction lastRebaseTime() external view returns (uint256);

\tfunction sharesBalanceOf(address account) external view returns (uint256);

\tfunction sharesPerTokenX18() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction burn(address account, uint256 amount) external;

\tfunction mint(address account, uint256 amount) external;

\tfunction pause() external;

\tfunction rebase() external;

\tfunction recoverERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction setFeeLogic(address account) external;

\tfunction unpause() external;

\t/* Events */

\tevent FeeLogicSet(address indexed author, address indexed account);
\tevent Rebased(address indexed author, uint256 totalShares);
\tevent Recovered(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
}
"
    }
  }
}
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

interface IETHtx {
\t/* Views */

\tfunction feeLogic() external view returns (address);

\tfunction lastRebaseTime() external view returns (uint256);

\tfunction sharesBalanceOf(address account) external view returns (uint256);

\tfunction sharesPerTokenX18() external view returns (uint256);

\tfunction totalShares() external view returns (uint256);

\t/* Mutators */

\tfunction burn(address account, uint256 amount) external;

\tfunction mint(address account, uint256 amount) external;

\tfunction pause() external;

\tfunction rebase() external;

\tfunction recoverERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction setFeeLogic(address account) external;

\tfunction unpause() external;

\t/* Events */

\tevent FeeLogicSet(address indexed author, address indexed account);
\tevent Rebased(address indexed author, uint256 totalShares);
\tevent Recovered(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
}
"
    }
  }
}
