pragma solidity 0.5.12;

import \"./IERC777.sol\";
import \"./IERC20.sol\";
import \"./LBasicToken.sol\";
import \"./PDelegate.sol\";
import \"./Owned.sol\";

contract BasicToken is IERC777, IERC20, Owned, PDelegate {

  uint8 public constant decimals = 18;
  uint256 public constant granularity = 1;
  string public name;
  string public symbol;

  LBasicToken.TokenState private tokenState;
  address public extensionContract; // TODO: Move to tokenState?

  event LogContractExtended(address indexed extensionContract);

  constructor(string memory _name, string memory _symbol, uint256 _initialSupply)
    public
  {
    require(bytes(_name).length != 0, \"Needs a name\");
    require(bytes(_symbol).length != 0, \"Needs a symbol\");
    name = _name;
    symbol = _symbol;
    LBasicToken.init(tokenState, decimals, _initialSupply);
  }

  modifier onlyOperator(address _holder) {
    require(isOperatorFor(msg.sender, _holder), \"Not an operator\");
    _;
  }

  // MUST be overriden by any extension contract to avoid recursion of delegateFwd calls.
  function ()
    external
  {
    require(extensionContract != address(0), \"Extended functionality contract not found\");
    delegatedFwd(extensionContract, msg.data);
  }

  function extend(address _extensionContract)
    external
    onlyOwner
  {
    extensionContract = _extensionContract;
    emit LogContractExtended(_extensionContract);
  }

  function balanceOf(address _holder)
    external
    view
    returns (uint256 balance_)
  {
    balance_ = tokenState.balances[_holder];
  }

  function transfer(address _to, uint256 _value)
    external
    returns (bool success_)
  {
    doSend(msg.sender, msg.sender, _to, _value, \"\", \"\", false);
    success_ = true;
  }

  function transferFrom(address _from, address _to, uint256 _value)
    external
    returns (bool success_)
  {
    LBasicToken.transferFrom(tokenState, _from, _to, _value);
    success_ = true;
  }

  function approve(address _spender, uint256 _value)
    external
    returns (bool success_)
  {
    LBasicToken.approve(tokenState, _spender, _value);
    success_ = true;
  }

  function allowance(address _holder, address _spender)
    external
    view
    returns (uint256 remaining_)
  {
    remaining_ = tokenState.approvals[_holder][_spender];
  }

  function defaultOperators()
    external
    view
    returns (address[] memory)
  {
    return tokenState.defaultOperators;
  }

  function authorizeOperator(address _operator)
    external
  {
    LBasicToken.authorizeOperator(tokenState, _operator);
  }

  function revokeOperator(address _operator)
    external
  {
    LBasicToken.revokeOperator(tokenState, _operator);
  }

  function send(address _to, uint256 _amount, bytes calldata _data)
    external
  {
    doSend(msg.sender, msg.sender, _to, _amount, _data, \"\", true);
  }

  function operatorSend(address _from, address _to, uint256 _amount, bytes calldata _data, bytes calldata _operatorData)
    external
    onlyOperator(_from)
  {
    doSend(msg.sender, _from, _to, _amount, _data, _operatorData, true);
  }

  function burn(uint256 _amount, bytes calldata _data)
    external
  {
    doBurn(msg.sender, msg.sender, _amount, _data, \"\");
  }

  function operatorBurn(address _from, uint256 _amount, bytes calldata _data, bytes calldata _operatorData)
    external
    onlyOperator(_from)
  {
    doBurn(msg.sender, _from, _amount, _data, _operatorData);
  }

  function totalSupply()
    external
    view
    returns (uint256 totalSupply_)
  {
    totalSupply_ = tokenState.totalSupply;
  }

  function isOperatorFor(address _operator, address _holder)
    public
    view
    returns (bool isOperatorFor_)
  {
    isOperatorFor_ = (_operator == _holder || tokenState.authorizedOperators[_operator][_holder]
        || _operator == address(this) \u0026\u0026 !tokenState.defaultOperatorIsRevoked[_holder]);
  }

  function doSend(address _operator, address _from, address _to, uint256 _amount, bytes memory _data,
      bytes memory _operatorData, bool _enforceERC777)
    internal
  {
    LBasicToken.doSend(tokenState, _operator, _from, _to, _amount, _data, _operatorData, _enforceERC777);
  }

  function doMint(address _to, uint256 _amount)
    internal
  {
    LBasicToken.doMint(tokenState, _to, _amount);
  }

  function doBurn(address _operator, address _from, uint256 _amount, bytes memory _data, bytes memory _operatorData)
    internal
  {
    LBasicToken.doBurn(tokenState, _operator, _from, _amount, _data, _operatorData);
  }
}"},"IERC1820Implementer.sol":{"content":"pragma solidity 0.5.12;

// As defined in https://eips.ethereum.org/EIPS/eip-1820 for use in contracts the 1820 registry needs to call
interface IERC1820Implementer {
  function canImplementInterfaceForAddress(bytes32 interfaceHash, address account) external view returns (bytes32);
}"},"IERC1820Registry.sol":{"content":"// From open https://github.com/OpenZeppelin/openzeppelin-contracts/blob/master/contracts/introspection/IERC1820Registry.sol

pragma solidity 0.5.12;

/**
 * @dev Interface of the global ERC1820 Registry, as defined in the
 * https://eips.ethereum.org/EIPS/eip-1820[EIP]. Accounts may register
 * implementers for interfaces in this registry, as well as query support.
 *
 * Implementers may be shared by multiple accounts, and can also implement more
 * than a single interface for each account. Contracts can implement interfaces
 * for themselves, but externally-owned accounts (EOA) must delegate this to a
 * contract.
 *
 * {IERC165} interfaces can also be queried via the registry.
 *
 * For an in-depth explanation and source code analysis, see the EIP text.
 */
interface IERC1820Registry {
    /**
     * @dev Sets `newManager` as the manager for `account`. A manager of an
     * account is able to set interface implementers for it.
     *
     * By default, each account is its own manager. Passing a value of `0x0` in
     * `newManager` will reset the manager to this initial state.
     *
     * Emits a {ManagerChanged} event.
     *
     * Requirements:
     *
     * - the caller must be the current manager for `account`.
     */
    function setManager(address account, address newManager) external;

    /**
     * @dev Returns the manager for `account`.
     *
     * See {setManager}.
     */
    function getManager(address account) external view returns (address);

    /**
     * @dev Sets the `implementer` contract as `account`\u0027s implementer for
     * `interfaceHash`.
     *
     * `account` being the zero address is an alias for the caller\u0027s address.
     * The zero address can also be used in `implementer` to remove an old one.
     *
     * See {interfaceHash} to learn how these are created.
     *
     * Emits an {InterfaceImplementerSet} event.
     *
     * Requirements:
     *
     * - the caller must be the current manager for `account`.
     * - `interfaceHash` must not be an {IERC165} interface id (i.e. it must not
     * end in 28 zeroes).
     * - `implementer` must implement {IERC1820Implementer} and return true when
     * queried for support, unless `implementer` is the caller. See
     * {IERC1820Implementer-canImplementInterfaceForAddress}.
     */
    function setInterfaceImplementer(address account, bytes32 interfaceHash, address implementer) external;

    /**
     * @dev Returns the implementer of `interfaceHash` for `account`. If no such
     * implementer is registered, returns the zero address.
     *
     * If `interfaceHash` is an {IERC165} interface id (i.e. it ends with 28
     * zeroes), `account` will be queried for support of it.
     *
     * `account` being the zero address is an alias for the caller\u0027s address.
     */
    function getInterfaceImplementer(address account, bytes32 interfaceHash) external view returns (address);

    /**
     * @dev Returns the interface hash for an `interfaceName`, as defined in the
     * corresponding
     * https://eips.ethereum.org/EIPS/eip-1820#interface-name[section of the EIP].
     */
    function interfaceHash(string calldata interfaceName) external pure returns (bytes32);

    /**
     *  @notice Updates the cache with whether the contract implements an ERC165 interface or not.
     *  @param account Address of the contract for which to update the cache.
     *  @param interfaceId ERC165 interface for which to update the cache.
     */
    function updateERC165Cache(address account, bytes4 interfaceId) external;

    /**
     *  @notice Checks whether a contract implements an ERC165 interface or not.
     *  If the result is not cached a direct lookup on the contract address is performed.
     *  If the result is not cached or the cached value is out-of-date, the cache MUST be updated manually by calling
     *  {updateERC165Cache} with the contract address.
     *  @param account Address of the contract to check.
     *  @param interfaceId ERC165 interface to check.
     *  @return True if `account` implements `interfaceId`, false otherwise.
     */
    function implementsERC165Interface(address account, bytes4 interfaceId) external view returns (bool);

    /**
     *  @notice Checks whether a contract implements an ERC165 interface or not without using nor updating the cache.
     *  @param account Address of the contract to check.
     *  @param interfaceId ERC165 interface to check.
     *  @return True if `account` implements `interfaceId`, false otherwise.
     */
    function implementsERC165InterfaceNoCache(address account, bytes4 interfaceId) external view returns (bool);

    event InterfaceImplementerSet(address indexed account, bytes32 indexed interfaceHash, address indexed implementer);

    event ManagerChanged(address indexed account, address indexed newManager);
}"},"IERC20.sol":{"content":"pragma solidity 0.5.12;

// https://eips.ethereum.org/EIPS/eip-20
interface IERC20 {
  function name() external view returns (string memory); // optional method - see eip spec
  function symbol() external view returns (string memory); // optional method - see eip spec
  function decimals() external view returns (uint8); // optional method - see eip spec
  function totalSupply() external view returns (uint256);
  function balanceOf(address owner) external view returns (uint256);
  function transfer(address to, uint256 value) external returns (bool);
  function transferFrom(address from, address to, uint256 value) external returns (bool);
  function approve(address spender, uint256 value) external returns (bool);
  function allowance(address owner, address spender) external view returns (uint256);

  event Transfer(address indexed from, address indexed to, uint256 value);
  event Approval(address indexed owner, address indexed spender, uint256 value);
}"},"IERC777.sol":{"content":"pragma solidity 0.5.12;

// https://eips.ethereum.org/EIPS/eip-777
interface IERC777 {
    function name() external view returns (string memory);
    function symbol() external view returns (string memory);
    function totalSupply() external view returns (uint256);
    function balanceOf(address holder) external view returns (uint256);
    function granularity() external view returns (uint256);

    function defaultOperators() external view returns (address[] memory);
    function isOperatorFor(
        address operator,
        address holder
    ) external view returns (bool);
    function authorizeOperator(address operator) external;
    function revokeOperator(address operator) external;

    function send(address to, uint256 amount, bytes calldata data) external;
    function operatorSend(
        address from,
        address to,
        uint256 amount,
        bytes calldata data,
        bytes calldata operatorData
    ) external;

    function burn(uint256 amount, bytes calldata data) external;
    function operatorBurn(
        address from,
        uint256 amount,
        bytes calldata data,
        bytes calldata operatorData
    ) external;

    event Sent(
        address indexed operator,
        address indexed from,
        address indexed to,
        uint256 amount,
        bytes data,
        bytes operatorData
    );
    event Minted(
        address indexed operator,
        address indexed to,
        uint256 amount,
        bytes data,
        bytes operatorData
    );
    event Burned(
        address indexed operator,
        address indexed from,
        uint256 amount,
        bytes data,
        bytes operatorData
    );
    event AuthorizedOperator(
        address indexed operator,
        address indexed holder
    );
    event RevokedOperator(address indexed operator, address indexed holder);
}"},"IERC777Recipient.sol":{"content":"pragma solidity 0.5.12;

// As defined in the \u0027ERC777TokensRecipient And The tokensReceived Hook\u0027 section of https://eips.ethereum.org/EIPS/eip-777
interface IERC777Recipient {
  function tokensReceived(address operator, address from, address to, uint256 amount, bytes calldata data,
    bytes calldata operatorData) external;
}"},"IERC777Sender.sol":{"content":"pragma solidity 0.5.12;

// As defined in the \u0027ERC777TokensSender And The tokensToSend Hook\u0027 section of https://eips.ethereum.org/EIPS/eip-777
interface IERC777Sender {
  function tokensToSend(address operator, address from, address to, uint256 amount, bytes calldata data,
      bytes calldata operatorData) external;
}"},"LBasicToken.sol":{"content":"pragma solidity 0.5.12;

import \"./IERC1820Registry.sol\";
import \"./IERC777Sender.sol\";
import \"./IERC777Recipient.sol\";
import \"./SafeMath.sol\";

library LBasicToken {
  using SafeMath for uint256;

  event Transfer(address indexed from, address indexed to, uint256 value);
  event Approval(address indexed owner, address indexed spender, uint256 value);
  event Sent(address indexed operator, address indexed from, address indexed to, uint256 amount, bytes data,
      bytes operatorData);
  event Minted(address indexed operator, address indexed to, uint256 amount, bytes data, bytes operatorData);
  event Burned(address indexed operator, address indexed from, uint256 amount, bytes data, bytes operatorData);
  event AuthorizedOperator(address indexed operator, address indexed holder);
  event RevokedOperator(address indexed operator, address indexed holder);

  // Universal address as defined in Registry Contract Address section of https://eips.ethereum.org/EIPS/eip-1820
  IERC1820Registry constant internal ERC1820_REGISTRY = IERC1820Registry(0x1820a4B7618BdE71Dce8cdc73aAB6C95905faD24);
  bytes32 constant internal TOKENS_SENDER_INTERFACE_HASH = keccak256(\"ERC777TokensSender\");
  bytes32 constant internal TOKENS_RECIPIENT_INTERFACE_HASH = keccak256(\"ERC777TokensRecipient\");

  struct TokenState {
    uint256 totalSupply;
    mapping(address =\u003e uint256) balances;
    mapping(address =\u003e mapping(address =\u003e uint256)) approvals;
    mapping(address =\u003e mapping(address =\u003e bool)) authorizedOperators;
    address[] defaultOperators;
    mapping(address =\u003e bool) defaultOperatorIsRevoked;
  }

  function init(TokenState storage _tokenState, uint8 _decimals, uint256 _initialSupply)
    external
  {
    _tokenState.defaultOperators.push(address(this));
    _tokenState.totalSupply = _initialSupply.mul(10**uint256(_decimals));
    _tokenState.balances[msg.sender] = _tokenState.totalSupply;
    ERC1820_REGISTRY.setInterfaceImplementer(address(this), keccak256(\"ERC777Token\"), address(this));
    ERC1820_REGISTRY.setInterfaceImplementer(address(this), keccak256(\"ERC20Token\"), address(this));
  }

  function transferFrom(TokenState storage _tokenState, address _from, address _to, uint256 _value)
    external
  {
    require(_tokenState.approvals[_from][msg.sender] \u003e= _value, \"Amount not approved\");
    _tokenState.approvals[_from][msg.sender] = _tokenState.approvals[_from][msg.sender].sub(_value);
    doSend(_tokenState, msg.sender, _from, _to, _value, \"\", \"\", false);
  }

  function approve(TokenState storage _tokenState, address _spender, uint256 _value)
    external
  {
    require(_spender != address(0), \"Cannot approve to zero address\");
    _tokenState.approvals[msg.sender][_spender] = _value;
    emit Approval(msg.sender, _spender, _value);
  }

  function authorizeOperator(TokenState storage _tokenState, address _operator)
    external
  {
    require(_operator != msg.sender, \"Self cannot be operator\");
    if (_operator == address(this))
      _tokenState.defaultOperatorIsRevoked[msg.sender] = false;
    else
      _tokenState.authorizedOperators[_operator][msg.sender] = true;
    emit AuthorizedOperator(_operator, msg.sender);
  }

  function revokeOperator(TokenState storage _tokenState, address _operator)
    external
  {
    require(_operator != msg.sender, \"Self cannot be operator\");
    if (_operator == address(this))
      _tokenState.defaultOperatorIsRevoked[msg.sender] = true;
    else
      _tokenState.authorizedOperators[_operator][msg.sender] = false;
    emit RevokedOperator(_operator, msg.sender);
  }

  function doMint(TokenState storage _tokenState, address _to, uint256 _amount)
    external
  {
    assert(_to != address(0));

    _tokenState.totalSupply = _tokenState.totalSupply.add(_amount);
    _tokenState.balances[_to] = _tokenState.balances[_to].add(_amount);

    // From ERC777: The token contract MUST call the tokensReceived hook after updating the state.
    receiveHook(address(this), address(0), _to, _amount, \"\", \"\", true);

    emit Minted(address(this), _to, _amount, \"\", \"\");
    emit Transfer(address(0), _to, _amount);
  }

  function doBurn(TokenState storage _tokenState, address _operator, address _from, uint256 _amount, bytes calldata _data,
      bytes calldata _operatorData)
    external
  {
    assert(_from != address(0));
    // From ERC777: The token contract MUST call the tokensToSend hook before updating the state.
    sendHook(_operator, _from, address(0), _amount, _data, _operatorData);

    _tokenState.balances[_from] = _tokenState.balances[_from].sub(_amount);
    _tokenState.totalSupply = _tokenState.totalSupply.sub(_amount);

    emit Burned(_operator, _from, _amount, _data, _operatorData);
    emit Transfer(_from, address(0), _amount);
  }

  function doSend(TokenState storage _tokenState, address _operator, address _from, address _to, uint256 _amount,
      bytes memory _data, bytes memory _operatorData, bool _enforceERC777)
    public
  {
    assert(_from != address(0));

    require(_to != address(0), \"Zero address cannot receive funds\");
    // From ERC777: The token contract MUST call the tokensToSend hook before updating the state.
    sendHook(_operator, _from, _to, _amount, _data, _operatorData);

    _tokenState.balances[_from] = _tokenState.balances[_from].sub(_amount);
    _tokenState.balances[_to] = _tokenState.balances[_to].add(_amount);

    emit Sent(_operator, _from, _to, _amount, _data, _operatorData);
    emit Transfer(_from, _to, _amount);

    // From ERC777: The token contract MUST call the tokensReceived hook after updating the state.
    receiveHook(_operator, _from, _to, _amount, _data, _operatorData, _enforceERC777);
  }

  function receiveHook(address _operator, address _from, address _to, uint256 _amount, bytes memory _data,
      bytes memory _operatorData, bool _enforceERC777)
    public
  {
    address implementer = ERC1820_REGISTRY.getInterfaceImplementer(_to, TOKENS_RECIPIENT_INTERFACE_HASH);
    if (implementer != address(0))
      IERC777Recipient(implementer).tokensReceived(_operator, _from, _to, _amount, _data, _operatorData);
    else if (_enforceERC777)
      require(!isContract(_to), \"Contract must be registered with ERC1820 as implementing ERC777TokensRecipient\");
  }

  function sendHook(address _operator, address _from, address _to, uint256 _amount, bytes memory _data,
      bytes memory _operatorData)
    public
  {
    address implementer = ERC1820_REGISTRY.getInterfaceImplementer(_from, TOKENS_SENDER_INTERFACE_HASH);
    if (implementer != address(0))
      IERC777Sender(implementer).tokensToSend(_operator, _from, _to, _amount, _data, _operatorData);
  }

  function isContract(address _account)
    private
    view
    returns (bool isContract_)
  {
    uint256 size;

    assembly {
      size := extcodesize(_account)
    }

    isContract_ = size != 0;
  }
}"},"Owned.sol":{"content":"pragma solidity 0.5.12;

contract Owned {

  address public owner = msg.sender;

  event LogOwnershipTransferred(address indexed owner, address indexed newOwner);

  modifier onlyOwner {
    require(msg.sender == owner, \"Sender must be owner\");
    _;
  }

  function setOwner(address _owner)
    external
    onlyOwner
  {
    require(_owner != address(0), \"Owner cannot be zero address\");
    emit LogOwnershipTransferred(owner, _owner);
    owner = _owner;
  }
}"},"PDelegate.sol":{"content":"pragma solidity 0.5.12;

contract PDelegate {

  /**
  * @dev Performs a delegatecall and returns whatever the delegatecall returned (entire context execution will return!)
  * @param _dst Destination address to perform the delegatecall
  * @param _calldata Calldata for the delegatecall
  */
  function delegatedFwd(address _dst, bytes memory _calldata)
    internal
  {
    assert(isContract(_dst));

    assembly {
      let result := delegatecall(sub(gas, 10000), _dst, add(_calldata, 0x20), mload(_calldata), 0, 0)
      let size := returndatasize
      let ptr := mload(0x40)
      returndatacopy(ptr, 0, size)

      // revert instead of invalid() bc if the underlying call failed with invalid() it already wasted gas.
      // if the call returned error data, forward it
      switch result case 0 { revert(ptr, size) }
      default { return(ptr, size) }
    }
  }

  // ONLY_IF_ASSERTS_ON:
  function isContract(address _target)
    view
    internal
    returns (bool result_)
  {
    uint256 size;

    assembly {
      size := extcodesize(_target)
    }

    result_ = (size != 0);
  }
  // :ONLY_IF_ASSERTS_ON
}"},"SafeMath.sol":{"content":"// Source: https://github.com/OpenZeppelin/openzeppelin-contracts/blob/master/contracts/math/SafeMath.sol

pragma solidity ^0.5.0;

/**
 * @dev Wrappers over Solidity\u0027s arithmetic operations with added overflow
 * checks.
 *
 * Arithmetic operations in Solidity wrap on overflow. This can easily result
 * in bugs, because programmers usually assume that an overflow raises an
 * error, which is the standard behavior in high level programming languages.
 * `SafeMath` restores this intuition by reverting the transaction when an
 * operation overflows.
 *
 * Using this library instead of the unchecked operations eliminates an entire
 * class of bugs, so it\u0027s recommended to use it always.
 */
library SafeMath {
    /**
     * @dev Returns the addition of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity\u0027s `+` operator.
     *
     * Requirements:
     * - Addition cannot overflow.
     */
    function add(uint256 a, uint256 b) internal pure returns (uint256) {
        uint256 c = a + b;
        require(c \u003e= a, \"SafeMath: addition overflow\");

        return c;
    }

    /**
     * @dev Returns the subtraction of two unsigned integers, reverting on
     * overflow (when the result is negative).
     *
     * Counterpart to Solidity\u0027s `-` operator.
     *
     * Requirements:
     * - Subtraction cannot overflow.
     */
    function sub(uint256 a, uint256 b) internal pure returns (uint256) {
        return sub(a, b, \"SafeMath: subtraction overflow\");
    }

    /**
     * @dev Returns the subtraction of two unsigned integers, reverting with custom message on
     * overflow (when the result is negative).
     *
     * Counterpart to Solidity\u0027s `-` operator.
     *
     * Requirements:
     * - Subtraction cannot overflow.
     *
     * _Available since v2.4.0._
     */
    function sub(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b \u003c= a, errorMessage);
        uint256 c = a - b;

        return c;
    }

    /**
     * @dev Returns the multiplication of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity\u0027s `*` operator.
     *
     * Requirements:
     * - Multiplication cannot overflow.
     */
    function mul(uint256 a, uint256 b) internal pure returns (uint256) {
        // Gas optimization: this is cheaper than requiring \u0027a\u0027 not being zero, but the
        // benefit is lost if \u0027b\u0027 is also tested.
        // See: https://github.com/OpenZeppelin/openzeppelin-contracts/pull/522
        if (a == 0) {
            return 0;
        }

        uint256 c = a * b;
        require(c / a == b, \"SafeMath: multiplication overflow\");

        return c;
    }

    /**
     * @dev Returns the integer division of two unsigned integers. Reverts on
     * division by zero. The result is rounded towards zero.
     *
     * Counterpart to Solidity\u0027s `/` operator. Note: this function uses a
     * `revert` opcode (which leaves remaining gas untouched) while Solidity
     * uses an invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     * - The divisor cannot be zero.
     */
    function div(uint256 a, uint256 b) internal pure returns (uint256) {
        return div(a, b, \"SafeMath: division by zero\");
    }

    /**
     * @dev Returns the integer division of two unsigned integers. Reverts with custom message on
     * division by zero. The result is rounded towards zero.
     *
     * Counterpart to Solidity\u0027s `/` operator. Note: this function uses a
     * `revert` opcode (which leaves remaining gas untouched) while Solidity
     * uses an invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     * - The divisor cannot be zero.
     *
     * _Available since v2.4.0._
     */
    function div(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        // Solidity only automatically asserts when dividing by 0
        require(b \u003e 0, errorMessage);
        uint256 c = a / b;
        // assert(a == b * c + a % b); // There is no case in which this doesn\u0027t hold

        return c;
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
     * Reverts when dividing by zero.
     *
     * Counterpart to Solidity\u0027s `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     * - The divisor cannot be zero.
     */
    function mod(uint256 a, uint256 b) internal pure returns (uint256) {
        return mod(a, b, \"SafeMath: modulo by zero\");
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
     * Reverts with custom message when dividing by zero.
     *
     * Counterpart to Solidity\u0027s `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     * - The divisor cannot be zero.
     *
     * _Available since v2.4.0._
     */
    function mod(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b != 0, errorMessage);
        return a % b;
    }
}"},"VOWTokenPublic.sol":{"content":"pragma solidity 0.5.12;

import \"./BasicToken.sol\";

contract VOWTokenPublic is BasicToken {
  mapping(address =\u003e bool) public vscContracts;

  event LogVSCContractRegistered(address indexed vscContract);
  event LogVSCContractDeregistered(address indexed vscContract);

  constructor(uint256 _initialSupply)
    BasicToken(\"Vow\", \"Vow\", _initialSupply)
    public {}

  function registerVSCContract(address _vscContract)
    external
    onlyOwner
  {
    require(!vscContracts[_vscContract], \"VSC contract already registered\");
    vscContracts[_vscContract] = true;

    emit LogVSCContractRegistered(_vscContract);
  }

  function deregisterVSCContract(address _vscContract)
    external
    onlyOwner
  {
    require(vscContracts[_vscContract], \"VSC contract is not registered\");
    vscContracts[_vscContract] = false;

    emit LogVSCContractDeregistered(_vscContract);
  }

  function isRegisteredVSCContract(address _vscContract)
    public
    view
    returns (bool isRegistered_)
  {
    isRegistered_ = vscContracts[_vscContract];
  }
}
