pragma solidity ^0.4.19;
interface tokenRecipient { function receiveApproval(address _from, uint256 _value, address _token, bytes _extraData) public; }
contract TokenERC20 {
\tstring public name;
\tstring public symbol;
\tuint8 public decimals = 18;
\tuint256 public totalSupply;

\t// ��mapping����ÿ����ַ��Ӧ�����
\tmapping (address =\u003e uint256) public balanceOf;
\t// �洢���˺ŵĿ���
\tmapping (address =\u003e mapping (address =\u003e uint256)) public allowance;
\t// �¼�������֪ͨ�ͻ��˽��׷���
\tevent Transfer(address indexed from, address indexed to, uint256 value);
\t// �¼�������֪ͨ�ͻ��˴��ұ�����
\tevent Burn(address indexed from, uint256 value);
\t
\t/*
\t*��ʼ������
\t*/
\tfunction TokenERC20(uint256 initialSupply, string tokenName, string tokenSymbol) public {
\t\ttotalSupply = initialSupply * 10 ** uint256(decimals);  // ��Ӧ�ķݶ�ݶ����С�Ĵ��ҵ�λ�йأ��ݶ� = ��
\t\tbalanceOf[msg.sender] = totalSupply;                // ������ӵ�����еĴ���
\t\tname = tokenName;                                   // ��������
\t\tsymbol = tokenSymbol;                               // ���ҷ���
}

\t//���ҽ���ת�Ƶ��ڲ�ʵ��
\tfunction _transfer(address _from, address _to, uint _value) internal {
\t\t// ȷ��Ŀ���ַ��Ϊ0x0����Ϊ0x0��ַ��������
\t\trequire(_to != 0x0);
\t\t// ��鷢�������
\t\trequire(balanceOf[_from] \u003e= _value);
\t\t// ȷ��ת��Ϊ������
\t\trequire(balanceOf[_to] + _value \u003e balanceOf[_to]);
\t\t
\t\t// ����������齻�ף�
\t\tuint previousBalances = balanceOf[_from] + balanceOf[_to];
\t\t// Subtract from the sender
\t\tbalanceOf[_from] -= _value;
\t\t
\t\t// Add the same to the recipient
\t\tbalanceOf[_to] += _value;
\t\tTransfer(_from, _to, _value);
\t\t
\t\t// ��assert���������߼���
\t\tassert(balanceOf[_from] + balanceOf[_to] == previousBalances);
}

\t/*****
\t**���ҽ���ת��
\t**���Լ������������ߣ��˺ŷ���`_value`�����ҵ� `_to`�˺�
\t**@param _to �����ߵ�ַ
\t**@param _value ת������
\t**/
\tfunction transfer(address _to, uint256 _value) public {
\t\t_transfer(msg.sender, _to, _value);
\t }
\t 
\t /*****
\t**�˺�֮����ҽ���ת��
\t**@param _from �����ߵ�ַ
\t**@param _to �����ߵ�ַ
\t**@param _value ת������
\t**/
\tfunction transferFrom(address _from, address _to, uint256 _value) public returns (bool success) {
\t\trequire(_value \u003c= allowance[_from][msg.sender]);     // Check allowance
\t\tallowance[_from][msg.sender] -= _value;
\t\t_transfer(_from, _to, _value);
\t\treturn true;
\t}
\t /*****
\t**����ĳ����ַ����Լ�����Դ������������廨�ѵĴ�����
\t**���������`_spender` ���Ѳ����� `_value` ������
\t**@param _spender The address authorized to spend
\t**@param _value the max amount they can spend
\t**/
\tfunction approve(address _spender, uint256 _value) public
\t\treturns (bool success) {
\t\tallowance[msg.sender][_spender] = _value;
\t\treturn true;
\t}
\t/*****
\t**��������һ����ַ����Լ�����ң����������ߣ����������໨�ѵĴ�����
\t**@param _spender ����Ȩ�ĵ�ַ����Լ��
\t**@param _value ���ɻ��Ѵ�����
\t**@param _extraData ���͸���Լ�ĸ�������
\t**/
\tfunction approveAndCall(address _spender, uint256 _value, bytes _extraData)
\t public
\t returns (bool success) {
\t tokenRecipient spender = tokenRecipient(_spender);
\t if (approve(_spender, _value)) {
\t\t// ֪ͨ��Լ
\t\tspender.receiveApproval(msg.sender, _value, this, _extraData);
\t\treturn true;
\t\t}
\t }
\t///�����ң����������ߣ��˻���ָ��������
\tfunction burn(uint256 _value) public returns (bool success) {
\t\trequire(balanceOf[msg.sender] \u003e= _value);   // Check if the sender has enough
\t\tbalanceOf[msg.sender] -= _value;            // Subtract from the sender
\t\ttotalSupply -= _value;                      // Updates totalSupply
\t\tBurn(msg.sender, _value);
\t\treturn true;
\t}
\t/*****
\t**�����û��˻���ָ��������
\t**Remove `_value` tokens from the system irreversibly on behalf of `_from
\t**@param _from the address of the sender
\t**@param _value the amount of money to burn
\t**/
\tfunction burnFrom(address _from, uint256 _value) public returns (bool success) {
\t\trequire(balanceOf[_from] \u003e= _value);                // Check if the targeted balance is enough
\t\trequire(_value \u003c= allowance[_from][msg.sender]);    // Check allowance
\t\tbalanceOf[_from] -= _value;                         // Subtract from the targeted balance
\t\tallowance[_from][msg.sender] -= _value;             // Subtract from the sender\u0027s allowance
\t\ttotalSupply -= _value;                              // Update totalSupply
\t\tBurn(_from, _value);
\t\treturn true;
\t}
}






pragma solidity ^0.4.19;
 
contract Token {
    /// token������Ĭ�ϻ�Ϊpublic��������һ��getter�����ӿڣ�����ΪtotalSupply().
    uint256 public totalSupply;
 
    /// ��ȡ�˻�_ownerӵ��token������
    function balanceOf(address _owner) constant returns (uint256 balance);
 
    //����Ϣ�������˻�����_to�˻�ת����Ϊ_value��token
    function transfer(address _to, uint256 _value) returns (bool success);
 
    //���˻�_from�����˻�_toת����Ϊ_value��token����approve�������ʹ��
    function transferFrom(address _from, address _to, uint256 _value) returns  (bool success);
 
    //��Ϣ�����˻������˻�_spender�ܴӷ����˻���ת������Ϊ_value��token
    function approve(address _spender, uint256 _value) returns (bool success);
 
    //��ȡ�˻�_spender���Դ��˻�_owner��ת��token������
    function allowance(address _owner, address _spender) constant returns  (uint256 remaining);
 
    //����ת��ʱ����Ҫ�������¼� 
    event Transfer(address indexed _from, address indexed _to, uint256 _value);
 
    //������approve(address _spender, uint256 _value)�ɹ�ִ��ʱ���봥�����¼�
    event Approval(address indexed _owner, address indexed _spender, uint256 _value);
}


"},"TokenERC20.sol":{"content":"pragma solidity ^0.4.19;
interface tokenRecipient { function receiveApproval(address _from, uint256 _value, address _token, bytes _extraData) public; }
contract TokenERC20 {
\tstring public name;
\tstring public symbol;
\tuint8 public decimals = 18;
\tuint256 public totalSupply;

\t// ��mapping����ÿ����ַ��Ӧ�����
\tmapping (address =\u003e uint256) public balanceOf;
\t// �洢���˺ŵĿ���
\tmapping (address =\u003e mapping (address =\u003e uint256)) public allowance;
\t// �¼�������֪ͨ�ͻ��˽��׷���
\tevent Transfer(address indexed from, address indexed to, uint256 value);
\t// �¼�������֪ͨ�ͻ��˴��ұ�����
\tevent Burn(address indexed from, uint256 value);
\t
\t/*
\t*��ʼ������
\t*/
\tfunction TokenERC20(uint256 initialSupply, string tokenName, string tokenSymbol) public {
\t\ttotalSupply = initialSupply * 10 ** uint256(decimals);  // ��Ӧ�ķݶ�ݶ����С�Ĵ��ҵ�λ�йأ��ݶ� = ��
\t\tbalanceOf[msg.sender] = totalSupply;                // ������ӵ�����еĴ���
\t\tname = tokenName;                                   // ��������
\t\tsymbol = tokenSymbol;                               // ���ҷ���
}
}
