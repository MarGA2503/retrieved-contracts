



// SPDX-License-Identifier: MIT

// solhint-disable-next-line compiler-version
pragma solidity >=0.4.24 <0.8.0;

import \"../utils/AddressUpgradeable.sol\";

/**
 * @dev This is a base contract to aid in writing upgradeable contracts, or any kind of contract that will be deployed
 * behind a proxy. Since a proxied contract can't have a constructor, it's common to move constructor logic to an
 * external initializer function, usually called `initialize`. It then becomes necessary to protect this initializer
 * function so it can only be called once. The {initializer} modifier provided by this contract will have this effect.
 *
 * TIP: To avoid leaving the proxy in an uninitialized state, the initializer function should be called as early as
 * possible by providing the encoded function call as the `_data` argument to {UpgradeableProxy-constructor}.
 *
 * CAUTION: When used with inheritance, manual care must be taken to not invoke a parent initializer twice, or to ensure
 * that all initializers are idempotent. This is not verified automatically as constructors are by Solidity.
 */
abstract contract Initializable {

    /**
     * @dev Indicates that the contract has been initialized.
     */
    bool private _initialized;

    /**
     * @dev Indicates that the contract is in the process of being initialized.
     */
    bool private _initializing;

    /**
     * @dev Modifier to protect an initializer function from being invoked twice.
     */
    modifier initializer() {
        require(_initializing || _isConstructor() || !_initialized, \"Initializable: contract is already initialized\");

        bool isTopLevelCall = !_initializing;
        if (isTopLevelCall) {
            _initializing = true;
            _initialized = true;
        }

        _;

        if (isTopLevelCall) {
            _initializing = false;
        }
    }

    /// @dev Returns true if and only if the function is running in the constructor
    function _isConstructor() private view returns (bool) {
        return !AddressUpgradeable.isContract(address(this));
    }
}
"
    
// SPDX-License-Identifier: MIT

pragma solidity ^0.7.0;

/**
 * @dev Collection of functions related to the address type
 */
library AddressUpgradeable {
    /**
     * @dev Returns true if `account` is a contract.
     *
     * [IMPORTANT]
     * ====
     * It is unsafe to assume that an address for which this function returns
     * false is an externally-owned account (EOA) and not a contract.
     *
     * Among others, `isContract` will return false for the following
     * types of addresses:
     *
     *  - an externally-owned account
     *  - a contract in construction
     *  - an address where a contract will be created
     *  - an address where a contract lived, but was destroyed
     * ====
     */
    function isContract(address account) internal view returns (bool) {
        // This method relies on extcodesize, which returns 0 for contracts in
        // construction, since the code is only stored at the end of the
        // constructor execution.

        uint256 size;
        // solhint-disable-next-line no-inline-assembly
        assembly { size := extcodesize(account) }
        return size > 0;
    }

    /**
     * @dev Replacement for Solidity's `transfer`: sends `amount` wei to
     * `recipient`, forwarding all available gas and reverting on errors.
     *
     * https://eips.ethereum.org/EIPS/eip-1884[EIP1884] increases the gas cost
     * of certain opcodes, possibly making contracts go over the 2300 gas limit
     * imposed by `transfer`, making them unable to receive funds via
     * `transfer`. {sendValue} removes this limitation.
     *
     * https://diligence.consensys.net/posts/2019/09/stop-using-soliditys-transfer-now/[Learn more].
     *
     * IMPORTANT: because control is transferred to `recipient`, care must be
     * taken to not create reentrancy vulnerabilities. Consider using
     * {ReentrancyGuard} or the
     * https://solidity.readthedocs.io/en/v0.5.11/security-considerations.html#use-the-checks-effects-interactions-pattern[checks-effects-interactions pattern].
     */
    function sendValue(address payable recipient, uint256 amount) internal {
        require(address(this).balance >= amount, \"Address: insufficient balance\");

        // solhint-disable-next-line avoid-low-level-calls, avoid-call-value
        (bool success, ) = recipient.call{ value: amount }(\"\");
        require(success, \"Address: unable to send value, recipient may have reverted\");
    }

    /**
     * @dev Performs a Solidity function call using a low level `call`. A
     * plain`call` is an unsafe replacement for a function call: use this
     * function instead.
     *
     * If `target` reverts with a revert reason, it is bubbled up by this
     * function (like regular Solidity function calls).
     *
     * Returns the raw returned data. To convert to the expected return value,
     * use https://solidity.readthedocs.io/en/latest/units-and-global-variables.html?highlight=abi.decode#abi-encoding-and-decoding-functions[`abi.decode`].
     *
     * Requirements:
     *
     * - `target` must be a contract.
     * - calling `target` with `data` must not revert.
     *
     * _Available since v3.1._
     */
    function functionCall(address target, bytes memory data) internal returns (bytes memory) {
      return functionCall(target, data, \"Address: low-level call failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`], but with
     * `errorMessage` as a fallback revert reason when `target` reverts.
     *
     * _Available since v3.1._
     */
    function functionCall(address target, bytes memory data, string memory errorMessage) internal returns (bytes memory) {
        return functionCallWithValue(target, data, 0, errorMessage);
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`],
     * but also transferring `value` wei to `target`.
     *
     * Requirements:
     *
     * - the calling contract must have an ETH balance of at least `value`.
     * - the called Solidity function must be `payable`.
     *
     * _Available since v3.1._
     */
    function functionCallWithValue(address target, bytes memory data, uint256 value) internal returns (bytes memory) {
        return functionCallWithValue(target, data, value, \"Address: low-level call with value failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCallWithValue-address-bytes-uint256-}[`functionCallWithValue`], but
     * with `errorMessage` as a fallback revert reason when `target` reverts.
     *
     * _Available since v3.1._
     */
    function functionCallWithValue(address target, bytes memory data, uint256 value, string memory errorMessage) internal returns (bytes memory) {
        require(address(this).balance >= value, \"Address: insufficient balance for call\");
        require(isContract(target), \"Address: call to non-contract\");

        // solhint-disable-next-line avoid-low-level-calls
        (bool success, bytes memory returndata) = target.call{ value: value }(data);
        return _verifyCallResult(success, returndata, errorMessage);
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`],
     * but performing a static call.
     *
     * _Available since v3.3._
     */
    function functionStaticCall(address target, bytes memory data) internal view returns (bytes memory) {
        return functionStaticCall(target, data, \"Address: low-level static call failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-string-}[`functionCall`],
     * but performing a static call.
     *
     * _Available since v3.3._
     */
    function functionStaticCall(address target, bytes memory data, string memory errorMessage) internal view returns (bytes memory) {
        require(isContract(target), \"Address: static call to non-contract\");

        // solhint-disable-next-line avoid-low-level-calls
        (bool success, bytes memory returndata) = target.staticcall(data);
        return _verifyCallResult(success, returndata, errorMessage);
    }

    function _verifyCallResult(bool success, bytes memory returndata, string memory errorMessage) private pure returns(bytes memory) {
        if (success) {
            return returndata;
        } else {
            // Look for revert reason and bubble it up if present
            if (returndata.length > 0) {
                // The easiest way to bubble the revert reason is using memory via assembly

                // solhint-disable-next-line no-inline-assembly
                assembly {
                    let returndata_size := mload(returndata)
                    revert(add(32, returndata), returndata_size)
                }
            } else {
                revert(errorMessage);
            }
        }
    }
}
"
    
// SPDX-License-Identifier: MIT

pragma solidity >=0.6.0 <0.8.0;
import \"../proxy/Initializable.sol\";

/*
 * @dev Provides information about the current execution context, including the
 * sender of the transaction and its data. While these are generally available
 * via msg.sender and msg.data, they should not be accessed in such a direct
 * manner, since when dealing with GSN meta-transactions the account sending and
 * paying for execution may not be the actual sender (as far as an application
 * is concerned).
 *
 * This contract is only required for intermediate, library-like contracts.
 */
abstract contract ContextUpgradeable is Initializable {
    function __Context_init() internal initializer {
        __Context_init_unchained();
    }

    function __Context_init_unchained() internal initializer {
    }
    function _msgSender() internal view virtual returns (address payable) {
        return msg.sender;
    }

    function _msgData() internal view virtual returns (bytes memory) {
        this; // silence state mutability warning without generating bytecode - see https://github.com/ethereum/solidity/issues/2691
        return msg.data;
    }
    uint256[50] private __gap;
}
"
    
// SPDX-License-Identifier: MIT

pragma solidity ^0.7.0;

import \"./ContextUpgradeable.sol\";
import \"../proxy/Initializable.sol\";

/**
 * @dev Contract module which allows children to implement an emergency stop
 * mechanism that can be triggered by an authorized account.
 *
 * This module is used through inheritance. It will make available the
 * modifiers `whenNotPaused` and `whenPaused`, which can be applied to
 * the functions of your contract. Note that they will not be pausable by
 * simply including this module, only once the modifiers are put in place.
 */
abstract contract PausableUpgradeable is Initializable, ContextUpgradeable {
    /**
     * @dev Emitted when the pause is triggered by `account`.
     */
    event Paused(address account);

    /**
     * @dev Emitted when the pause is lifted by `account`.
     */
    event Unpaused(address account);

    bool private _paused;

    /**
     * @dev Initializes the contract in unpaused state.
     */
    function __Pausable_init() internal initializer {
        __Context_init_unchained();
        __Pausable_init_unchained();
    }

    function __Pausable_init_unchained() internal initializer {
        _paused = false;
    }

    /**
     * @dev Returns true if the contract is paused, and false otherwise.
     */
    function paused() public view virtual returns (bool) {
        return _paused;
    }

    /**
     * @dev Modifier to make a function callable only when the contract is not paused.
     *
     * Requirements:
     *
     * - The contract must not be paused.
     */
    modifier whenNotPaused() {
        require(!paused(), \"Pausable: paused\");
        _;
    }

    /**
     * @dev Modifier to make a function callable only when the contract is paused.
     *
     * Requirements:
     *
     * - The contract must be paused.
     */
    modifier whenPaused() {
        require(paused(), \"Pausable: not paused\");
        _;
    }

    /**
     * @dev Triggers stopped state.
     *
     * Requirements:
     *
     * - The contract must not be paused.
     */
    function _pause() internal virtual whenNotPaused {
        _paused = true;
        emit Paused(_msgSender());
    }

    /**
     * @dev Returns to normal state.
     *
     * Requirements:
     *
     * - The contract must be paused.
     */
    function _unpause() internal virtual whenPaused {
        _paused = false;
        emit Unpaused(_msgSender());
    }
    uint256[49] private __gap;
}
"
    
// SPDX-License-Identifier: MIT

pragma solidity ^0.7.0;

/**
 * @dev Wrappers over Solidity's arithmetic operations with added overflow
 * checks.
 *
 * Arithmetic operations in Solidity wrap on overflow. This can easily result
 * in bugs, because programmers usually assume that an overflow raises an
 * error, which is the standard behavior in high level programming languages.
 * `SafeMath` restores this intuition by reverting the transaction when an
 * operation overflows.
 *
 * Using this library instead of the unchecked operations eliminates an entire
 * class of bugs, so it's recommended to use it always.
 */
library SafeMath {
    /**
     * @dev Returns the addition of two unsigned integers, with an overflow flag.
     *
     * _Available since v3.4._
     */
    function tryAdd(uint256 a, uint256 b) internal pure returns (bool, uint256) {
        uint256 c = a + b;
        if (c < a) return (false, 0);
        return (true, c);
    }

    /**
     * @dev Returns the substraction of two unsigned integers, with an overflow flag.
     *
     * _Available since v3.4._
     */
    function trySub(uint256 a, uint256 b) internal pure returns (bool, uint256) {
        if (b > a) return (false, 0);
        return (true, a - b);
    }

    /**
     * @dev Returns the multiplication of two unsigned integers, with an overflow flag.
     *
     * _Available since v3.4._
     */
    function tryMul(uint256 a, uint256 b) internal pure returns (bool, uint256) {
        // Gas optimization: this is cheaper than requiring 'a' not being zero, but the
        // benefit is lost if 'b' is also tested.
        // See: https://github.com/OpenZeppelin/openzeppelin-contracts/pull/522
        if (a == 0) return (true, 0);
        uint256 c = a * b;
        if (c / a != b) return (false, 0);
        return (true, c);
    }

    /**
     * @dev Returns the division of two unsigned integers, with a division by zero flag.
     *
     * _Available since v3.4._
     */
    function tryDiv(uint256 a, uint256 b) internal pure returns (bool, uint256) {
        if (b == 0) return (false, 0);
        return (true, a / b);
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers, with a division by zero flag.
     *
     * _Available since v3.4._
     */
    function tryMod(uint256 a, uint256 b) internal pure returns (bool, uint256) {
        if (b == 0) return (false, 0);
        return (true, a % b);
    }

    /**
     * @dev Returns the addition of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity's `+` operator.
     *
     * Requirements:
     *
     * - Addition cannot overflow.
     */
    function add(uint256 a, uint256 b) internal pure returns (uint256) {
        uint256 c = a + b;
        require(c >= a, \"SafeMath: addition overflow\");
        return c;
    }

    /**
     * @dev Returns the subtraction of two unsigned integers, reverting on
     * overflow (when the result is negative).
     *
     * Counterpart to Solidity's `-` operator.
     *
     * Requirements:
     *
     * - Subtraction cannot overflow.
     */
    function sub(uint256 a, uint256 b) internal pure returns (uint256) {
        require(b <= a, \"SafeMath: subtraction overflow\");
        return a - b;
    }

    /**
     * @dev Returns the multiplication of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity's `*` operator.
     *
     * Requirements:
     *
     * - Multiplication cannot overflow.
     */
    function mul(uint256 a, uint256 b) internal pure returns (uint256) {
        if (a == 0) return 0;
        uint256 c = a * b;
        require(c / a == b, \"SafeMath: multiplication overflow\");
        return c;
    }

    /**
     * @dev Returns the integer division of two unsigned integers, reverting on
     * division by zero. The result is rounded towards zero.
     *
     * Counterpart to Solidity's `/` operator. Note: this function uses a
     * `revert` opcode (which leaves remaining gas untouched) while Solidity
     * uses an invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function div(uint256 a, uint256 b) internal pure returns (uint256) {
        require(b > 0, \"SafeMath: division by zero\");
        return a / b;
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
     * reverting when dividing by zero.
     *
     * Counterpart to Solidity's `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function mod(uint256 a, uint256 b) internal pure returns (uint256) {
        require(b > 0, \"SafeMath: modulo by zero\");
        return a % b;
    }

    /**
     * @dev Returns the subtraction of two unsigned integers, reverting with custom message on
     * overflow (when the result is negative).
     *
     * CAUTION: This function is deprecated because it requires allocating memory for the error
     * message unnecessarily. For custom revert reasons use {trySub}.
     *
     * Counterpart to Solidity's `-` operator.
     *
     * Requirements:
     *
     * - Subtraction cannot overflow.
     */
    function sub(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b <= a, errorMessage);
        return a - b;
    }

    /**
     * @dev Returns the integer division of two unsigned integers, reverting with custom message on
     * division by zero. The result is rounded towards zero.
     *
     * CAUTION: This function is deprecated because it requires allocating memory for the error
     * message unnecessarily. For custom revert reasons use {tryDiv}.
     *
     * Counterpart to Solidity's `/` operator. Note: this function uses a
     * `revert` opcode (which leaves remaining gas untouched) while Solidity
     * uses an invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function div(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b > 0, errorMessage);
        return a / b;
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
     * reverting with custom message when dividing by zero.
     *
     * CAUTION: This function is deprecated because it requires allocating memory for the error
     * message unnecessarily. For custom revert reasons use {tryMod}.
     *
     * Counterpart to Solidity's `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function mod(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b > 0, errorMessage);
        return a % b;
    }
}
"
    
// SPDX-License-Identifier: MIT

pragma solidity ^0.7.0;

/**
 * @dev Interface of the ERC20 standard as defined in the EIP.
 */
interface IERC20 {
    /**
     * @dev Returns the amount of tokens in existence.
     */
    function totalSupply() external view returns (uint256);

    /**
     * @dev Returns the amount of tokens owned by `account`.
     */
    function balanceOf(address account) external view returns (uint256);

    /**
     * @dev Moves `amount` tokens from the caller's account to `recipient`.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transfer(address recipient, uint256 amount) external returns (bool);

    /**
     * @dev Returns the remaining number of tokens that `spender` will be
     * allowed to spend on behalf of `owner` through {transferFrom}. This is
     * zero by default.
     *
     * This value changes when {approve} or {transferFrom} are called.
     */
    function allowance(address owner, address spender) external view returns (uint256);

    /**
     * @dev Sets `amount` as the allowance of `spender` over the caller's tokens.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * IMPORTANT: Beware that changing an allowance with this method brings the risk
     * that someone may use both the old and the new allowance by unfortunate
     * transaction ordering. One possible solution to mitigate this race
     * condition is to first reduce the spender's allowance to 0 and set the
     * desired value afterwards:
     * https://github.com/ethereum/EIPs/issues/20#issuecomment-263524729
     *
     * Emits an {Approval} event.
     */
    function approve(address spender, uint256 amount) external returns (bool);

    /**
     * @dev Moves `amount` tokens from `sender` to `recipient` using the
     * allowance mechanism. `amount` is then deducted from the caller's
     * allowance.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transferFrom(address sender, address recipient, uint256 amount) external returns (bool);

    /**
     * @dev Emitted when `value` tokens are moved from one account (`from`) to
     * another (`to`).
     *
     * Note that `value` may be zero.
     */
    event Transfer(address indexed from, address indexed to, uint256 value);

    /**
     * @dev Emitted when the allowance of a `spender` for an `owner` is set by
     * a call to {approve}. `value` is the new allowance.
     */
    event Approval(address indexed owner, address indexed spender, uint256 value);
}
"
    
// SPDX-License-Identifier: MIT

pragma solidity ^0.7.0;

import \"./IERC20.sol\";
import \"../../math/SafeMath.sol\";
import \"../../utils/Address.sol\";

/**
 * @title SafeERC20
 * @dev Wrappers around ERC20 operations that throw on failure (when the token
 * contract returns false). Tokens that return no value (and instead revert or
 * throw on failure) are also supported, non-reverting calls are assumed to be
 * successful.
 * To use this library you can add a `using SafeERC20 for IERC20;` statement to your contract,
 * which allows you to call the safe operations as `token.safeTransfer(...)`, etc.
 */
library SafeERC20 {
    using SafeMath for uint256;
    using Address for address;

    function safeTransfer(IERC20 token, address to, uint256 value) internal {
        _callOptionalReturn(token, abi.encodeWithSelector(token.transfer.selector, to, value));
    }

    function safeTransferFrom(IERC20 token, address from, address to, uint256 value) internal {
        _callOptionalReturn(token, abi.encodeWithSelector(token.transferFrom.selector, from, to, value));
    }

    /**
     * @dev Deprecated. This function has issues similar to the ones found in
     * {IERC20-approve}, and its usage is discouraged.
     *
     * Whenever possible, use {safeIncreaseAllowance} and
     * {safeDecreaseAllowance} instead.
     */
    function safeApprove(IERC20 token, address spender, uint256 value) internal {
        // safeApprove should only be called when setting an initial allowance,
        // or when resetting it to zero. To increase and decrease it, use
        // 'safeIncreaseAllowance' and 'safeDecreaseAllowance'
        // solhint-disable-next-line max-line-length
        require((value == 0) || (token.allowance(address(this), spender) == 0),
            \"SafeERC20: approve from non-zero to non-zero allowance\"
        );
        _callOptionalReturn(token, abi.encodeWithSelector(token.approve.selector, spender, value));
    }

    function safeIncreaseAllowance(IERC20 token, address spender, uint256 value) internal {
        uint256 newAllowance = token.allowance(address(this), spender).add(value);
        _callOptionalReturn(token, abi.encodeWithSelector(token.approve.selector, spender, newAllowance));
    }

    function safeDecreaseAllowance(IERC20 token, address spender, uint256 value) internal {
        uint256 newAllowance = token.allowance(address(this), spender).sub(value, \"SafeERC20: decreased allowance below zero\");
        _callOptionalReturn(token, abi.encodeWithSelector(token.approve.selector, spender, newAllowance));
    }

    /**
     * @dev Imitates a Solidity high-level call (i.e. a regular function call to a contract), relaxing the requirement
     * on the return value: the return value is optional (but if data is returned, it must not be false).
     * @param token The token targeted by the call.
     * @param data The call data (encoded using abi.encode or one of its variants).
     */
    function _callOptionalReturn(IERC20 token, bytes memory data) private {
        // We need to perform a low level call here, to bypass Solidity's return data size checking mechanism, since
        // we're implementing it ourselves. We use {Address.functionCall} to perform this call, which verifies that
        // the target address contains contract code and also asserts for success in the low-level call.

        bytes memory returndata = address(token).functionCall(data, \"SafeERC20: low-level call failed\");
        if (returndata.length > 0) { // Return data is optional
            // solhint-disable-next-line max-line-length
            require(abi.decode(returndata, (bool)), \"SafeERC20: ERC20 operation did not succeed\");
        }
    }
}
"
    
// SPDX-License-Identifier: MIT

pragma solidity ^0.7.0;

/**
 * @dev Collection of functions related to the address type
 */
library Address {
    /**
     * @dev Returns true if `account` is a contract.
     *
     * [IMPORTANT]
     * ====
     * It is unsafe to assume that an address for which this function returns
     * false is an externally-owned account (EOA) and not a contract.
     *
     * Among others, `isContract` will return false for the following
     * types of addresses:
     *
     *  - an externally-owned account
     *  - a contract in construction
     *  - an address where a contract will be created
     *  - an address where a contract lived, but was destroyed
     * ====
     */
    function isContract(address account) internal view returns (bool) {
        // This method relies on extcodesize, which returns 0 for contracts in
        // construction, since the code is only stored at the end of the
        // constructor execution.

        uint256 size;
        // solhint-disable-next-line no-inline-assembly
        assembly { size := extcodesize(account) }
        return size > 0;
    }

    /**
     * @dev Replacement for Solidity's `transfer`: sends `amount` wei to
     * `recipient`, forwarding all available gas and reverting on errors.
     *
     * https://eips.ethereum.org/EIPS/eip-1884[EIP1884] increases the gas cost
     * of certain opcodes, possibly making contracts go over the 2300 gas limit
     * imposed by `transfer`, making them unable to receive funds via
     * `transfer`. {sendValue} removes this limitation.
     *
     * https://diligence.consensys.net/posts/2019/09/stop-using-soliditys-transfer-now/[Learn more].
     *
     * IMPORTANT: because control is transferred to `recipient`, care must be
     * taken to not create reentrancy vulnerabilities. Consider using
     * {ReentrancyGuard} or the
     * https://solidity.readthedocs.io/en/v0.5.11/security-considerations.html#use-the-checks-effects-interactions-pattern[checks-effects-interactions pattern].
     */
    function sendValue(address payable recipient, uint256 amount) internal {
        require(address(this).balance >= amount, \"Address: insufficient balance\");

        // solhint-disable-next-line avoid-low-level-calls, avoid-call-value
        (bool success, ) = recipient.call{ value: amount }(\"\");
        require(success, \"Address: unable to send value, recipient may have reverted\");
    }

    /**
     * @dev Performs a Solidity function call using a low level `call`. A
     * plain`call` is an unsafe replacement for a function call: use this
     * function instead.
     *
     * If `target` reverts with a revert reason, it is bubbled up by this
     * function (like regular Solidity function calls).
     *
     * Returns the raw returned data. To convert to the expected return value,
     * use https://solidity.readthedocs.io/en/latest/units-and-global-variables.html?highlight=abi.decode#abi-encoding-and-decoding-functions[`abi.decode`].
     *
     * Requirements:
     *
     * - `target` must be a contract.
     * - calling `target` with `data` must not revert.
     *
     * _Available since v3.1._
     */
    function functionCall(address target, bytes memory data) internal returns (bytes memory) {
      return functionCall(target, data, \"Address: low-level call failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`], but with
     * `errorMessage` as a fallback revert reason when `target` reverts.
     *
     * _Available since v3.1._
     */
    function functionCall(address target, bytes memory data, string memory errorMessage) internal returns (bytes memory) {
        return functionCallWithValue(target, data, 0, errorMessage);
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`],
     * but also transferring `value` wei to `target`.
     *
     * Requirements:
     *
     * - the calling contract must have an ETH balance of at least `value`.
     * - the called Solidity function must be `payable`.
     *
     * _Available since v3.1._
     */
    function functionCallWithValue(address target, bytes memory data, uint256 value) internal returns (bytes memory) {
        return functionCallWithValue(target, data, value, \"Address: low-level call with value failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCallWithValue-address-bytes-uint256-}[`functionCallWithValue`], but
     * with `errorMessage` as a fallback revert reason when `target` reverts.
     *
     * _Available since v3.1._
     */
    function functionCallWithValue(address target, bytes memory data, uint256 value, string memory errorMessage) internal returns (bytes memory) {
        require(address(this).balance >= value, \"Address: insufficient balance for call\");
        require(isContract(target), \"Address: call to non-contract\");

        // solhint-disable-next-line avoid-low-level-calls
        (bool success, bytes memory returndata) = target.call{ value: value }(data);
        return _verifyCallResult(success, returndata, errorMessage);
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`],
     * but performing a static call.
     *
     * _Available since v3.3._
     */
    function functionStaticCall(address target, bytes memory data) internal view returns (bytes memory) {
        return functionStaticCall(target, data, \"Address: low-level static call failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-string-}[`functionCall`],
     * but performing a static call.
     *
     * _Available since v3.3._
     */
    function functionStaticCall(address target, bytes memory data, string memory errorMessage) internal view returns (bytes memory) {
        require(isContract(target), \"Address: static call to non-contract\");

        // solhint-disable-next-line avoid-low-level-calls
        (bool success, bytes memory returndata) = target.staticcall(data);
        return _verifyCallResult(success, returndata, errorMessage);
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`],
     * but performing a delegate call.
     *
     * _Available since v3.4._
     */
    function functionDelegateCall(address target, bytes memory data) internal returns (bytes memory) {
        return functionDelegateCall(target, data, \"Address: low-level delegate call failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-string-}[`functionCall`],
     * but performing a delegate call.
     *
     * _Available since v3.4._
     */
    function functionDelegateCall(address target, bytes memory data, string memory errorMessage) internal returns (bytes memory) {
        require(isContract(target), \"Address: delegate call to non-contract\");

        // solhint-disable-next-line avoid-low-level-calls
        (bool success, bytes memory returndata) = target.delegatecall(data);
        return _verifyCallResult(success, returndata, errorMessage);
    }

    function _verifyCallResult(bool success, bytes memory returndata, string memory errorMessage) private pure returns(bytes memory) {
        if (success) {
            return returndata;
        } else {
            // Look for revert reason and bubble it up if present
            if (returndata.length > 0) {
                // The easiest way to bubble the revert reason is using memory via assembly

                // solhint-disable-next-line no-inline-assembly
                assembly {
                    let returndata_size := mload(returndata)
                    revert(add(32, returndata), returndata_size)
                }
            } else {
                revert(errorMessage);
            }
        }
    }
}
"
    
// SPDX-License-Identifier: MIT

pragma solidity ^0.7.0;

/**
 * @dev Library for managing
 * https://en.wikipedia.org/wiki/Set_(abstract_data_type)[sets] of primitive
 * types.
 *
 * Sets have the following properties:
 *
 * - Elements are added, removed, and checked for existence in constant time
 * (O(1)).
 * - Elements are enumerated in O(n). No guarantees are made on the ordering.
 *
 * ```
 * contract Example {
 *     // Add the library methods
 *     using EnumerableSet for EnumerableSet.AddressSet;
 *
 *     // Declare a set state variable
 *     EnumerableSet.AddressSet private mySet;
 * }
 * ```
 *
 * As of v3.3.0, sets of type `bytes32` (`Bytes32Set`), `address` (`AddressSet`)
 * and `uint256` (`UintSet`) are supported.
 */
library EnumerableSet {
    // To implement this library for multiple types with as little code
    // repetition as possible, we write it in terms of a generic Set type with
    // bytes32 values.
    // The Set implementation uses private functions, and user-facing
    // implementations (such as AddressSet) are just wrappers around the
    // underlying Set.
    // This means that we can only create new EnumerableSets for types that fit
    // in bytes32.

    struct Set {
        // Storage of set values
        bytes32[] _values;

        // Position of the value in the `values` array, plus 1 because index 0
        // means a value is not in the set.
        mapping (bytes32 => uint256) _indexes;
    }

    /**
     * @dev Add a value to a set. O(1).
     *
     * Returns true if the value was added to the set, that is if it was not
     * already present.
     */
    function _add(Set storage set, bytes32 value) private returns (bool) {
        if (!_contains(set, value)) {
            set._values.push(value);
            // The value is stored at length-1, but we add 1 to all indexes
            // and use 0 as a sentinel value
            set._indexes[value] = set._values.length;
            return true;
        } else {
            return false;
        }
    }

    /**
     * @dev Removes a value from a set. O(1).
     *
     * Returns true if the value was removed from the set, that is if it was
     * present.
     */
    function _remove(Set storage set, bytes32 value) private returns (bool) {
        // We read and store the value's index to prevent multiple reads from the same storage slot
        uint256 valueIndex = set._indexes[value];

        if (valueIndex != 0) { // Equivalent to contains(set, value)
            // To delete an element from the _values array in O(1), we swap the element to delete with the last one in
            // the array, and then remove the last element (sometimes called as 'swap and pop').
            // This modifies the order of the array, as noted in {at}.

            uint256 toDeleteIndex = valueIndex - 1;
            uint256 lastIndex = set._values.length - 1;

            // When the value to delete is the last one, the swap operation is unnecessary. However, since this occurs
            // so rarely, we still do the swap anyway to avoid the gas cost of adding an 'if' statement.

            bytes32 lastvalue = set._values[lastIndex];

            // Move the last value to the index where the value to delete is
            set._values[toDeleteIndex] = lastvalue;
            // Update the index for the moved value
            set._indexes[lastvalue] = toDeleteIndex + 1; // All indexes are 1-based

            // Delete the slot where the moved value was stored
            set._values.pop();

            // Delete the index for the deleted slot
            delete set._indexes[value];

            return true;
        } else {
            return false;
        }
    }

    /**
     * @dev Returns true if the value is in the set. O(1).
     */
    function _contains(Set storage set, bytes32 value) private view returns (bool) {
        return set._indexes[value] != 0;
    }

    /**
     * @dev Returns the number of values on the set. O(1).
     */
    function _length(Set storage set) private view returns (uint256) {
        return set._values.length;
    }

   /**
    * @dev Returns the value stored at position `index` in the set. O(1).
    *
    * Note that there are no guarantees on the ordering of values inside the
    * array, and it may change when more values are added or removed.
    *
    * Requirements:
    *
    * - `index` must be strictly less than {length}.
    */
    function _at(Set storage set, uint256 index) private view returns (bytes32) {
        require(set._values.length > index, \"EnumerableSet: index out of bounds\");
        return set._values[index];
    }

    // Bytes32Set

    struct Bytes32Set {
        Set _inner;
    }

    /**
     * @dev Add a value to a set. O(1).
     *
     * Returns true if the value was added to the set, that is if it was not
     * already present.
     */
    function add(Bytes32Set storage set, bytes32 value) internal returns (bool) {
        return _add(set._inner, value);
    }

    /**
     * @dev Removes a value from a set. O(1).
     *
     * Returns true if the value was removed from the set, that is if it was
     * present.
     */
    function remove(Bytes32Set storage set, bytes32 value) internal returns (bool) {
        return _remove(set._inner, value);
    }

    /**
     * @dev Returns true if the value is in the set. O(1).
     */
    function contains(Bytes32Set storage set, bytes32 value) internal view returns (bool) {
        return _contains(set._inner, value);
    }

    /**
     * @dev Returns the number of values in the set. O(1).
     */
    function length(Bytes32Set storage set) internal view returns (uint256) {
        return _length(set._inner);
    }

   /**
    * @dev Returns the value stored at position `index` in the set. O(1).
    *
    * Note that there are no guarantees on the ordering of values inside the
    * array, and it may change when more values are added or removed.
    *
    * Requirements:
    *
    * - `index` must be strictly less than {length}.
    */
    function at(Bytes32Set storage set, uint256 index) internal view returns (bytes32) {
        return _at(set._inner, index);
    }

    // AddressSet

    struct AddressSet {
        Set _inner;
    }

    /**
     * @dev Add a value to a set. O(1).
     *
     * Returns true if the value was added to the set, that is if it was not
     * already present.
     */
    function add(AddressSet storage set, address value) internal returns (bool) {
        return _add(set._inner, bytes32(uint256(uint160(value))));
    }

    /**
     * @dev Removes a value from a set. O(1).
     *
     * Returns true if the value was removed from the set, that is if it was
     * present.
     */
    function remove(AddressSet storage set, address value) internal returns (bool) {
        return _remove(set._inner, bytes32(uint256(uint160(value))));
    }

    /**
     * @dev Returns true if the value is in the set. O(1).
     */
    function contains(AddressSet storage set, address value) internal view returns (bool) {
        return _contains(set._inner, bytes32(uint256(uint160(value))));
    }

    /**
     * @dev Returns the number of values in the set. O(1).
     */
    function length(AddressSet storage set) internal view returns (uint256) {
        return _length(set._inner);
    }

   /**
    * @dev Returns the value stored at position `index` in the set. O(1).
    *
    * Note that there are no guarantees on the ordering of values inside the
    * array, and it may change when more values are added or removed.
    *
    * Requirements:
    *
    * - `index` must be strictly less than {length}.
    */
    function at(AddressSet storage set, uint256 index) internal view returns (address) {
        return address(uint160(uint256(_at(set._inner, index))));
    }


    // UintSet

    struct UintSet {
        Set _inner;
    }

    /**
     * @dev Add a value to a set. O(1).
     *
     * Returns true if the value was added to the set, that is if it was not
     * already present.
     */
    function add(UintSet storage set, uint256 value) internal returns (bool) {
        return _add(set._inner, bytes32(value));
    }

    /**
     * @dev Removes a value from a set. O(1).
     *
     * Returns true if the value was removed from the set, that is if it was
     * present.
     */
    function remove(UintSet storage set, uint256 value) internal returns (bool) {
        return _remove(set._inner, bytes32(value));
    }

    /**
     * @dev Returns true if the value is in the set. O(1).
     */
    function contains(UintSet storage set, uint256 value) internal view returns (bool) {
        return _contains(set._inner, bytes32(value));
    }

    /**
     * @dev Returns the number of values on the set. O(1).
     */
    function length(UintSet storage set) internal view returns (uint256) {
        return _length(set._inner);
    }

   /**
    * @dev Returns the value stored at position `index` in the set. O(1).
    *
    * Note that there are no guarantees on the ordering of values inside the
    * array, and it may change when more values are added or removed.
    *
    * Requirements:
    *
    * - `index` must be strictly less than {length}.
    */
    function at(UintSet storage set, uint256 index) internal view returns (uint256) {
        return uint256(_at(set._inner, index));
    }
}
"
    
// SPDX-License-Identifier: MIT

/**
 * Based on https://github.com/OpenZeppelin/openzeppelin-contracts-upgradeable/blob/v3.4.0-solc-0.7/contracts/access/OwnableUpgradeable.sol
 *
 * Changes:
 * - Added owner argument to initializer
 * - Reformatted styling in line with this repository.
 */

/*
The MIT License (MIT)

Copyright (c) 2016-2020 zOS Global Limited

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
\"Software\"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

/* solhint-disable func-name-mixedcase */

pragma solidity 0.7.6;

import \"@openzeppelin/contracts-upgradeable/utils/ContextUpgradeable.sol\";
import \"@openzeppelin/contracts-upgradeable/proxy/Initializable.sol\";

/**
 * @dev Contract module which provides a basic access control mechanism, where
 * there is an account (an owner) that can be granted exclusive access to
 * specific functions.
 *
 * By default, the owner account will be the one that deploys the contract. This
 * can later be changed with {transferOwnership}.
 *
 * This module is used through inheritance. It will make available the modifier
 * `onlyOwner`, which can be applied to your functions to restrict their use to
 * the owner.
 */
abstract contract OwnableUpgradeable is Initializable, ContextUpgradeable {
\taddress private _owner;

\tevent OwnershipTransferred(
\t\taddress indexed previousOwner,
\t\taddress indexed newOwner
\t);

\t/**
\t * @dev Initializes the contract setting the deployer as the initial owner.
\t */
\tfunction __Ownable_init(address owner_) internal initializer {
\t\t__Context_init_unchained();
\t\t__Ownable_init_unchained(owner_);
\t}

\tfunction __Ownable_init_unchained(address owner_) internal initializer {
\t\t_owner = owner_;
\t\temit OwnershipTransferred(address(0), owner_);
\t}

\t/**
\t * @dev Returns the address of the current owner.
\t */
\tfunction owner() public view virtual returns (address) {
\t\treturn _owner;
\t}

\t/**
\t * @dev Throws if called by any account other than the owner.
\t */
\tmodifier onlyOwner() {
\t\trequire(owner() == _msgSender(), \"Ownable: caller is not the owner\");
\t\t_;
\t}

\t/**
\t * @dev Leaves the contract without owner. It will not be possible to call
\t * `onlyOwner` functions anymore. Can only be called by the current owner.
\t *
\t * NOTE: Renouncing ownership will leave the contract without an owner,
\t * thereby removing any functionality that is only available to the owner.
\t */
\tfunction renounceOwnership() public virtual onlyOwner {
\t\temit OwnershipTransferred(_owner, address(0));
\t\t_owner = address(0);
\t}

\t/**
\t * @dev Transfers ownership of the contract to a new account (`newOwner`).
\t * Can only be called by the current owner.
\t */
\tfunction transferOwnership(address newOwner) public virtual onlyOwner {
\t\trequire(newOwner != address(0), \"Ownable: new owner is the zero address\");
\t\temit OwnershipTransferred(_owner, newOwner);
\t\t_owner = newOwner;
\t}

\tuint256[49] private __gap;
}
"
    
// SPDX-License-Identifier: MIT

/**
 * Based on https://github.com/OpenZeppelin/openzeppelin-contracts/blob/v3.3.0-solc-0.7/contracts/utils/EnumerableMap.sol
 *
 * Changes:
 * - Replaced UintToAddressMap with AddressToUintMap
 * - Reformatted styling in line with this repository.
 */

/*
The MIT License (MIT)

Copyright (c) 2016-2020 zOS Global Limited

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
\"Software\"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

pragma solidity 0.7.6;

/**
 * @dev Library for managing an enumerable variant of Solidity's
 * https://solidity.readthedocs.io/en/latest/types.html#mapping-types[`mapping`]
 * type.
 *
 * Maps have the following properties:
 *
 * - Entries are added, removed, and checked for existence in constant time
 * (O(1)).
 * - Entries are enumerated in O(n). No guarantees are made on the ordering.
 *
 * ```
 * contract Example {
 *     // Add the library methods
 *     using EnumerableMap for EnumerableMap.UintToAddressMap;
 *
 *     // Declare a set state variable
 *     EnumerableMap.UintToAddressMap private myMap;
 * }
 * ```
 *
 * As of v3.0.0, only maps of type `uint256 -> address` (`UintToAddressMap`) are
 * supported.
 */
library EnumerableMap {
\t// To implement this library for multiple types with as little code
\t// repetition as possible, we write it in terms of a generic Map type with
\t// bytes32 keys and values.
\t// The Map implementation uses private functions, and user-facing
\t// implementations (such as Uint256ToAddressMap) are just wrappers around
\t// the underlying Map.
\t// This means that we can only create new EnumerableMaps for types that fit
\t// in bytes32.

\tstruct MapEntry {
\t\tbytes32 _key;
\t\tbytes32 _value;
\t}

\tstruct Map {
\t\t// Storage of map keys and values
\t\tMapEntry[] _entries;
\t\t// Position of the entry defined by a key in the `entries` array, plus 1
\t\t// because index 0 means a key is not in the map.
\t\tmapping(bytes32 => uint256) _indexes;
\t}

\t/**
\t * @dev Adds a key-value pair to a map, or updates the value for an existing
\t * key. O(1).
\t *
\t * Returns true if the key was added to the map, that is if it was not
\t * already present.
\t */
\tfunction _set(
\t\tMap storage map,
\t\tbytes32 key,
\t\tbytes32 value
\t) private returns (bool) {
\t\t// We read and store the key's index to prevent multiple reads from the same storage slot
\t\tuint256 keyIndex = map._indexes[key];

\t\t// Equivalent to !contains(map, key)
\t\tif (keyIndex == 0) {
\t\t\tmap._entries.push(MapEntry({ _key: key, _value: value }));
\t\t\t// The entry is stored at length-1, but we add 1 to all indexes
\t\t\t// and use 0 as a sentinel value
\t\t\tmap._indexes[key] = map._entries.length;
\t\t\treturn true;
\t\t} else {
\t\t\tmap._entries[keyIndex - 1]._value = value;
\t\t\treturn false;
\t\t}
\t}

\t/**
\t * @dev Removes a key-value pair from a map. O(1).
\t *
\t * Returns true if the key was removed from the map, that is if it was present.
\t */
\tfunction _remove(Map storage map, bytes32 key) private returns (bool) {
\t\t// We read and store the key's index to prevent multiple reads from the same storage slot
\t\tuint256 keyIndex = map._indexes[key];

\t\t// Equivalent to contains(map, key)
\t\tif (keyIndex != 0) {
\t\t\t// To delete a key-value pair from the _entries array in O(1), we swap the entry to delete with the last one
\t\t\t// in the array, and then remove the last entry (sometimes called as 'swap and pop').
\t\t\t// This modifies the order of the array, as noted in {at}.

\t\t\tuint256 toDeleteIndex = keyIndex - 1;
\t\t\tuint256 lastIndex = map._entries.length - 1;

\t\t\t// When the entry to delete is the last one, the swap operation is unnecessary. However, since this occurs
\t\t\t// so rarely, we still do the swap anyway to avoid the gas cost of adding an 'if' statement.

\t\t\tMapEntry storage lastEntry = map._entries[lastIndex];

\t\t\t// Move the last entry to the index where the entry to delete is
\t\t\tmap._entries[toDeleteIndex] = lastEntry;
\t\t\t// Update the index for the moved entry
\t\t\tmap._indexes[lastEntry._key] = toDeleteIndex + 1; // All indexes are 1-based

\t\t\t// Delete the slot where the moved entry was stored
\t\t\tmap._entries.pop();

\t\t\t// Delete the index for the deleted slot
\t\t\tdelete map._indexes[key];

\t\t\treturn true;
\t\t} else {
\t\t\treturn false;
\t\t}
\t}

\t/**
\t * @dev Returns true if the key is in the map. O(1).
\t */
\tfunction _contains(Map storage map, bytes32 key)
\t\tprivate
\t\tview
\t\treturns (bool)
\t{
\t\treturn map._indexes[key] != 0;
\t}

\t/**
\t * @dev Returns the number of key-value pairs in the map. O(1).
\t */
\tfunction _length(Map storage map) private view returns (uint256) {
\t\treturn map._entries.length;
\t}

\t/**
\t * @dev Returns the key-value pair stored at position `index` in the map. O(1).
\t *
\t * Note that there are no guarantees on the ordering of entries inside the
\t * array, and it may change when more entries are added or removed.
\t *
\t * Requirements:
\t *
\t * - `index` must be strictly less than {length}.
\t */
\tfunction _at(Map storage map, uint256 index)
\t\tprivate
\t\tview
\t\treturns (bytes32, bytes32)
\t{
\t\trequire(map._entries.length > index, \"EnumerableMap: index out of bounds\");

\t\tMapEntry storage entry = map._entries[index];
\t\treturn (entry._key, entry._value);
\t}

\t/**
\t * @dev Returns the value associated with `key`.  O(1).
\t *
\t * Requirements:
\t *
\t * - `key` must be in the map.
\t */
\tfunction _get(Map storage map, bytes32 key) private view returns (bytes32) {
\t\treturn _get(map, key, \"EnumerableMap: nonexistent key\");
\t}

\t/**
\t * @dev Same as {_get}, with a custom error message when `key` is not in the map.
\t */
\tfunction _get(
\t\tMap storage map,
\t\tbytes32 key,
\t\tstring memory errorMessage
\t) private view returns (bytes32) {
\t\tuint256 keyIndex = map._indexes[key];
\t\trequire(keyIndex != 0, errorMessage); // Equivalent to contains(map, key)
\t\treturn map._entries[keyIndex - 1]._value; // All indexes are 1-based
\t}

\t// AddressToUintMap

\tstruct AddressToUintMap {
\t\tMap _inner;
\t}

\t/**
\t * @dev Adds a key-value pair to a map, or updates the value for an existing
\t * key. O(1).
\t *
\t * Returns true if the key was added to the map, that is if it was not
\t * already present.
\t */
\tfunction set(
\t\tAddressToUintMap storage map,
\t\taddress key,
\t\tuint256 value
\t) internal returns (bool) {
\t\treturn _set(map._inner, bytes32(uint256(key)), bytes32(value));
\t}

\t/**
\t * @dev Removes a value from a set. O(1).
\t *
\t * Returns true if the key was removed from the map, that is if it was present.
\t */
\tfunction remove(AddressToUintMap storage map, address key)
\t\tinternal
\t\treturns (bool)
\t{
\t\treturn _remove(map._inner, bytes32(uint256(key)));
\t}

\t/**
\t * @dev Returns true if the key is in the map. O(1).
\t */
\tfunction contains(AddressToUintMap storage map, address key)
\t\tinternal
\t\tview
\t\treturns (bool)
\t{
\t\treturn _contains(map._inner, bytes32(uint256(key)));
\t}

\t/**
\t * @dev Returns the number of elements in the map. O(1).
\t */
\tfunction length(AddressToUintMap storage map)
\t\tinternal
\t\tview
\t\treturns (uint256)
\t{
\t\treturn _length(map._inner);
\t}

\t/**
\t * @dev Returns the element stored at position `index` in the set. O(1).
\t * Note that there are no guarantees on the ordering of values inside the
\t * array, and it may change when more values are added or removed.
\t *
\t * Requirements:
\t *
\t * - `index` must be strictly less than {length}.
\t */
\tfunction at(AddressToUintMap storage map, uint256 index)
\t\tinternal
\t\tview
\t\treturns (address, uint256)
\t{
\t\t(bytes32 key, bytes32 value) = _at(map._inner, index);
\t\treturn (address(uint256(key)), uint256(value));
\t}

\t/**
\t * @dev Returns the value associated with `key`.  O(1).
\t *
\t * Requirements:
\t *
\t * - `key` must be in the map.
\t */
\tfunction get(AddressToUintMap storage map, address key)
\t\tinternal
\t\tview
\t\treturns (uint256)
\t{
\t\treturn uint256(_get(map._inner, bytes32(uint256(key))));
\t}

\t/**
\t * @dev Same as {get}, with a custom error message when `key` is not in the map.
\t */
\tfunction get(
\t\tAddressToUintMap storage map,
\t\taddress key,
\t\tstring memory errorMessage
\t) internal view returns (uint256) {
\t\treturn uint256(_get(map._inner, bytes32(uint256(key)), errorMessage));
\t}
}
"
    
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

import \"@openzeppelin/contracts/utils/Address.sol\";
import \"@openzeppelin/contracts-upgradeable/utils/ContextUpgradeable.sol\";
import \"@openzeppelin/contracts/utils/EnumerableSet.sol\";
import \"@openzeppelin/contracts/token/ERC20/IERC20.sol\";
import \"@openzeppelin/contracts-upgradeable/proxy/Initializable.sol\";
import \"@openzeppelin/contracts-upgradeable/utils/PausableUpgradeable.sol\";
import \"@openzeppelin/contracts/token/ERC20/SafeERC20.sol\";
import \"@openzeppelin/contracts/math/SafeMath.sol\";

import \"./LPRewardsData.sol\";
import \"../../libraries/EnumerableMap.sol\";
import \"../interfaces/ILPRewards.sol\";
import \"../interfaces/IValuePerToken.sol\";
import \"../../tokens/interfaces/IWETH.sol\";
import \"../../access/OwnableUpgradeable.sol\";

contract LPRewards is
\tInitializable,
\tContextUpgradeable,
\tOwnableUpgradeable,
\tPausableUpgradeable,
\tLPRewardsData,
\tILPRewards
{
\tusing Address for address payable;
\tusing EnumerableMap for EnumerableMap.AddressToUintMap;
\tusing EnumerableSet for EnumerableSet.AddressSet;
\tusing SafeERC20 for IERC20;
\tusing SafeMath for uint256;

\t/* Immutable Internal State */

\tuint256 internal constant _MULTIPLIER = 1e36;

\t/* Constructor */

\tconstructor(address owner_) {
\t\tinit(owner_);
\t}

\t/* Initializers */

\tfunction init(address owner_) public virtual initializer {
\t\t__Context_init_unchained();
\t\t__Ownable_init_unchained(owner_);
\t\t__Pausable_init_unchained();
\t}

\t/* Fallbacks */

\treceive() external payable {
\t\t// Only accept ETH via fallback from the WETH contract
\t\trequire(msg.sender == _rewardsToken);
\t}

\t/* Modifiers */

\tmodifier supportsToken(address token) {
\t\trequire(supportsStakingToken(token), \"LPRewards: unsupported token\");
\t\t_;
\t}

\t/* Public Views */

\tfunction accruedRewardsPerTokenFor(address token)
\t\tpublic
\t\tview
\t\tvirtual
\t\toverride
\t\treturns (uint256)
\t{
\t\treturn _tokenData[token].arpt;
\t}

\tfunction accruedRewardsPerTokenLastFor(address account, address token)
\t\tpublic
\t\tview
\t\tvirtual
\t\toverride
\t\treturns (uint256)
\t{
\t\treturn _users[account].rewardsFor[token].arptLast;
\t}

\tfunction lastRewardsBalanceOf(address account)
\t\tpublic
\t\tview
\t\tvirtual
\t\toverride
\t\treturns (uint256 total)
\t{
\t\tUserData storage user = _users[account];
\t\tEnumerableSet.AddressSet storage tokens = user.tokensWithRewards;
\t\tfor (uint256 i = 0; i < tokens.length(); i++) {
\t\t\ttotal += user.rewardsFor[tokens.at(i)].pending;
\t\t}
\t}

\tfunction lastRewardsBalanceOfFor(address account, address token)
\t\tpublic
\t\tview
\t\tvirtual
\t\toverride
\t\treturns (uint256)
\t{
\t\treturn _users[account].rewardsFor[token].pending;
\t}

\tfunction lastTotalRewardsAccrued()
\t\texternal
\t\tview
\t\tvirtual
\t\toverride
\t\treturns (uint256)
\t{
\t\treturn _lastTotalRewardsAccrued;
\t}

\tfunction lastTotalRewardsAccruedFor(address token)
\t\texternal
\t\tview
\t\tvirtual
\t\toverride
\t\treturns (uint256)
\t{
\t\treturn _tokenData[token].lastRewardsAccrued;
\t}

\tfunction numStakingTokens()
\t\texternal
\t\tview
\t\tvirtual
\t\toverride
\t\treturns (uint256)
\t{
\t\treturn _tokens.length();
\t}

\tfunction rewardsBalanceOf(address account)
\t\texternal
\t\tview
\t\tvirtual
\t\toverride
\t\treturns (uint256)
\t{
\t\treturn lastRewardsBalanceOf(account) + _allPendingRewardsFor(account);
\t}

\tfunction rewardsBalanceOfFor(address account, address token)
\t\texternal
\t\tview
\t\tvirtual
\t\toverride
\t\treturns (uint256)
\t{
\t\tuint256 rewards = lastRewardsBalanceOfFor(account, token);
\t\tuint256 amountStaked = stakedBalanceOf(account, token);
\t\tif (amountStaked != 0) {
\t\t\trewards += _pendingRewardsFor(account, token, amountStaked);
\t\t}
\t\treturn rewards;
\t}

\tfunction rewardsForToken(address token)
\t\texternal
\t\tview
\t\tvirtual
\t\toverride
\t\treturns (uint256)
\t{
\t\treturn _tokenData[token].rewards;
\t}

\tfunction rewardsToken() public view virtual override returns (address) {
\t\treturn _rewardsToken;
\t}

\tfunction sharesFor(address account, address token)
\t\texternal
\t\tview
\t\tvirtual
\t\toverride
\t\treturns (uint256)
\t{
\t\treturn _shares(token, stakedBalanceOf(account, token));
\t}

\tfunction sharesPerToken(address token)
\t\texternal
\t\tview
\t\tvirtual
\t\toverride
\t\treturns (uint256)
\t{
\t\treturn _shares(token, 1e18);
\t}

\tfunction stakedBalanceOf(address account, address token)
\t\tpublic
\t\tview
\t\tvirtual
\t\toverride
\t\treturns (uint256)
\t{
\t\tEnumerableMap.AddressToUintMap storage staked = _users[account].staked;
\t\tif (staked.contains(token)) {
\t\t\treturn staked.get(token);
\t\t}
\t\treturn 0;
\t}

\tfunction stakingTokenAt(uint256 index)
\t\texternal
\t\tview
\t\tvirtual
\t\toverride
\t\treturns (address)
\t{
\t\treturn _tokens.at(index);
\t}

\tfunction supportsStakingToken(address token)
\t\tpublic
\t\tview
\t\tvirtual
\t\toverride
\t\treturns (bool)
\t{
\t\treturn _tokens.contains(token);
\t}

\tfunction totalRewardsAccrued()
\t\tpublic
\t\tview
\t\tvirtual
\t\toverride
\t\treturns (uint256)
\t{
\t\t// Overflow is OK
\t\treturn _currentRewardsBalance() + _totalRewardsRedeemed;
\t}

\tfunction totalRewardsAccruedFor(address token)
\t\tpublic
\t\tview
\t\tvirtual
\t\toverride
\t\treturns (uint256)
\t{
\t\tTokenData storage td = _tokenData[token];
\t\t// Overflow is OK
\t\treturn td.rewards + td.rewardsRedeemed;
\t}

\tfunction totalRewardsRedeemed()
\t\texternal
\t\tview
\t\tvirtual
\t\toverride
\t\treturns (uint256)
\t{
\t\treturn _totalRewardsRedeemed;
\t}

\tfunction totalRewardsRedeemedFor(address token)
\t\texternal
\t\tview
\t\tvirtual
\t\toverride
\t\treturns (uint256)
\t{
\t\treturn _tokenData[token].rewardsRedeemed;
\t}

\tfunction totalShares()
\t\texternal
\t\tview
\t\tvirtual
\t\toverride
\t\treturns (uint256 total)
\t{
\t\tfor (uint256 i = 0; i < _tokens.length(); i++) {
\t\t\ttotal = total.add(_totalSharesForToken(_tokens.at(i)));
\t\t}
\t}

\tfunction totalSharesFor(address account)
\t\texternal
\t\tview
\t\tvirtual
\t\toverride
\t\treturns (uint256 total)
\t{
\t\tEnumerableMap.AddressToUintMap storage staked = _users[account].staked;
\t\tfor (uint256 i = 0; i < staked.length(); i++) {
\t\t\t(address token, uint256 amount) = staked.at(i);
\t\t\ttotal = total.add(_shares(token, amount));
\t\t}
\t}

\tfunction totalSharesForToken(address token)
\t\texternal
\t\tview
\t\tvirtual
\t\toverride
\t\treturns (uint256)
\t{
\t\treturn _totalSharesForToken(token);
\t}

\tfunction totalStaked(address token)
\t\tpublic
\t\tview
\t\tvirtual
\t\toverride
\t\treturns (uint256)
\t{
\t\treturn _tokenData[token].totalStaked;
\t}

\tfunction unredeemableRewards()
\t\texternal
\t\tview
\t\tvirtual
\t\toverride
\t\treturns (uint256)
\t{
\t\treturn _unredeemableRewards;
\t}

\tfunction valuePerTokenImpl(address token)
\t\tpublic
\t\tview
\t\tvirtual
\t\toverride
\t\treturns (address)
\t{
\t\treturn _tokenData[token].valueImpl;
\t}

\t/* Public Mutators */

\tfunction addToken(address token, address tokenValueImpl)
\t\texternal
\t\tvirtual
\t\toverride
\t\tonlyOwner
\t{
\t\trequire(!supportsStakingToken(token), \"LPRewards: token already added\");
\t\trequire(
\t\t\ttokenValueImpl != address(0),
\t\t\t\"LPRewards: tokenValueImpl cannot be zero address\"
\t\t);
\t\t_tokens.add(token);
\t\t// Only update implementation in case this was previously used and removed
\t\t_tokenData[token].valueImpl = tokenValueImpl;
\t\temit TokenAdded(_msgSender(), token, tokenValueImpl);
\t}

\tfunction changeTokenValueImpl(address token, address tokenValueImpl)
\t\texternal
\t\tvirtual
\t\toverride
\t\tonlyOwner
\t\tsupportsToken(token)
\t{
\t\trequire(
\t\t\ttokenValueImpl != address(0),
\t\t\t\"LPRewards: tokenValueImpl cannot be zero address\"
\t\t);
\t\t_tokenData[token].valueImpl = tokenValueImpl;
\t\temit TokenValueImplChanged(_msgSender(), token, tokenValueImpl);
\t}

\tfunction exit(bool asWETH) external virtual override {
\t\tunstakeAll();
\t\tredeemAllRewards(asWETH);
\t}

\tfunction exitFrom(address token, bool asWETH) external virtual override {
\t\tunstakeAllFrom(token);
\t\tredeemAllRewardsFrom(token, asWETH);
\t}

\tfunction pause() external virtual override onlyOwner {
\t\t_pause();
\t}

\tfunction recoverUnredeemableRewards(address to, uint256 amount)
\t\texternal
\t\tvirtual
\t\toverride
\t\tonlyOwner
\t{
\t\trequire(
\t\t\tamount <= _unredeemableRewards,
\t\t\t\"LPRewards: recovery amount > unredeemable\"
\t\t);
\t\t_unredeemableRewards -= amount;
\t\tIERC20(_rewardsToken).safeTransfer(to, amount);
\t\temit RecoveredUnredeemableRewards(_msgSender(), to, amount);
\t}

\tfunction recoverUnstaked(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external virtual override onlyOwner {
\t\trequire(token != _rewardsToken, \"LPRewards: cannot recover rewardsToken\");

\t\tuint256 unstaked =
\t\t\tIERC20(token).balanceOf(address(this)).sub(totalStaked(token));

\t\trequire(amount <= unstaked, \"LPRewards: recovery amount > unstaked\");

\t\tIERC20(token).safeTransfer(to, amount);
\t\temit RecoveredUnstaked(_msgSender(), token, to, amount);
\t}

\tfunction redeemAllRewards(bool asWETH) public virtual override {
\t\taddress account = _msgSender();
\t\t_updateAllRewardsFor(account);

\t\tUserData storage user = _users[account];
\t\tEnumerableSet.AddressSet storage tokens = user.tokensWithRewards;
\t\tuint256 redemption = 0;

\t\tfor (uint256 length = tokens.length(); length > 0; length--) {
\t\t\taddress token = tokens.at(0);
\t\t\tTokenData storage td = _tokenData[token];
\t\t\tUserTokenRewards storage rewards = user.rewardsFor[token];
\t\t\tuint256 pending = rewards.pending; // Save gas

\t\t\tredemption += pending;

\t\t\trewards.pending = 0;

\t\t\ttd.rewards = td.rewards.sub(pending);
\t\t\ttd.rewardsRedeemed += pending;

\t\t\temit RewardPaid(account, token, pending);
\t\t\ttokens.remove(token);
\t\t}

\t\t_totalRewardsRedeemed += redemption;

\t\t_sendRewards(account, redemption, asWETH);
\t}

\tfunction redeemAllRewardsFrom(address token, bool asWETH)
\t\tpublic
\t\tvirtual
\t\toverride
\t{
\t\taddress account = _msgSender();
\t\t_updateRewardFor(account, token);
\t\tuint256 pending = _users[account].rewardsFor[token].pending;
\t\tif (pending != 0) {
\t\t\t_redeemRewardFrom(token, pending, asWETH);
\t\t}
\t}

\tfunction redeemReward(uint256 amount, bool asWETH)
\t\texternal
\t\tvirtual
\t\toverride
\t{
\t\trequire(amount != 0, \"LPRewards: cannot redeem zero\");
\t\taddress account = _msgSender();
\t\t_updateAllRewardsFor(account);
\t\trequire(
\t\t\tamount <= lastRewardsBalanceOf(account),
\t\t\t\"LPRewards: cannot redeem more rewards than earned\"
\t\t);

\t\tUserData storage user = _users[account];
\t\tEnumerableSet.AddressSet storage tokens = user.tokensWithRewards;
\t\tuint256 amountLeft = amount;

\t\tfor (uint256 length = tokens.length(); length > 0; length--) {
\t\t\taddress token = tokens.at(0);
\t\t\tTokenData storage td = _tokenData[token];
\t\t\tUserTokenRewards storage rewards = user.rewardsFor[token];

\t\t\tuint256 pending = rewards.pending; // Save gas
\t\t\tuint256 taken = 0;
\t\t\tif (pending <= amountLeft) {
\t\t\t\ttaken = pending;
\t\t\t\ttokens.remove(token);
\t\t\t} else {
\t\t\t\ttaken = amountLeft;
\t\t\t}

\t\t\trewards.pending = pending - taken;

\t\t\ttd.rewards = td.rewards.sub(taken);
\t\t\ttd.rewardsRedeemed += taken;

\t\t\tamountLeft -= taken;

\t\t\temit RewardPaid(account, token, taken);

\t\t\tif (amountLeft == 0) {
\t\t\t\tbreak;
\t\t\t}
\t\t}

\t\t_totalRewardsRedeemed += amount;

\t\t_sendRewards(account, amount, asWETH);
\t}

\tfunction redeemRewardFrom(
\t\taddress token,
\t\tuint256 amount,
\t\tbool asWETH
\t) external virtual override {
\t\trequire(amount != 0, \"LPRewards: cannot redeem zero\");
\t\taddress account = _msgSender();
\t\t_updateRewardFor(account, token);
\t\trequire(
\t\t\tamount <= _users[account].rewardsFor[token].pending,
\t\t\t\"LPRewards: cannot redeem more rewards than earned\"
\t\t);
\t\t_redeemRewardFrom(token, amount, asWETH);
\t}

\tfunction removeToken(address token)
\t\texternal
\t\tvirtual
\t\toverride
\t\tonlyOwner
\t\tsupportsToken(token)
\t{
\t\t_tokens.remove(token);
\t\t// Clean up. Keep totalStaked and rewards since those will be cleaned up by
\t\t// users unstaking and redeeming.
\t\t_tokenData[token].valueImpl = address(0);
\t\temit TokenRemoved(_msgSender(), token);
\t}

\tfunction setRewardsToken(address token) public virtual override onlyOwner {
\t\t_rewardsToken = token;
\t\temit RewardsTokenSet(_msgSender(), token);
\t}

\tfunction stake(address token, uint256 amount)
\t\texternal
\t\tvirtual
\t\toverride
\t\twhenNotPaused
\t\tsupportsToken(token)
\t{
\t\trequire(amount != 0, \"LPRewards: cannot stake zero\");

\t\taddress account = _msgSender();
\t\t_updateRewardFor(account, token);

\t\tUserData storage user = _users[account];
\t\tTokenData storage td = _tokenData[token];
\t\ttd.totalStaked += amount;
\t\tuser.staked.set(token, amount + stakedBalanceOf(account, token));

\t\tIERC20(token).safeTransferFrom(account, address(this), amount);
\t\temit Staked(account, token, amount);
\t}

\tfunction unpause() external virtual override onlyOwner {
\t\t_unpause();
\t}

\tfunction unstake(address token, uint256 amount) external virtual override {
\t\trequire(amount != 0, \"LPRewards: cannot unstake zero\");

\t\taddress account = _msgSender();
\t\t// Prevent making calls to any addresses that were never supported.
\t\tuint256 staked = stakedBalanceOf(account, token);
\t\trequire(
\t\t\tamount <= staked,
\t\t\t\"LPRewards: cannot unstake more than staked balance\"
\t\t);

\t\t_unstake(token, amount);
\t}

\tfunction unstakeAll() public virtual override {
\t\tUserData storage user = _users[_msgSender()];
\t\tfor (uint256 length = user.staked.length(); length > 0; length--) {
\t\t\t(address token, uint256 amount) = user.staked.at(0);
\t\t\t_unstake(token, amount);
\t\t}
\t}

\tfunction unstakeAllFrom(address token) public virtual override {
\t\t_unstake(token, stakedBalanceOf(_msgSender(), token));
\t}

\tfunction updateAccrual() external virtual override {
\t\t// Gas savings
\t\tuint256 totalRewardsAccrued_ = totalRewardsAccrued();
\t\tuint256 pending = totalRewardsAccrued_ - _lastTotalRewardsAccrued;
\t\tif (pending == 0) {
\t\t\treturn;
\t\t}

\t\t_lastTotalRewardsAccrued = totalRewardsAccrued_;

\t\t// Iterate once to know totalShares
\t\tuint256 totalShares_ = 0;
\t\t// Store some math for current shares to save on gas and revert ASAP.
\t\tuint256[] memory pendingSharesFor = new uint256[](_tokens.length());
\t\tfor (uint256 i = 0; i < _tokens.length(); i++) {
\t\t\tuint256 share = _totalSharesForToken(_tokens.at(i));
\t\t\tpendingSharesFor[i] = pending.mul(share);
\t\t\ttotalShares_ = totalShares_.add(share);
\t\t}

\t\tif (totalShares_ == 0) {
\t\t\t_unredeemableRewards = _unredeemableRewards.add(pending);
\t\t\temit AccrualUpdated(_msgSender(), pending);
\t\t\treturn;
\t\t}

\t\t// Iterate twice to allocate rewards to each token.
\t\tfor (uint256 i = 0; i < _tokens.length(); i++) {
\t\t\taddress token = _tokens.at(i);
\t\t\tTokenData storage td = _tokenData[token];
\t\t\ttd.rewards += pendingSharesFor[i] / totalShares_;
\t\t\tuint256 rewardsAccrued = totalRewardsAccruedFor(token);
\t\t\ttd.arpt = _accruedRewardsPerTokenFor(token, rewardsAccrued);
\t\t\ttd.lastRewardsAccrued = rewardsAccrued;
\t\t}

\t\temit AccrualUpdated(_msgSender(), pending);
\t}

\tfunction updateReward() external virtual override {
\t\t_updateAllRewardsFor(_msgSender());
\t}

\tfunction updateRewardFor(address token) external virtual override {
\t\t_updateRewardFor(_msgSender(), token);
\t}

\t/* Internal Views */

\tfunction _accruedRewardsPerTokenFor(address token, uint256 rewardsAccrued)
\t\tinternal
\t\tview
\t\tvirtual
\t\treturns (uint256)
\t{
\t\tTokenData storage td = _tokenData[token];
\t\t// Gas savings
\t\tuint256 totalStaked_ = td.totalStaked;

\t\tif (totalStaked_ == 0) {
\t\t\treturn td.arpt;
\t\t}

\t\t// Overflow is OK
\t\tuint256 delta = rewardsAccrued - td.lastRewardsAccrued;
\t\tif (delta == 0) {
\t\t\treturn td.arpt;
\t\t}

\t\t// Use multiplier for better rounding
\t\tuint256 rewardsPerToken = delta.mul(_MULTIPLIER) / totalStaked_;

\t\t// Overflow is OK
\t\treturn td.arpt + rewardsPerToken;
\t}

\tfunction _allPendingRewardsFor(address account)
\t\tinternal
\t\tview
\t\tvirtual
\t\treturns (uint256 total)
\t{
\t\tEnumerableMap.AddressToUintMap storage staked = _users[account].staked;
\t\tfor (uint256 i = 0; i < staked.length(); i++) {
\t\t\t(address token, uint256 amount) = staked.at(i);
\t\t\ttotal += _pendingRewardsFor(account, token, amount);
\t\t}
\t}

\tfunction _currentRewardsBalance() internal view virtual returns (uint256) {
\t\treturn IERC20(_rewardsToken).balanceOf(address(this));
\t}

\tfunction _pendingRewardsFor(
\t\taddress account,
\t\taddress token,
\t\tuint256 amountStaked
\t) internal view virtual returns (uint256) {
\t\tuint256 arpt = accruedRewardsPerTokenFor(token);
\t\tuint256 arptLast = accruedRewardsPerTokenLastFor(account, token);
\t\t// Overflow is OK
\t\tuint256 arptDelta = arpt - arptLast;

\t\treturn amountStaked.mul(arptDelta) / _MULTIPLIER;
\t}

\tfunction _shares(address token, uint256 amountStaked)
\t\tinternal
\t\tview
\t\tvirtual
\t\treturns (uint256)
\t{
\t\tif (!supportsStakingToken(token)) {
\t\t\treturn 0;
\t\t}
\t\tIValuePerToken vptHandle = IValuePerToken(valuePerTokenImpl(token));
\t\t(uint256 numerator, uint256 denominator) = vptHandle.valuePerToken();
\t\tif (denominator == 0) {
\t\t\treturn 0;
\t\t}
\t\t// Return a 1:1 ratio for value to shares
\t\treturn amountStaked.mul(numerator) / denominator;
\t}

\tfunction _totalSharesForToken(address token)
\t\tinternal
\t\tview
\t\tvirtual
\t\treturns (uint256)
\t{
\t\treturn _shares(token, _tokenData[token].totalStaked);
\t}

\t/* Internal Mutators */

\tfunction _redeemRewardFrom(
\t\taddress token,
\t\tuint256 amount,
\t\tbool asWETH
\t) internal virtual {
\t\taddress account = _msgSender();
\t\tUserData storage user = _users[account];
\t\tUserTokenRewards storage rewards = user.rewardsFor[token];
\t\tTokenData storage td = _tokenData[token];
\t\tuint256 rewardLeft = rewards.pending - amount;

\t\trewards.pending = rewardLeft;
\t\tif (rewardLeft == 0) {
\t\t\tuser.tokensWithRewards.remove(token);
\t\t}

\t\ttd.rewards = td.rewards.sub(amount);
\t\ttd.rewardsRedeemed += amount;

\t\t_totalRewardsRedeemed += amount;

\t\t_sendRewards(account, amount, asWETH);
\t\temit RewardPaid(account, token, amount);
\t}

\tfunction _sendRewards(
\t\taddress to,
\t\tuint256 amount,
\t\tbool asWETH
\t) internal virtual {
\t\tif (asWETH) {
\t\t\tIERC20(_rewardsToken).safeTransfer(to, amount);
\t\t} else {
\t\t\tIWETH(_rewardsToken).withdraw(amount);
\t\t\tpayable(to).sendValue(amount);
\t\t}
\t}

\tfunction _unstake(address token, uint256 amount) internal virtual {
\t\taddress account = _msgSender();

\t\t_updateRewardFor(account, token);

\t\tTokenData storage td = _tokenData[token];
\t\ttd.totalStaked = td.totalStaked.sub(amount);

\t\tUserData storage user = _users[account];
\t\tEnumerableMap.AddressToUintMap storage staked = user.staked;

\t\tuint256 stakeLeft = staked.get(token).sub(amount);
\t\tif (stakeLeft == 0) {
\t\t\tstaked.remove(token);
\t\t\tuser.rewardsFor[token].arptLast = 0;
\t\t} else {
\t\t\tstaked.set(token, stakeLeft);
\t\t}

\t\tIERC20(token).safeTransfer(account, amount);
\t\temit Unstaked(account, token, amount);
\t}

\tfunction _updateRewardFor(address account, address token)
\t\tinternal
\t\tvirtual
\t\treturns (uint256)
\t{
\t\tUserData storage user = _users[account];
\t\tUserTokenRewards storage rewards = user.rewardsFor[token];
\t\tuint256 total = rewards.pending; // Save gas
\t\tuint256 amountStaked = stakedBalanceOf(account, token);
\t\tuint256 pending = _pendingRewardsFor(account, token, amountStaked);
\t\tif (pending != 0) {
\t\t\ttotal += pending;
\t\t\trewards.pending = total;
\t\t\tuser.tokensWithRewards.add(token);
\t\t}
\t\trewards.arptLast = accruedRewardsPerTokenFor(token);
\t\treturn total;
\t}

\tfunction _updateAllRewardsFor(address account) internal virtual {
\t\tEnumerableMap.AddressToUintMap storage staked = _users[account].staked;
\t\tfor (uint256 i = 0; i < staked.length(); i++) {
\t\t\t(address token, ) = staked.at(i);
\t\t\t_updateRewardFor(account, token);
\t\t}
\t}
}
"
    
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

import \"@openzeppelin/contracts/utils/EnumerableSet.sol\";

import \"../../libraries/EnumerableMap.sol\";

abstract contract LPRewardsData {
\t/* Structs */

\tstruct TokenData {
\t\tuint256 arpt;
\t\tuint256 lastRewardsAccrued;
\t\tuint256 rewards;
\t\tuint256 rewardsRedeemed;
\t\tuint256 totalStaked;
\t\taddress valueImpl;
\t}

\tstruct UserTokenRewards {
\t\tuint256 pending;
\t\tuint256 arptLast;
\t}

\tstruct UserData {
\t\tEnumerableSet.AddressSet tokensWithRewards;
\t\tmapping(address => UserTokenRewards) rewardsFor;
\t\tEnumerableMap.AddressToUintMap staked;
\t}

\t/* State */

\taddress internal _rewardsToken;
\tuint256 internal _lastTotalRewardsAccrued;
\tuint256 internal _totalRewardsRedeemed;
\tuint256 internal _unredeemableRewards;
\tEnumerableSet.AddressSet internal _tokens;
\tmapping(address => TokenData) internal _tokenData;
\tmapping(address => UserData) internal _users;

\tuint256[43] private __gap;
}
"
    
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

interface ILPRewards {
\t/* Views */

\tfunction accruedRewardsPerTokenFor(address token)
\t\texternal
\t\tview
\t\treturns (uint256);

\tfunction accruedRewardsPerTokenLastFor(address account, address token)
\t\texternal
\t\tview
\t\treturns (uint256);

\tfunction lastRewardsBalanceOf(address account)
\t\texternal
\t\tview
\t\treturns (uint256);

\tfunction lastRewardsBalanceOfFor(address account, address token)
\t\texternal
\t\tview
\t\treturns (uint256);

\tfunction lastTotalRewardsAccrued() external view returns (uint256);

\tfunction lastTotalRewardsAccruedFor(address token)
\t\texternal
\t\tview
\t\treturns (uint256);

\tfunction numStakingTokens() external view returns (uint256);

\tfunction rewardsBalanceOf(address account) external view returns (uint256);

\tfunction rewardsBalanceOfFor(address account, address token)
\t\texternal
\t\tview
\t\treturns (uint256);

\tfunction rewardsForToken(address token) external view returns (uint256);

\tfunction rewardsToken() external view returns (address);

\tfunction sharesFor(address account, address token)
\t\texternal
\t\tview
\t\treturns (uint256);

\tfunction sharesPerToken(address token) external view returns (uint256);

\tfunction stakedBalanceOf(address account, address token)
\t\texternal
\t\tview
\t\treturns (uint256);

\tfunction stakingTokenAt(uint256 index) external view returns (address);

\tfunction supportsStakingToken(address token) external view returns (bool);

\tfunction totalRewardsAccrued() external view returns (uint256);

\tfunction totalRewardsAccruedFor(address token)
\t\texternal
\t\tview
\t\treturns (uint256);

\tfunction totalRewardsRedeemed() external view returns (uint256);

\tfunction totalRewardsRedeemedFor(address token)
\t\texternal
\t\tview
\t\treturns (uint256);

\tfunction totalShares() external view returns (uint256);

\tfunction totalSharesFor(address account) external view returns (uint256);

\tfunction totalSharesForToken(address token) external view returns (uint256);

\tfunction totalStaked(address token) external view returns (uint256);

\tfunction unredeemableRewards() external view returns (uint256);

\tfunction valuePerTokenImpl(address token) external view returns (address);

\t/* Mutators */

\tfunction addToken(address token, address tokenValueImpl) external;

\tfunction changeTokenValueImpl(address token, address tokenValueImpl)
\t\texternal;

\tfunction exit(bool asWETH) external;

\tfunction exitFrom(address token, bool asWETH) external;

\tfunction pause() external;

\tfunction recoverUnredeemableRewards(address to, uint256 amount) external;

\tfunction recoverUnstaked(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction redeemAllRewards(bool asWETH) external;

\tfunction redeemAllRewardsFrom(address token, bool asWETH) external;

\tfunction redeemReward(uint256 amount, bool asWETH) external;

\tfunction redeemRewardFrom(
\t\taddress token,
\t\tuint256 amount,
\t\tbool asWETH
\t) external;

\tfunction removeToken(address token) external;

\tfunction setRewardsToken(address token) external;

\tfunction stake(address token, uint256 amount) external;

\tfunction unpause() external;

\tfunction unstake(address token, uint256 amount) external;

\tfunction unstakeAll() external;

\tfunction unstakeAllFrom(address token) external;

\tfunction updateAccrual() external;

\tfunction updateReward() external;

\tfunction updateRewardFor(address token) external;

\t/* Events */

\tevent AccrualUpdated(address indexed author, uint256 accruedRewards);
\tevent RecoveredUnredeemableRewards(
\t\taddress indexed author,
\t\taddress indexed to,
\t\tuint256 amount
\t);
\tevent RecoveredUnstaked(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
\tevent RewardPaid(
\t\taddress indexed account,
\t\taddress indexed token,
\t\tuint256 amount
\t);
\tevent RewardsTokenSet(address indexed author, address indexed token);
\tevent Staked(address indexed account, address indexed token, uint256 amount);
\tevent TokenAdded(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed tokenValueImpl
\t);
\tevent TokenRemoved(address indexed author, address indexed token);
\tevent TokenValueImplChanged(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed tokenValueImpl
\t);
\tevent Unstaked(
\t\taddress indexed account,
\t\taddress indexed token,
\t\tuint256 amount
\t);
}
"
    
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

interface IValuePerToken {
\t/* Views */

\tfunction token() external view returns (address);

\tfunction valuePerToken()
\t\texternal
\t\tview
\t\treturns (uint256 numerator, uint256 denominator);
}
"
    
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
