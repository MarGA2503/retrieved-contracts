pragma solidity 0.4.24;
import \"./SafeMath.sol\";
import \"./Modifiers.sol\";

contract ColorTeam is Modifiers {

    using SafeMath for uint;

    //100 last painters for winning color =color team
    function formColorTeam(uint _winnerColor) private returns (uint) {
        uint paintsCurrentRoundForColor = paintsCounterForColor[_winnerColor];
         if (paintsCurrentRoundForColor\u003e1000)
        {
            paintsCurrentRoundForColor = 1000;
        }
        for (uint i =paintsCurrentRoundForColor; i \u003e 0; i--) {
            uint teamMembersCounter;
            if (isInCBT[cbIteration][counterToPainterForColor[_winnerColor][i]] == false) {
                
                if (paintsCurrentRoundForColor \u003e 100) {
                    if (teamMembersCounter \u003e= 100)   
                        break;
                }
            
                else {
                    if (teamMembersCounter \u003e= paintsCurrentRoundForColor)
                        break;
                }
                
                cbTeam[cbIteration].push(counterToPainterForColor[_winnerColor][i]);
                teamMembersCounter = teamMembersCounter.add(1);
                isInCBT[cbIteration][counterToPainterForColor[_winnerColor][i]] = true;
            }
        }
        return cbTeam[cbIteration].length;
    }
    
    function calculateCBP(uint _winnerColor) private {

        uint length = formColorTeam(_winnerColor);
        address painter;
        uint totalPaintsForTeam; 

        for (uint i = 0; i \u003c length; i++) {
            painter = cbTeam[cbIteration][i];
            totalPaintsForTeam += colorBankShare[cbIteration][_winnerColor][painter];
        }
        
        for (i = 0; i \u003c length; i++) {
            painter = cbTeam[cbIteration][i];
            painterToCBP[cbIteration][painter] = (colorBankShare[cbIteration][_winnerColor][painter].mul(colorBankForRound[currentRound])).div(totalPaintsForTeam);
        }

    }

    function distributeCBP() external canDistributeCBP() {
        require(isCBPTransfered[cbIteration] == false, \"Color Bank Prizes already transferred for this cbIteration\");
        address painter;
        calculateCBP(winnerColorForRound[currentRound]);
        painterToCBP[cbIteration][winnerOfRound[currentRound]] += colorBankForRound[currentRound];
        uint length = cbTeam[cbIteration].length;
        for (uint i = 0; i \u003c length; i++) {
            painter = cbTeam[cbIteration][i];
            if (painterToCBP[cbIteration][painter] != 0) {
                uint prize = painterToCBP[cbIteration][painter];
                painter.transfer(prize);
                emit CBPDistributed(currentRound, cbIteration, painter, prize);
            }
        }
        isCBPDistributable = false;
        isCBPTransfered[cbIteration] = true;
        currentRound = currentRound.add(1); 
        cbIteration = cbIteration.add(1); 
        isGamePaused = false;
    }
    
}"},"DividendsDistributor.sol":{"content":"pragma solidity 0.4.24;
import \"./SafeMath.sol\";
import \"./IColor.sol\";
import \"./Modifiers.sol\";

contract DividendsDistributor is Modifiers {
    using SafeMath for uint;
    
    function claimDividends() external {
        //if users dividends=0, revert 
        require(pendingWithdrawals[msg.sender] != 0, \"Your withdrawal balance is zero.\");
        claimId = claimId.add(1);
        Claim memory c;
        c.id = claimId;
        c.claimer = msg.sender;
        c.isResolved = false;
        c.timestamp = now;
        claims.push(c);
        emit DividendsClaimed(msg.sender, claimId, now);
    }
    
     function withdrawFoundersComission() external onlyAdmin() returns (bool) {
        require(pendingWithdrawals[founders] != 0, \"Foundrs withdrawal balance is zero.\");
        uint balance = pendingWithdrawals[founders];
        pendingWithdrawals[founders] = 0;
        founders.transfer(balance);
        return true;
    }
    

    function approveClaim(uint _claimId) public onlyAdmin() {
        
        Claim storage claim = claims[_claimId];
        
        require(!claim.isResolved);
        
        address claimer = claim.claimer;

        //Checks-Effects-Interactions pattern
        uint withdrawalAmount = pendingWithdrawals[claimer];

        
        pendingWithdrawals[claimer] = 0;

        
        claimer.transfer(withdrawalAmount);
        
        //set last withdr time for user
        addressToLastWithdrawalTime[claimer] = now;
        emit DividendsWithdrawn(claimer, _claimId, withdrawalAmount);

        claim.isResolved = true;
    }

}"},"ERC1538Delegate.sol":{"content":"pragma solidity 0.4.24;
import \"./IERC1538.sol\";
import \"./ERC1538QueryDelegates.sol\";

/******************************************************************************\\
* Implementation of ERC1538.
* Function signatures are stored in an array so functions can be queried.
/******************************************************************************/

contract ERC1538Delegate is IERC1538, ERC1538QueryDelegates {

    function updateContract(address _delegate, string _functionSignatures, string commitMessage) external onlyOwner {
        // pos is first used to check the size of the delegate contract.
        // After that pos is the current memory location of _functionSignatures.
        // It is used to move through the characters of _functionSignatures
        uint256 pos;
        if(_delegate != address(0)) {
            assembly {
                pos := extcodesize(_delegate)
            }
            require(pos \u003e 0, \"_delegate address is not a contract and is not address(0)\");
        }
        // creates a bytes vesion of _functionSignatures
        bytes memory signatures = bytes(_functionSignatures);
        // stores the position in memory where _functionSignatures ends.
        uint256 signaturesEnd;
        // stores the starting position of a function signature in _functionSignatures
        uint256 start;
        assembly {
            pos := add(signatures,32)
            start := pos
            signaturesEnd := add(pos,mload(signatures))
        }
        // the function id of the current function signature
        bytes4 funcId;
        // the delegate address that is being replaced or address(0) if removing functions
        address oldDelegate;
        // the length of the current function signature in _functionSignatures
        uint256 num;
        // the current character in _functionSignatures
        uint256 char;
        // the position of the current function signature in the funcSignatures array
        uint256 index;
        // the last position in the funcSignatures array
        uint256 lastIndex;
        // parse the _functionSignatures string and handle each function
        for (; pos \u003c signaturesEnd; pos++) {
            assembly {char := byte(0,mload(pos))}
            // 0x29 == )
            if (char == 0x29) {
                pos++;
                num = (pos - start);
                start = pos;
                assembly {
                    mstore(signatures,num)
                }
                funcId = bytes4(keccak256(signatures));
                oldDelegate = delegates[funcId];
                if(_delegate == address(0)) {
                    index = funcSignatureToIndex[signatures];
                    require(index != 0, \"Function does not exist.\");
                    index--;
                    lastIndex = funcSignatures.length - 1;
                    if (index != lastIndex) {
                        funcSignatures[index] = funcSignatures[lastIndex];
                        funcSignatureToIndex[funcSignatures[lastIndex]] = index + 1;
                    }
                    funcSignatures.length--;
                    delete funcSignatureToIndex[signatures];
                    delete delegates[funcId];
                    emit FunctionUpdate(funcId, oldDelegate, address(0), string(signatures));
                }
                else if (funcSignatureToIndex[signatures] == 0) {
                    require(oldDelegate == address(0), \"Funcion id clash.\");
                    delegates[funcId] = _delegate;
                    funcSignatures.push(signatures);
                    funcSignatureToIndex[signatures] = funcSignatures.length;
                    emit FunctionUpdate(funcId, address(0), _delegate, string(signatures));
                }
                else if (delegates[funcId] != _delegate) {
                    delegates[funcId] = _delegate;
                    emit FunctionUpdate(funcId, oldDelegate, _delegate, string(signatures));

                }
                assembly {signatures := add(signatures,num)}
            }
        }
        emit CommitMessage(commitMessage);
    }
}"},"ERC1538QueryDelegates.sol":{"content":"pragma solidity 0.4.24;
/******************************************************************************\\
* 
* Contains functions for retrieving function signatures and delegate contract
* addresses.
/******************************************************************************/

import \"./StorageV0.sol\";
import \"./IERC1538Query.sol\";

contract ERC1538QueryDelegates is IERC1538Query, StorageV0 {

    function totalFunctions() external view returns(uint256) {
        return funcSignatures.length;
    }

    function functionByIndex(uint256 _index) external view returns(string memory functionSignature, bytes4 functionId, address delegate) {
        require(_index \u003c funcSignatures.length, \"functionSignatures index does not exist.\");
        bytes memory signature = funcSignatures[_index];
        functionId = bytes4(keccak256(signature));
        delegate = delegates[functionId];
        return (string(signature), functionId, delegate);
    }

    function functionExists(string _functionSignature) external view returns(bool) {
        return funcSignatureToIndex[bytes(_functionSignature)] != 0;
    }

    function functionSignatures() external view returns(string) {
        uint256 signaturesLength;
        bytes memory signatures;
        bytes memory signature;
        uint256 functionIndex;
        uint256 charPos;
        uint256 funcSignaturesNum = funcSignatures.length;
        bytes[] memory memoryFuncSignatures = new bytes[](funcSignaturesNum);
        for(; functionIndex \u003c funcSignaturesNum; functionIndex++) {
            signature = funcSignatures[functionIndex];
            signaturesLength += signature.length;
            memoryFuncSignatures[functionIndex] = signature;
        }
        signatures = new bytes(signaturesLength);
        functionIndex = 0;
        for(; functionIndex \u003c funcSignaturesNum; functionIndex++) {
            signature = memoryFuncSignatures[functionIndex];
            for(uint256 i = 0; i \u003c signature.length; i++) {
                signatures[charPos] = signature[i];
                charPos++;
            }
        }
        return string(signatures);
    }

    function delegateFunctionSignatures(address _delegate) external view returns(string) {
        uint256 funcSignaturesNum = funcSignatures.length;
        bytes[] memory delegateSignatures = new bytes[](funcSignaturesNum);
        uint256 delegateSignaturesPos;
        uint256 signaturesLength;
        bytes memory signatures;
        bytes memory signature;
        uint256 functionIndex;
        uint256 charPos;
        for(; functionIndex \u003c funcSignaturesNum; functionIndex++) {
            signature = funcSignatures[functionIndex];
            if(_delegate == delegates[bytes4(keccak256(signature))]) {
                signaturesLength += signature.length;
                delegateSignatures[delegateSignaturesPos] = signature;
                delegateSignaturesPos++;
            }

        }
        signatures = new bytes(signaturesLength);
        functionIndex = 0;
        for(; functionIndex \u003c delegateSignatures.length; functionIndex++) {
            signature = delegateSignatures[functionIndex];
            if(signature.length == 0) {
                break;
            }
            for(uint256 i = 0; i \u003c signature.length; i++) {
                signatures[charPos] = signature[i];
                charPos++;
            }
        }
        return string(signatures);
    }

    function delegateAddress(string _functionSignature) external view returns(address) {
        require(funcSignatureToIndex[bytes(_functionSignature)] != 0, \"Function signature not found.\");
        return delegates[bytes4(keccak256(bytes(_functionSignature)))];
    }

    function functionById(bytes4 _functionId) external view returns(string signature, address delegate) {
        for(uint256 i = 0; i \u003c funcSignatures.length; i++) {
            if(_functionId == bytes4(keccak256(funcSignatures[i]))) {
                return (string(funcSignatures[i]), delegates[_functionId]);
            }
        }
        revert(\"functionId not found\");
    }

    function delegateAddresses() external view returns(address[]) {
        uint256 funcSignaturesNum = funcSignatures.length;
        address[] memory delegatesBucket = new address[](funcSignaturesNum);
        uint256 numDelegates;
        uint256 functionIndex;
        bool foundDelegate;
        address delegate;
        for(; functionIndex \u003c funcSignaturesNum; functionIndex++) {
            delegate = delegates[bytes4(keccak256(funcSignatures[functionIndex]))];
            for(uint256 i = 0; i \u003c numDelegates; i++) {
                if(delegate == delegatesBucket[i]) {
                    foundDelegate = true;
                    break;
                }
            }
            if(foundDelegate == false) {
                delegatesBucket[numDelegates] = delegate;
                numDelegates++;
            }
            else {
                foundDelegate = false;
            }
        }
        address[] memory delegates_ = new address[](numDelegates);
        functionIndex = 0;
        for(; functionIndex \u003c numDelegates; functionIndex++) {
            delegates_[functionIndex] = delegatesBucket[functionIndex];
        }
        return delegates_;
    }
}"},"Game.sol":{"content":"pragma solidity 0.4.24;
import \"./PaintsPool.sol\";
import \"./PaintDiscount.sol\";
import \"./Modifiers.sol\";
import \"./Utils.sol\";

contract Game is PaintDiscount, PaintsPool, Modifiers {
    using SafeMath for uint;

     //function estimating call price for given color
    function estimateCallPrice(uint[] _pixels, uint _color) public view returns (uint totalCallPrice) {

        uint moneySpent = moneySpentByUserForColor[_color][msg.sender];
        bool hasDiscount = hasPaintDiscountForColor[_color][msg.sender];
        uint discount = usersPaintDiscountForColor[_color][msg.sender];
        
        for (uint i = 0; i \u003c _pixels.length; i++) {
            
            uint discountCallPrice = (nextCallPriceForColor[_color].mul(100 - discount)).div(100);
            
            if (hasDiscount == true) 
                uint price = discountCallPrice;
            else
                price = nextCallPriceForColor[_color]; 

            totalCallPrice += price;
            moneySpent += price;

            if (moneySpent \u003e= 1 ether) {
                
                hasDiscount = true;
                discount = moneySpent / 1 ether;
                
                if (moneySpent \u003e= 10 ether)
                    discount = 10;
            }
            
        }   
    }

    function drawTimeBank() public {

        uint lastPaintTime = lastPaintTimeForRound[currentRound];
        require ((now - lastPaintTime) \u003e 20 minutes \u0026\u0026 lastPaintTime != 0, \"20 minutes have not passed yet.\");

        
        winnerOfRound[currentRound] = lastPainterForRound[currentRound];

        //timebank(1) was drawn for this round
        winnerBankForRound[currentRound] = 1; 
        //10% of time bank goes to next round
        timeBankForRound[currentRound + 1] = timeBankForRound[currentRound].div(10); 
        //45% of time bank goes to every participant in a round
        timeBankForRound[currentRound] = timeBankForRound[currentRound].mul(45).div(100); 
        //color bank goes to next round
        colorBankForRound[currentRound + 1] = colorBankForRound[currentRound]; 
        
        colorBankForRound[currentRound] = 0; 
       
        emit TimeBankPlayed(winnerOfRound[currentRound], currentRound);

        isTBPDistributable = true;
        isGamePaused = true;
        timeBankDrawnForRound[currentRound] = true;

    }

    
    function paint(uint[] _pixels, uint _color, string _refLink) external payable isRegistered(_refLink) isLiveGame() {

        require(msg.value == estimateCallPrice(_pixels, _color), \"Wrong call price\");
        require(_color \u003e 0 \u0026\u0026 _color \u003c= totalColorsNumber, \"The color with such id does not exist.\"); 

        // bytes32 refLink32 = Utils.toBytes32(_refLink);
        // require(keccak256(abi.encodePacked(_refLink)) == keccak256(abi.encodePacked()) || refLinkExists[refLink32] == true, \"No such referral link exists.\");
        
       //check whether 20 minutes passed since last paint 
        if ((now - lastPaintTimeForRound[currentRound]) \u003e 20 minutes \u0026\u0026 
            lastPaintTimeForRound[currentRound] != 0 \u0026\u0026 
            timeBankDrawnForRound[currentRound] == false) {

            drawTimeBank();
            msg.sender.transfer(msg.value);

        }
        
        else {
            //distribute money to banks and dividends
            _setBanks(_color);

            //paint pixels
            for (uint i = 0; i \u003c _pixels.length; i++) {
                _paint(_pixels[i], _color);
            }
            
            
            _distributeDividends(_color, _refLink);
        
            //save user spended money for this color
            _setMoneySpentByUserForColor(_color); 
            
           
            _setUsersPaintDiscountForColor(_color);

            if (paintsCounterForColor[_color] == 0) {
                paintGenToEndTimeForColor[_color][currentPaintGenForColor[_color] - 1] = now;
            }

            paintsCounter++; //counter for all users paints
            paintsCounterForColor[_color] ++; //counter for given color
            counterToPainter[paintsCounter] = msg.sender; //counter for given user
            counterToPainterForColor[_color][paintsCounterForColor[_color]] = msg.sender; 

            if (isUserCountedForRound[currentRound][msg.sender] == false) {
                usersCounterForRound[currentRound] = usersCounterForRound[currentRound].add(1);
                isUserCountedForRound[currentRound][msg.sender] = true;
            }
        }

    }   

    
    function _paint(uint _pixel, uint _color) internal {

        //set paints amount in a pool and price for paint
        _fillPaintsPool(_color);
        
        require(msg.sender == tx.origin);

        require(_pixel \u003e 0 \u0026\u0026 _pixel \u003c= totalPixelsNumber, \"The pixel with such id does not exist.\");

       
     
        uint oldColor = pixelToColorForRound[currentRound][_pixel];
    
        
        pixelToColorForRound[currentRound][_pixel] = _color; 
            
        //save old color for pixel
        pixelToOldColorForRound[currentRound][_pixel] = oldColor; 
                
      
        lastPaintTimeForRound[currentRound] = now; 
    
       
        lastPainterForRound[currentRound] = msg.sender;
                
       
        if (colorToPaintedPixelsAmountForRound[currentRound][oldColor] \u003e 0) 
            colorToPaintedPixelsAmountForRound[currentRound][oldColor] = colorToPaintedPixelsAmountForRound[currentRound][oldColor].sub(1); 
    
        
        colorToPaintedPixelsAmountForRound[currentRound][_color] = colorToPaintedPixelsAmountForRound[currentRound][_color].add(1); 

        //increase paints amount for given color for color team iteration
        colorToTotalPaintsForCBIteration[cbIteration][_color] = colorToTotalPaintsForCBIteration[cbIteration][_color].add(1);

        
        totalPaintsForRound[currentRound] = totalPaintsForRound[currentRound].add(1); 

        pixelToPaintTimeForRound[currentRound][_pixel] = now;

       
        if (lastPaintTimeOfUser[msg.sender] != 0 \u0026\u0026 now - lastPaintTimeOfUser[msg.sender] \u003c 24 hours) 
            timeBankShare[tbIteration][msg.sender]++;
            
        else    
            timeBankShare[tbIteration][msg.sender] = 1;

        
        if (lastPaintTimeOfUserForColor[_color][msg.sender] != 0 \u0026\u0026 now - lastPaintTimeOfUserForColor[_color][msg.sender] \u003c 24 hours) 
            colorBankShare[cbIteration][_color][msg.sender]++;

        else 
            colorBankShare[cbIteration][_color][msg.sender] = 1;

        lastPaintTimeOfUser[msg.sender] = now;
        lastPaintTimeOfUserForColor[_color][msg.sender] = now;
                
        //decrease paints pool by 1 
        paintGenToAmountForColor[_color][currentPaintGenForColor[_color]] = paintGenToAmountForColor[_color][currentPaintGenForColor[_color]].sub(1);
        
       
        lastPaintedPixelForRound[currentRound] = _pixel;
        
        
        emit Paint(_pixel, _color, msg.sender, currentRound, now);    

        
        lastPlayedRound[msg.sender] = currentRound;
            
        //chreck wherether all pixels are the same color
        if (colorToPaintedPixelsAmountForRound[currentRound][_color] == totalPixelsNumber) {

           
            winnerColorForRound[currentRound] = _color;

            
            winnerOfRound[currentRound] = lastPainterForRound[currentRound];        
            //color bank(2)
            winnerBankForRound[currentRound] = 2;
            //10% goes to next round  
            colorBankForRound[currentRound + 1] = colorBankForRound[currentRound].div(10); 
            //45% for color team 
            colorBankForRound[currentRound] = colorBankForRound[currentRound].mul(45).div(100);
            //timebank goes to next round
            timeBankForRound[currentRound + 1] = timeBankForRound[currentRound];
            timeBankForRound[currentRound] = 0;     
            emit ColorBankPlayed(winnerOfRound[currentRound], currentRound);  
            
            isGamePaused = true;
            isCBPDistributable = true;
            //distributeCBP();
        }
    }

    
    function _setBanks(uint _color) private {
        
        colorBankToColorForRound[currentRound][_color] = colorBankToColorForRound[currentRound][_color].add(msg.value.mul(40).div(100));

        //40% to color colorBank
        colorBankForRound[currentRound] = colorBankForRound[currentRound].add(msg.value.mul(40).div(100));

        //40% to timebank
        timeBankForRound[currentRound] = timeBankForRound[currentRound].add(msg.value.mul(40).div(100));

        //20% goes to dividends 
        dividendsBank = dividendsBank.add(msg.value.div(5)); 
    }

    
    function _distributeDividends(uint _color, string _refLink) internal {
        
        //require(ownerOfColor[_color] != address(0), \"There is no such color\");
        bytes32 refLink32 = Utils.toBytes16(_refLink);
    
        //if  reflink provided
        if (refLinkExists[refLink32] == true) { 

            //25% goes to founders
            pendingWithdrawals[founders] = pendingWithdrawals[founders].add(dividendsBank.div(4)); 

            //25% owner of color
            pendingWithdrawals[ownerOfColor[_color]] += dividendsBank.div(4);
            //25% owner of pixel
            pendingWithdrawals[ownerOfPixel] += dividendsBank.div(4);

            //25% to referal
            pendingWithdrawals[refLinkToUser[refLink32]] += dividendsBank.div(4);
            dividendsBank = 0;
        }

        else {

            pendingWithdrawals[founders] = pendingWithdrawals[founders].add(dividendsBank.div(3)); 
            pendingWithdrawals[ownerOfColor[_color]] += dividendsBank.div(3);
            pendingWithdrawals[ownerOfPixel] += dividendsBank.div(3);
            dividendsBank = 0;
        }
    }

    modifier isRegistered(string _refLink) {
      
        if (isRegisteredUser[msg.sender] != true) {
            bytes32 refLink32 = Utils.toBytes16(_refLink);
             
            if (refLinkExists[refLink32]) { 
                address referrer = refLinkToUser[refLink32];
                referrerToReferrals[referrer].push(msg.sender);
                referralToReferrer[msg.sender] = referrer;
                hasReferrer[msg.sender] = true;
            }
            uniqueUsersCount = uniqueUsersCount.add(1);
            newUserToCounter[msg.sender] = uniqueUsersCount;
            registrationTimeForUser[msg.sender] = now;
            isRegisteredUser[msg.sender] = true;
        }
        _;
    }

}"},"GameStateController.sol":{"content":"pragma solidity 0.4.24;
import \"./Roles.sol\";
import \"./Modifiers.sol\";

contract GameStateController is Modifiers {

    function pauseGame() external onlyAdmin() {
        require (isGamePaused == false, \"Game is already paused\");
        isGamePaused = true;
    }

    function resumeGame() external onlyAdmin() {
        require (isGamePaused == true, \"Game is already live\");
        isGamePaused = false;
    }

    function withdrawEther() external onlyAdmin() returns (bool) {
        require (isGamePaused == true, \"Can withdraw when game is live\");
        uint balance = address(this).balance;
        uint colorBank = colorBankForRound[currentRound];
        uint timeBank = timeBankForRound[currentRound];
        owner().transfer(balance);
        colorBankForRound[currentRound]= 0;
        timeBankForRound[currentRound]= 0;
        emit EtherWithdrawn(balance, colorBank, timeBank, now);
        return true;
    }
    
}
"},"Helpers.sol":{"content":"pragma solidity 0.4.24;
import \"./Modifiers.sol\";

contract Helpers is Modifiers {

   
    function getPixelColor(uint _pixel) external view returns (uint) {
        return pixelToColorForRound[currentRound][_pixel];
    }

    //function adding new color to the game after minting
    function addNewColor() external onlyAdmin() {
        totalColorsNumber++; //TODO - Check whether this line should be put in the end or here
        currentPaintGenForColor[totalColorsNumber] = 1;
        callPriceForColor[totalColorsNumber] = 0.01 ether;
        nextCallPriceForColor[totalColorsNumber] = callPriceForColor[totalColorsNumber];
        paintGenToAmountForColor[totalColorsNumber][currentPaintGenForColor[totalColorsNumber]] = maxPaintsInPool;
        paintGenStartedForColor[totalColorsNumber][currentPaintGenForColor[totalColorsNumber]] = true;
        paintGenToEndTimeForColor[totalColorsNumber][currentPaintGenForColor[totalColorsNumber] - 1] = now;
        paintGenToStartTimeForColor[totalColorsNumber][currentPaintGenForColor[totalColorsNumber]] = now;
    }

}"},"IColor.sol":{"content":"pragma solidity 0.4.24;

interface Color {
    function totalSupply() external view returns (uint);
    function ownerOf(uint _tokenId) external view returns (address);
}

"},"IERC1538.sol":{"content":"pragma solidity 0.4.24;

interface IERC1538 {
    event CommitMessage(string message);
    event FunctionUpdate(bytes4 indexed functionId, address indexed oldDelegate, address indexed newDelegate, string functionSignature);
    function updateContract(address _delegate, string _functionSignatures, string commitMessage) external;
}"},"IERC1538Query.sol":{"content":"pragma solidity 0.4.24;

interface IERC1538Query {
    function totalFunctions() external view returns(uint256);
    function functionByIndex(uint256 _index) external view returns(string memory functionSignature, bytes4 functionId, address delegate);
    function functionExists(string _functionSignature) external view returns(bool);
    function functionSignatures() external view returns(string);
    function delegateFunctionSignatures(address _delegate) external view returns(string);
    function delegateAddress(string _functionSignature) external view returns(address);
    function functionById(bytes4 _functionId) external view returns(string signature, address delegate);
    function delegateAddresses() external view returns(address[]);
}"},"Initializer.sol":{"content":"pragma solidity 0.4.24;
import \"./StorageV1.sol\";

contract Initializer is StorageV1 {

    //constructor
    function _initializer() internal {

        
        refLinkPrice  = 0.1 ether;
        totalColorsNumber = 8;
        totalPixelsNumber = 49;
        
        isAdmin[msg.sender] = true;
        maxPaintsInPool = totalPixelsNumber; 
        currentRound = 1;
        cbIteration = 1;
        tbIteration = 1;
        
        for (uint i = 1; i \u003c= totalColorsNumber; i++) {
            currentPaintGenForColor[i] = 1;
            callPriceForColor[i] = 0.005 ether;
            nextCallPriceForColor[i] = callPriceForColor[i];
            paintGenToAmountForColor[i][currentPaintGenForColor[i]] = maxPaintsInPool;
            paintGenStartedForColor[i][currentPaintGenForColor[i]] = true;
            //paintGenToEndTimeForColor[i][currentPaintGenForColor[i] - 1] = now;
            paintGenToStartTimeForColor[i][currentPaintGenForColor[i]] = now;
        }
        
    }
}
"},"IPixel.sol":{"content":"pragma solidity 0.4.24;

interface Pixel {
    function totalSupply() external view returns (uint);
    function ownerOf(uint _tokenId) external view returns (address);
}

"},"Migrations.sol":{"content":"pragma solidity ^0.4.23;

contract Migrations {
  address public owner;
  uint public last_completed_migration;

  constructor() public {
    owner = msg.sender;
  }

  modifier restricted() {
    if (msg.sender == owner) _;
  }

  function setCompleted(uint completed) public restricted {
    last_completed_migration = completed;
  }

  function upgrade(address new_address) public restricted {
    Migrations upgraded = Migrations(new_address);
    upgraded.setCompleted(last_completed_migration);
  }
}
"},"Modifiers.sol":{"content":"pragma solidity 0.4.24;
import \"./SafeMath.sol\";
import \"./StorageV1.sol\";

contract Modifiers is StorageV1 {
    using SafeMath for uint;

    modifier onlyAdmin() {
        require(isAdmin[msg.sender] == true, \"You don\u0027t have admin rights.\");
        _;
    }

    modifier isLiveGame() {
        require(isGamePaused == false, \"Game is paused.\");
        _;
    }

    modifier canDistributeCBP() {
        require(isCBPDistributable == true, \"Cannot distribute color bank prize at the moment.\");
        _;
    }

    modifier canDistributeTBP() {
        require(isTBPDistributable == true, \"Cannot distribute time bank prize at the moment.\");
        _;
    }

    //should be 4-8 symbols
    modifier isValidRefLink(string _str) {
        require(bytes(_str).length \u003e= 4, \"Ref link should be of length [4,8]\");
        require(bytes(_str).length \u003c= 8, \"Ref link should be of length [4,8]\");
        _;
    }
    
}"},"Ownable.sol":{"content":"pragma solidity 0.4.24;

/**
 * @title Ownable
 * @dev The Ownable contract has an owner address, and provides basic authorization control
 * functions, this simplifies the implementation of \"user permissions\".
 */
contract Ownable {
  address private _owner;

  event OwnershipTransferred(
    address indexed previousOwner,
    address indexed newOwner
  );

  /**
   * @dev The Ownable constructor sets the original `owner` of the contract to the sender
   * account.
   */
  constructor() internal {
    _owner = msg.sender;
    emit OwnershipTransferred(address(0), _owner);
  }

  /**
   * @return the address of the owner.
   */
  function owner() public view returns(address) {
    return _owner;
  }

  /**
   * @dev Throws if called by any account other than the owner.
   */
  modifier onlyOwner() {
    require(isOwner());
    _;
  }

  /**
   * @return true if `msg.sender` is the owner of the contract.
   */
  function isOwner() public view returns(bool) {
    return msg.sender == _owner;
  }

  /**
   * @dev Allows the current owner to relinquish control of the contract.
   * @notice Renouncing to ownership will leave the contract without an owner.
   * It will not be possible to call the functions with the `onlyOwner`
   * modifier anymore.
   */
  function renounceOwnership() public onlyOwner {
    emit OwnershipTransferred(_owner, address(0));
    _owner = address(0);
  }

  /**
   * @dev Allows the current owner to transfer control of the contract to a newOwner.
   * @param newOwner The address to transfer ownership to.
   */
  function transferOwnership(address newOwner) public onlyOwner {
    _transferOwnership(newOwner);
  }

  /**
   * @dev Transfers control of the contract to a newOwner.
   * @param newOwner The address to transfer ownership to.
   */
  function _transferOwnership(address newOwner) internal {
    require(newOwner != address(0));
    emit OwnershipTransferred(_owner, newOwner);
    _owner = newOwner;
  }
}
"},"PaintDiscount.sol":{"content":"pragma solidity 0.4.24;
import \"./SafeMath.sol\";
import \"./StorageV1.sol\";

contract PaintDiscount is StorageV1 {
    using SafeMath for uint;
    
    //saving discount for user
    function _setUsersPaintDiscountForColor(uint _color) internal {
        
        //each 1 eth = 1% discount
        usersPaintDiscountForColor[_color][msg.sender] = moneySpentByUserForColor[_color][msg.sender] / 1 ether;
        
        //max discount 10% 
        if (moneySpentByUserForColor[_color][msg.sender] \u003e= 10 ether)
            usersPaintDiscountForColor[_color][msg.sender] = 10;
        
    }
    
    //Money spent by user buying this color  
    function _setMoneySpentByUserForColor(uint _color) internal {
        
        moneySpentByUserForColor[_color][msg.sender] += msg.value;
        moneySpentByUser[msg.sender] += msg.value;

        if (moneySpentByUserForColor[_color][msg.sender] \u003e= 1 ether)
            hasPaintDiscountForColor[_color][msg.sender] = true;
    }
}"},"PaintsPool.sol":{"content":"pragma solidity ^0.4.24;
import \"./SafeMath.sol\";
import \"./StorageV1.sol\";

contract PaintsPool is StorageV1 {
    using SafeMath for uint;

    //update paint price
    function _updateCallPrice(uint _color) private {
        
        //increase call price for 5%(for frontend)
        nextCallPriceForColor[_color] = callPriceForColor[_color].mul(105).div(100);
        
        
        emit CallPriceUpdated(callPriceForColor[_color]);
    }
     
    
    
    function _fillPaintsPool(uint _color) internal {

        
        uint nextPaintGen = currentPaintGenForColor[_color].add(1);
        //each 5 min we produce new paint generation
        if (now - paintGenToEndTimeForColor[_color][currentPaintGenForColor[_color] - 1] \u003e= 5 minutes) { 
            
            
            uint paintsRemain = paintGenToAmountForColor[_color][currentPaintGenForColor[_color]]; 
            
            //if 5 min passed and new gen not yet started     
            if (paintGenStartedForColor[_color][nextPaintGen] == false) {
                
                //we create new gen with amount of paints remaining 
                paintGenToAmountForColor[_color][nextPaintGen] = maxPaintsInPool.sub(paintsRemain); 
                
                
                paintGenToStartTimeForColor[_color][nextPaintGen] = now; 

                paintGenStartedForColor[_color][nextPaintGen] = true;
            }
            
            if (paintGenToAmountForColor[_color][currentPaintGenForColor[_color]] == 1) {
                
                
                _updateCallPrice(_color);
                
                //current gen paiints ends now 
                paintGenToEndTimeForColor[_color][currentPaintGenForColor[_color]] = now;
            }
               
            
            if (paintGenToAmountForColor[_color][currentPaintGenForColor[_color]] == 0) {
                
               
                callPriceForColor[_color] = nextCallPriceForColor[_color];

                if (paintGenToAmountForColor[_color][nextPaintGen] == 0) {
                    paintGenToAmountForColor[_color][nextPaintGen] = maxPaintsInPool;
                }
                //now we use next gen paints
                currentPaintGenForColor[_color] = nextPaintGen;
            }
        }
        ///if 5 min not yet passed
        else {

            if (paintGenToAmountForColor[_color][currentPaintGenForColor[_color]] == 0) {
               
                paintGenToAmountForColor[_color][nextPaintGen] = maxPaintsInPool;
                //we use next paint gen
                currentPaintGenForColor[_color] = nextPaintGen;
            }

        }
    }
}"},"Referral.sol":{"content":"pragma solidity 0.4.24;
import \"./SafeMath.sol\";
import \"./Modifiers.sol\";
import \"./Utils.sol\";

contract Referral is Modifiers {
    using SafeMath for uint;
    
    //ref link lenght 4-8 symbols 
    function buyRefLink(string _refLink) isValidRefLink (_refLink) external payable {
        require(msg.value == refLinkPrice, \"Setting referral link costs 0.1 ETH.\");
        require(hasRefLink[msg.sender] == false, \"You have already generated your ref link.\");
        bytes32 refLink32 = Utils.toBytes16(_refLink);
        require(refLinkExists[refLink32] != true, \"This referral link already exists, try different one.\");
        hasRefLink[msg.sender] = true;
        userToRefLink[msg.sender] = _refLink;
        refLinkExists[refLink32] = true;
        refLinkToUser[refLink32] = msg.sender;
        owner().transfer(msg.value);
    }

   
    function getReferralsForUser(address _user) external view returns (address[]) {
        return referrerToReferrals[_user];
    }

    function getReferralData(address _user) external view returns (uint registrationTime, uint moneySpent) {
        registrationTime = registrationTimeForUser[_user];
        moneySpent = moneySpentByUser[_user];
    }
    
}"},"Roles.sol":{"content":"pragma solidity 0.4.24;
import \"./Modifiers.sol\";

contract Roles is Modifiers {
    
    function addAdmin(address _new) external onlyOwner() {
        isAdmin[_new] = true;
    }
    
    function removeAdmin(address _admin) external onlyOwner() {
        isAdmin[_admin] = false;
    }

    function renounceAdmin() external onlyAdmin() {
        isAdmin[msg.sender] = false;
    }

}"},"Router.sol":{"content":"pragma solidity 0.4.24;
pragma experimental \"v0.5.0\";
import \"./Initializer.sol\";

contract Router is Initializer {
    
    event CommitMessage(string message);
    event FunctionUpdate(bytes4 indexed functionId, address indexed oldDelegate, address indexed newDelegate, string functionSignature);

    constructor(address _erc1538Delegate) public  {

        //Adding ERC1538 updateContract function
        bytes memory signature = \"updateContract(address,string,string)\";
        bytes4 funcId = bytes4(keccak256(signature));
        delegates[funcId] = _erc1538Delegate;
        funcSignatures.push(signature);
        funcSignatureToIndex[signature] = funcSignatures.length;
        emit FunctionUpdate(funcId, address(0), _erc1538Delegate, string(signature));
        emit CommitMessage(\"Added ERC1538 updateContract function at contract creation\");
    
        _initializer();
    }

    function() external payable {
        address delegate = delegates[msg.sig];
        require(delegate != address(0), \"Function does not exist.\");
        assembly {
            let ptr := mload(0x40)
            calldatacopy(ptr, 0, calldatasize)
            let result := delegatecall(gas, delegate, ptr, calldatasize, 0, 0)
            let size := returndatasize
            returndatacopy(ptr, 0, size)
            switch result
            case 0 {revert(ptr, size)}
            default {return (ptr, size)}
        }
    }
}"},"SafeMath.sol":{"content":"pragma solidity 0.4.24;

/**
 * @title SafeMath
 * @dev Math operations with safety checks that revert on error
 */
library SafeMath {

    /**
    * @dev Multiplies two numbers, reverts on overflow.
    */
    function mul(uint256 a, uint256 b) internal pure returns (uint256) {
        // Gas optimization: this is cheaper than requiring \u0027a\u0027 not being zero, but the
        // benefit is lost if \u0027b\u0027 is also tested.
        // See: https://github.com/OpenZeppelin/openzeppelin-solidity/pull/522
        if (a == 0) {
            return 0;
        }

        uint256 c = a * b;
        require(c / a == b);

        return c;
    }

    /**
    * @dev Integer division of two numbers truncating the quotient, reverts on division by zero.
    */
    function div(uint256 a, uint256 b) internal pure returns (uint256) {
        require(b \u003e 0); // Solidity only automatically asserts when dividing by 0
        uint256 c = a / b;
        // assert(a == b * c + a % b); // There is no case in which this doesn\u0027t hold

        return c;
    }

    /**
    * @dev Subtracts two numbers, reverts on overflow (i.e. if subtrahend is greater than minuend).
    */
    function sub(uint256 a, uint256 b) internal pure returns (uint256) {
        require(b \u003c= a);
        uint256 c = a - b;

        return c;
    }

    /**
    * @dev Adds two numbers, reverts on overflow.
    */
    function add(uint256 a, uint256 b) internal pure returns (uint256) {
        uint256 c = a + b;
        require(c \u003e= a);

        return c;
    }

    /**
    * @dev Divides two numbers and returns the remainder (unsigned integer modulo),
    * reverts when dividing by zero.
    */
    function mod(uint256 a, uint256 b) internal pure returns (uint256) {
        require(b != 0);
        return a % b;
    }
}"},"StorageV0.sol":{"content":"pragma solidity 0.4.24;
import \"./Ownable.sol\";

contract StorageV0 is Ownable {

    // maps functions to the delegate contracts that execute the functions
    // funcId =\u003e delegate contract
    mapping(bytes4 =\u003e address) internal delegates;

    // array of function signatures supported by the contract
    bytes[] internal funcSignatures;

    // maps each function signature to its position in the funcSignatures array.
    // signature =\u003e index+1
    mapping(bytes =\u003e uint256) internal funcSignatureToIndex;

}"},"StorageV1.sol":{"content":"pragma solidity 0.4.24;
import \"./StorageV0.sol\";
import \"./IColor.sol\";
import \"./IPixel.sol\";

contract StorageV1 is StorageV0 {

    //pixel color(round=\u003e pixel=\u003e color)
    mapping (uint =\u003e mapping (uint =\u003e uint)) public pixelToColorForRound; 

    //old pixel color(round=\u003e pixel=\u003e color)
    mapping (uint =\u003e mapping (uint =\u003e uint)) public pixelToOldColorForRound; 

    // (round =\u003e color =\u003e pixel amount)
    mapping (uint =\u003e mapping (uint =\u003e uint)) public colorToPaintedPixelsAmountForRound; 

    //color bank for round (round =\u003e color bank)
    mapping (uint =\u003e uint) public colorBankForRound; 

    //color bank for  color for round (round =\u003e color =\u003e color bank)
    mapping (uint =\u003e mapping (uint =\u003e uint)) public colorBankToColorForRound; 

    //time bank for round (round =\u003e time bank)
    mapping (uint =\u003e uint) public timeBankForRound; 
    
    // (round =\u003e timestamp)
    mapping (uint =\u003e uint) public lastPaintTimeForRound; 

    // (round =\u003e adress)
    mapping (uint =\u003e address) public lastPainterForRound; 

    
    mapping (uint =\u003e uint) public lastPaintedPixelForRound;

    // (round =\u003e color) 
    mapping (uint =\u003e uint) public winnerColorForRound; 

    // (round =\u003e color =\u003e paints amount)
    mapping (uint =\u003e mapping (uint =\u003e uint)) public colorToTotalPaintsForCBIteration; 

    // (round =\u003e adress)
    mapping (uint =\u003e address) public winnerOfRound; 

    //bank drawn in round (round =\u003e drawn bank) (1 = time bank, 2 = color bank)
    mapping (uint =\u003e uint) public winnerBankForRound; 

    
    mapping (uint =\u003e mapping (uint =\u003e uint)) public pixelToPaintTimeForRound;

   
    mapping (uint =\u003e uint) public totalPaintsForRound;
        
    
    mapping (uint =\u003e mapping (uint =\u003e uint)) public paintGenToAmountForColor;
    
    
    mapping (uint =\u003e mapping (uint =\u003e uint)) public paintGenToStartTimeForColor;
    
    
    mapping (uint =\u003e mapping (uint =\u003e uint)) public paintGenToEndTimeForColor;
    
    //bool 
    mapping (uint =\u003e mapping (uint =\u003e bool)) public paintGenStartedForColor;

   
    mapping (uint =\u003e uint) public currentPaintGenForColor;
    
   
    mapping (uint =\u003e uint) public callPriceForColor;
    
 
    mapping (uint =\u003e uint) public nextCallPriceForColor;
    
    
    mapping (uint =\u003e mapping (address =\u003e uint)) public moneySpentByUserForColor;

    
    mapping (address =\u003e uint) public moneySpentByUser;
    
    
    mapping (uint =\u003e mapping (address =\u003e bool)) public hasPaintDiscountForColor;
    
    //in percent 
    mapping (uint =\u003e mapping (address =\u003e uint)) public usersPaintDiscountForColor;

     
    mapping (address =\u003e bool) public isRegisteredUser;
    
    
    mapping (address =\u003e bool) public hasRefLink;

   
    mapping (address =\u003e address) public referralToReferrer;

    
    mapping (address =\u003e address[]) public referrerToReferrals;
    
   
    mapping (address =\u003e bool) public hasReferrer;
    
    
    mapping (address =\u003e string) public userToRefLink;
    

    mapping (bytes32 =\u003e address) public refLinkToUser;
    
    
    mapping (bytes32 =\u003e bool) public refLinkExists;
    
   
    mapping (address =\u003e uint) public newUserToCounter;
    
   
    uint public uniqueUsersCount;

   
    uint public maxPaintsInPool;

  
    uint public currentRound;

    //time bank iteration
    uint public tbIteration;

   //color bank iteration
    uint public cbIteration;

    
    uint public paintsCounter; 

    //Time Bank Iteration =\u003e Painter =\u003e Painter\u0027s Share in Time Team
    mapping (uint =\u003e mapping (address =\u003e uint)) public timeBankShare;

    //Color Bank Iteration =\u003e Color =\u003e Painter =\u003e Painter\u0027s Share in Time Team
    mapping (uint =\u003e mapping (uint =\u003e mapping (address =\u003e uint))) public colorBankShare;

   
    mapping (uint =\u003e uint) public paintsCounterForColor; 

    //cbIteration =\u003e color team
    mapping (uint =\u003e address[]) public cbTeam; 

    //tbIteration =\u003e color team 
    mapping (uint =\u003e address[]) public tbTeam;

     //counter =\u003e user
    mapping (uint =\u003e address) public counterToPainter;

    //color =\u003e counter =\u003e user    
    mapping (uint =\u003e mapping (uint =\u003e address)) public counterToPainterForColor; 

    //cbIteration =\u003e user !should not be public
    mapping (uint =\u003e mapping (address =\u003e bool)) public isInCBT; 

    //tbIteration =\u003e user !should not be public
    mapping (uint =\u003e mapping (address =\u003e bool)) public isInTBT; 

    //cbIteration =\u003e painter =\u003e color bank prize
    mapping (uint =\u003e mapping (address =\u003e uint)) public painterToCBP; 

    //tbIteration =\u003e painter =\u003e time bank prize
    mapping (uint =\u003e mapping (address =\u003e uint)) public painterToTBP; 

    //сbIteration =\u003e  bool
    mapping (uint =\u003e bool) public isCBPTransfered;

    //tbIteration =\u003e bool
    mapping (uint =\u003e bool) public isTBPTransfered;

    
    mapping (address =\u003e uint) public lastPlayedRound;
    
    //Dividends Distribution
    mapping (uint =\u003e address) public ownerOfColor;

    mapping (address =\u003e uint) public pendingWithdrawals; 
    
    // (adress =\u003e time)
    mapping (address =\u003e uint) public addressToLastWithdrawalTime; 
    
  
    uint public dividendsBank;

    struct Claim {
        uint id;
        address claimer;
        bool isResolved;
        uint timestamp;
    }

    uint public claimId;

    Claim[] public claims;

    
    address public ownerOfPixel = 0xca35b7d915458ef540ade6068dfe2f44e8fa733c;
    address public founders =0xe04f921cf3d6c882C0FAa79d0810a50B1101e2D4;

    bool public isGamePaused;
    
    bool public isCBPDistributable;
    bool public isTBPDistributable;
    
    mapping(address =\u003e bool) public isAdmin;

    Color public colorInstance;
    Pixel public pixelInstance;

    uint public totalColorsNumber; // 8
    uint public totalPixelsNumber; //225 in V1

   
    uint public refLinkPrice; 

   
    mapping (address =\u003e uint) public registrationTimeForUser;

    mapping (address =\u003e uint) public lastPaintTimeOfUser;
    mapping (uint =\u003e mapping (address =\u003e uint)) public lastPaintTimeOfUserForColor;

    mapping (uint =\u003e bool) public timeBankDrawnForRound;

    
    mapping (uint =\u003e uint) public usersCounterForRound;
 
    mapping (uint =\u003e mapping (address =\u003e bool)) public isUserCountedForRound;

   // Events

    event CBPDistributed(uint indexed round, uint indexed cbIteration, address indexed winner, uint prize);
    event DividendsWithdrawn(address indexed withdrawer, uint indexed claimId, uint indexed amount);
    event DividendsClaimed(address indexed claimer, uint indexed claimId, uint indexed currentTime);
    event Paint(uint indexed pixelId, uint colorId, address indexed painter, uint indexed round, uint timestamp);
    event ColorBankPlayed(address winnerOfRound, uint indexed round);
    event TimeBankPlayed(address winnerOfRound, uint indexed round);
    event CallPriceUpdated(uint indexed newCallPrice);
    event TBPDistributed(uint indexed round, uint indexed tbIteration, address indexed winner, uint prize);
    event EtherWithdrawn(uint balance, uint colorBank, uint timeBank, uint timestamp);
}"},"TimeTeam.sol":{"content":"pragma solidity 0.4.24;
import \"./SafeMath.sol\";
import \"./Modifiers.sol\";

contract TimeTeam is Modifiers {
    using SafeMath for uint;

    //last 100 painters form a time team
    function formTimeTeam() private returns (uint) {
    uint paintsCurrentRound = paintsCounter; 
  \t  if (paintsCurrentRound\u003e1000)
        {
            paintsCurrentRound = 1000;
        }
        
        for (uint i = paintsCurrentRound; i \u003e 0; i--) {
            uint teamMembersCounter;
            if (isInTBT[tbIteration][counterToPainter[i]] == false) {
                
                  if (teamMembersCounter \u003e= paintsCurrentRound)
                        break;
              
                else {
                    if (teamMembersCounter \u003e= 50)   
                        break;
                }
                
                
                tbTeam[tbIteration].push(counterToPainter[i]);
                teamMembersCounter = teamMembersCounter.add(1);
                isInTBT[tbIteration][counterToPainter[i]] = true;
            }
        }
        return tbTeam[tbIteration].length;
    }
    
    function calculateTBP() private {

        uint length = formTimeTeam();
        address painter;
        uint totalPaintsForTeam; 

        for (uint i = 0; i \u003c length; i++) {
            painter = tbTeam[tbIteration][i];
            totalPaintsForTeam += timeBankShare[tbIteration][painter];
        }

        for (i = 0; i \u003c length; i++) {
            painter = tbTeam[tbIteration][i];
            painterToTBP[tbIteration][painter] = (timeBankShare[tbIteration][painter].mul(timeBankForRound[currentRound])).div(totalPaintsForTeam);
        }

    }
    function resetPaintsPool() internal {
        
    
    for (uint i = 1; i \u003c= totalColorsNumber; i++){
      
        callPriceForColor[i] = 0.005 ether;
        nextCallPriceForColor[i] = callPriceForColor[i];
        currentPaintGenForColor[i]= 1;
        
        paintGenToAmountForColor[i][currentPaintGenForColor[i]] = maxPaintsInPool;
        paintGenStartedForColor[i][currentPaintGenForColor[i]] = true;
        paintGenToStartTimeForColor[i][currentPaintGenForColor[i]] = now;
    }
    
    }

    function distributeTBP() external canDistributeTBP() {
        require(isTBPTransfered[tbIteration] == false, \"Time Bank Prizes already transferred for this tbIteration\");
        address painter;
        calculateTBP();
        painterToTBP[tbIteration][winnerOfRound[currentRound]] += timeBankForRound[currentRound];
        uint length = tbTeam[tbIteration].length;
        for (uint i = 0; i \u003c length; i++) {
            painter = tbTeam[tbIteration][i];
            if (painterToTBP[tbIteration][painter] != 0) {
                uint prize = painterToTBP[tbIteration][painter];
                painter.transfer(prize);
                emit TBPDistributed(currentRound, tbIteration, painter, prize);
            }
        }
        isTBPDistributable = false;
        isTBPTransfered[tbIteration] = true;
        resetPaintsPool();
        currentRound = currentRound.add(1); 
        tbIteration = tbIteration.add(1); 
        isGamePaused = false;
    }
}"},"Utils.sol":{"content":"pragma solidity 0.4.24;

library Utils {
    
    // convert a string less than 32 characters long to bytes32
    function toBytes16(string _string) pure internal returns (bytes16) {
        // make sure that the string isn\u0027t too long for this function
        // will work but will cut off the any characters past the 32nd character
        bytes16 _stringBytes;
        string memory str = _string;
    
        // simplest way to convert 32 character long string
        assembly {
          // load the memory pointer of string with an offset of 32
          // 32 passes over non-core data parts of string such as length of text
          _stringBytes := mload(add(str, 32))
        }
        return _stringBytes;
    }

    
    
}"},"Wrapper.sol":{"content":"pragma solidity 0.4.24;
import \"./Modifiers.sol\";

/**
** Wrapper for Router Contract to interact with all the functions\u0027 signatures
**/

contract Wrapper is Modifiers {

    //ColorTeam.sol
    function distributeCBP() external {}

    //TimeTeam.sol
    function distributeTBP() external {}

    //DividendsDistributor.sol
    function claimDividends() external {}
    function approveClaim(uint _claimId) public {}

    //GameStateController.sol
    function pauseGame() external {}
    function resumeGame() external {}
    function withdrawEther() external returns (bool) {}

    //Referral.sol
    function buyRefLink(string _refLink) external payable {}
    function getReferralsForUser(address _user) external view returns (address[]) {}
    function getReferralData(address _user) external view returns (uint registrationTime, uint moneySpent) {}

    //Roles.sol
    function addAdmin(address _new) external {}
    function removeAdmin(address _admin) external {}
    function renounceAdmin() external {}

    //Game.sol
    function estimateCallPrice(uint[] _pixels, uint _color) public view returns (uint totalCallPrice) {}
    function paint(uint[] _pixels, uint _color, string _refLink) external payable {}
    function drawTimeBank() public {}

    //ERC1538.sol
    function updateContract(address _delegate, string _functionSignatures, string commitMessage) external {}

    //GameMock.sol
    function mock() external {}
    function mock2() external {}
    function mock3(uint _winnerColor) external {}
    function mockMaxPaintsInPool() external {}

    //Helpers.sol
    function getPixelColor(uint _pixel) external view returns (uint) {}
    function addNewColor() external {}

}
