// SPDX-License-Identifier: MIT

pragma solidity 0.7.0;

contract Context {
    function _msgSender() internal view returns (address payable) {
        return msg.sender;
    }

    function _msgData() internal view virtual returns (bytes memory) {
        this; // silence state mutability warning without generating bytecode - see https://github.com/ethereum/solidity/issues/2691
        return msg.data;
    }
}
"},"ERC20.sol":{"content":"// SPDX-License-Identifier: MIT
import \"./SafeMath.sol\";
import \"./IERC20.sol\";
import \"./Context.sol\";

pragma solidity 0.7.0;

contract ERC20 is Context, IERC20 {
    using SafeMath for uint;

    mapping (address =\u003e uint) private _balances;
    mapping (address =\u003e mapping (address =\u003e uint)) private _allowances;

    uint private _totalSupply;
    string private _name;
    string private _symbol;
    uint8 private _decimals;

    //Only create tokens on initial constructor call
    constructor (string memory name, string memory symbol, uint256 totalSupply) public {
        _name = name;
        _symbol = symbol;
        _decimals = 18;

        _totalSupply = _totalSupply.add(totalSupply * 10**_decimals);
        _balances[_msgSender()] = _balances[_msgSender()].add(_totalSupply);
        emit Transfer(address(0x0), _msgSender(), _totalSupply);
    }

    function totalSupply() public override view returns (uint) {
        return _totalSupply;
    }

    function name() public view returns (string memory) {
        return _name;
    }
    
    function symbol() public view returns (string memory) {
        return _symbol;
    }
    
    function decimals() public view returns (uint8) {
        return _decimals;
    }
    
    function balanceOf(address account) public view override returns (uint256) {
        return _balances[account];
    }

    function transfer(address recipient, uint256 amount) public virtual override returns (bool) {
        _transfer(_msgSender(), recipient, amount);
        return true;
    }
    
    function allowance(address owner, address spender) public view virtual override returns (uint256) {
        return _allowances[owner][spender];
    }
    
    function approve(address spender, uint256 amount) public virtual override returns (bool) {
        _approve(_msgSender(), spender, amount);
        return true;
    }
    
    function transferFrom(address sender, address recipient, uint256 amount) public virtual override returns (bool) {
        _transfer(sender, recipient, amount);
        _approve(sender, _msgSender(), _allowances[sender][_msgSender()].sub(amount, \"ERC20: transfer amount exceeds allowance\"));
        return true;
    }
    
    function increaseAllowance(address spender, uint256 addedValue) public virtual returns (bool) {
        _approve(_msgSender(), spender, _allowances[_msgSender()][spender].add(addedValue));
        return true;
    }
    
    function decreaseAllowance(address spender, uint256 subtractedValue) public virtual returns (bool) {
        _approve(_msgSender(), spender, _allowances[_msgSender()][spender].sub(subtractedValue, \"ERC20: decreased allowance below zero\"));
        return true;
    }
    
    function _transfer(address sender, address recipient, uint256 amount) internal virtual {
        require(sender != address(0), \"ERC20: transfer from the zero address\");
        require(recipient != address(0), \"ERC20: transfer to the zero address\");        

        _balances[sender] = _balances[sender].sub(amount, \"ERC20: transfer amount exceeds balance\");
        _balances[recipient] = _balances[recipient].add(amount);
        emit Transfer(sender, recipient, amount);
    } 
    
    function _burn(address account, uint256 amount) internal virtual {
        require(account != address(0), \"ERC20: burn from the zero address\");        

        _balances[account] = _balances[account].sub(amount, \"ERC20: burn amount exceeds balance\");
        _totalSupply = _totalSupply.sub(amount);
        emit Transfer(account, address(0), amount);
    }
    
    function _approve(address owner, address spender, uint256 amount) internal virtual {
        require(owner != address(0), \"ERC20: approve from the zero address\");
        require(spender != address(0), \"ERC20: approve to the zero address\");

        _allowances[owner][spender] = amount;
        emit Approval(owner, spender, amount);
    }
}"},"IERC20.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity 0.7.0;

interface IERC20 { 
    function totalSupply() external view returns (uint256);
    function balanceOf(address account) external view returns (uint256);
    function transfer(address recipient, uint256 amount) external returns (bool);
    function allowance(address owner, address spender) external view returns (uint256);
    function approve(address spender, uint256 amount) external returns (bool);
    function transferFrom(address sender, address recipient, uint256 amount) external returns (bool);
    
    event Transfer(address indexed from, address indexed to, uint256 value);
    event Approval(address indexed owner, address indexed spender, uint256 value);
}"},"SafeMath.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity 0.7.0;

library SafeMath {
    
    function add(uint256 a, uint256 b) internal pure returns (uint256) {
        uint256 c = a + b;
        require(c \u003e= a, \"SafeMath: addition overflow\");

        return c;
    }
    
    function sub(uint256 a, uint256 b) internal pure returns (uint256) {
        return sub(a, b, \"SafeMath: subtraction overflow\");
    }
   
    function sub(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b \u003c= a, errorMessage);
        uint256 c = a - b;

        return c;
    }
    
    function mul(uint256 a, uint256 b) internal pure returns (uint256) {
        // Gas optimization: this is cheaper than requiring \u0027a\u0027 not being zero, but the
        // benefit is lost if \u0027b\u0027 is also tested.
        // See: https://github.com/OpenZeppelin/openzeppelin-contracts/pull/522
        if (a == 0) {
            return 0;
        }

        uint256 c = a * b;
        require(c / a == b, \"SafeMath: multiplication overflow\");

        return c;
    }
    
    function div(uint256 a, uint256 b) internal pure returns (uint256) {
        return div(a, b, \"SafeMath: division by zero\");
    }
    
    function div(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b \u003e 0, errorMessage);
        uint256 c = a / b;
        // assert(a == b * c + a % b); // There is no case in which this doesn\u0027t hold

        return c;
    }
    
    function mod(uint256 a, uint256 b) internal pure returns (uint256) {
        return mod(a, b, \"SafeMath: modulo by zero\");
    }
    
    function mod(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b != 0, errorMessage);
        return a % b;
    }
}"},"YSEC.sol":{"content":"/*
__/\\\\\\________/\\\\\\_____/\\\\\\\\\\\\\\\\\\\\\\____/\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\________/\\\\\\\\\\\\\\\\\\_        
 _\\///\\\\\\____/\\\\\\/____/\\\\\\/////////\\\\\\_\\/\\\\\\///////////______/\\\\\\////////__       
  ___\\///\\\\\\/\\\\\\/_____\\//\\\\\\______\\///__\\/\\\\\\_______________/\\\\\\/___________      
   _____\\///\\\\\\/________\\////\\\\\\_________\\/\\\\\\\\\\\\\\\\\\\\\\______/\\\\\\_____________     
    _______\\/\\\\\\____________\\////\\\\\\______\\/\\\\\\///////______\\/\\\\\\_____________    
     _______\\/\\\\\\_______________\\////\\\\\\___\\/\\\\\\_____________\\//\\\\\\____________   
      _______\\/\\\\\\________/\\\\\\______\\//\\\\\\__\\/\\\\\\______________\\///\\\\\\__________  
       _______\\/\\\\\\_______\\///\\\\\\\\\\\\\\\\\\\\\\/___\\/\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\____\\////\\\\\\\\\\\\\\\\\\_ 
        _______\\///__________\\///////////_____\\///////////////________\\/////////__

Visit and follow!

* Website:  https://www.ysec.finance
* Twitter:  https://twitter.com/YearnSecure
* Telegram: https://t.me/YearnSecure
* Medium:   https://medium.com/@yearnsecure

*/

// SPDX-License-Identifier: MIT
import \"./ERC20.sol\";

pragma solidity 0.7.0;

contract YSEC is ERC20{
    using SafeMath for uint;

    address public Governance;

    constructor () ERC20(\"YearnSecure\", \"YSEC\", 1000000) {
        Governance = msg.sender;
    }

    function burn(uint256 amount) external {
        require(msg.sender == Governance, \"Caller does not have governance\");
        _burn(msg.sender, amount);
    }

    function burnGovernance() external{
        require(msg.sender == Governance, \"Caller does not have governance\");
        Governance = address(0x0);
    }
}

