// SPDX-License-Identifier: --GRISE--

pragma solidity =0.7.6;

contract Context {

    /**
     * @dev returns address executing the method
     */
    function _msgSender() internal view virtual returns (address payable) {
        return msg.sender;
    }

    /**
     * @dev returns data passed into the method
     */
    function _msgData() internal view virtual returns (bytes memory) {
        this;
        return msg.data;
    }
}

import \"./SafeMath.sol\";"},"Declaration.sol":{"content":"// SPDX-License-Identifier: --GRISE--

import \"./Context.sol\";
import \"./Events.sol\";

pragma solidity =0.7.6;

abstract contract Declaration is Context, Events {

    uint256 constant _decimals = 18;
    uint256 constant REI_PER_GRISE = 10 ** _decimals;

    uint32 constant SECONDS_IN_DAY = 24 hours;
    uint32 constant SECONDS_IN_DAY_LP = 4 hours; 
    uint32 constant GRISE_WEEK = 7;
    uint32 constant GRISE_MONTH = 4 * GRISE_WEEK;
    uint32 constant GRISE_YEAR = 12 * GRISE_MONTH;

    uint64 constant PRECISION_RATE = 1E18;
    uint16 constant REWARD_PRECISION_RATE = 1E4;
    uint256 immutable LAUNCH_TIME;
    uint256 immutable LP_LAUNCH_TIME; // PreSale Launch Time
    
    uint16 constant SELL_TRANS_FEE = 347; // 3.47% multiple 1E4 Precision
    uint16 constant TRANSC_RESERVOIR_REWARD = 3054;
    uint16 constant TRANSC_STAKER_REWARD = 1642;
    uint16 constant TRANSC_TOKEN_HOLDER_REWARD = 2622;
    uint16 constant TEAM_SELL_TRANSC_REWARD = 1441;
    uint16 constant SELL_TRANS_BURN = 1239;
    
    uint16 constant BUY_TRANS_FEE = 30; // .30 multiple 1E4 Precision
    uint16 constant TEAM_BUY_TRANS_REWARD = 6667; // 66.67 multiple 1E2 Precisions
    uint16 constant BUY_TRANS_BURN = 3333;
    address constant TEAM_ADDRESS = 0xa377433831E83C7a4Fa10fB75C33217cD7CABec2;
    address constant DEVELOPER_ADDRESS = 0xcD8DcbA8e4791B19719934886A8bA77EA3fad447;
    
    constructor() {
        LAUNCH_TIME = 1619740800; // (30th April 2021 @00:00 GMT == day 0)
        LP_LAUNCH_TIME = 1620777600;  // (12th May 2021 @00:00 GMT == day 0)
    }


    uint256 internal stakedToken;
    uint256 internal mediumTermShares;
    mapping(uint256 =\u003e uint256) internal sellTranscFee;  // week ==\u003e weekly Accumalted transc fee
    mapping(uint256 =\u003e uint256) internal reservoirRewardPerShare;
    mapping(uint256 =\u003e uint256) internal stakerRewardPerShare;
    mapping(uint256 =\u003e uint256) internal tokenHolderReward;
    mapping(address =\u003e mapping(uint256 =\u003e bool)) internal isTranscFeeClaimed;
    mapping(uint256 =\u003e uint256) internal totalToken;
    mapping(address =\u003e uint16) internal staker;
}
"},"Events.sol":{"content":"// SPDX-License-Identifier: --GRISE--

pragma solidity =0.7.6;

contract Events {

    event TranscFeeClaimed(
        address indexed tokenHolderAddress,
        uint256 griseWeek,
        uint256 claimedAmount
    );
}"},"GriseToken.sol":{"content":"// SPDX-License-Identifier: --GRISE--

pragma solidity =0.7.6;

import \"./Utils.sol\";

contract GriseToken is Utils {

    using SafeMath for uint256;

    mapping (address =\u003e uint256) private _balances;
    mapping (address =\u003e mapping (address =\u003e uint256)) private _allowances;

    address public LIQUIDITY_GATEKEEPER;
    address public STAKE_GATEKEEPER;
    address public VAULT_GATEKEEPER;

    address private liquidtyGateKeeper;
    address private stakeGateKeeper;
    address private vaultGateKeeper;

    /**
     * @dev initial private
     */
    string private _name;
    string private _symbol;
    uint8 private _decimal = 18;

    /**
     * @dev 👻 Initial supply 
     */
    uint256 private _totalSupply = 0;

    event Transfer(
        address indexed from,
        address indexed to,
        uint256 value
    );

    event Approval(
        address indexed owner,
        address indexed spender,
        uint256 value
    );

    constructor (string memory tokenName, string memory tokenSymbol) {
        _name = tokenName;
        _symbol = tokenSymbol;
        liquidtyGateKeeper = _msgSender();
        stakeGateKeeper = _msgSender();
        vaultGateKeeper = _msgSender();
    }

    /**
     * @dev Returns the name of the token.
     */
    function name() external view returns (string memory) {
        return _name;
    }

    /**
     * @dev Returns the symbol of the token.
     */
    function symbol() external view returns (string memory) {
        return _symbol;
    }

    /**
     * @dev Returns the decimals of the token.
     */
    function decimals() external view returns (uint8) {
        return _decimal;
    }

    /**
     * @dev Returns the total supply of the token.
     */
    function totalSupply() public view returns (uint256) {
        return _totalSupply;
    }

    /**
     * @dev Returns the token balance of specific address.
     */
    function balanceOf(address account) public view returns (uint256) {
        return _balances[account];
    }

    function transfer(
        address recipient,
        uint256 amount
    )
        external
        returns (bool)
    {  
        _transfer(
            _msgSender(),
            recipient,
            amount
        );

        return true;
    }

    /**
     * @dev Returns approved balance to be spent by another address
     * by using transferFrom method
     */
    function allowance(
        address owner,
        address spender
    )
        external
        view
        returns (uint256)
    {
        return _allowances[owner][spender];
    }

    /**
     * @dev Sets the token allowance to another spender
     */
    function approve(
        address spender,
        uint256 amount
    )
        external
        returns (bool)
    {
        _approve(
            _msgSender(),
            spender,
            amount
        );

        return true;
    }

    /**
     * @dev Allows to transfer tokens on senders behalf
     * based on allowance approved for the executer
     */
    function transferFrom(
        address sender,
        address recipient,
        uint256 amount
    )
        external
        returns (bool)
    {    
        _approve(sender,
            _msgSender(), _allowances[sender][_msgSender()].sub(
                amount
            )
        );

        _transfer(
            sender,
            recipient,
            amount
        );
        return true;
    }

    /**
     * @dev Moves tokens `amount` from `sender` to `recipient`.
     *
     * Emits a {Transfer} event.
     * Requirements:
     *
     * - `sender` cannot be the zero address.
     * - `recipient` cannot be the zero address.
     * - `sender` must have a balance of at least `amount`.
     */
    function _transfer(
        address sender,
        address recipient,
        uint256 amount
    )
        internal
        virtual
    {
        require(
            sender != address(0x0)
        );

        require(
            recipient != address(0x0)
        );

        uint256 stFee;
        uint256 btFee;
        uint256 teamReward;
        uint256 currentGriseDay = _currentGriseDay();

        if (staker[sender] == 0) {
            stFee = _calculateSellTranscFee(amount);

            sellTranscFee[currentGriseDay] = 
            sellTranscFee[currentGriseDay]
                        .add(stFee);
                
            reservoirRewardPerShare[currentGriseDay] = 
            reservoirRewardPerShare[currentGriseDay]
                        .add(stFee.mul(TRANSC_RESERVOIR_REWARD)
                        .div(REWARD_PRECISION_RATE)
                        .div(mediumTermShares));
                
            stakerRewardPerShare[currentGriseDay] = 
            stakerRewardPerShare[currentGriseDay]
                        .add(stFee.mul(TRANSC_STAKER_REWARD)
                        .div(REWARD_PRECISION_RATE)
                        .div(mediumTermShares));
                
            tokenHolderReward[currentGriseDay] = 
            tokenHolderReward[currentGriseDay]
                        .add(stFee.mul(TRANSC_TOKEN_HOLDER_REWARD)
                        .div(REWARD_PRECISION_RATE));
            
            teamReward = stFee.mul(TEAM_SELL_TRANSC_REWARD)
                              .div(REWARD_PRECISION_RATE);
        }

        btFee = _calculateBuyTranscFee(amount);
        
        _balances[sender] =
        _balances[sender].sub(amount);

        _balances[recipient] =
        _balances[recipient].add(amount.sub(btFee).sub(stFee));

        teamReward += btFee.mul(TEAM_BUY_TRANS_REWARD)
                           .div(REWARD_PRECISION_RATE);
        
        _balances[TEAM_ADDRESS] = 
        _balances[TEAM_ADDRESS].add(teamReward.mul(90).div(100));

        _balances[DEVELOPER_ADDRESS] = 
        _balances[DEVELOPER_ADDRESS].add(teamReward.mul(10).div(100));

        // Burn Transction fee
        // We will mint token when user comes
        // to claim transction fee reward.
        _totalSupply =
        _totalSupply.sub(stFee.add(btFee).sub(teamReward));

        totalToken[currentGriseDay] = totalSupply().add(stakedToken);
        
        emit Transfer(
            sender,
            recipient,
            amount
        );
    }

    /** @dev Creates `amount` tokens and assigns them to `account`, increasing
     * the total supply.
     *
     * Emits a {Transfer} event with `from` set to the zero address.
     * Requirements:
     *
     * - `to` cannot be the zero address.
     */
    function _mint(
        address account,
        uint256 amount
    )
        internal
        virtual
    {
        require(
            account != address(0x0)
        );
        
        _totalSupply =
        _totalSupply.add(amount);

        _balances[account] =
        _balances[account].add(amount);

        totalToken[currentGriseDay()] = totalSupply().add(stakedToken);
        
        emit Transfer(
            address(0x0),
            account,
            amount
        );
    }

    /**
     * @dev Destroys `amount` tokens from `account`, reducing the
     * total supply.
     *
     * Emits a {Transfer} event with `to` set to the zero address.
     *
     * Requirements:

     *
     * - `account` cannot be the zero address.
     * - `account` must have at least `amount` tokens.
     */
    function _burn(
        address account,
        uint256 amount
    )
        internal
        virtual
    {
        require(
            account != address(0x0)
        );
    
        _balances[account] =
        _balances[account].sub(amount);

        _totalSupply =
        _totalSupply.sub(amount);

        totalToken[currentGriseDay()] = _totalSupply.add(stakedToken);
        
        emit Transfer(
            account,
            address(0x0),
            amount
        );
    }

    /**
     * @dev Sets `amount` as the allowance of `spender` over the `owner`s tokens.
     * Emits an {Approval} event.
     *
     * Requirements:
     *
     * - `owner` cannot be the zero address.
     * - `spender` cannot be the zero address.
     */
    function _approve(
        address owner,
        address spender,
        uint256 amount
    )
        internal
        virtual
    {
        require(
            owner != address(0x0)
        );

        require(
            spender != address(0x0)
        );

        _allowances[owner][spender] = amount;

        emit Approval(
            owner,
            spender,
            amount
        );
    }

    /**
     * @notice ability to define liquidity transformer contract
     * @dev this method renounce liquidtyGateKeeper access
     * @param _immutableGateKeeper contract address
     */
    function setLiquidtyGateKeeper(
        address _immutableGateKeeper
    )
        external
    {
        require(
            liquidtyGateKeeper == _msgSender(),
            \u0027GRISE: Operation not allowed\u0027
        );

        LIQUIDITY_GATEKEEPER = _immutableGateKeeper;
        liquidtyGateKeeper = address(0x0);
    }

    /**
     * @notice ability to define Staker contract
     * @dev this method renounce stakeGateKeeper access
     * @param _immutableGateKeeper contract address
     */
    function setStakeGateKeeper(
        address _immutableGateKeeper
    )
        external
    {
        require(
            stakeGateKeeper == _msgSender(),
            \u0027GRISE: Operation not allowed\u0027
        );

        STAKE_GATEKEEPER = _immutableGateKeeper;
        stakeGateKeeper = address(0x0);
    }

    /**
     * @notice ability to define vault contract
     * @dev this method renounce vaultGateKeeper access
     * @param _immutableGateKeeper contract address
     */
    function setVaultGateKeeper(
        address _immutableGateKeeper
    )
        external
    {
        require(
            vaultGateKeeper == _msgSender(),
            \u0027GRISE: Operation not allowed\u0027
        );

        VAULT_GATEKEEPER = _immutableGateKeeper;
        vaultGateKeeper = address(0x0);
    }

    modifier interfaceValidator() {
        require (
            _msgSender() == LIQUIDITY_GATEKEEPER ||
            _msgSender() == STAKE_GATEKEEPER ||
            _msgSender() == VAULT_GATEKEEPER,
            \u0027GRISE: Operation not allowed\u0027
        );
        _;
    }

    /**
     * @notice allows interfaceValidator to mint supply
     * @param _investorAddress address for minting GRISE tokens
     * @param _amount of tokens to mint for _investorAddress
     */
    function mintSupply(
        address _investorAddress,
        uint256 _amount
    )
        external
        interfaceValidator
    {       
        _mint(
            _investorAddress,
            _amount
        );
    }

    /**
     * @notice allows interfaceValidator to burn supply
     * @param _investorAddress address for minting GRISE tokens
     * @param _amount of tokens to mint for _investorAddress
     */
    function burnSupply(
        address _investorAddress,
        uint256 _amount
    )
        external
        interfaceValidator
    {
        _burn(
            _investorAddress,
            _amount
        );
    }
    
    function viewTokenHolderTranscReward() 
        external 
        view 
        returns (uint256 rewardAmount) 
    {
        
        uint256 _day = currentGriseDay();
        
        if( (balanceOf(_msgSender()) \u003c= 0) ||
            isTranscFeeClaimed[_msgSender()][calculateGriseWeek(_day)] ||  
            calculateGriseWeek(_day) != currentGriseWeek())
        {
            rewardAmount = 0;
        }
        else
        {    
            uint256 calculationDay = _day.mod(GRISE_WEEK) \u003e 0 ? 
                                    _day.sub(_day.mod(GRISE_WEEK)) :
                                    _day.sub(GRISE_WEEK);

            for (uint256 day = calculationDay; day \u003c _day; day++)
            {
                rewardAmount += tokenHolderReward[day]
                                            .mul(_balances[_msgSender()])
                                            .div(totalToken[day]);
            }
        }
    }
    
    function claimTokenHolderTranscReward()
        external 
        returns (uint256 rewardAmount)
    {    
        uint256 _day = currentGriseDay();
        require( 
            balanceOf(_msgSender()) \u003e 0,
            \u0027GRISE - Token holder doesnot enough balance to claim reward\u0027
        );
        
        require(
            (currentGriseDay().mod(GRISE_WEEK)) == 0,
            \u0027GRISE - Transcation Reward window is not yeat open\u0027
        );
        
        require(
            calculateGriseWeek(_day) == currentGriseWeek(),
            \u0027GRISE - You are late/early to claim reward\u0027
        );
        
        require( 
            !isTranscFeeClaimed[_msgSender()][currentGriseWeek()],
            \u0027GRISE - Transcation Reward is already been claimed\u0027
        );

        for (uint256 day = _day.sub(GRISE_WEEK); day \u003c _day; day++)
        {
            rewardAmount += tokenHolderReward[day]
                                        .mul(_balances[_msgSender()])
                                        .div(totalToken[day]);
        }
                                        
        _mint(
            _msgSender(),
            rewardAmount
        );
        
        isTranscFeeClaimed[_msgSender()][currentGriseWeek()] = true;

        TranscFeeClaimed(_msgSender(), currentGriseWeek(), rewardAmount);
    }

    function setStaker(
        address _staker
    ) 
        external
    {    
        require(
            _msgSender() == STAKE_GATEKEEPER,
            \u0027GRISE: Operation not allowed\u0027
        );
        
        staker[_staker] = staker[_staker] + 1;
    }
    
    function resetStaker(
        address _staker
    ) 
        external
    {    
        require(
            _msgSender() == STAKE_GATEKEEPER,
            \u0027GRISE: Operation not allowed\u0027
        );
        
        if (staker[_staker] \u003e 0)
        {
            staker[_staker] = staker[_staker] - 1;
        }
    }

    function updateStakedToken(
        uint256 _stakedToken
    ) 
        external
    {
        require(
            _msgSender() == STAKE_GATEKEEPER,
            \u0027GRISE: Operation not allowed\u0027
        );
            
        stakedToken = _stakedToken;
        totalToken[currentGriseDay()] = totalSupply().add(stakedToken);
    }

    function updateMedTermShares(
        uint256 _shares
    ) 
        external
    {    
        require(
            _msgSender() == STAKE_GATEKEEPER,
            \u0027GRISE: Operation not allowed\u0027
        );
        
        mediumTermShares = _shares;
    }

    function getTransFeeReward(
        uint256 _fromDay,
        uint256 _toDay
    ) 
        external 
        view 
        returns (uint256 rewardAmount)
    {
        require(
            _msgSender() == STAKE_GATEKEEPER,
            \u0027GRISE: Operation not allowed\u0027
        );

        for(uint256 day = _fromDay; day \u003c _toDay; day++)
        {
            rewardAmount += stakerRewardPerShare[day];
        }
    }

    function getReservoirReward(
        uint256 _fromDay,
        uint256 _toDay
    ) 
        external
        view 
        returns (uint256 rewardAmount)
    {
        require(
            _msgSender() == STAKE_GATEKEEPER,
            \u0027GRISE: Operation not allowed\u0027
        );

        for(uint256 day = _fromDay; day \u003c _toDay; day++)
        {
            rewardAmount += reservoirRewardPerShare[day];
        }
    }

    function getTokenHolderReward(
        uint256 _fromDay,
        uint256 _toDay
    ) 
        external
        view 
        returns (uint256 rewardAmount)
    {

        require(
            _msgSender() == STAKE_GATEKEEPER,
            \u0027GRISE: Operation not allowed\u0027
        );

        for(uint256 day = _fromDay; day \u003c _toDay; day++)
        {
            rewardAmount += tokenHolderReward[day]
                            .mul(PRECISION_RATE)
                            .div(totalToken[day]);
        }
    }

    function timeToClaimWeeklyReward() 
        public
        view
        returns (uint256 _days)
    {
        _days = currentGriseDay().mod(GRISE_WEEK) \u003e 0 ?
                    GRISE_WEEK - currentGriseDay().mod(GRISE_WEEK) :
                    0;
    }

    function timeToClaimMonthlyReward() 
        public 
        view 
        returns (uint256 _days)
    {
        _days = currentGriseDay().mod(GRISE_MONTH) \u003e 0 ?
                    GRISE_MONTH - currentGriseDay().mod(GRISE_MONTH) :
                    0;
    }

    function balanceOfStaker(
        address account
    ) 
        external
        view
        returns (uint256)
    {
        return _balances[account];
    }

    function getEpocTime() 
        external
        view 
        returns (uint256)
    {
        return block.timestamp;
    }

    function getLaunchTime()
        external
        view
        returns (uint256)
    {
        return LAUNCH_TIME;
    }

    function getLPLaunchTime()
        external
        view
        returns (uint256)
    {
        return LP_LAUNCH_TIME;
    }

    function isStaker(
        address _staker
    ) 
        external
        view
        returns (bool status)
    {
        status = (staker[_staker] \u003e 0) ? true : false;
    }
}"},"Migrations.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.7.6;

contract Migrations {
    address public owner;
    uint public last_completed_migration;

    constructor() {
        owner = msg.sender;
    }

    modifier restricted() {
        if (msg.sender == owner)
        _;
    }

    function setCompleted(uint completed) public restricted {
        last_completed_migration = completed;
    }

    function upgrade(address new_address) public restricted {
        Migrations upgraded = Migrations(new_address);
        upgraded.setCompleted(last_completed_migration);
    }
}
"},"SafeMath.sol":{"content":"// SPDX-License-Identifier: --GRISE--

pragma solidity =0.7.6;

library SafeMath {

    function add(uint256 a, uint256 b) internal pure returns (uint256) {
        uint256 c = a + b;
        require(c \u003e= a, \u0027GRISE: SafeMath Add failed\u0027);
        return c;
    }

    function sub(uint256 a, uint256 b) internal pure returns (uint256) {
        require(b \u003c= a, \u0027GRISE: SafeMath Sub failed\u0027);
        uint256 c = a - b;
        return c;
    }

    function mul(uint256 a, uint256 b) internal pure returns (uint256) {

        if (a == 0 || b == 0) {
            return 0;
        }

        uint256 c = a * b;
        require(c / a == b, \u0027GRISE: SafeMath Mul failed\u0027);
        return c;
    }

    function div(uint256 a, uint256 b) internal pure returns (uint256) {
        
        if (b == 0) {
            return 0;
        }

        uint256 c = a / b;
        return c;
    }

    function mod(uint256 a, uint256 b) internal pure returns (uint256) {
        require(b != 0, \u0027GRISE: SafeMath Mod failed\u0027);
        return a % b;
    }
}
"},"Timing.sol":{"content":"// SPDX-License-Identifier: --GRISE--

pragma solidity =0.7.6;

import \"./Declaration.sol\";

abstract contract Timing is Declaration {

    function currentLPDay() public view returns (uint256) {
        return _getNow() \u003e= LP_LAUNCH_TIME ? _currentLPDay() : 0;
    } 

    function _currentLPDay() internal view returns (uint256) {
        return _LPDayFromStamp(_getNow()) + 1;
    }

    function _LPDayFromStamp(uint256 _timestamp) internal view returns (uint256) {
        return uint256((_timestamp - LP_LAUNCH_TIME) / SECONDS_IN_DAY_LP);
    }

    function currentGriseWeek() public view returns (uint256) {
        return (currentGriseDay() / GRISE_WEEK);
    }

    function currentGriseDay() public view returns (uint256) {
        return _getNow() \u003e= LAUNCH_TIME ? _currentGriseDay() : 0;
    }

    function _currentGriseDay() internal view returns (uint256) {
        return _griseDayFromStamp(_getNow());
    }

    function _nextGriseDay() internal view returns (uint256) {
        return _currentGriseDay() + 1;
    }

    function _previousGriseDay() internal view returns (uint256) {
        return _currentGriseDay() - 1;
    }

    function _griseDayFromStamp(uint256 _timestamp) internal view returns (uint256) {
        return uint256((_timestamp - LAUNCH_TIME) / SECONDS_IN_DAY);
    }

    function _getNow() internal view returns (uint256) {
        return block.timestamp;
    }
}"},"Utils.sol":{"content":"// SPDX-License-Identifier: --GRISE--

pragma solidity =0.7.6;

import \"./Timing.sol\";

abstract contract Utils is Timing {

    using SafeMath for uint256;

    function toUint256(bytes memory _bytes)   
    internal
    pure
    returns (uint256 value) {

    assembly {
      value := mload(add(_bytes, 0x20))
    }
    }

    function notContract(address _addr) internal view returns (bool) {
        uint32 size;
        assembly {
            size := extcodesize(_addr)
        }
        return (size == 0);
    }

    function toBytes16(uint256 x) internal pure returns (bytes16 b) {
       return bytes16(bytes32(x));
    }
    
    function _notFuture(uint256 _day) internal view returns (bool) {
        return _day \u003c= _currentGriseDay();
    }

    function _notPast(uint256 _day) internal view returns (bool) {
        return _day \u003e= _currentGriseDay();
    }

    function _nonZeroAddress(address _address) internal pure returns (bool) {
        return _address != address(0x0);
    }

    function _calculateSellTranscFee(uint256 _tAmount) internal pure returns (uint256) {
        return _tAmount.mul(SELL_TRANS_FEE).div(REWARD_PRECISION_RATE);
    }

    function _calculateBuyTranscFee(uint256 _tAmount) internal pure returns (uint256) {
        return _tAmount.mul(BUY_TRANS_FEE).div(REWARD_PRECISION_RATE);
    }
    
    function calculateGriseWeek(uint256 _day) internal pure returns (uint256) {
        return (_day / GRISE_WEEK);
    }
}
