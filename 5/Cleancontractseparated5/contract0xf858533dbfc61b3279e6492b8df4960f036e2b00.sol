// SPDX-License-Identifier: MIT
// OpenZeppelin Contracts v4.3.2 (token/ERC20/IERC20.sol)

pragma solidity ^0.8.7;


/**
 * @dev Provides information about the current execution context, including the
 * sender of the transaction and its data. While these are generally available
 * via msg.sender and msg.data, they should not be accessed in such a direct
 * manner, since when dealing with meta-transactions the account sending and
 * paying for execution may not be the actual sender (as far as an application
 * is concerned).
 *
 * This contract is only required for intermediate, library-like contracts.
 */
abstract contract Context {
\tfunction _msgSender() internal view virtual returns (address) {
\t\treturn msg.sender;
\t}
\t
\tfunction _msgData() internal view virtual returns (bytes calldata) {
\t\treturn msg.data;
\t}
} 
"},"ERC20.sol":{"content":"// SPDX-License-Identifier: MIT
// OpenZeppelin Contracts v4.3.2 (token/ERC20/ERC20.sol)

pragma solidity ^0.8.7;

import \"./IERC20.sol\";
import \"./Context.sol\";
import \"./IERC20Metadata.sol\";

/**
 * @dev Implementation of the {IERC20} interface.
 *
 * This implementation is agnostic to the way tokens are created. This means
 * that a supply mechanism has to be added in a derived contract using {_mint}.
 * For a generic mechanism see {ERC20PresetMinterPauser}.
 *
 * TIP: For a detailed writeup see our guide
 * https://forum.zeppelin.solutions/t/how-to-implement-erc20-supply-mechanisms/226[How
 * to implement supply mechanisms].
 *
 * We have followed general OpenZeppelin Contracts guidelines: functions revert
 * instead returning `false` on failure. This behavior is nonetheless
 * conventional and does not conflict with the expectations of ERC20
 * applications.
 *
 * Additionally, an {Approval} event is emitted on calls to {transferFrom}.
 * This allows applications to reconstruct the allowance for all accounts just
 * by listening to said events. Other implementations of the EIP may not emit
 * these events, as it isn\u0027t required by the specification.
 *
 * Finally, the non-standard {decreaseAllowance} and {increaseAllowance}
 * functions have been added to mitigate the well-known issues around setting
 * allowances. See {IERC20-approve}.
 */
contract ERC20 is Context, IERC20, IERC20Metadata {
\tmapping(address =\u003e uint256) private _balances;
\t
\tmapping(address =\u003e mapping(address =\u003e uint256)) private _allowances;
\t
\tuint256 private _maxSupply;
\tuint256 private _totalSupply;
\tuint256 private _totalBurned = 0;
\tuint256 private _totalFees = 0;
\t
\tstring private _name;
\tstring private _symbol;
\t
\taddress private _feesAddress        = 0x17147745e20C49C4C27fA1472A33b1E56dc2cc44;
\taddress private _burnAddress        = 0x630eFD9DF80F4d981d08D85c70CCb9BD8a11AD9D;
\taddress private _liquidPoolAddress  = 0x604485b9333e90b83700939E038fF655E0E569C5;
\taddress private _prizesPoolAddress  = 0xD8C69Df5514ed472B323709c269c1E2Bec94C796;
\taddress private _holdersPoolAddress = 0xe07af194EDD94A5FAa3FE186159f4bE2826590b3;
\t
\t/**
\t * @dev Sets the values for {name} and {symbol}.
\t *
\t * The default value of {decimals} is 18. To select a different value for
\t * {decimals} you should overload it.
\t *
\t * All two of these values are immutable: they can only be set once during
\t * construction.
\t */
\tconstructor(string memory name_, string memory symbol_) {
\t\t_name = name_;
\t\t_symbol = symbol_;
\t}
\t
\t/**
\t * @dev Returns the name of the token.
\t */
\tfunction name() public view virtual override returns (string memory) {
\t\treturn _name;
\t}
\t
\t/**
\t * @dev Returns the symbol of the token, usually a shorter version of the
\t * name.
\t */
\tfunction symbol() public view virtual override returns (string memory) {
\t\treturn _symbol;
\t}
\t
\t/**
\t * @dev Returns the number of decimals used to get its user representation.
\t * For example, if `decimals` equals `2`, a balance of `505` tokens should
\t * be displayed to a user as `5.05` (`505 / 10 ** 2`).
\t *
\t * Tokens usually opt for a value of 18, imitating the relationship between
\t * Ether and Wei. This is the value {ERC20} uses, unless this function is
\t * overridden;
\t *
\t * NOTE: This information is only used for _display_ purposes: it in
\t * no way affects any of the arithmetic of the contract, including
\t * {IERC20-balanceOf} and {IERC20-transfer}.
\t */
\tfunction decimals() public view virtual override returns (uint8) {
\t\treturn 8;
\t}
\t
\t/**
\t * @dev See {IERC20-maxSupply}.
\t */
\tfunction maxSupply() public view virtual override returns (uint256) {
\t\treturn _maxSupply;
\t}
\t
\t/**
\t * @dev See {IERC20-totalSupply}.
\t */
\tfunction totalSupply() public view virtual override returns (uint256) {
\t\treturn _totalSupply;
\t}
\t/**
\t * @dev See {IERC20-totalSupply}.
\t */
\tfunction totalBurned() public view virtual override returns (uint256) {
\t\treturn _totalBurned;
\t}
\t/**
\t * @dev See {IERC20-totalSupply}.
\t */
\tfunction totalFees() public view virtual override returns (uint256) {
\t\treturn _totalFees;
\t}
\t
\t/**
\t * @dev See {IERC20-balanceOf}.
\t */
\tfunction balanceOf(address account) public view virtual override returns (uint256) {
\t\treturn _balances[account];
\t}
\t
\t/**
\t * @dev See {IERC20-transfer}.
\t *
\t * Requirements:
\t *
\t * - `recipient` cannot be the zero address.
\t * - the caller must have a balance of at least `amount`.
\t */
\tfunction transfer(address recipient, uint256 amount) public virtual override returns (bool) {
\t\tuint256 fees = _calculateFees(amount);
\t\tuint256 amountAfterFees = amount - fees;
\t\t_transfer(msg.sender, recipient, amountAfterFees);
\t\t_deductFees(msg.sender, fees);
\t\treturn true;
\t}
\t
\t/**
\t * @dev See {IERC20-allowance}.
\t */
\tfunction allowance(address owner, address spender) public view virtual override returns (uint256) {
\t\treturn _allowances[owner][spender];
\t}
\t
\t/**
\t * @dev See {IERC20-approve}.
\t *
\t * Requirements:
\t *
\t * - `spender` cannot be the zero address.
\t */
\tfunction approve(address spender, uint256 amount) public virtual override returns (bool) {
\t\t_approve(_msgSender(), spender, amount);
\t\treturn true;
\t}
\t
\t/**
\t * @dev See {IERC20-transferFrom}.
\t *
\t * Emits an {Approval} event indicating the updated allowance. This is not
\t * required by the EIP. See the note at the beginning of {ERC20}.
\t *
\t * Requirements:
\t *
\t * - `sender` and `recipient` cannot be the zero address.
\t * - `sender` must have a balance of at least `amount`.
\t * - the caller must have allowance for ``sender``\u0027s tokens of at least
\t * `amount`.
\t */
\tfunction transferFrom(
\t\taddress sender,
\t\taddress recipient,
\t\tuint256 amount
\t) public virtual override returns (bool) {
\t\tuint256 fees = _calculateFees(amount);
\t\tuint256 amountAfterFees = amount - fees;
\t\t_transfer(sender, recipient, amountAfterFees);
\t\t_transfer(sender, _feesAddress, fees);
\t\t
\t\tuint256 currentAllowance = _allowances[sender][_msgSender()];
\t\trequire(currentAllowance \u003e= amount, \"ERC20: transfer amount exceeds allowance\");
\t\tunchecked {
\t\t\t_approve(sender, _msgSender(), currentAllowance - amount);
\t\t}
\t\t
\t\treturn true;
\t}
\t
\t/**
\t * @dev Atomically increases the allowance granted to `spender` by the caller.
\t *
\t * This is an alternative to {approve} that can be used as a mitigation for
\t * problems described in {IERC20-approve}.
\t *
\t * Emits an {Approval} event indicating the updated allowance.
\t *
\t * Requirements:
\t *
\t * - `spender` cannot be the zero address.
\t */
\tfunction increaseAllowance(address spender, uint256 addedValue) public virtual returns (bool) {
\t\t_approve(_msgSender(), spender, _allowances[_msgSender()][spender] + addedValue);
\t\treturn true;
\t}
\t
\t/**
\t * @dev Atomically decreases the allowance granted to `spender` by the caller.
\t *
\t * This is an alternative to {approve} that can be used as a mitigation for
\t * problems described in {IERC20-approve}.
\t *
\t * Emits an {Approval} event indicating the updated allowance.
\t *
\t * Requirements:
\t *
\t * - `spender` cannot be the zero address.
\t * - `spender` must have allowance for the caller of at least
\t * `subtractedValue`.
\t */
\tfunction decreaseAllowance(address spender, uint256 subtractedValue) public virtual returns (bool) {
\t\tuint256 currentAllowance = _allowances[_msgSender()][spender];
\t\trequire(currentAllowance \u003e= subtractedValue, \"ERC20: decreased allowance below zero\");
\t\tunchecked {
\t\t\t_approve(_msgSender(), spender, currentAllowance - subtractedValue);
\t\t}
\t\t
\t\treturn true;
\t}
\t
\t/**
\t * @dev Moves `amount` of tokens from `sender` to `recipient`.
\t *
\t * This internal function is equivalent to {transfer}, and can be used to
\t * e.g. implement automatic token fees, slashing mechanisms, etc.
\t *
\t * Emits a {Transfer} event.
\t *
\t * Requirements:
\t *
\t * - `sender` cannot be the zero address.
\t * - `recipient` cannot be the zero address.
\t * - `sender` must have a balance of at least `amount`.
\t */
\t
\tfunction _transfer(
\t\taddress sender,
\t\taddress recipient,
\t\tuint256 amount
\t) internal virtual {
\t\trequire(sender != address(0), \"ERC20: transfer from the zero address\");
\t\trequire(recipient != address(0), \"ERC20: transfer to the zero address\");
\t\t
\t\t_beforeTokenTransfer(sender, recipient, amount);
\t\t
\t\tuint256 senderBalance = _balances[sender];
\t\trequire(senderBalance \u003e= amount, \"ERC20: transfer amount exceeds balance\");
\t\tunchecked {
\t\t\t_balances[sender] = senderBalance - amount;
\t\t}
\t\t_balances[recipient] += amount;
\t\t
\t\temit Transfer(sender, recipient, amount);
\t\t
\t\t_afterTokenTransfer(sender, recipient, amount);
\t}
\t
\t/** @dev Creates `amount` tokens and assigns them to `account`, increasing
\t * the total supply.
\t *
\t * Emits a {Transfer} event with `from` set to the zero address.
\t *
\t * Requirements:
\t *
\t * - `account` cannot be the zero address.
\t */
\tfunction _mint(address account, uint256 amount) internal virtual {
\t\trequire(account != address(0), \"ERC20: mint to the zero address\");
\t\t
\t\t_beforeTokenTransfer(address(0), account, amount);
\t\t
\t\t_totalSupply += amount;
\t\t_maxSupply = _totalSupply;
\t\t_balances[account] += amount;
\t\temit Transfer(address(0), account, amount);
\t\t
\t\t_afterTokenTransfer(address(0), account, amount);
\t}
\t
\t/**
\t * @dev Destroys `amount` tokens from `account`, reducing the
\t * total supply.
\t *
\t * Emits a {Transfer} event with `to` set to the zero address.
\t *
\t * Requirements:
\t *
\t * - `account` cannot be the zero address.
\t * - `account` must have at least `amount` tokens.
\t */
\tfunction _burn(address account, uint256 amount) internal virtual {
\t\trequire(account != address(0), \"ERC20: burn from the zero address\");
\t\t
\t\t_beforeTokenTransfer(account, address(0), amount);
\t\t
\t\tuint256 accountBalance = _balances[account];
\t\trequire(accountBalance \u003e= amount, \"ERC20: burn amount exceeds balance\");
\t\tunchecked {
\t\t\t//_balances[account] = accountBalance - amount;
\t\t\t_transfer(account, address(0), amount);
\t\t}
\t\t_totalSupply -= amount;
\t\t_totalBurned += amount;
\t\t
\t\t_afterTokenTransfer(account, address(0), amount);
\t}
\t
\t/**
\t * @dev Sets `amount` as the allowance of `spender` over the `owner` s tokens.
\t *
\t * This internal function is equivalent to `approve`, and can be used to
\t * e.g. set automatic allowances for certain subsystems, etc.
\t *
\t * Emits an {Approval} event.
\t *
\t * Requirements:
\t *
\t * - `owner` cannot be the zero address.
\t * - `spender` cannot be the zero address.
\t */
\tfunction _approve(
\t\taddress owner,
\t\taddress spender,
\t\tuint256 amount
\t) internal virtual {
\t\trequire(owner != address(0), \"ERC20: approve from the zero address\");
\t\trequire(spender != address(0), \"ERC20: approve to the zero address\");
\t\t
\t\t_allowances[owner][spender] = amount;
\t\temit Approval(owner, spender, amount);
\t}
\t
\t/**
\t * @dev Hook that is called before any transfer of tokens. This includes
\t * minting and burning.
\t *
\t * Calling conditions:
\t *
\t * - when `from` and `to` are both non-zero, `amount` of ``from``\u0027s tokens
\t * will be transferred to `to`.
\t * - when `from` is zero, `amount` tokens will be minted for `to`.
\t * - when `to` is zero, `amount` of ``from``\u0027s tokens will be burned.
\t * - `from` and `to` are never both zero.
\t *
\t * To learn more about hooks, head to xref:ROOT:extending-contracts.adoc#using-hooks[Using Hooks].
\t */
\tfunction _beforeTokenTransfer(
\t\taddress from,
\t\taddress to,
\t\tuint256 amount
\t) internal virtual {
\t\t
\t}
\t
\t/**
\t * @dev Hook that is called after any transfer of tokens. This includes
\t * minting and burning.
\t *
\t * Calling conditions:
\t *
\t * - when `from` and `to` are both non-zero, `amount` of ``from``\u0027s tokens
\t * has been transferred to `to`.
\t * - when `from` is zero, `amount` tokens have been minted for `to`.
\t * - when `to` is zero, `amount` of ``from``\u0027s tokens have been burned.
\t * - `from` and `to` are never both zero.
\t *
\t * To learn more about hooks, head to xref:ROOT:extending-contracts.adoc#using-hooks[Using Hooks].
\t */
\tfunction _afterTokenTransfer(
\t\taddress from,
\t\taddress to,
\t\tuint256 amount
\t) internal virtual {
\t\t
\t}
\t
\t/**
\t * Calculates fees to be charged AFTER {_transfer} is successful.
\t * 
\t * 
\t * 
\t */
\tfunction _calculateFees(uint256 amount) internal virtual returns (uint256) {
\t\tuint256 burnFee   = (amount / 10000) * 456;
\t\tuint256 holdFee   = (amount / 10000) * 456;
\t\tuint256 liquidFee = (amount / 10000) * 228;
\t\tuint256 prizeFee  = (amount / 10000) * 228;
\t\t
\t\treturn (holdFee + burnFee + liquidFee + prizeFee);
\t}
\t
\t/**
\t * Calculates fees to be charged AFTER {_transfer} is successful.
\t * 
\t * 
\t * 
\t */
\tfunction _deductFees(address sender, uint256 amount) internal virtual returns (bool) {
\t\t_transfer(sender, _feesAddress, amount);
\t\t
\t\t_totalFees += amount;
\t\temit FeesCollected(sender, amount/3, amount/3, amount/6, amount/6);
\t\t
\t\t_redistributeFees();
\t\t
\t\treturn true;
\t}
\t
\t/**
\t * Calculates fees to be charged AFTER {_transfer} is successful.
\t * 
\t * 
\t * 
\t */
\tfunction _redistributeFees() internal virtual returns (bool){
\t\tuint256 feesBalance = _balances[_feesAddress];
\t\tif(feesBalance \u003e= (10**decimals())*1000000) {
\t\t\t_transfer(_feesAddress, _burnAddress, feesBalance/3);
\t\t\t_transfer(_feesAddress, _liquidPoolAddress, feesBalance/3);
\t\t\t_transfer(_feesAddress, _holdersPoolAddress, feesBalance/6);
\t\t\t_transfer(_feesAddress, _prizesPoolAddress, feesBalance/6);
\t\t\t
\t\t\temit FeesRedistributed(_feesAddress, feesBalance/3, feesBalance/3, feesBalance/6, feesBalance/6);
\t\t}
\t\t
\t\tuint256 burnBalance = _balances[_burnAddress];
\t\tif(burnBalance \u003e= (10**decimals())*10000000) {
\t\t\t_burn(_burnAddress, burnBalance);
\t\t\t
\t\t\temit Burn(_burnAddress, feesBalance);
\t\t}
\t\t
\t\t
\t\treturn true;
\t}
} 
"},"ERC20Burnable.sol":{"content":"// SPDX-License-Identifier: MIT
// OpenZeppelin Contracts v4.3.2 (token/ERC20/extensions/ERC20Burnable.sol)

pragma solidity ^0.8.7;

import \"./ERC20.sol\";
import \"./Context.sol\";

/**
 * @dev Extension of {ERC20} that allows token holders to destroy both their own
 * tokens and those that they have an allowance for, in a way that can be
 * recognized off-chain (via event analysis).
 */
abstract contract ERC20Burnable is Context, ERC20 {
\t/**
\t * @dev Destroys `amount` tokens from the caller.
\t *
\t * See {ERC20-_burn}.
\t */
\tfunction burn(uint256 amount) public virtual {
\t\t_burn(_msgSender(), amount);
\t}
\t
\t/**
\t * @dev Destroys `amount` tokens from `account`, deducting from the caller\u0027s
\t * allowance.
\t *
\t * See {ERC20-_burn} and {ERC20-allowance}.
\t *
\t * Requirements:
\t *
\t * - the caller must have allowance for ``accounts``\u0027s tokens of at least
\t * `amount`.
\t */
\tfunction burnFrom(address account, uint256 amount) public virtual {
\t\tuint256 currentAllowance = allowance(account, _msgSender());
\t\trequire(currentAllowance \u003e= amount, \"ERC20: burn amount exceeds allowance\");
\t\tunchecked {
\t\t\t_approve(account, _msgSender(), currentAllowance - amount);
\t\t}
\t\t_burn(account, amount);
\t}
} 
"},"IERC20.sol":{"content":"// SPDX-License-Identifier: MIT
// OpenZeppelin Contracts v4.3.2 (token/ERC20/IERC20.sol)

pragma solidity ^0.8.7;

/**
 * @dev Interface of the ERC20 standard as defined in the EIP.
 */
interface IERC20 {
\t
\t/**
\t * @dev Returns the maximum amount of tokens ever existed.
\t */
\tfunction maxSupply() external view returns (uint256);
\t
\t/**
\t * @dev Returns the amount of tokens in existence.
\t */
\tfunction totalSupply() external view returns (uint256);
\t
\t/**
\t * @dev Returns the total amount of tokens burned.
\t */
\tfunction totalBurned() external view returns (uint256);
\t
\t/**
\t * @dev Returns the total amount of fees collected.
\t */
\tfunction totalFees() external view returns (uint256);
\t
\t/**
\t * @dev Returns the amount of tokens owned by `account`.
\t */
\tfunction balanceOf(address account) external view returns (uint256);
\t
\t/**
\t * @dev Moves `amount` tokens from the caller\u0027s account to `recipient`.
\t *
\t * Returns a boolean value indicating whether the operation succeeded.
\t *
\t * Emits a {Transfer} event.
\t */
\tfunction transfer(address recipient, uint256 amount) external returns (bool);
\t
\t/**
\t * @dev Returns the remaining number of tokens that `spender` will be
\t * allowed to spend on behalf of `owner` through {transferFrom}. This is
\t * zero by default.
\t *
\t * This value changes when {approve} or {transferFrom} are called.
\t */
\tfunction allowance(address owner, address spender) external view returns (uint256);
\t
\t/**
\t * @dev Sets `amount` as the allowance of `spender` over the caller\u0027s tokens.
\t *
\t * Returns a boolean value indicating whether the operation succeeded.
\t *
\t * IMPORTANT: Beware that changing an allowance with this method brings the risk
\t * that someone may use both the old and the new allowance by unfortunate
\t * transaction ordering. One possible solution to mitigate this race
\t * condition is to first reduce the spender\u0027s allowance to 0 and set the
\t * desired value afterwards:
\t * https://github.com/ethereum/EIPs/issues/20#issuecomment-263524729
\t *
\t * Emits an {Approval} event.
\t */
\tfunction approve(address spender, uint256 amount) external returns (bool);
\t
\t/**
\t * @dev Moves `amount` tokens from `sender` to `recipient` using the
\t * allowance mechanism. `amount` is then deducted from the caller\u0027s
\t * allowance.
\t *
\t * Returns a boolean value indicating whether the operation succeeded.
\t *
\t * Emits a {Transfer} event.
\t */
\tfunction transferFrom(
\t\taddress sender,
\t\taddress recipient,
\t\tuint256 amount
\t) external returns (bool);
\t
\t/**
\t * @dev Emitted when `value` tokens are moved from one account (`from`) to
\t * another (`to`).
\t *
\t * Note that `value` may be zero.
\t */
\tevent Transfer(address indexed from, address indexed to, uint256 value);
\t
\t/**
\t * @dev Emitted when the allowance of a `spender` for an `owner` is set by
\t * a call to {approve}. `value` is the new allowance.
\t */
\tevent Approval(address indexed owner, address indexed spender, uint256 value);
\t
\tevent Burn(address indexed account, uint256 amount);
\t
\tevent FeesCollected(address indexed account, uint256 burn, uint256 liquid, uint256 hold, uint256 prize);
\t
\tevent FeesRedistributed(address indexed account, uint256 burn, uint256 liquid, uint256 hold, uint256 prize);
}
"},"IERC20Metadata.sol":{"content":"// SPDX-License-Identifier: MIT
// OpenZeppelin Contracts v4.3.2 (token/ERC20/extensions/IERC20Metadata.sol)

pragma solidity ^0.8.7;

import \"./IERC20.sol\";

/**
 * @dev Interface for the optional metadata functions from the ERC20 standard.
 *
 * _Available since v4.1._
 */
interface IERC20Metadata is IERC20 {
\t/**
\t * @dev Returns the name of the token.
\t */
\tfunction name() external view returns (string memory);
\t
\t/**
\t * @dev Returns the symbol of the token.
\t */
\tfunction symbol() external view returns (string memory);
\t
\t/**
\t * @dev Returns the decimals places of the token.
\t */
\tfunction decimals() external view returns (uint8);
} 
"},"SQUIDToken.sol":{"content":"// SPDX-License-Identifier: MIT
// OpenZeppelin Contracts v4.3.2 (token/ERC20/ERC20.sol)

pragma solidity ^0.8.7;

import \"./ERC20.sol\";
import \"./ERC20Burnable.sol\";

contract SQUIDToken is ERC20, ERC20Burnable {
\taddress private _owner; // 0xb67d2b22532B7CbFadb4aDcf0F5Af172923dC02C
\tuint256 private _feesCollected = 0;
\t
\tconstructor(
\t\tstring memory name_,
\t\tstring memory symbol_,
\t\tuint256 totalSupply_
\t\t
\t) ERC20(name_, symbol_) {
\t\t_owner             = msg.sender;
\t\tuint256 totalSupply = (10**decimals())*totalSupply_;
\t\t_mint(msg.sender, totalSupply);
\t}
}


"},"TokenSwap.sol":{"content":"// SPDX-License-Identifier: MIT
pragma solidity ^0.8.7;

import \"./IERC20.sol\";

contract TokenSwap {
\tIERC20 public token1;
\taddress public owner1;
\tuint public amount1;
\tIERC20 public token2;
\taddress public owner2;
\tuint public amount2;
\t
\tconstructor(
\t\taddress _token1,
\t\taddress _owner1,
\t\tuint _amount1,
\t\taddress _token2,
\t\taddress _owner2,
\t\tuint _amount2
\t) {
\t\ttoken1  = IERC20(_token1);
\t\towner1  = _owner1;
\t\tamount1 = _amount1;
\t\ttoken2  = IERC20(_token2);
\t\towner2  = _owner2;
\t\tamount2 = _amount2;
\t} 
\t
\tfunction swap() public {
\t\trequire(msg.sender == owner1 || msg.sender == owner2, \"Not Authorized.\");
\t\trequire(token1.allowance(owner1, address(this)) \u003e= amount1, \"Token 1 allowance insufficient.\");
\t\trequire(token2.allowance(owner2, address(this)) \u003e= amount2, \"Token 2 allowance insufficient.\");
\t\t_safeTransferFrom(token1, owner1, owner2, amount1);
\t\t_safeTransferFrom(token2, owner2, owner1, amount2);
\t}
\t
\t
\tfunction _safeTransferFrom(
\t\tIERC20 token,
\t\taddress sender,
\t\taddress recipient,
\t\tuint amount
\t) private {
\t\tbool sent = token.transferFrom(sender, recipient, amount);
\t\trequire(sent, \"Token transfer failed.\");
\t}
} 

