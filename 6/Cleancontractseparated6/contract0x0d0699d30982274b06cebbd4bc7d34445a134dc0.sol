



// SPDX-License-Identifier: MIT

// solhint-disable-next-line compiler-version
pragma solidity >=0.4.24 <0.8.0;

import \"../utils/AddressUpgradeable.sol\";

/**
 * @dev This is a base contract to aid in writing upgradeable contracts, or any kind of contract that will be deployed
 * behind a proxy. Since a proxied contract can't have a constructor, it's common to move constructor logic to an
 * external initializer function, usually called `initialize`. It then becomes necessary to protect this initializer
 * function so it can only be called once. The {initializer} modifier provided by this contract will have this effect.
 *
 * TIP: To avoid leaving the proxy in an uninitialized state, the initializer function should be called as early as
 * possible by providing the encoded function call as the `_data` argument to {UpgradeableProxy-constructor}.
 *
 * CAUTION: When used with inheritance, manual care must be taken to not invoke a parent initializer twice, or to ensure
 * that all initializers are idempotent. This is not verified automatically as constructors are by Solidity.
 */
abstract contract Initializable {

    /**
     * @dev Indicates that the contract has been initialized.
     */
    bool private _initialized;

    /**
     * @dev Indicates that the contract is in the process of being initialized.
     */
    bool private _initializing;

    /**
     * @dev Modifier to protect an initializer function from being invoked twice.
     */
    modifier initializer() {
        require(_initializing || _isConstructor() || !_initialized, \"Initializable: contract is already initialized\");

        bool isTopLevelCall = !_initializing;
        if (isTopLevelCall) {
            _initializing = true;
            _initialized = true;
        }

        _;

        if (isTopLevelCall) {
            _initializing = false;
        }
    }

    /// @dev Returns true if and only if the function is running in the constructor
    function _isConstructor() private view returns (bool) {
        return !AddressUpgradeable.isContract(address(this));
    }
}
"
    
// SPDX-License-Identifier: MIT

pragma solidity ^0.7.0;

/**
 * @dev Collection of functions related to the address type
 */
library AddressUpgradeable {
    /**
     * @dev Returns true if `account` is a contract.
     *
     * [IMPORTANT]
     * ====
     * It is unsafe to assume that an address for which this function returns
     * false is an externally-owned account (EOA) and not a contract.
     *
     * Among others, `isContract` will return false for the following
     * types of addresses:
     *
     *  - an externally-owned account
     *  - a contract in construction
     *  - an address where a contract will be created
     *  - an address where a contract lived, but was destroyed
     * ====
     */
    function isContract(address account) internal view returns (bool) {
        // This method relies on extcodesize, which returns 0 for contracts in
        // construction, since the code is only stored at the end of the
        // constructor execution.

        uint256 size;
        // solhint-disable-next-line no-inline-assembly
        assembly { size := extcodesize(account) }
        return size > 0;
    }

    /**
     * @dev Replacement for Solidity's `transfer`: sends `amount` wei to
     * `recipient`, forwarding all available gas and reverting on errors.
     *
     * https://eips.ethereum.org/EIPS/eip-1884[EIP1884] increases the gas cost
     * of certain opcodes, possibly making contracts go over the 2300 gas limit
     * imposed by `transfer`, making them unable to receive funds via
     * `transfer`. {sendValue} removes this limitation.
     *
     * https://diligence.consensys.net/posts/2019/09/stop-using-soliditys-transfer-now/[Learn more].
     *
     * IMPORTANT: because control is transferred to `recipient`, care must be
     * taken to not create reentrancy vulnerabilities. Consider using
     * {ReentrancyGuard} or the
     * https://solidity.readthedocs.io/en/v0.5.11/security-considerations.html#use-the-checks-effects-interactions-pattern[checks-effects-interactions pattern].
     */
    function sendValue(address payable recipient, uint256 amount) internal {
        require(address(this).balance >= amount, \"Address: insufficient balance\");

        // solhint-disable-next-line avoid-low-level-calls, avoid-call-value
        (bool success, ) = recipient.call{ value: amount }(\"\");
        require(success, \"Address: unable to send value, recipient may have reverted\");
    }

    /**
     * @dev Performs a Solidity function call using a low level `call`. A
     * plain`call` is an unsafe replacement for a function call: use this
     * function instead.
     *
     * If `target` reverts with a revert reason, it is bubbled up by this
     * function (like regular Solidity function calls).
     *
     * Returns the raw returned data. To convert to the expected return value,
     * use https://solidity.readthedocs.io/en/latest/units-and-global-variables.html?highlight=abi.decode#abi-encoding-and-decoding-functions[`abi.decode`].
     *
     * Requirements:
     *
     * - `target` must be a contract.
     * - calling `target` with `data` must not revert.
     *
     * _Available since v3.1._
     */
    function functionCall(address target, bytes memory data) internal returns (bytes memory) {
      return functionCall(target, data, \"Address: low-level call failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`], but with
     * `errorMessage` as a fallback revert reason when `target` reverts.
     *
     * _Available since v3.1._
     */
    function functionCall(address target, bytes memory data, string memory errorMessage) internal returns (bytes memory) {
        return functionCallWithValue(target, data, 0, errorMessage);
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`],
     * but also transferring `value` wei to `target`.
     *
     * Requirements:
     *
     * - the calling contract must have an ETH balance of at least `value`.
     * - the called Solidity function must be `payable`.
     *
     * _Available since v3.1._
     */
    function functionCallWithValue(address target, bytes memory data, uint256 value) internal returns (bytes memory) {
        return functionCallWithValue(target, data, value, \"Address: low-level call with value failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCallWithValue-address-bytes-uint256-}[`functionCallWithValue`], but
     * with `errorMessage` as a fallback revert reason when `target` reverts.
     *
     * _Available since v3.1._
     */
    function functionCallWithValue(address target, bytes memory data, uint256 value, string memory errorMessage) internal returns (bytes memory) {
        require(address(this).balance >= value, \"Address: insufficient balance for call\");
        require(isContract(target), \"Address: call to non-contract\");

        // solhint-disable-next-line avoid-low-level-calls
        (bool success, bytes memory returndata) = target.call{ value: value }(data);
        return _verifyCallResult(success, returndata, errorMessage);
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`],
     * but performing a static call.
     *
     * _Available since v3.3._
     */
    function functionStaticCall(address target, bytes memory data) internal view returns (bytes memory) {
        return functionStaticCall(target, data, \"Address: low-level static call failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-string-}[`functionCall`],
     * but performing a static call.
     *
     * _Available since v3.3._
     */
    function functionStaticCall(address target, bytes memory data, string memory errorMessage) internal view returns (bytes memory) {
        require(isContract(target), \"Address: static call to non-contract\");

        // solhint-disable-next-line avoid-low-level-calls
        (bool success, bytes memory returndata) = target.staticcall(data);
        return _verifyCallResult(success, returndata, errorMessage);
    }

    function _verifyCallResult(bool success, bytes memory returndata, string memory errorMessage) private pure returns(bytes memory) {
        if (success) {
            return returndata;
        } else {
            // Look for revert reason and bubble it up if present
            if (returndata.length > 0) {
                // The easiest way to bubble the revert reason is using memory via assembly

                // solhint-disable-next-line no-inline-assembly
                assembly {
                    let returndata_size := mload(returndata)
                    revert(add(32, returndata), returndata_size)
                }
            } else {
                revert(errorMessage);
            }
        }
    }
}
"
    
// SPDX-License-Identifier: MIT

pragma solidity >=0.6.0 <0.8.0;
import \"../proxy/Initializable.sol\";

/*
 * @dev Provides information about the current execution context, including the
 * sender of the transaction and its data. While these are generally available
 * via msg.sender and msg.data, they should not be accessed in such a direct
 * manner, since when dealing with GSN meta-transactions the account sending and
 * paying for execution may not be the actual sender (as far as an application
 * is concerned).
 *
 * This contract is only required for intermediate, library-like contracts.
 */
abstract contract ContextUpgradeable is Initializable {
    function __Context_init() internal initializer {
        __Context_init_unchained();
    }

    function __Context_init_unchained() internal initializer {
    }
    function _msgSender() internal view virtual returns (address payable) {
        return msg.sender;
    }

    function _msgData() internal view virtual returns (bytes memory) {
        this; // silence state mutability warning without generating bytecode - see https://github.com/ethereum/solidity/issues/2691
        return msg.data;
    }
    uint256[50] private __gap;
}
"
    
// SPDX-License-Identifier: MIT

pragma solidity ^0.7.0;

import \"./ContextUpgradeable.sol\";
import \"../proxy/Initializable.sol\";

/**
 * @dev Contract module which allows children to implement an emergency stop
 * mechanism that can be triggered by an authorized account.
 *
 * This module is used through inheritance. It will make available the
 * modifiers `whenNotPaused` and `whenPaused`, which can be applied to
 * the functions of your contract. Note that they will not be pausable by
 * simply including this module, only once the modifiers are put in place.
 */
abstract contract PausableUpgradeable is Initializable, ContextUpgradeable {
    /**
     * @dev Emitted when the pause is triggered by `account`.
     */
    event Paused(address account);

    /**
     * @dev Emitted when the pause is lifted by `account`.
     */
    event Unpaused(address account);

    bool private _paused;

    /**
     * @dev Initializes the contract in unpaused state.
     */
    function __Pausable_init() internal initializer {
        __Context_init_unchained();
        __Pausable_init_unchained();
    }

    function __Pausable_init_unchained() internal initializer {
        _paused = false;
    }

    /**
     * @dev Returns true if the contract is paused, and false otherwise.
     */
    function paused() public view virtual returns (bool) {
        return _paused;
    }

    /**
     * @dev Modifier to make a function callable only when the contract is not paused.
     *
     * Requirements:
     *
     * - The contract must not be paused.
     */
    modifier whenNotPaused() {
        require(!paused(), \"Pausable: paused\");
        _;
    }

    /**
     * @dev Modifier to make a function callable only when the contract is paused.
     *
     * Requirements:
     *
     * - The contract must be paused.
     */
    modifier whenPaused() {
        require(paused(), \"Pausable: not paused\");
        _;
    }

    /**
     * @dev Triggers stopped state.
     *
     * Requirements:
     *
     * - The contract must not be paused.
     */
    function _pause() internal virtual whenNotPaused {
        _paused = true;
        emit Paused(_msgSender());
    }

    /**
     * @dev Returns to normal state.
     *
     * Requirements:
     *
     * - The contract must be paused.
     */
    function _unpause() internal virtual whenPaused {
        _paused = false;
        emit Unpaused(_msgSender());
    }
    uint256[49] private __gap;
}
"
    
// SPDX-License-Identifier: MIT

pragma solidity ^0.7.0;

/**
 * @dev Wrappers over Solidity's arithmetic operations with added overflow
 * checks.
 *
 * Arithmetic operations in Solidity wrap on overflow. This can easily result
 * in bugs, because programmers usually assume that an overflow raises an
 * error, which is the standard behavior in high level programming languages.
 * `SafeMath` restores this intuition by reverting the transaction when an
 * operation overflows.
 *
 * Using this library instead of the unchecked operations eliminates an entire
 * class of bugs, so it's recommended to use it always.
 */
library SafeMath {
    /**
     * @dev Returns the addition of two unsigned integers, with an overflow flag.
     *
     * _Available since v3.4._
     */
    function tryAdd(uint256 a, uint256 b) internal pure returns (bool, uint256) {
        uint256 c = a + b;
        if (c < a) return (false, 0);
        return (true, c);
    }

    /**
     * @dev Returns the substraction of two unsigned integers, with an overflow flag.
     *
     * _Available since v3.4._
     */
    function trySub(uint256 a, uint256 b) internal pure returns (bool, uint256) {
        if (b > a) return (false, 0);
        return (true, a - b);
    }

    /**
     * @dev Returns the multiplication of two unsigned integers, with an overflow flag.
     *
     * _Available since v3.4._
     */
    function tryMul(uint256 a, uint256 b) internal pure returns (bool, uint256) {
        // Gas optimization: this is cheaper than requiring 'a' not being zero, but the
        // benefit is lost if 'b' is also tested.
        // See: https://github.com/OpenZeppelin/openzeppelin-contracts/pull/522
        if (a == 0) return (true, 0);
        uint256 c = a * b;
        if (c / a != b) return (false, 0);
        return (true, c);
    }

    /**
     * @dev Returns the division of two unsigned integers, with a division by zero flag.
     *
     * _Available since v3.4._
     */
    function tryDiv(uint256 a, uint256 b) internal pure returns (bool, uint256) {
        if (b == 0) return (false, 0);
        return (true, a / b);
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers, with a division by zero flag.
     *
     * _Available since v3.4._
     */
    function tryMod(uint256 a, uint256 b) internal pure returns (bool, uint256) {
        if (b == 0) return (false, 0);
        return (true, a % b);
    }

    /**
     * @dev Returns the addition of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity's `+` operator.
     *
     * Requirements:
     *
     * - Addition cannot overflow.
     */
    function add(uint256 a, uint256 b) internal pure returns (uint256) {
        uint256 c = a + b;
        require(c >= a, \"SafeMath: addition overflow\");
        return c;
    }

    /**
     * @dev Returns the subtraction of two unsigned integers, reverting on
     * overflow (when the result is negative).
     *
     * Counterpart to Solidity's `-` operator.
     *
     * Requirements:
     *
     * - Subtraction cannot overflow.
     */
    function sub(uint256 a, uint256 b) internal pure returns (uint256) {
        require(b <= a, \"SafeMath: subtraction overflow\");
        return a - b;
    }

    /**
     * @dev Returns the multiplication of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity's `*` operator.
     *
     * Requirements:
     *
     * - Multiplication cannot overflow.
     */
    function mul(uint256 a, uint256 b) internal pure returns (uint256) {
        if (a == 0) return 0;
        uint256 c = a * b;
        require(c / a == b, \"SafeMath: multiplication overflow\");
        return c;
    }

    /**
     * @dev Returns the integer division of two unsigned integers, reverting on
     * division by zero. The result is rounded towards zero.
     *
     * Counterpart to Solidity's `/` operator. Note: this function uses a
     * `revert` opcode (which leaves remaining gas untouched) while Solidity
     * uses an invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function div(uint256 a, uint256 b) internal pure returns (uint256) {
        require(b > 0, \"SafeMath: division by zero\");
        return a / b;
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
     * reverting when dividing by zero.
     *
     * Counterpart to Solidity's `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function mod(uint256 a, uint256 b) internal pure returns (uint256) {
        require(b > 0, \"SafeMath: modulo by zero\");
        return a % b;
    }

    /**
     * @dev Returns the subtraction of two unsigned integers, reverting with custom message on
     * overflow (when the result is negative).
     *
     * CAUTION: This function is deprecated because it requires allocating memory for the error
     * message unnecessarily. For custom revert reasons use {trySub}.
     *
     * Counterpart to Solidity's `-` operator.
     *
     * Requirements:
     *
     * - Subtraction cannot overflow.
     */
    function sub(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b <= a, errorMessage);
        return a - b;
    }

    /**
     * @dev Returns the integer division of two unsigned integers, reverting with custom message on
     * division by zero. The result is rounded towards zero.
     *
     * CAUTION: This function is deprecated because it requires allocating memory for the error
     * message unnecessarily. For custom revert reasons use {tryDiv}.
     *
     * Counterpart to Solidity's `/` operator. Note: this function uses a
     * `revert` opcode (which leaves remaining gas untouched) while Solidity
     * uses an invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function div(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b > 0, errorMessage);
        return a / b;
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
     * reverting with custom message when dividing by zero.
     *
     * CAUTION: This function is deprecated because it requires allocating memory for the error
     * message unnecessarily. For custom revert reasons use {tryMod}.
     *
     * Counterpart to Solidity's `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function mod(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b > 0, errorMessage);
        return a % b;
    }
}
"
    
// SPDX-License-Identifier: MIT

pragma solidity ^0.7.0;

/**
 * @dev Interface of the ERC20 standard as defined in the EIP.
 */
interface IERC20 {
    /**
     * @dev Returns the amount of tokens in existence.
     */
    function totalSupply() external view returns (uint256);

    /**
     * @dev Returns the amount of tokens owned by `account`.
     */
    function balanceOf(address account) external view returns (uint256);

    /**
     * @dev Moves `amount` tokens from the caller's account to `recipient`.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transfer(address recipient, uint256 amount) external returns (bool);

    /**
     * @dev Returns the remaining number of tokens that `spender` will be
     * allowed to spend on behalf of `owner` through {transferFrom}. This is
     * zero by default.
     *
     * This value changes when {approve} or {transferFrom} are called.
     */
    function allowance(address owner, address spender) external view returns (uint256);

    /**
     * @dev Sets `amount` as the allowance of `spender` over the caller's tokens.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * IMPORTANT: Beware that changing an allowance with this method brings the risk
     * that someone may use both the old and the new allowance by unfortunate
     * transaction ordering. One possible solution to mitigate this race
     * condition is to first reduce the spender's allowance to 0 and set the
     * desired value afterwards:
     * https://github.com/ethereum/EIPs/issues/20#issuecomment-263524729
     *
     * Emits an {Approval} event.
     */
    function approve(address spender, uint256 amount) external returns (bool);

    /**
     * @dev Moves `amount` tokens from `sender` to `recipient` using the
     * allowance mechanism. `amount` is then deducted from the caller's
     * allowance.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transferFrom(address sender, address recipient, uint256 amount) external returns (bool);

    /**
     * @dev Emitted when `value` tokens are moved from one account (`from`) to
     * another (`to`).
     *
     * Note that `value` may be zero.
     */
    event Transfer(address indexed from, address indexed to, uint256 value);

    /**
     * @dev Emitted when the allowance of a `spender` for an `owner` is set by
     * a call to {approve}. `value` is the new allowance.
     */
    event Approval(address indexed owner, address indexed spender, uint256 value);
}
"
    
// SPDX-License-Identifier: MIT

pragma solidity ^0.7.0;

import \"./IERC20.sol\";
import \"../../math/SafeMath.sol\";
import \"../../utils/Address.sol\";

/**
 * @title SafeERC20
 * @dev Wrappers around ERC20 operations that throw on failure (when the token
 * contract returns false). Tokens that return no value (and instead revert or
 * throw on failure) are also supported, non-reverting calls are assumed to be
 * successful.
 * To use this library you can add a `using SafeERC20 for IERC20;` statement to your contract,
 * which allows you to call the safe operations as `token.safeTransfer(...)`, etc.
 */
library SafeERC20 {
    using SafeMath for uint256;
    using Address for address;

    function safeTransfer(IERC20 token, address to, uint256 value) internal {
        _callOptionalReturn(token, abi.encodeWithSelector(token.transfer.selector, to, value));
    }

    function safeTransferFrom(IERC20 token, address from, address to, uint256 value) internal {
        _callOptionalReturn(token, abi.encodeWithSelector(token.transferFrom.selector, from, to, value));
    }

    /**
     * @dev Deprecated. This function has issues similar to the ones found in
     * {IERC20-approve}, and its usage is discouraged.
     *
     * Whenever possible, use {safeIncreaseAllowance} and
     * {safeDecreaseAllowance} instead.
     */
    function safeApprove(IERC20 token, address spender, uint256 value) internal {
        // safeApprove should only be called when setting an initial allowance,
        // or when resetting it to zero. To increase and decrease it, use
        // 'safeIncreaseAllowance' and 'safeDecreaseAllowance'
        // solhint-disable-next-line max-line-length
        require((value == 0) || (token.allowance(address(this), spender) == 0),
            \"SafeERC20: approve from non-zero to non-zero allowance\"
        );
        _callOptionalReturn(token, abi.encodeWithSelector(token.approve.selector, spender, value));
    }

    function safeIncreaseAllowance(IERC20 token, address spender, uint256 value) internal {
        uint256 newAllowance = token.allowance(address(this), spender).add(value);
        _callOptionalReturn(token, abi.encodeWithSelector(token.approve.selector, spender, newAllowance));
    }

    function safeDecreaseAllowance(IERC20 token, address spender, uint256 value) internal {
        uint256 newAllowance = token.allowance(address(this), spender).sub(value, \"SafeERC20: decreased allowance below zero\");
        _callOptionalReturn(token, abi.encodeWithSelector(token.approve.selector, spender, newAllowance));
    }

    /**
     * @dev Imitates a Solidity high-level call (i.e. a regular function call to a contract), relaxing the requirement
     * on the return value: the return value is optional (but if data is returned, it must not be false).
     * @param token The token targeted by the call.
     * @param data The call data (encoded using abi.encode or one of its variants).
     */
    function _callOptionalReturn(IERC20 token, bytes memory data) private {
        // We need to perform a low level call here, to bypass Solidity's return data size checking mechanism, since
        // we're implementing it ourselves. We use {Address.functionCall} to perform this call, which verifies that
        // the target address contains contract code and also asserts for success in the low-level call.

        bytes memory returndata = address(token).functionCall(data, \"SafeERC20: low-level call failed\");
        if (returndata.length > 0) { // Return data is optional
            // solhint-disable-next-line max-line-length
            require(abi.decode(returndata, (bool)), \"SafeERC20: ERC20 operation did not succeed\");
        }
    }
}
"
    
// SPDX-License-Identifier: MIT

pragma solidity ^0.7.0;

/**
 * @dev Collection of functions related to the address type
 */
library Address {
    /**
     * @dev Returns true if `account` is a contract.
     *
     * [IMPORTANT]
     * ====
     * It is unsafe to assume that an address for which this function returns
     * false is an externally-owned account (EOA) and not a contract.
     *
     * Among others, `isContract` will return false for the following
     * types of addresses:
     *
     *  - an externally-owned account
     *  - a contract in construction
     *  - an address where a contract will be created
     *  - an address where a contract lived, but was destroyed
     * ====
     */
    function isContract(address account) internal view returns (bool) {
        // This method relies on extcodesize, which returns 0 for contracts in
        // construction, since the code is only stored at the end of the
        // constructor execution.

        uint256 size;
        // solhint-disable-next-line no-inline-assembly
        assembly { size := extcodesize(account) }
        return size > 0;
    }

    /**
     * @dev Replacement for Solidity's `transfer`: sends `amount` wei to
     * `recipient`, forwarding all available gas and reverting on errors.
     *
     * https://eips.ethereum.org/EIPS/eip-1884[EIP1884] increases the gas cost
     * of certain opcodes, possibly making contracts go over the 2300 gas limit
     * imposed by `transfer`, making them unable to receive funds via
     * `transfer`. {sendValue} removes this limitation.
     *
     * https://diligence.consensys.net/posts/2019/09/stop-using-soliditys-transfer-now/[Learn more].
     *
     * IMPORTANT: because control is transferred to `recipient`, care must be
     * taken to not create reentrancy vulnerabilities. Consider using
     * {ReentrancyGuard} or the
     * https://solidity.readthedocs.io/en/v0.5.11/security-considerations.html#use-the-checks-effects-interactions-pattern[checks-effects-interactions pattern].
     */
    function sendValue(address payable recipient, uint256 amount) internal {
        require(address(this).balance >= amount, \"Address: insufficient balance\");

        // solhint-disable-next-line avoid-low-level-calls, avoid-call-value
        (bool success, ) = recipient.call{ value: amount }(\"\");
        require(success, \"Address: unable to send value, recipient may have reverted\");
    }

    /**
     * @dev Performs a Solidity function call using a low level `call`. A
     * plain`call` is an unsafe replacement for a function call: use this
     * function instead.
     *
     * If `target` reverts with a revert reason, it is bubbled up by this
     * function (like regular Solidity function calls).
     *
     * Returns the raw returned data. To convert to the expected return value,
     * use https://solidity.readthedocs.io/en/latest/units-and-global-variables.html?highlight=abi.decode#abi-encoding-and-decoding-functions[`abi.decode`].
     *
     * Requirements:
     *
     * - `target` must be a contract.
     * - calling `target` with `data` must not revert.
     *
     * _Available since v3.1._
     */
    function functionCall(address target, bytes memory data) internal returns (bytes memory) {
      return functionCall(target, data, \"Address: low-level call failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`], but with
     * `errorMessage` as a fallback revert reason when `target` reverts.
     *
     * _Available since v3.1._
     */
    function functionCall(address target, bytes memory data, string memory errorMessage) internal returns (bytes memory) {
        return functionCallWithValue(target, data, 0, errorMessage);
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`],
     * but also transferring `value` wei to `target`.
     *
     * Requirements:
     *
     * - the calling contract must have an ETH balance of at least `value`.
     * - the called Solidity function must be `payable`.
     *
     * _Available since v3.1._
     */
    function functionCallWithValue(address target, bytes memory data, uint256 value) internal returns (bytes memory) {
        return functionCallWithValue(target, data, value, \"Address: low-level call with value failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCallWithValue-address-bytes-uint256-}[`functionCallWithValue`], but
     * with `errorMessage` as a fallback revert reason when `target` reverts.
     *
     * _Available since v3.1._
     */
    function functionCallWithValue(address target, bytes memory data, uint256 value, string memory errorMessage) internal returns (bytes memory) {
        require(address(this).balance >= value, \"Address: insufficient balance for call\");
        require(isContract(target), \"Address: call to non-contract\");

        // solhint-disable-next-line avoid-low-level-calls
        (bool success, bytes memory returndata) = target.call{ value: value }(data);
        return _verifyCallResult(success, returndata, errorMessage);
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`],
     * but performing a static call.
     *
     * _Available since v3.3._
     */
    function functionStaticCall(address target, bytes memory data) internal view returns (bytes memory) {
        return functionStaticCall(target, data, \"Address: low-level static call failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-string-}[`functionCall`],
     * but performing a static call.
     *
     * _Available since v3.3._
     */
    function functionStaticCall(address target, bytes memory data, string memory errorMessage) internal view returns (bytes memory) {
        require(isContract(target), \"Address: static call to non-contract\");

        // solhint-disable-next-line avoid-low-level-calls
        (bool success, bytes memory returndata) = target.staticcall(data);
        return _verifyCallResult(success, returndata, errorMessage);
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`],
     * but performing a delegate call.
     *
     * _Available since v3.4._
     */
    function functionDelegateCall(address target, bytes memory data) internal returns (bytes memory) {
        return functionDelegateCall(target, data, \"Address: low-level delegate call failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-string-}[`functionCall`],
     * but performing a delegate call.
     *
     * _Available since v3.4._
     */
    function functionDelegateCall(address target, bytes memory data, string memory errorMessage) internal returns (bytes memory) {
        require(isContract(target), \"Address: delegate call to non-contract\");

        // solhint-disable-next-line avoid-low-level-calls
        (bool success, bytes memory returndata) = target.delegatecall(data);
        return _verifyCallResult(success, returndata, errorMessage);
    }

    function _verifyCallResult(bool success, bytes memory returndata, string memory errorMessage) private pure returns(bytes memory) {
        if (success) {
            return returndata;
        } else {
            // Look for revert reason and bubble it up if present
            if (returndata.length > 0) {
                // The easiest way to bubble the revert reason is using memory via assembly

                // solhint-disable-next-line no-inline-assembly
                assembly {
                    let returndata_size := mload(returndata)
                    revert(add(32, returndata), returndata_size)
                }
            } else {
                revert(errorMessage);
            }
        }
    }
}
"
    
// SPDX-License-Identifier: MIT

/**
 * Based on https://github.com/OpenZeppelin/openzeppelin-contracts-upgradeable/blob/v3.4.0-solc-0.7/contracts/access/OwnableUpgradeable.sol
 *
 * Changes:
 * - Added owner argument to initializer
 * - Reformatted styling in line with this repository.
 */

/*
The MIT License (MIT)

Copyright (c) 2016-2020 zOS Global Limited

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
\"Software\"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

/* solhint-disable func-name-mixedcase */

pragma solidity 0.7.6;

import \"@openzeppelin/contracts-upgradeable/utils/ContextUpgradeable.sol\";
import \"@openzeppelin/contracts-upgradeable/proxy/Initializable.sol\";

/**
 * @dev Contract module which provides a basic access control mechanism, where
 * there is an account (an owner) that can be granted exclusive access to
 * specific functions.
 *
 * By default, the owner account will be the one that deploys the contract. This
 * can later be changed with {transferOwnership}.
 *
 * This module is used through inheritance. It will make available the modifier
 * `onlyOwner`, which can be applied to your functions to restrict their use to
 * the owner.
 */
abstract contract OwnableUpgradeable is Initializable, ContextUpgradeable {
\taddress private _owner;

\tevent OwnershipTransferred(
\t\taddress indexed previousOwner,
\t\taddress indexed newOwner
\t);

\t/**
\t * @dev Initializes the contract setting the deployer as the initial owner.
\t */
\tfunction __Ownable_init(address owner_) internal initializer {
\t\t__Context_init_unchained();
\t\t__Ownable_init_unchained(owner_);
\t}

\tfunction __Ownable_init_unchained(address owner_) internal initializer {
\t\t_owner = owner_;
\t\temit OwnershipTransferred(address(0), owner_);
\t}

\t/**
\t * @dev Returns the address of the current owner.
\t */
\tfunction owner() public view virtual returns (address) {
\t\treturn _owner;
\t}

\t/**
\t * @dev Throws if called by any account other than the owner.
\t */
\tmodifier onlyOwner() {
\t\trequire(owner() == _msgSender(), \"Ownable: caller is not the owner\");
\t\t_;
\t}

\t/**
\t * @dev Leaves the contract without owner. It will not be possible to call
\t * `onlyOwner` functions anymore. Can only be called by the current owner.
\t *
\t * NOTE: Renouncing ownership will leave the contract without an owner,
\t * thereby removing any functionality that is only available to the owner.
\t */
\tfunction renounceOwnership() public virtual onlyOwner {
\t\temit OwnershipTransferred(_owner, address(0));
\t\t_owner = address(0);
\t}

\t/**
\t * @dev Transfers ownership of the contract to a new account (`newOwner`).
\t * Can only be called by the current owner.
\t */
\tfunction transferOwnership(address newOwner) public virtual onlyOwner {
\t\trequire(newOwner != address(0), \"Ownable: new owner is the zero address\");
\t\temit OwnershipTransferred(_owner, newOwner);
\t\t_owner = newOwner;
\t}

\tuint256[49] private __gap;
}
"
    
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

import \"@openzeppelin/contracts/utils/Address.sol\";
import \"@openzeppelin/contracts-upgradeable/utils/ContextUpgradeable.sol\";
import \"@openzeppelin/contracts/token/ERC20/IERC20.sol\";
import \"@openzeppelin/contracts-upgradeable/proxy/Initializable.sol\";
import \"@openzeppelin/contracts-upgradeable/utils/PausableUpgradeable.sol\";
import \"@openzeppelin/contracts/token/ERC20/SafeERC20.sol\";
import \"@openzeppelin/contracts/math/SafeMath.sol\";

import \"./ETHtxAMMData.sol\";
import \"../interfaces/IETHtxAMM.sol\";
import \"../../tokens/interfaces/IETHtx.sol\";
import \"../../tokens/interfaces/IERC20TxFee.sol\";
import \"../../tokens/interfaces/IWETH.sol\";
import \"../../rewards/interfaces/IFeeLogic.sol\";
import \"../../oracles/interfaces/IGasPrice.sol\";
import \"../../access/OwnableUpgradeable.sol\";

contract ETHtxAMM is
\tInitializable,
\tContextUpgradeable,
\tOwnableUpgradeable,
\tPausableUpgradeable,
\tETHtxAMMData,
\tIETHtxAMM
{
\tusing Address for address payable;
\tusing SafeERC20 for IERC20;
\tusing SafeMath for uint128;
\tusing SafeMath for uint256;

\tstruct ETHtxAMMArgs {
\t\taddress ethtx;
\t\taddress gasOracle;
\t\taddress weth;
\t\tuint128 targetCRatioNum;
\t\tuint128 targetCRatioDen;
\t}

\t/* Constructor */

\tconstructor(address owner_) {
\t\tinit(owner_);
\t}

\t/* Initializer */

\tfunction init(address owner_) public virtual initializer {
\t\t__Context_init_unchained();
\t\t__Ownable_init_unchained(owner_);
\t\t__Pausable_init_unchained();
\t}

\tfunction postInit(ETHtxAMMArgs memory _args) external virtual onlyOwner {
\t\taddress sender = _msgSender();

\t\t_ethtx = _args.ethtx;
\t\temit ETHtxSet(sender, _args.ethtx);

\t\t_gasOracle = _args.gasOracle;
\t\temit GasOracleSet(sender, _args.gasOracle);

\t\t_weth = _args.weth;
\t\temit WETHSet(sender, _args.weth);

\t\t_targetCRatioNum = _args.targetCRatioNum;
\t\t_targetCRatioDen = _args.targetCRatioDen;
\t\temit TargetCRatioSet(sender, _args.targetCRatioNum, _args.targetCRatioDen);
\t}

\t/* Fallbacks */

\treceive() external payable {
\t\t// Only accept ETH via fallback from the WETH contract
\t\taddress weth_ = weth();
\t\tif (msg.sender != weth_) {
\t\t\t// Otherwise try to convert it to WETH
\t\t\tIWETH(weth_).deposit{ value: msg.value }();
\t\t}
\t}

\t/* Modifiers */

\tmodifier ensure(uint256 deadline) {
\t\t// solhint-disable-next-line not-rely-on-time
\t\trequire(deadline >= block.timestamp, \"ETHtxAMM: expired\");
\t\t_;
\t}

\tmodifier priceIsFresh() {
\t\trequire(
\t\t\t!IGasPrice(gasOracle()).hasPriceExpired(),
\t\t\t\"ETHtxAMM: gas price is outdated\"
\t\t);
\t\t_;
\t}

\t/* External Mutators */

\tfunction swapEthForEthtx(uint256 deadline)
\t\texternal
\t\tpayable
\t\tvirtual
\t\toverride
\t{
\t\t_swapEthForEthtxRaw(msg.value, deadline, false);
\t}

\tfunction swapWethForEthtx(uint256 amountIn, uint256 deadline)
\t\texternal
\t\tvirtual
\t\toverride
\t{
\t\t_swapEthForEthtxRaw(amountIn, deadline, true);
\t}

\tfunction swapEthForExactEthtx(uint256 amountOut, uint256 deadline)
\t\texternal
\t\tpayable
\t\tvirtual
\t\toverride
\t{
\t\tuint256 amountInMax = msg.value;
\t\tuint256 amountIn =
\t\t\t_swapEthForExactEthtx(amountInMax, amountOut, deadline, false);
\t\t// refund leftover ETH
\t\tif (amountInMax != amountIn) {
\t\t\tpayable(_msgSender()).sendValue(amountInMax - amountIn);
\t\t}
\t}

\tfunction swapWethForExactEthtx(
\t\tuint256 amountInMax,
\t\tuint256 amountOut,
\t\tuint256 deadline
\t) external virtual override {
\t\t_swapEthForExactEthtx(amountInMax, amountOut, deadline, true);
\t}

\tfunction swapExactEthForEthtx(uint256 amountOutMin, uint256 deadline)
\t\texternal
\t\tpayable
\t\tvirtual
\t\toverride
\t{
\t\t_swapExactEthForEthtx(msg.value, amountOutMin, deadline, false);
\t}

\tfunction swapExactWethForEthtx(
\t\tuint256 amountIn,
\t\tuint256 amountOutMin,
\t\tuint256 deadline
\t) external virtual override {
\t\t_swapExactEthForEthtx(amountIn, amountOutMin, deadline, true);
\t}

\tfunction swapEthtxForEth(
\t\tuint256 amountIn,
\t\tuint256 deadline,
\t\tbool asWETH
\t) external virtual override ensure(deadline) priceIsFresh {
\t\trequire(amountIn != 0, \"ETHtxAMM: cannot swap zero\");
\t\tuint256 amountOut = exactEthtxToEth(amountIn);
\t\t_swapEthtxForEth(_msgSender(), amountIn, amountOut, asWETH);
\t}

\tfunction swapEthtxForExactEth(
\t\tuint256 amountInMax,
\t\tuint256 amountOut,
\t\tuint256 deadline,
\t\tbool asWETH
\t) external virtual override ensure(deadline) priceIsFresh {
\t\trequire(amountInMax != 0, \"ETHtxAMM: cannot swap zero\");
\t\tuint256 amountIn = ethtxToExactEth(amountOut);
\t\trequire(amountIn <= amountInMax, \"ETHtxAMM: amountIn exceeds max\");
\t\t_swapEthtxForEth(_msgSender(), amountIn, amountOut, asWETH);
\t}

\tfunction swapExactEthtxForEth(
\t\tuint256 amountIn,
\t\tuint256 amountOutMin,
\t\tuint256 deadline,
\t\tbool asWETH
\t) external virtual override ensure(deadline) priceIsFresh {
\t\trequire(amountIn != 0, \"ETHtxAMM: cannot swap zero\");
\t\tuint256 amountOut = exactEthtxToEth(amountIn);
\t\trequire(amountOut >= amountOutMin, \"ETHtxAMM: amountOut below min\");
\t\t_swapEthtxForEth(_msgSender(), amountIn, amountOut, asWETH);
\t}

\tfunction pause() external virtual override onlyOwner whenNotPaused {
\t\t_pause();
\t}

\tfunction recoverUnsupportedERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external virtual override onlyOwner {
\t\trequire(token != weth(), \"ETHtxAMM: cannot recover WETH\");
\t\trequire(token != ethtx(), \"ETHtxAMM: cannot recover ETHtx\");

\t\tIERC20(token).safeTransfer(to, amount);
\t\temit RecoveredUnsupported(_msgSender(), token, to, amount);
\t}

\tfunction setEthtx(address account) public virtual override onlyOwner {
\t\trequire(account != address(0), \"ETHtxAMM: ETHtx zero address\");
\t\t_ethtx = account;
\t\temit ETHtxSet(_msgSender(), account);
\t}

\tfunction setGasOracle(address account) public virtual override onlyOwner {
\t\trequire(account != address(0), \"ETHtxAMM: gasOracle zero address\");
\t\t_gasOracle = account;
\t\temit GasOracleSet(_msgSender(), account);
\t}

\tfunction setTargetCRatio(uint128 numerator, uint128 denominator)
\t\tpublic
\t\tvirtual
\t\toverride
\t\tonlyOwner
\t{
\t\trequire(numerator != 0, \"ETHtxAMM: targetCRatio numerator is zero\");
\t\trequire(denominator != 0, \"ETHtxAMM: targetCRatio denominator is zero\");
\t\t_targetCRatioNum = numerator;
\t\t_targetCRatioDen = denominator;
\t\temit TargetCRatioSet(_msgSender(), numerator, denominator);
\t}

\tfunction setWETH(address account) public virtual override onlyOwner {
\t\trequire(account != address(0), \"ETHtxAMM: WETH zero address\");
\t\t_weth = account;
\t\temit WETHSet(_msgSender(), account);
\t}

\tfunction unpause() external virtual override onlyOwner whenPaused {
\t\t_unpause();
\t}

\t/* Public Pure */

\tfunction gasPerETHtx() public pure virtual override returns (uint256) {
\t\treturn 21000; // Per 1e18
\t}

\t/* Public Views */

\tfunction cRatio()
\t\tpublic
\t\tview
\t\tvirtual
\t\toverride
\t\treturns (uint256 numerator, uint256 denominator)
\t{
\t\tnumerator = ethSupply();
\t\tdenominator = ethToExactEthtx(ethtxOutstanding());
\t}

\tfunction cRatioBelowTarget() public view virtual override returns (bool) {
\t\t(uint256 cRatioNum, uint256 cRatioDen) = cRatio();
\t\tif (cRatioDen == 0) {
\t\t\treturn false;
\t\t}

\t\tuint256 current = cRatioNum.mul(1e18) / cRatioDen;

\t\t(uint256 targetNum, uint256 targetDen) = targetCRatio();
\t\tuint256 target = targetNum.mul(1e18).div(targetDen);

\t\treturn current < target;
\t}

\tfunction ethNeeded() external view virtual override returns (uint256) {
\t\t(uint256 ethSupply_, uint256 ethOut) = cRatio();
\t\t(uint128 targetNum, uint128 targetDen) = targetCRatio();

\t\tuint256 target = ethOut.mul(targetNum).div(targetDen);

\t\tif (ethSupply_ > target) {
\t\t\treturn 0;
\t\t}

\t\treturn target - ethSupply_;
\t}

\tfunction ethtx() public view virtual override returns (address) {
\t\treturn _ethtx;
\t}

\tfunction exactEthToEthtx(uint256 amountEthIn)
\t\tpublic
\t\tview
\t\tvirtual
\t\toverride
\t\treturns (uint256)
\t{
\t\treturn _ethToEthtx(gasPrice(), amountEthIn);
\t}

\tfunction ethToExactEthtx(uint256 amountEthtxOut)
\t\tpublic
\t\tview
\t\tvirtual
\t\toverride
\t\treturns (uint256)
\t{
\t\treturn _ethtxToEth(gasPrice(), amountEthtxOut);
\t}

\tfunction exactEthtxToEth(uint256 amountEthtxIn)
\t\tpublic
\t\tview
\t\tvirtual
\t\toverride
\t\treturns (uint256)
\t{
\t\t// Account for fee
\t\tuint256 fee =
\t\t\tIFeeLogic(feeLogic()).getFee(_msgSender(), address(this), amountEthtxIn);

\t\treturn _ethtxToEth(gasPriceAtRedemption(), amountEthtxIn.sub(fee));
\t}

\tfunction ethtxToExactEth(uint256 amountEthOut)
\t\tpublic
\t\tview
\t\tvirtual
\t\toverride
\t\treturns (uint256)
\t{
\t\tuint256 amountEthtx = _ethToEthtx(gasPriceAtRedemption(), amountEthOut);

\t\t// Account for fee
\t\tuint256 amountBeforeFee =
\t\t\tIFeeLogic(feeLogic()).undoFee(_msgSender(), address(this), amountEthtx);

\t\treturn amountBeforeFee;
\t}

\tfunction ethSupply() public view virtual override returns (uint256) {
\t\treturn IERC20(weth()).balanceOf(address(this));
\t}

\tfunction ethSupplyTarget() external view virtual override returns (uint256) {
\t\t(uint128 targetNum, uint128 targetDen) = targetCRatio();
\t\treturn ethToExactEthtx(ethtxOutstanding()).mul(targetNum).div(targetDen);
\t}

\tfunction ethtxAvailable() public view virtual override returns (uint256) {
\t\treturn IERC20(ethtx()).balanceOf(address(this));
\t}

\tfunction ethtxOutstanding() public view virtual override returns (uint256) {
\t\treturn IERC20(ethtx()).totalSupply().sub(ethtxAvailable());
\t}

\tfunction feeLogic() public view virtual override returns (address) {
\t\treturn IERC20TxFee(ethtx()).feeLogic();
\t}

\tfunction gasOracle() public view virtual override returns (address) {
\t\treturn _gasOracle;
\t}

\tfunction gasPrice() public view virtual override returns (uint256) {
\t\treturn IGasPrice(gasOracle()).gasPrice();
\t}

\tfunction gasPriceAtRedemption()
\t\tpublic
\t\tview
\t\tvirtual
\t\toverride
\t\treturns (uint256)
\t{
\t\t// Apply cap when collateral below target
\t\tuint256 gasPrice_ = gasPrice();
\t\tuint256 maxGasPrice_ = maxGasPrice();
\t\tif (gasPrice_ > maxGasPrice_) {
\t\t\tgasPrice_ = maxGasPrice_;
\t\t}
\t\treturn gasPrice_;
\t}

\tfunction maxGasPrice() public view virtual override returns (uint256) {
\t\tuint256 liability = ethtxOutstanding();
\t\tif (liability == 0) {
\t\t\treturn gasPrice();
\t\t}

\t\t(uint128 targetNum, uint128 targetDen) = targetCRatio();

\t\tuint256 numerator = ethSupply().mul(1e18).mul(targetDen);
\t\tuint256 denominator = liability.mul(gasPerETHtx()).mul(targetNum);
\t\treturn numerator.div(denominator);
\t}

\tfunction targetCRatio()
\t\tpublic
\t\tview
\t\tvirtual
\t\toverride
\t\treturns (uint128 numerator, uint128 denominator)
\t{
\t\tnumerator = _targetCRatioNum;
\t\tdenominator = _targetCRatioDen;
\t}

\tfunction weth() public view virtual override returns (address) {
\t\treturn _weth;
\t}

\t/* Internal Pure */

\tfunction _ethtxToEth(uint256 gasPrice_, uint256 amountETHtx)
\t\tinternal
\t\tpure
\t\tvirtual
\t\treturns (uint256)
\t{
\t\treturn gasPrice_.mul(amountETHtx).mul(gasPerETHtx()) / 1e18;
\t}

\tfunction _ethToEthtx(uint256 gasPrice_, uint256 amountETH)
\t\tinternal
\t\tpure
\t\tvirtual
\t\treturns (uint256)
\t{
\t\trequire(gasPrice_ != 0, \"ETHtxAMM: gasPrice is zero\");
\t\treturn amountETH.mul(1e18) / gasPrice_.mul(gasPerETHtx());
\t}

\t/* Internal Mutators */

\tfunction _swapEthForEthtxRaw(
\t\tuint256 amountIn,
\t\tuint256 deadline,
\t\tbool useWETH
\t) internal virtual ensure(deadline) priceIsFresh {
\t\trequire(amountIn != 0, \"ETHtxAMM: cannot swap zero\");
\t\tuint256 amountOut = exactEthToEthtx(amountIn);
\t\t_swapEthForEthtx(_msgSender(), amountIn, amountOut, useWETH);
\t}

\tfunction _swapEthForExactEthtx(
\t\tuint256 amountInMax,
\t\tuint256 amountOut,
\t\tuint256 deadline,
\t\tbool useWETH
\t) internal virtual ensure(deadline) priceIsFresh returns (uint256 amountIn) {
\t\trequire(amountInMax != 0, \"ETHtxAMM: cannot swap zero\");
\t\t// Add 1 to account for rounding (can't get ETHtx for 0 wei)
\t\tamountIn = ethToExactEthtx(amountOut).add(1);
\t\trequire(amountIn <= amountInMax, \"ETHtxAMM: amountIn exceeds max\");
\t\t_swapEthForEthtx(_msgSender(), amountIn, amountOut, useWETH);
\t}

\tfunction _swapExactEthForEthtx(
\t\tuint256 amountIn,
\t\tuint256 amountOutMin,
\t\tuint256 deadline,
\t\tbool useWETH
\t) internal virtual ensure(deadline) priceIsFresh {
\t\trequire(amountIn != 0, \"ETHtxAMM: cannot swap zero\");
\t\tuint256 amountOut = exactEthToEthtx(amountIn);
\t\trequire(amountOut >= amountOutMin, \"ETHtxAMM: amountOut below min\");
\t\t_swapEthForEthtx(_msgSender(), amountIn, amountOut, useWETH);
\t}

\tfunction _swapEthForEthtx(
\t\taddress account,
\t\tuint256 amountIn,
\t\tuint256 amountOut,
\t\tbool useWETH
\t) internal virtual {
\t\tuint256 availableSupply = IERC20(ethtx()).balanceOf(address(this));
\t\trequire(
\t\t\tavailableSupply >= amountOut,
\t\t\t\"ETHtxAMM: not enough ETHtx available\"
\t\t);

\t\tif (useWETH) {
\t\t\tIERC20(weth()).safeTransferFrom(account, address(this), amountIn);
\t\t} else {
\t\t\tIWETH(weth()).deposit{ value: amountIn }();
\t\t}

\t\t// Bypass fee by setting exemption for AMM contract
\t\tIERC20(ethtx()).safeTransfer(account, amountOut);
\t}

\tfunction _swapEthtxForEth(
\t\taddress account,
\t\tuint256 amountIn,
\t\tuint256 amountOut,
\t\tbool asWETH
\t) internal virtual {
\t\t// Apply fee
\t\tIERC20(ethtx()).safeTransferFrom(account, address(this), amountIn);

\t\tif (asWETH) {
\t\t\tIERC20(weth()).safeTransfer(account, amountOut);
\t\t} else {
\t\t\tIWETH(weth()).withdraw(amountOut);
\t\t\tpayable(account).sendValue(amountOut);
\t\t}
\t}
}
"
    
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

abstract contract ETHtxAMMData {
\taddress internal _gasOracle;
\tuint128 internal _targetCRatioNum;
\tuint128 internal _targetCRatioDen;
\taddress internal _ethtx;
\taddress internal _weth;

\tuint256[46] private __gap;
}
"
    
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

interface IETHtxAMM {
\t/* Views */

\tfunction cRatio()
\t\texternal
\t\tview
\t\treturns (uint256 numerator, uint256 denominator);

\tfunction cRatioBelowTarget() external view returns (bool);

\tfunction ethNeeded() external view returns (uint256);

\tfunction ethtx() external view returns (address);

\tfunction exactEthToEthtx(uint256 amountEthIn)
\t\texternal
\t\tview
\t\treturns (uint256);

\tfunction ethToExactEthtx(uint256 amountEthtxOut)
\t\texternal
\t\tview
\t\treturns (uint256);

\tfunction exactEthtxToEth(uint256 amountEthtxIn)
\t\texternal
\t\tview
\t\treturns (uint256);

\tfunction ethtxToExactEth(uint256 amountEthOut)
\t\texternal
\t\tview
\t\treturns (uint256);

\tfunction ethSupply() external view returns (uint256);

\tfunction ethSupplyTarget() external view returns (uint256);

\tfunction ethtxAvailable() external view returns (uint256);

\tfunction ethtxOutstanding() external view returns (uint256);

\tfunction feeLogic() external view returns (address);

\tfunction gasOracle() external view returns (address);

\tfunction gasPerETHtx() external pure returns (uint256);

\tfunction gasPrice() external view returns (uint256);

\tfunction gasPriceAtRedemption() external view returns (uint256);

\tfunction maxGasPrice() external view returns (uint256);

\tfunction targetCRatio()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction weth() external view returns (address);

\t/* Mutators */

\tfunction swapEthForEthtx(uint256 deadline) external payable;

\tfunction swapWethForEthtx(uint256 amountIn, uint256 deadline) external;

\tfunction swapEthForExactEthtx(uint256 amountOut, uint256 deadline)
\t\texternal
\t\tpayable;

\tfunction swapWethForExactEthtx(
\t\tuint256 amountInMax,
\t\tuint256 amountOut,
\t\tuint256 deadline
\t) external;

\tfunction swapExactEthForEthtx(uint256 amountOutMin, uint256 deadline)
\t\texternal
\t\tpayable;

\tfunction swapExactWethForEthtx(
\t\tuint256 amountIn,
\t\tuint256 amountOutMin,
\t\tuint256 deadline
\t) external;

\tfunction swapEthtxForEth(
\t\tuint256 amountIn,
\t\tuint256 deadline,
\t\tbool asWETH
\t) external;

\tfunction swapEthtxForExactEth(
\t\tuint256 amountInMax,
\t\tuint256 amountOut,
\t\tuint256 deadline,
\t\tbool asWETH
\t) external;

\tfunction swapExactEthtxForEth(
\t\tuint256 amountIn,
\t\tuint256 amountOutMin,
\t\tuint256 deadline,
\t\tbool asWETH
\t) external;

\tfunction pause() external;

\tfunction recoverUnsupportedERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction setEthtx(address account) external;

\tfunction setGasOracle(address account) external;

\tfunction setTargetCRatio(uint128 numerator, uint128 denominator) external;

\tfunction setWETH(address account) external;

\tfunction unpause() external;

\t/* Events */

\tevent ETHtxSet(address indexed author, address indexed account);
\tevent GasOracleSet(address indexed author, address indexed account);
\tevent RecoveredUnsupported(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
\tevent TargetCRatioSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent WETHSet(address indexed author, address indexed account);
}
"
    
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

interface IGasPrice {
\t/* Views */

\tfunction gasPrice() external view returns (uint256);

\tfunction hasPriceExpired() external view returns (bool);

\tfunction updateThreshold() external view returns (uint256);

\tfunction updatedAt() external view returns (uint256);

\t/* Mutators */

\tfunction setGasPrice(uint256 _gasPrice) external;

\tfunction setUpdateThreshold(uint256 _updateThreshold) external;

\t/* Events */

\tevent GasPriceUpdate(address indexed author, uint256 newValue);
\tevent UpdateThresholdSet(address indexed author, uint256 value);
}
"
    
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IFeeLogic {
\t/* Types */

\tstruct ExemptData {
\t\taddress account;
\t\tbool isExempt;
\t}

\t/* Views */

\tfunction exemptsAt(uint256 index) external view returns (address);

\tfunction exemptsLength() external view returns (uint256);

\tfunction feeRate()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction getFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\tfunction isExempt(address account) external view returns (bool);

\tfunction recipient() external view returns (address);

\tfunction undoFee(
\t\taddress sender,
\t\taddress recipient_,
\t\tuint256 amount
\t) external view returns (uint256);

\t/* Mutators */

\tfunction notify(uint256 amount) external;

\tfunction setExempt(address account, bool isExempt_) external;

\tfunction setExemptBatch(ExemptData[] memory batch) external;

\tfunction setFeeRate(uint128 numerator, uint128 denominator) external;

\tfunction setRecipient(address account) external;

\t/* Events */

\tevent ExemptAdded(address indexed author, address indexed account);
\tevent ExemptRemoved(address indexed author, address indexed account);
\tevent FeeRateSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent RecipientSet(address indexed author, address indexed account);
}
"
    
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

interface IERC20TxFee {
\t/* Views */

\tfunction feeLogic() external view returns (address);
}
"
    
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

interface IETHtx {
\t/* Views */

\tfunction minter() external view returns (address);

\t/* Mutators */

\tfunction burn(address account, uint256 amount) external;

\tfunction mint(address account, uint256 amount) external;

\tfunction pause() external;

\tfunction recoverERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction setFeeLogic(address account) external;

\tfunction setMinter(address account) external;

\tfunction unpause() external;

\t/* Events */

\tevent FeeLogicSet(address indexed author, address indexed account);
\tevent MinterSet(address indexed author, address indexed account);
\tevent Recovered(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
}
"
    
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
