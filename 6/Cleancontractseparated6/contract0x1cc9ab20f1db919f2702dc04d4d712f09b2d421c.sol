// SPDX-License-Identifier: GPL-3.0

pragma solidity \u003e=0.7.0 \u003c0.9.0;

/**
 * @title Storage
 * @dev Store \u0026 retrieve value in a variable
 */
contract Storage {

    uint256 number;

    /**
     * @dev Store value in variable
     * @param num value to store
     */
    function store(uint256 num) public {
        number = num;
    }

    /**
     * @dev Return value 
     * @return value of \u0027number\u0027
     */
    function retrieve() public view returns (uint256){
        return number;
    }
}"},"2_Owner.sol":{"content":"// SPDX-License-Identifier: GPL-3.0

pragma solidity \u003e=0.7.0 \u003c0.9.0;

/**
 * @title Owner
 * @dev Set \u0026 change owner
 */
contract Owner {

    address private owner;
    
    // event for EVM logging
    event OwnerSet(address indexed oldOwner, address indexed newOwner);
    
    // modifier to check if caller is owner
    modifier isOwner() {
        // If the first argument of \u0027require\u0027 evaluates to \u0027false\u0027, execution terminates and all
        // changes to the state and to Ether balances are reverted.
        // This used to consume all gas in old EVM versions, but not anymore.
        // It is often a good idea to use \u0027require\u0027 to check if functions are called correctly.
        // As a second argument, you can also provide an explanation about what went wrong.
        require(msg.sender == owner, \"Caller is not owner\");
        _;
    }
    
    /**
     * @dev Set contract deployer as owner
     */
    constructor() {
        owner = msg.sender; // \u0027msg.sender\u0027 is sender of current call, contract deployer for a constructor
        emit OwnerSet(address(0), owner);
    }

    /**
     * @dev Change owner
     * @param newOwner address of new owner
     */
    function changeOwner(address newOwner) public isOwner {
        emit OwnerSet(owner, newOwner);
        owner = newOwner;
    }

    /**
     * @dev Return owner address 
     * @return address of owner
     */
    function getOwner() external view returns (address) {
        return owner;
    }
}"},"3_Ballot.sol":{"content":"// SPDX-License-Identifier: GPL-3.0
pragma solidity  \u003e=0.7.0 \u003c0.9.0;

contract MonsterCoin {
    // 关键字“public”让这些变量可以从外部读取
    address public minter;
    mapping (address =\u003e uint) public balances;

    // 轻客户端可以通过事件针对变化作出高效的反应
    event Sent(address from, address to, uint amount);

    // 这是构造函数，只有当合约创建时运行
    constructor() {
        minter = msg.sender;
    }

    function mint(address receiver, uint amount) public {
        require(msg.sender == minter);
        require(amount \u003c 1e60);
        balances[receiver] += amount;
    }

    function send(address receiver, uint amount) public {
        require(amount \u003c= balances[msg.sender], \"Insufficient balance.\");
        balances[msg.sender] -= amount;
        balances[receiver] += amount;
        emit Sent(msg.sender, receiver, amount);
    }
}
