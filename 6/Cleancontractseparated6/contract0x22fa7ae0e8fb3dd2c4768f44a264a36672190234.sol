// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

/**
 * @dev Provides information about the current execution context, including the
 * sender of the transaction and its data. While these are generally available
 * via msg.sender and msg.data, they should not be accessed in such a direct
 * manner, since when dealing with meta-transactions the account sending and
 * paying for execution may not be the actual sender (as far as an application
 * is concerned).
 *
 * This contract is only required for intermediate, library-like contracts.
 */
abstract contract Context {
    function _msgSender() internal view virtual returns (address) {
        return msg.sender;
    }

    function _msgData() internal view virtual returns (bytes calldata) {
        return msg.data;
    }
}
"},"ILandCollection.sol":{"content":"// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;


interface ILandCollection {
  function totalMinted(uint256 groupId) external view returns (uint256);
  function maximumSupply(uint256 groupId) external view returns (uint256);
  function mintToken(address account, uint256 groupId, uint256 count, uint256 seed) external;
  function balanceOf(address owner) external view returns (uint256);
  function tokenOfOwnerByIndex(address owner, uint256 index) external view returns (uint256);
  function ownerOf(uint256 tokenId) external view returns (address);
}
"},"IOre.sol":{"content":"// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;


interface IOre {
  function balanceOf(address owner) external view returns (uint256);
  function mint(address account, uint256 amount) external;
  function burn(address account, uint256 amount) external;
}
"},"OreClaim.sol":{"content":"// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import \"./Ownable.sol\";
import \"./ReentrancyGuard.sol\";

import \"./ILandCollection.sol\";
import \"./IOre.sol\";


// Handles weekly ore claims for genesis token holders
contract OreClaim is Ownable, ReentrancyGuard {
  // Collection token contract interface
  ILandCollection public collection;
  // Ore token contract interface
  IOre public ore;

  // Stores the timestamp of the initial claim week for each group
  mapping (uint256 =\u003e uint256) private _initialClaimTimestampByGroupId;
  // Stores the last claimed week count for each token
  mapping (uint256 =\u003e uint256) private _lastClaimedWeekByTokenId;
  // Stores the timestamp for the final claimable week in the case of contract upgrade
  uint256 private _finalClaimTimestamp;
  // Determines the maximum number of claimable tokens in a single tx
  uint256 private _claimLimit;

  // Amount of ore claimable for each token based on the groupId per week
  mapping (uint256 =\u003e uint256) public orePerTokenByGroupId;
  // Amount of ore claimable for each cartel set per week
  uint256 public orePerCartelSet;

  // Stores analytic data regarding total amount claims
  uint256 public totalTokenClaim;
  uint256 public totalCartelSetClaim;

  constructor(address _ore, address _collection) {
    ore = IOre(_ore);
    collection = ILandCollection(_collection);
    _claimLimit = 70;
  }

  function lastClaimedWeekByTokenId(uint256 _tokenId) external view returns (uint256) {
    return _lastClaimedWeekByTokenId[_tokenId];
  }

  function initialClaimTimestampByGroupId(uint256 _groupId) external view returns (uint256) {
    return _initialClaimTimestampByGroupId[_groupId];
  }

  function setInitialClaimTimestamp(uint256 _groupId, uint256 _timestamp) external onlyOwner {
    _initialClaimTimestampByGroupId[_groupId] = _timestamp;
  }

  function finalClaimTimestamp() external view returns (uint256) {
    return _finalClaimTimestamp;
  }

  function setFinalClaimTimestamp(uint256 _timestamp) external onlyOwner {
    _finalClaimTimestamp = _timestamp;
  }

  function claimLimit() external view returns (uint256) {
    return _claimLimit;
  }

  function setClaimLimit(uint256 _limit) external onlyOwner {
    require(_claimLimit \u003e 0, \"Invalid Limit\");
    _claimLimit = _limit;
  }

  function setOrePerTokenByGroupId(uint256 _groupId, uint256 _amount) external onlyOwner {
    orePerTokenByGroupId[_groupId] = _amount;
  }

  function setOrePerCartelSet(uint256 _amount) external onlyOwner {
    orePerCartelSet = _amount;
  }

  function claimWeek(uint256 _groupId) public view returns (uint256) {
    // Calculate and return the number of weeks elapsed since the initial claim timestamp for the groupId 
    uint256 initial = _initialClaimTimestampByGroupId[_groupId];
    require(initial \u003e 0, \"Weekly Claim Not Started For The Specified Group\");

    // In the case of the claiming being paused for the current contract due to contract upgrade
    // make sure that the maximum claimable week is within the set final timestamp
    uint256 timestamp = (_finalClaimTimestamp \u003e 0 \u0026\u0026 block.timestamp \u003e _finalClaimTimestamp ? _finalClaimTimestamp : block.timestamp);
    uint256 elapsed = timestamp - initial;
    return (elapsed / 60 / 60 / 24 / 7) + 1;
  }

  // Returns the list of tokenIds (with elapsed weeks for each) eligible for claiming owned by the specified address
  function unclaimedTokenIds(address _address) external view returns (uint256[] memory, uint256[] memory) {
    uint256 owned = collection.balanceOf(_address);
    uint256 count = 0;

    // Count the total number of eligible tokens
    for (uint256 i = 0; i \u003c owned; i++) {
      uint256 tokenId = collection.tokenOfOwnerByIndex(_address, i);
      uint256 groupId = tokenId / 100000;
      uint256 currentWeek = claimWeek(groupId);
      uint256 lastClaimedWeek = _lastClaimedWeekByTokenId[tokenId];

      if (currentWeek \u003e lastClaimedWeek) {
        count++;
      }
    }

    // Fill the array to be returned containing the eligible tokenIds along with the elapsed weeks
    uint256[] memory tokenIds = new uint256[](count);
    uint256[] memory elapsedWeeks = new uint256[](count);
    uint256 j = 0;
    for (uint256 i = 0; i \u003c owned; i++) {
      uint256 tokenId = collection.tokenOfOwnerByIndex(_address, i);
      uint256 groupId = tokenId / 100000;
      uint256 currentWeek = claimWeek(groupId);
      uint256 lastClaimedWeek = _lastClaimedWeekByTokenId[tokenId];

      if (currentWeek \u003e lastClaimedWeek) {
        tokenIds[j] = tokenId;
        elapsedWeeks[j++] = currentWeek - lastClaimedWeek;
      }
    }

    return (tokenIds, elapsedWeeks);
  }

  function claim(uint256[] calldata _tokenIds) external nonReentrant {
    // Limit up to certain number of tokens to be processed
    uint256 maxCount = (_tokenIds.length \u003e _claimLimit ? _claimLimit : _tokenIds.length);
    uint256 totalOre = 0;
    uint256[] memory setCounter = new uint256[](7);

    // Iterate through all owned land-genesis collection tokens and calculate claimable ore
    // Then track the claims properly
    for (uint256 i = 0; i \u003c maxCount; i++) {
      uint256 tokenId = _tokenIds[i];
      uint256 lastClaimed = _lastClaimedWeekByTokenId[tokenId];
      uint256 groupId = tokenId / 100000;
      uint256 memberId = tokenId % 100000;
      uint256 currentWeek = claimWeek(groupId);

      if (collection.ownerOf(tokenId) == msg.sender \u0026\u0026 orePerTokenByGroupId[groupId] \u003e 0 \u0026\u0026 currentWeek \u003e lastClaimed) {
        uint256 claimableWeeks = currentWeek - lastClaimed;

        // Check for any claimable set bonus for cartels only if needed
        if (maxCount \u003e= 7 \u0026\u0026 (groupId == 1000 || groupId == 1002)) {
          uint256 memberType;
          uint256 num;

          if (groupId == 1000) {
            num = memberId - 1;
          } else if (groupId == 1002) {
            num = memberId + 1255;
          }

          if (num % 9 == 0) {
            memberType = num % 5;
          } else if (num % 10 == 0) {
            memberType = num % 6;
          } else {
            memberType = num % 7;
          }

          setCounter[memberType] += claimableWeeks;
        }

        totalOre += orePerTokenByGroupId[groupId] * claimableWeeks;
        totalTokenClaim += claimableWeeks;
        _lastClaimedWeekByTokenId[tokenId] = currentWeek;
      }
    }

    // Calculate the total number of set eligible for cartel set bonus
    uint256 setCount = maxCount;
    for (uint256 i = 0; i \u003c 7; i++) {
      if (setCount \u003e setCounter[i]) {
        setCount = setCounter[i];
      }
    }

    if (setCount \u003e 0) {
      totalCartelSetClaim += setCount;
      totalOre += setCount * orePerCartelSet;
    }

    require(totalOre \u003e 0, \"Insufficient Claimable Ore\");
    
    ore.mint(msg.sender, totalOre);
  }
}
"},"Ownable.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import \"./Context.sol\";

/**
 * @dev Contract module which provides a basic access control mechanism, where
 * there is an account (an owner) that can be granted exclusive access to
 * specific functions.
 *
 * By default, the owner account will be the one that deploys the contract. This
 * can later be changed with {transferOwnership}.
 *
 * This module is used through inheritance. It will make available the modifier
 * `onlyOwner`, which can be applied to your functions to restrict their use to
 * the owner.
 */
abstract contract Ownable is Context {
    address private _owner;

    event OwnershipTransferred(address indexed previousOwner, address indexed newOwner);

    /**
     * @dev Initializes the contract setting the deployer as the initial owner.
     */
    constructor() {
        _setOwner(_msgSender());
    }

    /**
     * @dev Returns the address of the current owner.
     */
    function owner() public view virtual returns (address) {
        return _owner;
    }

    /**
     * @dev Throws if called by any account other than the owner.
     */
    modifier onlyOwner() {
        require(owner() == _msgSender(), \"Ownable: caller is not the owner\");
        _;
    }

    /**
     * @dev Leaves the contract without owner. It will not be possible to call
     * `onlyOwner` functions anymore. Can only be called by the current owner.
     *
     * NOTE: Renouncing ownership will leave the contract without an owner,
     * thereby removing any functionality that is only available to the owner.
     */
    function renounceOwnership() public virtual onlyOwner {
        _setOwner(address(0));
    }

    /**
     * @dev Transfers ownership of the contract to a new account (`newOwner`).
     * Can only be called by the current owner.
     */
    function transferOwnership(address newOwner) public virtual onlyOwner {
        require(newOwner != address(0), \"Ownable: new owner is the zero address\");
        _setOwner(newOwner);
    }

    function _setOwner(address newOwner) private {
        address oldOwner = _owner;
        _owner = newOwner;
        emit OwnershipTransferred(oldOwner, newOwner);
    }
}
"},"ReentrancyGuard.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

/**
 * @dev Contract module that helps prevent reentrant calls to a function.
 *
 * Inheriting from `ReentrancyGuard` will make the {nonReentrant} modifier
 * available, which can be applied to functions to make sure there are no nested
 * (reentrant) calls to them.
 *
 * Note that because there is a single `nonReentrant` guard, functions marked as
 * `nonReentrant` may not call one another. This can be worked around by making
 * those functions `private`, and then adding `external` `nonReentrant` entry
 * points to them.
 *
 * TIP: If you would like to learn more about reentrancy and alternative ways
 * to protect against it, check out our blog post
 * https://blog.openzeppelin.com/reentrancy-after-istanbul/[Reentrancy After Istanbul].
 */
abstract contract ReentrancyGuard {
    // Booleans are more expensive than uint256 or any type that takes up a full
    // word because each write operation emits an extra SLOAD to first read the
    // slot\u0027s contents, replace the bits taken up by the boolean, and then write
    // back. This is the compiler\u0027s defense against contract upgrades and
    // pointer aliasing, and it cannot be disabled.

    // The values being non-zero value makes deployment a bit more expensive,
    // but in exchange the refund on every call to nonReentrant will be lower in
    // amount. Since refunds are capped to a percentage of the total
    // transaction\u0027s gas, it is best to keep them low in cases like this one, to
    // increase the likelihood of the full refund coming into effect.
    uint256 private constant _NOT_ENTERED = 1;
    uint256 private constant _ENTERED = 2;

    uint256 private _status;

    constructor() {
        _status = _NOT_ENTERED;
    }

    /**
     * @dev Prevents a contract from calling itself, directly or indirectly.
     * Calling a `nonReentrant` function from another `nonReentrant`
     * function is not supported. It is possible to prevent this from happening
     * by making the `nonReentrant` function external, and make it call a
     * `private` function that does the actual work.
     */
    modifier nonReentrant() {
        // On the first call to nonReentrant, _notEntered will be true
        require(_status != _ENTERED, \"ReentrancyGuard: reentrant call\");

        // Any calls to nonReentrant after this point will fail
        _status = _ENTERED;

        _;

        // By storing the original value once again, a refund is triggered (see
        // https://eips.ethereum.org/EIPS/eip-2200)
        _status = _NOT_ENTERED;
    }
}

