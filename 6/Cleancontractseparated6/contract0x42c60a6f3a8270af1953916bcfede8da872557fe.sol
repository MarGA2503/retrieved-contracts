// SPDX-License-Identifier: UNLICENSED
/* Copyright (c) 2021 Kohi Art Community, Inc. All rights reserved. */

pragma solidity ^0.8.0;

import \"MathV1.sol\";

/*
    Provides mathematical operations and representation in Q31.Q32 format.

    exp: Adapted from Petteri Aimonen\u0027s libfixmath
    
    See: https://github.com/PetteriAimonen/libfixmath
         https://github.com/PetteriAimonen/libfixmath/blob/master/LICENSE

    other functions: Adapted from André Slupik\u0027s FixedMath.NET
                     https://github.com/asik/FixedMath.Net/blob/master/LICENSE.txt
         
    THIRD PARTY NOTICES:
    ====================

    libfixmath is Copyright (c) 2011-2021 Flatmush \u003cFlatmush@gmail.com\u003e,
    Petteri Aimonen \u003cPetteri.Aimonen@gmail.com\u003e, \u0026 libfixmath AUTHORS

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the \"Software\"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.

    Copyright 2012 André Slupik

    Licensed under the Apache License, Version 2.0 (the \"License\");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an \"AS IS\" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

    This project uses code from the log2fix library, which is under the following license:           
    The MIT License (MIT)

    Copyright (c) 2015 Dan Moulding
    
    Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the \"Software\"), 
    to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
    and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
    
    The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
    THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
    IN THE SOFTWARE.
*/

library Fix64V1 {
    int64 public constant FRACTIONAL_PLACES = 32;
    int64 public constant ONE = 4294967296; // 1 \u003c\u003c FRACTIONAL_PLACES
    int64 public constant TWO = ONE * 2;
    int64 public constant PI = 0x3243F6A88;
    int64 public constant TWO_PI = 0x6487ED511;
    int64 public constant MAX_VALUE = type(int64).max;
    int64 public constant MIN_VALUE = type(int64).min;
    int64 public constant PI_OVER_2 = 0x1921FB544;

    function countLeadingZeros(uint64 x) internal pure returns (int64) {        
        int64 result = 0;
        while ((x \u0026 0xF000000000000000) == 0) {
            result += 4;
            x \u003c\u003c= 4;
        }
        while ((x \u0026 0x8000000000000000) == 0) {
            result += 1;
            x \u003c\u003c= 1;
        }
        return result;
    }

    function div(int64 x, int64 y)
        internal
        pure
        returns (int64)
    {
        if (y == 0) {
            revert(\"attempted to divide by zero\");
        }

        int64 xl = x;
        int64 yl = y;        

        uint64 remainder = uint64(xl \u003e= 0 ? xl : -xl);
        uint64 divider = uint64((yl \u003e= 0 ? yl : -yl));
        uint64 quotient = 0;
        int64 bitPos = 64 / 2 + 1;

        while ((divider \u0026 0xF) == 0 \u0026\u0026 bitPos \u003e= 4) {
            divider \u003e\u003e= 4;
            bitPos -= 4;
        }

        while (remainder != 0 \u0026\u0026 bitPos \u003e= 0) {
            int64 shift = countLeadingZeros(remainder);
            if (shift \u003e bitPos) {
                shift = bitPos;
            }
            remainder \u003c\u003c= uint64(shift);
            bitPos -= shift;

            uint64 d = remainder / divider;
            remainder = remainder % divider;
            quotient += d \u003c\u003c uint64(bitPos);

            if ((d \u0026 ~(uint64(0xFFFFFFFFFFFFFFFF) \u003e\u003e uint64(bitPos)) != 0)) {
                return
                    ((xl ^ yl) \u0026 MIN_VALUE) == 0
                        ? MAX_VALUE
                        : MIN_VALUE;
            }

            remainder \u003c\u003c= 1;
            --bitPos;
        }

        ++quotient;
        int64 result = int64(quotient \u003e\u003e 1);
        if (((xl ^ yl) \u0026 MIN_VALUE) != 0) {
            result = -result;
        }

        return int64(result);
    }

    function mul(int64 x, int64 y)
        internal
        pure
        returns (int64)
    {
        int64 xl = x;
        int64 yl = y;

        uint64 xlo = (uint64)((xl \u0026 (int64)(0x00000000FFFFFFFF)));
        int64 xhi = xl \u003e\u003e 32; // FRACTIONAL_PLACES
        uint64 ylo = (uint64)(yl \u0026 (int64)(0x00000000FFFFFFFF));
        int64 yhi = yl \u003e\u003e 32; // FRACTIONAL_PLACES

        uint64 lolo = xlo * ylo;
        int64 lohi = int64(xlo) * yhi;
        int64 hilo = xhi * int64(ylo);
        int64 hihi = xhi * yhi;

        uint64 loResult = lolo \u003e\u003e 32; // FRACTIONAL_PLACES
        int64 midResult1 = lohi;
        int64 midResult2 = hilo;
        int64 hiResult = hihi \u003c\u003c 32; // FRACTIONAL_PLACES

        int64 sum = int64(loResult) + midResult1 + midResult2 + hiResult;

        return int64(sum);
    }

    function mul_256(int x, int y)
        internal
        pure
        returns (int)
    {
        int xl = x;
        int yl = y;

        uint xlo = uint((xl \u0026 int(0x00000000FFFFFFFF)));
        int xhi = xl \u003e\u003e 32; // FRACTIONAL_PLACES
        uint ylo = uint(yl \u0026 int(0x00000000FFFFFFFF));
        int yhi = yl \u003e\u003e 32; // FRACTIONAL_PLACES

        uint lolo = xlo * ylo;
        int lohi = int(xlo) * yhi;
        int hilo = xhi * int(ylo);
        int hihi = xhi * yhi;

        uint loResult = lolo \u003e\u003e 32; // FRACTIONAL_PLACES
        int midResult1 = lohi;
        int midResult2 = hilo;
        int hiResult = hihi \u003c\u003c 32; // FRACTIONAL_PLACES

        int sum = int(loResult) + midResult1 + midResult2 + hiResult;

        return sum;
    }

    function floor(int x) internal pure returns (int64) {
        return int64(x \u0026 0xFFFFFFFF00000000);
    }

    function round(int x) internal pure returns (int) {
        int fractionalPart = x \u0026 0x00000000FFFFFFFF;
        int integralPart = floor(x);
        if (fractionalPart \u003c 0x80000000) return integralPart;
        if (fractionalPart \u003e 0x80000000) return integralPart + ONE;
        if ((integralPart \u0026 ONE) == 0) return integralPart;
        return integralPart + ONE;
    }

    function sub(int64 x, int64 y)
        internal
        pure
        returns (int64)
    {
        int64 xl = x;
        int64 yl = y;
        int64 diff = xl - yl;
        if (((xl ^ yl) \u0026 (xl ^ diff) \u0026 MIN_VALUE) != 0) diff = xl \u003c 0 ? MIN_VALUE : MAX_VALUE;
        return diff;
    }

    function add(int64 x, int64 y)
        internal
        pure
        returns (int64)
    {
        int64 xl = x;
        int64 yl = y;
        int64 sum = xl + yl;
        if ((~(xl ^ yl) \u0026 (xl ^ sum) \u0026 MIN_VALUE) != 0) sum = xl \u003e 0 ? MAX_VALUE : MIN_VALUE;
        return sum;
    }

    function sign(int64 x) internal pure returns (int8) {
        return x == int8(0) ? int8(0) : x \u003e int8(0) ? int8(1) : int8(-1);
    }

    function abs(int64 x) internal pure returns (int64) {
        int64 mask = x \u003e\u003e 63;
        return (x + mask) ^ mask;
    }
}
"},"GeometryV1.sol":{"content":"// SPDX-License-Identifier: UNLICENSED
/* Copyright (c) 2021 Kohi Art Community, Inc. All rights reserved. */

pragma solidity ^0.8.0;

import \"TypesV1.sol\";

library GeometryV1 {
        
    struct Triangle2D {
        TypesV1.Point2D v0;
        TypesV1.Point2D v1;
        TypesV1.Point2D v2;
        uint32 strokeColor;
        uint32 fillColor;
        TypesV1.Chunk2D chunk;
    }

    struct Line2D {
        TypesV1.Point2D v0;
        TypesV1.Point2D v1;
        uint32 color;
        TypesV1.Chunk2D chunk;
    }

    struct Polygon2D {
        TypesV1.Point2D[40960] vertices;
        uint32 vertexCount;
        uint32 strokeColor;
        uint32 fillColor;
        TypesV1.Chunk2D chunk;
    }

    function edge(
        TypesV1.Point2D memory a,
        TypesV1.Point2D memory b,
        TypesV1.Point2D memory c
    ) external pure returns (int256) {
        return ((b.y - a.y) * (c.x - a.x)) - ((b.x - a.x) * (c.y - a.y));
    }

    function getBoundingBox(TypesV1.Point2D[] memory vertices)
        external
        pure
        returns (TypesV1.Point2D memory tl, TypesV1.Point2D memory br)
    {
        int256 xMax = vertices[0].x;
        int256 xMin = vertices[0].x;
        int256 yMax = vertices[0].y;
        int256 yMin = vertices[0].y;

        for (uint256 i; i \u003c vertices.length; i++) {
            TypesV1.Point2D memory p = vertices[i];

            if (p.x \u003e xMax) xMax = p.x;
            if (p.x \u003c xMin) xMin = p.x;
            if (p.y \u003e yMax) yMax = p.y;
            if (p.y \u003c yMin) yMin = p.y;
        }

        return (TypesV1.Point2D(xMin, yMin), TypesV1.Point2D(xMax, yMax));
    }
}"},"GraphicsV1.sol":{"content":"// SPDX-License-Identifier: UNLICENSED
/* Copyright (c) 2021 Kohi Art Community, Inc. All rights reserved. */

pragma solidity ^0.8.0;

library GraphicsV1 {
    
    function setPixel(
        uint32[16384 /* 128 * 128 */] memory result,
        uint256 width,
        int256 x,
        int256 y,
        uint32 color
    ) internal pure {
        uint256 p = uint256(int256(width) * y + x);
        result[p] = blend(result[p], color);
    }

    function blend(uint32 bg, uint32 fg) internal pure returns (uint32) {
        uint32 r1 = bg \u003e\u003e 16;
        uint32 g1 = bg \u003e\u003e 8;
        uint32 b1 = bg;
        
        uint32 a2 = fg \u003e\u003e 24;
        uint32 r2 = fg \u003e\u003e 16;
        uint32 g2 = fg \u003e\u003e 8;
        uint32 b2 = fg;
        
        uint32 alpha = (a2 \u0026 0xFF) + 1;
        uint32 inverseAlpha = 257 - alpha;

        uint32 r = (alpha * (r2 \u0026 0xFF) + inverseAlpha * (r1 \u0026 0xFF)) \u003e\u003e 8;
        uint32 g = (alpha * (g2 \u0026 0xFF) + inverseAlpha * (g1 \u0026 0xFF)) \u003e\u003e 8;
        uint32 b = (alpha * (b2 \u0026 0xFF) + inverseAlpha * (b1 \u0026 0xFF)) \u003e\u003e 8;

        uint32 rgb = 0;
        rgb |= uint32(0xFF) \u003c\u003c 24;
        rgb |= r \u003c\u003c 16;
        rgb |= g \u003c\u003c 8;
        rgb |= b;

        return rgb;
    }

    function setOpacity(uint32 color, uint32 opacity) internal pure returns (uint32) {

        require(opacity \u003e 0 \u0026\u0026 opacity \u003c= 255, \"opacity must be between 0 and 255\");
        
        uint32 r = color \u003e\u003e 16 \u0026 0xFF;
        uint32 g = color \u003e\u003e 8 \u0026 0xFF;
        uint32 b = color \u0026 0xFF;

        uint32 rgb = 0;
        rgb |= opacity \u003c\u003c 24;
        rgb |= r \u003c\u003c 16;
        rgb |= g \u003c\u003c 8;
        rgb |= b;

        return uint32(rgb);     
    }
}"},"KintsugiDraw.sol":{"content":"// SPDX-License-Identifier: UNLICENSED
/* Copyright (c) 2021 Kohi Art Community, Inc. All rights reserved. */

pragma solidity ^0.8.0;

import \"KintsugiLayer.sol\";
import \"ParticleSetV1.sol\";
import \"GraphicsV1.sol\";
import \"ProcessingV1.sol\";
import \"GeometryV1.sol\";

library KintsugiDraw {

    struct Draw {
        uint32[16384] result;
        KintsugiLayer.KintsugiParameters p;
        int64[4096] noiseTable;
        TypesV1.Chunk2D chunk;
    }

    uint16 internal constant NOISE_TABLE_SIZE = 4095;
    uint16 internal constant PARTICLE_COUNT = 5000;
    uint16 internal constant FRAME_COUNT = 400;
    
    function draw(Draw memory f) external pure returns(uint32[16384] memory buffer) {
        f.p.iteration = 0;
        f.p.frame = 0;

        while (f.p.frame \u003c FRAME_COUNT) {
            f.p.frame++;

            if (f.p.iteration \u003e= f.p.layers) {
                break;
            }

            bool dead = true;
            {
                for (uint256 i = 0; i \u003c f.p.layers; i++) {
                    ParticleSetV1.ParticleSet2D memory particleSet = f.p.particleSets[i];
                    update(
                        f.noiseTable,
                        particleSet,
                        PARTICLE_COUNT,
                        f.chunk.width,
                        f.chunk.height
                    );
                    if (!particleSet.dead) {
                        dead = false;
                    }
                    draw(particleSet, PARTICLE_COUNT, f.result, f.chunk);
                }
            }

            if (dead) {
                f.p.iteration++;
            }
        }

        return f.result;
    }

    function update(
        int64[NOISE_TABLE_SIZE + 1] memory noiseTable,
        ParticleSetV1.ParticleSet2D memory set,
        uint16 particleCount,
        uint256 width,
        uint256 height
    ) internal pure {
        set.dead = true;
        for (uint16 i = 0; i \u003c particleCount; i++) {
            ParticleV1.Particle2D memory p = set.particles[i];
            if (p.dead) {
                continue;
            }
            set.dead = false;
            ParticleV1.update(noiseTable, p, width, height);
        }
    }

    function draw(
        ParticleSetV1.ParticleSet2D memory set,
        uint16 particleCount,
        uint32[16384] memory result,
        TypesV1.Chunk2D memory chunk
    ) internal pure {
        if (set.dead) {
            return;
        }

        for (uint256 i = 0; i \u003c particleCount; i++) {
            ParticleV1.Particle2D memory p = set.particles[i];
            if (p.dead) {
                continue;
            }
            step(p, result, chunk);
        }
    }

    function step(
        ParticleV1.Particle2D memory p,
        uint32[16384] memory result,
        TypesV1.Chunk2D memory chunk
    ) internal pure {
        if (p.frames \u003c 40) {
            return;
        }

        uint32 dark = GraphicsV1.setOpacity(0xFFF4BB29, 10);

        TypesV1.Point2D memory v0 = TypesV1.Point2D(int32(p.x), int32(p.y));
        TypesV1.Point2D memory v1 = TypesV1.Point2D(int32(p.px), int32(p.py));

        ProcessingV1.line(
            result,
            GeometryV1.Line2D(
                TypesV1.Point2D(v0.x, v0.y - 2),
                TypesV1.Point2D(v1.x, v1.y - 2),
                dark,
                chunk
            )
        );
        ProcessingV1.line(
            result,
            GeometryV1.Line2D(
                TypesV1.Point2D(v0.x, v0.y + 2),
                TypesV1.Point2D(v1.x, v1.y + 2),
                dark,
                chunk
            )
        );

        uint32 bright = GraphicsV1.setOpacity(0xFFD5B983, 10);

        ProcessingV1.line(
            result,
            GeometryV1.Line2D(
                TypesV1.Point2D(v0.x, v0.y - 1),
                TypesV1.Point2D(v1.x, v1.y - 1),
                bright,
                chunk
            )
        );
        ProcessingV1.line(
            result,
            GeometryV1.Line2D(
                TypesV1.Point2D(v0.x, v0.y),
                TypesV1.Point2D(v1.x, v1.y),
                bright,
                chunk
            )
        );
        ProcessingV1.line(
            result,
            GeometryV1.Line2D(
                TypesV1.Point2D(v0.x, v0.y + 1),
                TypesV1.Point2D(v1.x, v1.y + 1),
                bright,
                chunk
            )
        );
    }
}"},"KintsugiLayer.sol":{"content":"// SPDX-License-Identifier: UNLICENSED
/* Copyright (c) 2021 Kohi Art Community, Inc. All rights reserved. */

pragma solidity ^0.8.0;

import \"ParticleSetV1.sol\";
import \"ParticleSetFactoryV1.sol\";
import \"RandomV1.sol\";
import \"LCG64.sol\";

library KintsugiLayer {

    uint16 public constant PARTICLE_COUNT = 5000;
    uint8 public constant PARTICLE_RANGE = 65;
    uint8 public constant PARTICLE_LIFETIME = 100;
    int64 public constant PARTICLE_FORCE_SCALE = 15 * 4294967296; /* 15 * Fix64V1.ONE */
    int64 public constant PARTICLE_NOISE_SCALE = 42949673; /* 0.01 */

    struct KintsugiParameters {
        uint8 layers;        
        uint256 frame;
        uint256 iteration;
        ParticleSetV1.ParticleSet2D[4] particleSets;
    }

    function getParameters(RandomV1.PRNG memory prng, int32 seed) 
    external pure returns (KintsugiParameters memory kintsugi, RandomV1.PRNG memory) {
        kintsugi.layers = uint8(uint32(RandomV1.next(prng, 1, 5)));

        for (uint256 i = 0; i \u003c kintsugi.layers; i++) {
            (ParticleSetV1.ParticleSet2D memory particleSet, RandomV1.PRNG memory p) = ParticleSetFactoryV1.createSet(
                ParticleSetFactoryV1.CreateParticleSet2D(
                    seed,
                    PARTICLE_RANGE,
                    1024,
                    1024,
                    PARTICLE_COUNT,                    
                    PARTICLE_FORCE_SCALE,
                    PARTICLE_NOISE_SCALE,
                    PARTICLE_LIFETIME                                        
                ),
                prng
            );
            prng = p;                
            kintsugi.particleSets[i] = particleSet;            
        }

        return (kintsugi, prng);
    }
}"},"LCG64.sol":{"content":"// SPDX-License-Identifier: UNLICENSED
/* Copyright (c) 2021 Kohi Art Community, Inc. All rights reserved. */

pragma solidity ^0.8.0;

import \"Fix64V1.sol\";
import \"Trig256.sol\";

/*
    An implementation of a Linear Congruential Generator in Q31.Q32 format.
    Adapted from the algorithm used by p5js, which is licensed under the LGPL v2.1.

    See: https://github.com/processing/p5.js/blob/374acfb44588bfd565c54d61264df197d798d121/src/math/noise.js
         https://github.com/processing/p5.js/blob/main/license.txt
         
    This adaptation was necessary to ensure generative art in p5js produces identical results for noise values in Solidity.

    THIRD PARTY NOTICES:
    ====================

                        GNU LESSER GENERAL PUBLIC LICENSE
                       Version 2.1, February 1999

 Copyright (C) 1991, 1999 Free Software Foundation, Inc.
 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 Everyone is permitted to copy and distribute verbatim copies
 of this license document, but changing it is not allowed.

(This is the first released version of the Lesser GPL.  It also counts
 as the successor of the GNU Library Public License, version 2, hence
 the version number 2.1.)

                            Preamble

  The licenses for most software are designed to take away your
freedom to share and change it.  By contrast, the GNU General Public
Licenses are intended to guarantee your freedom to share and change
free software--to make sure the software is free for all its users.

  This license, the Lesser General Public License, applies to some
specially designated software packages--typically libraries--of the
Free Software Foundation and other authors who decide to use it.  You
can use it too, but we suggest you first think carefully about whether
this license or the ordinary General Public License is the better
strategy to use in any particular case, based on the explanations below.

  When we speak of free software, we are referring to freedom of use,
not price.  Our General Public Licenses are designed to make sure that
you have the freedom to distribute copies of free software (and charge
for this service if you wish); that you receive source code or can get
it if you want it; that you can change the software and use pieces of
it in new free programs; and that you are informed that you can do
these things.

  To protect your rights, we need to make restrictions that forbid
distributors to deny you these rights or to ask you to surrender these
rights.  These restrictions translate to certain responsibilities for
you if you distribute copies of the library or if you modify it.

  For example, if you distribute copies of the library, whether gratis
or for a fee, you must give the recipients all the rights that we gave
you.  You must make sure that they, too, receive or can get the source
code.  If you link other code with the library, you must provide
complete object files to the recipients, so that they can relink them
with the library after making changes to the library and recompiling
it.  And you must show them these terms so they know their rights.

  We protect your rights with a two-step method: (1) we copyright the
library, and (2) we offer you this license, which gives you legal
permission to copy, distribute and/or modify the library.

  To protect each distributor, we want to make it very clear that
there is no warranty for the free library.  Also, if the library is
modified by someone else and passed on, the recipients should know
that what they have is not the original version, so that the original
author\u0027s reputation will not be affected by problems that might be
introduced by others.

  Finally, software patents pose a constant threat to the existence of
any free program.  We wish to make sure that a company cannot
effectively restrict the users of a free program by obtaining a
restrictive license from a patent holder.  Therefore, we insist that
any patent license obtained for a version of the library must be
consistent with the full freedom of use specified in this license.

  Most GNU software, including some libraries, is covered by the
ordinary GNU General Public License.  This license, the GNU Lesser
General Public License, applies to certain designated libraries, and
is quite different from the ordinary General Public License.  We use
this license for certain libraries in order to permit linking those
libraries into non-free programs.

  When a program is linked with a library, whether statically or using
a shared library, the combination of the two is legally speaking a
combined work, a derivative of the original library.  The ordinary
General Public License therefore permits such linking only if the
entire combination fits its criteria of freedom.  The Lesser General
Public License permits more lax criteria for linking other code with
the library.

  We call this license the \"Lesser\" General Public License because it
does Less to protect the user\u0027s freedom than the ordinary General
Public License.  It also provides other free software developers Less
of an advantage over competing non-free programs.  These disadvantages
are the reason we use the ordinary General Public License for many
libraries.  However, the Lesser license provides advantages in certain
special circumstances.

  For example, on rare occasions, there may be a special need to
encourage the widest possible use of a certain library, so that it becomes
a de-facto standard.  To achieve this, non-free programs must be
allowed to use the library.  A more frequent case is that a free
library does the same job as widely used non-free libraries.  In this
case, there is little to gain by limiting the free library to free
software only, so we use the Lesser General Public License.

  In other cases, permission to use a particular library in non-free
programs enables a greater number of people to use a large body of
free software.  For example, permission to use the GNU C Library in
non-free programs enables many more people to use the whole GNU
operating system, as well as its variant, the GNU/Linux operating
system.

  Although the Lesser General Public License is Less protective of the
users\u0027 freedom, it does ensure that the user of a program that is
linked with the Library has the freedom and the wherewithal to run
that program using a modified version of the Library.

  The precise terms and conditions for copying, distribution and
modification follow.  Pay close attention to the difference between a
\"work based on the library\" and a \"work that uses the library\".  The
former contains code derived from the library, whereas the latter must
be combined with the library in order to run.

                  GNU LESSER GENERAL PUBLIC LICENSE
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

  0. This License Agreement applies to any software library or other
program which contains a notice placed by the copyright holder or
other authorized party saying it may be distributed under the terms of
this Lesser General Public License (also called \"this License\").
Each licensee is addressed as \"you\".

  A \"library\" means a collection of software functions and/or data
prepared so as to be conveniently linked with application programs
(which use some of those functions and data) to form executables.

  The \"Library\", below, refers to any such software library or work
which has been distributed under these terms.  A \"work based on the
Library\" means either the Library or any derivative work under
copyright law: that is to say, a work containing the Library or a
portion of it, either verbatim or with modifications and/or translated
straightforwardly into another language.  (Hereinafter, translation is
included without limitation in the term \"modification\".)

  \"Source code\" for a work means the preferred form of the work for
making modifications to it.  For a library, complete source code means
all the source code for all modules it contains, plus any associated
interface definition files, plus the scripts used to control compilation
and installation of the library.

  Activities other than copying, distribution and modification are not
covered by this License; they are outside its scope.  The act of
running a program using the Library is not restricted, and output from
such a program is covered only if its contents constitute a work based
on the Library (independent of the use of the Library in a tool for
writing it).  Whether that is true depends on what the Library does
and what the program that uses the Library does.

  1. You may copy and distribute verbatim copies of the Library\u0027s
complete source code as you receive it, in any medium, provided that
you conspicuously and appropriately publish on each copy an
appropriate copyright notice and disclaimer of warranty; keep intact
all the notices that refer to this License and to the absence of any
warranty; and distribute a copy of this License along with the
Library.

  You may charge a fee for the physical act of transferring a copy,
and you may at your option offer warranty protection in exchange for a
fee.

  2. You may modify your copy or copies of the Library or any portion
of it, thus forming a work based on the Library, and copy and
distribute such modifications or work under the terms of Section 1
above, provided that you also meet all of these conditions:

    a) The modified work must itself be a software library.

    b) You must cause the files modified to carry prominent notices
    stating that you changed the files and the date of any change.

    c) You must cause the whole of the work to be licensed at no
    charge to all third parties under the terms of this License.

    d) If a facility in the modified Library refers to a function or a
    table of data to be supplied by an application program that uses
    the facility, other than as an argument passed when the facility
    is invoked, then you must make a good faith effort to ensure that,
    in the event an application does not supply such function or
    table, the facility still operates, and performs whatever part of
    its purpose remains meaningful.

    (For example, a function in a library to compute square roots has
    a purpose that is entirely well-defined independent of the
    application.  Therefore, Subsection 2d requires that any
    application-supplied function or table used by this function must
    be optional: if the application does not supply it, the square
    root function must still compute square roots.)

These requirements apply to the modified work as a whole.  If
identifiable sections of that work are not derived from the Library,
and can be reasonably considered independent and separate works in
themselves, then this License, and its terms, do not apply to those
sections when you distribute them as separate works.  But when you
distribute the same sections as part of a whole which is a work based
on the Library, the distribution of the whole must be on the terms of
this License, whose permissions for other licensees extend to the
entire whole, and thus to each and every part regardless of who wrote
it.

Thus, it is not the intent of this section to claim rights or contest
your rights to work written entirely by you; rather, the intent is to
exercise the right to control the distribution of derivative or
collective works based on the Library.

In addition, mere aggregation of another work not based on the Library
with the Library (or with a work based on the Library) on a volume of
a storage or distribution medium does not bring the other work under
the scope of this License.

  3. You may opt to apply the terms of the ordinary GNU General Public
License instead of this License to a given copy of the Library.  To do
this, you must alter all the notices that refer to this License, so
that they refer to the ordinary GNU General Public License, version 2,
instead of to this License.  (If a newer version than version 2 of the
ordinary GNU General Public License has appeared, then you can specify
that version instead if you wish.)  Do not make any other change in
these notices.

  Once this change is made in a given copy, it is irreversible for
that copy, so the ordinary GNU General Public License applies to all
subsequent copies and derivative works made from that copy.

  This option is useful when you wish to copy part of the code of
the Library into a program that is not a library.

  4. You may copy and distribute the Library (or a portion or
derivative of it, under Section 2) in object code or executable form
under the terms of Sections 1 and 2 above provided that you accompany
it with the complete corresponding machine-readable source code, which
must be distributed under the terms of Sections 1 and 2 above on a
medium customarily used for software interchange.

  If distribution of object code is made by offering access to copy
from a designated place, then offering equivalent access to copy the
source code from the same place satisfies the requirement to
distribute the source code, even though third parties are not
compelled to copy the source along with the object code.

  5. A program that contains no derivative of any portion of the
Library, but is designed to work with the Library by being compiled or
linked with it, is called a \"work that uses the Library\".  Such a
work, in isolation, is not a derivative work of the Library, and
therefore falls outside the scope of this License.

  However, linking a \"work that uses the Library\" with the Library
creates an executable that is a derivative of the Library (because it
contains portions of the Library), rather than a \"work that uses the
library\".  The executable is therefore covered by this License.
Section 6 states terms for distribution of such executables.

  When a \"work that uses the Library\" uses material from a header file
that is part of the Library, the object code for the work may be a
derivative work of the Library even though the source code is not.
Whether this is true is especially significant if the work can be
linked without the Library, or if the work is itself a library.  The
threshold for this to be true is not precisely defined by law.

  If such an object file uses only numerical parameters, data
structure layouts and accessors, and small macros and small inline
functions (ten lines or less in length), then the use of the object
file is unrestricted, regardless of whether it is legally a derivative
work.  (Executables containing this object code plus portions of the
Library will still fall under Section 6.)

  Otherwise, if the work is a derivative of the Library, you may
distribute the object code for the work under the terms of Section 6.
Any executables containing that work also fall under Section 6,
whether or not they are linked directly with the Library itself.

  6. As an exception to the Sections above, you may also combine or
link a \"work that uses the Library\" with the Library to produce a
work containing portions of the Library, and distribute that work
under terms of your choice, provided that the terms permit
modification of the work for the customer\u0027s own use and reverse
engineering for debugging such modifications.

  You must give prominent notice with each copy of the work that the
Library is used in it and that the Library and its use are covered by
this License.  You must supply a copy of this License.  If the work
during execution displays copyright notices, you must include the
copyright notice for the Library among them, as well as a reference
directing the user to the copy of this License.  Also, you must do one
of these things:

    a) Accompany the work with the complete corresponding
    machine-readable source code for the Library including whatever
    changes were used in the work (which must be distributed under
    Sections 1 and 2 above); and, if the work is an executable linked
    with the Library, with the complete machine-readable \"work that
    uses the Library\", as object code and/or source code, so that the
    user can modify the Library and then relink to produce a modified
    executable containing the modified Library.  (It is understood
    that the user who changes the contents of definitions files in the
    Library will not necessarily be able to recompile the application
    to use the modified definitions.)

    b) Use a suitable shared library mechanism for linking with the
    Library.  A suitable mechanism is one that (1) uses at run time a
    copy of the library already present on the user\u0027s computer system,
    rather than copying library functions into the executable, and (2)
    will operate properly with a modified version of the library, if
    the user installs one, as long as the modified version is
    interface-compatible with the version that the work was made with.

    c) Accompany the work with a written offer, valid for at
    least three years, to give the same user the materials
    specified in Subsection 6a, above, for a charge no more
    than the cost of performing this distribution.

    d) If distribution of the work is made by offering access to copy
    from a designated place, offer equivalent access to copy the above
    specified materials from the same place.

    e) Verify that the user has already received a copy of these
    materials or that you have already sent this user a copy.

  For an executable, the required form of the \"work that uses the
Library\" must include any data and utility programs needed for
reproducing the executable from it.  However, as a special exception,
the materials to be distributed need not include anything that is
normally distributed (in either source or binary form) with the major
components (compiler, kernel, and so on) of the operating system on
which the executable runs, unless that component itself accompanies
the executable.

  It may happen that this requirement contradicts the license
restrictions of other proprietary libraries that do not normally
accompany the operating system.  Such a contradiction means you cannot
use both them and the Library together in an executable that you
distribute.

  7. You may place library facilities that are a work based on the
Library side-by-side in a single library together with other library
facilities not covered by this License, and distribute such a combined
library, provided that the separate distribution of the work based on
the Library and of the other library facilities is otherwise
permitted, and provided that you do these two things:

    a) Accompany the combined library with a copy of the same work
    based on the Library, uncombined with any other library
    facilities.  This must be distributed under the terms of the
    Sections above.

    b) Give prominent notice with the combined library of the fact
    that part of it is a work based on the Library, and explaining
    where to find the accompanying uncombined form of the same work.

  8. You may not copy, modify, sublicense, link with, or distribute
the Library except as expressly provided under this License.  Any
attempt otherwise to copy, modify, sublicense, link with, or
distribute the Library is void, and will automatically terminate your
rights under this License.  However, parties who have received copies,
or rights, from you under this License will not have their licenses
terminated so long as such parties remain in full compliance.

  9. You are not required to accept this License, since you have not
signed it.  However, nothing else grants you permission to modify or
distribute the Library or its derivative works.  These actions are
prohibited by law if you do not accept this License.  Therefore, by
modifying or distributing the Library (or any work based on the
Library), you indicate your acceptance of this License to do so, and
all its terms and conditions for copying, distributing or modifying
the Library or works based on it.

  10. Each time you redistribute the Library (or any work based on the
Library), the recipient automatically receives a license from the
original licensor to copy, distribute, link with or modify the Library
subject to these terms and conditions.  You may not impose any further
restrictions on the recipients\u0027 exercise of the rights granted herein.
You are not responsible for enforcing compliance by third parties with
this License.

  11. If, as a consequence of a court judgment or allegation of patent
infringement or for any other reason (not limited to patent issues),
conditions are imposed on you (whether by court order, agreement or
otherwise) that contradict the conditions of this License, they do not
excuse you from the conditions of this License.  If you cannot
distribute so as to satisfy simultaneously your obligations under this
License and any other pertinent obligations, then as a consequence you
may not distribute the Library at all.  For example, if a patent
license would not permit royalty-free redistribution of the Library by
all those who receive copies directly or indirectly through you, then
the only way you could satisfy both it and this License would be to
refrain entirely from distribution of the Library.

If any portion of this section is held invalid or unenforceable under any
particular circumstance, the balance of the section is intended to apply,
and the section as a whole is intended to apply in other circumstances.

It is not the purpose of this section to induce you to infringe any
patents or other property right claims or to contest validity of any
such claims; this section has the sole purpose of protecting the
integrity of the free software distribution system which is
implemented by public license practices.  Many people have made
generous contributions to the wide range of software distributed
through that system in reliance on consistent application of that
system; it is up to the author/donor to decide if he or she is willing
to distribute software through any other system and a licensee cannot
impose that choice.

This section is intended to make thoroughly clear what is believed to
be a consequence of the rest of this License.

  12. If the distribution and/or use of the Library is restricted in
certain countries either by patents or by copyrighted interfaces, the
original copyright holder who places the Library under this License may add
an explicit geographical distribution limitation excluding those countries,
so that distribution is permitted only in or among countries not thus
excluded.  In such case, this License incorporates the limitation as if
written in the body of this License.

  13. The Free Software Foundation may publish revised and/or new
versions of the Lesser General Public License from time to time.
Such new versions will be similar in spirit to the present version,
but may differ in detail to address new problems or concerns.

Each version is given a distinguishing version number.  If the Library
specifies a version number of this License which applies to it and
\"any later version\", you have the option of following the terms and
conditions either of that version or of any later version published by
the Free Software Foundation.  If the Library does not specify a
license version number, you may choose any version ever published by
the Free Software Foundation.

  14. If you wish to incorporate parts of the Library into other free
programs whose distribution conditions are incompatible with these,
write to the author to ask for permission.  For software which is
copyrighted by the Free Software Foundation, write to the Free
Software Foundation; we sometimes make exceptions for this.  Our
decision will be guided by the two goals of preserving the free status
of all derivatives of our free software and of promoting the sharing
and reuse of software generally.

                            NO WARRANTY

  15. BECAUSE THE LIBRARY IS LICENSED FREE OF CHARGE, THERE IS NO
WARRANTY FOR THE LIBRARY, TO THE EXTENT PERMITTED BY APPLICABLE LAW.
EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR
OTHER PARTIES PROVIDE THE LIBRARY \"AS IS\" WITHOUT WARRANTY OF ANY
KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
PURPOSE.  THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE
LIBRARY IS WITH YOU.  SHOULD THE LIBRARY PROVE DEFECTIVE, YOU ASSUME
THE COST OF ALL NECESSARY SERVICING, REPAIR OR CORRECTION.

  16. IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN
WRITING WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MAY MODIFY
AND/OR REDISTRIBUTE THE LIBRARY AS PERMITTED ABOVE, BE LIABLE TO YOU
FOR DAMAGES, INCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR
CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OR INABILITY TO USE THE
LIBRARY (INCLUDING BUT NOT LIMITED TO LOSS OF DATA OR DATA BEING
RENDERED INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD PARTIES OR A
FAILURE OF THE LIBRARY TO OPERATE WITH ANY OTHER SOFTWARE), EVEN IF
SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH
DAMAGES.

                     END OF TERMS AND CONDITIONS
*/

library LCG64 {
    uint64 internal constant M = 4294967296;
    uint32 internal constant A = 1664525;
    uint32 internal constant C = 1013904223;

    function next(uint64 z) internal pure returns(uint64, int64) {                
                
        uint256 r = uint(A) * uint(z) + uint(C);            
        uint64 g = uint64(r % M);           
        
        int lz = Trig256.log_256(int64(g) * int(Fix64V1.ONE));
        int lm = Trig256.log_256(int64(M) * int(Fix64V1.ONE));
        int64 lml = Fix64V1.sub(int64(lz), int64(lm));
        int64 v = Trig256.exp(lml);

        return (g, v);
    }
}"},"MathV1.sol":{"content":"// SPDX-License-Identifier: UNLICENSED
/* Copyright (c) 2021 Kohi Art Community, Inc. All rights reserved. */

pragma solidity ^0.8.0;

library MathV1 {
    function max(int256 a, int256 b) internal pure returns (int256) {
        return a \u003e= b ? a : b;
    }

    function min(int256 a, int256 b) internal pure returns (int256) {
        return a \u003c b ? a : b;
    }

    function max3(
        int256 a,
        int256 b,
        int256 c
    ) internal pure returns (int256) {
        int256 d = b \u003e= c ? b : c;
        return a \u003e= d ? a : d;
    }

    function min3(
        int256 a,
        int256 b,
        int256 c
    ) internal pure returns (int256) {
        int256 d = b \u003c c ? b : c;
        return a \u003c d ? a : d;
    }

    function abs(int256 x) internal pure returns (int256) {
        return x \u003e= 0 ? x : -x;
    }

    function sign(int256 x) internal pure returns (int8) {
        return x == 0 ? int8(0) : x \u003e 0 ? int8(1) : int8(-1);
    }
}"},"NoiseV1.sol":{"content":"// SPDX-License-Identifier: UNLICENSED
/* Copyright (c) 2021 Kohi Art Community, Inc. All rights reserved. */

pragma solidity ^0.8.0;

import \"Fix64V1.sol\";
import \"Trig256.sol\";
import \"LCG64.sol\";

/*
    An implementation of fixed-point noise in Q31.Q32 format.
    Adapted from the algorithm used by p5js, which is licensed under the LGPL v2.1.

    See: https://github.com/processing/p5.js/blob/374acfb44588bfd565c54d61264df197d798d121/src/math/noise.js
         https://github.com/processing/p5.js/blob/main/license.txt

    This adaptation was necessary to ensure generative art in p5js produces identical results for noise values in Solidity.

    THIRD PARTY NOTICES:
    ====================

                    GNU LESSER GENERAL PUBLIC LICENSE
                       Version 2.1, February 1999

 Copyright (C) 1991, 1999 Free Software Foundation, Inc.
 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 Everyone is permitted to copy and distribute verbatim copies
 of this license document, but changing it is not allowed.

(This is the first released version of the Lesser GPL.  It also counts
 as the successor of the GNU Library Public License, version 2, hence
 the version number 2.1.)

                            Preamble

  The licenses for most software are designed to take away your
freedom to share and change it.  By contrast, the GNU General Public
Licenses are intended to guarantee your freedom to share and change
free software--to make sure the software is free for all its users.

  This license, the Lesser General Public License, applies to some
specially designated software packages--typically libraries--of the
Free Software Foundation and other authors who decide to use it.  You
can use it too, but we suggest you first think carefully about whether
this license or the ordinary General Public License is the better
strategy to use in any particular case, based on the explanations below.

  When we speak of free software, we are referring to freedom of use,
not price.  Our General Public Licenses are designed to make sure that
you have the freedom to distribute copies of free software (and charge
for this service if you wish); that you receive source code or can get
it if you want it; that you can change the software and use pieces of
it in new free programs; and that you are informed that you can do
these things.

  To protect your rights, we need to make restrictions that forbid
distributors to deny you these rights or to ask you to surrender these
rights.  These restrictions translate to certain responsibilities for
you if you distribute copies of the library or if you modify it.

  For example, if you distribute copies of the library, whether gratis
or for a fee, you must give the recipients all the rights that we gave
you.  You must make sure that they, too, receive or can get the source
code.  If you link other code with the library, you must provide
complete object files to the recipients, so that they can relink them
with the library after making changes to the library and recompiling
it.  And you must show them these terms so they know their rights.

  We protect your rights with a two-step method: (1) we copyright the
library, and (2) we offer you this license, which gives you legal
permission to copy, distribute and/or modify the library.

  To protect each distributor, we want to make it very clear that
there is no warranty for the free library.  Also, if the library is
modified by someone else and passed on, the recipients should know
that what they have is not the original version, so that the original
author\u0027s reputation will not be affected by problems that might be
introduced by others.

  Finally, software patents pose a constant threat to the existence of
any free program.  We wish to make sure that a company cannot
effectively restrict the users of a free program by obtaining a
restrictive license from a patent holder.  Therefore, we insist that
any patent license obtained for a version of the library must be
consistent with the full freedom of use specified in this license.

  Most GNU software, including some libraries, is covered by the
ordinary GNU General Public License.  This license, the GNU Lesser
General Public License, applies to certain designated libraries, and
is quite different from the ordinary General Public License.  We use
this license for certain libraries in order to permit linking those
libraries into non-free programs.

  When a program is linked with a library, whether statically or using
a shared library, the combination of the two is legally speaking a
combined work, a derivative of the original library.  The ordinary
General Public License therefore permits such linking only if the
entire combination fits its criteria of freedom.  The Lesser General
Public License permits more lax criteria for linking other code with
the library.

  We call this license the \"Lesser\" General Public License because it
does Less to protect the user\u0027s freedom than the ordinary General
Public License.  It also provides other free software developers Less
of an advantage over competing non-free programs.  These disadvantages
are the reason we use the ordinary General Public License for many
libraries.  However, the Lesser license provides advantages in certain
special circumstances.

  For example, on rare occasions, there may be a special need to
encourage the widest possible use of a certain library, so that it becomes
a de-facto standard.  To achieve this, non-free programs must be
allowed to use the library.  A more frequent case is that a free
library does the same job as widely used non-free libraries.  In this
case, there is little to gain by limiting the free library to free
software only, so we use the Lesser General Public License.

  In other cases, permission to use a particular library in non-free
programs enables a greater number of people to use a large body of
free software.  For example, permission to use the GNU C Library in
non-free programs enables many more people to use the whole GNU
operating system, as well as its variant, the GNU/Linux operating
system.

  Although the Lesser General Public License is Less protective of the
users\u0027 freedom, it does ensure that the user of a program that is
linked with the Library has the freedom and the wherewithal to run
that program using a modified version of the Library.

  The precise terms and conditions for copying, distribution and
modification follow.  Pay close attention to the difference between a
\"work based on the library\" and a \"work that uses the library\".  The
former contains code derived from the library, whereas the latter must
be combined with the library in order to run.

                  GNU LESSER GENERAL PUBLIC LICENSE
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

  0. This License Agreement applies to any software library or other
program which contains a notice placed by the copyright holder or
other authorized party saying it may be distributed under the terms of
this Lesser General Public License (also called \"this License\").
Each licensee is addressed as \"you\".

  A \"library\" means a collection of software functions and/or data
prepared so as to be conveniently linked with application programs
(which use some of those functions and data) to form executables.

  The \"Library\", below, refers to any such software library or work
which has been distributed under these terms.  A \"work based on the
Library\" means either the Library or any derivative work under
copyright law: that is to say, a work containing the Library or a
portion of it, either verbatim or with modifications and/or translated
straightforwardly into another language.  (Hereinafter, translation is
included without limitation in the term \"modification\".)

  \"Source code\" for a work means the preferred form of the work for
making modifications to it.  For a library, complete source code means
all the source code for all modules it contains, plus any associated
interface definition files, plus the scripts used to control compilation
and installation of the library.

  Activities other than copying, distribution and modification are not
covered by this License; they are outside its scope.  The act of
running a program using the Library is not restricted, and output from
such a program is covered only if its contents constitute a work based
on the Library (independent of the use of the Library in a tool for
writing it).  Whether that is true depends on what the Library does
and what the program that uses the Library does.

  1. You may copy and distribute verbatim copies of the Library\u0027s
complete source code as you receive it, in any medium, provided that
you conspicuously and appropriately publish on each copy an
appropriate copyright notice and disclaimer of warranty; keep intact
all the notices that refer to this License and to the absence of any
warranty; and distribute a copy of this License along with the
Library.

  You may charge a fee for the physical act of transferring a copy,
and you may at your option offer warranty protection in exchange for a
fee.

  2. You may modify your copy or copies of the Library or any portion
of it, thus forming a work based on the Library, and copy and
distribute such modifications or work under the terms of Section 1
above, provided that you also meet all of these conditions:

    a) The modified work must itself be a software library.

    b) You must cause the files modified to carry prominent notices
    stating that you changed the files and the date of any change.

    c) You must cause the whole of the work to be licensed at no
    charge to all third parties under the terms of this License.

    d) If a facility in the modified Library refers to a function or a
    table of data to be supplied by an application program that uses
    the facility, other than as an argument passed when the facility
    is invoked, then you must make a good faith effort to ensure that,
    in the event an application does not supply such function or
    table, the facility still operates, and performs whatever part of
    its purpose remains meaningful.

    (For example, a function in a library to compute square roots has
    a purpose that is entirely well-defined independent of the
    application.  Therefore, Subsection 2d requires that any
    application-supplied function or table used by this function must
    be optional: if the application does not supply it, the square
    root function must still compute square roots.)

These requirements apply to the modified work as a whole.  If
identifiable sections of that work are not derived from the Library,
and can be reasonably considered independent and separate works in
themselves, then this License, and its terms, do not apply to those
sections when you distribute them as separate works.  But when you
distribute the same sections as part of a whole which is a work based
on the Library, the distribution of the whole must be on the terms of
this License, whose permissions for other licensees extend to the
entire whole, and thus to each and every part regardless of who wrote
it.

Thus, it is not the intent of this section to claim rights or contest
your rights to work written entirely by you; rather, the intent is to
exercise the right to control the distribution of derivative or
collective works based on the Library.

In addition, mere aggregation of another work not based on the Library
with the Library (or with a work based on the Library) on a volume of
a storage or distribution medium does not bring the other work under
the scope of this License.

  3. You may opt to apply the terms of the ordinary GNU General Public
License instead of this License to a given copy of the Library.  To do
this, you must alter all the notices that refer to this License, so
that they refer to the ordinary GNU General Public License, version 2,
instead of to this License.  (If a newer version than version 2 of the
ordinary GNU General Public License has appeared, then you can specify
that version instead if you wish.)  Do not make any other change in
these notices.

  Once this change is made in a given copy, it is irreversible for
that copy, so the ordinary GNU General Public License applies to all
subsequent copies and derivative works made from that copy.

  This option is useful when you wish to copy part of the code of
the Library into a program that is not a library.

  4. You may copy and distribute the Library (or a portion or
derivative of it, under Section 2) in object code or executable form
under the terms of Sections 1 and 2 above provided that you accompany
it with the complete corresponding machine-readable source code, which
must be distributed under the terms of Sections 1 and 2 above on a
medium customarily used for software interchange.

  If distribution of object code is made by offering access to copy
from a designated place, then offering equivalent access to copy the
source code from the same place satisfies the requirement to
distribute the source code, even though third parties are not
compelled to copy the source along with the object code.

  5. A program that contains no derivative of any portion of the
Library, but is designed to work with the Library by being compiled or
linked with it, is called a \"work that uses the Library\".  Such a
work, in isolation, is not a derivative work of the Library, and
therefore falls outside the scope of this License.

  However, linking a \"work that uses the Library\" with the Library
creates an executable that is a derivative of the Library (because it
contains portions of the Library), rather than a \"work that uses the
library\".  The executable is therefore covered by this License.
Section 6 states terms for distribution of such executables.

  When a \"work that uses the Library\" uses material from a header file
that is part of the Library, the object code for the work may be a
derivative work of the Library even though the source code is not.
Whether this is true is especially significant if the work can be
linked without the Library, or if the work is itself a library.  The
threshold for this to be true is not precisely defined by law.

  If such an object file uses only numerical parameters, data
structure layouts and accessors, and small macros and small inline
functions (ten lines or less in length), then the use of the object
file is unrestricted, regardless of whether it is legally a derivative
work.  (Executables containing this object code plus portions of the
Library will still fall under Section 6.)

  Otherwise, if the work is a derivative of the Library, you may
distribute the object code for the work under the terms of Section 6.
Any executables containing that work also fall under Section 6,
whether or not they are linked directly with the Library itself.

  6. As an exception to the Sections above, you may also combine or
link a \"work that uses the Library\" with the Library to produce a
work containing portions of the Library, and distribute that work
under terms of your choice, provided that the terms permit
modification of the work for the customer\u0027s own use and reverse
engineering for debugging such modifications.

  You must give prominent notice with each copy of the work that the
Library is used in it and that the Library and its use are covered by
this License.  You must supply a copy of this License.  If the work
during execution displays copyright notices, you must include the
copyright notice for the Library among them, as well as a reference
directing the user to the copy of this License.  Also, you must do one
of these things:

    a) Accompany the work with the complete corresponding
    machine-readable source code for the Library including whatever
    changes were used in the work (which must be distributed under
    Sections 1 and 2 above); and, if the work is an executable linked
    with the Library, with the complete machine-readable \"work that
    uses the Library\", as object code and/or source code, so that the
    user can modify the Library and then relink to produce a modified
    executable containing the modified Library.  (It is understood
    that the user who changes the contents of definitions files in the
    Library will not necessarily be able to recompile the application
    to use the modified definitions.)

    b) Use a suitable shared library mechanism for linking with the
    Library.  A suitable mechanism is one that (1) uses at run time a
    copy of the library already present on the user\u0027s computer system,
    rather than copying library functions into the executable, and (2)
    will operate properly with a modified version of the library, if
    the user installs one, as long as the modified version is
    interface-compatible with the version that the work was made with.

    c) Accompany the work with a written offer, valid for at
    least three years, to give the same user the materials
    specified in Subsection 6a, above, for a charge no more
    than the cost of performing this distribution.

    d) If distribution of the work is made by offering access to copy
    from a designated place, offer equivalent access to copy the above
    specified materials from the same place.

    e) Verify that the user has already received a copy of these
    materials or that you have already sent this user a copy.

  For an executable, the required form of the \"work that uses the
Library\" must include any data and utility programs needed for
reproducing the executable from it.  However, as a special exception,
the materials to be distributed need not include anything that is
normally distributed (in either source or binary form) with the major
components (compiler, kernel, and so on) of the operating system on
which the executable runs, unless that component itself accompanies
the executable.

  It may happen that this requirement contradicts the license
restrictions of other proprietary libraries that do not normally
accompany the operating system.  Such a contradiction means you cannot
use both them and the Library together in an executable that you
distribute.

  7. You may place library facilities that are a work based on the
Library side-by-side in a single library together with other library
facilities not covered by this License, and distribute such a combined
library, provided that the separate distribution of the work based on
the Library and of the other library facilities is otherwise
permitted, and provided that you do these two things:

    a) Accompany the combined library with a copy of the same work
    based on the Library, uncombined with any other library
    facilities.  This must be distributed under the terms of the
    Sections above.

    b) Give prominent notice with the combined library of the fact
    that part of it is a work based on the Library, and explaining
    where to find the accompanying uncombined form of the same work.

  8. You may not copy, modify, sublicense, link with, or distribute
the Library except as expressly provided under this License.  Any
attempt otherwise to copy, modify, sublicense, link with, or
distribute the Library is void, and will automatically terminate your
rights under this License.  However, parties who have received copies,
or rights, from you under this License will not have their licenses
terminated so long as such parties remain in full compliance.

  9. You are not required to accept this License, since you have not
signed it.  However, nothing else grants you permission to modify or
distribute the Library or its derivative works.  These actions are
prohibited by law if you do not accept this License.  Therefore, by
modifying or distributing the Library (or any work based on the
Library), you indicate your acceptance of this License to do so, and
all its terms and conditions for copying, distributing or modifying
the Library or works based on it.

  10. Each time you redistribute the Library (or any work based on the
Library), the recipient automatically receives a license from the
original licensor to copy, distribute, link with or modify the Library
subject to these terms and conditions.  You may not impose any further
restrictions on the recipients\u0027 exercise of the rights granted herein.
You are not responsible for enforcing compliance by third parties with
this License.

  11. If, as a consequence of a court judgment or allegation of patent
infringement or for any other reason (not limited to patent issues),
conditions are imposed on you (whether by court order, agreement or
otherwise) that contradict the conditions of this License, they do not
excuse you from the conditions of this License.  If you cannot
distribute so as to satisfy simultaneously your obligations under this
License and any other pertinent obligations, then as a consequence you
may not distribute the Library at all.  For example, if a patent
license would not permit royalty-free redistribution of the Library by
all those who receive copies directly or indirectly through you, then
the only way you could satisfy both it and this License would be to
refrain entirely from distribution of the Library.

If any portion of this section is held invalid or unenforceable under any
particular circumstance, the balance of the section is intended to apply,
and the section as a whole is intended to apply in other circumstances.

It is not the purpose of this section to induce you to infringe any
patents or other property right claims or to contest validity of any
such claims; this section has the sole purpose of protecting the
integrity of the free software distribution system which is
implemented by public license practices.  Many people have made
generous contributions to the wide range of software distributed
through that system in reliance on consistent application of that
system; it is up to the author/donor to decide if he or she is willing
to distribute software through any other system and a licensee cannot
impose that choice.

This section is intended to make thoroughly clear what is believed to
be a consequence of the rest of this License.

  12. If the distribution and/or use of the Library is restricted in
certain countries either by patents or by copyrighted interfaces, the
original copyright holder who places the Library under this License may add
an explicit geographical distribution limitation excluding those countries,
so that distribution is permitted only in or among countries not thus
excluded.  In such case, this License incorporates the limitation as if
written in the body of this License.

  13. The Free Software Foundation may publish revised and/or new
versions of the Lesser General Public License from time to time.
Such new versions will be similar in spirit to the present version,
but may differ in detail to address new problems or concerns.

Each version is given a distinguishing version number.  If the Library
specifies a version number of this License which applies to it and
\"any later version\", you have the option of following the terms and
conditions either of that version or of any later version published by
the Free Software Foundation.  If the Library does not specify a
license version number, you may choose any version ever published by
the Free Software Foundation.

  14. If you wish to incorporate parts of the Library into other free
programs whose distribution conditions are incompatible with these,
write to the author to ask for permission.  For software which is
copyrighted by the Free Software Foundation, write to the Free
Software Foundation; we sometimes make exceptions for this.  Our
decision will be guided by the two goals of preserving the free status
of all derivatives of our free software and of promoting the sharing
and reuse of software generally.

                            NO WARRANTY

  15. BECAUSE THE LIBRARY IS LICENSED FREE OF CHARGE, THERE IS NO
WARRANTY FOR THE LIBRARY, TO THE EXTENT PERMITTED BY APPLICABLE LAW.
EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR
OTHER PARTIES PROVIDE THE LIBRARY \"AS IS\" WITHOUT WARRANTY OF ANY
KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
PURPOSE.  THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE
LIBRARY IS WITH YOU.  SHOULD THE LIBRARY PROVE DEFECTIVE, YOU ASSUME
THE COST OF ALL NECESSARY SERVICING, REPAIR OR CORRECTION.

  16. IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN
WRITING WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MAY MODIFY
AND/OR REDISTRIBUTE THE LIBRARY AS PERMITTED ABOVE, BE LIABLE TO YOU
FOR DAMAGES, INCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR
CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OR INABILITY TO USE THE
LIBRARY (INCLUDING BUT NOT LIMITED TO LOSS OF DATA OR DATA BEING
RENDERED INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD PARTIES OR A
FAILURE OF THE LIBRARY TO OPERATE WITH ANY OTHER SOFTWARE), EVEN IF
SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH
DAMAGES.

                     END OF TERMS AND CONDITIONS
*/

library NoiseV1 {

    uint32 private constant NOISE_TABLE_SIZE = 4095;
    
    int32 private constant PERLIN_YWRAPB = 4;
    int32 private constant PERLIN_YWRAP = 16; // 1 \u003c\u003c PERLIN_YWRAPB
    int32 private constant PERLIN_ZWRAPB = 8;
    int32 private constant PERLIN_ZWRAP = 256; // 1 \u003c\u003c PERLIN_ZWRAPB   
    uint8 private constant PERLIN_OCTAVES = 4;
    int64 private constant PERLIN_AMP_FALLOFF = 2147483648; // 0.5

    struct noiseFunction {
        int64[NOISE_TABLE_SIZE + 1] noiseTable;
        
        int64 x;
        int64 y;
        int64 z;

        int32 xi;
        int32 yi;
        int32 zi;

        int64 xf;
        int64 yf;
        int64 zf;

        int64 rxf;
        int64 ryf;

        int64 n1;
        int64 n2;
        int64 n3;
    }

    function buildNoiseTable(int32 seed) external pure returns (int64[4096] memory noiseTable){
        for (uint16 i = 0; i \u003c NOISE_TABLE_SIZE + 1; i++) {
            (uint64 s, int64 v) = LCG64.next(uint32(seed));
            noiseTable[i] = v;
            seed = int32(uint32(s));
        }
    }

    function noise(
        int64[NOISE_TABLE_SIZE + 1] memory noiseTable,
        int64 x,
        int64 y
    ) external pure returns (int64) {
        return noise_impl(noiseFunction(noiseTable, x, y, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0));
    }

    function noise(noiseFunction memory f) external pure returns (int64) {
        return noise_impl(f);
    }

    function noise_impl(noiseFunction memory f) private pure returns (int64) {
        if (f.x \u003c 0) {
            f.x = -f.x;
        }
        if (f.y \u003c 0) {
            f.y = -f.y;
        }
        if (f.z \u003c 0) {
            f.z = -f.z;
        }

        f.xi = int32(Fix64V1.floor(f.x) \u003e\u003e 32 /* FRACTIONAL_PLACES */);
        f.yi = int32(Fix64V1.floor(f.y) \u003e\u003e 32 /* FRACTIONAL_PLACES */);
        f.zi = int32(Fix64V1.floor(f.z) \u003e\u003e 32 /* FRACTIONAL_PLACES */);        

        f.xf = Fix64V1.sub(f.x, (f.xi * Fix64V1.ONE));
        f.yf = Fix64V1.sub(f.y, (f.yi * Fix64V1.ONE));
        f.zf = Fix64V1.sub(f.z, (f.zi * Fix64V1.ONE)); 
        
        int64 r = 0;
        int64 ampl = PERLIN_AMP_FALLOFF;     

        for (uint8 o = 0; o \u003c PERLIN_OCTAVES; o++) {

            int32 off = f.xi + (f.yi \u003c\u003c uint32(PERLIN_YWRAPB)) + (f.zi \u003c\u003c uint32(PERLIN_ZWRAPB));
            f.rxf = scaled_cosine(f.xf);
            f.ryf = scaled_cosine(f.yf);

            f.n1 = f.noiseTable[uint32(off) \u0026 NOISE_TABLE_SIZE];
            {
                f.n1 = Fix64V1.add(f.n1, Fix64V1.mul(f.rxf, Fix64V1.sub(int64(f.noiseTable[(uint32(off) + 1) \u0026 NOISE_TABLE_SIZE]), f.n1)));                        

                f.n2 = f.noiseTable[(uint32(off) + uint32(PERLIN_YWRAP)) \u0026 NOISE_TABLE_SIZE];
                f.n2 = Fix64V1.add(f.n2, Fix64V1.mul(f.rxf, Fix64V1.sub(int64(f.noiseTable[(uint32(off) + uint32(PERLIN_YWRAP) + 1) \u0026 NOISE_TABLE_SIZE]), f.n2)));
                f.n1 = Fix64V1.add(f.n1, Fix64V1.mul(f.ryf, Fix64V1.sub(f.n2, f.n1)));

                off += PERLIN_ZWRAP;

                f.n2 = f.noiseTable[uint32(off) \u0026 NOISE_TABLE_SIZE];
                f.n2 = Fix64V1.add(f.n2, Fix64V1.mul(f.rxf, Fix64V1.sub(int64(f.noiseTable[((uint32(off) + 1)) \u0026 NOISE_TABLE_SIZE]), f.n2)));

                f.n3 = f.noiseTable[(uint32(off) + uint32(PERLIN_YWRAP)) \u0026 NOISE_TABLE_SIZE];
                f.n3 = Fix64V1.add(f.n3, Fix64V1.mul(f.rxf, Fix64V1.sub(int64(f.noiseTable[(uint32(off) + uint32(PERLIN_YWRAP) + 1) \u0026 NOISE_TABLE_SIZE]), f.n3)));
                f.n2 = Fix64V1.add(f.n2, Fix64V1.mul(f.ryf, Fix64V1.sub(f.n3, f.n2)));
                f.n1 = Fix64V1.add(f.n1, Fix64V1.mul(scaled_cosine(f.zf), Fix64V1.sub(f.n2, f.n1)));
            }           

            r = Fix64V1.add(r, Fix64V1.mul(f.n1, ampl));
            ampl = Fix64V1.mul(ampl, PERLIN_AMP_FALLOFF);

            f.xi \u003c\u003c= 1;
            f.xf = Fix64V1.mul(f.xf, Fix64V1.TWO);
            f.yi \u003c\u003c= 1;
            f.yf = Fix64V1.mul(f.yf, Fix64V1.TWO);
            f.zi \u003c\u003c= 1;
            f.zf = Fix64V1.mul(f.zf, Fix64V1.TWO);

            if (f.xf \u003e= Fix64V1.ONE) {
                f.xi++;
                f.xf = f.xf - Fix64V1.ONE;
            }
            if (f.yf \u003e= Fix64V1.ONE) {
                f.yi++;
                f.yf = f.yf - Fix64V1.ONE;
            }
            if (f.zf \u003e= Fix64V1.ONE) {
                f.zi++;
                f.zf = f.zf - Fix64V1.ONE;
            }
        }

        return r;
    }

    function scaled_cosine(int64 i)
        private
        pure
        returns (int64)
    {
        int64 angle = Fix64V1.mul(i, Fix64V1.PI);      
        int64 cosine = Trig256.cos(angle);
        int64 scaled = Fix64V1.mul(
                2147483648, /* 0.5f */
                Fix64V1.sub(Fix64V1.ONE, cosine)
            );        

        return scaled;            
    }
}
"},"ParticleSetFactoryV1.sol":{"content":"// SPDX-License-Identifier: UNLICENSED
/* Copyright (c) 2021 Kohi Art Community, Inc. All rights reserved. */

pragma solidity ^0.8.0;

import \"ParticleSetV1.sol\";
import \"ParticleV1.sol\";
import \"RandomV1.sol\";

/*
    A noise-based particle simulator, built for generative art that uses flow fields.

    Based on techniques in Sighack\u0027s \"Getting Creative with Perlin Noise Fields\":
    See: https://github.com/sighack/perlin-noise-fields
    See: https://github.com/sighack/perlin-noise-fields/blob/master/LICENSE

    THIRD PARTY NOTICES:
    ====================

    MIT License

    Copyright (c) 2018 Manohar Vanga

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the \"Software\"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

library ParticleSetFactoryV1 {
    uint16 internal constant PARTICLE_TABLE_SIZE = 5000;

    struct CreateParticleSet2D {
        int32 seed;
        uint32 range;
        uint16 width;
        uint16 height;
        uint16 n;
        int64 forceScale;
        int64 noiseScale;
        uint8 lifetime;
    }

    function createSet(CreateParticleSet2D memory f, RandomV1.PRNG memory prng)
        external
        pure
        returns (ParticleSetV1.ParticleSet2D memory set, RandomV1.PRNG memory)
    {
        ParticleV1.Particle2D[PARTICLE_TABLE_SIZE] memory particles;

        for (uint16 i = 0; i \u003c f.n; i++) {  

            int256 px = RandomV1.next(
                prng,
                -int32(f.range),
                int16(f.width) + int32(f.range)
            );

            int256 py = RandomV1.next(
                prng,
                -int32(f.range),
                int16(f.height) + int32(f.range)
            );

            ParticleV1.Particle2D memory particle = ParticleV1.Particle2D(
                int64(px),
                int64(py),
                0,
                0,
                int64(px),
                int64(py),
                0,
                false,
                TypesV1.Point2D(0, 0),
                f.lifetime,
                f.forceScale,
                f.noiseScale
            );
            particles[i] = particle;
        }

        set.particles = particles;
        return (set, prng);
    }
}"},"ParticleSetV1.sol":{"content":"// SPDX-License-Identifier: UNLICENSED
/* Copyright (c) 2021 Kohi Art Community, Inc. All rights reserved. */

pragma solidity ^0.8.0;

import \"ParticleV1.sol\";
import \"TypesV1.sol\";

/*
    A noise-based particle simulator, built for generative art that uses flow fields.

    Based on techniques in Sighack\u0027s \"Getting Creative with Perlin Noise Fields\":
    See: https://github.com/sighack/perlin-noise-fields
    See: https://github.com/sighack/perlin-noise-fields/blob/master/LICENSE

    THIRD PARTY NOTICES:
    ====================

    MIT License

    Copyright (c) 2018 Manohar Vanga

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the \"Software\"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

abstract contract ParticleSetV1 {
    uint16 internal constant NOISE_TABLE_SIZE = 4095;
    uint16 internal constant PARTICLE_TABLE_SIZE = 5000;

    struct ParticleSet2D {
        ParticleV1.Particle2D[5000] particles;
        bool dead;
    }
    
    function update(
        int64[NOISE_TABLE_SIZE + 1] memory noiseTable,
        ParticleSet2D memory set,
        uint16 particleCount,
        uint256 width,
        uint256 height
    ) internal pure {
        set.dead = true;
        for (uint16 i = 0; i \u003c particleCount; i++) {
            ParticleV1.Particle2D memory p = set.particles[i];
            if (p.dead) {
                continue;
            }
            set.dead = false;
            ParticleV1.update(noiseTable, p, width, height);
        }
    }

    function draw(
        ParticleSet2D memory set,
        uint16 particleCount,
        uint32[16384] memory result,
        TypesV1.Chunk2D memory chunk
    ) internal pure {
        if (set.dead) {
            return;
        }

        for (uint256 i = 0; i \u003c particleCount; i++) {
            ParticleV1.Particle2D memory p = set.particles[i];
            if (p.dead) {
                continue;
            }
            step(p, result, chunk);
        }
    }

    function step(
        ParticleV1.Particle2D memory p, uint32[16384] memory result,
        TypesV1.Chunk2D memory chunk
    ) internal pure virtual;
}
"},"ParticleV1.sol":{"content":"// SPDX-License-Identifier: UNLICENSED
/* Copyright (c) 2021 Kohi Art Community, Inc. All rights reserved. */

pragma solidity ^0.8.0;

import \"Fix64V1.sol\";
import \"TypesV1.sol\";
import \"NoiseV1.sol\";
import \"RandomV1.sol\";
import \"Trig256.sol\";

/*
    A noise-based particle simulator, built for generative art that uses flow fields.

    Based on techniques in Sighack\u0027s \"Getting Creative with Perlin Noise Fields\":
    See: https://github.com/sighack/perlin-noise-fields
    See: https://github.com/sighack/perlin-noise-fields/blob/master/LICENSE

    THIRD PARTY NOTICES:
    ====================

    MIT License

    Copyright (c) 2018 Manohar Vanga

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the \"Software\"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

library ParticleV1 {
    uint16 internal constant NOISE_TABLE_SIZE = 4096;

    struct Particle2D {
        int64 ox;
        int64 oy;
        int64 px;
        int64 py;
        int64 x;
        int64 y;
        uint32 frames;
        bool dead;
        TypesV1.Point2D force;
        uint8 _lifetime;
        int64 _forceScale;
        int64 _noiseScale;
    }

    function update(
        int64[NOISE_TABLE_SIZE] memory noiseTable,
        Particle2D memory p,
        uint256 width,
        uint256 height
    ) internal pure {
        p.frames++;

        if (p.frames \u003e= p._lifetime) {
            p.dead = true;
            return;
        }

        p.force = forceAt(noiseTable, p, p.x, p.y);

        if (
            p.x \u003e= int256(width) + int256(width) / 2 ||
            p.x \u003c -int256(width) / 2 ||
            p.y \u003e= int256(height) + int256(height) / 2 ||
            p.y \u003c -int256(height) / 2
        ) {
            p.dead = true;
            return;
        }

        p.px = p.x;
        p.py = p.y;                

        p.x += int64(p.force.x);
        p.y += int64(p.force.y);        
    }

    function forceAt(
        int64[NOISE_TABLE_SIZE] memory noiseTable,
        Particle2D memory p,
        int64 x,
        int64 y
    ) internal pure returns (TypesV1.Point2D memory force) {

        int64 nx = Fix64V1.mul(x * Fix64V1.ONE, p._noiseScale);       
        int64 ny = Fix64V1.mul(y * Fix64V1.ONE, p._noiseScale);
        
        int64 noise = NoiseV1.noise(noiseTable, nx, ny);
        int64 theta = Fix64V1.mul(noise, Fix64V1.TWO_PI);

        return forceFromAngle(p, theta);
    }

    function forceFromAngle(Particle2D memory p, int64 theta)
        internal
        pure
        returns (TypesV1.Point2D memory force)
    {
        int64 px = Trig256.cos(theta);
        int64 py = Trig256.sin(theta);

        int64 pxl = Fix64V1.mul(px, p._forceScale) \u003e\u003e 32 /* FRACTIONAL_PLACES */;
        int64 pyl = Fix64V1.mul(py, p._forceScale) \u003e\u003e 32 /* FRACTIONAL_PLACES */;        
        
        force = TypesV1.Point2D(int32(pxl), int32(pyl));
    }
}
"},"ProcessingV1.sol":{"content":"// SPDX-License-Identifier: UNLICENSED
/* Copyright (c) 2021 Kohi Art Community, Inc. All rights reserved. */

pragma solidity ^0.8.0;

import \"TypesV1.sol\";
import \"GraphicsV1.sol\";
import \"GeometryV1.sol\";
import \"RandomV1.sol\";
import \"MathV1.sol\";

library ProcessingV1 {
    uint32 internal constant BG_COLOR = 0xFFD3D3D3;
    uint32 internal constant FILL_COLOR = 0xFFFFFFFF;
    uint32 internal constant STROKE_COLOR = 0x00000000;
    uint32 internal constant MAX_POLYGON_NODES = 400;

    /**
     * @notice Sets the color used for the background of the drawing surface.
     * @notice https://processing.org/reference/background_.html
     */
    function background(
        uint32[16384] /* 128 * 128 */
            memory result,
        uint32 color,
        TypesV1.Chunk2D memory chunk
    ) internal pure {
        for (uint256 x = 0; x \u003c chunk.chunkWidth; x++) {
            for (uint256 y = 0; y \u003c chunk.chunkHeight; y++) {
                GraphicsV1.setPixel(
                    result,
                    chunk.chunkWidth,
                    int256(x),
                    int256(y),
                    color
                );
            }
        }
    }

    /**
     * @notice A triangle is a plane created by connecting three points. The first two arguments specify the first point, the middle two arguments specify the second point, and the last two arguments specify the third point.
     * @notice https://processing.org/reference/triangle_.html
     * @dev Renders a filled triangle, using the Barycentric rasterization algorithm.
     */
    function triangle(
        uint32[16384] /* 128 * 128 */
            memory result,
        GeometryV1.Triangle2D memory f
    ) internal pure {
        TypesV1.Point2D memory p;

        uint256 minX = f.chunk.startX;
        uint256 maxX = (f.chunk.startX + f.chunk.chunkWidth) - 1;
        uint256 minY = f.chunk.startY;
        uint256 maxY = (f.chunk.startY + f.chunk.chunkHeight) - 1;

        while (GeometryV1.edge(f.v0, f.v1, f.v2) \u003c 0) {
            TypesV1.Point2D memory temp = f.v1;
            f.v1 = f.v2;
            f.v2 = temp;
        }

        for (p.x = int256(minX); p.x \u003c= int256(maxX); p.x++) {
            for (p.y = int256(minY); p.y \u003c= int256(maxY); p.y++) {
                int256 w0 = GeometryV1.edge(f.v1, f.v2, p);
                int256 w1 = GeometryV1.edge(f.v2, f.v0, p);
                int256 w2 = GeometryV1.edge(f.v0, f.v1, p);

                if (w0 \u003e= 0 \u0026\u0026 w1 \u003e= 0 \u0026\u0026 w2 \u003e= 0) {
                    GraphicsV1.setPixel(
                        result,
                        f.chunk.chunkWidth,
                        int256(p.x - int32(f.chunk.startX)),
                        int256(p.y - int32(f.chunk.startY)),
                        f.fillColor
                    );
                }
            }
        }

        if (f.strokeColor == f.fillColor) return;

        {
            line(result, GeometryV1.Line2D(f.v0, f.v1, f.strokeColor, f.chunk));
            line(result, GeometryV1.Line2D(f.v1, f.v2, f.strokeColor, f.chunk));
            line(result, GeometryV1.Line2D(f.v2, f.v0, f.strokeColor, f.chunk));
        }
    }

    /**
     * @notice Draws a line (a direct path between two points) to the screen.
     * @notice https://processing.org/reference/line_.html
     * @dev Renders a line between two points, using Bresenham\u0027s rasterization algorithm.
     */
    function line(uint32[16384]memory result, GeometryV1.Line2D memory f
    ) internal pure {
        int256 x0 = f.v0.x;
        int256 x1 = f.v1.x;
        int256 y0 = f.v0.y;
        int256 y1 = f.v1.y;

        int256 dx = MathV1.abs(x1 - x0);        
        int256 dy = MathV1.abs(y1 - y0);

        int256 err = (dx \u003e dy ? dx : -dy) / 2;
        int256 e2;

        for (;;) {
            if (
                x0 \u003c= int32(f.chunk.startX) + int16(f.chunk.chunkWidth) - 1 \u0026\u0026
                x0 \u003e= int32(f.chunk.startX) \u0026\u0026
                y0 \u003c= int32(f.chunk.startY) + int16(f.chunk.chunkHeight) - 1 \u0026\u0026
                y0 \u003e= int32(f.chunk.startY)
            ) {
                GraphicsV1.setPixel(
                    result,
                    f.chunk.chunkWidth,
                    x0 - int32(f.chunk.startX),
                    y0 - int32(f.chunk.startY),
                    f.color
                );
            }

            if (x0 == x1 \u0026\u0026 y0 == y1) break;
            e2 = err;
            if (e2 \u003e -dx) {
                err -= dy;
                x0 += x0 \u003c x1 ? int8(1) : -1;
            }
            if (e2 \u003c dy) {
                err += dx;
                y0 += y0 \u003c y1 ? int8(1) : -1;
            }
        }
    }

    /**
     * @notice Draw a polygon shape to the screen.
     * @notice https://processing.org/reference/beginShape_.html
     * @notice https://processing.org/reference/vertex_.html
     * @notice https://processing.org/reference/endShape_.html
     * @dev Renders a filled polygon, using Finley\u0027s algorithm.
     */
    function polygon(uint32[16384] memory result, GeometryV1.Polygon2D memory f
    ) internal pure {
        uint32 polyCorners = f.vertexCount;

        int32[MAX_POLYGON_NODES] memory nodeX;

        for (uint32 pixelY = f.chunk.startY; pixelY \u003c (f.chunk.startY + f.chunk.chunkHeight); pixelY++) {
            uint32 i;

            uint256 nodes = 0;
            uint32 j = polyCorners - 1;
            for (i = 0; i \u003c polyCorners; i++) {
                
                TypesV1.Point2D memory a = TypesV1.Point2D(
                    f.vertices[i].x,
                    f.vertices[i].y
                );
                TypesV1.Point2D memory b = TypesV1.Point2D(
                    f.vertices[j].x,
                    f.vertices[j].y
                );

                if (
                    (a.y \u003c int32(pixelY) \u0026\u0026 b.y \u003e= int32(pixelY)) ||
                    (b.y \u003c int32(pixelY) \u0026\u0026 a.y \u003e= int32(pixelY))
                ) {
                    int32 t = int32(a.x) + (int32(pixelY) - int32(a.y)) / (int32(b.y) - int32(a.y)) * (int32(b.x) - int32(a.x));
                    nodeX[nodes++] = t;
                }

                j = i;
            }

            if(nodes == 0) {
                continue; // nothing to draw
            }

            i = 0;
            while (i \u003c nodes - 1) {
                if (nodeX[i] \u003e nodeX[i + 1]) {
                    (nodeX[i], nodeX[i + 1]) = (nodeX[i + 1], nodeX[i]);
                    if (i != 0) i--;
                } else {
                    i++;
                }
            }

            for (i = 0; i \u003c nodes; i += 2) {
                
                if (nodeX[i] \u003e= int32(f.chunk.startX) + int16(f.chunk.chunkHeight)) break;
                if (nodeX[i + 1] \u003c= int32(f.chunk.startX)) continue;
                if (nodeX[i] \u003c int32(f.chunk.startX)) nodeX[i] = int32(f.chunk.startX);                
                if (nodeX[i + 1] \u003e int32(f.chunk.startX) + int16(f.chunk.chunkHeight))
                    nodeX[i + 1] = int32(int32(f.chunk.startX) + int16(f.chunk.chunkHeight));

                for (
                    int32 pixelX = nodeX[i];
                    pixelX \u003c nodeX[i + 1];
                    pixelX++
                ) {
                    if (pixelX \u003e= int32(f.chunk.startX) + int16(f.chunk.chunkHeight)) continue;

                    int32 px = int32(pixelX) - int32(f.chunk.startX);
                    int32 py = int32(pixelY) - int32(f.chunk.startY);

                    if (
                        px \u003e= 0 \u0026\u0026
                        px \u003c int16(f.chunk.chunkWidth) \u0026\u0026
                        py \u003e= 0 \u0026\u0026
                        py \u003c int16(f.chunk.chunkHeight)
                    ) {
                        GraphicsV1.setPixel(
                            result,
                            f.chunk.chunkWidth,
                            px,
                            py,
                            f.fillColor
                        );
                    }
                }
            }
        }

        if (f.strokeColor == f.fillColor) return;

        {
            uint256 j = f.vertices.length - 1;
            for (uint256 i = 0; i \u003c f.vertices.length; i++) {
                TypesV1.Point2D memory a = f.vertices[i];
                TypesV1.Point2D memory b = f.vertices[j];
                line(result, GeometryV1.Line2D(a, b, f.strokeColor, f.chunk));
                j = i;
            }
            line(
                result,
                GeometryV1.Line2D(
                    f.vertices[f.vertices.length - 1],
                    f.vertices[0],
                    f.strokeColor,
                    f.chunk
                )
            );
        }
    }

    /**
     * @notice Renders a number from a random series of numbers having a mean of 0 and standard deviation of 1.
     * @notice https://processing.org/reference/randomGaussian_.html
     */
    function randomGaussian(RandomV1.PRNG memory prng)
        internal
        pure
        returns (int64) {
        return RandomV1.nextGaussian(prng);
    }
}"},"RandomV1.sol":{"content":"// SPDX-License-Identifier: UNLICENSED
/* Copyright (c) 2021 Kohi Art Community, Inc. All rights reserved. */

pragma solidity ^0.8.0;

import \"Fix64V1.sol\";
import \"Trig256.sol\";
import \"MathV1.sol\";

/*
    A pseudo-random number generator, adapted from and matching the algorithm for .NET maximum compatibility Random implementation.

    See: https://github.com/dotnet/runtime/blob/f7633f498a8be34bee739b240a0aa9ae6a660cd9/src/libraries/System.Private.CoreLib/src/System/Random.Net5CompatImpl.cs#L192
         https://github.com/dotnet/runtime/blob/main/LICENSE.TXT

    THIRD PARTY NOTICES:
    ====================

    The MIT License (MIT)

    Copyright (c) .NET Foundation and Contributors

    All rights reserved.

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the \"Software\"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

library RandomV1 {

    int32 private constant MBIG = 0x7fffffff;
    int32 private constant MSEED = 161803398;

    struct PRNG {
        int32[56] _seedArray;
        int32 _inext;
        int32 _inextp;
    }
    
    function buildSeedTable(int32 seed) internal pure returns(PRNG memory prng) {
        uint8 ii = 0;
        int32 mj;
        int32 mk;

        int32 subtraction = (seed == type(int32).min) ? type(int32).max : int32(MathV1.abs(seed));
        mj = MSEED - subtraction;
        prng._seedArray[55] = mj;
        mk = 1;
        for (uint8 i = 1; i \u003c 55; i++) {  
            if ((ii += 21) \u003e= 55) {
                ii -= 55;
            }
            prng._seedArray[uint64(ii)] = mk;
            mk = mj - mk;
            if (mk \u003c 0) mk += MBIG;
            mj = prng._seedArray[uint8(ii)];
        }

        for (uint8 k = 1; k \u003c 5; k++) {

            for (uint8 i = 1; i \u003c 56; i++) {                
                uint8 n = i + 30;           
                if (n \u003e= 55) {
                    n -= 55;                
                }

                int64 an = prng._seedArray[1 + n];                
                int64 ai = prng._seedArray[i];
                prng._seedArray[i] = int32(ai - an);
                
                if (prng._seedArray[i] \u003c 0) {
                    int64 x = prng._seedArray[i];
                    x += MBIG;
                    prng._seedArray[i] = int32(x);
                }               
            }
        }

        prng._inextp = 21;
    }   

    function next(PRNG memory prng, int32 maxValue) internal pure returns (int32) {
        require(maxValue \u003e= 0, \"maxValue \u003c 0\");

        int32 retval = next(prng);

        int64 fretval = retval * Fix64V1.ONE;
        int64 sample = Fix64V1.mul(fretval, Fix64V1.div(Fix64V1.ONE, MBIG * Fix64V1.ONE));
        int64 sr = Fix64V1.mul(sample, maxValue * Fix64V1.ONE);
        int32 r = int32(sr \u003e\u003e 32 /* FRACTIONAL_PLACES */);

        return r;
    }

    function next(PRNG memory prng, int32 minValue, int32 maxValue) internal pure returns(int32) {
        require(maxValue \u003e minValue, \"maxValue \u003c= minValue\");
        
        int64 range = maxValue - minValue;
        
        if (range \u003c= type(int32).max) {
            int32 retval = next(prng);

            int64 fretval = retval * Fix64V1.ONE;
            int64 sample = Fix64V1.mul(fretval, Fix64V1.div(Fix64V1.ONE, MBIG * Fix64V1.ONE));
            int64 sr = Fix64V1.mul(sample, range * Fix64V1.ONE);
            int32 r = int32(sr \u003e\u003e 32  /* FRACTIONAL_PLACES */) + minValue;
            
            return r;
        }
        else {
            int64 fretval = nextForLargeRange(prng);
            int64 sr = Fix64V1.mul(fretval, range * Fix64V1.ONE);
            int32 r = int32(sr \u003e\u003e 32  /* FRACTIONAL_PLACES */) + minValue;
            return r;
        }
    }

    function next(PRNG memory prng) internal pure returns(int32) {

        int64 retVal;        
        int32 locINext = prng._inext;
        int32 locINextp = prng._inextp;

        if (++locINext \u003e= 56) locINext = 1;
        if (++locINextp \u003e= 56) locINextp = 1;

        int64 a = int64(prng._seedArray[uint32(locINext)]);
        int64 b = int64(prng._seedArray[uint32(locINextp)]);
        retVal = a - b;        

        if (retVal == MBIG) {
            retVal--;
        }
        if (retVal \u003c 0) {
            retVal += MBIG;
        }

        prng._seedArray[uint32(locINext)] = int32(retVal);
        prng._inext = locINext;
        prng._inextp = locINextp;        

        int32 r = int32(retVal);
        return r;
    }

    function nextForLargeRange(PRNG memory prng) private pure returns(int64) {

        int sample1 = next(prng);
        int sample2 = next(prng);

        bool negative = sample2 % 2 == 0;
        if (negative) {
            sample1 = -sample1;
        }

        int64 d = int64(sample1) * Fix64V1.ONE;
        d = Fix64V1.add(int64(d), (type(int32).max - 1));
        d = Fix64V1.div(int64(d), int64(2) * (type(int32).max - 1));

        return d; 
    }

    function nextGaussian(PRNG memory prng) internal pure returns (int64 randNormal) {
        int64 u1 = Fix64V1.sub(Fix64V1.ONE, Fix64V1.mul(next(prng) * Fix64V1.ONE, Fix64V1.div(Fix64V1.ONE, Fix64V1.MAX_VALUE)));
        int64 u2 = Fix64V1.sub(Fix64V1.ONE, Fix64V1.mul(next(prng) * Fix64V1.ONE, Fix64V1.div(Fix64V1.ONE, Fix64V1.MAX_VALUE)));
        int64 sqrt = Trig256.sqrt(Fix64V1.mul(-2 * Fix64V1.ONE, Trig256.log(u1)));
        int64 randStdNormal = Fix64V1.mul(sqrt, Trig256.sin(Fix64V1.mul(Fix64V1.TWO, Fix64V1.mul(Fix64V1.PI, u2))));
        randNormal = Fix64V1.add(0, Fix64V1.mul(Fix64V1.ONE, randStdNormal));
        return randNormal;
    }
}"},"SinLut256.sol":{"content":"// SPDX-License-Identifier: UNLICENSED
/* Copyright (c) 2021 Kohi Art Community Inc. All rights reserved. */

pragma solidity ^0.8.0;

library SinLut256 {
    /**
     * @notice Lookup tables for computing the sine value for a given angle.
     * @param i The clamped and rounded angle integral to index into the table.
     * @return The sine value in fixed-point (Q31.32) space.
     */
    function sinlut(int256 i) external pure returns (int64) {
        if (i \u003c= 127) {
            if (i \u003c= 63) {
                if (i \u003c= 31) {
                    if (i \u003c= 15) {
                        if (i \u003c= 7) {
                            if (i \u003c= 3) {
                                if (i \u003c= 1) {
                                    if (i == 0) {
                                        return 0;
                                    } else {
                                        return 26456769;
                                    }
                                } else {
                                    if (i == 2) {
                                        return 52912534;
                                    } else {
                                        return 79366292;
                                    }
                                }
                            } else {
                                if (i \u003c= 5) {
                                    if (i == 4) {
                                        return 105817038;
                                    } else {
                                        return 132263769;
                                    }
                                } else {
                                    if (i == 6) {
                                        return 158705481;
                                    } else {
                                        return 185141171;
                                    }
                                }
                            }
                        } else {
                            if (i \u003c= 11) {
                                if (i \u003c= 9) {
                                    if (i == 8) {
                                        return 211569835;
                                    } else {
                                        return 237990472;
                                    }
                                } else {
                                    if (i == 10) {
                                        return 264402078;
                                    } else {
                                        return 290803651;
                                    }
                                }
                            } else {
                                if (i \u003c= 13) {
                                    if (i == 12) {
                                        return 317194190;
                                    } else {
                                        return 343572692;
                                    }
                                } else {
                                    if (i == 14) {
                                        return 369938158;
                                    } else {
                                        return 396289586;
                                    }
                                }
                            }
                        }
                    } else {
                        if (i \u003c= 23) {
                            if (i \u003c= 19) {
                                if (i \u003c= 17) {
                                    if (i == 16) {
                                        return 422625977;
                                    } else {
                                        return 448946331;
                                    }
                                } else {
                                    if (i == 18) {
                                        return 475249649;
                                    } else {
                                        return 501534935;
                                    }
                                }
                            } else {
                                if (i \u003c= 21) {
                                    if (i == 20) {
                                        return 527801189;
                                    } else {
                                        return 554047416;
                                    }
                                } else {
                                    if (i == 22) {
                                        return 580272619;
                                    } else {
                                        return 606475804;
                                    }
                                }
                            }
                        } else {
                            if (i \u003c= 27) {
                                if (i \u003c= 25) {
                                    if (i == 24) {
                                        return 632655975;
                                    } else {
                                        return 658812141;
                                    }
                                } else {
                                    if (i == 26) {
                                        return 684943307;
                                    } else {
                                        return 711048483;
                                    }
                                }
                            } else {
                                if (i \u003c= 29) {
                                    if (i == 28) {
                                        return 737126679;
                                    } else {
                                        return 763176903;
                                    }
                                } else {
                                    if (i == 30) {
                                        return 789198169;
                                    } else {
                                        return 815189489;
                                    }
                                }
                            }
                        }
                    }
                } else {
                    if (i \u003c= 47) {
                        if (i \u003c= 39) {
                            if (i \u003c= 35) {
                                if (i \u003c= 33) {
                                    if (i == 32) {
                                        return 841149875;
                                    } else {
                                        return 867078344;
                                    }
                                } else {
                                    if (i == 34) {
                                        return 892973912;
                                    } else {
                                        return 918835595;
                                    }
                                }
                            } else {
                                if (i \u003c= 37) {
                                    if (i == 36) {
                                        return 944662413;
                                    } else {
                                        return 970453386;
                                    }
                                } else {
                                    if (i == 38) {
                                        return 996207534;
                                    } else {
                                        return 1021923881;
                                    }
                                }
                            }
                        } else {
                            if (i \u003c= 43) {
                                if (i \u003c= 41) {
                                    if (i == 40) {
                                        return 1047601450;
                                    } else {
                                        return 1073239268;
                                    }
                                } else {
                                    if (i == 42) {
                                        return 1098836362;
                                    } else {
                                        return 1124391760;
                                    }
                                }
                            } else {
                                if (i \u003c= 45) {
                                    if (i == 44) {
                                        return 1149904493;
                                    } else {
                                        return 1175373592;
                                    }
                                } else {
                                    if (i == 46) {
                                        return 1200798091;
                                    } else {
                                        return 1226177026;
                                    }
                                }
                            }
                        }
                    } else {
                        if (i \u003c= 55) {
                            if (i \u003c= 51) {
                                if (i \u003c= 49) {
                                    if (i == 48) {
                                        return 1251509433;
                                    } else {
                                        return 1276794351;
                                    }
                                } else {
                                    if (i == 50) {
                                        return 1302030821;
                                    } else {
                                        return 1327217884;
                                    }
                                }
                            } else {
                                if (i \u003c= 53) {
                                    if (i == 52) {
                                        return 1352354586;
                                    } else {
                                        return 1377439973;
                                    }
                                } else {
                                    if (i == 54) {
                                        return 1402473092;
                                    } else {
                                        return 1427452994;
                                    }
                                }
                            }
                        } else {
                            if (i \u003c= 59) {
                                if (i \u003c= 57) {
                                    if (i == 56) {
                                        return 1452378731;
                                    } else {
                                        return 1477249357;
                                    }
                                } else {
                                    if (i == 58) {
                                        return 1502063928;
                                    } else {
                                        return 1526821503;
                                    }
                                }
                            } else {
                                if (i \u003c= 61) {
                                    if (i == 60) {
                                        return 1551521142;
                                    } else {
                                        return 1576161908;
                                    }
                                } else {
                                    if (i == 62) {
                                        return 1600742866;
                                    } else {
                                        return 1625263084;
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                if (i \u003c= 95) {
                    if (i \u003c= 79) {
                        if (i \u003c= 71) {
                            if (i \u003c= 67) {
                                if (i \u003c= 65) {
                                    if (i == 64) {
                                        return 1649721630;
                                    } else {
                                        return 1674117578;
                                    }
                                } else {
                                    if (i == 66) {
                                        return 1698450000;
                                    } else {
                                        return 1722717974;
                                    }
                                }
                            } else {
                                if (i \u003c= 69) {
                                    if (i == 68) {
                                        return 1746920580;
                                    } else {
                                        return 1771056897;
                                    }
                                } else {
                                    if (i == 70) {
                                        return 1795126012;
                                    } else {
                                        return 1819127010;
                                    }
                                }
                            }
                        } else {
                            if (i \u003c= 75) {
                                if (i \u003c= 73) {
                                    if (i == 72) {
                                        return 1843058980;
                                    } else {
                                        return 1866921015;
                                    }
                                } else {
                                    if (i == 74) {
                                        return 1890712210;
                                    } else {
                                        return 1914431660;
                                    }
                                }
                            } else {
                                if (i \u003c= 77) {
                                    if (i == 76) {
                                        return 1938078467;
                                    } else {
                                        return 1961651733;
                                    }
                                } else {
                                    if (i == 78) {
                                        return 1985150563;
                                    } else {
                                        return 2008574067;
                                    }
                                }
                            }
                        }
                    } else {
                        if (i \u003c= 87) {
                            if (i \u003c= 83) {
                                if (i \u003c= 81) {
                                    if (i == 80) {
                                        return 2031921354;
                                    } else {
                                        return 2055191540;
                                    }
                                } else {
                                    if (i == 82) {
                                        return 2078383740;
                                    } else {
                                        return 2101497076;
                                    }
                                }
                            } else {
                                if (i \u003c= 85) {
                                    if (i == 84) {
                                        return 2124530670;
                                    } else {
                                        return 2147483647;
                                    }
                                } else {
                                    if (i == 86) {
                                        return 2170355138;
                                    } else {
                                        return 2193144275;
                                    }
                                }
                            }
                        } else {
                            if (i \u003c= 91) {
                                if (i \u003c= 89) {
                                    if (i == 88) {
                                        return 2215850191;
                                    } else {
                                        return 2238472027;
                                    }
                                } else {
                                    if (i == 90) {
                                        return 2261008923;
                                    } else {
                                        return 2283460024;
                                    }
                                }
                            } else {
                                if (i \u003c= 93) {
                                    if (i == 92) {
                                        return 2305824479;
                                    } else {
                                        return 2328101438;
                                    }
                                } else {
                                    if (i == 94) {
                                        return 2350290057;
                                    } else {
                                        return 2372389494;
                                    }
                                }
                            }
                        }
                    }
                } else {
                    if (i \u003c= 111) {
                        if (i \u003c= 103) {
                            if (i \u003c= 99) {
                                if (i \u003c= 97) {
                                    if (i == 96) {
                                        return 2394398909;
                                    } else {
                                        return 2416317469;
                                    }
                                } else {
                                    if (i == 98) {
                                        return 2438144340;
                                    } else {
                                        return 2459878695;
                                    }
                                }
                            } else {
                                if (i \u003c= 101) {
                                    if (i == 100) {
                                        return 2481519710;
                                    } else {
                                        return 2503066562;
                                    }
                                } else {
                                    if (i == 102) {
                                        return 2524518435;
                                    } else {
                                        return 2545874514;
                                    }
                                }
                            }
                        } else {
                            if (i \u003c= 107) {
                                if (i \u003c= 105) {
                                    if (i == 104) {
                                        return 2567133990;
                                    } else {
                                        return 2588296054;
                                    }
                                } else {
                                    if (i == 106) {
                                        return 2609359905;
                                    } else {
                                        return 2630324743;
                                    }
                                }
                            } else {
                                if (i \u003c= 109) {
                                    if (i == 108) {
                                        return 2651189772;
                                    } else {
                                        return 2671954202;
                                    }
                                } else {
                                    if (i == 110) {
                                        return 2692617243;
                                    } else {
                                        return 2713178112;
                                    }
                                }
                            }
                        }
                    } else {
                        if (i \u003c= 119) {
                            if (i \u003c= 115) {
                                if (i \u003c= 113) {
                                    if (i == 112) {
                                        return 2733636028;
                                    } else {
                                        return 2753990216;
                                    }
                                } else {
                                    if (i == 114) {
                                        return 2774239903;
                                    } else {
                                        return 2794384321;
                                    }
                                }
                            } else {
                                if (i \u003c= 117) {
                                    if (i == 116) {
                                        return 2814422705;
                                    } else {
                                        return 2834354295;
                                    }
                                } else {
                                    if (i == 118) {
                                        return 2854178334;
                                    } else {
                                        return 2873894071;
                                    }
                                }
                            }
                        } else {
                            if (i \u003c= 123) {
                                if (i \u003c= 121) {
                                    if (i == 120) {
                                        return 2893500756;
                                    } else {
                                        return 2912997648;
                                    }
                                } else {
                                    if (i == 122) {
                                        return 2932384004;
                                    } else {
                                        return 2951659090;
                                    }
                                }
                            } else {
                                if (i \u003c= 125) {
                                    if (i == 124) {
                                        return 2970822175;
                                    } else {
                                        return 2989872531;
                                    }
                                } else {
                                    if (i == 126) {
                                        return 3008809435;
                                    } else {
                                        return 3027632170;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } else {
            if (i \u003c= 191) {
                if (i \u003c= 159) {
                    if (i \u003c= 143) {
                        if (i \u003c= 135) {
                            if (i \u003c= 131) {
                                if (i \u003c= 129) {
                                    if (i == 128) {
                                        return 3046340019;
                                    } else {
                                        return 3064932275;
                                    }
                                } else {
                                    if (i == 130) {
                                        return 3083408230;
                                    } else {
                                        return 3101767185;
                                    }
                                }
                            } else {
                                if (i \u003c= 133) {
                                    if (i == 132) {
                                        return 3120008443;
                                    } else {
                                        return 3138131310;
                                    }
                                } else {
                                    if (i == 134) {
                                        return 3156135101;
                                    } else {
                                        return 3174019130;
                                    }
                                }
                            }
                        } else {
                            if (i \u003c= 139) {
                                if (i \u003c= 137) {
                                    if (i == 136) {
                                        return 3191782721;
                                    } else {
                                        return 3209425199;
                                    }
                                } else {
                                    if (i == 138) {
                                        return 3226945894;
                                    } else {
                                        return 3244344141;
                                    }
                                }
                            } else {
                                if (i \u003c= 141) {
                                    if (i == 140) {
                                        return 3261619281;
                                    } else {
                                        return 3278770658;
                                    }
                                } else {
                                    if (i == 142) {
                                        return 3295797620;
                                    } else {
                                        return 3312699523;
                                    }
                                }
                            }
                        }
                    } else {
                        if (i \u003c= 151) {
                            if (i \u003c= 147) {
                                if (i \u003c= 145) {
                                    if (i == 144) {
                                        return 3329475725;
                                    } else {
                                        return 3346125588;
                                    }
                                } else {
                                    if (i == 146) {
                                        return 3362648482;
                                    } else {
                                        return 3379043779;
                                    }
                                }
                            } else {
                                if (i \u003c= 149) {
                                    if (i == 148) {
                                        return 3395310857;
                                    } else {
                                        return 3411449099;
                                    }
                                } else {
                                    if (i == 150) {
                                        return 3427457892;
                                    } else {
                                        return 3443336630;
                                    }
                                }
                            }
                        } else {
                            if (i \u003c= 155) {
                                if (i \u003c= 153) {
                                    if (i == 152) {
                                        return 3459084709;
                                    } else {
                                        return 3474701532;
                                    }
                                } else {
                                    if (i == 154) {
                                        return 3490186507;
                                    } else {
                                        return 3505539045;
                                    }
                                }
                            } else {
                                if (i \u003c= 157) {
                                    if (i == 156) {
                                        return 3520758565;
                                    } else {
                                        return 3535844488;
                                    }
                                } else {
                                    if (i == 158) {
                                        return 3550796243;
                                    } else {
                                        return 3565613262;
                                    }
                                }
                            }
                        }
                    }
                } else {
                    if (i \u003c= 175) {
                        if (i \u003c= 167) {
                            if (i \u003c= 163) {
                                if (i \u003c= 161) {
                                    if (i == 160) {
                                        return 3580294982;
                                    } else {
                                        return 3594840847;
                                    }
                                } else {
                                    if (i == 162) {
                                        return 3609250305;
                                    } else {
                                        return 3623522808;
                                    }
                                }
                            } else {
                                if (i \u003c= 165) {
                                    if (i == 164) {
                                        return 3637657816;
                                    } else {
                                        return 3651654792;
                                    }
                                } else {
                                    if (i == 166) {
                                        return 3665513205;
                                    } else {
                                        return 3679232528;
                                    }
                                }
                            }
                        } else {
                            if (i \u003c= 171) {
                                if (i \u003c= 169) {
                                    if (i == 168) {
                                        return 3692812243;
                                    } else {
                                        return 3706251832;
                                    }
                                } else {
                                    if (i == 170) {
                                        return 3719550786;
                                    } else {
                                        return 3732708601;
                                    }
                                }
                            } else {
                                if (i \u003c= 173) {
                                    if (i == 172) {
                                        return 3745724777;
                                    } else {
                                        return 3758598821;
                                    }
                                } else {
                                    if (i == 174) {
                                        return 3771330243;
                                    } else {
                                        return 3783918561;
                                    }
                                }
                            }
                        }
                    } else {
                        if (i \u003c= 183) {
                            if (i \u003c= 179) {
                                if (i \u003c= 177) {
                                    if (i == 176) {
                                        return 3796363297;
                                    } else {
                                        return 3808663979;
                                    }
                                } else {
                                    if (i == 178) {
                                        return 3820820141;
                                    } else {
                                        return 3832831319;
                                    }
                                }
                            } else {
                                if (i \u003c= 181) {
                                    if (i == 180) {
                                        return 3844697060;
                                    } else {
                                        return 3856416913;
                                    }
                                } else {
                                    if (i == 182) {
                                        return 3867990433;
                                    } else {
                                        return 3879417181;
                                    }
                                }
                            }
                        } else {
                            if (i \u003c= 187) {
                                if (i \u003c= 185) {
                                    if (i == 184) {
                                        return 3890696723;
                                    } else {
                                        return 3901828632;
                                    }
                                } else {
                                    if (i == 186) {
                                        return 3912812484;
                                    } else {
                                        return 3923647863;
                                    }
                                }
                            } else {
                                if (i \u003c= 189) {
                                    if (i == 188) {
                                        return 3934334359;
                                    } else {
                                        return 3944871565;
                                    }
                                } else {
                                    if (i == 190) {
                                        return 3955259082;
                                    } else {
                                        return 3965496515;
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                if (i \u003c= 223) {
                    if (i \u003c= 207) {
                        if (i \u003c= 199) {
                            if (i \u003c= 195) {
                                if (i \u003c= 193) {
                                    if (i == 192) {
                                        return 3975583476;
                                    } else {
                                        return 3985519583;
                                    }
                                } else {
                                    if (i == 194) {
                                        return 3995304457;
                                    } else {
                                        return 4004937729;
                                    }
                                }
                            } else {
                                if (i \u003c= 197) {
                                    if (i == 196) {
                                        return 4014419032;
                                    } else {
                                        return 4023748007;
                                    }
                                } else {
                                    if (i == 198) {
                                        return 4032924300;
                                    } else {
                                        return 4041947562;
                                    }
                                }
                            }
                        } else {
                            if (i \u003c= 203) {
                                if (i \u003c= 201) {
                                    if (i == 200) {
                                        return 4050817451;
                                    } else {
                                        return 4059533630;
                                    }
                                } else {
                                    if (i == 202) {
                                        return 4068095769;
                                    } else {
                                        return 4076503544;
                                    }
                                }
                            } else {
                                if (i \u003c= 205) {
                                    if (i == 204) {
                                        return 4084756634;
                                    } else {
                                        return 4092854726;
                                    }
                                } else {
                                    if (i == 206) {
                                        return 4100797514;
                                    } else {
                                        return 4108584696;
                                    }
                                }
                            }
                        }
                    } else {
                        if (i \u003c= 215) {
                            if (i \u003c= 211) {
                                if (i \u003c= 209) {
                                    if (i == 208) {
                                        return 4116215977;
                                    } else {
                                        return 4123691067;
                                    }
                                } else {
                                    if (i == 210) {
                                        return 4131009681;
                                    } else {
                                        return 4138171544;
                                    }
                                }
                            } else {
                                if (i \u003c= 213) {
                                    if (i == 212) {
                                        return 4145176382;
                                    } else {
                                        return 4152023930;
                                    }
                                } else {
                                    if (i == 214) {
                                        return 4158713929;
                                    } else {
                                        return 4165246124;
                                    }
                                }
                            }
                        } else {
                            if (i \u003c= 219) {
                                if (i \u003c= 217) {
                                    if (i == 216) {
                                        return 4171620267;
                                    } else {
                                        return 4177836117;
                                    }
                                } else {
                                    if (i == 218) {
                                        return 4183893437;
                                    } else {
                                        return 4189791999;
                                    }
                                }
                            } else {
                                if (i \u003c= 221) {
                                    if (i == 220) {
                                        return 4195531577;
                                    } else {
                                        return 4201111955;
                                    }
                                } else {
                                    if (i == 222) {
                                        return 4206532921;
                                    } else {
                                        return 4211794268;
                                    }
                                }
                            }
                        }
                    }
                } else {
                    if (i \u003c= 239) {
                        if (i \u003c= 231) {
                            if (i \u003c= 227) {
                                if (i \u003c= 225) {
                                    if (i == 224) {
                                        return 4216895797;
                                    } else {
                                        return 4221837315;
                                    }
                                } else {
                                    if (i == 226) {
                                        return 4226618635;
                                    } else {
                                        return 4231239573;
                                    }
                                }
                            } else {
                                if (i \u003c= 229) {
                                    if (i == 228) {
                                        return 4235699957;
                                    } else {
                                        return 4239999615;
                                    }
                                } else {
                                    if (i == 230) {
                                        return 4244138385;
                                    } else {
                                        return 4248116110;
                                    }
                                }
                            }
                        } else {
                            if (i \u003c= 235) {
                                if (i \u003c= 233) {
                                    if (i == 232) {
                                        return 4251932639;
                                    } else {
                                        return 4255587827;
                                    }
                                } else {
                                    if (i == 234) {
                                        return 4259081536;
                                    } else {
                                        return 4262413632;
                                    }
                                }
                            } else {
                                if (i \u003c= 237) {
                                    if (i == 236) {
                                        return 4265583990;
                                    } else {
                                        return 4268592489;
                                    }
                                } else {
                                    if (i == 238) {
                                        return 4271439015;
                                    } else {
                                        return 4274123460;
                                    }
                                }
                            }
                        }
                    } else {
                        if (i \u003c= 247) {
                            if (i \u003c= 243) {
                                if (i \u003c= 241) {
                                    if (i == 240) {
                                        return 4276645722;
                                    } else {
                                        return 4279005706;
                                    }
                                } else {
                                    if (i == 242) {
                                        return 4281203321;
                                    } else {
                                        return 4283238485;
                                    }
                                }
                            } else {
                                if (i \u003c= 245) {
                                    if (i == 244) {
                                        return 4285111119;
                                    } else {
                                        return 4286821154;
                                    }
                                } else {
                                    if (i == 246) {
                                        return 4288368525;
                                    } else {
                                        return 4289753172;
                                    }
                                }
                            }
                        } else {
                            if (i \u003c= 251) {
                                if (i \u003c= 249) {
                                    if (i == 248) {
                                        return 4290975043;
                                    } else {
                                        return 4292034091;
                                    }
                                } else {
                                    if (i == 250) {
                                        return 4292930277;
                                    } else {
                                        return 4293663567;
                                    }
                                }
                            } else {
                                if (i \u003c= 253) {
                                    if (i == 252) {
                                        return 4294233932;
                                    } else {
                                        return 4294641351;
                                    }
                                } else {
                                    if (i == 254) {
                                        return 4294885809;
                                    } else {
                                        return 4294967296;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
"},"Trig256.sol":{"content":"// SPDX-License-Identifier: UNLICENSED
/* Copyright (c) 2021 Kohi Art Community, Inc. All rights reserved. */

pragma solidity ^0.8.0;

import \"Fix64V1.sol\";
import \"SinLut256.sol\";

/*
    Provides trigonometric functions in Q31.Q32 format.

    exp: Adapted from Petteri Aimonen\u0027s libfixmath

    See: https://github.com/PetteriAimonen/libfixmath
         https://github.com/PetteriAimonen/libfixmath/blob/master/LICENSE

    other functions: Adapted from André Slupik\u0027s FixedMath.NET
                     https://github.com/asik/FixedMath.Net/blob/master/LICENSE.txt
         
    THIRD PARTY NOTICES:
    ====================

    libfixmath is Copyright (c) 2011-2021 Flatmush \u003cFlatmush@gmail.com\u003e,
    Petteri Aimonen \u003cPetteri.Aimonen@gmail.com\u003e, \u0026 libfixmath AUTHORS

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the \"Software\"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.

    Copyright 2012 André Slupik

    Licensed under the Apache License, Version 2.0 (the \"License\");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an \"AS IS\" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

    This project uses code from the log2fix library, which is under the following license:           
    The MIT License (MIT)

    Copyright (c) 2015 Dan Moulding
    
    Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the \"Software\"), 
    to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
    and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
    
    The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
    THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
    IN THE SOFTWARE.
*/

library Trig256 {
    int64 private constant LARGE_PI = 7244019458077122842;
    int64 private constant LN2 = 0xB17217F7;
    int64 private constant LN_MAX = 0x157CD0E702;
    int64 private constant LN_MIN = -0x162E42FEFA;
    int64 private constant E = -0x2B7E15162;

    function sin(int64 x)
        internal
        pure
        returns (int64)
    {       
        (
            int64 clamped,
            bool flipHorizontal,
            bool flipVertical
        ) = clamp(x);

        int64 lutInterval = Fix64V1.div(((256 - 1) * Fix64V1.ONE), Fix64V1.PI_OVER_2);
        int rawIndex = Fix64V1.mul_256(clamped, lutInterval);
        int64 roundedIndex = int64(Fix64V1.round(rawIndex));
        int64 indexError = Fix64V1.sub(int64(rawIndex), roundedIndex);     

        roundedIndex = roundedIndex \u003e\u003e 32; /* FRACTIONAL_PLACES */

        int64 nearestValueIndex = flipHorizontal
            ? (256 - 1) - roundedIndex
            : roundedIndex;

        int64 nearestValue = SinLut256.sinlut(nearestValueIndex);

        int64 secondNearestValue = SinLut256.sinlut(
            flipHorizontal
                ? (256 - 1) -
                    roundedIndex -
                    Fix64V1.sign(indexError)
                : roundedIndex + Fix64V1.sign(indexError)
        );

        int64 delta = Fix64V1.mul(indexError, Fix64V1.abs(Fix64V1.sub(nearestValue, secondNearestValue)));
        int64 interpolatedValue = nearestValue + (flipHorizontal ? -delta : delta);
        int64 finalValue = flipVertical ? -interpolatedValue: interpolatedValue;
    
        return finalValue;
    }

    function cos(int64 x)
        internal
        pure
        returns (int64)
    {
        int64 xl = x;
        int64 angle;
        if(xl \u003e 0) {            
            angle = Fix64V1.add(xl, Fix64V1.sub(0 - Fix64V1.PI, Fix64V1.PI_OVER_2));            
        } else {            
            angle = Fix64V1.add(xl, Fix64V1.PI_OVER_2);
        }        
        return sin(angle);
    }

    function sqrt(int64 x)
        internal
        pure        
        returns (int64)
    {
        int64 xl = x;
        if (xl \u003c 0)
            revert(\"negative value passed to sqrt\");

        uint64 num = uint64(xl);
        uint64 result = uint64(0);
        uint64 bit = uint64(1) \u003c\u003c (64 - 2);

        while (bit \u003e num) bit \u003e\u003e= 2;
        for (uint8 i = 0; i \u003c 2; ++i)
        {
            while (bit != 0)
            {
                if (num \u003e= result + bit)
                {
                    num -= result + bit;
                    result = (result \u003e\u003e 1) + bit;
                }
                else
                {
                    result = result \u003e\u003e 1;
                }

                bit \u003e\u003e= 2;
            }

            if (i == 0)
            {
                if (num \u003e (uint64(1) \u003c\u003c (64 / 2)) - 1)
                {
                    num -= result;
                    num = (num \u003c\u003c (64 / 2)) - uint64(0x80000000);
                    result = (result \u003c\u003c (64 / 2)) + uint64(0x80000000);
                }
                else
                {
                    num \u003c\u003c= 64 / 2;
                    result \u003c\u003c= 64 / 2;
                }

                bit = uint64(1) \u003c\u003c (64 / 2 - 2);
            }
        }

        if (num \u003e result) ++result;
        return int64(result);
    }

     function log2_256(int x)
        internal
        pure        
        returns (int)
    {
        if (x \u003c= 0) {
            revert(\"negative value passed to log2_256\");
        }

        // This implementation is based on Clay. S. Turner\u0027s fast binary logarithm
        // algorithm (C. S. Turner,  \"A Fast Binary Logarithm Algorithm\", IEEE Signal
        //     Processing Mag., pp. 124,140, Sep. 2010.)

        int b = 1 \u003c\u003c 31; // FRACTIONAL_PLACES - 1
        int y = 0;

        int rawX = x;
        while (rawX \u003c Fix64V1.ONE) {
            rawX \u003c\u003c= 1;
            y -= Fix64V1.ONE;
        }

        while (rawX \u003e= Fix64V1.ONE \u003c\u003c 1) {
            rawX \u003e\u003e= 1;
            y += Fix64V1.ONE;
        }

        int z = rawX;

        for (uint8 i = 0; i \u003c 32 /* FRACTIONAL_PLACES */; i++) {
            z = Fix64V1.mul_256(z, z);
            if (z \u003e= Fix64V1.ONE \u003c\u003c 1) {
                z = z \u003e\u003e 1;
                y += b;
            }
            b \u003e\u003e= 1;
        }

        return y;
    }

    function log_256(int x)
        internal
        pure        
        returns (int)
    {
        return Fix64V1.mul_256(log2_256(x), LN2);
    }

    function log2(int64 x)
        internal
        pure        
        returns (int64)
    {
        if (x \u003c= 0) revert(\"non-positive value passed to log2\");

        // This implementation is based on Clay. S. Turner\u0027s fast binary logarithm
        // algorithm (C. S. Turner,  \"A Fast Binary Logarithm Algorithm\", IEEE Signal
        //     Processing Mag., pp. 124,140, Sep. 2010.)

        int64 b = 1 \u003c\u003c 31; // FRACTIONAL_PLACES - 1
        int64 y = 0;

        int64 rawX = x;
        while (rawX \u003c Fix64V1.ONE)
        {
            rawX \u003c\u003c= 1;
            y -= Fix64V1.ONE;
        }

        while (rawX \u003e= Fix64V1.ONE \u003c\u003c 1)
        {
            rawX \u003e\u003e= 1;
            y += Fix64V1.ONE;
        }

        int64 z = rawX;

        for (int32 i = 0; i \u003c Fix64V1.FRACTIONAL_PLACES; i++)
        {
            z = Fix64V1.mul(z, z);
            if (z \u003e= Fix64V1.ONE \u003c\u003c 1)
            {
                z = z \u003e\u003e 1;
                y += b;
            }

            b \u003e\u003e= 1;
        }

        return y;
    }

    function log(int64 x)
        internal
        pure        
        returns (int64)
    {
        return Fix64V1.mul(log2(x), LN2);
    }

    function exp(int64 x)
        internal
        pure        
        returns (int64)
    {
        if (x == 0) return Fix64V1.ONE;
        if (x == Fix64V1.ONE) return E;
        if (x \u003e= LN_MAX) return Fix64V1.MAX_VALUE;
        if (x \u003c= LN_MIN) return 0;

        /* The algorithm is based on the power series for exp(x):
         * http://en.wikipedia.org/wiki/Exponential_function#Formal_definition
         *
         * From term n, we get term n+1 by multiplying with x/n.
         * When the sum term drops to zero, we can stop summing.
         */

        // The power-series converges much faster on positive values
        // and exp(-x) = 1/exp(x).
        
        bool neg = (x \u003c 0);
        if (neg) x = -x;

        int64 result = Fix64V1.add(
            int64(x),
            Fix64V1.ONE
        );
        int64 term = x;

        for (uint32 i = 2; i \u003c 40; i++) {
            term = Fix64V1.mul(
                x,
                Fix64V1.div(term, int32(i) * Fix64V1.ONE)
            );
            result = Fix64V1.add(result, int64(term));
            if (term == 0) break;
        }

        if (neg) {
            result = Fix64V1.div(Fix64V1.ONE, result);
        }

        return result;
    }

    function clamp(int64 x)
        internal
        pure
        returns (
            int64,
            bool,
            bool
        )
    {
        int64 clamped2Pi = x;
        for (uint8 i = 0; i \u003c 29; ++i) {
            clamped2Pi %= LARGE_PI \u003e\u003e i;
        }
        if (x \u003c 0) {
            clamped2Pi += Fix64V1.TWO_PI;
        }

        bool flipVertical = clamped2Pi \u003e= Fix64V1.PI;
        int64 clampedPi = clamped2Pi;
        while (clampedPi \u003e= Fix64V1.PI) {
            clampedPi -= Fix64V1.PI;
        }

        bool flipHorizontal = clampedPi \u003e= Fix64V1.PI_OVER_2;

        int64 clampedPiOver2 = clampedPi;
        if (clampedPiOver2 \u003e= Fix64V1.PI_OVER_2)
            clampedPiOver2 -= Fix64V1.PI_OVER_2;

        return (clampedPiOver2, flipHorizontal, flipVertical);
    }
}
"},"TypesV1.sol":{"content":"// SPDX-License-Identifier: UNLICENSED
/* Copyright (c) 2021 Kohi Art Community, Inc. All rights reserved. */

pragma solidity ^0.8.0;

library TypesV1 {
    /**
     * @dev Represents a point in two-dimensional space.
     */
    struct Point2D {
        int256 x;
        int256 y;
    }

    /**
     * @dev Represents a chunked rendering region.
     */
    struct Chunk2D {
        uint16 index;
        uint16 width;
        uint16 height;
        uint16 chunkWidth;
        uint16 chunkHeight;
        uint32 startX;
        uint32 startY;
    }
}
