// SPDX-License-Identifier: MIT

pragma solidity \u003e=0.6.2 \u003c0.8.0;

import \"./IERC165.sol\";

/**
 * @dev Required interface of an ERC1155 compliant contract, as defined in the
 * https://eips.ethereum.org/EIPS/eip-1155[EIP].
 *
 * _Available since v3.1._
 */
interface IERC1155 is IERC165 {
    /**
     * @dev Emitted when `value` tokens of token type `id` are transferred from `from` to `to` by `operator`.
     */
    event TransferSingle(address indexed operator, address indexed from, address indexed to, uint256 id, uint256 value);

    /**
     * @dev Equivalent to multiple {TransferSingle} events, where `operator`, `from` and `to` are the same for all
     * transfers.
     */
    event TransferBatch(address indexed operator, address indexed from, address indexed to, uint256[] ids, uint256[] values);

    /**
     * @dev Emitted when `account` grants or revokes permission to `operator` to transfer their tokens, according to
     * `approved`.
     */
    event ApprovalForAll(address indexed account, address indexed operator, bool approved);

    /**
     * @dev Emitted when the URI for token type `id` changes to `value`, if it is a non-programmatic URI.
     *
     * If an {URI} event was emitted for `id`, the standard
     * https://eips.ethereum.org/EIPS/eip-1155#metadata-extensions[guarantees] that `value` will equal the value
     * returned by {IERC1155MetadataURI-uri}.
     */
    event URI(string value, uint256 indexed id);

    /**
     * @dev Returns the amount of tokens of token type `id` owned by `account`.
     *
     * Requirements:
     *
     * - `account` cannot be the zero address.
     */
    function balanceOf(address account, uint256 id) external view returns (uint256);

    /**
     * @dev xref:ROOT:erc1155.adoc#batch-operations[Batched] version of {balanceOf}.
     *
     * Requirements:
     *
     * - `accounts` and `ids` must have the same length.
     */
    function balanceOfBatch(address[] calldata accounts, uint256[] calldata ids) external view returns (uint256[] memory);

    /**
     * @dev Grants or revokes permission to `operator` to transfer the caller\u0027s tokens, according to `approved`,
     *
     * Emits an {ApprovalForAll} event.
     *
     * Requirements:
     *
     * - `operator` cannot be the caller.
     */
    function setApprovalForAll(address operator, bool approved) external;

    /**
     * @dev Returns true if `operator` is approved to transfer ``account``\u0027s tokens.
     *
     * See {setApprovalForAll}.
     */
    function isApprovedForAll(address account, address operator) external view returns (bool);

    /**
     * @dev Transfers `amount` tokens of token type `id` from `from` to `to`.
     *
     * Emits a {TransferSingle} event.
     *
     * Requirements:
     *
     * - `to` cannot be the zero address.
     * - If the caller is not `from`, it must be have been approved to spend ``from``\u0027s tokens via {setApprovalForAll}.
     * - `from` must have a balance of tokens of type `id` of at least `amount`.
     * - If `to` refers to a smart contract, it must implement {IERC1155Receiver-onERC1155Received} and return the
     * acceptance magic value.
     */
    function safeTransferFrom(address from, address to, uint256 id, uint256 amount, bytes calldata data) external;

    /**
     * @dev xref:ROOT:erc1155.adoc#batch-operations[Batched] version of {safeTransferFrom}.
     *
     * Emits a {TransferBatch} event.
     *
     * Requirements:
     *
     * - `ids` and `amounts` must have the same length.
     * - If `to` refers to a smart contract, it must implement {IERC1155Receiver-onERC1155BatchReceived} and return the
     * acceptance magic value.
     */
    function safeBatchTransferFrom(address from, address to, uint256[] calldata ids, uint256[] calldata amounts, bytes calldata data) external;
}
"},"IERC165.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity \u003e=0.6.0 \u003c0.8.0;

/**
 * @dev Interface of the ERC165 standard, as defined in the
 * https://eips.ethereum.org/EIPS/eip-165[EIP].
 *
 * Implementers can declare support of contract interfaces, which can then be
 * queried by others ({ERC165Checker}).
 *
 * For an implementation, see {ERC165}.
 */
interface IERC165 {
    /**
     * @dev Returns true if this contract implements the interface defined by
     * `interfaceId`. See the corresponding
     * https://eips.ethereum.org/EIPS/eip-165#how-interfaces-are-identified[EIP section]
     * to learn more about how these ids are created.
     *
     * This function call must use less than 30 000 gas.
     */
    function supportsInterface(bytes4 interfaceId) external view returns (bool);
}
"},"IERC20.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity \u003e=0.6.0 \u003c0.8.0;

/**
 * @dev Interface of the ERC20 standard as defined in the EIP.
 */
interface IERC20 {
    /**
     * @dev Returns the amount of tokens in existence.
     */
    function totalSupply() external view returns (uint256);

    /**
     * @dev Returns the amount of tokens owned by `account`.
     */
    function balanceOf(address account) external view returns (uint256);

    /**
     * @dev Moves `amount` tokens from the caller\u0027s account to `recipient`.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transfer(address recipient, uint256 amount) external returns (bool);

    /**
     * @dev Returns the remaining number of tokens that `spender` will be
     * allowed to spend on behalf of `owner` through {transferFrom}. This is
     * zero by default.
     *
     * This value changes when {approve} or {transferFrom} are called.
     */
    function allowance(address owner, address spender) external view returns (uint256);

    /**
     * @dev Sets `amount` as the allowance of `spender` over the caller\u0027s tokens.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * IMPORTANT: Beware that changing an allowance with this method brings the risk
     * that someone may use both the old and the new allowance by unfortunate
     * transaction ordering. One possible solution to mitigate this race
     * condition is to first reduce the spender\u0027s allowance to 0 and set the
     * desired value afterwards:
     * https://github.com/ethereum/EIPs/issues/20#issuecomment-263524729
     *
     * Emits an {Approval} event.
     */
    function approve(address spender, uint256 amount) external returns (bool);

    /**
     * @dev Moves `amount` tokens from `sender` to `recipient` using the
     * allowance mechanism. `amount` is then deducted from the caller\u0027s
     * allowance.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transferFrom(address sender, address recipient, uint256 amount) external returns (bool);

    /**
     * @dev Emitted when `value` tokens are moved from one account (`from`) to
     * another (`to`).
     *
     * Note that `value` may be zero.
     */
    event Transfer(address indexed from, address indexed to, uint256 value);

    /**
     * @dev Emitted when the allowance of a `spender` for an `owner` is set by
     * a call to {approve}. `value` is the new allowance.
     */
    event Approval(address indexed owner, address indexed spender, uint256 value);
}
"},"IERC721.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity \u003e=0.6.2 \u003c0.8.0;

import \"./IERC165.sol\";

/**
 * @dev Required interface of an ERC721 compliant contract.
 */
interface IERC721 is IERC165 {
    /**
     * @dev Emitted when `tokenId` token is transferred from `from` to `to`.
     */
    event Transfer(address indexed from, address indexed to, uint256 indexed tokenId);

    /**
     * @dev Emitted when `owner` enables `approved` to manage the `tokenId` token.
     */
    event Approval(address indexed owner, address indexed approved, uint256 indexed tokenId);

    /**
     * @dev Emitted when `owner` enables or disables (`approved`) `operator` to manage all of its assets.
     */
    event ApprovalForAll(address indexed owner, address indexed operator, bool approved);

    /**
     * @dev Returns the number of tokens in ``owner``\u0027s account.
     */
    function balanceOf(address owner) external view returns (uint256 balance);

    /**
     * @dev Returns the owner of the `tokenId` token.
     *
     * Requirements:
     *
     * - `tokenId` must exist.
     */
    function ownerOf(uint256 tokenId) external view returns (address owner);

    /**
     * @dev Safely transfers `tokenId` token from `from` to `to`, checking first that contract recipients
     * are aware of the ERC721 protocol to prevent tokens from being forever locked.
     *
     * Requirements:
     *
     * - `from` cannot be the zero address.
     * - `to` cannot be the zero address.
     * - `tokenId` token must exist and be owned by `from`.
     * - If the caller is not `from`, it must be have been allowed to move this token by either {approve} or {setApprovalForAll}.
     * - If `to` refers to a smart contract, it must implement {IERC721Receiver-onERC721Received}, which is called upon a safe transfer.
     *
     * Emits a {Transfer} event.
     */
    function safeTransferFrom(address from, address to, uint256 tokenId) external;

    /**
     * @dev Transfers `tokenId` token from `from` to `to`.
     *
     * WARNING: Usage of this method is discouraged, use {safeTransferFrom} whenever possible.
     *
     * Requirements:
     *
     * - `from` cannot be the zero address.
     * - `to` cannot be the zero address.
     * - `tokenId` token must be owned by `from`.
     * - If the caller is not `from`, it must be approved to move this token by either {approve} or {setApprovalForAll}.
     *
     * Emits a {Transfer} event.
     */
    function transferFrom(address from, address to, uint256 tokenId) external;

    /**
     * @dev Gives permission to `to` to transfer `tokenId` token to another account.
     * The approval is cleared when the token is transferred.
     *
     * Only a single account can be approved at a time, so approving the zero address clears previous approvals.
     *
     * Requirements:
     *
     * - The caller must own the token or be an approved operator.
     * - `tokenId` must exist.
     *
     * Emits an {Approval} event.
     */
    function approve(address to, uint256 tokenId) external;

    /**
     * @dev Returns the account approved for `tokenId` token.
     *
     * Requirements:
     *
     * - `tokenId` must exist.
     */
    function getApproved(uint256 tokenId) external view returns (address operator);

    /**
     * @dev Approve or remove `operator` as an operator for the caller.
     * Operators can call {transferFrom} or {safeTransferFrom} for any token owned by the caller.
     *
     * Requirements:
     *
     * - The `operator` cannot be the caller.
     *
     * Emits an {ApprovalForAll} event.
     */
    function setApprovalForAll(address operator, bool _approved) external;

    /**
     * @dev Returns if the `operator` is allowed to manage all of the assets of `owner`.
     *
     * See {setApprovalForAll}
     */
    function isApprovedForAll(address owner, address operator) external view returns (bool);

    /**
      * @dev Safely transfers `tokenId` token from `from` to `to`.
      *
      * Requirements:
      *
     * - `from` cannot be the zero address.
     * - `to` cannot be the zero address.
      * - `tokenId` token must exist and be owned by `from`.
      * - If the caller is not `from`, it must be approved to move this token by either {approve} or {setApprovalForAll}.
      * - If `to` refers to a smart contract, it must implement {IERC721Receiver-onERC721Received}, which is called upon a safe transfer.
      *
      * Emits a {Transfer} event.
      */
    function safeTransferFrom(address from, address to, uint256 tokenId, bytes calldata data) external;
}
"},"PortionExchangeV2.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity 0.7.4;

import \"./IERC721.sol\";
import \"./IERC20.sol\";
import \u0027./IERC1155.sol\u0027;

contract PortionExchangeV2 {
\tstruct ERC1155Offer {
\t\tuint tokenId;
\t\tuint quantity;
\t\tuint price;
\t\taddress seller;
\t}

\tevent TokenPriceListed (uint indexed _tokenId, address indexed _owner, uint _price);
\tevent TokenPriceDeleted (uint indexed _tokenId);
\tevent TokenSold (uint indexed _tokenId, uint _price, bool _soldForPRT);
\tevent TokenOwned (uint indexed _tokenId, address indexed _previousOwner, address indexed _newOwner);
\tevent Token1155OfferListed (uint indexed _tokenId, uint indexed _offerId, address indexed _owner, uint _quantity, uint _price);
\tevent Token1155OfferDeleted (uint indexed _tokenId, uint indexed _offerId);
\tevent Token1155Sold(uint indexed _tokenId, uint indexed _offerId, uint _quantity, uint _price, bool _soldForPRT);
\tevent Token1155Owned (uint indexed _tokenId, address indexed _previousOwner, address indexed _newOwner, uint _quantity);

\taddress public signer;
\taddress owner;

\tbytes32 public name = \"PortionExchangeV2\";

\tuint public offerIdCounter;

\tIERC20 public portionTokenContract;
\tIERC721 public artTokenContract;
\tIERC1155 public artToken1155Contract;

\tmapping(address =\u003e uint) public nonces;
\tmapping(uint =\u003e uint) public ERC721Prices;
\tmapping(uint =\u003e ERC1155Offer) public ERC1155Offers;
\tmapping(address =\u003e mapping(uint =\u003e uint)) public tokensListed;

\tconstructor (
\t\taddress _signer,
\t\taddress _artTokenAddress,
\t\taddress _artToken1155Address,
\t\taddress _portionTokenAddress
\t)
\t{
\t\trequire (_signer != address(0));
\t\trequire (_artTokenAddress != address(0));
\t\trequire (_artToken1155Address != address(0));
\t\trequire (_portionTokenAddress != address(0));

\t\towner = msg.sender;
\t\tsigner = _signer;
\t\tartTokenContract = IERC721(_artTokenAddress);
\t\tartToken1155Contract = IERC1155(_artToken1155Address);
\t\tportionTokenContract = IERC20(_portionTokenAddress);
\t}

\tfunction listToken(
\t\tuint _tokenId,
\t\tuint _price
\t)
\texternal
\t{
\t\trequire(_price \u003e 0);
\t\trequire(artTokenContract.ownerOf(_tokenId) == msg.sender);
\t\tERC721Prices[_tokenId] = _price;
\t\temit TokenPriceListed(_tokenId, msg.sender, _price);
\t}

\tfunction listToken1155(
\t\tuint _tokenId,
\t\tuint _quantity,
\t\tuint _price
\t)
\texternal
\t{
\t\trequire(_price \u003e 0);
\t\trequire(artToken1155Contract.balanceOf(msg.sender, _tokenId) \u003e= tokensListed[msg.sender][_tokenId] + _quantity);

\t\tuint offerId = offerIdCounter++;
\t\tERC1155Offers[offerId] = ERC1155Offer({
\t\t\ttokenId: _tokenId,
\t\t\tquantity: _quantity,
\t\t\tprice: _price,
\t\t\tseller: msg.sender
\t\t});

\t\ttokensListed[msg.sender][_tokenId] += _quantity;
\t\temit Token1155OfferListed(_tokenId, offerId, msg.sender, _quantity, _price);
\t}

\tfunction removeListToken(
\t\tuint _tokenId
\t)
\texternal
\t{
\t\trequire(artTokenContract.ownerOf(_tokenId) == msg.sender);
\t\tdeleteTokenPrice(_tokenId);
\t}

\tfunction removeListToken1155(
\t\tuint _offerId
\t)
\texternal
\t{
\t\trequire(ERC1155Offers[_offerId].seller == msg.sender);
\t\tdeleteToken1155Offer(_offerId);
\t}

\tfunction deleteTokenPrice(
\t\tuint _tokenId
\t)
\tinternal
\t{
\t\tdelete ERC721Prices[_tokenId];
\t\temit TokenPriceDeleted(_tokenId);
\t}

\tfunction deleteToken1155Offer(
\t\tuint _offerId
\t)
\tinternal
\t{
\t\tERC1155Offer memory offer = ERC1155Offers[_offerId];
\t\ttokensListed[offer.seller][offer.tokenId] -= offer.quantity;

\t\tdelete ERC1155Offers[_offerId];
\t\temit Token1155OfferDeleted(offer.tokenId, _offerId);
\t}

\tfunction buyToken(
\t\tuint _tokenId
\t)
\texternal
\tpayable
\t{
\t\trequire(ERC721Prices[_tokenId] \u003e 0, \"token is not for sale\");
\t\trequire(ERC721Prices[_tokenId] \u003c= msg.value);

\t\taddress tokenOwner = artTokenContract.ownerOf(_tokenId);

\t\taddress payable payableTokenOwner = payable(tokenOwner);
\t\t(bool sent, ) = payableTokenOwner.call{value: msg.value}(\"\");
\t\trequire(sent);

\t\tartTokenContract.safeTransferFrom(tokenOwner, msg.sender, _tokenId);

\t\temit TokenSold(_tokenId, msg.value, false);
\t\temit TokenOwned(_tokenId, tokenOwner, msg.sender);

\t\tdeleteTokenPrice(_tokenId);
\t}

\tfunction buyToken1155(
\t\tuint _offerId,
\t\tuint _quantity
\t)
\texternal
\tpayable
\t{
\t\tERC1155Offer memory offer = ERC1155Offers[_offerId];

\t\trequire(offer.price \u003e 0, \"offer does not exist\");
\t\trequire(offer.quantity \u003e= _quantity);
\t\trequire(offer.price * _quantity \u003c= msg.value);

\t\taddress payable payableSeller = payable(offer.seller);
\t\t(bool sent, ) = payableSeller.call{value: msg.value}(\"\");
\t\trequire(sent);

\t\tartToken1155Contract.safeTransferFrom(offer.seller, msg.sender, offer.tokenId, _quantity, \"\");

\t\temit Token1155Sold(offer.tokenId, _offerId, _quantity, offer.price, false);
\t\temit Token1155Owned(offer.tokenId, offer.seller, msg.sender, _quantity);

\t\tif (offer.quantity == _quantity) {
\t\t\tdeleteToken1155Offer(_offerId);
\t\t} else {
\t\t\tERC1155Offers[_offerId].quantity -= _quantity;
\t\t}
\t}

\tfunction buyTokenForPRT(
\t\tuint _tokenId,
\t\tuint _amountOfPRT,
\t\tuint _nonce,
\t\tbytes calldata _signature
\t)
\texternal
\t{
\t\trequire(ERC721Prices[_tokenId] \u003e 0, \"token is not for sale\");

\t\trequire(nonces[msg.sender] \u003c _nonce, \"invalid nonce\");
\t\tnonces[msg.sender] = _nonce;

\t\tbytes32 hash = keccak256(abi.encodePacked(_tokenId, _amountOfPRT, _nonce));
\t\tbytes32 ethSignedMessageHash = keccak256(abi.encodePacked(\"\\x19Ethereum Signed Message:\
32\", hash));
\t\trequire(recoverSignerAddress(ethSignedMessageHash, _signature) == signer, \"invalid secret signer\");

\t\taddress tokenOwner = artTokenContract.ownerOf(_tokenId);

\t\tbool sent = portionTokenContract.transferFrom(msg.sender, tokenOwner, _amountOfPRT);
\t\trequire(sent);

\t\tartTokenContract.safeTransferFrom(tokenOwner, msg.sender, _tokenId);

\t\temit TokenSold(_tokenId, _amountOfPRT, true);
\t\temit TokenOwned(_tokenId, tokenOwner, msg.sender);

\t\tdeleteTokenPrice(_tokenId);
\t}

\tfunction buyToken1155ForPRT(
\t\tuint _offerId,
\t\tuint _quantity,
\t\tuint _amountOfPRT,
\t\tuint _nonce,
\t\tbytes calldata _signature
\t)
\texternal
\t{
\t\tERC1155Offer memory offer = ERC1155Offers[_offerId];

\t\trequire(offer.price \u003e 0, \"offer does not exist\");
\t\trequire(offer.quantity \u003e= _quantity);

\t\trequire(nonces[msg.sender] \u003c _nonce, \"invalid nonce\");
\t\tnonces[msg.sender] = _nonce;

\t\tbytes32 hash = keccak256(abi.encodePacked(_offerId, _quantity, _amountOfPRT, _nonce));
\t\tbytes32 ethSignedMessageHash = keccak256(abi.encodePacked(\"\\x19Ethereum Signed Message:\
32\", hash));
\t\trequire(recoverSignerAddress(ethSignedMessageHash, _signature) == signer, \"invalid secret signer\");

\t\tportionTokenContract.transferFrom(msg.sender, offer.seller, _amountOfPRT * _quantity);
\t\tartToken1155Contract.safeTransferFrom(offer.seller, msg.sender, offer.tokenId, _quantity, \"\");

\t\temit Token1155Sold(offer.tokenId, _offerId, _quantity, _amountOfPRT, true);
\t\temit Token1155Owned(offer.tokenId, offer.seller, msg.sender, _quantity);

\t\tif (offer.quantity == _quantity) {
\t\t\tdeleteToken1155Offer(_offerId);
\t\t} else {
\t\t\tERC1155Offers[_offerId].quantity -= _quantity;
\t\t}
\t}

\tfunction setSigner(
\t\taddress _newSigner
\t)
\texternal
\t{
\t\trequire(msg.sender == owner);
\t\tsigner = _newSigner;
\t}

\tfunction recoverSignerAddress(
\t\tbytes32 _hash,
\t\tbytes memory _signature
\t)
\tinternal
\tpure
\treturns (address)
\t{
\t\trequire(_signature.length == 65, \"invalid signature length\");

\t\tbytes32 r;
\t\tbytes32 s;
\t\tuint8 v;

\t\tassembly {
\t\t\tr := mload(add(_signature, 32))
\t\t\ts := mload(add(_signature, 64))
\t\t\tv := and(mload(add(_signature, 65)), 255)
\t\t}

\t\tif (v \u003c 27) {
\t\t\tv += 27;
\t\t}

\t\tif (v != 27 \u0026\u0026 v != 28) {
\t\t\treturn address(0);
\t\t}

\t\treturn ecrecover(_hash, v, r, s);
\t}
}

