// SPDX-License-Identifier: MIT

pragma solidity \u003e=0.6.2 \u003c0.8.0;

/**
 * @dev Collection of functions related to the address type
 */
library Address {
    /**
     * @dev Returns true if `account` is a contract.
     *
     * [IMPORTANT]
     * ====
     * It is unsafe to assume that an address for which this function returns
     * false is an externally-owned account (EOA) and not a contract.
     *
     * Among others, `isContract` will return false for the following
     * types of addresses:
     *
     *  - an externally-owned account
     *  - a contract in construction
     *  - an address where a contract will be created
     *  - an address where a contract lived, but was destroyed
     * ====
     */
    function isContract(address account) internal view returns (bool) {
        // This method relies on extcodesize, which returns 0 for contracts in
        // construction, since the code is only stored at the end of the
        // constructor execution.

        uint256 size;
        // solhint-disable-next-line no-inline-assembly
        assembly { size := extcodesize(account) }
        return size \u003e 0;
    }

    /**
     * @dev Replacement for Solidity\u0027s `transfer`: sends `amount` wei to
     * `recipient`, forwarding all available gas and reverting on errors.
     *
     * https://eips.ethereum.org/EIPS/eip-1884[EIP1884] increases the gas cost
     * of certain opcodes, possibly making contracts go over the 2300 gas limit
     * imposed by `transfer`, making them unable to receive funds via
     * `transfer`. {sendValue} removes this limitation.
     *
     * https://diligence.consensys.net/posts/2019/09/stop-using-soliditys-transfer-now/[Learn more].
     *
     * IMPORTANT: because control is transferred to `recipient`, care must be
     * taken to not create reentrancy vulnerabilities. Consider using
     * {ReentrancyGuard} or the
     * https://solidity.readthedocs.io/en/v0.5.11/security-considerations.html#use-the-checks-effects-interactions-pattern[checks-effects-interactions pattern].
     */
    function sendValue(address payable recipient, uint256 amount) internal {
        require(address(this).balance \u003e= amount, \"Address: insufficient balance\");

        // solhint-disable-next-line avoid-low-level-calls, avoid-call-value
        (bool success, ) = recipient.call{ value: amount }(\"\");
        require(success, \"Address: unable to send value, recipient may have reverted\");
    }

    /**
     * @dev Performs a Solidity function call using a low level `call`. A
     * plain`call` is an unsafe replacement for a function call: use this
     * function instead.
     *
     * If `target` reverts with a revert reason, it is bubbled up by this
     * function (like regular Solidity function calls).
     *
     * Returns the raw returned data. To convert to the expected return value,
     * use https://solidity.readthedocs.io/en/latest/units-and-global-variables.html?highlight=abi.decode#abi-encoding-and-decoding-functions[`abi.decode`].
     *
     * Requirements:
     *
     * - `target` must be a contract.
     * - calling `target` with `data` must not revert.
     *
     * _Available since v3.1._
     */
    function functionCall(address target, bytes memory data) internal returns (bytes memory) {
      return functionCall(target, data, \"Address: low-level call failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`], but with
     * `errorMessage` as a fallback revert reason when `target` reverts.
     *
     * _Available since v3.1._
     */
    function functionCall(address target, bytes memory data, string memory errorMessage) internal returns (bytes memory) {
        return functionCallWithValue(target, data, 0, errorMessage);
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`],
     * but also transferring `value` wei to `target`.
     *
     * Requirements:
     *
     * - the calling contract must have an ETH balance of at least `value`.
     * - the called Solidity function must be `payable`.
     *
     * _Available since v3.1._
     */
    function functionCallWithValue(address target, bytes memory data, uint256 value) internal returns (bytes memory) {
        return functionCallWithValue(target, data, value, \"Address: low-level call with value failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCallWithValue-address-bytes-uint256-}[`functionCallWithValue`], but
     * with `errorMessage` as a fallback revert reason when `target` reverts.
     *
     * _Available since v3.1._
     */
    function functionCallWithValue(address target, bytes memory data, uint256 value, string memory errorMessage) internal returns (bytes memory) {
        require(address(this).balance \u003e= value, \"Address: insufficient balance for call\");
        require(isContract(target), \"Address: call to non-contract\");

        // solhint-disable-next-line avoid-low-level-calls
        (bool success, bytes memory returndata) = target.call{ value: value }(data);
        return _verifyCallResult(success, returndata, errorMessage);
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`],
     * but performing a static call.
     *
     * _Available since v3.3._
     */
    function functionStaticCall(address target, bytes memory data) internal view returns (bytes memory) {
        return functionStaticCall(target, data, \"Address: low-level static call failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-string-}[`functionCall`],
     * but performing a static call.
     *
     * _Available since v3.3._
     */
    function functionStaticCall(address target, bytes memory data, string memory errorMessage) internal view returns (bytes memory) {
        require(isContract(target), \"Address: static call to non-contract\");

        // solhint-disable-next-line avoid-low-level-calls
        (bool success, bytes memory returndata) = target.staticcall(data);
        return _verifyCallResult(success, returndata, errorMessage);
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`],
     * but performing a delegate call.
     *
     * _Available since v3.4._
     */
    function functionDelegateCall(address target, bytes memory data) internal returns (bytes memory) {
        return functionDelegateCall(target, data, \"Address: low-level delegate call failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-string-}[`functionCall`],
     * but performing a delegate call.
     *
     * _Available since v3.4._
     */
    function functionDelegateCall(address target, bytes memory data, string memory errorMessage) internal returns (bytes memory) {
        require(isContract(target), \"Address: delegate call to non-contract\");

        // solhint-disable-next-line avoid-low-level-calls
        (bool success, bytes memory returndata) = target.delegatecall(data);
        return _verifyCallResult(success, returndata, errorMessage);
    }

    function _verifyCallResult(bool success, bytes memory returndata, string memory errorMessage) private pure returns(bytes memory) {
        if (success) {
            return returndata;
        } else {
            // Look for revert reason and bubble it up if present
            if (returndata.length \u003e 0) {
                // The easiest way to bubble the revert reason is using memory via assembly

                // solhint-disable-next-line no-inline-assembly
                assembly {
                    let returndata_size := mload(returndata)
                    revert(add(32, returndata), returndata_size)
                }
            } else {
                revert(errorMessage);
            }
        }
    }
}
"},"Context.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity \u003e=0.6.0 \u003c0.8.0;

/*
 * @dev Provides information about the current execution context, including the
 * sender of the transaction and its data. While these are generally available
 * via msg.sender and msg.data, they should not be accessed in such a direct
 * manner, since when dealing with GSN meta-transactions the account sending and
 * paying for execution may not be the actual sender (as far as an application
 * is concerned).
 *
 * This contract is only required for intermediate, library-like contracts.
 */
abstract contract Context {
    function _msgSender() internal view virtual returns (address payable) {
        return msg.sender;
    }

    function _msgData() internal view virtual returns (bytes memory) {
        this; // silence state mutability warning without generating bytecode - see https://github.com/ethereum/solidity/issues/2691
        return msg.data;
    }
}
"},"ERC20.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity \u003e=0.6.0 \u003c0.8.0;

import \"./Context.sol\";
import \"./IERC20.sol\";
import \"./SafeMath.sol\";

/**
 * @dev Implementation of the {IERC20} interface.
 *
 * This implementation is agnostic to the way tokens are created. This means
 * that a supply mechanism has to be added in a derived contract using {_mint}.
 * For a generic mechanism see {ERC20PresetMinterPauser}.
 *
 * TIP: For a detailed writeup see our guide
 * https://forum.zeppelin.solutions/t/how-to-implement-erc20-supply-mechanisms/226[How
 * to implement supply mechanisms].
 *
 * We have followed general OpenZeppelin guidelines: functions revert instead
 * of returning `false` on failure. This behavior is nonetheless conventional
 * and does not conflict with the expectations of ERC20 applications.
 *
 * Additionally, an {Approval} event is emitted on calls to {transferFrom}.
 * This allows applications to reconstruct the allowance for all accounts just
 * by listening to said events. Other implementations of the EIP may not emit
 * these events, as it isn\u0027t required by the specification.
 *
 * Finally, the non-standard {decreaseAllowance} and {increaseAllowance}
 * functions have been added to mitigate the well-known issues around setting
 * allowances. See {IERC20-approve}.
 */
contract ERC20 is Context, IERC20 {
    using SafeMath for uint256;

    mapping (address =\u003e uint256) private _balances;

    mapping (address =\u003e mapping (address =\u003e uint256)) private _allowances;

    uint256 private _totalSupply;

    string private _name;
    string private _symbol;
    uint8 private _decimals;

    /**
     * @dev Sets the values for {name} and {symbol}, initializes {decimals} with
     * a default value of 18.
     *
     * To select a different value for {decimals}, use {_setupDecimals}.
     *
     * All three of these values are immutable: they can only be set once during
     * construction.
     */
    constructor (string memory name_, string memory symbol_) public {
        _name = name_;
        _symbol = symbol_;
        _decimals = 18;
    }

    /**
     * @dev Returns the name of the token.
     */
    function name() public view virtual returns (string memory) {
        return _name;
    }

    /**
     * @dev Returns the symbol of the token, usually a shorter version of the
     * name.
     */
    function symbol() public view virtual returns (string memory) {
        return _symbol;
    }

    /**
     * @dev Returns the number of decimals used to get its user representation.
     * For example, if `decimals` equals `2`, a balance of `505` tokens should
     * be displayed to a user as `5,05` (`505 / 10 ** 2`).
     *
     * Tokens usually opt for a value of 18, imitating the relationship between
     * Ether and Wei. This is the value {ERC20} uses, unless {_setupDecimals} is
     * called.
     *
     * NOTE: This information is only used for _display_ purposes: it in
     * no way affects any of the arithmetic of the contract, including
     * {IERC20-balanceOf} and {IERC20-transfer}.
     */
    function decimals() public view virtual returns (uint8) {
        return _decimals;
    }

    /**
     * @dev See {IERC20-totalSupply}.
     */
    function totalSupply() public view virtual override returns (uint256) {
        return _totalSupply;
    }

    /**
     * @dev See {IERC20-balanceOf}.
     */
    function balanceOf(address account) public view virtual override returns (uint256) {
        return _balances[account];
    }

    /**
     * @dev See {IERC20-transfer}.
     *
     * Requirements:
     *
     * - `recipient` cannot be the zero address.
     * - the caller must have a balance of at least `amount`.
     */
    function transfer(address recipient, uint256 amount) public virtual override returns (bool) {
        _transfer(_msgSender(), recipient, amount);
        return true;
    }

    /**
     * @dev See {IERC20-allowance}.
     */
    function allowance(address owner, address spender) public view virtual override returns (uint256) {
        return _allowances[owner][spender];
    }

    /**
     * @dev See {IERC20-approve}.
     *
     * Requirements:
     *
     * - `spender` cannot be the zero address.
     */
    function approve(address spender, uint256 amount) public virtual override returns (bool) {
        _approve(_msgSender(), spender, amount);
        return true;
    }

    /**
     * @dev See {IERC20-transferFrom}.
     *
     * Emits an {Approval} event indicating the updated allowance. This is not
     * required by the EIP. See the note at the beginning of {ERC20}.
     *
     * Requirements:
     *
     * - `sender` and `recipient` cannot be the zero address.
     * - `sender` must have a balance of at least `amount`.
     * - the caller must have allowance for ``sender``\u0027s tokens of at least
     * `amount`.
     */
    function transferFrom(address sender, address recipient, uint256 amount) public virtual override returns (bool) {
        _transfer(sender, recipient, amount);
        _approve(sender, _msgSender(), _allowances[sender][_msgSender()].sub(amount, \"ERC20: transfer amount exceeds allowance\"));
        return true;
    }

    /**
     * @dev Atomically increases the allowance granted to `spender` by the caller.
     *
     * This is an alternative to {approve} that can be used as a mitigation for
     * problems described in {IERC20-approve}.
     *
     * Emits an {Approval} event indicating the updated allowance.
     *
     * Requirements:
     *
     * - `spender` cannot be the zero address.
     */
    function increaseAllowance(address spender, uint256 addedValue) public virtual returns (bool) {
        _approve(_msgSender(), spender, _allowances[_msgSender()][spender].add(addedValue));
        return true;
    }

    /**
     * @dev Atomically decreases the allowance granted to `spender` by the caller.
     *
     * This is an alternative to {approve} that can be used as a mitigation for
     * problems described in {IERC20-approve}.
     *
     * Emits an {Approval} event indicating the updated allowance.
     *
     * Requirements:
     *
     * - `spender` cannot be the zero address.
     * - `spender` must have allowance for the caller of at least
     * `subtractedValue`.
     */
    function decreaseAllowance(address spender, uint256 subtractedValue) public virtual returns (bool) {
        _approve(_msgSender(), spender, _allowances[_msgSender()][spender].sub(subtractedValue, \"ERC20: decreased allowance below zero\"));
        return true;
    }

    /**
     * @dev Moves tokens `amount` from `sender` to `recipient`.
     *
     * This is internal function is equivalent to {transfer}, and can be used to
     * e.g. implement automatic token fees, slashing mechanisms, etc.
     *
     * Emits a {Transfer} event.
     *
     * Requirements:
     *
     * - `sender` cannot be the zero address.
     * - `recipient` cannot be the zero address.
     * - `sender` must have a balance of at least `amount`.
     */
    function _transfer(address sender, address recipient, uint256 amount) internal virtual {
        require(sender != address(0), \"ERC20: transfer from the zero address\");
        require(recipient != address(0), \"ERC20: transfer to the zero address\");

        _beforeTokenTransfer(sender, recipient, amount);

        _balances[sender] = _balances[sender].sub(amount, \"ERC20: transfer amount exceeds balance\");
        _balances[recipient] = _balances[recipient].add(amount);
        emit Transfer(sender, recipient, amount);
    }

    /** @dev Creates `amount` tokens and assigns them to `account`, increasing
     * the total supply.
     *
     * Emits a {Transfer} event with `from` set to the zero address.
     *
     * Requirements:
     *
     * - `to` cannot be the zero address.
     */
    function _mint(address account, uint256 amount) internal virtual {
        require(account != address(0), \"ERC20: mint to the zero address\");

        _beforeTokenTransfer(address(0), account, amount);

        _totalSupply = _totalSupply.add(amount);
        _balances[account] = _balances[account].add(amount);
        emit Transfer(address(0), account, amount);
    }

    /**
     * @dev Destroys `amount` tokens from `account`, reducing the
     * total supply.
     *
     * Emits a {Transfer} event with `to` set to the zero address.
     *
     * Requirements:
     *
     * - `account` cannot be the zero address.
     * - `account` must have at least `amount` tokens.
     */
    function _burn(address account, uint256 amount) internal virtual {
        require(account != address(0), \"ERC20: burn from the zero address\");

        _beforeTokenTransfer(account, address(0), amount);

        _balances[account] = _balances[account].sub(amount, \"ERC20: burn amount exceeds balance\");
        _totalSupply = _totalSupply.sub(amount);
        emit Transfer(account, address(0), amount);
    }

    /**
     * @dev Sets `amount` as the allowance of `spender` over the `owner` s tokens.
     *
     * This internal function is equivalent to `approve`, and can be used to
     * e.g. set automatic allowances for certain subsystems, etc.
     *
     * Emits an {Approval} event.
     *
     * Requirements:
     *
     * - `owner` cannot be the zero address.
     * - `spender` cannot be the zero address.
     */
    function _approve(address owner, address spender, uint256 amount) internal virtual {
        require(owner != address(0), \"ERC20: approve from the zero address\");
        require(spender != address(0), \"ERC20: approve to the zero address\");

        _allowances[owner][spender] = amount;
        emit Approval(owner, spender, amount);
    }

    /**
     * @dev Sets {decimals} to a value other than the default one of 18.
     *
     * WARNING: This function should only be called from the constructor. Most
     * applications that interact with token contracts will not expect
     * {decimals} to ever change, and may work incorrectly if it does.
     */
    function _setupDecimals(uint8 decimals_) internal virtual {
        _decimals = decimals_;
    }

    /**
     * @dev Hook that is called before any transfer of tokens. This includes
     * minting and burning.
     *
     * Calling conditions:
     *
     * - when `from` and `to` are both non-zero, `amount` of ``from``\u0027s tokens
     * will be to transferred to `to`.
     * - when `from` is zero, `amount` tokens will be minted for `to`.
     * - when `to` is zero, `amount` of ``from``\u0027s tokens will be burned.
     * - `from` and `to` are never both zero.
     *
     * To learn more about hooks, head to xref:ROOT:extending-contracts.adoc#using-hooks[Using Hooks].
     */
    function _beforeTokenTransfer(address from, address to, uint256 amount) internal virtual { }
}
"},"ERC20Burnable.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity \u003e=0.6.0 \u003c0.8.0;

import \"./Context.sol\";
import \"./ERC20.sol\";

/**
 * @dev Extension of {ERC20} that allows token holders to destroy both their own
 * tokens and those that they have an allowance for, in a way that can be
 * recognized off-chain (via event analysis).
 */
abstract contract ERC20Burnable is Context, ERC20 {
    using SafeMath for uint256;

    /**
     * @dev Destroys `amount` tokens from the caller.
     *
     * See {ERC20-_burn}.
     */
    function burn(uint256 amount) public virtual {
        _burn(_msgSender(), amount);
    }

    /**
     * @dev Destroys `amount` tokens from `account`, deducting from the caller\u0027s
     * allowance.
     *
     * See {ERC20-_burn} and {ERC20-allowance}.
     *
     * Requirements:
     *
     * - the caller must have allowance for ``accounts``\u0027s tokens of at least
     * `amount`.
     */
    function burnFrom(address account, uint256 amount) public virtual {
        uint256 decreasedAllowance = allowance(account, _msgSender()).sub(amount, \"ERC20: burn amount exceeds allowance\");

        _approve(account, _msgSender(), decreasedAllowance);
        _burn(account, amount);
    }
}
"},"ERC20Mintable.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.7.3;

import \"./Ownable.sol\";
import \"./ERC20.sol\";
import \"./ERC20Burnable.sol\";

contract ERC20Mintable is ERC20, ERC20Burnable, Ownable {
    constructor(
        address _owner,
        string memory _name,
        string memory _symbol
    ) ERC20(_name, _symbol) {
        transferOwnership(_owner);
    }

    function mint(address _recipient, uint256 _amount) public onlyOwner {
        _mint(_recipient, _amount);
    }
}
"},"IERC20.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity \u003e=0.6.0 \u003c0.8.0;

/**
 * @dev Interface of the ERC20 standard as defined in the EIP.
 */
interface IERC20 {
    /**
     * @dev Returns the amount of tokens in existence.
     */
    function totalSupply() external view returns (uint256);

    /**
     * @dev Returns the amount of tokens owned by `account`.
     */
    function balanceOf(address account) external view returns (uint256);

    /**
     * @dev Moves `amount` tokens from the caller\u0027s account to `recipient`.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transfer(address recipient, uint256 amount) external returns (bool);

    /**
     * @dev Returns the remaining number of tokens that `spender` will be
     * allowed to spend on behalf of `owner` through {transferFrom}. This is
     * zero by default.
     *
     * This value changes when {approve} or {transferFrom} are called.
     */
    function allowance(address owner, address spender) external view returns (uint256);

    /**
     * @dev Sets `amount` as the allowance of `spender` over the caller\u0027s tokens.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * IMPORTANT: Beware that changing an allowance with this method brings the risk
     * that someone may use both the old and the new allowance by unfortunate
     * transaction ordering. One possible solution to mitigate this race
     * condition is to first reduce the spender\u0027s allowance to 0 and set the
     * desired value afterwards:
     * https://github.com/ethereum/EIPs/issues/20#issuecomment-263524729
     *
     * Emits an {Approval} event.
     */
    function approve(address spender, uint256 amount) external returns (bool);

    /**
     * @dev Moves `amount` tokens from `sender` to `recipient` using the
     * allowance mechanism. `amount` is then deducted from the caller\u0027s
     * allowance.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transferFrom(address sender, address recipient, uint256 amount) external returns (bool);

    /**
     * @dev Emitted when `value` tokens are moved from one account (`from`) to
     * another (`to`).
     *
     * Note that `value` may be zero.
     */
    event Transfer(address indexed from, address indexed to, uint256 value);

    /**
     * @dev Emitted when the allowance of a `spender` for an `owner` is set by
     * a call to {approve}. `value` is the new allowance.
     */
    event Approval(address indexed owner, address indexed spender, uint256 value);
}
"},"IMigrator.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.7.3;

interface IMigrator {
    // Perform LP token migration from legacy LP token to new LP token.
    // Take the current LP token address and return the new LP token address.
    // Migrator should have full access to the caller\u0027s LP token.
    // Return the new LP token address.
    //
    // XXX Migrator must have allowance access to old LP tokens.
    // New LP tokens must mint EXACTLY the same amount of LP tokens or
    // else something bad will happen!!!
    function migrate(address token) external returns (address);
}
"},"MasterChef.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.7.3;

import \"./IMigrator.sol\";

import \"./ERC20Mintable.sol\";

import \"./Ownable.sol\";
import \"./ERC20.sol\";
import \"./SafeERC20.sol\";
import \"./SafeMath.sol\";

contract MasterChef is Ownable {
    using SafeMath for uint256;
    using SafeERC20 for IERC20;

    // Info of each user.
    struct UserInfo {
        uint256 amount; // How many LP tokens the user has provided.
        uint256 rewardDebt; // Reward debt. See explanation below.
        //
        // We do some fancy math here. Basically, any point in time, the amount of BASKETs
        // entitled to a user but is pending to be distributed is:
        //
        //   pending reward = (user.amount * pool.accBasketPerShare) - user.rewardDebt
        //
        // Whenever a user deposits or withdraws LP tokens to a pool. Here\u0027s what happens:
        //   1. The pool\u0027s `accBasketPerShare` (and `lastRewardBlock`) gets updated.
        //   2. User receives the pending reward sent to his/her address.
        //   3. User\u0027s `amount` gets updated.
        //   4. User\u0027s `rewardDebt` gets updated.
    }

    // Info of each pool.
    struct PoolInfo {
        IERC20 lpToken; // Address of LP token contract.
        uint256 allocPoint; // How many allocation points assigned to this pool. BASKETs to distribute per block.
        uint256 lastRewardBlock; // Last block number that BASKETs distribution occurs.
        uint256 accBasketPerShare; // Accumulated BASKETs per share, times 1e12. See below.
    }

    // The BASKET TOKEN!
    ERC20 public basket;

    // Div rate
    uint256 public constant divRate = 1e18;

    // Dev fund (10)%
    uint256 public constant devFundRate = 0.1e18;
    // Treasury rate (30)%
    uint256 public constant treasuryRate = 0.3e18;
    // Dev address
    address public devaddr;
    // Treasury address
    address public treasuryaddr;
    // BASKET tokens created per block.
    uint256 public basketPerBlock;
    // The migrator contract. It has a lot of power. Can only be set through governance (owner).
    IMigrator public migrator;

    // Info of each pool.
    PoolInfo[] public poolInfo;
    // Info of each user that stakes LP tokens.
    mapping(uint256 =\u003e mapping(address =\u003e UserInfo)) public userInfo;
    // Total allocation points. Must be the sum of all allocation points in all pools.
    uint256 public totalAllocPoint = 0;
    // The block number when BASKET mining starts.
    uint256 public startBlock;

    // Events
    event Recovered(address token, uint256 amount);
    event Deposit(address indexed user, uint256 indexed pid, uint256 amount);
    event Withdraw(address indexed user, uint256 indexed pid, uint256 amount);
    event EmergencyWithdraw(address indexed user, uint256 indexed pid, uint256 amount);

    constructor(
        ERC20 _basket,
        address _timelock,
        address _devaddr,
        address _treasuryaddr,
        uint256 _basketPerBlock,
        uint256 _startBlock
    ) {
        basket = _basket;
        devaddr = _devaddr;
        treasuryaddr = _treasuryaddr;
        basketPerBlock = _basketPerBlock;
        startBlock = _startBlock;

        transferOwnership(_timelock);
    }

    function poolLength() external view returns (uint256) {
        return poolInfo.length;
    }

    // Add a new lp to the pool. Can only be called by the owner.
    // XXX DO NOT add the same LP token more than once. Rewards will be messed up if you do.
    function add(
        uint256 _allocPoint,
        IERC20 _lpToken,
        bool _withUpdate
    ) public onlyOwner {
        if (_withUpdate) {
            massUpdatePools();
        }
        uint256 lastRewardBlock = block.number \u003e startBlock ? block.number : startBlock;
        totalAllocPoint = totalAllocPoint.add(_allocPoint);
        poolInfo.push(
            PoolInfo({
                lpToken: _lpToken,
                allocPoint: _allocPoint,
                lastRewardBlock: lastRewardBlock,
                accBasketPerShare: 0
            })
        );
    }

    // Update the given pool\u0027s BASKET allocation point. Can only be called by the owner.
    function set(
        uint256 _pid,
        uint256 _allocPoint,
        bool _withUpdate
    ) public onlyOwner {
        if (_withUpdate) {
            massUpdatePools();
        }
        totalAllocPoint = totalAllocPoint.sub(poolInfo[_pid].allocPoint).add(_allocPoint);
        poolInfo[_pid].allocPoint = _allocPoint;
    }

    // Return reward multiplier over the given _from to _to block.
    function getMultiplier(uint256 _from, uint256 _to) public pure returns (uint256) {
        return _to.sub(_from);
    }

    // View function to see pending BASKETs on frontend.
    function pendingBasket(uint256 _pid, address _user) external view returns (uint256) {
        PoolInfo storage pool = poolInfo[_pid];
        UserInfo storage user = userInfo[_pid][_user];
        uint256 accBasketPerShare = pool.accBasketPerShare;
        uint256 lpSupply = pool.lpToken.balanceOf(address(this));
        if (block.number \u003e pool.lastRewardBlock \u0026\u0026 lpSupply != 0) {
            uint256 multiplier = getMultiplier(pool.lastRewardBlock, block.number);
            uint256 basketReward = multiplier.mul(basketPerBlock).mul(pool.allocPoint).div(totalAllocPoint);
            accBasketPerShare = accBasketPerShare.add(basketReward.mul(1e12).div(lpSupply));
        }
        return user.amount.mul(accBasketPerShare).div(1e12).sub(user.rewardDebt);
    }

    // Update reward vairables for all pools. Be careful of gas spending!
    function massUpdatePools() public {
        uint256 length = poolInfo.length;
        for (uint256 pid = 0; pid \u003c length; ++pid) {
            updatePool(pid);
        }
    }

    // Update reward variables of the given pool to be up-to-date.
    function updatePool(uint256 _pid) public {
        PoolInfo storage pool = poolInfo[_pid];
        if (block.number \u003c= pool.lastRewardBlock) {
            return;
        }
        uint256 lpSupply = pool.lpToken.balanceOf(address(this));
        if (lpSupply == 0) {
            pool.lastRewardBlock = block.number;
            return;
        }
        uint256 multiplier = getMultiplier(pool.lastRewardBlock, block.number);
        uint256 basketReward = multiplier.mul(basketPerBlock).mul(pool.allocPoint).div(totalAllocPoint);

        uint256 devAlloc = basketReward.mul(devFundRate).div(divRate);
        uint256 treasuryAlloc = basketReward.mul(treasuryRate).div(divRate);

        uint256 basketWithoutDevAndTreasury = basketReward.sub(devAlloc).sub(treasuryAlloc);

        ERC20Mintable(address(basket)).mint(devaddr, devAlloc);
        ERC20Mintable(address(basket)).mint(treasuryaddr, treasuryAlloc);
        ERC20Mintable(address(basket)).mint(address(this), basketWithoutDevAndTreasury);

        pool.accBasketPerShare = pool.accBasketPerShare.add(basketWithoutDevAndTreasury.mul(1e12).div(lpSupply));
        pool.lastRewardBlock = block.number;
    }

    // Deposit LP tokens to MasterChef for BASKET allocation.
    function deposit(uint256 _pid, uint256 _amount) public {
        PoolInfo storage pool = poolInfo[_pid];
        UserInfo storage user = userInfo[_pid][msg.sender];
        updatePool(_pid);
        if (user.amount \u003e 0) {
            uint256 pending = user.amount.mul(pool.accBasketPerShare).div(1e12).sub(user.rewardDebt);
            safeBasketTransfer(msg.sender, pending);
        }
        pool.lpToken.safeTransferFrom(address(msg.sender), address(this), _amount);
        user.amount = user.amount.add(_amount);
        user.rewardDebt = user.amount.mul(pool.accBasketPerShare).div(1e12);
        emit Deposit(msg.sender, _pid, _amount);
    }

    // Withdraw LP tokens from MasterChef.
    function withdraw(uint256 _pid, uint256 _amount) public {
        PoolInfo storage pool = poolInfo[_pid];
        UserInfo storage user = userInfo[_pid][msg.sender];
        require(user.amount \u003e= _amount, \"withdraw: not good\");
        updatePool(_pid);
        uint256 pending = user.amount.mul(pool.accBasketPerShare).div(1e12).sub(user.rewardDebt);
        safeBasketTransfer(msg.sender, pending);
        user.amount = user.amount.sub(_amount);
        user.rewardDebt = user.amount.mul(pool.accBasketPerShare).div(1e12);
        pool.lpToken.safeTransfer(address(msg.sender), _amount);
        emit Withdraw(msg.sender, _pid, _amount);
    }

    // Withdraw without caring about rewards. EMERGENCY ONLY.
    function emergencyWithdraw(uint256 _pid) public {
        PoolInfo storage pool = poolInfo[_pid];
        UserInfo storage user = userInfo[_pid][msg.sender];
        pool.lpToken.safeTransfer(address(msg.sender), user.amount);
        emit EmergencyWithdraw(msg.sender, _pid, user.amount);
        user.amount = 0;
        user.rewardDebt = 0;
    }

    // Safe basket transfer function, just in case if rounding error causes pool to not have enough BASKETs.
    function safeBasketTransfer(address _to, uint256 _amount) internal {
        uint256 basketBal = basket.balanceOf(address(this));
        if (_amount \u003e basketBal) {
            basket.transfer(_to, basketBal);
        } else {
            basket.transfer(_to, _amount);
        }
    }

    // **** Custom functions ****

    function setDev(address _devaddr) public {
        require(msg.sender == devaddr, \"dev: wut?\");
        devaddr = _devaddr;
    }

    function setTreasury(address _treasury) public {
        require(msg.sender == treasuryaddr, \"treasury: wut?\");
        treasuryaddr = _treasury;
    }

    function setBasketPerBlock(uint256 _basketPerBlock) public onlyOwner {
        require(_basketPerBlock \u003e 0, \"!basketPerBlock-0\");
        basketPerBlock = _basketPerBlock;
    }

    function setStartBlock(uint256 _startBlock) public onlyOwner {
        require(block.number \u003c startBlock, \"started\");
        startBlock = _startBlock;
    }

    // **** Migrate ****

    // Set the migrator contract. Can only be called by the owner.
    function setMigrator(IMigrator _migrator) public onlyOwner {
        migrator = _migrator;
    }

    // Migrate lp token to another lp contract
    function migrate(uint256 _pid) public onlyOwner {
        require(address(migrator) != address(0), \"migrate: no migrator\");
        PoolInfo storage pool = poolInfo[_pid];
        IERC20 lpToken = pool.lpToken;
        uint256 bal = lpToken.balanceOf(address(this));
        lpToken.safeApprove(address(migrator), bal);
        IERC20 newLpToken = IERC20(migrator.migrate(address(lpToken)));
        require(bal == newLpToken.balanceOf(address(this)), \"migrate: bad\");
        pool.lpToken = newLpToken;
    }
}
"},"Ownable.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity \u003e=0.6.0 \u003c0.8.0;

import \"./Context.sol\";
/**
 * @dev Contract module which provides a basic access control mechanism, where
 * there is an account (an owner) that can be granted exclusive access to
 * specific functions.
 *
 * By default, the owner account will be the one that deploys the contract. This
 * can later be changed with {transferOwnership}.
 *
 * This module is used through inheritance. It will make available the modifier
 * `onlyOwner`, which can be applied to your functions to restrict their use to
 * the owner.
 */
abstract contract Ownable is Context {
    address private _owner;

    event OwnershipTransferred(address indexed previousOwner, address indexed newOwner);

    /**
     * @dev Initializes the contract setting the deployer as the initial owner.
     */
    constructor () internal {
        address msgSender = _msgSender();
        _owner = msgSender;
        emit OwnershipTransferred(address(0), msgSender);
    }

    /**
     * @dev Returns the address of the current owner.
     */
    function owner() public view virtual returns (address) {
        return _owner;
    }

    /**
     * @dev Throws if called by any account other than the owner.
     */
    modifier onlyOwner() {
        require(owner() == _msgSender(), \"Ownable: caller is not the owner\");
        _;
    }

    /**
     * @dev Leaves the contract without owner. It will not be possible to call
     * `onlyOwner` functions anymore. Can only be called by the current owner.
     *
     * NOTE: Renouncing ownership will leave the contract without an owner,
     * thereby removing any functionality that is only available to the owner.
     */
    function renounceOwnership() public virtual onlyOwner {
        emit OwnershipTransferred(_owner, address(0));
        _owner = address(0);
    }

    /**
     * @dev Transfers ownership of the contract to a new account (`newOwner`).
     * Can only be called by the current owner.
     */
    function transferOwnership(address newOwner) public virtual onlyOwner {
        require(newOwner != address(0), \"Ownable: new owner is the zero address\");
        emit OwnershipTransferred(_owner, newOwner);
        _owner = newOwner;
    }
}
"},"SafeERC20.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity \u003e=0.6.0 \u003c0.8.0;

import \"./IERC20.sol\";
import \"./SafeMath.sol\";
import \"./Address.sol\";

/**
 * @title SafeERC20
 * @dev Wrappers around ERC20 operations that throw on failure (when the token
 * contract returns false). Tokens that return no value (and instead revert or
 * throw on failure) are also supported, non-reverting calls are assumed to be
 * successful.
 * To use this library you can add a `using SafeERC20 for IERC20;` statement to your contract,
 * which allows you to call the safe operations as `token.safeTransfer(...)`, etc.
 */
library SafeERC20 {
    using SafeMath for uint256;
    using Address for address;

    function safeTransfer(IERC20 token, address to, uint256 value) internal {
        _callOptionalReturn(token, abi.encodeWithSelector(token.transfer.selector, to, value));
    }

    function safeTransferFrom(IERC20 token, address from, address to, uint256 value) internal {
        _callOptionalReturn(token, abi.encodeWithSelector(token.transferFrom.selector, from, to, value));
    }

    /**
     * @dev Deprecated. This function has issues similar to the ones found in
     * {IERC20-approve}, and its usage is discouraged.
     *
     * Whenever possible, use {safeIncreaseAllowance} and
     * {safeDecreaseAllowance} instead.
     */
    function safeApprove(IERC20 token, address spender, uint256 value) internal {
        // safeApprove should only be called when setting an initial allowance,
        // or when resetting it to zero. To increase and decrease it, use
        // \u0027safeIncreaseAllowance\u0027 and \u0027safeDecreaseAllowance\u0027
        // solhint-disable-next-line max-line-length
        require((value == 0) || (token.allowance(address(this), spender) == 0),
            \"SafeERC20: approve from non-zero to non-zero allowance\"
        );
        _callOptionalReturn(token, abi.encodeWithSelector(token.approve.selector, spender, value));
    }

    function safeIncreaseAllowance(IERC20 token, address spender, uint256 value) internal {
        uint256 newAllowance = token.allowance(address(this), spender).add(value);
        _callOptionalReturn(token, abi.encodeWithSelector(token.approve.selector, spender, newAllowance));
    }

    function safeDecreaseAllowance(IERC20 token, address spender, uint256 value) internal {
        uint256 newAllowance = token.allowance(address(this), spender).sub(value, \"SafeERC20: decreased allowance below zero\");
        _callOptionalReturn(token, abi.encodeWithSelector(token.approve.selector, spender, newAllowance));
    }

    /**
     * @dev Imitates a Solidity high-level call (i.e. a regular function call to a contract), relaxing the requirement
     * on the return value: the return value is optional (but if data is returned, it must not be false).
     * @param token The token targeted by the call.
     * @param data The call data (encoded using abi.encode or one of its variants).
     */
    function _callOptionalReturn(IERC20 token, bytes memory data) private {
        // We need to perform a low level call here, to bypass Solidity\u0027s return data size checking mechanism, since
        // we\u0027re implementing it ourselves. We use {Address.functionCall} to perform this call, which verifies that
        // the target address contains contract code and also asserts for success in the low-level call.

        bytes memory returndata = address(token).functionCall(data, \"SafeERC20: low-level call failed\");
        if (returndata.length \u003e 0) { // Return data is optional
            // solhint-disable-next-line max-line-length
            require(abi.decode(returndata, (bool)), \"SafeERC20: ERC20 operation did not succeed\");
        }
    }
}
"},"SafeMath.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity \u003e=0.6.0 \u003c0.8.0;

/**
 * @dev Wrappers over Solidity\u0027s arithmetic operations with added overflow
 * checks.
 *
 * Arithmetic operations in Solidity wrap on overflow. This can easily result
 * in bugs, because programmers usually assume that an overflow raises an
 * error, which is the standard behavior in high level programming languages.
 * `SafeMath` restores this intuition by reverting the transaction when an
 * operation overflows.
 *
 * Using this library instead of the unchecked operations eliminates an entire
 * class of bugs, so it\u0027s recommended to use it always.
 */
library SafeMath {
    /**
     * @dev Returns the addition of two unsigned integers, with an overflow flag.
     *
     * _Available since v3.4._
     */
    function tryAdd(uint256 a, uint256 b) internal pure returns (bool, uint256) {
        uint256 c = a + b;
        if (c \u003c a) return (false, 0);
        return (true, c);
    }

    /**
     * @dev Returns the substraction of two unsigned integers, with an overflow flag.
     *
     * _Available since v3.4._
     */
    function trySub(uint256 a, uint256 b) internal pure returns (bool, uint256) {
        if (b \u003e a) return (false, 0);
        return (true, a - b);
    }

    /**
     * @dev Returns the multiplication of two unsigned integers, with an overflow flag.
     *
     * _Available since v3.4._
     */
    function tryMul(uint256 a, uint256 b) internal pure returns (bool, uint256) {
        // Gas optimization: this is cheaper than requiring \u0027a\u0027 not being zero, but the
        // benefit is lost if \u0027b\u0027 is also tested.
        // See: https://github.com/OpenZeppelin/openzeppelin-contracts/pull/522
        if (a == 0) return (true, 0);
        uint256 c = a * b;
        if (c / a != b) return (false, 0);
        return (true, c);
    }

    /**
     * @dev Returns the division of two unsigned integers, with a division by zero flag.
     *
     * _Available since v3.4._
     */
    function tryDiv(uint256 a, uint256 b) internal pure returns (bool, uint256) {
        if (b == 0) return (false, 0);
        return (true, a / b);
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers, with a division by zero flag.
     *
     * _Available since v3.4._
     */
    function tryMod(uint256 a, uint256 b) internal pure returns (bool, uint256) {
        if (b == 0) return (false, 0);
        return (true, a % b);
    }

    /**
     * @dev Returns the addition of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity\u0027s `+` operator.
     *
     * Requirements:
     *
     * - Addition cannot overflow.
     */
    function add(uint256 a, uint256 b) internal pure returns (uint256) {
        uint256 c = a + b;
        require(c \u003e= a, \"SafeMath: addition overflow\");
        return c;
    }

    /**
     * @dev Returns the subtraction of two unsigned integers, reverting on
     * overflow (when the result is negative).
     *
     * Counterpart to Solidity\u0027s `-` operator.
     *
     * Requirements:
     *
     * - Subtraction cannot overflow.
     */
    function sub(uint256 a, uint256 b) internal pure returns (uint256) {
        require(b \u003c= a, \"SafeMath: subtraction overflow\");
        return a - b;
    }

    /**
     * @dev Returns the multiplication of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity\u0027s `*` operator.
     *
     * Requirements:
     *
     * - Multiplication cannot overflow.
     */
    function mul(uint256 a, uint256 b) internal pure returns (uint256) {
        if (a == 0) return 0;
        uint256 c = a * b;
        require(c / a == b, \"SafeMath: multiplication overflow\");
        return c;
    }

    /**
     * @dev Returns the integer division of two unsigned integers, reverting on
     * division by zero. The result is rounded towards zero.
     *
     * Counterpart to Solidity\u0027s `/` operator. Note: this function uses a
     * `revert` opcode (which leaves remaining gas untouched) while Solidity
     * uses an invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function div(uint256 a, uint256 b) internal pure returns (uint256) {
        require(b \u003e 0, \"SafeMath: division by zero\");
        return a / b;
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
     * reverting when dividing by zero.
     *
     * Counterpart to Solidity\u0027s `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function mod(uint256 a, uint256 b) internal pure returns (uint256) {
        require(b \u003e 0, \"SafeMath: modulo by zero\");
        return a % b;
    }

    /**
     * @dev Returns the subtraction of two unsigned integers, reverting with custom message on
     * overflow (when the result is negative).
     *
     * CAUTION: This function is deprecated because it requires allocating memory for the error
     * message unnecessarily. For custom revert reasons use {trySub}.
     *
     * Counterpart to Solidity\u0027s `-` operator.
     *
     * Requirements:
     *
     * - Subtraction cannot overflow.
     */
    function sub(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b \u003c= a, errorMessage);
        return a - b;
    }

    /**
     * @dev Returns the integer division of two unsigned integers, reverting with custom message on
     * division by zero. The result is rounded towards zero.
     *
     * CAUTION: This function is deprecated because it requires allocating memory for the error
     * message unnecessarily. For custom revert reasons use {tryDiv}.
     *
     * Counterpart to Solidity\u0027s `/` operator. Note: this function uses a
     * `revert` opcode (which leaves remaining gas untouched) while Solidity
     * uses an invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function div(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b \u003e 0, errorMessage);
        return a / b;
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
     * reverting with custom message when dividing by zero.
     *
     * CAUTION: This function is deprecated because it requires allocating memory for the error
     * message unnecessarily. For custom revert reasons use {tryMod}.
     *
     * Counterpart to Solidity\u0027s `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function mod(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b \u003e 0, errorMessage);
        return a % b;
    }
}
"},"StackedMasterchef.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.7.3;

import \"./MasterChef.sol\";
import \"./IMigrator.sol\";
import \"./ERC20Mintable.sol\";

import \"./Ownable.sol\";
import \"./ERC20.sol\";
import \"./SafeERC20.sol\";
import \"./SafeMath.sol\";

// Sushiswap - PID 232: BDI/ETH
// BasketDAO - PID   1: BDI/ETH

// Sushiswap - PID 233: BASK/ETH
// BasketDAO - PID   2: BASK/ETH

interface ISushiswapMasterchef {
    function sushiPerBlock() external view returns (uint256);
}

contract StackedMasterChef is Ownable {
    using SafeMath for uint256;
    using SafeERC20 for IERC20;

    struct PoolInfo {
        uint256 lastRewardBlock;
        uint256 accRewardsPerShare;
    }

    struct UserInfo {
        uint256 amount;
        uint256 sushiRewardDebt;
        uint256 basketRewardDebt;
    }

    address public timelock;

    IERC20 public stakingToken; // Stake in SUSHI
    IERC20 public mintableToken; // Mint and deposit in BASK (Since we have full control over it)

    IERC20 public constant baskToken = IERC20(0x44564d0bd94343f72E3C8a0D22308B7Fa71DB0Bb);
    IERC20 public constant sushiToken = IERC20(0x6B3595068778DD592e39A122f4f5a5cF09C90fE2);

    mapping(address =\u003e UserInfo) public userInfo;
    mapping(address =\u003e PoolInfo) public poolInfo;

    MasterChef basketdaoMasterChef = MasterChef(0xDB9daa0a50B33e4fe9d0ac16a1Df1d335F96595e);
    uint256 public baskPID;

    MasterChef sushiMasterChef = MasterChef(0xc2EdaD668740f1aA35E4D8f227fB8E17dcA888Cd);
    uint256 public sushiPID;

    constructor(
        address _timelock,
        IERC20 _stakingToken,
        IERC20 _mintableToken,
        uint256 _sushiPID,
        uint256 _baskPID
    ) {
        timelock = _timelock;

        stakingToken = _stakingToken;
        mintableToken = _mintableToken;
        sushiPID = _sushiPID;
        baskPID = _baskPID;

        stakingToken.approve(address(sushiMasterChef), uint256(-1));
        mintableToken.approve(address(basketdaoMasterChef), uint256(-1));

        updatePools();
    }

    modifier onlyTimelock {
        require(msg.sender == timelock, \"!timelock\");
        _;
    }

    function deposit(uint256 _amount) public {
        PoolInfo storage sushiPool = poolInfo[address(sushiToken)];
        PoolInfo storage baskPool = poolInfo[address(baskToken)];
        UserInfo storage user = userInfo[msg.sender];

        // Deposit first to trigger token transfer
        stakingToken.transferFrom(msg.sender, address(this), _amount);
        ERC20Mintable(address(mintableToken)).mint(address(this), _amount);

        sushiMasterChef.deposit(sushiPID, _amount);
        basketdaoMasterChef.deposit(baskPID, _amount);

        if (user.amount \u003e 0) {
            uint256 baskPending = user.amount.mul(baskPool.accRewardsPerShare).div(1e12).sub(user.basketRewardDebt);
            uint256 sushiPending = user.amount.mul(sushiPool.accRewardsPerShare).div(1e12).sub(user.sushiRewardDebt);

            _safeTransfer(baskToken, msg.sender, baskPending);
            _safeTransfer(sushiToken, msg.sender, sushiPending);
        }

        user.amount = user.amount.add(_amount);
        user.sushiRewardDebt = user.amount.mul(sushiPool.accRewardsPerShare).div(1e12);
        user.basketRewardDebt = user.amount.mul(baskPool.accRewardsPerShare).div(1e12);
    }

    function withdraw(uint256 _amount) public {
        PoolInfo storage sushiPool = poolInfo[address(sushiToken)];
        PoolInfo storage baskPool = poolInfo[address(baskToken)];
        UserInfo storage user = userInfo[msg.sender];

        // Withdraw first to trigger token transfer
        sushiMasterChef.withdraw(sushiPID, _amount);
        basketdaoMasterChef.withdraw(baskPID, _amount);

        stakingToken.transfer(msg.sender, _amount);
        ERC20Mintable(address(mintableToken)).burn(_amount);

        updatePools();
        if (user.amount \u003e 0) {
            uint256 baskPending = user.amount.mul(baskPool.accRewardsPerShare).div(1e12).sub(user.basketRewardDebt);
            uint256 sushiPending = user.amount.mul(sushiPool.accRewardsPerShare).div(1e12).sub(user.sushiRewardDebt);

            _safeTransfer(baskToken, msg.sender, baskPending);
            _safeTransfer(sushiToken, msg.sender, sushiPending);
        }

        user.amount = user.amount.sub(_amount);
        user.sushiRewardDebt = user.amount.mul(sushiPool.accRewardsPerShare).div(1e12);
        user.basketRewardDebt = user.amount.mul(baskPool.accRewardsPerShare).div(1e12);
    }

    // Withdraw without caring about rewards. EMERGENCY ONLY.
    function emergencyWithdraw() public {
        UserInfo storage user = userInfo[msg.sender];
        sushiMasterChef.withdraw(sushiPID, user.amount);
        stakingToken.safeTransfer(address(msg.sender), user.amount);

        user.amount = 0;
        user.sushiRewardDebt = 0;
        user.basketRewardDebt = 0;
    }

    // Update reward variables of the given pool to be up-to-date.
    function updatePools() public {
        // Update the pools
        sushiMasterChef.updatePool(sushiPID);
        basketdaoMasterChef.updatePool(baskPID);

        // Sushi Pools
        (, , uint256 sushiLastRewardBlock, uint256 accSushiPerShare) = sushiMasterChef.poolInfo(sushiPID);
        PoolInfo storage sushiPool = poolInfo[address(sushiToken)];
        sushiPool.accRewardsPerShare = accSushiPerShare;
        sushiPool.lastRewardBlock = sushiLastRewardBlock;

        // Bask Pools
        (, , uint256 baskLastRewardBlock, uint256 accBaskPerShare) = basketdaoMasterChef.poolInfo(baskPID);
        PoolInfo storage baskPool = poolInfo[address(baskToken)];
        baskPool.accRewardsPerShare = accBaskPerShare;
        baskPool.lastRewardBlock = baskLastRewardBlock;
    }

    // **** View ****

    function pendingRewards(address _user) external view returns (uint256, uint256) {
        return (pendingSushiRewards(_user), pendingBaskRewards(_user));
    }

    function pendingSushiRewards(address _user) public view returns (uint256) {
        (, uint256 allocPoint, uint256 lastRewardBlock, uint256 accSushiPerShare) = sushiMasterChef.poolInfo(sushiPID);
        UserInfo memory user = userInfo[_user];

        uint256 lpSupply = stakingToken.balanceOf(address(sushiMasterChef));
        if (block.number \u003e lastRewardBlock \u0026\u0026 lpSupply != 0) {
            uint256 multiplier = sushiMasterChef.getMultiplier(lastRewardBlock, block.number);
            uint256 sushiReward =
                multiplier.mul(ISushiswapMasterchef(address(sushiMasterChef)).sushiPerBlock()).mul(allocPoint).div(
                    sushiMasterChef.totalAllocPoint()
                );

            accSushiPerShare = accSushiPerShare.add(sushiReward.mul(1e12).div(lpSupply));
        }

        return user.amount.mul(accSushiPerShare).div(1e12).sub(user.sushiRewardDebt);
    }

    function pendingBaskRewards(address _user) public view returns (uint256) {
        (, uint256 allocPoint, uint256 lastRewardBlock, uint256 accBasketPerShare) =
            basketdaoMasterChef.poolInfo(baskPID);
        UserInfo memory user = userInfo[_user];

        uint256 lpSupply = mintableToken.balanceOf(address(basketdaoMasterChef));
        if (block.number \u003e lastRewardBlock \u0026\u0026 lpSupply != 0) {
            uint256 multiplier = basketdaoMasterChef.getMultiplier(lastRewardBlock, block.number);
            uint256 basketReward =
                multiplier.mul(basketdaoMasterChef.basketPerBlock()).mul(allocPoint).div(
                    basketdaoMasterChef.totalAllocPoint()
                );

            uint256 devAlloc = basketReward.mul(basketdaoMasterChef.devFundRate()).div(basketdaoMasterChef.divRate());
            uint256 treasuryAlloc =
                basketReward.mul(basketdaoMasterChef.treasuryRate()).div(basketdaoMasterChef.divRate());

            uint256 basketWithoutDevAndTreasury = basketReward.sub(devAlloc).sub(treasuryAlloc);

            accBasketPerShare = accBasketPerShare.add(basketWithoutDevAndTreasury.mul(1e12).div(lpSupply));
        }

        return user.amount.mul(accBasketPerShare).div(1e12).sub(user.basketRewardDebt);
    }

    // **** Restricted functions **** //

    function emergencyExecute(
        address target,
        uint256 value,
        string memory signature,
        bytes memory data
    ) public onlyTimelock returns (bytes memory) {
        bytes memory callData;

        if (bytes(signature).length == 0) {
            callData = data;
        } else {
            callData = abi.encodePacked(bytes4(keccak256(bytes(signature))), data);
        }

        // solium-disable-next-line security/no-call-value
        (bool success, bytes memory returnData) = target.call{ value: value }(callData);
        require(success, \"!tx\");

        return returnData;
    }

    // **** Internal Functions **** //

    function _safeTransfer(
        IERC20 _token,
        address _to,
        uint256 _amount
    ) internal {
        uint256 maxBal = _token.balanceOf(address(this));
        if (_amount \u003e maxBal) {
            _token.transfer(_to, maxBal);
        } else {
            _token.transfer(_to, _amount);
        }
    }
}

