// SPDX-License-Identifier: MIT

pragma solidity ^0.6.2;

/**
 * @dev Collection of functions related to the address type
 */
library Address {
    /**
     * @dev Returns true if `account` is a contract.
     *
     * [IMPORTANT]
     * ====
     * It is unsafe to assume that an address for which this function returns
     * false is an externally-owned account (EOA) and not a contract.
     *
     * Among others, `isContract` will return false for the following
     * types of addresses:
     *
     *  - an externally-owned account
     *  - a contract in construction
     *  - an address where a contract will be created
     *  - an address where a contract lived, but was destroyed
     * ====
     */
    function isContract(address account) internal view returns (bool) {
        // According to EIP-1052, 0x0 is the value returned for not-yet created accounts
        // and 0xc5d2460186f7233c927e7db2dcc703c0e500b653ca82273b7bfad8045d85a470 is returned
        // for accounts without code, i.e. `keccak256(\u0027\u0027)`
        bytes32 codehash;
        bytes32 accountHash = 0xc5d2460186f7233c927e7db2dcc703c0e500b653ca82273b7bfad8045d85a470;
        // solhint-disable-next-line no-inline-assembly
        assembly { codehash := extcodehash(account) }
        return (codehash != accountHash \u0026\u0026 codehash != 0x0);
    }

    /**
     * @dev Replacement for Solidity\u0027s `transfer`: sends `amount` wei to
     * `recipient`, forwarding all available gas and reverting on errors.
     *
     * https://eips.ethereum.org/EIPS/eip-1884[EIP1884] increases the gas cost
     * of certain opcodes, possibly making contracts go over the 2300 gas limit
     * imposed by `transfer`, making them unable to receive funds via
     * `transfer`. {sendValue} removes this limitation.
     *
     * https://diligence.consensys.net/posts/2019/09/stop-using-soliditys-transfer-now/[Learn more].
     *
     * IMPORTANT: because control is transferred to `recipient`, care must be
     * taken to not create reentrancy vulnerabilities. Consider using
     * {ReentrancyGuard} or the
     * https://solidity.readthedocs.io/en/v0.5.11/security-considerations.html#use-the-checks-effects-interactions-pattern[checks-effects-interactions pattern].
     */
    function sendValue(address payable recipient, uint256 amount) internal {
        require(address(this).balance \u003e= amount, \"Address: insufficient balance\");

        // solhint-disable-next-line avoid-low-level-calls, avoid-call-value
        (bool success, ) = recipient.call{ value: amount }(\"\");
        require(success, \"Address: unable to send value, recipient may have reverted\");
    }

    /**
     * @dev Performs a Solidity function call using a low level `call`. A
     * plain`call` is an unsafe replacement for a function call: use this
     * function instead.
     *
     * If `target` reverts with a revert reason, it is bubbled up by this
     * function (like regular Solidity function calls).
     *
     * Returns the raw returned data. To convert to the expected return value,
     * use https://solidity.readthedocs.io/en/latest/units-and-global-variables.html?highlight=abi.decode#abi-encoding-and-decoding-functions[`abi.decode`].
     *
     * Requirements:
     *
     * - `target` must be a contract.
     * - calling `target` with `data` must not revert.
     *
     * _Available since v3.1._
     */
    function functionCall(address target, bytes memory data) internal returns (bytes memory) {
      return functionCall(target, data, \"Address: low-level call failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`], but with
     * `errorMessage` as a fallback revert reason when `target` reverts.
     *
     * _Available since v3.1._
     */
    function functionCall(address target, bytes memory data, string memory errorMessage) internal returns (bytes memory) {
        return _functionCallWithValue(target, data, 0, errorMessage);
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`],
     * but also transferring `value` wei to `target`.
     *
     * Requirements:
     *
     * - the calling contract must have an ETH balance of at least `value`.
     * - the called Solidity function must be `payable`.
     *
     * _Available since v3.1._
     */
    function functionCallWithValue(address target, bytes memory data, uint256 value) internal returns (bytes memory) {
        return functionCallWithValue(target, data, value, \"Address: low-level call with value failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCallWithValue-address-bytes-uint256-}[`functionCallWithValue`], but
     * with `errorMessage` as a fallback revert reason when `target` reverts.
     *
     * _Available since v3.1._
     */
    function functionCallWithValue(address target, bytes memory data, uint256 value, string memory errorMessage) internal returns (bytes memory) {
        require(address(this).balance \u003e= value, \"Address: insufficient balance for call\");
        return _functionCallWithValue(target, data, value, errorMessage);
    }

    function _functionCallWithValue(address target, bytes memory data, uint256 weiValue, string memory errorMessage) private returns (bytes memory) {
        require(isContract(target), \"Address: call to non-contract\");

        // solhint-disable-next-line avoid-low-level-calls
        (bool success, bytes memory returndata) = target.call{ value: weiValue }(data);
        if (success) {
            return returndata;
        } else {
            // Look for revert reason and bubble it up if present
            if (returndata.length \u003e 0) {
                // The easiest way to bubble the revert reason is using memory via assembly

                // solhint-disable-next-line no-inline-assembly
                assembly {
                    let returndata_size := mload(returndata)
                    revert(add(32, returndata), returndata_size)
                }
            } else {
                revert(errorMessage);
            }
        }
    }
}
"},"Holdefi.sol":{"content":"// SPDX-License-Identifier: UNLICENSED
pragma solidity 0.6.12;
pragma experimental ABIEncoderV2;

import \"./SafeMath.sol\";
import \"./ReentrancyGuard.sol\";
import \"./HoldefiPausableOwnable.sol\";
import \"./HoldefiCollaterals.sol\";


/// @notice File: contracts/HoldefiPrices.sol
interface HoldefiPricesInterface {
\tfunction getAssetValueFromAmount(address asset, uint256 amount) external view returns(uint256 value);
\tfunction getAssetAmountFromValue(address asset, uint256 value) external view returns(uint256 amount);\t
}

/// @notice File: contracts/HoldefiSettings.sol
interface HoldefiSettingsInterface {

\t/// @notice Markets Features
\tstruct MarketSettings {
\t\tbool isExist;
\t\tbool isActive;      

\t\tuint256 borrowRate;
\t\tuint256 borrowRateUpdateTime;

\t\tuint256 suppliersShareRate;
\t\tuint256 suppliersShareRateUpdateTime;

\t\tuint256 promotionRate;
\t}

\t/// @notice Collateral Features
\tstruct CollateralSettings {
\t\tbool isExist;
\t\tbool isActive;    

\t\tuint256 valueToLoanRate; 
\t\tuint256 VTLUpdateTime;

\t\tuint256 penaltyRate;
\t\tuint256 penaltyUpdateTime;

\t\tuint256 bonusRate;
\t}

\tfunction getInterests(address market)
\t\texternal
\t\tview
\t\treturns (uint256 borrowRate, uint256 supplyRateBase, uint256 promotionRate);
\tfunction resetPromotionRate (address market) external;
\tfunction getMarketsList() external view returns(address[] memory marketsList);
\tfunction marketAssets(address market) external view returns(MarketSettings memory);
\tfunction collateralAssets(address collateral) external view returns(CollateralSettings memory);
}

/// @title Main Holdefi contract
/// @author Holdefi Team
/// @dev The address of ETH considered as 0xEeeeeEeeeEeEeeEeEeEeeEEEeeeeEeeeeeeeEEeE
/// @dev All indexes are scaled by (secondsPerYear * rateDecimals)
/// @dev All values are based ETH price considered 1 and all values decimals considered 30
/// @dev Error codes description: 
/// \tE01: Asset should not be ETH
/// \tE02: Market is not active
/// \tE03: Collateral is not active
/// \tE04: Account should not be the `msg.sender`
/// \tE05: User borrow balance is zero
/// \tE06: User should be under collateral or should have ativity in the past year
/// \tE07: Amount should be less than Max
/// \tE08: Cannot transfer
/// \tE09: Total balance should not be zero
/// \tE10: Borrow power should not be zero
/// \tE11: Requested amount is not available
/// \tE12: Borrow power should be more than the value of the requested amount to borrow
/// \tE13: Promotion debt should be less than the promotion reserve
///\t\tE14: Transfer amount exceeds allowance
///\t\tE15: Sender should be Holdefi Settings contract
///\t\tE16: There is not enough collateral
///\t\tE17: Amount should be less than the market debt
contract Holdefi is HoldefiPausableOwnable, ReentrancyGuard {

\tusing SafeMath for uint256;

\tusing SafeERC20 for IERC20;

\t/// @notice Markets are assets can be supplied and borrowed
\tstruct Market {
\t\tuint256 totalSupply;

\t\tuint256 supplyIndex;      \t\t\t\t// Scaled by: secondsPerYear * rateDecimals
\t\tuint256 supplyIndexUpdateTime;

\t\tuint256 totalBorrow;

\t\tuint256 borrowIndex;      \t\t\t\t// Scaled by: secondsPerYear * rateDecimals
\t\tuint256 borrowIndexUpdateTime;

\t\tuint256 promotionReserveScaled;      \t// Scaled by: secondsPerYear * rateDecimals
\t\tuint256 promotionReserveLastUpdateTime;

\t\tuint256 promotionDebtScaled;      \t\t// Scaled by: secondsPerYear * rateDecimals
\t\tuint256 promotionDebtLastUpdateTime;
\t}

\t/// @notice Collaterals are assets can be used only as collateral for borrowing with no interest
\tstruct Collateral {
\t\tuint256 totalCollateral;
\t\tuint256 totalLiquidatedCollateral;
\t}

\t/// @notice Users profile for each market
\tstruct MarketAccount {
\t\tmapping (address =\u003e uint) allowance;
\t\tuint256 balance;
\t\tuint256 accumulatedInterest;

\t\tuint256 lastInterestIndex;      \t\t// Scaled by: secondsPerYear * rateDecimals
\t}

\t/// @notice Users profile for each collateral
\tstruct CollateralAccount {
\t\tmapping (address =\u003e uint) allowance;
\t\tuint256 balance;
\t\tuint256 lastUpdateTime;
\t}

\tstruct MarketData {
\t\tuint256 balance;
\t\tuint256 interest;
\t\tuint256 currentIndex; 
\t}

\taddress constant private ethAddress = 0xEeeeeEeeeEeEeeEeEeEeeEEEeeeeEeeeeeeeEEeE;

\t/// @dev All rates in this contract are scaled by rateDecimals
\tuint256 constant private rateDecimals = 10 ** 4;

\tuint256 constant private secondsPerYear = 31536000;

\t/// @dev For round up borrow interests
\tuint256 constant private oneUnit = 1;

\t/// @dev Used for calculating liquidation threshold 
\t/// @dev There is 5% gap between value to loan rate and liquidation rate
\tuint256 constant private fivePercentLiquidationGap = 500;

\t/// @notice Contract for getting protocol settings
\tHoldefiSettingsInterface public holdefiSettings;

\t/// @notice Contract for getting asset prices
\tHoldefiPricesInterface public holdefiPrices;

\t/// @notice Contract for holding collaterals
\tHoldefiCollaterals public holdefiCollaterals;

\t/// @dev Markets: marketAddress =\u003e marketDetails
\tmapping (address =\u003e Market) public marketAssets;

\t/// @dev Collaterals: collateralAddress =\u003e collateralDetails
\tmapping (address =\u003e Collateral) public collateralAssets;

\t/// @dev Markets Debt after liquidation: collateralAddress =\u003e marketAddress =\u003e marketDebtBalance
\tmapping (address =\u003e mapping (address =\u003e uint256)) public marketDebt;

\t/// @dev Users Supplies: userAddress =\u003e marketAddress =\u003e supplyDetails
\tmapping (address =\u003e mapping (address =\u003e MarketAccount)) private supplies;

\t/// @dev Users Borrows: userAddress =\u003e collateralAddress =\u003e marketAddress =\u003e borrowDetails
\tmapping (address =\u003e mapping (address =\u003e mapping (address =\u003e MarketAccount))) private borrows;

\t/// @dev Users Collaterals: userAddress =\u003e collateralAddress =\u003e collateralDetails
\tmapping (address =\u003e mapping (address =\u003e CollateralAccount)) private collaterals;
\t
\t// ----------- Events -----------

\t/// @notice Event emitted when a market asset is supplied
\tevent Supply(
\t\taddress sender,
\t\taddress indexed supplier,
\t\taddress indexed market,
\t\tuint256 amount,
\t\tuint256 balance,
\t\tuint256 interest,
\t\tuint256 index,
\t\tuint16 referralCode
\t);

\t/// @notice Event emitted when a supply is withdrawn
\tevent WithdrawSupply(
\t\taddress sender,
\t\taddress indexed supplier,
\t\taddress indexed market,
\t\tuint256 amount,
\t\tuint256 balance,
\t\tuint256 interest,
\t\tuint256 index
\t);

\t/// @notice Event emitted when the collateral asset is deposited
\tevent Collateralize(
\t\taddress sender,
\t\taddress indexed collateralizer,
\t\taddress indexed collateral,
\t\tuint256 amount,
\t\tuint256 balance
\t);

\t/// @notice Event emitted when the collateral is withdrawn
\tevent WithdrawCollateral(
\t\taddress sender,
\t\taddress indexed collateralizer,
\t\taddress indexed collateral,
\t\tuint256 amount,
\t\tuint256 balance
\t);

\t/// @notice Event emitted when a market asset is borrowed
\tevent Borrow(
\t\taddress sender,
\t\taddress indexed borrower,
\t\taddress indexed market,
\t\taddress indexed collateral,
\t\tuint256 amount,
\t\tuint256 balance,
\t\tuint256 interest,
\t\tuint256 index,
\t\tuint16 referralCode
\t);

\t/// @notice Event emitted when a borrow is repaid
\tevent RepayBorrow(
\t\taddress sender,
\t\taddress indexed borrower,
\t\taddress indexed market,
\t\taddress indexed collateral,
\t\tuint256 amount,
\t\tuint256 balance,
\t\tuint256 interest,
\t\tuint256 index
\t);

\t/// @notice Event emitted when the supply index is updated for a market asset
\tevent UpdateSupplyIndex(address indexed market, uint256 newSupplyIndex, uint256 supplyRate);

\t/// @notice Event emitted when the borrow index is updated for a market asset
\tevent UpdateBorrowIndex(address indexed market, uint256 newBorrowIndex, uint256 borrowRate);

\t/// @notice Event emitted when the collateral is liquidated
\tevent CollateralLiquidated(
\t\taddress indexed borrower,
\t\taddress indexed market,
\t\taddress indexed collateral,
\t\tuint256 marketDebt,
\t\tuint256 liquidatedCollateral
\t);

\t/// @notice Event emitted when a liquidated collateral is purchased in exchange for the specified market
\tevent BuyLiquidatedCollateral(
\t\taddress indexed market,
\t\taddress indexed collateral,
\t\tuint256 marketAmount,
\t\tuint256 collateralAmount
\t);

\t/// @notice Event emitted when HoldefiPrices contract is changed
\tevent HoldefiPricesContractChanged(address newAddress, address oldAddress);

\t/// @notice Event emitted when a liquidation reserve is withdrawn by the owner
\tevent LiquidationReserveWithdrawn(address indexed collateral, uint256 amount);

\t/// @notice Event emitted when a liquidation reserve is deposited
\tevent LiquidationReserveDeposited(address indexed collateral, uint256 amount);

\t/// @notice Event emitted when a promotion reserve is withdrawn by the owner
\tevent PromotionReserveWithdrawn(address indexed market, uint256 amount, uint256 newPromotionReserve);

\t/// @notice Event emitted when a promotion reserve is deposited
\tevent PromotionReserveDeposited(address indexed market, uint256 amount, uint256 newPromotionReserve);

\t/// @notice Event emitted when a promotion reserve is updated
\tevent PromotionReserveUpdated(address indexed market, uint256 promotionReserve);

\t/// @notice Event emitted when a promotion debt is updated
\tevent PromotionDebtUpdated(address indexed market, uint256 promotionDebt);

\t/// @notice Initializes the Holdefi contract
    /// @param holdefiSettingsAddress Holdefi settings contract address
    /// @param holdefiPricesAddress Holdefi prices contract address
\tconstructor(
\t\tHoldefiSettingsInterface holdefiSettingsAddress,
\t\tHoldefiPricesInterface holdefiPricesAddress
\t)
\t\tpublic
\t{
\t\tholdefiSettings = holdefiSettingsAddress;
\t\tholdefiPrices = holdefiPricesAddress;
\t\tholdefiCollaterals = new HoldefiCollaterals();
\t}


\t/// @dev Modifier to check if the asset is ETH or not
\t/// @param asset Address of the given asset
    modifier isNotETHAddress(address asset) {
        require (asset != ethAddress, \"E01\");
        _;
    }

\t/// @dev Modifier to check if the market is active or not
\t/// @param market Address of the given market
    modifier marketIsActive(address market) {
\t\trequire (holdefiSettings.marketAssets(market).isActive, \"E02\");
        _;
    }

\t/// @dev Modifier to check if the collateral is active or not
\t/// @param collateral Address of the given collateral
    modifier collateralIsActive(address collateral) {
\t\trequire (holdefiSettings.collateralAssets(collateral).isActive, \"E03\");
        _;
    }

\t/// @dev Modifier to check if the account address is equal to the msg.sender or not
    /// @param account The given account address
    modifier accountIsValid(address account) {
\t\trequire (msg.sender != account, \"E04\");
        _;
    }

    receive() external payable {
        revert();
    }

\t/// @notice Returns balance and interest of an account for a given market
    /// @dev supplyInterest = accumulatedInterest + (balance * (marketSupplyIndex - userLastSupplyInterestIndex))
    /// @param account Supplier address to get supply information
    /// @param market Address of the given market
    /// @return balance Supplied amount on the specified market
    /// @return interest Profit earned
    /// @return currentSupplyIndex Supply index for the given market at current time
\tfunction getAccountSupply(address account, address market)
\t\tpublic
\t\tview
\t\treturns (uint256 balance, uint256 interest, uint256 currentSupplyIndex)
\t{
\t\tbalance = supplies[account][market].balance;

\t\t(currentSupplyIndex,) = getCurrentSupplyIndex(market);

\t\tuint256 deltaInterestIndex = currentSupplyIndex.sub(supplies[account][market].lastInterestIndex);
\t\tuint256 deltaInterestScaled = deltaInterestIndex.mul(balance);
\t\tuint256 deltaInterest = deltaInterestScaled.div(secondsPerYear).div(rateDecimals);
\t\t
\t\tinterest = supplies[account][market].accumulatedInterest.add(deltaInterest);
\t}

\t/// @notice Returns balance and interest of an account for a given market on a given collateral
    /// @dev borrowInterest = accumulatedInterest + (balance * (marketBorrowIndex - userLastBorrowInterestIndex))
    /// @param account Borrower address to get Borrow information
    /// @param market Address of the given market
    /// @param collateral Address of the given collateral
    /// @return balance Borrowed amount on the specified market
    /// @return interest The amount of interest the borrower should pay
    /// @return currentBorrowIndex Borrow index for the given market at current time
\tfunction getAccountBorrow(address account, address market, address collateral)
\t\tpublic
\t\tview
\t\treturns (uint256 balance, uint256 interest, uint256 currentBorrowIndex)
\t{
\t\tbalance = borrows[account][collateral][market].balance;

\t\t(currentBorrowIndex,) = getCurrentBorrowIndex(market);

\t\tuint256 deltaInterestIndex =
\t\t\tcurrentBorrowIndex.sub(borrows[account][collateral][market].lastInterestIndex);

\t\tuint256 deltaInterestScaled = deltaInterestIndex.mul(balance);
\t\tuint256 deltaInterest = deltaInterestScaled.div(secondsPerYear).div(rateDecimals);
\t\tif (balance \u003e 0) {
\t\t\tdeltaInterest = deltaInterest.add(oneUnit);
\t\t}

\t\tinterest = borrows[account][collateral][market].accumulatedInterest.add(deltaInterest);
\t}


\t/// @notice Returns collateral balance, time since last activity, borrow power, total borrow value, and liquidation status for a given collateral
    /// @dev borrowPower = (collateralValue / collateralValueToLoanRate) - totalBorrowValue
    /// @dev liquidationThreshold = collateralValueToLoanRate - 5%
    /// @dev User will be in liquidation state if (collateralValue / totalBorrowValue) \u003c liquidationThreshold
    /// @param account Account address to get collateral information
    /// @param collateral Address of the given collateral
    /// @return balance Amount of the specified collateral
    /// @return timeSinceLastActivity Time since last activity performed by the account
    /// @return borrowPowerValue The borrowing power for the account of the given collateral
    /// @return totalBorrowValue Accumulative borrowed values on the given collateral
    /// @return underCollateral A boolean value indicates whether the user is in the liquidation state or not
\tfunction getAccountCollateral(address account, address collateral)
\t\tpublic
\t\tview
\t\treturns (
\t\t\tuint256 balance,
\t\t\tuint256 timeSinceLastActivity,
\t\t\tuint256 borrowPowerValue,
\t\t\tuint256 totalBorrowValue,
\t\t\tbool underCollateral
\t\t)
\t{
\t\tuint256 valueToLoanRate = holdefiSettings.collateralAssets(collateral).valueToLoanRate;
\t\tif (valueToLoanRate == 0) {
\t\t\treturn (0, 0, 0, 0, false);
\t\t}

\t\tbalance = collaterals[account][collateral].balance;

\t\tuint256 collateralValue = holdefiPrices.getAssetValueFromAmount(collateral, balance);
\t\tuint256 liquidationThresholdRate = valueToLoanRate.sub(fivePercentLiquidationGap);

\t\tuint256 totalBorrowPowerValue = collateralValue.mul(rateDecimals).div(valueToLoanRate);
\t\tuint256 liquidationThresholdValue = collateralValue.mul(rateDecimals).div(liquidationThresholdRate);

\t\ttotalBorrowValue = getAccountTotalBorrowValue(account, collateral);
\t\tif (totalBorrowValue \u003e 0) {
\t\t\ttimeSinceLastActivity = block.timestamp.sub(collaterals[account][collateral].lastUpdateTime);
\t\t}

\t\tborrowPowerValue = 0;
\t\tif (totalBorrowValue \u003c totalBorrowPowerValue) {
\t\t\tborrowPowerValue = totalBorrowPowerValue.sub(totalBorrowValue);
\t\t}

\t\tunderCollateral = false;\t
\t\tif (totalBorrowValue \u003e liquidationThresholdValue) {
\t\t\tunderCollateral = true;
\t\t}
\t}

\t/// @notice Returns total borrow value of an account based on a given collateral 
\t/// @param account Account address
    /// @param collateral Address of the given collateral
    /// @return totalBorrowValue Accumulative borrowed values on the given collateral
\tfunction getAccountTotalBorrowValue (address account, address collateral)
\t\tpublic
\t\tview
\t\treturns (uint256 totalBorrowValue)
\t{
\t\tMarketData memory borrowData;
\t\taddress market;
\t\tuint256 totalDebt;
\t\tuint256 assetValue;
\t\t
\t\ttotalBorrowValue = 0;
\t\taddress[] memory marketsList = holdefiSettings.getMarketsList();
\t\tfor (uint256 i = 0 ; i \u003c marketsList.length ; i++) {
\t\t\tmarket = marketsList[i];
\t\t\t
\t\t\t(borrowData.balance, borrowData.interest,) = getAccountBorrow(account, market, collateral);
\t\t\ttotalDebt = borrowData.balance.add(borrowData.interest);

\t\t\tassetValue = holdefiPrices.getAssetValueFromAmount(market, totalDebt);
\t\t\ttotalBorrowValue = totalBorrowValue.add(assetValue);
\t\t}
\t}

\t/// @notice The collateral reserve amount for buying liquidated collateral
    /// @param collateral Address of the given collateral
    /// @return reserve Liquidation reserves for the given collateral
\tfunction getLiquidationReserve (address collateral) public view returns(uint256 reserve) {
\t\taddress market;
\t\tuint256 assetValue;
\t\tuint256 totalDebtValue = 0;

\t\taddress[] memory marketsList = holdefiSettings.getMarketsList();
\t\tfor (uint256 i = 0 ; i \u003c marketsList.length ; i++) {
\t\t\tmarket = marketsList[i];
\t\t\tassetValue = holdefiPrices.getAssetValueFromAmount(market, marketDebt[collateral][market]);
\t\t\ttotalDebtValue = totalDebtValue.add(assetValue); 
\t\t}

\t\tuint256 bonusRate = holdefiSettings.collateralAssets(collateral).bonusRate;
\t\tuint256 totalDebtCollateralValue = totalDebtValue.mul(bonusRate).div(rateDecimals);
\t\tuint256 liquidatedCollateralNeeded = holdefiPrices.getAssetAmountFromValue(
\t\t\tcollateral,
\t\t\ttotalDebtCollateralValue
\t\t);
\t\t
\t\treserve = 0;
\t\tuint256 totalLiquidatedCollateral = collateralAssets[collateral].totalLiquidatedCollateral;
\t\tif (totalLiquidatedCollateral \u003e liquidatedCollateralNeeded) {
\t\t\treserve = totalLiquidatedCollateral.sub(liquidatedCollateralNeeded);
\t\t}
\t}

\t/// @notice Returns the amount of discounted collateral can be bought in exchange for the amount of a given market
    /// @param market Address of the given market
    /// @param collateral Address of the given collateral
    /// @param marketAmount The amount of market should be paid
    /// @return collateralAmountWithDiscount Amount of discounted collateral can be bought
\tfunction getDiscountedCollateralAmount (address market, address collateral, uint256 marketAmount)
\t\tpublic
\t\tview
\t\treturns (uint256 collateralAmountWithDiscount)
\t{
\t\tuint256 marketValue = holdefiPrices.getAssetValueFromAmount(market, marketAmount);
\t\tuint256 bonusRate = holdefiSettings.collateralAssets(collateral).bonusRate;
\t\tuint256 collateralValue = marketValue.mul(bonusRate).div(rateDecimals);

\t\tcollateralAmountWithDiscount = holdefiPrices.getAssetAmountFromValue(collateral, collateralValue);
\t}

\t/// @notice Returns supply index and supply rate for a given market at current time
\t/// @dev newSupplyIndex = oldSupplyIndex + (deltaTime * supplyRate)
    /// @param market Address of the given market
    /// @return supplyIndex Supply index of the given market
    /// @return supplyRate Supply rate of the given market
\tfunction getCurrentSupplyIndex (address market)
\t\tpublic
\t\tview
\t\treturns (
\t\t\tuint256 supplyIndex,
\t\t\tuint256 supplyRate
\t\t)
\t{
\t\t(, uint256 supplyRateBase, uint256 promotionRate) = holdefiSettings.getInterests(market);
\t\tuint256 deltaTimeSupply = block.timestamp.sub(marketAssets[market].supplyIndexUpdateTime);

\t\tsupplyRate = supplyRateBase.add(promotionRate);
\t\tuint256 deltaTimeInterest = deltaTimeSupply.mul(supplyRate);
\t\tsupplyIndex = marketAssets[market].supplyIndex.add(deltaTimeInterest);
\t}

\t/// @notice Returns borrow index and borrow rate for the given market at current time
\t/// @dev newBorrowIndex = oldBorrowIndex + (deltaTime * borrowRate)
    /// @param market Address of the given market
    /// @return borrowIndex Borrow index of the given market
    /// @return borrowRate Borrow rate of the given market
\tfunction getCurrentBorrowIndex (address market)
\t\tpublic
\t\tview
\t\treturns (
\t\t\tuint256 borrowIndex,
\t\t\tuint256 borrowRate
\t\t)
\t{
\t\tborrowRate = holdefiSettings.marketAssets(market).borrowRate;
\t\tuint256 deltaTimeBorrow = block.timestamp.sub(marketAssets[market].borrowIndexUpdateTime);

\t\tuint256 deltaTimeInterest = deltaTimeBorrow.mul(borrowRate);
\t\tborrowIndex = marketAssets[market].borrowIndex.add(deltaTimeInterest);
\t}

\t/// @notice Returns promotion reserve for a given market at current time
\t/// @dev promotionReserveScaled is scaled by (secondsPerYear * rateDecimals)
\t/// @param market Address of the given market
    /// @return promotionReserveScaled Promotion reserve of the given market
\tfunction getPromotionReserve (address market)
\t\tpublic
\t\tview
\t\treturns (uint256 promotionReserveScaled)
\t{
\t\t(uint256 borrowRate, uint256 supplyRateBase,) = holdefiSettings.getInterests(market);
\t
\t\tuint256 allSupplyInterest = marketAssets[market].totalSupply.mul(supplyRateBase);
\t\tuint256 allBorrowInterest = marketAssets[market].totalBorrow.mul(borrowRate);

\t\tuint256 deltaTime = block.timestamp.sub(marketAssets[market].promotionReserveLastUpdateTime);
\t\tuint256 currentInterest = allBorrowInterest.sub(allSupplyInterest);
\t\tuint256 deltaTimeInterest = currentInterest.mul(deltaTime);
\t\tpromotionReserveScaled = marketAssets[market].promotionReserveScaled.add(deltaTimeInterest);
\t}

\t/// @notice Returns promotion debt for a given market at current time
\t/// @dev promotionDebtScaled is scaled by secondsPerYear * rateDecimals
\t/// @param market Address of the given market
    /// @return promotionDebtScaled Promotion debt of the given market
\tfunction getPromotionDebt (address market)
\t\tpublic
\t\tview
\t\treturns (uint256 promotionDebtScaled)
\t{
\t\tuint256 promotionRate = holdefiSettings.marketAssets(market).promotionRate;
\t\tpromotionDebtScaled = marketAssets[market].promotionDebtScaled;

\t\tif (promotionRate != 0) {
\t\t\tuint256 deltaTime = block.timestamp.sub(marketAssets[market].promotionDebtLastUpdateTime);
\t\t\tuint256 currentInterest = marketAssets[market].totalSupply.mul(promotionRate);
\t\t\tuint256 deltaTimeInterest = currentInterest.mul(deltaTime);
\t\t\tpromotionDebtScaled = promotionDebtScaled.add(deltaTimeInterest);
\t\t}
\t}

\t/// @notice Update a market supply index, promotion reserve, and promotion debt
\t/// @param market Address of the given market
\tfunction beforeChangeSupplyRate (address market) public {
\t\tupdateSupplyIndex(market);
\t\t
\t\tuint256 reserveScaled = getPromotionReserve(market);
\t\tuint256 debtScaled = getPromotionDebt(market);

    \tif (marketAssets[market].promotionDebtScaled != debtScaled) {
    \t\tif (debtScaled \u003e= reserveScaled) {
\t      \t\tholdefiSettings.resetPromotionRate(market);
\t      \t}

\t      \tmarketAssets[market].promotionDebtScaled = debtScaled;
\t      \tmarketAssets[market].promotionDebtLastUpdateTime = block.timestamp;
\t      \temit PromotionDebtUpdated(market, debtScaled);
    \t}

\t\tmarketAssets[market].promotionReserveScaled = reserveScaled;
    \tmarketAssets[market].promotionReserveLastUpdateTime = block.timestamp;
\t\temit PromotionReserveUpdated(market, reserveScaled);
\t}

\t/// @notice Update a market borrow index, supply index, promotion reserve, and promotion debt 
\t/// @param market Address of the given market
\tfunction beforeChangeBorrowRate (address market) external {
\t\tupdateBorrowIndex(market);
\t\tbeforeChangeSupplyRate(market);
\t}

\t/// @notice Returns maximum amount spender can withdraw from account supplies on a given market
\t/// @param account Supplier address
\t/// @param spender Spender address
\t/// @param market Address of the given market
\t/// @return res Maximum amount spender can withdraw from account supplies on a given market
\tfunction getAccountWithdrawSupplyAllowance (address account, address spender, address market)
\t\texternal 
\t\tview
\t\treturns (uint256 res)
\t{
\t\tres = supplies[account][market].allowance[spender];
\t}

\t/// @notice Returns maximum amount spender can withdraw from account balance on a given collateral
\t/// @param account Account address
\t/// @param spender Spender address
\t/// @param collateral Address of the given collateral
\t/// @return res Maximum amount spender can withdraw from account balance on a given collateral
\tfunction getAccountWithdrawCollateralAllowance (
\t\taddress account, 
\t\taddress spender, 
\t\taddress collateral
\t)
\t\texternal 
\t\tview
\t\treturns (uint256 res)
\t{
\t\tres = collaterals[account][collateral].allowance[spender];
\t}

\t/// @notice Returns maximum amount spender can withdraw from account borrows on a given market based on a given collteral
\t/// @param account Borrower address
\t/// @param spender Spender address
\t/// @param market Address of the given market
\t/// @param collateral Address of the given collateral
\t/// @return res Maximum amount spender can withdraw from account borrows on a given market based on a given collteral
\tfunction getAccountBorrowAllowance (
\t\taddress account, 
\t\taddress spender, 
\t\taddress market, 
\t\taddress collateral
\t)
\t\texternal 
\t\tview
\t\treturns (uint256 res)
\t{
\t\tres = borrows[account][collateral][market].allowance[spender];
\t}

\t/// @notice Deposit ERC20 asset for supplying
\t/// @param market Address of the given market
\t/// @param amount The amount of asset supplier supplies
\t/// @param referralCode A unique code used as an identifier of the referrer
\tfunction supply(address market, uint256 amount, uint16 referralCode)
\t\texternal
\t\tisNotETHAddress(market)
\t{
\t\tsupplyInternal(msg.sender, market, amount, referralCode);
\t}

\t/// @notice Deposit ETH for supplying
\t/// @notice msg.value The amount of asset supplier supplies
\t/// @param referralCode A unique code used as an identifier of the referrer
\tfunction supply(uint16 referralCode) external payable {\t\t
\t\tsupplyInternal(msg.sender, ethAddress, msg.value, referralCode);
\t}

\t/// @notice Sender supplies ERC20 asset belonging to the supplier
\t/// @param account Address of the supplier
\t/// @param market Address of the given market
\t/// @param amount The amount of asset sender deposits
\t/// @param referralCode A unique code used as an identifier of the referrer
\tfunction supplyBehalf(address account, address market, uint256 amount, uint16 referralCode)
\t\texternal
\t\tisNotETHAddress(market)
\t{
\t\tsupplyInternal(account, market, amount, referralCode);
\t}

\t/// @notice Sender supplies ETH belonging to the supplier
\t/// @notice msg.value The amount of ETH sender deposits
\t/// @param account Address of the supplier
\t/// @param referralCode A unique code used as an identifier of the referrer
\tfunction supplyBehalf(address account, uint16 referralCode) 
\t\texternal
\t\tpayable
\t{
\t\tsupplyInternal(account, ethAddress, msg.value, referralCode);
\t}

\t/// @notice Sender approves the account to withdraw supply
\t/// @param account Address of the spender
\t/// @param market Address of the given market
\t/// @param amount The amount is allowed to be withdrawn
\tfunction approveWithdrawSupply(address account, address market, uint256 amount)
\t\texternal
\t\taccountIsValid(account)
\t\tmarketIsActive(market)
\t{
\t\tsupplies[msg.sender][market].allowance[account] = amount;
\t}

\t/// @notice Withdraw supply of a given market
\t/// @param market Address of the given market
\t/// @param amount The amount will be withdrawn from the market
\tfunction withdrawSupply(address market, uint256 amount)
\t\texternal
\t{
\t\twithdrawSupplyInternal(msg.sender, market, amount);
\t}

\t/// @notice Sender withdraws supply belonging to the supplier
\t/// @param account Address of the supplier
\t/// @param market Address of the given market
\t/// @param amount The amount will be withdrawn from the market
\tfunction withdrawSupplyBehalf(address account, address market, uint256 amount) external {
\t\tsupplies[account][market].allowance[msg.sender] = supplies[account][market].allowance[msg.sender].sub(amount, \u0027E14\u0027);
\t\twithdrawSupplyInternal(account, market, amount);
\t}

\t/// @notice Deposit ERC20 asset as collateral
\t/// @param collateral Address of the given collateral
\t/// @param amount The amount will be collateralized
\tfunction collateralize (address collateral, uint256 amount)
\t\texternal
\t\tisNotETHAddress(collateral)
\t{
\t\tcollateralizeInternal(msg.sender, collateral, amount);
\t}

\t/// @notice Deposit ETH as collateral
\t/// @notice msg.value The amount of ETH will be collateralized
\tfunction collateralize () external payable {
\t\tcollateralizeInternal(msg.sender, ethAddress, msg.value);
\t}

\t/// @notice Sender deposits ERC20 asset as collateral belonging to another user
\t/// @param account Address of the user
\t/// @param collateral Address of the given collateral
\t/// @param amount The amount of asset sender deposits
\tfunction collateralizeBehalf (address account, address collateral, uint256 amount)
\t\texternal
\t\tisNotETHAddress(collateral)
\t{
\t\tcollateralizeInternal(account, collateral, amount);
\t}

\t/// @notice Sender deposits ETH as collateral belonging to another user
\t/// @notice msg.value The amount of ETH sender deposits
\t/// @param account Address of the user
\tfunction collateralizeBehalf (address account) external payable {
\t\tcollateralizeInternal(account, ethAddress, msg.value);
\t}

\t/// @notice Sender approves the account to withdraw the collateral
\t/// @param account Address of the spender
\t/// @param collateral Address of the given collateral
\t/// @param amount The amount is allowed to be withdrawn
\tfunction approveWithdrawCollateral (address account, address collateral, uint256 amount)
\t\texternal
\t\taccountIsValid(account)
\t\tcollateralIsActive(collateral)
\t{
\t\tcollaterals[msg.sender][collateral].allowance[account] = amount;
\t}

\t/// @notice Withdraw the collateral
\t/// @param collateral Address of the given collateral
\t/// @param amount The amount will be withdrawn from the collateral
\tfunction withdrawCollateral (address collateral, uint256 amount)
\t\texternal
\t{
\t\twithdrawCollateralInternal(msg.sender, collateral, amount);
\t}

\t/// @notice Sender withdraws the collateral belonging to another user
\t/// @param account Address of the user
\t/// @param collateral Address of the given collateral
\t/// @param amount The amount will be withdrawn from the collateral
\tfunction withdrawCollateralBehalf (address account, address collateral, uint256 amount)
\t\texternal
\t{
\t\tcollaterals[account][collateral].allowance[msg.sender] = 
\t\t\tcollaterals[account][collateral].allowance[msg.sender].sub(amount, \u0027E14\u0027);
\t\twithdrawCollateralInternal(account, collateral, amount);
\t}

\t/// @notice Sender approves the account to borrow a given market based on given collateral
\t/// @param account Address of the spender
\t/// @param market Address of the given market
\t/// @param collateral Address of the given collateral
\t/// @param amount The amount is allowed to be withdrawn
\tfunction approveBorrow (address account, address market, address collateral, uint256 amount)
\t\texternal
\t\taccountIsValid(account)
\t\tmarketIsActive(market)
\t{
\t\tborrows[msg.sender][collateral][market].allowance[account] = amount;
\t}

\t/// @notice Borrow an asset
\t/// @param market Address of the given market
\t/// @param collateral Address of the given collateral
\t/// @param amount The amount of the given market will be borrowed
\t/// @param referralCode A unique code used as an identifier of the referrer
\tfunction borrow (address market, address collateral, uint256 amount, uint16 referralCode)
\t\texternal
\t{
\t\tborrowInternal(msg.sender, market, collateral, amount, referralCode);
\t}

\t/// @notice Sender borrows an asset belonging to the borrower
\t/// @param account Address of the borrower
\t/// @param market Address of the given market
\t/// @param collateral Address of the given collateral
\t/// @param amount The amount will be borrowed
\t/// @param referralCode A unique code used as an identifier of the referrer
\tfunction borrowBehalf (address account, address market, address collateral, uint256 amount, uint16 referralCode)
\t\texternal
\t{
\t\tborrows[account][collateral][market].allowance[msg.sender] = 
\t\t\tborrows[account][collateral][market].allowance[msg.sender].sub(amount, \u0027E14\u0027);
\t\tborrowInternal(account, market, collateral, amount, referralCode);
\t}

\t/// @notice Repay an ERC20 asset based on a given collateral
\t/// @param market Address of the given market
\t/// @param collateral Address of the given collateral
\t/// @param amount The amount of the market will be Repaid
\tfunction repayBorrow (address market, address collateral, uint256 amount)
\t\texternal
\t\tisNotETHAddress(market)
\t{
\t\trepayBorrowInternal(msg.sender, market, collateral, amount);
\t}

\t/// @notice Repay an ETH based on a given collateral
\t/// @notice msg.value The amount of ETH will be repaid
\t/// @param collateral Address of the given collateral
\tfunction repayBorrow (address collateral) external payable {\t\t
\t\trepayBorrowInternal(msg.sender, ethAddress, collateral, msg.value);
\t}

\t/// @notice Sender repays an ERC20 asset based on a given collateral belonging to the borrower
\t/// @param account Address of the borrower
\t/// @param market Address of the given market
\t/// @param collateral Address of the given collateral
\t/// @param amount The amount of the market will be repaid
\tfunction repayBorrowBehalf (address account, address market, address collateral, uint256 amount)
\t\texternal
\t\tisNotETHAddress(market)
\t{
\t\trepayBorrowInternal(account, market, collateral, amount);
\t}

\t/// @notice Sender repays an ETH based on a given collateral belonging to the borrower
\t/// @notice msg.value The amount of ETH sender repays
\t/// @param account Address of the borrower
\t/// @param collateral Address of the given collateral
\tfunction repayBorrowBehalf (address account, address collateral)
\t\texternal
\t\tpayable
\t{\t\t
\t\trepayBorrowInternal(account, ethAddress, collateral, msg.value);
\t}

\t/// @notice Liquidate borrower\u0027s collateral
\t/// @param borrower Address of the borrower who should be liquidated
\t/// @param market Address of the given market
\t/// @param collateral Address of the given collateral
\tfunction liquidateBorrowerCollateral (address borrower, address market, address collateral)
\t\texternal
\t\twhenNotPaused(\"liquidateBorrowerCollateral\")
\t{
\t\tMarketData memory borrowData;
\t\t(borrowData.balance, borrowData.interest,) = getAccountBorrow(borrower, market, collateral);
\t\trequire(borrowData.balance \u003e 0, \"E05\");

\t\t(uint256 collateralBalance, uint256 timeSinceLastActivity,,, bool underCollateral) = 
\t\t\tgetAccountCollateral(borrower, collateral);
\t\trequire (underCollateral || (timeSinceLastActivity \u003e secondsPerYear),
\t\t\t\"E06\"
\t\t);

\t\tuint256 totalBorrowedBalance = borrowData.balance.add(borrowData.interest);
\t\tuint256 totalBorrowedBalanceValue = holdefiPrices.getAssetValueFromAmount(market, totalBorrowedBalance);
\t\t
\t\tuint256 liquidatedCollateralValue = totalBorrowedBalanceValue
\t\t\t.mul(holdefiSettings.collateralAssets(collateral).penaltyRate)
\t\t\t.div(rateDecimals);

\t\tuint256 liquidatedCollateral =
\t\t\tholdefiPrices.getAssetAmountFromValue(collateral, liquidatedCollateralValue);

\t\tif (liquidatedCollateral \u003e collateralBalance) {
\t\t\tliquidatedCollateral = collateralBalance;
\t\t}

\t\tcollaterals[borrower][collateral].balance = collateralBalance.sub(liquidatedCollateral);
\t\tcollateralAssets[collateral].totalCollateral =
\t\t\tcollateralAssets[collateral].totalCollateral.sub(liquidatedCollateral);
\t\tcollateralAssets[collateral].totalLiquidatedCollateral =
\t\t\tcollateralAssets[collateral].totalLiquidatedCollateral.add(liquidatedCollateral);

\t\tdelete borrows[borrower][collateral][market];
\t\tbeforeChangeSupplyRate(market);
\t\tmarketAssets[market].totalBorrow = marketAssets[market].totalBorrow.sub(borrowData.balance);
\t\tmarketDebt[collateral][market] = marketDebt[collateral][market].add(totalBorrowedBalance);

\t\temit CollateralLiquidated(borrower, market, collateral, totalBorrowedBalance, liquidatedCollateral);\t
\t}

\t/// @notice Buy collateral in exchange for ERC20 asset
\t/// @param market Address of the market asset should be paid to buy collateral
\t/// @param collateral Address of the liquidated collateral
\t/// @param marketAmount The amount of the given market will be paid
\tfunction buyLiquidatedCollateral (address market, address collateral, uint256 marketAmount)
\t\texternal
\t\tisNotETHAddress(market)
\t{
\t\tbuyLiquidatedCollateralInternal(market, collateral, marketAmount);
\t}

\t/// @notice Buy collateral in exchange for ETH
\t/// @notice msg.value The amount of the given market that will be paid
\t/// @param collateral Address of the liquidated collateral
\tfunction buyLiquidatedCollateral (address collateral) external payable {
\t\tbuyLiquidatedCollateralInternal(ethAddress, collateral, msg.value);
\t}

\t/// @notice Deposit ERC20 asset as liquidation reserve
\t/// @param collateral Address of the given collateral
\t/// @param amount The amount that will be deposited
\tfunction depositLiquidationReserve(address collateral, uint256 amount)
\t\texternal
\t\tisNotETHAddress(collateral)
\t{
\t\tdepositLiquidationReserveInternal(collateral, amount);
\t}

\t/// @notice Deposit ETH asset as liquidation reserve
\t/// @notice msg.value The amount of ETH that will be deposited
\tfunction depositLiquidationReserve() external payable {
\t\tdepositLiquidationReserveInternal(ethAddress, msg.value);
\t}

\t/// @notice Withdraw liquidation reserve only by the owner
\t/// @param collateral Address of the given collateral
\t/// @param amount The amount that will be withdrawn
\tfunction withdrawLiquidationReserve (address collateral, uint256 amount) external onlyOwner {
\t\tuint256 maxWithdraw = getLiquidationReserve(collateral);
\t\tuint256 transferAmount = amount;
\t\t
\t\tif (transferAmount \u003e maxWithdraw){
\t\t\ttransferAmount = maxWithdraw;
\t\t}

\t\tcollateralAssets[collateral].totalLiquidatedCollateral =
\t\t\tcollateralAssets[collateral].totalLiquidatedCollateral.sub(transferAmount);
\t\tholdefiCollaterals.withdraw(collateral, msg.sender, transferAmount);

\t\temit LiquidationReserveWithdrawn(collateral, transferAmount);
\t}

\t/// @notice Deposit ERC20 asset as promotion reserve
\t/// @param market Address of the given market
\t/// @param amount The amount that will be deposited
\tfunction depositPromotionReserve (address market, uint256 amount)
\t\texternal
\t\tisNotETHAddress(market)
\t{
\t\tdepositPromotionReserveInternal(market, amount);
\t}

\t/// @notice Deposit ETH as promotion reserve
\t/// @notice msg.value The amount of ETH that will be deposited
\tfunction depositPromotionReserve () external payable {
\t\tdepositPromotionReserveInternal(ethAddress, msg.value);
\t}

\t/// @notice Withdraw promotion reserve only by the owner
\t/// @param market Address of the given market
\t/// @param amount The amount that will be withdrawn
\tfunction withdrawPromotionReserve (address market, uint256 amount) external onlyOwner {
\t    uint256 reserveScaled = getPromotionReserve(market);
\t    uint256 debtScaled = getPromotionDebt(market);

\t    uint256 amountScaled = amount.mul(secondsPerYear).mul(rateDecimals);
\t    require (reserveScaled \u003e amountScaled.add(debtScaled), \"E07\");

\t    marketAssets[market].promotionReserveScaled = reserveScaled.sub(amountScaled);
\t    marketAssets[market].promotionReserveLastUpdateTime = block.timestamp;

\t    transferFromHoldefi(msg.sender, market, amount);

\t    emit PromotionReserveWithdrawn(market, amount, marketAssets[market].promotionReserveScaled);
\t }


\t/// @notice Set Holdefi prices contract only by the owner
\t/// @param newHoldefiPrices Address of the new Holdefi prices contract
\tfunction setHoldefiPricesContract (HoldefiPricesInterface newHoldefiPrices) external onlyOwner {
\t\temit HoldefiPricesContractChanged(address(newHoldefiPrices), address(holdefiPrices));
\t\tholdefiPrices = newHoldefiPrices;
\t}

\t/// @notice Promotion reserve and debt settlement
\t/// @param market Address of the given market
\tfunction reserveSettlement (address market) external {
\t\trequire(msg.sender == address(holdefiSettings), \"E15\");

\t\tupdateSupplyIndex(market);
\t\tuint256 reserveScaled = getPromotionReserve(market);
\t\tuint256 debtScaled = getPromotionDebt(market);

\t\tmarketAssets[market].promotionReserveScaled = reserveScaled.sub(debtScaled, \"E13\");
\t\tmarketAssets[market].promotionDebtScaled = 0;

\t\tmarketAssets[market].promotionReserveLastUpdateTime = block.timestamp;
\t\tmarketAssets[market].promotionDebtLastUpdateTime = block.timestamp;

\t\temit PromotionReserveUpdated(market, marketAssets[market].promotionReserveScaled);
\t\temit PromotionDebtUpdated(market, 0);
\t}

\t/// @notice Update supply index of a market
\t/// @param market Address of the given market
\tfunction updateSupplyIndex (address market) internal {
\t\t(uint256 currentSupplyIndex, uint256 supplyRate) = getCurrentSupplyIndex(market);

\t\tmarketAssets[market].supplyIndex = currentSupplyIndex;
\t\tmarketAssets[market].supplyIndexUpdateTime = block.timestamp;

\t\temit UpdateSupplyIndex(market, currentSupplyIndex, supplyRate);
\t}

\t/// @notice Update borrow index of a market
\t/// @param market Address of the given market
\tfunction updateBorrowIndex (address market) internal {
\t\t(uint256 currentBorrowIndex, uint256 borrowRate) = getCurrentBorrowIndex(market);

\t\tmarketAssets[market].borrowIndex = currentBorrowIndex;
\t\tmarketAssets[market].borrowIndexUpdateTime = block.timestamp;

\t\temit UpdateBorrowIndex(market, currentBorrowIndex, borrowRate);
\t}

\t/// @notice Transfer ETH or ERC20 asset from this contract
\tfunction transferFromHoldefi(address receiver, address asset, uint256 amount) internal {
\t\tif (asset == ethAddress){
\t\t\t(bool success, ) = receiver.call{value:amount}(\"\");
\t\t\trequire (success, \"E08\");
\t\t}
\t\telse {
\t\t\tIERC20 token = IERC20(asset);
\t\t\ttoken.safeTransfer(receiver, amount);
\t\t}
\t}

\t/// @notice Transfer ERC20 asset from msg.sender
\tfunction transferFromSender(address receiver, address asset, uint256 amount) internal returns(uint256 transferAmount) {
\t\ttransferAmount = amount;
\t\tif (asset != ethAddress) {
\t\t\tIERC20 token = IERC20(asset);
\t\t\tuint256 oldBalance = token.balanceOf(receiver);
\t\t\ttoken.safeTransferFrom(msg.sender, receiver, amount);
\t\t\ttransferAmount = token.balanceOf(receiver).sub(oldBalance);
\t\t}
\t}

\t/// @notice Perform supply operation
\tfunction supplyInternal(address account, address market, uint256 amount, uint16 referralCode)
\t\tinternal
\t\tnonReentrant
\t\twhenNotPaused(\"supply\")
\t\tmarketIsActive(market)
\t{
\t\tuint256 transferAmount = transferFromSender(address(this), market, amount);

\t\tMarketData memory supplyData;
\t\t(supplyData.balance, supplyData.interest, supplyData.currentIndex) = getAccountSupply(account, market);
\t\t
\t\tsupplyData.balance = supplyData.balance.add(transferAmount);
\t\tsupplies[account][market].balance = supplyData.balance;
\t\tsupplies[account][market].accumulatedInterest = supplyData.interest;
\t\tsupplies[account][market].lastInterestIndex = supplyData.currentIndex;

\t\tbeforeChangeSupplyRate(market);
\t\tmarketAssets[market].totalSupply = marketAssets[market].totalSupply.add(transferAmount);

\t\temit Supply(
\t\t\tmsg.sender,
\t\t\taccount,
\t\t\tmarket,
\t\t\ttransferAmount,
\t\t\tsupplyData.balance,
\t\t\tsupplyData.interest,
\t\t\tsupplyData.currentIndex,
\t\t\treferralCode
\t\t);
\t}

\t/// @notice Perform withdraw supply operation
\tfunction withdrawSupplyInternal (address account, address market, uint256 amount) 
\t\tinternal
\t\tnonReentrant
\t\twhenNotPaused(\"withdrawSupply\")
\t{
\t\tMarketData memory supplyData;
\t\t(supplyData.balance, supplyData.interest, supplyData.currentIndex) = getAccountSupply(account, market);
\t\tuint256 totalSuppliedBalance = supplyData.balance.add(supplyData.interest);
\t\trequire (totalSuppliedBalance != 0, \"E09\");

\t\tuint256 transferAmount = amount;
\t\tif (transferAmount \u003e totalSuppliedBalance){
\t\t\ttransferAmount = totalSuppliedBalance;
\t\t}

\t\tif (transferAmount \u003c= supplyData.interest) {
\t\t\tsupplyData.interest = supplyData.interest.sub(transferAmount);
\t\t}
\t\telse {
\t\t\tuint256 remaining = transferAmount.sub(supplyData.interest);
\t\t\tsupplyData.interest = 0;
\t\t\tsupplyData.balance = supplyData.balance.sub(remaining);

\t\t\tbeforeChangeSupplyRate(market);
\t\t\tmarketAssets[market].totalSupply = marketAssets[market].totalSupply.sub(remaining);\t
\t\t}

\t\tsupplies[account][market].balance = supplyData.balance;
\t\tsupplies[account][market].accumulatedInterest = supplyData.interest;
\t\tsupplies[account][market].lastInterestIndex = supplyData.currentIndex;

\t\ttransferFromHoldefi(msg.sender, market, transferAmount);
\t
\t\temit WithdrawSupply(
\t\t\tmsg.sender,
\t\t\taccount,
\t\t\tmarket,
\t\t\ttransferAmount,
\t\t\tsupplyData.balance,
\t\t\tsupplyData.interest,
\t\t\tsupplyData.currentIndex
\t\t);
\t}

\t/// @notice Perform collateralize operation
\tfunction collateralizeInternal (address account, address collateral, uint256 amount)
\t\tinternal
\t\tnonReentrant
\t\twhenNotPaused(\"collateralize\")
\t\tcollateralIsActive(collateral)
\t{
\t\tuint256 transferAmount = transferFromSender(address(holdefiCollaterals), collateral, amount);
\t\tif (collateral == ethAddress) {
\t\t\ttransferFromHoldefi(address(holdefiCollaterals), collateral, amount);
\t\t}

\t\tuint256 balance = collaterals[account][collateral].balance.add(transferAmount);
\t\tcollaterals[account][collateral].balance = balance;
\t\tcollaterals[account][collateral].lastUpdateTime = block.timestamp;

\t\tcollateralAssets[collateral].totalCollateral = collateralAssets[collateral].totalCollateral.add(transferAmount);\t
\t\t
\t\temit Collateralize(msg.sender, account, collateral, transferAmount, balance);
\t}

\t/// @notice Perform withdraw collateral operation
\tfunction withdrawCollateralInternal (address account, address collateral, uint256 amount) 
\t\tinternal
\t\tnonReentrant
\t\twhenNotPaused(\"withdrawCollateral\")
\t{
\t\t(uint256 balance,, uint256 borrowPowerValue, uint256 totalBorrowValue,) =
\t\t\tgetAccountCollateral(account, collateral);

\t\trequire (borrowPowerValue != 0, \"E10\");

\t\tuint256 collateralNedeed = 0;
\t\tif (totalBorrowValue != 0) {
\t\t\tuint256 valueToLoanRate = holdefiSettings.collateralAssets(collateral).valueToLoanRate;
\t\t\tuint256 totalCollateralValue = totalBorrowValue.mul(valueToLoanRate).div(rateDecimals);
\t\t\tcollateralNedeed = holdefiPrices.getAssetAmountFromValue(collateral, totalCollateralValue);
\t\t}

\t\tuint256 maxWithdraw = balance.sub(collateralNedeed);
\t\tuint256 transferAmount = amount;
\t\tif (transferAmount \u003e maxWithdraw){
\t\t\ttransferAmount = maxWithdraw;
\t\t}
\t\tbalance = balance.sub(transferAmount);
\t\tcollaterals[account][collateral].balance = balance;
\t\tcollaterals[account][collateral].lastUpdateTime = block.timestamp;

\t\tcollateralAssets[collateral].totalCollateral =
\t\t\tcollateralAssets[collateral].totalCollateral.sub(transferAmount);

\t\tholdefiCollaterals.withdraw(collateral, msg.sender, transferAmount);

\t\temit WithdrawCollateral(msg.sender, account, collateral, transferAmount, balance);
\t}

\t/// @notice Perform borrow operation
\tfunction borrowInternal (address account, address market, address collateral, uint256 amount, uint16 referralCode)
\t\tinternal
\t\tnonReentrant
\t\twhenNotPaused(\"borrow\")
\t\tmarketIsActive(market)
\t\tcollateralIsActive(collateral)
\t{
\t\trequire (amount \u003c= (marketAssets[market].totalSupply.sub(marketAssets[market].totalBorrow)), \"E11\");

\t\t(,, uint256 borrowPowerValue,,) = getAccountCollateral(account, collateral);
\t\tuint256 assetToBorrowValue = holdefiPrices.getAssetValueFromAmount(market, amount);
\t\trequire (borrowPowerValue \u003e= assetToBorrowValue, \"E12\");

\t\tMarketData memory borrowData;
\t\t(borrowData.balance, borrowData.interest, borrowData.currentIndex) = getAccountBorrow(account, market, collateral);
\t\t
\t\tborrowData.balance = borrowData.balance.add(amount);
\t\tborrows[account][collateral][market].balance = borrowData.balance;
\t\tborrows[account][collateral][market].accumulatedInterest = borrowData.interest;
\t\tborrows[account][collateral][market].lastInterestIndex = borrowData.currentIndex;
\t\tcollaterals[account][collateral].lastUpdateTime = block.timestamp;

\t\tbeforeChangeSupplyRate(market);
\t\tmarketAssets[market].totalBorrow = marketAssets[market].totalBorrow.add(amount);

\t\ttransferFromHoldefi(msg.sender, market, amount);

\t\temit Borrow(
\t\t\tmsg.sender, 
\t\t\taccount,
\t\t\tmarket,
\t\t\tcollateral,
\t\t\tamount,
\t\t\tborrowData.balance,
\t\t\tborrowData.interest,
\t\t\tborrowData.currentIndex,
\t\t\treferralCode
\t\t);
\t}

\t/// @notice Perform repay borrow operation
\tfunction repayBorrowInternal (address account, address market, address collateral, uint256 amount)
\t\tinternal
\t\tnonReentrant
\t\twhenNotPaused(\"repayBorrow\")
\t{
\t\tMarketData memory borrowData;
\t\t(borrowData.balance, borrowData.interest, borrowData.currentIndex) =
\t\t\tgetAccountBorrow(account, market, collateral);

\t\tuint256 totalBorrowedBalance = borrowData.balance.add(borrowData.interest);
\t\trequire (totalBorrowedBalance != 0, \"E09\");

\t\tuint256 transferAmount = transferFromSender(address(this), market, amount);
\t\tuint256 extra = 0;
\t\tif (transferAmount \u003e totalBorrowedBalance) {
\t\t\textra = transferAmount.sub(totalBorrowedBalance);
\t\t\ttransferAmount = totalBorrowedBalance;
\t\t}

\t\tif (transferAmount \u003c= borrowData.interest) {
\t\t\tborrowData.interest = borrowData.interest.sub(transferAmount);
\t\t}
\t\telse {
\t\t\tuint256 remaining = transferAmount.sub(borrowData.interest);
\t\t\tborrowData.interest = 0;
\t\t\tborrowData.balance = borrowData.balance.sub(remaining);

\t\t\tbeforeChangeSupplyRate(market);
\t\t\tmarketAssets[market].totalBorrow = marketAssets[market].totalBorrow.sub(remaining);\t
\t\t}
\t\tborrows[account][collateral][market].balance = borrowData.balance;
\t\tborrows[account][collateral][market].accumulatedInterest = borrowData.interest;
\t\tborrows[account][collateral][market].lastInterestIndex = borrowData.currentIndex;
\t\tcollaterals[account][collateral].lastUpdateTime = block.timestamp;

\t\tif (extra \u003e 0) {
\t\t\ttransferFromHoldefi(msg.sender, market, extra);
\t\t}
\t\t
\t\temit RepayBorrow (
\t\t\tmsg.sender,
\t\t\taccount,
\t\t\tmarket,
\t\t\tcollateral,
\t\t\ttransferAmount,
\t\t\tborrowData.balance,
\t\t\tborrowData.interest,
\t\t\tborrowData.currentIndex
\t\t);
\t}

\t/// @notice Perform buy liquidated collateral operation
\tfunction buyLiquidatedCollateralInternal (address market, address collateral, uint256 marketAmount)
\t\tinternal
\t\tnonReentrant
\t\twhenNotPaused(\"buyLiquidatedCollateral\")
\t{
\t\tuint256 transferAmount = transferFromSender(address(this), market, marketAmount);
\t\tmarketDebt[collateral][market] = marketDebt[collateral][market].sub(transferAmount, \u0027E17\u0027);

\t\tuint256 collateralAmountWithDiscount =
\t\t\tgetDiscountedCollateralAmount(market, collateral, transferAmount);\t\t
\t\tcollateralAssets[collateral].totalLiquidatedCollateral = 
\t\t\tcollateralAssets[collateral].totalLiquidatedCollateral.sub(collateralAmountWithDiscount, \u0027E16\u0027);
\t\t
\t\tholdefiCollaterals.withdraw(collateral, msg.sender, collateralAmountWithDiscount);

\t\temit BuyLiquidatedCollateral(market, collateral, transferAmount, collateralAmountWithDiscount);
\t}

\t/// @notice Perform deposit promotion reserve operation
\tfunction depositPromotionReserveInternal (address market, uint256 amount)
\t\tinternal
\t\tnonReentrant
\t\twhenNotPaused(\"depositPromotionReserve\")
\t\tmarketIsActive(market)
\t{
\t\tuint256 transferAmount = transferFromSender(address(this), market, amount);

\t\tuint256 amountScaled = transferAmount.mul(secondsPerYear).mul(rateDecimals);

\t\tmarketAssets[market].promotionReserveScaled = 
\t\t\tmarketAssets[market].promotionReserveScaled.add(amountScaled);

\t\temit PromotionReserveDeposited(market, transferAmount, marketAssets[market].promotionReserveScaled);
\t}

\t/// @notice Perform deposit liquidation reserve operation
\tfunction depositLiquidationReserveInternal (address collateral, uint256 amount)
\t\tinternal
\t\tnonReentrant
\t\twhenNotPaused(\"depositLiquidationReserve\")
\t\tcollateralIsActive(collateral)
\t{
\t\tuint256 transferAmount = transferFromSender(address(holdefiCollaterals), collateral, amount);
\t\tif (collateral == ethAddress) {
\t\t\ttransferFromHoldefi(address(holdefiCollaterals), collateral, amount);
\t\t}

\t\tcollateralAssets[collateral].totalLiquidatedCollateral =
\t\t\tcollateralAssets[collateral].totalLiquidatedCollateral.add(transferAmount);
\t\t
\t\temit LiquidationReserveDeposited(collateral, transferAmount);
\t}
}"},"HoldefiCollaterals.sol":{"content":"// SPDX-License-Identifier: UNLICENSED
pragma solidity 0.6.12;

import \"./SafeERC20.sol\";


/// @title HoldefiCollaterals
/// @author Holdefi Team
/// @notice Collaterals is held by this contract
/// @dev The address of ETH asset considered as 0xEeeeeEeeeEeEeeEeEeEeeEEEeeeeEeeeeeeeEEeE
/// @dev Error codes description: 
/// \tCE01: Sender should be Holdefi contract
/// \tCE02: Cannot transfer
contract HoldefiCollaterals {

\tusing SafeERC20 for IERC20;

\taddress constant private ethAddress = 0xEeeeeEeeeEeEeeEeEeEeeEEEeeeeEeeeeeeeEEeE;

\taddress public holdefiContract;

\t/// @dev Initializes the main Holdefi contract address
\tconstructor() public {
\t\tholdefiContract = msg.sender;
\t}

\t/// @notice Modifier to check that only Holdefi contract interacts with the function
    modifier onlyHoldefiContract() {
        require (msg.sender == holdefiContract, \"CE01\");
        _;
    }

\t/// @notice Only Holdefi contract can send ETH to this contract
    receive() external payable onlyHoldefiContract {
\t}

\t/// @notice Holdefi contract withdraws collateral from this contract to recipient account
\t/// @param collateral Address of the given collateral
\t/// @param recipient Address of the recipient
\t/// @param amount Amount to be withdrawn
\tfunction withdraw (address collateral, address recipient, uint256 amount)
\t\texternal
\t\tonlyHoldefiContract
\t{
\t\tif (collateral == ethAddress){
\t\t\t(bool success, ) = recipient.call{value:amount}(\"\");
\t\t\trequire (success, \"CE02\");
\t\t}
\t\telse {
\t\t\tIERC20 token = IERC20(collateral);
\t\t\ttoken.safeTransfer(recipient, amount);
\t\t}
\t}
}"},"HoldefiOwnable.sol":{"content":"// SPDX-License-Identifier: UNLICENSED
pragma solidity 0.6.12;

/// @title HoldefiOwnable
/// @author Holdefi Team
/// @notice Taking ideas from Open Zeppelin\u0027s Ownable contract
/// @dev Contract module which provides a basic access control mechanism, where
/// there is an account (an owner) that can be granted exclusive access to
/// specific functions.
///
/// By default, the owner account will be the one that deploys the contract. This
/// can later be changed with {transferOwnership}.
///
/// This module is used through inheritance. It will make available the modifier
/// `onlyOwner`, which can be applied to your functions to restrict their use to
/// the owner.
/// @dev Error codes description: 
///     OE01: Sender should be the owner
///     OE02: New owner can not be zero address
///     OE03: Pending owner is empty
///     OE04: Pending owner is not same as the `msg.sender`
contract HoldefiOwnable {
    address public owner;
    address public pendingOwner;

    /// @notice Event emitted when an ownership transfer request is recieved
    event OwnershipTransferRequested(address newPendingOwner);

    /// @notice Event emitted when an ownership transfer request is accepted by the pending owner
    event OwnershipTransferred(address newOwner, address oldOwner);

    /// @notice Initializes the contract owner
    constructor () public {
        owner = msg.sender;
        emit OwnershipTransferred(owner, address(0));
    }

    /// @notice Throws if called by any account other than the owner
    modifier onlyOwner() {
        require(msg.sender == owner, \"OE01\");
        _;
    }

    /// @notice Transfers ownership of the contract to a new owner
    /// @dev Can only be called by the current owner
    /// @param newOwner Address of new owner
    function transferOwnership(address newOwner) external onlyOwner {
        require(newOwner != address(0), \"OE02\");
        pendingOwner = newOwner;

        emit OwnershipTransferRequested(newOwner);
    }

    /// @notice Pending owner accepts ownership of the contract
    /// @dev Only Pending owner can call this function
    function acceptTransferOwnership () external {
        require (pendingOwner != address(0), \"OE03\");
        require (pendingOwner == msg.sender, \"OE04\");
        
        emit OwnershipTransferred(pendingOwner, owner);
        owner = pendingOwner;
        pendingOwner = address(0);
    }
}"},"HoldefiPausableOwnable.sol":{"content":"// SPDX-License-Identifier: UNLICENSED
pragma solidity 0.6.12;
pragma experimental ABIEncoderV2;

import \"./HoldefiOwnable.sol\";

/// @title HoldefiPausableOwnable
/// @author Holdefi Team
/// @notice Taking ideas from Open Zeppelin\u0027s Pausable contract
/// @dev Base contract which allows children to implement an emergency stop mechanism.
/// @dev Error codes description: 
///     POE01: Sender should be the owner or the pauser
///     POE02: Operation is paused
///     POE03: Operation is unpaused
///     POE04: Operation is not valid
///     POE05: Duration is not in the allowed range
///     POE06: Lists are not equal in length
contract HoldefiPausableOwnable is HoldefiOwnable {

    uint256 constant private maxPauseDuration = 2592000;     //seconds per month

    struct Operation {
        bool isValid;
        uint256 pauseEndTime;
    }

    /// @notice Pauser can pause operations but can\u0027t unpause them
    address public pauser;

    mapping(string =\u003e Operation) public paused;

    /// @notice Event emitted when the pauser is changed by the owner
    event PauserChanged(address newPauser, address oldPauser);

    /// @notice Event emitted when an operation is paused by the pauser
    event OperationPaused(string operation, uint256 pauseDuration);

    /// @notice Event emitted when an operation is unpaused by the owner
    event OperationUnpaused(string operation);
    
    /// @notice Define valid operations that can be paused
    constructor () public {
        paused[\"supply\"].isValid = true;
        paused[\"withdrawSupply\"].isValid = true;
        paused[\"collateralize\"].isValid = true;
        paused[\"withdrawCollateral\"].isValid = true;
        paused[\"borrow\"].isValid = true;
        paused[\"repayBorrow\"].isValid = true;
        paused[\"liquidateBorrowerCollateral\"].isValid = true;
        paused[\"buyLiquidatedCollateral\"].isValid = true;
        paused[\"depositPromotionReserve\"].isValid = true;
        paused[\"depositLiquidationReserve\"].isValid = true;
    }

    /// @dev Modifier to make a function callable only by owner or pauser
    modifier onlyPausers() {
        require(msg.sender == owner || msg.sender == pauser , \"POE01\");
        _;
    }
    
    /// @dev Modifier to make a function callable only when an operation is not paused
    /// @param operation Name of the operation
    modifier whenNotPaused(string memory operation) {
        require(!isPaused(operation), \"POE02\");
        _;
    }

    /// @dev Modifier to make a function callable only when an operation is paused
    /// @param operation Name of the operation
    modifier whenPaused(string memory operation) {
        require(isPaused(operation), \"POE03\");
        _;
    }

    /// @dev Modifier to make a function callable only when an operation is valid
    /// @param operation Name of the operation
    modifier operationIsValid(string memory operation) {
        require(paused[operation].isValid ,\"POE04\");
        _;
    }

    /// @notice Returns the pause status of a given operation
    /// @param operation Name of the operation
    /// @return res Pause status of a given operation
    function isPaused(string memory operation) public view returns (bool res) {
        res = block.timestamp \u003c= paused[operation].pauseEndTime;
    }

    /// @notice Called by pausers to pause an operation, triggers stopped state
    /// @param operation Name of the operation
    /// @param pauseDuration The length of time the operation must be paused
    function pause(string memory operation, uint256 pauseDuration)
        public
        onlyPausers
        operationIsValid(operation)
        whenNotPaused(operation)
    {
        require (pauseDuration \u003c= maxPauseDuration, \"POE05\");
        paused[operation].pauseEndTime = block.timestamp + pauseDuration;
        emit OperationPaused(operation, pauseDuration);
    }

    /// @notice Called by owner to unpause an operation, returns to normal state
    /// @param operation Name of the operation
    function unpause(string memory operation)
        public
        onlyOwner
        operationIsValid(operation)
        whenPaused(operation)
    {
        paused[operation].pauseEndTime = 0;
        emit OperationUnpaused(operation);
    }

    /// @notice Called by pausers to pause operations, triggers stopped state for selected operations
    /// @param operations List of operation names
    /// @param pauseDurations List of durations specifying the pause time of each operation
    function batchPause(string[] memory operations, uint256[] memory pauseDurations) external {
        require (operations.length == pauseDurations.length, \"POE06\");
        for (uint256 i = 0 ; i \u003c operations.length ; i++) {
            pause(operations[i], pauseDurations[i]);
        }
    }

    /// @notice Called by pausers to unpause operations, returns to normal state for selected operations
    /// @param operations List of operation names
    function batchUnpause(string[] memory operations) external {
        for (uint256 i = 0 ; i \u003c operations.length ; i++) {
            unpause(operations[i]);
        }
    }

    /// @notice Called by owner to set a new pauser
    /// @param newPauser Address of new pauser
    function setPauser(address newPauser) external onlyOwner {
        emit PauserChanged(newPauser, pauser);
        pauser = newPauser;
    }
}"},"IERC20.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.6.0;

/**
 * @dev Interface of the ERC20 standard as defined in the EIP.
 */
interface IERC20 {
    /**
     * @dev Returns the amount of tokens in existence.
     */
    function totalSupply() external view returns (uint256);

    /**
     * @dev Returns the amount of tokens owned by `account`.
     */
    function balanceOf(address account) external view returns (uint256);

    /**
     * @dev Moves `amount` tokens from the caller\u0027s account to `recipient`.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transfer(address recipient, uint256 amount) external returns (bool);

    /**
     * @dev Returns the remaining number of tokens that `spender` will be
     * allowed to spend on behalf of `owner` through {transferFrom}. This is
     * zero by default.
     *
     * This value changes when {approve} or {transferFrom} are called.
     */
    function allowance(address owner, address spender) external view returns (uint256);

    /**
     * @dev Sets `amount` as the allowance of `spender` over the caller\u0027s tokens.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * IMPORTANT: Beware that changing an allowance with this method brings the risk
     * that someone may use both the old and the new allowance by unfortunate
     * transaction ordering. One possible solution to mitigate this race
     * condition is to first reduce the spender\u0027s allowance to 0 and set the
     * desired value afterwards:
     * https://github.com/ethereum/EIPs/issues/20#issuecomment-263524729
     *
     * Emits an {Approval} event.
     */
    function approve(address spender, uint256 amount) external returns (bool);

    /**
     * @dev Moves `amount` tokens from `sender` to `recipient` using the
     * allowance mechanism. `amount` is then deducted from the caller\u0027s
     * allowance.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transferFrom(address sender, address recipient, uint256 amount) external returns (bool);

    /**
     * @dev Emitted when `value` tokens are moved from one account (`from`) to
     * another (`to`).
     *
     * Note that `value` may be zero.
     */
    event Transfer(address indexed from, address indexed to, uint256 value);

    /**
     * @dev Emitted when the allowance of a `spender` for an `owner` is set by
     * a call to {approve}. `value` is the new allowance.
     */
    event Approval(address indexed owner, address indexed spender, uint256 value);
}
"},"ReentrancyGuard.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity \u003e=0.6.0 \u003c0.8.0;

/**
 * @dev Contract module that helps prevent reentrant calls to a function.
 *
 * Inheriting from `ReentrancyGuard` will make the {nonReentrant} modifier
 * available, which can be applied to functions to make sure there are no nested
 * (reentrant) calls to them.
 *
 * Note that because there is a single `nonReentrant` guard, functions marked as
 * `nonReentrant` may not call one another. This can be worked around by making
 * those functions `private`, and then adding `external` `nonReentrant` entry
 * points to them.
 *
 * TIP: If you would like to learn more about reentrancy and alternative ways
 * to protect against it, check out our blog post
 * https://blog.openzeppelin.com/reentrancy-after-istanbul/[Reentrancy After Istanbul].
 */
abstract contract ReentrancyGuard {
    // Booleans are more expensive than uint256 or any type that takes up a full
    // word because each write operation emits an extra SLOAD to first read the
    // slot\u0027s contents, replace the bits taken up by the boolean, and then write
    // back. This is the compiler\u0027s defense against contract upgrades and
    // pointer aliasing, and it cannot be disabled.

    // The values being non-zero value makes deployment a bit more expensive,
    // but in exchange the refund on every call to nonReentrant will be lower in
    // amount. Since refunds are capped to a percentage of the total
    // transaction\u0027s gas, it is best to keep them low in cases like this one, to
    // increase the likelihood of the full refund coming into effect.
    uint256 private constant _NOT_ENTERED = 1;
    uint256 private constant _ENTERED = 2;

    uint256 private _status;

    constructor () internal {
        _status = _NOT_ENTERED;
    }

    /**
     * @dev Prevents a contract from calling itself, directly or indirectly.
     * Calling a `nonReentrant` function from another `nonReentrant`
     * function is not supported. It is possible to prevent this from happening
     * by making the `nonReentrant` function external, and make it call a
     * `private` function that does the actual work.
     */
    modifier nonReentrant() {
        // On the first call to nonReentrant, _notEntered will be true
        require(_status != _ENTERED, \"ReentrancyGuard: reentrant call\");

        // Any calls to nonReentrant after this point will fail
        _status = _ENTERED;

        _;

        // By storing the original value once again, a refund is triggered (see
        // https://eips.ethereum.org/EIPS/eip-2200)
        _status = _NOT_ENTERED;
    }
}
"},"SafeERC20.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity \u003e=0.6.0 \u003c0.8.0;

import \"./IERC20.sol\";
import \"./SafeMath.sol\";
import \"./Address.sol\";

/**
 * @title SafeERC20
 * @dev Wrappers around ERC20 operations that throw on failure (when the token
 * contract returns false). Tokens that return no value (and instead revert or
 * throw on failure) are also supported, non-reverting calls are assumed to be
 * successful.
 * To use this library you can add a `using SafeERC20 for IERC20;` statement to your contract,
 * which allows you to call the safe operations as `token.safeTransfer(...)`, etc.
 */
library SafeERC20 {
    using SafeMath for uint256;
    using Address for address;

    function safeTransfer(IERC20 token, address to, uint256 value) internal {
        _callOptionalReturn(token, abi.encodeWithSelector(token.transfer.selector, to, value));
    }

    function safeTransferFrom(IERC20 token, address from, address to, uint256 value) internal {
        _callOptionalReturn(token, abi.encodeWithSelector(token.transferFrom.selector, from, to, value));
    }

    /**
     * @dev Deprecated. This function has issues similar to the ones found in
     * {IERC20-approve}, and its usage is discouraged.
     *
     * Whenever possible, use {safeIncreaseAllowance} and
     * {safeDecreaseAllowance} instead.
     */
    function safeApprove(IERC20 token, address spender, uint256 value) internal {
        // safeApprove should only be called when setting an initial allowance,
        // or when resetting it to zero. To increase and decrease it, use
        // \u0027safeIncreaseAllowance\u0027 and \u0027safeDecreaseAllowance\u0027
        // solhint-disable-next-line max-line-length
        require((value == 0) || (token.allowance(address(this), spender) == 0),
            \"SafeERC20: approve from non-zero to non-zero allowance\"
        );
        _callOptionalReturn(token, abi.encodeWithSelector(token.approve.selector, spender, value));
    }

    function safeIncreaseAllowance(IERC20 token, address spender, uint256 value) internal {
        uint256 newAllowance = token.allowance(address(this), spender).add(value);
        _callOptionalReturn(token, abi.encodeWithSelector(token.approve.selector, spender, newAllowance));
    }

    function safeDecreaseAllowance(IERC20 token, address spender, uint256 value) internal {
        uint256 newAllowance = token.allowance(address(this), spender).sub(value, \"SafeERC20: decreased allowance below zero\");
        _callOptionalReturn(token, abi.encodeWithSelector(token.approve.selector, spender, newAllowance));
    }

    /**
     * @dev Imitates a Solidity high-level call (i.e. a regular function call to a contract), relaxing the requirement
     * on the return value: the return value is optional (but if data is returned, it must not be false).
     * @param token The token targeted by the call.
     * @param data The call data (encoded using abi.encode or one of its variants).
     */
    function _callOptionalReturn(IERC20 token, bytes memory data) private {
        // We need to perform a low level call here, to bypass Solidity\u0027s return data size checking mechanism, since
        // we\u0027re implementing it ourselves. We use {Address.functionCall} to perform this call, which verifies that
        // the target address contains contract code and also asserts for success in the low-level call.

        bytes memory returndata = address(token).functionCall(data, \"SafeERC20: low-level call failed\");
        if (returndata.length \u003e 0) { // Return data is optional
            // solhint-disable-next-line max-line-length
            require(abi.decode(returndata, (bool)), \"SafeERC20: ERC20 operation did not succeed\");
        }
    }
}
"},"SafeMath.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity \u003e=0.6.0 \u003c0.8.0;

/**
 * @dev Wrappers over Solidity\u0027s arithmetic operations with added overflow
 * checks.
 *
 * Arithmetic operations in Solidity wrap on overflow. This can easily result
 * in bugs, because programmers usually assume that an overflow raises an
 * error, which is the standard behavior in high level programming languages.
 * `SafeMath` restores this intuition by reverting the transaction when an
 * operation overflows.
 *
 * Using this library instead of the unchecked operations eliminates an entire
 * class of bugs, so it\u0027s recommended to use it always.
 */
library SafeMath {
    /**
     * @dev Returns the addition of two unsigned integers, with an overflow flag.
     *
     * _Available since v3.4._
     */
    function tryAdd(uint256 a, uint256 b) internal pure returns (bool, uint256) {
        uint256 c = a + b;
        if (c \u003c a) return (false, 0);
        return (true, c);
    }

    /**
     * @dev Returns the substraction of two unsigned integers, with an overflow flag.
     *
     * _Available since v3.4._
     */
    function trySub(uint256 a, uint256 b) internal pure returns (bool, uint256) {
        if (b \u003e a) return (false, 0);
        return (true, a - b);
    }

    /**
     * @dev Returns the multiplication of two unsigned integers, with an overflow flag.
     *
     * _Available since v3.4._
     */
    function tryMul(uint256 a, uint256 b) internal pure returns (bool, uint256) {
        // Gas optimization: this is cheaper than requiring \u0027a\u0027 not being zero, but the
        // benefit is lost if \u0027b\u0027 is also tested.
        // See: https://github.com/OpenZeppelin/openzeppelin-contracts/pull/522
        if (a == 0) return (true, 0);
        uint256 c = a * b;
        if (c / a != b) return (false, 0);
        return (true, c);
    }

    /**
     * @dev Returns the division of two unsigned integers, with a division by zero flag.
     *
     * _Available since v3.4._
     */
    function tryDiv(uint256 a, uint256 b) internal pure returns (bool, uint256) {
        if (b == 0) return (false, 0);
        return (true, a / b);
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers, with a division by zero flag.
     *
     * _Available since v3.4._
     */
    function tryMod(uint256 a, uint256 b) internal pure returns (bool, uint256) {
        if (b == 0) return (false, 0);
        return (true, a % b);
    }

    /**
     * @dev Returns the addition of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity\u0027s `+` operator.
     *
     * Requirements:
     *
     * - Addition cannot overflow.
     */
    function add(uint256 a, uint256 b) internal pure returns (uint256) {
        uint256 c = a + b;
        require(c \u003e= a, \"SafeMath: addition overflow\");
        return c;
    }

    /**
     * @dev Returns the subtraction of two unsigned integers, reverting on
     * overflow (when the result is negative).
     *
     * Counterpart to Solidity\u0027s `-` operator.
     *
     * Requirements:
     *
     * - Subtraction cannot overflow.
     */
    function sub(uint256 a, uint256 b) internal pure returns (uint256) {
        require(b \u003c= a, \"SafeMath: subtraction overflow\");
        return a - b;
    }

    /**
     * @dev Returns the multiplication of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity\u0027s `*` operator.
     *
     * Requirements:
     *
     * - Multiplication cannot overflow.
     */
    function mul(uint256 a, uint256 b) internal pure returns (uint256) {
        if (a == 0) return 0;
        uint256 c = a * b;
        require(c / a == b, \"SafeMath: multiplication overflow\");
        return c;
    }

    /**
     * @dev Returns the integer division of two unsigned integers, reverting on
     * division by zero. The result is rounded towards zero.
     *
     * Counterpart to Solidity\u0027s `/` operator. Note: this function uses a
     * `revert` opcode (which leaves remaining gas untouched) while Solidity
     * uses an invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function div(uint256 a, uint256 b) internal pure returns (uint256) {
        require(b \u003e 0, \"SafeMath: division by zero\");
        return a / b;
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
     * reverting when dividing by zero.
     *
     * Counterpart to Solidity\u0027s `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function mod(uint256 a, uint256 b) internal pure returns (uint256) {
        require(b \u003e 0, \"SafeMath: modulo by zero\");
        return a % b;
    }

    /**
     * @dev Returns the subtraction of two unsigned integers, reverting with custom message on
     * overflow (when the result is negative).
     *
     * CAUTION: This function is deprecated because it requires allocating memory for the error
     * message unnecessarily. For custom revert reasons use {trySub}.
     *
     * Counterpart to Solidity\u0027s `-` operator.
     *
     * Requirements:
     *
     * - Subtraction cannot overflow.
     */
    function sub(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b \u003c= a, errorMessage);
        return a - b;
    }

    /**
     * @dev Returns the integer division of two unsigned integers, reverting with custom message on
     * division by zero. The result is rounded towards zero.
     *
     * CAUTION: This function is deprecated because it requires allocating memory for the error
     * message unnecessarily. For custom revert reasons use {tryDiv}.
     *
     * Counterpart to Solidity\u0027s `/` operator. Note: this function uses a
     * `revert` opcode (which leaves remaining gas untouched) while Solidity
     * uses an invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function div(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b \u003e 0, errorMessage);
        return a / b;
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
     * reverting with custom message when dividing by zero.
     *
     * CAUTION: This function is deprecated because it requires allocating memory for the error
     * message unnecessarily. For custom revert reasons use {tryMod}.
     *
     * Counterpart to Solidity\u0027s `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function mod(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b \u003e 0, errorMessage);
        return a % b;
    }
}

