pragma solidity ^0.5.0;
import \"./SafeMath.sol\";

contract AffiliateStorage {
\tusing SafeMath for uint;

\tmapping(uint=\u003eaddress) public codeToAddressMap;
\tmapping(address=\u003euint) public addressToCodeMap;
\tmapping(address=\u003euint) public addressToFatherCode;
\tuint private constant maxCode = 100000000;
\tbytes public constant baseString = \"0123456789abcdefghjklmnpqrstuvwxyz\";


\t/**
\t* code is the last 6 digt of user
\t**/
\tfunction newCode(address user) public returns(uint) {
\t\trequire(addressToCodeMap[user] == 0, \"user existed\");

\t\tuint code = uint(user);
\t\tcode = code.sub(code.div(maxCode).mul(maxCode));

\t\trequire(code !=0, \"code must \u003e 0\");

\t\twhile(codeToAddressMap[code]!=address(0)) {
\t\t\tcode = code.add(7);
\t\t}
\t\tcodeToAddressMap[code] = user;
\t\taddressToCodeMap[user] = code;
\t\treturn code;

\t}

\tfunction getCode(address user) public view returns(uint) {
\t\tuint code = addressToCodeMap[user];
\t\tif(code != 0)
\t\t\treturn code;
\t}

\tfunction getUser(uint code) public view returns(address) {
\t\treturn codeToAddressMap[code];
\t}
\t
\tfunction setFather(uint code) public {
\t    require(codeToAddressMap[code] != address(0), \"inviteCode not existed\");
\t    addressToFatherCode[msg.sender] = code;
\t}
\t
\tfunction getFatherCode(address userAddress) public view returns (uint){
\t    uint FatherCode = addressToFatherCode[userAddress];
\t    return FatherCode;
\t}
\t
\tfunction getFather(address userAddress) public view returns(address){
        uint FatherCode = addressToFatherCode[userAddress];
        if(FatherCode != 0 ){
        address FatherAddress = getUser(FatherCode);
        return  FatherAddress;
        }
        //require(addressToFatherCode[userAddress]!=0,\"FatherCode not existed\");
\t}
\t
} "},"SafeMath.sol":{"content":"pragma solidity ^0.5.0;
/**
 * @title SafeMath
 * @dev Math operations with safety checks that throw on error
 */
library SafeMath {
    /**
     * @dev Returns the addition of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity\u0027s `+` operator.
     *
     * Requirements:
     * - Addition cannot overflow.
     */
    function add(uint256 a, uint256 b) internal pure returns (uint256) {
        uint256 c = a + b;
        require(c \u003e= a, \"SafeMath: addition overflow\");

        return c;
    }

    /**
     * @dev Returns the subtraction of two unsigned integers, reverting on
     * overflow (when the result is negative).
     *
     * Counterpart to Solidity\u0027s `-` operator.
     *
     * Requirements:
     * - Subtraction cannot overflow.
     */
    function sub(uint256 a, uint256 b) internal pure returns (uint256) {
        return sub(a, b, \"SafeMath: subtraction overflow\");
    }

    /**
     * @dev Returns the subtraction of two unsigned integers, reverting with custom message on
     * overflow (when the result is negative).
     *
     * Counterpart to Solidity\u0027s `-` operator.
     *
     * Requirements:
     * - Subtraction cannot overflow.
     *
     * _Available since v2.4.0._
     */
    function sub(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b \u003c= a, errorMessage);
        uint256 c = a - b;

        return c;
    }

    /**
     * @dev Returns the multiplication of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity\u0027s `*` operator.
     *
     * Requirements:
     * - Multiplication cannot overflow.
     */
    function mul(uint256 a, uint256 b) internal pure returns (uint256) {
        // Gas optimization: this is cheaper than requiring \u0027a\u0027 not being zero, but the
        // benefit is lost if \u0027b\u0027 is also tested.
        // See: https://github.com/OpenZeppelin/openzeppelin-contracts/pull/522
        if (a == 0) {
            return 0;
        }

        uint256 c = a * b;
        require(c / a == b, \"SafeMath: multiplication overflow\");

        return c;
    }

    /**
     * @dev Returns the integer division of two unsigned integers. Reverts on
     * division by zero. The result is rounded towards zero.
     *
     * Counterpart to Solidity\u0027s `/` operator. Note: this function uses a
     * `revert` opcode (which leaves remaining gas untouched) while Solidity
     * uses an invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     * - The divisor cannot be zero.
     */
    function div(uint256 a, uint256 b) internal pure returns (uint256) {
        return div(a, b, \"SafeMath: division by zero\");
    }

    /**
     * @dev Returns the integer division of two unsigned integers. Reverts with custom message on
     * division by zero. The result is rounded towards zero.
     *
     * Counterpart to Solidity\u0027s `/` operator. Note: this function uses a
     * `revert` opcode (which leaves remaining gas untouched) while Solidity
     * uses an invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     * - The divisor cannot be zero.
     *
     * _Available since v2.4.0._
     */
    function div(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        // Solidity only automatically asserts when dividing by 0
        require(b \u003e 0, errorMessage);
        uint256 c = a / b;
        // assert(a == b * c + a % b); // There is no case in which this doesn\u0027t hold

        return c;
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
     * Reverts when dividing by zero.
     *
     * Counterpart to Solidity\u0027s `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     * - The divisor cannot be zero.
     */
    function mod(uint256 a, uint256 b) internal pure returns (uint256) {
        return mod(a, b, \"SafeMath: modulo by zero\");
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
     * Reverts with custom message when dividing by zero.
     *
     * Counterpart to Solidity\u0027s `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     * - The divisor cannot be zero.
     *
     * _Available since v2.4.0._
     */
    function mod(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b != 0, errorMessage);
        return a % b;
    }
}
