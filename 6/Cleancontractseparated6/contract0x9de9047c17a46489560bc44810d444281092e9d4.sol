// SPDX-License-Identifier: GPL-3.0
pragma solidity \u003e=0.8.0 \u003c0.9.0;

import \"./ERC20.sol\";
import \"./Ownable.sol\";

contract CoinvestingDeFiToken is Ownable, ERC20 {
    // Public variables
    address public saleReserve;
    address public technicalAndOperationalReserve;

    // Events
    event Received(address, uint);
    
    // Constructor
    constructor (
        string memory name,
        string memory symbol,
        uint _initialSupply,
        address _saleReserve,
        address _technicalAndOperationalReserve          
    ) payable ERC20 (name, symbol) {
        saleReserve = _saleReserve;
        technicalAndOperationalReserve = _technicalAndOperationalReserve;
        if (_initialSupply \u003e 0) {
            require((_initialSupply % 10) == 0, \"_initialSupply has to be a multiple of 10!\");
            uint eightyFivePerCent = _initialSupply * 85 / 100;
            uint fifteenPerCent = _initialSupply * 15 / 100; 
            mint(saleReserve, fifteenPerCent); 
            mint(technicalAndOperationalReserve, eightyFivePerCent);       
            mintingFinished = true;
        }
    }

    // Receive function 
    receive() external payable {
        emit Received(msg.sender, msg.value);
    }

    // External functions
    function withdraw() external onlyOwner {
        require(address(this).balance \u003e 0, \"Insuficient funds!\");
        uint amount = address(this).balance;
        // sending to prevent re-entrancy attacks
        address(this).balance - amount;
        payable(msg.sender).transfer(amount);
    }
    
    // Public functions
    function mint(address account, uint amount) public onlyOwner canMint {
        _mint(account, amount);
    }
}
"},"CoinvestingDeFiTokenSale.sol":{"content":"// SPDX-License-Identifier: GPL-3.0
pragma solidity \u003e=0.8.0 \u003c0.9.0;

import \"./CoinvestingDeFiToken.sol\";
import \"./Ownable.sol\";

contract CoinvestingDeFiTokenSale is Ownable {
    // Public variables
    CoinvestingDeFiToken public tokenContract;
    uint public bonusLevelOne = 50;
    uint public bonusLevelTwo = 35;
    uint public bonusLevelThree = 20;
    uint public bonusLevelFour = 5;
    uint public calculatedBonus;    
    uint public levelOneDate;
    uint public levelTwoDate;
    uint public levelThreeDate;
    uint public levelFourDate;
    uint public tokenBonus;
    uint public tokenPrice;
    uint public tokenSale;
    uint public tokensSold;
    
    // Internal variables
    bool internal contractSeted = false;

    // Events
    event Received(address, uint);
    event Sell(address _buyer, uint _amount);
    event SetPercents(uint _bonusLevelOne, uint _bonusLevelTwo, uint _bonusLevelThree, uint _bonusLevelFour);
    
    // Modifiers
    modifier canContractSet() {
        require(!contractSeted, \"Set contract token is not allowed!\");
        _;
    }

    // Constructor
    constructor(uint _levelOneDate, uint _levelTwoDate, uint _levelThreeDate, uint _levelFourDate) payable {
        levelOneDate = _levelOneDate * 1 seconds;
        levelTwoDate = _levelTwoDate * 1 days;
        levelThreeDate = _levelThreeDate * 1 days;
        levelFourDate = _levelFourDate * 1 days;
    }

    // Receive function
    receive() external payable {
        emit Received(msg.sender, msg.value);
    }

    // External functions
    function endSale() external onlyOwner {
        require(
            tokenContract.transfer(
                msg.sender,
                tokenContract.balanceOf(address(this))
            ),
            \"Unable to transfer tokens to admin!\"
        );
        // destroy contract
        payable(msg.sender).transfer(address(this).balance);
        contractSeted = false;
    }

    function setTokenContract(CoinvestingDeFiToken _tokenContract) external onlyOwner canContractSet {
        tokenContract = _tokenContract;
        contractSeted = true;
        tokenBonus = tokenContract.balanceOf(address(this)) / 3;
        tokenSale = tokenContract.balanceOf(address(this)) - tokenBonus;        
    }

    function setPercents(
        uint _bonusLevelOne,
        uint _bonusLevelTwo,
        uint _bonusLevelThree,
        uint _bonusLevelFour
    ) 
    external 
    onlyOwner {
        require(_bonusLevelOne \u003c= 50, \"L1 - Maximum 50 %.\");
        require(_bonusLevelTwo \u003c= bonusLevelOne, \"L2 - The maximum value must be the current L1.\");
        require(_bonusLevelThree \u003c= bonusLevelTwo, \"L3 - The maximum value must be the current L2.\");
        require(_bonusLevelFour \u003c= bonusLevelThree, \"L4 - The maximum value must be the current L3.\");
        bonusLevelOne = _bonusLevelOne;
        bonusLevelTwo = _bonusLevelTwo;
        bonusLevelThree = _bonusLevelThree;
        bonusLevelFour = _bonusLevelFour;
        emit SetPercents(bonusLevelOne, bonusLevelTwo, bonusLevelThree, bonusLevelFour);
    }

    function withdraw() external onlyOwner {
        require(address(this).balance \u003e 0, \"Insuficient funds!\");
        uint amount = address(this).balance;
        // sending to prevent re-entrancy attacks
        address(this).balance - amount;
        payable(msg.sender).transfer(amount);
    }

    // External functions that are view
    function getBalance() external view returns(uint) {
        return address(this).balance;
    }

    function getTokenBonusBalance() external view returns(uint) {
        return tokenBonus;
    }

    function getTokenSaleBalance() external view returns(uint) {
        return tokenSale;
    }

    function getPercents() 
    external 
    view 
    returns(
        uint levelOne,
        uint levelTwo,
        uint levelThree,
        uint levelFour)
    {
        levelOne = bonusLevelOne;
        levelTwo = bonusLevelTwo;
        levelThree = bonusLevelThree;
        levelFour = bonusLevelFour;
    }

    // Public functions
    function buyTokens(uint _numberOfTokens, uint _tokenPrice) public payable {
        if (block.timestamp \u003c= levelOneDate + levelTwoDate) {
            calculatedBonus = _numberOfTokens * bonusLevelOne / 100;
        }
        else if (block.timestamp \u003c= levelOneDate + levelTwoDate + levelThreeDate) {
            calculatedBonus = _numberOfTokens * bonusLevelTwo / 100;
        }
        else if (block.timestamp \u003c= levelOneDate + levelTwoDate + levelThreeDate + levelFourDate) {
            calculatedBonus = _numberOfTokens * bonusLevelThree / 100;
        }
        else {
            calculatedBonus = _numberOfTokens * bonusLevelFour / 100;
        }

        require(
            msg.value == _numberOfTokens * _tokenPrice,
            \"Number of tokens does not match with the value!\"
        );
            
        uint scaledAmount = (calculatedBonus + _numberOfTokens) * 10 ** tokenContract.decimals();
        require(
            tokenSale \u003e= _numberOfTokens * 10 ** tokenContract.decimals(),
            \"The contract does not have enough TOKENS!\"
        );

        require(
            tokenBonus \u003e= calculatedBonus * 10 ** tokenContract.decimals(),
            \"The contract does not have enough BONUS tokens!\"
        );

        tokensSold += _numberOfTokens;
        tokenSale -= _numberOfTokens * 10 ** tokenContract.decimals();
        tokenBonus -= calculatedBonus * 10 ** tokenContract.decimals();
        emit Sell(msg.sender, _numberOfTokens);
        require(
            tokenContract.transfer(payable(msg.sender), scaledAmount),
            \"Some problem with token transfer!\"
        );        
    }
}
"},"Context.sol":{"content":"// SPDX-License-Identifier: GPL-3.0
pragma solidity \u003e=0.8.0 \u003c0.9.0;

/*
 * @dev Provides information about the current execution context, including the
 * sender of the transaction and its data. While these are generally available
 * via msg.sender and msg.data, they should not be accessed in such a direct
 * manner, since when dealing with GSN meta-transactions the account sending and
 * paying for execution may not be the actual sender (as far as an application
 * is concerned).
 *
 * This contract is only required for intermediate, library-like contracts.
 */
abstract contract Context {
    function _msgSender() internal view virtual returns (address payable) {
        return payable(msg.sender);
    }

    function _msgData() internal view virtual returns (bytes memory) {
        this; // silence state mutability warning without generating bytecode - see https://github.com/ethereum/solidity/issues/2691
        return msg.data;
    }
}
"},"ERC20.sol":{"content":"// SPDX-License-Identifier: GPL-3.0
pragma solidity \u003e=0.8.0 \u003c0.9.0;

import \"./Context.sol\";
import \"./IERC20.sol\";

/**
 * @dev Implementation of the {IERC20} interface.
 *
 * This implementation is agnostic to the way tokens are created. This means
 * that a supply mechanism has to be added in a derived contract using {_mint}.
 * For a generic mechanism see {ERC20PresetMinterPauser}.
 *
 * TIP: For a detailed writeup see our guide
 * https://forum.zeppelin.solutions/t/how-to-implement-erc20-supply-mechanisms/226[How
 * to implement supply mechanisms].
 *
 * We have followed general OpenZeppelin guidelines: functions revert instead
 * of returning `false` on failure. This behavior is nonetheless conventional
 * and does not conflict with the expectations of ERC20 applications.
 *
 * Additionally, an {Approval} event is emitted on calls to {transferFrom}.
 * This allows applications to reconstruct the allowance for all accounts just
 * by listening to said events. Other implementations of the EIP may not emit
 * these events, as it isn\u0027t required by the specification.
 *
 * Finally, the non-standard {decreaseAllowance} and {increaseAllowance}
 * functions have been added to mitigate the well-known issues around setting
 * allowances. See {IERC20-approve}.
 */
contract ERC20 is Context, IERC20 {

    bool internal mintingFinished = false;

    modifier canMint() {
        require(!mintingFinished, \"Creating new tokens is not allowed.\");
        _;
    }

    mapping (address =\u003e uint) private _balances;

    mapping (address =\u003e mapping (address =\u003e uint)) private _allowances;

    uint private _totalSupply;

    string private _name;
    string private _symbol;
    uint8 private _decimals;

    /**
     * @dev Sets the values for {name} and {symbol}, initializes {decimals} with
     * a default value of 18.
     *
     * To select a different value for {decimals}, use {_setupDecimals}.
     *
     * All three of these values are immutable: they can only be set once during
     * construction.
     */
    constructor (string memory name_, string memory symbol_) {
        _name = name_;
        _symbol = symbol_;
        _decimals = 18;
    }

    /**
     * @dev Returns the name of the token.
     */
    function name() public view returns (string memory) {
        return _name;
    }

    /**
     * @dev Returns the symbol of the token, usually a shorter version of the
     * name.
     */
    function symbol() public view returns (string memory) {
        return _symbol;
    }

    /**
     * @dev Returns the number of decimals used to get its user representation.
     * For example, if `decimals` equals `2`, a balance of `505` tokens should
     * be displayed to a user as `5,05` (`505 / 10 ** 2`).
     *
     * Tokens usually opt for a value of 18, imitating the relationship between
     * Ether and Wei. This is the value {ERC20} uses, unless {_setupDecimals} is
     * called.
     *
     * NOTE: This information is only used for _display_ purposes: it in
     * no way affects any of the arithmetic of the contract, including
     * {IERC20-balanceOf} and {IERC20-transfer}.
     */
    function decimals() public view returns (uint8) {
        return _decimals;
    }

    /**
     * @dev See {IERC20-totalSupply}.
     */
    function totalSupply() public view override returns (uint) {
        return _totalSupply;
    }

    /**
     * @dev See {IERC20-balanceOf}.
     */
    function balanceOf(address account) public view override returns (uint) {
        return _balances[account];
    }

    /**
     * @dev See {IERC20-transfer}.
     *
     * Requirements:
     *
     * - `recipient` cannot be the zero address.
     * - the caller must have a balance of at least `amount`.
     */
    function transfer(address recipient, uint amount) public virtual override returns (bool) {
        _transfer(_msgSender(), recipient, amount);
        return true;
    }

    /**
     * @dev See {IERC20-allowance}.
     */
    function allowance(address owner, address spender) public view virtual override returns (uint) {
        return _allowances[owner][spender];
    }

    /**
     * @dev See {IERC20-approve}.
     *
     * Requirements:
     *
     * - `spender` cannot be the zero address.
     */
    function approve(address spender, uint amount) public virtual override returns (bool) {
        _approve(_msgSender(), spender, amount);
        return true;
    }

    /**
     * @dev See {IERC20-transferFrom}.
     *
     * Emits an {Approval} event indicating the updated allowance. This is not
     * required by the EIP. See the note at the beginning of {ERC20}.
     *
     * Requirements:
     *
     * - `sender` and `recipient` cannot be the zero address.
     * - `sender` must have a balance of at least `amount`.
     * - the caller must have allowance for ``sender``\u0027s tokens of at least
     * `amount`.
     */
    function transferFrom(address sender, address recipient, uint amount) public virtual override returns (bool) {
        _transfer(sender, recipient, amount);
        _approve(sender, _msgSender(), _allowances[sender][_msgSender()] - amount);
        return true;
    }

    /**
     * @dev Atomically increases the allowance granted to `spender` by the caller.
     *
     * This is an alternative to {approve} that can be used as a mitigation for
     * problems described in {IERC20-approve}.
     *
     * Emits an {Approval} event indicating the updated allowance.
     *
     * Requirements:
     *
     * - `spender` cannot be the zero address.
     */
    function increaseAllowance(address spender, uint addedValue) public virtual returns (bool) {
        _approve(_msgSender(), spender, _allowances[_msgSender()][spender] + addedValue);
        return true;
    }

    /**
     * @dev Atomically decreases the allowance granted to `spender` by the caller.
     *
     * This is an alternative to {approve} that can be used as a mitigation for
     * problems described in {IERC20-approve}.
     *
     * Emits an {Approval} event indicating the updated allowance.
     *
     * Requirements:
     *
     * - `spender` cannot be the zero address.
     * - `spender` must have allowance for the caller of at least
     * `subtractedValue`.
     */
    function decreaseAllowance(address spender, uint subtractedValue) public virtual returns (bool) {
        _approve(_msgSender(), spender, _allowances[_msgSender()][spender] - subtractedValue);
        return true;
    }

    /**
     * @dev Moves tokens `amount` from `sender` to `recipient`.
     *
     * This is internal function is equivalent to {transfer}, and can be used to
     * e.g. implement automatic token fees, slashing mechanisms, etc.
     *
     * Emits a {Transfer} event.
     *
     * Requirements:
     *
     * - `sender` cannot be the zero address.
     * - `recipient` cannot be the zero address.
     * - `sender` must have a balance of at least `amount`.
     */
    function _transfer(address sender, address recipient, uint amount) internal virtual {
        require(sender != address(0), \"Transfer from the zero address not allowed.\");
        require(recipient != address(0), \"Transfer to the zero address not allowed.\");

        _beforeTokenTransfer(sender, recipient, amount);

        _balances[sender] = _balances[sender] - amount;
        _balances[recipient] = _balances[recipient] + amount;
        emit Transfer(sender, recipient, amount);
    }

    /** @dev Creates `amount` tokens and assigns them to `account`, increasing
     * the total supply.
     *
     * Emits a {Transfer} event with `from` set to the zero address.
     *
     * Requirements:
     *
     * - `to` cannot be the zero address.
     */
    function _mint(address account, uint amount) internal virtual {
        require(account != address(0), \"Mint to the zero address not allowed.\");

        _beforeTokenTransfer(address(0), account, amount);

        _totalSupply = _totalSupply + amount;
        _balances[account] = _balances[account] + amount;
        emit Transfer(address(0), account, amount);
    }

    /**
     * @dev Sets `amount` as the allowance of `spender` over the `owner` s tokens.
     *
     * This internal function is equivalent to `approve`, and can be used to
     * e.g. set automatic allowances for certain subsystems, etc.
     *
     * Emits an {Approval} event.
     *
     * Requirements:
     *
     * - `owner` cannot be the zero address.
     * - `spender` cannot be the zero address.
     */
    function _approve(address owner, address spender, uint amount) internal virtual {
        require(owner != address(0), \"Approve from the zero address not allowed.\");
        require(spender != address(0), \"Approve to the zero address not allowed.\");

        _allowances[owner][spender] = amount;
        emit Approval(owner, spender, amount);
    }

    /**
     * @dev Hook that is called before any transfer of tokens. This includes
     * minting and burning.
     *
     * Calling conditions:
     *
     * - when `from` and `to` are both non-zero, `amount` of ``from``\u0027s tokens
     * will be to transferred to `to`.
     * - when `from` is zero, `amount` tokens will be minted for `to`.
     * - when `to` is zero, `amount` of ``from``\u0027s tokens will be burned.
     * - `from` and `to` are never both zero.
     *
     * To learn more about hooks, head to xref:ROOT:extending-contracts.adoc#using-hooks[Using Hooks].
     */
    function _beforeTokenTransfer(address from, address to, uint amount) internal virtual { }
}
"},"IERC20.sol":{"content":"// SPDX-License-Identifier: GPL-3.0
pragma solidity \u003e=0.8.0 \u003c0.9.0;

/**
 * @dev Interface of the ERC20 standard as defined in the EIP.
 */
interface IERC20 {
    /**
     * @dev Returns the amount of tokens in existence.
     */
    function totalSupply() external view returns (uint);

    /**
     * @dev Returns the amount of tokens owned by `account`.
     */
    function balanceOf(address account) external view returns (uint);

    /**
     * @dev Moves `amount` tokens from the caller\u0027s account to `recipient`.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transfer(address recipient, uint amount) external returns (bool);

    /**
     * @dev Returns the remaining number of tokens that `spender` will be
     * allowed to spend on behalf of `owner` through {transferFrom}. This is
     * zero by default.
     *
     * This value changes when {approve} or {transferFrom} are called.
     */
    function allowance(address owner, address spender) external view returns (uint);

    /**
     * @dev Sets `amount` as the allowance of `spender` over the caller\u0027s tokens.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * IMPORTANT: Beware that changing an allowance with this method brings the risk
     * that someone may use both the old and the new allowance by unfortunate
     * transaction ordering. One possible solution to mitigate this race
     * condition is to first reduce the spender\u0027s allowance to 0 and set the
     * desired value afterwards:
     * https://github.com/ethereum/EIPs/issues/20#issuecomment-263524729
     *
     * Emits an {Approval} event.
     */
    function approve(address spender, uint amount) external returns (bool);

    /**
     * @dev Moves `amount` tokens from `sender` to `recipient` using the
     * allowance mechanism. `amount` is then deducted from the caller\u0027s
     * allowance.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transferFrom(address sender, address recipient, uint amount) external returns (bool);

    /**
     * @dev Emitted when `value` tokens are moved from one account (`from`) to
     * another (`to`).
     *
     * Note that `value` may be zero.
     */
    event Transfer(address indexed from, address indexed to, uint value);

    /**
     * @dev Emitted when the allowance of a `spender` for an `owner` is set by
     * a call to {approve}. `value` is the new allowance.
     */
    event Approval(address indexed owner, address indexed spender, uint value);

    event MintFinished();
}
"},"Ownable.sol":{"content":"// SPDX-License-Identifier: GPL-3.0
pragma solidity \u003e=0.8.0 \u003c0.9.0;

import \"./Context.sol\";
/**
 * @dev Contract module which provides a basic access control mechanism, where
 * there is an account (an owner) that can be granted exclusive access to
 * specific functions.
 *
 * By default, the owner account will be the one that deploys the contract. This
 * can later be changed with {transferOwnership}.
 *
 * This module is used through inheritance. It will make available the modifier
 * `onlyOwner`, which can be applied to your functions to restrict their use to
 * the owner.
 */
abstract contract Ownable is Context {
    address private _owner;

    event OwnershipTransferred(address indexed previousOwner, address indexed newOwner);

    /**
     * @dev Initializes the contract setting the deployer as the initial owner.
     */
    constructor () {
        address msgSender = _msgSender();
        _owner = msgSender;
        emit OwnershipTransferred(address(0), msgSender);
    }

    /**
     * @dev Returns the address of the current owner.
     */
    function owner() public view returns (address) {
        return _owner;
    }

    /**
     * @dev Throws if called by any account other than the owner.
     */
    modifier onlyOwner() {
        require(_owner == _msgSender(), \"The caller is not the owner.\");
        _;
    }

    /**
     * @dev Leaves the contract without owner. It will not be possible to call
     * `onlyOwner` functions anymore. Can only be called by the current owner.
     *
     * NOTE: Renouncing ownership will leave the contract without an owner,
     * thereby removing any functionality that is only available to the owner.
     */
    function renounceOwnership() public virtual onlyOwner {
        emit OwnershipTransferred(_owner, address(0));
        _owner = address(0);
    }

    /**
     * @dev Transfers ownership of the contract to a new account (`newOwner`).
     * Can only be called by the current owner.
     */
    function transferOwnership(address newOwner) public virtual onlyOwner {
        require(newOwner != address(0), \"New owner is the zero address.\");
        emit OwnershipTransferred(_owner, newOwner);
        _owner = newOwner;
    }
}

