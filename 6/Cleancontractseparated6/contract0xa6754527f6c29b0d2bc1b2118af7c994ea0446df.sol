pragma solidity \u003e=0.5.0;

import \u0027./IBullswapPair.sol\u0027;

import \"./SafeMath.sol\";

library BullswapLibrary {
    using SafeMath for uint;

    // returns sorted token addresses, used to handle return values from pairs sorted in this order
    function sortTokens(address tokenA, address tokenB) internal pure returns (address token0, address token1) {
        require(tokenA != tokenB, \u0027BullswapLibrary: IDENTICAL_ADDRESSES\u0027);
        (token0, token1) = tokenA \u003c tokenB ? (tokenA, tokenB) : (tokenB, tokenA);
        require(token0 != address(0), \u0027BullswapLibrary: ZERO_ADDRESS\u0027);
    }

    // calculates the CREATE2 address for a pair without making any external calls
    function pairFor(address factory, address tokenA, address tokenB) internal pure returns (address pair) {
        (address token0, address token1) = sortTokens(tokenA, tokenB);
        pair = address(uint(keccak256(abi.encodePacked(
                hex\u0027ff\u0027,
                factory,
                keccak256(abi.encodePacked(token0, token1)),
                hex\u0027c413c9755ed462cdead00e40202ebe3ba9125edc44140cc0ff6e8e9cb4fa3498\u0027 // init code hash
            ))));
    }

    // fetches and sorts the reserves for a pair
    function getReserves(address factory, address tokenA, address tokenB) internal view returns (uint reserveA, uint reserveB) {
        (address token0,) = sortTokens(tokenA, tokenB);
        (uint reserve0, uint reserve1,) = IBullswapPair(pairFor(factory, tokenA, tokenB)).getReserves();
        (reserveA, reserveB) = tokenA == token0 ? (reserve0, reserve1) : (reserve1, reserve0);
    }

    // given some amount of an asset and pair reserves, returns an equivalent amount of the other asset
    function quote(uint amountA, uint reserveA, uint reserveB) internal pure returns (uint amountB) {
        require(amountA \u003e 0, \u0027BullswapLibrary: INSUFFICIENT_AMOUNT\u0027);
        require(reserveA \u003e 0 \u0026\u0026 reserveB \u003e 0, \u0027BullswapLibrary: INSUFFICIENT_LIQUIDITY\u0027);
        amountB = amountA.mul(reserveB) / reserveA;
    }

    // given an input amount of an asset and pair reserves, returns the maximum output amount of the other asset
    function getAmountOut(uint amountIn, uint reserveIn, uint reserveOut) internal pure returns (uint amountOut) {
        require(amountIn \u003e 0, \u0027BullswapLibrary: INSUFFICIENT_INPUT_AMOUNT\u0027);
        require(reserveIn \u003e 0 \u0026\u0026 reserveOut \u003e 0, \u0027BullswapLibrary: INSUFFICIENT_LIQUIDITY\u0027);
        uint amountInWithFee = amountIn.mul(997);
        uint numerator = amountInWithFee.mul(reserveOut);
        uint denominator = reserveIn.mul(1000).add(amountInWithFee);
        amountOut = numerator / denominator;
    }

    // given an output amount of an asset and pair reserves, returns a required input amount of the other asset
    function getAmountIn(uint amountOut, uint reserveIn, uint reserveOut) internal pure returns (uint amountIn) {
        require(amountOut \u003e 0, \u0027BullswapLibrary: INSUFFICIENT_OUTPUT_AMOUNT\u0027);
        require(reserveIn \u003e 0 \u0026\u0026 reserveOut \u003e 0, \u0027BullswapLibrary: INSUFFICIENT_LIQUIDITY\u0027);
        uint numerator = reserveIn.mul(amountOut).mul(1000);
        uint denominator = reserveOut.sub(amountOut).mul(997);
        amountIn = (numerator / denominator).add(1);
    }

    // performs chained getAmountOut calculations on any number of pairs
    function getAmountsOut(address factory, uint amountIn, address[] memory path) internal view returns (uint[] memory amounts) {
        require(path.length \u003e= 2, \u0027BullswapLibrary: INVALID_PATH\u0027);
        amounts = new uint[](path.length);
        amounts[0] = amountIn;
        for (uint i; i \u003c path.length - 1; i++) {
            (uint reserveIn, uint reserveOut) = getReserves(factory, path[i], path[i + 1]);
            amounts[i + 1] = getAmountOut(amounts[i], reserveIn, reserveOut);
        }
    }

    // performs chained getAmountIn calculations on any number of pairs
    function getAmountsIn(address factory, uint amountOut, address[] memory path) internal view returns (uint[] memory amounts) {
        require(path.length \u003e= 2, \u0027BullswapLibrary: INVALID_PATH\u0027);
        amounts = new uint[](path.length);
        amounts[amounts.length - 1] = amountOut;
        for (uint i = path.length - 1; i \u003e 0; i--) {
            (uint reserveIn, uint reserveOut) = getReserves(factory, path[i - 1], path[i]);
            amounts[i - 1] = getAmountIn(amounts[i], reserveIn, reserveOut);
        }
    }
}
"},"BullswapRouterBase.sol":{"content":"pragma solidity =0.6.6;

import \u0027./IBullswapFactory.sol\u0027;
import \u0027./TransferHelper.sol\u0027;

import \u0027./BullswapLibrary.sol\u0027;
import \u0027./IBullswapRouterBase.sol\u0027;
import \u0027./IERC20.sol\u0027;
import \u0027./IWETH.sol\u0027;
import \"./IBullswapRouterMain.sol\";

contract BullswapRouterBase is IBullswapRouterBase {
address public immutable override factory;
address public immutable override WETH;

modifier ensure(uint deadline) {
    require(deadline \u003e= block.timestamp, \u0027BullswapRouter: EXPIRED\u0027);
    _;
}

constructor(address _factory, address _WETH) public {
    factory = _factory;
    WETH = _WETH;
}

receive() external payable {
    assert(msg.sender == WETH); // only accept ETH via fallback from the WETH contract
}

// **** ADD LIQUIDITY ****
function _addLiquidity(
    address tokenA,
    address tokenB,
    uint amountADesired,
    uint amountBDesired,
    uint amountAMin,
    uint amountBMin
) private returns (uint amountA, uint amountB) {
    // create the pair if it doesn\u0027t exist yet
    if (IBullswapFactory(factory).getPair(tokenA, tokenB) == address(0)) {
    IBullswapFactory(factory).createPair(tokenA, tokenB);
    }
    (uint reserveA, uint reserveB) = BullswapLibrary.getReserves(factory, tokenA, tokenB);
    if (reserveA == 0 \u0026\u0026 reserveB == 0) {
    (amountA, amountB) = (amountADesired, amountBDesired);
    } else {
    uint amountBOptimal = BullswapLibrary.quote(amountADesired, reserveA, reserveB);
    if (amountBOptimal \u003c= amountBDesired) {
    require(amountBOptimal \u003e= amountBMin, \u0027BullswapRouter: INSUFFICIENT_B_AMOUNT\u0027);
    (amountA, amountB) = (amountADesired, amountBOptimal);
    } else {
    uint amountAOptimal = BullswapLibrary.quote(amountBDesired, reserveB, reserveA);
    assert(amountAOptimal \u003c= amountADesired);
    require(amountAOptimal \u003e= amountAMin, \u0027BullswapRouter: INSUFFICIENT_A_AMOUNT\u0027);
    (amountA, amountB) = (amountAOptimal, amountBDesired);
    }
    }
}
function addLiquidity(
address tokenA,
address tokenB,
uint amountADesired,
uint amountBDesired,
uint amountAMin,
uint amountBMin,
address to,
uint deadline
) external override ensure(deadline) returns (uint amountA, uint amountB, uint liquidity) {
(amountA, amountB) = _addLiquidity(tokenA, tokenB, amountADesired, amountBDesired, amountAMin, amountBMin);
address pair = BullswapLibrary.pairFor(factory, tokenA, tokenB);
TransferHelper.safeTransferFrom(tokenA, msg.sender, pair, amountA);
TransferHelper.safeTransferFrom(tokenB, msg.sender, pair, amountB);
liquidity = IBullswapPair(pair).mint(to);
}
function addLiquidityETH(
address token,
uint amountTokenDesired,
uint amountTokenMin,
uint amountETHMin,
address to,
uint deadline
) external override payable ensure(deadline) returns (uint amountToken, uint amountETH, uint liquidity) {
(amountToken, amountETH) = _addLiquidity(
token,
WETH,
amountTokenDesired,
msg.value,
amountTokenMin,
amountETHMin
);
address pair = BullswapLibrary.pairFor(factory, token, WETH);
TransferHelper.safeTransferFrom(token, msg.sender, pair, amountToken);
IWETH(WETH).deposit{value : amountETH}();
assert(IWETH(WETH).transfer(pair, amountETH));
liquidity = IBullswapPair(pair).mint(to);
if (msg.value \u003e amountETH) TransferHelper.safeTransferETH(msg.sender, msg.value - amountETH); // refund dust eth, if any
}

// **** REMOVE LIQUIDITY ****
function removeLiquidity(
address tokenA,
address tokenB,
uint liquidity,
uint amountAMin,
uint amountBMin,
address to,
uint deadline
) public override ensure(deadline) returns (uint amountA, uint amountB) {
address pair = BullswapLibrary.pairFor(factory, tokenA, tokenB);
IBullswapPair(pair).transferFrom(msg.sender, pair, liquidity); // send liquidity to pair
(uint amount0, uint amount1) = IBullswapPair(pair).burn(to);
(address token0,) = BullswapLibrary.sortTokens(tokenA, tokenB);
(amountA, amountB) = tokenA == token0 ? (amount0, amount1) : (amount1, amount0);
require(amountA \u003e= amountAMin, \u0027BullswapRouter: INSUFFICIENT_A_AMOUNT\u0027);
require(amountB \u003e= amountBMin, \u0027BullswapRouter: INSUFFICIENT_B_AMOUNT\u0027);
}
function removeLiquidityETH(
address token,
uint liquidity,
uint amountTokenMin,
uint amountETHMin,
address to,
uint deadline
) public override ensure(deadline) returns (uint amountToken, uint amountETH) {
(amountToken, amountETH) = removeLiquidity(
token,
WETH,
liquidity,
amountTokenMin,
amountETHMin,
address(this),
deadline
);
TransferHelper.safeTransfer(token, to, amountToken);
IWETH(WETH).withdraw(amountETH);
TransferHelper.safeTransferETH(to, amountETH);
}
function removeLiquidityWithPermit(
address tokenA,
address tokenB,
uint liquidity,
uint amountAMin,
uint amountBMin,
address to,
uint deadline,
bool approveMax, uint8 v, bytes32 r, bytes32 s
) external override returns (uint amountA, uint amountB) {
address pair = BullswapLibrary.pairFor(factory, tokenA, tokenB);
uint value = approveMax ? uint(- 1) : liquidity;
IBullswapPair(pair).permit(msg.sender, address(this), value, deadline, v, r, s);
(amountA, amountB) = removeLiquidity(tokenA, tokenB, liquidity, amountAMin, amountBMin, to, deadline);
}
function removeLiquidityETHWithPermit(
address token,
uint liquidity,
uint amountTokenMin,
uint amountETHMin,
address to,
uint deadline,
bool approveMax, uint8 v, bytes32 r, bytes32 s
) external override returns (uint amountToken, uint amountETH) {
address pair = BullswapLibrary.pairFor(factory, token, WETH);
uint value = approveMax ? uint(- 1) : liquidity;
IBullswapPair(pair).permit(msg.sender, address(this), value, deadline, v, r, s);
(amountToken, amountETH) = removeLiquidityETH(token, liquidity, amountTokenMin, amountETHMin, to, deadline);
}

// **** SWAP ****
// requires the initial amount to have already been sent to the first pair
function _swap(uint[] memory amounts, address[] memory path, address _to) private {
for (uint i; i \u003c path.length - 1; i++) {
(address input, address output) = (path[i], path[i + 1]);
(address token0,) = BullswapLibrary.sortTokens(input, output);
uint amountOut = amounts[i + 1];
(uint amount0Out, uint amount1Out) = input == token0 ? (uint(0), amountOut) : (amountOut, uint(0));
address to = i \u003c path.length - 2 ? BullswapLibrary.pairFor(factory, output, path[i + 2]) : _to;
IBullswapPair(BullswapLibrary.pairFor(factory, input, output)).swap(amount0Out, amount1Out, to, new bytes(0));
}
}
function swapExactTokensForTokens(
uint amountIn,
uint amountOutMin,
address[] calldata path,
address to,
uint deadline
) external override ensure(deadline) returns (uint[] memory amounts) {
amounts = BullswapLibrary.getAmountsOut(factory, amountIn, path);
require(amounts[amounts.length - 1] \u003e= amountOutMin, \u0027BullswapRouter: INSUFFICIENT_OUTPUT_AMOUNT\u0027);
TransferHelper.safeTransferFrom(path[0], msg.sender, BullswapLibrary.pairFor(factory, path[0], path[1]), amounts[0]);
_swap(amounts, path, to);
}
function swapTokensForExactTokens(
uint amountOut,
uint amountInMax,
address[] calldata path,
address to,
uint deadline
) external override ensure(deadline) returns (uint[] memory amounts) {
amounts = BullswapLibrary.getAmountsIn(factory, amountOut, path);
require(amounts[0] \u003c= amountInMax, \u0027BullswapRouter: EXCESSIVE_INPUT_AMOUNT\u0027);
TransferHelper.safeTransferFrom(path[0], msg.sender, BullswapLibrary.pairFor(factory, path[0], path[1]), amounts[0]);
_swap(amounts, path, to);
}
function swapExactETHForTokens(uint amountOutMin, address[] calldata path, address to, uint deadline)
external
override
payable
ensure(deadline)
returns (uint[] memory amounts)
{
require(path[0] == WETH, \u0027BullswapRouter: INVALID_PATH\u0027);
amounts = BullswapLibrary.getAmountsOut(factory, msg.value, path);
require(amounts[amounts.length - 1] \u003e= amountOutMin, \u0027BullswapRouter: INSUFFICIENT_OUTPUT_AMOUNT\u0027);
IWETH(WETH).deposit{value : amounts[0]}();
assert(IWETH(WETH).transfer(BullswapLibrary.pairFor(factory, path[0], path[1]), amounts[0]));
_swap(amounts, path, to);
}
function swapTokensForExactETH(uint amountOut, uint amountInMax, address[] calldata path, address to, uint deadline)
external
override
ensure(deadline)
returns (uint[] memory amounts)
{
require(path[path.length - 1] == WETH, \u0027BullswapRouter: INVALID_PATH\u0027);
amounts = BullswapLibrary.getAmountsIn(factory, amountOut, path);
require(amounts[0] \u003c= amountInMax, \u0027BullswapRouter: EXCESSIVE_INPUT_AMOUNT\u0027);
TransferHelper.safeTransferFrom(path[0], msg.sender, BullswapLibrary.pairFor(factory, path[0], path[1]), amounts[0]);
_swap(amounts, path, address(this));
IWETH(WETH).withdraw(amounts[amounts.length - 1]);
TransferHelper.safeTransferETH(to, amounts[amounts.length - 1]);
}
function swapExactTokensForETH(uint amountIn, uint amountOutMin, address[] calldata path, address to, uint deadline)
external
override
ensure(deadline)
returns (uint[] memory amounts)
{
require(path[path.length - 1] == WETH, \u0027BullswapRouter: INVALID_PATH\u0027);
amounts = BullswapLibrary.getAmountsOut(factory, amountIn, path);
require(amounts[amounts.length - 1] \u003e= amountOutMin, \u0027BullswapRouter: INSUFFICIENT_OUTPUT_AMOUNT\u0027);
TransferHelper.safeTransferFrom(path[0], msg.sender, BullswapLibrary.pairFor(factory, path[0], path[1]), amounts[0]);
_swap(amounts, path, address(this));
IWETH(WETH).withdraw(amounts[amounts.length - 1]);
TransferHelper.safeTransferETH(to, amounts[amounts.length - 1]);
}
function swapETHForExactTokens(uint amountOut, address[] calldata path, address to, uint deadline)
external
override
payable
ensure(deadline)
returns (uint[] memory amounts)
{
require(path[0] == WETH, \u0027BullswapRouter: INVALID_PATH\u0027);
amounts = BullswapLibrary.getAmountsIn(factory, amountOut, path);
require(amounts[0] \u003c= msg.value, \u0027BullswapRouter: EXCESSIVE_INPUT_AMOUNT\u0027);
IWETH(WETH).deposit{value : amounts[0]}();
assert(IWETH(WETH).transfer(BullswapLibrary.pairFor(factory, path[0], path[1]), amounts[0]));
_swap(amounts, path, to);
if (msg.value \u003e amounts[0]) TransferHelper.safeTransferETH(msg.sender, msg.value - amounts[0]); // refund dust eth, if any
}

function quote(uint amountA, uint reserveA, uint reserveB) public pure override returns (uint amountB) {
return BullswapLibrary.quote(amountA, reserveA, reserveB);
}

function getAmountOut(uint amountIn, uint reserveIn, uint reserveOut) public pure override returns (uint amountOut) {
return BullswapLibrary.getAmountOut(amountIn, reserveIn, reserveOut);
}

function getAmountIn(uint amountOut, uint reserveIn, uint reserveOut) public pure override returns (uint amountIn) {
return BullswapLibrary.getAmountOut(amountOut, reserveIn, reserveOut);
}

function getAmountsOut(uint amountIn, address[] memory path) public view override returns (uint[] memory amounts) {
return BullswapLibrary.getAmountsOut(factory, amountIn, path);
}

function getAmountsIn(uint amountOut, address[] memory path) public view override returns (uint[] memory amounts) {
return BullswapLibrary.getAmountsIn(factory, amountOut, path);
}
}"},"BullswapRouterMain.sol":{"content":"pragma solidity =0.6.6;

import \u0027./IBullswapFactory.sol\u0027;
import \u0027./TransferHelper.sol\u0027;

import \u0027./IBullswapRouterMain.sol\u0027;
import \u0027./BullswapLibrary.sol\u0027;
import \u0027./SafeMath.sol\u0027;
import \u0027./IERC20.sol\u0027;
import \u0027./IWETH.sol\u0027;

contract BullswapRouterMain is IBullswapRouterMain {
    using SafeMath for uint;

    address public immutable override factory;
    address public immutable override WETH;

    modifier ensure(uint deadline) {
        require(deadline \u003e= block.timestamp, \u0027BullswapRouterMain: EXPIRED\u0027);
        _;
    }

    constructor(address _factory, address _WETH) public {
        factory = _factory;
        WETH = _WETH;
    }

    receive() external payable {
        assert(msg.sender == WETH); // only accept ETH via fallback from the WETH contract
    }

    // **** ADD LIQUIDITY ****
    function _addLiquidity(
        address tokenA,
        address tokenB,
        uint amountADesired,
        uint amountBDesired,
        uint amountAMin,
        uint amountBMin
    ) internal virtual returns (uint amountA, uint amountB) {
        // create the pair if it doesn\u0027t exist yet
        if (IBullswapFactory(factory).getPair(tokenA, tokenB) == address(0)) {
            IBullswapFactory(factory).createPair(tokenA, tokenB);
        }
        (uint reserveA, uint reserveB) = BullswapLibrary.getReserves(factory, tokenA, tokenB);
        if (reserveA == 0 \u0026\u0026 reserveB == 0) {
            (amountA, amountB) = (amountADesired, amountBDesired);
        } else {
            uint amountBOptimal = BullswapLibrary.quote(amountADesired, reserveA, reserveB);
            if (amountBOptimal \u003c= amountBDesired) {
                require(amountBOptimal \u003e= amountBMin, \u0027BullswapRouterMain: INSUFFICIENT_B_AMOUNT\u0027);
                (amountA, amountB) = (amountADesired, amountBOptimal);
            } else {
                uint amountAOptimal = BullswapLibrary.quote(amountBDesired, reserveB, reserveA);
                assert(amountAOptimal \u003c= amountADesired);
                require(amountAOptimal \u003e= amountAMin, \u0027BullswapRouterMain: INSUFFICIENT_A_AMOUNT\u0027);
                (amountA, amountB) = (amountAOptimal, amountBDesired);
            }
        }
    }
    function addLiquidity(
        address tokenA,
        address tokenB,
        uint amountADesired,
        uint amountBDesired,
        uint amountAMin,
        uint amountBMin,
        address to,
        uint deadline
    ) external virtual override ensure(deadline) returns (uint amountA, uint amountB, uint liquidity) {
        (amountA, amountB) = _addLiquidity(tokenA, tokenB, amountADesired, amountBDesired, amountAMin, amountBMin);
        address pair = BullswapLibrary.pairFor(factory, tokenA, tokenB);
        TransferHelper.safeTransferFrom(tokenA, msg.sender, pair, amountA);
        TransferHelper.safeTransferFrom(tokenB, msg.sender, pair, amountB);
        liquidity = IBullswapPair(pair).mint(to);
    }
    function addLiquidityETH(
        address token,
        uint amountTokenDesired,
        uint amountTokenMin,
        uint amountETHMin,
        address to,
        uint deadline
    ) external virtual override payable ensure(deadline) returns (uint amountToken, uint amountETH, uint liquidity) {
        (amountToken, amountETH) = _addLiquidity(
            token,
            WETH,
            amountTokenDesired,
            msg.value,
            amountTokenMin,
            amountETHMin
        );
        address pair = BullswapLibrary.pairFor(factory, token, WETH);
        TransferHelper.safeTransferFrom(token, msg.sender, pair, amountToken);
        IWETH(WETH).deposit{value: amountETH}();
        assert(IWETH(WETH).transfer(pair, amountETH));
        liquidity = IBullswapPair(pair).mint(to);
        // refund dust eth, if any
        if (msg.value \u003e amountETH) TransferHelper.safeTransferETH(msg.sender, msg.value - amountETH);
    }

    // **** REMOVE LIQUIDITY ****
    function removeLiquidity(
        address tokenA,
        address tokenB,
        uint liquidity,
        uint amountAMin,
        uint amountBMin,
        address to,
        uint deadline
    ) public virtual override ensure(deadline) returns (uint amountA, uint amountB) {
        address pair = BullswapLibrary.pairFor(factory, tokenA, tokenB);
        IBullswapPair(pair).transferFrom(msg.sender, pair, liquidity); // send liquidity to pair
        (uint amount0, uint amount1) = IBullswapPair(pair).burn(to);
        (address token0,) = BullswapLibrary.sortTokens(tokenA, tokenB);
        (amountA, amountB) = tokenA == token0 ? (amount0, amount1) : (amount1, amount0);
        require(amountA \u003e= amountAMin, \u0027BullswapRouterMain: INSUFFICIENT_A_AMOUNT\u0027);
        require(amountB \u003e= amountBMin, \u0027BullswapRouterMain: INSUFFICIENT_B_AMOUNT\u0027);
    }
    function removeLiquidityETH(
        address token,
        uint liquidity,
        uint amountTokenMin,
        uint amountETHMin,
        address to,
        uint deadline
    ) public virtual override ensure(deadline) returns (uint amountToken, uint amountETH) {
        (amountToken, amountETH) = removeLiquidity(
            token,
            WETH,
            liquidity,
            amountTokenMin,
            amountETHMin,
            address(this),
            deadline
        );
        TransferHelper.safeTransfer(token, to, amountToken);
        IWETH(WETH).withdraw(amountETH);
        TransferHelper.safeTransferETH(to, amountETH);
    }
    function removeLiquidityWithPermit(
        address tokenA,
        address tokenB,
        uint liquidity,
        uint amountAMin,
        uint amountBMin,
        address to,
        uint deadline,
        bool approveMax, uint8 v, bytes32 r, bytes32 s
    ) external virtual override returns (uint amountA, uint amountB) {
        address pair = BullswapLibrary.pairFor(factory, tokenA, tokenB);
        uint value = approveMax ? uint(-1) : liquidity;
        IBullswapPair(pair).permit(msg.sender, address(this), value, deadline, v, r, s);
        (amountA, amountB) = removeLiquidity(tokenA, tokenB, liquidity, amountAMin, amountBMin, to, deadline);
    }
    function removeLiquidityETHWithPermit(
        address token,
        uint liquidity,
        uint amountTokenMin,
        uint amountETHMin,
        address to,
        uint deadline,
        bool approveMax, uint8 v, bytes32 r, bytes32 s
    ) external virtual override returns (uint amountToken, uint amountETH) {
        address pair = BullswapLibrary.pairFor(factory, token, WETH);
        uint value = approveMax ? uint(-1) : liquidity;
        IBullswapPair(pair).permit(msg.sender, address(this), value, deadline, v, r, s);
        (amountToken, amountETH) = removeLiquidityETH(token, liquidity, amountTokenMin, amountETHMin, to, deadline);
    }

    // **** REMOVE LIQUIDITY (supporting fee-on-transfer tokens) ****
    function removeLiquidityETHSupportingFeeOnTransferTokens(
        address token,
        uint liquidity,
        uint amountTokenMin,
        uint amountETHMin,
        address to,
        uint deadline
    ) public virtual override ensure(deadline) returns (uint amountETH) {
        (, amountETH) = removeLiquidity(
            token,
            WETH,
            liquidity,
            amountTokenMin,
            amountETHMin,
            address(this),
            deadline
        );
        TransferHelper.safeTransfer(token, to, IERC20(token).balanceOf(address(this)));
        IWETH(WETH).withdraw(amountETH);
        TransferHelper.safeTransferETH(to, amountETH);
    }
    function removeLiquidityETHWithPermitSupportingFeeOnTransferTokens(
        address token,
        uint liquidity,
        uint amountTokenMin,
        uint amountETHMin,
        address to,
        uint deadline,
        bool approveMax, uint8 v, bytes32 r, bytes32 s
    ) external virtual override returns (uint amountETH) {
        address pair = BullswapLibrary.pairFor(factory, token, WETH);
        uint value = approveMax ? uint(-1) : liquidity;
        IBullswapPair(pair).permit(msg.sender, address(this), value, deadline, v, r, s);
        amountETH = removeLiquidityETHSupportingFeeOnTransferTokens(
            token, liquidity, amountTokenMin, amountETHMin, to, deadline
        );
    }

    // **** SWAP ****
    // requires the initial amount to have already been sent to the first pair
    function _swap(uint[] memory amounts, address[] memory path, address _to) internal virtual {
        for (uint i; i \u003c path.length - 1; i++) {
            (address input, address output) = (path[i], path[i + 1]);
            (address token0,) = BullswapLibrary.sortTokens(input, output);
            uint amountOut = amounts[i + 1];
            (uint amount0Out, uint amount1Out) = input == token0 ? (uint(0), amountOut) : (amountOut, uint(0));
            address to = i \u003c path.length - 2 ? BullswapLibrary.pairFor(factory, output, path[i + 2]) : _to;
            IBullswapPair(BullswapLibrary.pairFor(factory, input, output)).swap(
                amount0Out, amount1Out, to, new bytes(0)
            );
        }
    }
    function swapExactTokensForTokens(
        uint amountIn,
        uint amountOutMin,
        address[] calldata path,
        address to,
        uint deadline
    ) external virtual override ensure(deadline) returns (uint[] memory amounts) {
        amounts = BullswapLibrary.getAmountsOut(factory, amountIn, path);
        require(amounts[amounts.length - 1] \u003e= amountOutMin, \u0027BullswapRouterMain: INSUFFICIENT_OUTPUT_AMOUNT\u0027);
        TransferHelper.safeTransferFrom(
            path[0], msg.sender, BullswapLibrary.pairFor(factory, path[0], path[1]), amounts[0]
        );
        _swap(amounts, path, to);
    }
    function swapTokensForExactTokens(
        uint amountOut,
        uint amountInMax,
        address[] calldata path,
        address to,
        uint deadline
    ) external virtual override ensure(deadline) returns (uint[] memory amounts) {
        amounts = BullswapLibrary.getAmountsIn(factory, amountOut, path);
        require(amounts[0] \u003c= amountInMax, \u0027BullswapRouterMain: EXCESSIVE_INPUT_AMOUNT\u0027);
        TransferHelper.safeTransferFrom(
            path[0], msg.sender, BullswapLibrary.pairFor(factory, path[0], path[1]), amounts[0]
        );
        _swap(amounts, path, to);
    }
    function swapExactETHForTokens(uint amountOutMin, address[] calldata path, address to, uint deadline)
        external
        virtual
        override
        payable
        ensure(deadline)
        returns (uint[] memory amounts)
    {
        require(path[0] == WETH, \u0027BullswapRouterMain: INVALID_PATH\u0027);
        amounts = BullswapLibrary.getAmountsOut(factory, msg.value, path);
        require(amounts[amounts.length - 1] \u003e= amountOutMin, \u0027BullswapRouterMain: INSUFFICIENT_OUTPUT_AMOUNT\u0027);
        IWETH(WETH).deposit{value: amounts[0]}();
        assert(IWETH(WETH).transfer(BullswapLibrary.pairFor(factory, path[0], path[1]), amounts[0]));
        _swap(amounts, path, to);
    }
    function swapTokensForExactETH(uint amountOut, uint amountInMax, address[] calldata path, address to, uint deadline)
        external
        virtual
        override
        ensure(deadline)
        returns (uint[] memory amounts)
    {
        require(path[path.length - 1] == WETH, \u0027BullswapRouterMain: INVALID_PATH\u0027);
        amounts = BullswapLibrary.getAmountsIn(factory, amountOut, path);
        require(amounts[0] \u003c= amountInMax, \u0027BullswapRouterMain: EXCESSIVE_INPUT_AMOUNT\u0027);
        TransferHelper.safeTransferFrom(
            path[0], msg.sender, BullswapLibrary.pairFor(factory, path[0], path[1]), amounts[0]
        );
        _swap(amounts, path, address(this));
        IWETH(WETH).withdraw(amounts[amounts.length - 1]);
        TransferHelper.safeTransferETH(to, amounts[amounts.length - 1]);
    }
    function swapExactTokensForETH(uint amountIn, uint amountOutMin, address[] calldata path, address to, uint deadline)
        external
        virtual
        override
        ensure(deadline)
        returns (uint[] memory amounts)
    {
        require(path[path.length - 1] == WETH, \u0027BullswapRouterMain: INVALID_PATH\u0027);
        amounts = BullswapLibrary.getAmountsOut(factory, amountIn, path);
        require(amounts[amounts.length - 1] \u003e= amountOutMin, \u0027BullswapRouterMain: INSUFFICIENT_OUTPUT_AMOUNT\u0027);
        TransferHelper.safeTransferFrom(
            path[0], msg.sender, BullswapLibrary.pairFor(factory, path[0], path[1]), amounts[0]
        );
        _swap(amounts, path, address(this));
        IWETH(WETH).withdraw(amounts[amounts.length - 1]);
        TransferHelper.safeTransferETH(to, amounts[amounts.length - 1]);
    }
    function swapETHForExactTokens(uint amountOut, address[] calldata path, address to, uint deadline)
        external
        virtual
        override
        payable
        ensure(deadline)
        returns (uint[] memory amounts)
    {
        require(path[0] == WETH, \u0027BullswapRouterMain: INVALID_PATH\u0027);
        amounts = BullswapLibrary.getAmountsIn(factory, amountOut, path);
        require(amounts[0] \u003c= msg.value, \u0027BullswapRouterMain: EXCESSIVE_INPUT_AMOUNT\u0027);
        IWETH(WETH).deposit{value: amounts[0]}();
        assert(IWETH(WETH).transfer(BullswapLibrary.pairFor(factory, path[0], path[1]), amounts[0]));
        _swap(amounts, path, to);
        // refund dust eth, if any
        if (msg.value \u003e amounts[0]) TransferHelper.safeTransferETH(msg.sender, msg.value - amounts[0]);
    }

    // **** SWAP (supporting fee-on-transfer tokens) ****
    // requires the initial amount to have already been sent to the first pair
    function _swapSupportingFeeOnTransferTokens(address[] memory path, address _to) internal virtual {
        for (uint i; i \u003c path.length - 1; i++) {
            (address input, address output) = (path[i], path[i + 1]);
            (address token0,) = BullswapLibrary.sortTokens(input, output);
            IBullswapPair pair = IBullswapPair(BullswapLibrary.pairFor(factory, input, output));
            uint amountInput;
            uint amountOutput;
            { // scope to avoid stack too deep errors
            (uint reserve0, uint reserve1,) = pair.getReserves();
            (uint reserveInput, uint reserveOutput) = input == token0 ? (reserve0, reserve1) : (reserve1, reserve0);
            amountInput = IERC20(input).balanceOf(address(pair)).sub(reserveInput);
            amountOutput = BullswapLibrary.getAmountOut(amountInput, reserveInput, reserveOutput);
            }
            (uint amount0Out, uint amount1Out) = input == token0 ? (uint(0), amountOutput) : (amountOutput, uint(0));
            address to = i \u003c path.length - 2 ? BullswapLibrary.pairFor(factory, output, path[i + 2]) : _to;
            pair.swap(amount0Out, amount1Out, to, new bytes(0));
        }
    }
    function swapExactTokensForTokensSupportingFeeOnTransferTokens(
        uint amountIn,
        uint amountOutMin,
        address[] calldata path,
        address to,
        uint deadline
    ) external virtual override ensure(deadline) {
        TransferHelper.safeTransferFrom(
            path[0], msg.sender, BullswapLibrary.pairFor(factory, path[0], path[1]), amountIn
        );
        uint balanceBefore = IERC20(path[path.length - 1]).balanceOf(to);
        _swapSupportingFeeOnTransferTokens(path, to);
        require(
            IERC20(path[path.length - 1]).balanceOf(to).sub(balanceBefore) \u003e= amountOutMin,
            \u0027BullswapRouterMain: INSUFFICIENT_OUTPUT_AMOUNT\u0027
        );
    }
    function swapExactETHForTokensSupportingFeeOnTransferTokens(
        uint amountOutMin,
        address[] calldata path,
        address to,
        uint deadline
    )
        external
        virtual
        override
        payable
        ensure(deadline)
    {
        require(path[0] == WETH, \u0027BullswapRouterMain: INVALID_PATH\u0027);
        uint amountIn = msg.value;
        IWETH(WETH).deposit{value: amountIn}();
        assert(IWETH(WETH).transfer(BullswapLibrary.pairFor(factory, path[0], path[1]), amountIn));
        uint balanceBefore = IERC20(path[path.length - 1]).balanceOf(to);
        _swapSupportingFeeOnTransferTokens(path, to);
        require(
            IERC20(path[path.length - 1]).balanceOf(to).sub(balanceBefore) \u003e= amountOutMin,
            \u0027BullswapRouterMain: INSUFFICIENT_OUTPUT_AMOUNT\u0027
        );
    }
    function swapExactTokensForETHSupportingFeeOnTransferTokens(
        uint amountIn,
        uint amountOutMin,
        address[] calldata path,
        address to,
        uint deadline
    )
        external
        virtual
        override
        ensure(deadline)
    {
        require(path[path.length - 1] == WETH, \u0027BullswapRouterMain: INVALID_PATH\u0027);
        TransferHelper.safeTransferFrom(
            path[0], msg.sender, BullswapLibrary.pairFor(factory, path[0], path[1]), amountIn
        );
        _swapSupportingFeeOnTransferTokens(path, address(this));
        uint amountOut = IERC20(WETH).balanceOf(address(this));
        require(amountOut \u003e= amountOutMin, \u0027BullswapRouterMain: INSUFFICIENT_OUTPUT_AMOUNT\u0027);
        IWETH(WETH).withdraw(amountOut);
        TransferHelper.safeTransferETH(to, amountOut);
    }

    // **** LIBRARY FUNCTIONS ****
    function quote(uint amountA, uint reserveA, uint reserveB) public pure virtual override returns (uint amountB) {
        return BullswapLibrary.quote(amountA, reserveA, reserveB);
    }

    function getAmountOut(uint amountIn, uint reserveIn, uint reserveOut)
        public
        pure
        virtual
        override
        returns (uint amountOut)
    {
        return BullswapLibrary.getAmountOut(amountIn, reserveIn, reserveOut);
    }

    function getAmountIn(uint amountOut, uint reserveIn, uint reserveOut)
        public
        pure
        virtual
        override
        returns (uint amountIn)
    {
        return BullswapLibrary.getAmountIn(amountOut, reserveIn, reserveOut);
    }

    function getAmountsOut(uint amountIn, address[] memory path)
        public
        view
        virtual
        override
        returns (uint[] memory amounts)
    {
        return BullswapLibrary.getAmountsOut(factory, amountIn, path);
    }

    function getAmountsIn(uint amountOut, address[] memory path)
        public
        view
        virtual
        override
        returns (uint[] memory amounts)
    {
        return BullswapLibrary.getAmountsIn(factory, amountOut, path);
    }
}
"},"IBullswapFactory.sol":{"content":"pragma solidity \u003e=0.5.0;

interface IBullswapFactory {
    event PairCreated(address indexed token0, address indexed token1, address pair, uint);

    function feeTo() external view returns (address);
    function feeToSetter() external view returns (address);

    function getPair(address tokenA, address tokenB) external view returns (address pair);
    function allPairs(uint) external view returns (address pair);
    function allPairsLength() external view returns (uint);

    function createPair(address tokenA, address tokenB) external returns (address pair);

    function setFeeTo(address) external;
    function setFeeToSetter(address) external;
}
"},"IBullswapPair.sol":{"content":"pragma solidity \u003e=0.5.0;

interface IBullswapPair {
    event Approval(address indexed owner, address indexed spender, uint value);
    event Transfer(address indexed from, address indexed to, uint value);

    function name() external pure returns (string memory);
    function symbol() external pure returns (string memory);
    function decimals() external pure returns (uint8);
    function totalSupply() external view returns (uint);
    function balanceOf(address owner) external view returns (uint);
    function allowance(address owner, address spender) external view returns (uint);

    function approve(address spender, uint value) external returns (bool);
    function transfer(address to, uint value) external returns (bool);
    function transferFrom(address from, address to, uint value) external returns (bool);

    function DOMAIN_SEPARATOR() external view returns (bytes32);
    function PERMIT_TYPEHASH() external pure returns (bytes32);
    function nonces(address owner) external view returns (uint);

    function permit(address owner, address spender, uint value, uint deadline, uint8 v, bytes32 r, bytes32 s) external;

    event Mint(address indexed sender, uint amount0, uint amount1);
    event Burn(address indexed sender, uint amount0, uint amount1, address indexed to);
    event Swap(
        address indexed sender,
        uint amount0In,
        uint amount1In,
        uint amount0Out,
        uint amount1Out,
        address indexed to
    );
    event Sync(uint112 reserve0, uint112 reserve1);

    function MINIMUM_LIQUIDITY() external pure returns (uint);
    function factory() external view returns (address);
    function token0() external view returns (address);
    function token1() external view returns (address);
    function getReserves() external view returns (uint112 reserve0, uint112 reserve1, uint32 blockTimestampLast);
    function price0CumulativeLast() external view returns (uint);
    function price1CumulativeLast() external view returns (uint);
    function kLast() external view returns (uint);

    function mint(address to) external returns (uint liquidity);
    function burn(address to) external returns (uint amount0, uint amount1);
    function swap(uint amount0Out, uint amount1Out, address to, bytes calldata data) external;
    function skim(address to) external;
    function sync() external;

    function initialize(address, address) external;
}
"},"IBullswapRouterBase.sol":{"content":"pragma solidity \u003e=0.6.2;

interface IBullswapRouterBase {
    function factory() external pure returns (address);
    function WETH() external pure returns (address);

    function addLiquidity(
        address tokenA,
        address tokenB,
        uint amountADesired,
        uint amountBDesired,
        uint amountAMin,
        uint amountBMin,
        address to,
        uint deadline
    ) external returns (uint amountA, uint amountB, uint liquidity);
    function addLiquidityETH(
        address token,
        uint amountTokenDesired,
        uint amountTokenMin,
        uint amountETHMin,
        address to,
        uint deadline
    ) external payable returns (uint amountToken, uint amountETH, uint liquidity);
    function removeLiquidity(
        address tokenA,
        address tokenB,
        uint liquidity,
        uint amountAMin,
        uint amountBMin,
        address to,
        uint deadline
    ) external returns (uint amountA, uint amountB);
    function removeLiquidityETH(
        address token,
        uint liquidity,
        uint amountTokenMin,
        uint amountETHMin,
        address to,
        uint deadline
    ) external returns (uint amountToken, uint amountETH);
    function removeLiquidityWithPermit(
        address tokenA,
        address tokenB,
        uint liquidity,
        uint amountAMin,
        uint amountBMin,
        address to,
        uint deadline,
        bool approveMax, uint8 v, bytes32 r, bytes32 s
    ) external returns (uint amountA, uint amountB);
    function removeLiquidityETHWithPermit(
        address token,
        uint liquidity,
        uint amountTokenMin,
        uint amountETHMin,
        address to,
        uint deadline,
        bool approveMax, uint8 v, bytes32 r, bytes32 s
    ) external returns (uint amountToken, uint amountETH);
    function swapExactTokensForTokens(
        uint amountIn,
        uint amountOutMin,
        address[] calldata path,
        address to,
        uint deadline
    ) external returns (uint[] memory amounts);
    function swapTokensForExactTokens(
        uint amountOut,
        uint amountInMax,
        address[] calldata path,
        address to,
        uint deadline
    ) external returns (uint[] memory amounts);
    function swapExactETHForTokens(uint amountOutMin, address[] calldata path, address to, uint deadline)
    external
    payable
    returns (uint[] memory amounts);
    function swapTokensForExactETH(uint amountOut, uint amountInMax, address[] calldata path, address to, uint deadline)
    external
    returns (uint[] memory amounts);
    function swapExactTokensForETH(uint amountIn, uint amountOutMin, address[] calldata path, address to, uint deadline)
    external
    returns (uint[] memory amounts);
    function swapETHForExactTokens(uint amountOut, address[] calldata path, address to, uint deadline)
    external
    payable
    returns (uint[] memory amounts);

    function quote(uint amountA, uint reserveA, uint reserveB) external pure returns (uint amountB);
    function getAmountOut(uint amountIn, uint reserveIn, uint reserveOut) external pure returns (uint amountOut);
    function getAmountIn(uint amountOut, uint reserveIn, uint reserveOut) external pure returns (uint amountIn);
    function getAmountsOut(uint amountIn, address[] calldata path) external view returns (uint[] memory amounts);
    function getAmountsIn(uint amountOut, address[] calldata path) external view returns (uint[] memory amounts);
}"},"IBullswapRouterMain.sol":{"content":"pragma solidity \u003e=0.6.2;

import \u0027./IBullswapRouterBase.sol\u0027;

interface IBullswapRouterMain is IBullswapRouterBase {

    function removeLiquidityETHSupportingFeeOnTransferTokens(
        address token,
        uint liquidity,
        uint amountTokenMin,
        uint amountETHMin,
        address to,
        uint deadline
    ) external returns (uint amountETH);
    function removeLiquidityETHWithPermitSupportingFeeOnTransferTokens(
        address token,
        uint liquidity,
        uint amountTokenMin,
        uint amountETHMin,
        address to,
        uint deadline,
        bool approveMax, uint8 v, bytes32 r, bytes32 s
    ) external returns (uint amountETH);

    function swapExactTokensForTokensSupportingFeeOnTransferTokens(
        uint amountIn,
        uint amountOutMin,
        address[] calldata path,
        address to,
        uint deadline
    ) external;
    function swapExactETHForTokensSupportingFeeOnTransferTokens(
        uint amountOutMin,
        address[] calldata path,
        address to,
        uint deadline
    ) external payable;
    function swapExactTokensForETHSupportingFeeOnTransferTokens(
        uint amountIn,
        uint amountOutMin,
        address[] calldata path,
        address to,
        uint deadline
    ) external;
}
"},"IERC20.sol":{"content":"pragma solidity \u003e=0.5.0;

interface IERC20 {
    event Approval(address indexed owner, address indexed spender, uint value);
    event Transfer(address indexed from, address indexed to, uint value);

    function name() external view returns (string memory);
    function symbol() external view returns (string memory);
    function decimals() external view returns (uint8);
    function totalSupply() external view returns (uint);
    function balanceOf(address owner) external view returns (uint);
    function allowance(address owner, address spender) external view returns (uint);

    function approve(address spender, uint value) external returns (bool);
    function transfer(address to, uint value) external returns (bool);
    function transferFrom(address from, address to, uint value) external returns (bool);
}
"},"IWETH.sol":{"content":"pragma solidity \u003e=0.5.0;

interface IWETH {
    function deposit() external payable;
    function transfer(address to, uint value) external returns (bool);
    function withdraw(uint) external;
}
"},"SafeMath.sol":{"content":"pragma solidity =0.6.6;

// a library for performing overflow-safe math, courtesy of DappHub (https://github.com/dapphub/ds-math)

library SafeMath {
    function add(uint x, uint y) internal pure returns (uint z) {
        require((z = x + y) \u003e= x, \u0027ds-math-add-overflow\u0027);
    }

    function sub(uint x, uint y) internal pure returns (uint z) {
        require((z = x - y) \u003c= x, \u0027ds-math-sub-underflow\u0027);
    }

    function mul(uint x, uint y) internal pure returns (uint z) {
        require(y == 0 || (z = x * y) / y == x, \u0027ds-math-mul-overflow\u0027);
    }
}
"},"TransferHelper.sol":{"content":"// SPDX-License-Identifier: GPL-3.0-or-later

pragma solidity \u003e=0.6.0;

// helper methods for interacting with ERC20 tokens and sending ETH that do not consistently return true/false
library TransferHelper {
    function safeApprove(address token, address to, uint value) internal {
        // bytes4(keccak256(bytes(\u0027approve(address,uint256)\u0027)));
        (bool success, bytes memory data) = token.call(abi.encodeWithSelector(0x095ea7b3, to, value));
        require(success \u0026\u0026 (data.length == 0 || abi.decode(data, (bool))), \u0027TransferHelper: APPROVE_FAILED\u0027);
    }

    function safeTransfer(address token, address to, uint value) internal {
        // bytes4(keccak256(bytes(\u0027transfer(address,uint256)\u0027)));
        (bool success, bytes memory data) = token.call(abi.encodeWithSelector(0xa9059cbb, to, value));
        require(success \u0026\u0026 (data.length == 0 || abi.decode(data, (bool))), \u0027TransferHelper: TRANSFER_FAILED\u0027);
    }

    function safeTransferFrom(address token, address from, address to, uint value) internal {
        // bytes4(keccak256(bytes(\u0027transferFrom(address,address,uint256)\u0027)));
        (bool success, bytes memory data) = token.call(abi.encodeWithSelector(0x23b872dd, from, to, value));
        require(success \u0026\u0026 (data.length == 0 || abi.decode(data, (bool))), \u0027TransferHelper: TRANSFER_FROM_FAILED\u0027);
    }

    function safeTransferETH(address to, uint value) internal {
        (bool success,) = to.call{value:value}(new bytes(0));
        require(success, \u0027TransferHelper: ETH_TRANSFER_FAILED\u0027);
    }
}

