// SPDX-License-Identifier: MIT

pragma solidity ^0.6.0;

/*
 * @dev Provides information about the current execution context, including the
 * sender of the transaction and its data. While these are generally available
 * via msg.sender and msg.data, they should not be accessed in such a direct
 * manner, since when dealing with GSN meta-transactions the account sending and
 * paying for execution may not be the actual sender (as far as an application
 * is concerned).
 *
 * This contract is only required for intermediate, library-like contracts.
 */
abstract contract Context {
    function _msgSender() internal view virtual returns (address payable) {
        return msg.sender;
    }

    function _msgData() internal view virtual returns (bytes memory) {
        this; // silence state mutability warning without generating bytecode - see https://github.com/ethereum/solidity/issues/2691
        return msg.data;
    }
}
"},"ContractRegistryAccessor.sol":{"content":"// SPDX-License-Identifier: UNLICENSED

pragma solidity 0.6.12;

import \"./IContractRegistry.sol\";
import \"./WithClaimableRegistryManagement.sol\";
import \"./Initializable.sol\";

contract ContractRegistryAccessor is WithClaimableRegistryManagement, Initializable {

    IContractRegistry private contractRegistry;

    constructor(IContractRegistry _contractRegistry, address _registryAdmin) public {
        require(address(_contractRegistry) != address(0), \"_contractRegistry cannot be 0\");
        setContractRegistry(_contractRegistry);
        _transferRegistryManagement(_registryAdmin);
    }

    modifier onlyAdmin {
        require(isAdmin(), \"sender is not an admin (registryManger or initializationAdmin)\");

        _;
    }

    function isManager(string memory role) internal view returns (bool) {
        IContractRegistry _contractRegistry = contractRegistry;
        return isAdmin() || _contractRegistry != IContractRegistry(0) \u0026\u0026 contractRegistry.getManager(role) == msg.sender;
    }

    function isAdmin() internal view returns (bool) {
        return msg.sender == registryAdmin() || msg.sender == initializationAdmin() || msg.sender == address(contractRegistry);
    }

    function getProtocolContract() internal view returns (address) {
        return contractRegistry.getContract(\"protocol\");
    }

    function getStakingRewardsContract() internal view returns (address) {
        return contractRegistry.getContract(\"stakingRewards\");
    }

    function getFeesAndBootstrapRewardsContract() internal view returns (address) {
        return contractRegistry.getContract(\"feesAndBootstrapRewards\");
    }

    function getCommitteeContract() internal view returns (address) {
        return contractRegistry.getContract(\"committee\");
    }

    function getElectionsContract() internal view returns (address) {
        return contractRegistry.getContract(\"elections\");
    }

    function getDelegationsContract() internal view returns (address) {
        return contractRegistry.getContract(\"delegations\");
    }

    function getGuardiansRegistrationContract() internal view returns (address) {
        return contractRegistry.getContract(\"guardiansRegistration\");
    }

    function getCertificationContract() internal view returns (address) {
        return contractRegistry.getContract(\"certification\");
    }

    function getStakingContract() internal view returns (address) {
        return contractRegistry.getContract(\"staking\");
    }

    function getSubscriptionsContract() internal view returns (address) {
        return contractRegistry.getContract(\"subscriptions\");
    }

    function getStakingRewardsWallet() internal view returns (address) {
        return contractRegistry.getContract(\"stakingRewardsWallet\");
    }

    function getBootstrapRewardsWallet() internal view returns (address) {
        return contractRegistry.getContract(\"bootstrapRewardsWallet\");
    }

    function getGeneralFeesWallet() internal view returns (address) {
        return contractRegistry.getContract(\"generalFeesWallet\");
    }

    function getCertifiedFeesWallet() internal view returns (address) {
        return contractRegistry.getContract(\"certifiedFeesWallet\");
    }

    function getStakingContractHandler() internal view returns (address) {
        return contractRegistry.getContract(\"stakingContractHandler\");
    }

    /*
    * Governance functions
    */

    event ContractRegistryAddressUpdated(address addr);

    function setContractRegistry(IContractRegistry newContractRegistry) public onlyAdmin {
        require(newContractRegistry.getPreviousContractRegistry() == address(contractRegistry), \"new contract registry must provide the previous contract registry\");
        contractRegistry = newContractRegistry;
        emit ContractRegistryAddressUpdated(address(newContractRegistry));
    }

    function getContractRegistry() public view returns (IContractRegistry) {
        return contractRegistry;
    }

}
"},"GuardiansRegistration.sol":{"content":"// SPDX-License-Identifier: UNLICENSED

pragma solidity 0.6.12;

import \"./IGuardiansRegistration.sol\";
import \"./IElections.sol\";
import \"./ManagedContract.sol\";

contract GuardiansRegistration is IGuardiansRegistration, ManagedContract {

\tstruct Guardian {
\t\taddress orbsAddr;
\t\tbytes4 ip;
\t\tuint32 registrationTime;
\t\tuint32 lastUpdateTime;
\t\tstring name;
\t\tstring website;
\t}
\tmapping(address =\u003e Guardian) guardians;
\tmapping(address =\u003e address) orbsAddressToGuardianAddress;
\tmapping(bytes4 =\u003e address) public ipToGuardian;
\tmapping(address =\u003e mapping(string =\u003e string)) guardianMetadata;

\tconstructor(IContractRegistry _contractRegistry, address _registryAdmin) ManagedContract(_contractRegistry, _registryAdmin) public {}

\tmodifier onlyRegisteredGuardian {
\t\trequire(isRegistered(msg.sender), \"Guardian is not registered\");

\t\t_;
\t}

\t/*
     * External methods
     */

    /// @dev Called by a participant who wishes to register as a guardian
\tfunction registerGuardian(bytes4 ip, address orbsAddr, string calldata name, string calldata website) external override onlyWhenActive {
\t\trequire(!isRegistered(msg.sender), \"registerGuardian: Guardian is already registered\");

\t\tguardians[msg.sender].registrationTime = uint32(block.timestamp);
\t\temit GuardianRegistered(msg.sender);

\t\t_updateGuardian(msg.sender, ip, orbsAddr, name, website);
\t}

    /// @dev Called by a participant who wishes to update its properties
\tfunction updateGuardian(bytes4 ip, address orbsAddr, string calldata name, string calldata website) external override onlyRegisteredGuardian onlyWhenActive {
\t\t_updateGuardian(msg.sender, ip, orbsAddr, name, website);
\t}

\tfunction updateGuardianIp(bytes4 ip) external override onlyWhenActive {
\t\taddress guardianAddr = resolveGuardianAddress(msg.sender);
\t\tGuardian memory data = guardians[guardianAddr];
\t\t_updateGuardian(guardianAddr, ip, data.orbsAddr, data.name, data.website);
\t}

    /// @dev Called by a guardian to update additional guardian metadata properties.
    function setMetadata(string calldata key, string calldata value) external override onlyRegisteredGuardian onlyWhenActive {
\t\t_setMetadata(msg.sender, key, value);
\t}

\tfunction getMetadata(address guardian, string calldata key) external override view returns (string memory) {
\t\treturn guardianMetadata[guardian][key];
\t}

\t/// @dev Called by a participant who wishes to unregister
\tfunction unregisterGuardian() external override onlyRegisteredGuardian onlyWhenActive {
\t\tdelete orbsAddressToGuardianAddress[guardians[msg.sender].orbsAddr];
\t\tdelete ipToGuardian[guardians[msg.sender].ip];
\t\tGuardian memory guardian = guardians[msg.sender];
\t\tdelete guardians[msg.sender];

\t\telectionsContract.guardianUnregistered(msg.sender);
\t\temit GuardianDataUpdated(msg.sender, false, guardian.ip, guardian.orbsAddr, guardian.name, guardian.website);
\t\temit GuardianUnregistered(msg.sender);
\t}

    /// @dev Returns a guardian\u0027s data
\tfunction getGuardianData(address guardian) external override view returns (bytes4 ip, address orbsAddr, string memory name, string memory website, uint registrationTime, uint lastUpdateTime) {
\t\tGuardian memory v = guardians[guardian];
\t\treturn (v.ip, v.orbsAddr, v.name, v.website, v.registrationTime, v.lastUpdateTime);
\t}

\tfunction getGuardiansOrbsAddress(address[] calldata guardianAddrs) external override view returns (address[] memory orbsAddrs) {
\t\torbsAddrs = new address[](guardianAddrs.length);
\t\tfor (uint i = 0; i \u003c guardianAddrs.length; i++) {
\t\t\torbsAddrs[i] = guardians[guardianAddrs[i]].orbsAddr;
\t\t}
\t}

\tfunction getGuardianIp(address guardian) external override view returns (bytes4 ip) {
\t\treturn guardians[guardian].ip;
\t}

\tfunction getGuardianIps(address[] calldata guardianAddrs) external override view returns (bytes4[] memory ips) {
\t\tips = new bytes4[](guardianAddrs.length);
\t\tfor (uint i = 0; i \u003c guardianAddrs.length; i++) {
\t\t\tips[i] = guardians[guardianAddrs[i]].ip;
\t\t}
\t}

\tfunction isRegistered(address guardian) public override view returns (bool) {
\t\treturn guardians[guardian].registrationTime != 0;
\t}

\tfunction resolveGuardianAddress(address guardianOrOrbsAddress) public override view returns (address guardianAddress) {
\t\tif (isRegistered(guardianOrOrbsAddress)) {
\t\t\tguardianAddress = guardianOrOrbsAddress;
\t\t} else {
\t\t\tguardianAddress = orbsAddressToGuardianAddress[guardianOrOrbsAddress];
\t\t}

\t\trequire(guardianAddress != address(0), \"Cannot resolve address\");
\t}

\t/// @dev Translates a list guardians Orbs addresses to Ethereum addresses
\tfunction getGuardianAddresses(address[] calldata orbsAddrs) external override view returns (address[] memory guardianAddrs) {
\t\tguardianAddrs = new address[](orbsAddrs.length);
\t\tfor (uint i = 0; i \u003c orbsAddrs.length; i++) {
\t\t\tguardianAddrs[i] = orbsAddressToGuardianAddress[orbsAddrs[i]];
\t\t}
\t}

\t/*
\t * Governance
\t */

\tfunction migrateGuardians(address[] calldata guardiansToMigrate, IGuardiansRegistration previousContract) external override onlyInitializationAdmin {
\t\trequire(previousContract != IGuardiansRegistration(0), \"previousContract must not be the zero address\");

\t\tfor (uint i = 0; i \u003c guardiansToMigrate.length; i++) {
\t\t\trequire(guardiansToMigrate[i] != address(0), \"guardian must not be the zero address\");
\t\t\tmigrateGuardianData(previousContract, guardiansToMigrate[i]);
\t\t\tmigrateGuardianMetadata(previousContract, guardiansToMigrate[i]);
\t\t}
\t}

\t/*
\t * Private methods
\t */

\tfunction _updateGuardian(address guardianAddr, bytes4 ip, address orbsAddr, string memory name, string memory website) private {
\t\trequire(orbsAddr != address(0), \"orbs address must be non zero\");
\t\trequire(orbsAddr != guardianAddr, \"orbs address must be different than the guardian address\");
\t\trequire(!isRegistered(orbsAddr), \"orbs address must not be a guardian address of a registered guardian\");
\t\trequire(bytes(name).length != 0, \"name must be given\");

\t\tdelete ipToGuardian[guardians[guardianAddr].ip];
\t\trequire(ipToGuardian[ip] == address(0), \"ip is already in use\");
\t\tipToGuardian[ip] = guardianAddr;

\t\tdelete orbsAddressToGuardianAddress[guardians[guardianAddr].orbsAddr];
\t\trequire(orbsAddressToGuardianAddress[orbsAddr] == address(0), \"orbs address is already in use\");
\t\torbsAddressToGuardianAddress[orbsAddr] = guardianAddr;

\t\tguardians[guardianAddr].orbsAddr = orbsAddr;
\t\tguardians[guardianAddr].ip = ip;
\t\tguardians[guardianAddr].name = name;
\t\tguardians[guardianAddr].website = website;
\t\tguardians[guardianAddr].lastUpdateTime = uint32(block.timestamp);

        emit GuardianDataUpdated(guardianAddr, true, ip, orbsAddr, name, website);
    }

\tfunction _setMetadata(address guardian, string memory key, string memory value) private {
\t\tstring memory oldValue = guardianMetadata[guardian][key];
\t\tguardianMetadata[guardian][key] = value;
\t\temit GuardianMetadataChanged(guardian, key, value, oldValue);
\t}

\tfunction migrateGuardianData(IGuardiansRegistration previousContract, address guardianAddress) private {
\t\t(bytes4 ip, address orbsAddr, string memory name, string memory website, uint registrationTime, uint lastUpdateTime) = previousContract.getGuardianData(guardianAddress);
\t\tguardians[guardianAddress] = Guardian({
\t\t\torbsAddr: orbsAddr,
\t\t\tip: ip,
\t\t\tname: name,
\t\t\twebsite: website,
\t\t\tregistrationTime: uint32(registrationTime),
\t\t\tlastUpdateTime: uint32(lastUpdateTime)
\t\t});
\t\torbsAddressToGuardianAddress[orbsAddr] = guardianAddress;
\t\tipToGuardian[ip] = guardianAddress;

\t\temit GuardianDataUpdated(guardianAddress, true, ip, orbsAddr, name, website);
\t}

\tstring public constant ID_FORM_URL_METADATA_KEY = \"ID_FORM_URL\";
\tfunction migrateGuardianMetadata(IGuardiansRegistration previousContract, address guardianAddress) private {
\t\tstring memory rewardsFreqMetadata = previousContract.getMetadata(guardianAddress, ID_FORM_URL_METADATA_KEY);
\t\tif (bytes(rewardsFreqMetadata).length \u003e 0) {
\t\t\t_setMetadata(guardianAddress, ID_FORM_URL_METADATA_KEY, rewardsFreqMetadata);
\t\t}
\t}

\t/*
     * Contracts topology / registry interface
     */

\tIElections electionsContract;
\tfunction refreshContracts() external override {
\t\telectionsContract = IElections(getElectionsContract());
\t}

}
"},"IContractRegistry.sol":{"content":"// SPDX-License-Identifier: UNLICENSED

pragma solidity 0.6.12;

interface IContractRegistry {

\tevent ContractAddressUpdated(string contractName, address addr, bool managedContract);
\tevent ManagerChanged(string role, address newManager);
\tevent ContractRegistryUpdated(address newContractRegistry);

\t/*
\t* External functions
\t*/

\t/// @dev updates the contracts address and emits a corresponding event
\t/// managedContract indicates whether the contract is managed by the registry and notified on changes
\tfunction setContract(string calldata contractName, address addr, bool managedContract) external /* onlyAdmin */;

\t/// @dev returns the current address of the given contracts
\tfunction getContract(string calldata contractName) external view returns (address);

\t/// @dev returns the list of contract addresses managed by the registry
\tfunction getManagedContracts() external view returns (address[] memory);

\tfunction setManager(string calldata role, address manager) external /* onlyAdmin */;

\tfunction getManager(string calldata role) external view returns (address);

\tfunction lockContracts() external /* onlyAdmin */;

\tfunction unlockContracts() external /* onlyAdmin */;

\tfunction setNewContractRegistry(IContractRegistry newRegistry) external /* onlyAdmin */;

\tfunction getPreviousContractRegistry() external view returns (address);

}
"},"IElections.sol":{"content":"// SPDX-License-Identifier: UNLICENSED

pragma solidity 0.6.12;

/// @title Elections contract interface
interface IElections {
\t
\t// Election state change events
\tevent StakeChanged(address indexed addr, uint256 selfStake, uint256 delegatedStake, uint256 effectiveStake);
\tevent GuardianStatusUpdated(address indexed guardian, bool readyToSync, bool readyForCommittee);

\t// Vote out / Vote unready
\tevent GuardianVotedUnready(address indexed guardian);
\tevent VoteUnreadyCasted(address indexed voter, address indexed subject, uint256 expiration);
\tevent GuardianVotedOut(address indexed guardian);
\tevent VoteOutCasted(address indexed voter, address indexed subject);

\t/*
\t * External functions
\t */

\t/// @dev Called by a guardian when ready to start syncing with other nodes
\tfunction readyToSync() external;

\t/// @dev Called by a guardian when ready to join the committee, typically after syncing is complete or after being voted out
\tfunction readyForCommittee() external;

\t/// @dev Called to test if a guardian calling readyForCommittee() will lead to joining the committee
\tfunction canJoinCommittee(address guardian) external view returns (bool);

\t/// @dev Returns an address effective stake
\tfunction getEffectiveStake(address guardian) external view returns (uint effectiveStake);

\t/// @dev returns the current committee
\t/// used also by the rewards and fees contracts
\tfunction getCommittee() external view returns (address[] memory committee, uint256[] memory weights, address[] memory orbsAddrs, bool[] memory certification, bytes4[] memory ips);

\t// Vote-unready

\t/// @dev Called by a guardian as part of the automatic vote-unready flow
\tfunction voteUnready(address subject, uint expiration) external;

\tfunction getVoteUnreadyVote(address voter, address subject) external view returns (bool valid, uint256 expiration);

\t/// @dev Returns the current vote-unready status of a subject guardian.
\t/// votes indicates wether the specific committee member voted the guardian unready
\tfunction getVoteUnreadyStatus(address subject) external view returns (
\t\taddress[] memory committee,
\t\tuint256[] memory weights,
\t\tbool[] memory certification,
\t\tbool[] memory votes,
\t\tbool subjectInCommittee,
\t\tbool subjectInCertifiedCommittee
\t);

\t// Vote-out

\t/// @dev Casts a voteOut vote by the sender to the given address
\tfunction voteOut(address subject) external;

\t/// @dev Returns the subject address the addr has voted-out against
\tfunction getVoteOutVote(address voter) external view returns (address);

\t/// @dev Returns the governance voteOut status of a guardian.
\t/// A guardian is voted out if votedStake / totalDelegatedStake (in percent mille) \u003e threshold
\tfunction getVoteOutStatus(address subject) external view returns (bool votedOut, uint votedStake, uint totalDelegatedStake);

\t/*
\t * Notification functions from other PoS contracts
\t */

\t/// @dev Called by: delegation contract
\t/// Notifies a delegated stake change event
\t/// total_delegated_stake = 0 if addr delegates to another guardian
\tfunction delegatedStakeChange(address delegate, uint256 selfStake, uint256 delegatedStake, uint256 totalDelegatedStake) external /* onlyDelegationsContract onlyWhenActive */;

\t/// @dev Called by: guardian registration contract
\t/// Notifies a new guardian was unregistered
\tfunction guardianUnregistered(address guardian) external /* onlyGuardiansRegistrationContract */;

\t/// @dev Called by: guardian registration contract
\t/// Notifies on a guardian certification change
\tfunction guardianCertificationChanged(address guardian, bool isCertified) external /* onlyCertificationContract */;


\t/*
     * Governance functions
\t */

\tevent VoteUnreadyTimeoutSecondsChanged(uint32 newValue, uint32 oldValue);
\tevent VoteOutPercentMilleThresholdChanged(uint32 newValue, uint32 oldValue);
\tevent VoteUnreadyPercentMilleThresholdChanged(uint32 newValue, uint32 oldValue);
\tevent MinSelfStakePercentMilleChanged(uint32 newValue, uint32 oldValue);

\t/// @dev Sets the minimum self-stake required for the effective stake
\t/// minSelfStakePercentMille - the minimum self stake in percent-mille (0-100,000)
\tfunction setMinSelfStakePercentMille(uint32 minSelfStakePercentMille) external /* onlyFunctionalManager onlyWhenActive */;

\t/// @dev Returns the minimum self-stake required for the effective stake
\tfunction getMinSelfStakePercentMille() external view returns (uint32);

\t/// @dev Sets the vote-out threshold
\t/// voteOutPercentMilleThreshold - the minimum threshold in percent-mille (0-100,000)
\tfunction setVoteOutPercentMilleThreshold(uint32 voteUnreadyPercentMilleThreshold) external /* onlyFunctionalManager onlyWhenActive */;

\t/// @dev Returns the vote-out threshold
\tfunction getVoteOutPercentMilleThreshold() external view returns (uint32);

\t/// @dev Sets the vote-unready threshold
\t/// voteUnreadyPercentMilleThreshold - the minimum threshold in percent-mille (0-100,000)
\tfunction setVoteUnreadyPercentMilleThreshold(uint32 voteUnreadyPercentMilleThreshold) external /* onlyFunctionalManager onlyWhenActive */;

\t/// @dev Returns the vote-unready threshold
\tfunction getVoteUnreadyPercentMilleThreshold() external view returns (uint32);

\t/// @dev Returns the contract\u0027s settings 
\tfunction getSettings() external view returns (
\t\tuint32 minSelfStakePercentMille,
\t\tuint32 voteUnreadyPercentMilleThreshold,
\t\tuint32 voteOutPercentMilleThreshold
\t);

\tfunction initReadyForCommittee(address[] calldata guardians) external /* onlyInitializationAdmin */;

}

"},"IGuardiansRegistration.sol":{"content":"// SPDX-License-Identifier: UNLICENSED

pragma solidity 0.6.12;

/// @title Guardian registration contract interface
interface IGuardiansRegistration {
\tevent GuardianRegistered(address indexed guardian);
\tevent GuardianUnregistered(address indexed guardian);
\tevent GuardianDataUpdated(address indexed guardian, bool isRegistered, bytes4 ip, address orbsAddr, string name, string website);
\tevent GuardianMetadataChanged(address indexed guardian, string key, string newValue, string oldValue);

\t/*
     * External methods
     */

    /// @dev Called by a participant who wishes to register as a guardian
\tfunction registerGuardian(bytes4 ip, address orbsAddr, string calldata name, string calldata website) external;

    /// @dev Called by a participant who wishes to update its propertires
\tfunction updateGuardian(bytes4 ip, address orbsAddr, string calldata name, string calldata website) external;

\t/// @dev Called by a participant who wishes to update its IP address (can be call by both main and Orbs addresses)
\tfunction updateGuardianIp(bytes4 ip) external /* onlyWhenActive */;

    /// @dev Called by a participant to update additional guardian metadata properties.
    function setMetadata(string calldata key, string calldata value) external;

    /// @dev Called by a participant to get additional guardian metadata properties.
    function getMetadata(address guardian, string calldata key) external view returns (string memory);

    /// @dev Called by a participant who wishes to unregister
\tfunction unregisterGuardian() external;

    /// @dev Returns a guardian\u0027s data
\tfunction getGuardianData(address guardian) external view returns (bytes4 ip, address orbsAddr, string memory name, string memory website, uint registrationTime, uint lastUpdateTime);

\t/// @dev Returns the Orbs addresses of a list of guardians
\tfunction getGuardiansOrbsAddress(address[] calldata guardianAddrs) external view returns (address[] memory orbsAddrs);

\t/// @dev Returns a guardian\u0027s ip
\tfunction getGuardianIp(address guardian) external view returns (bytes4 ip);

\t/// @dev Returns guardian ips
\tfunction getGuardianIps(address[] calldata guardian) external view returns (bytes4[] memory ips);

\t/// @dev Returns true if the given address is of a registered guardian
\tfunction isRegistered(address guardian) external view returns (bool);

\t/// @dev Translates a list guardians Orbs addresses to guardian addresses
\tfunction getGuardianAddresses(address[] calldata orbsAddrs) external view returns (address[] memory guardianAddrs);

\t/// @dev Resolves the guardian address for a guardian, given a Guardian/Orbs address
\tfunction resolveGuardianAddress(address guardianOrOrbsAddress) external view returns (address guardianAddress);

\t/*
\t * Governance functions
\t */

\tfunction migrateGuardians(address[] calldata guardiansToMigrate, IGuardiansRegistration previousContract) external /* onlyInitializationAdmin */;

}
"},"ILockable.sol":{"content":"// SPDX-License-Identifier: UNLICENSED

pragma solidity 0.6.12;

interface ILockable {

    event Locked();
    event Unlocked();

    function lock() external /* onlyLockOwner */;
    function unlock() external /* onlyLockOwner */;
    function isLocked() view external returns (bool);

}
"},"Initializable.sol":{"content":"// SPDX-License-Identifier: UNLICENSED

pragma solidity 0.6.12;

contract Initializable {

    address private _initializationAdmin;

    event InitializationComplete();

    constructor() public{
        _initializationAdmin = msg.sender;
    }

    modifier onlyInitializationAdmin() {
        require(msg.sender == initializationAdmin(), \"sender is not the initialization admin\");

        _;
    }

    /*
    * External functions
    */

    function initializationAdmin() public view returns (address) {
        return _initializationAdmin;
    }

    function initializationComplete() external onlyInitializationAdmin {
        _initializationAdmin = address(0);
        emit InitializationComplete();
    }

    function isInitializationComplete() public view returns (bool) {
        return _initializationAdmin == address(0);
    }

}"},"Lockable.sol":{"content":"// SPDX-License-Identifier: UNLICENSED

pragma solidity 0.6.12;

import \"./ContractRegistryAccessor.sol\";
import \"./ILockable.sol\";

contract Lockable is ILockable, ContractRegistryAccessor {

    bool public locked;

    constructor(IContractRegistry _contractRegistry, address _registryAdmin) ContractRegistryAccessor(_contractRegistry, _registryAdmin) public {}

    modifier onlyLockOwner() {
        require(msg.sender == registryAdmin() || msg.sender == address(getContractRegistry()), \"caller is not a lock owner\");

        _;
    }

    function lock() external override onlyLockOwner {
        locked = true;
        emit Locked();
    }

    function unlock() external override onlyLockOwner {
        locked = false;
        emit Unlocked();
    }

    function isLocked() external override view returns (bool) {
        return locked;
    }

    modifier onlyWhenActive() {
        require(!locked, \"contract is locked for this operation\");

        _;
    }
}
"},"ManagedContract.sol":{"content":"// SPDX-License-Identifier: UNLICENSED

pragma solidity 0.6.12;

import \"./Lockable.sol\";

contract ManagedContract is Lockable {

    constructor(IContractRegistry _contractRegistry, address _registryAdmin) Lockable(_contractRegistry, _registryAdmin) public {}

    modifier onlyMigrationManager {
        require(isManager(\"migrationManager\"), \"sender is not the migration manager\");

        _;
    }

    modifier onlyFunctionalManager {
        require(isManager(\"functionalManager\"), \"sender is not the functional manager\");

        _;
    }

    function refreshContracts() virtual external {}

}"},"WithClaimableRegistryManagement.sol":{"content":"// SPDX-License-Identifier: UNLICENSED

pragma solidity 0.6.12;

import \"./Context.sol\";

/**
 * @title Claimable
 * @dev Extension for the Ownable contract, where the ownership needs to be claimed.
 * This allows the new owner to accept the transfer.
 */
contract WithClaimableRegistryManagement is Context {
    address private _registryAdmin;
    address private _pendingRegistryAdmin;

    event RegistryManagementTransferred(address indexed previousRegistryAdmin, address indexed newRegistryAdmin);

    /**
     * @dev Initializes the contract setting the deployer as the initial registryRegistryAdmin.
     */
    constructor () internal {
        address msgSender = _msgSender();
        _registryAdmin = msgSender;
        emit RegistryManagementTransferred(address(0), msgSender);
    }

    /**
     * @dev Returns the address of the current registryAdmin.
     */
    function registryAdmin() public view returns (address) {
        return _registryAdmin;
    }

    /**
     * @dev Throws if called by any account other than the registryAdmin.
     */
    modifier onlyRegistryAdmin() {
        require(isRegistryAdmin(), \"WithClaimableRegistryManagement: caller is not the registryAdmin\");
        _;
    }

    /**
     * @dev Returns true if the caller is the current registryAdmin.
     */
    function isRegistryAdmin() public view returns (bool) {
        return _msgSender() == _registryAdmin;
    }

    /**
     * @dev Leaves the contract without registryAdmin. It will not be possible to call
     * `onlyManager` functions anymore. Can only be called by the current registryAdmin.
     *
     * NOTE: Renouncing registryManagement will leave the contract without an registryAdmin,
     * thereby removing any functionality that is only available to the registryAdmin.
     */
    function renounceRegistryManagement() public onlyRegistryAdmin {
        emit RegistryManagementTransferred(_registryAdmin, address(0));
        _registryAdmin = address(0);
    }

    /**
     * @dev Transfers registryManagement of the contract to a new account (`newManager`).
     */
    function _transferRegistryManagement(address newRegistryAdmin) internal {
        require(newRegistryAdmin != address(0), \"RegistryAdmin: new registryAdmin is the zero address\");
        emit RegistryManagementTransferred(_registryAdmin, newRegistryAdmin);
        _registryAdmin = newRegistryAdmin;
    }

    /**
     * @dev Modifier throws if called by any account other than the pendingManager.
     */
    modifier onlyPendingRegistryAdmin() {
        require(msg.sender == _pendingRegistryAdmin, \"Caller is not the pending registryAdmin\");
        _;
    }
    /**
     * @dev Allows the current registryAdmin to set the pendingManager address.
     * @param newRegistryAdmin The address to transfer registryManagement to.
     */
    function transferRegistryManagement(address newRegistryAdmin) public onlyRegistryAdmin {
        _pendingRegistryAdmin = newRegistryAdmin;
    }

    /**
     * @dev Allows the _pendingRegistryAdmin address to finalize the transfer.
     */
    function claimRegistryManagement() external onlyPendingRegistryAdmin {
        _transferRegistryManagement(_pendingRegistryAdmin);
        _pendingRegistryAdmin = address(0);
    }

    /**
     * @dev Returns the current pendingRegistryAdmin
    */
    function pendingRegistryAdmin() public view returns (address) {
       return _pendingRegistryAdmin;  
    }
}

