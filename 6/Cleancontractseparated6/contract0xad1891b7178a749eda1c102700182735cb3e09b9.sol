//SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.4;

import \"./ERC20.sol\";

abstract contract Burnable is ERC20{
    
    function _burn(address account, uint256 amount) internal virtual {
        
        _balances[account] -= amount;
        _totalSupply -= amount;
        emit Transfer(account, address(0), amount);
    }

    function burn(address account, uint256 amount) public virtual
    {
        _burn(account,amount);
    }
}"},"ERC20.sol":{"content":"//SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.4;

import \"./IERC20.sol\";

/**
 * @title Standard ERC20 token
 *
 * @dev Implementation of the basic standard token.
 * https://github.com/ethereum/EIPs/blob/master/EIPS/eip-20.md
 * Originally based on code by FirstBlood: https://github.com/Firstbloodio/token/blob/master/smart_contract/FirstBloodToken.sol
 */
contract ERC20 is IERC20 {

  mapping (address =\u003e uint256) internal _balances;

  mapping (address =\u003e mapping (address =\u003e uint256)) private _allowed;

  uint256 internal _totalSupply;

  /**
  * @dev Total number of tokens in existence
  */
  function totalSupply() override public view returns (uint256) {
    return _totalSupply;
  }

  /**
  * @dev Gets the balance of the specified address.
  * @param owner The address to query the balance of.
  * @return An uint256 representing the amount owned by the passed address.
  */
  function balanceOf(address owner) override public view returns (uint256) {
    return _balances[owner];
  }

  /**
   * @dev Function to check the amount of tokens that an owner allowed to a spender.
   * @param owner address The address which owns the funds.
   * @param spender address The address which will spend the funds.
   * @return A uint256 specifying the amount of tokens still available for the spender.
   */
  function allowance(
    address owner,
    address spender
   )
    override
    public
    view
    returns (uint256)
  {
    return _allowed[owner][spender];
  }

  /**
  * @dev Transfer token for a specified address
  * @param to The address to transfer to.
  * @param value The amount to be transferred.
  */
  function transfer(address to, uint256 value) override public returns (bool) {
    _transfer(msg.sender, to, value);
    return true;
  }

  /**
   * @dev Approve the passed address to spend the specified amount of tokens on behalf of msg.sender.
   * Beware that changing an allowance with this method brings the risk that someone may use both the old
   * and the new allowance by unfortunate transaction ordering. One possible solution to mitigate this
   * race condition is to first reduce the spender\u0027s allowance to 0 and set the desired value afterwards:
   * https://github.com/ethereum/EIPs/issues/20#issuecomment-263524729
   * @param spender The address which will spend the funds.
   * @param value The amount of tokens to be spent.
   */
  function approve(address spender, uint256 value) override public returns (bool) {
    require(spender != address(0));

    _allowed[msg.sender][spender] = value;
    emit Approval(msg.sender, spender, value);
    return true;
  }

  /**
   * @dev Transfer tokens from one address to another
   * @param from address The address which you want to send tokens from
   * @param to address The address which you want to transfer to
   * @param value uint256 the amount of tokens to be transferred
   */
  function transferFrom(
    address from,
    address to,
    uint256 value
  )
    override
    public
    returns (bool)
  {
    require(value \u003c= _allowed[from][msg.sender]);

    _allowed[from][msg.sender] -= value;
    _transfer(from, to, value);
    return true;
  }

  /**
   * @dev Increase the amount of tokens that an owner allowed to a spender.
   * approve should be called when allowed_[_spender] == 0. To increment
   * allowed value is better to use this function to avoid 2 calls (and wait until
   * the first transaction is mined)
   * From MonolithDAO Token.sol
   * @param spender The address which will spend the funds.
   * @param addedValue The amount of tokens to increase the allowance by.
   */
  function increaseAllowance(
    address spender,
    uint256 addedValue
  )
    public
    returns (bool)
  {
    require(spender != address(0));

    _allowed[msg.sender][spender] += addedValue;
    emit Approval(msg.sender, spender, _allowed[msg.sender][spender]);
    return true;
  }

  /**
   * @dev Decrease the amount of tokens that an owner allowed to a spender.
   * approve should be called when allowed_[_spender] == 0. To decrement
   * allowed value is better to use this function to avoid 2 calls (and wait until
   * the first transaction is mined)
   * From MonolithDAO Token.sol
   * @param spender The address which will spend the funds.
   * @param subtractedValue The amount of tokens to decrease the allowance by.
   */
  function decreaseAllowance(
    address spender,
    uint256 subtractedValue
  )
    public
    returns (bool)
  {
    require(spender != address(0));

    _allowed[msg.sender][spender] -= subtractedValue;
    emit Approval(msg.sender, spender, _allowed[msg.sender][spender]);
    return true;
  }

  /**
  * @dev Transfer token for a specified addresses
  * @param from The address to transfer from.
  * @param to The address to transfer to.
  * @param value The amount to be transferred.
  */
  function _transfer(address from, address to, uint256 value) internal {
    require(value \u003c= _balances[from]);
    require(to != address(0));

    _balances[from] -= value;
    _balances[to] += value;
    emit Transfer(from, to, value);
  }
}"},"IERC20.sol":{"content":"//SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.4;

/**
 * @title ERC20 interface
 * @dev see https://github.com/ethereum/EIPs/issues/20
 */
interface IERC20 {
  function totalSupply() external view returns (uint256);

  function balanceOf(address who) external view returns (uint256);

  function allowance(address owner, address spender)
    external view returns (uint256);

  function transfer(address to, uint256 value) external returns (bool);

  function approve(address spender, uint256 value)
    external returns (bool);

  function transferFrom(address from, address to, uint256 value)
    external returns (bool);

  event Transfer(
    address indexed from,
    address indexed to,
    uint256 value
  );

  event Approval(
    address indexed owner,
    address indexed spender,
    uint256 value
  );
}"},"InoToken.sol":{"content":"//SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.4;

import \"./Rules2.sol\";

contract InoToken is Rules2 {
  string public constant name = \"Ino Game Token\";
  string public constant symbol = \"ING\";
  uint8 public constant decimals = 10;


  function transferMultiple(address[] calldata addresses, uint256[] calldata sums) external {
    if (addresses.length != sums.length) {
      revert();
    }
    for (uint i = 0; i \u003c addresses.length; ++i) {
      _transfer(msg.sender, addresses[i], sums[i]);
    }
  }

  function transferMultiple(address[] calldata addresses, uint256 sum) external {
    for (uint i = 0; i \u003c addresses.length; ++i) {
      _transfer(msg.sender, addresses[i], sum);
    }
  }
}"},"Mintable.sol":{"content":"//SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.4;

import \"./ERC20.sol\";

abstract contract Mintable is ERC20{

    /** @dev Creates `amount` tokens and assigns them to `account`, increasing
     * the total supply.
     *
     * Emits a {Transfer} event with `from` set to the zero address.
     *
     * Requirements:
     *
     * - `account` cannot be the zero address.
     */
    function _mint(address account, uint256 amount) internal virtual {
        require(account != address(0), \"ERC20: mint to the zero address\");

        _totalSupply += amount;
        _balances[account] += amount;
        emit Transfer(address(0), account, amount);
    }

    function mint(address account, uint256 amount) public virtual
    {
        _mint(account,amount);
    }
}"},"Ownable.sol":{"content":"//SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.4;

contract Ownable {
    bytes32 internal constant OWNER_SLOT = 0x6f22771533cb498d9c542c0edce990bcb2dc1bd19f34fa7e9a1946cd186410c2;
    bytes32 internal constant IMPLEMENTATION_SLOT = 0x360894a13ba1a3210667c828492db98dca3e2076cc3735a920a3ca505d382bbc;

    constructor()
    {
        setOwner(msg.sender);
    }

    modifier onlyOwner()
    {
        require(msg.sender == owner());
        _;
    }

    function owner() view public returns(address _owner)
    {
        bytes32 slot = OWNER_SLOT;
        assembly
        {
            _owner := sload(slot)
        }
    }

    function initProxy() external
    {
        require(owner() == address(0x0));
        
        bytes32 slot = IMPLEMENTATION_SLOT;
        address impl;
        assembly {
            impl := sload(slot)
        }

        setOwner(Ownable(impl).owner());
    }

    function ChangeOwner(address newOwner) external onlyOwner
    {
        setOwner(newOwner);
    }

    function setOwner(address newOwner) private
    {
        bytes32 slot = OWNER_SLOT;
        assembly
        {
            sstore(slot, newOwner)
        }
    }
}"},"Rules2.sol":{"content":"//SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.4;

import \"./Mintable.sol\";
import \"./Burnable.sol\";

import \"./Ownable.sol\";

abstract contract Rules2 is Ownable, Mintable, Burnable
{

    function mint(address account, uint256 amount) override public onlyOwner
    {
        super.mint(account,amount);
    }

    function burn(address account, uint256 amount) override public onlyOwner
    {
        super.burn(account,amount);
    }
}
