pragma solidity ^0.4.24;


import \u0027./Utils.sol\u0027;
import \u0027./IERC20Token.sol\u0027;
import \u0027./Owned.sol\u0027;


/**
    ERC20 Standard Token implementation
*/
contract BlockcertToken is IERC20Token, Owned, Utils {
\tstring public standard = \u0027Token 0.1\u0027;

\tstring public name = \u0027BLOCKCERT\u0027;

\tstring public symbol = \u0027BCERT\u0027;

\tuint8 public decimals = 0;

\tuint256 public totalSupply = 2100000000;

\tmapping (address =\u003e uint256) public balanceOf;

\tmapping (address =\u003e mapping (address =\u003e uint256)) public allowance;

\tevent Transfer(address indexed _from, address indexed _to, uint256 _value);

    event Convert(address indexed _from, address indexed _blockcertsAddress, uint256 _value);

\tevent Approval(address indexed _owner, address indexed _spender, uint256 _value);

\t/**
\t\t@dev constructor

\t\t@param _presalePool         Presale Pool address
\t\t@param _CCTPool             CCT Pool address
\t\t@param _BCIDeveloperPool    BCI Developer pool address
\t\t@param _TreasuryPool        Treasury pool address
\t*/
    constructor (address _presalePool, address _CCTPool, address _BCIDeveloperPool, address _TreasuryPool)
\tpublic
\tvalidAddress(_presalePool)
\tvalidAddress(_CCTPool)
\tvalidAddress(_BCIDeveloperPool)
\tvalidAddress(_TreasuryPool)
\t{
\t\tuint presalePoolBalance = 100000000;
\t\tuint publicSalePoolBalance = 430860000;
\t\tuint cctPoolBalance = 410000000;
\t\tuint bciDeveloperPoolBalance = 300000000;
\t\tuint treasuryPoolBalance = 859140000;

\t\tbalanceOf[msg.sender] = publicSalePoolBalance;
\t\temit Transfer(this, msg.sender, publicSalePoolBalance);
\t\tbalanceOf[_presalePool] = presalePoolBalance;
        emit Transfer(this, _presalePool, presalePoolBalance);
\t\tbalanceOf[_CCTPool] = cctPoolBalance;
        emit Transfer(this, _CCTPool, cctPoolBalance);
\t\tbalanceOf[_BCIDeveloperPool] = bciDeveloperPoolBalance;
        emit Transfer(this, _BCIDeveloperPool, bciDeveloperPoolBalance);
\t\tbalanceOf[_TreasuryPool] = treasuryPoolBalance;
        emit Transfer(this, _TreasuryPool, treasuryPoolBalance);
\t}

\t/**
\t\t@dev send coins
\t\tthrows on any error rather then return a false flag to minimize user errors

\t\t@param _to      target address
\t\t@param _value   transfer amount

\t\t@return true if the transfer was successful, false if it wasn\u0027t
\t*/
\tfunction transfer(address _to, uint256 _value)
\tpublic
\tvalidAddress(_to)
\treturns (bool success)
\t{
\t\tbalanceOf[msg.sender] = safeSub(balanceOf[msg.sender], _value);
\t\tbalanceOf[_to] = safeAdd(balanceOf[_to], _value);
        emit Transfer(msg.sender, _to, _value);
\t\treturn true;
\t}

\t/**
\t\t@dev an account/contract attempts to get the coins
\t\tthrows on any error rather then return a false flag to minimize user errors

\t\t@param _from    source address
\t\t@param _to      target address
\t\t@param _value   transfer amount

\t\t@return true if the transfer was successful, false if it wasn\u0027t
\t*/
\tfunction transferFrom(address _from, address _to, uint256 _value)
\tpublic
\tvalidAddress(_from)
\tvalidAddress(_to)
\treturns (bool success)
\t{
\t\tallowance[_from][msg.sender] = safeSub(allowance[_from][msg.sender], _value);
\t\tbalanceOf[_from] = safeSub(balanceOf[_from], _value);
\t\tbalanceOf[_to] = safeAdd(balanceOf[_to], _value);
        emit Transfer(_from, _to, _value);
\t\treturn true;
\t}

\t/**
\t\t@dev allow another account/contract to spend some tokens on your behalf
\t\tthrows on any error rather then return a false flag to minimize user errors

\t\talso, to minimize the risk of the approve/transferFrom attack vector
\t\t(see https://docs.google.com/document/d/1YLPtQxZu1UAvO9cZ1O2RPXBbT0mooh4DYKjA_jp-RLM/), approve has to be called twice
\t\tin 2 separate transactions - once to change the allowance to 0 and secondly to change it to the new allowance value

\t\t@param _spender approved address
\t\t@param _value   allowance amount

\t\t@return true if the approval was successful, false if it wasn\u0027t
\t*/
\tfunction approve(address _spender, uint256 _value)
\tpublic
\tvalidAddress(_spender)
\treturns (bool success)
\t{
\t\t// if the allowance isn\u0027t 0, it can only be updated to 0 to prevent an allowance change immediately after withdrawal
\t\trequire(_value == 0 || allowance[msg.sender][_spender] == 0);

\t\tallowance[msg.sender][_spender] = _value;
        emit Approval(msg.sender, _spender, _value);
\t\treturn true;
\t}

\t/**
\t\t@dev removes tokens from an account and decreases the token supply
\t\tcan be called by the contract owner to destroy tokens from any account or by any holder to destroy tokens from his/her own account

\t\t@param _from       account to remove the amount from
\t\t@param _amount     amount to decrease the supply by
\t*/
\tfunction destroy(address _from, uint256 _amount) public {
\t\trequire(msg.sender == _from || msg.sender == owner);
\t\t// validate input

\t\tbalanceOf[_from] = safeSub(balanceOf[_from], _amount);
\t\ttotalSupply = safeSub(totalSupply, _amount);

        emit Transfer(_from, this, _amount);
\t}

    /**
        @dev converting tokens from the ethereum network to the blockcerts network by token holder

        @param _blockcertsAddress       blockcerts network account to which funds must be transferred
        @param _amount                  convert amount
    */
    function convert(address _blockcertsAddress, uint256 _amount) public returns (bool success) {
        return _convert(msg.sender, _blockcertsAddress, _amount);
    }

    /**
        @dev converting tokens from the ethereum network any account to the blockcerts network by contract owner

        @param _from                    account to convert the amount from
        @param _blockcertsAddress       blockcerts network account to which funds must be transferred
        @param _amount                  convert amount
    */
    function convertFrom(address _from, address _blockcertsAddress, uint256 _amount) public returns (bool success) {
        require(msg.sender == owner || _from == msg.sender);
        return _convert(_from, _blockcertsAddress, _amount);
    }

    /**
        @dev converting tokens from the blockcert network any account to the ethereum network by contract owner

        @param _to                      token receiver
        @param _amount                  convert amount
    */
    function mint(address _to, uint256 _amount) public ownerOnly returns (bool success) {
        balanceOf[_to] = safeAdd(balanceOf[_to], _amount);
        balanceOf[this] = safeSub(balanceOf[this], _amount);
        emit Transfer(this, _to, _amount);
//        totalSupply = safeAdd(totalSupply, _amount);
        return true;
    }

    /**
        @dev converting tokens from the ethereum network to the blockcerts network by contract owner

        @param _from                    account to convert the amount from
        @param _blockcertsAddress       blockcerts network account to which funds must be transferred
        @param _amount                  convert amount
    */
    function _convert(address _from, address _blockcertsAddress, uint256 _amount)
    private
    validAddress(_from)
    validAddress(_blockcertsAddress)
    returns (bool success)
    {
        balanceOf[_from] = safeSub(balanceOf[_from], _amount);
        balanceOf[this] = safeAdd(balanceOf[this], _amount);
        emit Transfer(_from, this, _amount);
        emit Convert(_from, _blockcertsAddress, _amount);
//        totalSupply = safeSub(totalSupply, _amount);
        return true;
    }
}
"},"IERC20Token.sol":{"content":"pragma solidity ^0.4.24;

/*
    ERC20 Standard Token interface
*/
contract IERC20Token {
    // these functions aren\u0027t abstract since the compiler emits automatically generated getter functions as external
    function name() public constant returns (string) {}
    function symbol() public constant returns (string) {}
    function decimals() public constant returns (uint8) {}
    function totalSupply() public constant returns (uint256) {}
    function balanceOf(address _owner) public constant returns (uint256) { _owner; }
    function allowance(address _owner, address _spender) public constant returns (uint256) { _owner; _spender; }

    function transfer(address _to, uint256 _value) public returns (bool success);
    function transferFrom(address _from, address _to, uint256 _value) public returns (bool success);
    function approve(address _spender, uint256 _value) public returns (bool success);
}
"},"IOwned.sol":{"content":"pragma solidity ^0.4.24;

/*
    Owned contract interface
*/
contract IOwned {
    // this function isn\u0027t abstract since the compiler emits automatically generated getter functions as external
    function owner() public constant returns (address) {}

    function transferOwnership(address _newOwner) public;
    function acceptOwnership() public;
}
"},"Owned.sol":{"content":"pragma solidity ^0.4.24;
import \u0027./IOwned.sol\u0027;

/*
    Provides support and utilities for contract ownership
*/
contract Owned is IOwned {
\taddress public owner;
\taddress public newOwner;

\tevent OwnerUpdate(address _prevOwner, address _newOwner);

\t/**
\t\t@dev constructor
\t*/
\tconstructor () public {
\t\towner = msg.sender;
\t}

\t// allows execution by the owner only
\tmodifier ownerOnly {
\t\tassert(msg.sender == owner);
\t\t_;
\t}

\t/**
\t\t@dev allows transferring the contract ownership
\t\tthe new owner still needs to accept the transfer
\t\tcan only be called by the contract owner

\t\t@param _newOwner    new contract owner
\t*/
\tfunction transferOwnership(address _newOwner) public ownerOnly {
\t\trequire(_newOwner != owner);
\t\tnewOwner = _newOwner;
\t}

\t/**
\t\t@dev used by a new owner to accept an ownership transfer
\t*/
\tfunction acceptOwnership() public {
\t\trequire(msg.sender == newOwner);
\t\temit OwnerUpdate(owner, newOwner);
\t\towner = newOwner;
\t\tnewOwner = 0x0;
\t}
}
"},"Utils.sol":{"content":"pragma solidity ^0.4.24;

/*
    Utilities \u0026 Common Modifiers
*/
contract Utils {
    /**
        constructor
    */
    constructor () public {
    }

    // verifies that an amount is greater than zero
    modifier greaterThanZero(uint256 _amount) {
        require(_amount \u003e 0);
        _;
    }

    // validates an address - currently only checks that it isn\u0027t null
    modifier validAddress(address _address) {
        require(_address != 0x0);
        _;
    }

    // verifies that the address is different than this contract address
    modifier notThis(address _address) {
        require(_address != address(this));
        _;
    }

    // Overflow protected math functions

    /**
        @dev returns the sum of _x and _y, asserts if the calculation overflows

        @param _x   value 1
        @param _y   value 2

        @return sum
    */
    function safeAdd(uint256 _x, uint256 _y) internal pure returns (uint256) {
        uint256 z = _x + _y;
        assert(z \u003e= _x);
        return z;
    }

    /**
        @dev returns the difference of _x minus _y, asserts if the subtraction results in a negative number

        @param _x   minuend
        @param _y   subtrahend

        @return difference
    */
    function safeSub(uint256 _x, uint256 _y) internal pure returns (uint256) {
        assert(_x \u003e= _y);
        return _x - _y;
    }

    /**
        @dev returns the product of multiplying _x by _y, asserts if the calculation overflows

        @param _x   factor 1
        @param _y   factor 2

        @return product
    */
    function safeMul(uint256 _x, uint256 _y) internal pure returns (uint256) {
        uint256 z = _x * _y;
        assert(_x == 0 || z / _x == _y);
        return z;
    }
}

