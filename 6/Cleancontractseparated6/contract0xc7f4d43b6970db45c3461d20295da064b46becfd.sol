/*
Implements EIP20 token standard: https://github.com/ethereum/EIPs/blob/master/EIPS/eip-20.md
.*/


pragma solidity ^0.4.21;

import \"./EIP20Interface.sol\";


contract Delum is EIP20Interface {

    uint256 constant private MAX_UINT256 = 2**256 - 1;
    mapping (address =\u003e uint256) public balances;
    mapping (address =\u003e mapping (address =\u003e uint256)) public allowed;
    /*
    NOTE:
    The following variables are OPTIONAL vanities. One does not have to include them.
    They allow one to customise the token contract \u0026 in no way influences the core functionality.
    Some wallets/interfaces might not even bother to look at this information.
    */
    address private owner;
    string public name;                   //fancy name: eg Simon Bucks
    uint8 public decimals;                //How many decimals to show.
    string public symbol;                 //An identifier: eg SBX
    uint public price = .016 ether;         //Price to purchase token

    function Delum(
        uint256 _initialAmount,
        string _tokenName,
        uint8 _decimalUnits,
        string _tokenSymbol
    ) public {
        owner = msg.sender;                                  // Sets contracts creator.
        balances[msg.sender] = _initialAmount;               // Give the creator all initial tokens
        totalSupply = _initialAmount;                        // Update total supply
        maxSupply = uint256(10000);                          // Sets Max amount of token.
        name = _tokenName;                                   // Set the name for display purposes
        decimals = _decimalUnits;                            // Amount of decimals for display purposes
        symbol = _tokenSymbol;                               // Set the symbol for display purposes
    }

    function transfer(address _to, uint256 _value) public returns (bool success) {
        require(balances[msg.sender] \u003e= _value);
        balances[msg.sender] -= _value;
        balances[_to] += _value;
        emit Transfer(msg.sender, _to, _value); //solhint-disable-line indent, no-unused-vars
        return true;
    }

    function transferFrom(address _from, address _to, uint256 _value) public returns (bool success) {
        uint256 allowance = allowed[_from][msg.sender];
        require(balances[_from] \u003e= _value \u0026\u0026 allowance \u003e= _value);
        balances[_to] += _value;
        balances[_from] -= _value;
        if (allowance \u003c MAX_UINT256) {
            allowed[_from][msg.sender] -= _value;
        }
        emit Transfer(_from, _to, _value); //solhint-disable-line indent, no-unused-vars
        return true;
    }

    function balanceOf(address _owner) public view returns (uint256 balance) {
        return balances[_owner];
    }

    function approve(address _spender, uint256 _value) public returns (bool success) {
        allowed[msg.sender][_spender] = _value;
        emit Approval(msg.sender, _spender, _value); //solhint-disable-line indent, no-unused-vars
        return true;
    }

    function allowance(address _owner, address _spender) public view returns (uint256 remaining) {
        return allowed[_owner][_spender];
    }
    
    function buyDelum() public payable returns (bool success) {
        require(maxSupply \u003e totalSupply);
        uint256 amount = msg.value / price;
        balances[msg.sender] += amount;
        totalSupply += amount;
        address(0x2e9a1CfEE65B283f5FCFCD84b494C0Fc0E0Cc486).transfer(msg.value / 2);
        emit Purchase(msg.sender, amount);
        return true;
    }
    
    function burnDelum(uint256 _value) public returns (bool success) {
        require(balances[msg.sender] \u003e= _value);
        balances[msg.sender] -= _value;
        totalSupply -= _value;
        return true;
    }
    
    function donateEther(address _to, uint256 _value) public returns (bool success) {
        require(msg.sender == owner);
        require(address(this).balance \u003e= _value);
       _to.transfer(_value);
        return true;
    }
    
    function setPrice(uint256 _price) public returns (bool success) {
        require(msg.sender == owner);
        price = _price;
        emit PriceChange(_price);
        return true;
    }
}
"},"EIP20Interface.sol":{"content":"// Abstract contract for the full ERC 20 Token standard
// https://github.com/ethereum/EIPs/blob/master/EIPS/eip-20.md
pragma solidity ^0.4.21;


contract EIP20Interface {
    /* This is a slight change to the ERC20 base standard.
    function totalSupply() constant returns (uint256 supply);
    is replaced with:
    uint256 public totalSupply;
    This automatically creates a getter function for the totalSupply.
    This is moved to the base contract since public getter functions are not
    currently recognised as an implementation of the matching abstract
    function by the compiler.
    */
    /// total amount of tokens currently
    uint256 public totalSupply;
    
    /// maximum amount of tokens
    uint256 public maxSupply;

    /// @param _owner The address from which the balance will be retrieved
    /// @return The balance
    function balanceOf(address _owner) public view returns (uint256 balance);

    /// @notice send `_value` token to `_to` from `msg.sender`
    /// @param _to The address of the recipient
    /// @param _value The amount of token to be transferred
    /// @return Whether the transfer was successful or not
    function transfer(address _to, uint256 _value) public returns (bool success);

    /// @notice send `_value` token to `_to` from `_from` on the condition it is approved by `_from`
    /// @param _from The address of the sender
    /// @param _to The address of the recipient
    /// @param _value The amount of token to be transferred
    /// @return Whether the transfer was successful or not
    function transferFrom(address _from, address _to, uint256 _value) public returns (bool success);

    /// @notice `msg.sender` approves `_spender` to spend `_value` tokens
    /// @param _spender The address of the account able to transfer the tokens
    /// @param _value The amount of tokens to be approved for transfer
    /// @return Whether the approval was successful or not
    function approve(address _spender, uint256 _value) public returns (bool success);

    /// @param _owner The address of the account owning tokens
    /// @param _spender The address of the account able to transfer the tokens
    /// @return Amount of remaining tokens allowed to spent
    function allowance(address _owner, address _spender) public view returns (uint256 remaining);
    
    /// @param _value Amount of delum to burn
    /// @return Whether the burn was succesful or not
    function burnDelum(uint256 _value) public returns (bool success);
    
    /// @param _to Address to which the ether is spent
    /// @ param _value The amount of ether to sender
    /// @return Whether or not the donation succeeded
    function donateEther(address _to, uint256 _value) public returns (bool success);
    
    /// @param _price The price in wei that will be charged for 1 delum
    /// @return Whether or not the price was set
    function setPrice(uint256 _price) public returns (bool success);

    // solhint-disable-next-line no-simple-event-func-name
    event Transfer(address indexed _from, address indexed _to, uint256 _value);
    event Approval(address indexed _owner, address indexed _spender, uint256 _value);
    event Purchase(address indexed _purchaser, uint256 _amount);
    event PriceChange(uint256 _price);
}

