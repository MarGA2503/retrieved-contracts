pragma solidity ^0.6.6;
pragma experimental ABIEncoderV2;

import \"openzeppelin-solidity/contracts/access/Ownable.sol\";
import \"@nomiclabs/buidler/console.sol\";

contract SkillTree is Ownable {
  address public admin;

  // States
  uint constant nonexistent    = 0;
  uint constant needs_approval = 1;
  
  uint constant approved       = 2;
  uint constant active         = 2;

  uint constant declined       = 3;
  uint constant canceled       = 4;

  // Certificate Levels
  uint constant attendance     = 1;
  uint constant material       = 2;
  uint constant exams          = 3;
  uint constant extra          = 4;

  // actions to approve
  byte constant a_expert = 0x01;
  byte constant a_issuer = 0x02; 

  // =========================================
  //  Events
  // =========================================
  event NewUser(address user,string name,bool issuer,bool expert,string dataHash);
  event UserChanged(address user,string name,bool issuer,bool expert,string dataHash);
  event ApprovalNeeded(address sender, bytes32 id);
  event DecideApproval(address sender, bytes32 user, bool decision);
  event NewCourse(bytes32 course, address user, string name, string dataHash);
  event CourseChanged(bytes32 course, address user, string name, string dataHash);
  event NewEndorsement(bytes32 endorsement, address endorser, bytes32 course);
  event DecideEndorsement(bytes32 endorsement, address decider, bytes32 course, bool decision);
  event CancelEndorsement(bytes32 endorsement, address endorser);
  event NewCertificateTemplate(bytes32 courseId, string _title, uint level);
  event NewCertificate(bytes32 templateId, address issuer, address recipient);

  // =========================================
  //  Users
  // =========================================
  // 0: nonexistent
  // 1: needs_approval
  // 2: active
  // 3: disabled
  
  struct User {
    string     name;
    string     dataHash;  // ipfs hash?
    bool       issuer;
    bool       expert;
    uint       state;
    bytes32[]  courseArray;
    bytes32[]  pendingEndorsementArray;
    bytes32[]  receivedCertificateArray;
    bytes32[]  issuedCertificateArray;
    bytes32[]  approvalArray;
    bytes32[] endorsementArray;
  }


  address[]  private userArray;
  mapping (address => User) private Users;

  // =========================================
  //  Courses
  // =========================================
  struct Course {
    string name;
    address issuer;
    string dataHash;
    bytes32[] endorsementArray;
    bytes32[] certificateTemplateArray;
    uint state;
  }

  bytes32[] public courseArray;
  mapping (bytes32 => Course) private Courses;

  // =========================================
  // Endorsements
  // =========================================
  struct Endorsement {
    bytes32 certificateTemplateId;
    address endorser;
    uint    level;
    uint    state;
  }
  
  bytes32[] private endorsementArray;
  mapping (bytes32 => Endorsement) private Endorsements;

  // =========================================
  // Certificate Templates
  // =========================================
  struct CertificateTemplate {
    string    title;
    string    description;
    string    dataHash;
    address   issuer;
    bytes32   courseId;
    bytes32[] certificateArray;
    uint      level;          // attendance, material, exams, extra
    uint      state;
  }

  uint private certificateTemplateNonce;
  mapping(bytes32 => CertificateTemplate) private CertificateTemplates;

  // =========================================
  // Certificates
  // =========================================
  struct Certificate {
    bytes32 templateId;
    address recipient;
    address issuer;
    string dataHash;
    uint state;
  }

  bytes32[] private certificateArray;
  mapping (bytes32 => Certificate) private Certificates;

  // =========================================
  // Approvals
  // =========================================
  struct Approval {
    address subject;
    address approver;
    byte    permission;
    uint    state;
  }
  mapping (bytes32 => Approval) private Approvals;
  bytes32[] private approvalArray;

  // =========================================
  //
  //  Constructor
  //
  // =========================================
  constructor() public{
    admin = msg.sender;
  }

  // =========================================
  //
  //  User Management
  //
  // =========================================
  function update_user(string calldata _name, string calldata _dataHash, bool _issuer, bool _expert) external {
    require((Users[msg.sender].state > nonexistent), \"Create user first\");
    
    Users[msg.sender].name       = _name;
    Users[msg.sender].dataHash = _dataHash;
    
    if (_issuer && !(Users[msg.sender].issuer)) {
      request_approval(a_issuer);
    } else if(_expert && !(Users[msg.sender].expert)) {
      request_approval(a_expert);
    } else {
      Users[msg.sender].state      = active;
    }
      
    emit UserChanged(msg.sender, _name, _issuer, _expert, _dataHash);
  }
  
  function sign_up(string calldata _name, string calldata _dataHash, bool _issuer, bool _expert) external {
    require((Users[msg.sender].state == nonexistent), \"This user already exists\");
   
    Users[msg.sender].name       = _name;
    Users[msg.sender].dataHash   = _dataHash;
    Users[msg.sender].state      = active;
    
    if (_issuer) {
      request_approval(a_issuer);
    }

    if(_expert) {
      request_approval(a_expert);
    }
    userArray.push(msg.sender);
    emit NewUser(msg.sender, _name, _issuer, _expert, _dataHash);
  }

  function update_user_dataHash(string memory _dataHash) public {
    require((Users[msg.sender].state > nonexistent), \"Create user first\");
    Users[msg.sender].dataHash = _dataHash;
    emit UserChanged(msg.sender, Users[msg.sender].name, Users[msg.sender].issuer, Users[msg.sender].expert, _dataHash);
  }
  
  function number_of_users() public view returns (uint256) {
    return userArray.length;
  }
  
  // =========================================
  //
  //  Approvals & decisions
  //
  // =========================================
  
  // TODO As of now, once granted, admin can NOT take away expert and issuer privileges.

  function request_approval(byte permission) internal {
    Approval memory a;
    a.subject = msg.sender;
    a.permission = permission;
    a.approver = admin;
    a.state = needs_approval;
    bytes32 id = keccak256(abi.encodePacked(a.subject, a.permission, a.approver, approvalArray.length));
    Approvals[id] = a;
    Users[admin].approvalArray.push(id);
    approvalArray.push(id);
    emit ApprovalNeeded(msg.sender, id);
  }
 
  function decide_approval(uint _pos, bool _decision) public{
    require((msg.sender == admin), \"Only admin can approve\");
    
    bytes32 approval_id = Users[msg.sender].approvalArray[_pos]; // bytes32 id of the approval

    require((Approvals[approval_id].state == needs_approval), \"No approval needed for this\");

    //delete Users[Approvals[approval_id].approver].approvalArray[_pos];

    if(_decision) {
      Approvals[approval_id].state = approved;
      if(Approvals[approval_id].permission == a_expert) {
        Users[Approvals[approval_id].subject].expert = true;
      }

      if(Approvals[approval_id].permission == a_issuer) {
        Users[Approvals[approval_id].subject].issuer = true;
      }
    } else {
      Approvals[approval_id].state = declined;
    }


    emit DecideApproval(msg.sender, approval_id, _decision);
  }

  function get_approval(bytes32 id) external view returns(Approval memory){
    return(Approvals[id]);
  }

  function get_approvals(address _address) external view returns(bytes32[] memory){
    return(Users[_address].approvalArray);
  }

  // =========================================
  // Admin functions
  // =========================================
  function set_admin (address _admin) external onlyOwner{
    admin = _admin;
  }   

  //
  // getter functions to check account state
  //

  function account_state(address _address) public view returns (uint) {
    return(Users[_address].state);
  }

  // get user by address
  function get_user(address _address) public view returns (User memory) {
    return(Users[_address]);
  }

  // get user by position
  function get_user_by_position(uint _position) public view returns (User memory) {
    require((_position < userArray.length), \"No such user\");
    return(Users[userArray[_position]]);
  }
  
  // Determining user type
  function expert(address _address) public view returns (bool) {
    return(Users[_address].expert);
  }

  function issuer(address _address) public view returns (bool) {
    return(Users[_address].issuer);
  }
  
  function get_user_dataHash(address _address) public view returns (string memory) {
    return(Users[_address].dataHash);
  }
  
  function get_user_state(address _address) public view returns (string memory) {
    if(Users[_address].state == 0){ return(\"nonexistent\"); }
    if(Users[_address].state == 1){ return(\"needs approval\"); }
    if(Users[_address].state == 2){ return(\"approved\"); }
    if(Users[_address].state == 3){ return(\"declined\"); }
    if(Users[_address].state == 3){ return(\"canceled\"); }
    return(\"error\");
  }
  
  // =========================================
  //
  //  Course Management
  //
  // =========================================

  function add_course(string calldata _name, string calldata _dataHash) external{
    require((Users[msg.sender].state == approved), \"An approved user is required\");
    require((Users[msg.sender].issuer), \"Account has no issuer privilege\");

    bytes32 id = keccak256(abi.encodePacked(_name, _dataHash, courseArray.length));
    require((Courses[id].state == nonexistent ), \"Already a course exists with this id. This is very unlikely.\");

    Courses[id].name = _name;
    Courses[id].dataHash = _dataHash;
    Courses[id].issuer = msg.sender;
    Courses[id].state = active;

    courseArray.push(id);
    Users[msg.sender].courseArray.push(id);
    emit NewCourse(id, msg.sender, _name, _dataHash);
  }

  function update_course(bytes32 _id, string calldata _name, string calldata _dataHash) external {
    require((Users[msg.sender].state == approved), \"An approved user is required\");
    require((Courses[_id].issuer == msg.sender), \"You don't have permission to edit this course\"); 
    
    Courses[_id].name = _name;
    Courses[_id].dataHash = _dataHash;
    emit CourseChanged(_id, msg.sender, _name, _dataHash);
  }
 
  function get_course(bytes32 _id) external view returns (Course memory){
    return(Courses[_id]);
  }

  function get_courses() external view returns(bytes32[] memory){
    return(courseArray);
  }
  
  function get_courses_by_issuer(address _address) external view returns(bytes32[] memory){
    return(Users[_address].courseArray);
  }
  
  // =========================================
  //
  //  Endorsement Management
  //
  // =========================================
  function add_endorsement(bytes32 _certificateTemplateId, uint _endorsement_level) external{
    require(Users[msg.sender].expert, \"You need to be an expert to endorse\");
    require((CertificateTemplates[_certificateTemplateId].state > nonexistent), \"CertificateTemplate not found\");

    // get unique ID
    bytes32 id = keccak256(abi.encodePacked(_certificateTemplateId, msg.sender, endorsementArray.length));
    require((Endorsements[id].state == nonexistent ), \"Already an endorsement exists with this id. This is very unlikely.\");
    
    // Create and save endorsement
    Endorsements[id].certificateTemplateId  = _certificateTemplateId;
    Endorsements[id].endorser  = msg.sender;
    Endorsements[id].level     = _endorsement_level;
    Endorsements[id].state     = 1; 

    Courses[CertificateTemplates[_certificateTemplateId].courseId].endorsementArray.push(id);
    Users[msg.sender].endorsementArray.push(id);
    endorsementArray.push(id);
    
    // update a list of endorsements requiring approval by issuer
    Users[CertificateTemplates[_certificateTemplateId].issuer].pendingEndorsementArray.push(id);

    // emit event
    emit NewEndorsement(id, msg.sender, _certificateTemplateId);
  }

  // cancel endorsement (by endorser)
  function cancel_endorsement(bytes32 _endorsement) external{
    require((Endorsements[_endorsement].endorser == msg.sender), \"Not your endorsement to cancel\");
    Endorsements[_endorsement].state = canceled;
    emit CancelEndorsement(_endorsement, msg.sender);
  }

  // accept (or refuse) endorsement (by certificate template issuer)
  function decide_endorsement(bytes32 _endorsement, bool _decision) external{
    require((Endorsements[_endorsement].state == needs_approval), \"This endorsement does not need approval\");
    bytes32 certificateTemplateId = Endorsements[_endorsement].certificateTemplateId;
    require((CertificateTemplates[certificateTemplateId].issuer == msg.sender), \"Not your endorsement to decide\");

    Endorsements[_endorsement].state = _decision ? approved : declined;
    emit DecideEndorsement(_endorsement, msg.sender, certificateTemplateId, _decision);
  }

  function get_endorsement(bytes32 _id) external view returns (Endorsement memory){
    return(Endorsements[_id]);
  }
  
  // =========================================
  //
  //  Certificate template Management
  //
  // =========================================
  function add_certificate_template(bytes32 _courseId, string calldata _title, string calldata _description, string calldata _dataHash, uint _level) external{
    require(Users[msg.sender].issuer, \"You need to be an Issuer to create a certificate template\");
    require((Courses[_courseId].issuer == msg.sender), \"That is not your course\");
    // get unique ID
    bytes32 id = keccak256(abi.encodePacked(_courseId, msg.sender, certificateTemplateNonce++));
    require((CertificateTemplates[id].state == nonexistent ), \"Already a cerificate template exists with this id. This is very unlikely.\");
   
    CertificateTemplates[id].title       = _title;
    CertificateTemplates[id].description = _description;
    CertificateTemplates[id].dataHash    = _dataHash;
    CertificateTemplates[id].level       = _level;          // attendance, material, exams, extra
    CertificateTemplates[id].state       = active;
    CertificateTemplates[id].issuer      = msg.sender;
    CertificateTemplates[id].courseId    = _courseId;


    Courses[_courseId].certificateTemplateArray.push(id);
    emit NewCertificateTemplate(_courseId, _title, _level);
  }

  function get_certificate_template(bytes32 _id) public view returns(CertificateTemplate memory){
    return(CertificateTemplates[_id]);
  }

  // =========================================
  //
  //  Certificate Management
  //
  // =========================================
  function issue_certificate(bytes32 _templateId, address _recipient, string calldata _dataHash) external{
    CertificateTemplate memory ct = CertificateTemplates[_templateId];
    require( (ct.issuer == msg.sender), \"This is not your certificate template.\");
    require((Users[_recipient].state != nonexistent), \"The recipient does not exist\");

    // get unique ID
    bytes32 id = keccak256(abi.encodePacked(_templateId, msg.sender, certificateArray.length));
    require((Certificates[id].state == nonexistent ), \"Already a cerificate template exists with this id. This is very unlikely.\");
    
    Certificates[id].templateId = _templateId; 
    Certificates[id].recipient  = _recipient;
    Certificates[id].issuer     = msg.sender;
    Certificates[id].dataHash   = _dataHash;
    Certificates[id].state      = active;

    // add the cert to various lists
    certificateArray.push(id);
    CertificateTemplates[_templateId].certificateArray.push(id);
    Users[msg.sender].issuedCertificateArray.push(id);
    Users[_recipient].receivedCertificateArray.push(id);
    emit NewCertificate(_templateId, msg.sender, _recipient);
  }

  function get_certificate(bytes32 _id) external view returns(Certificate memory){
    return(Certificates[_id]);
  }
  
  function number_of_certificates() external view returns (uint256) {
    return certificateArray.length;
  }
} 
"
    
// SPDX-License-Identifier: MIT

pragma solidity ^0.6.0;

import \"../GSN/Context.sol\";
/**
 * @dev Contract module which provides a basic access control mechanism, where
 * there is an account (an owner) that can be granted exclusive access to
 * specific functions.
 *
 * By default, the owner account will be the one that deploys the contract. This
 * can later be changed with {transferOwnership}.
 *
 * This module is used through inheritance. It will make available the modifier
 * `onlyOwner`, which can be applied to your functions to restrict their use to
 * the owner.
 */
contract Ownable is Context {
    address private _owner;

    event OwnershipTransferred(address indexed previousOwner, address indexed newOwner);

    /**
     * @dev Initializes the contract setting the deployer as the initial owner.
     */
    constructor () internal {
        address msgSender = _msgSender();
        _owner = msgSender;
        emit OwnershipTransferred(address(0), msgSender);
    }

    /**
     * @dev Returns the address of the current owner.
     */
    function owner() public view returns (address) {
        return _owner;
    }

    /**
     * @dev Throws if called by any account other than the owner.
     */
    modifier onlyOwner() {
        require(_owner == _msgSender(), \"Ownable: caller is not the owner\");
        _;
    }

    /**
     * @dev Leaves the contract without owner. It will not be possible to call
     * `onlyOwner` functions anymore. Can only be called by the current owner.
     *
     * NOTE: Renouncing ownership will leave the contract without an owner,
     * thereby removing any functionality that is only available to the owner.
     */
    function renounceOwnership() public virtual onlyOwner {
        emit OwnershipTransferred(_owner, address(0));
        _owner = address(0);
    }

    /**
     * @dev Transfers ownership of the contract to a new account (`newOwner`).
     * Can only be called by the current owner.
     */
    function transferOwnership(address newOwner) public virtual onlyOwner {
        require(newOwner != address(0), \"Ownable: new owner is the zero address\");
        emit OwnershipTransferred(_owner, newOwner);
        _owner = newOwner;
    }
}
"
    
// SPDX-License-Identifier: MIT

pragma solidity ^0.6.0;

/*
 * @dev Provides information about the current execution context, including the
 * sender of the transaction and its data. While these are generally available
 * via msg.sender and msg.data, they should not be accessed in such a direct
 * manner, since when dealing with GSN meta-transactions the account sending and
 * paying for execution may not be the actual sender (as far as an application
 * is concerned).
 *
 * This contract is only required for intermediate, library-like contracts.
 */
abstract contract Context {
    function _msgSender() internal view virtual returns (address payable) {
        return msg.sender;
    }

    function _msgData() internal view virtual returns (bytes memory) {
        this; // silence state mutability warning without generating bytecode - see https://github.com/ethereum/solidity/issues/2691
        return msg.data;
    }
}
"
    
// SPDX-License-Identifier: MIT
pragma solidity >= 0.4.22 <0.7.0;

library console {
\taddress constant CONSOLE_ADDRESS = address(0x000000000000000000636F6e736F6c652e6c6f67);

\tfunction _sendLogPayload(bytes memory payload) private view {
\t\tuint256 payloadLength = payload.length;
\t\taddress consoleAddress = CONSOLE_ADDRESS;
\t\tassembly {
\t\t\tlet payloadStart := add(payload, 32)
\t\t\tlet r := staticcall(gas(), consoleAddress, payloadStart, payloadLength, 0, 0)
\t\t}
\t}

\tfunction log() internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log()\"));
\t}

\tfunction logInt(int p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(int)\", p0));
\t}

\tfunction logUint(uint p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint)\", p0));
\t}

\tfunction logString(string memory p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string)\", p0));
\t}

\tfunction logBool(bool p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool)\", p0));
\t}

\tfunction logAddress(address p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address)\", p0));
\t}

\tfunction logBytes(bytes memory p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes)\", p0));
\t}

\tfunction logByte(byte p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(byte)\", p0));
\t}

\tfunction logBytes1(bytes1 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes1)\", p0));
\t}

\tfunction logBytes2(bytes2 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes2)\", p0));
\t}

\tfunction logBytes3(bytes3 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes3)\", p0));
\t}

\tfunction logBytes4(bytes4 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes4)\", p0));
\t}

\tfunction logBytes5(bytes5 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes5)\", p0));
\t}

\tfunction logBytes6(bytes6 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes6)\", p0));
\t}

\tfunction logBytes7(bytes7 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes7)\", p0));
\t}

\tfunction logBytes8(bytes8 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes8)\", p0));
\t}

\tfunction logBytes9(bytes9 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes9)\", p0));
\t}

\tfunction logBytes10(bytes10 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes10)\", p0));
\t}

\tfunction logBytes11(bytes11 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes11)\", p0));
\t}

\tfunction logBytes12(bytes12 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes12)\", p0));
\t}

\tfunction logBytes13(bytes13 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes13)\", p0));
\t}

\tfunction logBytes14(bytes14 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes14)\", p0));
\t}

\tfunction logBytes15(bytes15 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes15)\", p0));
\t}

\tfunction logBytes16(bytes16 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes16)\", p0));
\t}

\tfunction logBytes17(bytes17 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes17)\", p0));
\t}

\tfunction logBytes18(bytes18 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes18)\", p0));
\t}

\tfunction logBytes19(bytes19 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes19)\", p0));
\t}

\tfunction logBytes20(bytes20 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes20)\", p0));
\t}

\tfunction logBytes21(bytes21 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes21)\", p0));
\t}

\tfunction logBytes22(bytes22 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes22)\", p0));
\t}

\tfunction logBytes23(bytes23 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes23)\", p0));
\t}

\tfunction logBytes24(bytes24 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes24)\", p0));
\t}

\tfunction logBytes25(bytes25 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes25)\", p0));
\t}

\tfunction logBytes26(bytes26 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes26)\", p0));
\t}

\tfunction logBytes27(bytes27 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes27)\", p0));
\t}

\tfunction logBytes28(bytes28 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes28)\", p0));
\t}

\tfunction logBytes29(bytes29 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes29)\", p0));
\t}

\tfunction logBytes30(bytes30 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes30)\", p0));
\t}

\tfunction logBytes31(bytes31 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes31)\", p0));
\t}

\tfunction logBytes32(bytes32 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes32)\", p0));
\t}

\tfunction log(uint p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint)\", p0));
\t}

\tfunction log(string memory p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string)\", p0));
\t}

\tfunction log(bool p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool)\", p0));
\t}

\tfunction log(address p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address)\", p0));
\t}

\tfunction log(uint p0, uint p1) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,uint)\", p0, p1));
\t}

\tfunction log(uint p0, string memory p1) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,string)\", p0, p1));
\t}

\tfunction log(uint p0, bool p1) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,bool)\", p0, p1));
\t}

\tfunction log(uint p0, address p1) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,address)\", p0, p1));
\t}

\tfunction log(string memory p0, uint p1) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,uint)\", p0, p1));
\t}

\tfunction log(string memory p0, string memory p1) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,string)\", p0, p1));
\t}

\tfunction log(string memory p0, bool p1) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,bool)\", p0, p1));
\t}

\tfunction log(string memory p0, address p1) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,address)\", p0, p1));
\t}

\tfunction log(bool p0, uint p1) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,uint)\", p0, p1));
\t}

\tfunction log(bool p0, string memory p1) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,string)\", p0, p1));
\t}

\tfunction log(bool p0, bool p1) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,bool)\", p0, p1));
\t}

\tfunction log(bool p0, address p1) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,address)\", p0, p1));
\t}

\tfunction log(address p0, uint p1) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,uint)\", p0, p1));
\t}

\tfunction log(address p0, string memory p1) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,string)\", p0, p1));
\t}

\tfunction log(address p0, bool p1) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,bool)\", p0, p1));
\t}

\tfunction log(address p0, address p1) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,address)\", p0, p1));
\t}

\tfunction log(uint p0, uint p1, uint p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,uint,uint)\", p0, p1, p2));
\t}

\tfunction log(uint p0, uint p1, string memory p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,uint,string)\", p0, p1, p2));
\t}

\tfunction log(uint p0, uint p1, bool p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,uint,bool)\", p0, p1, p2));
\t}

\tfunction log(uint p0, uint p1, address p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,uint,address)\", p0, p1, p2));
\t}

\tfunction log(uint p0, string memory p1, uint p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,string,uint)\", p0, p1, p2));
\t}

\tfunction log(uint p0, string memory p1, string memory p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,string,string)\", p0, p1, p2));
\t}

\tfunction log(uint p0, string memory p1, bool p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,string,bool)\", p0, p1, p2));
\t}

\tfunction log(uint p0, string memory p1, address p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,string,address)\", p0, p1, p2));
\t}

\tfunction log(uint p0, bool p1, uint p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,bool,uint)\", p0, p1, p2));
\t}

\tfunction log(uint p0, bool p1, string memory p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,bool,string)\", p0, p1, p2));
\t}

\tfunction log(uint p0, bool p1, bool p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,bool,bool)\", p0, p1, p2));
\t}

\tfunction log(uint p0, bool p1, address p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,bool,address)\", p0, p1, p2));
\t}

\tfunction log(uint p0, address p1, uint p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,address,uint)\", p0, p1, p2));
\t}

\tfunction log(uint p0, address p1, string memory p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,address,string)\", p0, p1, p2));
\t}

\tfunction log(uint p0, address p1, bool p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,address,bool)\", p0, p1, p2));
\t}

\tfunction log(uint p0, address p1, address p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,address,address)\", p0, p1, p2));
\t}

\tfunction log(string memory p0, uint p1, uint p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,uint,uint)\", p0, p1, p2));
\t}

\tfunction log(string memory p0, uint p1, string memory p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,uint,string)\", p0, p1, p2));
\t}

\tfunction log(string memory p0, uint p1, bool p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,uint,bool)\", p0, p1, p2));
\t}

\tfunction log(string memory p0, uint p1, address p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,uint,address)\", p0, p1, p2));
\t}

\tfunction log(string memory p0, string memory p1, uint p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,string,uint)\", p0, p1, p2));
\t}

\tfunction log(string memory p0, string memory p1, string memory p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,string,string)\", p0, p1, p2));
\t}

\tfunction log(string memory p0, string memory p1, bool p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,string,bool)\", p0, p1, p2));
\t}

\tfunction log(string memory p0, string memory p1, address p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,string,address)\", p0, p1, p2));
\t}

\tfunction log(string memory p0, bool p1, uint p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,bool,uint)\", p0, p1, p2));
\t}

\tfunction log(string memory p0, bool p1, string memory p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,bool,string)\", p0, p1, p2));
\t}

\tfunction log(string memory p0, bool p1, bool p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,bool,bool)\", p0, p1, p2));
\t}

\tfunction log(string memory p0, bool p1, address p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,bool,address)\", p0, p1, p2));
\t}

\tfunction log(string memory p0, address p1, uint p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,address,uint)\", p0, p1, p2));
\t}

\tfunction log(string memory p0, address p1, string memory p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,address,string)\", p0, p1, p2));
\t}

\tfunction log(string memory p0, address p1, bool p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,address,bool)\", p0, p1, p2));
\t}

\tfunction log(string memory p0, address p1, address p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,address,address)\", p0, p1, p2));
\t}

\tfunction log(bool p0, uint p1, uint p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,uint,uint)\", p0, p1, p2));
\t}

\tfunction log(bool p0, uint p1, string memory p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,uint,string)\", p0, p1, p2));
\t}

\tfunction log(bool p0, uint p1, bool p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,uint,bool)\", p0, p1, p2));
\t}

\tfunction log(bool p0, uint p1, address p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,uint,address)\", p0, p1, p2));
\t}

\tfunction log(bool p0, string memory p1, uint p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,string,uint)\", p0, p1, p2));
\t}

\tfunction log(bool p0, string memory p1, string memory p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,string,string)\", p0, p1, p2));
\t}

\tfunction log(bool p0, string memory p1, bool p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,string,bool)\", p0, p1, p2));
\t}

\tfunction log(bool p0, string memory p1, address p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,string,address)\", p0, p1, p2));
\t}

\tfunction log(bool p0, bool p1, uint p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,bool,uint)\", p0, p1, p2));
\t}

\tfunction log(bool p0, bool p1, string memory p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,bool,string)\", p0, p1, p2));
\t}

\tfunction log(bool p0, bool p1, bool p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,bool,bool)\", p0, p1, p2));
\t}

\tfunction log(bool p0, bool p1, address p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,bool,address)\", p0, p1, p2));
\t}

\tfunction log(bool p0, address p1, uint p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,address,uint)\", p0, p1, p2));
\t}

\tfunction log(bool p0, address p1, string memory p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,address,string)\", p0, p1, p2));
\t}

\tfunction log(bool p0, address p1, bool p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,address,bool)\", p0, p1, p2));
\t}

\tfunction log(bool p0, address p1, address p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,address,address)\", p0, p1, p2));
\t}

\tfunction log(address p0, uint p1, uint p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,uint,uint)\", p0, p1, p2));
\t}

\tfunction log(address p0, uint p1, string memory p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,uint,string)\", p0, p1, p2));
\t}

\tfunction log(address p0, uint p1, bool p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,uint,bool)\", p0, p1, p2));
\t}

\tfunction log(address p0, uint p1, address p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,uint,address)\", p0, p1, p2));
\t}

\tfunction log(address p0, string memory p1, uint p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,string,uint)\", p0, p1, p2));
\t}

\tfunction log(address p0, string memory p1, string memory p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,string,string)\", p0, p1, p2));
\t}

\tfunction log(address p0, string memory p1, bool p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,string,bool)\", p0, p1, p2));
\t}

\tfunction log(address p0, string memory p1, address p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,string,address)\", p0, p1, p2));
\t}

\tfunction log(address p0, bool p1, uint p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,bool,uint)\", p0, p1, p2));
\t}

\tfunction log(address p0, bool p1, string memory p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,bool,string)\", p0, p1, p2));
\t}

\tfunction log(address p0, bool p1, bool p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,bool,bool)\", p0, p1, p2));
\t}

\tfunction log(address p0, bool p1, address p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,bool,address)\", p0, p1, p2));
\t}

\tfunction log(address p0, address p1, uint p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,address,uint)\", p0, p1, p2));
\t}

\tfunction log(address p0, address p1, string memory p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,address,string)\", p0, p1, p2));
\t}

\tfunction log(address p0, address p1, bool p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,address,bool)\", p0, p1, p2));
\t}

\tfunction log(address p0, address p1, address p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,address,address)\", p0, p1, p2));
\t}

\tfunction log(uint p0, uint p1, uint p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,uint,uint,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, uint p1, uint p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,uint,uint,string)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, uint p1, uint p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,uint,uint,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, uint p1, uint p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,uint,uint,address)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, uint p1, string memory p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,uint,string,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, uint p1, string memory p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,uint,string,string)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, uint p1, string memory p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,uint,string,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, uint p1, string memory p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,uint,string,address)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, uint p1, bool p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,uint,bool,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, uint p1, bool p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,uint,bool,string)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, uint p1, bool p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,uint,bool,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, uint p1, bool p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,uint,bool,address)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, uint p1, address p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,uint,address,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, uint p1, address p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,uint,address,string)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, uint p1, address p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,uint,address,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, uint p1, address p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,uint,address,address)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, string memory p1, uint p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,string,uint,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, string memory p1, uint p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,string,uint,string)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, string memory p1, uint p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,string,uint,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, string memory p1, uint p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,string,uint,address)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, string memory p1, string memory p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,string,string,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, string memory p1, string memory p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,string,string,string)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, string memory p1, string memory p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,string,string,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, string memory p1, string memory p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,string,string,address)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, string memory p1, bool p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,string,bool,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, string memory p1, bool p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,string,bool,string)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, string memory p1, bool p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,string,bool,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, string memory p1, bool p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,string,bool,address)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, string memory p1, address p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,string,address,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, string memory p1, address p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,string,address,string)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, string memory p1, address p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,string,address,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, string memory p1, address p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,string,address,address)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, bool p1, uint p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,bool,uint,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, bool p1, uint p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,bool,uint,string)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, bool p1, uint p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,bool,uint,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, bool p1, uint p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,bool,uint,address)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, bool p1, string memory p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,bool,string,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, bool p1, string memory p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,bool,string,string)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, bool p1, string memory p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,bool,string,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, bool p1, string memory p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,bool,string,address)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, bool p1, bool p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,bool,bool,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, bool p1, bool p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,bool,bool,string)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, bool p1, bool p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,bool,bool,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, bool p1, bool p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,bool,bool,address)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, bool p1, address p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,bool,address,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, bool p1, address p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,bool,address,string)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, bool p1, address p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,bool,address,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, bool p1, address p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,bool,address,address)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, address p1, uint p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,address,uint,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, address p1, uint p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,address,uint,string)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, address p1, uint p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,address,uint,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, address p1, uint p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,address,uint,address)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, address p1, string memory p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,address,string,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, address p1, string memory p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,address,string,string)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, address p1, string memory p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,address,string,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, address p1, string memory p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,address,string,address)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, address p1, bool p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,address,bool,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, address p1, bool p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,address,bool,string)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, address p1, bool p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,address,bool,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, address p1, bool p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,address,bool,address)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, address p1, address p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,address,address,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, address p1, address p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,address,address,string)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, address p1, address p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,address,address,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, address p1, address p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,address,address,address)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, uint p1, uint p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,uint,uint,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, uint p1, uint p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,uint,uint,string)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, uint p1, uint p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,uint,uint,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, uint p1, uint p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,uint,uint,address)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, uint p1, string memory p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,uint,string,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, uint p1, string memory p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,uint,string,string)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, uint p1, string memory p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,uint,string,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, uint p1, string memory p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,uint,string,address)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, uint p1, bool p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,uint,bool,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, uint p1, bool p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,uint,bool,string)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, uint p1, bool p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,uint,bool,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, uint p1, bool p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,uint,bool,address)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, uint p1, address p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,uint,address,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, uint p1, address p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,uint,address,string)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, uint p1, address p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,uint,address,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, uint p1, address p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,uint,address,address)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, string memory p1, uint p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,string,uint,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, string memory p1, uint p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,string,uint,string)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, string memory p1, uint p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,string,uint,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, string memory p1, uint p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,string,uint,address)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, string memory p1, string memory p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,string,string,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, string memory p1, string memory p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,string,string,string)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, string memory p1, string memory p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,string,string,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, string memory p1, string memory p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,string,string,address)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, string memory p1, bool p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,string,bool,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, string memory p1, bool p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,string,bool,string)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, string memory p1, bool p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,string,bool,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, string memory p1, bool p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,string,bool,address)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, string memory p1, address p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,string,address,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, string memory p1, address p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,string,address,string)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, string memory p1, address p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,string,address,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, string memory p1, address p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,string,address,address)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, bool p1, uint p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,bool,uint,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, bool p1, uint p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,bool,uint,string)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, bool p1, uint p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,bool,uint,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, bool p1, uint p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,bool,uint,address)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, bool p1, string memory p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,bool,string,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, bool p1, string memory p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,bool,string,string)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, bool p1, string memory p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,bool,string,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, bool p1, string memory p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,bool,string,address)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, bool p1, bool p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,bool,bool,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, bool p1, bool p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,bool,bool,string)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, bool p1, bool p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,bool,bool,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, bool p1, bool p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,bool,bool,address)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, bool p1, address p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,bool,address,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, bool p1, address p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,bool,address,string)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, bool p1, address p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,bool,address,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, bool p1, address p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,bool,address,address)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, address p1, uint p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,address,uint,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, address p1, uint p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,address,uint,string)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, address p1, uint p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,address,uint,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, address p1, uint p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,address,uint,address)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, address p1, string memory p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,address,string,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, address p1, string memory p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,address,string,string)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, address p1, string memory p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,address,string,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, address p1, string memory p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,address,string,address)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, address p1, bool p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,address,bool,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, address p1, bool p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,address,bool,string)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, address p1, bool p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,address,bool,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, address p1, bool p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,address,bool,address)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, address p1, address p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,address,address,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, address p1, address p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,address,address,string)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, address p1, address p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,address,address,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, address p1, address p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,address,address,address)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, uint p1, uint p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,uint,uint,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, uint p1, uint p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,uint,uint,string)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, uint p1, uint p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,uint,uint,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, uint p1, uint p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,uint,uint,address)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, uint p1, string memory p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,uint,string,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, uint p1, string memory p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,uint,string,string)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, uint p1, string memory p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,uint,string,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, uint p1, string memory p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,uint,string,address)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, uint p1, bool p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,uint,bool,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, uint p1, bool p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,uint,bool,string)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, uint p1, bool p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,uint,bool,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, uint p1, bool p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,uint,bool,address)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, uint p1, address p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,uint,address,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, uint p1, address p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,uint,address,string)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, uint p1, address p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,uint,address,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, uint p1, address p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,uint,address,address)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, string memory p1, uint p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,string,uint,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, string memory p1, uint p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,string,uint,string)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, string memory p1, uint p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,string,uint,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, string memory p1, uint p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,string,uint,address)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, string memory p1, string memory p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,string,string,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, string memory p1, string memory p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,string,string,string)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, string memory p1, string memory p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,string,string,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, string memory p1, string memory p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,string,string,address)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, string memory p1, bool p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,string,bool,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, string memory p1, bool p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,string,bool,string)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, string memory p1, bool p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,string,bool,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, string memory p1, bool p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,string,bool,address)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, string memory p1, address p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,string,address,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, string memory p1, address p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,string,address,string)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, string memory p1, address p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,string,address,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, string memory p1, address p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,string,address,address)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, bool p1, uint p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,bool,uint,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, bool p1, uint p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,bool,uint,string)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, bool p1, uint p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,bool,uint,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, bool p1, uint p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,bool,uint,address)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, bool p1, string memory p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,bool,string,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, bool p1, string memory p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,bool,string,string)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, bool p1, string memory p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,bool,string,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, bool p1, string memory p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,bool,string,address)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, bool p1, bool p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,bool,bool,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, bool p1, bool p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,bool,bool,string)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, bool p1, bool p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,bool,bool,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, bool p1, bool p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,bool,bool,address)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, bool p1, address p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,bool,address,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, bool p1, address p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,bool,address,string)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, bool p1, address p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,bool,address,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, bool p1, address p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,bool,address,address)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, address p1, uint p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,address,uint,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, address p1, uint p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,address,uint,string)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, address p1, uint p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,address,uint,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, address p1, uint p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,address,uint,address)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, address p1, string memory p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,address,string,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, address p1, string memory p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,address,string,string)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, address p1, string memory p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,address,string,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, address p1, string memory p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,address,string,address)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, address p1, bool p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,address,bool,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, address p1, bool p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,address,bool,string)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, address p1, bool p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,address,bool,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, address p1, bool p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,address,bool,address)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, address p1, address p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,address,address,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, address p1, address p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,address,address,string)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, address p1, address p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,address,address,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, address p1, address p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,address,address,address)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, uint p1, uint p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,uint,uint,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, uint p1, uint p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,uint,uint,string)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, uint p1, uint p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,uint,uint,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, uint p1, uint p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,uint,uint,address)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, uint p1, string memory p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,uint,string,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, uint p1, string memory p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,uint,string,string)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, uint p1, string memory p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,uint,string,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, uint p1, string memory p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,uint,string,address)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, uint p1, bool p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,uint,bool,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, uint p1, bool p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,uint,bool,string)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, uint p1, bool p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,uint,bool,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, uint p1, bool p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,uint,bool,address)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, uint p1, address p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,uint,address,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, uint p1, address p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,uint,address,string)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, uint p1, address p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,uint,address,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, uint p1, address p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,uint,address,address)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, string memory p1, uint p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,string,uint,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, string memory p1, uint p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,string,uint,string)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, string memory p1, uint p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,string,uint,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, string memory p1, uint p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,string,uint,address)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, string memory p1, string memory p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,string,string,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, string memory p1, string memory p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,string,string,string)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, string memory p1, string memory p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,string,string,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, string memory p1, string memory p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,string,string,address)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, string memory p1, bool p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,string,bool,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, string memory p1, bool p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,string,bool,string)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, string memory p1, bool p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,string,bool,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, string memory p1, bool p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,string,bool,address)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, string memory p1, address p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,string,address,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, string memory p1, address p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,string,address,string)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, string memory p1, address p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,string,address,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, string memory p1, address p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,string,address,address)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, bool p1, uint p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,bool,uint,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, bool p1, uint p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,bool,uint,string)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, bool p1, uint p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,bool,uint,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, bool p1, uint p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,bool,uint,address)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, bool p1, string memory p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,bool,string,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, bool p1, string memory p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,bool,string,string)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, bool p1, string memory p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,bool,string,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, bool p1, string memory p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,bool,string,address)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, bool p1, bool p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,bool,bool,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, bool p1, bool p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,bool,bool,string)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, bool p1, bool p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,bool,bool,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, bool p1, bool p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,bool,bool,address)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, bool p1, address p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,bool,address,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, bool p1, address p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,bool,address,string)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, bool p1, address p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,bool,address,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, bool p1, address p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,bool,address,address)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, address p1, uint p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,address,uint,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, address p1, uint p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,address,uint,string)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, address p1, uint p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,address,uint,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, address p1, uint p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,address,uint,address)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, address p1, string memory p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,address,string,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, address p1, string memory p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,address,string,string)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, address p1, string memory p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,address,string,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, address p1, string memory p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,address,string,address)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, address p1, bool p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,address,bool,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, address p1, bool p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,address,bool,string)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, address p1, bool p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,address,bool,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, address p1, bool p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,address,bool,address)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, address p1, address p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,address,address,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, address p1, address p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,address,address,string)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, address p1, address p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,address,address,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, address p1, address p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,address,address,address)\", p0, p1, p2, p3));
\t}

}
"
    }
  
