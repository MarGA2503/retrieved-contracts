// SPDX-License-Identifier: BSD-4-Clause
/*
 * ABDK Math 64.64 Smart Contract Library.  Copyright © 2019 by ABDK Consulting.
 * Author: Mikhail Vladimirov \u003cmikhail.vladimirov@gmail.com\u003e
 */
pragma solidity ^0.7.0;

/**
 * Smart contract library of mathematical functions operating with signed
 * 64.64-bit fixed point numbers.  Signed 64.64-bit fixed point number is
 * basically a simple fraction whose numerator is signed 128-bit integer and
 * denominator is 2^64.  As long as denominator is always the same, there is no
 * need to store it, thus in Solidity signed 64.64-bit fixed point numbers are
 * represented by int128 type holding only the numerator.
 */
library ABDKMath64x64 {
  /*
   * Minimum value signed 64.64-bit fixed point number may have. 
   */
  int128 private constant MIN_64x64 = -0x80000000000000000000000000000000;

  /*
   * Maximum value signed 64.64-bit fixed point number may have. 
   */
  int128 private constant MAX_64x64 = 0x7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;

  /**
   * Convert signed 256-bit integer number into signed 64.64-bit fixed point
   * number.  Revert on overflow.
   *
   * @param x signed 256-bit integer number
   * @return signed 64.64-bit fixed point number
   */
  function fromInt (int256 x) internal pure returns (int128) {
    require (x \u003e= -0x8000000000000000 \u0026\u0026 x \u003c= 0x7FFFFFFFFFFFFFFF);
    return int128 (x \u003c\u003c 64);
  }

  /**
   * Convert signed 64.64 fixed point number into signed 64-bit integer number
   * rounding down.
   *
   * @param x signed 64.64-bit fixed point number
   * @return signed 64-bit integer number
   */
  function toInt (int128 x) internal pure returns (int64) {
    return int64 (x \u003e\u003e 64);
  }

  /**
   * Convert unsigned 256-bit integer number into signed 64.64-bit fixed point
   * number.  Revert on overflow.
   *
   * @param x unsigned 256-bit integer number
   * @return signed 64.64-bit fixed point number
   */
  function fromUInt (uint256 x) internal pure returns (int128) {
    require (x \u003c= 0x7FFFFFFFFFFFFFFF);
    return int128 (x \u003c\u003c 64);
  }

  /**
   * Convert signed 64.64 fixed point number into unsigned 64-bit integer
   * number rounding down.  Revert on underflow.
   *
   * @param x signed 64.64-bit fixed point number
   * @return unsigned 64-bit integer number
   */
  function toUInt (int128 x) internal pure returns (uint64) {
    require (x \u003e= 0);
    return uint64 (x \u003e\u003e 64);
  }

  /**
   * Convert signed 128.128 fixed point number into signed 64.64-bit fixed point
   * number rounding down.  Revert on overflow.
   *
   * @param x signed 128.128-bin fixed point number
   * @return signed 64.64-bit fixed point number
   */
  function from128x128 (int256 x) internal pure returns (int128) {
    int256 result = x \u003e\u003e 64;
    require (result \u003e= MIN_64x64 \u0026\u0026 result \u003c= MAX_64x64);
    return int128 (result);
  }

  /**
   * Convert signed 64.64 fixed point number into signed 128.128 fixed point
   * number.
   *
   * @param x signed 64.64-bit fixed point number
   * @return signed 128.128 fixed point number
   */
  function to128x128 (int128 x) internal pure returns (int256) {
    return int256 (x) \u003c\u003c 64;
  }

  /**
   * Calculate x + y.  Revert on overflow.
   *
   * @param x signed 64.64-bit fixed point number
   * @param y signed 64.64-bit fixed point number
   * @return signed 64.64-bit fixed point number
   */
  function add (int128 x, int128 y) internal pure returns (int128) {
    int256 result = int256(x) + y;
    require (result \u003e= MIN_64x64 \u0026\u0026 result \u003c= MAX_64x64);
    return int128 (result);
  }

  /**
   * Calculate x - y.  Revert on overflow.
   *
   * @param x signed 64.64-bit fixed point number
   * @param y signed 64.64-bit fixed point number
   * @return signed 64.64-bit fixed point number
   */
  function sub (int128 x, int128 y) internal pure returns (int128) {
    int256 result = int256(x) - y;
    require (result \u003e= MIN_64x64 \u0026\u0026 result \u003c= MAX_64x64);
    return int128 (result);
  }

  /**
   * Calculate x * y rounding down.  Revert on overflow.
   *
   * @param x signed 64.64-bit fixed point number
   * @param y signed 64.64-bit fixed point number
   * @return signed 64.64-bit fixed point number
   */
  function mul (int128 x, int128 y) internal pure returns (int128) {
    int256 result = int256(x) * y \u003e\u003e 64;
    require (result \u003e= MIN_64x64 \u0026\u0026 result \u003c= MAX_64x64);
    return int128 (result);
  }

  /**
   * Calculate x * y rounding towards zero, where x is signed 64.64 fixed point
   * number and y is signed 256-bit integer number.  Revert on overflow.
   *
   * @param x signed 64.64 fixed point number
   * @param y signed 256-bit integer number
   * @return signed 256-bit integer number
   */
  function muli (int128 x, int256 y) internal pure returns (int256) {
    if (x == MIN_64x64) {
      require (y \u003e= -0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF \u0026\u0026
        y \u003c= 0x1000000000000000000000000000000000000000000000000);
      return -y \u003c\u003c 63;
    } else {
      bool negativeResult = false;
      if (x \u003c 0) {
        x = -x;
        negativeResult = true;
      }
      if (y \u003c 0) {
        y = -y; // We rely on overflow behavior here
        negativeResult = !negativeResult;
      }
      uint256 absoluteResult = mulu (x, uint256 (y));
      if (negativeResult) {
        require (absoluteResult \u003c=
          0x8000000000000000000000000000000000000000000000000000000000000000);
        return -int256 (absoluteResult); // We rely on overflow behavior here
      } else {
        require (absoluteResult \u003c=
          0x7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF);
        return int256 (absoluteResult);
      }
    }
  }

  /**
   * Calculate x * y rounding down, where x is signed 64.64 fixed point number
   * and y is unsigned 256-bit integer number.  Revert on overflow.
   *
   * @param x signed 64.64 fixed point number
   * @param y unsigned 256-bit integer number
   * @return unsigned 256-bit integer number
   */
  function mulu (int128 x, uint256 y) internal pure returns (uint256) {
    if (y == 0) return 0;

    require (x \u003e= 0);

    uint256 lo = (uint256 (x) * (y \u0026 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)) \u003e\u003e 64;
    uint256 hi = uint256 (x) * (y \u003e\u003e 128);

    require (hi \u003c= 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF);
    hi \u003c\u003c= 64;

    require (hi \u003c=
      0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF - lo);
    return hi + lo;
  }

  /**
   * Calculate x / y rounding towards zero.  Revert on overflow or when y is
   * zero.
   *
   * @param x signed 64.64-bit fixed point number
   * @param y signed 64.64-bit fixed point number
   * @return signed 64.64-bit fixed point number
   */
  function div (int128 x, int128 y) internal pure returns (int128) {
    require (y != 0);
    int256 result = (int256 (x) \u003c\u003c 64) / y;
    require (result \u003e= MIN_64x64 \u0026\u0026 result \u003c= MAX_64x64);
    return int128 (result);
  }

  /**
   * Calculate x / y rounding towards zero, where x and y are signed 256-bit
   * integer numbers.  Revert on overflow or when y is zero.
   *
   * @param x signed 256-bit integer number
   * @param y signed 256-bit integer number
   * @return signed 64.64-bit fixed point number
   */
  function divi (int256 x, int256 y) internal pure returns (int128) {
    require (y != 0);

    bool negativeResult = false;
    if (x \u003c 0) {
      x = -x; // We rely on overflow behavior here
      negativeResult = true;
    }
    if (y \u003c 0) {
      y = -y; // We rely on overflow behavior here
      negativeResult = !negativeResult;
    }
    uint128 absoluteResult = divuu (uint256 (x), uint256 (y));
    if (negativeResult) {
      require (absoluteResult \u003c= 0x80000000000000000000000000000000);
      return -int128 (absoluteResult); // We rely on overflow behavior here
    } else {
      require (absoluteResult \u003c= 0x7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF);
      return int128 (absoluteResult); // We rely on overflow behavior here
    }
  }

  /**
   * Calculate x / y rounding towards zero, where x and y are unsigned 256-bit
   * integer numbers.  Revert on overflow or when y is zero.
   *
   * @param x unsigned 256-bit integer number
   * @param y unsigned 256-bit integer number
   * @return signed 64.64-bit fixed point number
   */
  function divu (uint256 x, uint256 y) internal pure returns (int128) {
    require (y != 0);
    uint128 result = divuu (x, y);
    require (result \u003c= uint128 (MAX_64x64));
    return int128 (result);
  }

  /**
   * Calculate -x.  Revert on overflow.
   *
   * @param x signed 64.64-bit fixed point number
   * @return signed 64.64-bit fixed point number
   */
  function neg (int128 x) internal pure returns (int128) {
    require (x != MIN_64x64);
    return -x;
  }

  /**
   * Calculate |x|.  Revert on overflow.
   *
   * @param x signed 64.64-bit fixed point number
   * @return signed 64.64-bit fixed point number
   */
  function abs (int128 x) internal pure returns (int128) {
    require (x != MIN_64x64);
    return x \u003c 0 ? -x : x;
  }

  /**
   * Calculate 1 / x rounding towards zero.  Revert on overflow or when x is
   * zero.
   *
   * @param x signed 64.64-bit fixed point number
   * @return signed 64.64-bit fixed point number
   */
  function inv (int128 x) internal pure returns (int128) {
    require (x != 0);
    int256 result = int256 (0x100000000000000000000000000000000) / x;
    require (result \u003e= MIN_64x64 \u0026\u0026 result \u003c= MAX_64x64);
    return int128 (result);
  }

  /**
   * Calculate arithmetics average of x and y, i.e. (x + y) / 2 rounding down.
   *
   * @param x signed 64.64-bit fixed point number
   * @param y signed 64.64-bit fixed point number
   * @return signed 64.64-bit fixed point number
   */
  function avg (int128 x, int128 y) internal pure returns (int128) {
    return int128 ((int256 (x) + int256 (y)) \u003e\u003e 1);
  }

  /**
   * Calculate geometric average of x and y, i.e. sqrt (x * y) rounding down.
   * Revert on overflow or in case x * y is negative.
   *
   * @param x signed 64.64-bit fixed point number
   * @param y signed 64.64-bit fixed point number
   * @return signed 64.64-bit fixed point number
   */
  function gavg (int128 x, int128 y) internal pure returns (int128) {
    int256 m = int256 (x) * int256 (y);
    require (m \u003e= 0);
    require (m \u003c
        0x4000000000000000000000000000000000000000000000000000000000000000);
    return int128 (sqrtu (uint256 (m)));
  }

  /**
   * Calculate x^y assuming 0^0 is 1, where x is signed 64.64 fixed point number
   * and y is unsigned 256-bit integer number.  Revert on overflow.
   *
   * @param x signed 64.64-bit fixed point number
   * @param y uint256 value
   * @return signed 64.64-bit fixed point number
   */
  function pow (int128 x, uint256 y) internal pure returns (int128) {
    uint256 absoluteResult;
    bool negativeResult = false;
    if (x \u003e= 0) {
      absoluteResult = powu (uint256 (x) \u003c\u003c 63, y);
    } else {
      // We rely on overflow behavior here
      absoluteResult = powu (uint256 (uint128 (-x)) \u003c\u003c 63, y);
      negativeResult = y \u0026 1 \u003e 0;
    }

    absoluteResult \u003e\u003e= 63;

    if (negativeResult) {
      require (absoluteResult \u003c= 0x80000000000000000000000000000000);
      return -int128 (absoluteResult); // We rely on overflow behavior here
    } else {
      require (absoluteResult \u003c= 0x7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF);
      return int128 (absoluteResult); // We rely on overflow behavior here
    }
  }

  /**
   * Calculate sqrt (x) rounding down.  Revert if x \u003c 0.
   *
   * @param x signed 64.64-bit fixed point number
   * @return signed 64.64-bit fixed point number
   */
  function sqrt (int128 x) internal pure returns (int128) {
    require (x \u003e= 0);
    return int128 (sqrtu (uint256 (x) \u003c\u003c 64));
  }

  /**
   * Calculate binary logarithm of x.  Revert if x \u003c= 0.
   *
   * @param x signed 64.64-bit fixed point number
   * @return signed 64.64-bit fixed point number
   */
  function log_2 (int128 x) internal pure returns (int128) {
    require (x \u003e 0);

    int256 msb = 0;
    int256 xc = x;
    if (xc \u003e= 0x10000000000000000) { xc \u003e\u003e= 64; msb += 64; }
    if (xc \u003e= 0x100000000) { xc \u003e\u003e= 32; msb += 32; }
    if (xc \u003e= 0x10000) { xc \u003e\u003e= 16; msb += 16; }
    if (xc \u003e= 0x100) { xc \u003e\u003e= 8; msb += 8; }
    if (xc \u003e= 0x10) { xc \u003e\u003e= 4; msb += 4; }
    if (xc \u003e= 0x4) { xc \u003e\u003e= 2; msb += 2; }
    if (xc \u003e= 0x2) msb += 1;  // No need to shift xc anymore

    int256 result = msb - 64 \u003c\u003c 64;
    uint256 ux = uint256 (x) \u003c\u003c uint256 (127 - msb);
    for (int256 bit = 0x8000000000000000; bit \u003e 0; bit \u003e\u003e= 1) {
      ux *= ux;
      uint256 b = ux \u003e\u003e 255;
      ux \u003e\u003e= 127 + b;
      result += bit * int256 (b);
    }

    return int128 (result);
  }

  /**
   * Calculate natural logarithm of x.  Revert if x \u003c= 0.
   *
   * @param x signed 64.64-bit fixed point number
   * @return signed 64.64-bit fixed point number
   */
  function ln (int128 x) internal pure returns (int128) {
    require (x \u003e 0);

    return int128 (
        uint256 (log_2 (x)) * 0xB17217F7D1CF79ABC9E3B39803F2F6AF \u003e\u003e 128);
  }

  /**
   * Calculate binary exponent of x.  Revert on overflow.
   *
   * @param x signed 64.64-bit fixed point number
   * @return signed 64.64-bit fixed point number
   */
  function exp_2 (int128 x) internal pure returns (int128) {
    require (x \u003c 0x400000000000000000); // Overflow

    if (x \u003c -0x400000000000000000) return 0; // Underflow

    uint256 result = 0x80000000000000000000000000000000;

    if (x \u0026 0x8000000000000000 \u003e 0)
      result = result * 0x16A09E667F3BCC908B2FB1366EA957D3E \u003e\u003e 128;
    if (x \u0026 0x4000000000000000 \u003e 0)
      result = result * 0x1306FE0A31B7152DE8D5A46305C85EDEC \u003e\u003e 128;
    if (x \u0026 0x2000000000000000 \u003e 0)
      result = result * 0x1172B83C7D517ADCDF7C8C50EB14A791F \u003e\u003e 128;
    if (x \u0026 0x1000000000000000 \u003e 0)
      result = result * 0x10B5586CF9890F6298B92B71842A98363 \u003e\u003e 128;
    if (x \u0026 0x800000000000000 \u003e 0)
      result = result * 0x1059B0D31585743AE7C548EB68CA417FD \u003e\u003e 128;
    if (x \u0026 0x400000000000000 \u003e 0)
      result = result * 0x102C9A3E778060EE6F7CACA4F7A29BDE8 \u003e\u003e 128;
    if (x \u0026 0x200000000000000 \u003e 0)
      result = result * 0x10163DA9FB33356D84A66AE336DCDFA3F \u003e\u003e 128;
    if (x \u0026 0x100000000000000 \u003e 0)
      result = result * 0x100B1AFA5ABCBED6129AB13EC11DC9543 \u003e\u003e 128;
    if (x \u0026 0x80000000000000 \u003e 0)
      result = result * 0x10058C86DA1C09EA1FF19D294CF2F679B \u003e\u003e 128;
    if (x \u0026 0x40000000000000 \u003e 0)
      result = result * 0x1002C605E2E8CEC506D21BFC89A23A00F \u003e\u003e 128;
    if (x \u0026 0x20000000000000 \u003e 0)
      result = result * 0x100162F3904051FA128BCA9C55C31E5DF \u003e\u003e 128;
    if (x \u0026 0x10000000000000 \u003e 0)
      result = result * 0x1000B175EFFDC76BA38E31671CA939725 \u003e\u003e 128;
    if (x \u0026 0x8000000000000 \u003e 0)
      result = result * 0x100058BA01FB9F96D6CACD4B180917C3D \u003e\u003e 128;
    if (x \u0026 0x4000000000000 \u003e 0)
      result = result * 0x10002C5CC37DA9491D0985C348C68E7B3 \u003e\u003e 128;
    if (x \u0026 0x2000000000000 \u003e 0)
      result = result * 0x1000162E525EE054754457D5995292026 \u003e\u003e 128;
    if (x \u0026 0x1000000000000 \u003e 0)
      result = result * 0x10000B17255775C040618BF4A4ADE83FC \u003e\u003e 128;
    if (x \u0026 0x800000000000 \u003e 0)
      result = result * 0x1000058B91B5BC9AE2EED81E9B7D4CFAB \u003e\u003e 128;
    if (x \u0026 0x400000000000 \u003e 0)
      result = result * 0x100002C5C89D5EC6CA4D7C8ACC017B7C9 \u003e\u003e 128;
    if (x \u0026 0x200000000000 \u003e 0)
      result = result * 0x10000162E43F4F831060E02D839A9D16D \u003e\u003e 128;
    if (x \u0026 0x100000000000 \u003e 0)
      result = result * 0x100000B1721BCFC99D9F890EA06911763 \u003e\u003e 128;
    if (x \u0026 0x80000000000 \u003e 0)
      result = result * 0x10000058B90CF1E6D97F9CA14DBCC1628 \u003e\u003e 128;
    if (x \u0026 0x40000000000 \u003e 0)
      result = result * 0x1000002C5C863B73F016468F6BAC5CA2B \u003e\u003e 128;
    if (x \u0026 0x20000000000 \u003e 0)
      result = result * 0x100000162E430E5A18F6119E3C02282A5 \u003e\u003e 128;
    if (x \u0026 0x10000000000 \u003e 0)
      result = result * 0x1000000B1721835514B86E6D96EFD1BFE \u003e\u003e 128;
    if (x \u0026 0x8000000000 \u003e 0)
      result = result * 0x100000058B90C0B48C6BE5DF846C5B2EF \u003e\u003e 128;
    if (x \u0026 0x4000000000 \u003e 0)
      result = result * 0x10000002C5C8601CC6B9E94213C72737A \u003e\u003e 128;
    if (x \u0026 0x2000000000 \u003e 0)
      result = result * 0x1000000162E42FFF037DF38AA2B219F06 \u003e\u003e 128;
    if (x \u0026 0x1000000000 \u003e 0)
      result = result * 0x10000000B17217FBA9C739AA5819F44F9 \u003e\u003e 128;
    if (x \u0026 0x800000000 \u003e 0)
      result = result * 0x1000000058B90BFCDEE5ACD3C1CEDC823 \u003e\u003e 128;
    if (x \u0026 0x400000000 \u003e 0)
      result = result * 0x100000002C5C85FE31F35A6A30DA1BE50 \u003e\u003e 128;
    if (x \u0026 0x200000000 \u003e 0)
      result = result * 0x10000000162E42FF0999CE3541B9FFFCF \u003e\u003e 128;
    if (x \u0026 0x100000000 \u003e 0)
      result = result * 0x100000000B17217F80F4EF5AADDA45554 \u003e\u003e 128;
    if (x \u0026 0x80000000 \u003e 0)
      result = result * 0x10000000058B90BFBF8479BD5A81B51AD \u003e\u003e 128;
    if (x \u0026 0x40000000 \u003e 0)
      result = result * 0x1000000002C5C85FDF84BD62AE30A74CC \u003e\u003e 128;
    if (x \u0026 0x20000000 \u003e 0)
      result = result * 0x100000000162E42FEFB2FED257559BDAA \u003e\u003e 128;
    if (x \u0026 0x10000000 \u003e 0)
      result = result * 0x1000000000B17217F7D5A7716BBA4A9AE \u003e\u003e 128;
    if (x \u0026 0x8000000 \u003e 0)
      result = result * 0x100000000058B90BFBE9DDBAC5E109CCE \u003e\u003e 128;
    if (x \u0026 0x4000000 \u003e 0)
      result = result * 0x10000000002C5C85FDF4B15DE6F17EB0D \u003e\u003e 128;
    if (x \u0026 0x2000000 \u003e 0)
      result = result * 0x1000000000162E42FEFA494F1478FDE05 \u003e\u003e 128;
    if (x \u0026 0x1000000 \u003e 0)
      result = result * 0x10000000000B17217F7D20CF927C8E94C \u003e\u003e 128;
    if (x \u0026 0x800000 \u003e 0)
      result = result * 0x1000000000058B90BFBE8F71CB4E4B33D \u003e\u003e 128;
    if (x \u0026 0x400000 \u003e 0)
      result = result * 0x100000000002C5C85FDF477B662B26945 \u003e\u003e 128;
    if (x \u0026 0x200000 \u003e 0)
      result = result * 0x10000000000162E42FEFA3AE53369388C \u003e\u003e 128;
    if (x \u0026 0x100000 \u003e 0)
      result = result * 0x100000000000B17217F7D1D351A389D40 \u003e\u003e 128;
    if (x \u0026 0x80000 \u003e 0)
      result = result * 0x10000000000058B90BFBE8E8B2D3D4EDE \u003e\u003e 128;
    if (x \u0026 0x40000 \u003e 0)
      result = result * 0x1000000000002C5C85FDF4741BEA6E77E \u003e\u003e 128;
    if (x \u0026 0x20000 \u003e 0)
      result = result * 0x100000000000162E42FEFA39FE95583C2 \u003e\u003e 128;
    if (x \u0026 0x10000 \u003e 0)
      result = result * 0x1000000000000B17217F7D1CFB72B45E1 \u003e\u003e 128;
    if (x \u0026 0x8000 \u003e 0)
      result = result * 0x100000000000058B90BFBE8E7CC35C3F0 \u003e\u003e 128;
    if (x \u0026 0x4000 \u003e 0)
      result = result * 0x10000000000002C5C85FDF473E242EA38 \u003e\u003e 128;
    if (x \u0026 0x2000 \u003e 0)
      result = result * 0x1000000000000162E42FEFA39F02B772C \u003e\u003e 128;
    if (x \u0026 0x1000 \u003e 0)
      result = result * 0x10000000000000B17217F7D1CF7D83C1A \u003e\u003e 128;
    if (x \u0026 0x800 \u003e 0)
      result = result * 0x1000000000000058B90BFBE8E7BDCBE2E \u003e\u003e 128;
    if (x \u0026 0x400 \u003e 0)
      result = result * 0x100000000000002C5C85FDF473DEA871F \u003e\u003e 128;
    if (x \u0026 0x200 \u003e 0)
      result = result * 0x10000000000000162E42FEFA39EF44D91 \u003e\u003e 128;
    if (x \u0026 0x100 \u003e 0)
      result = result * 0x100000000000000B17217F7D1CF79E949 \u003e\u003e 128;
    if (x \u0026 0x80 \u003e 0)
      result = result * 0x10000000000000058B90BFBE8E7BCE544 \u003e\u003e 128;
    if (x \u0026 0x40 \u003e 0)
      result = result * 0x1000000000000002C5C85FDF473DE6ECA \u003e\u003e 128;
    if (x \u0026 0x20 \u003e 0)
      result = result * 0x100000000000000162E42FEFA39EF366F \u003e\u003e 128;
    if (x \u0026 0x10 \u003e 0)
      result = result * 0x1000000000000000B17217F7D1CF79AFA \u003e\u003e 128;
    if (x \u0026 0x8 \u003e 0)
      result = result * 0x100000000000000058B90BFBE8E7BCD6D \u003e\u003e 128;
    if (x \u0026 0x4 \u003e 0)
      result = result * 0x10000000000000002C5C85FDF473DE6B2 \u003e\u003e 128;
    if (x \u0026 0x2 \u003e 0)
      result = result * 0x1000000000000000162E42FEFA39EF358 \u003e\u003e 128;
    if (x \u0026 0x1 \u003e 0)
      result = result * 0x10000000000000000B17217F7D1CF79AB \u003e\u003e 128;

    result \u003e\u003e= uint256 (63 - (x \u003e\u003e 64));
    require (result \u003c= uint256 (MAX_64x64));

    return int128 (result);
  }

  /**
   * Calculate natural exponent of x.  Revert on overflow.
   *
   * @param x signed 64.64-bit fixed point number
   * @return signed 64.64-bit fixed point number
   */
  function exp (int128 x) internal pure returns (int128) {
    require (x \u003c 0x400000000000000000); // Overflow

    if (x \u003c -0x400000000000000000) return 0; // Underflow

    return exp_2 (
        int128 (int256 (x) * 0x171547652B82FE1777D0FFDA0D23A7D12 \u003e\u003e 128));
  }

  /**
   * Calculate x / y rounding towards zero, where x and y are unsigned 256-bit
   * integer numbers.  Revert on overflow or when y is zero.
   *
   * @param x unsigned 256-bit integer number
   * @param y unsigned 256-bit integer number
   * @return unsigned 64.64-bit fixed point number
   */
  function divuu (uint256 x, uint256 y) private pure returns (uint128) {
    require (y != 0);

    uint256 result;

    if (x \u003c= 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)
      result = (x \u003c\u003c 64) / y;
    else {
      uint256 msb = 192;
      uint256 xc = x \u003e\u003e 192;
      if (xc \u003e= 0x100000000) { xc \u003e\u003e= 32; msb += 32; }
      if (xc \u003e= 0x10000) { xc \u003e\u003e= 16; msb += 16; }
      if (xc \u003e= 0x100) { xc \u003e\u003e= 8; msb += 8; }
      if (xc \u003e= 0x10) { xc \u003e\u003e= 4; msb += 4; }
      if (xc \u003e= 0x4) { xc \u003e\u003e= 2; msb += 2; }
      if (xc \u003e= 0x2) msb += 1;  // No need to shift xc anymore

      result = (x \u003c\u003c 255 - msb) / ((y - 1 \u003e\u003e msb - 191) + 1);
      require (result \u003c= 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF);

      uint256 hi = result * (y \u003e\u003e 128);
      uint256 lo = result * (y \u0026 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF);

      uint256 xh = x \u003e\u003e 192;
      uint256 xl = x \u003c\u003c 64;

      if (xl \u003c lo) xh -= 1;
      xl -= lo; // We rely on overflow behavior here
      lo = hi \u003c\u003c 128;
      if (xl \u003c lo) xh -= 1;
      xl -= lo; // We rely on overflow behavior here

      assert (xh == hi \u003e\u003e 128);

      result += xl / y;
    }

    require (result \u003c= 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF);
    return uint128 (result);
  }

  /**
   * Calculate x^y assuming 0^0 is 1, where x is unsigned 129.127 fixed point
   * number and y is unsigned 256-bit integer number.  Revert on overflow.
   *
   * @param x unsigned 129.127-bit fixed point number
   * @param y uint256 value
   * @return unsigned 129.127-bit fixed point number
   */
  function powu (uint256 x, uint256 y) private pure returns (uint256) {
    if (y == 0) return 0x80000000000000000000000000000000;
    else if (x == 0) return 0;
    else {
      int256 msb = 0;
      uint256 xc = x;
      if (xc \u003e= 0x100000000000000000000000000000000) { xc \u003e\u003e= 128; msb += 128; }
      if (xc \u003e= 0x10000000000000000) { xc \u003e\u003e= 64; msb += 64; }
      if (xc \u003e= 0x100000000) { xc \u003e\u003e= 32; msb += 32; }
      if (xc \u003e= 0x10000) { xc \u003e\u003e= 16; msb += 16; }
      if (xc \u003e= 0x100) { xc \u003e\u003e= 8; msb += 8; }
      if (xc \u003e= 0x10) { xc \u003e\u003e= 4; msb += 4; }
      if (xc \u003e= 0x4) { xc \u003e\u003e= 2; msb += 2; }
      if (xc \u003e= 0x2) msb += 1;  // No need to shift xc anymore

      int256 xe = msb - 127;
      if (xe \u003e 0) x \u003e\u003e= uint256 (xe);
      else x \u003c\u003c= uint256 (-xe);

      uint256 result = 0x80000000000000000000000000000000;
      int256 re = 0;

      while (y \u003e 0) {
        if (y \u0026 1 \u003e 0) {
          result = result * x;
          y -= 1;
          re += xe;
          if (result \u003e=
            0x8000000000000000000000000000000000000000000000000000000000000000) {
            result \u003e\u003e= 128;
            re += 1;
          } else result \u003e\u003e= 127;
          if (re \u003c -127) return 0; // Underflow
          require (re \u003c 128); // Overflow
        } else {
          x = x * x;
          y \u003e\u003e= 1;
          xe \u003c\u003c= 1;
          if (x \u003e=
            0x8000000000000000000000000000000000000000000000000000000000000000) {
            x \u003e\u003e= 128;
            xe += 1;
          } else x \u003e\u003e= 127;
          if (xe \u003c -127) return 0; // Underflow
          require (xe \u003c 128); // Overflow
        }
      }

      if (re \u003e 0) result \u003c\u003c= uint256 (re);
      else if (re \u003c 0) result \u003e\u003e= uint256 (-re);

      return result;
    }
  }

  /**
   * Calculate sqrt (x) rounding down, where x is unsigned 256-bit integer
   * number.
   *
   * @param x unsigned 256-bit integer number
   * @return unsigned 128-bit integer number
   */
  function sqrtu (uint256 x) private pure returns (uint128) {
    if (x == 0) return 0;
    else {
      uint256 xx = x;
      uint256 r = 1;
      if (xx \u003e= 0x100000000000000000000000000000000) { xx \u003e\u003e= 128; r \u003c\u003c= 64; }
      if (xx \u003e= 0x10000000000000000) { xx \u003e\u003e= 64; r \u003c\u003c= 32; }
      if (xx \u003e= 0x100000000) { xx \u003e\u003e= 32; r \u003c\u003c= 16; }
      if (xx \u003e= 0x10000) { xx \u003e\u003e= 16; r \u003c\u003c= 8; }
      if (xx \u003e= 0x100) { xx \u003e\u003e= 8; r \u003c\u003c= 4; }
      if (xx \u003e= 0x10) { xx \u003e\u003e= 4; r \u003c\u003c= 2; }
      if (xx \u003e= 0x8) { r \u003c\u003c= 1; }
      r = (r + x / r) \u003e\u003e 1;
      r = (r + x / r) \u003e\u003e 1;
      r = (r + x / r) \u003e\u003e 1;
      r = (r + x / r) \u003e\u003e 1;
      r = (r + x / r) \u003e\u003e 1;
      r = (r + x / r) \u003e\u003e 1;
      r = (r + x / r) \u003e\u003e 1; // Seven iterations should be enough
      uint256 r1 = x / r;
      return uint128 (r \u003c r1 ? r : r1);
    }
  }
}"},"Address.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity \u003e=0.6.2 \u003c0.8.0;

/**
 * @dev Collection of functions related to the address type
 */
library Address {
    /**
     * @dev Returns true if `account` is a contract.
     *
     * [IMPORTANT]
     * ====
     * It is unsafe to assume that an address for which this function returns
     * false is an externally-owned account (EOA) and not a contract.
     *
     * Among others, `isContract` will return false for the following
     * types of addresses:
     *
     *  - an externally-owned account
     *  - a contract in construction
     *  - an address where a contract will be created
     *  - an address where a contract lived, but was destroyed
     * ====
     */
    function isContract(address account) internal view returns (bool) {
        // This method relies on extcodesize, which returns 0 for contracts in
        // construction, since the code is only stored at the end of the
        // constructor execution.

        uint256 size;
        // solhint-disable-next-line no-inline-assembly
        assembly { size := extcodesize(account) }
        return size \u003e 0;
    }

    /**
     * @dev Replacement for Solidity\u0027s `transfer`: sends `amount` wei to
     * `recipient`, forwarding all available gas and reverting on errors.
     *
     * https://eips.ethereum.org/EIPS/eip-1884[EIP1884] increases the gas cost
     * of certain opcodes, possibly making contracts go over the 2300 gas limit
     * imposed by `transfer`, making them unable to receive funds via
     * `transfer`. {sendValue} removes this limitation.
     *
     * https://diligence.consensys.net/posts/2019/09/stop-using-soliditys-transfer-now/[Learn more].
     *
     * IMPORTANT: because control is transferred to `recipient`, care must be
     * taken to not create reentrancy vulnerabilities. Consider using
     * {ReentrancyGuard} or the
     * https://solidity.readthedocs.io/en/v0.5.11/security-considerations.html#use-the-checks-effects-interactions-pattern[checks-effects-interactions pattern].
     */
    function sendValue(address payable recipient, uint256 amount) internal {
        require(address(this).balance \u003e= amount, \"Address: insufficient balance\");

        // solhint-disable-next-line avoid-low-level-calls, avoid-call-value
        (bool success, ) = recipient.call{ value: amount }(\"\");
        require(success, \"Address: unable to send value, recipient may have reverted\");
    }

    /**
     * @dev Performs a Solidity function call using a low level `call`. A
     * plain`call` is an unsafe replacement for a function call: use this
     * function instead.
     *
     * If `target` reverts with a revert reason, it is bubbled up by this
     * function (like regular Solidity function calls).
     *
     * Returns the raw returned data. To convert to the expected return value,
     * use https://solidity.readthedocs.io/en/latest/units-and-global-variables.html?highlight=abi.decode#abi-encoding-and-decoding-functions[`abi.decode`].
     *
     * Requirements:
     *
     * - `target` must be a contract.
     * - calling `target` with `data` must not revert.
     *
     * _Available since v3.1._
     */
    function functionCall(address target, bytes memory data) internal returns (bytes memory) {
      return functionCall(target, data, \"Address: low-level call failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`], but with
     * `errorMessage` as a fallback revert reason when `target` reverts.
     *
     * _Available since v3.1._
     */
    function functionCall(address target, bytes memory data, string memory errorMessage) internal returns (bytes memory) {
        return functionCallWithValue(target, data, 0, errorMessage);
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`],
     * but also transferring `value` wei to `target`.
     *
     * Requirements:
     *
     * - the calling contract must have an ETH balance of at least `value`.
     * - the called Solidity function must be `payable`.
     *
     * _Available since v3.1._
     */
    function functionCallWithValue(address target, bytes memory data, uint256 value) internal returns (bytes memory) {
        return functionCallWithValue(target, data, value, \"Address: low-level call with value failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCallWithValue-address-bytes-uint256-}[`functionCallWithValue`], but
     * with `errorMessage` as a fallback revert reason when `target` reverts.
     *
     * _Available since v3.1._
     */
    function functionCallWithValue(address target, bytes memory data, uint256 value, string memory errorMessage) internal returns (bytes memory) {
        require(address(this).balance \u003e= value, \"Address: insufficient balance for call\");
        require(isContract(target), \"Address: call to non-contract\");

        // solhint-disable-next-line avoid-low-level-calls
        (bool success, bytes memory returndata) = target.call{ value: value }(data);
        return _verifyCallResult(success, returndata, errorMessage);
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`],
     * but performing a static call.
     *
     * _Available since v3.3._
     */
    function functionStaticCall(address target, bytes memory data) internal view returns (bytes memory) {
        return functionStaticCall(target, data, \"Address: low-level static call failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-string-}[`functionCall`],
     * but performing a static call.
     *
     * _Available since v3.3._
     */
    function functionStaticCall(address target, bytes memory data, string memory errorMessage) internal view returns (bytes memory) {
        require(isContract(target), \"Address: static call to non-contract\");

        // solhint-disable-next-line avoid-low-level-calls
        (bool success, bytes memory returndata) = target.staticcall(data);
        return _verifyCallResult(success, returndata, errorMessage);
    }

    function _verifyCallResult(bool success, bytes memory returndata, string memory errorMessage) private pure returns(bytes memory) {
        if (success) {
            return returndata;
        } else {
            // Look for revert reason and bubble it up if present
            if (returndata.length \u003e 0) {
                // The easiest way to bubble the revert reason is using memory via assembly

                // solhint-disable-next-line no-inline-assembly
                assembly {
                    let returndata_size := mload(returndata)
                    revert(add(32, returndata), returndata_size)
                }
            } else {
                revert(errorMessage);
            }
        }
    }
}
"},"Assimilators.sol":{"content":"// SPDX-License-Identifier: MIT

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see \u003chttp://www.gnu.org/licenses/\u003e.

pragma solidity ^0.7.3;

import \"./Address.sol\";
import \"./IAssimilator.sol\";
import \"./ABDKMath64x64.sol\";

library Assimilators {
    using ABDKMath64x64 for int128;
    using Address for address;

    IAssimilator public constant iAsmltr = IAssimilator(address(0));

    function delegate(address _callee, bytes memory _data) internal returns (bytes memory) {
        require(_callee.isContract(), \"Assimilators/callee-is-not-a-contract\");

        // solhint-disable-next-line
        (bool _success, bytes memory returnData_) = _callee.delegatecall(_data);

        // solhint-disable-next-line
        assembly {
            if eq(_success, 0) {
                revert(add(returnData_, 0x20), returndatasize())
            }
        }

        return returnData_;
    }

    function getRate(address _assim) internal view returns (uint256 amount_) {
        amount_ = IAssimilator(_assim).getRate();
    }

    function viewRawAmount(address _assim, int128 _amt) internal view returns (uint256 amount_) {
        amount_ = IAssimilator(_assim).viewRawAmount(_amt);
    }

    function viewRawAmountLPRatio(
        address _assim,
        uint256 _baseWeight,
        uint256 _quoteWeight,
        int128 _amount
    ) internal view returns (uint256 amount_) {
        amount_ = IAssimilator(_assim).viewRawAmountLPRatio(_baseWeight, _quoteWeight, address(this), _amount);
    }

    function viewNumeraireAmount(address _assim, uint256 _amt) internal view returns (int128 amt_) {
        amt_ = IAssimilator(_assim).viewNumeraireAmount(_amt);
    }

    function viewNumeraireAmountAndBalance(address _assim, uint256 _amt)
        internal
        view
        returns (int128 amt_, int128 bal_)
    {
        (amt_, bal_) = IAssimilator(_assim).viewNumeraireAmountAndBalance(address(this), _amt);
    }

    function viewNumeraireBalance(address _assim) internal view returns (int128 bal_) {
        bal_ = IAssimilator(_assim).viewNumeraireBalance(address(this));
    }

    function viewNumeraireBalanceLPRatio(
        uint256 _baseWeight,
        uint256 _quoteWeight,
        address _assim
    ) internal view returns (int128 bal_) {
        bal_ = IAssimilator(_assim).viewNumeraireBalanceLPRatio(_baseWeight, _quoteWeight, address(this));
    }

    function intakeRaw(address _assim, uint256 _amt) internal returns (int128 amt_) {
        bytes memory data = abi.encodeWithSelector(iAsmltr.intakeRaw.selector, _amt);

        amt_ = abi.decode(delegate(_assim, data), (int128));
    }

    function intakeRawAndGetBalance(address _assim, uint256 _amt) internal returns (int128 amt_, int128 bal_) {
        bytes memory data = abi.encodeWithSelector(iAsmltr.intakeRawAndGetBalance.selector, _amt);

        (amt_, bal_) = abi.decode(delegate(_assim, data), (int128, int128));
    }

    function intakeNumeraire(address _assim, int128 _amt) internal returns (uint256 amt_) {
        bytes memory data = abi.encodeWithSelector(iAsmltr.intakeNumeraire.selector, _amt);

        amt_ = abi.decode(delegate(_assim, data), (uint256));
    }

    function intakeNumeraireLPRatio(
        address _assim,
        uint256 _baseWeight,
        uint256 _quoteWeight,
        int128 _amount
    ) internal returns (uint256 amt_) {
        bytes memory data =
            abi.encodeWithSelector(
                iAsmltr.intakeNumeraireLPRatio.selector,
                _baseWeight,
                _quoteWeight,
                address(this),
                _amount
            );

        amt_ = abi.decode(delegate(_assim, data), (uint256));
    }

    function outputRaw(
        address _assim,
        address _dst,
        uint256 _amt
    ) internal returns (int128 amt_) {
        bytes memory data = abi.encodeWithSelector(iAsmltr.outputRaw.selector, _dst, _amt);

        amt_ = abi.decode(delegate(_assim, data), (int128));

        amt_ = amt_.neg();
    }

    function outputRawAndGetBalance(
        address _assim,
        address _dst,
        uint256 _amt
    ) internal returns (int128 amt_, int128 bal_) {
        bytes memory data = abi.encodeWithSelector(iAsmltr.outputRawAndGetBalance.selector, _dst, _amt);

        (amt_, bal_) = abi.decode(delegate(_assim, data), (int128, int128));

        amt_ = amt_.neg();
    }

    function outputNumeraire(
        address _assim,
        address _dst,
        int128 _amt
    ) internal returns (uint256 amt_) {
        bytes memory data = abi.encodeWithSelector(iAsmltr.outputNumeraire.selector, _dst, _amt.abs());

        amt_ = abi.decode(delegate(_assim, data), (uint256));
    }
}
"},"CurveMath.sol":{"content":"// SPDX-License-Identifier: MIT

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see \u003chttp://www.gnu.org/licenses/\u003e.

pragma solidity ^0.7.3;

import \"./Storage.sol\";

import \"./UnsafeMath64x64.sol\";
import \"./ABDKMath64x64.sol\";

library CurveMath {
    int128 private constant ONE = 0x10000000000000000;
    int128 private constant MAX = 0x4000000000000000; // .25 in layman\u0027s terms
    int128 private constant MAX_DIFF = -0x10C6F7A0B5EE;
    int128 private constant ONE_WEI = 0x12;

    using ABDKMath64x64 for int128;
    using UnsafeMath64x64 for int128;
    using ABDKMath64x64 for uint256;

    // This is used to prevent stack too deep errors
    function calculateFee(
        int128 _gLiq,
        int128[] memory _bals,
        Storage.Curve storage curve,
        int128[] memory _weights
    ) internal view returns (int128 psi_) {
        int128 _beta = curve.beta;
        int128 _delta = curve.delta;

        psi_ = calculateFee(_gLiq, _bals, _beta, _delta, _weights);
    }

    function calculateFee(
        int128 _gLiq,
        int128[] memory _bals,
        int128 _beta,
        int128 _delta,
        int128[] memory _weights
    ) internal pure returns (int128 psi_) {
        uint256 _length = _bals.length;

        for (uint256 i = 0; i \u003c _length; i++) {
            int128 _ideal = _gLiq.mul(_weights[i]);
            psi_ += calculateMicroFee(_bals[i], _ideal, _beta, _delta);
        }
    }

    function calculateMicroFee(
        int128 _bal,
        int128 _ideal,
        int128 _beta,
        int128 _delta
    ) private pure returns (int128 fee_) {
        if (_bal \u003c _ideal) {
            int128 _threshold = _ideal.mul(ONE - _beta);

            if (_bal \u003c _threshold) {
                int128 _feeMargin = _threshold - _bal;

                fee_ = _feeMargin.div(_ideal);
                fee_ = fee_.mul(_delta);

                if (fee_ \u003e MAX) fee_ = MAX;

                fee_ = fee_.mul(_feeMargin);
            } else fee_ = 0;
        } else {
            int128 _threshold = _ideal.mul(ONE + _beta);

            if (_bal \u003e _threshold) {
                int128 _feeMargin = _bal - _threshold;

                fee_ = _feeMargin.div(_ideal);
                fee_ = fee_.mul(_delta);

                if (fee_ \u003e MAX) fee_ = MAX;

                fee_ = fee_.mul(_feeMargin);
            } else fee_ = 0;
        }
    }

    function calculateTrade(
        Storage.Curve storage curve,
        int128 _oGLiq,
        int128 _nGLiq,
        int128[] memory _oBals,
        int128[] memory _nBals,
        int128 _inputAmt,
        uint256 _outputIndex
    ) internal view returns (int128 outputAmt_) {
        outputAmt_ = -_inputAmt;

        int128 _lambda = curve.lambda;
        int128[] memory _weights = curve.weights;

        int128 _omega = calculateFee(_oGLiq, _oBals, curve, _weights);
        int128 _psi;

        for (uint256 i = 0; i \u003c 32; i++) {
            _psi = calculateFee(_nGLiq, _nBals, curve, _weights);

            int128 prevAmount;
            {
                prevAmount = outputAmt_;
                outputAmt_ = _omega \u003c _psi ? -(_inputAmt + _omega - _psi) : -(_inputAmt + _lambda.mul(_omega - _psi));
            }

            if (outputAmt_ / 1e13 == prevAmount / 1e13) {
                _nGLiq = _oGLiq + _inputAmt + outputAmt_;

                _nBals[_outputIndex] = _oBals[_outputIndex] + outputAmt_;

                enforceHalts(curve, _oGLiq, _nGLiq, _oBals, _nBals, _weights);

                enforceSwapInvariant(_oGLiq, _omega, _nGLiq, _psi);

                return outputAmt_;
            } else {
                _nGLiq = _oGLiq + _inputAmt + outputAmt_;

                _nBals[_outputIndex] = _oBals[_outputIndex].add(outputAmt_);
            }
        }

        revert(\"Curve/swap-convergence-failed\");
    }

    function calculateLiquidityMembrane(
        Storage.Curve storage curve,
        int128 _oGLiq,
        int128 _nGLiq,
        int128[] memory _oBals,
        int128[] memory _nBals
    ) internal view returns (int128 curves_) {
        enforceHalts(curve, _oGLiq, _nGLiq, _oBals, _nBals, curve.weights);

        int128 _omega;
        int128 _psi;

        {
            int128 _beta = curve.beta;
            int128 _delta = curve.delta;
            int128[] memory _weights = curve.weights;

            _omega = calculateFee(_oGLiq, _oBals, _beta, _delta, _weights);
            _psi = calculateFee(_nGLiq, _nBals, _beta, _delta, _weights);
        }

        int128 _feeDiff = _psi.sub(_omega);
        int128 _liqDiff = _nGLiq.sub(_oGLiq);
        int128 _oUtil = _oGLiq.sub(_omega);
        int128 _totalShells = curve.totalSupply.divu(1e18);
        int128 _curveMultiplier;

        if (_totalShells == 0) {
            curves_ = _nGLiq.sub(_psi);
        } else if (_feeDiff \u003e= 0) {
            _curveMultiplier = _liqDiff.sub(_feeDiff).div(_oUtil);
        } else {
            _curveMultiplier = _liqDiff.sub(curve.lambda.mul(_feeDiff));

            _curveMultiplier = _curveMultiplier.div(_oUtil);
        }

        if (_totalShells != 0) {
            curves_ = _totalShells.mul(_curveMultiplier);

            enforceLiquidityInvariant(_totalShells, curves_, _oGLiq, _nGLiq, _omega, _psi);
        }
    }

    function enforceSwapInvariant(
        int128 _oGLiq,
        int128 _omega,
        int128 _nGLiq,
        int128 _psi
    ) private pure {
        int128 _nextUtil = _nGLiq - _psi;

        int128 _prevUtil = _oGLiq - _omega;

        int128 _diff = _nextUtil - _prevUtil;

        require(0 \u003c _diff || _diff \u003e= MAX_DIFF, \"Curve/swap-invariant-violation\");
    }

    function enforceLiquidityInvariant(
        int128 _totalShells,
        int128 _newShells,
        int128 _oGLiq,
        int128 _nGLiq,
        int128 _omega,
        int128 _psi
    ) internal pure {
        if (_totalShells == 0 || 0 == _totalShells + _newShells) return;

        int128 _prevUtilPerShell = _oGLiq.sub(_omega).div(_totalShells);

        int128 _nextUtilPerShell = _nGLiq.sub(_psi).div(_totalShells.add(_newShells));

        int128 _diff = _nextUtilPerShell - _prevUtilPerShell;

        require(0 \u003c _diff || _diff \u003e= MAX_DIFF, \"Curve/liquidity-invariant-violation\");
    }

    function enforceHalts(
        Storage.Curve storage curve,
        int128 _oGLiq,
        int128 _nGLiq,
        int128[] memory _oBals,
        int128[] memory _nBals,
        int128[] memory _weights
    ) private view {
        uint256 _length = _nBals.length;
        int128 _alpha = curve.alpha;

        for (uint256 i = 0; i \u003c _length; i++) {
            int128 _nIdeal = _nGLiq.mul(_weights[i]);

            if (_nBals[i] \u003e _nIdeal) {
                int128 _upperAlpha = ONE + _alpha;

                int128 _nHalt = _nIdeal.mul(_upperAlpha);

                if (_nBals[i] \u003e _nHalt) {
                    int128 _oHalt = _oGLiq.mul(_weights[i]).mul(_upperAlpha);

                    if (_oBals[i] \u003c _oHalt) revert(\"Curve/upper-halt\");
                    if (_nBals[i] - _nHalt \u003e _oBals[i] - _oHalt) revert(\"Curve/upper-halt\");
                }
            } else {
                int128 _lowerAlpha = ONE - _alpha;

                int128 _nHalt = _nIdeal.mul(_lowerAlpha);

                if (_nBals[i] \u003c _nHalt) {
                    int128 _oHalt = _oGLiq.mul(_weights[i]);
                    _oHalt = _oHalt.mul(_lowerAlpha);

                    if (_oBals[i] \u003e _oHalt) revert(\"Curve/lower-halt\");
                    if (_nHalt - _nBals[i] \u003e _oHalt - _oBals[i]) revert(\"Curve/lower-halt\");
                }
            }
        }
    }
}
"},"IAssimilator.sol":{"content":"// SPDX-License-Identifier: MIT

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see \u003chttp://www.gnu.org/licenses/\u003e.

pragma solidity ^0.7.3;

interface IAssimilator {
    function getRate() external view returns (uint256);

    function intakeRaw(uint256 amount) external returns (int128);

    function intakeRawAndGetBalance(uint256 amount) external returns (int128, int128);

    function intakeNumeraire(int128 amount) external returns (uint256);

    function intakeNumeraireLPRatio(
        uint256,
        uint256,
        address,
        int128
    ) external returns (uint256);

    function outputRaw(address dst, uint256 amount) external returns (int128);

    function outputRawAndGetBalance(address dst, uint256 amount) external returns (int128, int128);

    function outputNumeraire(address dst, int128 amount) external returns (uint256);

    function viewRawAmount(int128) external view returns (uint256);

    function viewRawAmountLPRatio(
        uint256,
        uint256,
        address,
        int128
    ) external view returns (uint256);

    function viewNumeraireAmount(uint256) external view returns (int128);

    function viewNumeraireBalanceLPRatio(
        uint256,
        uint256,
        address
    ) external view returns (int128);

    function viewNumeraireBalance(address) external view returns (int128);

    function viewNumeraireAmountAndBalance(address, uint256) external view returns (int128, int128);
}
"},"IOracle.sol":{"content":"// SPDX-License-Identifier: MIT

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see \u003chttp://www.gnu.org/licenses/\u003e.

pragma solidity ^0.7.3;

interface IOracle {
    function acceptOwnership() external;

    function accessController() external view returns (address);

    function aggregator() external view returns (address);

    function confirmAggregator(address _aggregator) external;

    function decimals() external view returns (uint8);

    function description() external view returns (string memory);

    function getAnswer(uint256 _roundId) external view returns (int256);

    function getRoundData(uint80 _roundId)
        external
        view
        returns (
            uint80 roundId,
            int256 answer,
            uint256 startedAt,
            uint256 updatedAt,
            uint80 answeredInRound
        );

    function getTimestamp(uint256 _roundId) external view returns (uint256);

    function latestAnswer() external view returns (int256);

    function latestRound() external view returns (uint256);

    function latestRoundData()
        external
        view
        returns (
            uint80 roundId,
            int256 answer,
            uint256 startedAt,
            uint256 updatedAt,
            uint80 answeredInRound
        );

    function latestTimestamp() external view returns (uint256);

    function owner() external view returns (address);

    function phaseAggregators(uint16) external view returns (address);

    function phaseId() external view returns (uint16);

    function proposeAggregator(address _aggregator) external;

    function proposedAggregator() external view returns (address);

    function proposedGetRoundData(uint80 _roundId)
        external
        view
        returns (
            uint80 roundId,
            int256 answer,
            uint256 startedAt,
            uint256 updatedAt,
            uint80 answeredInRound
        );

    function proposedLatestRoundData()
        external
        view
        returns (
            uint80 roundId,
            int256 answer,
            uint256 startedAt,
            uint256 updatedAt,
            uint80 answeredInRound
        );

    function setController(address _accessController) external;

    function transferOwnership(address _to) external;

    function version() external view returns (uint256);
}
"},"ProportionalLiquidity.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.7.3;

import \"./Assimilators.sol\";

import \"./Storage.sol\";

import \"./UnsafeMath64x64.sol\";
import \"./ABDKMath64x64.sol\";

import \"./CurveMath.sol\";

library ProportionalLiquidity {
    using ABDKMath64x64 for uint256;
    using ABDKMath64x64 for int128;
    using UnsafeMath64x64 for int128;

    event Transfer(address indexed from, address indexed to, uint256 value);

    int128 public constant ONE = 0x10000000000000000;
    int128 public constant ONE_WEI = 0x12;

    function proportionalDeposit(Storage.Curve storage curve, uint256 _deposit)
        external
        returns (uint256 curves_, uint256[] memory)
    {
        int128 __deposit = _deposit.divu(1e18);

        uint256 _length = curve.assets.length;

        uint256[] memory deposits_ = new uint256[](_length);

        (int128 _oGLiq, int128[] memory _oBals) = getGrossLiquidityAndBalancesForDeposit(curve);

        // Needed to calculate liquidity invariant
        (int128 _oGLiqProp, int128[] memory _oBalsProp) = getGrossLiquidityAndBalances(curve);

        // No liquidity, oracle sets the ratio
        if (_oGLiq == 0) {
            for (uint256 i = 0; i \u003c _length; i++) {
                // Variable here to avoid stack-too-deep errors
                int128 _d = __deposit.mul(curve.weights[i]);
                deposits_[i] = Assimilators.intakeNumeraire(curve.assets[i].addr, _d.add(ONE_WEI));
            }
        } else {
            // We already have an existing pool ratio
            // which must be respected
            int128 _multiplier = __deposit.div(_oGLiq);

            uint256 _baseWeight = curve.weights[0].mulu(1e18);
            uint256 _quoteWeight = curve.weights[1].mulu(1e18);

            for (uint256 i = 0; i \u003c _length; i++) {
                deposits_[i] = Assimilators.intakeNumeraireLPRatio(
                    curve.assets[i].addr,
                    _baseWeight,
                    _quoteWeight,
                    _oBals[i].mul(_multiplier).add(ONE_WEI)
                );
            }
        }

        int128 _totalShells = curve.totalSupply.divu(1e18);

        int128 _newShells = __deposit;

        if (_totalShells \u003e 0) {
            _newShells = __deposit.div(_oGLiq);
            _newShells = _newShells.mul(_totalShells);
        }

        requireLiquidityInvariant(curve, _totalShells, _newShells, _oGLiqProp, _oBalsProp);

        mint(curve, msg.sender, curves_ = _newShells.mulu(1e18));

        return (curves_, deposits_);
    }

    function viewProportionalDeposit(Storage.Curve storage curve, uint256 _deposit)
        external
        view
        returns (uint256 curves_, uint256[] memory)
    {
        int128 __deposit = _deposit.divu(1e18);

        uint256 _length = curve.assets.length;

        (int128 _oGLiq, int128[] memory _oBals) = getGrossLiquidityAndBalancesForDeposit(curve);

        uint256[] memory deposits_ = new uint256[](_length);

        // No liquidity
        if (_oGLiq == 0) {
            for (uint256 i = 0; i \u003c _length; i++) {
                deposits_[i] = Assimilators.viewRawAmount(
                    curve.assets[i].addr,
                    __deposit.mul(curve.weights[i]).add(ONE_WEI)
                );
            }
        } else {
            // We already have an existing pool ratio
            // this must be respected
            int128 _multiplier = __deposit.div(_oGLiq);

            uint256 _baseWeight = curve.weights[0].mulu(1e18);
            uint256 _quoteWeight = curve.weights[1].mulu(1e18);

            // Deposits into the pool is determined by existing LP ratio
            for (uint256 i = 0; i \u003c _length; i++) {
                deposits_[i] = Assimilators.viewRawAmountLPRatio(
                    curve.assets[i].addr,
                    _baseWeight,
                    _quoteWeight,
                    _oBals[i].mul(_multiplier).add(ONE_WEI)
                );
            }
        }

        int128 _totalShells = curve.totalSupply.divu(1e18);

        int128 _newShells = __deposit;

        if (_totalShells \u003e 0) {
            _newShells = __deposit.div(_oGLiq);
            _newShells = _newShells.mul(_totalShells);
        }

        curves_ = _newShells.mulu(1e18);

        return (curves_, deposits_);
    }

    function emergencyProportionalWithdraw(Storage.Curve storage curve, uint256 _withdrawal)
        external
        returns (uint256[] memory)
    {
        uint256 _length = curve.assets.length;

        (, int128[] memory _oBals) = getGrossLiquidityAndBalances(curve);

        uint256[] memory withdrawals_ = new uint256[](_length);

        int128 _totalShells = curve.totalSupply.divu(1e18);
        int128 __withdrawal = _withdrawal.divu(1e18);

        int128 _multiplier = __withdrawal.div(_totalShells);

        for (uint256 i = 0; i \u003c _length; i++) {
            withdrawals_[i] = Assimilators.outputNumeraire(
                curve.assets[i].addr,
                msg.sender,
                _oBals[i].mul(_multiplier)
            );
        }

        burn(curve, msg.sender, _withdrawal);

        return withdrawals_;
    }

    function proportionalWithdraw(Storage.Curve storage curve, uint256 _withdrawal)
        external
        returns (uint256[] memory)
    {
        uint256 _length = curve.assets.length;

        (int128 _oGLiq, int128[] memory _oBals) = getGrossLiquidityAndBalances(curve);

        uint256[] memory withdrawals_ = new uint256[](_length);

        int128 _totalShells = curve.totalSupply.divu(1e18);
        int128 __withdrawal = _withdrawal.divu(1e18);

        int128 _multiplier = __withdrawal.div(_totalShells);

        for (uint256 i = 0; i \u003c _length; i++) {
            withdrawals_[i] = Assimilators.outputNumeraire(
                curve.assets[i].addr,
                msg.sender,
                _oBals[i].mul(_multiplier)
            );
        }

        requireLiquidityInvariant(curve, _totalShells, __withdrawal.neg(), _oGLiq, _oBals);

        burn(curve, msg.sender, _withdrawal);

        return withdrawals_;
    }

    function viewProportionalWithdraw(Storage.Curve storage curve, uint256 _withdrawal)
        external
        view
        returns (uint256[] memory)
    {
        uint256 _length = curve.assets.length;

        (, int128[] memory _oBals) = getGrossLiquidityAndBalances(curve);

        uint256[] memory withdrawals_ = new uint256[](_length);

        int128 _multiplier = _withdrawal.divu(1e18).div(curve.totalSupply.divu(1e18));

        for (uint256 i = 0; i \u003c _length; i++) {
            withdrawals_[i] = Assimilators.viewRawAmount(curve.assets[i].addr, _oBals[i].mul(_multiplier));
        }

        return withdrawals_;
    }

    function getGrossLiquidityAndBalancesForDeposit(Storage.Curve storage curve)
        internal
        view
        returns (int128 grossLiquidity_, int128[] memory)
    {
        uint256 _length = curve.assets.length;

        int128[] memory balances_ = new int128[](_length);
        uint256 _baseWeight = curve.weights[0].mulu(1e18);
        uint256 _quoteWeight = curve.weights[1].mulu(1e18);

        for (uint256 i = 0; i \u003c _length; i++) {
            int128 _bal = Assimilators.viewNumeraireBalanceLPRatio(_baseWeight, _quoteWeight, curve.assets[i].addr);

            balances_[i] = _bal;
            grossLiquidity_ += _bal;
        }

        return (grossLiquidity_, balances_);
    }

    function getGrossLiquidityAndBalances(Storage.Curve storage curve)
        internal
        view
        returns (int128 grossLiquidity_, int128[] memory)
    {
        uint256 _length = curve.assets.length;

        int128[] memory balances_ = new int128[](_length);

        for (uint256 i = 0; i \u003c _length; i++) {
            int128 _bal = Assimilators.viewNumeraireBalance(curve.assets[i].addr);

            balances_[i] = _bal;
            grossLiquidity_ += _bal;
        }

        return (grossLiquidity_, balances_);
    }

    function requireLiquidityInvariant(
        Storage.Curve storage curve,
        int128 _curves,
        int128 _newShells,
        int128 _oGLiq,
        int128[] memory _oBals
    ) private view {
        (int128 _nGLiq, int128[] memory _nBals) = getGrossLiquidityAndBalances(curve);

        int128 _beta = curve.beta;
        int128 _delta = curve.delta;
        int128[] memory _weights = curve.weights;

        int128 _omega = CurveMath.calculateFee(_oGLiq, _oBals, _beta, _delta, _weights);

        int128 _psi = CurveMath.calculateFee(_nGLiq, _nBals, _beta, _delta, _weights);

        CurveMath.enforceLiquidityInvariant(_curves, _newShells, _oGLiq, _nGLiq, _omega, _psi);
    }

    function burn(
        Storage.Curve storage curve,
        address account,
        uint256 amount
    ) private {
        curve.balances[account] = burnSub(curve.balances[account], amount);

        curve.totalSupply = burnSub(curve.totalSupply, amount);

        emit Transfer(msg.sender, address(0), amount);
    }

    function mint(
        Storage.Curve storage curve,
        address account,
        uint256 amount
    ) private {
        curve.totalSupply = mintAdd(curve.totalSupply, amount);

        curve.balances[account] = mintAdd(curve.balances[account], amount);

        emit Transfer(address(0), msg.sender, amount);
    }

    function mintAdd(uint256 x, uint256 y) private pure returns (uint256 z) {
        require((z = x + y) \u003e= x, \"Curve/mint-overflow\");
    }

    function burnSub(uint256 x, uint256 y) private pure returns (uint256 z) {
        require((z = x - y) \u003c= x, \"Curve/burn-underflow\");
    }
}
"},"Storage.sol":{"content":"// SPDX-License-Identifier: MIT

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see \u003chttp://www.gnu.org/licenses/\u003e.

pragma solidity ^0.7.3;

import \"./IOracle.sol\";
import \"./Assimilators.sol\";

contract Storage {
    struct Curve {
        // Curve parameters
        int128 alpha;
        int128 beta;
        int128 delta;
        int128 epsilon;
        int128 lambda;
        int128[] weights;
        // Assets and their assimilators
        Assimilator[] assets;
        mapping(address =\u003e Assimilator) assimilators;
        // Oracles to determine the price
        // Note that 0\u0027th index should always be USDC 1e18
        // Oracle\u0027s pricing should be denominated in Currency/USDC
        mapping(address =\u003e IOracle) oracles;
        // ERC20 Interface
        uint256 totalSupply;
        mapping(address =\u003e uint256) balances;
        mapping(address =\u003e mapping(address =\u003e uint256)) allowances;
    }

    struct Assimilator {
        address addr;
        uint8 ix;
    }

    // Curve parameters
    Curve public curve;

    // Ownable
    address public owner;

    string public name;
    string public symbol;
    uint8 public constant decimals = 18;

    address[] public derivatives;
    address[] public numeraires;
    address[] public reserves;

    // Curve operational state
    bool public frozen = false;
    bool public emergency = false;
    bool public whitelistingStage = true;
    bool internal notEntered = true;

    mapping(address =\u003e uint256) public whitelistedDeposited;
}
"},"UnsafeMath64x64.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.7.3;

library UnsafeMath64x64 {

  /**
   * Calculate x * y rounding down.
   *
   * @param x signed 64.64-bit fixed point number
   * @param y signed 64.64-bit fixed point number
   * @return signed 64.64-bit fixed point number
   */

  function us_mul (int128 x, int128 y) internal pure returns (int128) {
    int256 result = int256(x) * y \u003e\u003e 64;
    return int128 (result);
  }

  /**
   * Calculate x / y rounding towards zero.  Revert on overflow or when y is
   * zero.
   *
   * @param x signed 64.64-bit fixed point number
   * @param y signed 64.64-bit fixed point number
   * @return signed 64.64-bit fixed point number
   */

  function us_div (int128 x, int128 y) internal pure returns (int128) {
    int256 result = (int256 (x) \u003c\u003c 64) / y;
    return int128 (result);
  }

}
