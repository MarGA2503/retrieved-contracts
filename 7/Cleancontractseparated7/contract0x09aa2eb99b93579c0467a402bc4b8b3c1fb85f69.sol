// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import \"./Context.sol\";
import \"./Strings.sol\";
import \"./ERC165.sol\";

/**
 * @dev External interface of AccessControl declared to support ERC165 detection.
 */
interface IAccessControl {
    function hasRole(bytes32 role, address account) external view returns (bool);
    function getRoleAdmin(bytes32 role) external view returns (bytes32);
    function grantRole(bytes32 role, address account) external;
    function revokeRole(bytes32 role, address account) external;
    function renounceRole(bytes32 role, address account) external;
}

/**
 * @dev Contract module that allows children to implement role-based access
 * control mechanisms. This is a lightweight version that doesn\u0027t allow enumerating role
 * members except through off-chain means by accessing the contract event logs. Some
 * applications may benefit from on-chain enumerability, for those cases see
 * {AccessControlEnumerable}.
 *
 * Roles are referred to by their `bytes32` identifier. These should be exposed
 * in the external API and be unique. The best way to achieve this is by
 * using `public constant` hash digests:
 *
 * ```
 * bytes32 public constant MY_ROLE = keccak256(\"MY_ROLE\");
 * ```
 *
 * Roles can be used to represent a set of permissions. To restrict access to a
 * function call, use {hasRole}:
 *
 * ```
 * function foo() public {
 *     require(hasRole(MY_ROLE, msg.sender));
 *     ...
 * }
 * ```
 *
 * Roles can be granted and revoked dynamically via the {grantRole} and
 * {revokeRole} functions. Each role has an associated admin role, and only
 * accounts that have a role\u0027s admin role can call {grantRole} and {revokeRole}.
 *
 * By default, the admin role for all roles is `DEFAULT_ADMIN_ROLE`, which means
 * that only accounts with this role will be able to grant or revoke other
 * roles. More complex role relationships can be created by using
 * {_setRoleAdmin}.
 *
 * WARNING: The `DEFAULT_ADMIN_ROLE` is also its own admin: it has permission to
 * grant and revoke this role. Extra precautions should be taken to secure
 * accounts that have been granted it.
 */
abstract contract AccessControl is Context, IAccessControl, ERC165 {
    struct RoleData {
        mapping (address =\u003e bool) members;
        bytes32 adminRole;
    }

    mapping (bytes32 =\u003e RoleData) private _roles;

    bytes32 public constant DEFAULT_ADMIN_ROLE = 0x00;

    /**
     * @dev Emitted when `newAdminRole` is set as ``role``\u0027s admin role, replacing `previousAdminRole`
     *
     * `DEFAULT_ADMIN_ROLE` is the starting admin for all roles, despite
     * {RoleAdminChanged} not being emitted signaling this.
     *
     * _Available since v3.1._
     */
    event RoleAdminChanged(bytes32 indexed role, bytes32 indexed previousAdminRole, bytes32 indexed newAdminRole);

    /**
     * @dev Emitted when `account` is granted `role`.
     *
     * `sender` is the account that originated the contract call, an admin role
     * bearer except when using {_setupRole}.
     */
    event RoleGranted(bytes32 indexed role, address indexed account, address indexed sender);

    /**
     * @dev Emitted when `account` is revoked `role`.
     *
     * `sender` is the account that originated the contract call:
     *   - if using `revokeRole`, it is the admin role bearer
     *   - if using `renounceRole`, it is the role bearer (i.e. `account`)
     */
    event RoleRevoked(bytes32 indexed role, address indexed account, address indexed sender);

    /**
     * @dev Modifier that checks that an account has a specific role. Reverts
     * with a standardized message including the required role.
     *
     * The format of the revert reason is given by the following regular expression:
     *
     *  /^AccessControl: account (0x[0-9a-f]{20}) is missing role (0x[0-9a-f]{32})$/
     *
     * _Available since v4.1._
     */
    modifier onlyRole(bytes32 role) {
        _checkRole(role, _msgSender());
        _;
    }

    /**
     * @dev See {IERC165-supportsInterface}.
     */
    function supportsInterface(bytes4 interfaceId) public view virtual override returns (bool) {
        return interfaceId == type(IAccessControl).interfaceId
            || super.supportsInterface(interfaceId);
    }

    /**
     * @dev Returns `true` if `account` has been granted `role`.
     */
    function hasRole(bytes32 role, address account) public view override returns (bool) {
        return _roles[role].members[account];
    }

    /**
     * @dev Revert with a standard message if `account` is missing `role`.
     *
     * The format of the revert reason is given by the following regular expression:
     *
     *  /^AccessControl: account (0x[0-9a-f]{20}) is missing role (0x[0-9a-f]{32})$/
     */
    function _checkRole(bytes32 role, address account) internal view {
        if(!hasRole(role, account)) {
            revert(string(abi.encodePacked(
                \"AccessControl: account \",
                Strings.toHexString(uint160(account), 20),
                \" is missing role \",
                Strings.toHexString(uint256(role), 32)
            )));
        }
    }

    /**
     * @dev Returns the admin role that controls `role`. See {grantRole} and
     * {revokeRole}.
     *
     * To change a role\u0027s admin, use {_setRoleAdmin}.
     */
    function getRoleAdmin(bytes32 role) public view override returns (bytes32) {
        return _roles[role].adminRole;
    }

    /**
     * @dev Grants `role` to `account`.
     *
     * If `account` had not been already granted `role`, emits a {RoleGranted}
     * event.
     *
     * Requirements:
     *
     * - the caller must have ``role``\u0027s admin role.
     */
    function grantRole(bytes32 role, address account) public virtual override onlyRole(getRoleAdmin(role)) {
        _grantRole(role, account);
    }

    /**
     * @dev Revokes `role` from `account`.
     *
     * If `account` had been granted `role`, emits a {RoleRevoked} event.
     *
     * Requirements:
     *
     * - the caller must have ``role``\u0027s admin role.
     */
    function revokeRole(bytes32 role, address account) public virtual override onlyRole(getRoleAdmin(role)) {
        _revokeRole(role, account);
    }

    /**
     * @dev Revokes `role` from the calling account.
     *
     * Roles are often managed via {grantRole} and {revokeRole}: this function\u0027s
     * purpose is to provide a mechanism for accounts to lose their privileges
     * if they are compromised (such as when a trusted device is misplaced).
     *
     * If the calling account had been granted `role`, emits a {RoleRevoked}
     * event.
     *
     * Requirements:
     *
     * - the caller must be `account`.
     */
    function renounceRole(bytes32 role, address account) public virtual override {
        require(account == _msgSender(), \"AccessControl: can only renounce roles for self\");

        _revokeRole(role, account);
    }

    /**
     * @dev Grants `role` to `account`.
     *
     * If `account` had not been already granted `role`, emits a {RoleGranted}
     * event. Note that unlike {grantRole}, this function doesn\u0027t perform any
     * checks on the calling account.
     *
     * [WARNING]
     * ====
     * This function should only be called from the constructor when setting
     * up the initial roles for the system.
     *
     * Using this function in any other way is effectively circumventing the admin
     * system imposed by {AccessControl}.
     * ====
     */
    function _setupRole(bytes32 role, address account) internal virtual {
        _grantRole(role, account);
    }

    /**
     * @dev Sets `adminRole` as ``role``\u0027s admin role.
     *
     * Emits a {RoleAdminChanged} event.
     */
    function _setRoleAdmin(bytes32 role, bytes32 adminRole) internal virtual {
        emit RoleAdminChanged(role, getRoleAdmin(role), adminRole);
        _roles[role].adminRole = adminRole;
    }

    function _grantRole(bytes32 role, address account) private {
        if (!hasRole(role, account)) {
            _roles[role].members[account] = true;
            emit RoleGranted(role, account, _msgSender());
        }
    }

    function _revokeRole(bytes32 role, address account) private {
        if (hasRole(role, account)) {
            _roles[role].members[account] = false;
            emit RoleRevoked(role, account, _msgSender());
        }
    }
}"},"Context.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

/*
 * @dev Provides information about the current execution context, including the
 * sender of the transaction and its data. While these are generally available
 * via msg.sender and msg.data, they should not be accessed in such a direct
 * manner, since when dealing with meta-transactions the account sending and
 * paying for execution may not be the actual sender (as far as an application
 * is concerned).
 *
 * This contract is only required for intermediate, library-like contracts.
 */
abstract contract Context {
    function _msgSender() internal view virtual returns (address) {
        return msg.sender;
    }

    function _msgData() internal view virtual returns (bytes calldata) {
        this; // silence state mutability warning without generating bytecode - see https://github.com/ethereum/solidity/issues/2691
        return msg.data;
    }
}"},"ERC165.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import \"./IERC165.sol\";

/**
 * @dev Implementation of the {IERC165} interface.
 *
 * Contracts that want to implement ERC165 should inherit from this contract and override {supportsInterface} to check
 * for the additional interface id that will be supported. For example:
 *
 * ```solidity
 * function supportsInterface(bytes4 interfaceId) public view virtual override returns (bool) {
 *     return interfaceId == type(MyInterface).interfaceId || super.supportsInterface(interfaceId);
 * }
 * ```
 *
 * Alternatively, {ERC165Storage} provides an easier to use but more expensive implementation.
 */
abstract contract ERC165 is IERC165 {
    /**
     * @dev See {IERC165-supportsInterface}.
     */
    function supportsInterface(bytes4 interfaceId) public view virtual override returns (bool) {
        return interfaceId == type(IERC165).interfaceId;
    }
}"},"IERC165.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

/**
 * @dev Interface of the ERC165 standard, as defined in the
 * https://eips.ethereum.org/EIPS/eip-165[EIP].
 *
 * Implementers can declare support of contract interfaces, which can then be
 * queried by others ({ERC165Checker}).
 *
 * For an implementation, see {ERC165}.
 */
interface IERC165 {
    /**
     * @dev Returns true if this contract implements the interface defined by
     * `interfaceId`. See the corresponding
     * https://eips.ethereum.org/EIPS/eip-165#how-interfaces-are-identified[EIP section]
     * to learn more about how these ids are created.
     *
     * This function call must use less than 30 000 gas.
     */
    function supportsInterface(bytes4 interfaceId) external view returns (bool);
}
"},"ReentrancyGuard.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

/**
 * @dev Contract module that helps prevent reentrant calls to a function.
 *
 * Inheriting from `ReentrancyGuard` will make the {nonReentrant} modifier
 * available, which can be applied to functions to make sure there are no nested
 * (reentrant) calls to them.
 *
 * Note that because there is a single `nonReentrant` guard, functions marked as
 * `nonReentrant` may not call one another. This can be worked around by making
 * those functions `private`, and then adding `external` `nonReentrant` entry
 * points to them.
 *
 * TIP: If you would like to learn more about reentrancy and alternative ways
 * to protect against it, check out our blog post
 * https://blog.openzeppelin.com/reentrancy-after-istanbul/[Reentrancy After Istanbul].
 */
abstract contract ReentrancyGuard {
    // Booleans are more expensive than uint256 or any type that takes up a full
    // word because each write operation emits an extra SLOAD to first read the
    // slot\u0027s contents, replace the bits taken up by the boolean, and then write
    // back. This is the compiler\u0027s defense against contract upgrades and
    // pointer aliasing, and it cannot be disabled.

    // The values being non-zero value makes deployment a bit more expensive,
    // but in exchange the refund on every call to nonReentrant will be lower in
    // amount. Since refunds are capped to a percentage of the total
    // transaction\u0027s gas, it is best to keep them low in cases like this one, to
    // increase the likelihood of the full refund coming into effect.
    uint256 private constant _NOT_ENTERED = 1;
    uint256 private constant _ENTERED = 2;

    uint256 private _status;

    constructor () {
        _status = _NOT_ENTERED;
    }

    /**
     * @dev Prevents a contract from calling itself, directly or indirectly.
     * Calling a `nonReentrant` function from another `nonReentrant`
     * function is not supported. It is possible to prevent this from happening
     * by making the `nonReentrant` function external, and make it call a
     * `private` function that does the actual work.
     */
    modifier nonReentrant() {
        // On the first call to nonReentrant, _notEntered will be true
        require(_status != _ENTERED, \"ReentrancyGuard: reentrant call\");

        // Any calls to nonReentrant after this point will fail
        _status = _ENTERED;

        _;

        // By storing the original value once again, a refund is triggered (see
        // https://eips.ethereum.org/EIPS/eip-2200)
        _status = _NOT_ENTERED;
    }
}"},"SealedSwapper.sol":{"content":"// Be Name KHODA
// Bime Abolfazl

// SPDX-License-Identifier: MIT
pragma solidity ^0.8.4;

import \"./AccessControl.sol\";
import \"./ReentrancyGuard.sol\";


interface IBPool {
\tfunction totalSupply() external view returns (uint);
\tfunction exitPool(uint poolAmountIn, uint[] calldata minAmountsOut) external;
\tfunction exitswapPoolAmountIn(address tokenOut, uint256 poolAmountIn, uint256 minAmountOut) external returns (uint256 tokenAmountOut);
\tfunction transferFrom(address src, address dst, uint256 amt) external returns (bool);
}

interface IERC20 {
\tfunction approve(address dst, uint256 amt) external returns (bool);
\tfunction totalSupply() external view returns (uint);
\tfunction burn(address from, uint256 amount) external;
\tfunction transfer(address recipient, uint256 amount) external returns (bool);
\tfunction transferFrom(address src, address dst, uint256 amt) external returns (bool);
\tfunction balanceOf(address owner) external view returns (uint);
}

interface Vault {
\tfunction lockFor(uint256 amount, address _user) external returns (uint256);
}

interface IUniswapV2Pair {
\tfunction getReserves() external view returns (uint112 reserve0, uint112 reserve1, uint32 blockTimestampLast);
}

interface IUniswapV2Router02 {
\tfunction removeLiquidityETH(
\t\taddress token,
\t\tuint256 liquidity,
\t\tuint256 amountTokenMin,
\t\tuint256 amountETHMin,
\t\taddress to,
\t\tuint256 deadline
\t) external returns (uint256 amountToken, uint256 amountETH);

\tfunction removeLiquidity(
\t\taddress tokenA,
\t\taddress tokenB,
\t\tuint256 liquidity,
\t\tuint256 amountAMin,
\t\tuint256 amountBMin,
\t\taddress to,
\t\tuint256 deadline
\t) external returns (uint256 amountA, uint256 amountB);

\tfunction swapExactTokensForTokens(
\t\tuint256 amountIn,
\t\tuint256 amountOutMin,
\t\taddress[] calldata path,
\t\taddress to,
\t\tuint256 deadline
\t) external returns (uint256[] memory amounts);

\tfunction swapExactTokensForETH(
\t\tuint amountIn,
\t\tuint amountOutMin,
\t\taddress[] calldata path,
\t\taddress to,
\t\tuint deadline
\t) external returns (uint[] memory amounts);

\tfunction getAmountsOut(uint256 amountIn, address[] memory path) external  returns (uint256[] memory amounts);
}

interface AutomaticMarketMaker {
\tfunction calculateSaleReturn(uint256 tokenAmount) external view returns (uint256);
\tfunction calculatePurchaseReturn(uint256 etherAmount) external returns (uint256);
\tfunction buy(uint256 _tokenAmount) external payable;
\tfunction sell(uint256 tokenAmount, uint256 _etherAmount) external;
\tfunction withdrawPayments(address payable payee) external;
}

contract SealedSwapper is AccessControl, ReentrancyGuard {

\tbytes32 public constant OPERATOR_ROLE = keccak256(\"OPERATOR_ROLE\");
\tbytes32 public constant ADMIN_SWAPPER_ROLE = keccak256(\"ADMIN_SWAPPER_ROLE\");
\tbytes32 public constant TRUSTY_ROLE = keccak256(\"TRUSTY_ROLE\");
\t
\tIBPool public bpt;
\tIUniswapV2Router02 public uniswapRouter;
\tAutomaticMarketMaker public AMM;
\tVault public sdeaVault;
\tIERC20 public sdeus;
\tIERC20 public sdea;
\tIERC20 public sUniDD;
\tIERC20 public sUniDE;
\tIERC20 public sUniDU;
\tIERC20 public dea;
\tIERC20 public deus;
\tIERC20 public usdc;
\tIERC20 public uniDD;
\tIERC20 public uniDU;
\tIERC20 public uniDE;

\taddress[] public usdc2wethPath =  [0xA0b86991c6218b36c1d19D4a2e9Eb0cE3606eB48, 0xC02aaA39b223FE8D0A0e5C4F27eAD9083C756Cc2];
\taddress[] public deus2deaPath =  [0x3b62F3820e0B035cc4aD602dECe6d796BC325325, 0x80aB141F324C3d6F2b18b030f1C4E95d4d658778];
\t

\tuint256 public MAX_INT = type(uint256).max;
\tuint256 public scale = 1e18;
\tuint256 public DDRatio;
\tuint256 public DERatio;
\tuint256 public DURatio;
\tuint256 public deusRatio;
\tuint256 public DUVaultRatio;

\tevent Swap(address user, address tokenIn, address tokenOut, uint256 amountIn, uint256 amountOut);

\tconstructor (
\t\taddress _uniswapRouter,
\t\taddress _bpt,
\t\taddress _amm,
\t\taddress _sdeaVault
\t) ReentrancyGuard() {
\t    _setupRole(DEFAULT_ADMIN_ROLE, msg.sender);
\t    _setupRole(OPERATOR_ROLE, msg.sender);
\t\tuniswapRouter = IUniswapV2Router02(_uniswapRouter);
\t\tbpt = IBPool(_bpt);
\t\tAMM = AutomaticMarketMaker(_amm);
\t\tsdeaVault = Vault(_sdeaVault);
\t}
\t
\tfunction init(
\t\taddress[] memory tokens,
\t\tuint256 _DERatio,
\t\tuint256 _DURatio,
\t\tuint256 _DDRatio,
\t\tuint256 _deusRatio,
\t\tuint256 _DUVaultRatio
\t) external {
\t\trequire(hasRole(OPERATOR_ROLE, msg.sender), \"OPERATOR_ROLE ERROR\");
\t\tsdea = IERC20(tokens[0]);
\t\tsdeus = IERC20(tokens[1]);
\t\tsUniDD = IERC20(tokens[2]);
\t\tsUniDE = IERC20(tokens[3]);
\t\tsUniDU = IERC20(tokens[4]);
\t\tdea = IERC20(tokens[5]);
\t\tdeus = IERC20(tokens[6]);
\t\tusdc = IERC20(tokens[7]);
\t\tuniDD = IERC20(tokens[8]);
\t\tuniDU = IERC20(tokens[9]);
\t\tuniDE = IERC20(tokens[10]);
\t\tdea.approve(address(uniswapRouter), MAX_INT);
\t\tdeus.approve(address(uniswapRouter), MAX_INT);
\t\tusdc.approve(address(uniswapRouter), MAX_INT);
\t\tuniDD.approve(address(uniswapRouter), MAX_INT);
\t\tuniDE.approve(address(uniswapRouter), MAX_INT);
\t\tuniDU.approve(address(uniswapRouter), MAX_INT);
\t\tdea.approve(address(sdeaVault), MAX_INT);
\t\tDDRatio = _DDRatio;
\t\tDURatio = _DURatio;
\t\tDERatio = _DERatio;
\t\tdeusRatio = _deusRatio;
\t\tDUVaultRatio = _DUVaultRatio;
\t}

\tfunction setRatios(uint256 _DERatio, uint256 _DURatio, uint256 _DDRatio, uint256 _deusRatio, uint256 _DUVaultRatio) external {
\t\trequire(hasRole(OPERATOR_ROLE, msg.sender), \"OPERATOR_ROLE ERROR\");
\t\tDDRatio = _DDRatio;
\t\tDURatio = _DURatio;
\t\tDERatio = _DERatio;
\t\tdeusRatio = _deusRatio;
\t\tDUVaultRatio = _DUVaultRatio;
\t}

\tfunction approve(address token, address recipient, uint256 amount) external {
\t\trequire(hasRole(TRUSTY_ROLE, msg.sender), \"TRUSTY_ROLE ERROR\");
\t\tIERC20(token).approve(recipient, amount);
\t}

\tfunction bpt2eth(address tokenOut, uint256 poolAmountIn, uint256[] memory minAmountsOut) public nonReentrant() {
\t\tbpt.transferFrom(msg.sender, address(this), poolAmountIn);
\t\tuint256 deaAmount = bpt.exitswapPoolAmountIn(tokenOut, poolAmountIn, minAmountsOut[0]);
\t\tuint256 deusAmount = uniswapRouter.swapExactTokensForTokens(deaAmount, minAmountsOut[1], deus2deaPath, address(this), block.timestamp + 1 days)[1];
\t\tuint256 ethAmount = AMM.calculateSaleReturn(deusAmount);
\t\tAMM.sell(deusAmount, minAmountsOut[2]);
\t\tAMM.withdrawPayments(payable(address(this)));
\t\tpayable(msg.sender).transfer(ethAmount);
\t}

\tfunction deus2dea(uint256 amountIn) internal returns(uint256) {
\t\treturn uniswapRouter.swapExactTokensForTokens(amountIn, 1, deus2deaPath, address(this), block.timestamp + 1 days)[1];
\t}

\tfunction bpt2sdea(address tokenOut, uint256 poolAmountIn, uint256 minAmountOut) public nonReentrant() {
\t\tbpt.transferFrom(msg.sender, address(this), poolAmountIn);

\t\tuint256 deaAmount = bpt.exitswapPoolAmountIn(tokenOut, poolAmountIn, minAmountOut);
\t\tuint256 sdeaAmount = sdeaVault.lockFor(deaAmount, msg.sender);

\t\tsdea.transfer(msg.sender, sdeaAmount);
\t\temit Swap(msg.sender, address(bpt), address(sdea), poolAmountIn, sdeaAmount);
\t}

\tfunction sdea2dea(uint256 amount, address recipient) external nonReentrant() {
\t\trequire(hasRole(ADMIN_SWAPPER_ROLE, msg.sender), \"ADMIN_SWAPPER_ROLE ERROR\");
\t\tsdea.burn(msg.sender, amount);
\t\tdea.transfer(recipient, amount);
\t\t
\t\temit Swap(recipient, address(sdea), address(dea), amount, amount);
\t}

\tfunction sdeus2deus(uint256 amount, address recipient) external nonReentrant() {
\t\trequire(hasRole(ADMIN_SWAPPER_ROLE, msg.sender), \"ADMIN_SWAPPER_ROLE ERROR\");
\t\tsdeus.burn(msg.sender, amount);
\t\tdeus.transfer(recipient, amount);

\t\temit Swap(recipient, address(sdeus), address(deus), amount, amount);
\t}

\tfunction sUniDE2UniDE(uint256 amount, address recipient) external nonReentrant() {
\t\trequire(hasRole(ADMIN_SWAPPER_ROLE, msg.sender), \"ADMIN_SWAPPER_ROLE ERROR\");
\t\tsUniDE.burn(msg.sender, amount);
\t\tuniDE.transfer(recipient, amount);

\t\temit Swap(recipient, address(sUniDE), address(uniDE), amount, amount);
\t}

\tfunction sUniDD2UniDD(uint256 amount, address recipient) external nonReentrant() {
\t\trequire(hasRole(ADMIN_SWAPPER_ROLE, msg.sender), \"ADMIN_SWAPPER_ROLE ERROR\");
\t\tsUniDD.burn(msg.sender, amount);
\t\tuniDD.transfer(recipient, amount);

\t\temit Swap(recipient, address(sUniDD), address(uniDD), amount, amount);
\t}

\tfunction sUniDU2UniDU(uint256 amount, address recipient) external nonReentrant() {
\t\trequire(hasRole(ADMIN_SWAPPER_ROLE, msg.sender), \"ADMIN_SWAPPER_ROLE ERROR\");
\t\tsUniDU.burn(msg.sender, amount);
\t\tuniDU.transfer(recipient, amount/DUVaultRatio);

\t\temit Swap(recipient, address(sUniDU), address(uniDU), amount, amount/DUVaultRatio);
\t}

\tfunction deaExitAmount(uint256 Predeemed) public view returns(uint256) {
\t\tuint256 Psupply = bpt.totalSupply();
\t\tuint256 Bk = dea.balanceOf(address(bpt));
\t\treturn Bk - (((Psupply - Predeemed) * Bk) / Psupply);
\t}

\tfunction bpt2sdea(
\t\tuint256 poolAmountIn,
\t\tuint256[] memory balancerMinAmountsOut,
\t\tuint256 minAmountOut
\t) external nonReentrant() {
\t\tbpt.transferFrom(msg.sender, address(this), poolAmountIn);
\t\tuint256 deaAmount = deaExitAmount(poolAmountIn);

\t\tbpt.exitPool(poolAmountIn, balancerMinAmountsOut);

\t\tuint256 sdeusAmount = sdeus.balanceOf(address(this));
\t\tsdeus.burn(address(this), sdeusAmount);
\t\tdeaAmount += deus2dea(sdeusAmount * deusRatio / scale);

\t\tuint256 sUniDDAmount = sUniDD.balanceOf(address(this));
\t\tsUniDD.burn(address(this), sUniDDAmount);
\t\tdeaAmount += uniDD2dea(sUniDDAmount * DDRatio / scale);

\t\tuint256 sUniDEAmount = sUniDE.balanceOf(address(this));
\t\tsUniDE.burn(address(this), sUniDEAmount);
\t\tdeaAmount += uniDE2dea(sUniDEAmount * DERatio / scale);

\t\tuint256 sUniDUAmount = sUniDU.balanceOf(address(this));
\t\tsUniDU.burn(address(this), sUniDUAmount);
\t\tdeaAmount += uniDU2dea(sUniDUAmount * DURatio / scale);

\t\trequire(deaAmount \u003e= minAmountOut, \"INSUFFICIENT_OUTPUT_AMOUNT\");

\t\tsdeaVault.lockFor(deaAmount, msg.sender);
\t\tsdea.transfer(msg.sender, deaAmount);

\t\temit Swap(msg.sender, address(bpt), address(sdea), poolAmountIn, deaAmount);
\t}

\tfunction uniDD2dea(uint256 sUniDDAmount) internal returns(uint256) {
\t\t(uint256 deusAmount, uint256 deaAmount) = uniswapRouter.removeLiquidity(address(deus), address(dea), sUniDDAmount, 1, 1, address(this), block.timestamp + 1 days);

\t\tuint256 deaAmount2 = uniswapRouter.swapExactTokensForTokens(deusAmount, 1, deus2deaPath, address(this), block.timestamp + 1 days)[1];

\t\treturn deaAmount + deaAmount2;
\t}

\tfunction sUniDD2sdea(uint256 sUniDDAmount, uint256 minAmountOut) public nonReentrant() {
\t\tsUniDD.burn(msg.sender, sUniDDAmount);

\t\tuint256 deaAmount = uniDD2dea(sUniDDAmount * DDRatio / scale);

\t\trequire(deaAmount \u003e= minAmountOut, \"INSUFFICIENT_OUTPUT_AMOUNT\");
\t\tsdeaVault.lockFor(deaAmount, msg.sender);
\t\tsdea.transfer(msg.sender, deaAmount);

\t\temit Swap(msg.sender, address(uniDD), address(sdea), sUniDDAmount, deaAmount);
\t}

\tfunction uniDU2dea(uint256 sUniDUAmount) internal returns(uint256) {
\t\t(uint256 deaAmount, uint256 usdcAmount) = uniswapRouter.removeLiquidity(address(dea), address(usdc), (sUniDUAmount/DUVaultRatio), 1, 1, address(this), block.timestamp + 1 days);

\t\tuint256 ethAmount = uniswapRouter.swapExactTokensForETH(usdcAmount, 1, usdc2wethPath, address(this), block.timestamp + 1 days)[1];

\t\tuint256 deusAmount = AMM.calculatePurchaseReturn(ethAmount);
\t\tAMM.buy{value: ethAmount}(deusAmount);
\t\t
\t\tuint256 deaAmount2 = uniswapRouter.swapExactTokensForTokens(deusAmount, 1, deus2deaPath, address(this), block.timestamp + 1 days)[1];

\t\treturn deaAmount + deaAmount2;
\t}
\t

\tfunction sUniDU2sdea(uint256 sUniDUAmount, uint256 minAmountOut) public nonReentrant() {
\t\tsUniDU.burn(msg.sender, sUniDUAmount);

\t\tuint256 deaAmount = uniDU2dea(sUniDUAmount * DURatio / scale);

\t\trequire(deaAmount \u003e= minAmountOut, \"INSUFFICIENT_OUTPUT_AMOUNT\");
\t\tsdeaVault.lockFor(deaAmount, msg.sender);
\t\tsdea.transfer(msg.sender, deaAmount);
\t\t
\t\temit Swap(msg.sender, address(uniDU), address(sdea), sUniDUAmount, deaAmount);
\t}

\tfunction uniDE2dea(uint256 sUniDEAmount) internal returns(uint256) {
\t\t(uint256 deusAmount, uint256 ethAmount) = uniswapRouter.removeLiquidityETH(address(deus), sUniDEAmount, 1, 1, address(this), block.timestamp + 1 days);
\t\tuint256 deusAmount2 = AMM.calculatePurchaseReturn(ethAmount);
\t\tAMM.buy{value: ethAmount}(deusAmount2);
\t\tuint256 deaAmount = uniswapRouter.swapExactTokensForTokens(deusAmount + deusAmount2, 1, deus2deaPath, address(this), block.timestamp + 1 days)[1];
\t\treturn deaAmount;
\t}

\tfunction sUniDE2sdea(uint256 sUniDEAmount, uint256 minAmountOut) public nonReentrant() {
\t\tsUniDE.burn(msg.sender, sUniDEAmount);

\t\tuint256 deaAmount = uniDE2dea(sUniDEAmount * DERatio / scale);

\t\trequire(deaAmount \u003e= minAmountOut, \"INSUFFICIENT_OUTPUT_AMOUNT\");
\t\tsdeaVault.lockFor(deaAmount, msg.sender);
\t\tsdea.transfer(msg.sender, deaAmount);

\t\temit Swap(msg.sender, address(uniDE), address(sdea), sUniDEAmount, deaAmount);
\t}

\tfunction withdraw(address token, uint256 amount, address to) public {
\t\trequire(hasRole(TRUSTY_ROLE, msg.sender), \"TRUSTY_ROLE ERROR\");
\t\tIERC20(token).transfer(to, amount);
\t}

\treceive() external payable {}
}

// Dar panahe Khoda"},"Strings.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

/**
 * @dev String operations.
 */
library Strings {
    bytes16 private constant alphabet = \"0123456789abcdef\";

    /**
     * @dev Converts a `uint256` to its ASCII `string` decimal representation.
     */
    function toString(uint256 value) internal pure returns (string memory) {
        // Inspired by OraclizeAPI\u0027s implementation - MIT licence
        // https://github.com/oraclize/ethereum-api/blob/b42146b063c7d6ee1358846c198246239e9360e8/oraclizeAPI_0.4.25.sol

        if (value == 0) {
            return \"0\";
        }
        uint256 temp = value;
        uint256 digits;
        while (temp != 0) {
            digits++;
            temp /= 10;
        }
        bytes memory buffer = new bytes(digits);
        while (value != 0) {
            digits -= 1;
            buffer[digits] = bytes1(uint8(48 + uint256(value % 10)));
            value /= 10;
        }
        return string(buffer);
    }

    /**
     * @dev Converts a `uint256` to its ASCII `string` hexadecimal representation.
     */
    function toHexString(uint256 value) internal pure returns (string memory) {
        if (value == 0) {
            return \"0x00\";
        }
        uint256 temp = value;
        uint256 length = 0;
        while (temp != 0) {
            length++;
            temp \u003e\u003e= 8;
        }
        return toHexString(value, length);
    }

    /**
     * @dev Converts a `uint256` to its ASCII `string` hexadecimal representation with fixed length.
     */
    function toHexString(uint256 value, uint256 length) internal pure returns (string memory) {
        bytes memory buffer = new bytes(2 * length + 2);
        buffer[0] = \"0\";
        buffer[1] = \"x\";
        for (uint256 i = 2 * length + 1; i \u003e 1; --i) {
            buffer[i] = alphabet[value \u0026 0xf];
            value \u003e\u003e= 4;
        }
        require(value == 0, \"Strings: hex length insufficient\");
        return string(buffer);
    }

}
