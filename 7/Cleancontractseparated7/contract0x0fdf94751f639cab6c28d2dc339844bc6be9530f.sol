// SPDX-License-Identifier: MIT

pragma solidity ^0.6.0;

/*
 * @dev Provides information about the current execution context, including the
 * sender of the transaction and its data. While these are generally available
 * via msg.sender and msg.data, they should not be accessed in such a direct
 * manner, since when dealing with GSN meta-transactions the account sending and
 * paying for execution may not be the actual sender (as far as an application
 * is concerned).
 *
 * This contract is only required for intermediate, library-like contracts.
 */
abstract contract Context {
    function _msgSender() internal view virtual returns (address payable) {
        return msg.sender;
    }

    function _msgData() internal view virtual returns (bytes memory) {
        this; // silence state mutability warning without generating bytecode - see https://github.com/ethereum/solidity/issues/2691
        return msg.data;
    }
}
"},"ContractRegistryAccessor.sol":{"content":"// SPDX-License-Identifier: UNLICENSED

pragma solidity 0.6.12;

import \"./IContractRegistry.sol\";
import \"./WithClaimableRegistryManagement.sol\";
import \"./Initializable.sol\";

contract ContractRegistryAccessor is WithClaimableRegistryManagement, Initializable {

    IContractRegistry private contractRegistry;

    constructor(IContractRegistry _contractRegistry, address _registryAdmin) public {
        require(address(_contractRegistry) != address(0), \"_contractRegistry cannot be 0\");
        setContractRegistry(_contractRegistry);
        _transferRegistryManagement(_registryAdmin);
    }

    modifier onlyAdmin {
        require(isAdmin(), \"sender is not an admin (registryManger or initializationAdmin)\");

        _;
    }

    function isManager(string memory role) internal view returns (bool) {
        IContractRegistry _contractRegistry = contractRegistry;
        return isAdmin() || _contractRegistry != IContractRegistry(0) \u0026\u0026 contractRegistry.getManager(role) == msg.sender;
    }

    function isAdmin() internal view returns (bool) {
        return msg.sender == registryAdmin() || msg.sender == initializationAdmin() || msg.sender == address(contractRegistry);
    }

    function getProtocolContract() internal view returns (address) {
        return contractRegistry.getContract(\"protocol\");
    }

    function getStakingRewardsContract() internal view returns (address) {
        return contractRegistry.getContract(\"stakingRewards\");
    }

    function getFeesAndBootstrapRewardsContract() internal view returns (address) {
        return contractRegistry.getContract(\"feesAndBootstrapRewards\");
    }

    function getCommitteeContract() internal view returns (address) {
        return contractRegistry.getContract(\"committee\");
    }

    function getElectionsContract() internal view returns (address) {
        return contractRegistry.getContract(\"elections\");
    }

    function getDelegationsContract() internal view returns (address) {
        return contractRegistry.getContract(\"delegations\");
    }

    function getGuardiansRegistrationContract() internal view returns (address) {
        return contractRegistry.getContract(\"guardiansRegistration\");
    }

    function getCertificationContract() internal view returns (address) {
        return contractRegistry.getContract(\"certification\");
    }

    function getStakingContract() internal view returns (address) {
        return contractRegistry.getContract(\"staking\");
    }

    function getSubscriptionsContract() internal view returns (address) {
        return contractRegistry.getContract(\"subscriptions\");
    }

    function getStakingRewardsWallet() internal view returns (address) {
        return contractRegistry.getContract(\"stakingRewardsWallet\");
    }

    function getBootstrapRewardsWallet() internal view returns (address) {
        return contractRegistry.getContract(\"bootstrapRewardsWallet\");
    }

    function getGeneralFeesWallet() internal view returns (address) {
        return contractRegistry.getContract(\"generalFeesWallet\");
    }

    function getCertifiedFeesWallet() internal view returns (address) {
        return contractRegistry.getContract(\"certifiedFeesWallet\");
    }

    function getStakingContractHandler() internal view returns (address) {
        return contractRegistry.getContract(\"stakingContractHandler\");
    }

    /*
    * Governance functions
    */

    event ContractRegistryAddressUpdated(address addr);

    function setContractRegistry(IContractRegistry newContractRegistry) public onlyAdmin {
        require(newContractRegistry.getPreviousContractRegistry() == address(contractRegistry), \"new contract registry must provide the previous contract registry\");
        contractRegistry = newContractRegistry;
        emit ContractRegistryAddressUpdated(address(newContractRegistry));
    }

    function getContractRegistry() public view returns (IContractRegistry) {
        return contractRegistry;
    }

}
"},"IContractRegistry.sol":{"content":"// SPDX-License-Identifier: UNLICENSED

pragma solidity 0.6.12;

interface IContractRegistry {

\tevent ContractAddressUpdated(string contractName, address addr, bool managedContract);
\tevent ManagerChanged(string role, address newManager);
\tevent ContractRegistryUpdated(address newContractRegistry);

\t/*
\t* External functions
\t*/

\t/// @dev updates the contracts address and emits a corresponding event
\t/// managedContract indicates whether the contract is managed by the registry and notified on changes
\tfunction setContract(string calldata contractName, address addr, bool managedContract) external /* onlyAdmin */;

\t/// @dev returns the current address of the given contracts
\tfunction getContract(string calldata contractName) external view returns (address);

\t/// @dev returns the list of contract addresses managed by the registry
\tfunction getManagedContracts() external view returns (address[] memory);

\tfunction setManager(string calldata role, address manager) external /* onlyAdmin */;

\tfunction getManager(string calldata role) external view returns (address);

\tfunction lockContracts() external /* onlyAdmin */;

\tfunction unlockContracts() external /* onlyAdmin */;

\tfunction setNewContractRegistry(IContractRegistry newRegistry) external /* onlyAdmin */;

\tfunction getPreviousContractRegistry() external view returns (address);

}
"},"IERC20.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.6.0;

/**
 * @dev Interface of the ERC20 standard as defined in the EIP.
 */
interface IERC20 {
    /**
     * @dev Returns the amount of tokens in existence.
     */
    function totalSupply() external view returns (uint256);

    /**
     * @dev Returns the amount of tokens owned by `account`.
     */
    function balanceOf(address account) external view returns (uint256);

    /**
     * @dev Moves `amount` tokens from the caller\u0027s account to `recipient`.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transfer(address recipient, uint256 amount) external returns (bool);

    /**
     * @dev Returns the remaining number of tokens that `spender` will be
     * allowed to spend on behalf of `owner` through {transferFrom}. This is
     * zero by default.
     *
     * This value changes when {approve} or {transferFrom} are called.
     */
    function allowance(address owner, address spender) external view returns (uint256);

    /**
     * @dev Sets `amount` as the allowance of `spender` over the caller\u0027s tokens.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * IMPORTANT: Beware that changing an allowance with this method brings the risk
     * that someone may use both the old and the new allowance by unfortunate
     * transaction ordering. One possible solution to mitigate this race
     * condition is to first reduce the spender\u0027s allowance to 0 and set the
     * desired value afterwards:
     * https://github.com/ethereum/EIPs/issues/20#issuecomment-263524729
     *
     * Emits an {Approval} event.
     */
    function approve(address spender, uint256 amount) external returns (bool);

    /**
     * @dev Moves `amount` tokens from `sender` to `recipient` using the
     * allowance mechanism. `amount` is then deducted from the caller\u0027s
     * allowance.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transferFrom(address sender, address recipient, uint256 amount) external returns (bool);

    /**
     * @dev Emitted when `value` tokens are moved from one account (`from`) to
     * another (`to`).
     *
     * Note that `value` may be zero.
     */
    event Transfer(address indexed from, address indexed to, uint256 value);

    /**
     * @dev Emitted when the allowance of a `spender` for an `owner` is set by
     * a call to {approve}. `value` is the new allowance.
     */
    event Approval(address indexed owner, address indexed spender, uint256 value);
}
"},"ILockable.sol":{"content":"// SPDX-License-Identifier: UNLICENSED

pragma solidity 0.6.12;

interface ILockable {

    event Locked();
    event Unlocked();

    function lock() external /* onlyLockOwner */;
    function unlock() external /* onlyLockOwner */;
    function isLocked() view external returns (bool);

}
"},"IMigratableStakingContract.sol":{"content":"// SPDX-License-Identifier: UNLICENSED

pragma solidity 0.6.12;

import \"./IERC20.sol\";

/// @title An interface for staking contracts which support stake migration.
interface IMigratableStakingContract {
    /// @dev Returns the address of the underlying staked token.
    /// @return IERC20 The address of the token.
    function getToken() external view returns (IERC20);

    /// @dev Stakes ORBS tokens on behalf of msg.sender. This method assumes that the user has already approved at least
    /// the required amount using ERC20 approve.
    /// @param _stakeOwner address The specified stake owner.
    /// @param _amount uint256 The number of tokens to stake.
    function acceptMigration(address _stakeOwner, uint256 _amount) external;

    event AcceptedMigration(address indexed stakeOwner, uint256 amount, uint256 totalStakedAmount);
}
"},"Initializable.sol":{"content":"// SPDX-License-Identifier: UNLICENSED

pragma solidity 0.6.12;

contract Initializable {

    address private _initializationAdmin;

    event InitializationComplete();

    constructor() public{
        _initializationAdmin = msg.sender;
    }

    modifier onlyInitializationAdmin() {
        require(msg.sender == initializationAdmin(), \"sender is not the initialization admin\");

        _;
    }

    /*
    * External functions
    */

    function initializationAdmin() public view returns (address) {
        return _initializationAdmin;
    }

    function initializationComplete() external onlyInitializationAdmin {
        _initializationAdmin = address(0);
        emit InitializationComplete();
    }

    function isInitializationComplete() public view returns (bool) {
        return _initializationAdmin == address(0);
    }

}"},"IStakeChangeNotifier.sol":{"content":"// SPDX-License-Identifier: UNLICENSED

pragma solidity 0.6.12;

/// @title An interface for notifying of stake change events (e.g., stake, unstake, partial unstake, restate, etc.).
interface IStakeChangeNotifier {
    /// @dev Notifies of stake change event.
    /// @param _stakeOwner address The address of the subject stake owner.
    /// @param _amount uint256 The difference in the total staked amount.
    /// @param _sign bool The sign of the added (true) or subtracted (false) amount.
    /// @param _updatedStake uint256 The updated total staked amount.
    function stakeChange(address _stakeOwner, uint256 _amount, bool _sign, uint256 _updatedStake) external;

    /// @dev Notifies of multiple stake change events.
    /// @param _stakeOwners address[] The addresses of subject stake owners.
    /// @param _amounts uint256[] The differences in total staked amounts.
    /// @param _signs bool[] The signs of the added (true) or subtracted (false) amounts.
    /// @param _updatedStakes uint256[] The updated total staked amounts.
    function stakeChangeBatch(address[] calldata _stakeOwners, uint256[] calldata _amounts, bool[] calldata _signs,
        uint256[] calldata _updatedStakes) external;

    /// @dev Notifies of stake migration event.
    /// @param _stakeOwner address The address of the subject stake owner.
    /// @param _amount uint256 The migrated amount.
    function stakeMigration(address _stakeOwner, uint256 _amount) external;
}
"},"IStakingContract.sol":{"content":"// SPDX-License-Identifier: UNLICENSED

pragma solidity 0.6.12;

import \"./IMigratableStakingContract.sol\";

/// @title An interface for staking contracts.
interface IStakingContract {
    /// @dev Stakes ORBS tokens on behalf of msg.sender. This method assumes that the user has already approved at least
    /// the required amount using ERC20 approve.
    /// @param _amount uint256 The amount of tokens to stake.
    function stake(uint256 _amount) external;

    /// @dev Unstakes ORBS tokens from msg.sender. If successful, this will start the cooldown period, after which
    /// msg.sender would be able to withdraw all of his tokens.
    /// @param _amount uint256 The amount of tokens to unstake.
    function unstake(uint256 _amount) external;

    /// @dev Requests to withdraw all of staked ORBS tokens back to msg.sender. Stake owners can withdraw their ORBS
    /// tokens only after previously unstaking them and after the cooldown period has passed (unless the contract was
    /// requested to release all stakes).
    function withdraw() external;

    /// @dev Restakes unstaked ORBS tokens (in or after cooldown) for msg.sender.
    function restake() external;

    /// @dev Distributes staking rewards to a list of addresses by directly adding rewards to their stakes. This method
    /// assumes that the user has already approved at least the required amount using ERC20 approve. Since this is a
    /// convenience method, we aren\u0027t concerned about reaching block gas limit by using large lists. We assume that
    /// callers will be able to properly batch/paginate their requests.
    /// @param _totalAmount uint256 The total amount of rewards to distributes.
    /// @param _stakeOwners address[] The addresses of the stake owners.
    /// @param _amounts uint256[] The amounts of the rewards.
    function distributeRewards(uint256 _totalAmount, address[] calldata _stakeOwners, uint256[] calldata _amounts) external;

    /// @dev Returns the stake of the specified stake owner (excluding unstaked tokens).
    /// @param _stakeOwner address The address to check.
    /// @return uint256 The total stake.
    function getStakeBalanceOf(address _stakeOwner) external view returns (uint256);

    /// @dev Returns the total amount staked tokens (excluding unstaked tokens).
    /// @return uint256 The total staked tokens of all stake owners.
    function getTotalStakedTokens() external view returns (uint256);

    /// @dev Returns the time that the cooldown period ends (or ended) and the amount of tokens to be released.
    /// @param _stakeOwner address The address to check.
    /// @return cooldownAmount uint256 The total tokens in cooldown.
    /// @return cooldownEndTime uint256 The time when the cooldown period ends (in seconds).
    function getUnstakeStatus(address _stakeOwner) external view returns (uint256 cooldownAmount,
        uint256 cooldownEndTime);

    /// @dev Migrates the stake of msg.sender from this staking contract to a new approved staking contract.
    /// @param _newStakingContract IMigratableStakingContract The new staking contract which supports stake migration.
    /// @param _amount uint256 The amount of tokens to migrate.
    function migrateStakedTokens(IMigratableStakingContract _newStakingContract, uint256 _amount) external;

    event Staked(address indexed stakeOwner, uint256 amount, uint256 totalStakedAmount);
    event Unstaked(address indexed stakeOwner, uint256 amount, uint256 totalStakedAmount);
    event Withdrew(address indexed stakeOwner, uint256 amount, uint256 totalStakedAmount);
    event Restaked(address indexed stakeOwner, uint256 amount, uint256 totalStakedAmount);
    event MigratedStake(address indexed stakeOwner, uint256 amount, uint256 totalStakedAmount);
}
"},"IStakingContractHandler.sol":{"content":"// SPDX-License-Identifier: UNLICENSED

pragma solidity 0.6.12;

/// @title An interface for staking contracts.
interface IStakingContractHandler {
    event StakeChangeNotificationSkipped(address indexed stakeOwner);
    event StakeChangeBatchNotificationSkipped(address[] stakeOwners);
    event StakeMigrationNotificationSkipped(address indexed stakeOwner);

    /*
    * External functions
    */

    /// @dev Returns the stake of the specified stake owner (excluding unstaked tokens).
    /// @param _stakeOwner address The address to check.
    /// @return uint256 The total stake.
    function getStakeBalanceOf(address _stakeOwner) external view returns (uint256);

    /// @dev Returns the total amount staked tokens (excluding unstaked tokens).
    /// @return uint256 The total staked tokens of all stake owners.
    function getTotalStakedTokens() external view returns (uint256);

    /*
    * Governance functions
    */

    event NotifyDelegationsChanged(bool notifyDelegations);

    function setNotifyDelegations(bool notifyDelegations) external; /* onlyMigrationManager */

    function getNotifyDelegations() external returns (bool);
}
"},"Lockable.sol":{"content":"// SPDX-License-Identifier: UNLICENSED

pragma solidity 0.6.12;

import \"./ContractRegistryAccessor.sol\";
import \"./ILockable.sol\";

contract Lockable is ILockable, ContractRegistryAccessor {

    bool public locked;

    constructor(IContractRegistry _contractRegistry, address _registryAdmin) ContractRegistryAccessor(_contractRegistry, _registryAdmin) public {}

    modifier onlyLockOwner() {
        require(msg.sender == registryAdmin() || msg.sender == address(getContractRegistry()), \"caller is not a lock owner\");

        _;
    }

    function lock() external override onlyLockOwner {
        locked = true;
        emit Locked();
    }

    function unlock() external override onlyLockOwner {
        locked = false;
        emit Unlocked();
    }

    function isLocked() external override view returns (bool) {
        return locked;
    }

    modifier onlyWhenActive() {
        require(!locked, \"contract is locked for this operation\");

        _;
    }
}
"},"ManagedContract.sol":{"content":"// SPDX-License-Identifier: UNLICENSED

pragma solidity 0.6.12;

import \"./Lockable.sol\";

contract ManagedContract is Lockable {

    constructor(IContractRegistry _contractRegistry, address _registryAdmin) Lockable(_contractRegistry, _registryAdmin) public {}

    modifier onlyMigrationManager {
        require(isManager(\"migrationManager\"), \"sender is not the migration manager\");

        _;
    }

    modifier onlyFunctionalManager {
        require(isManager(\"functionalManager\"), \"sender is not the functional manager\");

        _;
    }

    function refreshContracts() virtual external {}

}"},"StakingContractHandler.sol":{"content":"// SPDX-License-Identifier: UNLICENSED

pragma solidity 0.6.12;

import \"./IStakingContractHandler.sol\";
import \"./IStakeChangeNotifier.sol\";
import \"./IStakingContract.sol\";
import \"./ManagedContract.sol\";

contract StakingContractHandler is IStakingContractHandler, IStakeChangeNotifier, ManagedContract {

    bool notifyDelegations = true;

    constructor(IContractRegistry _contractRegistry, address _registryAdmin) public ManagedContract(_contractRegistry, _registryAdmin) {}

    modifier onlyStakingContract() {
        require(msg.sender == address(getStakingContract()), \"caller is not the staking contract\");

        _;
    }

    /*
    * External functions
    */

    function stakeChange(address stakeOwner, uint256 amount, bool sign, uint256 updatedStake) external override onlyStakingContract {
        if (!notifyDelegations) {
            emit StakeChangeNotificationSkipped(stakeOwner);
            return;
        }

        delegationsContract.stakeChange(stakeOwner, amount, sign, updatedStake);
    }

    /// @dev Notifies of multiple stake change events.
    /// @param stakeOwners address[] The addresses of subject stake owners.
    /// @param amounts uint256[] The differences in total staked amounts.
    /// @param signs bool[] The signs of the added (true) or subtracted (false) amounts.
    /// @param updatedStakes uint256[] The updated total staked amounts.
    function stakeChangeBatch(address[] calldata stakeOwners, uint256[] calldata amounts, bool[] calldata signs, uint256[] calldata updatedStakes) external override onlyStakingContract {
        if (!notifyDelegations) {
            emit StakeChangeBatchNotificationSkipped(stakeOwners);
            return;
        }

        delegationsContract.stakeChangeBatch(stakeOwners, amounts, signs, updatedStakes);
    }

    /// @dev Notifies of stake migration event.
    /// @param stakeOwner address The address of the subject stake owner.
    /// @param amount uint256 The migrated amount.
    function stakeMigration(address stakeOwner, uint256 amount) external override onlyStakingContract {
        if (!notifyDelegations) {
            emit StakeMigrationNotificationSkipped(stakeOwner);
            return;
        }

        delegationsContract.stakeMigration(stakeOwner, amount);
    }

    /// @dev Returns the stake of the specified stake owner (excluding unstaked tokens).
    /// @param stakeOwner address The address to check.
    /// @return uint256 The total stake.
    function getStakeBalanceOf(address stakeOwner) external override view returns (uint256) {
        return stakingContract.getStakeBalanceOf(stakeOwner);
    }

    /// @dev Returns the total amount staked tokens (excluding unstaked tokens).
    /// @return uint256 The total staked tokens of all stake owners.
    function getTotalStakedTokens() external override view returns (uint256) {
        return stakingContract.getTotalStakedTokens();
    }

    /*
    * Governance functions
    */

    function setNotifyDelegations(bool _notifyDelegations) external override onlyMigrationManager {
        notifyDelegations = _notifyDelegations;
        emit NotifyDelegationsChanged(_notifyDelegations);
    }

    function getNotifyDelegations() external override returns (bool) {
        return notifyDelegations;
    }

    /*
     * Contracts topology / registry interface
     */

    IStakeChangeNotifier delegationsContract;
    IStakingContract stakingContract;
    function refreshContracts() external override {
        delegationsContract = IStakeChangeNotifier(getDelegationsContract());
        stakingContract = IStakingContract(getStakingContract());
    }
}"},"WithClaimableRegistryManagement.sol":{"content":"// SPDX-License-Identifier: UNLICENSED

pragma solidity 0.6.12;

import \"./Context.sol\";

/**
 * @title Claimable
 * @dev Extension for the Ownable contract, where the ownership needs to be claimed.
 * This allows the new owner to accept the transfer.
 */
contract WithClaimableRegistryManagement is Context {
    address private _registryAdmin;
    address private _pendingRegistryAdmin;

    event RegistryManagementTransferred(address indexed previousRegistryAdmin, address indexed newRegistryAdmin);

    /**
     * @dev Initializes the contract setting the deployer as the initial registryRegistryAdmin.
     */
    constructor () internal {
        address msgSender = _msgSender();
        _registryAdmin = msgSender;
        emit RegistryManagementTransferred(address(0), msgSender);
    }

    /**
     * @dev Returns the address of the current registryAdmin.
     */
    function registryAdmin() public view returns (address) {
        return _registryAdmin;
    }

    /**
     * @dev Throws if called by any account other than the registryAdmin.
     */
    modifier onlyRegistryAdmin() {
        require(isRegistryAdmin(), \"WithClaimableRegistryManagement: caller is not the registryAdmin\");
        _;
    }

    /**
     * @dev Returns true if the caller is the current registryAdmin.
     */
    function isRegistryAdmin() public view returns (bool) {
        return _msgSender() == _registryAdmin;
    }

    /**
     * @dev Leaves the contract without registryAdmin. It will not be possible to call
     * `onlyManager` functions anymore. Can only be called by the current registryAdmin.
     *
     * NOTE: Renouncing registryManagement will leave the contract without an registryAdmin,
     * thereby removing any functionality that is only available to the registryAdmin.
     */
    function renounceRegistryManagement() public onlyRegistryAdmin {
        emit RegistryManagementTransferred(_registryAdmin, address(0));
        _registryAdmin = address(0);
    }

    /**
     * @dev Transfers registryManagement of the contract to a new account (`newManager`).
     */
    function _transferRegistryManagement(address newRegistryAdmin) internal {
        require(newRegistryAdmin != address(0), \"RegistryAdmin: new registryAdmin is the zero address\");
        emit RegistryManagementTransferred(_registryAdmin, newRegistryAdmin);
        _registryAdmin = newRegistryAdmin;
    }

    /**
     * @dev Modifier throws if called by any account other than the pendingManager.
     */
    modifier onlyPendingRegistryAdmin() {
        require(msg.sender == _pendingRegistryAdmin, \"Caller is not the pending registryAdmin\");
        _;
    }
    /**
     * @dev Allows the current registryAdmin to set the pendingManager address.
     * @param newRegistryAdmin The address to transfer registryManagement to.
     */
    function transferRegistryManagement(address newRegistryAdmin) public onlyRegistryAdmin {
        _pendingRegistryAdmin = newRegistryAdmin;
    }

    /**
     * @dev Allows the _pendingRegistryAdmin address to finalize the transfer.
     */
    function claimRegistryManagement() external onlyPendingRegistryAdmin {
        _transferRegistryManagement(_pendingRegistryAdmin);
        _pendingRegistryAdmin = address(0);
    }

    /**
     * @dev Returns the current pendingRegistryAdmin
    */
    function pendingRegistryAdmin() public view returns (address) {
       return _pendingRegistryAdmin;  
    }
}

