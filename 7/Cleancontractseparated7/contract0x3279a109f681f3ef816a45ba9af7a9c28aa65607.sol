// SPDX-License-Identifier: MIT

pragma solidity ^0.6.0;

/**
 * @dev Interface of the ERC20 standard as defined in the EIP.
 */
interface IERC20 {
    /**
     * @dev Returns the amount of tokens in existence.
     */
    function totalSupply() external view returns (uint256);

    /**
     * @dev Returns the amount of tokens owned by `account`.
     */
    function balanceOf(address account) external view returns (uint256);

    /**
     * @dev Moves `amount` tokens from the caller\u0027s account to `recipient`.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transfer(address recipient, uint256 amount) external returns (bool);

    /**
     * @dev Returns the remaining number of tokens that `spender` will be
     * allowed to spend on behalf of `owner` through {transferFrom}. This is
     * zero by default.
     *
     * This value changes when {approve} or {transferFrom} are called.
     */
    function allowance(address owner, address spender) external view returns (uint256);

    /**
     * @dev Sets `amount` as the allowance of `spender` over the caller\u0027s tokens.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * IMPORTANT: Beware that changing an allowance with this method brings the risk
     * that someone may use both the old and the new allowance by unfortunate
     * transaction ordering. One possible solution to mitigate this race
     * condition is to first reduce the spender\u0027s allowance to 0 and set the
     * desired value afterwards:
     * https://github.com/ethereum/EIPs/issues/20#issuecomment-263524729
     *
     * Emits an {Approval} event.
     */
    function approve(address spender, uint256 amount) external returns (bool);

    /**
     * @dev Moves `amount` tokens from `sender` to `recipient` using the
     * allowance mechanism. `amount` is then deducted from the caller\u0027s
     * allowance.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transferFrom(address sender, address recipient, uint256 amount) external returns (bool);

    /**
     * @dev Emitted when `value` tokens are moved from one account (`from`) to
     * another (`to`).
     *
     * Note that `value` may be zero.
     */
    event Transfer(address indexed from, address indexed to, uint256 value);

    /**
     * @dev Emitted when the allowance of a `spender` for an `owner` is set by
     * a call to {approve}. `value` is the new allowance.
     */
    event Approval(address indexed owner, address indexed spender, uint256 value);
}
"},"SafeMath.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.6.0;

/**
 * @dev Wrappers over Solidity\u0027s arithmetic operations with added overflow
 * checks.
 *
 * Arithmetic operations in Solidity wrap on overflow. This can easily result
 * in bugs, because programmers usually assume that an overflow raises an
 * error, which is the standard behavior in high level programming languages.
 * `SafeMath` restores this intuition by reverting the transaction when an
 * operation overflows.
 *
 * Using this library instead of the unchecked operations eliminates an entire
 * class of bugs, so it\u0027s recommended to use it always.
 */
library SafeMath {
    /**
     * @dev Returns the addition of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity\u0027s `+` operator.
     *
     * Requirements:
     *
     * - Addition cannot overflow.
     */
    function add(uint256 a, uint256 b) internal pure returns (uint256) {
        uint256 c = a + b;
        require(c \u003e= a, \"SafeMath: addition overflow\");

        return c;
    }

    /**
     * @dev Returns the subtraction of two unsigned integers, reverting on
     * overflow (when the result is negative).
     *
     * Counterpart to Solidity\u0027s `-` operator.
     *
     * Requirements:
     *
     * - Subtraction cannot overflow.
     */
    function sub(uint256 a, uint256 b) internal pure returns (uint256) {
        return sub(a, b, \"SafeMath: subtraction overflow\");
    }

    /**
     * @dev Returns the subtraction of two unsigned integers, reverting with custom message on
     * overflow (when the result is negative).
     *
     * Counterpart to Solidity\u0027s `-` operator.
     *
     * Requirements:
     *
     * - Subtraction cannot overflow.
     */
    function sub(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b \u003c= a, errorMessage);
        uint256 c = a - b;

        return c;
    }

    /**
     * @dev Returns the multiplication of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity\u0027s `*` operator.
     *
     * Requirements:
     *
     * - Multiplication cannot overflow.
     */
    function mul(uint256 a, uint256 b) internal pure returns (uint256) {
        // Gas optimization: this is cheaper than requiring \u0027a\u0027 not being zero, but the
        // benefit is lost if \u0027b\u0027 is also tested.
        // See: https://github.com/OpenZeppelin/openzeppelin-contracts/pull/522
        if (a == 0) {
            return 0;
        }

        uint256 c = a * b;
        require(c / a == b, \"SafeMath: multiplication overflow\");

        return c;
    }

    /**
     * @dev Returns the integer division of two unsigned integers. Reverts on
     * division by zero. The result is rounded towards zero.
     *
     * Counterpart to Solidity\u0027s `/` operator. Note: this function uses a
     * `revert` opcode (which leaves remaining gas untouched) while Solidity
     * uses an invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function div(uint256 a, uint256 b) internal pure returns (uint256) {
        return div(a, b, \"SafeMath: division by zero\");
    }

    /**
     * @dev Returns the integer division of two unsigned integers. Reverts with custom message on
     * division by zero. The result is rounded towards zero.
     *
     * Counterpart to Solidity\u0027s `/` operator. Note: this function uses a
     * `revert` opcode (which leaves remaining gas untouched) while Solidity
     * uses an invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function div(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b \u003e 0, errorMessage);
        uint256 c = a / b;
        // assert(a == b * c + a % b); // There is no case in which this doesn\u0027t hold

        return c;
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
     * Reverts when dividing by zero.
     *
     * Counterpart to Solidity\u0027s `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function mod(uint256 a, uint256 b) internal pure returns (uint256) {
        return mod(a, b, \"SafeMath: modulo by zero\");
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
     * Reverts with custom message when dividing by zero.
     *
     * Counterpart to Solidity\u0027s `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function mod(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b != 0, errorMessage);
        return a % b;
    }
}
"},"sbController.sol":{"content":"// SPDX-License-Identifier: MIT
pragma solidity ^0.6.12;

import \"./IERC20.sol\";
import \"./SafeMath.sol\";
import \"./sbStrongValuePoolInterface.sol\";

contract sbController {
    using SafeMath for uint256;

    bool public initDone;
    address public admin;
    address public pendingAdmin;
    address public superAdmin;
    address public pendingSuperAdmin;
    address public parameterAdmin;

    IERC20 public strongToken;
    sbStrongValuePoolInterface public sbStrongValuePool;
    address public sbVotes;

    address[] public valuePools;
    mapping(address =\u003e bool) public valuePoolAccepted;
    mapping(address =\u003e uint256[]) public valuePoolDays;
    mapping(address =\u003e uint256[]) public valuePoolWeights;
    mapping(address =\u003e uint256) public valuePoolVestingDays;
    mapping(address =\u003e uint256) public valuePoolMiningFeeNumerator;
    mapping(address =\u003e uint256) public valuePoolMiningFeeDenominator;
    mapping(address =\u003e uint256) public valuePoolUnminingFeeNumerator;
    mapping(address =\u003e uint256) public valuePoolUnminingFeeDenominator;
    mapping(address =\u003e uint256) public valuePoolClaimingFeeNumerator;
    mapping(address =\u003e uint256) public valuePoolClaimingFeeDenominator;

    address[] public servicePools;
    mapping(address =\u003e bool) public servicePoolAccepted;
    mapping(address =\u003e uint256[]) public servicePoolDays;
    mapping(address =\u003e uint256[]) public servicePoolWeights;
    mapping(address =\u003e uint256) public servicePoolVestingDays;
    mapping(address =\u003e uint256) public servicePoolRequestFeeInWei;
    mapping(address =\u003e uint256) public servicePoolClaimingFeeNumerator;
    mapping(address =\u003e uint256) public servicePoolClaimingFeeDenominator;

    uint256 public voteCasterVestingDays;
    uint256 public voteReceiverVestingDays;

    uint256[] public rewardDays;
    uint256[] public rewardAmounts;

    uint256[] public valuePoolsDays;
    uint256[] public valuePoolsWeights;

    uint256[] public servicePoolsDays;
    uint256[] public servicePoolsWeights;

    uint256[] public voteCastersDays;
    uint256[] public voteCastersWeights;

    uint256[] public voteReceiversDays;
    uint256[] public voteReceiversWeights;

    uint256 public voteForServicePoolsCount;
    uint256 public voteForServicesCount;

    uint256 public minerMinMineDays;
    uint256 public minerMinMineAmountInWei;

    uint256 public serviceMinMineDays;
    uint256 public serviceMinMineAmountInWei;

    function init(
        address strongTokenAddress,
        address sbStrongValuePoolAddress,
        address sbVotesAddress,
        address adminAddress,
        address superAdminAddress,
        address parameterAdminAddress
    ) public {
        require(!initDone);
        strongToken = IERC20(strongTokenAddress);
        sbStrongValuePool = sbStrongValuePoolInterface(
            sbStrongValuePoolAddress
        );
        sbVotes = sbVotesAddress;
        admin = adminAddress;
        superAdmin = superAdminAddress;
        parameterAdmin = parameterAdminAddress;
        initDone = true;
    }

    // ADMIN
    // *************************************************************************************
    function removeTokens(address account, uint256 amount) public {
        require(msg.sender == superAdmin, \"not superAdmin\");
        strongToken.transfer(account, amount);
    }

    function updateParameterAdmin(address newParameterAdmin) public {
        require(msg.sender == superAdmin);
        parameterAdmin = newParameterAdmin;
    }

    function setPendingAdmin(address newPendingAdmin) public {
        require(msg.sender == admin);
        pendingAdmin = newPendingAdmin;
    }

    function acceptAdmin() public {
        require(msg.sender == pendingAdmin \u0026\u0026 msg.sender != address(0));
        admin = pendingAdmin;
        pendingAdmin = address(0);
    }

    function setPendingSuperAdmin(address newPendingSuperAdmin) public {
        require(msg.sender == superAdmin);
        pendingSuperAdmin = newPendingSuperAdmin;
    }

    function acceptSuperAdmin() public {
        require(msg.sender == pendingSuperAdmin \u0026\u0026 msg.sender != address(0));
        superAdmin = pendingSuperAdmin;
        pendingSuperAdmin = address(0);
    }

    // VESTING
    // *************************************************************************************
    function getValuePoolVestingDays(address valuePool)
        public
        view
        returns (uint256)
    {
        require(valuePoolAccepted[valuePool]);
        return valuePoolVestingDays[valuePool];
    }

    function getServicePoolVestingDays(address servicePool)
        public
        view
        returns (uint256)
    {
        require(servicePoolAccepted[servicePool]);
        return servicePoolVestingDays[servicePool];
    }

    function getVoteCasterVestingDays() public view returns (uint256) {
        return voteCasterVestingDays;
    }

    function getVoteReceiverVestingDays() public view returns (uint256) {
        return voteReceiverVestingDays;
    }

    function updateVoteCasterVestingDays(uint256 vestingDayCount)
        public
        returns (uint256)
    {
        require(
            msg.sender == admin ||
                msg.sender == parameterAdmin ||
                msg.sender == superAdmin
        );
        require(vestingDayCount \u003e= 1);
        voteCasterVestingDays = vestingDayCount;
    }

    function updateVoteReceiverVestingDays(uint256 vestingDayCount)
        public
        returns (uint256)
    {
        require(
            msg.sender == admin ||
                msg.sender == parameterAdmin ||
                msg.sender == superAdmin
        );
        require(vestingDayCount \u003e= 1);
        voteReceiverVestingDays = vestingDayCount;
    }

    function updateValuePoolVestingDays(
        address valuePool,
        uint256 vestingDayCount
    ) public {
        require(
            msg.sender == admin ||
                msg.sender == parameterAdmin ||
                msg.sender == superAdmin
        );
        require(valuePoolAccepted[valuePool]);
        require(vestingDayCount \u003e= 1);
        valuePoolVestingDays[valuePool] = vestingDayCount;
    }

    function updateServicePoolVestingDays(
        address servicePool,
        uint256 vestingDayCount
    ) public {
        require(
            msg.sender == admin ||
                msg.sender == parameterAdmin ||
                msg.sender == superAdmin
        );
        require(servicePoolAccepted[servicePool]);
        require(vestingDayCount \u003e= 1);
        servicePoolVestingDays[servicePool] = vestingDayCount;
    }

    // MIN MINING
    // *************************************************************************************
    function getMinerMinMineDays() public view returns (uint256) {
        return minerMinMineDays;
    }

    function getServiceMinMineDays() public view returns (uint256) {
        return serviceMinMineDays;
    }

    function getMinerMinMineAmountInWei() public view returns (uint256) {
        return minerMinMineAmountInWei;
    }

    function getServiceMinMineAmountInWei() public view returns (uint256) {
        return serviceMinMineAmountInWei;
    }

    function updateMinerMinMineDays(uint256 dayCount) public {
        require(
            msg.sender == admin ||
                msg.sender == parameterAdmin ||
                msg.sender == superAdmin
        );
        minerMinMineDays = dayCount;
    }

    function updateServiceMinMineDays(uint256 dayCount) public {
        require(
            msg.sender == admin ||
                msg.sender == parameterAdmin ||
                msg.sender == superAdmin
        );
        serviceMinMineDays = dayCount;
    }

    function updateMinerMinMineAmountInWei(uint256 amountInWei) public {
        require(
            msg.sender == admin ||
                msg.sender == parameterAdmin ||
                msg.sender == superAdmin
        );
        minerMinMineAmountInWei = amountInWei;
    }

    function updateServiceMinMineAmountInWei(uint256 amountInWei) public {
        require(
            msg.sender == admin ||
                msg.sender == parameterAdmin ||
                msg.sender == superAdmin
        );
        serviceMinMineAmountInWei = amountInWei;
    }

    // WEIGHTS
    // *************************************************************************************
    function getValuePoolsWeight(uint256 dayNumber)
        public
        view
        returns (uint256)
    {
        uint256 day = dayNumber == 0 ? _getCurrentDay() : dayNumber;
        (, uint256 weight) = _get(valuePoolsDays, valuePoolsWeights, day);
        return weight;
    }

    function getValuePoolWeight(address valuePool, uint256 dayNumber)
        public
        view
        returns (uint256, uint256)
    {
        require(valuePoolAccepted[valuePool], \"invalid valuePool\");
        uint256 day = dayNumber == 0 ? _getCurrentDay() : dayNumber;
        return _get(valuePoolDays[valuePool], valuePoolWeights[valuePool], day);
    }

    function getServicePoolsWeight(uint256 dayNumber)
        public
        view
        returns (uint256)
    {
        uint256 day = dayNumber == 0 ? _getCurrentDay() : dayNumber;
        (, uint256 weight) = _get(servicePoolsDays, servicePoolsWeights, day);
        return weight;
    }

    function getServicePoolWeight(address servicePool, uint256 dayNumber)
        public
        view
        returns (uint256, uint256)
    {
        require(servicePoolAccepted[servicePool], \"invalid servicePool\");
        uint256 day = dayNumber == 0 ? _getCurrentDay() : dayNumber;
        return
            _get(
                servicePoolDays[servicePool],
                servicePoolWeights[servicePool],
                day
            );
    }

    function getVoteCastersWeight(uint256 dayNumber)
        public
        view
        returns (uint256)
    {
        uint256 day = dayNumber == 0 ? _getCurrentDay() : dayNumber;
        (, uint256 weight) = _get(voteCastersDays, voteCastersWeights, day);
        return weight;
    }

    function getVoteReceiversWeight(uint256 dayNumber)
        public
        view
        returns (uint256)
    {
        uint256 day = dayNumber == 0 ? _getCurrentDay() : dayNumber;
        (, uint256 weight) = _get(voteReceiversDays, voteReceiversWeights, day);
        return weight;
    }

    function getValuePoolsSumWeights(uint256 dayNumber)
        public
        view
        returns (uint256)
    {
        uint256 day = dayNumber == 0 ? _getCurrentDay() : dayNumber;
        return _getValuePoolsSumWeights(day);
    }

    function getServicePoolsSumWeights(uint256 dayNumber)
        public
        view
        returns (uint256)
    {
        uint256 day = dayNumber == 0 ? _getCurrentDay() : dayNumber;
        return _getServicePoolsSumWeights(day);
    }

    function getSumWeights(uint256 dayNumber) public view returns (uint256) {
        uint256 day = dayNumber == 0 ? _getCurrentDay() : dayNumber;
        return _getSumWeights(day);
    }

    function updateValuePoolsWeight(uint256 weight, uint256 day) public {
        require(
            msg.sender == admin ||
                msg.sender == parameterAdmin ||
                msg.sender == superAdmin
        );
        _updateWeight(valuePoolsDays, valuePoolsWeights, day, weight);
    }

    function updateServicePoolsWeight(uint256 weight, uint256 day) public {
        require(
            msg.sender == admin ||
                msg.sender == parameterAdmin ||
                msg.sender == superAdmin
        );
        _updateWeight(servicePoolsDays, servicePoolsWeights, day, weight);
    }

    function updateValuePoolWeight(
        address valuePool,
        uint256 weight,
        uint256 day
    ) public {
        require(
            msg.sender == admin ||
                msg.sender == parameterAdmin ||
                msg.sender == superAdmin
        );
        require(valuePoolAccepted[valuePool], \"invalid valuePool\");
        _updateWeight(
            valuePoolDays[valuePool],
            valuePoolWeights[valuePool],
            day,
            weight
        );
    }

    function updateServicePoolWeight(
        address servicePool,
        uint256 weight,
        uint256 day
    ) public {
        require(
            msg.sender == admin ||
                msg.sender == parameterAdmin ||
                msg.sender == superAdmin
        );
        require(servicePoolAccepted[servicePool], \"invalid servicePool\");
        _updateWeight(
            servicePoolDays[servicePool],
            servicePoolWeights[servicePool],
            day,
            weight
        );
    }

    function updateVoteCastersWeight(uint256 weight, uint256 day) public {
        require(
            msg.sender == admin ||
                msg.sender == parameterAdmin ||
                msg.sender == superAdmin
        );
        _updateWeight(voteCastersDays, voteCastersWeights, day, weight);
    }

    function updateVoteReceiversWeight(uint256 weight, uint256 day) public {
        require(
            msg.sender == admin ||
                msg.sender == parameterAdmin ||
                msg.sender == superAdmin
        );
        _updateWeight(voteReceiversDays, voteReceiversWeights, day, weight);
    }

    // FEES
    // *************************************************************************************
    function getValuePoolMiningFee(address valuePool)
        public
        view
        returns (uint256, uint256)
    {
        require(valuePoolAccepted[valuePool], \"invalid valuePool\");
        return (
            valuePoolMiningFeeNumerator[valuePool],
            valuePoolMiningFeeDenominator[valuePool]
        );
    }

    function getValuePoolUnminingFee(address valuePool)
        public
        view
        returns (uint256, uint256)
    {
        require(valuePoolAccepted[valuePool], \"invalid valuePool\");
        return (
            valuePoolUnminingFeeNumerator[valuePool],
            valuePoolUnminingFeeDenominator[valuePool]
        );
    }

    function getValuePoolClaimingFee(address valuePool)
        public
        view
        returns (uint256, uint256)
    {
        require(valuePoolAccepted[valuePool], \"invalid valuePool\");
        return (
            valuePoolClaimingFeeNumerator[valuePool],
            valuePoolClaimingFeeDenominator[valuePool]
        );
    }

    function updateValuePoolMiningFee(
        address valuePool,
        uint256 numerator,
        uint256 denominator
    ) public {
        require(
            msg.sender == admin ||
                msg.sender == parameterAdmin ||
                msg.sender == superAdmin
        );
        require(valuePoolAccepted[valuePool], \"invalid valuePool\");
        require(denominator != 0, \"invalid value\");
        valuePoolMiningFeeNumerator[valuePool] = numerator;
        valuePoolMiningFeeDenominator[valuePool] = denominator;
    }

    function updateValuePoolUnminingFee(
        address valuePool,
        uint256 numerator,
        uint256 denominator
    ) public {
        require(
            msg.sender == admin ||
                msg.sender == parameterAdmin ||
                msg.sender == superAdmin
        );
        require(valuePoolAccepted[valuePool], \"invalid valuePool\");
        require(denominator != 0, \"invalid value\");
        valuePoolUnminingFeeNumerator[valuePool] = numerator;
        valuePoolUnminingFeeDenominator[valuePool] = denominator;
    }

    function updateValuePoolClaimingFee(
        address valuePool,
        uint256 numerator,
        uint256 denominator
    ) public {
        require(
            msg.sender == admin ||
                msg.sender == parameterAdmin ||
                msg.sender == superAdmin
        );
        require(valuePoolAccepted[valuePool], \"invalid valuePool\");
        require(denominator != 0, \"invalid value\");
        valuePoolClaimingFeeNumerator[valuePool] = numerator;
        valuePoolClaimingFeeDenominator[valuePool] = denominator;
    }

    function getServicePoolRequestFeeInWei(address servicePool)
        public
        view
        returns (uint256)
    {
        require(servicePoolAccepted[servicePool], \"invalid servicePool\");
        return servicePoolRequestFeeInWei[servicePool];
    }

    function getServicePoolClaimingFee(address servicePool)
        public
        view
        returns (uint256, uint256)
    {
        require(servicePoolAccepted[servicePool], \"invalid servicePool\");
        return (
            servicePoolClaimingFeeNumerator[servicePool],
            servicePoolClaimingFeeDenominator[servicePool]
        );
    }

    function updateServicePoolRequestFeeInWei(
        address servicePool,
        uint256 feeInWei
    ) public {
        require(
            msg.sender == admin ||
                msg.sender == parameterAdmin ||
                msg.sender == superAdmin
        );
        require(servicePoolAccepted[servicePool], \"invalid servicePool\");
        servicePoolRequestFeeInWei[servicePool] = feeInWei;
    }

    function updateServicePoolClaimingFee(
        address servicePool,
        uint256 numerator,
        uint256 denominator
    ) public {
        require(
            msg.sender == admin ||
                msg.sender == parameterAdmin ||
                msg.sender == superAdmin
        );
        require(servicePoolAccepted[servicePool], \"invalid servicePool\");
        require(denominator != 0, \"invalid value\");
        servicePoolClaimingFeeNumerator[servicePool] = numerator;
        servicePoolClaimingFeeDenominator[servicePool] = denominator;
    }

    // REWARDS
    // *************************************************************************************
    function getRewards(uint256 dayNumber) public view returns (uint256) {
        uint256 day = dayNumber == 0 ? _getCurrentDay() : dayNumber;
        (, uint256 rewards) = _get(rewardDays, rewardAmounts, day);
        return rewards;
    }

    function getValuePoolsRewards(uint256 dayNumber)
        public
        view
        returns (uint256)
    {
        uint256 day = dayNumber == 0 ? _getCurrentDay() : dayNumber;
        (, uint256 reward) = _get(rewardDays, rewardAmounts, day);
        uint256 weight = getValuePoolsWeight(day);
        uint256 sumWeight = _getSumWeights(day);
        return sumWeight == 0 ? 0 : reward.mul(weight).div(sumWeight);
    }

    function getServicePoolsRewards(uint256 dayNumber)
        public
        view
        returns (uint256)
    {
        uint256 day = dayNumber == 0 ? _getCurrentDay() : dayNumber;
        (, uint256 reward) = _get(rewardDays, rewardAmounts, day);
        uint256 weight = getServicePoolsWeight(day);
        uint256 sumWeight = _getSumWeights(day);
        return sumWeight == 0 ? 0 : reward.mul(weight).div(sumWeight);
    }

    function getVoteCastersRewards(uint256 dayNumber)
        public
        view
        returns (uint256)
    {
        uint256 day = dayNumber == 0 ? _getCurrentDay() : dayNumber;
        (, uint256 reward) = _get(rewardDays, rewardAmounts, day);
        uint256 weight = getVoteCastersWeight(day);
        uint256 sumWeight = _getSumWeights(day);
        return sumWeight == 0 ? 0 : reward.mul(weight).div(sumWeight);
    }

    function getVoteReceiversRewards(uint256 dayNumber)
        public
        view
        returns (uint256)
    {
        uint256 day = dayNumber == 0 ? _getCurrentDay() : dayNumber;
        (, uint256 reward) = _get(rewardDays, rewardAmounts, day);
        uint256 weight = getVoteReceiversWeight(day);
        uint256 sumWeight = _getSumWeights(day);
        return sumWeight == 0 ? 0 : reward.mul(weight).div(sumWeight);
    }

    function getValuePoolRewards(address valuePool, uint256 dayNumber)
        public
        view
        returns (uint256)
    {
        require(valuePoolAccepted[valuePool], \"invalid valuePool\");
        uint256 day = dayNumber == 0 ? _getCurrentDay() : dayNumber;
        uint256 reward = getValuePoolsRewards(day);
        (, uint256 weight) = _get(
            valuePoolDays[valuePool],
            valuePoolWeights[valuePool],
            day
        );
        uint256 sumWeights = _getValuePoolsSumWeights(day);
        return sumWeights == 0 ? 0 : reward.mul(weight).div(sumWeights);
    }

    function getServicePoolRewards(address servicePool, uint256 dayNumber)
        public
        view
        returns (uint256)
    {
        require(servicePoolAccepted[servicePool], \"invalid servicePool\");
        uint256 day = dayNumber == 0 ? _getCurrentDay() : dayNumber;
        uint256 reward = getServicePoolsRewards(day);
        (, uint256 weight) = _get(
            servicePoolDays[servicePool],
            servicePoolWeights[servicePool],
            day
        );
        uint256 sumWeights = _getServicePoolsSumWeights(day);
        return sumWeights == 0 ? 0 : reward.mul(weight).div(sumWeights);
    }

    function addReward(uint256 amount, uint256 day) public {
        require(
            msg.sender == admin ||
                msg.sender == parameterAdmin ||
                msg.sender == superAdmin
        );
        if (rewardDays.length == 0) {
            require(day == _getCurrentDay(), \"1: invalid day\");
        } else {
            uint256 lastIndex = rewardDays.length.sub(1);
            uint256 lastDay = rewardDays[lastIndex];
            require(day != lastDay, \"2: invalid day\");
            require(day \u003e= _getCurrentDay(), \"3: invalid day\");
        }
        rewardDays.push(day);
        rewardAmounts.push(amount);
    }

    function updateReward(uint256 amount, uint256 day) public {
        require(
            msg.sender == admin ||
                msg.sender == parameterAdmin ||
                msg.sender == superAdmin
        );
        require(rewardDays.length != 0, \"zero\");
        require(day \u003e= _getCurrentDay(), \"1: invalid day\");
        (bool found, uint256 index) = _findIndex(rewardDays, day);
        require(found, \"2: invalid day\");
        rewardAmounts[index] = amount;
    }

    function requestRewards(address miner, uint256 amount) public {
        require(
            valuePoolAccepted[msg.sender] ||
                servicePoolAccepted[msg.sender] ||
                msg.sender == sbVotes,
            \"invalid caller\"
        );
        strongToken.approve(address(sbStrongValuePool), amount);
        sbStrongValuePool.mineFor(miner, amount);
    }

    // VALUE POOLS
    // *************************************************************************************
    function isValuePoolAccepted(address valuePool) public view returns (bool) {
        return valuePoolAccepted[valuePool];
    }

    function getValuePools() public view returns (address[] memory) {
        return valuePools;
    }

    function addValuePool(address valuePool) public {
        require(msg.sender == admin || msg.sender == superAdmin);
        require(!valuePoolAccepted[valuePool], \"exists\");
        valuePoolAccepted[valuePool] = true;
        valuePools.push(valuePool);
    }

    // SERVICE POOLS
    // *************************************************************************************
    function isServicePoolAccepted(address servicePool)
        public
        view
        returns (bool)
    {
        return servicePoolAccepted[servicePool];
    }

    function getServicePools() public view returns (address[] memory) {
        return servicePools;
    }

    function addServicePool(address servicePool) public {
        require(msg.sender == admin || msg.sender == superAdmin);
        require(!servicePoolAccepted[servicePool], \"exists\");
        servicePoolAccepted[servicePool] = true;
        servicePools.push(servicePool);
    }

    // VOTES
    // *************************************************************************************
    function getVoteForServicePoolsCount() public view returns (uint256) {
        return voteForServicePoolsCount;
    }

    function getVoteForServicesCount() public view returns (uint256) {
        return voteForServicesCount;
    }

    function updateVoteForServicePoolsCount(uint256 count) public {
        require(
            msg.sender == admin ||
                msg.sender == parameterAdmin ||
                msg.sender == superAdmin
        );
        voteForServicePoolsCount = count;
    }

    function updateVoteForServicesCount(uint256 count) public {
        require(
            msg.sender == admin ||
                msg.sender == parameterAdmin ||
                msg.sender == superAdmin
        );
        voteForServicesCount = count;
    }

    // SUPPORT
    // *************************************************************************************
    function getCurrentDay() public view returns (uint256) {
        return _getCurrentDay();
    }

    function _getCurrentDay() internal view returns (uint256) {
        return block.timestamp.div(1 days).add(1);
    }

    function _getValuePoolsSumWeights(uint256 day)
        internal
        view
        returns (uint256)
    {
        uint256 sum;
        for (uint256 i = 0; i \u003c valuePools.length; i++) {
            address valuePool = valuePools[i];
            (, uint256 weight) = _get(
                valuePoolDays[valuePool],
                valuePoolWeights[valuePool],
                day
            );
            sum = sum.add(weight);
        }
        return sum;
    }

    function _getServicePoolsSumWeights(uint256 day)
        internal
        view
        returns (uint256)
    {
        uint256 sum;
        for (uint256 i = 0; i \u003c servicePools.length; i++) {
            address servicePool = servicePools[i];
            (, uint256 weight) = _get(
                servicePoolDays[servicePool],
                servicePoolWeights[servicePool],
                day
            );
            sum = sum.add(weight);
        }
        return sum;
    }

    function _getSumWeights(uint256 day) internal view returns (uint256) {
        (, uint256 vpWeight) = _get(valuePoolsDays, valuePoolsWeights, day);
        (, uint256 spWeight) = _get(servicePoolsDays, servicePoolsWeights, day);
        (, uint256 vcWeight) = _get(voteCastersDays, voteCastersWeights, day);
        (, uint256 vrWeight) = _get(
            voteReceiversDays,
            voteReceiversWeights,
            day
        );
        return vpWeight.add(spWeight).add(vcWeight).add(vrWeight);
    }

    function _get(
        uint256[] memory _Days,
        uint256[] memory _Units,
        uint256 day
    ) internal pure returns (uint256, uint256) {
        uint256 len = _Days.length;
        if (len == 0) {
            return (day, 0);
        }
        if (day \u003c _Days[0]) {
            return (day, 0);
        }
        uint256 lastIndex = len.sub(1);
        uint256 lastDay = _Days[lastIndex];
        if (day \u003e= lastDay) {
            return (day, _Units[lastIndex]);
        }
        return _find(_Days, _Units, day);
    }

    function _find(
        uint256[] memory _Days,
        uint256[] memory _Units,
        uint256 day
    ) internal pure returns (uint256, uint256) {
        uint256 left = 0;
        uint256 right = _Days.length.sub(1);
        uint256 middle = right.add(left).div(2);
        while (left \u003c right) {
            if (_Days[middle] == day) {
                return (day, _Units[middle]);
            } else if (_Days[middle] \u003e day) {
                if (middle \u003e 0 \u0026\u0026 _Days[middle.sub(1)] \u003c day) {
                    return (day, _Units[middle.sub(1)]);
                }
                if (middle == 0) {
                    return (day, 0);
                }
                right = middle.sub(1);
            } else if (_Days[middle] \u003c day) {
                if (
                    middle \u003c _Days.length.sub(1) \u0026\u0026 _Days[middle.add(1)] \u003e day
                ) {
                    return (day, _Units[middle]);
                }
                left = middle.add(1);
            }
            middle = right.add(left).div(2);
        }
        if (_Days[middle] != day) {
            return (day, 0);
        } else {
            return (day, _Units[middle]);
        }
    }

    function _findIndex(uint256[] memory _Array, uint256 element)
        internal
        pure
        returns (bool, uint256)
    {
        uint256 left = 0;
        uint256 right = _Array.length.sub(1);
        while (left \u003c= right) {
            uint256 middle = right.add(left).div(2);
            if (_Array[middle] == element) {
                return (true, middle);
            } else if (_Array[middle] \u003e element) {
                right = middle.sub(1);
            } else if (_Array[middle] \u003c element) {
                left = middle.add(1);
            }
        }
        return (false, 0);
    }

    function _updateWeight(
        uint256[] storage _Days,
        uint256[] storage _Weights,
        uint256 day,
        uint256 weight
    ) internal {
        uint256 currentDay = _getCurrentDay();
        if (_Days.length == 0) {
            require(day == currentDay, \"1: invalid day\");
            _Days.push(day);
            _Weights.push(weight);
        } else {
            require(day \u003e= currentDay, \"2: invalid day\");
            (bool found, uint256 index) = _findIndex(_Days, day);
            if (found) {
                _Weights[index] = weight;
            } else {
                _Days.push(day);
                _Weights.push(weight);
            }
        }
    }
}
"},"sbStrongValuePoolInterface.sol":{"content":"// SPDX-License-Identifier: MIT
pragma solidity ^0.6.12;

interface sbStrongValuePoolInterface {
  function mineFor(address miner, uint256 amount) external;

  function getMinerMineData(address miner, uint256 day)
    external
    view
    returns (
      uint256,
      uint256,
      uint256
    );

  function getMineData(uint256 day)
    external
    view
    returns (
      uint256,
      uint256,
      uint256
    );

  function serviceMinMined(address miner) external view returns (bool);

  function minerMinMined(address miner) external view returns (bool);
}

