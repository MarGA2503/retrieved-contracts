// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

/**
 * @dev Collection of functions related to the address type
 */
library Address {
    /**
     * @dev Returns true if `account` is a contract.
     *
     * [IMPORTANT]
     * ====
     * It is unsafe to assume that an address for which this function returns
     * false is an externally-owned account (EOA) and not a contract.
     *
     * Among others, `isContract` will return false for the following
     * types of addresses:
     *
     *  - an externally-owned account
     *  - a contract in construction
     *  - an address where a contract will be created
     *  - an address where a contract lived, but was destroyed
     * ====
     */
    function isContract(address account) internal view returns (bool) {
        // This method relies on extcodesize, which returns 0 for contracts in
        // construction, since the code is only stored at the end of the
        // constructor execution.

        uint256 size;
        assembly {
            size := extcodesize(account)
        }
        return size \u003e 0;
    }

    /**
     * @dev Replacement for Solidity\u0027s `transfer`: sends `amount` wei to
     * `recipient`, forwarding all available gas and reverting on errors.
     *
     * https://eips.ethereum.org/EIPS/eip-1884[EIP1884] increases the gas cost
     * of certain opcodes, possibly making contracts go over the 2300 gas limit
     * imposed by `transfer`, making them unable to receive funds via
     * `transfer`. {sendValue} removes this limitation.
     *
     * https://diligence.consensys.net/posts/2019/09/stop-using-soliditys-transfer-now/[Learn more].
     *
     * IMPORTANT: because control is transferred to `recipient`, care must be
     * taken to not create reentrancy vulnerabilities. Consider using
     * {ReentrancyGuard} or the
     * https://solidity.readthedocs.io/en/v0.5.11/security-considerations.html#use-the-checks-effects-interactions-pattern[checks-effects-interactions pattern].
     */
    function sendValue(address payable recipient, uint256 amount) internal {
        require(address(this).balance \u003e= amount, \"Address: insufficient balance\");

        (bool success, ) = recipient.call{value: amount}(\"\");
        require(success, \"Address: unable to send value, recipient may have reverted\");
    }

    /**
     * @dev Performs a Solidity function call using a low level `call`. A
     * plain `call` is an unsafe replacement for a function call: use this
     * function instead.
     *
     * If `target` reverts with a revert reason, it is bubbled up by this
     * function (like regular Solidity function calls).
     *
     * Returns the raw returned data. To convert to the expected return value,
     * use https://solidity.readthedocs.io/en/latest/units-and-global-variables.html?highlight=abi.decode#abi-encoding-and-decoding-functions[`abi.decode`].
     *
     * Requirements:
     *
     * - `target` must be a contract.
     * - calling `target` with `data` must not revert.
     *
     * _Available since v3.1._
     */
    function functionCall(address target, bytes memory data) internal returns (bytes memory) {
        return functionCall(target, data, \"Address: low-level call failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`], but with
     * `errorMessage` as a fallback revert reason when `target` reverts.
     *
     * _Available since v3.1._
     */
    function functionCall(
        address target,
        bytes memory data,
        string memory errorMessage
    ) internal returns (bytes memory) {
        return functionCallWithValue(target, data, 0, errorMessage);
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`],
     * but also transferring `value` wei to `target`.
     *
     * Requirements:
     *
     * - the calling contract must have an ETH balance of at least `value`.
     * - the called Solidity function must be `payable`.
     *
     * _Available since v3.1._
     */
    function functionCallWithValue(
        address target,
        bytes memory data,
        uint256 value
    ) internal returns (bytes memory) {
        return functionCallWithValue(target, data, value, \"Address: low-level call with value failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCallWithValue-address-bytes-uint256-}[`functionCallWithValue`], but
     * with `errorMessage` as a fallback revert reason when `target` reverts.
     *
     * _Available since v3.1._
     */
    function functionCallWithValue(
        address target,
        bytes memory data,
        uint256 value,
        string memory errorMessage
    ) internal returns (bytes memory) {
        require(address(this).balance \u003e= value, \"Address: insufficient balance for call\");
        require(isContract(target), \"Address: call to non-contract\");

        (bool success, bytes memory returndata) = target.call{value: value}(data);
        return verifyCallResult(success, returndata, errorMessage);
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`],
     * but performing a static call.
     *
     * _Available since v3.3._
     */
    function functionStaticCall(address target, bytes memory data) internal view returns (bytes memory) {
        return functionStaticCall(target, data, \"Address: low-level static call failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-string-}[`functionCall`],
     * but performing a static call.
     *
     * _Available since v3.3._
     */
    function functionStaticCall(
        address target,
        bytes memory data,
        string memory errorMessage
    ) internal view returns (bytes memory) {
        require(isContract(target), \"Address: static call to non-contract\");

        (bool success, bytes memory returndata) = target.staticcall(data);
        return verifyCallResult(success, returndata, errorMessage);
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`],
     * but performing a delegate call.
     *
     * _Available since v3.4._
     */
    function functionDelegateCall(address target, bytes memory data) internal returns (bytes memory) {
        return functionDelegateCall(target, data, \"Address: low-level delegate call failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-string-}[`functionCall`],
     * but performing a delegate call.
     *
     * _Available since v3.4._
     */
    function functionDelegateCall(
        address target,
        bytes memory data,
        string memory errorMessage
    ) internal returns (bytes memory) {
        require(isContract(target), \"Address: delegate call to non-contract\");

        (bool success, bytes memory returndata) = target.delegatecall(data);
        return verifyCallResult(success, returndata, errorMessage);
    }

    /**
     * @dev Tool to verifies that a low level call was successful, and revert if it wasn\u0027t, either by bubbling the
     * revert reason using the provided one.
     *
     * _Available since v4.3._
     */
    function verifyCallResult(
        bool success,
        bytes memory returndata,
        string memory errorMessage
    ) internal pure returns (bytes memory) {
        if (success) {
            return returndata;
        } else {
            // Look for revert reason and bubble it up if present
            if (returndata.length \u003e 0) {
                // The easiest way to bubble the revert reason is using memory via assembly

                assembly {
                    let returndata_size := mload(returndata)
                    revert(add(32, returndata), returndata_size)
                }
            } else {
                revert(errorMessage);
            }
        }
    }
}
"},"Context.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

/**
 * @dev Provides information about the current execution context, including the
 * sender of the transaction and its data. While these are generally available
 * via msg.sender and msg.data, they should not be accessed in such a direct
 * manner, since when dealing with meta-transactions the account sending and
 * paying for execution may not be the actual sender (as far as an application
 * is concerned).
 *
 * This contract is only required for intermediate, library-like contracts.
 */
abstract contract Context {
    function _msgSender() internal view virtual returns (address) {
        return msg.sender;
    }

    function _msgData() internal view virtual returns (bytes calldata) {
        return msg.data;
    }
}
"},"IDEXFactory.sol":{"content":"pragma solidity ^0.8.9;

// SPDX-License-Identifier: MIT

interface IDEXFactory {
\tevent PairCreated(address indexed token0, address indexed token1, address pair, uint);

\tfunction feeTo() external view returns (address);

\tfunction feeToSetter() external view returns (address);

\tfunction getPair(address tokenA, address tokenB) external view returns (address pair);

\tfunction allPairs(uint) external view returns (address pair);

\tfunction allPairsLength() external view returns (uint);

\tfunction createPair(address tokenA, address tokenB) external returns (address pair);

\tfunction setFeeTo(address) external;

\tfunction setFeeToSetter(address) external;
}"},"IDEXRouter.sol":{"content":"pragma solidity ^0.8.9;

// SPDX-License-Identifier: MIT

interface IDEXRouter {
\tfunction factory() external pure returns (address);

\tfunction WETH() external pure returns (address);

\tfunction addLiquidity(
\t\taddress tokenA,
\t\taddress tokenB,
\t\tuint amountADesired,
\t\tuint amountBDesired,
\t\tuint amountAMin,
\t\tuint amountBMin,
\t\taddress to,
\t\tuint deadline
\t) external returns (uint amountA, uint amountB, uint liquidity);

\tfunction addLiquidityETH(
\t\taddress token,
\t\tuint amountTokenDesired,
\t\tuint amountTokenMin,
\t\tuint amountETHMin,
\t\taddress to,
\t\tuint deadline
\t) external payable returns (uint amountToken, uint amountETH, uint liquidity);

\tfunction removeLiquidity(
\t\taddress tokenA,
\t\taddress tokenB,
\t\tuint liquidity,
\t\tuint amountAMin,
\t\tuint amountBMin,
\t\taddress to,
\t\tuint deadline
\t) external returns (uint amountA, uint amountB);

\tfunction removeLiquidityETH(
\t\taddress token,
\t\tuint liquidity,
\t\tuint amountTokenMin,
\t\tuint amountETHMin,
\t\taddress to,
\t\tuint deadline
\t) external returns (uint amountToken, uint amountETH);

\tfunction removeLiquidityWithPermit(
\t\taddress tokenA,
\t\taddress tokenB,
\t\tuint liquidity,
\t\tuint amountAMin,
\t\tuint amountBMin,
\t\taddress to,
\t\tuint deadline,
\t\tbool approveMax, uint8 v, bytes32 r, bytes32 s
\t) external returns (uint amountA, uint amountB);

\tfunction removeLiquidityETHWithPermit(
\t\taddress token,
\t\tuint liquidity,
\t\tuint amountTokenMin,
\t\tuint amountETHMin,
\t\taddress to,
\t\tuint deadline,
\t\tbool approveMax, uint8 v, bytes32 r, bytes32 s
\t) external returns (uint amountToken, uint amountETH);

\tfunction swapExactTokensForTokens(
\t\tuint amountIn,
\t\tuint amountOutMin,
\t\taddress[] calldata path,
\t\taddress to,
\t\tuint deadline
\t) external returns (uint[] memory amounts);

\tfunction swapTokensForExactTokens(
\t\tuint amountOut,
\t\tuint amountInMax,
\t\taddress[] calldata path,
\t\taddress to,
\t\tuint deadline
\t) external returns (uint[] memory amounts);

\tfunction swapExactETHForTokens(uint amountOutMin, address[] calldata path, address to, uint deadline)
\texternal
\tpayable
\treturns (uint[] memory amounts);

\tfunction swapTokensForExactETH(uint amountOut, uint amountInMax, address[] calldata path, address to, uint deadline)
\texternal
\treturns (uint[] memory amounts);

\tfunction swapExactTokensForETH(uint amountIn, uint amountOutMin, address[] calldata path, address to, uint deadline)
\texternal
\treturns (uint[] memory amounts);

\tfunction swapETHForExactTokens(uint amountOut, address[] calldata path, address to, uint deadline)
\texternal
\tpayable
\treturns (uint[] memory amounts);

\tfunction quote(uint amountA, uint reserveA, uint reserveB) external pure returns (uint amountB);

\tfunction getAmountOut(uint amountIn, uint reserveIn, uint reserveOut) external pure returns (uint amountOut);

\tfunction getAmountIn(uint amountOut, uint reserveIn, uint reserveOut) external pure returns (uint amountIn);

\tfunction getAmountsOut(uint amountIn, address[] calldata path) external view returns (uint[] memory amounts);

\tfunction getAmountsIn(uint amountOut, address[] calldata path) external view returns (uint[] memory amounts);

\tfunction removeLiquidityETHSupportingFeeOnTransferTokens(
\t\taddress token,
\t\tuint liquidity,
\t\tuint amountTokenMin,
\t\tuint amountETHMin,
\t\taddress to,
\t\tuint deadline
\t) external returns (uint amountETH);

\tfunction removeLiquidityETHWithPermitSupportingFeeOnTransferTokens(
\t\taddress token,
\t\tuint liquidity,
\t\tuint amountTokenMin,
\t\tuint amountETHMin,
\t\taddress to,
\t\tuint deadline,
\t\tbool approveMax, uint8 v, bytes32 r, bytes32 s
\t) external returns (uint amountETH);

\tfunction swapExactTokensForTokensSupportingFeeOnTransferTokens(
\t\tuint amountIn,
\t\tuint amountOutMin,
\t\taddress[] calldata path,
\t\taddress to,
\t\tuint deadline
\t) external;

\tfunction swapExactETHForTokensSupportingFeeOnTransferTokens(
\t\tuint amountOutMin,
\t\taddress[] calldata path,
\t\taddress to,
\t\tuint deadline
\t) external payable;

\tfunction swapExactTokensForETHSupportingFeeOnTransferTokens(
\t\tuint amountIn,
\t\tuint amountOutMin,
\t\taddress[] calldata path,
\t\taddress to,
\t\tuint deadline
\t) external;
}"},"IERC20.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

/**
 * @dev Interface of the ERC20 standard as defined in the EIP.
 */
interface IERC20 {
    /**
     * @dev Returns the amount of tokens in existence.
     */
    function totalSupply() external view returns (uint256);

    /**
     * @dev Returns the amount of tokens owned by `account`.
     */
    function balanceOf(address account) external view returns (uint256);

    /**
     * @dev Moves `amount` tokens from the caller\u0027s account to `recipient`.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transfer(address recipient, uint256 amount) external returns (bool);

    /**
     * @dev Returns the remaining number of tokens that `spender` will be
     * allowed to spend on behalf of `owner` through {transferFrom}. This is
     * zero by default.
     *
     * This value changes when {approve} or {transferFrom} are called.
     */
    function allowance(address owner, address spender) external view returns (uint256);

    /**
     * @dev Sets `amount` as the allowance of `spender` over the caller\u0027s tokens.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * IMPORTANT: Beware that changing an allowance with this method brings the risk
     * that someone may use both the old and the new allowance by unfortunate
     * transaction ordering. One possible solution to mitigate this race
     * condition is to first reduce the spender\u0027s allowance to 0 and set the
     * desired value afterwards:
     * https://github.com/ethereum/EIPs/issues/20#issuecomment-263524729
     *
     * Emits an {Approval} event.
     */
    function approve(address spender, uint256 amount) external returns (bool);

    /**
     * @dev Moves `amount` tokens from `sender` to `recipient` using the
     * allowance mechanism. `amount` is then deducted from the caller\u0027s
     * allowance.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transferFrom(
        address sender,
        address recipient,
        uint256 amount
    ) external returns (bool);

    /**
     * @dev Emitted when `value` tokens are moved from one account (`from`) to
     * another (`to`).
     *
     * Note that `value` may be zero.
     */
    event Transfer(address indexed from, address indexed to, uint256 value);

    /**
     * @dev Emitted when the allowance of a `spender` for an `owner` is set by
     * a call to {approve}. `value` is the new allowance.
     */
    event Approval(address indexed owner, address indexed spender, uint256 value);
}
"},"Ownable.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import \"./Context.sol\";

/**
 * @dev Contract module which provides a basic access control mechanism, where
 * there is an account (an owner) that can be granted exclusive access to
 * specific functions.
 *
 * By default, the owner account will be the one that deploys the contract. This
 * can later be changed with {transferOwnership}.
 *
 * This module is used through inheritance. It will make available the modifier
 * `onlyOwner`, which can be applied to your functions to restrict their use to
 * the owner.
 */
abstract contract Ownable is Context {
    address private _owner;

    event OwnershipTransferred(address indexed previousOwner, address indexed newOwner);

    /**
     * @dev Initializes the contract setting the deployer as the initial owner.
     */
    constructor() {
        _setOwner(_msgSender());
    }

    /**
     * @dev Returns the address of the current owner.
     */
    function owner() public view virtual returns (address) {
        return _owner;
    }

    /**
     * @dev Throws if called by any account other than the owner.
     */
    modifier onlyOwner() {
        require(owner() == _msgSender(), \"Ownable: caller is not the owner\");
        _;
    }

    /**
     * @dev Leaves the contract without owner. It will not be possible to call
     * `onlyOwner` functions anymore. Can only be called by the current owner.
     *
     * NOTE: Renouncing ownership will leave the contract without an owner,
     * thereby removing any functionality that is only available to the owner.
     */
    function renounceOwnership() public virtual onlyOwner {
        _setOwner(address(0));
    }

    /**
     * @dev Transfers ownership of the contract to a new account (`newOwner`).
     * Can only be called by the current owner.
     */
    function transferOwnership(address newOwner) public virtual onlyOwner {
        require(newOwner != address(0), \"Ownable: new owner is the zero address\");
        _setOwner(newOwner);
    }

    function _setOwner(address newOwner) private {
        address oldOwner = _owner;
        _owner = newOwner;
        emit OwnershipTransferred(oldOwner, newOwner);
    }
}
"},"Shibamon.sol":{"content":"pragma solidity 0.8.9;

// SPDX-License-Identifier: MIT

import \"./IERC20.sol\";
import \"./Ownable.sol\";
import \"./Address.sol\";
import \"./Context.sol\";
import \"./IDEXRouter.sol\";
import \"./IDEXFactory.sol\";

contract Shibamon is Context, IERC20, Ownable {
\tusing Address for address payable;

\tstring constant NAME = \"Shibamon\";
\tstring constant SYMBOL = \"SHIBAMON\";
\tuint8 constant DECIMALS = 9;

\tuint256 constant MAX_UINT = 2 ** 256 - 1;
\taddress constant ROUTER_ADDRESS = address(0x7a250d5630B4cF539739dF2C5dAcb4c659F2488D);
\taddress constant ZERO_ADDRESS = address(0);
\taddress constant DEAD_ADDRESS = address(57005);

\tmapping(address =\u003e uint256) rOwned;
\tmapping(address =\u003e uint256) tOwned;

\tmapping(address =\u003e mapping(address =\u003e uint256)) allowances;

\tmapping(address =\u003e bool) public isExcludedFromFees;
\tmapping(address =\u003e bool) public isExcludedFromRewards;
\tmapping(address =\u003e bool) public isExcludedFromMaxWallet;
\taddress[] excluded;

\tmapping(address =\u003e bool) public isBot;

\tuint256 tTotal = 10 ** 12 * 10 ** DECIMALS;
\tuint256 rTotal = (MAX_UINT - (MAX_UINT % tTotal));

\tuint256 public maxTxAmountBuy = tTotal / 200;
\tuint256 public maxTxAmountSell = tTotal / 200;
\tuint256 public maxWalletAmount = tTotal / 100;

\tuint256 launchedAt;

\taddress payable marketingAddress;

\tmapping(address =\u003e bool) automatedMarketMakerPairs;

\tbool areFeesBeingProcessed;
\tbool public isFeeProcessingEnabled = false;
\tuint256 public feeProcessingThreshold = tTotal / 500;

\tIDEXRouter router;

\tbool isTradingOpen;

\tstruct FeeSet {
\t\tuint256 reflectFee;
\t\tuint256 marketingFee;
\t\tuint256 liquidityFee;
\t}

\tFeeSet public fees = FeeSet({
\t\treflectFee: 3,
\t\tmarketingFee: 5,
\t\tliquidityFee: 2
\t});

\tstruct ReflectValueSet {
\t\tuint256 rAmount;
\t\tuint256 rTransferAmount;
\t\tuint256 rReflectFee;
\t\tuint256 rOtherFee;
\t\tuint256 tTransferAmount;
\t\tuint256 tReflectFee;
\t\tuint256 tOtherFee;
\t}

\tmodifier lockTheSwap {
\t\tareFeesBeingProcessed = true;
\t\t_;
\t\tareFeesBeingProcessed = false;
\t}

\tconstructor() {
\t\taddress self = address(this);

\t\trOwned[owner()] = rTotal;

\t\trouter = IDEXRouter(ROUTER_ADDRESS);

\t\tmarketingAddress = payable(msg.sender);

\t\tisExcludedFromFees[owner()] = true;
\t\tisExcludedFromFees[marketingAddress] = true;
\t\tisExcludedFromFees[self] = true;
\t\tisExcludedFromFees[DEAD_ADDRESS] = true;

\t\tisExcludedFromMaxWallet[owner()] = true;
\t\tisExcludedFromMaxWallet[marketingAddress] = true;
\t\tisExcludedFromMaxWallet[self] = true;
\t\tisExcludedFromMaxWallet[DEAD_ADDRESS] = true;

\t\temit Transfer(ZERO_ADDRESS, owner(), tTotal);
\t}

\tfunction name() public pure returns (string memory) {
\t\treturn NAME;
\t}

\tfunction symbol() public pure returns (string memory) {
\t\treturn SYMBOL;
\t}

\tfunction decimals() public pure returns (uint8) {
\t\treturn DECIMALS;
\t}

\tfunction totalSupply() public view override returns (uint256) {
\t\treturn tTotal;
\t}

\tfunction balanceOf(address account) public view override returns (uint256) {
\t\tif (isExcludedFromRewards[account]) return tOwned[account];
\t\treturn tokenFromReflection(rOwned[account]);
\t}

\tfunction transfer(address recipient, uint256 amount) public override returns (bool) {
\t\t_transfer(_msgSender(), recipient, amount);
\t\treturn true;
\t}

\tfunction allowance(address owner, address spender) public view override returns (uint256) {
\t\treturn allowances[owner][spender];
\t}

\tfunction approve(address spender, uint256 amount) public override returns (bool) {
\t\t_approve(_msgSender(), spender, amount);
\t\treturn true;
\t}

\tfunction transferFrom(address sender, address recipient, uint256 amount) public override returns (bool) {
\t\t_transfer(sender, recipient, amount);

\t\tuint256 currentAllowance = allowances[sender][_msgSender()];
\t\trequire(currentAllowance \u003e= amount, \"ERC20: transfer amount exceeds allowance\");

\t\tunchecked {
\t\t\t_approve(sender, _msgSender(), currentAllowance - amount);
\t\t}

\t\treturn true;
\t}

\tfunction increaseAllowance(address spender, uint256 addedValue) public virtual returns (bool) {
\t\t_approve(_msgSender(), spender, allowances[_msgSender()][spender] + addedValue);
\t\treturn true;
\t}

\tfunction decreaseAllowance(address spender, uint256 subtractedValue) public virtual returns (bool) {
\t\tuint256 currentAllowance = allowances[_msgSender()][spender];
\t\trequire(currentAllowance \u003e= subtractedValue, \"ERC20: decreased allowance below zero\");

\t\tunchecked {
\t\t\t_approve(_msgSender(), spender, currentAllowance - subtractedValue);
\t\t}

\t\treturn true;
\t}

\tfunction tokenFromReflection(uint256 rAmount) public view returns (uint256) {
\t\trequire(rAmount \u003c= rTotal, \"Amount must be less than total reflections\");
\t\tuint256 currentRate = _getRate();
\t\treturn rAmount / currentRate;
\t}

\tfunction excludeFromRewards(address account) external onlyOwner {
\t\trequire(!isExcludedFromRewards[account], \"Account is already excluded\");

\t\tif (rOwned[account] \u003e 0) {
\t\t\ttOwned[account] = tokenFromReflection(rOwned[account]);
\t\t}

\t\tisExcludedFromRewards[account] = true;
\t\texcluded.push(account);
\t}

\tfunction includeInRewards(address account) external onlyOwner {
\t\trequire(isExcludedFromRewards[account], \"Account is not excluded\");

\t\tfor (uint256 i = 0; i \u003c excluded.length; i++) {
\t\t\tif (excluded[i] == account) {
\t\t\t\texcluded[i] = excluded[excluded.length - 1];
\t\t\t\ttOwned[account] = 0;
\t\t\t\tisExcludedFromRewards[account] = false;
\t\t\t\texcluded.pop();
\t\t\t\tbreak;
\t\t\t}
\t\t}
\t}

\tfunction _getValues(uint256 tAmount, bool takeFee) private view returns (ReflectValueSet memory set) {
\t\tset = _getTValues(tAmount, takeFee);
\t\t(set.rAmount, set.rTransferAmount, set.rReflectFee, set.rOtherFee) = _getRValues(set, tAmount, takeFee, _getRate());
\t\treturn set;
\t}

\tfunction _getTValues(uint256 tAmount, bool takeFee) private view returns (ReflectValueSet memory set) {
\t\tif (!takeFee) {
\t\t\tset.tTransferAmount = tAmount;
\t\t\treturn set;
\t\t}

\t\tset.tReflectFee = tAmount * fees.reflectFee / 100;
\t\tset.tOtherFee = tAmount * (fees.marketingFee + fees.liquidityFee) / 100;
\t\tset.tTransferAmount = tAmount - set.tReflectFee - set.tOtherFee;

\t\treturn set;
\t}

\tfunction _getRValues(ReflectValueSet memory set, uint256 tAmount, bool takeFee, uint256 currentRate) private pure returns (uint256 rAmount, uint256 rTransferAmount, uint256 rReflectFee, uint256 rOtherFee) {
\t\trAmount = tAmount * currentRate;

\t\tif (!takeFee) {
\t\t\treturn (rAmount, rAmount, 0, 0);
\t\t}

\t\trReflectFee = set.tReflectFee * currentRate;
\t\trOtherFee = set.tOtherFee * currentRate;
\t\trTransferAmount = rAmount - rReflectFee - rOtherFee;
\t\treturn (rAmount, rTransferAmount, rReflectFee, rOtherFee);
\t}

\tfunction _getRate() private view returns (uint256) {
\t\t(uint256 rSupply, uint256 tSupply) = _getCurrentSupply();
\t\treturn rSupply / tSupply;
\t}

\tfunction _getCurrentSupply() private view returns (uint256, uint256) {
\t\tuint256 rSupply = rTotal;
\t\tuint256 tSupply = tTotal;

\t\tfor (uint256 i = 0; i \u003c excluded.length; i++) {
\t\t\tif (rOwned[excluded[i]] \u003e rSupply || tOwned[excluded[i]] \u003e tSupply) return (rTotal, tTotal);
\t\t\trSupply -= rOwned[excluded[i]];
\t\t\ttSupply -= tOwned[excluded[i]];
\t\t}

\t\tif (rSupply \u003c rTotal / tTotal) return (rTotal, tTotal);
\t\treturn (rSupply, tSupply);
\t}

\tfunction _approve(address owner, address spender, uint256 amount) private {
\t\trequire(owner != ZERO_ADDRESS, \"ERC20: approve from the zero address\");
\t\trequire(spender != ZERO_ADDRESS, \"ERC20: approve to the zero address\");

\t\tallowances[owner][spender] = amount;

\t\temit Approval(owner, spender, amount);
\t}

\tfunction _transfer(address from, address to, uint256 amount) private {
\t\trequire(from != ZERO_ADDRESS, \"ERC20: transfer from the zero address\");
\t\trequire(to != ZERO_ADDRESS, \"ERC20: transfer to the zero address\");
\t\trequire(!isBot[from], \"ERC20: address blacklisted (bot)\");
\t\trequire(amount \u003e 0, \"Transfer amount must be greater than zero\");
\t\trequire(amount \u003c= balanceOf(from), \"You are trying to transfer more than your balance\");

\t\tif (maxWalletAmount \u003e 0 \u0026\u0026 !automatedMarketMakerPairs[to] \u0026\u0026 !isExcludedFromMaxWallet[to]) {
\t\t\trequire((balanceOf(to) + amount) \u003c= maxWalletAmount, \"You are trying to transfer more than the max wallet amount\");
\t\t}

\t\tif (launchedAt == 0 \u0026\u0026 automatedMarketMakerPairs[to]) {
\t\t\tlaunchedAt = block.number;
\t\t}

\t\tbool shouldTakeFees = !isExcludedFromFees[from] \u0026\u0026 !isExcludedFromFees[to];
\t\tif (shouldTakeFees) {
\t\t\trequire(amount \u003c= (automatedMarketMakerPairs[from] ? maxTxAmountBuy : maxTxAmountSell), \"You are trying to transfer too many tokens\");

\t\t\tif (automatedMarketMakerPairs[from] \u0026\u0026 block.number \u003c= launchedAt) {
\t\t\t\tisBot[to] = true;
\t\t\t}
\t\t}

\t\tuint256 balance = balanceOf(address(this));

\t\tif (balance \u003e maxTxAmountSell) {
\t\t\tbalance = maxTxAmountSell;
\t\t}

\t\tif (isFeeProcessingEnabled \u0026\u0026 !areFeesBeingProcessed \u0026\u0026 balance \u003e= feeProcessingThreshold \u0026\u0026 !automatedMarketMakerPairs[from]) {
\t\t\tareFeesBeingProcessed = true;
\t\t\t_processFees(balance);
\t\t\tareFeesBeingProcessed = false;
\t\t}

\t\t_tokenTransfer(from, to, amount, shouldTakeFees);
\t}

\tfunction _takeReflectFees(uint256 rReflectFee) private {
\t\trTotal -= rReflectFee;
\t}

\tfunction _takeOtherFees(uint256 rOtherFee, uint256 tOtherFee) private {
\t\taddress self = address(this);

\t\trOwned[self] += rOtherFee;

\t\tif (isExcludedFromRewards[self]) {
\t\t\ttOwned[self] += tOtherFee;
\t\t}
\t}

\tfunction _tokenTransfer(address sender, address recipient, uint256 tAmount, bool shouldTakeFees) private {
\t\tReflectValueSet memory set = _getValues(tAmount, shouldTakeFees);

\t\tif (isExcludedFromRewards[sender]) {
\t\t\ttOwned[sender] -= tAmount;
\t\t}

\t\tif (isExcludedFromRewards[recipient]) {
\t\t\ttOwned[recipient] += set.tTransferAmount;
\t\t}

\t\trOwned[sender] -= set.rAmount;
\t\trOwned[recipient] += set.rTransferAmount;

\t\tif (shouldTakeFees) {
\t\t\t_takeReflectFees(set.rReflectFee);
\t\t\t_takeOtherFees(set.rOtherFee, set.tOtherFee);
\t\t\temit Transfer(sender, address(this), set.tOtherFee);
\t\t}

\t\temit Transfer(sender, recipient, set.tTransferAmount);
\t}

\tfunction _processFees(uint256 amount) private lockTheSwap {
\t\tuint256 feeSum = fees.marketingFee + fees.liquidityFee;
\t\tif (feeSum == 0) return;

\t\t//Calculate amounts
\t\tuint256 amountForMarketing = amount * fees.marketingFee / feeSum;
\t\tuint256 amountForLiquidity = amount - amountForMarketing;

\t\t//Do processing
\t\t_swapExactTokensForETH(amountForMarketing);
\t\t_addLiquidity(amountForLiquidity);

\t\t//Send marketing funds
\t\tmarketingAddress.transfer(address(this).balance);
\t}

\tfunction _addLiquidity(uint256 amount) private {
\t\taddress self = address(this);

\t\tuint256 tokensToSell = amount / 2;
\t\tuint256 tokensForLiquidity = amount - tokensToSell;

\t\tuint256 ethForLiquidity = _swapExactTokensForETH(tokensToSell);

\t\t_approve(self, address(router), MAX_UINT);
\t\trouter.addLiquidityETH{value: ethForLiquidity}(self, tokensForLiquidity, 0, 0, DEAD_ADDRESS, block.timestamp);
\t}

\tfunction _swapExactTokensForETH(uint256 amountIn) private returns (uint256) {
\t\taddress self = address(this);

\t\taddress[] memory path = new address[](2);
\t\tpath[0] = self;
\t\tpath[1] = router.WETH();

\t\t_approve(self, address(router), MAX_UINT);

\t\tuint256 previousBalance = self.balance;
\t\trouter.swapExactTokensForETHSupportingFeeOnTransferTokens(amountIn, 0, path, self, block.timestamp);
\t\treturn self.balance - previousBalance;
\t}

\tfunction openTrading(uint256 tokensForLiquidity) external payable onlyOwner {
\t\taddress self = address(this);
\t\trequire(!isTradingOpen, \"Trading is already open\");
\t\trequire(balanceOf(self) \u003e= tokensForLiquidity, \"Insufficient token balance for initial liquidity\");
\t\trequire(msg.value \u003e 0, \"Insufficient ETH for initial liquidity\");

\t\t//Create pair
\t\taddress pairAddress = IDEXFactory(router.factory()).createPair(self, router.WETH());
\t\tautomatedMarketMakerPairs[pairAddress] = true;
\t\tisExcludedFromMaxWallet[pairAddress] = true;

\t\t//Add liquidity
\t\t_approve(self, address(router), MAX_UINT);
\t\trouter.addLiquidityETH{value: msg.value}(self, tokensForLiquidity, 0, 0, owner(), block.timestamp);

\t\tisFeeProcessingEnabled = true;
\t\tisTradingOpen = true;
\t}

\tfunction setAutomatedMarketMakerPair(address pair, bool value) external onlyOwner {
\t\trequire(automatedMarketMakerPairs[pair] != value, \"Automated market maker pair is already set to that value\");
\t\tautomatedMarketMakerPairs[pair] = value;

\t\tif (value) {
\t\t\tisExcludedFromMaxWallet[pair] = true;
\t\t}
\t}

\tfunction setFees(uint256 reflectFee, uint256 marketingFee, uint256 liquidityFee) external onlyOwner {
\t\trequire((reflectFee + marketingFee + liquidityFee) \u003c= 20, \"Cannot set fees to above a combined total of 20%\");

\t\tfees = FeeSet({
\t\t\treflectFee: reflectFee,
\t\t\tmarketingFee: marketingFee,
\t\t\tliquidityFee: liquidityFee
\t\t});
\t}

\tfunction setIsFeeProcessingEnabled(bool value) public onlyOwner {
\t\tisFeeProcessingEnabled = value;
\t}

\tfunction setFeeProcessingThreshold(uint256 value) external onlyOwner {
\t\tfeeProcessingThreshold = value;
\t}

\tfunction setMaxTransactionAmounts(uint256 maxBuy, uint256 maxSell) external onlyOwner {
\t\trequire(maxBuy \u003e= (tTotal / 400), \"Must set max buy to at least 0.25% of total supply\");
\t\trequire(maxSell \u003e= (tTotal / 400), \"Must set max sell to at least 0.25% of total supply\");

\t\tmaxTxAmountBuy = maxBuy;
\t\tmaxTxAmountSell = maxSell;
\t}

\tfunction setMarketingAddress(address payable value) external onlyOwner {
\t\trequire(marketingAddress != value, \"Marketing address is already set to this value\");
\t\tmarketingAddress = value;
\t}

\tfunction setIsBot(address account, bool value) external onlyOwner {
\t\trequire(isBot[account] != value, \"Account is already set to this value\");
\t\tisBot[account] = value;
\t}

\tfunction setMaxWalletAmount(uint256 value) external onlyOwner {
\t\trequire(value \u003e= (tTotal / 200), \"Must set max wallet to at least 0.5% of total supply\");
\t\tmaxWalletAmount = value;
\t}

\tfunction setIsExcludedFromMaxWallet(address account, bool value) external onlyOwner {
\t\trequire(isExcludedFromMaxWallet[account] != value, \"Account is already set to this value\");
\t\tisExcludedFromMaxWallet[account] = value;
\t}

\tfunction setIsExcludedFromFees(address account, bool value) external onlyOwner {
\t\trequire(isExcludedFromFees[account] != value, \"Account is already set to this value\");
\t\tisExcludedFromFees[account] = value;
\t}

\treceive() external payable {}
}
