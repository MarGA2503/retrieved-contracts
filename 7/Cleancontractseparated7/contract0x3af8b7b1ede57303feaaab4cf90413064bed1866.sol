// SPDX-License-Identifier:MIT
pragma solidity \u003e=0.7.0;
import \"./SafeMathTyped.sol\";

// The MIT License
//
// Copyright (c) 2017-2018 0xcert, d.o.o. https://0xcert.org
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the \"Software\"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
/**
 * @title ERC20 standard token implementation.
 * @dev Standard ERC20 token. This contract follows the implementation at https://goo.gl/mLbAPJ.
 */
contract Token
{
  string internal tokenName;

  string internal tokenSymbol;

  uint8 internal tokenDecimals;

  uint256 internal tokenTotalSupply;

  mapping (address =\u003e uint256) internal balances;

  mapping (address =\u003e mapping (address =\u003e uint256)) internal allowed;

  /**
   * @dev Trigger when tokens are transferred, including zero value transfers.
   */
  event Transfer(
    address indexed from,
    address indexed to,
    uint256 value
  );

  /**
   * @dev Trigger on any successful call to approve(address _spender, uint256 _value).
   */
  event Approval(
    address indexed owner,
    address indexed spender,
    uint256 value
  );

  /**
   * @dev Returns the name of the token.
   */
  function name()
    external
    view
    returns (string memory _name)
  {
    _name = tokenName;
  }

  /**
   * @dev Returns the symbol of the token.
   */
  function symbol()
    external
    view
    returns (string memory _symbol)
  {
    _symbol = tokenSymbol;
  }

  /**
   * @dev Returns the number of decimals the token uses.
   */
  function decimals()
    external
    view
    returns (uint8 _decimals)
  {
    _decimals = tokenDecimals;
  }

  /**
   * @dev Returns the total token supply.
   */
  function totalSupply()
    external
    view
    returns (uint256 _totalSupply)
  {
    _totalSupply = tokenTotalSupply;
  }

  /**
   * @dev Returns the account balance of another account with address _owner.
   * @param _owner The address from which the balance will be retrieved.
   */
  function balanceOf(
    address _owner
  )
    external
    view
    returns (uint256 _balance)
  {
    _balance = balances[_owner];
  }

  /**
   * @dev Transfers _value amount of tokens to address _to, and MUST fire the Transfer event. The
   * function SHOULD throw if the _from account balance does not have enough tokens to spend.
   * @param _to The address of the recipient.
   * @param _value The amount of token to be transferred.
   */
  function transfer(
    address _to,
    uint256 _value
  )
    public
    returns (bool _success)
  {
    require(_value \u003c= balances[msg.sender]);

    balances[msg.sender] = SafeMathTyped.sub256(balances[msg.sender], _value);
    balances[_to] = SafeMathTyped.add256(balances[_to], _value);

    emit Transfer(msg.sender, _to, _value);
    _success = true;
  }

  /**
   * @dev Allows _spender to withdraw from your account multiple times, up to the _value amount. If
   * this function is called again it overwrites the current allowance with _value.
   * @param _spender The address of the account able to transfer the tokens.
   * @param _value The amount of tokens to be approved for transfer.
   */
  function approve(
    address _spender,
    uint256 _value
  )
    public
    returns (bool _success)
  {
    require((_value == 0) || (allowed[msg.sender][_spender] == 0));

    allowed[msg.sender][_spender] = _value;

    emit Approval(msg.sender, _spender, _value);
    _success = true;
  }

  /**
   * @dev Returns the amount which _spender is still allowed to withdraw from _owner.
   * @param _owner The address of the account owning tokens.
   * @param _spender The address of the account able to transfer the tokens.
   */
  function allowance(
    address _owner,
    address _spender
  )
    external
    view
    returns (uint256 _remaining)
  {
    _remaining = allowed[_owner][_spender];
  }

  /**
   * @dev Transfers _value amount of tokens from address _from to address _to, and MUST fire the
   * Transfer event.
   * @param _from The address of the sender.
   * @param _to The address of the recipient.
   * @param _value The amount of token to be transferred.
   */
  function transferFrom(
    address _from,
    address _to,
    uint256 _value
  )
    public
    returns (bool _success)
  {
    require(_value \u003c= balances[_from]);
    require(_value \u003c= allowed[_from][msg.sender]);

    balances[_from] = SafeMathTyped.sub256(balances[_from], _value);
    balances[_to] = SafeMathTyped.add256(balances[_to], _value);
    allowed[_from][msg.sender] = SafeMathTyped.sub256(allowed[_from][msg.sender], _value);

    emit Transfer(_from, _to, _value);
    _success = true;
  }

}

/// @notice This is the ABQ token. It allows the owner (the Aardbanq DAO) to mint new tokens. It also allow the 
/// owner to change owners. The ABQ token has 18 decimals.
contract AbqErc20 is Token
{
    /// @notice The owner of the ABQ Token. This is the Aardbanq DAO.
    address public owner;
    /// @notice The address for the founders\u0027 contract.
    address public founderContract;

    constructor(address _owner, address _founderContract)
    {
        tokenName = \"Aardbanq DAO\";
        tokenSymbol = \"ABQ\";
        tokenDecimals = 18;
        tokenTotalSupply = 0;
        owner = _owner;
        founderContract = _founderContract;
    }

    modifier onlyOwner()
    {
        require(msg.sender == owner, \"ABQ/only-owner\");
        _;
    }

    event OwnerChange(address indexed newOwner);
    /// @notice Allows the owner to change the ownership to another address.
    /// @param _newOwner The address that should be the new owner.
    function changeOwner(address _newOwner)
        external
        onlyOwner()
    {
        owner = _newOwner;
        emit OwnerChange(_newOwner);
    }

    /// @notice Allows the owner to mint tokens.
    /// @param _target The address to mint the tokens to.
    /// @param _abqAmount The amount of ABQ to mint.
    function mint(address _target, uint256 _abqAmount)
        external
        onlyOwner()
    {
        balances[_target] = SafeMathTyped.add256(balances[_target], _abqAmount);
        emit Transfer(address(0), _target, _abqAmount);

        // CG: Founder\u0027s part 15% of total that will be issued
        // MATH:
        // totalIncrease = mintAmount + founderPart {A}
        // also:
        // founderPart = 0.15 * totalIncrease
        // ... founderPart / 0.15 = totalIncrease   {B}
        // substituting {A} in {B}
        // ... founderPart / 0.15 = mintAmount + founderPart
        // ... (founderPart / 0.15) - founderPart = mintAmount
        // ... (founderPart - (0.15 * founderPart)) / 0.15 = mintAmount
        // ... (0.85 * founderPart) / 0.15 = mintAmount
        // ... 0.85 * founderPart = 0.15 * mintAmount
        // ... founderPart = (0.15 / 0.85) * mintAmount
        // ... founderPart ~= (mintAmount * 17647) / 100000
        uint256 founderShare = SafeMathTyped.mul256(_abqAmount, 17647) / 100000;
        balances[founderContract] = SafeMathTyped.add256(balances[founderContract], founderShare);
        tokenTotalSupply = SafeMathTyped.add256(SafeMathTyped.add256(tokenTotalSupply, _abqAmount), founderShare);
        emit Transfer(address(0), founderContract, founderShare);
    }

    /// @notice Allow the sender to burn tokens in their account.
    /// @param _abqAmount The amount of tokens to burn from the msg.sender account.
    function burn(uint256 _abqAmount)
      external
    {
      tokenTotalSupply = SafeMathTyped.sub256(tokenTotalSupply, _abqAmount);
      balances[msg.sender] = SafeMathTyped.sub256(balances[msg.sender], _abqAmount);
      emit Transfer(msg.sender, address(0), _abqAmount);
    }

    event NameChange(string newName);
    /// @notice Allow the owner to change the name of the token. 
    /// @param _newName The new name for the token
    function changeName(string calldata _newName)
      external
      onlyOwner()
    {
      tokenName = _newName;
      emit NameChange(_newName);
    }

    event SymbolChange(string newSymbol);
    /// @notice Allow the owner to change the symbol of the token.
    /// @param _newSymbol The new symbol for the token.
    function changeSymbol(string calldata _newSymbol)
      external
      onlyOwner()
    {
      tokenSymbol = _newSymbol;
      emit SymbolChange(_newSymbol);
    }
}"},"DelegateOwnershipManager.sol":{"content":"// SPDX-License-Identifier: UNLICENSED
// This code is the property of the Aardbanq DAO.
// The Aardbanq DAO is located at 0x829c094f5034099E91AB1d553828F8A765a3DaA1 on the Ethereum Main Net.
// It is the author\u0027s wish that this code should be open sourced under the MIT license, but the final 
// decision on this would be taken by the Aardbanq DAO with a vote once sufficient ABQ tokens have been 
// distributed.
// THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
pragma solidity \u003e=0.7.0;
import \"./Minter.sol\";
import \"./AbqErc20.sol\";

/// @notice A delegate ownership manager to allow minting permissions to be set independent of ownership on the ABQ token.
contract DelegateOwnershipManager is Minter
{
    /// @notice The ABQ token.
    AbqErc20 public abqToken;
    /// @notice The owner of the DelegateOwnershipManager. This should be the Aardbanq DAO.
    address public owner;
    /// @notice The addresses that have mint permissions.
    mapping(address =\u003e bool) public mintPermission;

    modifier onlyOwner()
    {
        require(msg.sender == owner, \"ABQ/only-owner\");
        _;
    }

    modifier onlyOwnerOrMintPermission()
    {
        require(msg.sender == owner || mintPermission[msg.sender], \"ABQ/only-owner-or-mint-permission\");
        _;
    }

    /// @notice Construct a DelegateOwnershipManager.
    /// @param _abqToken The ABQ token.
    /// @param _owner The owner for this contract. This should be the Aardbanq DAO.
    constructor (AbqErc20 _abqToken, address _owner)
    {
        abqToken = _abqToken;
        owner = _owner;
    }

    /// @notice Event emitted when minting permission is set.
    /// @param target The address to set permission for.
    /// @param mayMint The permission state.
    event MintPermission(address indexed target, bool mayMint);
    /// @notice Set minting permission for a given address.
    /// @param _target The address to set minting permission for.
    /// @param _mayMint If set to true the _target address will be allowed to mint.
    function setMintPermission(address _target, bool _mayMint)
        onlyOwner()
        external
    {
        mintPermission[_target] = _mayMint;
        emit MintPermission(_target, _mayMint);
    }

    /// @notice The event emitted if the owner is changed.
    /// @param newOwner The new owner for this contract.
    event OwnerChange(address indexed newOwner);
    /// @notice Allows the owner to change the ownership to another address.
    /// @param _newOwner The address that should be the new owner.
    function changeThisOwner(address _newOwner)
        external
        onlyOwner()
    {
        owner = _newOwner;
        emit OwnerChange(_newOwner);
    }

    /// @notice Mint tokens should the msg.sender has permission to mint.
    /// @param _target The address to mint tokens to.
    /// @param _amount The amount of tokens to mint.
    function mint(address _target, uint256 _amount)
        onlyOwnerOrMintPermission()
        override
        external
    {
        abqToken.mint(_target, _amount);
    }

    /// @notice Change the owner of the token. Only the owner may call this.
    /// @param _newOwner The new owner of the token.
    function changeTokenOwner(address _newOwner)
        onlyOwner()
        external
    {
        abqToken.changeOwner(_newOwner);
    }

    /// @notice Change the name of the token. Only the owner may call this.
    function changeName(string calldata _newName)
        onlyOwner()
        external
    {
        abqToken.changeName(_newName);
    }

    /// @notice Change the symbol of the token. Only the owner may call this.
    function changeSymbol(string calldata _newSymbol)
        onlyOwner()
        external
    {
        abqToken.changeSymbol(_newSymbol);
    }
}"},"Erc20.sol":{"content":"// SPDX-License-Identifier: MIT
pragma solidity \u003e=0.7.0;

interface Erc20
{
    function symbol() view external returns (string memory _symbol);
    function decimals() view external returns (uint8 _decimals);
    
    function balanceOf(address _owner) 
        view
        external
        returns (uint256 _balance);
        
    event Transfer(address indexed from, address indexed to, uint256 value);
    function transfer(address _to, uint256 _amount) 
        external
        returns (bool _success);
    function transferFrom(address _from, address _to, uint256 _amount)
        external
        returns (bool _success);

    function approve(address _spender, uint256 _amount) 
        external
        returns (bool _success);
}"},"ILiquidityEstablisher.sol":{"content":"// SPDX-License-Identifier: UNLICENSED
// This code is the property of the Aardbanq DAO.
// The Aardbanq DAO is located at 0x829c094f5034099E91AB1d553828F8A765a3DaA1 on the Ethereum Main Net.
// It is the author\u0027s wish that this code should be open sourced under the MIT license, but the final 
// decision on this would be taken by the Aardbanq DAO with a vote once sufficient ABQ tokens have been 
// distributed.
// THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
pragma solidity \u003e=0.7.0;

interface ILiquidityEstablisher
{
    function isLiquidityEstablishedOrExpired()
        external
        view
        returns (bool _isEstablishedOrExpired);
}"},"InitialLiquidityOffering.sol":{"content":"// SPDX-License-Identifier: UNLICENSED
// This code is the property of the Aardbanq DAO.
// The Aardbanq DAO is located at 0x829c094f5034099E91AB1d553828F8A765a3DaA1 on the Ethereum Main Net.
// It is the author\u0027s wish that this code should be open sourced under the MIT license, but the final 
// decision on this would be taken by the Aardbanq DAO with a vote once sufficient ABQ tokens have been 
// distributed.
// THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

pragma solidity \u003e=0.7.0;
import \"./Minter.sol\";
import \"./Erc20.sol\";
import \"./IUniswapV2Router02.sol\";
import \"./IUniswapV2Factory.sol\";
import \"./IUniswapV2Pair.sol\";
import \"./IPricer.sol\";
import \"./SafeMathTyped.sol\";
import \"./ILiquidityEstablisher.sol\";

/// @notice A contract to offer initial liquidity purchase and an reward.
contract InitialLiquidityOffering is ILiquidityEstablisher
{
    /// @notice The time in unix (seconds) timestamp that the offer closes.
    uint64 public offerCloseTime;
    /// @notice The address funds are sent to.
    address public treasury;
    /// @notice The token that is offered.
    Erc20 public tokenOffer;
    /// @notice The time in unix (seconds) timestamp that the liquidity may be claimed.
    uint64 public liquidityReleaseTime;
    /// @notice The token liquidity will be established with.
    Erc20 public liquidityToken;
    /// @notice Flag to indicate if liquidity has been established.
    bool public isLiquidityEstablished;
    /// @notice The minter of the token on offer.
    Minter public minter;
    /// @notice The uniswap router used to establish liquidity.
    IUniswapV2Router02 public uniswapRouter;
    /// @notice The ICO used to establish the price at which liquidity will be established.
    IPricer public pricer;
    /// @notice The maximum liquidity (priced in liquidityToken) up for sale.
    uint256 public maxLiquidityAllowed;
    /// @notice The total liquidity sold so far.
    uint256 public totalLiquidityProvided;

    /// @notice The total liquidity provided by each address (priced in liquidityToken).
    mapping(address =\u003e uint256) public liquidityBalances;

    /// @notice Constructs an initial liquidity token offering.
    /// @param _offerCloseTime The time in unix (seconds) timestamp that the offer closes.
    /// @param _tokenOffer The token on offer.
    /// @param _liquidityReleaseTime The time in unix (secods) timestamp that the liquidity will be released.
    /// @param _liquidityToken The token liquidity will be sold in. (like DAI)
    /// @param _minter The minter that can mint _tokenOffer tokens.
    /// @param _uniswapRouter The uniswap router to use.
    /// @param _maxLiquidityAllowed The maximum liquidity (priced in _liquidityToken) on sale.
    constructor (uint64 _offerCloseTime, address _treasury, Erc20 _tokenOffer, uint64 _liquidityReleaseTime, Erc20 _liquidityToken, 
        Minter _minter, IUniswapV2Router02 _uniswapRouter, uint256 _maxLiquidityAllowed)
    {
        offerCloseTime = _offerCloseTime;
        treasury = _treasury;
        tokenOffer = _tokenOffer;
        liquidityReleaseTime = _liquidityReleaseTime;
        liquidityToken = _liquidityToken;
        minter = _minter;
        uniswapRouter = _uniswapRouter;
        maxLiquidityAllowed = _maxLiquidityAllowed;
    }

    /// @notice Returns true if either liquidity has been established or the ILO has been closed for more than 7 days. False otherwise.
    /// @return _isEstablishedOrExpired True if either liquidity has been established or the ILO has been closed for more than 7 days. False otherwise.
    function isLiquidityEstablishedOrExpired()
        external
        override
        view
        returns (bool _isEstablishedOrExpired)
    {
        return isLiquidityEstablished || (offerCloseTime + 7 days \u003c= block.timestamp); 
    }

    /// @notice Set the ICO address to use to establish the price of the token on offer.
    function setPricer(IPricer _pricer) 
        external
    {
        require(address(pricer) == address(0), \"ABQICO/pricer-already-set\");
        pricer = _pricer;
    }

    /// @notice Event emitted when liquidity was provided.
    /// @param to The address that provided the liquidity.
    /// @param amount The amount of liquidity (priced in liquidityToken) that was provided.
    event LiquidityOfferReceipt(address to, uint256 amount);
    /// @notice Provide liquidity. Liquidity is paid for from the msg.sender.
    /// @param _target The address that will own and receive the liquidity pool tokens and reward.
    /// @param _amount The amount of liquidity to offer (priced in liquidityToken).
    function provideLiquidity(address _target, uint256 _amount)
        external
    {
        require(offerCloseTime \u003e= block.timestamp \u0026\u0026 maxLiquidityAllowed \u003e totalLiquidityProvided, \"ABQILO/offer-closed\");

        // CG: ensure only whole token amounts have no values in the last 18 places
        _amount = (_amount / 1 ether) * 1 ether;
        require(_amount \u003e= 1 ether, \"ABQILO/amount-too-small\");

        // CG: ensure amounts don\u0027t go above max allowed
        uint256 amountLeft = SafeMathTyped.sub256(maxLiquidityAllowed, totalLiquidityProvided);
        if (_amount \u003e amountLeft)
        {
            _amount = amountLeft;
        }

        // CG: transfer funds
        bool couldTransfer = liquidityToken.transferFrom(msg.sender, address(this), _amount);
        require(couldTransfer, \"ABQILO/could-not-transfer\");

        // CG: account for funds
        totalLiquidityProvided = SafeMathTyped.add256(totalLiquidityProvided, _amount);
        liquidityBalances[_target] = SafeMathTyped.add256(liquidityBalances[_target], _amount);

        emit LiquidityOfferReceipt(_target, _amount);
    }

    /// @notice Event emitted when liquidity is established.
    /// @param liquidityAssetAmount The amount of liquidityToken that was contributed.
    /// @param offerTokenAmount The amount of the token on offer that was added as liquidity.
    /// @param liquidityTokenAmount The amount of liquidity pool tokens that was minted.
    event LiquidityEstablishment(uint256 liquidityAssetAmount, uint256 offerTokenAmount, uint256 liquidityTokenAmount);
    /// @notice Establish liquidity if the sale period ended or the liquidity sale has been sold out.
    function establishLiquidity()
        external
    {
        require(offerCloseTime \u003c block.timestamp || maxLiquidityAllowed == totalLiquidityProvided, \"ABQILO/offer-still-open\");
        require(!isLiquidityEstablished, \"ABQILO/liquidity-already-established\");

        if (totalLiquidityProvided \u003e 0)
        {
            uint256 currentPrice = pricer.currentPrice();
            if (currentPrice \u003e 10 ether)
            {
                // CG: in the event the ICO was sold out.
                currentPrice = 10 ether;
            }
            uint256 totalOfferProvided = SafeMathTyped.mul256(totalLiquidityProvided / currentPrice, 1 ether);
            minter.mint(address(this), totalOfferProvided);

            bool isOfferApproved = tokenOffer.approve(address(uniswapRouter), totalOfferProvided);
            require(isOfferApproved, \"ABQICO/could-not-approve-offer\");
            bool isLiquidityApproved = liquidityToken.approve(address(uniswapRouter), totalLiquidityProvided);
            require(isLiquidityApproved, \"ABQICO/could-not-approve-liquidity\");

            (, , uint256 liquidityTokensCount) = uniswapRouter.addLiquidity(address(liquidityToken), address(tokenOffer), totalLiquidityProvided, totalOfferProvided, 0, 0, address(this), block.timestamp);

            IUniswapV2Factory factory = IUniswapV2Factory(uniswapRouter.factory());
            IUniswapV2Pair pair = IUniswapV2Pair(factory.getPair(address(tokenOffer), address(liquidityToken)));
            require(address(pair) != address(0), \"ABQILO/pair-not-created\");
            bool couldSendDaoShare = pair.transfer(treasury, liquidityTokensCount / 2);
            require(couldSendDaoShare, \"ABQILO/could-not-send\");

            emit LiquidityEstablishment(totalLiquidityProvided, totalOfferProvided, liquidityTokensCount);
        }

        isLiquidityEstablished = true;
    }

    /// @notice Event emitted when liquidity pool tokens are claimed.
    /// @param to The address the claim was for.
    /// @param amount The amount of liquidity pool tokens that was claimed.
    /// @param reward The reward (in the token on offer) that was also claimed.
    event Claim(address to, uint256 amount, uint256 reward);
    /// @notice Claim liquidity pool tokens and the reward after liquidity has been released.
    /// @param _for The address to release the liquidity pool tokens and the reward for.
    function claim(address _for)
        external
    {
        require(liquidityReleaseTime \u003c= block.timestamp, \"ABQILO/liquidity-locked\");
        require(isLiquidityEstablished, \"ABQILO/liquidity-not-established\");

        // CG: we can divide be 1 ether since we made sure values does not include any values in the last 18 decimals. See the provideLiquidity token.
        uint256 claimShareFull = liquidityBalances[_for];
        if (claimShareFull == 0)
        {
            return;
        }
        uint256 claimShare = (claimShareFull / 1 ether);
        uint256 claimPool = (totalLiquidityProvided / 1 ether);

        // CG: remove claim share from accounts
        totalLiquidityProvided = SafeMathTyped.sub256(totalLiquidityProvided, claimShareFull);
        liquidityBalances[_for] = 0;

        // CG: get uniswap pair
        IUniswapV2Factory factory = IUniswapV2Factory(uniswapRouter.factory());
        IUniswapV2Pair pair = IUniswapV2Pair(factory.getPair(address(tokenOffer), address(liquidityToken)));
        require(address(pair) != address(0), \"ABQILO/pair-not-created\");
        uint256 pairBalance = pair.balanceOf(address(this));

        // CG: transfer claim
        uint256 claimTotal = SafeMathTyped.mul256(pairBalance, claimShare) / claimPool;
        bool couldTransfer = pair.transfer(_for, claimTotal);
        require(couldTransfer, \"ABQILO/could-not-transfer\");

        // CG: mint reward: 25% of original contribution as reward tokens.
        uint256 reward = SafeMathTyped.mul256(claimShareFull, 25) / 100;
        minter.mint(_for, reward);

        emit Claim(_for, claimTotal, reward);
    }
}"},"IPricer.sol":{"content":"// SPDX-License-Identifier: UNLICENSED
// This code is the property of the Aardbanq DAO.
// The Aardbanq DAO is located at 0x829c094f5034099E91AB1d553828F8A765a3DaA1 on the Ethereum Main Net.
// It is the author\u0027s wish that this code should be open sourced under the MIT license, but the final 
// decision on this would be taken by the Aardbanq DAO with a vote once sufficient ABQ tokens have been 
// distributed.
// THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
pragma solidity \u003e=0.7.0;

interface IPricer
{
    function currentPrice()
        view
        external
        returns (uint256 _currentPrice);
}"},"IUniswapV2Factory.sol":{"content":"pragma solidity \u003e=0.5.0;

interface IUniswapV2Factory {
    event PairCreated(address indexed token0, address indexed token1, address pair, uint);

    function feeTo() external view returns (address);
    function feeToSetter() external view returns (address);

    function getPair(address tokenA, address tokenB) external view returns (address pair);
    function allPairs(uint) external view returns (address pair);
    function allPairsLength() external view returns (uint);

    function createPair(address tokenA, address tokenB) external returns (address pair);

    function setFeeTo(address) external;
    function setFeeToSetter(address) external;
}"},"IUniswapV2Pair.sol":{"content":"pragma solidity \u003e=0.5.0;

interface IUniswapV2Pair {
    event Approval(address indexed owner, address indexed spender, uint value);
    event Transfer(address indexed from, address indexed to, uint value);

    function name() external pure returns (string memory);
    function symbol() external pure returns (string memory);
    function decimals() external pure returns (uint8);
    function totalSupply() external view returns (uint);
    function balanceOf(address owner) external view returns (uint);
    function allowance(address owner, address spender) external view returns (uint);

    function approve(address spender, uint value) external returns (bool);
    function transfer(address to, uint value) external returns (bool);
    function transferFrom(address from, address to, uint value) external returns (bool);

    function DOMAIN_SEPARATOR() external view returns (bytes32);
    function PERMIT_TYPEHASH() external pure returns (bytes32);
    function nonces(address owner) external view returns (uint);

    function permit(address owner, address spender, uint value, uint deadline, uint8 v, bytes32 r, bytes32 s) external;

    event Mint(address indexed sender, uint amount0, uint amount1);
    event Burn(address indexed sender, uint amount0, uint amount1, address indexed to);
    event Swap(
        address indexed sender,
        uint amount0In,
        uint amount1In,
        uint amount0Out,
        uint amount1Out,
        address indexed to
    );
    event Sync(uint112 reserve0, uint112 reserve1);

    function MINIMUM_LIQUIDITY() external pure returns (uint);
    function factory() external view returns (address);
    function token0() external view returns (address);
    function token1() external view returns (address);
    function getReserves() external view returns (uint112 reserve0, uint112 reserve1, uint32 blockTimestampLast);
    function price0CumulativeLast() external view returns (uint);
    function price1CumulativeLast() external view returns (uint);
    function kLast() external view returns (uint);

    function mint(address to) external returns (uint liquidity);
    function burn(address to) external returns (uint amount0, uint amount1);
    function swap(uint amount0Out, uint amount1Out, address to, bytes calldata data) external;
    function skim(address to) external;
    function sync() external;

    function initialize(address, address) external;
}"},"IUniswapV2Router01.sol":{"content":"pragma solidity \u003e=0.6.2;

interface IUniswapV2Router01 {
    function factory() external pure returns (address);
    function WETH() external pure returns (address);

    function addLiquidity(
        address tokenA,
        address tokenB,
        uint amountADesired,
        uint amountBDesired,
        uint amountAMin,
        uint amountBMin,
        address to,
        uint deadline
    ) external returns (uint amountA, uint amountB, uint liquidity);
    function addLiquidityETH(
        address token,
        uint amountTokenDesired,
        uint amountTokenMin,
        uint amountETHMin,
        address to,
        uint deadline
    ) external payable returns (uint amountToken, uint amountETH, uint liquidity);
    function removeLiquidity(
        address tokenA,
        address tokenB,
        uint liquidity,
        uint amountAMin,
        uint amountBMin,
        address to,
        uint deadline
    ) external returns (uint amountA, uint amountB);
    function removeLiquidityETH(
        address token,
        uint liquidity,
        uint amountTokenMin,
        uint amountETHMin,
        address to,
        uint deadline
    ) external returns (uint amountToken, uint amountETH);
    function removeLiquidityWithPermit(
        address tokenA,
        address tokenB,
        uint liquidity,
        uint amountAMin,
        uint amountBMin,
        address to,
        uint deadline,
        bool approveMax, uint8 v, bytes32 r, bytes32 s
    ) external returns (uint amountA, uint amountB);
    function removeLiquidityETHWithPermit(
        address token,
        uint liquidity,
        uint amountTokenMin,
        uint amountETHMin,
        address to,
        uint deadline,
        bool approveMax, uint8 v, bytes32 r, bytes32 s
    ) external returns (uint amountToken, uint amountETH);
    function swapExactTokensForTokens(
        uint amountIn,
        uint amountOutMin,
        address[] calldata path,
        address to,
        uint deadline
    ) external returns (uint[] memory amounts);
    function swapTokensForExactTokens(
        uint amountOut,
        uint amountInMax,
        address[] calldata path,
        address to,
        uint deadline
    ) external returns (uint[] memory amounts);
    function swapExactETHForTokens(uint amountOutMin, address[] calldata path, address to, uint deadline)
        external
        payable
        returns (uint[] memory amounts);
    function swapTokensForExactETH(uint amountOut, uint amountInMax, address[] calldata path, address to, uint deadline)
        external
        returns (uint[] memory amounts);
    function swapExactTokensForETH(uint amountIn, uint amountOutMin, address[] calldata path, address to, uint deadline)
        external
        returns (uint[] memory amounts);
    function swapETHForExactTokens(uint amountOut, address[] calldata path, address to, uint deadline)
        external
        payable
        returns (uint[] memory amounts);

    function quote(uint amountA, uint reserveA, uint reserveB) external pure returns (uint amountB);
    function getAmountOut(uint amountIn, uint reserveIn, uint reserveOut) external pure returns (uint amountOut);
    function getAmountIn(uint amountOut, uint reserveIn, uint reserveOut) external pure returns (uint amountIn);
    function getAmountsOut(uint amountIn, address[] calldata path) external view returns (uint[] memory amounts);
    function getAmountsIn(uint amountOut, address[] calldata path) external view returns (uint[] memory amounts);
}"},"IUniswapV2Router02.sol":{"content":"pragma solidity \u003e=0.6.2;

import \u0027./IUniswapV2Router01.sol\u0027;

interface IUniswapV2Router02 is IUniswapV2Router01 {
    function removeLiquidityETHSupportingFeeOnTransferTokens(
        address token,
        uint liquidity,
        uint amountTokenMin,
        uint amountETHMin,
        address to,
        uint deadline
    ) external returns (uint amountETH);
    function removeLiquidityETHWithPermitSupportingFeeOnTransferTokens(
        address token,
        uint liquidity,
        uint amountTokenMin,
        uint amountETHMin,
        address to,
        uint deadline,
        bool approveMax, uint8 v, bytes32 r, bytes32 s
    ) external returns (uint amountETH);

    function swapExactTokensForTokensSupportingFeeOnTransferTokens(
        uint amountIn,
        uint amountOutMin,
        address[] calldata path,
        address to,
        uint deadline
    ) external;
    function swapExactETHForTokensSupportingFeeOnTransferTokens(
        uint amountOutMin,
        address[] calldata path,
        address to,
        uint deadline
    ) external payable;
    function swapExactTokensForETHSupportingFeeOnTransferTokens(
        uint amountIn,
        uint amountOutMin,
        address[] calldata path,
        address to,
        uint deadline
    ) external;
}"},"Minter.sol":{"content":"// SPDX-License-Identifier: UNLICENSED
// This code is the property of the Aardbanq DAO.
// The Aardbanq DAO is located at 0x829c094f5034099E91AB1d553828F8A765a3DaA1 on the Ethereum Main Net.
// It is the author\u0027s wish that this code should be open sourced under the MIT license, but the final 
// decision on this would be taken by the Aardbanq DAO with a vote once sufficient ABQ tokens have been 
// distributed.
// THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
pragma solidity \u003e=0.7.0;

interface Minter
{
    function mint(address _target, uint256 _amount) external;
}"},"SafeMathTyped.sol":{"content":"// SPDX-License-Identifier: MIT
pragma solidity ^0.7.0;

/**
 * @title SafeMath
 * @dev Unsigned math operations with safety checks that revert on error
 */
library SafeMathTyped {
    /**
    * @dev Multiplies two unsigned integers, reverts on overflow.
    */
    function mul256(uint256 a, uint256 b) internal pure returns (uint256) {
        // Gas optimization: this is cheaper than requiring \u0027a\u0027 not being zero, but the
        // benefit is lost if \u0027b\u0027 is also tested.
        // See: https://github.com/OpenZeppelin/openzeppelin-solidity/pull/522
        if (a == 0) {
            return 0;
        }

        uint256 c = a * b;
        require(c / a == b, \"uint256 overflow\");

        return c;
    }

    /**
    * @dev Integer division of two unsigned integers truncating the quotient, reverts on division by zero.
    */
    function div256(uint256 a, uint256 b) internal pure returns (uint256) {
        // Solidity only automatically asserts when dividing by 0
        require(b \u003e 0, \"Can\u0027t divide by 0\");
        uint256 c = a / b;
        // assert(a == b * c + a % b); // There is no case in which this doesn\u0027t hold

        return c;
    }

    /**
    * @dev Subtracts two unsigned integers, reverts on overflow (i.e. if subtrahend is greater than minuend).
    */
    function sub256(uint256 a, uint256 b) internal pure returns (uint256) {
        require(b \u003c= a, \"uint256 underflow\");
        uint256 c = a - b;

        return c;
    }

    /**
    * @dev Adds two unsigned integers, reverts on overflow.
    */
    function add256(uint256 a, uint256 b) internal pure returns (uint256) {
        uint256 c = a + b;
        require(c \u003e= a, \"uint256 overflow\");

        return c;
    }

    /**
    * @dev Divides two unsigned integers and returns the remainder (unsigned integer modulo),
    * reverts when dividing by zero.
    */
    function mod256(uint256 a, uint256 b) internal pure returns (uint256) {
        require(b != 0, \"Can\u0027t mod by 0\");
        return a % b;
    }

    /**
    * @dev returns the greater of two numbers
    */
    function max256(uint256 a, uint256 b) internal pure returns (uint) {
        return a \u003e b ? a : b;
    }

    /**
    * @dev returns the lesser of two numbers
    */
    function min256(uint256 a, uint256 b) internal pure returns (uint) {
        return a \u003c b ? a : b;
    }
}"},"SaleRatification.sol":{"content":"// SPDX-License-Identifier: MIT
pragma solidity \u003e=0.7.0;
import \"./ScaleBuying.sol\";
import \"./InitialLiquidityOffering.sol\";
import \"./DelegateOwnershipManager.sol\";
import \"./AbqErc20.sol\";

/// @notice Contract used to ratify the ICO and ILO for the ABQ token.
contract SaleRatification
{
    /// @notice Ratify the ICO and ILO for the ABQ token.
    /// @param _ilo The intended Initial Liquidity Offering (ILO) to use.
    /// @param _ico The intended Initial Coin Offering (ICO) to use.
    /// @param _treasury The Aardbanq DAO treasury that bounty tokens will be minted for.
    /// @param _ownershipManager The delegate manager for the ownership and minting permissions for the ABQ token.
    /// @param _token The ABQ token.
    function ratify(InitialLiquidityOffering _ilo, ScaleBuying _ico, address _treasury, DelegateOwnershipManager _ownershipManager, AbqErc20 _token)
        external
    {
        // CG: set the pricer for the ILO as the ICO and the liquidity establisher for the ICO as the ILO
        _ilo.setPricer(_ico);
        _ico.setLiquidityEstablisher(_ilo);

        // CG: Give ownership of the token to the DelegateOwnershipManager and give mint permission to the ICO and ILO.
        _token.changeOwner(address(_ownershipManager));
        _ownershipManager.setMintPermission(address(_ilo), true);
        _ownershipManager.setMintPermission(address(_ico), true);

        _ownershipManager.mint(address(_ico), 11585 ether); // CG: Auction tokens
        _ownershipManager.mint(address(_treasury), 50000 ether);    // CG: Bounty tokens
    }
}"},"ScaleBuying.sol":{"content":"// SPDX-License-Identifier: UNLICENSED
// This code is the property of the Aardbanq DAO.
// The Aardbanq DAO is located at 0x829c094f5034099E91AB1d553828F8A765a3DaA1 on the Ethereum Main Net.
// It is the author\u0027s wish that this code should be open sourced under the MIT license, but the final 
// decision on this would be taken by the Aardbanq DAO with a vote once sufficient ABQ tokens have been 
// distributed.
// THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

pragma solidity \u003e=0.7.0;
import \"./SafeMathTyped.sol\";
import \"./Erc20.sol\";
import \u0027./Minter.sol\u0027;
import \"./IPricer.sol\";
import \"./ILiquidityEstablisher.sol\";

/// @notice Allow buying of tokens in batches of increasing price.
contract ScaleBuying is IPricer
{
    /// @notice The token to use for purchasing
    Erc20 public paymentAsset;
    /// @notice The token that is being bought
    Erc20 public boughtAsset;
    /// @notice The minter for the token being bought
    Minter public minter;
    /// @notice The date in unix timestamp (seconds) when the sale closes.
    uint64 public closingDate;
    /// @notice The address to which the funds should be sent to.
    address public treasury;
    /// @notice The location of the ILO, used to know when tokens can be claimed.
    ILiquidityEstablisher public liquidityEstablisher;
    /// @notice The amount that has been awarded so far.
    uint256 public amountAwarded;
    /// @notice The initial price for the sale.
    uint256 public initialPrice;
    /// @notice The price increase for each token block.
    uint256 public priceIncrease;
    /// @notice The amount of tokens per block.
    uint256 public tokensPerPriceBlock;
    /// @notice The number of blocks up for sale.
    uint256 public maxBlocks;

    /// @notice The amounts of tokens claimable by an address.
    mapping(address =\u003e uint256) public amountsClaimable;

    /// @notice Constructs a ScaleBuying
    /// @param _paymentAsset The token address to be used for payment.
    /// @param _boughtAsset The token address to be issued.
    /// @param _minter The minter for the issued token.
    /// @param _treasury The address to receive all funds.
    /// @param _initialPrice The initial price per token.
    /// @param _priceIncrease The increase of the price per token for each block.
    /// @param _tokensPerPriceBlock The tokens in each block.
    /// @param _maxBlocks The maximum number of blocks on sale.
    /// @param _closingDate The date in unix (seconds) timestamp that the sale will close.
    constructor (Erc20 _paymentAsset, Erc20 _boughtAsset, Minter _minter, address _treasury, uint256 _initialPrice, 
        uint256 _priceIncrease, uint256 _tokensPerPriceBlock, uint256 _maxBlocks, uint64 _closingDate)
    {
        paymentAsset = _paymentAsset;
        boughtAsset = _boughtAsset;
        minter = _minter;
        treasury = _treasury;
        amountAwarded = 0;
        initialPrice = _initialPrice;
        priceIncrease = _priceIncrease;
        tokensPerPriceBlock = _tokensPerPriceBlock;
        maxBlocks = _maxBlocks;
        closingDate = _closingDate;

        allocateTokensRaisedByAuction();
    }

    // CG: This allocates the amount of tokens that was already bought on auction before\\
    //     switching over to this Scale Buying.
    function allocateTokensRaisedByAuction() 
        private
    {
        uint256 price = initialPrice;
        
        uint256 buyerAAmount = 7165 ether;
        amountsClaimable[0xEE779e4b3e7b11454ed80cFE12Cf48ee3Ff4579E] = buyerAAmount;
        emit Bought(0xEE779e4b3e7b11454ed80cFE12Cf48ee3Ff4579E, buyerAAmount, price);
        
        uint256 buyerBAmount = 4065 ether;
        amountsClaimable[0x6C4f3Db0E743A9e8f44A756b6585192B358D7664] = buyerBAmount;
        emit Bought(0x6C4f3Db0E743A9e8f44A756b6585192B358D7664, buyerAAmount, price);

        uint256 buyerCAmount = 355 ether;
        amountsClaimable[0x0FB79E6C0F5447ffe36a0050221275Da487b0E09] = buyerCAmount;
        emit Bought(0x0FB79E6C0F5447ffe36a0050221275Da487b0E09, buyerAAmount, price);

        amountAwarded = buyerAAmount + buyerBAmount + buyerCAmount;
    }

    /// @notice Set the ILO to use to track if liquidity has been astablished and thus claims can be allowed.
    /// @param _liquidityEstablisher The ILO.
    function setLiquidityEstablisher(ILiquidityEstablisher _liquidityEstablisher)
        external
    {
        require(address(liquidityEstablisher) == address(0), \"ABQDAO/already-set\");

        liquidityEstablisher = _liquidityEstablisher;
    }

    /// @notice The event emitted when a claim is executed.
    /// @param claimer The address the claim has been processed for.
    /// @param amount The amount that was claimed.
    event Claimed(address indexed claimer, uint256 amount);
    /// @notice Claim ABQ bought for the given address. Claims can only be processed after liquidity has been established.
    /// @param _target The address to process claims for.
    function claim(address _target)
        external
    {
        // CG: Claims cannot be executed before liquidity is established or closed more than a week ago.
        require(liquidityEstablisher.isLiquidityEstablishedOrExpired(), \"ABQDAO/cannot-claim-yet\");

        uint256 amountClaimable = amountsClaimable[_target];
        if (amountClaimable \u003e 0)
        {
            bool isSuccess = boughtAsset.transfer(_target, amountClaimable);
            require(isSuccess, \"ABQDAO/could-not-transfer-claim\");
            amountsClaimable[_target] = 0;
            emit Claimed(_target, amountClaimable);
        }
    }

    /// @notice The event emitted when tokens are bought.
    /// @param buyer The address that may claim the tokens.
    /// @param amount The amount of token bought.
    /// @param pricePerToken The price per token that the tokens were bought for.
    event Bought(address indexed buyer, uint256 amount, uint256 pricePerToken);
    /// @notice Buy tokens in the current block.
    /// @param _paymentAmount The amount to spend. This will be transfered from msg.sender who should approved this amount first.
    /// @param _target The address that the amounts would be bought for. Tokens are distributed after calling the claim method.
    function buy(uint256 _paymentAmount, address _target) 
        external
        returns (uint256 _paymentLeft)
    {
        // CG: only allow buys before the ico closes.
        require(block.timestamp \u003c= closingDate, \"ABQDAO/ico-concluded\");

        (uint256 paymentLeft, uint256 paymentDue) = buyInBlock(_paymentAmount, _target);
        // CG: transfer payment
        if (paymentDue \u003e 0)
        {
            bool isSuccess = paymentAsset.transferFrom(msg.sender, treasury, paymentDue);
            require(isSuccess, \"ABQDAO/could-not-pay\");
        }
        return paymentLeft;
    }

    function buyInBlock(uint256 _paymentAmount, address _target)
        private
        returns (uint256 _paymentLeft, uint256 _paymentDue)
    {
        uint256 currentBlockIndex = currentBlock();
        uint256 tokensLeft = tokensLeftInBlock(currentBlockIndex);

        if (currentBlockIndex \u003e= maxBlocks)
        {
            // CG: If all block are sold out, then amount bought should be zero.
            return (_paymentAmount, 0);
        }
        else
        {
            uint256 currentPriceLocal = currentPrice();
            uint256 tokensCanPayFor = _paymentAmount / currentPriceLocal;
            if (tokensCanPayFor == 0)
            {
                return (_paymentAmount, 0);
            }
            if (tokensCanPayFor \u003e (tokensLeft / 1 ether))
            {
                tokensCanPayFor = tokensLeft / 1 ether;
            }

            // CG: Get the amount of tokens that can be bought in this block.
            uint256 paymentDue = SafeMathTyped.mul256(tokensCanPayFor, currentPriceLocal);
            tokensCanPayFor = SafeMathTyped.mul256(tokensCanPayFor, 1 ether);
            amountsClaimable[_target] = SafeMathTyped.add256(amountsClaimable[_target], tokensCanPayFor);
            amountAwarded = SafeMathTyped.add256(amountAwarded, tokensCanPayFor);
            minter.mint(address(this), tokensCanPayFor);
            emit Bought(_target, tokensCanPayFor, currentPriceLocal);
            uint256 paymentLeft = SafeMathTyped.sub256(_paymentAmount, paymentDue);
            
            if (paymentLeft \u003c= currentPriceLocal)
            {
                return (paymentLeft, paymentDue);
            }
            else
            {
                // CG: should this block be sold out, buy the remainder in the next box.
                (uint256 subcallPaymentLeft, uint256 subcallPaymentDue) = buyInBlock(paymentLeft, _target);
                paymentDue = SafeMathTyped.add256(paymentDue, subcallPaymentDue);
                return (subcallPaymentLeft, paymentDue);
            }
        }
    }

    /// @notice Get the current price per token.
    /// @return _currentPrice The current price per token.
    function currentPrice()
        view
        public
        override
        returns (uint256 _currentPrice)
    {
        return SafeMathTyped.add256(initialPrice, SafeMathTyped.mul256(currentBlock(), priceIncrease));
    }

    /// @notice Get the current block number, starting at 0.
    function currentBlock() 
        view
        public
        returns (uint256 _currentBlock)
    {
        return amountAwarded / tokensPerPriceBlock;
    }

    /// @notice Get the amount of tokens left in a given block.
    /// @param _block The block to get the number of tokens left for.
    /// @return _tokensLeft The number of tokens left in the given _block.
    function tokensLeftInBlock(uint256 _block)
        view
        public
        returns (uint256 _tokensLeft)
    {
        uint256 currentBlockIndex = currentBlock();

        if (_block \u003e maxBlocks || _block \u003c currentBlockIndex)
        {
            return 0;
        }

        if (_block == currentBlockIndex)
        {
            //CG: non overflow code: return ((currentBlockIndex + 1) * tokensPerPriceBlock) - amountAwarded;
            return SafeMathTyped.sub256(SafeMathTyped.mul256(SafeMathTyped.add256(currentBlockIndex, 1), tokensPerPriceBlock), amountAwarded);
        }
        else
        {
            return tokensPerPriceBlock;
        }
    }
}
