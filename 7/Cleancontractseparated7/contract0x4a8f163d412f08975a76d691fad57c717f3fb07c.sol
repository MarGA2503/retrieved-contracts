// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

/**
 * @dev Provides information about the current execution context, including the
 * sender of the transaction and its data. While these are generally available
 * via msg.sender and msg.data, they should not be accessed in such a direct
 * manner, since when dealing with meta-transactions the account sending and
 * paying for execution may not be the actual sender (as far as an application
 * is concerned).
 *
 * This contract is only required for intermediate, library-like contracts.
 */
abstract contract Context {
    function _msgSender() internal view virtual returns (address) {
        return msg.sender;
    }

    function _msgData() internal view virtual returns (bytes calldata) {
        return msg.data;
    }
}
"},"ILand.sol":{"content":"// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;


interface ILand {
  function maximumSupply() external view returns (uint256);
  function balanceOf(address owner) external view returns (uint256);
  function tokenOfOwnerByIndex(address owner, uint256 index) external view returns (uint256);
}
"},"IMintPass.sol":{"content":"// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;


interface IMintPass {
  function passExists(uint256 _passId) external view returns (bool);
  function passDetail(uint256 _tokenId) external view returns (address, uint256, uint256);
  function mintToken(
    address _account,
    uint256 _passId,
    uint256 _count
  ) external;
  function burnToken(uint256 _tokenId) external;
}
"},"IOre.sol":{"content":"// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;


interface IOre {
  function balanceOf(address owner) external view returns (uint256);
  function mint(address account, uint256 amount) external;
  function burn(address account, uint256 amount) external;
}
"},"MintPassMinter.sol":{"content":"// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import \"./Ownable.sol\";
import \"./ReentrancyGuard.sol\";

import \"./ILand.sol\";
import \"./IOre.sol\";
import \"./IMintPass.sol\";


contract MintPassMinter is Ownable, ReentrancyGuard {
  // MintPass token contract interface
  IMintPass public mintPass;
  // Land token contract interface
  ILand public land;
  // Ore token contract interface
  IOre public ore;

  // Stores the currently set pass prices
  mapping (uint256 =\u003e uint256) private _passPrices;
  // Keeps track of the timestamp of the latest claiming period
  uint256 private _lastClaimTimestamp;

  // Keeps track of the total free pass claimed by landowners
  uint256 public totalFreePassClaimed;
  // Keeps track of the total purchased mint passes
  mapping (uint256 =\u003e uint256) public totalPurchasedByPassId;
  // Keeps track of total claimed free passes for land owners
  mapping (uint256 =\u003e uint256) public lastClaimTimestampByLandId;

  // The passId used to indicate the free passes claimable by landowners
  uint256 private _freePassId;

  constructor(
    address _ore,
    address _land,
    address _mintPass
  ) {
    ore = IOre(_ore);
    land = ILand(_land);
    mintPass = IMintPass(_mintPass);
  }

  function freePassId() external view returns (uint256) {
    return _freePassId;
  }

  function passPrice(uint256 _passId) external view returns (uint256) {
    require(mintPass.passExists(_passId), \"Invalid PassId\");
    return _passPrices[_passId];
  }

  // Enable new claiming by updating the ending timestamp to 24 hours after enabled
  function setLastClaimTimestamp(uint256 _timestamp) external onlyOwner {
    _lastClaimTimestamp = _timestamp;
  }

  function lastClaimTimestamp() external view returns (uint256) {
    return _lastClaimTimestamp;
  }

  // Update the token price
  function setPassPrice(uint256 _passId, uint256 _price) external onlyOwner {
    require(mintPass.passExists(_passId), \"Invalid PassId\");
    _passPrices[_passId] = _price;
  }

  // Set the passId used as landowner\u0027s free passes
  function setFreePassId(uint256 _passId) external onlyOwner {
    require(mintPass.passExists(_passId), \"Invalid PassId\");
    _freePassId = _passId;
  }

  // Generate passes to be used for marketing purposes
  function generatePassForMarketing(uint256 _passId, uint256 _count) external onlyOwner {
    require(_count \u003e 0, \"Invalid Amount\");
    mintPass.mintToken(msg.sender, _passId, _count);
  }

  // Fetch the total count of unclaimed free passes for the specified landowner account
  function unclaimedFreePass(address _account) external view returns (uint256) {
    uint256 landOwned = land.balanceOf(_account);
    uint256 mintCount = 0;

    for (uint256 i = 0; i \u003c landOwned; i++) {
      uint256 tokenId = land.tokenOfOwnerByIndex(_account, i);
      if (_lastClaimTimestamp \u003e block.timestamp \u0026\u0026
        _lastClaimTimestamp \u003e lastClaimTimestampByLandId[tokenId]) {
        mintCount++;
      }
    }

    return mintCount;
  }

  // Handles free passes claiming for the landowners
  function claimFreePass() external nonReentrant {
    require(_freePassId \u003e 0, \"Pass Id Not Set\");
    uint256 landOwned = land.balanceOf(msg.sender);
    require(landOwned \u003e 0, \"Reserved For Land Owners\");

    // Iterate through all the land tokens owned to get the mint count and mark them as claimed
    uint256 mintCount = 0;

    for (uint256 i = 0; i \u003c landOwned; i++) {
      uint256 tokenId = land.tokenOfOwnerByIndex(msg.sender, i);
      if (_lastClaimTimestamp \u003e block.timestamp \u0026\u0026
        _lastClaimTimestamp \u003e lastClaimTimestampByLandId[tokenId]) {
        mintCount++;
        lastClaimTimestampByLandId[tokenId] = _lastClaimTimestamp;
      }
    }

    require(mintCount \u003e 0, \"No Unclaimed Free Passes Found\");

    totalFreePassClaimed += mintCount;
    mintPass.mintToken(msg.sender, _freePassId, mintCount);
  }

  // Handles pass purchases using ORE
  function buyPass(uint256 _passId, uint256 _count) external nonReentrant {
    // Check if sufficient funds are sent
    require(mintPass.passExists(_passId), \"Invalid PassId\");
    require(_passPrices[_passId] \u003e 0, \"Price Not Set\");
    uint256 totalPrice = _count * _passPrices[_passId];
    require(ore.balanceOf(msg.sender) \u003e= totalPrice, \"Insufficient Ore\");

    totalPurchasedByPassId[_passId] += _count;

    // Burn the ORE and proceed to mint the passes
    ore.burn(msg.sender, totalPrice);
    mintPass.mintToken(msg.sender, _passId, _count);
  }
}
"},"Ownable.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import \"./Context.sol\";

/**
 * @dev Contract module which provides a basic access control mechanism, where
 * there is an account (an owner) that can be granted exclusive access to
 * specific functions.
 *
 * By default, the owner account will be the one that deploys the contract. This
 * can later be changed with {transferOwnership}.
 *
 * This module is used through inheritance. It will make available the modifier
 * `onlyOwner`, which can be applied to your functions to restrict their use to
 * the owner.
 */
abstract contract Ownable is Context {
    address private _owner;

    event OwnershipTransferred(address indexed previousOwner, address indexed newOwner);

    /**
     * @dev Initializes the contract setting the deployer as the initial owner.
     */
    constructor() {
        _setOwner(_msgSender());
    }

    /**
     * @dev Returns the address of the current owner.
     */
    function owner() public view virtual returns (address) {
        return _owner;
    }

    /**
     * @dev Throws if called by any account other than the owner.
     */
    modifier onlyOwner() {
        require(owner() == _msgSender(), \"Ownable: caller is not the owner\");
        _;
    }

    /**
     * @dev Leaves the contract without owner. It will not be possible to call
     * `onlyOwner` functions anymore. Can only be called by the current owner.
     *
     * NOTE: Renouncing ownership will leave the contract without an owner,
     * thereby removing any functionality that is only available to the owner.
     */
    function renounceOwnership() public virtual onlyOwner {
        _setOwner(address(0));
    }

    /**
     * @dev Transfers ownership of the contract to a new account (`newOwner`).
     * Can only be called by the current owner.
     */
    function transferOwnership(address newOwner) public virtual onlyOwner {
        require(newOwner != address(0), \"Ownable: new owner is the zero address\");
        _setOwner(newOwner);
    }

    function _setOwner(address newOwner) private {
        address oldOwner = _owner;
        _owner = newOwner;
        emit OwnershipTransferred(oldOwner, newOwner);
    }
}
"},"ReentrancyGuard.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

/**
 * @dev Contract module that helps prevent reentrant calls to a function.
 *
 * Inheriting from `ReentrancyGuard` will make the {nonReentrant} modifier
 * available, which can be applied to functions to make sure there are no nested
 * (reentrant) calls to them.
 *
 * Note that because there is a single `nonReentrant` guard, functions marked as
 * `nonReentrant` may not call one another. This can be worked around by making
 * those functions `private`, and then adding `external` `nonReentrant` entry
 * points to them.
 *
 * TIP: If you would like to learn more about reentrancy and alternative ways
 * to protect against it, check out our blog post
 * https://blog.openzeppelin.com/reentrancy-after-istanbul/[Reentrancy After Istanbul].
 */
abstract contract ReentrancyGuard {
    // Booleans are more expensive than uint256 or any type that takes up a full
    // word because each write operation emits an extra SLOAD to first read the
    // slot\u0027s contents, replace the bits taken up by the boolean, and then write
    // back. This is the compiler\u0027s defense against contract upgrades and
    // pointer aliasing, and it cannot be disabled.

    // The values being non-zero value makes deployment a bit more expensive,
    // but in exchange the refund on every call to nonReentrant will be lower in
    // amount. Since refunds are capped to a percentage of the total
    // transaction\u0027s gas, it is best to keep them low in cases like this one, to
    // increase the likelihood of the full refund coming into effect.
    uint256 private constant _NOT_ENTERED = 1;
    uint256 private constant _ENTERED = 2;

    uint256 private _status;

    constructor() {
        _status = _NOT_ENTERED;
    }

    /**
     * @dev Prevents a contract from calling itself, directly or indirectly.
     * Calling a `nonReentrant` function from another `nonReentrant`
     * function is not supported. It is possible to prevent this from happening
     * by making the `nonReentrant` function external, and make it call a
     * `private` function that does the actual work.
     */
    modifier nonReentrant() {
        // On the first call to nonReentrant, _notEntered will be true
        require(_status != _ENTERED, \"ReentrancyGuard: reentrant call\");

        // Any calls to nonReentrant after this point will fail
        _status = _ENTERED;

        _;

        // By storing the original value once again, a refund is triggered (see
        // https://eips.ethereum.org/EIPS/eip-2200)
        _status = _NOT_ENTERED;
    }
}

