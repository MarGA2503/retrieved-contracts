pragma solidity ^0.5.0;

interface GameInterface {
    function maxBet(uint _num, uint _bankRoll) external view returns(uint);

    function resultNumber(bytes32 _serverSeed, bytes32 _userSeed, uint _num) external view returns(uint);

    function userProfit(uint _num, uint _betValue, uint _resultNum) external view returns(int);

    function maxUserProfit(uint _num, uint _betValue) external view returns(int);
}
"},"Plinko.sol":{"content":"pragma solidity ^0.5.0;

import \"./GameInterface.sol\";
import \"./Utilities.sol\";
import \"../SafeMath.sol\";
import \"../SafeCast.sol\";


contract Plinko is GameInterface, Utilities {
    using SafeCast for uint;
    using SafeMath for uint;

    uint public constant MAX_BET_DIVIDER = 10000;

    uint public constant PAYOUT_DIVIDER = 10;

    mapping (uint =\u003e mapping(uint =\u003e uint16)) public MAX_BET;

    mapping (uint =\u003e mapping(uint =\u003e uint16[])) public PAYOUT;

    constructor() public {
        MAX_BET[1][8] = 264;
        MAX_BET[1][12] = 607;
        MAX_BET[1][16] = 758;
        MAX_BET[2][8] = 55;
        MAX_BET[2][12] = 175;
        MAX_BET[2][16] = 208;
        MAX_BET[3][8] = 24;
        MAX_BET[3][12] = 77;
        MAX_BET[3][16] = 68;

        PAYOUT[1][8] = [4, 9, 14, 19, 73];
        PAYOUT[1][12] = [4, 10, 11, 15, 18, 31, 100];
        PAYOUT[1][16] = [4, 10, 11, 12, 16, 17, 18, 75, 130];
        PAYOUT[2][8] = [3, 5, 15, 37, 160];
        PAYOUT[2][12] = [3, 6, 14, 20, 30, 42, 220];
        PAYOUT[2][16] = [2, 5, 14, 17, 19, 40, 63, 96, 250];
        PAYOUT[3][8] = [1, 3, 10, 71, 210];
        PAYOUT[3][12] = [1, 4, 11, 31, 46, 81, 270];
        PAYOUT[3][16] = [1, 3, 11, 20, 32, 56, 100, 260, 800];
    }

    modifier onlyValidNum(uint _betNum) {
        uint risk = getRisk(_betNum);
        uint rows = getRows(_betNum);

        require(risk \u003e= 1 \u0026\u0026 risk \u003c= 3 \u0026\u0026 (rows == 8 || rows == 12 || rows == 16) , \"Invalid num\");
        _;
    }

    modifier onlyValidResultNum(uint _betNum, uint _resultNum) {
        uint rows = getRows(_betNum);
        require(_resultNum \u003e= 0 \u0026\u0026 _resultNum \u003c (1 \u003c\u003c rows));
        _;
    }

    function maxBet(uint _betNum, uint _bankRoll) external onlyValidNum(_betNum) view returns(uint) {
        uint risk = getRisk(_betNum);
        uint rows = getRows(_betNum);

        uint maxBetValue = MAX_BET[risk][rows];

        return _bankRoll.mul(maxBetValue).div(MAX_BET_DIVIDER);
    }

    function resultNumber(bytes32 _serverSeed, bytes32 _userSeed, uint _betNum) external onlyValidNum(_betNum) view returns(uint) {
        uint randNum = Utilities.generateRandomNumber(_serverSeed, _userSeed);
        uint rows = getRows(_betNum);
        return randNum \u0026 ((1 \u003c\u003c rows) - 1);
    }

    function userProfit(uint _betNum, uint _betValue, uint _resultNum)
        external
        onlyValidNum(_betNum)
        onlyValidResultNum(_betNum, _resultNum)
        view
        returns(int)
    {
        uint risk = getRisk(_betNum);
        uint rows = getRows(_betNum);

        uint result = countBits(_resultNum, rows);
        uint resultIndex = calculateResultIndex(result, rows);
        uint16 payoutValue = PAYOUT[risk][rows][resultIndex];

        return calculateProfit(payoutValue, _betValue);
    }


    function maxUserProfit(uint _betNum, uint _betValue) external onlyValidNum(_betNum) view returns(int) {
        uint risk = getRisk(_betNum);
        uint rows = getRows(_betNum);

        uint16[] storage payout = PAYOUT[risk][rows];
        uint maxPayout = 0;
        for (uint i = 0; i \u003c payout.length; i++) {
            if (payout[i] \u003e maxPayout) {
                maxPayout = payout[i];
            }
        }

        return calculateProfit(maxPayout, _betValue);
    }

    function calculateProfit(uint _payout, uint _betValue) private pure returns(int) {
        return _betValue.mul(_payout).div(PAYOUT_DIVIDER).castToInt().sub(_betValue.castToInt());
    }

    function calculateResultIndex(uint _result, uint _rows) private pure returns(uint) {
        uint halfRows = _rows / 2;
        return _result \u003c halfRows ? halfRows - _result : _result - halfRows;
    }

    function getRisk(uint _num) private pure returns(uint) {
        return (_num / 100) % 10;
    }

    function getRows(uint _num) private pure returns(uint) {
        return _num % 100;
    }

    function countBits(uint _num, uint _rows) private pure returns(uint) {
        uint selectedBits = 0;
        // Could be calculated more efficient.
        // But as it\u0027s only needed if a conflict arises, let\u0027s keep it simple.
        for (uint i = 0; i \u003c _rows; i++) {
            if (_num \u0026 (1 \u003c\u003c i) \u003e 0) {
                selectedBits += 1;
            }
        }
        return selectedBits;
    }
}
"},"SafeCast.sol":{"content":"pragma solidity ^0.5.0;

library SafeCast {
    /**
     * Cast unsigned a to signed a.
     */
    function castToInt(uint a) internal pure returns(int) {
        assert(a \u003c (1 \u003c\u003c 255));
        return int(a);
    }

    /**
     * Cast signed a to unsigned a.
     */
    function castToUint(int a) internal pure returns(uint) {
        assert(a \u003e= 0);
        return uint(a);
    }
}
"},"SafeMath.sol":{"content":"pragma solidity ^0.5.0;


/**
 * @title SafeMath
 * @dev Math operations with safety checks that throw on error.
 * From zeppelin-solidity
 */
library SafeMath {

    /**
    * @dev Multiplies two unsigned integers, throws on overflow.
    */
    function mul(uint256 a, uint256 b) internal pure returns (uint256 c) {
        // Gas optimization: this is cheaper than asserting \u0027a\u0027 not being zero, but the
        // benefit is lost if \u0027b\u0027 is also tested.
        // See: https://github.com/OpenZeppelin/openzeppelin-solidity/pull/522
        if (a == 0) {
            return 0;
        }

        c = a * b;
        assert(c / a == b);
        return c;
    }

    /**
    * @dev Multiplies two signed integers, throws on overflow.
    */
    function mul(int256 a, int256 b) internal pure returns (int256) {
        // Gas optimization: this is cheaper than asserting \u0027a\u0027 not being zero, but the
        // benefit is lost if \u0027b\u0027 is also tested.
        // See: https://github.com/OpenZeppelin/openzeppelin-solidity/pull/522
        if (a == 0) {
            return 0;
        }
        int256 c = a * b;
        assert(c / a == b);
        return c;
    }

    /**
    * @dev Integer division of two unsigned integers, truncating the quotient.
    */
    function div(uint256 a, uint256 b) internal pure returns (uint256) {
        // assert(b \u003e 0); // Solidity automatically throws when dividing by 0
        // uint256 c = a / b;
        // assert(a == b * c + a % b); // There is no case in which this doesn\u0027t hold
        return a / b;
    }

    /**
    * @dev Integer division of two signed integers, truncating the quotient.
    */
    function div(int256 a, int256 b) internal pure returns (int256) {
        // assert(b \u003e 0); // Solidity automatically throws when dividing by 0
        // Overflow only happens when the smallest negative int is multiplied by -1.
        int256 INT256_MIN = int256((uint256(1) \u003c\u003c 255));
        assert(a != INT256_MIN || b != - 1);
        return a / b;
    }

    /**
    * @dev Subtracts two unsigned integers, throws on overflow (i.e. if subtrahend is greater than minuend).
    */
    function sub(uint256 a, uint256 b) internal pure returns (uint256) {
        assert(b \u003c= a);
        return a - b;
    }

    /**
    * @dev Subtracts two signed integers, throws on overflow.
    */
    function sub(int256 a, int256 b) internal pure returns (int256) {
        int256 c = a - b;
        assert((b \u003e= 0 \u0026\u0026 c \u003c= a) || (b \u003c 0 \u0026\u0026 c \u003e a));
        return c;
    }

    /**
    * @dev Adds two unsigned integers, throws on overflow.
    */
    function add(uint256 a, uint256 b) internal pure returns (uint256 c) {
        c = a + b;
        assert(c \u003e= a);
        return c;
    }

    /**
    * @dev Adds two signed integers, throws on overflow.
    */
    function add(int256 a, int256 b) internal pure returns (int256) {
        int256 c = a + b;
        assert((b \u003e= 0 \u0026\u0026 c \u003e= a) || (b \u003c 0 \u0026\u0026 c \u003c a));
        return c;
    }
}
"},"Utilities.sol":{"content":"pragma solidity ^0.5.0;

import \"../SafeMath.sol\";
import \"../SafeCast.sol\";


contract Utilities {
    using SafeCast for int;
    using SafeCast for uint;
    using SafeMath for int;
    using SafeMath for uint;

    uint constant public PROBABILITY_DIVISOR = 10000;
    uint constant public HOUSE_EDGE = 150;
    uint constant public HOUSE_EDGE_DIVISOR = 10000;

    /**
     * @dev Calc max bet we allow
     * We definitely do not allow bets greater than kelly criterion would allow.
     * =\u003e The max bet is limited to the max profit of houseEdge * bankroll.
     * =\u003e maxBet = houseEdge / (1/p * (1 - houseEdge) - 1) * bankroll, with p is win probability.
     * The max bet can be further restricted on backend.
     * @param _winProbability winProbability.
     * @return max allowed bet.
     */
    function maxBetFromProbability(uint _winProbability, uint _bankRoll) public pure returns(uint) {
        assert(0 \u003c _winProbability \u0026\u0026 _winProbability \u003c PROBABILITY_DIVISOR);

        uint tmp1 = PROBABILITY_DIVISOR.mul(HOUSE_EDGE_DIVISOR).div(_winProbability);
        uint tmp2 = PROBABILITY_DIVISOR.mul(HOUSE_EDGE).div(_winProbability);

        uint enumerator = HOUSE_EDGE.mul(_bankRoll);
        uint denominator = tmp1.sub(tmp2).sub(HOUSE_EDGE_DIVISOR);
        return enumerator.div(denominator);
    }

    /**
     * Calculate user profit from total won.
     * @param _totalWon user winnings.
     * @param _betValue bet value.
     * @return user profit.
     */
    function calcProfitFromTotalWon(uint _totalWon, uint _betValue) public pure returns(int) {
        uint houseEdgeValue = _totalWon.mul(HOUSE_EDGE).div(HOUSE_EDGE_DIVISOR);

        return _totalWon.castToInt().sub(houseEdgeValue.castToInt()).sub(_betValue.castToInt());
    }

    /**
     * @dev Generates a 256 bit random number by combining server and user seed.
     * @param _serverSeed server seed.
     * @param _userSeed user seed.
     * @return random number generated by combining server and user seed.
     */
    function generateRandomNumber(bytes32 _serverSeed, bytes32 _userSeed) public pure returns(uint) {
        bytes32 combinedHash = keccak256(abi.encodePacked(_serverSeed, _userSeed));
        return uint(combinedHash);
    }
}

