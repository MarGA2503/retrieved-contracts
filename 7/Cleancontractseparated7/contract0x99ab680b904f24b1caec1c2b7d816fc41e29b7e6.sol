// SPDX-License-Identifier: MIT

pragma solidity ^0.6.0;

/**
 * @dev Interface of the ERC20 standard as defined in the EIP.
 */
interface IERC20 {
    /**
     * @dev Returns the amount of tokens in existence.
     */
    function totalSupply() external view returns (uint256);

    /**
     * @dev Returns the amount of tokens owned by `account`.
     */
    function balanceOf(address account) external view returns (uint256);

    /**
     * @dev Moves `amount` tokens from the caller\u0027s account to `recipient`.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transfer(address recipient, uint256 amount) external returns (bool);

    /**
     * @dev Returns the remaining number of tokens that `spender` will be
     * allowed to spend on behalf of `owner` through {transferFrom}. This is
     * zero by default.
     *
     * This value changes when {approve} or {transferFrom} are called.
     */
    function allowance(address owner, address spender) external view returns (uint256);

    /**
     * @dev Sets `amount` as the allowance of `spender` over the caller\u0027s tokens.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * IMPORTANT: Beware that changing an allowance with this method brings the risk
     * that someone may use both the old and the new allowance by unfortunate
     * transaction ordering. One possible solution to mitigate this race
     * condition is to first reduce the spender\u0027s allowance to 0 and set the
     * desired value afterwards:
     * https://github.com/ethereum/EIPs/issues/20#issuecomment-263524729
     *
     * Emits an {Approval} event.
     */
    function approve(address spender, uint256 amount) external returns (bool);

    /**
     * @dev Moves `amount` tokens from `sender` to `recipient` using the
     * allowance mechanism. `amount` is then deducted from the caller\u0027s
     * allowance.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transferFrom(address sender, address recipient, uint256 amount) external returns (bool);

    /**
     * @dev Emitted when `value` tokens are moved from one account (`from`) to
     * another (`to`).
     *
     * Note that `value` may be zero.
     */
    event Transfer(address indexed from, address indexed to, uint256 value);

    /**
     * @dev Emitted when the allowance of a `spender` for an `owner` is set by
     * a call to {approve}. `value` is the new allowance.
     */
    event Approval(address indexed owner, address indexed spender, uint256 value);
}
"},"Ownable.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.6.0;

/**
 * @dev Contract module which provides a basic access control mechanism, where
 * there is an account (an owner) that can be granted exclusive access to
 * specific functions.
 *
 * By default, the owner account will be the one that deploys the contract. This
 * can later be changed with {transferOwnership}.
 *
 * This module is used through inheritance. It will make available the modifier
 * `onlyOwner`, which can be applied to your functions to restrict their use to
 * the owner.
 */
contract Ownable {
    address private _owner;

    event OwnershipTransferred(address indexed previousOwner, address indexed newOwner);

    /**
     * @dev Initializes the contract setting the deployer as the initial owner.
     */
    constructor () internal {
        address msgSender = msg.sender;
        _owner = msgSender;
        emit OwnershipTransferred(address(0), msgSender);
    }

    /**
     * @dev Returns the address of the current owner.
     */
    function owner() public view returns (address) {
        return _owner;
    }

    /**
     * @dev Throws if called by any account other than the owner.
     */
    modifier onlyOwner() {
        require(_owner == msg.sender, \"Ownable: caller is not the owner\");
        _;
    }

    /**
     * @dev Transfers ownership of the contract to a new account (`newOwner`).
     * Can only be called by the current owner.
     */
    function transferOwnership(address newOwner) public virtual onlyOwner {
        require(newOwner != address(0), \"Ownable: new owner is the zero address\");
        emit OwnershipTransferred(_owner, newOwner);
        _owner = newOwner;
    }
}
"},"TokenTimelock.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.6.0;

import \"./IERC20.sol\";
import \"./Ownable.sol\";

/**
 * @dev A token holder contract that will allow a beneficiary to extract the
 * tokens after a given release time.
 *
 * Useful for simple vesting schedules like \"advisors get all of their tokens
 * after 1 year\".
 */
contract FattTimelock is Ownable {

    //using SafeERC20 for IERC20;

    // ERC20 basic token contract being held
    IERC20 private _token;

    // beneficiary of tokens after they are released
    //address private _beneficiary;
\t
\tuint256 private _released;
\tuint256 private _startTime;


    constructor (IERC20 token, address beneficiary) public {
        // solhint-disable-next-line not-rely-on-time
        //require(releaseTime \u003e block.timestamp, \"TokenTimelock: release time is before current time\");
        _token = token;
\t\ttransferOwnership(beneficiary);
\t\t_startTime = block.timestamp;
    }

    /**
     * @return the token being held.
     */
    function token() public view returns (IERC20) {
        return _token;
    }

\tfunction released() public view returns (uint256) {
\t\treturn _released;
\t}
\t
\tfunction tokenBalance() public view returns (uint256) {
\t\treturn _token.balanceOf(address(this));
\t}
\t
\tfunction releasable() public view returns (uint256) {
\t\tuint16[37] memory locked = [0,1500,1792,2084,2376,2668,2960,3252,3544,3836,4128,4420,4712,6042,7084,8126,9168,10210,11252,12294,13336,14378,15420,16462,17504,18546,19588,20630,21672,22714,23756,24798,25840,26882,27924,28966,30000];
\t\tuint256 month = (block.timestamp - _startTime) / 30 days;
\t\tif (month \u003e 36) month = 36;
\t\tuint256 releasableAmount = uint256(locked[month]) * 10 ** 23;
\t\treturn (releasableAmount);
\t}
\t
    function release() public virtual onlyOwner {
\t
\t\tuint256 releasableAmount = releasable();

\t\tif (releasableAmount \u003e _released) {
\t\t
\t\t\tuint256 releaseAmount = releasableAmount - _released;
\t\t\trequire(releaseAmount \u003c= _token.balanceOf(address(this)), \"TokenTimelock: no tokens to release\");
\t\t\t_released = _released + releaseAmount;
\t\t\t_token.transfer(owner(), releaseAmount);
\t\t}
    }
}

