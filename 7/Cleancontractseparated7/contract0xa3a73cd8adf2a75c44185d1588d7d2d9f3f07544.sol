// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

/**
 * @dev Collection of functions related to the address type
 */
library Address {
    /**
     * @dev Returns true if `account` is a contract.
     *
     * [IMPORTANT]
     * ====
     * It is unsafe to assume that an address for which this function returns
     * false is an externally-owned account (EOA) and not a contract.
     *
     * Among others, `isContract` will return false for the following
     * types of addresses:
     *
     *  - an externally-owned account
     *  - a contract in construction
     *  - an address where a contract will be created
     *  - an address where a contract lived, but was destroyed
     * ====
     */
    function isContract(address account) internal view returns (bool) {
        // This method relies on extcodesize, which returns 0 for contracts in
        // construction, since the code is only stored at the end of the
        // constructor execution.

        uint256 size;
        assembly {
            size := extcodesize(account)
        }
        return size \u003e 0;
    }

    /**
     * @dev Replacement for Solidity\u0027s `transfer`: sends `amount` wei to
     * `recipient`, forwarding all available gas and reverting on errors.
     *
     * https://eips.ethereum.org/EIPS/eip-1884[EIP1884] increases the gas cost
     * of certain opcodes, possibly making contracts go over the 2300 gas limit
     * imposed by `transfer`, making them unable to receive funds via
     * `transfer`. {sendValue} removes this limitation.
     *
     * https://diligence.consensys.net/posts/2019/09/stop-using-soliditys-transfer-now/[Learn more].
     *
     * IMPORTANT: because control is transferred to `recipient`, care must be
     * taken to not create reentrancy vulnerabilities. Consider using
     * {ReentrancyGuard} or the
     * https://solidity.readthedocs.io/en/v0.5.11/security-considerations.html#use-the-checks-effects-interactions-pattern[checks-effects-interactions pattern].
     */
    function sendValue(address payable recipient, uint256 amount) internal {
        require(address(this).balance \u003e= amount, \"Address: insufficient balance\");

        (bool success, ) = recipient.call{value: amount}(\"\");
        require(success, \"Address: unable to send value, recipient may have reverted\");
    }

    /**
     * @dev Performs a Solidity function call using a low level `call`. A
     * plain `call` is an unsafe replacement for a function call: use this
     * function instead.
     *
     * If `target` reverts with a revert reason, it is bubbled up by this
     * function (like regular Solidity function calls).
     *
     * Returns the raw returned data. To convert to the expected return value,
     * use https://solidity.readthedocs.io/en/latest/units-and-global-variables.html?highlight=abi.decode#abi-encoding-and-decoding-functions[`abi.decode`].
     *
     * Requirements:
     *
     * - `target` must be a contract.
     * - calling `target` with `data` must not revert.
     *
     * _Available since v3.1._
     */
    function functionCall(address target, bytes memory data) internal returns (bytes memory) {
        return functionCall(target, data, \"Address: low-level call failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`], but with
     * `errorMessage` as a fallback revert reason when `target` reverts.
     *
     * _Available since v3.1._
     */
    function functionCall(
        address target,
        bytes memory data,
        string memory errorMessage
    ) internal returns (bytes memory) {
        return functionCallWithValue(target, data, 0, errorMessage);
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`],
     * but also transferring `value` wei to `target`.
     *
     * Requirements:
     *
     * - the calling contract must have an ETH balance of at least `value`.
     * - the called Solidity function must be `payable`.
     *
     * _Available since v3.1._
     */
    function functionCallWithValue(
        address target,
        bytes memory data,
        uint256 value
    ) internal returns (bytes memory) {
        return functionCallWithValue(target, data, value, \"Address: low-level call with value failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCallWithValue-address-bytes-uint256-}[`functionCallWithValue`], but
     * with `errorMessage` as a fallback revert reason when `target` reverts.
     *
     * _Available since v3.1._
     */
    function functionCallWithValue(
        address target,
        bytes memory data,
        uint256 value,
        string memory errorMessage
    ) internal returns (bytes memory) {
        require(address(this).balance \u003e= value, \"Address: insufficient balance for call\");
        require(isContract(target), \"Address: call to non-contract\");

        (bool success, bytes memory returndata) = target.call{value: value}(data);
        return verifyCallResult(success, returndata, errorMessage);
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`],
     * but performing a static call.
     *
     * _Available since v3.3._
     */
    function functionStaticCall(address target, bytes memory data) internal view returns (bytes memory) {
        return functionStaticCall(target, data, \"Address: low-level static call failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-string-}[`functionCall`],
     * but performing a static call.
     *
     * _Available since v3.3._
     */
    function functionStaticCall(
        address target,
        bytes memory data,
        string memory errorMessage
    ) internal view returns (bytes memory) {
        require(isContract(target), \"Address: static call to non-contract\");

        (bool success, bytes memory returndata) = target.staticcall(data);
        return verifyCallResult(success, returndata, errorMessage);
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`],
     * but performing a delegate call.
     *
     * _Available since v3.4._
     */
    function functionDelegateCall(address target, bytes memory data) internal returns (bytes memory) {
        return functionDelegateCall(target, data, \"Address: low-level delegate call failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-string-}[`functionCall`],
     * but performing a delegate call.
     *
     * _Available since v3.4._
     */
    function functionDelegateCall(
        address target,
        bytes memory data,
        string memory errorMessage
    ) internal returns (bytes memory) {
        require(isContract(target), \"Address: delegate call to non-contract\");

        (bool success, bytes memory returndata) = target.delegatecall(data);
        return verifyCallResult(success, returndata, errorMessage);
    }

    /**
     * @dev Tool to verifies that a low level call was successful, and revert if it wasn\u0027t, either by bubbling the
     * revert reason using the provided one.
     *
     * _Available since v4.3._
     */
    function verifyCallResult(
        bool success,
        bytes memory returndata,
        string memory errorMessage
    ) internal pure returns (bytes memory) {
        if (success) {
            return returndata;
        } else {
            // Look for revert reason and bubble it up if present
            if (returndata.length \u003e 0) {
                // The easiest way to bubble the revert reason is using memory via assembly

                assembly {
                    let returndata_size := mload(returndata)
                    revert(add(32, returndata), returndata_size)
                }
            } else {
                revert(errorMessage);
            }
        }
    }
}
"},"Context.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

/**
 * @dev Provides information about the current execution context, including the
 * sender of the transaction and its data. While these are generally available
 * via msg.sender and msg.data, they should not be accessed in such a direct
 * manner, since when dealing with meta-transactions the account sending and
 * paying for execution may not be the actual sender (as far as an application
 * is concerned).
 *
 * This contract is only required for intermediate, library-like contracts.
 */
abstract contract Context {
    function _msgSender() internal view virtual returns (address) {
        return msg.sender;
    }

    function _msgData() internal view virtual returns (bytes calldata) {
        return msg.data;
    }
}
"},"ERC165.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import \"./IERC165.sol\";

/**
 * @dev Implementation of the {IERC165} interface.
 *
 * Contracts that want to implement ERC165 should inherit from this contract and override {supportsInterface} to check
 * for the additional interface id that will be supported. For example:
 *
 * ```solidity
 * function supportsInterface(bytes4 interfaceId) public view virtual override returns (bool) {
 *     return interfaceId == type(MyInterface).interfaceId || super.supportsInterface(interfaceId);
 * }
 * ```
 *
 * Alternatively, {ERC165Storage} provides an easier to use but more expensive implementation.
 */
abstract contract ERC165 is IERC165 {
    /**
     * @dev See {IERC165-supportsInterface}.
     */
    function supportsInterface(bytes4 interfaceId) public view virtual override returns (bool) {
        return interfaceId == type(IERC165).interfaceId;
    }
}
"},"ERC721.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import \"./IERC721.sol\";
import \"./IERC721Receiver.sol\";
import \"./IERC721Metadata.sol\";
import \"./Address.sol\";
import \"./Context.sol\";
import \"./Strings.sol\";
import \"./ERC165.sol\";

/**
 * @dev Implementation of https://eips.ethereum.org/EIPS/eip-721[ERC721] Non-Fungible Token Standard, including
 * the Metadata extension, but not including the Enumerable extension, which is available separately as
 * {ERC721Enumerable}.
 */
contract ERC721 is Context, ERC165, IERC721, IERC721Metadata {
    using Address for address;
    using Strings for uint256;

    // Token name
    string private _name;

    // Token symbol
    string private _symbol;

    // Mapping from token ID to owner address
    mapping(uint256 =\u003e address) private _owners;

    // Mapping owner address to token count
    mapping(address =\u003e uint256) private _balances;

    // Mapping from token ID to approved address
    mapping(uint256 =\u003e address) private _tokenApprovals;

    // Mapping from owner to operator approvals
    mapping(address =\u003e mapping(address =\u003e bool)) private _operatorApprovals;

    /**
     * @dev Initializes the contract by setting a `name` and a `symbol` to the token collection.
     */
    constructor(string memory name_, string memory symbol_) {
        _name = name_;
        _symbol = symbol_;
    }

    /**
     * @dev See {IERC165-supportsInterface}.
     */
    function supportsInterface(bytes4 interfaceId) public view virtual override(ERC165, IERC165) returns (bool) {
        return
            interfaceId == type(IERC721).interfaceId ||
            interfaceId == type(IERC721Metadata).interfaceId ||
            super.supportsInterface(interfaceId);
    }

    /**
     * @dev See {IERC721-balanceOf}.
     */
    function balanceOf(address owner) public view virtual override returns (uint256) {
        require(owner != address(0), \"ERC721: balance query for the zero address\");
        return _balances[owner];
    }

    /**
     * @dev See {IERC721-ownerOf}.
     */
    function ownerOf(uint256 tokenId) public view virtual override returns (address) {
        address owner = _owners[tokenId];
        require(owner != address(0), \"ERC721: owner query for nonexistent token\");
        return owner;
    }

    /**
     * @dev See {IERC721Metadata-name}.
     */
    function name() public view virtual override returns (string memory) {
        return _name;
    }

    /**
     * @dev See {IERC721Metadata-symbol}.
     */
    function symbol() public view virtual override returns (string memory) {
        return _symbol;
    }

    /**
     * @dev See {IERC721Metadata-tokenURI}.
     */
    function tokenURI(uint256 tokenId) public view virtual override returns (string memory) {
        require(_exists(tokenId), \"ERC721Metadata: URI query for nonexistent token\");

        string memory baseURI = _baseURI();
        return bytes(baseURI).length \u003e 0 ? string(abi.encodePacked(baseURI, tokenId.toString())) : \"\";
    }

    /**
     * @dev Base URI for computing {tokenURI}. If set, the resulting URI for each
     * token will be the concatenation of the `baseURI` and the `tokenId`. Empty
     * by default, can be overriden in child contracts.
     */
    function _baseURI() internal view virtual returns (string memory) {
        return \"\";
    }

    /**
     * @dev See {IERC721-approve}.
     */
    function approve(address to, uint256 tokenId) public virtual override {
        address owner = ERC721.ownerOf(tokenId);
        require(to != owner, \"ERC721: approval to current owner\");

        require(
            _msgSender() == owner || isApprovedForAll(owner, _msgSender()),
            \"ERC721: approve caller is not owner nor approved for all\"
        );

        _approve(to, tokenId);
    }

    /**
     * @dev See {IERC721-getApproved}.
     */
    function getApproved(uint256 tokenId) public view virtual override returns (address) {
        require(_exists(tokenId), \"ERC721: approved query for nonexistent token\");

        return _tokenApprovals[tokenId];
    }

    /**
     * @dev See {IERC721-setApprovalForAll}.
     */
    function setApprovalForAll(address operator, bool approved) public virtual override {
        _setApprovalForAll(_msgSender(), operator, approved);
    }

    /**
     * @dev See {IERC721-isApprovedForAll}.
     */
    function isApprovedForAll(address owner, address operator) public view virtual override returns (bool) {
        return _operatorApprovals[owner][operator];
    }

    /**
     * @dev See {IERC721-transferFrom}.
     */
    function transferFrom(
        address from,
        address to,
        uint256 tokenId
    ) public virtual override {
        //solhint-disable-next-line max-line-length
        require(_isApprovedOrOwner(_msgSender(), tokenId), \"ERC721: transfer caller is not owner nor approved\");

        _transfer(from, to, tokenId);
    }

    /**
     * @dev See {IERC721-safeTransferFrom}.
     */
    function safeTransferFrom(
        address from,
        address to,
        uint256 tokenId
    ) public virtual override {
        safeTransferFrom(from, to, tokenId, \"\");
    }

    /**
     * @dev See {IERC721-safeTransferFrom}.
     */
    function safeTransferFrom(
        address from,
        address to,
        uint256 tokenId,
        bytes memory _data
    ) public virtual override {
        require(_isApprovedOrOwner(_msgSender(), tokenId), \"ERC721: transfer caller is not owner nor approved\");
        _safeTransfer(from, to, tokenId, _data);
    }

    /**
     * @dev Safely transfers `tokenId` token from `from` to `to`, checking first that contract recipients
     * are aware of the ERC721 protocol to prevent tokens from being forever locked.
     *
     * `_data` is additional data, it has no specified format and it is sent in call to `to`.
     *
     * This internal function is equivalent to {safeTransferFrom}, and can be used to e.g.
     * implement alternative mechanisms to perform token transfer, such as signature-based.
     *
     * Requirements:
     *
     * - `from` cannot be the zero address.
     * - `to` cannot be the zero address.
     * - `tokenId` token must exist and be owned by `from`.
     * - If `to` refers to a smart contract, it must implement {IERC721Receiver-onERC721Received}, which is called upon a safe transfer.
     *
     * Emits a {Transfer} event.
     */
    function _safeTransfer(
        address from,
        address to,
        uint256 tokenId,
        bytes memory _data
    ) internal virtual {
        _transfer(from, to, tokenId);
        require(_checkOnERC721Received(from, to, tokenId, _data), \"ERC721: transfer to non ERC721Receiver implementer\");
    }

    /**
     * @dev Returns whether `tokenId` exists.
     *
     * Tokens can be managed by their owner or approved accounts via {approve} or {setApprovalForAll}.
     *
     * Tokens start existing when they are minted (`_mint`),
     * and stop existing when they are burned (`_burn`).
     */
    function _exists(uint256 tokenId) internal view virtual returns (bool) {
        return _owners[tokenId] != address(0);
    }

    /**
     * @dev Returns whether `spender` is allowed to manage `tokenId`.
     *
     * Requirements:
     *
     * - `tokenId` must exist.
     */
    function _isApprovedOrOwner(address spender, uint256 tokenId) internal view virtual returns (bool) {
        require(_exists(tokenId), \"ERC721: operator query for nonexistent token\");
        address owner = ERC721.ownerOf(tokenId);
        return (spender == owner || getApproved(tokenId) == spender || isApprovedForAll(owner, spender));
    }

    /**
     * @dev Safely mints `tokenId` and transfers it to `to`.
     *
     * Requirements:
     *
     * - `tokenId` must not exist.
     * - If `to` refers to a smart contract, it must implement {IERC721Receiver-onERC721Received}, which is called upon a safe transfer.
     *
     * Emits a {Transfer} event.
     */
    function _safeMint(address to, uint256 tokenId) internal virtual {
        _safeMint(to, tokenId, \"\");
    }

    /**
     * @dev Same as {xref-ERC721-_safeMint-address-uint256-}[`_safeMint`], with an additional `data` parameter which is
     * forwarded in {IERC721Receiver-onERC721Received} to contract recipients.
     */
    function _safeMint(
        address to,
        uint256 tokenId,
        bytes memory _data
    ) internal virtual {
        _mint(to, tokenId);
        require(
            _checkOnERC721Received(address(0), to, tokenId, _data),
            \"ERC721: transfer to non ERC721Receiver implementer\"
        );
    }

    /**
     * @dev Mints `tokenId` and transfers it to `to`.
     *
     * WARNING: Usage of this method is discouraged, use {_safeMint} whenever possible
     *
     * Requirements:
     *
     * - `tokenId` must not exist.
     * - `to` cannot be the zero address.
     *
     * Emits a {Transfer} event.
     */
    function _mint(address to, uint256 tokenId) internal virtual {
        require(to != address(0), \"ERC721: mint to the zero address\");
        require(!_exists(tokenId), \"ERC721: token already minted\");

        _beforeTokenTransfer(address(0), to, tokenId);

        _balances[to] += 1;
        _owners[tokenId] = to;

        emit Transfer(address(0), to, tokenId);
    }

    /**
     * @dev Destroys `tokenId`.
     * The approval is cleared when the token is burned.
     *
     * Requirements:
     *
     * - `tokenId` must exist.
     *
     * Emits a {Transfer} event.
     */
    function _burn(uint256 tokenId) internal virtual {
        address owner = ERC721.ownerOf(tokenId);

        _beforeTokenTransfer(owner, address(0), tokenId);

        // Clear approvals
        _approve(address(0), tokenId);

        _balances[owner] -= 1;
        delete _owners[tokenId];

        emit Transfer(owner, address(0), tokenId);
    }

    /**
     * @dev Transfers `tokenId` from `from` to `to`.
     *  As opposed to {transferFrom}, this imposes no restrictions on msg.sender.
     *
     * Requirements:
     *
     * - `to` cannot be the zero address.
     * - `tokenId` token must be owned by `from`.
     *
     * Emits a {Transfer} event.
     */
    function _transfer(
        address from,
        address to,
        uint256 tokenId
    ) internal virtual {
        require(ERC721.ownerOf(tokenId) == from, \"ERC721: transfer of token that is not own\");
        require(to != address(0), \"ERC721: transfer to the zero address\");

        _beforeTokenTransfer(from, to, tokenId);

        // Clear approvals from the previous owner
        _approve(address(0), tokenId);

        _balances[from] -= 1;
        _balances[to] += 1;
        _owners[tokenId] = to;

        emit Transfer(from, to, tokenId);
    }

    /**
     * @dev Approve `to` to operate on `tokenId`
     *
     * Emits a {Approval} event.
     */
    function _approve(address to, uint256 tokenId) internal virtual {
        _tokenApprovals[tokenId] = to;
        emit Approval(ERC721.ownerOf(tokenId), to, tokenId);
    }

    /**
     * @dev Approve `operator` to operate on all of `owner` tokens
     *
     * Emits a {ApprovalForAll} event.
     */
    function _setApprovalForAll(
        address owner,
        address operator,
        bool approved
    ) internal virtual {
        require(owner != operator, \"ERC721: approve to caller\");
        _operatorApprovals[owner][operator] = approved;
        emit ApprovalForAll(owner, operator, approved);
    }

    /**
     * @dev Internal function to invoke {IERC721Receiver-onERC721Received} on a target address.
     * The call is not executed if the target address is not a contract.
     *
     * @param from address representing the previous owner of the given token ID
     * @param to target address that will receive the tokens
     * @param tokenId uint256 ID of the token to be transferred
     * @param _data bytes optional data to send along with the call
     * @return bool whether the call correctly returned the expected magic value
     */
    function _checkOnERC721Received(
        address from,
        address to,
        uint256 tokenId,
        bytes memory _data
    ) private returns (bool) {
        if (to.isContract()) {
            try IERC721Receiver(to).onERC721Received(_msgSender(), from, tokenId, _data) returns (bytes4 retval) {
                return retval == IERC721Receiver.onERC721Received.selector;
            } catch (bytes memory reason) {
                if (reason.length == 0) {
                    revert(\"ERC721: transfer to non ERC721Receiver implementer\");
                } else {
                    assembly {
                        revert(add(32, reason), mload(reason))
                    }
                }
            }
        } else {
            return true;
        }
    }

    /**
     * @dev Hook that is called before any token transfer. This includes minting
     * and burning.
     *
     * Calling conditions:
     *
     * - When `from` and `to` are both non-zero, ``from``\u0027s `tokenId` will be
     * transferred to `to`.
     * - When `from` is zero, `tokenId` will be minted for `to`.
     * - When `to` is zero, ``from``\u0027s `tokenId` will be burned.
     * - `from` and `to` are never both zero.
     *
     * To learn more about hooks, head to xref:ROOT:extending-contracts.adoc#using-hooks[Using Hooks].
     */
    function _beforeTokenTransfer(
        address from,
        address to,
        uint256 tokenId
    ) internal virtual {}
}
"},"ERC721Enumerable.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import \"./ERC721.sol\";
import \"./IERC721Enumerable.sol\";

/**
 * @dev This implements an optional extension of {ERC721} defined in the EIP that adds
 * enumerability of all the token ids in the contract as well as all token ids owned by each
 * account.
 */
abstract contract ERC721Enumerable is ERC721, IERC721Enumerable {
    // Mapping from owner to list of owned token IDs
    mapping(address =\u003e mapping(uint256 =\u003e uint256)) private _ownedTokens;

    // Mapping from token ID to index of the owner tokens list
    mapping(uint256 =\u003e uint256) private _ownedTokensIndex;

    // Array with all token ids, used for enumeration
    uint256[] private _allTokens;

    // Mapping from token id to position in the allTokens array
    mapping(uint256 =\u003e uint256) private _allTokensIndex;

    /**
     * @dev See {IERC165-supportsInterface}.
     */
    function supportsInterface(bytes4 interfaceId) public view virtual override(IERC165, ERC721) returns (bool) {
        return interfaceId == type(IERC721Enumerable).interfaceId || super.supportsInterface(interfaceId);
    }

    /**
     * @dev See {IERC721Enumerable-tokenOfOwnerByIndex}.
     */
    function tokenOfOwnerByIndex(address owner, uint256 index) public view virtual override returns (uint256) {
        require(index \u003c ERC721.balanceOf(owner), \"ERC721Enumerable: owner index out of bounds\");
        return _ownedTokens[owner][index];
    }

    /**
     * @dev See {IERC721Enumerable-totalSupply}.
     */
    function totalSupply() public view virtual override returns (uint256) {
        return _allTokens.length;
    }

    /**
     * @dev See {IERC721Enumerable-tokenByIndex}.
     */
    function tokenByIndex(uint256 index) public view virtual override returns (uint256) {
        require(index \u003c ERC721Enumerable.totalSupply(), \"ERC721Enumerable: global index out of bounds\");
        return _allTokens[index];
    }

    /**
     * @dev Hook that is called before any token transfer. This includes minting
     * and burning.
     *
     * Calling conditions:
     *
     * - When `from` and `to` are both non-zero, ``from``\u0027s `tokenId` will be
     * transferred to `to`.
     * - When `from` is zero, `tokenId` will be minted for `to`.
     * - When `to` is zero, ``from``\u0027s `tokenId` will be burned.
     * - `from` cannot be the zero address.
     * - `to` cannot be the zero address.
     *
     * To learn more about hooks, head to xref:ROOT:extending-contracts.adoc#using-hooks[Using Hooks].
     */
    function _beforeTokenTransfer(
        address from,
        address to,
        uint256 tokenId
    ) internal virtual override {
        super._beforeTokenTransfer(from, to, tokenId);

        if (from == address(0)) {
            _addTokenToAllTokensEnumeration(tokenId);
        } else if (from != to) {
            _removeTokenFromOwnerEnumeration(from, tokenId);
        }
        if (to == address(0)) {
            _removeTokenFromAllTokensEnumeration(tokenId);
        } else if (to != from) {
            _addTokenToOwnerEnumeration(to, tokenId);
        }
    }

    /**
     * @dev Private function to add a token to this extension\u0027s ownership-tracking data structures.
     * @param to address representing the new owner of the given token ID
     * @param tokenId uint256 ID of the token to be added to the tokens list of the given address
     */
    function _addTokenToOwnerEnumeration(address to, uint256 tokenId) private {
        uint256 length = ERC721.balanceOf(to);
        _ownedTokens[to][length] = tokenId;
        _ownedTokensIndex[tokenId] = length;
    }

    /**
     * @dev Private function to add a token to this extension\u0027s token tracking data structures.
     * @param tokenId uint256 ID of the token to be added to the tokens list
     */
    function _addTokenToAllTokensEnumeration(uint256 tokenId) private {
        _allTokensIndex[tokenId] = _allTokens.length;
        _allTokens.push(tokenId);
    }

    /**
     * @dev Private function to remove a token from this extension\u0027s ownership-tracking data structures. Note that
     * while the token is not assigned a new owner, the `_ownedTokensIndex` mapping is _not_ updated: this allows for
     * gas optimizations e.g. when performing a transfer operation (avoiding double writes).
     * This has O(1) time complexity, but alters the order of the _ownedTokens array.
     * @param from address representing the previous owner of the given token ID
     * @param tokenId uint256 ID of the token to be removed from the tokens list of the given address
     */
    function _removeTokenFromOwnerEnumeration(address from, uint256 tokenId) private {
        // To prevent a gap in from\u0027s tokens array, we store the last token in the index of the token to delete, and
        // then delete the last slot (swap and pop).

        uint256 lastTokenIndex = ERC721.balanceOf(from) - 1;
        uint256 tokenIndex = _ownedTokensIndex[tokenId];

        // When the token to delete is the last token, the swap operation is unnecessary
        if (tokenIndex != lastTokenIndex) {
            uint256 lastTokenId = _ownedTokens[from][lastTokenIndex];

            _ownedTokens[from][tokenIndex] = lastTokenId; // Move the last token to the slot of the to-delete token
            _ownedTokensIndex[lastTokenId] = tokenIndex; // Update the moved token\u0027s index
        }

        // This also deletes the contents at the last position of the array
        delete _ownedTokensIndex[tokenId];
        delete _ownedTokens[from][lastTokenIndex];
    }

    /**
     * @dev Private function to remove a token from this extension\u0027s token tracking data structures.
     * This has O(1) time complexity, but alters the order of the _allTokens array.
     * @param tokenId uint256 ID of the token to be removed from the tokens list
     */
    function _removeTokenFromAllTokensEnumeration(uint256 tokenId) private {
        // To prevent a gap in the tokens array, we store the last token in the index of the token to delete, and
        // then delete the last slot (swap and pop).

        uint256 lastTokenIndex = _allTokens.length - 1;
        uint256 tokenIndex = _allTokensIndex[tokenId];

        // When the token to delete is the last token, the swap operation is unnecessary. However, since this occurs so
        // rarely (when the last minted token is burnt) that we still do the swap here to avoid the gas cost of adding
        // an \u0027if\u0027 statement (like in _removeTokenFromOwnerEnumeration)
        uint256 lastTokenId = _allTokens[lastTokenIndex];

        _allTokens[tokenIndex] = lastTokenId; // Move the last token to the slot of the to-delete token
        _allTokensIndex[lastTokenId] = tokenIndex; // Update the moved token\u0027s index

        // This also deletes the contents at the last position of the array
        delete _allTokensIndex[tokenId];
        _allTokens.pop();
    }
}
"},"HasFile.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

/**
 * @dev Fully On-Chain File Access Interface -- Single file per token variant
 * Interface for contracts which expose a single file for each of their tokens.
 * Originally created for the NonagonCup.
 */
 interface HasFile {

   /**
    * @dev Each file must have a filename including the file extension, for example:  \"my-file-1.stl\"
    * If the contract is an NFT where the object of the NFT is the exposed files then the filenames
    * within the contract must be unique and must include the tokenId that they belong to.
    */
   function getFilename(uint256 tokenId) external view returns(string memory filename);

   /**
    * @dev Single call to get the full file contents.
    * NB: This may not work in all conditions due to gas limits, it is instead
    * recommended to call getFileChunksTotal and iterate calls to getFileChunk.
    */
   function getFullFile(uint256 tokenId) external view returns(bytes memory fileContents);


   /**
    * @dev Each file has zero or more fileChunks.
    */
   function getFileChunksTotal(uint256 tokenId) external view returns(uint256 count);


   /**
    * @dev Use repeated calls to getFileChunk to get the binary data for the file.
    *  -- Size of returned data for each fileChunk must be no more than 1024 bytes.
    *  -- First fileChunk should be the file header, if the file format has one.
    */
   function getFileChunk(uint256 tokenId, uint256 index) external view returns(bytes memory data);

 }
"},"IERC165.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

/**
 * @dev Interface of the ERC165 standard, as defined in the
 * https://eips.ethereum.org/EIPS/eip-165[EIP].
 *
 * Implementers can declare support of contract interfaces, which can then be
 * queried by others ({ERC165Checker}).
 *
 * For an implementation, see {ERC165}.
 */
interface IERC165 {
    /**
     * @dev Returns true if this contract implements the interface defined by
     * `interfaceId`. See the corresponding
     * https://eips.ethereum.org/EIPS/eip-165#how-interfaces-are-identified[EIP section]
     * to learn more about how these ids are created.
     *
     * This function call must use less than 30 000 gas.
     */
    function supportsInterface(bytes4 interfaceId) external view returns (bool);
}
"},"IERC721.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import \"./IERC165.sol\";

/**
 * @dev Required interface of an ERC721 compliant contract.
 */
interface IERC721 is IERC165 {
    /**
     * @dev Emitted when `tokenId` token is transferred from `from` to `to`.
     */
    event Transfer(address indexed from, address indexed to, uint256 indexed tokenId);

    /**
     * @dev Emitted when `owner` enables `approved` to manage the `tokenId` token.
     */
    event Approval(address indexed owner, address indexed approved, uint256 indexed tokenId);

    /**
     * @dev Emitted when `owner` enables or disables (`approved`) `operator` to manage all of its assets.
     */
    event ApprovalForAll(address indexed owner, address indexed operator, bool approved);

    /**
     * @dev Returns the number of tokens in ``owner``\u0027s account.
     */
    function balanceOf(address owner) external view returns (uint256 balance);

    /**
     * @dev Returns the owner of the `tokenId` token.
     *
     * Requirements:
     *
     * - `tokenId` must exist.
     */
    function ownerOf(uint256 tokenId) external view returns (address owner);

    /**
     * @dev Safely transfers `tokenId` token from `from` to `to`, checking first that contract recipients
     * are aware of the ERC721 protocol to prevent tokens from being forever locked.
     *
     * Requirements:
     *
     * - `from` cannot be the zero address.
     * - `to` cannot be the zero address.
     * - `tokenId` token must exist and be owned by `from`.
     * - If the caller is not `from`, it must be have been allowed to move this token by either {approve} or {setApprovalForAll}.
     * - If `to` refers to a smart contract, it must implement {IERC721Receiver-onERC721Received}, which is called upon a safe transfer.
     *
     * Emits a {Transfer} event.
     */
    function safeTransferFrom(
        address from,
        address to,
        uint256 tokenId
    ) external;

    /**
     * @dev Transfers `tokenId` token from `from` to `to`.
     *
     * WARNING: Usage of this method is discouraged, use {safeTransferFrom} whenever possible.
     *
     * Requirements:
     *
     * - `from` cannot be the zero address.
     * - `to` cannot be the zero address.
     * - `tokenId` token must be owned by `from`.
     * - If the caller is not `from`, it must be approved to move this token by either {approve} or {setApprovalForAll}.
     *
     * Emits a {Transfer} event.
     */
    function transferFrom(
        address from,
        address to,
        uint256 tokenId
    ) external;

    /**
     * @dev Gives permission to `to` to transfer `tokenId` token to another account.
     * The approval is cleared when the token is transferred.
     *
     * Only a single account can be approved at a time, so approving the zero address clears previous approvals.
     *
     * Requirements:
     *
     * - The caller must own the token or be an approved operator.
     * - `tokenId` must exist.
     *
     * Emits an {Approval} event.
     */
    function approve(address to, uint256 tokenId) external;

    /**
     * @dev Returns the account approved for `tokenId` token.
     *
     * Requirements:
     *
     * - `tokenId` must exist.
     */
    function getApproved(uint256 tokenId) external view returns (address operator);

    /**
     * @dev Approve or remove `operator` as an operator for the caller.
     * Operators can call {transferFrom} or {safeTransferFrom} for any token owned by the caller.
     *
     * Requirements:
     *
     * - The `operator` cannot be the caller.
     *
     * Emits an {ApprovalForAll} event.
     */
    function setApprovalForAll(address operator, bool _approved) external;

    /**
     * @dev Returns if the `operator` is allowed to manage all of the assets of `owner`.
     *
     * See {setApprovalForAll}
     */
    function isApprovedForAll(address owner, address operator) external view returns (bool);

    /**
     * @dev Safely transfers `tokenId` token from `from` to `to`.
     *
     * Requirements:
     *
     * - `from` cannot be the zero address.
     * - `to` cannot be the zero address.
     * - `tokenId` token must exist and be owned by `from`.
     * - If the caller is not `from`, it must be approved to move this token by either {approve} or {setApprovalForAll}.
     * - If `to` refers to a smart contract, it must implement {IERC721Receiver-onERC721Received}, which is called upon a safe transfer.
     *
     * Emits a {Transfer} event.
     */
    function safeTransferFrom(
        address from,
        address to,
        uint256 tokenId,
        bytes calldata data
    ) external;
}
"},"IERC721Enumerable.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import \"./IERC721.sol\";

/**
 * @title ERC-721 Non-Fungible Token Standard, optional enumeration extension
 * @dev See https://eips.ethereum.org/EIPS/eip-721
 */
interface IERC721Enumerable is IERC721 {
    /**
     * @dev Returns the total amount of tokens stored by the contract.
     */
    function totalSupply() external view returns (uint256);

    /**
     * @dev Returns a token ID owned by `owner` at a given `index` of its token list.
     * Use along with {balanceOf} to enumerate all of ``owner``\u0027s tokens.
     */
    function tokenOfOwnerByIndex(address owner, uint256 index) external view returns (uint256 tokenId);

    /**
     * @dev Returns a token ID at a given `index` of all the tokens stored by the contract.
     * Use along with {totalSupply} to enumerate all tokens.
     */
    function tokenByIndex(uint256 index) external view returns (uint256);
}
"},"IERC721Metadata.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import \"./IERC721.sol\";

/**
 * @title ERC-721 Non-Fungible Token Standard, optional metadata extension
 * @dev See https://eips.ethereum.org/EIPS/eip-721
 */
interface IERC721Metadata is IERC721 {
    /**
     * @dev Returns the token collection name.
     */
    function name() external view returns (string memory);

    /**
     * @dev Returns the token collection symbol.
     */
    function symbol() external view returns (string memory);

    /**
     * @dev Returns the Uniform Resource Identifier (URI) for `tokenId` token.
     */
    function tokenURI(uint256 tokenId) external view returns (string memory);
}
"},"IERC721Receiver.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

/**
 * @title ERC721 token receiver interface
 * @dev Interface for any contract that wants to support safeTransfers
 * from ERC721 asset contracts.
 */
interface IERC721Receiver {
    /**
     * @dev Whenever an {IERC721} `tokenId` token is transferred to this contract via {IERC721-safeTransferFrom}
     * by `operator` from `from`, this function is called.
     *
     * It must return its Solidity selector to confirm the token transfer.
     * If any other value is returned or the interface is not implemented by the recipient, the transfer will be reverted.
     *
     * The selector can be obtained in Solidity with `IERC721.onERC721Received.selector`.
     */
    function onERC721Received(
        address operator,
        address from,
        uint256 tokenId,
        bytes calldata data
    ) external returns (bytes4);
}
"},"Migrations.sol":{"content":"// SPDX-License-Identifier: MIT
pragma solidity \u003e=0.4.22 \u003c0.9.0;

contract Migrations {
  address public owner = msg.sender;
  uint public last_completed_migration;

  modifier restricted() {
    require(
      msg.sender == owner,
      \"This function is restricted to the contract\u0027s owner\"
    );
    _;
  }

  function setCompleted(uint completed) public restricted {
    last_completed_migration = completed;
  }
}
"},"NonagonCup.sol":{"content":"/**
 *  NonagonCup
 *  An ERC721 NFT consisting of on-chain STL files describing unique nine-sided cups
 *  by Jacob Robbins
*/
// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import \"./ERC721Enumerable.sol\";
import \"./Ownable.sol\";
import \"./ReentrancyGuard.sol\";
import \"./HasFile.sol\";


contract NonagonCup is ERC721Enumerable, ReentrancyGuard, Ownable, HasFile {
  using Strings for uint256;

  bytes32 private constant header = hex\"53544C42204154462031302E31302E302E3133393120434F4C4F523DA0A0A0FF\";
  uint32 private constant facetCount = 68;
  uint8 private constant heightProgVal = 254;
  uint8 private constant thickProgVal = 253;

  // Era 1
  bytes4 private constant thick0 = hex\u0027000080C0\u0027;
  bytes private constant verts0 = hex\u0027F095DBC123E09A40000000000B538FC1AECEAA41B519C1C121F95EC1CB8518C1B786D1C1CB851841B519C141F095DB410B538F41000020A721F9DE41DF449AC1C5D9B741975AECC1C7B3A6409AD8CFC1000070C16F2B24C1B786E1C16F2B2441DF449A410000F041975AEC419AD8CF41\u0027;
  bytes private constant prog0 = hex\u00270001020304020001FE0001FE0304020304FE0506020001020506FE0506FE0001020001FE0708020506020708FE0708FE0506020506FE0908020708020908FE0908FE0708020708FE0A06020908020A06FE0A06FE0908020908FE0B01020A06020B01FE0B01FE0A06020A06FE0C04020B01020C04FE0C04FE0B01020B01FE0D0E020C04020D0EFE0D0EFE0C04020C04FE0304020D0E020304FE0304FE0D0E020D0EFE0D0EFE0F10FE0304FE0304FE0F10FE1112FE0304FE1112FE0001FE0001FE1112FE0506FE0506FE1112FE1314FE0506FE1314FE0708FE0708FE1314FE1516FE0708FE1516FE1716FE0C04FE1810FE0D0EFE0D0EFE1810FE0219FE0D0EFE0219FE0F10FE1810FE0C04FE1A12FE1A12FE0C04FE0B01FE1A12FE0B01FE0A06FE1A12FE0A06FE1B14FE1B14FE0A06FE0908FE1B14FE0908FE1716FE1716FE0908FE0708FE0219FD0F10FD0219FE0219FE0F10FD0F10FE0F10FD1112FD0F10FE0F10FE1112FD1112FE1112FD1314FD1112FE1112FE1314FD1314FE1314FD1516FD1314FE1314FE1516FD1516FE1516FD1716FD1516FE1516FE1716FD1716FE1716FD1B14FD1716FE1716FE1B14FD1B14FE1B14FD1A12FD1B14FE1B14FE1A12FD1A12FE1A12FD1810FD1A12FE1A12FE1810FD1810FE1810FD0219FD1810FE1810FE0219FD0219FE0001020B01020304020304020B01020C04020304020C04020D0E020B01020001020A06020A06020001020506020A06020506020908020908020506020708020219FD1810FD0F10FD0F10FD1810FD1112FD1112FD1810FD1A12FD1112FD1A12FD1314FD1314FD1A12FD1B14FD1314FD1B14FD1516FD1516FD1B14FD1716FD\u0027;

  // Era 2
  bytes4 private constant thick1 = hex\u00270000FEC0\u0027;
  bytes private constant verts1 = hex\u0027C3131CC23C2ADC40000000008BBECBC122D0F241824009C2257C9EC1F7D158C158ED14C2F7D1584182400942C3131C428BBECB41257C1E42731EDDC178C203420E6329C29DF0EE40D5F414C20000ACC1534F6BC18CA021C2534F6B41731EDD4100002C420E632942D5F41442\u0027;
  bytes private constant prog1 = hex\u00270001020304020001FE0001FE0304020304FE0506020001020506FE0506FE0001020001FE0708020506020708FE0708FE0506020506FE0908020708020908FE0908FE0708020708FE0A06020908020A06FE0A06FE0908020908FE0B01020A06020B01FE0B01FE0A06020A06FE0C04020B01020C04FE0C04FE0B01020B01FE020D020C0402020DFE020DFE0C04020C04FE030402020D020304FE0304FE020D02020DFE020DFE0E0FFE0304FE0304FE0E0FFE1011FE0304FE1011FE0001FE0001FE1011FE0506FE0506FE1011FE1213FE0506FE1213FE0708FE0708FE1213FE1415FE0708FE1415FE1615FE0C04FE170FFE020DFE020DFE170FFE0218FE020DFE0218FE0E0FFE170FFE0C04FE1911FE1911FE0C04FE0B01FE1911FE0B01FE0A06FE1911FE0A06FE1A13FE1A13FE0A06FE0908FE1A13FE0908FE1615FE1615FE0908FE0708FE0218FD0E0FFD0218FE0218FE0E0FFD0E0FFE0E0FFD1011FD0E0FFE0E0FFE1011FD1011FE1011FD1213FD1011FE1011FE1213FD1213FE1213FD1415FD1213FE1213FE1415FD1415FE1415FD1615FD1415FE1415FE1615FD1615FE1615FD1A13FD1615FE1615FE1A13FD1A13FE1A13FD1911FD1A13FE1A13FE1911FD1911FE1911FD170FFD1911FE1911FE170FFD170FFE170FFD0218FD170FFE170FFE0218FD0218FE0001020B01020304020304020B01020C04020304020C0402020D020B01020001020A06020A06020001020506020A06020506020908020908020506020708020218FD170FFD0E0FFD0E0FFD170FFD1011FD1011FD170FFD1911FD1011FD1911FD1213FD1213FD1911FD1A13FD1213FD1A13FD1415FD1415FD1A13FD1615FD\u0027;

  // Era 3
  bytes4 private constant thick2 = hex\u0027000048C1\u0027;
  bytes private constant verts2 = hex\u00273BE137C21FB1014100000000BD09F0C179080F4281B321C269B7BAC172717FC1C1742FC272717F4181B321423BE13742BD09F0410000A02769B73A4228B305C256561F420BD74CC2AC7910411F2234C20000D0C1C7478EC1C17443C2C7478E4128B30542000050420BD74C421F223442\u0027;
  bytes private constant prog2 = hex\u00270001020304020001FE0001FE0304020304FE0506020001020506FE0506FE0001020001FE0708020506020708FE0708FE0506020506FE0908020708020908FE0908FE0708020708FE0A06020908020A06FE0A06FE0908020908FE0B01020A06020B01FE0B01FE0A06020A06FE0C04020B01020C04FE0C04FE0B01020B01FE0D0E020C04020D0EFE0D0EFE0C04020C04FE0304020D0E020304FE0304FE0D0E020D0EFE0D0EFE0F10FE0304FE0304FE0F10FE1112FE0304FE1112FE0001FE0001FE1112FE0506FE0506FE1112FE1314FE0506FE1314FE0708FE0708FE1314FE1516FE0708FE1516FE1716FE0C04FE1810FE0D0EFE0D0EFE1810FE0219FE0D0EFE0219FE0F10FE1810FE0C04FE1A12FE1A12FE0C04FE0B01FE1A12FE0B01FE0A06FE1A12FE0A06FE1B14FE1B14FE0A06FE0908FE1B14FE0908FE1716FE1716FE0908FE0708FE0219FD0F10FD0219FE0219FE0F10FD0F10FE0F10FD1112FD0F10FE0F10FE1112FD1112FE1112FD1314FD1112FE1112FE1314FD1314FE1314FD1516FD1314FE1314FE1516FD1516FE1516FD1716FD1516FE1516FE1716FD1716FE1716FD1B14FD1716FE1716FE1B14FD1B14FE1B14FD1A12FD1B14FE1B14FE1A12FD1A12FE1A12FD1810FD1A12FE1A12FE1810FD1810FE1810FD0219FD1810FE1810FE0219FD0219FE0001020B01020304020304020B01020C04020304020C04020D0E020B01020001020A06020A06020001020506020A06020506020908020908020506020708020219FD1810FD0F10FD0F10FD1810FD1112FD1112FD1810FD1A12FD1112FD1A12FD1314FD1314FD1A12FD1B14FD1314FD1B14FD1516FD1516FD1B14FD1716FD\u0027;

  bool private _mintApprovalRequired = true;

  mapping(address =\u003e bool) private _allowedToMint;

  mapping(uint256 =\u003e string) private _ownerSetURIs;

  constructor() ERC721(\"NonagonCup\", \"NON\") Ownable() {}

  event Supported(
    address indexed supported,
    uint32 amount
  );

  event AllowedToMint(
    address indexed recipient,
    bool allowed
  );

  receive() external payable {
    emit Supported(_msgSender(), uint32(msg.value));
  }

  function allowedToMint(address a) public view returns(bool) {
    return(_allowedToMint[a]);
  }

  // when minting approval is required, accounts can mint when they have been added to the allowed mapping
  function claim() public nonReentrant {
    if (_mintApprovalRequired) {
      require(_allowedToMint[_msgSender()], \u0027address not allowed\u0027);
    }
    uint256 tokenId = 1 + totalSupply();
    require(tokenId \u003c 2001);
    _allowedToMint[_msgSender()] = false;
    _safeMint(_msgSender(), tokenId);
  }

  // owner can add accounts to the allowed mapping
  function updateAllowedToMint(address[] calldata toAddresses, bool isAllowed) public onlyOwner {
    address curr;
    for (uint256 i=0; i \u003c toAddresses.length; i++) {
      curr = toAddresses[i];
      require(curr !=  address(0));
      _allowedToMint[curr] = isAllowed;
      emit AllowedToMint(curr, isAllowed);
    }
  }

  // owner can airdop to any address
  function mintFullRide(address to) public onlyOwner {
    uint256 tokenId = 1 + totalSupply();
    require(tokenId \u003c 2001);
    _safeMint(to, tokenId);
  }

  // owner can withdraw funds, partially motivated by need to carry out admin actions
  function withdrawFunds(uint256 amount) public onlyOwner {
    payable(owner()).transfer(amount);
  }

  function setMintApprovalRequired(bool val) public onlyOwner {
    _mintApprovalRequired = val;
  }

  function _baseURI() internal pure override returns (string memory) {
    return \"https://nonagoncup.com/tokenId/\";
  }

  // token owners can set their token\u0027s URI to point to an external URL
  // Note that it must return JSON matching the ERC-721 Non-Fungible Token Standard, optional metadata extension
  // See https://eips.ethereum.org/EIPS/eip-721
  function ownerSetTokenURI(uint256 tokenId, string calldata userSuppliedURI) public {
    require(_msgSender() == ownerOf(tokenId));
    _ownerSetURIs[tokenId] = userSuppliedURI;
  }

  function tokenURI(uint256 tokenId) override public view returns (string memory) {
    bytes memory userSetURI = bytes(_ownerSetURIs[tokenId]);
    if (userSetURI.length != 0) {
      return(_ownerSetURIs[tokenId]);
    }
    return(super.tokenURI(tokenId));
  }

  // Implementation of the HasFile interface

  function getFilename(uint256 tokenId) public pure override returns(string memory filename) {
    require(tokenId \u003e 0 \u0026\u0026 tokenId \u003c 2001, \"Token ID invalid\");
    filename = string(abi.encodePacked(\"NonagonCup-\", tokenId.toString(), \".stl\" ));
  }

  function getFullFile(uint256 tokenId) public pure override returns(bytes memory fileContents) {
    for (uint256 i=0; i \u003c 10; i++) {
      fileContents = bytes.concat(fileContents, getFileChunk(tokenId, i));
    }
  }

  function getFileChunksTotal(uint256) public pure override returns(uint256 count) {
    count = 10;
  }

  function getFileChunk(uint256 tokenId, uint256 index) public pure override returns(bytes memory data) {
    require(tokenId \u003e 0 \u0026\u0026 tokenId \u003c 2001, \"Token ID invalid\");
    require(index \u003c 10, \"index too large\");
    if (index == 0) {
      // first chunk is header
      // STL File Header -- https://en.wikipedia.org/wiki/STL_(file_format)
      // 80 bytes -- descriptive text
      // 4 bytes -- little-endian unsigned int holding the total number of triangles
      bytes8 _c = hex\"2020202020202020\";
      data = bytes.concat(header, _c, _c, _c, _c, _c, _c, bytes4(facetCount \u003c\u003c 24));
    } else {
      // body chunks have 8 triangles, except for last chunk which has 4
      index = (index - 1) * 8;
      data = bytes.concat(_getTriangleData(tokenId, index), _getTriangleData(tokenId, index + 1),
                          _getTriangleData(tokenId, index + 2), _getTriangleData(tokenId, index + 3));
      if (index \u003c 64) {
        data = bytes.concat(data, _getTriangleData(tokenId, index + 4), _getTriangleData(tokenId, index + 5),
                                  _getTriangleData(tokenId, index + 6), _getTriangleData(tokenId, index + 7));
      }
    }
  }

  // STL File Triangle -- https://en.wikipedia.org/wiki/STL_(file_format)
  // total size - 50 bytes
  // 3 floats - normal -- NB omitted as interpreter will generate via right-hand rule
  // 3 floats - vertex 1
  // 3 floats - vertex 2
  // 3 floats - vertex 3
  // 2 bytes - attribute byte count (unused)
  function _getTriangleData(uint256 tokenId, uint256 triangle_offset) private pure returns(bytes memory data) {
    uint256 progOffset = triangle_offset * 9;
    bytes4[9] memory triData;
    bytes4 zeroFloat;
    bytes2 attrData;

    for (uint256 i=0; i \u003c 9; i++) {
      triData[i] = _getVertexFloatValue(tokenId, progOffset + i);
    }

    bytes memory firstPart = bytes.concat(zeroFloat, zeroFloat, zeroFloat, triData[0], triData[1], triData[2], triData[3], triData[4], triData[5]);
    data =  bytes.concat(firstPart, triData[6], triData[7], triData[8], attrData);
  }

  function _getVertexFloatValue(uint256 tokenId, uint256 progOffset) private pure returns(bytes4 value){
    uint8 progVal;
    progVal = _getProgVals(tokenId, progOffset);

    // height scales with tokenId
    if (progVal == heightProgVal) {
      return(getHeightFloatValue(tokenId));
    }


    if ( tokenId \u003c 460 ) {
      if (progVal == thickProgVal) {
        return(thick0);
      } else {
        uint256 start = progVal * 4;
        value = bytes4(bytes.concat(verts0[start], verts0[start + 1], verts0[start + 2], verts0[start + 3]));
      }
    } else if ( tokenId \u003c 1144) {
      if (progVal == thickProgVal) {
        return(thick1);
      } else {
        uint256 start = progVal * 4;
        value = bytes4(bytes.concat(verts1[start], verts1[start + 1], verts1[start + 2], verts1[start + 3]));
      }
    } else {
      if (progVal == thickProgVal) {
        return(thick2);
      } else {
        uint256 start = progVal * 4;
        value = bytes4(bytes.concat(verts2[start], verts2[start + 1], verts2[start + 2], verts2[start + 3]));
      }
    }
  }

  function _getProgVals(uint256 tokenId, uint256 stepNo) private pure returns(uint8 progVal) {
    if (tokenId \u003c 460) {
      progVal = uint8(prog0[stepNo]);
    }else if (tokenId \u003c 1144) {
      progVal = uint8(prog1[stepNo]);
    }else {
      progVal = uint8(prog0[stepNo]);
    }
  }

  // returns height of cup for token in millimeters as IEEE 754 32-bit little-endian float
  // this is the interior height and does not include the vertical thickness of the base
  // To get the full height add the appropriate thick constant for the cup\u0027s era
  function getHeightFloatValue(uint256 tokenId) public pure returns(bytes4 value) {
    require(tokenId \u003e 0 \u0026\u0026 tokenId \u003c 2001, \"Token ID invalid\");
    uint32 heightBase = uint32(tokenId - 1);

    // determine height to nearest millimeter
    uint32 mmHeight = 50 + heightBase / 8;

    // determine mm height most significant bit
    uint32 mmHeightMSB = 6;
    if (mmHeight \u003e 255) {
      mmHeightMSB = 9;
    } else if (mmHeight \u003e 127) {
      mmHeightMSB = 8;
    } else if (mmHeight \u003e 63) {
      mmHeightMSB = 7;
    }

    // position bits into mantissa
    mmHeight \u003c\u003c= 24 - mmHeightMSB;
    // clip off implicit first bit of mantissa
    uint32 mmHeightMask = 8388607;
    mmHeight \u0026= mmHeightMask;

    // determine height fraction in 1/8 of a millimeter
    uint32 mmHeightFraction = heightBase % 8;
    // position bits into mantissa
    mmHeightFraction \u003c\u003c= 20 - mmHeightMSB;

    // normalize mantissa
    uint32 exponent = 126 + mmHeightMSB;
    // position bits into exponent
    exponent \u003c\u003c= 23;

    // combine parts
    bytes4 b_val = bytes4(exponent | mmHeight | mmHeightFraction);
    // return little-endian representation
    value = bytes4(bytes.concat(b_val[3], b_val[2], b_val[1], b_val[0]));
  }

}
"},"Ownable.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import \"./Context.sol\";

/**
 * @dev Contract module which provides a basic access control mechanism, where
 * there is an account (an owner) that can be granted exclusive access to
 * specific functions.
 *
 * By default, the owner account will be the one that deploys the contract. This
 * can later be changed with {transferOwnership}.
 *
 * This module is used through inheritance. It will make available the modifier
 * `onlyOwner`, which can be applied to your functions to restrict their use to
 * the owner.
 */
abstract contract Ownable is Context {
    address private _owner;

    event OwnershipTransferred(address indexed previousOwner, address indexed newOwner);

    /**
     * @dev Initializes the contract setting the deployer as the initial owner.
     */
    constructor() {
        _transferOwnership(_msgSender());
    }

    /**
     * @dev Returns the address of the current owner.
     */
    function owner() public view virtual returns (address) {
        return _owner;
    }

    /**
     * @dev Throws if called by any account other than the owner.
     */
    modifier onlyOwner() {
        require(owner() == _msgSender(), \"Ownable: caller is not the owner\");
        _;
    }

    /**
     * @dev Leaves the contract without owner. It will not be possible to call
     * `onlyOwner` functions anymore. Can only be called by the current owner.
     *
     * NOTE: Renouncing ownership will leave the contract without an owner,
     * thereby removing any functionality that is only available to the owner.
     */
    function renounceOwnership() public virtual onlyOwner {
        _transferOwnership(address(0));
    }

    /**
     * @dev Transfers ownership of the contract to a new account (`newOwner`).
     * Can only be called by the current owner.
     */
    function transferOwnership(address newOwner) public virtual onlyOwner {
        require(newOwner != address(0), \"Ownable: new owner is the zero address\");
        _transferOwnership(newOwner);
    }

    /**
     * @dev Transfers ownership of the contract to a new account (`newOwner`).
     * Internal function without access restriction.
     */
    function _transferOwnership(address newOwner) internal virtual {
        address oldOwner = _owner;
        _owner = newOwner;
        emit OwnershipTransferred(oldOwner, newOwner);
    }
}
"},"ReentrancyGuard.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

/**
 * @dev Contract module that helps prevent reentrant calls to a function.
 *
 * Inheriting from `ReentrancyGuard` will make the {nonReentrant} modifier
 * available, which can be applied to functions to make sure there are no nested
 * (reentrant) calls to them.
 *
 * Note that because there is a single `nonReentrant` guard, functions marked as
 * `nonReentrant` may not call one another. This can be worked around by making
 * those functions `private`, and then adding `external` `nonReentrant` entry
 * points to them.
 *
 * TIP: If you would like to learn more about reentrancy and alternative ways
 * to protect against it, check out our blog post
 * https://blog.openzeppelin.com/reentrancy-after-istanbul/[Reentrancy After Istanbul].
 */
abstract contract ReentrancyGuard {
    // Booleans are more expensive than uint256 or any type that takes up a full
    // word because each write operation emits an extra SLOAD to first read the
    // slot\u0027s contents, replace the bits taken up by the boolean, and then write
    // back. This is the compiler\u0027s defense against contract upgrades and
    // pointer aliasing, and it cannot be disabled.

    // The values being non-zero value makes deployment a bit more expensive,
    // but in exchange the refund on every call to nonReentrant will be lower in
    // amount. Since refunds are capped to a percentage of the total
    // transaction\u0027s gas, it is best to keep them low in cases like this one, to
    // increase the likelihood of the full refund coming into effect.
    uint256 private constant _NOT_ENTERED = 1;
    uint256 private constant _ENTERED = 2;

    uint256 private _status;

    constructor() {
        _status = _NOT_ENTERED;
    }

    /**
     * @dev Prevents a contract from calling itself, directly or indirectly.
     * Calling a `nonReentrant` function from another `nonReentrant`
     * function is not supported. It is possible to prevent this from happening
     * by making the `nonReentrant` function external, and making it call a
     * `private` function that does the actual work.
     */
    modifier nonReentrant() {
        // On the first call to nonReentrant, _notEntered will be true
        require(_status != _ENTERED, \"ReentrancyGuard: reentrant call\");

        // Any calls to nonReentrant after this point will fail
        _status = _ENTERED;

        _;

        // By storing the original value once again, a refund is triggered (see
        // https://eips.ethereum.org/EIPS/eip-2200)
        _status = _NOT_ENTERED;
    }
}
"},"Strings.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

/**
 * @dev String operations.
 */
library Strings {
    bytes16 private constant _HEX_SYMBOLS = \"0123456789abcdef\";

    /**
     * @dev Converts a `uint256` to its ASCII `string` decimal representation.
     */
    function toString(uint256 value) internal pure returns (string memory) {
        // Inspired by OraclizeAPI\u0027s implementation - MIT licence
        // https://github.com/oraclize/ethereum-api/blob/b42146b063c7d6ee1358846c198246239e9360e8/oraclizeAPI_0.4.25.sol

        if (value == 0) {
            return \"0\";
        }
        uint256 temp = value;
        uint256 digits;
        while (temp != 0) {
            digits++;
            temp /= 10;
        }
        bytes memory buffer = new bytes(digits);
        while (value != 0) {
            digits -= 1;
            buffer[digits] = bytes1(uint8(48 + uint256(value % 10)));
            value /= 10;
        }
        return string(buffer);
    }

    /**
     * @dev Converts a `uint256` to its ASCII `string` hexadecimal representation.
     */
    function toHexString(uint256 value) internal pure returns (string memory) {
        if (value == 0) {
            return \"0x00\";
        }
        uint256 temp = value;
        uint256 length = 0;
        while (temp != 0) {
            length++;
            temp \u003e\u003e= 8;
        }
        return toHexString(value, length);
    }

    /**
     * @dev Converts a `uint256` to its ASCII `string` hexadecimal representation with fixed length.
     */
    function toHexString(uint256 value, uint256 length) internal pure returns (string memory) {
        bytes memory buffer = new bytes(2 * length + 2);
        buffer[0] = \"0\";
        buffer[1] = \"x\";
        for (uint256 i = 2 * length + 1; i \u003e 1; --i) {
            buffer[i] = _HEX_SYMBOLS[value \u0026 0xf];
            value \u003e\u003e= 4;
        }
        require(value == 0, \"Strings: hex length insufficient\");
        return string(buffer);
    }
}

