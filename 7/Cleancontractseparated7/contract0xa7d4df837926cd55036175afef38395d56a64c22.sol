// SPDX-License-Identifier: agpl-3.0
pragma solidity 0.6.12;

import {SafeMath} from \u0027./SafeMath.sol\u0027;
import {IReserveInterestRateStrategy} from \u0027./IReserveInterestRateStrategy.sol\u0027;
import {WadRayMath} from \u0027./WadRayMath.sol\u0027;
import {PercentageMath} from \u0027./PercentageMath.sol\u0027;
import {ILendingPoolAddressesProvider} from \u0027./ILendingPoolAddressesProvider.sol\u0027;
import {ILendingRateOracle} from \u0027./ILendingRateOracle.sol\u0027;

/**
 * @title DefaultReserveInterestRateStrategy contract
 * @notice Implements the calculation of the interest rates depending on the reserve state
 * @dev The model of interest rate is based on 2 slopes, one before the `OPTIMAL_UTILIZATION_RATE`
 * point of utilization and another from that one to 100%
 * - An instance of this same contract, can\u0027t be used across different Aave markets, due to the caching
 *   of the LendingPoolAddressesProvider
 * @author Aave
 **/
contract DefaultReserveInterestRateStrategy is IReserveInterestRateStrategy {
  using WadRayMath for uint256;
  using SafeMath for uint256;
  using PercentageMath for uint256;

  /**
   * @dev this constant represents the utilization rate at which the pool aims to obtain most competitive borrow rates.
   * Expressed in ray
   **/
  uint256 public immutable OPTIMAL_UTILIZATION_RATE;

  /**
   * @dev This constant represents the excess utilization rate above the optimal. It\u0027s always equal to
   * 1-optimal utilization rate. Added as a constant here for gas optimizations.
   * Expressed in ray
   **/

  uint256 public immutable EXCESS_UTILIZATION_RATE;

  ILendingPoolAddressesProvider public immutable addressesProvider;

  // Base variable borrow rate when Utilization rate = 0. Expressed in ray
  uint256 internal immutable _baseVariableBorrowRate;

  // Slope of the variable interest curve when utilization rate \u003e 0 and \u003c= OPTIMAL_UTILIZATION_RATE. Expressed in ray
  uint256 internal immutable _variableRateSlope1;

  // Slope of the variable interest curve when utilization rate \u003e OPTIMAL_UTILIZATION_RATE. Expressed in ray
  uint256 internal immutable _variableRateSlope2;

  // Slope of the stable interest curve when utilization rate \u003e 0 and \u003c= OPTIMAL_UTILIZATION_RATE. Expressed in ray
  uint256 internal immutable _stableRateSlope1;

  // Slope of the stable interest curve when utilization rate \u003e OPTIMAL_UTILIZATION_RATE. Expressed in ray
  uint256 internal immutable _stableRateSlope2;

  constructor(
    ILendingPoolAddressesProvider provider,
    uint256 optimalUtilizationRate,
    uint256 baseVariableBorrowRate,
    uint256 variableRateSlope1,
    uint256 variableRateSlope2,
    uint256 stableRateSlope1,
    uint256 stableRateSlope2
  ) public {
    OPTIMAL_UTILIZATION_RATE = optimalUtilizationRate;
    EXCESS_UTILIZATION_RATE = WadRayMath.ray().sub(optimalUtilizationRate);
    addressesProvider = provider;
    _baseVariableBorrowRate = baseVariableBorrowRate;
    _variableRateSlope1 = variableRateSlope1;
    _variableRateSlope2 = variableRateSlope2;
    _stableRateSlope1 = stableRateSlope1;
    _stableRateSlope2 = stableRateSlope2;
  }

  function variableRateSlope1() external view returns (uint256) {
    return _variableRateSlope1;
  }

  function variableRateSlope2() external view returns (uint256) {
    return _variableRateSlope2;
  }

  function stableRateSlope1() external view returns (uint256) {
    return _stableRateSlope1;
  }

  function stableRateSlope2() external view returns (uint256) {
    return _stableRateSlope2;
  }

  function baseVariableBorrowRate() external view override returns (uint256) {
    return _baseVariableBorrowRate;
  }

  function getMaxVariableBorrowRate() external view override returns (uint256) {
    return _baseVariableBorrowRate.add(_variableRateSlope1).add(_variableRateSlope2);
  }

  struct CalcInterestRatesLocalVars {
    uint256 totalDebt;
    uint256 currentVariableBorrowRate;
    uint256 currentStableBorrowRate;
    uint256 currentLiquidityRate;
    uint256 utilizationRate;
  }

  /**
   * @dev Calculates the interest rates depending on the reserve\u0027s state and configurations
   * @param reserve The address of the reserve
   * @param availableLiquidity The liquidity available in the reserve
   * @param totalStableDebt The total borrowed from the reserve a stable rate
   * @param totalVariableDebt The total borrowed from the reserve at a variable rate
   * @param averageStableBorrowRate The weighted average of all the stable rate loans
   * @param reserveFactor The reserve portion of the interest that goes to the treasury of the market
   * @return The liquidity rate, the stable borrow rate and the variable borrow rate
   **/
  function calculateInterestRates(
    address reserve,
    uint256 availableLiquidity,
    uint256 totalStableDebt,
    uint256 totalVariableDebt,
    uint256 averageStableBorrowRate,
    uint256 reserveFactor
  )
    external
    view
    override
    returns (
      uint256,
      uint256,
      uint256
    )
  {
    CalcInterestRatesLocalVars memory vars;

    vars.totalDebt = totalStableDebt.add(totalVariableDebt);
    vars.currentVariableBorrowRate = 0;
    vars.currentStableBorrowRate = 0;
    vars.currentLiquidityRate = 0;

    uint256 utilizationRate =
      vars.totalDebt == 0 ? 0 : vars.totalDebt.rayDiv(availableLiquidity.add(vars.totalDebt));

    vars.currentStableBorrowRate = ILendingRateOracle(addressesProvider.getLendingRateOracle())
      .getMarketBorrowRate(reserve);

    if (utilizationRate \u003e OPTIMAL_UTILIZATION_RATE) {
      uint256 excessUtilizationRateRatio =
        utilizationRate.sub(OPTIMAL_UTILIZATION_RATE).rayDiv(EXCESS_UTILIZATION_RATE);

      vars.currentStableBorrowRate = vars.currentStableBorrowRate.add(_stableRateSlope1).add(
        _stableRateSlope2.rayMul(excessUtilizationRateRatio)
      );

      vars.currentVariableBorrowRate = _baseVariableBorrowRate.add(_variableRateSlope1).add(
        _variableRateSlope2.rayMul(excessUtilizationRateRatio)
      );
    } else {
      vars.currentStableBorrowRate = vars.currentStableBorrowRate.add(
        _stableRateSlope1.rayMul(utilizationRate.rayDiv(OPTIMAL_UTILIZATION_RATE))
      );
      vars.currentVariableBorrowRate = _baseVariableBorrowRate.add(
        utilizationRate.rayMul(_variableRateSlope1).rayDiv(OPTIMAL_UTILIZATION_RATE)
      );
    }

    vars.currentLiquidityRate = _getOverallBorrowRate(
      totalStableDebt,
      totalVariableDebt,
      vars
        .currentVariableBorrowRate,
      averageStableBorrowRate
    )
      .rayMul(utilizationRate)
      .percentMul(PercentageMath.PERCENTAGE_FACTOR.sub(reserveFactor));

    return (
      vars.currentLiquidityRate,
      vars.currentStableBorrowRate,
      vars.currentVariableBorrowRate
    );
  }

  /**
   * @dev Calculates the overall borrow rate as the weighted average between the total variable debt and total stable debt
   * @param totalStableDebt The total borrowed from the reserve a stable rate
   * @param totalVariableDebt The total borrowed from the reserve at a variable rate
   * @param currentVariableBorrowRate The current variable borrow rate of the reserve
   * @param currentAverageStableBorrowRate The current weighted average of all the stable rate loans
   * @return The weighted averaged borrow rate
   **/
  function _getOverallBorrowRate(
    uint256 totalStableDebt,
    uint256 totalVariableDebt,
    uint256 currentVariableBorrowRate,
    uint256 currentAverageStableBorrowRate
  ) internal pure returns (uint256) {
    uint256 totalDebt = totalStableDebt.add(totalVariableDebt);

    if (totalDebt == 0) return 0;

    uint256 weightedVariableRate = totalVariableDebt.wadToRay().rayMul(currentVariableBorrowRate);

    uint256 weightedStableRate = totalStableDebt.wadToRay().rayMul(currentAverageStableBorrowRate);

    uint256 overallBorrowRate =
      weightedVariableRate.add(weightedStableRate).rayDiv(totalDebt.wadToRay());

    return overallBorrowRate;
  }
}"},"Errors.sol":{"content":"// SPDX-License-Identifier: agpl-3.0
pragma solidity 0.6.12;

/**
 * @title Errors library
 * @author Aave
 * @notice Defines the error messages emitted by the different contracts of the Aave protocol
 * @dev Error messages prefix glossary:
 *  - VL = ValidationLogic
 *  - MATH = Math libraries
 *  - CT = Common errors between tokens (AToken, VariableDebtToken and StableDebtToken)
 *  - AT = AToken
 *  - SDT = StableDebtToken
 *  - VDT = VariableDebtToken
 *  - LP = LendingPool
 *  - LPAPR = LendingPoolAddressesProviderRegistry
 *  - LPC = LendingPoolConfiguration
 *  - RL = ReserveLogic
 *  - LPCM = LendingPoolCollateralManager
 *  - P = Pausable
 */
library Errors {
  //common errors
  string public constant CALLER_NOT_POOL_ADMIN = \u002733\u0027; // \u0027The caller must be the pool admin\u0027
  string public constant BORROW_ALLOWANCE_NOT_ENOUGH = \u002759\u0027; // User borrows on behalf, but allowance are too small

  //contract specific errors
  string public constant VL_INVALID_AMOUNT = \u00271\u0027; // \u0027Amount must be greater than 0\u0027
  string public constant VL_NO_ACTIVE_RESERVE = \u00272\u0027; // \u0027Action requires an active reserve\u0027
  string public constant VL_RESERVE_FROZEN = \u00273\u0027; // \u0027Action cannot be performed because the reserve is frozen\u0027
  string public constant VL_CURRENT_AVAILABLE_LIQUIDITY_NOT_ENOUGH = \u00274\u0027; // \u0027The current liquidity is not enough\u0027
  string public constant VL_NOT_ENOUGH_AVAILABLE_USER_BALANCE = \u00275\u0027; // \u0027User cannot withdraw more than the available balance\u0027
  string public constant VL_TRANSFER_NOT_ALLOWED = \u00276\u0027; // \u0027Transfer cannot be allowed.\u0027
  string public constant VL_BORROWING_NOT_ENABLED = \u00277\u0027; // \u0027Borrowing is not enabled\u0027
  string public constant VL_INVALID_INTEREST_RATE_MODE_SELECTED = \u00278\u0027; // \u0027Invalid interest rate mode selected\u0027
  string public constant VL_COLLATERAL_BALANCE_IS_0 = \u00279\u0027; // \u0027The collateral balance is 0\u0027
  string public constant VL_HEALTH_FACTOR_LOWER_THAN_LIQUIDATION_THRESHOLD = \u002710\u0027; // \u0027Health factor is lesser than the liquidation threshold\u0027
  string public constant VL_COLLATERAL_CANNOT_COVER_NEW_BORROW = \u002711\u0027; // \u0027There is not enough collateral to cover a new borrow\u0027
  string public constant VL_STABLE_BORROWING_NOT_ENABLED = \u002712\u0027; // stable borrowing not enabled
  string public constant VL_COLLATERAL_SAME_AS_BORROWING_CURRENCY = \u002713\u0027; // collateral is (mostly) the same currency that is being borrowed
  string public constant VL_AMOUNT_BIGGER_THAN_MAX_LOAN_SIZE_STABLE = \u002714\u0027; // \u0027The requested amount is greater than the max loan size in stable rate mode
  string public constant VL_NO_DEBT_OF_SELECTED_TYPE = \u002715\u0027; // \u0027for repayment of stable debt, the user needs to have stable debt, otherwise, he needs to have variable debt\u0027
  string public constant VL_NO_EXPLICIT_AMOUNT_TO_REPAY_ON_BEHALF = \u002716\u0027; // \u0027To repay on behalf of an user an explicit amount to repay is needed\u0027
  string public constant VL_NO_STABLE_RATE_LOAN_IN_RESERVE = \u002717\u0027; // \u0027User does not have a stable rate loan in progress on this reserve\u0027
  string public constant VL_NO_VARIABLE_RATE_LOAN_IN_RESERVE = \u002718\u0027; // \u0027User does not have a variable rate loan in progress on this reserve\u0027
  string public constant VL_UNDERLYING_BALANCE_NOT_GREATER_THAN_0 = \u002719\u0027; // \u0027The underlying balance needs to be greater than 0\u0027
  string public constant VL_DEPOSIT_ALREADY_IN_USE = \u002720\u0027; // \u0027User deposit is already being used as collateral\u0027
  string public constant LP_NOT_ENOUGH_STABLE_BORROW_BALANCE = \u002721\u0027; // \u0027User does not have any stable rate loan for this reserve\u0027
  string public constant LP_INTEREST_RATE_REBALANCE_CONDITIONS_NOT_MET = \u002722\u0027; // \u0027Interest rate rebalance conditions were not met\u0027
  string public constant LP_LIQUIDATION_CALL_FAILED = \u002723\u0027; // \u0027Liquidation call failed\u0027
  string public constant LP_NOT_ENOUGH_LIQUIDITY_TO_BORROW = \u002724\u0027; // \u0027There is not enough liquidity available to borrow\u0027
  string public constant LP_REQUESTED_AMOUNT_TOO_SMALL = \u002725\u0027; // \u0027The requested amount is too small for a FlashLoan.\u0027
  string public constant LP_INCONSISTENT_PROTOCOL_ACTUAL_BALANCE = \u002726\u0027; // \u0027The actual balance of the protocol is inconsistent\u0027
  string public constant LP_CALLER_NOT_LENDING_POOL_CONFIGURATOR = \u002727\u0027; // \u0027The caller of the function is not the lending pool configurator\u0027
  string public constant LP_INCONSISTENT_FLASHLOAN_PARAMS = \u002728\u0027;
  string public constant CT_CALLER_MUST_BE_LENDING_POOL = \u002729\u0027; // \u0027The caller of this function must be a lending pool\u0027
  string public constant CT_CANNOT_GIVE_ALLOWANCE_TO_HIMSELF = \u002730\u0027; // \u0027User cannot give allowance to himself\u0027
  string public constant CT_TRANSFER_AMOUNT_NOT_GT_0 = \u002731\u0027; // \u0027Transferred amount needs to be greater than zero\u0027
  string public constant RL_RESERVE_ALREADY_INITIALIZED = \u002732\u0027; // \u0027Reserve has already been initialized\u0027
  string public constant LPC_RESERVE_LIQUIDITY_NOT_0 = \u002734\u0027; // \u0027The liquidity of the reserve needs to be 0\u0027
  string public constant LPC_INVALID_ATOKEN_POOL_ADDRESS = \u002735\u0027; // \u0027The liquidity of the reserve needs to be 0\u0027
  string public constant LPC_INVALID_STABLE_DEBT_TOKEN_POOL_ADDRESS = \u002736\u0027; // \u0027The liquidity of the reserve needs to be 0\u0027
  string public constant LPC_INVALID_VARIABLE_DEBT_TOKEN_POOL_ADDRESS = \u002737\u0027; // \u0027The liquidity of the reserve needs to be 0\u0027
  string public constant LPC_INVALID_STABLE_DEBT_TOKEN_UNDERLYING_ADDRESS = \u002738\u0027; // \u0027The liquidity of the reserve needs to be 0\u0027
  string public constant LPC_INVALID_VARIABLE_DEBT_TOKEN_UNDERLYING_ADDRESS = \u002739\u0027; // \u0027The liquidity of the reserve needs to be 0\u0027
  string public constant LPC_INVALID_ADDRESSES_PROVIDER_ID = \u002740\u0027; // \u0027The liquidity of the reserve needs to be 0\u0027
  string public constant LPC_INVALID_CONFIGURATION = \u002775\u0027; // \u0027Invalid risk parameters for the reserve\u0027
  string public constant LPC_CALLER_NOT_EMERGENCY_ADMIN = \u002776\u0027; // \u0027The caller must be the emergency admin\u0027
  string public constant LPAPR_PROVIDER_NOT_REGISTERED = \u002741\u0027; // \u0027Provider is not registered\u0027
  string public constant LPCM_HEALTH_FACTOR_NOT_BELOW_THRESHOLD = \u002742\u0027; // \u0027Health factor is not below the threshold\u0027
  string public constant LPCM_COLLATERAL_CANNOT_BE_LIQUIDATED = \u002743\u0027; // \u0027The collateral chosen cannot be liquidated\u0027
  string public constant LPCM_SPECIFIED_CURRENCY_NOT_BORROWED_BY_USER = \u002744\u0027; // \u0027User did not borrow the specified currency\u0027
  string public constant LPCM_NOT_ENOUGH_LIQUIDITY_TO_LIQUIDATE = \u002745\u0027; // \"There isn\u0027t enough liquidity available to liquidate\"
  string public constant LPCM_NO_ERRORS = \u002746\u0027; // \u0027No errors\u0027
  string public constant LP_INVALID_FLASHLOAN_MODE = \u002747\u0027; //Invalid flashloan mode selected
  string public constant MATH_MULTIPLICATION_OVERFLOW = \u002748\u0027;
  string public constant MATH_ADDITION_OVERFLOW = \u002749\u0027;
  string public constant MATH_DIVISION_BY_ZERO = \u002750\u0027;
  string public constant RL_LIQUIDITY_INDEX_OVERFLOW = \u002751\u0027; //  Liquidity index overflows uint128
  string public constant RL_VARIABLE_BORROW_INDEX_OVERFLOW = \u002752\u0027; //  Variable borrow index overflows uint128
  string public constant RL_LIQUIDITY_RATE_OVERFLOW = \u002753\u0027; //  Liquidity rate overflows uint128
  string public constant RL_VARIABLE_BORROW_RATE_OVERFLOW = \u002754\u0027; //  Variable borrow rate overflows uint128
  string public constant RL_STABLE_BORROW_RATE_OVERFLOW = \u002755\u0027; //  Stable borrow rate overflows uint128
  string public constant CT_INVALID_MINT_AMOUNT = \u002756\u0027; //invalid amount to mint
  string public constant LP_FAILED_REPAY_WITH_COLLATERAL = \u002757\u0027;
  string public constant CT_INVALID_BURN_AMOUNT = \u002758\u0027; //invalid amount to burn
  string public constant LP_FAILED_COLLATERAL_SWAP = \u002760\u0027;
  string public constant LP_INVALID_EQUAL_ASSETS_TO_SWAP = \u002761\u0027;
  string public constant LP_REENTRANCY_NOT_ALLOWED = \u002762\u0027;
  string public constant LP_CALLER_MUST_BE_AN_ATOKEN = \u002763\u0027;
  string public constant LP_IS_PAUSED = \u002764\u0027; // \u0027Pool is paused\u0027
  string public constant LP_NO_MORE_RESERVES_ALLOWED = \u002765\u0027;
  string public constant LP_INVALID_FLASH_LOAN_EXECUTOR_RETURN = \u002766\u0027;
  string public constant RC_INVALID_LTV = \u002767\u0027;
  string public constant RC_INVALID_LIQ_THRESHOLD = \u002768\u0027;
  string public constant RC_INVALID_LIQ_BONUS = \u002769\u0027;
  string public constant RC_INVALID_DECIMALS = \u002770\u0027;
  string public constant RC_INVALID_RESERVE_FACTOR = \u002771\u0027;
  string public constant LPAPR_INVALID_ADDRESSES_PROVIDER_ID = \u002772\u0027;
  string public constant VL_INCONSISTENT_FLASHLOAN_PARAMS = \u002773\u0027;
  string public constant LP_INCONSISTENT_PARAMS_LENGTH = \u002774\u0027;
  string public constant UL_INVALID_INDEX = \u002777\u0027;
  string public constant LP_NOT_CONTRACT = \u002778\u0027;
  string public constant SDT_STABLE_DEBT_OVERFLOW = \u002779\u0027;
  string public constant SDT_BURN_EXCEEDS_BALANCE = \u002780\u0027;

  enum CollateralManagerErrors {
    NO_ERROR,
    NO_COLLATERAL_AVAILABLE,
    COLLATERAL_CANNOT_BE_LIQUIDATED,
    CURRRENCY_NOT_BORROWED,
    HEALTH_FACTOR_ABOVE_THRESHOLD,
    NOT_ENOUGH_LIQUIDITY,
    NO_ACTIVE_RESERVE,
    HEALTH_FACTOR_LOWER_THAN_LIQUIDATION_THRESHOLD,
    INVALID_EQUAL_ASSETS_TO_SWAP,
    FROZEN_RESERVE
  }
}"},"ILendingPoolAddressesProvider.sol":{"content":"// SPDX-License-Identifier: agpl-3.0
pragma solidity 0.6.12;

/**
 * @title LendingPoolAddressesProvider contract
 * @dev Main registry of addresses part of or connected to the protocol, including permissioned roles
 * - Acting also as factory of proxies and admin of those, so with right to change its implementations
 * - Owned by the Aave Governance
 * @author Aave
 **/
interface ILendingPoolAddressesProvider {
  event MarketIdSet(string newMarketId);
  event LendingPoolUpdated(address indexed newAddress);
  event ConfigurationAdminUpdated(address indexed newAddress);
  event EmergencyAdminUpdated(address indexed newAddress);
  event LendingPoolConfiguratorUpdated(address indexed newAddress);
  event LendingPoolCollateralManagerUpdated(address indexed newAddress);
  event PriceOracleUpdated(address indexed newAddress);
  event LendingRateOracleUpdated(address indexed newAddress);
  event ProxyCreated(bytes32 id, address indexed newAddress);
  event AddressSet(bytes32 id, address indexed newAddress, bool hasProxy);

  function getMarketId() external view returns (string memory);

  function setMarketId(string calldata marketId) external;

  function setAddress(bytes32 id, address newAddress) external;

  function setAddressAsProxy(bytes32 id, address impl) external;

  function getAddress(bytes32 id) external view returns (address);

  function getLendingPool() external view returns (address);

  function setLendingPoolImpl(address pool) external;

  function getLendingPoolConfigurator() external view returns (address);

  function setLendingPoolConfiguratorImpl(address configurator) external;

  function getLendingPoolCollateralManager() external view returns (address);

  function setLendingPoolCollateralManager(address manager) external;

  function getPoolAdmin() external view returns (address);

  function setPoolAdmin(address admin) external;

  function getEmergencyAdmin() external view returns (address);

  function setEmergencyAdmin(address admin) external;

  function getPriceOracle() external view returns (address);

  function setPriceOracle(address priceOracle) external;

  function getLendingRateOracle() external view returns (address);

  function setLendingRateOracle(address lendingRateOracle) external;
}"},"ILendingRateOracle.sol":{"content":"// SPDX-License-Identifier: agpl-3.0
pragma solidity 0.6.12;

/**
 * @title ILendingRateOracle interface
 * @notice Interface for the Aave borrow rate oracle. Provides the average market borrow rate to be used as a base for the stable borrow rate calculations
 **/

interface ILendingRateOracle {
  /**
    @dev returns the market borrow rate in ray
    **/
  function getMarketBorrowRate(address asset) external view returns (uint256);

  /**
    @dev sets the market borrow rate. Rate value must be in ray
    **/
  function setMarketBorrowRate(address asset, uint256 rate) external;
}"},"IReserveInterestRateStrategy.sol":{"content":"// SPDX-License-Identifier: agpl-3.0
pragma solidity 0.6.12;

/**
 * @title IReserveInterestRateStrategyInterface interface
 * @dev Interface for the calculation of the interest rates
 * @author Aave
 */
interface IReserveInterestRateStrategy {
  function baseVariableBorrowRate() external view returns (uint256);

  function getMaxVariableBorrowRate() external view returns (uint256);

  function calculateInterestRates(
    address reserve,
    uint256 utilizationRate,
    uint256 totalStableDebt,
    uint256 totalVariableDebt,
    uint256 averageStableBorrowRate,
    uint256 reserveFactor
  )
    external
    view
    returns (
      uint256 liquidityRate,
      uint256 stableBorrowRate,
      uint256 variableBorrowRate
    );
}"},"PercentageMath.sol":{"content":"// SPDX-License-Identifier: agpl-3.0
pragma solidity 0.6.12;

import {Errors} from \u0027./Errors.sol\u0027;

/**
 * @title PercentageMath library
 * @author Aave
 * @notice Provides functions to perform percentage calculations
 * @dev Percentages are defined by default with 2 decimals of precision (100.00). The precision is indicated by PERCENTAGE_FACTOR
 * @dev Operations are rounded half up
 **/

library PercentageMath {
  uint256 constant PERCENTAGE_FACTOR = 1e4; //percentage plus two decimals
  uint256 constant HALF_PERCENT = PERCENTAGE_FACTOR / 2;

  /**
   * @dev Executes a percentage multiplication
   * @param value The value of which the percentage needs to be calculated
   * @param percentage The percentage of the value to be calculated
   * @return The percentage of value
   **/
  function percentMul(uint256 value, uint256 percentage) internal pure returns (uint256) {
    if (value == 0 || percentage == 0) {
      return 0;
    }

    require(
      value \u003c= (type(uint256).max - HALF_PERCENT) / percentage,
      Errors.MATH_MULTIPLICATION_OVERFLOW
    );

    return (value * percentage + HALF_PERCENT) / PERCENTAGE_FACTOR;
  }

  /**
   * @dev Executes a percentage division
   * @param value The value of which the percentage needs to be calculated
   * @param percentage The percentage of the value to be calculated
   * @return The value divided the percentage
   **/
  function percentDiv(uint256 value, uint256 percentage) internal pure returns (uint256) {
    require(percentage != 0, Errors.MATH_DIVISION_BY_ZERO);
    uint256 halfPercentage = percentage / 2;

    require(
      value \u003c= (type(uint256).max - halfPercentage) / PERCENTAGE_FACTOR,
      Errors.MATH_MULTIPLICATION_OVERFLOW
    );

    return (value * PERCENTAGE_FACTOR + halfPercentage) / percentage;
  }
}"},"SafeMath.sol":{"content":"// SPDX-License-Identifier: agpl-3.0
pragma solidity 0.6.12;

/**
 * @dev Wrappers over Solidity\u0027s arithmetic operations with added overflow
 * checks.
 *
 * Arithmetic operations in Solidity wrap on overflow. This can easily result
 * in bugs, because programmers usually assume that an overflow raises an
 * error, which is the standard behavior in high level programming languages.
 * `SafeMath` restores this intuition by reverting the transaction when an
 * operation overflows.
 *
 * Using this library instead of the unchecked operations eliminates an entire
 * class of bugs, so it\u0027s recommended to use it always.
 */
library SafeMath {
  /**
   * @dev Returns the addition of two unsigned integers, reverting on
   * overflow.
   *
   * Counterpart to Solidity\u0027s `+` operator.
   *
   * Requirements:
   * - Addition cannot overflow.
   */
  function add(uint256 a, uint256 b) internal pure returns (uint256) {
    uint256 c = a + b;
    require(c \u003e= a, \u0027SafeMath: addition overflow\u0027);

    return c;
  }

  /**
   * @dev Returns the subtraction of two unsigned integers, reverting on
   * overflow (when the result is negative).
   *
   * Counterpart to Solidity\u0027s `-` operator.
   *
   * Requirements:
   * - Subtraction cannot overflow.
   */
  function sub(uint256 a, uint256 b) internal pure returns (uint256) {
    return sub(a, b, \u0027SafeMath: subtraction overflow\u0027);
  }

  /**
   * @dev Returns the subtraction of two unsigned integers, reverting with custom message on
   * overflow (when the result is negative).
   *
   * Counterpart to Solidity\u0027s `-` operator.
   *
   * Requirements:
   * - Subtraction cannot overflow.
   */
  function sub(
    uint256 a,
    uint256 b,
    string memory errorMessage
  ) internal pure returns (uint256) {
    require(b \u003c= a, errorMessage);
    uint256 c = a - b;

    return c;
  }

  /**
   * @dev Returns the multiplication of two unsigned integers, reverting on
   * overflow.
   *
   * Counterpart to Solidity\u0027s `*` operator.
   *
   * Requirements:
   * - Multiplication cannot overflow.
   */
  function mul(uint256 a, uint256 b) internal pure returns (uint256) {
    // Gas optimization: this is cheaper than requiring \u0027a\u0027 not being zero, but the
    // benefit is lost if \u0027b\u0027 is also tested.
    // See: https://github.com/OpenZeppelin/openzeppelin-contracts/pull/522
    if (a == 0) {
      return 0;
    }

    uint256 c = a * b;
    require(c / a == b, \u0027SafeMath: multiplication overflow\u0027);

    return c;
  }

  /**
   * @dev Returns the integer division of two unsigned integers. Reverts on
   * division by zero. The result is rounded towards zero.
   *
   * Counterpart to Solidity\u0027s `/` operator. Note: this function uses a
   * `revert` opcode (which leaves remaining gas untouched) while Solidity
   * uses an invalid opcode to revert (consuming all remaining gas).
   *
   * Requirements:
   * - The divisor cannot be zero.
   */
  function div(uint256 a, uint256 b) internal pure returns (uint256) {
    return div(a, b, \u0027SafeMath: division by zero\u0027);
  }

  /**
   * @dev Returns the integer division of two unsigned integers. Reverts with custom message on
   * division by zero. The result is rounded towards zero.
   *
   * Counterpart to Solidity\u0027s `/` operator. Note: this function uses a
   * `revert` opcode (which leaves remaining gas untouched) while Solidity
   * uses an invalid opcode to revert (consuming all remaining gas).
   *
   * Requirements:
   * - The divisor cannot be zero.
   */
  function div(
    uint256 a,
    uint256 b,
    string memory errorMessage
  ) internal pure returns (uint256) {
    // Solidity only automatically asserts when dividing by 0
    require(b \u003e 0, errorMessage);
    uint256 c = a / b;
    // assert(a == b * c + a % b); // There is no case in which this doesn\u0027t hold

    return c;
  }

  /**
   * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
   * Reverts when dividing by zero.
   *
   * Counterpart to Solidity\u0027s `%` operator. This function uses a `revert`
   * opcode (which leaves remaining gas untouched) while Solidity uses an
   * invalid opcode to revert (consuming all remaining gas).
   *
   * Requirements:
   * - The divisor cannot be zero.
   */
  function mod(uint256 a, uint256 b) internal pure returns (uint256) {
    return mod(a, b, \u0027SafeMath: modulo by zero\u0027);
  }

  /**
   * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
   * Reverts with custom message when dividing by zero.
   *
   * Counterpart to Solidity\u0027s `%` operator. This function uses a `revert`
   * opcode (which leaves remaining gas untouched) while Solidity uses an
   * invalid opcode to revert (consuming all remaining gas).
   *
   * Requirements:
   * - The divisor cannot be zero.
   */
  function mod(
    uint256 a,
    uint256 b,
    string memory errorMessage
  ) internal pure returns (uint256) {
    require(b != 0, errorMessage);
    return a % b;
  }
}"},"WadRayMath.sol":{"content":"// SPDX-License-Identifier: agpl-3.0
pragma solidity 0.6.12;

import {Errors} from \u0027./Errors.sol\u0027;

/**
 * @title WadRayMath library
 * @author Aave
 * @dev Provides mul and div function for wads (decimal numbers with 18 digits precision) and rays (decimals with 27 digits)
 **/

library WadRayMath {
  uint256 internal constant WAD = 1e18;
  uint256 internal constant halfWAD = WAD / 2;

  uint256 internal constant RAY = 1e27;
  uint256 internal constant halfRAY = RAY / 2;

  uint256 internal constant WAD_RAY_RATIO = 1e9;

  /**
   * @return One ray, 1e27
   **/
  function ray() internal pure returns (uint256) {
    return RAY;
  }

  /**
   * @return One wad, 1e18
   **/

  function wad() internal pure returns (uint256) {
    return WAD;
  }

  /**
   * @return Half ray, 1e27/2
   **/
  function halfRay() internal pure returns (uint256) {
    return halfRAY;
  }

  /**
   * @return Half ray, 1e18/2
   **/
  function halfWad() internal pure returns (uint256) {
    return halfWAD;
  }

  /**
   * @dev Multiplies two wad, rounding half up to the nearest wad
   * @param a Wad
   * @param b Wad
   * @return The result of a*b, in wad
   **/
  function wadMul(uint256 a, uint256 b) internal pure returns (uint256) {
    if (a == 0 || b == 0) {
      return 0;
    }

    require(a \u003c= (type(uint256).max - halfWAD) / b, Errors.MATH_MULTIPLICATION_OVERFLOW);

    return (a * b + halfWAD) / WAD;
  }

  /**
   * @dev Divides two wad, rounding half up to the nearest wad
   * @param a Wad
   * @param b Wad
   * @return The result of a/b, in wad
   **/
  function wadDiv(uint256 a, uint256 b) internal pure returns (uint256) {
    require(b != 0, Errors.MATH_DIVISION_BY_ZERO);
    uint256 halfB = b / 2;

    require(a \u003c= (type(uint256).max - halfB) / WAD, Errors.MATH_MULTIPLICATION_OVERFLOW);

    return (a * WAD + halfB) / b;
  }

  /**
   * @dev Multiplies two ray, rounding half up to the nearest ray
   * @param a Ray
   * @param b Ray
   * @return The result of a*b, in ray
   **/
  function rayMul(uint256 a, uint256 b) internal pure returns (uint256) {
    if (a == 0 || b == 0) {
      return 0;
    }

    require(a \u003c= (type(uint256).max - halfRAY) / b, Errors.MATH_MULTIPLICATION_OVERFLOW);

    return (a * b + halfRAY) / RAY;
  }

  /**
   * @dev Divides two ray, rounding half up to the nearest ray
   * @param a Ray
   * @param b Ray
   * @return The result of a/b, in ray
   **/
  function rayDiv(uint256 a, uint256 b) internal pure returns (uint256) {
    require(b != 0, Errors.MATH_DIVISION_BY_ZERO);
    uint256 halfB = b / 2;

    require(a \u003c= (type(uint256).max - halfB) / RAY, Errors.MATH_MULTIPLICATION_OVERFLOW);

    return (a * RAY + halfB) / b;
  }

  /**
   * @dev Casts ray down to wad
   * @param a Ray
   * @return a casted to wad, rounded half up to the nearest wad
   **/
  function rayToWad(uint256 a) internal pure returns (uint256) {
    uint256 halfRatio = WAD_RAY_RATIO / 2;
    uint256 result = halfRatio + a;
    require(result \u003e= halfRatio, Errors.MATH_ADDITION_OVERFLOW);

    return result / WAD_RAY_RATIO;
  }

  /**
   * @dev Converts wad up to ray
   * @param a Wad
   * @return a converted in ray
   **/
  function wadToRay(uint256 a) internal pure returns (uint256) {
    uint256 result = a * WAD_RAY_RATIO;
    require(result / WAD_RAY_RATIO == a, Errors.MATH_MULTIPLICATION_OVERFLOW);
    return result;
  }
}
