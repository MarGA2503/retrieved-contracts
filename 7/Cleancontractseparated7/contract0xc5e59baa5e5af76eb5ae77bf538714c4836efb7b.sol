/*

Ñíguez Randomity Engine API

MIT License

Copyright (c) 2019 niguezrandomityengine | Programmed and designed by Scheich R. Ahmed

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the \"Software\"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/

// SPDX-License-Identifier: --🦉--

pragma solidity \u003e= 0.4.0; // Compiler version incompatible error!

abstract contract niguezRandomityEngine {

\tfunction rd() external virtual returns (uint256);
\tfunction rm() external virtual returns (uint256);
\tfunction rv() external virtual returns (uint256);
\tfunction rx() external virtual returns (uint256);
\tfunction rf() external virtual returns (uint256);

}

contract usingNRE {

  niguezRandomityEngine internal nre = niguezRandomityEngine(0x031eaE8a8105217ab64359D4361022d0947f4572);\t
\tfunction rd() internal returns (uint256) {
        return nre.rd();
    }

\tfunction rf() internal returns (uint256) {
        return nre.rf();
    }
\t\t
\tfunction rm() internal returns (uint256) {
        return nre.rm();
    }

\tfunction rv() internal returns (uint256) {
        return nre.rv();
    }
\t
\tfunction rx() internal returns (uint256) {
        return nre.rx();
    }
}

/*
End of API
*/
"},"Randomness.sol":{"content":"// SPDX-License-Identifier: --GRISE--
pragma solidity =0.7.6;

import \"./nreAPI.sol\";

contract Randomness is usingNRE { 
    
   function stateRandomNumber() public returns (uint256) {
       uint256 randomNumber;
       randomNumber = (rm()%(10**5));
       return randomNumber;
    }
 
}

