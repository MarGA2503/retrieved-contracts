// SPDX-License-Identifier: MIT

pragma solidity \u003e=0.6.0 \u003c0.8.0;

/*
 * @dev Provides information about the current execution context, including the
 * sender of the transaction and its data. While these are generally available
 * via msg.sender and msg.data, they should not be accessed in such a direct
 * manner, since when dealing with GSN meta-transactions the account sending and
 * paying for execution may not be the actual sender (as far as an application
 * is concerned).
 *
 * This contract is only required for intermediate, library-like contracts.
 */
abstract contract Context {
    function _msgSender() internal view virtual returns (address payable) {
        return msg.sender;
    }

    function _msgData() internal view virtual returns (bytes memory) {
        this; // silence state mutability warning without generating bytecode - see https://github.com/ethereum/solidity/issues/2691
        return msg.data;
    }
}
"},"FluxAggregatorSweeper.sol":{"content":"// SPDX-License-Identifier: UNLICENSED
pragma solidity 0.7.4;

import \"./IFluxAggregator.sol\";
import \"./Sweeper.sol\";

/**
 * @title FluxAggregatorSweeper
 * @dev Handles withdrawing of rewards from flux aggregator Chainlink contracts.
 */
contract FluxAggregatorSweeper is Sweeper {
    address public oracle;

    constructor(
        address _nodeRewards,
        uint256 _minTowithdraw,
        address _oracle
    ) Sweeper(_nodeRewards, _minTowithdraw) {
        oracle = _oracle;
    }

    /**
     * @dev returns withdrawable amount for each flux aggregator
     * @return withdrawable balance of each flux aggregator
     **/
    function withdrawable() external view override returns (uint256[] memory) {
        uint256[] memory _withdrawable = new uint256[](contracts.length);
        for (uint i = 0; i \u003c contracts.length; i++) {
            _withdrawable[i] = IFluxAggregator(contracts[i]).withdrawablePayment(oracle);
        }
        return _withdrawable;
    }

    /**
     * @dev withdraw rewards from flux aggregators
     * @param _contractIdxs indexes corresponding to the flux aggregators
     **/
    function _withdraw(uint256[] calldata _contractIdxs) internal override {
        for (uint i = 0; i \u003c _contractIdxs.length; i++) {
            require(_contractIdxs[i] \u003c contracts.length, \"contractIdx must be \u003c contracts length\");
            IFluxAggregator aggregator = IFluxAggregator(contracts[_contractIdxs[i]]);
            uint256 amount = aggregator.withdrawablePayment(oracle);
            if (amount \u003e= minToWithdraw) {
                aggregator.withdrawPayment(oracle, msg.sender, amount);
            }
        }
    }

    /**
     * @dev transfers admin to new address for selected flux aggregators
     * @param _contractIdxs indexes corresponsing to flux aggregators
     * @param _newAdmin address to transfer admin to
     **/
    function _transferAdmin(uint256[] calldata _contractIdxs, address _newAdmin) internal override {
        for (uint i = 0; i \u003c _contractIdxs.length; i++) {
            require(_contractIdxs[i] \u003c contracts.length, \"contractIdx must be \u003c contracts length\");
            IFluxAggregator(contracts[_contractIdxs[i]]).transferAdmin(oracle, _newAdmin);
        }
    }

    /**
     * @dev accepts admin for flux aggregators
     * @param _contractIdxs corresponding to the flux aggregators
     **/
    function _acceptAdmin(uint256[] calldata _contractIdxs) internal override {
        for (uint i = 0; i \u003c _contractIdxs.length; i++) {
            require(_contractIdxs[i] \u003c contracts.length, \"contractIdx must be \u003c contracts length\");
            IFluxAggregator(contracts[_contractIdxs[i]]).acceptAdmin(oracle);
        }
    }
}
"},"IFluxAggregator.sol":{"content":"// SPDX-License-Identifier: UNLICENSED
pragma solidity 0.7.4;

interface IFluxAggregator {
    function withdrawablePayment(address _oracle) external view returns (uint256);

    function withdrawPayment(
        address _oracle,
        address _recipient,
        uint256 _amount
    ) external;

    function transferAdmin(address _oracle, address _newAdmin) external;

    function acceptAdmin(address _oracle) external;
}
"},"Ownable.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity \u003e=0.6.0 \u003c0.8.0;

import \"./Context.sol\";
/**
 * @dev Contract module which provides a basic access control mechanism, where
 * there is an account (an owner) that can be granted exclusive access to
 * specific functions.
 *
 * By default, the owner account will be the one that deploys the contract. This
 * can later be changed with {transferOwnership}.
 *
 * This module is used through inheritance. It will make available the modifier
 * `onlyOwner`, which can be applied to your functions to restrict their use to
 * the owner.
 */
abstract contract Ownable is Context {
    address private _owner;

    event OwnershipTransferred(address indexed previousOwner, address indexed newOwner);

    /**
     * @dev Initializes the contract setting the deployer as the initial owner.
     */
    constructor () internal {
        address msgSender = _msgSender();
        _owner = msgSender;
        emit OwnershipTransferred(address(0), msgSender);
    }

    /**
     * @dev Returns the address of the current owner.
     */
    function owner() public view virtual returns (address) {
        return _owner;
    }

    /**
     * @dev Throws if called by any account other than the owner.
     */
    modifier onlyOwner() {
        require(owner() == _msgSender(), \"Ownable: caller is not the owner\");
        _;
    }

    /**
     * @dev Leaves the contract without owner. It will not be possible to call
     * `onlyOwner` functions anymore. Can only be called by the current owner.
     *
     * NOTE: Renouncing ownership will leave the contract without an owner,
     * thereby removing any functionality that is only available to the owner.
     */
    function renounceOwnership() public virtual onlyOwner {
        emit OwnershipTransferred(_owner, address(0));
        _owner = address(0);
    }

    /**
     * @dev Transfers ownership of the contract to a new account (`newOwner`).
     * Can only be called by the current owner.
     */
    function transferOwnership(address newOwner) public virtual onlyOwner {
        require(newOwner != address(0), \"Ownable: new owner is the zero address\");
        emit OwnershipTransferred(_owner, newOwner);
        _owner = newOwner;
    }
}
"},"Sweeper.sol":{"content":"// SPDX-License-Identifier: UNLICENSED
pragma solidity 0.7.4;

import \"./Ownable.sol\";

/**
 * @title Sweeper
 * @dev Base sweeper contract that other sweeper contracts should inherit from
 */
abstract contract Sweeper is Ownable {
    uint256 public minToWithdraw;

    address[] public contracts;
    address nodeRewards;

    modifier onlyNodeRewards() {
        require(nodeRewards == msg.sender, \"NodeRewards only\");
        _;
    }

    constructor(address _nodeRewards, uint256 _minToWithdraw) {
        nodeRewards = _nodeRewards;
        minToWithdraw = _minToWithdraw;
    }

    /**
     * @dev returns current list of contracts
     * @return list of contracts
     **/
    function getContracts() external view returns (address[] memory) {
        return contracts;
    }

    /**
     * @dev withdraws rewards from contracts
     * @param _contractIdxs indexes corresponding to the contracts
     **/
    function withdraw(uint256[] calldata _contractIdxs) external virtual onlyNodeRewards() {
        require(_contractIdxs.length \u003c= contracts.length, \"contractIdxs length must be \u003c= contracts length\");
        _withdraw(_contractIdxs);
    }

    /**
     * @dev returns the withdrawable amount for each contract
     * @return withdrawable balance of each contract
     **/
    function withdrawable() external view virtual returns (uint256[] memory);

    /**
     * @dev transfers admin to new address for selected contracts
     * @param _contractIdxs indexes corresponsing to contracts
     * @param _newAdmin address to transfer admin to
     **/
    function transferAdmin(uint256[] calldata _contractIdxs, address _newAdmin) external onlyOwner() {
        require(_contractIdxs.length \u003c= contracts.length, \"contractIdxs length must be \u003c= contracts length\");
        _transferAdmin(_contractIdxs, _newAdmin);
    }

    /**
     * @dev accepts admin transfer for selected contracts
     * @param _contractIdxs indexes corresponsing to contracts
     **/
    function acceptAdmin(uint256[] calldata _contractIdxs) external onlyOwner() {
        require(_contractIdxs.length \u003c= contracts.length, \"contractIdxs length must be \u003c= contracts length\");
        _acceptAdmin(_contractIdxs);
    }

    /**
     * @dev sets the minimum amount needed to withdraw for each contract
     * @param _minToWithdraw amount to set
     **/
    function setMinToWithdraw(uint256 _minToWithdraw) external onlyOwner() {
        minToWithdraw = _minToWithdraw;
    }

    /**
     * @dev adds contract addresses
     * @param _contracts contracts to add
     **/
    function addContracts(address[] calldata _contracts) external onlyOwner() {
        for (uint i = 0; i \u003c _contracts.length; i++) {
            contracts.push(_contracts[i]);
        }
    }

    /**
     * @dev removes contract address
     * @param _index index of contract to remove
     **/
    function removeContract(uint256 _index) external onlyOwner() {
        require(_index \u003c contracts.length, \"Contract does not exist\");

        contracts[_index] = contracts[contracts.length - 1];
        delete contracts[contracts.length - 1];
    }

    /**
     * @dev withdraws rewards from contracts
     * @param _contractIdxs indexes corresponding to the contracts
     **/
    function _withdraw(uint256[] calldata _contractIdxs) internal virtual;

    /**
     * @dev transfers admin to new address for selected contracts
     * @param _contractIdxs indexes corresponsing to contracts
     * @param _newAdmin address to transfer admin to
     **/
    function _transferAdmin(uint256[] calldata _contractIdxs, address _newAdmin) internal virtual;

    /**
     * @dev accepts admin transfer for selected contracts
     * @param _contractIdxs indexes corresponsing to contracts
     **/
    function _acceptAdmin(uint256[] calldata _contractIdxs) internal virtual {}
}

