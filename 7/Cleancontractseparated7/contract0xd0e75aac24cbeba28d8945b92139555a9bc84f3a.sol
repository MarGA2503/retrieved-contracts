pragma solidity ^0.7.0;

import { SafeMath } from \"./SafeMath.sol\";

interface TokenInterface {
    function transfer(address, uint) external returns (bool);
    function balanceOf(address) external view returns (uint256);
}

interface UniswapV2Pair {
    function sync() external;
}

contract PastaChef {
    using SafeMath for uint256;

    /// @notice Contract name
    string public constant name = \"Pasta Chef v1\";

    /// @notice Contract owner (Timelock contract)
    address public immutable owner;

    /// @notice Reward starting block
    uint256 public immutable startBlock;

    /// @notice Reward ending block
    uint256 public endBlock;

    /// @notice Last block in which pasta rewards have been distributed
    uint256 public lastUpdatedBlock;

    /// @notice Pasta reward per block
    uint256 public pastaPerBlock;

    /// @notice ETH/PASTA v2 Uniswap pool
    UniswapV2Pair public constant pool = UniswapV2Pair(0xE92346d9369Fe03b735Ed9bDeB6bdC2591b8227E);

    /// @notice PASTA v2 token
    TokenInterface public constant pasta = TokenInterface(0xE54f9E6Ab80ebc28515aF8b8233c1aeE6506a15E);

    event Claimed(address indexed claimer, uint256 amount);
    event UpdateRewardRate(uint256 oldRate, uint256 newRate);
    event UpdateEndBlock(uint256 oldEnd, uint256 newEnd);

    constructor(address _timelock, uint256 _startBlock, uint256 _endBlock, uint256 _pastaPerBlock) {
        require(_timelock != address(0x0), \"PastaChef::invalid-address\");
        require(_startBlock \u003e= block.number, \"PastaChef::invalid-start-block\");
        require(_endBlock \u003e= block.number \u0026\u0026 _endBlock \u003e _startBlock, \"PastaChef::invalid-end-block\");
        require(_pastaPerBlock \u003e 0, \"PastaChef::invalid-rewards\");

        owner = _timelock;
        startBlock = _startBlock;
        endBlock = _endBlock;
        lastUpdatedBlock = _startBlock;
        pastaPerBlock = _pastaPerBlock;
    }

    modifier onlyOwner {
        require(msg.sender == owner, \"PastaChef::unauthorized\");
        _;
    }

    function pendingRewards() public view returns (uint256) {
        if (block.number \u003c= startBlock || lastUpdatedBlock \u003e endBlock) return 0;

        uint256 blockNumber = block.number;
        if (blockNumber \u003e endBlock) {
            blockNumber = endBlock;
        }
        return blockNumber.sub(lastUpdatedBlock).mul(pastaPerBlock);
    }

    function claimForAll() public {
        uint256 rewards = pendingRewards();
        require(rewards \u003e 0, \"PastaChef::already-claimed\");

        uint256 balance = pasta.balanceOf(address(this));
        require(balance \u003e= rewards, \"PastaChef::insufficient-pasta\");

        require(pasta.transfer(address(pool), rewards), \"PastaChef::failed-to-distribute\");
        pool.sync();

        lastUpdatedBlock = block.number;

        emit Claimed(msg.sender, rewards);
    }

    function updateRewardRate(uint256 _pastaPerBlock) external onlyOwner {
        require(_pastaPerBlock \u003e 0, \"PastaChef::invalid-rewards\");

        claimForAll();

        emit UpdateRewardRate(pastaPerBlock, _pastaPerBlock);

        pastaPerBlock = _pastaPerBlock;
    }

    function updateEndBlock(uint256 _endBlock) external onlyOwner {
        require(_endBlock \u003e= block.number \u0026\u0026 _endBlock \u003e startBlock, \"PastaChef::invalid-end-block\");
        require(endBlock \u003e block.number, \"PastaChef::reward-period-over\");

        emit UpdateEndBlock(endBlock, _endBlock);

        endBlock = _endBlock;
    }

    function sweep(address to) external onlyOwner {
        require(block.number \u003e endBlock, \"PastaChef::reward-period-not-over\");
        uint256 balance = pasta.balanceOf(address(this));

        require(pasta.transfer(to, balance));
    }
}"},"SafeMath.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.7.0;

/**
 * @dev Wrappers over Solidity\u0027s arithmetic operations with added overflow
 * checks.
 *
 * Arithmetic operations in Solidity wrap on overflow. This can easily result
 * in bugs, because programmers usually assume that an overflow raises an
 * error, which is the standard behavior in high level programming languages.
 * `SafeMath` restores this intuition by reverting the transaction when an
 * operation overflows.
 *
 * Using this library instead of the unchecked operations eliminates an entire
 * class of bugs, so it\u0027s recommended to use it always.
 */
library SafeMath {
    /**
     * @dev Returns the addition of two unsigned integers, with an overflow flag.
     *
     * _Available since v3.4._
     */
    function tryAdd(uint256 a, uint256 b) internal pure returns (bool, uint256) {
        uint256 c = a + b;
        if (c \u003c a) return (false, 0);
        return (true, c);
    }

    /**
     * @dev Returns the substraction of two unsigned integers, with an overflow flag.
     *
     * _Available since v3.4._
     */
    function trySub(uint256 a, uint256 b) internal pure returns (bool, uint256) {
        if (b \u003e a) return (false, 0);
        return (true, a - b);
    }

    /**
     * @dev Returns the multiplication of two unsigned integers, with an overflow flag.
     *
     * _Available since v3.4._
     */
    function tryMul(uint256 a, uint256 b) internal pure returns (bool, uint256) {
        // Gas optimization: this is cheaper than requiring \u0027a\u0027 not being zero, but the
        // benefit is lost if \u0027b\u0027 is also tested.
        // See: https://github.com/OpenZeppelin/openzeppelin-contracts/pull/522
        if (a == 0) return (true, 0);
        uint256 c = a * b;
        if (c / a != b) return (false, 0);
        return (true, c);
    }

    /**
     * @dev Returns the division of two unsigned integers, with a division by zero flag.
     *
     * _Available since v3.4._
     */
    function tryDiv(uint256 a, uint256 b) internal pure returns (bool, uint256) {
        if (b == 0) return (false, 0);
        return (true, a / b);
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers, with a division by zero flag.
     *
     * _Available since v3.4._
     */
    function tryMod(uint256 a, uint256 b) internal pure returns (bool, uint256) {
        if (b == 0) return (false, 0);
        return (true, a % b);
    }

    /**
     * @dev Returns the addition of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity\u0027s `+` operator.
     *
     * Requirements:
     *
     * - Addition cannot overflow.
     */
    function add(uint256 a, uint256 b) internal pure returns (uint256) {
        uint256 c = a + b;
        require(c \u003e= a, \"SafeMath: addition overflow\");
        return c;
    }

    /**
     * @dev Returns the subtraction of two unsigned integers, reverting on
     * overflow (when the result is negative).
     *
     * Counterpart to Solidity\u0027s `-` operator.
     *
     * Requirements:
     *
     * - Subtraction cannot overflow.
     */
    function sub(uint256 a, uint256 b) internal pure returns (uint256) {
        require(b \u003c= a, \"SafeMath: subtraction overflow\");
        return a - b;
    }

    /**
     * @dev Returns the multiplication of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity\u0027s `*` operator.
     *
     * Requirements:
     *
     * - Multiplication cannot overflow.
     */
    function mul(uint256 a, uint256 b) internal pure returns (uint256) {
        if (a == 0) return 0;
        uint256 c = a * b;
        require(c / a == b, \"SafeMath: multiplication overflow\");
        return c;
    }

    /**
     * @dev Returns the integer division of two unsigned integers, reverting on
     * division by zero. The result is rounded towards zero.
     *
     * Counterpart to Solidity\u0027s `/` operator. Note: this function uses a
     * `revert` opcode (which leaves remaining gas untouched) while Solidity
     * uses an invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function div(uint256 a, uint256 b) internal pure returns (uint256) {
        require(b \u003e 0, \"SafeMath: division by zero\");
        return a / b;
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
     * reverting when dividing by zero.
     *
     * Counterpart to Solidity\u0027s `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function mod(uint256 a, uint256 b) internal pure returns (uint256) {
        require(b \u003e 0, \"SafeMath: modulo by zero\");
        return a % b;
    }

    /**
     * @dev Returns the subtraction of two unsigned integers, reverting with custom message on
     * overflow (when the result is negative).
     *
     * CAUTION: This function is deprecated because it requires allocating memory for the error
     * message unnecessarily. For custom revert reasons use {trySub}.
     *
     * Counterpart to Solidity\u0027s `-` operator.
     *
     * Requirements:
     *
     * - Subtraction cannot overflow.
     */
    function sub(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b \u003c= a, errorMessage);
        return a - b;
    }

    /**
     * @dev Returns the integer division of two unsigned integers, reverting with custom message on
     * division by zero. The result is rounded towards zero.
     *
     * CAUTION: This function is deprecated because it requires allocating memory for the error
     * message unnecessarily. For custom revert reasons use {tryDiv}.
     *
     * Counterpart to Solidity\u0027s `/` operator. Note: this function uses a
     * `revert` opcode (which leaves remaining gas untouched) while Solidity
     * uses an invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function div(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b \u003e 0, errorMessage);
        return a / b;
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
     * reverting with custom message when dividing by zero.
     *
     * CAUTION: This function is deprecated because it requires allocating memory for the error
     * message unnecessarily. For custom revert reasons use {tryMod}.
     *
     * Counterpart to Solidity\u0027s `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function mod(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b \u003e 0, errorMessage);
        return a % b;
    }
}
