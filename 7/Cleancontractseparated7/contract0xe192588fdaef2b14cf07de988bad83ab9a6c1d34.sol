// SPDX-License-Identifier: Apache-2.0
pragma solidity 0.7.4;


interface IERC1155 {

  /****************************************|
  |                 Events                 |
  |_______________________________________*/

  /**
   * @dev Either TransferSingle or TransferBatch MUST emit when tokens are transferred, including zero amount transfers as well as minting or burning
   *   Operator MUST be msg.sender
   *   When minting/creating tokens, the `_from` field MUST be set to `0x0`
   *   When burning/destroying tokens, the `_to` field MUST be set to `0x0`
   *   The total amount transferred from address 0x0 minus the total amount transferred to 0x0 may be used by clients and exchanges to be added to the \"circulating supply\" for a given token ID
   *   To broadcast the existence of a token ID with no initial balance, the contract SHOULD emit the TransferSingle event from `0x0` to `0x0`, with the token creator as `_operator`, and a `_amount` of 0
   */
  event TransferSingle(address indexed _operator, address indexed _from, address indexed _to, uint256 _id, uint256 _amount);

  /**
   * @dev Either TransferSingle or TransferBatch MUST emit when tokens are transferred, including zero amount transfers as well as minting or burning
   *   Operator MUST be msg.sender
   *   When minting/creating tokens, the `_from` field MUST be set to `0x0`
   *   When burning/destroying tokens, the `_to` field MUST be set to `0x0`
   *   The total amount transferred from address 0x0 minus the total amount transferred to 0x0 may be used by clients and exchanges to be added to the \"circulating supply\" for a given token ID
   *   To broadcast the existence of multiple token IDs with no initial balance, this SHOULD emit the TransferBatch event from `0x0` to `0x0`, with the token creator as `_operator`, and a `_amount` of 0
   */
  event TransferBatch(address indexed _operator, address indexed _from, address indexed _to, uint256[] _ids, uint256[] _amounts);

  /**
   * @dev MUST emit when an approval is updated
   */
  event ApprovalForAll(address indexed _owner, address indexed _operator, bool _approved);


  /****************************************|
  |                Functions               |
  |_______________________________________*/

  /**
    * @notice Transfers amount of an _id from the _from address to the _to address specified
    * @dev MUST emit TransferSingle event on success
    * Caller must be approved to manage the _from account\u0027s tokens (see isApprovedForAll)
    * MUST throw if `_to` is the zero address
    * MUST throw if balance of sender for token `_id` is lower than the `_amount` sent
    * MUST throw on any other error
    * When transfer is complete, this function MUST check if `_to` is a smart contract (code size \u003e 0). If so, it MUST call `onERC1155Received` on `_to` and revert if the return amount is not `bytes4(keccak256(\"onERC1155Received(address,address,uint256,uint256,bytes)\"))`
    * @param _from    Source address
    * @param _to      Target address
    * @param _id      ID of the token type
    * @param _amount  Transfered amount
    * @param _data    Additional data with no specified format, sent in call to `_to`
    */
  function safeTransferFrom(address _from, address _to, uint256 _id, uint256 _amount, bytes calldata _data) external;

  /**
    * @notice Send multiple types of Tokens from the _from address to the _to address (with safety call)
    * @dev MUST emit TransferBatch event on success
    * Caller must be approved to manage the _from account\u0027s tokens (see isApprovedForAll)
    * MUST throw if `_to` is the zero address
    * MUST throw if length of `_ids` is not the same as length of `_amounts`
    * MUST throw if any of the balance of sender for token `_ids` is lower than the respective `_amounts` sent
    * MUST throw on any other error
    * When transfer is complete, this function MUST check if `_to` is a smart contract (code size \u003e 0). If so, it MUST call `onERC1155BatchReceived` on `_to` and revert if the return amount is not `bytes4(keccak256(\"onERC1155BatchReceived(address,address,uint256[],uint256[],bytes)\"))`
    * Transfers and events MUST occur in the array order they were submitted (_ids[0] before _ids[1], etc)
    * @param _from     Source addresses
    * @param _to       Target addresses
    * @param _ids      IDs of each token type
    * @param _amounts  Transfer amounts per token type
    * @param _data     Additional data with no specified format, sent in call to `_to`
  */
  function safeBatchTransferFrom(address _from, address _to, uint256[] calldata _ids, uint256[] calldata _amounts, bytes calldata _data) external;

  /**
   * @notice Get the balance of an account\u0027s Tokens
   * @param _owner  The address of the token holder
   * @param _id     ID of the Token
   * @return        The _owner\u0027s balance of the Token type requested
   */
  function balanceOf(address _owner, uint256 _id) external view returns (uint256);

  /**
   * @notice Get the balance of multiple account/token pairs
   * @param _owners The addresses of the token holders
   * @param _ids    ID of the Tokens
   * @return        The _owner\u0027s balance of the Token types requested (i.e. balance for each (owner, id) pair)
   */
  function balanceOfBatch(address[] calldata _owners, uint256[] calldata _ids) external view returns (uint256[] memory);

  /**
   * @notice Enable or disable approval for a third party (\"operator\") to manage all of caller\u0027s tokens
   * @dev MUST emit the ApprovalForAll event on success
   * @param _operator  Address to add to the set of authorized operators
   * @param _approved  True if the operator is approved, false to revoke approval
   */
  function setApprovalForAll(address _operator, bool _approved) external;

  /**
   * @notice Queries the approval status of an operator for a given owner
   * @param _owner     The owner of the Tokens
   * @param _operator  Address of authorized operator
   * @return isOperator True if the operator is approved, false if not
   */
  function isApprovedForAll(address _owner, address _operator) external view returns (bool isOperator);
}
"},"IERC165.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity \u003e=0.6.0 \u003c0.8.0;

/**
 * @dev Interface of the ERC165 standard, as defined in the
 * https://eips.ethereum.org/EIPS/eip-165[EIP].
 *
 * Implementers can declare support of contract interfaces, which can then be
 * queried by others ({ERC165Checker}).
 *
 * For an implementation, see {ERC165}.
 */
interface IERC165 {
    /**
     * @dev Returns true if this contract implements the interface defined by
     * `interfaceId`. See the corresponding
     * https://eips.ethereum.org/EIPS/eip-165#how-interfaces-are-identified[EIP section]
     * to learn more about how these ids are created.
     *
     * This function call must use less than 30 000 gas.
     */
    function supportsInterface(bytes4 interfaceId) external view returns (bool);
}
"},"IERC20.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity \u003e=0.6.0 \u003c0.8.0;

/**
 * @dev Interface of the ERC20 standard as defined in the EIP.
 */
interface IERC20 {
    /**
     * @dev Returns the amount of tokens in existence.
     */
    function totalSupply() external view returns (uint256);

    /**
     * @dev Returns the amount of tokens owned by `account`.
     */
    function balanceOf(address account) external view returns (uint256);

    /**
     * @dev Moves `amount` tokens from the caller\u0027s account to `recipient`.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transfer(address recipient, uint256 amount) external returns (bool);

    /**
     * @dev Returns the remaining number of tokens that `spender` will be
     * allowed to spend on behalf of `owner` through {transferFrom}. This is
     * zero by default.
     *
     * This value changes when {approve} or {transferFrom} are called.
     */
    function allowance(address owner, address spender) external view returns (uint256);

    /**
     * @dev Sets `amount` as the allowance of `spender` over the caller\u0027s tokens.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * IMPORTANT: Beware that changing an allowance with this method brings the risk
     * that someone may use both the old and the new allowance by unfortunate
     * transaction ordering. One possible solution to mitigate this race
     * condition is to first reduce the spender\u0027s allowance to 0 and set the
     * desired value afterwards:
     * https://github.com/ethereum/EIPs/issues/20#issuecomment-263524729
     *
     * Emits an {Approval} event.
     */
    function approve(address spender, uint256 amount) external returns (bool);

    /**
     * @dev Moves `amount` tokens from `sender` to `recipient` using the
     * allowance mechanism. `amount` is then deducted from the caller\u0027s
     * allowance.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transferFrom(address sender, address recipient, uint256 amount) external returns (bool);

    /**
     * @dev Emitted when `value` tokens are moved from one account (`from`) to
     * another (`to`).
     *
     * Note that `value` may be zero.
     */
    event Transfer(address indexed from, address indexed to, uint256 value);

    /**
     * @dev Emitted when the allowance of a `spender` for an `owner` is set by
     * a call to {approve}. `value` is the new allowance.
     */
    event Approval(address indexed owner, address indexed spender, uint256 value);
}
"},"IERC721.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity \u003e=0.6.2 \u003c0.8.0;

import \"./IERC165.sol\";

/**
 * @dev Required interface of an ERC721 compliant contract.
 */
interface IERC721 is IERC165 {
    /**
     * @dev Emitted when `tokenId` token is transferred from `from` to `to`.
     */
    event Transfer(address indexed from, address indexed to, uint256 indexed tokenId);

    /**
     * @dev Emitted when `owner` enables `approved` to manage the `tokenId` token.
     */
    event Approval(address indexed owner, address indexed approved, uint256 indexed tokenId);

    /**
     * @dev Emitted when `owner` enables or disables (`approved`) `operator` to manage all of its assets.
     */
    event ApprovalForAll(address indexed owner, address indexed operator, bool approved);

    /**
     * @dev Returns the number of tokens in ``owner``\u0027s account.
     */
    function balanceOf(address owner) external view returns (uint256 balance);

    /**
     * @dev Returns the owner of the `tokenId` token.
     *
     * Requirements:
     *
     * - `tokenId` must exist.
     */
    function ownerOf(uint256 tokenId) external view returns (address owner);

    /**
     * @dev Safely transfers `tokenId` token from `from` to `to`, checking first that contract recipients
     * are aware of the ERC721 protocol to prevent tokens from being forever locked.
     *
     * Requirements:
     *
     * - `from` cannot be the zero address.
     * - `to` cannot be the zero address.
     * - `tokenId` token must exist and be owned by `from`.
     * - If the caller is not `from`, it must be have been allowed to move this token by either {approve} or {setApprovalForAll}.
     * - If `to` refers to a smart contract, it must implement {IERC721Receiver-onERC721Received}, which is called upon a safe transfer.
     *
     * Emits a {Transfer} event.
     */
    function safeTransferFrom(address from, address to, uint256 tokenId) external;

    /**
     * @dev Transfers `tokenId` token from `from` to `to`.
     *
     * WARNING: Usage of this method is discouraged, use {safeTransferFrom} whenever possible.
     *
     * Requirements:
     *
     * - `from` cannot be the zero address.
     * - `to` cannot be the zero address.
     * - `tokenId` token must be owned by `from`.
     * - If the caller is not `from`, it must be approved to move this token by either {approve} or {setApprovalForAll}.
     *
     * Emits a {Transfer} event.
     */
    function transferFrom(address from, address to, uint256 tokenId) external;

    /**
     * @dev Gives permission to `to` to transfer `tokenId` token to another account.
     * The approval is cleared when the token is transferred.
     *
     * Only a single account can be approved at a time, so approving the zero address clears previous approvals.
     *
     * Requirements:
     *
     * - The caller must own the token or be an approved operator.
     * - `tokenId` must exist.
     *
     * Emits an {Approval} event.
     */
    function approve(address to, uint256 tokenId) external;

    /**
     * @dev Returns the account approved for `tokenId` token.
     *
     * Requirements:
     *
     * - `tokenId` must exist.
     */
    function getApproved(uint256 tokenId) external view returns (address operator);

    /**
     * @dev Approve or remove `operator` as an operator for the caller.
     * Operators can call {transferFrom} or {safeTransferFrom} for any token owned by the caller.
     *
     * Requirements:
     *
     * - The `operator` cannot be the caller.
     *
     * Emits an {ApprovalForAll} event.
     */
    function setApprovalForAll(address operator, bool _approved) external;

    /**
     * @dev Returns if the `operator` is allowed to manage all of the assets of `owner`.
     *
     * See {setApprovalForAll}
     */
    function isApprovedForAll(address owner, address operator) external view returns (bool);

    /**
      * @dev Safely transfers `tokenId` token from `from` to `to`.
      *
      * Requirements:
      *
     * - `from` cannot be the zero address.
     * - `to` cannot be the zero address.
      * - `tokenId` token must exist and be owned by `from`.
      * - If the caller is not `from`, it must be approved to move this token by either {approve} or {setApprovalForAll}.
      * - If `to` refers to a smart contract, it must implement {IERC721Receiver-onERC721Received}, which is called upon a safe transfer.
      *
      * Emits a {Transfer} event.
      */
    function safeTransferFrom(address from, address to, uint256 tokenId, bytes calldata data) external;
}
"},"PortionExchange.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity 0.7.4;

import \"./IERC721.sol\";
import \"./IERC20.sol\";
import \u0027./IERC1155.sol\u0027;

contract PortionExchange {

\tstring name = \"PortionExchange\";
\taddress signer;
\tIERC721 public artTokensContract;
\tIERC1155 public artTokens1155Contract;
\tIERC20 public potionTokensContract;
\tuint nonceCounter = 0;

\tmapping(uint =\u003e uint) public prices;

\tstruct Erc1155Offer {
\t\tuint tokenId;
\t\tuint quantity;
\t\tuint pricePerToken;
\t\taddress seller;
\t}
\tErc1155Offer[] public erc1155Offers;

\tevent TokenListed (uint indexed _tokenId, uint indexed _price, address indexed _owner);
\tevent TokenSold (uint indexed _tokenId, uint indexed _price, string indexed _currency);
\tevent TokenDeleted (uint indexed _tokenId, address indexed _previousOwner, uint indexed _previousPrice);
\tevent TokenOwned (uint indexed _tokenId, address indexed _previousOwner, address indexed _newOwner);

\tevent Token1155Listed (uint _erc1155OfferId, uint _tokenId, uint _quantity, uint _price, address _owner);
\tevent Token1155Deleted (
\t\tuint _erc1155OfferId,
\t\tuint _tokenId,
\t\tuint _previousQuantity,
\t\tuint _previousPrice,
\t\taddress _previousOwner
\t);
\tevent Token1155Sold(
\t\tuint _erc1155OfferId,
\t\tuint _tokenId,
\t\tuint _quantity,
\t\tuint _price,
\t\tstring _currency,
\t\taddress _previousOwner,
\t\taddress _newOwner
\t);
\tconstructor (address _artTokensAddress, address _artToken1155Address, address _portionTokensAddress) public {
\t\tsigner = msg.sender;
\t\tartTokensContract = IERC721(_artTokensAddress);
\t\tartTokens1155Contract = IERC1155(_artToken1155Address);
\t\tpotionTokensContract = IERC20(_portionTokensAddress);
\t\trequire (_artTokensAddress != address(0), \"_artTokensAddress is null\");
\t\trequire (_artToken1155Address != address(0), \"_artToken1155Address is null\");
\t\trequire (_portionTokensAddress != address(0), \"_portionTokensAddress is null\");
\t}

\tfunction listToken(uint _tokenId, uint _price) external {
\t\taddress owner = artTokensContract.ownerOf(_tokenId);
\t\trequire(owner == msg.sender, \u0027message sender is not the owner\u0027);
\t\tprices[_tokenId] = _price;
\t\temit TokenListed(_tokenId, _price, msg.sender);
\t}

\tfunction listToken1155(uint _tokenId, uint _quantity, uint _price) external returns (uint) {
\t\trequire(artTokens1155Contract.balanceOf(msg.sender, _tokenId) \u003e= _quantity, \u0027Not enough balance\u0027);
\t\tuint tokenListed = 0;
\t\tfor (uint i = 0; i \u003c erc1155Offers.length; i++) {
\t\t\tif (erc1155Offers[i].seller == msg.sender \u0026\u0026 erc1155Offers[i].tokenId == _tokenId) {
\t\t\t\ttokenListed += erc1155Offers[i].quantity;
\t\t\t}
\t\t}
\t\trequire(artTokens1155Contract.balanceOf(msg.sender, _tokenId) \u003e= _quantity + tokenListed, \u0027Not enough balance\u0027);

\t\terc1155Offers.push(Erc1155Offer({
\t\t\ttokenId: _tokenId,
\t\t\tquantity: _quantity,
\t\t\tpricePerToken: _price,
\t\t\tseller: msg.sender
\t\t}));
\t\tuint offerId = erc1155Offers.length - 1;

\t\temit Token1155Listed(offerId, _tokenId, _quantity, _price, msg.sender);

\t\treturn offerId;
\t}

\tfunction removeListToken(uint _tokenId) external {
\t\taddress owner = artTokensContract.ownerOf(_tokenId);
\t\trequire(owner == msg.sender, \u0027message sender is not the owner\u0027);
\t\tdeleteToken(_tokenId, owner);
\t}

\tfunction removeListToken1155(uint _offerId) external {
\t\trequire(erc1155Offers[_offerId].seller == msg.sender, \u0027message sender is not the owner\u0027);
\t\tdeleteToken1155(_offerId);
\t}

\tfunction isValidBuyOrder(uint _tokenId, uint _askPrice) private view returns (bool) {
\t\trequire(prices[_tokenId] \u003e 0, \"invalid price, token is not for sale\");
\t\treturn (_askPrice \u003e= prices[_tokenId]);
\t}

\tfunction isValidBuyOrder1155(uint _offerId, uint _amount, uint _askPrice) private view returns (bool) {
\t\trequire(erc1155Offers[_offerId].pricePerToken \u003e 0, \"invalid price, token is not for sale\");
\t\treturn (_askPrice \u003e= _amount * erc1155Offers[_offerId].pricePerToken);
\t}

\tfunction deleteToken(uint _tokenId, address owner) private {
\t\temit TokenDeleted(_tokenId, owner, prices[_tokenId]);
\t\tdelete prices[_tokenId];
\t}

\tfunction deleteToken1155(uint _offerId) private {
\t\temit Token1155Deleted(_offerId, erc1155Offers[_offerId].tokenId, erc1155Offers[_offerId].quantity, erc1155Offers[_offerId].pricePerToken, erc1155Offers[_offerId].seller);
\t\tdelete erc1155Offers[_offerId];
\t}

\tfunction listingPrice(uint _tokenId) external view returns (uint) {
\t\treturn prices[_tokenId];
\t}

\tfunction listing1155Price(uint _offerId) external view returns (uint) {
\t\treturn erc1155Offers[_offerId].pricePerToken;
\t}

\tfunction buyToken(uint _tokenId, uint _nonce) external payable {
\t\tnonceCounter++;
\t\trequire(nonceCounter == _nonce, \"invalid nonce\");

\t\trequire(isValidBuyOrder(_tokenId, msg.value), \"invalid price\");

\t\taddress owner = artTokensContract.ownerOf(_tokenId);
\t\taddress payable payableOwner = address(uint160(owner));
\t\tpayableOwner.transfer(msg.value);
\t\tartTokensContract.safeTransferFrom(owner, msg.sender, _tokenId);
\t\temit TokenSold(_tokenId, msg.value, \"ETH\");
\t\temit TokenOwned(_tokenId, owner, msg.sender);
\t\tdeleteToken(_tokenId, owner);
\t}

\tfunction buyToken1155(uint _offerId, uint _quantity, uint _nonce) external payable {
\t\tnonceCounter++;
\t\trequire(nonceCounter == _nonce, \"invalid nonce\");
\t\trequire(_quantity \u003c= erc1155Offers[_offerId].quantity, \"invalid quantity\");

\t\trequire(isValidBuyOrder1155(_offerId, _quantity, msg.value), \"invalid price\");

\t\taddress owner = erc1155Offers[_offerId].seller;
\t\taddress payable payableOwner = address(uint160(owner));
\t\tpayableOwner.transfer(msg.value);
\t\tartTokens1155Contract.safeTransferFrom(owner, msg.sender, erc1155Offers[_offerId].tokenId, _quantity, \"\");
\t\temit Token1155Sold(_offerId,
\t\t\terc1155Offers[_offerId].tokenId,
\t\t\t_quantity,
\t\t\terc1155Offers[_offerId].pricePerToken,
\t\t\t\"ETH\",
\t\t\towner,
\t\t\tmsg.sender
\t\t);
\t\tif (erc1155Offers[_offerId].quantity == _quantity) {
\t\t\tdeleteToken1155(_offerId);
\t\t} else {
\t\t\terc1155Offers[_offerId].quantity -= _quantity;
\t\t}
\t}

\tfunction buyTokenForPRT(uint _tokenId, uint256 _amountOfPRT, uint256 _nonce, bytes calldata _signature) external {
\t\tnonceCounter++;
\t\trequire(nonceCounter == _nonce, \"invalid nonce\");

\t\tbytes32 hash = keccak256(abi.encodePacked(_tokenId, _amountOfPRT, _nonce));
\t\tbytes32 ethSignedMessageHash = keccak256(abi.encodePacked(\"\\x19Ethereum Signed Message:\
32\", hash));
\t\taddress recoveredSignerAddress = recoverSignerAddress(ethSignedMessageHash, _signature);
\t\trequire(recoveredSignerAddress == signer, \"invalid secret signer\"); // to be sure that the price in PRT is correct

\t\trequire(prices[_tokenId] \u003e 0, \"invalid price, token is not for sale\");

\t\taddress owner = artTokensContract.ownerOf(_tokenId);
\t\tpotionTokensContract.transferFrom(msg.sender, owner, _amountOfPRT);
\t\tartTokensContract.safeTransferFrom(owner, msg.sender, _tokenId);
\t\temit TokenSold(_tokenId, _amountOfPRT, \"PRT\");
\t\temit TokenOwned(_tokenId, owner, msg.sender);
\t\tdeleteToken(_tokenId, owner);
\t}

\tfunction buyArtwork1155ForPRT(uint256 _offerId, uint256 _quantity, uint256 _amountOfPRT, uint256 _nonce, bytes calldata _signature) external {
\t\tnonceCounter++;
\t\trequire(nonceCounter == _nonce, \"invalid nonce\");

\t\tbytes32 hash = keccak256(abi.encodePacked(_offerId, _quantity, _amountOfPRT, _nonce));
\t\tbytes32 ethSignedMessageHash = keccak256(abi.encodePacked(\"\\x19Ethereum Signed Message:\
32\", hash));
\t\taddress recoveredSignerAddress = recoverSignerAddress(ethSignedMessageHash, _signature);
\t\trequire(recoveredSignerAddress == signer, \"invalid secret signer\"); // to be sure that the price in PRT is correct

\t\trequire(erc1155Offers[_offerId].pricePerToken \u003e 0, \"invalid price, token is not for sale\");

\t\taddress owner = erc1155Offers[_offerId].seller;
\t\tpotionTokensContract.transferFrom(msg.sender, owner, _amountOfPRT * _quantity);
\t\tartTokens1155Contract.safeTransferFrom(owner, msg.sender, erc1155Offers[_offerId].tokenId, _quantity, \"\");
\t\temit Token1155Sold(_offerId,
\t\t\terc1155Offers[_offerId].tokenId,
\t\t\t_quantity,
\t\t\t_amountOfPRT,
\t\t\t\"PRT\",
\t\t\towner,
\t\t\tmsg.sender
\t\t);
\t\tif (erc1155Offers[_offerId].quantity == _quantity) {
\t\t\tdeleteToken1155(_offerId);
\t\t} else {
\t\t\terc1155Offers[_offerId].quantity -= _quantity;
\t\t}
\t}

\tfunction recoverSignerAddress(bytes32 _hash, bytes memory _signature) public pure returns (address) {
\t\trequire(_signature.length == 65, \"invalid signature length\");

\t\tbytes32 r;
\t\tbytes32 s;
\t\tuint8 v;

\t\tassembly {
\t\t\tr := mload(add(_signature, 32))
\t\t\ts := mload(add(_signature, 64))
\t\t\tv := and(mload(add(_signature, 65)), 255)
\t\t}

\t\tif (v \u003c 27) {
\t\t\tv += 27;
\t\t}

\t\tif (v != 27 \u0026\u0026 v != 28) {
\t\t\treturn address(0);
\t\t}

\t\treturn ecrecover(_hash, v, r, s);
\t}

\tfunction getName() external view returns (string memory) {
\t\treturn name;
\t}

\tfunction getSigner() external view returns (address) {
\t\treturn signer;
\t}

\tfunction setSigner(address _newSigner) external {
\t\trequire(msg.sender == signer, \"not enough permissions to change the signer\");
\t\tsigner = _newSigner;
\t}

\tfunction getNextNonce() external view returns (uint) {
\t\treturn nonceCounter + 1;
\t}

\tfunction getArtwork1155Owner(uint _offerId) external view returns (address) {
\t\treturn erc1155Offers[_offerId].seller;
\t}
}

