// SPDX-License-Identifier: MIT
pragma solidity ^0.7.4;

import \"IERC20.sol\";
import \"SafeMath.sol\";

contract FourTBank is IERC20 {

    mapping(address =\u003e mapping (address =\u003e uint256)) allowed;
    mapping(address =\u003e uint256) balances;
    
    address _owner;
    uint256 _totalSupply;
    
    uint8 public constant decimals = 4;
    string public constant name = \"4TB Coin\";
    string public constant symbol = \"4TB\";

    using SafeMath for uint256;

    constructor(uint256 total) {
        _owner = msg.sender;
        _totalSupply = total;
        balances[msg.sender] = _totalSupply;
    }

    function allowance(address owner, address delegate) public override view returns (uint) {
        return allowed[owner][delegate];
    }

    function approve(address delegate, uint256 numTokens) public override returns (bool) {
        allowed[msg.sender][delegate] = numTokens;
        emit Approval(msg.sender, delegate, numTokens);
        return true;
    }

    function balanceOf(address tokenOwner) public override view returns (uint256) {
        return balances[tokenOwner];
    }

    function burn(uint256 amount) public {
        require(amount \u003c= balances[msg.sender]);
        _totalSupply -= amount;
        balances[msg.sender] -= amount;
        emit Transfer(msg.sender, address(0), amount);
    }

    function mint(uint256 amount) public {
        require(msg.sender == _owner);
        _totalSupply += amount;
        balances[_owner] += amount;
        emit Transfer(address(0), _owner, amount);
    }

    function totalSupply() public override view returns (uint256) {
        return _totalSupply;
    }

    function transfer(address receiver, uint256 numTokens) public override returns (bool) {
        require(numTokens \u003c= balances[msg.sender]);
        balances[msg.sender] = balances[msg.sender].sub(numTokens);
        balances[receiver] = balances[receiver].add(numTokens);
        emit Transfer(msg.sender, receiver, numTokens);
        return true;
    }

    function transferFrom(address owner, address buyer, uint256 numTokens) public override returns (bool) {
        require(numTokens \u003c= balances[owner]);
        require(numTokens \u003c= allowed[owner][msg.sender]);

        balances[owner] = balances[owner].sub(numTokens);
        allowed[owner][msg.sender] = allowed[owner][msg.sender].sub(numTokens);
        balances[buyer] = balances[buyer].add(numTokens);
        emit Transfer(owner, buyer, numTokens);
        return true;
    }
}
"},"IERC20.sol":{"content":"// SPDX-License-Identifier: MIT
pragma solidity ^0.7.4;

interface IERC20 {

    function totalSupply() external view returns (uint256);
    function balanceOf(address account) external view returns (uint256);
    function allowance(address owner, address spender) external view returns (uint256);
    function transfer(address recipient, uint256 amount) external returns (bool);
    function approve(address spender, uint256 amount) external returns (bool);
    function transferFrom(address sender, address recipient, uint256 amount) external returns (bool);

    event Transfer(address indexed from, address indexed to, uint256 value);
    event Approval(address indexed owner, address indexed spender, uint256 value);
}
"},"SafeMath.sol":{"content":"// SPDX-License-Identifier: MIT
pragma solidity ^0.7.4;

library SafeMath {
    function sub(uint256 a, uint256 b) internal pure returns (uint256) {
      assert(b \u003c= a);
      return a - b;
    }

    function add(uint256 a, uint256 b) internal pure returns (uint256) {
      uint256 c = a + b;
      assert(c \u003e= a);
      return c;
    }
}

