// SPDX-License-Identifier: GPL-3.0
pragma solidity \u003e=0.8.0 \u003c0.9.0;

import \"./ICoinvestingDeFiERC20.sol\";
import \"./SafeMath.sol\";

contract CoinvestingDeFiERC20 is ICoinvestingDeFiERC20 {
    using SafeMath for uint;
    // Public variables
    uint8 public constant override decimals = 18;    
    string public constant override name = \"Coinvesting DeFi V2\";
    string public constant override symbol = \"COINVEX-V2\";    
    uint public override totalSupply;

    bytes32 public override DOMAIN_SEPARATOR;
    // keccak256(\"Permit(address owner,address spender,uint256 value,uint256 nonce,uint256 deadline)\");
    bytes32 public constant override PERMIT_TYPEHASH = 0x6e71edae12b1b97f4d1f60370fef10105fa2faae0126114a169c64845d6126c9;
    
    mapping(address =\u003e mapping(address =\u003e uint)) public override allowance;
    mapping(address =\u003e uint) public override balanceOf;
    mapping(address =\u003e uint) public override nonces;

    constructor() {
        uint chainId;
        assembly {
            chainId := chainid()
        }
        DOMAIN_SEPARATOR = keccak256(
            abi.encode(
                keccak256(\u0027EIP712Domain(string name,string version,uint256 chainId,address verifyingContract)\u0027),
                keccak256(bytes(name)),
                keccak256(bytes(\u00271\u0027)),
                chainId,
                address(this)
            )
        );
    }

    // External functions
    function approve(
        address spender,
        uint value
    )
    external
    virtual 
    override
    returns (bool)
    {
        _approve(
            payable(msg.sender), 
            spender, 
            value
        );
        return true;
    }

    function permit(
        address owner,
        address spender,
        uint value,
        uint deadline,
        uint8 v,
        bytes32 r,
        bytes32 s
    )
    external 
    override
    {
        require(deadline \u003e= block.timestamp, \"ERC20: EXPD\");
        bytes32 digest = keccak256(
            abi.encodePacked(
                \u0027\\x19\\x01\u0027,
                DOMAIN_SEPARATOR,
                keccak256(
                    abi.encode(
                        PERMIT_TYPEHASH,
                        owner,
                        spender,
                        value,
                        nonces[owner]++,
                        deadline
                    )
                )
            )
        );
        address recoveredAddress = ecrecover(digest, v, r, s);
        require(recoveredAddress != address(0) \u0026\u0026 recoveredAddress == owner,
            \"ERC20: INV_SIG\");
        _approve(owner, spender, value);
    }

    function transfer(
        address to,
        uint value
    )
    external
    virtual 
    override
    returns (bool)
    {
        _transfer(
            payable(msg.sender),
            to,
            value
        );
        return true;
    }

    function transferFrom(
        address from, 
        address to, 
        uint value
    ) 
    external 
    virtual 
    override 
    returns (bool) 
    {
        if (allowance[from][msg.sender] != type(uint).max) {
            allowance[from][msg.sender] = allowance[from][msg.sender].sub(value);
        }
        _transfer(
            from, 
            to, 
            value
        );
        return true;
    }

    // Internal functions
    function _burn(
        address from,
        uint value
    )
    internal
    virtual
    {
        balanceOf[from] = balanceOf[from].sub(value);
        totalSupply = totalSupply.sub(value);
        emit Transfer(from, address(0), value);
    }

    function _mint(
        address to, 
        uint value
    )
    internal 
    virtual 
    {
        totalSupply = totalSupply.add(value);
        balanceOf[to] = balanceOf[to].add(value);
        emit Transfer(address(0), to, value);
    }

    // Private functions
    function _approve(
        address owner, 
        address spender, 
        uint value
    ) 
    private  
    {
        allowance[owner][spender] = value;
        emit Approval(owner, spender, value);
    }
    
    function _transfer(
        address from, 
        address to, 
        uint value
    ) 
    private 
    {
        balanceOf[from] = balanceOf[from].sub(value);
        balanceOf[to] = balanceOf[to].add(value);
        emit Transfer(from, to, value);
    }
}
"},"CoinvestingDeFiFactory.sol":{"content":"// SPDX-License-Identifier: GPL-3.0
pragma solidity \u003e=0.8.0 \u003c0.9.0;

import \"./ICoinvestingDeFiFactory.sol\";
import \"./CoinvestingDeFiPair.sol\";

contract CoinvestingDeFiFactory is ICoinvestingDeFiFactory {
    // Public variables
    address[] public override allPairs;
    address public override feeTo;
    address public override feeToSetter;
    
    mapping(address =\u003e mapping(address =\u003e address)) public override getPair;

    constructor(address _feeToSetter) {
        feeToSetter = _feeToSetter;
    }

    //External functions
    function createPair(
        address tokenA,
        address tokenB
    )
    external
    override
    returns (address pair)
    {
        require(tokenA != tokenB, \"FAC: IDT_ADDR\");
        (address token0, address token1) = tokenA \u003c tokenB ? (tokenA, tokenB) : (tokenB, tokenA);
        require(token0 != address(0), \"FAC: ZERO_ADDR\");
        require(getPair[token0][token1] == address(0), \"FAC: PAIR_EXISTS\");
        bytes memory bytecode = type(CoinvestingDeFiPair).creationCode;
        bytes32 salt = keccak256(abi.encodePacked(token0, token1));
        assembly {
            pair := create2(0, add(bytecode, 32), mload(bytecode), salt)
        }
        ICoinvestingDeFiPair(pair).initialize(token0, token1);
        getPair[token0][token1] = pair;
        getPair[token1][token0] = pair;
        allPairs.push(pair);
        emit PairCreated(token0, token1, pair, allPairs.length);
    }

    function setFeeTo(address _feeTo) external override {
        require(msg.sender == feeToSetter,
            \"FAC: CALLER_AINT_SETTER\");
        feeTo = _feeTo;
    }

    function setFeeToSetter(address _feeToSetter) external override {
        require(msg.sender == feeToSetter,
            \"FAC: CALLER_AINT_SETTER\");
        feeToSetter = _feeToSetter;
    }

    //External functions that are view
    function allPairsLength() external view override returns (uint) {
        return allPairs.length;
    }
}
"},"CoinvestingDeFiPair.sol":{"content":"// SPDX-License-Identifier: GPL-3.0
pragma solidity \u003e=0.8.0 \u003c0.9.0;

import \"./ICoinvestingDeFiCallee.sol\";
import \"./ICoinvestingDeFiFactory.sol\";
import \"./ICoinvestingDeFiPair.sol\";
import \"./IERC20.sol\";
import \"./Math.sol\";
import \"./UQ112x112.sol\";
import \"./CoinvestingDeFiERC20.sol\";

contract CoinvestingDeFiPair is ICoinvestingDeFiPair, CoinvestingDeFiERC20 {
    using SafeMath  for uint;
    using UQ112x112 for uint224;

    // Public variables
    address public override factory;
    uint public override kLast;
    uint public override price0CumulativeLast;
    uint public override price1CumulativeLast;
    address public override token0;
    address public override token1;

    uint public constant override MINIMUM_LIQUIDITY = 10**3;
        
    // Private variables
    uint32  private blockTimestampLast;
    uint112 private reserve0;
    uint112 private reserve1;    
    uint private unlocked = 1;

    bytes4 private constant SELECTOR = bytes4(keccak256(bytes(\u0027transfer(address,uint256)\u0027)));

    // Modifiers
    modifier lock() {
        require(unlocked == 1, \"PAIR: LOCKED\");
        unlocked = 0;
        _;
        unlocked = 1;
    }

    constructor() {
        factory = payable(msg.sender);
    }

    // External functions
    // this low-level function should be called from a contract which performs important safety checks
    function burn(address to) external override lock returns (
        uint amount0,
        uint amount1
    ) 
    {
        (uint112 _reserve0, uint112 _reserve1,) = getReserves();
        address _token0 = token0;
        address _token1 = token1;
        uint balance0 = IERC20(_token0).balanceOf(address(this));
        uint balance1 = IERC20(_token1).balanceOf(address(this));
        uint liquidity = balanceOf[address(this)];

        bool feeOn = _mintFee(_reserve0, _reserve1);
        uint _totalSupply = totalSupply;
        amount0 = liquidity.mul(balance0) / _totalSupply;
        amount1 = liquidity.mul(balance1) / _totalSupply;
        require(
            amount0 \u003e 0 \u0026\u0026 amount1 \u003e 0, 
            \"PAIR: INSUF_LIQ_BURN\"
        );

        _burn(address(this), liquidity);
        _safeTransfer(_token0, to, amount0);
        _safeTransfer(_token1, to, amount1);
        balance0 = IERC20(_token0).balanceOf(address(this));
        balance1 = IERC20(_token1).balanceOf(address(this));

        _update(balance0, balance1, _reserve0, _reserve1);
        if (feeOn) kLast = uint(reserve0).mul(reserve1); 
        emit Burn(msg.sender, amount0, amount1, to);
    }
    
    // called once by the factory at time of deployment
    function initialize(
        address _token0,
        address _token1
    )
    external
    override
    {
        require(msg.sender == factory, \"PAIR: CALLER_MUST_BE_FAC\");
        token0 = _token0;
        token1 = _token1;
    }
    // this low-level function should be called from a contract which performs important safety checks
    function mint(address to) external override lock returns (uint liquidity) {
        (uint112 _reserve0, uint112 _reserve1,) = getReserves();
        uint balance0 = IERC20(token0).balanceOf(address(this));
        uint balance1 = IERC20(token1).balanceOf(address(this));
        uint amount0 = balance0.sub(_reserve0);
        uint amount1 = balance1.sub(_reserve1);

        bool feeOn = _mintFee(_reserve0, _reserve1);
        uint _totalSupply = totalSupply; 
        if (_totalSupply == 0) {
            liquidity = Math.sqrt(amount0.mul(amount1)).sub(MINIMUM_LIQUIDITY);
           _mint(address(0), MINIMUM_LIQUIDITY);
        } else
            liquidity = Math.min(
                amount0.mul(_totalSupply) / _reserve0,
                amount1.mul(_totalSupply) / _reserve1
            );
        require(liquidity \u003e 0, \"PAIR: INSUF_LIQ_MINT\");
        _mint(to, liquidity);

        _update(balance0, balance1, _reserve0, _reserve1);
        if (feeOn) kLast = uint(reserve0).mul(reserve1);
        emit Mint(msg.sender, amount0, amount1);
    }

    // force balances to match reserves
    function skim(address to) external override lock {
        address _token0 = token0; 
        address _token1 = token1; 
        _safeTransfer(
            _token0,
            to,
            IERC20(_token0).balanceOf(address(this)).sub(reserve0)
        );
        _safeTransfer(
            _token1,
            to,
            IERC20(_token1).balanceOf(address(this)).sub(reserve1)
        );
    }

    // this low-level function should be called from a contract which performs important safety checks
    function swap(
        uint amount0Out,
        uint amount1Out,
        address to,
        bytes calldata data
    )
    external
    override
    lock
    {
        require(
            amount0Out \u003e 0 || amount1Out \u003e 0, 
            \"PAIR: INSUF_OUT_AMT\"
        );

        (uint112 _reserve0, uint112 _reserve1,) = getReserves();
        require(
            amount0Out \u003c _reserve0 \u0026\u0026 amount1Out \u003c _reserve1, 
            \"PAIR: INSUF_LIQ\"
        );
        
        uint balance0;
        uint balance1;
        {
            address _token0 = token0;
            address _token1 = token1;
            require(
                to != _token0 \u0026\u0026 to != _token1, 
                \"PAIR: INV_TO\"
            );

            if (amount0Out \u003e 0) _safeTransfer(_token0, to, amount0Out);
            if (amount1Out \u003e 0) _safeTransfer(_token1, to, amount1Out);
            if (data.length \u003e 0) 
                ICoinvestingDeFiCallee(to).coinvestingDeFiCall(
                    payable(msg.sender),
                    amount0Out,
                    amount1Out,
                    data
                );
            balance0 = IERC20(_token0).balanceOf(address(this));
            balance1 = IERC20(_token1).balanceOf(address(this));
        } 

        uint amount0In = balance0 \u003e _reserve0 - amount0Out ? balance0 - (_reserve0 - amount0Out) : 0;
        uint amount1In = balance1 \u003e _reserve1 - amount1Out ? balance1 - (_reserve1 - amount1Out) : 0;
        require(amount0In \u003e 0 || amount1In \u003e 0, \"PAIR: INSUF_IN_AMT\");
        {
            uint balance0Adjusted = balance0.mul(1000).sub(amount0In.mul(3));
            uint balance1Adjusted = balance1.mul(1000).sub(amount1In.mul(3));
            require(
                balance0Adjusted.mul(balance1Adjusted) \u003e= uint(_reserve0).mul(_reserve1).mul(1000**2),
                \"Pair: K\"
            );
        }

        _update(balance0, balance1, _reserve0, _reserve1);
        emit Swap(msg.sender, amount0In, amount1In, amount0Out, amount1Out, to);
    }

    // force reserves to match balances
    function sync() external override lock {
        _update(
            IERC20(token0).balanceOf(address(this)), 
            IERC20(token1).balanceOf(address(this)), 
            reserve0, 
            reserve1
        );
    }

    // External functions that are view
    function getReserves() 
        public
        override 
        view 
        returns (
            uint112 _reserve0, 
            uint112 _reserve1, 
            uint32 _blockTimestampLast
        ) 
    {
        _reserve0 = reserve0;
        _reserve1 = reserve1;
        _blockTimestampLast = blockTimestampLast;
    }

    // Private functions
    // if fee is on, mint liquidity equivalent to 1/6th of the growth in sqrt(k)
    function _mintFee(
        uint112 _reserve0,
        uint112 _reserve1
    )
    private
    returns (bool feeOn)
    {
        address feeTo = ICoinvestingDeFiFactory(factory).feeTo();
        feeOn = feeTo != address(0);
        uint _kLast = kLast; // gas savings
        if (feeOn) {
            if (_kLast != 0) {
                uint rootK = Math.sqrt(uint(_reserve0).mul(_reserve1));
                uint rootKLast = Math.sqrt(_kLast);
                if (rootK \u003e rootKLast) {
                    uint numerator = totalSupply.mul(rootK.sub(rootKLast));
                    uint denominator = rootK.mul(5).add(rootKLast);
                    uint liquidity = numerator / denominator;
                    if (liquidity \u003e 0) _mint(feeTo, liquidity);
                }
            }
        } else if (_kLast != 0) {
           kLast = 0; 
        } 
    }

    function _safeTransfer(
        address token,
        address to,
        uint value
    ) 
    private 
    {
        (bool success, bytes memory data) = 
            token.call(abi.encodeWithSelector(SELECTOR, to, value));
        require(
            success \u0026\u0026 (data.length == 0 || abi.decode(data, (bool))),
            \"PAIR: TXFR_FL\"
        );
    }

    // update reserves and, on the first call per block, price accumulators
    function _update(
        uint balance0,
        uint balance1,
        uint112 _reserve0,
        uint112 _reserve1
    ) 
    private
    {
        require(balance0 \u003c= type(uint112).max \u0026\u0026 balance1 \u003c= type(uint).max, \"PAIR: OVF\");
        uint32 blockTimestamp = uint32(block.timestamp % 2**32);
        uint32 timeElapsed = blockTimestamp - blockTimestampLast; // overflow is desired
        if (timeElapsed \u003e 0 \u0026\u0026 _reserve0 != 0 \u0026\u0026 _reserve1 != 0) {
            // * never overflows, and + overflow is desired
            price0CumulativeLast += uint(UQ112x112.encode(_reserve1).uqdiv(_reserve0)) * timeElapsed;
            price1CumulativeLast += uint(UQ112x112.encode(_reserve0).uqdiv(_reserve1)) * timeElapsed;
        }
        reserve0 = uint112(balance0);
        reserve1 = uint112(balance1);
        blockTimestampLast = blockTimestamp;
        emit Sync(reserve0, reserve1);
    }
}
"},"ICoinvestingDeFiCallee.sol":{"content":"// SPDX-License-Identifier: GPL-3.0
pragma solidity \u003e=0.8.0 \u003c0.9.0;

interface ICoinvestingDeFiCallee {
    // External functions
    function coinvestingDeFiCall(
        address sender, 
        uint amount0,
        uint amount1,
        bytes calldata data
    )
    external;
}
"},"ICoinvestingDeFiERC20.sol":{"content":"// SPDX-License-Identifier: GPL-3.0
pragma solidity \u003e=0.8.0 \u003c0.9.0;

interface ICoinvestingDeFiERC20 {
    // Events
    event Approval(
        address indexed owner,
        address indexed spender,
        uint value
    );

    event Transfer(
        address indexed from,
        address indexed to,
        uint value
    );

    // External functions
    function approve(
        address spender,
        uint value
    )
        external 
        returns (bool);

    function permit(
        address owner,
        address spender,
        uint value,
        uint deadline,
        uint8 v,
        bytes32 r,
        bytes32 s
    )
        external;
    
    function transfer(
        address to,
        uint value
    )
        external
        returns (bool);

    function transferFrom(
        address from,
        address to,
        uint value
    )
        external
        returns (bool);
    
    // External functions that are view        
    function allowance(
        address owner,
        address spender
    )
        external 
        view 
        returns (uint);

    function balanceOf(address owner) external view returns (uint);
    function DOMAIN_SEPARATOR() external view returns (bytes32);
    function nonces(address owner) external view returns (uint);
    function totalSupply() external view returns (uint);

    // External functions that are pure
    function decimals() external pure returns (uint8);
    function name() external pure returns (string memory);
    function PERMIT_TYPEHASH() external pure returns (bytes32);
    function symbol() external pure returns (string memory);
}
"},"ICoinvestingDeFiFactory.sol":{"content":"// SPDX-License-Identifier: GPL-3.0
pragma solidity \u003e=0.8.0 \u003c0.9.0;

interface ICoinvestingDeFiFactory {
    //Events
    event PairCreated(
        address indexed token0,
        address indexed token1,
        address pair,
        uint
    );

    //External functions
    function createPair(
        address tokenA,
        address tokenB
    )
    external
    returns (address pair);

    function setFeeTo(address) external;
    function setFeeToSetter(address) external;

    // External functions that are view
    function allPairs(uint) external view returns (address pair);
    function allPairsLength() external view returns (uint);
    function feeTo() external view returns (address);
    function feeToSetter() external view returns (address);
    function getPair(
        address tokenA,
        address tokenB
    )
    external
    view
    returns (address pair);
}
"},"ICoinvestingDeFiPair.sol":{"content":"// SPDX-License-Identifier: GPL-3.0
pragma solidity \u003e=0.8.0 \u003c0.9.0;

import \"./ICoinvestingDeFiERC20.sol\";

interface ICoinvestingDeFiPair is ICoinvestingDeFiERC20 {
    // Events
    event Burn(
        address indexed sender,
        uint amount0,
        uint amount1,
        address indexed to
    );

    event Mint(
        address indexed sender,
        uint amount0,
        uint amount1
    );

    event Swap(
        address indexed sender,
        uint amount0In,
        uint amount1In,
        uint amount0Out,
        uint amount1Out,
        address indexed to
    );

    event Sync(
        uint112 reserve0, 
        uint112 reserve1
    );

    // External functions
    function burn(address to) external returns (
        uint amount0,
        uint amount1
    );

    function initialize(
        address,
        address
    ) external;

    function mint(address to) external returns (uint liquidity);
    function skim(address to) external;
    function swap(
        uint amount0Out,
        uint amount1Out,
        address to,
        bytes calldata data
    ) external;

    function sync() external;

    // External functions that are view
    function factory() external view returns (address);
    function getReserves() external view returns (
        uint112 reserve0,
        uint112 reserve1,
        uint32 blockTimestampLast
    );

    function kLast() external view returns (uint);    
    function price0CumulativeLast() external view returns (uint);
    function price1CumulativeLast() external view returns (uint);
    function token0() external view returns (address);
    function token1() external view returns (address);
    

    // External functions that are pure
    function MINIMUM_LIQUIDITY() external pure returns (uint);
}
"},"IERC20.sol":{"content":"// SPDX-License-Identifier: GPL-3.0
pragma solidity \u003e=0.8.0 \u003c0.9.0;

interface IERC20 {
    //Events
    event Approval(
        address indexed owner,
        address indexed spender,
        uint value
    );

    event Transfer(
        address indexed from,
        address indexed to,
        uint value
    );

    //External functions
    function approve(
        address spender,
        uint value
    )
        external 
        returns (bool);
    
    function transfer(
        address to,
        uint value
    )
        external
        returns (bool);

    function transferFrom(
        address from,
        address to,
        uint value
    )
        external
        returns (bool);
    
    // External functions that are view        
    function allowance(
        address owner,
        address spender
    )
        external 
        view 
        returns (uint);

    function balanceOf(address owner) external view returns (uint);
    function decimals() external view returns (uint8);
    function name() external view returns (string memory);
    function symbol() external view returns (string memory);
    function totalSupply() external view returns (uint);
}
"},"Math.sol":{"content":"// SPDX-License-Identifier: GPL-3.0
pragma solidity \u003e=0.8.0 \u003c0.9.0;

library Math {
    
    /// @dev Returns the smallest of two numbers.
    function min(
        uint x, 
        uint y
    ) 
    internal 
    pure 
    returns (uint z) 
    {
        z = x \u003c y ? x : y;
    }

    /// @dev babylonian method 
    ///(https://en.wikipedia.org/wiki/Methods_of_computing_square_roots#Babylonian_method)
    function sqrt(uint y) internal pure returns (uint z) {
        if (y \u003e 3) {
            z = y;
            uint x = y / 2 + 1;
            while (x \u003c z) {
                z = x;
                x = (y / x + x) / 2;
            }
        } else if (y != 0) {
            z = 1;
        }
    }
}
"},"SafeMath.sol":{"content":"// SPDX-License-Identifier: GPL-3.0
pragma solidity \u003e=0.8.0 \u003c0.9.0;

// CAUTION
// This version of SafeMath should only be used with Solidity 0.8 or later,
// because it relies on the compiler\u0027s built in overflow checks.

/**
 * @dev Wrappers over Solidity\u0027s arithmetic operations.
 *
 * NOTE: `SafeMath` is no longer needed starting with Solidity 0.8. The compiler
 * now has built in overflow checking.
 */
library SafeMath {
    /**
     * @dev Returns the addition of two unsigned integers, with an overflow flag.
     *
     * _Available since v3.4._
     */
    function tryAdd(uint256 a, uint256 b) internal pure returns (bool, uint256) {
        unchecked {
            uint256 c = a + b;
            if (c \u003c a) return (false, 0);
            return (true, c);
        }
    }

    /**
     * @dev Returns the substraction of two unsigned integers, with an overflow flag.
     *
     * _Available since v3.4._
     */
    function trySub(uint256 a, uint256 b) internal pure returns (bool, uint256) {
        unchecked {
            if (b \u003e a) return (false, 0);
            return (true, a - b);
        }
    }

    /**
     * @dev Returns the multiplication of two unsigned integers, with an overflow flag.
     *
     * _Available since v3.4._
     */
    function tryMul(uint256 a, uint256 b) internal pure returns (bool, uint256) {
        unchecked {
            // Gas optimization: this is cheaper than requiring \u0027a\u0027 not being zero, but the
            // benefit is lost if \u0027b\u0027 is also tested.
            // See: https://github.com/OpenZeppelin/openzeppelin-contracts/pull/522
            if (a == 0) return (true, 0);
            uint256 c = a * b;
            if (c / a != b) return (false, 0);
            return (true, c);
        }
    }

    /**
     * @dev Returns the division of two unsigned integers, with a division by zero flag.
     *
     * _Available since v3.4._
     */
    function tryDiv(uint256 a, uint256 b) internal pure returns (bool, uint256) {
        unchecked {
            if (b == 0) return (false, 0);
            return (true, a / b);
        }
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers, with a division by zero flag.
     *
     * _Available since v3.4._
     */
    function tryMod(uint256 a, uint256 b) internal pure returns (bool, uint256) {
        unchecked {
            if (b == 0) return (false, 0);
            return (true, a % b);
        }
    }

    /**
     * @dev Returns the addition of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity\u0027s `+` operator.
     *
     * Requirements:
     *
     * - Addition cannot overflow.
     */
    function add(uint256 a, uint256 b) internal pure returns (uint256) {
        return a + b;
    }

    /**
     * @dev Returns the subtraction of two unsigned integers, reverting on
     * overflow (when the result is negative).
     *
     * Counterpart to Solidity\u0027s `-` operator.
     *
     * Requirements:
     *
     * - Subtraction cannot overflow.
     */
    function sub(uint256 a, uint256 b) internal pure returns (uint256) {
        return a - b;
    }

    /**
     * @dev Returns the multiplication of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity\u0027s `*` operator.
     *
     * Requirements:
     *
     * - Multiplication cannot overflow.
     */
    function mul(uint256 a, uint256 b) internal pure returns (uint256) {
        return a * b;
    }

    /**
     * @dev Returns the integer division of two unsigned integers, reverting on
     * division by zero. The result is rounded towards zero.
     *
     * Counterpart to Solidity\u0027s `/` operator.
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function div(uint256 a, uint256 b) internal pure returns (uint256) {
        return a / b;
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
     * reverting when dividing by zero.
     *
     * Counterpart to Solidity\u0027s `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function mod(uint256 a, uint256 b) internal pure returns (uint256) {
        return a % b;
    }

    /**
     * @dev Returns the subtraction of two unsigned integers, reverting with custom message on
     * overflow (when the result is negative).
     *
     * CAUTION: This function is deprecated because it requires allocating memory for the error
     * message unnecessarily. For custom revert reasons use {trySub}.
     *
     * Counterpart to Solidity\u0027s `-` operator.
     *
     * Requirements:
     *
     * - Subtraction cannot overflow.
     */
    function sub(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        unchecked {
            require(b \u003c= a, errorMessage);
            return a - b;
        }
    }

    /**
     * @dev Returns the integer division of two unsigned integers, reverting with custom message on
     * division by zero. The result is rounded towards zero.
     *
     * Counterpart to Solidity\u0027s `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     *
     * Counterpart to Solidity\u0027s `/` operator. Note: this function uses a
     * `revert` opcode (which leaves remaining gas untouched) while Solidity
     * uses an invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function div(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        unchecked {
            require(b \u003e 0, errorMessage);
            return a / b;
        }
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
     * reverting with custom message when dividing by zero.
     *
     * CAUTION: This function is deprecated because it requires allocating memory for the error
     * message unnecessarily. For custom revert reasons use {tryMod}.
     *
     * Counterpart to Solidity\u0027s `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function mod(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        unchecked {
            require(b \u003e 0, errorMessage);
            return a % b;
        }
    }
}
"},"UQ112x112.sol":{"content":"// SPDX-License-Identifier: GPL-3.0
pragma solidity \u003e=0.8.0 \u003c0.9.0;

/// @dev Library for handling binary fixed point numbers
/// (https://en.wikipedia.org/wiki/Q_(number_format))
library UQ112x112 {
    uint224 constant Q112 = 2**112;

    /// @dev encode a uint112 as a UQ112x112
    function encode(uint112 y) internal pure returns (uint224 z) {
        z = uint224(y) * Q112;
    }

    /// @dev // divide a UQ112x112 by a uint112, returning a UQ112x112
    function uqdiv(
        uint224 x,
        uint112 y
    )
    internal
    pure
    returns (uint224 z) 
    {
        z = x / uint224(y);
    }
}

