// SPDX-License-Identifier: MIT

pragma solidity ^0.6.0;

/**
 * @dev Interface of the ERC20 standard as defined in the EIP.
 */
interface IERC20 {
    /**
     * @dev Returns the amount of tokens in existence.
     */
    function totalSupply() external view returns (uint256);

    /**
     * @dev Returns the amount of tokens owned by `account`.
     */
    function balanceOf(address account) external view returns (uint256);

    /**
     * @dev Moves `amount` tokens from the caller\u0027s account to `recipient`.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transfer(address recipient, uint256 amount) external returns (bool);

    /**
     * @dev Returns the remaining number of tokens that `spender` will be
     * allowed to spend on behalf of `owner` through {transferFrom}. This is
     * zero by default.
     *
     * This value changes when {approve} or {transferFrom} are called.
     */
    function allowance(address owner, address spender) external view returns (uint256);

    /**
     * @dev Sets `amount` as the allowance of `spender` over the caller\u0027s tokens.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * IMPORTANT: Beware that changing an allowance with this method brings the risk
     * that someone may use both the old and the new allowance by unfortunate
     * transaction ordering. One possible solution to mitigate this race
     * condition is to first reduce the spender\u0027s allowance to 0 and set the
     * desired value afterwards:
     * https://github.com/ethereum/EIPs/issues/20#issuecomment-263524729
     *
     * Emits an {Approval} event.
     */
    function approve(address spender, uint256 amount) external returns (bool);

    /**
     * @dev Moves `amount` tokens from `sender` to `recipient` using the
     * allowance mechanism. `amount` is then deducted from the caller\u0027s
     * allowance.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transferFrom(address sender, address recipient, uint256 amount) external returns (bool);

    /**
     * @dev Emitted when `value` tokens are moved from one account (`from`) to
     * another (`to`).
     *
     * Note that `value` may be zero.
     */
    event Transfer(address indexed from, address indexed to, uint256 value);

    /**
     * @dev Emitted when the allowance of a `spender` for an `owner` is set by
     * a call to {approve}. `value` is the new allowance.
     */
    event Approval(address indexed owner, address indexed spender, uint256 value);
}
"},"SafeMath.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.6.0;

/**
 * @dev Wrappers over Solidity\u0027s arithmetic operations with added overflow
 * checks.
 *
 * Arithmetic operations in Solidity wrap on overflow. This can easily result
 * in bugs, because programmers usually assume that an overflow raises an
 * error, which is the standard behavior in high level programming languages.
 * `SafeMath` restores this intuition by reverting the transaction when an
 * operation overflows.
 *
 * Using this library instead of the unchecked operations eliminates an entire
 * class of bugs, so it\u0027s recommended to use it always.
 */
library SafeMath {
    /**
     * @dev Returns the addition of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity\u0027s `+` operator.
     *
     * Requirements:
     *
     * - Addition cannot overflow.
     */
    function add(uint256 a, uint256 b) internal pure returns (uint256) {
        uint256 c = a + b;
        require(c \u003e= a, \"SafeMath: addition overflow\");

        return c;
    }

    /**
     * @dev Returns the subtraction of two unsigned integers, reverting on
     * overflow (when the result is negative).
     *
     * Counterpart to Solidity\u0027s `-` operator.
     *
     * Requirements:
     *
     * - Subtraction cannot overflow.
     */
    function sub(uint256 a, uint256 b) internal pure returns (uint256) {
        return sub(a, b, \"SafeMath: subtraction overflow\");
    }

    /**
     * @dev Returns the subtraction of two unsigned integers, reverting with custom message on
     * overflow (when the result is negative).
     *
     * Counterpart to Solidity\u0027s `-` operator.
     *
     * Requirements:
     *
     * - Subtraction cannot overflow.
     */
    function sub(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b \u003c= a, errorMessage);
        uint256 c = a - b;

        return c;
    }

    /**
     * @dev Returns the multiplication of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity\u0027s `*` operator.
     *
     * Requirements:
     *
     * - Multiplication cannot overflow.
     */
    function mul(uint256 a, uint256 b) internal pure returns (uint256) {
        // Gas optimization: this is cheaper than requiring \u0027a\u0027 not being zero, but the
        // benefit is lost if \u0027b\u0027 is also tested.
        // See: https://github.com/OpenZeppelin/openzeppelin-contracts/pull/522
        if (a == 0) {
            return 0;
        }

        uint256 c = a * b;
        require(c / a == b, \"SafeMath: multiplication overflow\");

        return c;
    }

    /**
     * @dev Returns the integer division of two unsigned integers. Reverts on
     * division by zero. The result is rounded towards zero.
     *
     * Counterpart to Solidity\u0027s `/` operator. Note: this function uses a
     * `revert` opcode (which leaves remaining gas untouched) while Solidity
     * uses an invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function div(uint256 a, uint256 b) internal pure returns (uint256) {
        return div(a, b, \"SafeMath: division by zero\");
    }

    /**
     * @dev Returns the integer division of two unsigned integers. Reverts with custom message on
     * division by zero. The result is rounded towards zero.
     *
     * Counterpart to Solidity\u0027s `/` operator. Note: this function uses a
     * `revert` opcode (which leaves remaining gas untouched) while Solidity
     * uses an invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function div(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b \u003e 0, errorMessage);
        uint256 c = a / b;
        // assert(a == b * c + a % b); // There is no case in which this doesn\u0027t hold

        return c;
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
     * Reverts when dividing by zero.
     *
     * Counterpart to Solidity\u0027s `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function mod(uint256 a, uint256 b) internal pure returns (uint256) {
        return mod(a, b, \"SafeMath: modulo by zero\");
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
     * Reverts with custom message when dividing by zero.
     *
     * Counterpart to Solidity\u0027s `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function mod(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b != 0, errorMessage);
        return a % b;
    }
}
"},"sbControllerInterface.sol":{"content":"// SPDX-License-Identifier: MIT
pragma solidity ^0.6.12;

interface sbControllerInterface {
  function requestRewards(address miner, uint256 amount) external;

  function isValuePoolAccepted(address valuePool) external view returns (bool);

  function getValuePoolRewards(address valuePool, uint256 day) external view returns (uint256);

  function getValuePoolMiningFee(address valuePool) external returns (uint256, uint256);

  function getValuePoolUnminingFee(address valuePool) external returns (uint256, uint256);

  function getValuePoolClaimingFee(address valuePool) external returns (uint256, uint256);

  function isServicePoolAccepted(address servicePool) external view returns (bool);

  function getServicePoolRewards(address servicePool, uint256 day) external view returns (uint256);

  function getServicePoolClaimingFee(address servicePool) external returns (uint256, uint256);

  function getServicePoolRequestFeeInWei(address servicePool) external returns (uint256);

  function getVoteForServicePoolsCount() external view returns (uint256);

  function getVoteForServicesCount() external view returns (uint256);

  function getVoteCastersRewards(uint256 dayNumber) external view returns (uint256);

  function getVoteReceiversRewards(uint256 dayNumber) external view returns (uint256);

  function getMinerMinMineDays() external view returns (uint256);

  function getServiceMinMineDays() external view returns (uint256);

  function getMinerMinMineAmountInWei() external view returns (uint256);

  function getServiceMinMineAmountInWei() external view returns (uint256);

  function getValuePoolVestingDays(address valuePool) external view returns (uint256);

  function getServicePoolVestingDays(address poservicePoolol) external view returns (uint256);

  function getVoteCasterVestingDays() external view returns (uint256);

  function getVoteReceiverVestingDays() external view returns (uint256);
}
"},"sbEthFeePool.sol":{"content":"// SPDX-License-Identifier: MIT
pragma solidity ^0.6.12;

import \"./IERC20.sol\";
import \"./SafeMath.sol\";
import \"./sbControllerInterface.sol\";
import \"./sbStrongValuePoolInterface.sol\";

contract sbEthFeePool {
    using SafeMath for uint256;
    bool public initDone;
    address public admin;
    address public pendingAdmin;
    address payable public superAdmin;
    address payable public pendingSuperAdmin;

    sbControllerInterface public sbController;
    sbStrongValuePoolInterface public sbStrongValuePool;
    IERC20 public strongToken;

    uint256 public superAdminFeeNumerator;
    uint256 public superAdminFeeDenominator;

    uint256 public logsSumFeeAmount;
    mapping(address =\u003e uint256[]) public logsContractFeeDays;
    mapping(address =\u003e uint256[]) public logsContractFeeAmounts;

    address public constant burnAddress = address(
        0x000000000000000000000000000000000000dEaD
    );

    function init(
        address sbControllerAddress,
        address sbStrongValuePoolAddress,
        address strongTokenAddress,
        address adminAddress,
        address payable superAdminAddress
    ) public {
        require(!initDone, \"init done\");
        sbController = sbControllerInterface(sbControllerAddress);
        sbStrongValuePool = sbStrongValuePoolInterface(
            sbStrongValuePoolAddress
        );
        strongToken = IERC20(strongTokenAddress);
        admin = adminAddress;
        superAdmin = superAdminAddress;
        initDone = true;
    }

    function updateSuperAdminFee(uint256 numerator, uint256 denominator)
        public
    {
        require(msg.sender == superAdmin);
        require(denominator != 0, \"invalid value\");
        superAdminFeeNumerator = numerator;
        superAdminFeeDenominator = denominator;
    }

    function deposit() public payable {
        uint256 currentDay = _getCurrentDay();
        uint256 len = logsContractFeeDays[msg.sender].length;
        if (len == 0) {
            logsContractFeeDays[msg.sender].push(currentDay);
            logsContractFeeAmounts[msg.sender].push(msg.value);
        } else {
            uint256 lastIndex = logsContractFeeDays[msg.sender].length.sub(1);
            uint256 lastDay = logsContractFeeDays[msg.sender][lastIndex];
            if (lastDay == currentDay) {
                logsContractFeeAmounts[msg
                    .sender][lastIndex] = logsContractFeeAmounts[msg
                    .sender][lastIndex]
                    .add(msg.value);
            } else {
                logsContractFeeDays[msg.sender].push(currentDay);
                logsContractFeeAmounts[msg.sender].push(msg.value);
            }
        }
        uint256 toSuperAdmin = msg.value.mul(superAdminFeeNumerator).div(
            superAdminFeeDenominator
        );
        logsSumFeeAmount = logsSumFeeAmount.add(msg.value);
        superAdmin.transfer(toSuperAdmin);
    }

    function setPendingAdmin(address newPendingAdmin) public {
        require(msg.sender == admin, \"not admin\");
        pendingAdmin = newPendingAdmin;
    }

    function acceptAdmin() public {
        require(
            msg.sender == pendingAdmin \u0026\u0026 msg.sender != address(0),
            \"not pendingAdmin\"
        );
        admin = pendingAdmin;
        pendingAdmin = address(0);
    }

    function setPendingSuperAdmin(address payable newPendingSuperAdmin) public {
        require(msg.sender == superAdmin, \"not superAdmin\");
        pendingSuperAdmin = newPendingSuperAdmin;
    }

    function acceptSuperAdmin() public {
        require(
            msg.sender == pendingSuperAdmin \u0026\u0026 msg.sender != address(0),
            \"not pendingSuperAdmin\"
        );
        superAdmin = pendingSuperAdmin;
        pendingSuperAdmin = address(0);
    }

    function getContractFeeData(address cntrct, uint256 dayNumber)
        public
        view
        returns (uint256, uint256)
    {
        uint256 day = dayNumber == 0 ? _getCurrentDay() : dayNumber;
        return _getContractFeeData(cntrct, day);
    }

    function _getContractFeeData(address cntrct, uint256 day)
        internal
        view
        returns (uint256, uint256)
    {
        uint256[] memory _Days = logsContractFeeDays[cntrct];
        uint256[] memory _Amounts = logsContractFeeAmounts[cntrct];
        return _get(_Days, _Amounts, day);
    }

    function _get(
        uint256[] memory _Days,
        uint256[] memory _Units,
        uint256 day
    ) internal pure returns (uint256, uint256) {
        uint256 len = _Days.length;
        if (len == 0) {
            return (day, 0);
        }
        if (day \u003c _Days[0]) {
            return (day, 0);
        }
        uint256 lastIndex = len.sub(1);
        uint256 lastDay = _Days[lastIndex];
        if (day == lastDay) {
            return (day, _Units[lastIndex]);
        }
        return _find(_Days, _Units, day);
    }

    function _find(
        uint256[] memory _Days,
        uint256[] memory _Units,
        uint256 day
    ) internal pure returns (uint256, uint256) {
        uint256 left = 0;
        uint256 right = _Days.length.sub(1);
        uint256 middle = right.add(left).div(2);
        while (_Days[middle] != day \u0026\u0026 left \u003c right) {
            if (_Days[middle] \u003e day) {
                right = middle.sub(1);
            } else if (_Days[middle] \u003c day) {
                left = middle.add(1);
            }
            middle = right.add(left).div(2);
        }
        if (_Days[middle] != day) {
            return (day, 0);
        } else {
            return (day, _Units[middle]);
        }
    }

    function _getCurrentDay() internal view returns (uint256) {
        return block.timestamp.div(1 days).add(1);
    }
}
"},"sbStrongValuePoolInterface.sol":{"content":"// SPDX-License-Identifier: MIT
pragma solidity ^0.6.12;

interface sbStrongValuePoolInterface {
  function mineFor(address miner, uint256 amount) external;

  function getMinerMineData(address miner, uint256 day)
    external
    view
    returns (
      uint256,
      uint256,
      uint256
    );

  function getMineData(uint256 day)
    external
    view
    returns (
      uint256,
      uint256,
      uint256
    );

  function serviceMinMined(address miner) external view returns (bool);

  function minerMinMined(address miner) external view returns (bool);
}

