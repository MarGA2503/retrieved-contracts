// SPDX-License-Identifier: MIT

pragma solidity ^0.6.0;

/**
 * @dev Interface of the ERC20 standard as defined in the EIP.
 */
interface IERC20 {
    /**
     * @dev Returns the amount of tokens in existence.
     */
    function totalSupply() external view returns (uint256);

    /**
     * @dev Returns the amount of tokens owned by `account`.
     */
    function balanceOf(address account) external view returns (uint256);

    /**
     * @dev Moves `amount` tokens from the caller\u0027s account to `recipient`.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transfer(address recipient, uint256 amount) external returns (bool);

    /**
     * @dev Returns the remaining number of tokens that `spender` will be
     * allowed to spend on behalf of `owner` through {transferFrom}. This is
     * zero by default.
     *
     * This value changes when {approve} or {transferFrom} are called.
     */
    function allowance(address owner, address spender) external view returns (uint256);

    /**
     * @dev Sets `amount` as the allowance of `spender` over the caller\u0027s tokens.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * IMPORTANT: Beware that changing an allowance with this method brings the risk
     * that someone may use both the old and the new allowance by unfortunate
     * transaction ordering. One possible solution to mitigate this race
     * condition is to first reduce the spender\u0027s allowance to 0 and set the
     * desired value afterwards:
     * https://github.com/ethereum/EIPs/issues/20#issuecomment-263524729
     *
     * Emits an {Approval} event.
     */
    function approve(address spender, uint256 amount) external returns (bool);

    /**
     * @dev Moves `amount` tokens from `sender` to `recipient` using the
     * allowance mechanism. `amount` is then deducted from the caller\u0027s
     * allowance.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transferFrom(address sender, address recipient, uint256 amount) external returns (bool);

    /**
     * @dev Emitted when `value` tokens are moved from one account (`from`) to
     * another (`to`).
     *
     * Note that `value` may be zero.
     */
    event Transfer(address indexed from, address indexed to, uint256 value);

    /**
     * @dev Emitted when the allowance of a `spender` for an `owner` is set by
     * a call to {approve}. `value` is the new allowance.
     */
    event Approval(address indexed owner, address indexed spender, uint256 value);
}
"},"SafeMath.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.6.0;

/**
 * @dev Wrappers over Solidity\u0027s arithmetic operations with added overflow
 * checks.
 *
 * Arithmetic operations in Solidity wrap on overflow. This can easily result
 * in bugs, because programmers usually assume that an overflow raises an
 * error, which is the standard behavior in high level programming languages.
 * `SafeMath` restores this intuition by reverting the transaction when an
 * operation overflows.
 *
 * Using this library instead of the unchecked operations eliminates an entire
 * class of bugs, so it\u0027s recommended to use it always.
 */
library SafeMath {
    /**
     * @dev Returns the addition of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity\u0027s `+` operator.
     *
     * Requirements:
     *
     * - Addition cannot overflow.
     */
    function add(uint256 a, uint256 b) internal pure returns (uint256) {
        uint256 c = a + b;
        require(c \u003e= a, \"SafeMath: addition overflow\");

        return c;
    }

    /**
     * @dev Returns the subtraction of two unsigned integers, reverting on
     * overflow (when the result is negative).
     *
     * Counterpart to Solidity\u0027s `-` operator.
     *
     * Requirements:
     *
     * - Subtraction cannot overflow.
     */
    function sub(uint256 a, uint256 b) internal pure returns (uint256) {
        return sub(a, b, \"SafeMath: subtraction overflow\");
    }

    /**
     * @dev Returns the subtraction of two unsigned integers, reverting with custom message on
     * overflow (when the result is negative).
     *
     * Counterpart to Solidity\u0027s `-` operator.
     *
     * Requirements:
     *
     * - Subtraction cannot overflow.
     */
    function sub(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b \u003c= a, errorMessage);
        uint256 c = a - b;

        return c;
    }

    /**
     * @dev Returns the multiplication of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity\u0027s `*` operator.
     *
     * Requirements:
     *
     * - Multiplication cannot overflow.
     */
    function mul(uint256 a, uint256 b) internal pure returns (uint256) {
        // Gas optimization: this is cheaper than requiring \u0027a\u0027 not being zero, but the
        // benefit is lost if \u0027b\u0027 is also tested.
        // See: https://github.com/OpenZeppelin/openzeppelin-contracts/pull/522
        if (a == 0) {
            return 0;
        }

        uint256 c = a * b;
        require(c / a == b, \"SafeMath: multiplication overflow\");

        return c;
    }

    /**
     * @dev Returns the integer division of two unsigned integers. Reverts on
     * division by zero. The result is rounded towards zero.
     *
     * Counterpart to Solidity\u0027s `/` operator. Note: this function uses a
     * `revert` opcode (which leaves remaining gas untouched) while Solidity
     * uses an invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function div(uint256 a, uint256 b) internal pure returns (uint256) {
        return div(a, b, \"SafeMath: division by zero\");
    }

    /**
     * @dev Returns the integer division of two unsigned integers. Reverts with custom message on
     * division by zero. The result is rounded towards zero.
     *
     * Counterpart to Solidity\u0027s `/` operator. Note: this function uses a
     * `revert` opcode (which leaves remaining gas untouched) while Solidity
     * uses an invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function div(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b \u003e 0, errorMessage);
        uint256 c = a / b;
        // assert(a == b * c + a % b); // There is no case in which this doesn\u0027t hold

        return c;
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
     * Reverts when dividing by zero.
     *
     * Counterpart to Solidity\u0027s `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function mod(uint256 a, uint256 b) internal pure returns (uint256) {
        return mod(a, b, \"SafeMath: modulo by zero\");
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
     * Reverts with custom message when dividing by zero.
     *
     * Counterpart to Solidity\u0027s `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function mod(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b != 0, errorMessage);
        return a % b;
    }
}
"},"sbCommunityInterface.sol":{"content":"// SPDX-License-Identifier: MIT
pragma solidity ^0.6.12;
pragma experimental ABIEncoderV2;

interface sbCommunityInterface {
  function getTokenData(address token, uint256 day)
    external
    view
    returns (
      uint256,
      uint256,
      uint256
    );

  function receiveRewards(uint256 day, uint256 amount) external;

  function serviceAccepted(address service) external view returns (bool);

  function getMinerRewardPercentage() external view returns (uint256);
}
"},"sbControllerV2.sol":{"content":"// SPDX-License-Identifier: MIT
pragma solidity ^0.6.12;
pragma experimental ABIEncoderV2;

import \u0027./SafeMath.sol\u0027;
import \u0027./IERC20.sol\u0027;
import \u0027./sbTokensInterface.sol\u0027;
import \u0027./sbCommunityInterface.sol\u0027;
import \u0027./sbStrongPoolInterface.sol\u0027;
import \u0027./sbVotesInterface.sol\u0027;

contract sbControllerV2 {
  event CommunityAdded(address indexed community);
  event RewardsReleased(address indexed receiver, uint256 amount, uint256 indexed day);

  using SafeMath for uint256;

  bool internal initDone;

  address internal sbTimelock;
  IERC20 internal strongToken;
  sbTokensInterface internal sbTokens;
  sbStrongPoolInterface internal sbStrongPool;
  sbVotesInterface internal sbVotes;
  uint256 internal startDay;

  mapping(uint256 =\u003e uint256) internal COMMUNITY_DAILY_REWARDS_BY_YEAR;
  mapping(uint256 =\u003e uint256) internal STRONGPOOL_DAILY_REWARDS_BY_YEAR;
  mapping(uint256 =\u003e uint256) internal VOTER_DAILY_REWARDS_BY_YEAR;
  uint256 internal MAX_YEARS;

  address[] internal communities;

  mapping(uint256 =\u003e uint256) internal dayMineSecondsUSDTotal;
  mapping(address =\u003e mapping(uint256 =\u003e uint256)) internal communityDayMineSecondsUSD;
  mapping(address =\u003e mapping(uint256 =\u003e uint256)) internal communityDayRewards;
  mapping(address =\u003e uint256) internal communityDayStart;
  uint256 internal dayLastReleasedRewardsFor;

  address internal superAdmin;
  address internal pendingSuperAdmin;

  function setSuperAdmin() public {
    require(superAdmin == address(0), \u0027superAdmin already set\u0027);
    superAdmin = address(0x4B5057B2c87Ec9e7C047fb00c0E406dfF2FDaCad);
  }

  function setPendingSuperAdmin(address newPendingSuperAdmin) public {
    require(msg.sender == superAdmin \u0026\u0026 msg.sender != address(0), \u0027not superAdmin\u0027);
    pendingSuperAdmin = newPendingSuperAdmin;
  }

  function acceptSuperAdmin() public {
    require(msg.sender == pendingSuperAdmin \u0026\u0026 msg.sender != address(0), \u0027not pendingSuperAdmin\u0027);
    superAdmin = pendingSuperAdmin;
    pendingSuperAdmin = address(0);
  }

  function getSuperAdminAddressUsed() public view returns (address) {
    return superAdmin;
  }

  function getPendingSuperAdminAddressUsed() public view returns (address) {
    return pendingSuperAdmin;
  }

  function updateCommunityDailyRewardsByYear(uint256 amount) public {
    require(msg.sender == superAdmin \u0026\u0026 msg.sender != address(0), \u0027not superAdmin\u0027);
    uint256 year = _getYearDayIsIn(_getCurrentDay());
    require(year \u003c= MAX_YEARS, \u0027invalid year\u0027);
    COMMUNITY_DAILY_REWARDS_BY_YEAR[year] = amount;
  }

  function updateStrongPoolDailyRewardsByYear(uint256 amount) public {
    require(msg.sender == superAdmin \u0026\u0026 msg.sender != address(0), \u0027not superAdmin\u0027);
    uint256 year = _getYearDayIsIn(_getCurrentDay());
    require(year \u003c= MAX_YEARS, \u0027invalid year\u0027);
    STRONGPOOL_DAILY_REWARDS_BY_YEAR[year] = amount;
  }

  // TODO: Double check me
  function updateVoterDailyRewardsByYear(uint256 amount) public {
    require(msg.sender == superAdmin \u0026\u0026 msg.sender != address(0), \u0027not superAdmin\u0027);
    uint256 year = _getYearDayIsIn(_getCurrentDay());
    require(year \u003c= MAX_YEARS, \u0027invalid year\u0027);
    VOTER_DAILY_REWARDS_BY_YEAR[year] = amount;
  }

  function upToDate() external view returns (bool) {
    return dayLastReleasedRewardsFor == _getCurrentDay().sub(1);
  }

  function addCommunity(address community) external {
    require(msg.sender == sbTimelock, \u0027not sbTimelock\u0027);
    require(community != address(0), \u0027community not zero address\u0027);
    require(!_communityExists(community), \u0027community exists\u0027);
    communities.push(community);
    communityDayStart[community] = _getCurrentDay();
    emit CommunityAdded(community);
  }

  function getCommunities() external view returns (address[] memory) {
    return communities;
  }

  function getDayMineSecondsUSDTotal(uint256 day) external view returns (uint256) {
    require(day \u003e= startDay, \u00271: invalid day\u0027);
    require(day \u003c= dayLastReleasedRewardsFor, \u00272: invalid day\u0027);
    return dayMineSecondsUSDTotal[day];
  }

  function getCommunityDayMineSecondsUSD(address community, uint256 day) external view returns (uint256) {
    require(_communityExists(community), \u0027invalid community\u0027);
    require(day \u003e= communityDayStart[community], \u00271: invalid day\u0027);
    require(day \u003c= dayLastReleasedRewardsFor, \u00272: invalid day\u0027);
    return communityDayMineSecondsUSD[community][day];
  }

  function getCommunityDayRewards(address community, uint256 day) external view returns (uint256) {
    require(_communityExists(community), \u0027invalid community\u0027);
    require(day \u003e= communityDayStart[community], \u00271: invalid day\u0027);
    require(day \u003c= dayLastReleasedRewardsFor, \u00272: invalid day\u0027);
    return communityDayRewards[community][day];
  }

  function getCommunityDailyRewards(uint256 day) external view returns (uint256) {
    require(day \u003e= startDay, \u0027invalid day\u0027);
    uint256 year = _getYearDayIsIn(day);
    require(year \u003c= MAX_YEARS, \u0027invalid year\u0027);
    return COMMUNITY_DAILY_REWARDS_BY_YEAR[year];
  }

  function getStrongPoolDailyRewards(uint256 day) external view returns (uint256) {
    require(day \u003e= startDay, \u0027invalid day\u0027);
    uint256 year = _getYearDayIsIn(day);
    require(year \u003c= MAX_YEARS, \u0027invalid year\u0027);
    return STRONGPOOL_DAILY_REWARDS_BY_YEAR[year];
  }

  function getVoterDailyRewards(uint256 day) external view returns (uint256) {
    require(day \u003e= startDay, \u0027invalid day\u0027);
    uint256 year = _getYearDayIsIn(day);
    require(year \u003c= MAX_YEARS, \u0027invalid year\u0027);
    return VOTER_DAILY_REWARDS_BY_YEAR[year];
  }

  function getStartDay() external view returns (uint256) {
    return startDay;
  }

  function communityAccepted(address community) external view returns (bool) {
    return _communityExists(community);
  }

  function getMaxYears() public view returns (uint256) {
    return MAX_YEARS;
  }

  function getCommunityDayStart(address community) public view returns (uint256) {
    require(_communityExists(community), \u0027invalid community\u0027);
    return communityDayStart[community];
  }

  function getSbTimelockAddressUsed() public view returns (address) {
    return sbTimelock;
  }

  function getStrongAddressUsed() public view returns (address) {
    return address(strongToken);
  }

  function getSbTokensAddressUsed() public view returns (address) {
    return address(sbTokens);
  }

  function getSbStrongPoolAddressUsed() public view returns (address) {
    return address(sbStrongPool);
  }

  function getSbVotesAddressUsed() public view returns (address) {
    return address(sbVotes);
  }

  function getCurrentYear() public view returns (uint256) {
    uint256 day = _getCurrentDay().sub(startDay);
    return _getYearDayIsIn(day == 0 ? startDay : day);
  }

  function getYearDayIsIn(uint256 day) public view returns (uint256) {
    require(day \u003e= startDay, \u0027invalid day\u0027);
    return _getYearDayIsIn(day);
  }

  function getCurrentDay() public view returns (uint256) {
    return _getCurrentDay();
  }

  function getDayLastReleasedRewardsFor() public view returns (uint256) {
    return dayLastReleasedRewardsFor;
  }

  function releaseRewards() public {
    uint256 currentDay = _getCurrentDay();
    require(currentDay \u003e dayLastReleasedRewardsFor.add(1), \u0027already released\u0027);
    require(sbTokens.upToDate(), \u0027need token prices\u0027);
    dayLastReleasedRewardsFor = dayLastReleasedRewardsFor.add(1);
    uint256 year = _getYearDayIsIn(dayLastReleasedRewardsFor);
    require(year \u003c= MAX_YEARS, \u0027invalid year\u0027);
    address[] memory tokenAddresses = sbTokens.getTokens();
    uint256[] memory tokenPrices = sbTokens.getTokenPrices(dayLastReleasedRewardsFor);
    for (uint256 i = 0; i \u003c communities.length; i++) {
      address community = communities[i];
      uint256 sum = 0;
      for (uint256 j = 0; j \u003c tokenAddresses.length; j++) {
        address token = tokenAddresses[j];
        (, , uint256 minedSeconds) = sbCommunityInterface(community).getTokenData(token, dayLastReleasedRewardsFor);
        uint256 tokenPrice = tokenPrices[j];
        uint256 minedSecondsUSD = tokenPrice.mul(minedSeconds).div(1e18);
        sum = sum.add(minedSecondsUSD);
      }
      communityDayMineSecondsUSD[community][dayLastReleasedRewardsFor] = sum;
      dayMineSecondsUSDTotal[dayLastReleasedRewardsFor] = dayMineSecondsUSDTotal[dayLastReleasedRewardsFor].add(sum);
    }
    for (uint256 i = 0; i \u003c communities.length; i++) {
      address community = communities[i];
      if (communityDayMineSecondsUSD[community][dayLastReleasedRewardsFor] == 0) {
        continue;
      }
      communityDayRewards[community][dayLastReleasedRewardsFor] = communityDayMineSecondsUSD[community][dayLastReleasedRewardsFor]
        .mul(COMMUNITY_DAILY_REWARDS_BY_YEAR[year])
        .div(dayMineSecondsUSDTotal[dayLastReleasedRewardsFor]);

      uint256 amount = communityDayRewards[community][dayLastReleasedRewardsFor];
      strongToken.approve(community, amount);
      sbCommunityInterface(community).receiveRewards(dayLastReleasedRewardsFor, amount);
      emit RewardsReleased(community, amount, currentDay);
    }
    (, , uint256 strongPoolMineSeconds) = sbStrongPool.getMineData(dayLastReleasedRewardsFor);
    if (strongPoolMineSeconds != 0) {
      strongToken.approve(address(sbStrongPool), STRONGPOOL_DAILY_REWARDS_BY_YEAR[year]);
      sbStrongPool.receiveRewards(dayLastReleasedRewardsFor, STRONGPOOL_DAILY_REWARDS_BY_YEAR[year]);
      emit RewardsReleased(address(sbStrongPool), STRONGPOOL_DAILY_REWARDS_BY_YEAR[year], currentDay);
    }
    bool hasVoteSeconds = false;
    for (uint256 i = 0; i \u003c communities.length; i++) {
      address community = communities[i];
      (, , uint256 voteSeconds) = sbVotes.getCommunityData(community, dayLastReleasedRewardsFor);
      if (voteSeconds \u003e 0) {
        hasVoteSeconds = true;
        break;
      }
    }
    if (hasVoteSeconds) {
      strongToken.approve(address(sbVotes), VOTER_DAILY_REWARDS_BY_YEAR[year]);
      sbVotes.receiveVoterRewards(dayLastReleasedRewardsFor, VOTER_DAILY_REWARDS_BY_YEAR[year]);
      emit RewardsReleased(address(sbVotes), VOTER_DAILY_REWARDS_BY_YEAR[year], currentDay);
    }
  }

  function _getCurrentDay() internal view returns (uint256) {
    return block.timestamp.div(1 days).add(1);
  }

  function _communityExists(address community) internal view returns (bool) {
    for (uint256 i = 0; i \u003c communities.length; i++) {
      if (communities[i] == community) {
        return true;
      }
    }
    return false;
  }

  function _getYearDayIsIn(uint256 day) internal view returns (uint256) {
    return day.sub(startDay).div(366).add(1); // dividing by 366 makes day 1 and 365 be in year 1
  }
}
"},"sbStrongPoolInterface.sol":{"content":"// SPDX-License-Identifier: MIT
pragma solidity ^0.6.12;
pragma experimental ABIEncoderV2;

interface sbStrongPoolInterface {
  function serviceMinMined(address miner) external view returns (bool);

  function minerMinMined(address miner) external view returns (bool);

  function mineFor(address miner, uint256 amount) external;

  function getMineData(uint256 day)
    external
    view
    returns (
      uint256,
      uint256,
      uint256
    );

  function receiveRewards(uint256 day, uint256 amount) external;
}
"},"sbTokensInterface.sol":{"content":"// SPDX-License-Identifier: MIT
pragma solidity ^0.6.12;
pragma experimental ABIEncoderV2;

interface sbTokensInterface {
  function getTokens() external view returns (address[] memory);

  function getTokenPrices(uint256 day) external view returns (uint256[] memory);

  function tokenAccepted(address token) external view returns (bool);

  function upToDate() external view returns (bool);

  function getTokenPrice(address token, uint256 day) external view returns (uint256);
}
"},"sbVotesInterface.sol":{"content":"// SPDX-License-Identifier: MIT
pragma solidity ^0.6.12;
pragma experimental ABIEncoderV2;

interface sbVotesInterface {
  function getCommunityData(address community, uint256 day)
    external
    view
    returns (
      uint256,
      uint256,
      uint256
    );

  function getPriorProposalVotes(address account, uint256 blockNumber) external view returns (uint96);

  function receiveServiceRewards(uint256 day, uint256 amount) external;

  function receiveVoterRewards(uint256 day, uint256 amount) external;

  function updateVotes(
    address staker,
    uint256 rawAmount,
    bool adding
  ) external;
}

