// SPDX-License-Identifier: MIT

pragma solidity ^0.7.0;

/*
 * @dev Provides information about the current execution context, including the
 * sender of the transaction and its data. While these are generally available
 * via msg.sender and msg.data, they should not be accessed in such a direct
 * manner, since when dealing with GSN meta-transactions the account sending and
 * paying for execution may not be the actual sender (as far as an application
 * is concerned).
 *
 * This contract is only required for intermediate, library-like contracts.
 */
abstract contract Context {
    function _msgSender() internal view virtual returns (address payable) {
        return msg.sender;
    }

    function _msgData() internal view virtual returns (bytes memory) {
        this; // silence state mutability warning without generating bytecode - see https://github.com/ethereum/solidity/issues/2691
        return msg.data;
    }
}
"},"IERC20.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.7.0;

/**
 * @dev Interface of the ERC20 standard as defined in the EIP.
 */
interface IERC20 {
    /**
     * @dev Returns the amount of tokens in existence.
     */
    function totalSupply() external view returns (uint256);

    /**
     * @dev Returns the amount of tokens owned by `account`.
     */
    function balanceOf(address account) external view returns (uint256);

    /**
     * @dev Moves `amount` tokens from the caller\u0027s account to `recipient`.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transfer(address recipient, uint256 amount) external returns (bool);

    /**
     * @dev Returns the remaining number of tokens that `spender` will be
     * allowed to spend on behalf of `owner` through {transferFrom}. This is
     * zero by default.
     *
     * This value changes when {approve} or {transferFrom} are called.
     */
    function allowance(address owner, address spender) external view returns (uint256);

    /**
     * @dev Sets `amount` as the allowance of `spender` over the caller\u0027s tokens.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * IMPORTANT: Beware that changing an allowance with this method brings the risk
     * that someone may use both the old and the new allowance by unfortunate
     * transaction ordering. One possible solution to mitigate this race
     * condition is to first reduce the spender\u0027s allowance to 0 and set the
     * desired value afterwards:
     * https://github.com/ethereum/EIPs/issues/20#issuecomment-263524729
     *
     * Emits an {Approval} event.
     */
    function approve(address spender, uint256 amount) external returns (bool);

    /**
     * @dev Moves `amount` tokens from `sender` to `recipient` using the
     * allowance mechanism. `amount` is then deducted from the caller\u0027s
     * allowance.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transferFrom(address sender, address recipient, uint256 amount) external returns (bool);

    /**
     * @dev Emitted when `value` tokens are moved from one account (`from`) to
     * another (`to`).
     *
     * Note that `value` may be zero.
     */
    event Transfer(address indexed from, address indexed to, uint256 value);

    /**
     * @dev Emitted when the allowance of a `spender` for an `owner` is set by
     * a call to {approve}. `value` is the new allowance.
     */
    event Approval(address indexed owner, address indexed spender, uint256 value);
}
"},"Ownable.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.7.0;

import \"./Context.sol\";
/**
 * @dev Contract module which provides a basic access control mechanism, where
 * there is an account (an owner) that can be granted exclusive access to
 * specific functions.
 *
 * By default, the owner account will be the one that deploys the contract. This
 * can later be changed with {transferOwnership}.
 *
 * This module is used through inheritance. It will make available the modifier
 * `onlyOwner`, which can be applied to your functions to restrict their use to
 * the owner.
 */
contract Ownable is Context {
    address private _owner;

    event OwnershipTransferred(address indexed previousOwner, address indexed newOwner);

    /**
     * @dev Initializes the contract setting the deployer as the initial owner.
     */
    constructor () {
        address msgSender = _msgSender();
        _owner = msgSender;
        emit OwnershipTransferred(address(0), msgSender);
    }

    /**
     * @dev Returns the address of the current owner.
     */
    function owner() public view returns (address) {
        return _owner;
    }

    /**
     * @dev Throws if called by any account other than the owner.
     */
    modifier onlyOwner() {
        require(_owner == _msgSender(), \"Ownable: caller is not the owner\");
        _;
    }

    /**
     * @dev Leaves the contract without owner. It will not be possible to call
     * `onlyOwner` functions anymore. Can only be called by the current owner.
     *
     * NOTE: Renouncing ownership will leave the contract without an owner,
     * thereby removing any functionality that is only available to the owner.
     */
    function renounceOwnership() public virtual onlyOwner {
        emit OwnershipTransferred(_owner, address(0));
        _owner = address(0);
    }

    /**
     * @dev Transfers ownership of the contract to a new account (`newOwner`).
     * Can only be called by the current owner.
     */
    function transferOwnership(address newOwner) public virtual onlyOwner {
        require(newOwner != address(0), \"Ownable: new owner is the zero address\");
        emit OwnershipTransferred(_owner, newOwner);
        _owner = newOwner;
    }
}
"},"Root.sol":{"content":"// SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.7.0;
pragma experimental ABIEncoderV2;

import \"./IERC20.sol\";
import \"./SafeMath.sol\";
import \"./Ownable.sol\";

/// @title Contract Root
contract Root is Ownable {
    using SafeMath for uint;

    enum Group { PrivateRound1, PrivateRound2, UnlockedTokenFromPR2, PublicSale, Marketing, Liquidity, Team,
        Advisor, Development}

    IERC20 public token;

    struct GroupInfo {
        uint256[] balances;
        uint256[] balancesBase;
        uint256[] percents;
        address[] addresses;
    }

    mapping(Group =\u003e GroupInfo) private groupToInfo;
    uint256 private totalGroupsBalance;

    /// @notice Time when the contract was deployed
    uint256 public deployTime;

    constructor(address _token) {
        token = IERC20(_token);
        deployTime = block.timestamp;
    }

    /// @notice Load group data to contract.
    /// @return bool True if data successfully loaded
    function loadGroupInfo(GroupInfo memory _group, Group _groupNumber) external onlyOwner returns(bool) {
        require(groupToInfo[_groupNumber].addresses.length == 0, \"[E-39] - Group already loaded.\");
        require(_group.addresses.length \u003e 0, \"[E-40] - Empty address array in group.\");

        _updateTotalGroupBalance(_group);
        _checkTotalPercentSumInGroup(_group);

        _setupBaseBalance(_group);
        _transferTokensOnLoad(_group);

        groupToInfo[_groupNumber] = _group;

        return true;
    }

    /// @notice Write total group balance to storage. Need to calculate total groups balance
    /// @param _group Group to upload
    function _updateTotalGroupBalance(GroupInfo memory _group) private {
        uint256 _groupBalances = 0;
        for (uint256 k = 0; k \u003c _group.balances.length; k++) {
            _groupBalances = _groupBalances.add(_group.balances[k]);
        }

        totalGroupsBalance += _groupBalances;
    }

    /// @notice Check that percent sum inside group equal to 1
    /// @param _group Group to upload
    function _checkTotalPercentSumInGroup(GroupInfo memory _group) private pure {
        uint256 _percentSum = 0;
        for (uint256 k = 0; k \u003c _group.percents.length; k++) {
            _percentSum = _percentSum.add(_group.percents[k]);
        }
        require(_percentSum == getDecimal(), \"[E-104] - Invalid percent sum in group.\");
    }

    /// @notice Copy user balances to baseBalances
    /// @param _group Group to upload
    function _setupBaseBalance(GroupInfo memory _group) private pure {
        _group.balancesBase = new uint256[](_group.balances.length);

        for (uint256 k = 0; k \u003c _group.balances.length; k++) {
            _group.balancesBase[k] = _group.balances[k];
        }
    }

    /// @notice Transfer tokens in groups where TGE is 100% and execute base input validation
    /// @param _group Group to upload
    function _transferTokensOnLoad(GroupInfo memory _group) private {
        require(_group.addresses.length == _group.balancesBase.length, \"[E-50] - Address and balance length should be equal.\");

        if (_group.percents[0] == getDecimal()) {
            for (uint256 k = 0; k \u003c _group.addresses.length; k++) {
                _group.balances[k] = 0;
                token.transfer(_group.addresses[k], _group.balancesBase[k]);
            }
        }
    }

    /// @notice Transfer amount from contract to `msg.sender`
    /// @param _group Group number
    /// @param _amount Withdrawal amount
    /// @return True if withdrawal success
    function withdraw(Group _group, uint256 _amount) external returns(bool) {
        GroupInfo memory _groupInfo = groupToInfo[_group];

        uint256 _senderIndex = _getSenderIndexInGroup(_groupInfo);

        uint256 _availableToWithdraw = _getAvailableToWithdraw(_groupInfo);

        uint256 _amountToWithdraw = _amount \u003e _availableToWithdraw ? _availableToWithdraw : _amount;
        require(_amountToWithdraw != 0, \"[E-51] - Amount to withdraw is zero.\");

        groupToInfo[_group].balances[_senderIndex] = (_groupInfo.balances[_senderIndex]).sub(_amountToWithdraw);

        return token.transfer(msg.sender, _amountToWithdraw);
    }

    /// @notice Function for external call. See _getWithdrawPercent
    /// @param _group Group number
    function getWithdrawPercent(Group _group) external view returns(uint256) {
        GroupInfo memory _groupInfo = groupToInfo[_group];
        return _getWithdrawPercent(_groupInfo);
    }

    /// @notice Get total percent for group depending on the number of days elapsed after contract deploy.
    /// @notice For example, percent for first 30 days - 15%, all next 30 days - 5%, return 25% after 90 days.
    /// @param _groupInfo Structure with group info
    function _getWithdrawPercent(GroupInfo memory _groupInfo) private view returns(uint256) {
        uint256 _index = 0;
        uint256 _timePerIndex = 30 days;
        uint256 _deployTime = deployTime;

        while(_deployTime + _timePerIndex * (_index + 1) \u003c= block.timestamp) {
            _index++;
        }

        // Return 1 if last month is passed
        if (_groupInfo.percents.length - 1 \u003c= _index) return getDecimal();

        uint256 _monthWithdrawPercent = 0;
        for (uint256 i = 0; i \u003c= _index; i++) {
            _monthWithdrawPercent = _monthWithdrawPercent.add(_groupInfo.percents[i]);
        }

        uint256 _daysFromDeploy = (block.timestamp).sub(_deployTime).div(24 * 3600).mod(30);
        uint256 _daysWithdrawPercent = (_groupInfo.percents[_index + 1]).mul(_daysFromDeploy).div(30);

        return _monthWithdrawPercent.add(_daysWithdrawPercent);
    }

    /// @notice Function for external call. See _getWithdrawPercent.
    /// @param _group Group number
    function getAvailableToWithdraw(Group _group) external view returns(uint256) {
        GroupInfo memory _groupInfo = groupToInfo[_group];
        return _getAvailableToWithdraw(_groupInfo);
    }

    /// @param _groupInfo Structure with group info
    /// @return Amount that user can withdraw.
    function _getAvailableToWithdraw(GroupInfo memory _groupInfo) private view returns(uint256) {
        uint256 _withdrawPercent = _getWithdrawPercent(_groupInfo);
        uint256 _senderIndex = _getSenderIndexInGroup(_groupInfo);

        uint256 _availableToWithdraw = _withdrawPercent.mul(_groupInfo.balancesBase[_senderIndex]).div(getDecimal());
        uint256 _alreadyWithdraw = (_groupInfo.balancesBase[_senderIndex]).sub(_groupInfo.balances[_senderIndex]);

        return _availableToWithdraw.sub(_alreadyWithdraw);
    }

    /// @param _groupInfo Structure with group info
    /// @return Sender index that corresponds to the user balance and user balanceBase
    function _getSenderIndexInGroup(GroupInfo memory _groupInfo) private view returns(uint256) {
        bool _isAddressExistInGroup = false;
        uint256 _senderIndex = 0;

        for (uint256 i = 0; i \u003c _groupInfo.addresses.length; i++) {
            if (_groupInfo.addresses[i] == msg.sender) {
                _isAddressExistInGroup = true;
                _senderIndex = i;
                break;
            }
        }
        require(_isAddressExistInGroup, \u0027[E-55] - Address not found in selected group.\u0027);

        return _senderIndex;
    }

    /// @notice Decimal for contract
    function getDecimal() private pure returns (uint256) {
        return 10 ** 27;
    }
}
"},"SafeMath.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.7.0;

/**
 * @dev Wrappers over Solidity\u0027s arithmetic operations with added overflow
 * checks.
 *
 * Arithmetic operations in Solidity wrap on overflow. This can easily result
 * in bugs, because programmers usually assume that an overflow raises an
 * error, which is the standard behavior in high level programming languages.
 * `SafeMath` restores this intuition by reverting the transaction when an
 * operation overflows.
 *
 * Using this library instead of the unchecked operations eliminates an entire
 * class of bugs, so it\u0027s recommended to use it always.
 */
library SafeMath {
    /**
     * @dev Returns the addition of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity\u0027s `+` operator.
     *
     * Requirements:
     *
     * - Addition cannot overflow.
     */
    function add(uint256 a, uint256 b) internal pure returns (uint256) {
        uint256 c = a + b;
        require(c \u003e= a, \"SafeMath: addition overflow\");

        return c;
    }

    /**
     * @dev Returns the subtraction of two unsigned integers, reverting on
     * overflow (when the result is negative).
     *
     * Counterpart to Solidity\u0027s `-` operator.
     *
     * Requirements:
     *
     * - Subtraction cannot overflow.
     */
    function sub(uint256 a, uint256 b) internal pure returns (uint256) {
        return sub(a, b, \"SafeMath: subtraction overflow\");
    }

    /**
     * @dev Returns the subtraction of two unsigned integers, reverting with custom message on
     * overflow (when the result is negative).
     *
     * Counterpart to Solidity\u0027s `-` operator.
     *
     * Requirements:
     *
     * - Subtraction cannot overflow.
     */
    function sub(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b \u003c= a, errorMessage);
        uint256 c = a - b;

        return c;
    }

    /**
     * @dev Returns the multiplication of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity\u0027s `*` operator.
     *
     * Requirements:
     *
     * - Multiplication cannot overflow.
     */
    function mul(uint256 a, uint256 b) internal pure returns (uint256) {
        // Gas optimization: this is cheaper than requiring \u0027a\u0027 not being zero, but the
        // benefit is lost if \u0027b\u0027 is also tested.
        // See: https://github.com/OpenZeppelin/openzeppelin-contracts/pull/522
        if (a == 0) {
            return 0;
        }

        uint256 c = a * b;
        require(c / a == b, \"SafeMath: multiplication overflow\");

        return c;
    }

    /**
     * @dev Returns the integer division of two unsigned integers. Reverts on
     * division by zero. The result is rounded towards zero.
     *
     * Counterpart to Solidity\u0027s `/` operator. Note: this function uses a
     * `revert` opcode (which leaves remaining gas untouched) while Solidity
     * uses an invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function div(uint256 a, uint256 b) internal pure returns (uint256) {
        return div(a, b, \"SafeMath: division by zero\");
    }

    /**
     * @dev Returns the integer division of two unsigned integers. Reverts with custom message on
     * division by zero. The result is rounded towards zero.
     *
     * Counterpart to Solidity\u0027s `/` operator. Note: this function uses a
     * `revert` opcode (which leaves remaining gas untouched) while Solidity
     * uses an invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function div(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b \u003e 0, errorMessage);
        uint256 c = a / b;
        // assert(a == b * c + a % b); // There is no case in which this doesn\u0027t hold

        return c;
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
     * Reverts when dividing by zero.
     *
     * Counterpart to Solidity\u0027s `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function mod(uint256 a, uint256 b) internal pure returns (uint256) {
        return mod(a, b, \"SafeMath: modulo by zero\");
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
     * Reverts with custom message when dividing by zero.
     *
     * Counterpart to Solidity\u0027s `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function mod(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b != 0, errorMessage);
        return a % b;
    }
}

