



// SPDX-License-Identifier: MIT

// solhint-disable-next-line compiler-version
pragma solidity >=0.4.24 <0.8.0;

import \"../utils/AddressUpgradeable.sol\";

/**
 * @dev This is a base contract to aid in writing upgradeable contracts, or any kind of contract that will be deployed
 * behind a proxy. Since a proxied contract can't have a constructor, it's common to move constructor logic to an
 * external initializer function, usually called `initialize`. It then becomes necessary to protect this initializer
 * function so it can only be called once. The {initializer} modifier provided by this contract will have this effect.
 *
 * TIP: To avoid leaving the proxy in an uninitialized state, the initializer function should be called as early as
 * possible by providing the encoded function call as the `_data` argument to {UpgradeableProxy-constructor}.
 *
 * CAUTION: When used with inheritance, manual care must be taken to not invoke a parent initializer twice, or to ensure
 * that all initializers are idempotent. This is not verified automatically as constructors are by Solidity.
 */
abstract contract Initializable {

    /**
     * @dev Indicates that the contract has been initialized.
     */
    bool private _initialized;

    /**
     * @dev Indicates that the contract is in the process of being initialized.
     */
    bool private _initializing;

    /**
     * @dev Modifier to protect an initializer function from being invoked twice.
     */
    modifier initializer() {
        require(_initializing || _isConstructor() || !_initialized, \"Initializable: contract is already initialized\");

        bool isTopLevelCall = !_initializing;
        if (isTopLevelCall) {
            _initializing = true;
            _initialized = true;
        }

        _;

        if (isTopLevelCall) {
            _initializing = false;
        }
    }

    /// @dev Returns true if and only if the function is running in the constructor
    function _isConstructor() private view returns (bool) {
        return !AddressUpgradeable.isContract(address(this));
    }
}
"
    
// SPDX-License-Identifier: MIT

pragma solidity ^0.7.0;

/**
 * @dev Collection of functions related to the address type
 */
library AddressUpgradeable {
    /**
     * @dev Returns true if `account` is a contract.
     *
     * [IMPORTANT]
     * ====
     * It is unsafe to assume that an address for which this function returns
     * false is an externally-owned account (EOA) and not a contract.
     *
     * Among others, `isContract` will return false for the following
     * types of addresses:
     *
     *  - an externally-owned account
     *  - a contract in construction
     *  - an address where a contract will be created
     *  - an address where a contract lived, but was destroyed
     * ====
     */
    function isContract(address account) internal view returns (bool) {
        // This method relies on extcodesize, which returns 0 for contracts in
        // construction, since the code is only stored at the end of the
        // constructor execution.

        uint256 size;
        // solhint-disable-next-line no-inline-assembly
        assembly { size := extcodesize(account) }
        return size > 0;
    }

    /**
     * @dev Replacement for Solidity's `transfer`: sends `amount` wei to
     * `recipient`, forwarding all available gas and reverting on errors.
     *
     * https://eips.ethereum.org/EIPS/eip-1884[EIP1884] increases the gas cost
     * of certain opcodes, possibly making contracts go over the 2300 gas limit
     * imposed by `transfer`, making them unable to receive funds via
     * `transfer`. {sendValue} removes this limitation.
     *
     * https://diligence.consensys.net/posts/2019/09/stop-using-soliditys-transfer-now/[Learn more].
     *
     * IMPORTANT: because control is transferred to `recipient`, care must be
     * taken to not create reentrancy vulnerabilities. Consider using
     * {ReentrancyGuard} or the
     * https://solidity.readthedocs.io/en/v0.5.11/security-considerations.html#use-the-checks-effects-interactions-pattern[checks-effects-interactions pattern].
     */
    function sendValue(address payable recipient, uint256 amount) internal {
        require(address(this).balance >= amount, \"Address: insufficient balance\");

        // solhint-disable-next-line avoid-low-level-calls, avoid-call-value
        (bool success, ) = recipient.call{ value: amount }(\"\");
        require(success, \"Address: unable to send value, recipient may have reverted\");
    }

    /**
     * @dev Performs a Solidity function call using a low level `call`. A
     * plain`call` is an unsafe replacement for a function call: use this
     * function instead.
     *
     * If `target` reverts with a revert reason, it is bubbled up by this
     * function (like regular Solidity function calls).
     *
     * Returns the raw returned data. To convert to the expected return value,
     * use https://solidity.readthedocs.io/en/latest/units-and-global-variables.html?highlight=abi.decode#abi-encoding-and-decoding-functions[`abi.decode`].
     *
     * Requirements:
     *
     * - `target` must be a contract.
     * - calling `target` with `data` must not revert.
     *
     * _Available since v3.1._
     */
    function functionCall(address target, bytes memory data) internal returns (bytes memory) {
      return functionCall(target, data, \"Address: low-level call failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`], but with
     * `errorMessage` as a fallback revert reason when `target` reverts.
     *
     * _Available since v3.1._
     */
    function functionCall(address target, bytes memory data, string memory errorMessage) internal returns (bytes memory) {
        return functionCallWithValue(target, data, 0, errorMessage);
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`],
     * but also transferring `value` wei to `target`.
     *
     * Requirements:
     *
     * - the calling contract must have an ETH balance of at least `value`.
     * - the called Solidity function must be `payable`.
     *
     * _Available since v3.1._
     */
    function functionCallWithValue(address target, bytes memory data, uint256 value) internal returns (bytes memory) {
        return functionCallWithValue(target, data, value, \"Address: low-level call with value failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCallWithValue-address-bytes-uint256-}[`functionCallWithValue`], but
     * with `errorMessage` as a fallback revert reason when `target` reverts.
     *
     * _Available since v3.1._
     */
    function functionCallWithValue(address target, bytes memory data, uint256 value, string memory errorMessage) internal returns (bytes memory) {
        require(address(this).balance >= value, \"Address: insufficient balance for call\");
        require(isContract(target), \"Address: call to non-contract\");

        // solhint-disable-next-line avoid-low-level-calls
        (bool success, bytes memory returndata) = target.call{ value: value }(data);
        return _verifyCallResult(success, returndata, errorMessage);
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`],
     * but performing a static call.
     *
     * _Available since v3.3._
     */
    function functionStaticCall(address target, bytes memory data) internal view returns (bytes memory) {
        return functionStaticCall(target, data, \"Address: low-level static call failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-string-}[`functionCall`],
     * but performing a static call.
     *
     * _Available since v3.3._
     */
    function functionStaticCall(address target, bytes memory data, string memory errorMessage) internal view returns (bytes memory) {
        require(isContract(target), \"Address: static call to non-contract\");

        // solhint-disable-next-line avoid-low-level-calls
        (bool success, bytes memory returndata) = target.staticcall(data);
        return _verifyCallResult(success, returndata, errorMessage);
    }

    function _verifyCallResult(bool success, bytes memory returndata, string memory errorMessage) private pure returns(bytes memory) {
        if (success) {
            return returndata;
        } else {
            // Look for revert reason and bubble it up if present
            if (returndata.length > 0) {
                // The easiest way to bubble the revert reason is using memory via assembly

                // solhint-disable-next-line no-inline-assembly
                assembly {
                    let returndata_size := mload(returndata)
                    revert(add(32, returndata), returndata_size)
                }
            } else {
                revert(errorMessage);
            }
        }
    }
}
"
    
// SPDX-License-Identifier: MIT

pragma solidity >=0.6.0 <0.8.0;
import \"../proxy/Initializable.sol\";

/*
 * @dev Provides information about the current execution context, including the
 * sender of the transaction and its data. While these are generally available
 * via msg.sender and msg.data, they should not be accessed in such a direct
 * manner, since when dealing with GSN meta-transactions the account sending and
 * paying for execution may not be the actual sender (as far as an application
 * is concerned).
 *
 * This contract is only required for intermediate, library-like contracts.
 */
abstract contract ContextUpgradeable is Initializable {
    function __Context_init() internal initializer {
        __Context_init_unchained();
    }

    function __Context_init_unchained() internal initializer {
    }
    function _msgSender() internal view virtual returns (address payable) {
        return msg.sender;
    }

    function _msgData() internal view virtual returns (bytes memory) {
        this; // silence state mutability warning without generating bytecode - see https://github.com/ethereum/solidity/issues/2691
        return msg.data;
    }
    uint256[50] private __gap;
}
"
    
// SPDX-License-Identifier: MIT

pragma solidity ^0.7.0;

import \"./ContextUpgradeable.sol\";
import \"../proxy/Initializable.sol\";

/**
 * @dev Contract module which allows children to implement an emergency stop
 * mechanism that can be triggered by an authorized account.
 *
 * This module is used through inheritance. It will make available the
 * modifiers `whenNotPaused` and `whenPaused`, which can be applied to
 * the functions of your contract. Note that they will not be pausable by
 * simply including this module, only once the modifiers are put in place.
 */
abstract contract PausableUpgradeable is Initializable, ContextUpgradeable {
    /**
     * @dev Emitted when the pause is triggered by `account`.
     */
    event Paused(address account);

    /**
     * @dev Emitted when the pause is lifted by `account`.
     */
    event Unpaused(address account);

    bool private _paused;

    /**
     * @dev Initializes the contract in unpaused state.
     */
    function __Pausable_init() internal initializer {
        __Context_init_unchained();
        __Pausable_init_unchained();
    }

    function __Pausable_init_unchained() internal initializer {
        _paused = false;
    }

    /**
     * @dev Returns true if the contract is paused, and false otherwise.
     */
    function paused() public view virtual returns (bool) {
        return _paused;
    }

    /**
     * @dev Modifier to make a function callable only when the contract is not paused.
     *
     * Requirements:
     *
     * - The contract must not be paused.
     */
    modifier whenNotPaused() {
        require(!paused(), \"Pausable: paused\");
        _;
    }

    /**
     * @dev Modifier to make a function callable only when the contract is paused.
     *
     * Requirements:
     *
     * - The contract must be paused.
     */
    modifier whenPaused() {
        require(paused(), \"Pausable: not paused\");
        _;
    }

    /**
     * @dev Triggers stopped state.
     *
     * Requirements:
     *
     * - The contract must not be paused.
     */
    function _pause() internal virtual whenNotPaused {
        _paused = true;
        emit Paused(_msgSender());
    }

    /**
     * @dev Returns to normal state.
     *
     * Requirements:
     *
     * - The contract must be paused.
     */
    function _unpause() internal virtual whenPaused {
        _paused = false;
        emit Unpaused(_msgSender());
    }
    uint256[49] private __gap;
}
"
    
// SPDX-License-Identifier: MIT

pragma solidity ^0.7.0;

/**
 * @dev Wrappers over Solidity's arithmetic operations with added overflow
 * checks.
 *
 * Arithmetic operations in Solidity wrap on overflow. This can easily result
 * in bugs, because programmers usually assume that an overflow raises an
 * error, which is the standard behavior in high level programming languages.
 * `SafeMath` restores this intuition by reverting the transaction when an
 * operation overflows.
 *
 * Using this library instead of the unchecked operations eliminates an entire
 * class of bugs, so it's recommended to use it always.
 */
library SafeMath {
    /**
     * @dev Returns the addition of two unsigned integers, with an overflow flag.
     *
     * _Available since v3.4._
     */
    function tryAdd(uint256 a, uint256 b) internal pure returns (bool, uint256) {
        uint256 c = a + b;
        if (c < a) return (false, 0);
        return (true, c);
    }

    /**
     * @dev Returns the substraction of two unsigned integers, with an overflow flag.
     *
     * _Available since v3.4._
     */
    function trySub(uint256 a, uint256 b) internal pure returns (bool, uint256) {
        if (b > a) return (false, 0);
        return (true, a - b);
    }

    /**
     * @dev Returns the multiplication of two unsigned integers, with an overflow flag.
     *
     * _Available since v3.4._
     */
    function tryMul(uint256 a, uint256 b) internal pure returns (bool, uint256) {
        // Gas optimization: this is cheaper than requiring 'a' not being zero, but the
        // benefit is lost if 'b' is also tested.
        // See: https://github.com/OpenZeppelin/openzeppelin-contracts/pull/522
        if (a == 0) return (true, 0);
        uint256 c = a * b;
        if (c / a != b) return (false, 0);
        return (true, c);
    }

    /**
     * @dev Returns the division of two unsigned integers, with a division by zero flag.
     *
     * _Available since v3.4._
     */
    function tryDiv(uint256 a, uint256 b) internal pure returns (bool, uint256) {
        if (b == 0) return (false, 0);
        return (true, a / b);
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers, with a division by zero flag.
     *
     * _Available since v3.4._
     */
    function tryMod(uint256 a, uint256 b) internal pure returns (bool, uint256) {
        if (b == 0) return (false, 0);
        return (true, a % b);
    }

    /**
     * @dev Returns the addition of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity's `+` operator.
     *
     * Requirements:
     *
     * - Addition cannot overflow.
     */
    function add(uint256 a, uint256 b) internal pure returns (uint256) {
        uint256 c = a + b;
        require(c >= a, \"SafeMath: addition overflow\");
        return c;
    }

    /**
     * @dev Returns the subtraction of two unsigned integers, reverting on
     * overflow (when the result is negative).
     *
     * Counterpart to Solidity's `-` operator.
     *
     * Requirements:
     *
     * - Subtraction cannot overflow.
     */
    function sub(uint256 a, uint256 b) internal pure returns (uint256) {
        require(b <= a, \"SafeMath: subtraction overflow\");
        return a - b;
    }

    /**
     * @dev Returns the multiplication of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity's `*` operator.
     *
     * Requirements:
     *
     * - Multiplication cannot overflow.
     */
    function mul(uint256 a, uint256 b) internal pure returns (uint256) {
        if (a == 0) return 0;
        uint256 c = a * b;
        require(c / a == b, \"SafeMath: multiplication overflow\");
        return c;
    }

    /**
     * @dev Returns the integer division of two unsigned integers, reverting on
     * division by zero. The result is rounded towards zero.
     *
     * Counterpart to Solidity's `/` operator. Note: this function uses a
     * `revert` opcode (which leaves remaining gas untouched) while Solidity
     * uses an invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function div(uint256 a, uint256 b) internal pure returns (uint256) {
        require(b > 0, \"SafeMath: division by zero\");
        return a / b;
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
     * reverting when dividing by zero.
     *
     * Counterpart to Solidity's `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function mod(uint256 a, uint256 b) internal pure returns (uint256) {
        require(b > 0, \"SafeMath: modulo by zero\");
        return a % b;
    }

    /**
     * @dev Returns the subtraction of two unsigned integers, reverting with custom message on
     * overflow (when the result is negative).
     *
     * CAUTION: This function is deprecated because it requires allocating memory for the error
     * message unnecessarily. For custom revert reasons use {trySub}.
     *
     * Counterpart to Solidity's `-` operator.
     *
     * Requirements:
     *
     * - Subtraction cannot overflow.
     */
    function sub(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b <= a, errorMessage);
        return a - b;
    }

    /**
     * @dev Returns the integer division of two unsigned integers, reverting with custom message on
     * division by zero. The result is rounded towards zero.
     *
     * CAUTION: This function is deprecated because it requires allocating memory for the error
     * message unnecessarily. For custom revert reasons use {tryDiv}.
     *
     * Counterpart to Solidity's `/` operator. Note: this function uses a
     * `revert` opcode (which leaves remaining gas untouched) while Solidity
     * uses an invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function div(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b > 0, errorMessage);
        return a / b;
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
     * reverting with custom message when dividing by zero.
     *
     * CAUTION: This function is deprecated because it requires allocating memory for the error
     * message unnecessarily. For custom revert reasons use {tryMod}.
     *
     * Counterpart to Solidity's `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function mod(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b > 0, errorMessage);
        return a % b;
    }
}
"
    
// SPDX-License-Identifier: MIT

pragma solidity ^0.7.0;

/**
 * @dev Interface of the ERC20 standard as defined in the EIP.
 */
interface IERC20 {
    /**
     * @dev Returns the amount of tokens in existence.
     */
    function totalSupply() external view returns (uint256);

    /**
     * @dev Returns the amount of tokens owned by `account`.
     */
    function balanceOf(address account) external view returns (uint256);

    /**
     * @dev Moves `amount` tokens from the caller's account to `recipient`.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transfer(address recipient, uint256 amount) external returns (bool);

    /**
     * @dev Returns the remaining number of tokens that `spender` will be
     * allowed to spend on behalf of `owner` through {transferFrom}. This is
     * zero by default.
     *
     * This value changes when {approve} or {transferFrom} are called.
     */
    function allowance(address owner, address spender) external view returns (uint256);

    /**
     * @dev Sets `amount` as the allowance of `spender` over the caller's tokens.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * IMPORTANT: Beware that changing an allowance with this method brings the risk
     * that someone may use both the old and the new allowance by unfortunate
     * transaction ordering. One possible solution to mitigate this race
     * condition is to first reduce the spender's allowance to 0 and set the
     * desired value afterwards:
     * https://github.com/ethereum/EIPs/issues/20#issuecomment-263524729
     *
     * Emits an {Approval} event.
     */
    function approve(address spender, uint256 amount) external returns (bool);

    /**
     * @dev Moves `amount` tokens from `sender` to `recipient` using the
     * allowance mechanism. `amount` is then deducted from the caller's
     * allowance.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transferFrom(address sender, address recipient, uint256 amount) external returns (bool);

    /**
     * @dev Emitted when `value` tokens are moved from one account (`from`) to
     * another (`to`).
     *
     * Note that `value` may be zero.
     */
    event Transfer(address indexed from, address indexed to, uint256 value);

    /**
     * @dev Emitted when the allowance of a `spender` for an `owner` is set by
     * a call to {approve}. `value` is the new allowance.
     */
    event Approval(address indexed owner, address indexed spender, uint256 value);
}
"
    
// SPDX-License-Identifier: MIT

pragma solidity ^0.7.0;

import \"./IERC20.sol\";
import \"../../math/SafeMath.sol\";
import \"../../utils/Address.sol\";

/**
 * @title SafeERC20
 * @dev Wrappers around ERC20 operations that throw on failure (when the token
 * contract returns false). Tokens that return no value (and instead revert or
 * throw on failure) are also supported, non-reverting calls are assumed to be
 * successful.
 * To use this library you can add a `using SafeERC20 for IERC20;` statement to your contract,
 * which allows you to call the safe operations as `token.safeTransfer(...)`, etc.
 */
library SafeERC20 {
    using SafeMath for uint256;
    using Address for address;

    function safeTransfer(IERC20 token, address to, uint256 value) internal {
        _callOptionalReturn(token, abi.encodeWithSelector(token.transfer.selector, to, value));
    }

    function safeTransferFrom(IERC20 token, address from, address to, uint256 value) internal {
        _callOptionalReturn(token, abi.encodeWithSelector(token.transferFrom.selector, from, to, value));
    }

    /**
     * @dev Deprecated. This function has issues similar to the ones found in
     * {IERC20-approve}, and its usage is discouraged.
     *
     * Whenever possible, use {safeIncreaseAllowance} and
     * {safeDecreaseAllowance} instead.
     */
    function safeApprove(IERC20 token, address spender, uint256 value) internal {
        // safeApprove should only be called when setting an initial allowance,
        // or when resetting it to zero. To increase and decrease it, use
        // 'safeIncreaseAllowance' and 'safeDecreaseAllowance'
        // solhint-disable-next-line max-line-length
        require((value == 0) || (token.allowance(address(this), spender) == 0),
            \"SafeERC20: approve from non-zero to non-zero allowance\"
        );
        _callOptionalReturn(token, abi.encodeWithSelector(token.approve.selector, spender, value));
    }

    function safeIncreaseAllowance(IERC20 token, address spender, uint256 value) internal {
        uint256 newAllowance = token.allowance(address(this), spender).add(value);
        _callOptionalReturn(token, abi.encodeWithSelector(token.approve.selector, spender, newAllowance));
    }

    function safeDecreaseAllowance(IERC20 token, address spender, uint256 value) internal {
        uint256 newAllowance = token.allowance(address(this), spender).sub(value, \"SafeERC20: decreased allowance below zero\");
        _callOptionalReturn(token, abi.encodeWithSelector(token.approve.selector, spender, newAllowance));
    }

    /**
     * @dev Imitates a Solidity high-level call (i.e. a regular function call to a contract), relaxing the requirement
     * on the return value: the return value is optional (but if data is returned, it must not be false).
     * @param token The token targeted by the call.
     * @param data The call data (encoded using abi.encode or one of its variants).
     */
    function _callOptionalReturn(IERC20 token, bytes memory data) private {
        // We need to perform a low level call here, to bypass Solidity's return data size checking mechanism, since
        // we're implementing it ourselves. We use {Address.functionCall} to perform this call, which verifies that
        // the target address contains contract code and also asserts for success in the low-level call.

        bytes memory returndata = address(token).functionCall(data, \"SafeERC20: low-level call failed\");
        if (returndata.length > 0) { // Return data is optional
            // solhint-disable-next-line max-line-length
            require(abi.decode(returndata, (bool)), \"SafeERC20: ERC20 operation did not succeed\");
        }
    }
}
"
    
// SPDX-License-Identifier: MIT

pragma solidity ^0.7.0;

/**
 * @dev Collection of functions related to the address type
 */
library Address {
    /**
     * @dev Returns true if `account` is a contract.
     *
     * [IMPORTANT]
     * ====
     * It is unsafe to assume that an address for which this function returns
     * false is an externally-owned account (EOA) and not a contract.
     *
     * Among others, `isContract` will return false for the following
     * types of addresses:
     *
     *  - an externally-owned account
     *  - a contract in construction
     *  - an address where a contract will be created
     *  - an address where a contract lived, but was destroyed
     * ====
     */
    function isContract(address account) internal view returns (bool) {
        // This method relies on extcodesize, which returns 0 for contracts in
        // construction, since the code is only stored at the end of the
        // constructor execution.

        uint256 size;
        // solhint-disable-next-line no-inline-assembly
        assembly { size := extcodesize(account) }
        return size > 0;
    }

    /**
     * @dev Replacement for Solidity's `transfer`: sends `amount` wei to
     * `recipient`, forwarding all available gas and reverting on errors.
     *
     * https://eips.ethereum.org/EIPS/eip-1884[EIP1884] increases the gas cost
     * of certain opcodes, possibly making contracts go over the 2300 gas limit
     * imposed by `transfer`, making them unable to receive funds via
     * `transfer`. {sendValue} removes this limitation.
     *
     * https://diligence.consensys.net/posts/2019/09/stop-using-soliditys-transfer-now/[Learn more].
     *
     * IMPORTANT: because control is transferred to `recipient`, care must be
     * taken to not create reentrancy vulnerabilities. Consider using
     * {ReentrancyGuard} or the
     * https://solidity.readthedocs.io/en/v0.5.11/security-considerations.html#use-the-checks-effects-interactions-pattern[checks-effects-interactions pattern].
     */
    function sendValue(address payable recipient, uint256 amount) internal {
        require(address(this).balance >= amount, \"Address: insufficient balance\");

        // solhint-disable-next-line avoid-low-level-calls, avoid-call-value
        (bool success, ) = recipient.call{ value: amount }(\"\");
        require(success, \"Address: unable to send value, recipient may have reverted\");
    }

    /**
     * @dev Performs a Solidity function call using a low level `call`. A
     * plain`call` is an unsafe replacement for a function call: use this
     * function instead.
     *
     * If `target` reverts with a revert reason, it is bubbled up by this
     * function (like regular Solidity function calls).
     *
     * Returns the raw returned data. To convert to the expected return value,
     * use https://solidity.readthedocs.io/en/latest/units-and-global-variables.html?highlight=abi.decode#abi-encoding-and-decoding-functions[`abi.decode`].
     *
     * Requirements:
     *
     * - `target` must be a contract.
     * - calling `target` with `data` must not revert.
     *
     * _Available since v3.1._
     */
    function functionCall(address target, bytes memory data) internal returns (bytes memory) {
      return functionCall(target, data, \"Address: low-level call failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`], but with
     * `errorMessage` as a fallback revert reason when `target` reverts.
     *
     * _Available since v3.1._
     */
    function functionCall(address target, bytes memory data, string memory errorMessage) internal returns (bytes memory) {
        return functionCallWithValue(target, data, 0, errorMessage);
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`],
     * but also transferring `value` wei to `target`.
     *
     * Requirements:
     *
     * - the calling contract must have an ETH balance of at least `value`.
     * - the called Solidity function must be `payable`.
     *
     * _Available since v3.1._
     */
    function functionCallWithValue(address target, bytes memory data, uint256 value) internal returns (bytes memory) {
        return functionCallWithValue(target, data, value, \"Address: low-level call with value failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCallWithValue-address-bytes-uint256-}[`functionCallWithValue`], but
     * with `errorMessage` as a fallback revert reason when `target` reverts.
     *
     * _Available since v3.1._
     */
    function functionCallWithValue(address target, bytes memory data, uint256 value, string memory errorMessage) internal returns (bytes memory) {
        require(address(this).balance >= value, \"Address: insufficient balance for call\");
        require(isContract(target), \"Address: call to non-contract\");

        // solhint-disable-next-line avoid-low-level-calls
        (bool success, bytes memory returndata) = target.call{ value: value }(data);
        return _verifyCallResult(success, returndata, errorMessage);
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`],
     * but performing a static call.
     *
     * _Available since v3.3._
     */
    function functionStaticCall(address target, bytes memory data) internal view returns (bytes memory) {
        return functionStaticCall(target, data, \"Address: low-level static call failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-string-}[`functionCall`],
     * but performing a static call.
     *
     * _Available since v3.3._
     */
    function functionStaticCall(address target, bytes memory data, string memory errorMessage) internal view returns (bytes memory) {
        require(isContract(target), \"Address: static call to non-contract\");

        // solhint-disable-next-line avoid-low-level-calls
        (bool success, bytes memory returndata) = target.staticcall(data);
        return _verifyCallResult(success, returndata, errorMessage);
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`],
     * but performing a delegate call.
     *
     * _Available since v3.4._
     */
    function functionDelegateCall(address target, bytes memory data) internal returns (bytes memory) {
        return functionDelegateCall(target, data, \"Address: low-level delegate call failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-string-}[`functionCall`],
     * but performing a delegate call.
     *
     * _Available since v3.4._
     */
    function functionDelegateCall(address target, bytes memory data, string memory errorMessage) internal returns (bytes memory) {
        require(isContract(target), \"Address: delegate call to non-contract\");

        // solhint-disable-next-line avoid-low-level-calls
        (bool success, bytes memory returndata) = target.delegatecall(data);
        return _verifyCallResult(success, returndata, errorMessage);
    }

    function _verifyCallResult(bool success, bytes memory returndata, string memory errorMessage) private pure returns(bytes memory) {
        if (success) {
            return returndata;
        } else {
            // Look for revert reason and bubble it up if present
            if (returndata.length > 0) {
                // The easiest way to bubble the revert reason is using memory via assembly

                // solhint-disable-next-line no-inline-assembly
                assembly {
                    let returndata_size := mload(returndata)
                    revert(add(32, returndata), returndata_size)
                }
            } else {
                revert(errorMessage);
            }
        }
    }
}
"
    
// SPDX-License-Identifier: MIT

/**
 * Based on https://github.com/OpenZeppelin/openzeppelin-contracts-upgradeable/blob/v3.4.0-solc-0.7/contracts/access/OwnableUpgradeable.sol
 *
 * Changes:
 * - Added owner argument to initializer
 * - Reformatted styling in line with this repository.
 */

/*
The MIT License (MIT)

Copyright (c) 2016-2020 zOS Global Limited

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
\"Software\"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

/* solhint-disable func-name-mixedcase */

pragma solidity 0.7.6;

import \"@openzeppelin/contracts-upgradeable/utils/ContextUpgradeable.sol\";
import \"@openzeppelin/contracts-upgradeable/proxy/Initializable.sol\";

/**
 * @dev Contract module which provides a basic access control mechanism, where
 * there is an account (an owner) that can be granted exclusive access to
 * specific functions.
 *
 * By default, the owner account will be the one that deploys the contract. This
 * can later be changed with {transferOwnership}.
 *
 * This module is used through inheritance. It will make available the modifier
 * `onlyOwner`, which can be applied to your functions to restrict their use to
 * the owner.
 */
abstract contract OwnableUpgradeable is Initializable, ContextUpgradeable {
\taddress private _owner;

\tevent OwnershipTransferred(
\t\taddress indexed previousOwner,
\t\taddress indexed newOwner
\t);

\t/**
\t * @dev Initializes the contract setting the deployer as the initial owner.
\t */
\tfunction __Ownable_init(address owner_) internal initializer {
\t\t__Context_init_unchained();
\t\t__Ownable_init_unchained(owner_);
\t}

\tfunction __Ownable_init_unchained(address owner_) internal initializer {
\t\t_owner = owner_;
\t\temit OwnershipTransferred(address(0), owner_);
\t}

\t/**
\t * @dev Returns the address of the current owner.
\t */
\tfunction owner() public view virtual returns (address) {
\t\treturn _owner;
\t}

\t/**
\t * @dev Throws if called by any account other than the owner.
\t */
\tmodifier onlyOwner() {
\t\trequire(owner() == _msgSender(), \"Ownable: caller is not the owner\");
\t\t_;
\t}

\t/**
\t * @dev Leaves the contract without owner. It will not be possible to call
\t * `onlyOwner` functions anymore. Can only be called by the current owner.
\t *
\t * NOTE: Renouncing ownership will leave the contract without an owner,
\t * thereby removing any functionality that is only available to the owner.
\t */
\tfunction renounceOwnership() public virtual onlyOwner {
\t\temit OwnershipTransferred(_owner, address(0));
\t\t_owner = address(0);
\t}

\t/**
\t * @dev Transfers ownership of the contract to a new account (`newOwner`).
\t * Can only be called by the current owner.
\t */
\tfunction transferOwnership(address newOwner) public virtual onlyOwner {
\t\trequire(newOwner != address(0), \"Ownable: new owner is the zero address\");
\t\temit OwnershipTransferred(_owner, newOwner);
\t\t_owner = newOwner;
\t}

\tuint256[49] private __gap;
}
"
    
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

import \"@openzeppelin/contracts/utils/Address.sol\";
import \"@openzeppelin/contracts-upgradeable/utils/ContextUpgradeable.sol\";
import \"@openzeppelin/contracts/token/ERC20/IERC20.sol\";
import \"@openzeppelin/contracts-upgradeable/proxy/Initializable.sol\";
import \"@openzeppelin/contracts-upgradeable/utils/PausableUpgradeable.sol\";
import \"@openzeppelin/contracts/token/ERC20/SafeERC20.sol\";
import \"@openzeppelin/contracts/math/SafeMath.sol\";

import \"./ETHmxRewardsData.sol\";
import \"../../tokens/interfaces/IETHmx.sol\";
import \"../interfaces/IETHmxRewards.sol\";
import \"../../tokens/interfaces/IWETH.sol\";
import \"../../access/OwnableUpgradeable.sol\";

// High accuracy in block.timestamp is not needed.
// https://consensys.github.io/smart-contract-best-practices/recommendations/#the-15-second-rule
/* solhint-disable not-rely-on-time */

contract ETHmxRewards is
\tInitializable,
\tContextUpgradeable,
\tOwnableUpgradeable,
\tPausableUpgradeable,
\tETHmxRewardsData,
\tIETHmxRewards
{
\tusing Address for address payable;
\tusing SafeERC20 for IERC20;
\tusing SafeMath for uint256;

\tstruct ETHmxRewardsArgs {
\t\taddress ethmx;
\t\taddress weth;
\t\tuint256 accrualUpdateInterval;
\t}

\t/* Immutable Internal State */

\tuint256 internal constant _MULTIPLIER = 1e36;

\t/* Constructor */

\tconstructor(address owner_) {
\t\tinit(owner_);
\t}

\t/* Initializer */

\tfunction init(address owner_) public virtual initializer {
\t\t__Context_init_unchained();
\t\t__Ownable_init_unchained(owner_);
\t\t__Pausable_init_unchained();

\t\t_arptSnapshots.push(0);
\t}

\tfunction postInit(ETHmxRewardsArgs memory _args) external virtual onlyOwner {
\t\taddress sender = _msgSender();

\t\t_ethmx = _args.ethmx;
\t\temit ETHmxSet(sender, _args.ethmx);

\t\t_weth = _args.weth;
\t\temit WETHSet(sender, _args.weth);

\t\t_accrualUpdateInterval = _args.accrualUpdateInterval;
\t\temit AccrualUpdateIntervalSet(sender, _args.accrualUpdateInterval);
\t}

\t/* Fallbacks */

\treceive() external payable {
\t\t// Only accept ETH via fallback from the WETH contract
\t\trequire(msg.sender == weth());
\t}

\t/* Public Views */

\tfunction accrualUpdateInterval()
\t\texternal
\t\tview
\t\tvirtual
\t\toverride
\t\treturns (uint256)
\t{
\t\treturn _accrualUpdateInterval;
\t}

\tfunction accruedRewardsPerToken()
\t\tpublic
\t\tview
\t\tvirtual
\t\toverride
\t\treturns (uint256)
\t{
\t\treturn _arptSnapshots[_arptSnapshots.length - 1];
\t}

\tfunction accruedRewardsPerTokenLast(address account)
\t\tpublic
\t\tview
\t\tvirtual
\t\toverride
\t\treturns (uint256)
\t{
\t\treturn _arptSnapshots[_arptLastIdx[account]];
\t}

\tfunction ethmx() public view virtual override returns (address) {
\t\treturn _ethmx;
\t}

\tfunction lastAccrualUpdate()
\t\texternal
\t\tview
\t\tvirtual
\t\toverride
\t\treturns (uint256)
\t{
\t\treturn _lastAccrualUpdate;
\t}

\tfunction lastRewardsBalanceOf(address account)
\t\texternal
\t\tview
\t\tvirtual
\t\toverride
\t\treturns (uint256)
\t{
\t\treturn _rewardsFor[account];
\t}

\tfunction lastStakedBalanceOf(address account)
\t\texternal
\t\tview
\t\tvirtual
\t\toverride
\t\treturns (uint256)
\t{
\t\treturn _stakedFor[account];
\t}

\tfunction lastTotalRewardsAccrued()
\t\tpublic
\t\tview
\t\tvirtual
\t\toverride
\t\treturns (uint256)
\t{
\t\treturn _lastTotalRewardsAccrued;
\t}

\tfunction readyForUpdate() external view virtual override returns (bool) {
\t\tif (_lastAccrualUpdate > block.timestamp) {
\t\t\treturn false;
\t\t}
\t\tuint256 timePassed = block.timestamp - _lastAccrualUpdate;
\t\treturn timePassed >= _accrualUpdateInterval;
\t}

\tfunction rewardsBalanceOf(address account)
\t\texternal
\t\tview
\t\tvirtual
\t\toverride
\t\treturns (uint256)
\t{
\t\t// Gas savings
\t\tuint256 rewards = _rewardsFor[account];
\t\tuint256 staked = _stakedFor[account];

\t\tif (staked == 0) {
\t\t\treturn rewards;
\t\t}

\t\tuint256[] memory arptValues = _arptSnapshots;
\t\tuint256 length = arptValues.length;
\t\tuint256 arpt = arptValues[length - 1];
\t\tuint256 lastIdx = _arptLastIdx[account];
\t\tuint256 arptDelta = arpt - arptValues[lastIdx];

\t\tif (arptDelta == 0) {
\t\t\treturn rewards;
\t\t}

\t\t// Calculate reward and new stake
\t\tuint256 currentRewards = 0;
\t\tfor (uint256 i = lastIdx + 1; i < length; i++) {
\t\t\tarptDelta = arptValues[i] - arptValues[i - 1];
\t\t\tif (arptDelta >= _MULTIPLIER) {
\t\t\t\t// This should handle any plausible overflow
\t\t\t\trewards += staked;
\t\t\t\tstaked = 0;
\t\t\t\tbreak;
\t\t\t}
\t\t\tcurrentRewards = staked.mul(arptDelta) / _MULTIPLIER;
\t\t\trewards += currentRewards;
\t\t\tstaked -= currentRewards;
\t\t}

\t\treturn rewards;
\t}

\tfunction stakedBalanceOf(address account)
\t\texternal
\t\tview
\t\tvirtual
\t\toverride
\t\treturns (uint256)
\t{
\t\t// Gas savings
\t\tuint256 staked = _stakedFor[account];
\t\tif (staked == 0) {
\t\t\treturn 0;
\t\t}

\t\tuint256[] memory arptValues = _arptSnapshots;
\t\tuint256 length = arptValues.length;
\t\tuint256 arpt = arptValues[length - 1];
\t\tuint256 lastIdx = _arptLastIdx[account];
\t\tuint256 arptDelta = arpt - arptValues[lastIdx];

\t\tif (arptDelta == 0) {
\t\t\treturn staked;
\t\t}

\t\t// Calculate reward and new stake
\t\tuint256 currentRewards = 0;
\t\tfor (uint256 i = lastIdx + 1; i < length; i++) {
\t\t\tarptDelta = arptValues[i] - arptValues[i - 1];
\t\t\tif (arptDelta >= _MULTIPLIER) {
\t\t\t\t// This should handle any plausible overflow
\t\t\t\tstaked = 0;
\t\t\t\tbreak;
\t\t\t}
\t\t\tcurrentRewards = staked.mul(arptDelta) / _MULTIPLIER;
\t\t\tstaked -= currentRewards;
\t\t}

\t\treturn staked;
\t}

\tfunction totalRewardsAccrued()
\t\tpublic
\t\tview
\t\tvirtual
\t\toverride
\t\treturns (uint256)
\t{
\t\t// Overflow is OK
\t\treturn _currentRewardsBalance() + _totalRewardsRedeemed;
\t}

\tfunction totalRewardsRedeemed()
\t\tpublic
\t\tview
\t\tvirtual
\t\toverride
\t\treturns (uint256)
\t{
\t\treturn _totalRewardsRedeemed;
\t}

\tfunction totalStaked() public view virtual override returns (uint256) {
\t\treturn _totalStaked;
\t}

\tfunction unredeemableRewards()
\t\tpublic
\t\tview
\t\tvirtual
\t\toverride
\t\treturns (uint256)
\t{
\t\treturn _rewardsFor[address(0)];
\t}

\tfunction weth() public view virtual override returns (address) {
\t\treturn _weth;
\t}

\t/* Public Mutators */

\tfunction exit(bool asWETH) public virtual override {
\t\taddress account = _msgSender();
\t\tunstakeAll();
\t\t_redeemReward(account, _rewardsFor[account], asWETH);
\t}

\tfunction pause() public virtual override onlyOwner {
\t\t_pause();
\t}

\tfunction recoverUnredeemableRewards(address to, uint256 amount)
\t\tpublic
\t\tvirtual
\t\toverride
\t\tonlyOwner
\t{
\t\trequire(
\t\t\tamount <= _rewardsFor[address(0)],
\t\t\t\"ETHmxRewards: recovery amount greater than unredeemable\"
\t\t);
\t\t_rewardsFor[address(0)] -= amount;
\t\tIERC20(weth()).safeTransfer(to, amount);
\t\temit RecoveredUnredeemableRewards(_msgSender(), to, amount);
\t}

\tfunction recoverUnstaked(address to, uint256 amount)
\t\tpublic
\t\tvirtual
\t\toverride
\t\tonlyOwner
\t{
\t\tIERC20 ethmxHandle = IERC20(ethmx());
\t\tuint256 unstaked = ethmxHandle.balanceOf(address(this)).sub(_totalStaked);

\t\trequire(
\t\t\tamount <= unstaked,
\t\t\t\"ETHmxRewards: recovery amount greater than unstaked\"
\t\t);

\t\tethmxHandle.safeTransfer(to, amount);
\t\temit RecoveredUnstaked(_msgSender(), to, amount);
\t}

\tfunction recoverUnsupportedERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) public virtual override onlyOwner {
\t\trequire(token != ethmx(), \"ETHmxRewards: cannot recover ETHmx\");
\t\trequire(token != weth(), \"ETHmxRewards: cannot recover WETH\");
\t\tIERC20(token).safeTransfer(to, amount);
\t\temit RecoveredUnsupported(_msgSender(), token, to, amount);
\t}

\tfunction redeemAllRewards(bool asWETH) public virtual override {
\t\taddress account = _msgSender();
\t\t_updateRewardFor(account);
\t\t_redeemReward(account, _rewardsFor[account], asWETH);
\t}

\tfunction redeemReward(uint256 amount, bool asWETH) public virtual override {
\t\trequire(amount != 0, \"ETHmxRewards: cannot redeem zero\");
\t\taddress account = _msgSender();
\t\t// Update reward first (since it only goes up)
\t\t_updateRewardFor(account);
\t\trequire(
\t\t\tamount <= _rewardsFor[account],
\t\t\t\"ETHmxRewards: cannot redeem more rewards than earned\"
\t\t);
\t\t_redeemReward(account, amount, asWETH);
\t}

\tfunction setAccrualUpdateInterval(uint256 interval)
\t\tpublic
\t\tvirtual
\t\toverride
\t\tonlyOwner
\t{
\t\t_accrualUpdateInterval = interval;
\t\temit AccrualUpdateIntervalSet(_msgSender(), interval);
\t}

\tfunction setEthmx(address account) public virtual override onlyOwner {
\t\t_ethmx = account;
\t\temit ETHmxSet(_msgSender(), account);
\t}

\tfunction setWeth(address account) public virtual override onlyOwner {
\t\t_weth = account;
\t\temit WETHSet(_msgSender(), account);
\t}

\tfunction stake(uint256 amount) public virtual override whenNotPaused {
\t\trequire(amount != 0, \"ETHmxRewards: cannot stake zero\");

\t\taddress account = _msgSender();
\t\t_updateRewardFor(account);

\t\t_stakedFor[account] = _stakedFor[account].add(amount);
\t\t_totalStaked = _totalStaked.add(amount);

\t\tIERC20(ethmx()).safeTransferFrom(account, address(this), amount);
\t\temit Staked(account, amount);
\t}

\tfunction unpause() public virtual override onlyOwner {
\t\t_unpause();
\t}

\tfunction unstake(uint256 amount) public virtual override {
\t\trequire(amount != 0, \"ETHmxRewards: cannot unstake zero\");
\t\taddress account = _msgSender();

\t\t// Check against initial stake (since it only goes down)
\t\trequire(
\t\t\tamount <= _stakedFor[account],
\t\t\t\"ETHmxRewards: cannot unstake more than staked balance\"
\t\t);

\t\t// Update stake
\t\t_updateRewardFor(account);
\t\t// Cap amount with updated stake
\t\tuint256 staked = _stakedFor[account];
\t\tif (amount > staked) {
\t\t\tamount = staked;
\t\t}

\t\t_unstake(account, amount);
\t}

\tfunction unstakeAll() public virtual override {
\t\taddress account = _msgSender();
\t\t// Update stake first
\t\t_updateRewardFor(account);
\t\t_unstake(account, _stakedFor[account]);
\t}

\tfunction updateAccrual() public virtual override {
\t\tuint256 timePassed =
\t\t\tblock.timestamp.sub(
\t\t\t\t_lastAccrualUpdate,
\t\t\t\t\"ETHmxRewards: block is older than last accrual update\"
\t\t\t);
\t\trequire(
\t\t\ttimePassed >= _accrualUpdateInterval,
\t\t\t\"ETHmxRewards: too soon to update accrual\"
\t\t);

\t\t_updateAccrual();
\t}

\tfunction updateReward() public virtual override {
\t\t_updateRewardFor(_msgSender());
\t}

\t/* Internal Views */

\tfunction _currentRewardsBalance() internal view virtual returns (uint256) {
\t\treturn IERC20(weth()).balanceOf(address(this));
\t}

\t/* Internal Mutators */

\tfunction _burnETHmx(uint256 amount) internal virtual {
\t\t_totalStaked = _totalStaked.sub(amount);
\t\tIETHmx(ethmx()).burn(amount);
\t}

\tfunction _redeemReward(
\t\taddress account,
\t\tuint256 amount,
\t\tbool asWETH
\t) internal virtual {
\t\t// Should be guaranteed safe by caller (gas savings)
\t\t_rewardsFor[account] -= amount;
\t\t// Overflow is OK
\t\t_totalRewardsRedeemed += amount;

\t\tif (asWETH) {
\t\t\tIERC20(weth()).safeTransfer(account, amount);
\t\t} else {
\t\t\tIWETH(weth()).withdraw(amount);
\t\t\tpayable(account).sendValue(amount);
\t\t}

\t\temit RewardPaid(account, amount);
\t}

\tfunction _unstake(address account, uint256 amount) internal virtual {
\t\tif (amount == 0) {
\t\t\treturn;
\t\t}

\t\t// Should be guaranteed safe by caller
\t\t_stakedFor[account] -= amount;
\t\t_totalStaked = _totalStaked.sub(amount);

\t\tIERC20(ethmx()).safeTransfer(account, amount);
\t\temit Unstaked(account, amount);
\t}

\tfunction _updateAccrual() internal virtual {
\t\tuint256 rewardsAccrued = totalRewardsAccrued();
\t\t// Overflow is OK
\t\tuint256 newRewards = rewardsAccrued - _lastTotalRewardsAccrued;

\t\tif (newRewards == 0) {
\t\t\treturn;
\t\t}

\t\t// Gas savings
\t\tuint256 tstaked = _totalStaked;

\t\tif (newRewards < tstaked) {
\t\t\t// Add breathing room for better rounding, overflow is OK
\t\t\tuint256 arpt = accruedRewardsPerToken();
\t\t\tarpt += newRewards.mul(_MULTIPLIER) / tstaked;
\t\t\t_arptSnapshots.push(arpt);
\t\t\t_burnETHmx(newRewards);
\t\t} else {
\t\t\tuint256 leftover = newRewards - tstaked;
\t\t\t// Assign excess to zero address
\t\t\t_rewardsFor[address(0)] = _rewardsFor[address(0)].add(leftover);

\t\t\tif (tstaked != 0) {
\t\t\t\tuint256 arpt = accruedRewardsPerToken();
\t\t\t\t// newRewards when tokens == totalStaked
\t\t\t\tarpt += _MULTIPLIER;
\t\t\t\t_arptSnapshots.push(arpt);
\t\t\t\t_burnETHmx(tstaked);
\t\t\t}
\t\t}

\t\t_lastTotalRewardsAccrued = rewardsAccrued;
\t\t_lastAccrualUpdate = block.timestamp;
\t\temit AccrualUpdated(_msgSender(), rewardsAccrued);
\t}

\tfunction _updateRewardFor(address account) internal virtual {
\t\t// Gas savings
\t\tuint256[] memory arptValues = _arptSnapshots;
\t\tuint256 length = arptValues.length;
\t\tuint256 arpt = arptValues[length - 1];
\t\tuint256 lastIdx = _arptLastIdx[account];
\t\tuint256 arptDelta = arpt - arptValues[lastIdx];
\t\tuint256 staked = _stakedFor[account];

\t\t_arptLastIdx[account] = length - 1;

\t\tif (staked == 0 || arptDelta == 0) {
\t\t\treturn;
\t\t}

\t\t// Calculate reward and new stake
\t\tuint256 currentRewards = 0;
\t\tuint256 newRewards = 0;
\t\tfor (uint256 i = lastIdx + 1; i < length; i++) {
\t\t\tarptDelta = arptValues[i] - arptValues[i - 1];
\t\t\tif (arptDelta >= _MULTIPLIER) {
\t\t\t\t// This should handle any plausible overflow
\t\t\t\tnewRewards += staked;
\t\t\t\tstaked = 0;
\t\t\t\tbreak;
\t\t\t}
\t\t\tcurrentRewards = staked.mul(arptDelta) / _MULTIPLIER;
\t\t\tnewRewards += currentRewards;
\t\t\tstaked -= currentRewards;
\t\t}

\t\t// Update state
\t\t_stakedFor[account] = staked;
\t\t_rewardsFor[account] = _rewardsFor[account].add(newRewards);
\t}
}
"
    
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

abstract contract ETHmxRewardsData {
\taddress internal _ethmx;
\taddress internal _weth;

\tuint256[] internal _arptSnapshots;
\tmapping(address => uint256) internal _arptLastIdx;

\tuint256 internal _lastAccrualUpdate;
\tuint256 internal _accrualUpdateInterval;

\tmapping(address => uint256) internal _rewardsFor;
\tuint256 internal _lastTotalRewardsAccrued;
\tuint256 internal _totalRewardsRedeemed;

\tmapping(address => uint256) internal _stakedFor;
\tuint256 internal _totalStaked;

\tuint256[39] private __gap;
}
"
    
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

interface IETHmxRewards {
\t/* Views */

\tfunction accrualUpdateInterval() external view returns (uint256);

\tfunction accruedRewardsPerToken() external view returns (uint256);

\tfunction accruedRewardsPerTokenLast(address account)
\t\texternal
\t\tview
\t\treturns (uint256);

\tfunction ethmx() external view returns (address);

\tfunction lastAccrualUpdate() external view returns (uint256);

\tfunction lastRewardsBalanceOf(address account)
\t\texternal
\t\tview
\t\treturns (uint256);

\tfunction lastStakedBalanceOf(address account)
\t\texternal
\t\tview
\t\treturns (uint256);

\tfunction lastTotalRewardsAccrued() external view returns (uint256);

\tfunction readyForUpdate() external view returns (bool);

\tfunction rewardsBalanceOf(address account) external view returns (uint256);

\tfunction stakedBalanceOf(address account) external view returns (uint256);

\tfunction totalRewardsAccrued() external view returns (uint256);

\tfunction totalRewardsRedeemed() external view returns (uint256);

\tfunction totalStaked() external view returns (uint256);

\tfunction unredeemableRewards() external view returns (uint256);

\tfunction weth() external view returns (address);

\t/* Mutators */

\tfunction exit(bool asWETH) external;

\tfunction pause() external;

\tfunction recoverUnredeemableRewards(address to, uint256 amount) external;

\tfunction recoverUnstaked(address to, uint256 amount) external;

\tfunction recoverUnsupportedERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction redeemAllRewards(bool asWETH) external;

\tfunction redeemReward(uint256 amount, bool asWETH) external;

\tfunction setAccrualUpdateInterval(uint256 interval) external;

\tfunction setEthmx(address account) external;

\tfunction setWeth(address account) external;

\tfunction stake(uint256 amount) external;

\tfunction unpause() external;

\tfunction unstake(uint256 amount) external;

\tfunction unstakeAll() external;

\tfunction updateAccrual() external;

\tfunction updateReward() external;

\t/* Events */

\tevent AccrualUpdated(address indexed author, uint256 accruedRewards);
\tevent AccrualUpdateIntervalSet(address indexed author, uint256 interval);
\tevent ETHmxSet(address indexed author, address indexed account);
\tevent RecoveredUnredeemableRewards(
\t\taddress indexed author,
\t\taddress indexed to,
\t\tuint256 amount
\t);
\tevent RecoveredUnstaked(
\t\taddress indexed author,
\t\taddress indexed to,
\t\tuint256 amount
\t);
\tevent RecoveredUnsupported(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
\tevent RewardPaid(address indexed to, uint256 amount);
\tevent Staked(address indexed account, uint256 amount);
\tevent Unstaked(address indexed account, uint256 amount);
\tevent WETHSet(address indexed author, address indexed account);
}
"
    
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

interface IETHmx {
\t/* Views */

\tfunction minter() external view returns (address);

\t/* Mutators */

\tfunction burn(uint256 amount) external;

\tfunction mintTo(address account, uint256 amount) external;

\tfunction pause() external;

\tfunction recoverERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction setMinter(address account) external;

\tfunction unpause() external;

\t/* Events */

\tevent MinterSet(address indexed author, address indexed account);
\tevent Recovered(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
}
"
    
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
