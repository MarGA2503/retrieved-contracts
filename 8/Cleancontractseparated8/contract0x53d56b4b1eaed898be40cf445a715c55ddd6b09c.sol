// SPDX-License-Identifier: MIT

pragma solidity ^0.6.0;

/*
 * @dev Provides information about the current execution context, including the
 * sender of the transaction and its data. While these are generally available
 * via msg.sender and msg.data, they should not be accessed in such a direct
 * manner, since when dealing with GSN meta-transactions the account sending and
 * paying for execution may not be the actual sender (as far as an application
 * is concerned).
 *
 * This contract is only required for intermediate, library-like contracts.
 */
abstract contract Context {
    function _msgSender() internal view virtual returns (address payable) {
        return msg.sender;
    }

    function _msgData() internal view virtual returns (bytes memory) {
        this; // silence state mutability warning without generating bytecode - see https://github.com/ethereum/solidity/issues/2691
        return msg.data;
    }
}
"},"ContractRegistryAccessor.sol":{"content":"// SPDX-License-Identifier: UNLICENSED

pragma solidity 0.6.12;

import \"./IContractRegistry.sol\";
import \"./WithClaimableRegistryManagement.sol\";
import \"./Initializable.sol\";

contract ContractRegistryAccessor is WithClaimableRegistryManagement, Initializable {

    IContractRegistry private contractRegistry;

    constructor(IContractRegistry _contractRegistry, address _registryAdmin) public {
        require(address(_contractRegistry) != address(0), \"_contractRegistry cannot be 0\");
        setContractRegistry(_contractRegistry);
        _transferRegistryManagement(_registryAdmin);
    }

    modifier onlyAdmin {
        require(isAdmin(), \"sender is not an admin (registryManger or initializationAdmin)\");

        _;
    }

    function isManager(string memory role) internal view returns (bool) {
        IContractRegistry _contractRegistry = contractRegistry;
        return isAdmin() || _contractRegistry != IContractRegistry(0) \u0026\u0026 contractRegistry.getManager(role) == msg.sender;
    }

    function isAdmin() internal view returns (bool) {
        return msg.sender == registryAdmin() || msg.sender == initializationAdmin() || msg.sender == address(contractRegistry);
    }

    function getProtocolContract() internal view returns (address) {
        return contractRegistry.getContract(\"protocol\");
    }

    function getStakingRewardsContract() internal view returns (address) {
        return contractRegistry.getContract(\"stakingRewards\");
    }

    function getFeesAndBootstrapRewardsContract() internal view returns (address) {
        return contractRegistry.getContract(\"feesAndBootstrapRewards\");
    }

    function getCommitteeContract() internal view returns (address) {
        return contractRegistry.getContract(\"committee\");
    }

    function getElectionsContract() internal view returns (address) {
        return contractRegistry.getContract(\"elections\");
    }

    function getDelegationsContract() internal view returns (address) {
        return contractRegistry.getContract(\"delegations\");
    }

    function getGuardiansRegistrationContract() internal view returns (address) {
        return contractRegistry.getContract(\"guardiansRegistration\");
    }

    function getCertificationContract() internal view returns (address) {
        return contractRegistry.getContract(\"certification\");
    }

    function getStakingContract() internal view returns (address) {
        return contractRegistry.getContract(\"staking\");
    }

    function getSubscriptionsContract() internal view returns (address) {
        return contractRegistry.getContract(\"subscriptions\");
    }

    function getStakingRewardsWallet() internal view returns (address) {
        return contractRegistry.getContract(\"stakingRewardsWallet\");
    }

    function getBootstrapRewardsWallet() internal view returns (address) {
        return contractRegistry.getContract(\"bootstrapRewardsWallet\");
    }

    function getGeneralFeesWallet() internal view returns (address) {
        return contractRegistry.getContract(\"generalFeesWallet\");
    }

    function getCertifiedFeesWallet() internal view returns (address) {
        return contractRegistry.getContract(\"certifiedFeesWallet\");
    }

    function getStakingContractHandler() internal view returns (address) {
        return contractRegistry.getContract(\"stakingContractHandler\");
    }

    /*
    * Governance functions
    */

    event ContractRegistryAddressUpdated(address addr);

    function setContractRegistry(IContractRegistry newContractRegistry) public onlyAdmin {
        require(newContractRegistry.getPreviousContractRegistry() == address(contractRegistry), \"new contract registry must provide the previous contract registry\");
        contractRegistry = newContractRegistry;
        emit ContractRegistryAddressUpdated(address(newContractRegistry));
    }

    function getContractRegistry() public view returns (IContractRegistry) {
        return contractRegistry;
    }

}
"},"Delegations.sol":{"content":"// SPDX-License-Identifier: UNLICENSED

pragma solidity 0.6.12;

import \"./SafeMath.sol\";
import \"./SafeMath96.sol\";
import \"./IElections.sol\";
import \"./IDelegation.sol\";
import \"./IStakeChangeNotifier.sol\";
import \"./IStakingContractHandler.sol\";
import \"./IStakingRewards.sol\";
import \"./ManagedContract.sol\";

contract Delegations is IDelegations, IStakeChangeNotifier, ManagedContract {
\tusing SafeMath for uint256;
\tusing SafeMath96 for uint96;

\taddress constant public VOID_ADDR = address(-1);

\tstruct StakeOwnerData {
\t\taddress delegation;
\t\tuint96 stake;
\t}
\tmapping(address =\u003e StakeOwnerData) public stakeOwnersData;
\tmapping(address =\u003e uint256) public uncappedDelegatedStake;

\tuint256 totalDelegatedStake;

\tstruct DelegateStatus {
\t\taddress addr;
\t\tuint256 uncappedDelegatedStake;
\t\tbool isSelfDelegating;
\t\tuint256 delegatedStake;
\t\tuint96 selfDelegatedStake;
\t}

\tconstructor(IContractRegistry _contractRegistry, address _registryAdmin) ManagedContract(_contractRegistry, _registryAdmin) public {
\t\taddress VOID_ADDRESS_DUMMY_DELEGATION = address(-2);
\t\tassert(VOID_ADDR != VOID_ADDRESS_DUMMY_DELEGATION \u0026\u0026 VOID_ADDR != address(0) \u0026\u0026 VOID_ADDRESS_DUMMY_DELEGATION != address(0));
\t\tstakeOwnersData[VOID_ADDR].delegation = VOID_ADDRESS_DUMMY_DELEGATION;
\t}

\tmodifier onlyStakingContractHandler() {
\t\trequire(msg.sender == address(stakingContractHandler), \"caller is not the staking contract handler\");

\t\t_;
\t}

\t/*
\t* External functions
\t*/

\tfunction delegate(address to) external override onlyWhenActive {
\t\tdelegateFrom(msg.sender, to);
\t}

\tfunction getDelegation(address addr) external override view returns (address) {
\t\treturn getStakeOwnerData(addr).delegation;
\t}

\tfunction getDelegationInfo(address addr) external override view returns (address delegation, uint256 delegatorStake) {
\t\tStakeOwnerData memory data = getStakeOwnerData(addr);
\t\treturn (data.delegation, data.stake);
\t}

\tfunction getDelegatedStake(address addr) external override view returns (uint256) {
\t\treturn getDelegateStatus(addr).delegatedStake;
\t}

\tfunction getTotalDelegatedStake() external override view returns (uint256) {
\t\treturn totalDelegatedStake;
\t}

\tfunction refreshStake(address addr) external override onlyWhenActive {
\t\t_stakeChange(addr, stakingContractHandler.getStakeBalanceOf(addr));
\t}

\t/*
\t* Notifications from staking contract
\t*/

\tfunction stakeChange(address _stakeOwner, uint256, bool, uint256 _updatedStake) external override onlyStakingContractHandler onlyWhenActive {
\t\t_stakeChange(_stakeOwner, _updatedStake);
\t}

\tfunction stakeChangeBatch(address[] calldata _stakeOwners, uint256[] calldata _amounts, bool[] calldata _signs, uint256[] calldata _updatedStakes) external override onlyStakingContractHandler onlyWhenActive {
\t\tuint batchLength = _stakeOwners.length;
\t\trequire(batchLength == _amounts.length, \"_stakeOwners, _amounts - array length mismatch\");
\t\trequire(batchLength == _signs.length, \"_stakeOwners, _signs - array length mismatch\");
\t\trequire(batchLength == _updatedStakes.length, \"_stakeOwners, _updatedStakes - array length mismatch\");

\t\tfor (uint i = 0; i \u003c _stakeOwners.length; i++) {
\t\t\t_stakeChange(_stakeOwners[i], _updatedStakes[i]);
\t\t}
\t}

\tfunction stakeMigration(address _stakeOwner, uint256 _amount) external override onlyStakingContractHandler onlyWhenActive {}

\t/*
\t* Governance functions
\t*/

\tfunction importDelegations(address[] calldata from, address to) external override onlyInitializationAdmin {
\t\trequire(to != address(0), \"to must be a non zero address\");
\t\trequire(from.length \u003e 0, \"from array must contain at least one address\");
\t\t(uint96 stakingRewardsPerWeight, ) = stakingRewardsContract.getStakingRewardsState();
\t\trequire(stakingRewardsPerWeight == 0, \"no rewards may be allocated prior to importing delegations\");

\t\tuint256 uncappedDelegatedStakeDelta = 0;
\t\tStakeOwnerData memory data;
\t\tuint256 newTotalDelegatedStake = totalDelegatedStake;
\t\tDelegateStatus memory delegateStatus = getDelegateStatus(to);
\t\tIStakingContractHandler _stakingContractHandler = stakingContractHandler;
\t\tuint256 delegatorUncapped;
\t\tuint256[] memory delegatorsStakes = new uint256[](from.length);
\t\tfor (uint i = 0; i \u003c from.length; i++) {
\t\t\tdata = stakeOwnersData[from[i]];
\t\t\trequire(data.delegation == address(0), \"import allowed only for uninitialized accounts. existing delegation detected\");
\t\t\trequire(from[i] != to, \"import cannot be used for self-delegation (already self delegated)\");
\t\t\trequire(data.stake == 0 , \"import allowed only for uninitialized accounts. existing stake detected\");

\t\t\t// from[i] stops being self delegating. any uncappedDelegatedStake it has now stops being counted towards totalDelegatedStake
\t\t\tdelegatorUncapped = uncappedDelegatedStake[from[i]];
\t\t\tif (delegatorUncapped \u003e 0) {
\t\t\t\tnewTotalDelegatedStake = newTotalDelegatedStake.sub(delegatorUncapped);
\t\t\t\temit DelegatedStakeChanged(
\t\t\t\t\tfrom[i],
\t\t\t\t\t0,
\t\t\t\t\t0,
\t\t\t\t\tfrom[i],
\t\t\t\t\t0
\t\t\t\t);
\t\t\t}

\t\t\t// update state
\t\t\tdata.delegation = to;
\t\t\tdata.stake = uint96(_stakingContractHandler.getStakeBalanceOf(from[i]));
\t\t\tstakeOwnersData[from[i]] = data;

\t\t\tuncappedDelegatedStakeDelta = uncappedDelegatedStakeDelta.add(data.stake);

\t\t\t// store individual stake for event
\t\t\tdelegatorsStakes[i] = data.stake;

\t\t\temit Delegated(from[i], to);

\t\t\temit DelegatedStakeChanged(
\t\t\t\tto,
\t\t\t\tdelegateStatus.selfDelegatedStake,
\t\t\t\tdelegateStatus.isSelfDelegating ? delegateStatus.delegatedStake.add(uncappedDelegatedStakeDelta) : 0,
\t\t\t\tfrom[i],
\t\t\t\tdata.stake
\t\t\t);
\t\t}

\t\t// update totals
\t\tuncappedDelegatedStake[to] = uncappedDelegatedStake[to].add(uncappedDelegatedStakeDelta);

\t\tif (delegateStatus.isSelfDelegating) {
\t\t\tnewTotalDelegatedStake = newTotalDelegatedStake.add(uncappedDelegatedStakeDelta);
\t\t}
\t\ttotalDelegatedStake = newTotalDelegatedStake;

\t\t// emit events
\t\temit DelegationsImported(from, to);
\t}

\tfunction initDelegation(address from, address to) external override onlyInitializationAdmin {
\t\tdelegateFrom(from, to);
\t\temit DelegationInitialized(from, to);
\t}

\t/*
\t* Private functions
\t*/

\tfunction getDelegateStatus(address addr) private view returns (DelegateStatus memory status) {
\t\tStakeOwnerData memory data = getStakeOwnerData(addr);

\t\tstatus.addr = addr;
\t\tstatus.uncappedDelegatedStake = uncappedDelegatedStake[addr];
\t\tstatus.isSelfDelegating = data.delegation == addr;
\t\tstatus.selfDelegatedStake = status.isSelfDelegating ? data.stake : 0;
\t\tstatus.delegatedStake = status.isSelfDelegating ? status.uncappedDelegatedStake : 0;

\t\treturn status;
\t}

\tfunction getStakeOwnerData(address addr) private view returns (StakeOwnerData memory data) {
\t\tdata = stakeOwnersData[addr];
\t\tdata.delegation = (data.delegation == address(0)) ? addr : data.delegation;
\t\treturn data;
\t}

\tstruct DelegateFromVars {
\t\tDelegateStatus prevDelegateStatusBefore;
\t\tDelegateStatus newDelegateStatusBefore;
\t\tDelegateStatus prevDelegateStatusAfter;
\t\tDelegateStatus newDelegateStatusAfter;
\t}

\tfunction delegateFrom(address from, address to) private {
\t\trequire(to != address(0), \"cannot delegate to a zero address\");

\t\tDelegateFromVars memory vars;

\t\tStakeOwnerData memory delegatorData = getStakeOwnerData(from);

\t\t// Optimization - no need for the full flow in the case of a zero staked delegator with no delegations
\t\tif (delegatorData.stake == 0 \u0026\u0026 uncappedDelegatedStake[from] == 0) {
\t\t\tstakeOwnersData[from].delegation = to;
\t\t\temit Delegated(from, to);
\t\t\treturn;
\t\t}

\t\taddress prevDelegate = delegatorData.delegation;

\t\tvars.prevDelegateStatusBefore = getDelegateStatus(prevDelegate);
\t\tvars.newDelegateStatusBefore = getDelegateStatus(to);

\t\tstakingRewardsContract.delegationWillChange(prevDelegate, vars.prevDelegateStatusBefore.delegatedStake, from, delegatorData.stake, to, vars.newDelegateStatusBefore.delegatedStake);

\t\tstakeOwnersData[from].delegation = to;

\t\tuint256 delegatorStake = delegatorData.stake;

\t\tuncappedDelegatedStake[prevDelegate] = vars.prevDelegateStatusBefore.uncappedDelegatedStake.sub(delegatorStake);
\t\tuncappedDelegatedStake[to] = vars.newDelegateStatusBefore.uncappedDelegatedStake.add(delegatorStake);

\t\tvars.prevDelegateStatusAfter = getDelegateStatus(prevDelegate);
\t\tvars.newDelegateStatusAfter = getDelegateStatus(to);

\t\tuint256 _totalDelegatedStake = totalDelegatedStake.sub(
\t\t\tvars.prevDelegateStatusBefore.delegatedStake
\t\t).add(
\t\t\tvars.prevDelegateStatusAfter.delegatedStake
\t\t).sub(
\t\t\tvars.newDelegateStatusBefore.delegatedStake
\t\t).add(
\t\t\tvars.newDelegateStatusAfter.delegatedStake
\t\t);

\t\ttotalDelegatedStake = _totalDelegatedStake;

\t\temit Delegated(from, to);

\t\tIElections _electionsContract = electionsContract;

\t\tif (vars.prevDelegateStatusBefore.delegatedStake != vars.prevDelegateStatusAfter.delegatedStake) {
\t\t\t_electionsContract.delegatedStakeChange(
\t\t\t\tprevDelegate,
\t\t\t\tvars.prevDelegateStatusAfter.selfDelegatedStake,
\t\t\t\tvars.prevDelegateStatusAfter.delegatedStake,
\t\t\t\t_totalDelegatedStake
\t\t\t);

\t\t\temit DelegatedStakeChanged(
\t\t\t\tprevDelegate,
\t\t\t\tvars.prevDelegateStatusAfter.selfDelegatedStake,
\t\t\t\tvars.prevDelegateStatusAfter.delegatedStake,
\t\t\t\tfrom,
\t\t\t\t0
\t\t\t);
\t\t}

\t\tif (vars.newDelegateStatusBefore.delegatedStake != vars.newDelegateStatusAfter.delegatedStake) {
\t\t\t_electionsContract.delegatedStakeChange(
\t\t\t\tto,
\t\t\t\tvars.newDelegateStatusAfter.selfDelegatedStake,
\t\t\t\tvars.newDelegateStatusAfter.delegatedStake,
\t\t\t\t_totalDelegatedStake
\t\t\t);

\t\t\temit DelegatedStakeChanged(
\t\t\t\tto,
\t\t\t\tvars.newDelegateStatusAfter.selfDelegatedStake,
\t\t\t\tvars.newDelegateStatusAfter.delegatedStake,
\t\t\t\tfrom,
\t\t\t\tdelegatorStake
\t\t\t);
\t\t}
\t}

\tfunction _stakeChange(address _stakeOwner, uint256 _updatedStake) private {
\t\tStakeOwnerData memory stakeOwnerDataBefore = getStakeOwnerData(_stakeOwner);
\t\tDelegateStatus memory delegateStatusBefore = getDelegateStatus(stakeOwnerDataBefore.delegation);

\t\tuint256 prevUncappedStake = delegateStatusBefore.uncappedDelegatedStake;
\t\tuint256 newUncappedStake = prevUncappedStake.sub(stakeOwnerDataBefore.stake).add(_updatedStake);

\t\tstakingRewardsContract.delegationWillChange(stakeOwnerDataBefore.delegation, delegateStatusBefore.delegatedStake, _stakeOwner, stakeOwnerDataBefore.stake, stakeOwnerDataBefore.delegation, delegateStatusBefore.delegatedStake);

\t\tuncappedDelegatedStake[stakeOwnerDataBefore.delegation] = newUncappedStake;

\t\trequire(uint256(uint96(_updatedStake)) == _updatedStake, \"Delegations::updatedStakes value too big (\u003e96 bits)\");
\t\tstakeOwnersData[_stakeOwner].stake = uint96(_updatedStake);

\t\tuint256 _totalDelegatedStake = totalDelegatedStake;
\t\tif (delegateStatusBefore.isSelfDelegating) {
\t\t\t_totalDelegatedStake = _totalDelegatedStake.sub(stakeOwnerDataBefore.stake).add(_updatedStake);
\t\t\ttotalDelegatedStake = _totalDelegatedStake;
\t\t}

\t\tDelegateStatus memory delegateStatusAfter = getDelegateStatus(stakeOwnerDataBefore.delegation);

\t\telectionsContract.delegatedStakeChange(
\t\t\tstakeOwnerDataBefore.delegation,
\t\t\tdelegateStatusAfter.selfDelegatedStake,
\t\t\tdelegateStatusAfter.delegatedStake,
\t\t\t_totalDelegatedStake
\t\t);

\t\tif (_updatedStake != stakeOwnerDataBefore.stake) {
\t\t\temit DelegatedStakeChanged(
\t\t\t\tstakeOwnerDataBefore.delegation,
\t\t\t\tdelegateStatusAfter.selfDelegatedStake,
\t\t\t\tdelegateStatusAfter.delegatedStake,
\t\t\t\t_stakeOwner,
\t\t\t\t_updatedStake
\t\t\t);
\t\t}
\t}

\t/*
     * Contracts topology / registry interface
     */

\tIElections electionsContract;
\tIStakingRewards stakingRewardsContract;
\tIStakingContractHandler stakingContractHandler;
\tfunction refreshContracts() external override {
\t\telectionsContract = IElections(getElectionsContract());
\t\tstakingContractHandler = IStakingContractHandler(getStakingContractHandler());
\t\tstakingRewardsContract = IStakingRewards(getStakingRewardsContract());
\t}

}
"},"IContractRegistry.sol":{"content":"// SPDX-License-Identifier: UNLICENSED

pragma solidity 0.6.12;

interface IContractRegistry {

\tevent ContractAddressUpdated(string contractName, address addr, bool managedContract);
\tevent ManagerChanged(string role, address newManager);
\tevent ContractRegistryUpdated(address newContractRegistry);

\t/*
\t* External functions
\t*/

\t/// @dev updates the contracts address and emits a corresponding event
\t/// managedContract indicates whether the contract is managed by the registry and notified on changes
\tfunction setContract(string calldata contractName, address addr, bool managedContract) external /* onlyAdmin */;

\t/// @dev returns the current address of the given contracts
\tfunction getContract(string calldata contractName) external view returns (address);

\t/// @dev returns the list of contract addresses managed by the registry
\tfunction getManagedContracts() external view returns (address[] memory);

\tfunction setManager(string calldata role, address manager) external /* onlyAdmin */;

\tfunction getManager(string calldata role) external view returns (address);

\tfunction lockContracts() external /* onlyAdmin */;

\tfunction unlockContracts() external /* onlyAdmin */;

\tfunction setNewContractRegistry(IContractRegistry newRegistry) external /* onlyAdmin */;

\tfunction getPreviousContractRegistry() external view returns (address);

}
"},"IDelegation.sol":{"content":"// SPDX-License-Identifier: UNLICENSED

pragma solidity 0.6.12;

/// @title Delegations contract interface
interface IDelegations /* is IStakeChangeNotifier */ {

    // Delegation state change events
\tevent DelegatedStakeChanged(address indexed addr, uint256 selfDelegatedStake, uint256 delegatedStake, address indexed delegator, uint256 delegatorContributedStake);

    // Function calls
\tevent Delegated(address indexed from, address indexed to);

\t/*
     * External functions
     */

\t/// @dev Stake delegation
\tfunction delegate(address to) external /* onlyWhenActive */;

\tfunction refreshStake(address addr) external /* onlyWhenActive */;

\tfunction getDelegatedStake(address addr) external view returns (uint256);

\tfunction getDelegation(address addr) external view returns (address);

\tfunction getDelegationInfo(address addr) external view returns (address delegation, uint256 delegatorStake);

\tfunction getTotalDelegatedStake() external view returns (uint256) ;

\t/*
\t * Governance functions
\t */

\tevent DelegationsImported(address[] from, address indexed to);

\tevent DelegationInitialized(address indexed from, address indexed to);

\tfunction importDelegations(address[] calldata from, address to) external /* onlyMigrationManager onlyDuringDelegationImport */;

\tfunction initDelegation(address from, address to) external /* onlyInitializationAdmin */;
}
"},"IElections.sol":{"content":"// SPDX-License-Identifier: UNLICENSED

pragma solidity 0.6.12;

/// @title Elections contract interface
interface IElections {
\t
\t// Election state change events
\tevent StakeChanged(address indexed addr, uint256 selfStake, uint256 delegatedStake, uint256 effectiveStake);
\tevent GuardianStatusUpdated(address indexed guardian, bool readyToSync, bool readyForCommittee);

\t// Vote out / Vote unready
\tevent GuardianVotedUnready(address indexed guardian);
\tevent VoteUnreadyCasted(address indexed voter, address indexed subject, uint256 expiration);
\tevent GuardianVotedOut(address indexed guardian);
\tevent VoteOutCasted(address indexed voter, address indexed subject);

\t/*
\t * External functions
\t */

\t/// @dev Called by a guardian when ready to start syncing with other nodes
\tfunction readyToSync() external;

\t/// @dev Called by a guardian when ready to join the committee, typically after syncing is complete or after being voted out
\tfunction readyForCommittee() external;

\t/// @dev Called to test if a guardian calling readyForCommittee() will lead to joining the committee
\tfunction canJoinCommittee(address guardian) external view returns (bool);

\t/// @dev Returns an address effective stake
\tfunction getEffectiveStake(address guardian) external view returns (uint effectiveStake);

\t/// @dev returns the current committee
\t/// used also by the rewards and fees contracts
\tfunction getCommittee() external view returns (address[] memory committee, uint256[] memory weights, address[] memory orbsAddrs, bool[] memory certification, bytes4[] memory ips);

\t// Vote-unready

\t/// @dev Called by a guardian as part of the automatic vote-unready flow
\tfunction voteUnready(address subject, uint expiration) external;

\tfunction getVoteUnreadyVote(address voter, address subject) external view returns (bool valid, uint256 expiration);

\t/// @dev Returns the current vote-unready status of a subject guardian.
\t/// votes indicates wether the specific committee member voted the guardian unready
\tfunction getVoteUnreadyStatus(address subject) external view returns (
\t\taddress[] memory committee,
\t\tuint256[] memory weights,
\t\tbool[] memory certification,
\t\tbool[] memory votes,
\t\tbool subjectInCommittee,
\t\tbool subjectInCertifiedCommittee
\t);

\t// Vote-out

\t/// @dev Casts a voteOut vote by the sender to the given address
\tfunction voteOut(address subject) external;

\t/// @dev Returns the subject address the addr has voted-out against
\tfunction getVoteOutVote(address voter) external view returns (address);

\t/// @dev Returns the governance voteOut status of a guardian.
\t/// A guardian is voted out if votedStake / totalDelegatedStake (in percent mille) \u003e threshold
\tfunction getVoteOutStatus(address subject) external view returns (bool votedOut, uint votedStake, uint totalDelegatedStake);

\t/*
\t * Notification functions from other PoS contracts
\t */

\t/// @dev Called by: delegation contract
\t/// Notifies a delegated stake change event
\t/// total_delegated_stake = 0 if addr delegates to another guardian
\tfunction delegatedStakeChange(address delegate, uint256 selfStake, uint256 delegatedStake, uint256 totalDelegatedStake) external /* onlyDelegationsContract onlyWhenActive */;

\t/// @dev Called by: guardian registration contract
\t/// Notifies a new guardian was unregistered
\tfunction guardianUnregistered(address guardian) external /* onlyGuardiansRegistrationContract */;

\t/// @dev Called by: guardian registration contract
\t/// Notifies on a guardian certification change
\tfunction guardianCertificationChanged(address guardian, bool isCertified) external /* onlyCertificationContract */;


\t/*
     * Governance functions
\t */

\tevent VoteUnreadyTimeoutSecondsChanged(uint32 newValue, uint32 oldValue);
\tevent VoteOutPercentMilleThresholdChanged(uint32 newValue, uint32 oldValue);
\tevent VoteUnreadyPercentMilleThresholdChanged(uint32 newValue, uint32 oldValue);
\tevent MinSelfStakePercentMilleChanged(uint32 newValue, uint32 oldValue);

\t/// @dev Sets the minimum self-stake required for the effective stake
\t/// minSelfStakePercentMille - the minimum self stake in percent-mille (0-100,000)
\tfunction setMinSelfStakePercentMille(uint32 minSelfStakePercentMille) external /* onlyFunctionalManager onlyWhenActive */;

\t/// @dev Returns the minimum self-stake required for the effective stake
\tfunction getMinSelfStakePercentMille() external view returns (uint32);

\t/// @dev Sets the vote-out threshold
\t/// voteOutPercentMilleThreshold - the minimum threshold in percent-mille (0-100,000)
\tfunction setVoteOutPercentMilleThreshold(uint32 voteUnreadyPercentMilleThreshold) external /* onlyFunctionalManager onlyWhenActive */;

\t/// @dev Returns the vote-out threshold
\tfunction getVoteOutPercentMilleThreshold() external view returns (uint32);

\t/// @dev Sets the vote-unready threshold
\t/// voteUnreadyPercentMilleThreshold - the minimum threshold in percent-mille (0-100,000)
\tfunction setVoteUnreadyPercentMilleThreshold(uint32 voteUnreadyPercentMilleThreshold) external /* onlyFunctionalManager onlyWhenActive */;

\t/// @dev Returns the vote-unready threshold
\tfunction getVoteUnreadyPercentMilleThreshold() external view returns (uint32);

\t/// @dev Returns the contract\u0027s settings 
\tfunction getSettings() external view returns (
\t\tuint32 minSelfStakePercentMille,
\t\tuint32 voteUnreadyPercentMilleThreshold,
\t\tuint32 voteOutPercentMilleThreshold
\t);

\tfunction initReadyForCommittee(address[] calldata guardians) external /* onlyInitializationAdmin */;

}

"},"ILockable.sol":{"content":"// SPDX-License-Identifier: UNLICENSED

pragma solidity 0.6.12;

interface ILockable {

    event Locked();
    event Unlocked();

    function lock() external /* onlyLockOwner */;
    function unlock() external /* onlyLockOwner */;
    function isLocked() view external returns (bool);

}
"},"Initializable.sol":{"content":"// SPDX-License-Identifier: UNLICENSED

pragma solidity 0.6.12;

contract Initializable {

    address private _initializationAdmin;

    event InitializationComplete();

    constructor() public{
        _initializationAdmin = msg.sender;
    }

    modifier onlyInitializationAdmin() {
        require(msg.sender == initializationAdmin(), \"sender is not the initialization admin\");

        _;
    }

    /*
    * External functions
    */

    function initializationAdmin() public view returns (address) {
        return _initializationAdmin;
    }

    function initializationComplete() external onlyInitializationAdmin {
        _initializationAdmin = address(0);
        emit InitializationComplete();
    }

    function isInitializationComplete() public view returns (bool) {
        return _initializationAdmin == address(0);
    }

}"},"IStakeChangeNotifier.sol":{"content":"// SPDX-License-Identifier: UNLICENSED

pragma solidity 0.6.12;

/// @title An interface for notifying of stake change events (e.g., stake, unstake, partial unstake, restate, etc.).
interface IStakeChangeNotifier {
    /// @dev Notifies of stake change event.
    /// @param _stakeOwner address The address of the subject stake owner.
    /// @param _amount uint256 The difference in the total staked amount.
    /// @param _sign bool The sign of the added (true) or subtracted (false) amount.
    /// @param _updatedStake uint256 The updated total staked amount.
    function stakeChange(address _stakeOwner, uint256 _amount, bool _sign, uint256 _updatedStake) external;

    /// @dev Notifies of multiple stake change events.
    /// @param _stakeOwners address[] The addresses of subject stake owners.
    /// @param _amounts uint256[] The differences in total staked amounts.
    /// @param _signs bool[] The signs of the added (true) or subtracted (false) amounts.
    /// @param _updatedStakes uint256[] The updated total staked amounts.
    function stakeChangeBatch(address[] calldata _stakeOwners, uint256[] calldata _amounts, bool[] calldata _signs,
        uint256[] calldata _updatedStakes) external;

    /// @dev Notifies of stake migration event.
    /// @param _stakeOwner address The address of the subject stake owner.
    /// @param _amount uint256 The migrated amount.
    function stakeMigration(address _stakeOwner, uint256 _amount) external;
}
"},"IStakingContractHandler.sol":{"content":"// SPDX-License-Identifier: UNLICENSED

pragma solidity 0.6.12;

/// @title An interface for staking contracts.
interface IStakingContractHandler {
    event StakeChangeNotificationSkipped(address indexed stakeOwner);
    event StakeChangeBatchNotificationSkipped(address[] stakeOwners);
    event StakeMigrationNotificationSkipped(address indexed stakeOwner);

    /*
    * External functions
    */

    /// @dev Returns the stake of the specified stake owner (excluding unstaked tokens).
    /// @param _stakeOwner address The address to check.
    /// @return uint256 The total stake.
    function getStakeBalanceOf(address _stakeOwner) external view returns (uint256);

    /// @dev Returns the total amount staked tokens (excluding unstaked tokens).
    /// @return uint256 The total staked tokens of all stake owners.
    function getTotalStakedTokens() external view returns (uint256);

    /*
    * Governance functions
    */

    event NotifyDelegationsChanged(bool notifyDelegations);

    function setNotifyDelegations(bool notifyDelegations) external; /* onlyMigrationManager */

    function getNotifyDelegations() external returns (bool);
}
"},"IStakingRewards.sol":{"content":"// SPDX-License-Identifier: UNLICENSED

pragma solidity 0.6.12;

/// @title Staking rewards contract interface
interface IStakingRewards {

    event DelegatorStakingRewardsAssigned(address indexed delegator, uint256 amount, uint256 totalAwarded, address guardian, uint256 delegatorRewardsPerToken);
    event GuardianStakingRewardsAssigned(address indexed guardian, uint256 amount, uint256 totalAwarded, uint256 delegatorRewardsPerToken, uint256 stakingRewardsPerWeight);
    event StakingRewardsClaimed(address indexed addr, uint256 claimedDelegatorRewards, uint256 claimedGuardianRewards, uint256 totalClaimedDelegatorRewards, uint256 totalClaimedGuardianRewards);
    event StakingRewardsAllocated(uint256 allocatedRewards, uint256 stakingRewardsPerWeight);
    event GuardianDelegatorsStakingRewardsPercentMilleUpdated(address indexed guardian, uint256 delegatorsStakingRewardsPercentMille);

    /*
     * External functions
     */

    /// @dev Returns the currently unclaimed orbs token reward balance of the given address.
    function getStakingRewardsBalance(address addr) external view returns (uint256 balance);

    /// @dev Allows Guardian to set a different delegator staking reward cut than the default
    /// delegatorRewardsPercentMille accepts values between 0 - maxDelegatorsStakingRewardsPercentMille
    function setGuardianDelegatorsStakingRewardsPercentMille(uint32 delegatorRewardsPercentMille) external;

    /// @dev Returns the guardian\u0027s delegatorRewardsPercentMille
    function getGuardianDelegatorsStakingRewardsPercentMille(address guardian) external view returns (uint256 delegatorRewardsRatioPercentMille);

    /// @dev Claims the staking rewards balance of addr by staking
    function claimStakingRewards(address addr) external;

    /// @dev Returns the amount of ORBS tokens in the staking wallet that were allocated
    /// but not yet claimed. The staking wallet balance must always larger than the allocated value.
    function getStakingRewardsWalletAllocatedTokens() external view returns (uint256 allocated);

    function getGuardianStakingRewardsData(address guardian) external view returns (
        uint256 balance,
        uint256 claimed,
        uint256 delegatorRewardsPerToken,
        uint256 lastStakingRewardsPerWeight
    );

    function getDelegatorStakingRewardsData(address delegator) external view returns (
        uint256 balance,
        uint256 claimed,
        uint256 lastDelegatorRewardsPerToken
    );

    function getStakingRewardsState() external view returns (
        uint96 stakingRewardsPerWeight,
        uint96 unclaimedStakingRewards
    );

    function getCurrentStakingRewardsRatePercentMille() external returns (uint256);

    /// @dev called by the Committee contract upon expected change in the committee membership of the guardian
    /// Triggers update of the member rewards
    function committeeMembershipWillChange(address guardian, uint256 weight, uint256 totalCommitteeWeight, bool inCommittee, bool inCommitteeAfter) external /* onlyCommitteeContract */;

    /// @dev called by the Delegation contract upon expected change in a committee member delegator stake
    /// Triggers update of the delegator and guardian staking rewards
    function delegationWillChange(address guardian, uint256 delegatedStake, address delegator, uint256 delegatorStake, address nextGuardian, uint256 nextGuardianDelegatedStake) external /* onlyDelegationsContract */;

    /*
     * Governance functions
     */

    event AnnualStakingRewardsRateChanged(uint256 annualRateInPercentMille, uint256 annualCap);
    event DefaultDelegatorsStakingRewardsChanged(uint32 defaultDelegatorsStakingRewardsPercentMille);
    event MaxDelegatorsStakingRewardsChanged(uint32 maxDelegatorsStakingRewardsPercentMille);
    event RewardDistributionActivated(uint256 startTime);
    event RewardDistributionDeactivated();
    event StakingRewardsBalanceMigrated(address indexed addr, uint256 guardianStakingRewards, uint256 delegatorStakingRewards, address toRewardsContract);
    event StakingRewardsBalanceMigrationAccepted(address from, address indexed addr, uint256 guardianStakingRewards, uint256 delegatorStakingRewards);
    event EmergencyWithdrawal(address addr);

    /// @dev activates reward distribution, all rewards will be distributed up
    /// assuming the last assignment was on startTime (the time the old contarct was deactivated)
    function activateRewardDistribution(uint startTime) external /* onlyInitializationAdmin */;

    /// @dev deactivates reward distribution, all rewards will be distributed up
    /// deactivate moment.
    function deactivateRewardDistribution() external /* onlyMigrationManager */;

    /// @dev Sets the default cut of the delegators staking reward.
    function setDefaultDelegatorsStakingRewardsPercentMille(uint32 defaultDelegatorsStakingRewardsPercentMille) external /* onlyFunctionalManager onlyWhenActive */;

    function getDefaultDelegatorsStakingRewardsPercentMille() external view returns (uint32);

    /// @dev Sets the maximum cut of the delegators staking reward.
    function setMaxDelegatorsStakingRewardsPercentMille(uint32 maxDelegatorsStakingRewardsPercentMille) external /* onlyFunctionalManager onlyWhenActive */;

    function getMaxDelegatorsStakingRewardsPercentMille() external view returns (uint32);

    /// @dev Sets a new annual rate and cap for the staking reward.
    function setAnnualStakingRewardsRate(uint256 annualRateInPercentMille, uint256 annualCap) external /* onlyFunctionalManager */;

    function getAnnualStakingRewardsRatePercentMille() external view returns (uint32);

    function getAnnualStakingRewardsCap() external view returns (uint256);

    function isRewardAllocationActive() external view returns (bool);

    /// @dev Returns the contract\u0027s settings
    function getSettings() external view returns (
        uint annualStakingRewardsCap,
        uint32 annualStakingRewardsRatePercentMille,
        uint32 defaultDelegatorsStakingRewardsPercentMille,
        uint32 maxDelegatorsStakingRewardsPercentMille,
        bool rewardAllocationActive
    );

    /// @dev migrates the staking rewards balance of the guardian to the rewards contract as set in the registry.
    function migrateRewardsBalance(address guardian) external;

    /// @dev accepts guardian\u0027s balance migration from a previous rewards contarct.
    function acceptRewardsBalanceMigration(address guardian, uint256 guardianStakingRewards, uint256 delegatorStakingRewards) external;

    /// @dev emergency withdrawal of the rewards contract balances, may eb called only by the EmergencyManager. 
    function emergencyWithdraw() external /* onlyMigrationManager */;
}

"},"Lockable.sol":{"content":"// SPDX-License-Identifier: UNLICENSED

pragma solidity 0.6.12;

import \"./ContractRegistryAccessor.sol\";
import \"./ILockable.sol\";

contract Lockable is ILockable, ContractRegistryAccessor {

    bool public locked;

    constructor(IContractRegistry _contractRegistry, address _registryAdmin) ContractRegistryAccessor(_contractRegistry, _registryAdmin) public {}

    modifier onlyLockOwner() {
        require(msg.sender == registryAdmin() || msg.sender == address(getContractRegistry()), \"caller is not a lock owner\");

        _;
    }

    function lock() external override onlyLockOwner {
        locked = true;
        emit Locked();
    }

    function unlock() external override onlyLockOwner {
        locked = false;
        emit Unlocked();
    }

    function isLocked() external override view returns (bool) {
        return locked;
    }

    modifier onlyWhenActive() {
        require(!locked, \"contract is locked for this operation\");

        _;
    }
}
"},"ManagedContract.sol":{"content":"// SPDX-License-Identifier: UNLICENSED

pragma solidity 0.6.12;

import \"./Lockable.sol\";

contract ManagedContract is Lockable {

    constructor(IContractRegistry _contractRegistry, address _registryAdmin) Lockable(_contractRegistry, _registryAdmin) public {}

    modifier onlyMigrationManager {
        require(isManager(\"migrationManager\"), \"sender is not the migration manager\");

        _;
    }

    modifier onlyFunctionalManager {
        require(isManager(\"functionalManager\"), \"sender is not the functional manager\");

        _;
    }

    function refreshContracts() virtual external {}

}"},"SafeMath.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.6.0;

/**
 * @dev Wrappers over Solidity\u0027s arithmetic operations with added overflow
 * checks.
 *
 * Arithmetic operations in Solidity wrap on overflow. This can easily result
 * in bugs, because programmers usually assume that an overflow raises an
 * error, which is the standard behavior in high level programming languages.
 * `SafeMath` restores this intuition by reverting the transaction when an
 * operation overflows.
 *
 * Using this library instead of the unchecked operations eliminates an entire
 * class of bugs, so it\u0027s recommended to use it always.
 */
library SafeMath {
    /**
     * @dev Returns the addition of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity\u0027s `+` operator.
     *
     * Requirements:
     *
     * - Addition cannot overflow.
     */
    function add(uint256 a, uint256 b) internal pure returns (uint256) {
        uint256 c = a + b;
        require(c \u003e= a, \"SafeMath: addition overflow\");

        return c;
    }

    /**
     * @dev Returns the subtraction of two unsigned integers, reverting on
     * overflow (when the result is negative).
     *
     * Counterpart to Solidity\u0027s `-` operator.
     *
     * Requirements:
     *
     * - Subtraction cannot overflow.
     */
    function sub(uint256 a, uint256 b) internal pure returns (uint256) {
        return sub(a, b, \"SafeMath: subtraction overflow\");
    }

    /**
     * @dev Returns the subtraction of two unsigned integers, reverting with custom message on
     * overflow (when the result is negative).
     *
     * Counterpart to Solidity\u0027s `-` operator.
     *
     * Requirements:
     *
     * - Subtraction cannot overflow.
     */
    function sub(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b \u003c= a, errorMessage);
        uint256 c = a - b;

        return c;
    }

    /**
     * @dev Returns the multiplication of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity\u0027s `*` operator.
     *
     * Requirements:
     *
     * - Multiplication cannot overflow.
     */
    function mul(uint256 a, uint256 b) internal pure returns (uint256) {
        // Gas optimization: this is cheaper than requiring \u0027a\u0027 not being zero, but the
        // benefit is lost if \u0027b\u0027 is also tested.
        // See: https://github.com/OpenZeppelin/openzeppelin-contracts/pull/522
        if (a == 0) {
            return 0;
        }

        uint256 c = a * b;
        require(c / a == b, \"SafeMath: multiplication overflow\");

        return c;
    }

    /**
     * @dev Returns the integer division of two unsigned integers. Reverts on
     * division by zero. The result is rounded towards zero.
     *
     * Counterpart to Solidity\u0027s `/` operator. Note: this function uses a
     * `revert` opcode (which leaves remaining gas untouched) while Solidity
     * uses an invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function div(uint256 a, uint256 b) internal pure returns (uint256) {
        return div(a, b, \"SafeMath: division by zero\");
    }

    /**
     * @dev Returns the integer division of two unsigned integers. Reverts with custom message on
     * division by zero. The result is rounded towards zero.
     *
     * Counterpart to Solidity\u0027s `/` operator. Note: this function uses a
     * `revert` opcode (which leaves remaining gas untouched) while Solidity
     * uses an invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function div(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b \u003e 0, errorMessage);
        uint256 c = a / b;
        // assert(a == b * c + a % b); // There is no case in which this doesn\u0027t hold

        return c;
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
     * Reverts when dividing by zero.
     *
     * Counterpart to Solidity\u0027s `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function mod(uint256 a, uint256 b) internal pure returns (uint256) {
        return mod(a, b, \"SafeMath: modulo by zero\");
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
     * Reverts with custom message when dividing by zero.
     *
     * Counterpart to Solidity\u0027s `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function mod(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b != 0, errorMessage);
        return a % b;
    }
}
"},"SafeMath96.sol":{"content":"// SPDX-License-Identifier: UNLICENSED

pragma solidity 0.6.12;

/**
 * @dev Wrappers over Solidity\u0027s arithmetic operations with added overflow
 * checks.
 *
 * Arithmetic operations in Solidity wrap on overflow. This can easily result
 * in bugs, because programmers usually assume that an overflow raises an
 * error, which is the standard behavior in high level programming languages.
 * `SafeMath` restores this intuition by reverting the transaction when an
 * operation overflows.
 *
 * Using this library instead of the unchecked operations eliminates an entire
 * class of bugs, so it\u0027s recommended to use it always.
 */
library SafeMath96 {
    /**
     * @dev Returns the addition of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity\u0027s `+` operator.
     *
     * Requirements:
     * - Addition cannot overflow.
     */
    function add(uint96 a, uint256 b) internal pure returns (uint96) {
        require(uint256(uint96(b)) == b, \"SafeMath: addition overflow\");
        uint96 c = a + uint96(b);
        require(c \u003e= a, \"SafeMath: addition overflow\");

        return c;
    }

    /**
     * @dev Returns the subtraction of two unsigned integers, reverting on
     * overflow (when the result is negative).
     *
     * Counterpart to Solidity\u0027s `-` operator.
     *
     * Requirements:
     * - Subtraction cannot overflow.
     */
    function sub(uint96 a, uint256 b) internal pure returns (uint96) {
        require(uint256(uint96(b)) == b, \"SafeMath: subtraction overflow\");
        return sub(a, uint96(b), \"SafeMath: subtraction overflow\");
    }

    /**
     * @dev Returns the subtraction of two unsigned integers, reverting with custom message on
     * overflow (when the result is negative).
     *
     * Counterpart to Solidity\u0027s `-` operator.
     *
     * Requirements:
     * - Subtraction cannot overflow.
     *
     * _Available since v2.4.0._
     */
    function sub(uint96 a, uint96 b, string memory errorMessage) internal pure returns (uint96) {
        require(b \u003c= a, errorMessage);
        uint96 c = a - b;

        return c;
    }

}
"},"WithClaimableRegistryManagement.sol":{"content":"// SPDX-License-Identifier: UNLICENSED

pragma solidity 0.6.12;

import \"./Context.sol\";

/**
 * @title Claimable
 * @dev Extension for the Ownable contract, where the ownership needs to be claimed.
 * This allows the new owner to accept the transfer.
 */
contract WithClaimableRegistryManagement is Context {
    address private _registryAdmin;
    address private _pendingRegistryAdmin;

    event RegistryManagementTransferred(address indexed previousRegistryAdmin, address indexed newRegistryAdmin);

    /**
     * @dev Initializes the contract setting the deployer as the initial registryRegistryAdmin.
     */
    constructor () internal {
        address msgSender = _msgSender();
        _registryAdmin = msgSender;
        emit RegistryManagementTransferred(address(0), msgSender);
    }

    /**
     * @dev Returns the address of the current registryAdmin.
     */
    function registryAdmin() public view returns (address) {
        return _registryAdmin;
    }

    /**
     * @dev Throws if called by any account other than the registryAdmin.
     */
    modifier onlyRegistryAdmin() {
        require(isRegistryAdmin(), \"WithClaimableRegistryManagement: caller is not the registryAdmin\");
        _;
    }

    /**
     * @dev Returns true if the caller is the current registryAdmin.
     */
    function isRegistryAdmin() public view returns (bool) {
        return _msgSender() == _registryAdmin;
    }

    /**
     * @dev Leaves the contract without registryAdmin. It will not be possible to call
     * `onlyManager` functions anymore. Can only be called by the current registryAdmin.
     *
     * NOTE: Renouncing registryManagement will leave the contract without an registryAdmin,
     * thereby removing any functionality that is only available to the registryAdmin.
     */
    function renounceRegistryManagement() public onlyRegistryAdmin {
        emit RegistryManagementTransferred(_registryAdmin, address(0));
        _registryAdmin = address(0);
    }

    /**
     * @dev Transfers registryManagement of the contract to a new account (`newManager`).
     */
    function _transferRegistryManagement(address newRegistryAdmin) internal {
        require(newRegistryAdmin != address(0), \"RegistryAdmin: new registryAdmin is the zero address\");
        emit RegistryManagementTransferred(_registryAdmin, newRegistryAdmin);
        _registryAdmin = newRegistryAdmin;
    }

    /**
     * @dev Modifier throws if called by any account other than the pendingManager.
     */
    modifier onlyPendingRegistryAdmin() {
        require(msg.sender == _pendingRegistryAdmin, \"Caller is not the pending registryAdmin\");
        _;
    }
    /**
     * @dev Allows the current registryAdmin to set the pendingManager address.
     * @param newRegistryAdmin The address to transfer registryManagement to.
     */
    function transferRegistryManagement(address newRegistryAdmin) public onlyRegistryAdmin {
        _pendingRegistryAdmin = newRegistryAdmin;
    }

    /**
     * @dev Allows the _pendingRegistryAdmin address to finalize the transfer.
     */
    function claimRegistryManagement() external onlyPendingRegistryAdmin {
        _transferRegistryManagement(_pendingRegistryAdmin);
        _pendingRegistryAdmin = address(0);
    }

    /**
     * @dev Returns the current pendingRegistryAdmin
    */
    function pendingRegistryAdmin() public view returns (address) {
       return _pendingRegistryAdmin;  
    }
}

