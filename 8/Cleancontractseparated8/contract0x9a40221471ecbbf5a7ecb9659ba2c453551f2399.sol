// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;


/**
 * @title ERC20
 * @dev see https://github.com/ethereum/EIPs/issues/20
 */
abstract contract ERC20 {
  uint256 public totalSupply;

  function balanceOf(address who) public virtual view returns (uint256);
  function transfer(address to, uint256 value) public virtual returns (bool);
  function allowance(address owner, address spender) public virtual view returns (uint256);
  function transferFrom(address from, address to, uint256 value) public virtual returns (bool);
  function approve(address spender, uint256 value) public virtual returns (bool);

  event Approval(address indexed owner, address indexed spender, uint256 value);
  event Transfer(address indexed from, address indexed to, uint256 value);
}"},"KtlyoStaking.sol":{"content":"// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import \"./ERC20.sol\";
import \"./SafeMath.sol\";


contract KtlyoStaking {

    address public token1;
\taddress public token2;
\tuint256 public apy;
\tuint256 public duration;
\tuint256 public maxStakeAmt1;
\tuint256 public maxStakeAmt2;
    uint256 private interestRate;
\tuint256 private tokenRatio;
\tuint256 public rewardAmt1;
\tuint256 public rewardAmt2;
\tuint256 private amtRewardRemainingBalance1;
\tuint256 private amtRewardRemainingBalance2;
\tuint256 private totalStaked1;
\tuint256 private totalStaked2;
\tuint256 private totalRedeemed1;
\tuint256 private totalRedeemed2;
\tuint256 private openRewards1;
\tuint256 private openRewards2;
\taddress public owner;
    uint256 public createdAt;
\tuint256 private daysInYear;
\tuint256 private secondsInYear;
\tuint256 private precision = 1000000000000000000;
\tbool private stakingStarted;
\tstruct Transaction { 
\t\taddress wallet;
\t\taddress token;
\t\tuint256 amount;
\t\tuint256 createdAt;
\t\tbool redeemed;
\t\tuint256 rewardAmt1;
\t\tuint256 rewardAmt2;
\t\tuint256 redeemedAt;
\t\tuint256 stakeEnd;
\t}
\tmapping(address =\u003e Transaction[]) transactions;
\t
\tstruct MaxLimit { 
\t\tuint256 limit1;
\t\tuint256 limit2;
\t}
\t
\tmapping(address =\u003e bool) blackListed;
\tmapping(address =\u003e MaxLimit) limits;
\t
    modifier onlyOwner {
        require(msg.sender == owner);
        _;
    }

    constructor(
        address _token1,
\t\taddress _token2,
\t\tuint256 _apy,
\t\tuint256 _duration,
\t\tuint256 _tokenRatio,
\t\tuint256 _maxStakeAmt1,
\t\tuint256 _rewardAmt1,
        address _owner
\t\t
    ) {
        token1 = _token1;
\t\ttoken2 = _token2;
\t\tapy = _apy;
\t\tduration = _duration;
\t\ttokenRatio = _tokenRatio;
\t\tmaxStakeAmt1 = _maxStakeAmt1;
\t\tmaxStakeAmt2 = SafeMath.div(SafeMath.mul(maxStakeAmt1,tokenRatio),precision);
\t\trewardAmt1 = _rewardAmt1;
\t\trewardAmt2 = SafeMath.div(SafeMath.mul(rewardAmt1,tokenRatio),precision);
        owner = _owner;
        createdAt = block.timestamp;
\t\tstakingStarted = false;
\t\tdaysInYear = uint256(365);
\t\tsecondsInYear = daysInYear*24*60*60;
\t\tinterestRate = SafeMath.div(SafeMath.div(SafeMath.mul(apy,duration),secondsInYear),100);
\t\temit CreatedContract(token1,token2,apy,duration,maxStakeAmt1,maxStakeAmt2, rewardAmt1,rewardAmt2,msg.sender,block.timestamp,interestRate,tokenRatio);
\t\t
\t\t
    }
\t
\t// return ETH
    receive() external payable {
\t\t
        emit Reverted(msg.sender, msg.value);
\t\trevert(\"ETH is not accepted\");
    }
\t
    // return ETH
    fallback() external payable { 
      
\t   emit Reverted(msg.sender, msg.value);
\t   revert(\"ETH is not accepted\");
    }
\t
\t// callable by owner only
    function activate() onlyOwner public {
      
\t\t//check for rewards
\t\tERC20 tokenOne = ERC20(token1);
\t\tERC20 tokenTwo = ERC20(token2);
\t\tuint256 tenPerc=10;
\t\tuint256 balanceForToken1= tokenOne.balanceOf(address(this));
\t\tuint256 balanceForToken2= tokenTwo.balanceOf(address(this));
\t\tuint256 token1CheckAmount;
\t\tuint256 token2CheckAmount;
\t\tuint256 rewardBalance1;
\t\tuint256 rewardBalance2;
\t\t
\t\ttoken1CheckAmount = SafeMath.sub(balanceForToken1,totalStaked1);
\t\ttoken2CheckAmount = SafeMath.sub(balanceForToken2,totalStaked2);
\t\t
\t\trewardBalance1 = SafeMath.sub(SafeMath.sub(rewardAmt1,totalRedeemed1),openRewards1);
\t\trewardBalance2 = SafeMath.sub(SafeMath.sub(rewardAmt2,totalRedeemed2),openRewards2);
\t\t
\t\trequire (token1CheckAmount\u003e=SafeMath.div(rewardBalance1,tenPerc),\"Activation error. Insufficient balance of rewards for token1\");
\t\trequire (token2CheckAmount\u003e=SafeMath.div(rewardBalance2,tenPerc),\"Activation error. Insufficient balance of rewards for token2\");
\t\t//activate staking
\t\tstakingStarted = true;
\t\temit StartStaking(msg.sender,block.timestamp);
    }
\t
\t// callable by owner only
    function deActivate() onlyOwner public {
      
\t\t
\t\t//de-activate staking
\t\tstakingStarted = false;
\t\temit StopStaking(msg.sender,block.timestamp);
    }
\t
\t// callable by owner only
    function blackList(address[] memory addressList,bool blStatus) onlyOwner public {
\t\t
\t\tuint256 i;
\t\t
\t\tfor (i=0;i\u003caddressList.length;i++)
\t\t{
\t\t\tblackListed[addressList[i]]=blStatus;
\t\t}
\t\tif (blStatus) emit AddedToBlackList(msg.sender,block.timestamp);
\t\telse emit RemovedFromBlackList(msg.sender,block.timestamp);
    }
\t
\t
\t// function to stake
    function stake(address tokenContract, uint256 amt) public {
       
\t   uint256 amount_reward1;
\t   uint256 amount_reward2;
\t   uint256 limit1;
\t   uint256 limit2;
\t   
\t   require(stakingStarted,\"Staking not active\");
\t   require(rewardAmt1\u003eSafeMath.add(totalRedeemed1,openRewards1) \u0026\u0026 rewardAmt2\u003eSafeMath.add(totalRedeemed2,openRewards2),\"Rewards are spent. Staking contract is closed.\");
\t   require(amtRewardRemainingBalance1 \u003e 0 \u0026\u0026 amtRewardRemainingBalance2 \u003e 0,\"Staking rewards are 0\");
\t   require(tokenContract==token1 || tokenContract==token2,\"Invalid token contract\");
\t   
\t   limit1 = limits[msg.sender].limit1;
\t   limit2 = limits[msg.sender].limit2;
\t   
\t   if (token1==tokenContract) 
\t   {
\t    
\t\tif (SafeMath.add(amt,limit1)\u003emaxStakeAmt1) amt = SafeMath.sub(maxStakeAmt1,limit1);
\t\tlimits[msg.sender].limit1 = SafeMath.add(limits[msg.sender].limit1,amt);
\t\tamount_reward1 = SafeMath.div(SafeMath.mul(amt,interestRate),precision);
\t\tif (amtRewardRemainingBalance1\u003camount_reward1)
\t    {
\t\t\tamount_reward1 = amtRewardRemainingBalance1;
\t\t\tamt = SafeMath.div(SafeMath.mul(amount_reward1,precision),interestRate);
\t    }
\t\t
\t\tamount_reward2 = SafeMath.div(SafeMath.mul(amount_reward1,tokenRatio),precision);
\t\ttotalStaked1+=amt;
\t   }
\t   
\t   if (token2==tokenContract) 
\t   {
\t\tif (amt+limit2\u003emaxStakeAmt2) amt = SafeMath.sub(maxStakeAmt2,limit2);
\t\t
\t\tlimits[msg.sender].limit2 = SafeMath.add(limits[msg.sender].limit2, amt);
\t\t
\t\tamount_reward2 = SafeMath.div(SafeMath.mul(amt,interestRate),precision);
\t\t
\t\tif (amtRewardRemainingBalance2\u003camount_reward2)
\t    {
\t\t\tamount_reward2 = amtRewardRemainingBalance2;
\t\t\tamt = SafeMath.div(SafeMath.mul(amount_reward2,precision),interestRate);
\t    }
\t\tamount_reward1 = SafeMath.div(SafeMath.mul(amount_reward2,precision),tokenRatio);
\t\ttotalStaked2+=amt;
\t   }
\t   
\t   require(amt\u003e0,\"Amount is equal to 0\");
\t   
\t   
\t   amtRewardRemainingBalance1 = SafeMath.sub(amtRewardRemainingBalance1, amount_reward1,\"Insufficient rewards balance for token 1\");
\t   amtRewardRemainingBalance2 = SafeMath.sub(amtRewardRemainingBalance2, amount_reward2,\"Insufficient rewards balance for token 2\");
\t   
\t   ERC20 tokenERC20 = ERC20(tokenContract);
\t   //transfer token
\t   require(tokenERC20.transferFrom(msg.sender, address(this), amt),\"Token transfer for staking not approved!\");
\t   
\t   //create transaction
\t   Transaction memory trx = Transaction(
\t   {
\t\t   wallet : msg.sender,
\t\t   token : tokenContract,
\t\t   amount : amt,
\t\t   createdAt:block.timestamp,
\t\t   redeemed : false,
\t\t   rewardAmt1 : amount_reward1,
\t\t   rewardAmt2 : amount_reward2,
\t\t   stakeEnd: SafeMath.add(block.timestamp,duration),
\t\t   redeemedAt : 0
\t   });
\t   openRewards1+=amount_reward1;
\t   openRewards2+=amount_reward2;
\t   transactions[msg.sender].push(trx);
\t   
\t   emit Staked(msg.sender,tokenContract, amt);
    }
\tfunction redeemTrx(address requestor, uint256 indexId) 
\tinternal 
\treturns (uint256 returnAmount, uint256 returnAmount2) 
\t{
\t   
\t    
\t\t
\t\tif (transactions[requestor][indexId].token==token1)
\t\t{
\t\t\treturnAmount = transactions[requestor][indexId].amount;
\t\t\tif (transactions[requestor][indexId].stakeEnd \u003c block.timestamp \u0026\u0026 blackListed[requestor]!=true)
\t\t\t{
\t\t\t\treturnAmount = SafeMath.add(returnAmount,transactions[requestor][indexId].rewardAmt1);
\t\t\t\treturnAmount2 = transactions[requestor][indexId].rewardAmt2;
\t\t\t}
\t\t\tlimits[requestor].limit1-=transactions[requestor][indexId].amount;
\t\t}else
\t\t{
\t\t\t
\t\t\treturnAmount2 = transactions[requestor][indexId].amount;
\t\t\tif (transactions[requestor][indexId].stakeEnd \u003c block.timestamp \u0026\u0026 blackListed[requestor]!=true)
\t\t\t{
\t\t\t\treturnAmount2 = SafeMath.add(returnAmount2,transactions[requestor][indexId].rewardAmt2);
\t\t\t\treturnAmount = transactions[requestor][indexId].rewardAmt1;
\t\t\t}
\t\t\tlimits[requestor].limit2-=transactions[requestor][indexId].amount;
\t\t\t
\t\t}
\t\t
\t\ttransactions[requestor][indexId].redeemed = true;
\t\ttransactions[requestor][indexId].redeemedAt = block.timestamp;
\t\topenRewards1-=transactions[requestor][indexId].rewardAmt1;
\t\topenRewards2-=transactions[requestor][indexId].rewardAmt2;
\t\treturn (returnAmount,returnAmount2);
\t  
\t   
    }
\tfunction redeem(uint256 indexId) public {
\t   uint256 returnAmount;
\t   uint256 returnAmount2;
\t   require(transactions[msg.sender][indexId].redeemed==false \u0026\u0026 transactions[msg.sender][indexId].stakeEnd\u003cblock.timestamp ,\"Stake is already redeemed or end_date not reached\");
\t   
\t   (returnAmount,returnAmount2) = redeemTrx(msg.sender,indexId);
\t   ERC20 tokenERC20 = ERC20(token1);
\t   ERC20 tokenERC20t2 = ERC20(token2);
\t   if (returnAmount\u003e0) tokenERC20.transfer(msg.sender, returnAmount);
\t   if (returnAmount2\u003e0) tokenERC20t2.transfer(msg.sender, returnAmount2);
\t   if (transactions[msg.sender][indexId].token==token1) totalStaked1-=transactions[msg.sender][indexId].amount;
\t   else totalStaked2-=transactions[msg.sender][indexId].amount;
\t   totalRedeemed1+=transactions[msg.sender][indexId].rewardAmt1;
       totalRedeemed2+=transactions[msg.sender][indexId].rewardAmt2;
\t   emit Redeemed(msg.sender,block.timestamp, returnAmount,returnAmount2);
    }
\t
\tfunction redeemAll() public {
\t\t//check if available to redeem and transfer if available
\t\tuint256 returnAmount;
\t\tuint256 returnAmount2;
\t\tuint256 returnAmountTotal;
\t\tuint256 returnAmountTotal2;
\t\tuint256 i;
\t\tERC20 tokenERC20t2;
\t\tERC20 tokenERC20;
\t\treturnAmountTotal = 0;
\t\treturnAmountTotal2 = 0;
\t   
\t\tfor (i=0;i\u003ctransactions[msg.sender].length;i++)
\t\t{
\t\t\tif (transactions[msg.sender][i].redeemed==false \u0026\u0026 transactions[msg.sender][i].stakeEnd\u003cblock.timestamp)
\t\t\t{
\t\t\t\t(returnAmount,returnAmount2) = redeemTrx(msg.sender,i);
\t\t\t\t
\t\t\t\treturnAmountTotal = returnAmountTotal + returnAmount;
\t\t\t\treturnAmountTotal2 = returnAmountTotal2 + returnAmount2;
\t\t\t\tif (transactions[msg.sender][i].token==token1) 
\t\t\t    {
\t\t\t\t totalStaked1-=transactions[msg.sender][i].amount;
\t\t\t\t totalRedeemed1+=transactions[msg.sender][i].rewardAmt1; 
\t\t\t    }
\t\t\t    else
\t\t\t    {
\t\t\t\t totalStaked2-=transactions[msg.sender][i].amount;
\t\t\t\t totalRedeemed2+=transactions[msg.sender][i].rewardAmt2;
\t\t\t\t}
\t\t\t}
\t\t}
\t\t
\t\ttokenERC20 = ERC20(token1);
\t\tif (returnAmountTotal\u003e0) tokenERC20.transfer(msg.sender, returnAmountTotal);
\t\ttokenERC20t2 = ERC20(token2);
\t\tif (returnAmountTotal2\u003e0) tokenERC20t2.transfer(msg.sender, returnAmountTotal2);
\t\temit RedeemedAll(msg.sender,block.timestamp, returnAmountTotal,returnAmountTotal2);
\t   
    }
\t
\tfunction redeemEarly(uint256 indexId) public {
\t
       uint256 returnAmount;
\t   uint256 returnAmount2;
\t   
\t   require(transactions[msg.sender][indexId].redeemed==false,\"Stake is already redeemed\");
\t   
\t   (returnAmount,returnAmount2) = redeemTrx(msg.sender,indexId);
\t   
\t   if (transactions[msg.sender][indexId].stakeEnd\u003eblock.timestamp)
\t   {
\t\t   amtRewardRemainingBalance1 = SafeMath.add(amtRewardRemainingBalance1, transactions[msg.sender][indexId].rewardAmt1);
\t\t   amtRewardRemainingBalance2 = SafeMath.add(amtRewardRemainingBalance2, transactions[msg.sender][indexId].rewardAmt2);
\t\t   
\t\t   if (transactions[msg.sender][indexId].token==token1) totalStaked1-=transactions[msg.sender][indexId].amount;
\t\t   else 
\t\t   {
\t\t\ttotalStaked2-=transactions[msg.sender][indexId].amount;
\t\t\treturnAmount = returnAmount2;
\t\t   }
\t\t   
\t\t   ERC20 tokenERC20 = ERC20(transactions[msg.sender][indexId].token);
\t\t   if (returnAmount\u003e0) tokenERC20.transfer(msg.sender, returnAmount);
\t\t   emit EarlyRedeemed(msg.sender,block.timestamp, returnAmount);
\t   }else{
\t\t
\t\t\tERC20 tokenERC20 = ERC20(token1);
\t\t\tERC20 tokenERC20t2 = ERC20(token2);
\t\t\tif (returnAmount\u003e0) tokenERC20.transfer(msg.sender, returnAmount);
\t\t\tif (returnAmount2\u003e0) tokenERC20t2.transfer(msg.sender, returnAmount2);
\t\t\tif (transactions[msg.sender][indexId].token==token1) totalStaked1-=transactions[msg.sender][indexId].amount;
\t\t\telse totalStaked2-=transactions[msg.sender][indexId].amount;
\t\t\ttotalRedeemed1+=transactions[msg.sender][indexId].rewardAmt1;
\t\t\ttotalRedeemed2+=transactions[msg.sender][indexId].rewardAmt2;
\t\t\temit Redeemed(msg.sender,block.timestamp, returnAmount,returnAmount2);
\t   }
    }
\t
\tfunction redeemEarlyAll() public {
\t   //check if available to redeem and transfer if available
       uint256 returnAmount;
\t   uint256 returnAmount2;
\t   uint256 returnAmountTotal;
\t   uint256 returnAmountTotal2;
\t   uint i;
\t   ERC20 tokenERC20t2;
\t   ERC20 tokenERC20;
\t  
\t   for (i=0;i\u003ctransactions[msg.sender].length;i++)
\t   {
\t\t\tif (transactions[msg.sender][i].redeemed==false)
\t\t\t{
\t\t\t\t(returnAmount,returnAmount2) = redeemTrx(msg.sender,i);
\t\t\t\t
\t\t\t\treturnAmountTotal+= returnAmount;
\t\t\t\treturnAmountTotal2+= returnAmount2;
\t\t\t\t
\t\t\t\tif (transactions[msg.sender][i].stakeEnd\u003eblock.timestamp)
\t\t\t\t{
\t\t\t\t\tif (transactions[msg.sender][i].token==token1) totalStaked1-=transactions[msg.sender][i].amount;
\t\t\t\t\telse totalStaked2-=transactions[msg.sender][i].amount;
\t\t\t\t
\t\t\t\t}else{
\t\t\t\t\t
\t\t\t\t\tif (transactions[msg.sender][i].token==token1) 
\t\t\t\t\t{
\t\t\t\t\t totalStaked1-=transactions[msg.sender][i].amount;
\t\t\t\t\t totalRedeemed1+=transactions[msg.sender][i].rewardAmt1; 
\t\t\t\t\t}
\t\t\t\t\telse
\t\t\t\t\t{
\t\t\t\t\t totalStaked2-=transactions[msg.sender][i].amount;
\t\t\t\t\t totalRedeemed2+=transactions[msg.sender][i].rewardAmt2;
\t\t\t\t\t}
\t\t\t\t\t
\t\t\t\t}
\t\t\t\t
\t\t\t\t
\t\t\t}
\t   }
\t   
\t    tokenERC20 = ERC20(token1);
\t\tif (returnAmountTotal\u003e0) tokenERC20.transfer(msg.sender, returnAmountTotal);
\t\ttokenERC20t2 = ERC20(token2);
\t\tif (returnAmountTotal2\u003e0) tokenERC20t2.transfer(msg.sender, returnAmountTotal2);
\t   
\t   emit EarlyRedeemedAll(msg.sender,block.timestamp, returnAmountTotal,returnAmountTotal2);
\t\t
    }
\t
\tfunction transferReward(address token, uint256 reward_amount) public {
\t
\t   require(reward_amount\u003e0,\"Reward amount is 0\");
\t   
\t   ERC20 tokenERC20 = ERC20(token);
\t   //uint256 allowance = tokenERC20.allowance(msg.sender,address(this));
\t   //require(allowance\u003e=reward_amount,\"Transfer not approved!\");
\t   tokenERC20.transferFrom(msg.sender,address(this), reward_amount);
\t   if (token==token1) amtRewardRemainingBalance1 = SafeMath.add(amtRewardRemainingBalance1, reward_amount);
\t   if (token==token2) amtRewardRemainingBalance2 = SafeMath.add(amtRewardRemainingBalance2, reward_amount);
\t  
\t   emit TransferReward(msg.sender,block.timestamp, reward_amount);
    }
\t
\tfunction transferBackReward(address token, uint256 reward_amount) onlyOwner public {
\t
\t   require(reward_amount\u003e0 \u0026\u0026 stakingStarted==false,\"Reward amount is 0 or staking is activated\");
\t   require(openRewards1==0 \u0026\u0026 openRewards2==0,\"There are open rewards\");
\t   
\t   ERC20 tokenERC20 = ERC20(token);
\t  
\t   if (token==token1) 
\t   {
\t\tif (reward_amount\u003eSafeMath.sub(amtRewardRemainingBalance1, openRewards1)) reward_amount = SafeMath.sub(amtRewardRemainingBalance1, openRewards1);
\t\tamtRewardRemainingBalance1 = SafeMath.sub(amtRewardRemainingBalance1, reward_amount);
\t\t
\t   }
\t   if (token==token2) 
\t   {
\t\tif (reward_amount\u003eSafeMath.sub(amtRewardRemainingBalance2, openRewards2)) reward_amount = SafeMath.sub(amtRewardRemainingBalance2, openRewards2);
\t    amtRewardRemainingBalance2 = SafeMath.sub(amtRewardRemainingBalance2, reward_amount);
\t   }
\t   tokenERC20.transfer(msg.sender, reward_amount);
\t   
\t   emit TransferBackReward(msg.sender,block.timestamp, reward_amount);
    }
\t
    
    function info() public view returns(address,uint256,uint256,uint256,uint256,uint256,uint256){
        return (owner,createdAt,apy,duration,rewardAmt1,interestRate,tokenRatio);
    }
\tfunction getRewardsInfo() public view returns(address,address,uint256,uint256,uint256,uint256){ 
        return (token1,token2,rewardAmt1,rewardAmt2,amtRewardRemainingBalance1,amtRewardRemainingBalance2);
    }
\tfunction getStakeRewardAmounts() public view returns(uint256,uint256,uint256,uint256,uint256,uint256){ 
        return (totalStaked1,totalStaked2,openRewards1,openRewards2,maxStakeAmt1,maxStakeAmt2);
    }
\tfunction getMyInfo() public view returns(Transaction [] memory,MaxLimit memory){ 
        return (transactions[msg.sender],limits[msg.sender]);
    }
\tfunction getMyStakings() public view returns(Transaction [] memory){ 
        return (transactions[msg.sender]);
    }
\tfunction getStakings(address wallet) public view onlyOwner returns(Transaction [] memory){ 
        return (transactions[wallet]);
    }
\tfunction getMyLimits() public view returns(MaxLimit memory){ 
        return (limits[msg.sender]);
    }
\tfunction getBlackListedStatus(address wallet) public view returns(bool){ 
        
\t\treturn (blackListed[wallet]);
    }
\t
\tevent CreatedContract(address token1,address token2,uint256 apy,uint256 duration,uint256 maxStakeAmt1, uint256 maxStakeAmt2, uint256 rewardAmt1,uint256 rewardAmt2,address owner,uint256 createdAt,uint256 interestRate,uint256 tokenRatio);
    event Received(address from, uint256 amount);
\tevent Reverted(address from, uint256 amount);
\tevent StartStaking(address from,uint256 startDate);
\tevent StopStaking(address from,uint256 stopDate);
\tevent Staked(address from,address tokenCtr,uint256 amount);
\tevent EarlyRedeemed(address to,uint256 redeemedDate,uint256 amount);
\tevent EarlyRedeemedAll(address to,uint256 redeemedDate,uint256 amount1,uint256 amount2);
\tevent Redeemed(address to,uint256 redeemedDate,uint256 amount1,uint256 amount2);
\tevent RedeemedAll(address to,uint256 redeemedDate,uint256 amount1,uint256 amount2);
\tevent TransferReward(address from,uint256 sentDate,uint256 amount);
\tevent TransferBackReward(address to,uint256 sentDate,uint256 amount);
\tevent AddedToBlackList(address from, uint256 sentDate);
\tevent RemovedFromBlackList(address from, uint256 sentDate);
}"},"SafeMath.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

/**
 * @dev Wrappers over Solidity\u0027s arithmetic operations with added overflow
 * checks.
 *
 * Arithmetic operations in Solidity wrap on overflow. This can easily result
 * in bugs, because programmers usually assume that an overflow raises an
 * error, which is the standard behavior in high level programming languages.
 * `SafeMath` restores this intuition by reverting the transaction when an
 * operation overflows.
 *
 * Using this library instead of the unchecked operations eliminates an entire
 * class of bugs, so it\u0027s recommended to use it always.
 */
library SafeMath {
    /**
     * @dev Returns the addition of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity\u0027s `+` operator.
     *
     * Requirements:
     *
     * - Addition cannot overflow.
     */
    function add(uint256 a, uint256 b) internal pure returns (uint256) {
        uint256 c = a + b;
        require(c \u003e= a, \"SafeMath: addition overflow\");

        return c;
    }

    /**
     * @dev Returns the subtraction of two unsigned integers, reverting on
     * overflow (when the result is negative).
     *
     * Counterpart to Solidity\u0027s `-` operator.
     *
     * Requirements:
     *
     * - Subtraction cannot overflow.
     */
    function sub(uint256 a, uint256 b) internal pure returns (uint256) {
        return sub(a, b, \"SafeMath: subtraction overflow\");
    }

    /**
     * @dev Returns the subtraction of two unsigned integers, reverting with custom message on
     * overflow (when the result is negative).
     *
     * Counterpart to Solidity\u0027s `-` operator.
     *
     * Requirements:
     *
     * - Subtraction cannot overflow.
     */
    function sub(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b \u003c= a, errorMessage);
        uint256 c = a - b;

        return c;
    }

    /**
     * @dev Returns the multiplication of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity\u0027s `*` operator.
     *
     * Requirements:
     *
     * - Multiplication cannot overflow.
     */
    function mul(uint256 a, uint256 b) internal pure returns (uint256) {
        // Gas optimization: this is cheaper than requiring \u0027a\u0027 not being zero, but the
        // benefit is lost if \u0027b\u0027 is also tested.
        // See: https://github.com/OpenZeppelin/openzeppelin-contracts/pull/522
        if (a == 0) {
            return 0;
        }

        uint256 c = a * b;
        require(c / a == b, \"SafeMath: multiplication overflow\");

        return c;
    }

    /**
     * @dev Returns the integer division of two unsigned integers. Reverts on
     * division by zero. The result is rounded towards zero.
     *
     * Counterpart to Solidity\u0027s `/` operator. Note: this function uses a
     * `revert` opcode (which leaves remaining gas untouched) while Solidity
     * uses an invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function div(uint256 a, uint256 b) internal pure returns (uint256) {
        return div(a, b, \"SafeMath: division by zero\");
    }

    /**
     * @dev Returns the integer division of two unsigned integers. Reverts with custom message on
     * division by zero. The result is rounded towards zero.
     *
     * Counterpart to Solidity\u0027s `/` operator. Note: this function uses a
     * `revert` opcode (which leaves remaining gas untouched) while Solidity
     * uses an invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function div(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b \u003e 0, errorMessage);
        uint256 c = a / b;
        // assert(a == b * c + a % b); // There is no case in which this doesn\u0027t hold

        return c;
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
     * Reverts when dividing by zero.
     *
     * Counterpart to Solidity\u0027s `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function mod(uint256 a, uint256 b) internal pure returns (uint256) {
        return mod(a, b, \"SafeMath: modulo by zero\");
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
     * Reverts with custom message when dividing by zero.
     *
     * Counterpart to Solidity\u0027s `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function mod(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b != 0, errorMessage);
        return a % b;
    }
}
