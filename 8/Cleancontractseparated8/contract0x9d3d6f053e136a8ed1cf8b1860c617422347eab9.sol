  // SPDX-License-Identifier: GPL-3.0

pragma solidity ^0.8.4;

import \"./Ownable.sol\";

interface IERC20 { 
   function transfer(address recipient, uint256 amount) external returns (bool);  
} 

contract CRDNetworkAirdropV3 is Ownable { 
   
    address private admin;     
    address public token;   
    uint256 public reward;  
    mapping (address =\u003e bool) private processedRewards; 
    
    constructor(address _token, uint _reward, address _admin) Ownable() {
        token = _token; 
        reward = _reward; 
        admin = _admin;
    }  
      
    function claimReward(bytes calldata signature ) public {  
        bytes32 message = prefixed(keccak256(abi.encodePacked(msg.sender)));  
        require (recoverSigner(message, signature) == admin , \u0027Wrong signature\u0027); 
        require (processedRewards[msg.sender] == false, \u0027Reward already processed\u0027);  
        IERC20(token).transfer(msg.sender, reward); 
        processedRewards[msg.sender] = true;  
    }  
    
    function prefixed(bytes32 hash) internal pure returns (bytes32) {
        return keccak256(abi.encodePacked( \u0027\\x19Ethereum Signed Message:\
32\u0027,   hash  ));
    } 
    
    function recoverSigner(bytes32 message, bytes memory sig)  internal pure returns (address)  {
        uint8 v;
        bytes32 r;
        bytes32 s;
  
        (v, r, s) = splitSignature(sig); 
        return ecrecover(message, v, r, s);
    } 

    function splitSignature(bytes memory sig)  internal  pure  returns (uint8, bytes32, bytes32) {
        require(sig.length == 65); 
        bytes32 r;
        bytes32 s;
        uint8 v;
  
        assembly { 
            r := mload(add(sig, 32)) 
            s := mload(add(sig, 64)) 
            v := byte(0, mload(add(sig, 96)))
        } 
        return (v, r, s);
    }
     
    function checkStatus(address  _address) public view returns(bool) { 
        return (processedRewards[_address]);
    }
     
    function setReward(uint256 _reward) public onlyOwner { 
        reward = _reward; 
    }  
    
    function setToken(address _token) public onlyOwner { 
        token = _token; 
    } 
    
    function resetAdmin(address _admin) public onlyOwner { 
        admin = _admin; 
    }  
    
    function withdraw(  uint _amount, address _token) public  onlyOwner returns (bool){  
        IERC20(_token).transfer(msg.sender, _amount);
        return true;
    }  
       
}
"},"Context.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

/**
 * @dev Provides information about the current execution context, including the
 * sender of the transaction and its data. While these are generally available
 * via msg.sender and msg.data, they should not be accessed in such a direct
 * manner, since when dealing with meta-transactions the account sending and
 * paying for execution may not be the actual sender (as far as an application
 * is concerned).
 *
 * This contract is only required for intermediate, library-like contracts.
 */
abstract contract Context {
    function _msgSender() internal view virtual returns (address) {
        return msg.sender;
    }

    function _msgData() internal view virtual returns (bytes calldata) {
        return msg.data;
    }
}"},"Ownable.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import \"./Context.sol\";

/**
 * @dev Contract module which provides a basic access control mechanism, where
 * there is an account (an owner) that can be granted exclusive access to
 * specific functions.
 *
 * By default, the owner account will be the one that deploys the contract. This
 * can later be changed with {transferOwnership}.
 *
 * This module is used through inheritance. It will make available the modifier
 * `onlyOwner`, which can be applied to your functions to restrict their use to
 * the owner.
 */
abstract contract Ownable is Context {
    address private _owner;

    event OwnershipTransferred(address indexed previousOwner, address indexed newOwner);

    /**
     * @dev Initializes the contract setting the deployer as the initial owner.
     */
    constructor() {
        _setOwner(_msgSender());
    }

    /**
     * @dev Returns the address of the current owner.
     */
    function owner() public view virtual returns (address) {
        return _owner;
    }

    /**
     * @dev Throws if called by any account other than the owner.
     */
    modifier onlyOwner() {
        require(owner() == _msgSender(), \"Ownable: caller is not the owner\");
        _;
    }

    /**
     * @dev Leaves the contract without owner. It will not be possible to call
     * `onlyOwner` functions anymore. Can only be called by the current owner.
     *
     * NOTE: Renouncing ownership will leave the contract without an owner,
     * thereby removing any functionality that is only available to the owner.
     */
    function renounceOwnership() public virtual onlyOwner {
        _setOwner(address(0));
    }

    /**
     * @dev Transfers ownership of the contract to a new account (`newOwner`).
     * Can only be called by the current owner.
     */
    function transferOwnership(address newOwner) public virtual onlyOwner {
        require(newOwner != address(0), \"Ownable: new owner is the zero address\");
        _setOwner(newOwner);
    }

    function _setOwner(address newOwner) private {
        address oldOwner = _owner;
        _owner = newOwner;
        emit OwnershipTransferred(oldOwner, newOwner);
    }
}
