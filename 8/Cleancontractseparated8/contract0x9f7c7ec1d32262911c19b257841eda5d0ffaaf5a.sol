pragma solidity ^0.5.0;

import \"./IERC20.sol\";
import \"./SafeMath.sol\";

/**
 * Open Zeppelin ERC20 implementation. https://github.com/OpenZeppelin/openzeppelin-solidity/tree/master/contracts/token/ERC20
 */

/**
 * @dev Implementation of the `IERC20` interface.
 *
 * This implementation is agnostic to the way tokens are created. This means
 * that a supply mechanism has to be added in a derived contract using `_mint`.
 * For a generic mechanism see `ERC20Mintable`.
 *
 * *For a detailed writeup see our guide [How to implement supply
 * mechanisms](https://forum.zeppelin.solutions/t/how-to-implement-erc20-supply-mechanisms/226).*
 *
 * We have followed general OpenZeppelin guidelines: functions revert instead
 * of returning `false` on failure. This behavior is nonetheless conventional
 * and does not conflict with the expectations of ERC20 applications.
 *
 * Additionally, an `Approval` event is emitted on calls to `transferFrom`.
 * This allows applications to reconstruct the allowance for all accounts just
 * by listening to said events. Other implementations of the EIP may not emit
 * these events, as it isn\u0027t required by the specification.
 *
 * Finally, the non-standard `decreaseAllowance` and `increaseAllowance`
 * functions have been added to mitigate the well-known issues around setting
 * allowances. See `IERC20.approve`.
 */
contract ERC20 is IERC20 {
    using SafeMath for uint256;

    mapping (address =\u003e uint256) private _balances;

    mapping (address =\u003e mapping (address =\u003e uint256)) private _allowances;

    uint256 private _totalSupply;

    /**
     * @dev See `IERC20.totalSupply`.
     */
    function totalSupply() public view returns (uint256) {
        return _totalSupply;
    }

    /**
     * @dev See `IERC20.balanceOf`.
     */
    function balanceOf(address account) public view returns (uint256) {
        return _balances[account];
    }

    /**
     * @dev See `IERC20.transfer`.
     *
     * Requirements:
     *
     * - `recipient` cannot be the zero address.
     * - the caller must have a balance of at least `amount`.
     */
    function transfer(address recipient, uint256 amount) public returns (bool) {
        _transfer(msg.sender, recipient, amount);
        return true;
    }

    /**
     * @dev See `IERC20.allowance`.
     */
    function allowance(address owner, address spender) public view returns (uint256) {
        return _allowances[owner][spender];
    }

    /**
     * @dev See `IERC20.approve`.
     *
     * Requirements:
     *
     * - `spender` cannot be the zero address.
     */
    function approve(address spender, uint256 value) public returns (bool) {
        _approve(msg.sender, spender, value);
        return true;
    }

    /**
     * @dev See `IERC20.transferFrom`.
     *
     * Emits an `Approval` event indicating the updated allowance. This is not
     * required by the EIP. See the note at the beginning of `ERC20`;
     *
     * Requirements:
     * - `sender` and `recipient` cannot be the zero address.
     * - `sender` must have a balance of at least `value`.
     * - the caller must have allowance for `sender`\u0027s tokens of at least
     * `amount`.
     */
    function transferFrom(address sender, address recipient, uint256 amount) public returns (bool) {
        _transfer(sender, recipient, amount);
        _approve(sender, msg.sender, _allowances[sender][msg.sender].sub(amount));
        return true;
    }

    /**
     * @dev Atomically increases the allowance granted to `spender` by the caller.
     *
     * This is an alternative to `approve` that can be used as a mitigation for
     * problems described in `IERC20.approve`.
     *
     * Emits an `Approval` event indicating the updated allowance.
     *
     * Requirements:
     *
     * - `spender` cannot be the zero address.
     */
    function increaseAllowance(address spender, uint256 addedValue) public returns (bool) {
        _approve(msg.sender, spender, _allowances[msg.sender][spender].add(addedValue));
        return true;
    }

    /**
     * @dev Atomically decreases the allowance granted to `spender` by the caller.
     *
     * This is an alternative to `approve` that can be used as a mitigation for
     * problems described in `IERC20.approve`.
     *
     * Emits an `Approval` event indicating the updated allowance.
     *
     * Requirements:
     *
     * - `spender` cannot be the zero address.
     * - `spender` must have allowance for the caller of at least
     * `subtractedValue`.
     */
    function decreaseAllowance(address spender, uint256 subtractedValue) public returns (bool) {
        _approve(msg.sender, spender, _allowances[msg.sender][spender].sub(subtractedValue));
        return true;
    }

    /**
     * @dev Moves tokens `amount` from `sender` to `recipient`.
     *
     * This is internal function is equivalent to `transfer`, and can be used to
     * e.g. implement automatic token fees, slashing mechanisms, etc.
     *
     * Emits a `Transfer` event.
     *
     * Requirements:
     *
     * - `sender` cannot be the zero address.
     * - `recipient` cannot be the zero address.
     * - `sender` must have a balance of at least `amount`.
     */
    function _transfer(address sender, address recipient, uint256 amount) internal {
        require(sender != address(0), \"ERC20: transfer from the zero address\");
        require(recipient != address(0), \"ERC20: transfer to the zero address\");

        _balances[sender] = _balances[sender].sub(amount);
        _balances[recipient] = _balances[recipient].add(amount);
        emit Transfer(sender, recipient, amount);
    }

    /** @dev Creates `amount` tokens and assigns them to `account`, increasing
     * the total supply.
     *
     * Emits a `Transfer` event with `from` set to the zero address.
     *
     * Requirements
     *
     * - `to` cannot be the zero address.
     */
    function _mint(address account, uint256 amount) internal {
        require(account != address(0), \"ERC20: mint to the zero address\");

        _totalSupply = _totalSupply.add(amount);
        _balances[account] = _balances[account].add(amount);
        emit Transfer(address(0), account, amount);
    }

     /**
     * @dev Destoys `amount` tokens from `account`, reducing the
     * total supply.
     *
     * Emits a `Transfer` event with `to` set to the zero address.
     *
     * Requirements
     *
     * - `account` cannot be the zero address.
     * - `account` must have at least `amount` tokens.
     */
    function _burn(address account, uint256 value) internal {
        require(account != address(0), \"ERC20: burn from the zero address\");

        _totalSupply = _totalSupply.sub(value);
        _balances[account] = _balances[account].sub(value);
        emit Transfer(account, address(0), value);
    }

    /**
     * @dev Sets `amount` as the allowance of `spender` over the `owner`s tokens.
     *
     * This is internal function is equivalent to `approve`, and can be used to
     * e.g. set automatic allowances for certain subsystems, etc.
     *
     * Emits an `Approval` event.
     *
     * Requirements:
     *
     * - `owner` cannot be the zero address.
     * - `spender` cannot be the zero address.
     */
    function _approve(address owner, address spender, uint256 value) internal {
        require(owner != address(0), \"ERC20: approve from the zero address\");
        require(spender != address(0), \"ERC20: approve to the zero address\");

        _allowances[owner][spender] = value;
        emit Approval(owner, spender, value);
    }

    /**
     * @dev Destoys `amount` tokens from `account`.`amount` is then deducted
     * from the caller\u0027s allowance.
     *
     * See `_burn` and `_approve`.
     */
    function _burnFrom(address account, uint256 amount) internal {
        _burn(account, amount);
        _approve(account, msg.sender, _allowances[account][msg.sender].sub(amount));
    }
}"},"IERC20.sol":{"content":"pragma solidity ^0.5.0;

/**
 * Open Zeppelin ERC20 implementation. https://github.com/OpenZeppelin/openzeppelin-solidity/tree/master/contracts/token/ERC20
 */

/**
 * @dev Interface of the ERC20 standard as defined in the EIP. Does not include
 * the optional functions; to access them see `ERC20Detailed`.
 */
interface IERC20 {
    /**
     * @dev Returns the amount of tokens in existence.
     */
    function totalSupply() external view returns (uint256);

    /**
     * @dev Returns the amount of tokens owned by `account`.
     */
    function balanceOf(address account) external view returns (uint256);

    /**
     * @dev Moves `amount` tokens from the caller\u0027s account to `recipient`.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a `Transfer` event.
     */
    function transfer(address recipient, uint256 amount) external returns (bool);

    /**
     * @dev Returns the remaining number of tokens that `spender` will be
     * allowed to spend on behalf of `owner` through `transferFrom`. This is
     * zero by default.
     *
     * This value changes when `approve` or `transferFrom` are called.
     */
    function allowance(address owner, address spender) external view returns (uint256);

    /**
     * @dev Sets `amount` as the allowance of `spender` over the caller\u0027s tokens.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * \u003e Beware that changing an allowance with this method brings the risk
     * that someone may use both the old and the new allowance by unfortunate
     * transaction ordering. One possible solution to mitigate this race
     * condition is to first reduce the spender\u0027s allowance to 0 and set the
     * desired value afterwards:
     * https://github.com/ethereum/EIPs/issues/20#issuecomment-263524729
     *
     * Emits an `Approval` event.
     */
    function approve(address spender, uint256 amount) external returns (bool);

    /**
     * @dev Moves `amount` tokens from `sender` to `recipient` using the
     * allowance mechanism. `amount` is then deducted from the caller\u0027s
     * allowance.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a `Transfer` event.
     */
    function transferFrom(address sender, address recipient, uint256 amount) external returns (bool);

    /**
     * @dev Emitted when `value` tokens are moved from one account (`from`) to
     * another (`to`).
     *
     * Note that `value` may be zero.
     */
    event Transfer(address indexed from, address indexed to, uint256 value);

    /**
     * @dev Emitted when the allowance of a `spender` for an `owner` is set by
     * a call to `approve`. `value` is the new allowance.
     */
    event Approval(address indexed owner, address indexed spender, uint256 value);
}"},"SafeMath.sol":{"content":"pragma solidity ^0.5.0;

/**
 * Open Zeppelin SafeMath implementation. https://github.com/OpenZeppelin/openzeppelin-solidity/tree/master/contracts/math
 */

/**
 * @dev Wrappers over Solidity\u0027s arithmetic operations with added overflow
 * checks.
 *
 * Arithmetic operations in Solidity wrap on overflow. This can easily result
 * in bugs, because programmers usually assume that an overflow raises an
 * error, which is the standard behavior in high level programming languages.
 * `SafeMath` restores this intuition by reverting the transaction when an
 * operation overflows.
 *
 * Using this library instead of the unchecked operations eliminates an entire
 * class of bugs, so it\u0027s recommended to use it always.
 */
library SafeMath {
    /**
     * @dev Returns the addition of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity\u0027s `+` operator.
     *
     * Requirements:
     * - Addition cannot overflow.
     */
    function add(uint256 a, uint256 b) internal pure returns (uint256) {
        uint256 c = a + b;
        require(c \u003e= a, \"SafeMath: addition overflow\");

        return c;
    }

    /**
     * @dev Returns the subtraction of two unsigned integers, reverting on
     * overflow (when the result is negative).
     *
     * Counterpart to Solidity\u0027s `-` operator.
     *
     * Requirements:
     * - Subtraction cannot overflow.
     */
    function sub(uint256 a, uint256 b) internal pure returns (uint256) {
        require(b \u003c= a, \"SafeMath: subtraction overflow\");
        uint256 c = a - b;

        return c;
    }

    /**
     * @dev Returns the multiplication of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity\u0027s `*` operator.
     *
     * Requirements:
     * - Multiplication cannot overflow.
     */
    function mul(uint256 a, uint256 b) internal pure returns (uint256) {
        // Gas optimization: this is cheaper than requiring \u0027a\u0027 not being zero, but the
        // benefit is lost if \u0027b\u0027 is also tested.
        // See: https://github.com/OpenZeppelin/openzeppelin-solidity/pull/522
        if (a == 0) {
            return 0;
        }

        uint256 c = a * b;
        require(c / a == b, \"SafeMath: multiplication overflow\");

        return c;
    }

    /**
     * @dev Returns the integer division of two unsigned integers. Reverts on
     * division by zero. The result is rounded towards zero.
     *
     * Counterpart to Solidity\u0027s `/` operator. Note: this function uses a
     * `revert` opcode (which leaves remaining gas untouched) while Solidity
     * uses an invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     * - The divisor cannot be zero.
     */
    function div(uint256 a, uint256 b) internal pure returns (uint256) {
        // Solidity only automatically asserts when dividing by 0
        require(b \u003e 0, \"SafeMath: division by zero\");
        uint256 c = a / b;
        // assert(a == b * c + a % b); // There is no case in which this doesn\u0027t hold

        return c;
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
     * Reverts when dividing by zero.
     *
     * Counterpart to Solidity\u0027s `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     * - The divisor cannot be zero.
     */
    function mod(uint256 a, uint256 b) internal pure returns (uint256) {
        require(b != 0, \"SafeMath: modulo by zero\");
        return a % b;
    }
}"},"SmartInvoice.sol":{"content":"pragma solidity ^0.5.0;

import \"./IERC20.sol\";

contract SmartInvoice {
    enum Status { UNCOMMITTED, COMMITTED, SETTLED }
    function getStatusString(Status status)
    public
    pure
    returns (string memory)
    {
        if (Status.UNCOMMITTED == status) {
            return \"UNCOMMITTED\";
        }
        if (Status.COMMITTED == status) {
            return \"COMMITTED\";
        }
        if (Status.SETTLED == status) {
            return \"SETTLED\";
        }
        return \"ERROR\";
    }

    uint256 public amount;
    uint256 public dueDate;
    IERC20 public assetToken;
    address public beneficiary;
    address public payer;
    string public referenceHash;

    Status  public status;

    /**
     * @dev Constructor that gives msg.sender all of existing tokens.
     */
    constructor(uint256 _amount,
                uint256 _dueDate,
                IERC20 _assetToken,
                address _beneficiary,
                address _payer,
                string memory _referenceHash) public {
        require(_beneficiary != address(0), \"beneficiary cannot be 0x0\");
        require(_payer != address(0), \"payer cannot be 0x0\");
        amount = _amount;
        dueDate = _dueDate;
        assetToken = _assetToken;
        beneficiary = _beneficiary;
        payer = _payer;
        referenceHash = _referenceHash;

        status = Status.UNCOMMITTED;
    }

    function changeBeneficiary(address _newBeneficiary) public returns (bool) {
        require(msg.sender == beneficiary, \"caller not current beneficiary\");
        require(_newBeneficiary != address(0), \"new beneficiary cannot be 0x0\");
        require(status != Status.SETTLED, \"can not change beneficiary after settlement\");
        beneficiary = _newBeneficiary;
        return true;
    }

    function commit() public returns (bool) {
        require(msg.sender == payer, \"only payer can commit to settle\");
        require(status == Status.UNCOMMITTED, \"can only commit while status in UNCOMMITTED\");
        status = Status.COMMITTED;
        return true;
    }

    function settle() public returns (bool) {
        require(msg.sender == payer, \"only payer can settle\");
        require(status != Status.SETTLED, \"already settled\");
        require(now \u003e= dueDate, \"can only settle after due date\");
        require(assetToken.transferFrom(payer, beneficiary, amount), \"could not complete transfer\");
        status = Status.SETTLED;
        return true;
    }

}

"},"SmartInvoiceToken.sol":{"content":"pragma solidity ^0.5.0;

import \"./ERC20.sol\";
import \"./SmartInvoice.sol\";

/**
 * @title SmartInvoiceToken
 */
contract SmartInvoiceToken is ERC20 {

    SmartInvoice public smartInvoice;

    constructor(uint256 _amount,
                uint256 _dueDate,
                IERC20 _assetToken,
                address _beneficiary,
                address _payer,
                string memory _referenceHash) public {
        smartInvoice = new SmartInvoice(_amount, _dueDate, _assetToken, address(this), _payer, _referenceHash);
        _mint(_beneficiary, smartInvoice.amount());
    }

    function canRedeem() public view returns (bool) {
        return smartInvoice.status() == SmartInvoice.Status.SETTLED;
    }

    function redeem(uint256 amount) public returns (bool) {
        require(canRedeem(), \"Can only redeem after settlement\");
        require(smartInvoice.assetToken().transfer(msg.sender, amount), \"smartInvoice uses different asset token\");
        //note the internal _burn function will fail if not enough tokens to burn
        _burn(msg.sender, amount);
        return true;
    }

}
"},"SmartInvoiceWallet.sol":{"content":"pragma solidity ^0.5.0;

import \"./IERC20.sol\";
import \"./SmartInvoice.sol\";
import \"./SmartInvoiceToken.sol\";

contract SmartInvoiceWallet {
  using SafeMath for uint256;

  address public owner;
  IERC20 public assetToken;

  //internal book keeping. Needed so that we only pay invoices we know we explicitly committed to pay
  mapping(address =\u003e SmartInvoice.Status) private _smartInvoiceStatus;

  modifier isOwner() {
    require(msg.sender == owner, \"not owner\");
    _;
  }

  constructor(address _owner, IERC20 _assetToken) public {
    require(_owner != address(0), \"owner can not be 0x0\");
    require(address(_assetToken) != address(0), \"asset token can not be 0x0\");
    owner = _owner;
    assetToken = _assetToken;
  }

  function () external payable {
    require(false, \"Eth transfers not allowed\");
  }

  function balance() public view returns (uint256) {
    return this.assetToken().balanceOf(address(this));
  }

  function transfer(address to, uint256 value) external isOwner returns (bool) {
    return this.assetToken().transfer(to, value);
  }

  function invoiceTokenTransfer(SmartInvoiceToken smartInvoiceToken, address to, uint256 value) external isOwner  returns (bool) {
    return smartInvoiceToken.transfer(to, value);
  }

  function invoiceTokenBalance(SmartInvoiceToken smartInvoiceToken) public view returns (uint256) {
    require(smartInvoiceToken.smartInvoice().assetToken() == this.assetToken(), \"smartInvoice uses different asset token\");

    return smartInvoiceToken.balanceOf(address(this));
  }

  function invoiceTokenBalanceSum(SmartInvoiceToken[] memory smartInvoiceTokens) public view returns (uint256) {
    uint256 total = 0;
    for (uint32 index = 0; index \u003c smartInvoiceTokens.length; index++) {
      require(smartInvoiceTokens[index].smartInvoice().assetToken() == this.assetToken(), \"smartInvoice uses different asset token\");
      total = total.add(smartInvoiceTokens[index].balanceOf(address(this)));
    }
    return total;
  }

  //Note: owner (or their advocate) is expected to have audited what they commit to,
  // including confidence that the terms are guaranteed not to change. i.e. the smartInvoice is trusted
  function commit(SmartInvoice smartInvoice) external isOwner  returns (bool) {
    require(smartInvoice.payer() == address(this), \"not smart invoice payer\");
    require(smartInvoice.status() == SmartInvoice.Status.UNCOMMITTED, \"smart invoice already committed\");
    require(smartInvoice.assetToken() == this.assetToken(), \"smartInvoice uses different asset token\");
    require(smartInvoice.commit(), \"could not commit\");
    require(smartInvoice.status() == SmartInvoice.Status.COMMITTED, \"commit did not update status\");
    _smartInvoiceStatus[address(smartInvoice)] = SmartInvoice.Status.COMMITTED;
    return true;
  }

  function hasValidCommit(SmartInvoice smartInvoice) public view returns (bool) {
    return smartInvoice.payer() == address(this)
        \u0026\u0026 smartInvoice.status() == SmartInvoice.Status.COMMITTED
        \u0026\u0026 _smartInvoiceStatus[address(smartInvoice)] == SmartInvoice.Status.COMMITTED;
  }

  function canSettleSmartInvoice(SmartInvoice smartInvoice) public view returns (bool) {
    return hasValidCommit(smartInvoice)
        \u0026\u0026 now \u003e= smartInvoice.dueDate();
  }

  function settle(SmartInvoice smartInvoice) external returns (bool) {
    require(canSettleSmartInvoice(smartInvoice), \"settle not valid\");
    require(assetToken.approve(address(smartInvoice), smartInvoice.amount()), \"approve failed\");
    require(smartInvoice.settle(), \"settle smart invoice failed\");
    require(smartInvoice.status() == SmartInvoice.Status.SETTLED, \"settle did not update status\");
    _smartInvoiceStatus[address(smartInvoice)] = SmartInvoice.Status.SETTLED;
    return true;
  }

  function redeem(SmartInvoiceToken smartInvoiceToken) external isOwner returns (bool) {
    require(smartInvoiceToken.canRedeem(), \"redeem not valid\");
    require(smartInvoiceToken.smartInvoice().assetToken() == this.assetToken(), \"smartInvoice uses different asset token\");
    uint256 amount = this.invoiceTokenBalance(smartInvoiceToken);
    require(smartInvoiceToken.redeem(amount), \"redeem smart invoice failed\");
    return true;
  }
}


