pragma solidity ^0.4.24;

import \u0027./IERC20.sol\u0027;

contract ETToken is IERC20 {
    
    
    uint public constant _totalSupply = 93333333319000000000000000000;
    string public constant name = \"League Pay\";
    uint8 public constant decimals = 18;  
    string public constant symbol = \"LP\";               
 
    mapping (address =\u003e uint256) balances;
    mapping (address =\u003e mapping (address =\u003e uint256)) allowed;
    
    function ETToken()  {
        balances[msg.sender] = _totalSupply; 
    }
        
     function totalSupply() constant returns (uint256 totalSupply){
     return _totalSupply;
  }
    
    function balanceOf(address _owner) constant returns (uint256 balance){
        return balances[_owner];
        
    }


    function transfer(address _to, uint256 _value) public returns (bool success) {
       
        require(balances[msg.sender] \u003e= _value \u0026\u0026 balances[_to] + _value \u003e balances[_to]);
        require(_to != 0x0);
        balances[msg.sender] -= _value;
        balances[_to] += _value;
        Transfer(msg.sender, _to, _value);
        return true;
    }


    function transferFrom(address _from, address _to, uint256 _value) public returns 
    (bool success) {
        require(balances[_from] \u003e= _value \u0026\u0026 allowed[_from][msg.sender] \u003e= _value);
        balances[_to] += _value;
        balances[_from] -= _value; 
        allowed[_from][msg.sender] -= _value;
        Transfer(_from, _to, _value);
        return true;
    }


    function approve(address _spender, uint256 _value) public returns (bool success)   
    { 
        allowed[msg.sender][_spender] = _value;
        Approval(msg.sender, _spender, _value);
        return true;
    }

    function allowance(address _owner, address _spender) public constant returns (uint256 remaining) {
        return allowed[_owner][_spender];
    }

}"},"IERC20.sol":{"content":"pragma solidity ^0.4.11;

interface IERC20 {
      //function name() constant returns (string name);
      //function symbol() constant returns (string symbol);
     //function decimals() constant returns (uint8 decimals);
      //function totalSupply() constant returns (uint256 totalSupply);
      //function balanceOf(address _owner) constant returns (uint256 balance);
     // function transfer(address _to, uint _value) returns (bool success);
     // function transferFrom(address _from, address _to, uint _value) returns (bool success);
     // function approve(address _spender, uint _value) returns (bool success);
     // function allowance(address _owner, address _spender) constant returns (uint remaining);
      event Transfer(address indexed _from, address indexed _to, uint _value);
      event Approval(address indexed _owner, address indexed _spender, uint _value);
    }
