pragma solidity ^0.6.0;
pragma experimental ABIEncoderV2;

import \"./SafeMath.sol\";

contract HDS {
\tusing SafeMath for uint256;

\t/// @notice EIP-20 token name for this token
\tstring public constant name = \"Hades governance token\";

\t/// @notice EIP-20 token symbol for this token
\tstring public constant symbol = \"HDS\";

\t/// @notice EIP-20 token decimals for this token
\tuint8 public constant decimals = 8;

\t/// @notice Total number of tokens in circulation
\tuint256 public totalSupply;

\t/// @notice Max supply of tokens
\tuint256 public constant maxSupply = 21000000e8; // 21 million

\t/// @notice Allowance amounts on behalf of others
\tmapping(address =\u003e mapping(address =\u003e uint256)) internal allowances;

\t/// @notice Official record of token balances for each account
\tmapping(address =\u003e uint256) internal balances;

\t/// @notice A record of each accounts delegate
\tmapping(address =\u003e address) public delegates;

\t/// @notice A checkpoint for marking number of votes from a given block
\tstruct Checkpoint {
\t\tuint32 fromBlock;
\t\tuint256 votes;
\t}

\t/// @notice A record of votes checkpoints for each account, by index
\tmapping(address =\u003e mapping(uint32 =\u003e Checkpoint)) public checkpoints;

\t/// @notice The number of checkpoints for each account
\tmapping(address =\u003e uint32) public numCheckpoints;

\t/// @notice The admin address that have the auth to initialize the superior
\taddress public admin;

\t/// @notice The distributor address that have the auth to mint or burn tokens
\taddress public superior;

\t/// @notice The EIP-712 typehash for the contract\u0027s domain
\tbytes32 public constant DOMAIN_TYPEHASH = keccak256(
\t\t\"EIP712Domain(string name,uint256 chainId,address verifyingContract)\"
\t);

\t/// @notice The EIP-712 typehash for the delegation struct used by the contract
\tbytes32 public constant DELEGATION_TYPEHASH = keccak256(\"Delegation(address delegatee,uint256 nonce,uint256 expiry)\");

\t/// @notice A record of states for signing / validating signatures
\tmapping(address =\u003e uint256) public nonces;

\t/// @notice An event thats emitted when an account changes its delegate
\tevent DelegateChanged(address indexed delegator, address indexed fromDelegate, address indexed toDelegate);

\t/// @notice An event thats emitted when a delegate account\u0027s vote balance changes
\tevent DelegateVotesChanged(address indexed delegate, uint256 previousBalance, uint256 newBalance);

\t/// @notice The standard EIP-20 transfer event
\tevent Transfer(address indexed from, address indexed to, uint256 amount);

\t/// @notice The standard EIP-20 approval event
\tevent Approval(address indexed owner, address indexed spender, uint256 amount);

\t/// @notice For safety auditor: the superior should be the deployed MarketController contract address
\tmodifier onlySuperior {
\t\trequire(superior == msg.sender, \"HDS/permission denied\");
\t\t_;
\t}

\tconstructor() public {
\t\tadmin = msg.sender;
\t\tuint256 initialSupply = 4200000e8; // 4.2 million
\t\tbalances[admin] = initialSupply;
\t\ttotalSupply = initialSupply;
\t}

\tfunction initialize(address _superior) external {
\t\trequire(admin == msg.sender, \"HDS/permission denied\");
\t\trequire(superior == address(0), \"HDS/Already initialized\");
\t\tsuperior = _superior;
\t}

\t/**
\t * @notice Get the number of tokens `spender` is approved to spend on behalf of `account`
\t * @param account The address of the account holding the funds
\t * @param spender The address of the account spending the funds
\t * @return The number of tokens approved
\t */
\tfunction allowance(address account, address spender) external view returns (uint256) {
\t\treturn allowances[account][spender];
\t}

\t/**
\t * @notice Approve `spender` to transfer up to `amount` from `src`
\t * @dev This will overwrite the approval amount for `spender`
\t *  and is subject to issues noted [here](https://eips.ethereum.org/EIPS/eip-20#approve)
\t * @param spender The address of the account which may transfer tokens
\t * @param amount The number of tokens that are approved (2^256-1 means infinite)
\t * @return Whether or not the approval succeeded
\t */
\tfunction approve(address spender, uint256 amount) external returns (bool) {
\t\taddress owner = msg.sender;
\t\trequire(spender != address(0), \"HDS/approve to zero address\");
\t\tallowances[owner][spender] = amount;
\t\temit Approval(owner, spender, amount);
\t\treturn true;
\t}

\t/**
\t * @notice Get the number of tokens held by the `account`
\t * @param account The address of the account to get the balance of
\t * @return The number of tokens held
\t */
\tfunction balanceOf(address account) external view returns (uint256) {
\t\treturn balances[account];
\t}

\t/**
\t * @notice Transfer `amount` tokens from `msg.sender` to `dst`
\t * @param dst The address of the destination account
\t * @param amount The number of tokens to transfer
\t * @return Whether or not the transfer succeeded
\t */
\tfunction transfer(address dst, uint256 amount) external returns (bool) {
\t\treturn transferFrom(msg.sender, dst, amount);
\t}

\t/**
\t * @notice Transfer `amount` tokens from `src` to `dst`
\t * @param src The address of the source account
\t * @param dst The address of the destination account
\t * @param amount The number of tokens to transfer
\t * @return Whether or not the transfer succeeded
\t */
\tfunction transferFrom(
\t\taddress src,
\t\taddress dst,
\t\tuint256 amount
\t) public returns (bool) {
\t\trequire(balances[src] \u003e= amount, \"HDS/insufficient-balance\");
\t\trequire(src != address(0), \"HDS/transfer from zero address\");
\t\trequire(dst != address(0), \"HDS/transfer to zero address\");

\t\taddress sender = msg.sender;
\t\tuint256 allowed = allowances[src][sender];
\t\tif (sender != src \u0026\u0026 allowed != uint256(-1)) {
\t\t\trequire(allowed \u003e= amount, \"HDS/insufficient-allowance\");
\t\t\tallowances[src][sender] = allowed.sub(amount);
\t\t\temit Approval(src, sender, allowances[src][sender]);
\t\t}
\t\tbalances[src] = balances[src].sub(amount);
\t\tbalances[dst] = balances[dst].add(amount);
\t\temit Transfer(src, dst, amount);

\t\t_moveDelegates(delegates[src], delegates[dst], amount);
\t\treturn true;
\t}

\t/**
\t * @notice Mint `amount` tokens for \u0027src\u0027
\t * @param src The address to receive the mint tokens
\t * @param amount The number of tokens to mint
\t */
\tfunction mint(address src, uint256 amount) external onlySuperior {
\t\trequire(totalSupply.add(amount) \u003c= maxSupply, \"HDS/Max supply exceeded\");
\t\trequire(src != address(0), \"HDS/mint to zero address\");

\t\tbalances[src] = balances[src].add(amount);
\t\ttotalSupply = totalSupply.add(amount);
\t\temit Transfer(address(0), src, amount);
\t}

\t/**
\t * @notice Burn `amount` tokens for \u0027src\u0027
\t * @param src The address to burn tokens
\t * @param amount The number of tokens to burn
\t */
\tfunction burn(address src, uint256 amount) external {
\t\trequire(balances[src] \u003e= amount, \"HDS/insufficient-balance\");
\t\trequire(src != address(0), \"HDS/burn from zero address\");

\t\taddress sender = msg.sender;
\t\tuint256 allowed = allowances[src][sender];
\t\tif (src != sender \u0026\u0026 allowed != uint256(-1)) {
\t\t\trequire(allowed \u003e= amount, \"HDS/insufficient-allowance\");
\t\t\tallowances[src][sender] = allowed.sub(amount);
\t\t\temit Approval(src, sender, allowances[src][sender]);
\t\t}
\t\tbalances[src] = balances[src].sub(amount);
\t\ttotalSupply = totalSupply.sub(amount);
\t\temit Transfer(src, address(0), amount);
\t}

\t/**
\t * @notice Delegate votes from `msg.sender` to `delegatee`
\t * @param delegatee The address to delegate votes to
\t */
\tfunction delegate(address delegatee) public {
\t\treturn _delegate(msg.sender, delegatee);
\t}

\t/**
\t * @notice Delegates votes from signatory to `delegatee`
\t * @param delegatee The address to delegate votes to
\t * @param nonce The contract state required to match the signature
\t * @param expiry The time at which to expire the signature
\t * @param v The recovery byte of the signature
\t * @param r Half of the ECDSA signature pair
\t * @param s Half of the ECDSA signature pair
\t */
\tfunction delegateBySig(
\t\taddress delegatee,
\t\tuint256 nonce,
\t\tuint256 expiry,
\t\tuint8 v,
\t\tbytes32 r,
\t\tbytes32 s
\t) public {
\t\tbytes32 domainSeparator = keccak256(
\t\t\tabi.encode(DOMAIN_TYPEHASH, keccak256(bytes(name)), getChainId(), address(this))
\t\t);
\t\tbytes32 structHash = keccak256(abi.encode(DELEGATION_TYPEHASH, delegatee, nonce, expiry));
\t\tbytes32 digest = keccak256(abi.encodePacked(\"\\x19\\x01\", domainSeparator, structHash));
\t\taddress signatory = ecrecover(digest, v, r, s);
\t\trequire(signatory != address(0), \"HDS/ invalid signature\");
\t\trequire(nonce == nonces[signatory]++, \"HDS/ invalid nonce\");
\t\trequire(now \u003c= expiry, \"HDS/signature expired\");
\t\treturn _delegate(signatory, delegatee);
\t}

\t/**
\t * @notice Gets the current votes balance for `account`
\t * @param account The address to get votes balance
\t * @return The number of current votes for `account`
\t */
\tfunction getCurrentVotes(address account) external view returns (uint256) {
\t\tuint32 nCheckpoints = numCheckpoints[account];
\t\treturn nCheckpoints \u003e 0 ? checkpoints[account][nCheckpoints - 1].votes : 0;
\t}

\t/**
\t * @notice Determine the prior number of votes for an account as of a block number
\t * @dev Block number must be a finalized block or else this function will revert to prevent misinformation.
\t * @param account The address of the account to check
\t * @param blockNumber The block number to get the vote balance at
\t * @return The number of votes the account had as of the given block
\t */
\tfunction getPriorVotes(address account, uint256 blockNumber) public view returns (uint256) {
\t\trequire(blockNumber \u003c block.number, \"HDS/not yet determined\");

\t\tuint32 nCheckpoints = numCheckpoints[account];
\t\tif (nCheckpoints == 0) {
\t\t\treturn 0;
\t\t}

\t\t// First check most recent balance
\t\tif (checkpoints[account][nCheckpoints - 1].fromBlock \u003c= blockNumber) {
\t\t\treturn checkpoints[account][nCheckpoints - 1].votes;
\t\t}

\t\t// Next check implicit zero balance
\t\tif (checkpoints[account][0].fromBlock \u003e blockNumber) {
\t\t\treturn 0;
\t\t}

\t\tuint32 lower = 0;
\t\tuint32 upper = nCheckpoints - 1;
\t\twhile (upper \u003e lower) {
\t\t\tuint32 center = upper - (upper - lower) / 2; // ceil, avoiding overflow
\t\t\tCheckpoint memory cp = checkpoints[account][center];
\t\t\tif (cp.fromBlock == blockNumber) {
\t\t\t\treturn cp.votes;
\t\t\t} else if (cp.fromBlock \u003c blockNumber) {
\t\t\t\tlower = center;
\t\t\t} else {
\t\t\t\tupper = center - 1;
\t\t\t}
\t\t}
\t\treturn checkpoints[account][lower].votes;
\t}

\tfunction _delegate(address delegator, address delegatee) internal {
\t\taddress currentDelegate = delegates[delegator];
\t\tdelegates[delegator] = delegatee;
\t\temit DelegateChanged(delegator, currentDelegate, delegatee);
\t\t_moveDelegates(currentDelegate, delegatee, balances[delegator]);
\t}

\tfunction _moveDelegates(
\t\taddress srcRep,
\t\taddress dstRep,
\t\tuint256 amount
\t) internal {
\t\tif (srcRep != dstRep \u0026\u0026 amount \u003e 0) {
\t\t\tif (srcRep != address(0)) {
\t\t\t\tuint32 srcRepNum = numCheckpoints[srcRep];
\t\t\t\tuint256 srcRepOld = srcRepNum \u003e 0 ? checkpoints[srcRep][srcRepNum - 1].votes : 0;
\t\t\t\tuint256 srcRepNew = srcRepOld.sub(amount);
\t\t\t\t_writeCheckpoint(srcRep, srcRepNum, srcRepOld, srcRepNew);
\t\t\t}

\t\t\tif (dstRep != address(0)) {
\t\t\t\tuint32 dstRepNum = numCheckpoints[dstRep];
\t\t\t\tuint256 dstRepOld = dstRepNum \u003e 0 ? checkpoints[dstRep][dstRepNum - 1].votes : 0;
\t\t\t\tuint256 dstRepNew = dstRepOld.add(amount);
\t\t\t\t_writeCheckpoint(dstRep, dstRepNum, dstRepOld, dstRepNew);
\t\t\t}
\t\t}
\t}

\tfunction _writeCheckpoint(
\t\taddress delegatee,
\t\tuint32 nCheckpoints,
\t\tuint256 oldVotes,
\t\tuint256 newVotes
\t) internal {
\t\tuint32 blockNumber = safe32(block.number, \"HDS/Block number overflow\");

\t\tif (nCheckpoints \u003e 0 \u0026\u0026 checkpoints[delegatee][nCheckpoints - 1].fromBlock == blockNumber) {
\t\t\tcheckpoints[delegatee][nCheckpoints - 1].votes = newVotes;
\t\t} else {
\t\t\tcheckpoints[delegatee][nCheckpoints] = Checkpoint(blockNumber, newVotes);
\t\t\tnumCheckpoints[delegatee] = nCheckpoints + 1;
\t\t}

\t\temit DelegateVotesChanged(delegatee, oldVotes, newVotes);
\t}

\tfunction safe32(uint256 n, string memory errorMessage) internal pure returns (uint32) {
\t\trequire(n \u003c 2**32, errorMessage);
\t\treturn uint32(n);
\t}

\tfunction getChainId() internal pure returns (uint256) {
\t\tuint256 chainId;
\t\tassembly {
\t\t\tchainId := chainid()
\t\t}
\t\treturn chainId;
\t}
}
"},"SafeMath.sol":{"content":"pragma solidity ^0.6.0;

// From https://github.com/OpenZeppelin/openzeppelin-contracts/blob/master/contracts/math/Math.sol
// Subject to the MIT license.

/**
 * @dev Wrappers over Solidity\u0027s arithmetic operations with added overflow
 * checks.
 *
 * Arithmetic operations in Solidity wrap on overflow. This can easily result
 * in bugs, because programmers usually assume that an overflow raises an
 * error, which is the standard behavior in high level programming languages.
 * `SafeMath` restores this intuition by reverting the transaction when an
 * operation overflows.
 *
 * Using this library instead of the unchecked operations eliminates an entire
 * class of bugs, so it\u0027s recommended to use it always.
 */
library SafeMath {
\t/**
\t * @dev Returns the addition of two unsigned integers, reverting on overflow.
\t *
\t * Counterpart to Solidity\u0027s `+` operator.
\t *
\t * Requirements:
\t * - Addition cannot overflow.
\t */
\tfunction add(uint256 a, uint256 b) internal pure returns (uint256) {
\t\tuint256 c = a + b;
\t\trequire(c \u003e= a, \"SafeMath: addition overflow\");

\t\treturn c;
\t}

\t/**
\t * @dev Returns the addition of two unsigned integers, reverting with custom message on overflow.
\t *
\t * Counterpart to Solidity\u0027s `+` operator.
\t *
\t * Requirements:
\t * - Addition cannot overflow.
\t */
\tfunction add(
\t\tuint256 a,
\t\tuint256 b,
\t\tstring memory errorMessage
\t) internal pure returns (uint256) {
\t\tuint256 c = a + b;
\t\trequire(c \u003e= a, errorMessage);

\t\treturn c;
\t}

\t/**
\t * @dev Returns the subtraction of two unsigned integers, reverting on underflow (when the result is negative).
\t *
\t * Counterpart to Solidity\u0027s `-` operator.
\t *
\t * Requirements:
\t * - Subtraction cannot underflow.
\t */
\tfunction sub(uint256 a, uint256 b) internal pure returns (uint256) {
\t\treturn sub(a, b, \"SafeMath: subtraction underflow\");
\t}

\t/**
\t * @dev Returns the subtraction of two unsigned integers, reverting with custom message on underflow (when the result is negative).
\t *
\t * Counterpart to Solidity\u0027s `-` operator.
\t *
\t * Requirements:
\t * - Subtraction cannot underflow.
\t */
\tfunction sub(
\t\tuint256 a,
\t\tuint256 b,
\t\tstring memory errorMessage
\t) internal pure returns (uint256) {
\t\trequire(b \u003c= a, errorMessage);
\t\tuint256 c = a - b;

\t\treturn c;
\t}

\t/**
\t * @dev Returns the multiplication of two unsigned integers, reverting on overflow.
\t *
\t * Counterpart to Solidity\u0027s `*` operator.
\t *
\t * Requirements:
\t * - Multiplication cannot overflow.
\t */
\tfunction mul(uint256 a, uint256 b) internal pure returns (uint256) {
\t\t// Gas optimization: this is cheaper than requiring \u0027a\u0027 not being zero, but the
\t\t// benefit is lost if \u0027b\u0027 is also tested.
\t\t// See: https://github.com/OpenZeppelin/openzeppelin-contracts/pull/522
\t\tif (a == 0) {
\t\t\treturn 0;
\t\t}

\t\tuint256 c = a * b;
\t\trequire(c / a == b, \"SafeMath: multiplication overflow\");

\t\treturn c;
\t}

\t/**
\t * @dev Returns the multiplication of two unsigned integers, reverting on overflow.
\t *
\t * Counterpart to Solidity\u0027s `*` operator.
\t *
\t * Requirements:
\t * - Multiplication cannot overflow.
\t */
\tfunction mul(
\t\tuint256 a,
\t\tuint256 b,
\t\tstring memory errorMessage
\t) internal pure returns (uint256) {
\t\t// Gas optimization: this is cheaper than requiring \u0027a\u0027 not being zero, but the
\t\t// benefit is lost if \u0027b\u0027 is also tested.
\t\t// See: https://github.com/OpenZeppelin/openzeppelin-contracts/pull/522
\t\tif (a == 0) {
\t\t\treturn 0;
\t\t}

\t\tuint256 c = a * b;
\t\trequire(c / a == b, errorMessage);

\t\treturn c;
\t}

\t/**
\t * @dev Returns the integer division of two unsigned integers.
\t * Reverts on division by zero. The result is rounded towards zero.
\t *
\t * Counterpart to Solidity\u0027s `/` operator. Note: this function uses a
\t * `revert` opcode (which leaves remaining gas untouched) while Solidity
\t * uses an invalid opcode to revert (consuming all remaining gas).
\t *
\t * Requirements:
\t * - The divisor cannot be zero.
\t */
\tfunction div(uint256 a, uint256 b) internal pure returns (uint256) {
\t\treturn div(a, b, \"SafeMath: division by zero\");
\t}

\t/**
\t * @dev Returns the integer division of two unsigned integers.
\t * Reverts with custom message on division by zero. The result is rounded towards zero.
\t *
\t * Counterpart to Solidity\u0027s `/` operator. Note: this function uses a
\t * `revert` opcode (which leaves remaining gas untouched) while Solidity
\t * uses an invalid opcode to revert (consuming all remaining gas).
\t *
\t * Requirements:
\t * - The divisor cannot be zero.
\t */
\tfunction div(
\t\tuint256 a,
\t\tuint256 b,
\t\tstring memory errorMessage
\t) internal pure returns (uint256) {
\t\t// Solidity only automatically asserts when dividing by 0
\t\trequire(b \u003e 0, errorMessage);
\t\tuint256 c = a / b;
\t\t// assert(a == b * c + a % b); // There is no case in which this doesn\u0027t hold

\t\treturn c;
\t}

\t/**
\t * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
\t * Reverts when dividing by zero.
\t *
\t * Counterpart to Solidity\u0027s `%` operator. This function uses a `revert`
\t * opcode (which leaves remaining gas untouched) while Solidity uses an
\t * invalid opcode to revert (consuming all remaining gas).
\t *
\t * Requirements:
\t * - The divisor cannot be zero.
\t */
\tfunction mod(uint256 a, uint256 b) internal pure returns (uint256) {
\t\treturn mod(a, b, \"SafeMath: modulo by zero\");
\t}

\t/**
\t * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
\t * Reverts with custom message when dividing by zero.
\t *
\t * Counterpart to Solidity\u0027s `%` operator. This function uses a `revert`
\t * opcode (which leaves remaining gas untouched) while Solidity uses an
\t * invalid opcode to revert (consuming all remaining gas).
\t *
\t * Requirements:
\t * - The divisor cannot be zero.
\t */
\tfunction mod(
\t\tuint256 a,
\t\tuint256 b,
\t\tstring memory errorMessage
\t) internal pure returns (uint256) {
\t\trequire(b != 0, errorMessage);
\t\treturn a % b;
\t}
}

