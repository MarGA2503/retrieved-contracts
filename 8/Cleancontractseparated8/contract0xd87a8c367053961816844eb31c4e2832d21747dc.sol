



// SPDX-License-Identifier: MIT

// solhint-disable-next-line compiler-version
pragma solidity >=0.4.24 <0.8.0;

import \"../utils/AddressUpgradeable.sol\";

/**
 * @dev This is a base contract to aid in writing upgradeable contracts, or any kind of contract that will be deployed
 * behind a proxy. Since a proxied contract can't have a constructor, it's common to move constructor logic to an
 * external initializer function, usually called `initialize`. It then becomes necessary to protect this initializer
 * function so it can only be called once. The {initializer} modifier provided by this contract will have this effect.
 *
 * TIP: To avoid leaving the proxy in an uninitialized state, the initializer function should be called as early as
 * possible by providing the encoded function call as the `_data` argument to {UpgradeableProxy-constructor}.
 *
 * CAUTION: When used with inheritance, manual care must be taken to not invoke a parent initializer twice, or to ensure
 * that all initializers are idempotent. This is not verified automatically as constructors are by Solidity.
 */
abstract contract Initializable {

    /**
     * @dev Indicates that the contract has been initialized.
     */
    bool private _initialized;

    /**
     * @dev Indicates that the contract is in the process of being initialized.
     */
    bool private _initializing;

    /**
     * @dev Modifier to protect an initializer function from being invoked twice.
     */
    modifier initializer() {
        require(_initializing || _isConstructor() || !_initialized, \"Initializable: contract is already initialized\");

        bool isTopLevelCall = !_initializing;
        if (isTopLevelCall) {
            _initializing = true;
            _initialized = true;
        }

        _;

        if (isTopLevelCall) {
            _initializing = false;
        }
    }

    /// @dev Returns true if and only if the function is running in the constructor
    function _isConstructor() private view returns (bool) {
        return !AddressUpgradeable.isContract(address(this));
    }
}
"
    
// SPDX-License-Identifier: MIT

pragma solidity ^0.7.0;

/**
 * @dev Collection of functions related to the address type
 */
library AddressUpgradeable {
    /**
     * @dev Returns true if `account` is a contract.
     *
     * [IMPORTANT]
     * ====
     * It is unsafe to assume that an address for which this function returns
     * false is an externally-owned account (EOA) and not a contract.
     *
     * Among others, `isContract` will return false for the following
     * types of addresses:
     *
     *  - an externally-owned account
     *  - a contract in construction
     *  - an address where a contract will be created
     *  - an address where a contract lived, but was destroyed
     * ====
     */
    function isContract(address account) internal view returns (bool) {
        // This method relies on extcodesize, which returns 0 for contracts in
        // construction, since the code is only stored at the end of the
        // constructor execution.

        uint256 size;
        // solhint-disable-next-line no-inline-assembly
        assembly { size := extcodesize(account) }
        return size > 0;
    }

    /**
     * @dev Replacement for Solidity's `transfer`: sends `amount` wei to
     * `recipient`, forwarding all available gas and reverting on errors.
     *
     * https://eips.ethereum.org/EIPS/eip-1884[EIP1884] increases the gas cost
     * of certain opcodes, possibly making contracts go over the 2300 gas limit
     * imposed by `transfer`, making them unable to receive funds via
     * `transfer`. {sendValue} removes this limitation.
     *
     * https://diligence.consensys.net/posts/2019/09/stop-using-soliditys-transfer-now/[Learn more].
     *
     * IMPORTANT: because control is transferred to `recipient`, care must be
     * taken to not create reentrancy vulnerabilities. Consider using
     * {ReentrancyGuard} or the
     * https://solidity.readthedocs.io/en/v0.5.11/security-considerations.html#use-the-checks-effects-interactions-pattern[checks-effects-interactions pattern].
     */
    function sendValue(address payable recipient, uint256 amount) internal {
        require(address(this).balance >= amount, \"Address: insufficient balance\");

        // solhint-disable-next-line avoid-low-level-calls, avoid-call-value
        (bool success, ) = recipient.call{ value: amount }(\"\");
        require(success, \"Address: unable to send value, recipient may have reverted\");
    }

    /**
     * @dev Performs a Solidity function call using a low level `call`. A
     * plain`call` is an unsafe replacement for a function call: use this
     * function instead.
     *
     * If `target` reverts with a revert reason, it is bubbled up by this
     * function (like regular Solidity function calls).
     *
     * Returns the raw returned data. To convert to the expected return value,
     * use https://solidity.readthedocs.io/en/latest/units-and-global-variables.html?highlight=abi.decode#abi-encoding-and-decoding-functions[`abi.decode`].
     *
     * Requirements:
     *
     * - `target` must be a contract.
     * - calling `target` with `data` must not revert.
     *
     * _Available since v3.1._
     */
    function functionCall(address target, bytes memory data) internal returns (bytes memory) {
      return functionCall(target, data, \"Address: low-level call failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`], but with
     * `errorMessage` as a fallback revert reason when `target` reverts.
     *
     * _Available since v3.1._
     */
    function functionCall(address target, bytes memory data, string memory errorMessage) internal returns (bytes memory) {
        return functionCallWithValue(target, data, 0, errorMessage);
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`],
     * but also transferring `value` wei to `target`.
     *
     * Requirements:
     *
     * - the calling contract must have an ETH balance of at least `value`.
     * - the called Solidity function must be `payable`.
     *
     * _Available since v3.1._
     */
    function functionCallWithValue(address target, bytes memory data, uint256 value) internal returns (bytes memory) {
        return functionCallWithValue(target, data, value, \"Address: low-level call with value failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCallWithValue-address-bytes-uint256-}[`functionCallWithValue`], but
     * with `errorMessage` as a fallback revert reason when `target` reverts.
     *
     * _Available since v3.1._
     */
    function functionCallWithValue(address target, bytes memory data, uint256 value, string memory errorMessage) internal returns (bytes memory) {
        require(address(this).balance >= value, \"Address: insufficient balance for call\");
        require(isContract(target), \"Address: call to non-contract\");

        // solhint-disable-next-line avoid-low-level-calls
        (bool success, bytes memory returndata) = target.call{ value: value }(data);
        return _verifyCallResult(success, returndata, errorMessage);
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`],
     * but performing a static call.
     *
     * _Available since v3.3._
     */
    function functionStaticCall(address target, bytes memory data) internal view returns (bytes memory) {
        return functionStaticCall(target, data, \"Address: low-level static call failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-string-}[`functionCall`],
     * but performing a static call.
     *
     * _Available since v3.3._
     */
    function functionStaticCall(address target, bytes memory data, string memory errorMessage) internal view returns (bytes memory) {
        require(isContract(target), \"Address: static call to non-contract\");

        // solhint-disable-next-line avoid-low-level-calls
        (bool success, bytes memory returndata) = target.staticcall(data);
        return _verifyCallResult(success, returndata, errorMessage);
    }

    function _verifyCallResult(bool success, bytes memory returndata, string memory errorMessage) private pure returns(bytes memory) {
        if (success) {
            return returndata;
        } else {
            // Look for revert reason and bubble it up if present
            if (returndata.length > 0) {
                // The easiest way to bubble the revert reason is using memory via assembly

                // solhint-disable-next-line no-inline-assembly
                assembly {
                    let returndata_size := mload(returndata)
                    revert(add(32, returndata), returndata_size)
                }
            } else {
                revert(errorMessage);
            }
        }
    }
}
"
    
// SPDX-License-Identifier: MIT

pragma solidity >=0.6.0 <0.8.0;
import \"../proxy/Initializable.sol\";

/*
 * @dev Provides information about the current execution context, including the
 * sender of the transaction and its data. While these are generally available
 * via msg.sender and msg.data, they should not be accessed in such a direct
 * manner, since when dealing with GSN meta-transactions the account sending and
 * paying for execution may not be the actual sender (as far as an application
 * is concerned).
 *
 * This contract is only required for intermediate, library-like contracts.
 */
abstract contract ContextUpgradeable is Initializable {
    function __Context_init() internal initializer {
        __Context_init_unchained();
    }

    function __Context_init_unchained() internal initializer {
    }
    function _msgSender() internal view virtual returns (address payable) {
        return msg.sender;
    }

    function _msgData() internal view virtual returns (bytes memory) {
        this; // silence state mutability warning without generating bytecode - see https://github.com/ethereum/solidity/issues/2691
        return msg.data;
    }
    uint256[50] private __gap;
}
"
    
// SPDX-License-Identifier: MIT

pragma solidity ^0.7.0;

import \"./ContextUpgradeable.sol\";
import \"../proxy/Initializable.sol\";

/**
 * @dev Contract module which allows children to implement an emergency stop
 * mechanism that can be triggered by an authorized account.
 *
 * This module is used through inheritance. It will make available the
 * modifiers `whenNotPaused` and `whenPaused`, which can be applied to
 * the functions of your contract. Note that they will not be pausable by
 * simply including this module, only once the modifiers are put in place.
 */
abstract contract PausableUpgradeable is Initializable, ContextUpgradeable {
    /**
     * @dev Emitted when the pause is triggered by `account`.
     */
    event Paused(address account);

    /**
     * @dev Emitted when the pause is lifted by `account`.
     */
    event Unpaused(address account);

    bool private _paused;

    /**
     * @dev Initializes the contract in unpaused state.
     */
    function __Pausable_init() internal initializer {
        __Context_init_unchained();
        __Pausable_init_unchained();
    }

    function __Pausable_init_unchained() internal initializer {
        _paused = false;
    }

    /**
     * @dev Returns true if the contract is paused, and false otherwise.
     */
    function paused() public view virtual returns (bool) {
        return _paused;
    }

    /**
     * @dev Modifier to make a function callable only when the contract is not paused.
     *
     * Requirements:
     *
     * - The contract must not be paused.
     */
    modifier whenNotPaused() {
        require(!paused(), \"Pausable: paused\");
        _;
    }

    /**
     * @dev Modifier to make a function callable only when the contract is paused.
     *
     * Requirements:
     *
     * - The contract must be paused.
     */
    modifier whenPaused() {
        require(paused(), \"Pausable: not paused\");
        _;
    }

    /**
     * @dev Triggers stopped state.
     *
     * Requirements:
     *
     * - The contract must not be paused.
     */
    function _pause() internal virtual whenNotPaused {
        _paused = true;
        emit Paused(_msgSender());
    }

    /**
     * @dev Returns to normal state.
     *
     * Requirements:
     *
     * - The contract must be paused.
     */
    function _unpause() internal virtual whenPaused {
        _paused = false;
        emit Unpaused(_msgSender());
    }
    uint256[49] private __gap;
}
"
    
// SPDX-License-Identifier: MIT

pragma solidity ^0.7.0;

/**
 * @dev Wrappers over Solidity's arithmetic operations with added overflow
 * checks.
 *
 * Arithmetic operations in Solidity wrap on overflow. This can easily result
 * in bugs, because programmers usually assume that an overflow raises an
 * error, which is the standard behavior in high level programming languages.
 * `SafeMath` restores this intuition by reverting the transaction when an
 * operation overflows.
 *
 * Using this library instead of the unchecked operations eliminates an entire
 * class of bugs, so it's recommended to use it always.
 */
library SafeMath {
    /**
     * @dev Returns the addition of two unsigned integers, with an overflow flag.
     *
     * _Available since v3.4._
     */
    function tryAdd(uint256 a, uint256 b) internal pure returns (bool, uint256) {
        uint256 c = a + b;
        if (c < a) return (false, 0);
        return (true, c);
    }

    /**
     * @dev Returns the substraction of two unsigned integers, with an overflow flag.
     *
     * _Available since v3.4._
     */
    function trySub(uint256 a, uint256 b) internal pure returns (bool, uint256) {
        if (b > a) return (false, 0);
        return (true, a - b);
    }

    /**
     * @dev Returns the multiplication of two unsigned integers, with an overflow flag.
     *
     * _Available since v3.4._
     */
    function tryMul(uint256 a, uint256 b) internal pure returns (bool, uint256) {
        // Gas optimization: this is cheaper than requiring 'a' not being zero, but the
        // benefit is lost if 'b' is also tested.
        // See: https://github.com/OpenZeppelin/openzeppelin-contracts/pull/522
        if (a == 0) return (true, 0);
        uint256 c = a * b;
        if (c / a != b) return (false, 0);
        return (true, c);
    }

    /**
     * @dev Returns the division of two unsigned integers, with a division by zero flag.
     *
     * _Available since v3.4._
     */
    function tryDiv(uint256 a, uint256 b) internal pure returns (bool, uint256) {
        if (b == 0) return (false, 0);
        return (true, a / b);
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers, with a division by zero flag.
     *
     * _Available since v3.4._
     */
    function tryMod(uint256 a, uint256 b) internal pure returns (bool, uint256) {
        if (b == 0) return (false, 0);
        return (true, a % b);
    }

    /**
     * @dev Returns the addition of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity's `+` operator.
     *
     * Requirements:
     *
     * - Addition cannot overflow.
     */
    function add(uint256 a, uint256 b) internal pure returns (uint256) {
        uint256 c = a + b;
        require(c >= a, \"SafeMath: addition overflow\");
        return c;
    }

    /**
     * @dev Returns the subtraction of two unsigned integers, reverting on
     * overflow (when the result is negative).
     *
     * Counterpart to Solidity's `-` operator.
     *
     * Requirements:
     *
     * - Subtraction cannot overflow.
     */
    function sub(uint256 a, uint256 b) internal pure returns (uint256) {
        require(b <= a, \"SafeMath: subtraction overflow\");
        return a - b;
    }

    /**
     * @dev Returns the multiplication of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity's `*` operator.
     *
     * Requirements:
     *
     * - Multiplication cannot overflow.
     */
    function mul(uint256 a, uint256 b) internal pure returns (uint256) {
        if (a == 0) return 0;
        uint256 c = a * b;
        require(c / a == b, \"SafeMath: multiplication overflow\");
        return c;
    }

    /**
     * @dev Returns the integer division of two unsigned integers, reverting on
     * division by zero. The result is rounded towards zero.
     *
     * Counterpart to Solidity's `/` operator. Note: this function uses a
     * `revert` opcode (which leaves remaining gas untouched) while Solidity
     * uses an invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function div(uint256 a, uint256 b) internal pure returns (uint256) {
        require(b > 0, \"SafeMath: division by zero\");
        return a / b;
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
     * reverting when dividing by zero.
     *
     * Counterpart to Solidity's `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function mod(uint256 a, uint256 b) internal pure returns (uint256) {
        require(b > 0, \"SafeMath: modulo by zero\");
        return a % b;
    }

    /**
     * @dev Returns the subtraction of two unsigned integers, reverting with custom message on
     * overflow (when the result is negative).
     *
     * CAUTION: This function is deprecated because it requires allocating memory for the error
     * message unnecessarily. For custom revert reasons use {trySub}.
     *
     * Counterpart to Solidity's `-` operator.
     *
     * Requirements:
     *
     * - Subtraction cannot overflow.
     */
    function sub(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b <= a, errorMessage);
        return a - b;
    }

    /**
     * @dev Returns the integer division of two unsigned integers, reverting with custom message on
     * division by zero. The result is rounded towards zero.
     *
     * CAUTION: This function is deprecated because it requires allocating memory for the error
     * message unnecessarily. For custom revert reasons use {tryDiv}.
     *
     * Counterpart to Solidity's `/` operator. Note: this function uses a
     * `revert` opcode (which leaves remaining gas untouched) while Solidity
     * uses an invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function div(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b > 0, errorMessage);
        return a / b;
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
     * reverting with custom message when dividing by zero.
     *
     * CAUTION: This function is deprecated because it requires allocating memory for the error
     * message unnecessarily. For custom revert reasons use {tryMod}.
     *
     * Counterpart to Solidity's `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function mod(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b > 0, errorMessage);
        return a % b;
    }
}
"
    
// SPDX-License-Identifier: MIT

pragma solidity ^0.7.0;

/**
 * @dev Interface of the ERC20 standard as defined in the EIP.
 */
interface IERC20 {
    /**
     * @dev Returns the amount of tokens in existence.
     */
    function totalSupply() external view returns (uint256);

    /**
     * @dev Returns the amount of tokens owned by `account`.
     */
    function balanceOf(address account) external view returns (uint256);

    /**
     * @dev Moves `amount` tokens from the caller's account to `recipient`.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transfer(address recipient, uint256 amount) external returns (bool);

    /**
     * @dev Returns the remaining number of tokens that `spender` will be
     * allowed to spend on behalf of `owner` through {transferFrom}. This is
     * zero by default.
     *
     * This value changes when {approve} or {transferFrom} are called.
     */
    function allowance(address owner, address spender) external view returns (uint256);

    /**
     * @dev Sets `amount` as the allowance of `spender` over the caller's tokens.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * IMPORTANT: Beware that changing an allowance with this method brings the risk
     * that someone may use both the old and the new allowance by unfortunate
     * transaction ordering. One possible solution to mitigate this race
     * condition is to first reduce the spender's allowance to 0 and set the
     * desired value afterwards:
     * https://github.com/ethereum/EIPs/issues/20#issuecomment-263524729
     *
     * Emits an {Approval} event.
     */
    function approve(address spender, uint256 amount) external returns (bool);

    /**
     * @dev Moves `amount` tokens from `sender` to `recipient` using the
     * allowance mechanism. `amount` is then deducted from the caller's
     * allowance.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transferFrom(address sender, address recipient, uint256 amount) external returns (bool);

    /**
     * @dev Emitted when `value` tokens are moved from one account (`from`) to
     * another (`to`).
     *
     * Note that `value` may be zero.
     */
    event Transfer(address indexed from, address indexed to, uint256 value);

    /**
     * @dev Emitted when the allowance of a `spender` for an `owner` is set by
     * a call to {approve}. `value` is the new allowance.
     */
    event Approval(address indexed owner, address indexed spender, uint256 value);
}
"
    
// SPDX-License-Identifier: MIT

pragma solidity ^0.7.0;

import \"./IERC20.sol\";
import \"../../math/SafeMath.sol\";
import \"../../utils/Address.sol\";

/**
 * @title SafeERC20
 * @dev Wrappers around ERC20 operations that throw on failure (when the token
 * contract returns false). Tokens that return no value (and instead revert or
 * throw on failure) are also supported, non-reverting calls are assumed to be
 * successful.
 * To use this library you can add a `using SafeERC20 for IERC20;` statement to your contract,
 * which allows you to call the safe operations as `token.safeTransfer(...)`, etc.
 */
library SafeERC20 {
    using SafeMath for uint256;
    using Address for address;

    function safeTransfer(IERC20 token, address to, uint256 value) internal {
        _callOptionalReturn(token, abi.encodeWithSelector(token.transfer.selector, to, value));
    }

    function safeTransferFrom(IERC20 token, address from, address to, uint256 value) internal {
        _callOptionalReturn(token, abi.encodeWithSelector(token.transferFrom.selector, from, to, value));
    }

    /**
     * @dev Deprecated. This function has issues similar to the ones found in
     * {IERC20-approve}, and its usage is discouraged.
     *
     * Whenever possible, use {safeIncreaseAllowance} and
     * {safeDecreaseAllowance} instead.
     */
    function safeApprove(IERC20 token, address spender, uint256 value) internal {
        // safeApprove should only be called when setting an initial allowance,
        // or when resetting it to zero. To increase and decrease it, use
        // 'safeIncreaseAllowance' and 'safeDecreaseAllowance'
        // solhint-disable-next-line max-line-length
        require((value == 0) || (token.allowance(address(this), spender) == 0),
            \"SafeERC20: approve from non-zero to non-zero allowance\"
        );
        _callOptionalReturn(token, abi.encodeWithSelector(token.approve.selector, spender, value));
    }

    function safeIncreaseAllowance(IERC20 token, address spender, uint256 value) internal {
        uint256 newAllowance = token.allowance(address(this), spender).add(value);
        _callOptionalReturn(token, abi.encodeWithSelector(token.approve.selector, spender, newAllowance));
    }

    function safeDecreaseAllowance(IERC20 token, address spender, uint256 value) internal {
        uint256 newAllowance = token.allowance(address(this), spender).sub(value, \"SafeERC20: decreased allowance below zero\");
        _callOptionalReturn(token, abi.encodeWithSelector(token.approve.selector, spender, newAllowance));
    }

    /**
     * @dev Imitates a Solidity high-level call (i.e. a regular function call to a contract), relaxing the requirement
     * on the return value: the return value is optional (but if data is returned, it must not be false).
     * @param token The token targeted by the call.
     * @param data The call data (encoded using abi.encode or one of its variants).
     */
    function _callOptionalReturn(IERC20 token, bytes memory data) private {
        // We need to perform a low level call here, to bypass Solidity's return data size checking mechanism, since
        // we're implementing it ourselves. We use {Address.functionCall} to perform this call, which verifies that
        // the target address contains contract code and also asserts for success in the low-level call.

        bytes memory returndata = address(token).functionCall(data, \"SafeERC20: low-level call failed\");
        if (returndata.length > 0) { // Return data is optional
            // solhint-disable-next-line max-line-length
            require(abi.decode(returndata, (bool)), \"SafeERC20: ERC20 operation did not succeed\");
        }
    }
}
"
    
// SPDX-License-Identifier: MIT

pragma solidity ^0.7.0;

/**
 * @dev Collection of functions related to the address type
 */
library Address {
    /**
     * @dev Returns true if `account` is a contract.
     *
     * [IMPORTANT]
     * ====
     * It is unsafe to assume that an address for which this function returns
     * false is an externally-owned account (EOA) and not a contract.
     *
     * Among others, `isContract` will return false for the following
     * types of addresses:
     *
     *  - an externally-owned account
     *  - a contract in construction
     *  - an address where a contract will be created
     *  - an address where a contract lived, but was destroyed
     * ====
     */
    function isContract(address account) internal view returns (bool) {
        // This method relies on extcodesize, which returns 0 for contracts in
        // construction, since the code is only stored at the end of the
        // constructor execution.

        uint256 size;
        // solhint-disable-next-line no-inline-assembly
        assembly { size := extcodesize(account) }
        return size > 0;
    }

    /**
     * @dev Replacement for Solidity's `transfer`: sends `amount` wei to
     * `recipient`, forwarding all available gas and reverting on errors.
     *
     * https://eips.ethereum.org/EIPS/eip-1884[EIP1884] increases the gas cost
     * of certain opcodes, possibly making contracts go over the 2300 gas limit
     * imposed by `transfer`, making them unable to receive funds via
     * `transfer`. {sendValue} removes this limitation.
     *
     * https://diligence.consensys.net/posts/2019/09/stop-using-soliditys-transfer-now/[Learn more].
     *
     * IMPORTANT: because control is transferred to `recipient`, care must be
     * taken to not create reentrancy vulnerabilities. Consider using
     * {ReentrancyGuard} or the
     * https://solidity.readthedocs.io/en/v0.5.11/security-considerations.html#use-the-checks-effects-interactions-pattern[checks-effects-interactions pattern].
     */
    function sendValue(address payable recipient, uint256 amount) internal {
        require(address(this).balance >= amount, \"Address: insufficient balance\");

        // solhint-disable-next-line avoid-low-level-calls, avoid-call-value
        (bool success, ) = recipient.call{ value: amount }(\"\");
        require(success, \"Address: unable to send value, recipient may have reverted\");
    }

    /**
     * @dev Performs a Solidity function call using a low level `call`. A
     * plain`call` is an unsafe replacement for a function call: use this
     * function instead.
     *
     * If `target` reverts with a revert reason, it is bubbled up by this
     * function (like regular Solidity function calls).
     *
     * Returns the raw returned data. To convert to the expected return value,
     * use https://solidity.readthedocs.io/en/latest/units-and-global-variables.html?highlight=abi.decode#abi-encoding-and-decoding-functions[`abi.decode`].
     *
     * Requirements:
     *
     * - `target` must be a contract.
     * - calling `target` with `data` must not revert.
     *
     * _Available since v3.1._
     */
    function functionCall(address target, bytes memory data) internal returns (bytes memory) {
      return functionCall(target, data, \"Address: low-level call failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`], but with
     * `errorMessage` as a fallback revert reason when `target` reverts.
     *
     * _Available since v3.1._
     */
    function functionCall(address target, bytes memory data, string memory errorMessage) internal returns (bytes memory) {
        return functionCallWithValue(target, data, 0, errorMessage);
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`],
     * but also transferring `value` wei to `target`.
     *
     * Requirements:
     *
     * - the calling contract must have an ETH balance of at least `value`.
     * - the called Solidity function must be `payable`.
     *
     * _Available since v3.1._
     */
    function functionCallWithValue(address target, bytes memory data, uint256 value) internal returns (bytes memory) {
        return functionCallWithValue(target, data, value, \"Address: low-level call with value failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCallWithValue-address-bytes-uint256-}[`functionCallWithValue`], but
     * with `errorMessage` as a fallback revert reason when `target` reverts.
     *
     * _Available since v3.1._
     */
    function functionCallWithValue(address target, bytes memory data, uint256 value, string memory errorMessage) internal returns (bytes memory) {
        require(address(this).balance >= value, \"Address: insufficient balance for call\");
        require(isContract(target), \"Address: call to non-contract\");

        // solhint-disable-next-line avoid-low-level-calls
        (bool success, bytes memory returndata) = target.call{ value: value }(data);
        return _verifyCallResult(success, returndata, errorMessage);
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`],
     * but performing a static call.
     *
     * _Available since v3.3._
     */
    function functionStaticCall(address target, bytes memory data) internal view returns (bytes memory) {
        return functionStaticCall(target, data, \"Address: low-level static call failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-string-}[`functionCall`],
     * but performing a static call.
     *
     * _Available since v3.3._
     */
    function functionStaticCall(address target, bytes memory data, string memory errorMessage) internal view returns (bytes memory) {
        require(isContract(target), \"Address: static call to non-contract\");

        // solhint-disable-next-line avoid-low-level-calls
        (bool success, bytes memory returndata) = target.staticcall(data);
        return _verifyCallResult(success, returndata, errorMessage);
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`],
     * but performing a delegate call.
     *
     * _Available since v3.4._
     */
    function functionDelegateCall(address target, bytes memory data) internal returns (bytes memory) {
        return functionDelegateCall(target, data, \"Address: low-level delegate call failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-string-}[`functionCall`],
     * but performing a delegate call.
     *
     * _Available since v3.4._
     */
    function functionDelegateCall(address target, bytes memory data, string memory errorMessage) internal returns (bytes memory) {
        require(isContract(target), \"Address: delegate call to non-contract\");

        // solhint-disable-next-line avoid-low-level-calls
        (bool success, bytes memory returndata) = target.delegatecall(data);
        return _verifyCallResult(success, returndata, errorMessage);
    }

    function _verifyCallResult(bool success, bytes memory returndata, string memory errorMessage) private pure returns(bytes memory) {
        if (success) {
            return returndata;
        } else {
            // Look for revert reason and bubble it up if present
            if (returndata.length > 0) {
                // The easiest way to bubble the revert reason is using memory via assembly

                // solhint-disable-next-line no-inline-assembly
                assembly {
                    let returndata_size := mload(returndata)
                    revert(add(32, returndata), returndata_size)
                }
            } else {
                revert(errorMessage);
            }
        }
    }
}
"
    
// SPDX-License-Identifier: MIT

pragma solidity ^0.7.0;

/**
 * @dev Library for managing
 * https://en.wikipedia.org/wiki/Set_(abstract_data_type)[sets] of primitive
 * types.
 *
 * Sets have the following properties:
 *
 * - Elements are added, removed, and checked for existence in constant time
 * (O(1)).
 * - Elements are enumerated in O(n). No guarantees are made on the ordering.
 *
 * ```
 * contract Example {
 *     // Add the library methods
 *     using EnumerableSet for EnumerableSet.AddressSet;
 *
 *     // Declare a set state variable
 *     EnumerableSet.AddressSet private mySet;
 * }
 * ```
 *
 * As of v3.3.0, sets of type `bytes32` (`Bytes32Set`), `address` (`AddressSet`)
 * and `uint256` (`UintSet`) are supported.
 */
library EnumerableSet {
    // To implement this library for multiple types with as little code
    // repetition as possible, we write it in terms of a generic Set type with
    // bytes32 values.
    // The Set implementation uses private functions, and user-facing
    // implementations (such as AddressSet) are just wrappers around the
    // underlying Set.
    // This means that we can only create new EnumerableSets for types that fit
    // in bytes32.

    struct Set {
        // Storage of set values
        bytes32[] _values;

        // Position of the value in the `values` array, plus 1 because index 0
        // means a value is not in the set.
        mapping (bytes32 => uint256) _indexes;
    }

    /**
     * @dev Add a value to a set. O(1).
     *
     * Returns true if the value was added to the set, that is if it was not
     * already present.
     */
    function _add(Set storage set, bytes32 value) private returns (bool) {
        if (!_contains(set, value)) {
            set._values.push(value);
            // The value is stored at length-1, but we add 1 to all indexes
            // and use 0 as a sentinel value
            set._indexes[value] = set._values.length;
            return true;
        } else {
            return false;
        }
    }

    /**
     * @dev Removes a value from a set. O(1).
     *
     * Returns true if the value was removed from the set, that is if it was
     * present.
     */
    function _remove(Set storage set, bytes32 value) private returns (bool) {
        // We read and store the value's index to prevent multiple reads from the same storage slot
        uint256 valueIndex = set._indexes[value];

        if (valueIndex != 0) { // Equivalent to contains(set, value)
            // To delete an element from the _values array in O(1), we swap the element to delete with the last one in
            // the array, and then remove the last element (sometimes called as 'swap and pop').
            // This modifies the order of the array, as noted in {at}.

            uint256 toDeleteIndex = valueIndex - 1;
            uint256 lastIndex = set._values.length - 1;

            // When the value to delete is the last one, the swap operation is unnecessary. However, since this occurs
            // so rarely, we still do the swap anyway to avoid the gas cost of adding an 'if' statement.

            bytes32 lastvalue = set._values[lastIndex];

            // Move the last value to the index where the value to delete is
            set._values[toDeleteIndex] = lastvalue;
            // Update the index for the moved value
            set._indexes[lastvalue] = toDeleteIndex + 1; // All indexes are 1-based

            // Delete the slot where the moved value was stored
            set._values.pop();

            // Delete the index for the deleted slot
            delete set._indexes[value];

            return true;
        } else {
            return false;
        }
    }

    /**
     * @dev Returns true if the value is in the set. O(1).
     */
    function _contains(Set storage set, bytes32 value) private view returns (bool) {
        return set._indexes[value] != 0;
    }

    /**
     * @dev Returns the number of values on the set. O(1).
     */
    function _length(Set storage set) private view returns (uint256) {
        return set._values.length;
    }

   /**
    * @dev Returns the value stored at position `index` in the set. O(1).
    *
    * Note that there are no guarantees on the ordering of values inside the
    * array, and it may change when more values are added or removed.
    *
    * Requirements:
    *
    * - `index` must be strictly less than {length}.
    */
    function _at(Set storage set, uint256 index) private view returns (bytes32) {
        require(set._values.length > index, \"EnumerableSet: index out of bounds\");
        return set._values[index];
    }

    // Bytes32Set

    struct Bytes32Set {
        Set _inner;
    }

    /**
     * @dev Add a value to a set. O(1).
     *
     * Returns true if the value was added to the set, that is if it was not
     * already present.
     */
    function add(Bytes32Set storage set, bytes32 value) internal returns (bool) {
        return _add(set._inner, value);
    }

    /**
     * @dev Removes a value from a set. O(1).
     *
     * Returns true if the value was removed from the set, that is if it was
     * present.
     */
    function remove(Bytes32Set storage set, bytes32 value) internal returns (bool) {
        return _remove(set._inner, value);
    }

    /**
     * @dev Returns true if the value is in the set. O(1).
     */
    function contains(Bytes32Set storage set, bytes32 value) internal view returns (bool) {
        return _contains(set._inner, value);
    }

    /**
     * @dev Returns the number of values in the set. O(1).
     */
    function length(Bytes32Set storage set) internal view returns (uint256) {
        return _length(set._inner);
    }

   /**
    * @dev Returns the value stored at position `index` in the set. O(1).
    *
    * Note that there are no guarantees on the ordering of values inside the
    * array, and it may change when more values are added or removed.
    *
    * Requirements:
    *
    * - `index` must be strictly less than {length}.
    */
    function at(Bytes32Set storage set, uint256 index) internal view returns (bytes32) {
        return _at(set._inner, index);
    }

    // AddressSet

    struct AddressSet {
        Set _inner;
    }

    /**
     * @dev Add a value to a set. O(1).
     *
     * Returns true if the value was added to the set, that is if it was not
     * already present.
     */
    function add(AddressSet storage set, address value) internal returns (bool) {
        return _add(set._inner, bytes32(uint256(uint160(value))));
    }

    /**
     * @dev Removes a value from a set. O(1).
     *
     * Returns true if the value was removed from the set, that is if it was
     * present.
     */
    function remove(AddressSet storage set, address value) internal returns (bool) {
        return _remove(set._inner, bytes32(uint256(uint160(value))));
    }

    /**
     * @dev Returns true if the value is in the set. O(1).
     */
    function contains(AddressSet storage set, address value) internal view returns (bool) {
        return _contains(set._inner, bytes32(uint256(uint160(value))));
    }

    /**
     * @dev Returns the number of values in the set. O(1).
     */
    function length(AddressSet storage set) internal view returns (uint256) {
        return _length(set._inner);
    }

   /**
    * @dev Returns the value stored at position `index` in the set. O(1).
    *
    * Note that there are no guarantees on the ordering of values inside the
    * array, and it may change when more values are added or removed.
    *
    * Requirements:
    *
    * - `index` must be strictly less than {length}.
    */
    function at(AddressSet storage set, uint256 index) internal view returns (address) {
        return address(uint160(uint256(_at(set._inner, index))));
    }


    // UintSet

    struct UintSet {
        Set _inner;
    }

    /**
     * @dev Add a value to a set. O(1).
     *
     * Returns true if the value was added to the set, that is if it was not
     * already present.
     */
    function add(UintSet storage set, uint256 value) internal returns (bool) {
        return _add(set._inner, bytes32(value));
    }

    /**
     * @dev Removes a value from a set. O(1).
     *
     * Returns true if the value was removed from the set, that is if it was
     * present.
     */
    function remove(UintSet storage set, uint256 value) internal returns (bool) {
        return _remove(set._inner, bytes32(value));
    }

    /**
     * @dev Returns true if the value is in the set. O(1).
     */
    function contains(UintSet storage set, uint256 value) internal view returns (bool) {
        return _contains(set._inner, bytes32(value));
    }

    /**
     * @dev Returns the number of values on the set. O(1).
     */
    function length(UintSet storage set) internal view returns (uint256) {
        return _length(set._inner);
    }

   /**
    * @dev Returns the value stored at position `index` in the set. O(1).
    *
    * Note that there are no guarantees on the ordering of values inside the
    * array, and it may change when more values are added or removed.
    *
    * Requirements:
    *
    * - `index` must be strictly less than {length}.
    */
    function at(UintSet storage set, uint256 index) internal view returns (uint256) {
        return uint256(_at(set._inner, index));
    }
}
"
    
// SPDX-License-Identifier: MIT

/**
 * Based on https://github.com/OpenZeppelin/openzeppelin-contracts-upgradeable/blob/v3.4.0-solc-0.7/contracts/access/OwnableUpgradeable.sol
 *
 * Changes:
 * - Added owner argument to initializer
 * - Reformatted styling in line with this repository.
 */

/*
The MIT License (MIT)

Copyright (c) 2016-2020 zOS Global Limited

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
\"Software\"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

/* solhint-disable func-name-mixedcase */

pragma solidity 0.7.6;

import \"@openzeppelin/contracts-upgradeable/utils/ContextUpgradeable.sol\";
import \"@openzeppelin/contracts-upgradeable/proxy/Initializable.sol\";

/**
 * @dev Contract module which provides a basic access control mechanism, where
 * there is an account (an owner) that can be granted exclusive access to
 * specific functions.
 *
 * By default, the owner account will be the one that deploys the contract. This
 * can later be changed with {transferOwnership}.
 *
 * This module is used through inheritance. It will make available the modifier
 * `onlyOwner`, which can be applied to your functions to restrict their use to
 * the owner.
 */
abstract contract OwnableUpgradeable is Initializable, ContextUpgradeable {
\taddress private _owner;

\tevent OwnershipTransferred(
\t\taddress indexed previousOwner,
\t\taddress indexed newOwner
\t);

\t/**
\t * @dev Initializes the contract setting the deployer as the initial owner.
\t */
\tfunction __Ownable_init(address owner_) internal initializer {
\t\t__Context_init_unchained();
\t\t__Ownable_init_unchained(owner_);
\t}

\tfunction __Ownable_init_unchained(address owner_) internal initializer {
\t\t_owner = owner_;
\t\temit OwnershipTransferred(address(0), owner_);
\t}

\t/**
\t * @dev Returns the address of the current owner.
\t */
\tfunction owner() public view virtual returns (address) {
\t\treturn _owner;
\t}

\t/**
\t * @dev Throws if called by any account other than the owner.
\t */
\tmodifier onlyOwner() {
\t\trequire(owner() == _msgSender(), \"Ownable: caller is not the owner\");
\t\t_;
\t}

\t/**
\t * @dev Leaves the contract without owner. It will not be possible to call
\t * `onlyOwner` functions anymore. Can only be called by the current owner.
\t *
\t * NOTE: Renouncing ownership will leave the contract without an owner,
\t * thereby removing any functionality that is only available to the owner.
\t */
\tfunction renounceOwnership() public virtual onlyOwner {
\t\temit OwnershipTransferred(_owner, address(0));
\t\t_owner = address(0);
\t}

\t/**
\t * @dev Transfers ownership of the contract to a new account (`newOwner`).
\t * Can only be called by the current owner.
\t */
\tfunction transferOwnership(address newOwner) public virtual onlyOwner {
\t\trequire(newOwner != address(0), \"Ownable: new owner is the zero address\");
\t\temit OwnershipTransferred(_owner, newOwner);
\t\t_owner = newOwner;
\t}

\tuint256[49] private __gap;
}
"
    
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

import \"@openzeppelin/contracts-upgradeable/utils/ContextUpgradeable.sol\";
import \"@openzeppelin/contracts/utils/EnumerableSet.sol\";
import \"@openzeppelin/contracts/token/ERC20/IERC20.sol\";
import \"@openzeppelin/contracts-upgradeable/proxy/Initializable.sol\";
import \"@openzeppelin/contracts-upgradeable/utils/PausableUpgradeable.sol\";
import \"@openzeppelin/contracts/token/ERC20/SafeERC20.sol\";
import \"@openzeppelin/contracts/math/SafeMath.sol\";

import \"./ETHmxMinterData.sol\";
import \"../../tokens/interfaces/IETHmx.sol\";
import \"../interfaces/IETHmxMinter.sol\";
import \"../../tokens/interfaces/IETHtx.sol\";
import \"../interfaces/IETHtxAMM.sol\";
import \"../../tokens/interfaces/IWETH.sol\";
import \"../../access/OwnableUpgradeable.sol\";
import \"../../libraries/UintLog.sol\";

/* solhint-disable not-rely-on-time */

interface IPool {
\tfunction addLiquidity(
\t\taddress tokenA,
\t\taddress tokenB,
\t\tuint256 amountADesired,
\t\tuint256 amountBDesired,
\t\tuint256 amountAMin,
\t\tuint256 amountBMin,
\t\taddress to,
\t\tuint256 deadline
\t)
\t\texternal
\t\treturns (
\t\t\tuint256 amountA,
\t\t\tuint256 amountB,
\t\t\tuint256 liquidity
\t\t);
}

contract ETHmxMinter is
\tInitializable,
\tContextUpgradeable,
\tOwnableUpgradeable,
\tPausableUpgradeable,
\tETHmxMinterData,
\tIETHmxMinter
{
\tusing EnumerableSet for EnumerableSet.AddressSet;
\tusing SafeERC20 for IERC20;
\tusing SafeMath for uint256;
\tusing SafeMath for uint32;
\tusing UintLog for uint256;

\tstruct ETHmxMinterArgs {
\t\taddress ethmx;
\t\taddress ethtx;
\t\taddress ethtxAMM;
\t\taddress weth;
\t\tETHmxMintParams ethmxMintParams;
\t\tETHtxMintParams ethtxMintParams;
\t\tuint128 lpShareNumerator;
\t\tuint128 lpShareDenominator;
\t\taddress[] lps;
\t\taddress lpRecipient;
\t}

\tuint256 internal constant _GAS_PER_ETHTX = 21000; // per 1e18
\tuint256 internal constant _GENESIS_START = 1620655200; // 05/10/2021 1400 UTC
\tuint256 internal constant _GENESIS_END = 1621260000; // 05/17/2021 1400 UTC
\tuint256 internal constant _GENESIS_AMOUNT = 3e21; // 3k ETH

\t/* Constructor */

\tconstructor(address owner_) {
\t\tinit(owner_);
\t}

\t/* Initializer */

\tfunction init(address owner_) public virtual initializer {
\t\t__Context_init_unchained();
\t\t__Ownable_init_unchained(owner_);
\t\t__Pausable_init_unchained();
\t}

\tfunction postInit(ETHmxMinterArgs memory _args) external virtual onlyOwner {
\t\taddress sender = _msgSender();

\t\t_ethmx = _args.ethmx;
\t\temit EthmxSet(sender, _args.ethmx);

\t\t_ethtx = _args.ethtx;
\t\temit EthtxSet(sender, _args.ethtx);

\t\t_ethtxAMM = _args.ethtxAMM;
\t\temit EthtxAMMSet(sender, _args.ethtxAMM);

\t\t_weth = _args.weth;
\t\temit WethSet(sender, _args.weth);

\t\t_ethmxMintParams = _args.ethmxMintParams;
\t\temit EthmxMintParamsSet(sender, _args.ethmxMintParams);

\t\t_inGenesis = block.timestamp <= _GENESIS_END;
\t\t_minMintPrice = _args.ethtxMintParams.minMintPrice;
\t\t_mu = _args.ethtxMintParams.mu;
\t\t_lambda = _args.ethtxMintParams.lambda;
\t\temit EthtxMintParamsSet(sender, _args.ethtxMintParams);

\t\t_lpShareNum = _args.lpShareNumerator;
\t\t_lpShareDen = _args.lpShareDenominator;
\t\temit LpShareSet(sender, _args.lpShareNumerator, _args.lpShareDenominator);

\t\tfor (uint256 i = 0; i < _lps.length(); i++) {
\t\t\taddress lp = _lps.at(i);
\t\t\t_lps.remove(lp);
\t\t\temit LpRemoved(sender, lp);
\t\t}
\t\tfor (uint256 i = 0; i < _args.lps.length; i++) {
\t\t\taddress lp = _args.lps[i];
\t\t\t_lps.add(lp);
\t\t\temit LpAdded(sender, lp);
\t\t}

\t\t_lpRecipient = _args.lpRecipient;
\t\temit LpRecipientSet(sender, _args.lpRecipient);
\t}

\tfunction addLp(address pool) external virtual override onlyOwner {
\t\tbool added = _lps.add(pool);
\t\trequire(added, \"ETHmxMinter: liquidity pool already added\");
\t\temit LpAdded(_msgSender(), pool);
\t}

\tfunction mint() external payable virtual override whenNotPaused {
\t\trequire(block.timestamp >= _GENESIS_START, \"ETHmxMinter: before genesis\");
\t\tuint256 amountIn = msg.value;
\t\trequire(amountIn != 0, \"ETHmxMinter: cannot mint with zero amount\");

\t\t// Convert to WETH
\t\taddress weth_ = weth();
\t\tIWETH(weth_).deposit{ value: amountIn }();

\t\t// Check if we're in genesis
\t\tbool exitingGenesis;
\t\tuint256 ethToMintEthtx = amountIn;
\t\tif (_inGenesis) {
\t\t\tuint256 totalGiven_ = _totalGiven.add(amountIn);
\t\t\tif (block.timestamp >= _GENESIS_END || totalGiven_ >= _GENESIS_AMOUNT) {
\t\t\t\t// Exiting genesis
\t\t\t\tethToMintEthtx = totalGiven_;
\t\t\t\texitingGenesis = true;
\t\t\t} else {
\t\t\t\tethToMintEthtx = 0;
\t\t\t}
\t\t}

\t\t// Mint ETHtx and send ETHtx-WETH pair.
\t\t_mintEthtx(ethToMintEthtx);

\t\t// Mint ETHmx to sender.
\t\tuint256 amountOut = ethmxFromEth(amountIn);
\t\t_mint(_msgSender(), amountOut);
\t\t_totalGiven += amountIn;
\t\t// WARN this could cause re-entrancy if we ever called an unkown address
\t\tif (exitingGenesis) {
\t\t\t_inGenesis = false;
\t\t}
\t}

\tfunction mintWithETHtx(uint256 amount)
\t\texternal
\t\tvirtual
\t\toverride
\t\twhenNotPaused
\t{
\t\trequire(amount != 0, \"ETHmxMinter: cannot mint with zero amount\");

\t\tIETHtxAMM ammHandle = IETHtxAMM(ethtxAMM());
\t\tuint256 amountETHIn = ammHandle.ethToExactEthtx(amount);
\t\trequire(
\t\t\tammHandle.ethNeeded() >= amountETHIn,
\t\t\t\"ETHmxMinter: ETHtx value burnt exceeds ETH needed\"
\t\t);

\t\taddress account = _msgSender();
\t\tIETHtx(ethtx()).burn(account, amount);

\t\t_mint(account, amountETHIn);
\t}

\tfunction mintWithWETH(uint256 amount)
\t\texternal
\t\tvirtual
\t\toverride
\t\twhenNotPaused
\t{
\t\trequire(block.timestamp >= _GENESIS_START, \"ETHmxMinter: before genesis\");
\t\trequire(amount != 0, \"ETHmxMinter: cannot mint with zero amount\");
\t\taddress account = _msgSender();

\t\t// Need ownership for router
\t\tIERC20(weth()).safeTransferFrom(account, address(this), amount);

\t\t// Check if we're in genesis
\t\tbool exitingGenesis;
\t\tuint256 ethToMintEthtx = amount;
\t\tif (_inGenesis) {
\t\t\tuint256 totalGiven_ = _totalGiven.add(amount);
\t\t\tif (block.timestamp >= _GENESIS_END || totalGiven_ >= _GENESIS_AMOUNT) {
\t\t\t\t// Exiting genesis
\t\t\t\tethToMintEthtx = totalGiven_;
\t\t\t\texitingGenesis = true;
\t\t\t} else {
\t\t\t\tethToMintEthtx = 0;
\t\t\t}
\t\t}

\t\t// Mint ETHtx and send ETHtx-WETH pair.
\t\t_mintEthtx(ethToMintEthtx);

\t\tuint256 amountOut = ethmxFromEth(amount);
\t\t_mint(account, amountOut);
\t\t_totalGiven += amount;
\t\t// WARN this could cause re-entrancy if we ever called an unkown address
\t\tif (exitingGenesis) {
\t\t\t_inGenesis = false;
\t\t}
\t}

\tfunction pause() external virtual override onlyOwner {
\t\t_pause();
\t}

\tfunction recoverERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external virtual override onlyOwner {
\t\trequire(token != _weth, \"ETHmxMinter: cannot recover WETH\");
\t\tIERC20(token).safeTransfer(to, amount);
\t\temit Recovered(_msgSender(), token, to, amount);
\t}

\tfunction removeLp(address pool) external virtual override onlyOwner {
\t\tbool removed = _lps.remove(pool);
\t\trequire(removed, \"ETHmxMinter: liquidity pool not present\");
\t\temit LpRemoved(_msgSender(), pool);
\t}

\tfunction setEthmx(address addr) public virtual override onlyOwner {
\t\t_ethmx = addr;
\t\temit EthmxSet(_msgSender(), addr);
\t}

\tfunction setEthmxMintParams(ETHmxMintParams memory mp)
\t\tpublic
\t\tvirtual
\t\toverride
\t\tonlyOwner
\t{
\t\t_ethmxMintParams = mp;
\t\temit EthmxMintParamsSet(_msgSender(), mp);
\t}

\tfunction setEthtxMintParams(ETHtxMintParams memory mp)
\t\tpublic
\t\tvirtual
\t\toverride
\t\tonlyOwner
\t{
\t\t_minMintPrice = mp.minMintPrice;
\t\t_mu = mp.mu;
\t\t_lambda = mp.lambda;
\t\temit EthtxMintParamsSet(_msgSender(), mp);
\t}

\tfunction setEthtx(address addr) public virtual override onlyOwner {
\t\t_ethtx = addr;
\t\temit EthtxSet(_msgSender(), addr);
\t}

\tfunction setEthtxAMM(address addr) public virtual override onlyOwner {
\t\t_ethtxAMM = addr;
\t\temit EthtxAMMSet(_msgSender(), addr);
\t}

\tfunction setLpRecipient(address account)
\t\texternal
\t\tvirtual
\t\toverride
\t\tonlyOwner
\t{
\t\t_lpRecipient = account;
\t\temit LpRecipientSet(_msgSender(), account);
\t}

\tfunction setLpShare(uint128 numerator, uint128 denominator)
\t\texternal
\t\tvirtual
\t\toverride
\t\tonlyOwner
\t{
\t\t// Also guarantees that the denominator cannot be zero.
\t\trequire(denominator > numerator, \"ETHmxMinter: cannot set lpShare >= 1\");
\t\t_lpShareNum = numerator;
\t\t_lpShareDen = denominator;
\t\temit LpShareSet(_msgSender(), numerator, denominator);
\t}

\tfunction setWeth(address addr) public virtual override onlyOwner {
\t\t_weth = addr;
\t\temit WethSet(_msgSender(), addr);
\t}

\tfunction unpause() external virtual override onlyOwner {
\t\t_unpause();
\t}

\t/* Public Views */

\tfunction ethmx() public view virtual override returns (address) {
\t\treturn _ethmx;
\t}

\tfunction ethmxMintParams()
\t\tpublic
\t\tview
\t\tvirtual
\t\toverride
\t\treturns (ETHmxMintParams memory)
\t{
\t\treturn _ethmxMintParams;
\t}

\tfunction ethmxFromEth(uint256 amountETHIn)
\t\tpublic
\t\tview
\t\tvirtual
\t\toverride
\t\treturns (uint256)
\t{
\t\tif (amountETHIn == 0) {
\t\t\treturn 0;
\t\t}

\t\tETHmxMintParams memory mp = _ethmxMintParams;
\t\tuint256 amountOut = _ethmxCurve(amountETHIn, mp);

\t\tif (_inGenesis) {
\t\t\tuint256 totalGiven_ = _totalGiven;
\t\t\tuint256 totalEnd = totalGiven_.add(amountETHIn);

\t\t\tif (totalEnd > _GENESIS_AMOUNT) {
\t\t\t\t// Exiting genesis
\t\t\t\tuint256 amtUnder = _GENESIS_AMOUNT - totalGiven_;
\t\t\t\tamountOut -= amtUnder.mul(amountOut).div(amountETHIn);
\t\t\t\tuint256 added =
\t\t\t\t\tamtUnder.mul(2).mul(mp.zetaFloorNum).div(mp.zetaFloorDen);
\t\t\t\treturn amountOut.add(added);
\t\t\t}

\t\t\treturn amountOut.mul(2);
\t\t}

\t\treturn amountOut;
\t}

\tfunction ethmxFromEthtx(uint256 amountETHtxIn)
\t\tpublic
\t\tview
\t\tvirtual
\t\toverride
\t\treturns (uint256)
\t{
\t\treturn IETHtxAMM(ethtxAMM()).ethToExactEthtx(amountETHtxIn);
\t}

\tfunction ethtx() public view virtual override returns (address) {
\t\treturn _ethtx;
\t}

\tfunction ethtxMintParams()
\t\tpublic
\t\tview
\t\tvirtual
\t\toverride
\t\treturns (ETHtxMintParams memory)
\t{
\t\treturn ETHtxMintParams(_minMintPrice, _mu, _lambda);
\t}

\tfunction ethtxAMM() public view virtual override returns (address) {
\t\treturn _ethtxAMM;
\t}

\tfunction ethtxFromEth(uint256 amountETHIn)
\t\tpublic
\t\tview
\t\tvirtual
\t\toverride
\t\treturns (uint256)
\t{
\t\tif (amountETHIn == 0) {
\t\t\treturn 0;
\t\t}

\t\tIETHtxAMM ammHandle = IETHtxAMM(_ethtxAMM);
\t\t(uint256 collat, uint256 liability) = ammHandle.cRatio();
\t\tuint256 gasPrice = ammHandle.gasPrice();

\t\tuint256 basePrice;
\t\tuint256 lambda_;
\t\t{
\t\t\tuint256 minMintPrice_ = _minMintPrice;
\t\t\tuint256 mu_ = _mu;
\t\t\tlambda_ = _lambda;

\t\t\tbasePrice = mu_.mul(gasPrice).add(minMintPrice_);
\t\t}

\t\tif (liability == 0) {
\t\t\t// If exiting genesis, flat 2x on minting price up to threshold
\t\t\tif (_inGenesis) {
\t\t\t\tuint256 totalGiven_ = _totalGiven;
\t\t\t\tuint256 totalEnd = totalGiven_.add(amountETHIn);

\t\t\t\tif (totalEnd > _GENESIS_AMOUNT) {
\t\t\t\t\tuint256 amtOver = totalEnd - _GENESIS_AMOUNT;
\t\t\t\t\tuint256 amtOut =
\t\t\t\t\t\t_ethToEthtx(basePrice.mul(2), amountETHIn - amtOver);
\t\t\t\t\treturn amtOut.add(_ethToEthtx(basePrice, amtOver));
\t\t\t\t}
\t\t\t\treturn _ethToEthtx(basePrice.mul(2), amountETHIn);
\t\t\t}

\t\t\treturn _ethToEthtx(basePrice, amountETHIn);
\t\t}

\t\tuint256 ethTarget;
\t\t{
\t\t\t(uint256 cTargetNum, uint256 cTargetDen) = ammHandle.targetCRatio();
\t\t\tethTarget = liability.mul(cTargetNum).div(cTargetDen);
\t\t}

\t\tif (collat < ethTarget) {
\t\t\tuint256 ethEnd = collat.add(amountETHIn);
\t\t\tif (ethEnd <= ethTarget) {
\t\t\t\treturn 0;
\t\t\t}
\t\t\tamountETHIn = ethEnd - ethTarget;
\t\t\tcollat = ethTarget;
\t\t}

\t\tuint256 firstTerm = basePrice.mul(amountETHIn);

\t\tuint256 collatDiff = collat - liability;
\t\tuint256 coeffA = lambda_.mul(liability).mul(gasPrice);

\t\tuint256 secondTerm =
\t\t\tbasePrice.mul(collatDiff).add(coeffA).mul(1e18).ln().mul(coeffA);
\t\tsecondTerm /= 1e18;

\t\tuint256 thirdTerm = basePrice.mul(collatDiff.add(amountETHIn));
\t\t// avoids stack too deep error
\t\tthirdTerm = thirdTerm.add(coeffA).mul(1e18).ln().mul(coeffA) / 1e18;

\t\tuint256 numerator = firstTerm.add(secondTerm).sub(thirdTerm).mul(1e18);
\t\tuint256 denominator = _GAS_PER_ETHTX.mul(basePrice).mul(basePrice);
\t\treturn numerator.div(denominator);
\t}

\tfunction inGenesis() external view virtual override returns (bool) {
\t\treturn _inGenesis;
\t}

\tfunction numLiquidityPools()
\t\texternal
\t\tview
\t\tvirtual
\t\toverride
\t\treturns (uint256)
\t{
\t\treturn _lps.length();
\t}

\tfunction liquidityPoolsAt(uint256 index)
\t\texternal
\t\tview
\t\tvirtual
\t\toverride
\t\treturns (address)
\t{
\t\treturn _lps.at(index);
\t}

\tfunction lpRecipient() public view virtual override returns (address) {
\t\treturn _lpRecipient;
\t}

\tfunction lpShare()
\t\tpublic
\t\tview
\t\tvirtual
\t\toverride
\t\treturns (uint128 numerator, uint128 denominator)
\t{
\t\tnumerator = _lpShareNum;
\t\tdenominator = _lpShareDen;
\t}

\tfunction totalGiven() public view virtual override returns (uint256) {
\t\treturn _totalGiven;
\t}

\tfunction weth() public view virtual override returns (address) {
\t\treturn _weth;
\t}

\t/* Internal Views */

\tfunction _ethmxCurve(uint256 amountETHIn, ETHmxMintParams memory mp)
\t\tinternal
\t\tview
\t\tvirtual
\t\treturns (uint256)
\t{
\t\tuint256 cRatioNum;
\t\tuint256 cRatioDen;
\t\tuint256 cTargetNum;
\t\tuint256 cTargetDen;
\t\t{
\t\t\tIETHtxAMM ammHandle = IETHtxAMM(_ethtxAMM);
\t\t\t(cRatioNum, cRatioDen) = ammHandle.cRatio();

\t\t\tif (cRatioDen == 0) {
\t\t\t\t// cRatio > cCap
\t\t\t\treturn amountETHIn.mul(mp.zetaFloorNum).div(mp.zetaFloorDen);
\t\t\t}

\t\t\t(cTargetNum, cTargetDen) = ammHandle.targetCRatio();
\t\t}

\t\tuint256 ethEnd = cRatioNum.add(amountETHIn);
\t\tuint256 ethTarget = cRatioDen.mul(cTargetNum).div(cTargetDen);
\t\tuint256 ethCap = cRatioDen.mul(mp.cCapNum).div(mp.cCapDen);
\t\tif (cRatioNum >= ethCap) {
\t\t\t// cRatio >= cCap
\t\t\treturn amountETHIn.mul(mp.zetaFloorNum).div(mp.zetaFloorDen);
\t\t}

\t\tif (cRatioNum < ethTarget) {
\t\t\t// cRatio < cTarget
\t\t\tif (ethEnd > ethCap) {
\t\t\t\t// Add definite integral
\t\t\t\tuint256 curveAmt =
\t\t\t\t\t_ethmxDefiniteIntegral(
\t\t\t\t\t\tethCap - ethTarget,
\t\t\t\t\t\tmp,
\t\t\t\t\t\tcTargetNum,
\t\t\t\t\t\tcTargetDen,
\t\t\t\t\t\tethTarget,
\t\t\t\t\t\tcRatioDen
\t\t\t\t\t);

\t\t\t\t// Add amount past cap
\t\t\t\tuint256 pastCapAmt =
\t\t\t\t\t(ethEnd - ethCap).mul(mp.zetaFloorNum).div(mp.zetaFloorDen);

\t\t\t\t// add initial amount
\t\t\t\tuint256 flatAmt =
\t\t\t\t\t(ethTarget - cRatioNum).mul(mp.zetaCeilNum).div(mp.zetaCeilDen);

\t\t\t\treturn flatAmt.add(curveAmt).add(pastCapAmt);
\t\t\t} else if (ethEnd > ethTarget) {
\t\t\t\t// Add definite integral for partial amount
\t\t\t\tuint256 ethOver = ethEnd - ethTarget;
\t\t\t\tuint256 curveAmt =
\t\t\t\t\t_ethmxDefiniteIntegral(
\t\t\t\t\t\tethOver,
\t\t\t\t\t\tmp,
\t\t\t\t\t\tcTargetNum,
\t\t\t\t\t\tcTargetDen,
\t\t\t\t\t\tethTarget,
\t\t\t\t\t\tcRatioDen
\t\t\t\t\t);

\t\t\t\tuint256 ethBeforeCurve = amountETHIn - ethOver;
\t\t\t\tuint256 flatAmt =
\t\t\t\t\tethBeforeCurve.mul(mp.zetaCeilNum).div(mp.zetaCeilDen);
\t\t\t\treturn flatAmt.add(curveAmt);
\t\t\t}

\t\t\treturn amountETHIn.mul(mp.zetaCeilNum).div(mp.zetaCeilDen);
\t\t}

\t\t// cTarget < cRatio < cCap
\t\tif (ethEnd > ethCap) {
\t\t\tuint256 ethOver = ethEnd - ethCap;
\t\t\tuint256 curveAmt =
\t\t\t\t_ethmxDefiniteIntegral(
\t\t\t\t\tamountETHIn - ethOver,
\t\t\t\t\tmp,
\t\t\t\t\tcTargetNum,
\t\t\t\t\tcTargetDen,
\t\t\t\t\tcRatioNum,
\t\t\t\t\tcRatioDen
\t\t\t\t);

\t\t\tuint256 flatAmt = ethOver.mul(mp.zetaFloorNum).div(mp.zetaFloorDen);

\t\t\treturn curveAmt.add(flatAmt);
\t\t}

\t\treturn
\t\t\t_ethmxDefiniteIntegral(
\t\t\t\tamountETHIn,
\t\t\t\tmp,
\t\t\t\tcTargetNum,
\t\t\t\tcTargetDen,
\t\t\t\tcRatioNum,
\t\t\t\tcRatioDen
\t\t\t);
\t}

\tfunction _ethmxDefiniteIntegral(
\t\tuint256 amountETHIn,
\t\tETHmxMintParams memory mp,
\t\tuint256 cTargetNum,
\t\tuint256 cTargetDen,
\t\tuint256 initCollateral,
\t\tuint256 liability
\t) internal pure virtual returns (uint256) {
\t\tuint256 fctMulNum = mp.zetaFloorNum.mul(mp.zetaCeilDen).mul(cTargetDen);
\t\tuint256 fctMulDen = mp.zetaFloorDen.mul(mp.zetaCeilNum).mul(cTargetNum);

\t\t// prettier-ignore
\t\tuint256 first =
\t\t\tamountETHIn
\t\t\t.mul(fctMulNum.mul(mp.cCapNum))
\t\t\t.div(fctMulDen.mul(mp.cCapDen));

\t\tuint256 second = amountETHIn.mul(mp.zetaFloorNum).div(mp.zetaFloorDen);

\t\tuint256 tNum = fctMulNum.mul(amountETHIn);
\t\tuint256 tDen = fctMulDen.mul(2).mul(liability);
\t\tuint256 third = initCollateral.mul(2).add(amountETHIn);
\t\t// avoids stack too deep error
\t\tthird = third.mul(tNum).div(tDen);

\t\treturn first.add(second).sub(third);
\t}

\tfunction _ethToEthtx(uint256 gasPrice, uint256 amountETH)
\t\tinternal
\t\tpure
\t\tvirtual
\t\treturns (uint256)
\t{
\t\trequire(gasPrice != 0, \"ETHmxMinter: gasPrice is zero\");
\t\treturn amountETH.mul(1e18) / gasPrice.mul(_GAS_PER_ETHTX);
\t}

\t/* Internal Mutators */

\tfunction _mint(address account, uint256 amount) internal virtual {
\t\tIETHmx(ethmx()).mintTo(account, amount);
\t}

\tfunction _mintEthtx(uint256 amountEthIn) internal virtual {
\t\t// Mint ETHtx.
\t\tuint256 ethtxToMint = ethtxFromEth(amountEthIn);

\t\tif (ethtxToMint == 0) {
\t\t\treturn;
\t\t}

\t\taddress ethtx_ = ethtx();
\t\tIETHtx(ethtx_).mint(address(this), ethtxToMint);

\t\t// Lock portion into liquidity in designated pools
\t\t(uint256 ethtxSentToLp, uint256 ethSentToLp) = _sendToLps(ethtxToMint);

\t\t// Send the rest to the AMM.
\t\taddress ethtxAmm_ = ethtxAMM();
\t\tIERC20(weth()).safeTransfer(ethtxAmm_, amountEthIn.sub(ethSentToLp));
\t\tIERC20(ethtx_).safeTransfer(ethtxAmm_, ethtxToMint.sub(ethtxSentToLp));
\t}

\tfunction _sendToLps(uint256 ethtxTotal)
\t\tinternal
\t\tvirtual
\t\treturns (uint256 totalEthtxSent, uint256 totalEthSent)
\t{
\t\tuint256 numLps = _lps.length();
\t\tif (numLps == 0) {
\t\t\treturn (0, 0);
\t\t}

\t\t(uint256 lpShareNum, uint256 lpShareDen) = lpShare();
\t\tif (lpShareNum == 0) {
\t\t\treturn (0, 0);
\t\t}

\t\tuint256 ethtxToLp = ethtxTotal.mul(lpShareNum).div(lpShareDen).div(numLps);
\t\tuint256 ethToLp = IETHtxAMM(ethtxAMM()).ethToExactEthtx(ethtxToLp);
\t\taddress ethtx_ = ethtx();
\t\taddress weth_ = weth();
\t\taddress to = lpRecipient();

\t\tfor (uint256 i = 0; i < numLps; i++) {
\t\t\taddress pool = _lps.at(i);

\t\t\tIERC20(ethtx_).safeIncreaseAllowance(pool, ethtxToLp);
\t\t\tIERC20(weth_).safeIncreaseAllowance(pool, ethToLp);

\t\t\t(uint256 ethtxSent, uint256 ethSent, ) =
\t\t\t\tIPool(pool).addLiquidity(
\t\t\t\t\tethtx_,
\t\t\t\t\tweth_,
\t\t\t\t\tethtxToLp,
\t\t\t\t\tethToLp,
\t\t\t\t\t0,
\t\t\t\t\t0,
\t\t\t\t\tto,
\t\t\t\t\t// solhint-disable-next-line not-rely-on-time
\t\t\t\t\tblock.timestamp
\t\t\t\t);

\t\t\ttotalEthtxSent = totalEthtxSent.add(ethtxSent);
\t\t\ttotalEthSent = totalEthSent.add(ethSent);
\t\t}
\t}
}
"
    
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

import \"@openzeppelin/contracts/utils/EnumerableSet.sol\";

import \"../interfaces/IETHmxMinter.sol\";

abstract contract ETHmxMinterData {
\taddress internal _ethmx;
\taddress internal _ethtx;
\taddress internal _ethtxAMM;
\taddress internal _weth;

\t// ETHmx minting
\tuint256 internal _totalGiven;
\tIETHmxMinter.ETHmxMintParams internal _ethmxMintParams;

\t// ETHtx minting
\tuint128 internal _minMintPrice;
\tuint64 internal _mu;
\tuint64 internal _lambda;

\t// Liquidity pool distribution
\tuint128 internal _lpShareNum;
\tuint128 internal _lpShareDen;
\tEnumerableSet.AddressSet internal _lps;
\taddress internal _lpRecipient;

\tbool internal _inGenesis;

\tuint256[39] private __gap;
}
"
    
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;
pragma abicoder v2;

interface IETHmxMinter {
\t/* Types */

\tstruct ETHmxMintParams {
\t\t// Uses a single 32 byte slot and avoids stack too deep errors
\t\tuint32 cCapNum;
\t\tuint32 cCapDen;
\t\tuint32 zetaFloorNum;
\t\tuint32 zetaFloorDen;
\t\tuint32 zetaCeilNum;
\t\tuint32 zetaCeilDen;
\t}

\tstruct ETHtxMintParams {
\t\tuint128 minMintPrice;
\t\tuint64 mu;
\t\tuint64 lambda;
\t}

\t/* Views */

\tfunction ethmx() external view returns (address);

\tfunction ethmxMintParams() external view returns (ETHmxMintParams memory);

\tfunction ethmxFromEth(uint256 amountETHIn) external view returns (uint256);

\tfunction ethmxFromEthtx(uint256 amountETHtxIn)
\t\texternal
\t\tview
\t\treturns (uint256);

\tfunction ethtx() external view returns (address);

\tfunction ethtxMintParams() external view returns (ETHtxMintParams memory);

\tfunction ethtxAMM() external view returns (address);

\tfunction ethtxFromEth(uint256 amountETHIn) external view returns (uint256);

\tfunction inGenesis() external view returns (bool);

\tfunction numLiquidityPools() external view returns (uint256);

\tfunction liquidityPoolsAt(uint256 index) external view returns (address);

\tfunction lpRecipient() external view returns (address);

\tfunction lpShare()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction totalGiven() external view returns (uint256);

\tfunction weth() external view returns (address);

\t/* Mutators */

\tfunction addLp(address pool) external;

\tfunction mint() external payable;

\tfunction mintWithETHtx(uint256 amountIn) external;

\tfunction mintWithWETH(uint256 amountIn) external;

\tfunction pause() external;

\tfunction recoverERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction removeLp(address pool) external;

\tfunction setEthmx(address addr) external;

\tfunction setEthmxMintParams(ETHmxMintParams memory mp) external;

\tfunction setEthtxMintParams(ETHtxMintParams memory mp) external;

\tfunction setEthtx(address addr) external;

\tfunction setEthtxAMM(address addr) external;

\tfunction setLpRecipient(address account) external;

\tfunction setLpShare(uint128 numerator, uint128 denominator) external;

\tfunction setWeth(address addr) external;

\tfunction unpause() external;

\t/* Events */

\tevent EthmxSet(address indexed author, address indexed addr);
\tevent EthmxMintParamsSet(address indexed author, ETHmxMintParams mp);
\tevent EthtxMintParamsSet(address indexed author, ETHtxMintParams mp);
\tevent EthtxSet(address indexed author, address indexed addr);
\tevent EthtxAMMSet(address indexed author, address indexed addr);
\tevent LpAdded(address indexed author, address indexed account);
\tevent LpRecipientSet(address indexed author, address indexed account);
\tevent LpRemoved(address indexed author, address indexed account);
\tevent LpShareSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent Recovered(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
\tevent WethSet(address indexed author, address indexed addr);
}
"
    
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

interface IETHtxAMM {
\t/* Views */

\tfunction cRatio()
\t\texternal
\t\tview
\t\treturns (uint256 numerator, uint256 denominator);

\tfunction cRatioBelowTarget() external view returns (bool);

\tfunction ethNeeded() external view returns (uint256);

\tfunction ethtx() external view returns (address);

\tfunction exactEthToEthtx(uint256 amountEthIn)
\t\texternal
\t\tview
\t\treturns (uint256);

\tfunction ethToExactEthtx(uint256 amountEthtxOut)
\t\texternal
\t\tview
\t\treturns (uint256);

\tfunction exactEthtxToEth(uint256 amountEthtxIn)
\t\texternal
\t\tview
\t\treturns (uint256);

\tfunction ethtxToExactEth(uint256 amountEthOut)
\t\texternal
\t\tview
\t\treturns (uint256);

\tfunction ethSupply() external view returns (uint256);

\tfunction ethSupplyTarget() external view returns (uint256);

\tfunction ethtxAvailable() external view returns (uint256);

\tfunction ethtxOutstanding() external view returns (uint256);

\tfunction feeLogic() external view returns (address);

\tfunction gasOracle() external view returns (address);

\tfunction gasPerETHtx() external pure returns (uint256);

\tfunction gasPrice() external view returns (uint256);

\tfunction gasPriceAtRedemption() external view returns (uint256);

\tfunction maxGasPrice() external view returns (uint256);

\tfunction targetCRatio()
\t\texternal
\t\tview
\t\treturns (uint128 numerator, uint128 denominator);

\tfunction weth() external view returns (address);

\t/* Mutators */

\tfunction swapEthForEthtx(uint256 deadline) external payable;

\tfunction swapWethForEthtx(uint256 amountIn, uint256 deadline) external;

\tfunction swapEthForExactEthtx(uint256 amountOut, uint256 deadline)
\t\texternal
\t\tpayable;

\tfunction swapWethForExactEthtx(
\t\tuint256 amountInMax,
\t\tuint256 amountOut,
\t\tuint256 deadline
\t) external;

\tfunction swapExactEthForEthtx(uint256 amountOutMin, uint256 deadline)
\t\texternal
\t\tpayable;

\tfunction swapExactWethForEthtx(
\t\tuint256 amountIn,
\t\tuint256 amountOutMin,
\t\tuint256 deadline
\t) external;

\tfunction swapEthtxForEth(
\t\tuint256 amountIn,
\t\tuint256 deadline,
\t\tbool asWETH
\t) external;

\tfunction swapEthtxForExactEth(
\t\tuint256 amountInMax,
\t\tuint256 amountOut,
\t\tuint256 deadline,
\t\tbool asWETH
\t) external;

\tfunction swapExactEthtxForEth(
\t\tuint256 amountIn,
\t\tuint256 amountOutMin,
\t\tuint256 deadline,
\t\tbool asWETH
\t) external;

\tfunction pause() external;

\tfunction recoverUnsupportedERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction setEthtx(address account) external;

\tfunction setGasOracle(address account) external;

\tfunction setTargetCRatio(uint128 numerator, uint128 denominator) external;

\tfunction setWETH(address account) external;

\tfunction unpause() external;

\t/* Events */

\tevent ETHtxSet(address indexed author, address indexed account);
\tevent GasOracleSet(address indexed author, address indexed account);
\tevent RecoveredUnsupported(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
\tevent TargetCRatioSet(
\t\taddress indexed author,
\t\tuint128 numerator,
\t\tuint128 denominator
\t);
\tevent WETHSet(address indexed author, address indexed account);
}
"
    
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

library UintLog {
\tuint256 internal constant _LOG2_E = 1442695040888963407;

\tfunction ln(uint256 x) internal pure returns (uint256) {
\t\treturn (blog2(x) * 1e18) / _LOG2_E;
\t}

\t// Most significant bit
\t// prettier-ignore
\tfunction msb(uint256 x) internal pure returns (uint256 n) {
\t\tif (x >= 0x100000000000000000000000000000000) { x >>= 128; n += 128; }
\t\tif (x >= 0x10000000000000000) { x >>= 64; n += 64; }
\t\tif (x >= 0x100000000) { x >>= 32; n += 32; }
\t\tif (x >= 0x10000) { x >>= 16; n += 16; }
\t\tif (x >= 0x100) { x >>= 8; n += 8; }
\t\tif (x >= 0x10) { x >>= 4; n += 4; }
\t\tif (x >= 0x4) { x >>= 2; n += 2; }
\t\tif (x >= 0x2) { /* x >>= 1; */ n += 1; }
\t}

\t// Approximate binary log of uint
\t// https://en.wikipedia.org/wiki/Binary_logarithm#Iterative_approximation
\t// https://github.com/hifi-finance/prb-math/blob/5c6817860496ec40fd269934f3c531822402f1ce/contracts/PRBMathUD60x18.sol#L334-L380
\tfunction blog2(uint256 x) internal pure returns (uint256 result) {
\t\trequire(x >= 1e18, \"blog2 too small\");
\t\tuint256 n = msb(x / 1e18);

\t\tresult = n * 1e18;
\t\tuint256 y = x >> n;

\t\tif (y == 1e18) {
\t\t\treturn result;
\t\t}

\t\tfor (uint256 delta = 5e17; delta > 0; delta >>= 1) {
\t\t\ty = (y * y) / 1e18;
\t\t\tif (y >= 2e18) {
\t\t\t\tresult += delta;
\t\t\t\ty >>= 1;
\t\t\t}
\t\t}
\t}
}
"
    
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

interface IETHmx {
\t/* Views */

\tfunction minter() external view returns (address);

\t/* Mutators */

\tfunction burn(uint256 amount) external;

\tfunction mintTo(address account, uint256 amount) external;

\tfunction pause() external;

\tfunction recoverERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction setMinter(address account) external;

\tfunction unpause() external;

\t/* Events */

\tevent MinterSet(address indexed author, address indexed account);
\tevent Recovered(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
}
"
    
// SPDX-License-Identifier: Apache-2.0

/**
 * Copyright 2021 weiWard LLC
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.7.6;

interface IETHtx {
\t/* Views */

\tfunction minter() external view returns (address);

\t/* Mutators */

\tfunction burn(address account, uint256 amount) external;

\tfunction mint(address account, uint256 amount) external;

\tfunction pause() external;

\tfunction recoverERC20(
\t\taddress token,
\t\taddress to,
\t\tuint256 amount
\t) external;

\tfunction setFeeLogic(address account) external;

\tfunction setMinter(address account) external;

\tfunction unpause() external;

\t/* Events */

\tevent FeeLogicSet(address indexed author, address indexed account);
\tevent MinterSet(address indexed author, address indexed account);
\tevent Recovered(
\t\taddress indexed author,
\t\taddress indexed token,
\t\taddress indexed to,
\t\tuint256 amount
\t);
}
"
    
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
// SPDX-License-Identifier: MIT
pragma solidity 0.7.6;

interface IWETH {
\tfunction deposit() external payable;

\tfunction withdraw(uint256) external;
}
"
    }
  }
}
