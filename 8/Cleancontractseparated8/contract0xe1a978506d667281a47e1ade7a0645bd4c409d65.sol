// SPDX-License-Identifier: MIT

pragma solidity ^0.7.4;

/**
 * @dev Collection of functions related to the address type
 */
library Address {
    /**
     * @dev Returns true if `account` is a contract.
     *
     * [IMPORTANT]
     * ====
     * It is unsafe to assume that an address for which this function returns
     * false is an externally-owned account (EOA) and not a contract.
     *
     * Among others, `isContract` will return false for the following
     * types of addresses:
     *
     *  - an externally-owned account
     *  - a contract in construction
     *  - an address where a contract will be created
     *  - an address where a contract lived, but was destroyed
     * ====
     */
    function isContract(address account) internal view returns (bool) {
        // This method relies on extcodesize, which returns 0 for contracts in
        // construction, since the code is only stored at the end of the
        // constructor execution.

        uint256 size;
        // solhint-disable-next-line no-inline-assembly
        assembly { size := extcodesize(account) }
        return size \u003e 0;
    }

    /**
     * @dev Replacement for Solidity\u0027s `transfer`: sends `amount` wei to
     * `recipient`, forwarding all available gas and reverting on errors.
     *
     * https://eips.ethereum.org/EIPS/eip-1884[EIP1884] increases the gas cost
     * of certain opcodes, possibly making contracts go over the 2300 gas limit
     * imposed by `transfer`, making them unable to receive funds via
     * `transfer`. {sendValue} removes this limitation.
     *
     * https://diligence.consensys.net/posts/2019/09/stop-using-soliditys-transfer-now/[Learn more].
     *
     * IMPORTANT: because control is transferred to `recipient`, care must be
     * taken to not create reentrancy vulnerabilities. Consider using
     * {ReentrancyGuard} or the
     * https://solidity.readthedocs.io/en/v0.5.11/security-considerations.html#use-the-checks-effects-interactions-pattern[checks-effects-interactions pattern].
     */
    function sendValue(address payable recipient, uint256 amount) internal {
        require(address(this).balance \u003e= amount, \"Address: insufficient balance\");

        // solhint-disable-next-line avoid-low-level-calls, avoid-call-value
        (bool success, ) = recipient.call{ value: amount }(\"\");
        require(success, \"Address: unable to send value, recipient may have reverted\");
    }

    /**
     * @dev Performs a Solidity function call using a low level `call`. A
     * plain`call` is an unsafe replacement for a function call: use this
     * function instead.
     *
     * If `target` reverts with a revert reason, it is bubbled up by this
     * function (like regular Solidity function calls).
     *
     * Returns the raw returned data. To convert to the expected return value,
     * use https://solidity.readthedocs.io/en/latest/units-and-global-variables.html?highlight=abi.decode#abi-encoding-and-decoding-functions[`abi.decode`].
     *
     * Requirements:
     *
     * - `target` must be a contract.
     * - calling `target` with `data` must not revert.
     *
     * _Available since v3.1._
     */
    function functionCall(address target, bytes memory data) internal returns (bytes memory) {
        return functionCall(target, data, \"Address: low-level call failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`], but with
     * `errorMessage` as a fallback revert reason when `target` reverts.
     *
     * _Available since v3.1._
     */
    function functionCall(address target, bytes memory data, string memory errorMessage) internal returns (bytes memory) {
        return functionCallWithValue(target, data, 0, errorMessage);
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`],
     * but also transferring `value` wei to `target`.
     *
     * Requirements:
     *
     * - the calling contract must have an ETH balance of at least `value`.
     * - the called Solidity function must be `payable`.
     *
     * _Available since v3.1._
     */
    function functionCallWithValue(address target, bytes memory data, uint256 value) internal returns (bytes memory) {
        return functionCallWithValue(target, data, value, \"Address: low-level call with value failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCallWithValue-address-bytes-uint256-}[`functionCallWithValue`], but
     * with `errorMessage` as a fallback revert reason when `target` reverts.
     *
     * _Available since v3.1._
     */
    function functionCallWithValue(address target, bytes memory data, uint256 value, string memory errorMessage) internal returns (bytes memory) {
        require(address(this).balance \u003e= value, \"Address: insufficient balance for call\");
        require(isContract(target), \"Address: call to non-contract\");

        // solhint-disable-next-line avoid-low-level-calls
        (bool success, bytes memory returndata) = target.call{ value: value }(data);
        return _verifyCallResult(success, returndata, errorMessage);
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`],
     * but performing a static call.
     *
     * _Available since v3.3._
     */
    function functionStaticCall(address target, bytes memory data) internal view returns (bytes memory) {
        return functionStaticCall(target, data, \"Address: low-level static call failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-string-}[`functionCall`],
     * but performing a static call.
     *
     * _Available since v3.3._
     */
    function functionStaticCall(address target, bytes memory data, string memory errorMessage) internal view returns (bytes memory) {
        require(isContract(target), \"Address: static call to non-contract\");

        // solhint-disable-next-line avoid-low-level-calls
        (bool success, bytes memory returndata) = target.staticcall(data);
        return _verifyCallResult(success, returndata, errorMessage);
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`],
     * but performing a delegate call.
     *
     * _Available since v3.3._
     */
    function functionDelegateCall(address target, bytes memory data) internal returns (bytes memory) {
        return functionDelegateCall(target, data, \"Address: low-level delegate call failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-string-}[`functionCall`],
     * but performing a delegate call.
     *
     * _Available since v3.3._
     */
    function functionDelegateCall(address target, bytes memory data, string memory errorMessage) internal returns (bytes memory) {
        require(isContract(target), \"Address: delegate call to non-contract\");

        // solhint-disable-next-line avoid-low-level-calls
        (bool success, bytes memory returndata) = target.delegatecall(data);
        return _verifyCallResult(success, returndata, errorMessage);
    }

    function _verifyCallResult(bool success, bytes memory returndata, string memory errorMessage) private pure returns (bytes memory) {
        if (success) {
            return returndata;
        } else {
            // Look for revert reason and bubble it up if present
            if (returndata.length \u003e 0) {
                // The easiest way to bubble the revert reason is using memory via assembly

                // solhint-disable-next-line no-inline-assembly
                assembly {
                    let returndata_size := mload(returndata)
                    revert(add(32, returndata), returndata_size)
                }
            } else {
                revert(errorMessage);
            }
        }
    }
}"},"AmericanVotes.sol":{"content":"// SPDX-License-Identifier: MIT
// Developed by Pironmind
// https://t.me/pironmind

pragma solidity \u003e=0.4.22 \u003c0.8.0;

import \"./SafeMath.sol\";
import \"./Ownable.sol\";
import \"./IVotes.sol\";
import \"./ReentrancyGuard.sol\";
import \"./Address.sol\";
import \"./IRandom.sol\";

/*
 * @title AmericanVotes
 * @dev Implements voting process along with vote delegation
 */
contract AmericanVotes is IVotes, Ownable, ReentrancyGuard {
    using Address for *;
    using SafeMath for uint;

    event EthereumDeposited(address voter, uint256 amount);
    event LuckyBonusClaimed(address lacky, uint256 amount);
    event TeamRewardClaimed(address teammate, uint256 amount);
    event EthereumWithdrawn(address resipient, uint256 amount);
    event DemocratsWon(bool x);

    enum Consignmen { NONE, REPUBLICANS, DEMOCRATS }
    enum State { NONE, CHANGE }

    struct Voter {
        uint weight; // balance
        uint balance;
        bool voted;  // if true, that person already voted
        Consignmen voteType;
        State state;
        bool credited;
    }

    struct Proposal {
        uint voteCount; // number of accumulated votes
        uint voteWeight;
        uint weightRepublicans;
        uint weightDemocrats;
        uint forRepublicans;
        uint forDemocrats;
        uint startDate;
        uint endDate;
        Consignmen winner;
    }

    Proposal public proposal;

    mapping(address =\u003e Voter) public voters;

    address[] public votersList;

    uint256 private _minimumDeposit;

    uint256 public serviceFee;

    uint256 public luckyFee;

    mapping (address =\u003e uint) public balances;

    address public teamWallet;
    IRandom public randomOracle;
    address public luckyAddress = address(0);

    /*
     * @dev Create a new ballot to choose one of \u0027proposalNames\u0027.
     * @param proposalNames names of proposals
     */
    constructor(address _teamWallet, address _randomOracle, uint startDate, uint endDate) {
        teamWallet = _teamWallet;
        randomOracle = IRandom(_randomOracle);
        proposal.startDate = startDate;
        proposal.endDate = endDate;
        serviceFee = 20; // %
        luckyFee = 5; // %
        _minimumDeposit = 1e17;
    }

    // @dev How much lucky guy get in random
    function luckyBonus() public view returns (uint256) {
        return balances[address(0)];
    }

    // @dev How much team get if anything will be okey
    function teamReward() public view returns (uint256) {
        return balances[teamWallet];
    }

    function isVoted(address _who) public override view returns (bool) {
        return voters[_who].voted;
    }


    function totalVotes() public override view returns (uint) {
        return proposal.voteCount;
    }

    function totalWeight() public override view returns (uint) {
        return proposal.voteWeight;
    }

    function minimumDeposit() public view returns (uint) {
        return _minimumDeposit;
    }

    // @dev _amount (wei)
    function setMinimumDeposit(uint256 _amount) external onlyOwner {
        require(_amount \u003e 0, \"Must be more then 0\");

        _minimumDeposit = _amount;
    }

    function _deposit() internal /*override*/ /*payable*/ /*nonReentrant*/ {
        require(!Address.isContract(msg.sender), \"Contract deposit not accepted\");
        require(msg.value \u003e= minimumDeposit(), \"Not enough balance for deposit\");

        uint _serviceShare = msg.value.mul(serviceFee).div(100);
        uint _luckyShare = msg.value.mul(luckyFee).div(100);
        balances[address(teamWallet)] = balances[address(teamWallet)]
            .add(_serviceShare.sub(_luckyShare));
        balances[address(luckyAddress)] = balances[address(luckyAddress)]
            .add(_luckyShare);

        voters[msg.sender].balance = voters[msg.sender].balance.add(msg.value.sub(_serviceShare));

        emit EthereumDeposited(msg.sender, msg.value.sub(_serviceShare));
        emit EthereumDeposited(teamWallet, _serviceShare.sub(_luckyShare));
        emit EthereumDeposited(luckyAddress, _luckyShare);
    }

    function withdraw() public override onlyAfter afterWinnerSet {

        // team wallet withdraw
        if (msg.sender == address(teamWallet)) {
            uint amount = balances[address(teamWallet)];
            balances[address(teamWallet)] = balances[address(teamWallet)].sub(amount);
            balances[address(teamWallet)] = 0;
            payable(msg.sender).transfer(amount);
            emit TeamRewardClaimed(address(teamWallet), amount);
            return;
        }

        // lucky wallet withdraw
        if (msg.sender == address(luckyAddress) \u0026\u0026 balances[address(luckyAddress)] \u003e 0) {
            uint amount = balances[address(luckyAddress)];
            balances[address(luckyAddress)] = balances[address(luckyAddress)].sub(amount);
            balances[address(luckyAddress)] = 0;
            payable(msg.sender).transfer(amount);
            emit LuckyBonusClaimed(address(luckyAddress), amount);
            return;
        }

        // count withdraw
        require(!voters[msg.sender].credited, \"Balance counted\");
        require(voters[msg.sender].voteType == proposal.winner, \"Only if winner\");

        voters[msg.sender].credited = true;

        uint weight = voters[msg.sender].weight;
        uint share = _countShare(proposal.weightDemocrats, proposal.weightRepublicans, weight);

        voters[msg.sender].weight = voters[msg.sender].weight.sub(weight, \"WSUB1\");
        voters[msg.sender].balance = voters[msg.sender].balance.add(weight).add(share);

        uint _amount = voters[msg.sender].balance;
        voters[msg.sender].balance = voters[msg.sender].balance.sub(_amount, \"WSUB2\");

        payable(msg.sender).transfer(_amount);
        emit EthereumWithdrawn(address(msg.sender), _amount);
    }

    function voteForRepublicans() public override payable nonReentrant {
        _deposit();
        _vote(Consignmen.REPUBLICANS, msg.sender);
    }

    function voteForDemocrats() public override payable nonReentrant {
        _deposit();
        _vote(Consignmen.DEMOCRATS, msg.sender);
    }

    // after 1 month contract can be destroyed
    function destroyIt() public override onlyOwner {
        require(block.timestamp \u003e proposal.endDate.add(31 * 1 days));
        selfdestruct(msg.sender);
    }

    function democratsWon() public override onlyOwner onlyAfter winnerNotSet {
        _setWinner(Consignmen.DEMOCRATS);
        _selectLucky();
        emit DemocratsWon(true);
    }

    function republicansWon() public override onlyOwner onlyAfter winnerNotSet {
        _setWinner(Consignmen.REPUBLICANS);
        _selectLucky();
        emit DemocratsWon(false);
    }

    function _setWinner(Consignmen _winner) internal {
        proposal.winner = _winner;
    }

    function _selectLucky() internal {
        require(randomOracle.getNumber(0, uint(votersList.length).sub(1, \"SL1\")) \u003e= 0, \"Oracle connected\");
        uint luckyNum = randomOracle.getNumber(0, uint(votersList.length).sub(1, \"SL1\"));
        luckyAddress = votersList[luckyNum];
        uint luckyBank = balances[address(0)];
        balances[address(0)] = balances[address(0)].sub(luckyBank);
        balances[votersList[luckyNum]] = balances[votersList[luckyNum]].add(luckyBank);
    }

    function _vote(Consignmen _type, address voter)
        internal
        onlyDuring
    {
        Voter storage sender = voters[msg.sender];

        if (sender.voteType != Consignmen.NONE) {
            require(sender.voteType == _type, \"Only for one candidate\");
        }

        uint256 amount = sender.balance;
        sender.balance = sender.balance.sub(amount);
        sender.weight = sender.weight.add(amount);
        sender.voteType = _type;

        if (_type == Consignmen.DEMOCRATS) {
            proposal.weightDemocrats = proposal.weightDemocrats.add(amount);
        } else {
            proposal.weightRepublicans = proposal.weightRepublicans.add(amount);
        }

        if (!sender.voted) {
            proposal.voteCount++;
            if (_type == Consignmen.DEMOCRATS) {
                proposal.forDemocrats++;
            } else {
                proposal.forRepublicans++;
            }
            votersList.push(voter);
        }

        sender.voted = true;

        proposal.voteWeight = proposal.voteWeight.add(amount);
    }

    function _countShare(uint256 share1, uint256 share2, uint256 userShare) public view returns(uint) {
        if (proposal.winner == Consignmen.DEMOCRATS) {
            return userShare.mul(share1).div(share2);
        }
        return userShare.mul(share2).div(share1);
    }

    modifier onlyDuring() {
        require(
            block.timestamp \u003e= proposal.startDate \u0026\u0026
            block.timestamp \u003c= proposal.endDate,
            \"Voting not has not started or just ended yet\"
        );
        _;
    }

    modifier onlyAfter() {
        require(block.timestamp \u003e proposal.endDate, \"Voting in progress\");
        _;
    }

    modifier winnerNotSet() {
        require(proposal.winner == Consignmen.NONE, \"Only if no winner\");
        _;
    }

    modifier afterWinnerSet() {
        require(proposal.winner != Consignmen.NONE, \"Only after winner was set\");
        _;
    }
}
"},"IERC20.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity \u003e=0.4.22 \u003c0.8.0;

/**
 * @dev Interface of the ERC20 standard as defined in the EIP.
 */
interface IERC20 {
    /**
     * @dev Returns the amount of tokens in existence.
     */
    function totalSupply() external view returns (uint256);

    /**
     * @dev Returns the amount of tokens owned by `account`.
     */
    function balanceOf(address account) external view returns (uint256);

    /**
     * @dev Moves `amount` tokens from the caller\u0027s account to `recipient`.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transfer(address recipient, uint256 amount) external returns (bool);

    /**
     * @dev Returns the remaining number of tokens that `spender` will be
     * allowed to spend on behalf of `owner` through {transferFrom}. This is
     * zero by default.
     *
     * This value changes when {approve} or {transferFrom} are called.
     */
    function allowance(address owner, address spender) external view returns (uint256);

    /**
     * @dev Sets `amount` as the allowance of `spender` over the caller\u0027s tokens.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * IMPORTANT: Beware that changing an allowance with this method brings the risk
     * that someone may use both the old and the new allowance by unfortunate
     * transaction ordering. One possible solution to mitigate this race
     * condition is to first reduce the spender\u0027s allowance to 0 and set the
     * desired value afterwards:
     * https://github.com/ethereum/EIPs/issues/20#issuecomment-263524729
     *
     * Emits an {Approval} event.
     */
    function approve(address spender, uint256 amount) external returns (bool);

    /**
     * @dev Moves `amount` tokens from `sender` to `recipient` using the
     * allowance mechanism. `amount` is then deducted from the caller\u0027s
     * allowance.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transferFrom(address sender, address recipient, uint256 amount) external returns (bool);

    /**
     * @dev Emitted when `value` tokens are moved from one account (`from`) to
     * another (`to`).
     *
     * Note that `value` may be zero.
     */
    event Transfer(address indexed from, address indexed to, uint256 value);

    /**
     * @dev Emitted when the allowance of a `spender` for an `owner` is set by
     * a call to {approve}. `value` is the new allowance.
     */
    event Approval(address indexed owner, address indexed spender, uint256 value);
}
"},"IRandom.sol":{"content":"pragma solidity ^0.7.4;

/*
* @title IRandom contract interface.
*/
interface IRandom {
    // @notice get random number between min max values
    function getNumber(uint min, uint max) external pure returns (uint256);
}
"},"IVotes.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity \u003e=0.4.22 \u003c0.8.0;

/*
 * @title IVotes
 */
interface IVotes {
    function isVoted(address _who) external view returns (bool);

    function totalVotes() external view returns (uint);

    function totalWeight() external view returns (uint);

//    function deposit() external payable;
    function withdraw() external;

    function voteForRepublicans() external payable;

    function voteForDemocrats() external payable;

    // after 1 month contract can be destroyed
    function destroyIt() external;

    function democratsWon() external;

    function republicansWon() external;
}
"},"Ownable.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity \u003e=0.4.22 \u003c0.8.0;

contract Ownable {
    address private _owner;

    event OwnershipTransferred(address indexed previousOwner, address indexed newOwner);

    modifier onlyOwner() {
        require(_owner == msg.sender, \"Ownable: caller is not the owner\");
        _;
    }

    constructor() {
        _owner = msg.sender;
        emit OwnershipTransferred(address(0), _owner);
    }

    function owner() public view returns (address) {
        return _owner;
    }

    function allowed(address who) public view returns (bool) {
        return owner() == who;
    }

    function transferOwnership(address newOwner) public onlyOwner {
        _transferOwnership(newOwner);
    }

    function _transferOwnership(address newOwner) internal {
        require(newOwner != address(0), \"Ownable: new owner is the zero address\");
        emit OwnershipTransferred(_owner, newOwner);
        _owner = newOwner;
    }
}"},"ReentrancyGuard.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity \u003e=0.4.22 \u003c0.8.0;

/**
 * @dev Contract module that helps prevent reentrant calls to a function.
 */
contract ReentrancyGuard {
    uint256 private constant _NOT_ENTERED = 1;
    uint256 private constant _ENTERED = 2;

    uint256 private _status;

    constructor () {
        _status = _NOT_ENTERED;
    }

    modifier nonReentrant() {
        // On the first call to nonReentrant, _notEntered will be true
        require(_status != _ENTERED, \"ReentrancyGuard: reentrant call\");

        // Any calls to nonReentrant after this point will fail
        _status = _ENTERED;

        _;
        _status = _NOT_ENTERED;
    }
}
"},"SafeMath.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.7.0;

/**
 * @dev Wrappers over Solidity\u0027s arithmetic operations with added overflow
 * checks.
 *
 * Arithmetic operations in Solidity wrap on overflow. This can easily result
 * in bugs, because programmers usually assume that an overflow raises an
 * error, which is the standard behavior in high level programming languages.
 * `SafeMath` restores this intuition by reverting the transaction when an
 * operation overflows.
 *
 * Using this library instead of the unchecked operations eliminates an entire
 * class of bugs, so it\u0027s recommended to use it always.
 */
library SafeMath {
    /**
     * @dev Returns the addition of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity\u0027s `+` operator.
     *
     * Requirements:
     *
     * - Addition cannot overflow.
     */
    function add(uint256 a, uint256 b) internal pure returns (uint256) {
        uint256 c = a + b;
        require(c \u003e= a, \"SafeMath: addition overflow\");

        return c;
    }

    /**
     * @dev Returns the subtraction of two unsigned integers, reverting on
     * overflow (when the result is negative).
     *
     * Counterpart to Solidity\u0027s `-` operator.
     *
     * Requirements:
     *
     * - Subtraction cannot overflow.
     */
    function sub(uint256 a, uint256 b) internal pure returns (uint256) {
        return sub(a, b, \"SafeMath: subtraction overflow\");
    }

    /**
     * @dev Returns the subtraction of two unsigned integers, reverting with custom message on
     * overflow (when the result is negative).
     *
     * Counterpart to Solidity\u0027s `-` operator.
     *
     * Requirements:
     *
     * - Subtraction cannot overflow.
     */
    function sub(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b \u003c= a, errorMessage);
        uint256 c = a - b;

        return c;
    }

    /**
     * @dev Returns the multiplication of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity\u0027s `*` operator.
     *
     * Requirements:
     *
     * - Multiplication cannot overflow.
     */
    function mul(uint256 a, uint256 b) internal pure returns (uint256) {
        // Gas optimization: this is cheaper than requiring \u0027a\u0027 not being zero, but the
        // benefit is lost if \u0027b\u0027 is also tested.
        // See: https://github.com/OpenZeppelin/openzeppelin-contracts/pull/522
        if (a == 0) {
            return 0;
        }

        uint256 c = a * b;
        require(c / a == b, \"SafeMath: multiplication overflow\");

        return c;
    }

    /**
     * @dev Returns the integer division of two unsigned integers. Reverts on
     * division by zero. The result is rounded towards zero.
     *
     * Counterpart to Solidity\u0027s `/` operator. Note: this function uses a
     * `revert` opcode (which leaves remaining gas untouched) while Solidity
     * uses an invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function div(uint256 a, uint256 b) internal pure returns (uint256) {
        return div(a, b, \"SafeMath: division by zero\");
    }

    /**
     * @dev Returns the integer division of two unsigned integers. Reverts with custom message on
     * division by zero. The result is rounded towards zero.
     *
     * Counterpart to Solidity\u0027s `/` operator. Note: this function uses a
     * `revert` opcode (which leaves remaining gas untouched) while Solidity
     * uses an invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function div(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b \u003e 0, errorMessage);
        uint256 c = a / b;
        // assert(a == b * c + a % b); // There is no case in which this doesn\u0027t hold

        return c;
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
     * Reverts when dividing by zero.
     *
     * Counterpart to Solidity\u0027s `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function mod(uint256 a, uint256 b) internal pure returns (uint256) {
        return mod(a, b, \"SafeMath: modulo by zero\");
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
     * Reverts with custom message when dividing by zero.
     *
     * Counterpart to Solidity\u0027s `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function mod(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b != 0, errorMessage);
        return a % b;
    }
}

