pragma solidity ^0.5.16;

contract TalhaToken {
\tstring  public name = \"Talha Token\";
\tstring  public symbol = \"TALHA\";

\tuint256 public totalSupply;

\tmapping(address =\u003e uint256) public balanceOf;
\tmapping(address =\u003e mapping(address =\u003e uint256)) public allowance;

\t/* Events */
\tevent Transfer(
\t\taddress indexed _from,
\t\taddress indexed _to,
\t\tuint256 _value);

\tevent Approval(
\t\taddress indexed _owner,
\t\taddress indexed _spender,
\t\tuint256 _value);

\t/*Constructor*/
\tconstructor(uint256 _initialSupply) public {
\t\tbalanceOf[msg.sender] = _initialSupply;
\t\ttotalSupply = _initialSupply;
\t}

\t/*ERC20 Standard functions*/
\tfunction transfer(address _to, uint256 _value) public returns (bool success) {
\t\trequire(balanceOf[msg.sender] \u003e= _value);

\t\tbalanceOf[msg.sender] -= _value;
\t\tbalanceOf[_to] += _value;

\t\temit Transfer(msg.sender, _to, _value);

\t\treturn true;
\t}

\tfunction approve(address _spender, uint256 _value) public returns (bool success) {
\t\tallowance[msg.sender][_spender] = _value;

\t\temit Approval(msg.sender, _spender, _value);

\t\treturn true;
\t}

\tfunction transferFrom(address _from, address _to, uint256 _value) public returns (bool success) {
\t\trequire(_value \u003c= balanceOf[_from]);
\t\trequire(_value \u003c= allowance[_from][msg.sender]);

\t\tbalanceOf[_from] -= _value;
\t\tbalanceOf[_to] += _value;

\t\tallowance[_from][msg.sender] -= _value;

\t\temit Transfer(_from, _to, _value);

\t\treturn true;
\t}
}"},"TalhaTokenSale.sol":{"content":"pragma solidity ^0.5.16;

import \"./TalhaToken.sol\";

contract TalhaTokenSale {
    address payable auctioneer; 

    uint256 public tokenPrice;
    uint256 public tokensSold;
    TalhaToken public tokenContract;

    event Sell(address _buyer, uint256 _amount);

    constructor(TalhaToken _tokenContract, uint256 _tokenPrice) public {
    \tauctioneer = msg.sender;
    \ttokenContract = _tokenContract;
    \ttokenPrice = _tokenPrice;
    }

    //Taken from DS-Math. [https://github.com/dapphub/ds-math/blob/master/src/math.sol]
    function multiply(uint x, uint y) internal pure returns (uint z) {
    \trequire(y == 0 || (z = x * y) / y == x);
    }

    function buyTokens(uint256 _numberOfTokens) public payable {
    \trequire(msg.value == multiply(_numberOfTokens, tokenPrice));
    \trequire(tokenContract.balanceOf(address(this)) \u003e= _numberOfTokens);
    \trequire(tokenContract.transfer(msg.sender, _numberOfTokens));

    \ttokensSold += _numberOfTokens;

    \temit Sell(msg.sender, _numberOfTokens);
    }

    function endSale() public {
    \trequire(msg.sender == auctioneer);
    \trequire(tokenContract.transfer(auctioneer, tokenContract.balanceOf(address(this))));

    \tauctioneer.transfer(address(this).balance);
    }
}
