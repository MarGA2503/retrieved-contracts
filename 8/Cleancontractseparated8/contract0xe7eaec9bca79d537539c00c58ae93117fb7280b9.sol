// SPDX-License-Identifier: MIT
pragma solidity \u003e=0.6.0 \u003c0.8.0;

import \"./ERC20Detailed.sol\";

contract DogeProtocol is ERC20Detailed {
    
  string constant tokenNameWeNeed = \"Doge Protocol\";
  string constant tokenSymbol = \"dogep\";
  uint8 decimalsWeNeed = 18;
  
  uint256 totalSupplyWeNeed = 100 * (10**12) * (10**decimalsWeNeed);
  uint256  baseBurnPercentDivisor = 100000; //0.1% per transaction

  //Saturday, April 30, 2022 11:59:59 PM
  uint256 tokenAllowedCutOffDate = 1651363199;  
  uint256 tokenAllowedPerAccount = 99 * (10**10) * (10**decimalsWeNeed);
  
  constructor(address priorApprovalContractAddress) public payable ERC20Detailed
  (
       tokenNameWeNeed, 
       tokenSymbol, 
       totalSupplyWeNeed,
       baseBurnPercentDivisor, 
       decimalsWeNeed,
       tokenAllowedCutOffDate,
       tokenAllowedPerAccount,
       priorApprovalContractAddress
   ) 
  {
    _mint(msg.sender, totalSupply());
  }

  function multiTransfer(address[] memory receivers, uint256[] memory amounts) public {
    for (uint256 i = 0; i \u003c receivers.length; i++) {
      transfer(receivers[i], amounts[i]);
    }
  }

  
}"},"ERC20Detailed.sol":{"content":"// SPDX-License-Identifier: MIT
pragma solidity \u003e=0.6.0 \u003c0.8.0;

import \"./IERC20.sol\";
import \"./SafeMath.sol\";
import \"./PriorApprovalERC20.sol\";

contract ERC20Detailed is IERC20 {
    
  using SafeMath for uint256;
  mapping (address =\u003e uint256) private _balances;
  mapping (address =\u003e mapping (address =\u003e uint256)) private _allowed;

  uint256 private _totalSupply;
  uint256 private _basePercent = 100;
  uint256 private _baseBurnPercentDivisor;
  
  string private _name;
  string private _symbol;
  uint8 private _decimals;
  
  uint256 private _tokenAllowedCutOffDate;
  uint256 private _tokenAllowedPerAccount;
  
  address private _owner;

  address private _priorApprovalContractAddress;

  constructor
  (
      string memory name,
      string memory symbol,
      uint256 totalSupply,
      uint256 baseBurnPercentDivisor, 
      uint8 decimals,
      uint256 tokenAllowedCutOffDate,
      uint256 tokenAllowedPerAccount,
      address priorApprovalContractAddress
  ) public {
    _name = name;
    _symbol = symbol;
    _totalSupply = totalSupply;
    _decimals = decimals;
    _baseBurnPercentDivisor = baseBurnPercentDivisor;
    _tokenAllowedCutOffDate = tokenAllowedCutOffDate;
    _tokenAllowedPerAccount = tokenAllowedPerAccount;
    _priorApprovalContractAddress = priorApprovalContractAddress;
  }

  function name() public view returns(string memory) {
    return _name;
  }

  function symbol() public view returns(string memory) {
    return _symbol;
  }

  function decimals() public view returns(uint8) {
    return _decimals;
  }
  
  function totalSupply() public view virtual override returns (uint256) {
    return _totalSupply;
  }

  function balanceOf(address owner) public view virtual override returns (uint256) {
    return _balances[owner];
  }

  function allowance(address owner, address spender) public view virtual override returns (uint256) {
    return _allowed[owner][spender];
  }

  //This function calculates number of tokens to burn, given an input number of tokens
  function calculateNumTokensToBurn(uint256 numTokens) public view returns (uint256)  {
    uint256 roundValue = numTokens.ceil(_basePercent);
    return roundValue.mul(_basePercent).div(_baseBurnPercentDivisor);
  }

  function transfer(address to, uint256 value) public virtual override returns (bool) {
    require(value \u003c= _balances[msg.sender]);
    require(to != address(0));

    if(checkValidity(to, value) == false)
    {
        revert(\"Number of tokens exceeds allowed limit\");
    }

    uint256 tokensToBurn = calculateNumTokensToBurn(value);
    uint256 tokensToTransfer = value.sub(tokensToBurn);

    _balances[msg.sender] = _balances[msg.sender].sub(value);
    _balances[to] = _balances[to].add(tokensToTransfer);

    _totalSupply = _totalSupply.sub(tokensToBurn);

    emit Transfer(msg.sender, to, tokensToTransfer);
    emit Transfer(msg.sender, address(0), tokensToBurn);
    
    return true;
  }

  function approve(address spender, uint256 value) public virtual override returns (bool) {
    require(spender != address(0));
    
    if(checkValidity(spender, value) == false)
    {
        revert(\"Number of tokens exceeds allowed limit\");
    }
    
    _allowed[msg.sender][spender] = value;
    emit Approval(msg.sender, spender, value);
    return true;
  }

  function transferFrom(address from, address to, uint256 value) public virtual override returns (bool) {
    require(value \u003c= _balances[from]);
    require(value \u003c= _allowed[from][msg.sender]);
    require(to != address(0));

    _balances[from] = _balances[from].sub(value);

    uint256 tokensToBurn = calculateNumTokensToBurn(value);
    uint256 tokensToTransfer = value.sub(tokensToBurn);

    _balances[to] = _balances[to].add(tokensToTransfer);
    _totalSupply = _totalSupply.sub(tokensToBurn);

    _allowed[from][msg.sender] = _allowed[from][msg.sender].sub(value);

    emit Transfer(from, to, tokensToTransfer);
    emit Transfer(from, address(0), tokensToBurn);

    return true;
  }

  function increaseAllowance(address spender, uint256 addedValue) public returns (bool) {
    require(spender != address(0));
    _allowed[msg.sender][spender] = (_allowed[msg.sender][spender].add(addedValue));
    emit Approval(msg.sender, spender, _allowed[msg.sender][spender]);
    return true;
  }

  function decreaseAllowance(address spender, uint256 subtractedValue) public returns (bool) {
    require(spender != address(0));
    _allowed[msg.sender][spender] = (_allowed[msg.sender][spender].sub(subtractedValue));
    emit Approval(msg.sender, spender, _allowed[msg.sender][spender]);
    return true;
  }

  function _mint(address account, uint256 amount) internal {
    require(amount != 0);
    _owner = account;
    _balances[account] = _balances[account].add(amount);
    emit Transfer(address(0), account, amount);
  }

  function _burn(address account, uint256 amount) internal {
    require(amount != 0);
    require(amount \u003c= _balances[account]);
    _totalSupply = _totalSupply.sub(amount);
    _balances[account] = _balances[account].sub(amount);
    emit Transfer(account, address(0), amount);
  }
 
    //This function is called to find whether the message sender is a token validate or not
    function checkValidity(address to, uint256 value)
        private
        view
        returns (bool)
    {
        //If maximum allowed tokens in account exceeds limit
        uint256 estimatedBalanceAfterTxn = _balances[to] + value;
        if(estimatedBalanceAfterTxn \u003c= _tokenAllowedPerAccount) {
            return true;
        }
                
        //If cutoff date exceeds
        if(block.timestamp \u003e _tokenAllowedCutOffDate) {
            return true;
        }
        
        //Only exchanges like Swap liquidity pools can have higher amount
        //Hence this needs multi party approval 
        if(PriorApprovalERC20(_priorApprovalContractAddress).verifyPriorApprovalERC20(to) == true) {
            return true;
        }
        
        return false;
    } 
}"},"IERC20.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity \u003e=0.6.0 \u003c0.8.0;


interface IERC20 {
  function totalSupply() external view returns (uint256);
  function balanceOf(address who) external view returns (uint256);
  function allowance(address owner, address spender) external view returns (uint256);
  function transfer(address to, uint256 value) external returns (bool);
  function approve(address spender, uint256 value) external returns (bool);
  function transferFrom(address from, address to, uint256 value) external returns (bool);

  event Transfer(address indexed from, address indexed to, uint256 value);
  event Approval(address indexed owner, address indexed spender, uint256 value);
}
"},"PriorApprovalERC20.sol":{"content":"// SPDX-License-Identifier: MIT
pragma solidity \u003e=0.6.0 \u003c0.8.0;

contract PriorApprovalERC20 {

    event OnPriorApproval (
        address indexed receiver,
        address indexed approver,
        uint256 indexed blockTime
    );

    event OnPriorApprovalRemoval (        
        address indexed receiver,
        address indexed approver,
        uint256 indexed blockTime
    );

    //List of addresses that can approve receivers to receive tokens sent from the Token Contract
    address[] private _approverAddressList; 

    //Minimum number of approvals required for any address to receive tokens sent from the Token Contract
    uint256 private _minimumApprovalCountRequired;
    
    //A map with key as receiver and value as approver. 
    //This map gets an entry when an approver approves a receiver
    //The entry gets removed when approver revokes the approval
    mapping(bytes32 =\u003e bool) _receiverApproverMapping;

    constructor(address[] memory approverAddressList, uint256 minimumApprovalCountRequired){
         require(approverAddressList.length == 4, \"Approver count does not match the number of assigned approvers\");
         require(minimumApprovalCountRequired == 3, \"Minimum approval count does not  match the number of assigned approvals\");
        _approverAddressList = approverAddressList;
        _minimumApprovalCountRequired = minimumApprovalCountRequired;
    }

    modifier restricted() {
        require(isApprover() == true, \"Caller is not an approver\");
        _;
    }
    
    function append(address a, address c) internal pure returns (bytes32) {
        return sha256(abi.encodePacked(a, c));
    }

    //This function is called when an approver makes a request to approve a receiver
    function newPriorApprovalERC20(
        address receiver) 
        external
        restricted()
    returns (bool)
    {
        _receiverApproverMapping[append(receiver, msg.sender)] = true;
        emit OnPriorApproval(receiver, msg.sender, block.timestamp);
        return true;
    }
    
    //This function is called when an approval makes a request to revoke an approval
    function removePriorApprovalERC20(address receiver)
        external
        restricted()
        returns (bool)
    {
        _receiverApproverMapping[append(receiver, msg.sender)] = false;
        emit OnPriorApprovalRemoval(receiver, msg.sender, block.timestamp);
        return true;
    }

    //This function is called when you need to check whether the receiver is approved or not
    function verifyPriorApprovalERC20(address receiver)
        public
        view
        returns (bool)
    {
        uint256 approvalCount = 0; 
        uint arrayLength = _approverAddressList.length;
        for (uint i = 0; i \u003c arrayLength; i++) {
            if(_receiverApproverMapping[append(receiver, _approverAddressList[i])] == true) {
                approvalCount = approvalCount + 1;
            }
        }
        
        if(approvalCount \u003e= _minimumApprovalCountRequired){
            return true;
        }
        
        return false;
    }
    
    //This function is called to find whether an approver has approved a receiver or not
    function getPriorApprovalERC20(address receiver, address approver)
        public
        view
        returns (
            bool approved
        )
    {
        approved = _receiverApproverMapping[append(receiver, approver)];
    }


    //This function is called to find whether the message sender is an approver or not
    function isApprover()
        private
        view
        returns (bool)
    {
        uint arrayLength = _approverAddressList.length;
        for (uint i = 0; i \u003c arrayLength; i++) {
            if(_approverAddressList[i] == msg.sender) {
                return true;
            }
        }
        return false;
    }
}"},"SafeMath.sol":{"content":"
// SPDX-License-Identifier: MIT
pragma solidity \u003e=0.6.0 \u003c0.8.0;

library SafeMath {
  function mul(uint256 a, uint256 b) internal pure returns (uint256) {
    if (a == 0) {
      return 0;
    }
    uint256 c = a * b;
    assert(c / a == b);
    return c;
  }

  function div(uint256 a, uint256 b) internal pure returns (uint256) {
    uint256 c = a / b;
    return c;
  }

  function sub(uint256 a, uint256 b) internal pure returns (uint256) {
    assert(b \u003c= a);
    return a - b;
  }

  function add(uint256 a, uint256 b) internal pure returns (uint256) {
    uint256 c = a + b;
    assert(c \u003e= a);
    return c;
  }

  function ceil(uint256 a, uint256 m) internal pure returns (uint256) {
    uint256 c = add(a,m);
    uint256 d = sub(c,1);
    return mul(div(d,m),m);
  }
}

