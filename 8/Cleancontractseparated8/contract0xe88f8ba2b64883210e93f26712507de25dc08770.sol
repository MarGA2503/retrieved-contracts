// Copyright (C) 2020 Easy Chain. <https://easychain.tech>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

pragma solidity 0.6.5;
pragma experimental ABIEncoderV2;

import { ERC20 } from \"../../ERC20.sol\";
import { TokenMetadata, Component } from \"../../Structs.sol\";
import { TokenAdapter } from \"../TokenAdapter.sol\";
import { ProtocolAdapter } from \"../ProtocolAdapter.sol\";
import { TypedToken } from \"./BerezkaTokenAdapterGovernance.sol\";


interface IBerezkaTokenAdapterGovernance {
    
    function listTokens() external view returns (TypedToken[] memory);

    function listProtocols() external view returns (address[] memory);

    function listEthProtocols() external view returns (address[] memory);

    function listProducts() external view returns (address[] memory);

    function getVaults(address _token) external view returns (address[] memory);
}


/**
 * @title Token adapter for Berezka DAO.
 * @dev Implementation of TokenAdapter interface.
 * @author Vasin Denis <denis.vasin@easychain.tech>
 */
contract BerezkaTokenAdapter is TokenAdapter {

    address internal constant ETH = 0xEeeeeEeeeEeEeeEeEeEeeEEEeeeeEeeeeeeeEEeE;

    string internal constant ERC20_TOKEN = \"ERC20\";

    IBerezkaTokenAdapterGovernance immutable private governance;

    constructor(address _governance) public {
        governance = IBerezkaTokenAdapterGovernance(_governance);
    }

    /**
     * @return TokenMetadata struct with ERC20-style token info.
     * @dev Implementation of TokenAdapter interface function.
     */
    function getMetadata(address token) 
        external 
        view 
        override 
        returns (TokenMetadata memory) 
    {
        return TokenMetadata({
            token: token,
            name: ERC20(token).name(),
            symbol: ERC20(token).symbol(),
            decimals: ERC20(token).decimals()
        });
    }

    /**
     * @return Array of Component structs with underlying tokens rates for the given token.
     * @dev Implementation of TokenAdapter interface function.
     */
    function getComponents(address token)
        external
        view
        override
        returns (Component[] memory)
    {
        address[] memory vaults = governance.getVaults(token);
        TypedToken[] memory assets = governance.listTokens();
        address[] memory debtAdapters = governance.listProtocols();
        uint256 length = assets.length;
        uint256 totalSupply = ERC20(token).totalSupply();

        Component[] memory underlyingTokens = new Component[](1 + length);
        
        // Handle ERC20 assets + debt
        for (uint256 i = 0; i < length; i++) {
            Component memory tokenComponent =
                _getTokenComponents(
                    assets[i].token, 
                    assets[i].tokenType, 
                    vaults, 
                    debtAdapters, 
                    totalSupply
                );
            underlyingTokens[i] = tokenComponent;
        }

        // Handle ETH
        {
            Component memory ethComponent = _getEthComponents(vaults, totalSupply);
            underlyingTokens[length] = ethComponent;
        }
        
        return underlyingTokens;
    }

    // Internal functions

    function _getEthComponents(
        address[] memory _vaults,
        uint256 _totalSupply
    )
        internal
        view
        returns (Component memory)
    {
        address[] memory debtsInEth = governance.listEthProtocols();

        uint256 ethBalance = 0;
        uint256 ethDebt = 0;

        // Compute negative amount for a given asset using all debt adapters
        for (uint256 j = 0; j < _vaults.length; j++) {
            address vault = _vaults[j];
            ethBalance += vault.balance;
            ethDebt += _computeDebt(debtsInEth, ETH, vault);
        }

        return Component({
            token: ETH,
            tokenType: ERC20_TOKEN,
            rate: (ethBalance * 1e18 / _totalSupply) - (ethDebt * 1e18 / _totalSupply)
        });
    }

    function _getTokenComponents(
        address _asset,
        string memory _type,
        address[] memory _vaults,
        address[] memory _debtAdapters,
        uint256 _totalSupply
    ) 
        internal
        view
        returns (Component memory)
    {
        uint256 componentBalance = 0;
        uint256 componentDebt = 0;

        // Compute positive amount for a given asset
        uint256 vaultsLength = _vaults.length;
        for (uint256 j = 0; j < vaultsLength; j++) {
            address vault = _vaults[j];
            componentBalance += ERC20(_asset).balanceOf(vault);
            componentDebt += _computeDebt(_debtAdapters, _asset, vault);
        }

        // Asset amount
        return(Component({
            token: _asset,
            tokenType: _type,
            rate: (componentBalance * 1e18 / _totalSupply) - (componentDebt * 1e18 / _totalSupply)
        }));
    }

    function _computeDebt(
        address[] memory _debtAdapters,
        address _asset,
        address _vault
    ) 
        internal
        view
        returns (uint256)
    {
        // Compute negative amount for a given asset using all debt adapters
        uint256 componentDebt = 0;
        uint256 debtsLength = _debtAdapters.length;
        for (uint256 k = 0; k < debtsLength; k++) {
            ProtocolAdapter debtAdapter = ProtocolAdapter(_debtAdapters[k]);
            try debtAdapter.getBalance(_asset, _vault) returns (uint256 _amount) {
                componentDebt += _amount;
            } catch {} // solhint-disable-line no-empty-blocks
        }
        return componentDebt;
    }
}
"
    
// Copyright (C) 2020 Zerion Inc. <https://zerion.io>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

pragma solidity 0.6.5;
pragma experimental ABIEncoderV2;


interface ERC20 {
    function approve(address, uint256) external returns (bool);
    function transfer(address, uint256) external returns (bool);
    function transferFrom(address, address, uint256) external returns (bool);
    function name() external view returns (string memory);
    function symbol() external view returns (string memory);
    function decimals() external view returns (uint8);
    function totalSupply() external view returns (uint256);
    function balanceOf(address) external view returns (uint256);
}
"
    
// Copyright (C) 2020 Zerion Inc. <https://zerion.io>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

pragma solidity 0.6.5;
pragma experimental ABIEncoderV2;


struct ProtocolBalance {
    ProtocolMetadata metadata;
    AdapterBalance[] adapterBalances;
}


struct ProtocolMetadata {
    string name;
    string description;
    string websiteURL;
    string iconURL;
    uint256 version;
}


struct AdapterBalance {
    AdapterMetadata metadata;
    FullTokenBalance[] balances;
}


struct AdapterMetadata {
    address adapterAddress;
    string adapterType; // \"Asset\", \"Debt\"
}


// token and its underlying tokens (if exist) balances
struct FullTokenBalance {
    TokenBalance base;
    TokenBalance[] underlying;
}


struct TokenBalance {
    TokenMetadata metadata;
    uint256 amount;
}


// ERC20-style token metadata
// 0xEeeeeEeeeEeEeeEeEeEeeEEEeeeeEeeeeeeeEEeE address is used for ETH
struct TokenMetadata {
    address token;
    string name;
    string symbol;
    uint8 decimals;
}


struct Component {
    address token;
    string tokenType;  // \"ERC20\" by default
    uint256 rate;  // price per full share (1e18)
}
"
    
// Copyright (C) 2020 Zerion Inc. <https://zerion.io>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

pragma solidity 0.6.5;
pragma experimental ABIEncoderV2;

import { TokenMetadata, Component } from \"../Structs.sol\";


/**
 * @title Token adapter interface.
 * @dev getMetadata() and getComponents() functions MUST be implemented.
 * @author Igor Sobolev <sobolev@zerion.io>
 */
interface TokenAdapter {

    /**
     * @dev MUST return TokenMetadata struct with ERC20-style token info.
     * struct TokenMetadata {
     *     address token;
     *     string name;
     *     string symbol;
     *     uint8 decimals;
     * }
     */
    function getMetadata(address token) external view returns (TokenMetadata memory);

    /**
     * @dev MUST return array of Component structs with underlying tokens rates for the given token.
     * struct Component {
     *     address token;    // Address of token contract
     *     string tokenType; // Token type (\"ERC20\" by default)
     *     uint256 rate;     // Price per share (1e18)
     * }
     */
    function getComponents(address token) external view returns (Component[] memory);
}
"
    
// Copyright (C) 2020 Zerion Inc. <https://zerion.io>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

pragma solidity 0.6.5;
pragma experimental ABIEncoderV2;


/**
 * @title Protocol adapter interface.
 * @dev adapterType(), tokenType(), and getBalance() functions MUST be implemented.
 * @author Igor Sobolev <sobolev@zerion.io>
 */
interface ProtocolAdapter {

    /**
     * @dev MUST return \"Asset\" or \"Debt\".
     * SHOULD be implemented by the public constant state variable.
     */
    function adapterType() external pure returns (string memory);

    /**
     * @dev MUST return token type (default is \"ERC20\").
     * SHOULD be implemented by the public constant state variable.
     */
    function tokenType() external pure returns (string memory);

    /**
     * @dev MUST return amount of the given token locked on the protocol by the given account.
     */
    function getBalance(address token, address account) external view returns (uint256);
}
"
    
// Copyright (C) 2020 Easy Chain. <https://easychain.tech>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

pragma solidity 0.6.5;
pragma experimental ABIEncoderV2;

import { EnumerableSet } from \"./lib/EnumerableSet.sol\";
import { Ownable } from \"../../Ownable.sol\";

interface AdapterRegistry {

    function isValidTokenAdapter(
        string calldata tokenAdapterName
    ) 
        external 
        returns (bool);
}

struct TypedToken {
    string tokenType;
    address token;
}

/**
 * @dev BerezkaTokenAdapterGovernance contract.
 * Main function of this contract is to maintains a Structure of BerezkaDAO
 * @author Vasin Denis <denis.vasin@easychain.tech>
 */
contract BerezkaTokenAdapterGovernance is Ownable() {

    AdapterRegistry internal constant ADAPTER_REGISTRY = 
        AdapterRegistry(0x06FE76B2f432fdfEcAEf1a7d4f6C3d41B5861672);

    using EnumerableSet for EnumerableSet.AddressSet;

    /// @dev This is a set of plain assets (ERC20) used by DAO. 
    /// This list also include addresses of Uniswap/Balancer tokenized pools.
    mapping (string => EnumerableSet.AddressSet) private tokens;

    /// @dev This is a list of all token types that are managed by contract
    /// New token type is added to this list upon first adding a token with given type
    string[] public tokenTypes;

    /// @dev This is a set of debt protocol adapters that return debt in ETH
    EnumerableSet.AddressSet private ethProtocols;

    /// @dev This is a set of debt protocol adapters that return debt for ERC20 tokens
    EnumerableSet.AddressSet private protocols;

    /// @dev This is a mapping from Berezka DAO product to corresponding Vault addresses
    mapping(address => address[]) private productVaults;

    constructor(address[] memory _protocols, address[] memory _ethProtocols) public {
        _add(protocols, _protocols);
        _add(ethProtocols, _ethProtocols);
    }

    // Modification functions (all only by owner)

    function setProductVaults(address _product, address[] memory _vaults) public onlyOwner() {
        require(_product != address(0), \"_product is 0\");
        require(_vaults.length > 0, \"_vaults.length should be > 0\");

        productVaults[_product] = _vaults;
    }

    function removeProduct(address _product) public onlyOwner() {
        require(_product != address(0), \"_product is 0\");

        delete productVaults[_product];
    }

    function addTokens(string memory _type, address[] memory _tokens) public onlyOwner() {
        require(_tokens.length > 0, \"Length should be > 0\");
        require(ADAPTER_REGISTRY.isValidTokenAdapter(_type), \"Invalid token adapter name\");

        if (tokens[_type].length() == 0) {
            tokenTypes.push(_type);
        }
        _add(tokens[_type], _tokens);
    }

    function addProtocols(address[] memory _protocols) public onlyOwner() {
        require(_protocols.length > 0, \"Length should be > 0\");

        _add(protocols, _protocols);
    }

    function addEthProtocols(address[] memory _ethProtocols) public onlyOwner() {
        require(_ethProtocols.length > 0, \"Length should be > 0\");

        _add(ethProtocols, _ethProtocols);
    }

    function removeTokens(string memory _type, address[] memory _tokens) public onlyOwner() {
        require(_tokens.length > 0, \"Length should be > 0\");

        _remove(tokens[_type], _tokens);
    }

    function removeProtocols(address[] memory _protocols) public onlyOwner() {
        require(_protocols.length > 0, \"Length should be > 0\");

        _remove(protocols, _protocols);
    }

    function removeEthProtocols(address[] memory _ethProtocols) public onlyOwner() {
        require(_ethProtocols.length > 0, \"Length should be > 0\");

        _remove(ethProtocols, _ethProtocols);
    }

    function setTokenTypes(string[] memory _tokenTypes) public onlyOwner() {
        require(_tokenTypes.length > 0, \"Length should be > 0\");

        tokenTypes = _tokenTypes;
    }

    // View functions

    function listTokens() external view returns (TypedToken[] memory) {
        uint256 tokenLength = tokenTypes.length;
        uint256 resultLength = 0;
        for (uint256 i = 0; i < tokenLength; i++) {
            resultLength += tokens[tokenTypes[i]].length();
        }
        TypedToken[] memory result = new TypedToken[](resultLength);
        uint256 resultIndex = 0;
        for (uint256 i = 0; i < tokenLength; i++) {
            string memory tokenType = tokenTypes[i];
            address[] memory typedTokens = _list(tokens[tokenType]);
            uint256 typedTokenLength = typedTokens.length;
            for (uint256 j = 0; j < typedTokenLength; j++) {
                result[resultIndex] = TypedToken(tokenType, typedTokens[j]);
                resultIndex++;
            }
        }
        return result;
    }

    function listTokens(string calldata _type) external view returns (address[] memory) {
        return _list(tokens[_type]);
    }

    function listProtocols() external view returns (address[] memory) {
        return _list(protocols);
    }

    function listEthProtocols() external view returns (address[] memory) {
        return _list(ethProtocols);
    }

    function getVaults(address _token) external view returns (address[] memory) {
        return productVaults[_token];
    }

    // Internal functions

    function _add(EnumerableSet.AddressSet storage _set, address[] memory _addresses) internal {
        for (uint i = 0; i < _addresses.length; i++) {
            _set.add(_addresses[i]);
        }
    }

    function _remove(EnumerableSet.AddressSet storage _set, address[] memory _addresses) internal {
        for (uint i = 0; i < _addresses.length; i++) {
            _set.remove(_addresses[i]);
        }
    }

    function _list(EnumerableSet.AddressSet storage _set) internal view returns(address[] memory) {
        address[] memory result = new address[](_set.length());
        for (uint i = 0; i < _set.length(); i++) {
            result[i] = _set.at(i);
        }
        return result;
    }
}
"
    
// SPDX-License-Identifier: MIT

pragma solidity 0.6.5;

/**
 * @dev Library for managing
 * https://en.wikipedia.org/wiki/Set_(abstract_data_type)[sets] of primitive
 * types.
 *
 * Sets have the following properties:
 *
 * - Elements are added, removed, and checked for existence in constant time
 * (O(1)).
 * - Elements are enumerated in O(n). No guarantees are made on the ordering.
 *
 * ```
 * contract Example {
 *     // Add the library methods
 *     using EnumerableSet for EnumerableSet.AddressSet;
 *
 *     // Declare a set state variable
 *     EnumerableSet.AddressSet private mySet;
 * }
 * ```
 *
 * As of v3.0.0, only sets of type `address` (`AddressSet`) and `uint256`
 * (`UintSet`) are supported.
 */
library EnumerableSet {
    // To implement this library for multiple types with as little code
    // repetition as possible, we write it in terms of a generic Set type with
    // bytes32 values.
    // The Set implementation uses private functions, and user-facing
    // implementations (such as AddressSet) are just wrappers around the
    // underlying Set.
    // This means that we can only create new EnumerableSets for types that fit
    // in bytes32.

    struct Set {
        // Storage of set values
        bytes32[] _values;

        // Position of the value in the `values` array, plus 1 because index 0
        // means a value is not in the set.
        mapping (bytes32 => uint256) _indexes;
    }

    /**
     * @dev Add a value to a set. O(1).
     *
     * Returns true if the value was added to the set, that is if it was not
     * already present.
     */
    function _add(Set storage set, bytes32 value) private returns (bool) {
        if (!_contains(set, value)) {
            set._values.push(value);
            // The value is stored at length-1, but we add 1 to all indexes
            // and use 0 as a sentinel value
            set._indexes[value] = set._values.length;
            return true;
        } else {
            return false;
        }
    }

    /**
     * @dev Removes a value from a set. O(1).
     *
     * Returns true if the value was removed from the set, that is if it was
     * present.
     */
    function _remove(Set storage set, bytes32 value) private returns (bool) {
        // We read and store the value's index to prevent multiple reads from the same storage slot
        uint256 valueIndex = set._indexes[value];

        if (valueIndex != 0) { // Equivalent to contains(set, value)
            // To delete an element from the _values array in O(1), we swap the element to delete with the last one in
            // the array, and then remove the last element (sometimes called as 'swap and pop').
            // This modifies the order of the array, as noted in {at}.

            uint256 toDeleteIndex = valueIndex - 1;
            uint256 lastIndex = set._values.length - 1;

            // When the value to delete is the last one, the swap operation is unnecessary. However, since this occurs
            // so rarely, we still do the swap anyway to avoid the gas cost of adding an 'if' statement.

            bytes32 lastvalue = set._values[lastIndex];

            // Move the last value to the index where the value to delete is
            set._values[toDeleteIndex] = lastvalue;
            // Update the index for the moved value
            set._indexes[lastvalue] = toDeleteIndex + 1; // All indexes are 1-based

            // Delete the slot where the moved value was stored
            set._values.pop();

            // Delete the index for the deleted slot
            delete set._indexes[value];

            return true;
        } else {
            return false;
        }
    }

    /**
     * @dev Returns true if the value is in the set. O(1).
     */
    function _contains(Set storage set, bytes32 value) private view returns (bool) {
        return set._indexes[value] != 0;
    }

    /**
     * @dev Returns the number of values on the set. O(1).
     */
    function _length(Set storage set) private view returns (uint256) {
        return set._values.length;
    }

   /**
    * @dev Returns the value stored at position `index` in the set. O(1).
    *
    * Note that there are no guarantees on the ordering of values inside the
    * array, and it may change when more values are added or removed.
    *
    * Requirements:
    *
    * - `index` must be strictly less than {length}.
    */
    function _at(Set storage set, uint256 index) private view returns (bytes32) {
        require(set._values.length > index, \"EnumerableSet: index out of bounds\");
        return set._values[index];
    }

    // AddressSet

    struct AddressSet {
        Set _inner;
    }

    /**
     * @dev Add a value to a set. O(1).
     *
     * Returns true if the value was added to the set, that is if it was not
     * already present.
     */
    function add(AddressSet storage set, address value) internal returns (bool) {
        return _add(set._inner, bytes32(uint256(value)));
    }

    /**
     * @dev Removes a value from a set. O(1).
     *
     * Returns true if the value was removed from the set, that is if it was
     * present.
     */
    function remove(AddressSet storage set, address value) internal returns (bool) {
        return _remove(set._inner, bytes32(uint256(value)));
    }

    /**
     * @dev Returns true if the value is in the set. O(1).
     */
    function contains(AddressSet storage set, address value) internal view returns (bool) {
        return _contains(set._inner, bytes32(uint256(value)));
    }

    /**
     * @dev Returns the number of values in the set. O(1).
     */
    function length(AddressSet storage set) internal view returns (uint256) {
        return _length(set._inner);
    }

   /**
    * @dev Returns the value stored at position `index` in the set. O(1).
    *
    * Note that there are no guarantees on the ordering of values inside the
    * array, and it may change when more values are added or removed.
    *
    * Requirements:
    *
    * - `index` must be strictly less than {length}.
    */
    function at(AddressSet storage set, uint256 index) internal view returns (address) {
        return address(uint256(_at(set._inner, index)));
    }


    // UintSet

    struct UintSet {
        Set _inner;
    }

    /**
     * @dev Add a value to a set. O(1).
     *
     * Returns true if the value was added to the set, that is if it was not
     * already present.
     */
    function add(UintSet storage set, uint256 value) internal returns (bool) {
        return _add(set._inner, bytes32(value));
    }

    /**
     * @dev Removes a value from a set. O(1).
     *
     * Returns true if the value was removed from the set, that is if it was
     * present.
     */
    function remove(UintSet storage set, uint256 value) internal returns (bool) {
        return _remove(set._inner, bytes32(value));
    }

    /**
     * @dev Returns true if the value is in the set. O(1).
     */
    function contains(UintSet storage set, uint256 value) internal view returns (bool) {
        return _contains(set._inner, bytes32(value));
    }

    /**
     * @dev Returns the number of values on the set. O(1).
     */
    function length(UintSet storage set) internal view returns (uint256) {
        return _length(set._inner);
    }

   /**
    * @dev Returns the value stored at position `index` in the set. O(1).
    *
    * Note that there are no guarantees on the ordering of values inside the
    * array, and it may change when more values are added or removed.
    *
    * Requirements:
    *
    * - `index` must be strictly less than {length}.
    */
    function at(UintSet storage set, uint256 index) internal view returns (uint256) {
        return uint256(_at(set._inner, index));
    }
}"
    
// Copyright (C) 2020 Zerion Inc. <https://zerion.io>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

pragma solidity 0.6.5;
pragma experimental ABIEncoderV2;


abstract contract Ownable {

    modifier onlyOwner {
        require(msg.sender == owner, \"O: onlyOwner function!\");
        _;
    }

    address public owner;

    event OwnershipTransferred(address indexed previousOwner, address indexed newOwner);

    /**
     * @notice Initializes owner variable with msg.sender address.
     */
    constructor() internal {
        owner = msg.sender;
        emit OwnershipTransferred(address(0), msg.sender);
    }

    /**
     * @notice Transfers ownership to the desired address.
     * The function is callable only by the owner.
     */
    function transferOwnership(address _owner) external onlyOwner {
        require(_owner != address(0), \"O: new owner is the zero address!\");
        emit OwnershipTransferred(owner, _owner);
        owner = _owner;
    }
}
"
    }
  
