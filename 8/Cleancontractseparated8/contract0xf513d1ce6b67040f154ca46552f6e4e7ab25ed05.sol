pragma solidity ^0.6.12;

interface DaiErc20 {
    function transfer(address, uint) external returns (bool);
    function transferFrom(address,address,uint256) external returns (bool);
    function approve(address,uint256) external returns (bool);
    function balanceOf(address) external view returns (uint);
    function allowance(address, address) external view returns (uint);
}

"},"mathlib.sol":{"content":"pragma solidity ^0.6.12;

library mathlib
{
     
     // --- Math functions as implemented in DAI ERC20 Token---
    function add(uint x, uint y) internal pure returns (uint z) {
        require((z = x + y) \u003e= x);
    }
    
    function sub(uint x, uint y) internal pure returns (uint z) {
        require((z = x - y) \u003c= x);
    }
    
    function mul(uint x, uint y) internal pure returns (uint z) {
        require(y == 0 || (z = x * y) / y == x);
    }

   function calculatereservationdays(uint rstart,uint rend) internal pure returns(uint)
    {
        /*
\t    1)Calculates the length of stay between two dates.
\t    2)For example the number of days between reservation start and end.\t\t
            3)Days are rounded, so 5.6 days becomes 6 days. 0.5 days become 1 day.
        */
        
\t    require(rend \u003e rstart,\"Reservation End has to be greater than Reservation Start\");

        uint diff = sub(rend,rstart);
        
        uint dlen = diff / 86400; //div is Math.floor()
        
        uint md = diff % 86400;
        
        return md \u003e= 43200 ? add(dlen,1) : dlen;
        
    }
    
}

"},"owned.sol":{"content":"pragma solidity ^0.6.12;

contract owned
{
    /*
        1) Allows the manager to pause the main Factory contract
        2) Only the Factory contract is owned.
        3) The Manager has no control over the Reservation
    */
    
    address public manager;
    
    constructor() public 
\t{
\t    manager = msg.sender;
\t}


    modifier onlyManager()
    {
        require(msg.sender == manager);
        _;
    }
    

    function setManager(address newmanager) external onlyManager
    {
        /*
            Allows the current manager to set a new manager
        */
        
        require(newmanager.balance \u003e 0);
        manager = newmanager;
    }
    
}




"},"stayusb.sol":{"content":"pragma solidity ^0.6.12;

import \"./owned.sol\";
import \"./mathlib.sol\";
import \"./interfaces.sol\";


enum reservationstatus {CANCELLED, ACTIVATED, COMPLETED}


contract ReservationFactory is owned
{
    /*
        1) The Reservation Factory contract to create and manage reservations
\t    2) Only the Reservation Factory is owned by the manager
\t    3) The manager has no control over each reservation or the cumulative advance payment locked in the Factory contract
    */
    
    address constant private dai_ = 0x6B175474E89094C44Da98b954EedeAC495271d0F;
    DaiErc20 constant private daiToken  = DaiErc20(dai_);

    //Reservation Fee in wad payed to the manager to create a reservation
    uint public reservationfee;
    
    uint constant private secondsinday = 86400;
    uint constant private secondsin21hrs = 75600;
    
    //Switch that controls whether the factory is active
    bool public factorycontractactive;
    
    uint private reservationid;
    
    uint constant private maxuint = 2**256-1;
    
    struct Reservation 
    {
        address guest;
        address host;
        uint reservationstart;
        uint reservationend;
        uint dailyprice;
        uint advancepayment;
        reservationstatus rstatus;
    }
    
    mapping (bytes32 =\u003e Reservation) public Reservations;
    
    //Event for new Reservation
    event NewReservationEvent(bytes32 indexed rsvid, address indexed guest, address indexed host, uint rstart, uint rend, uint dprice, uint advpay, bytes8 rstartformat, bytes8 rendformat, uint eventtime);
    
    //Reservation Status Change Event
    event ReservationStatusEvent(bytes32 indexed rsvid, reservationstatus rstatus, uint rbalance, uint eventtime);

    constructor() public 
\t{
\t    reservationid =0;
\t    reservationfee = 1000000000000000000; //1 DAI
\t\tfactorycontractactive = true;
\t}


    function setReservationFee(uint newfee) external onlyManager
    {
        /*
            1) Changes the Reservation fee that is paid to the manager
            2) The Reservation fee at launch of contract is set to 1 DAI
\t        3) The reservationfee is a public variable and can always queried\t
        */
        
        require(newfee \u003e 0);
        
        reservationfee = newfee;
    }
    

    function setFactoryContractSwitch() external onlyManager
    {
        /*
            1) Switch that controls whether the contract is active
\t        2) If the contract is paused new reservations can not be created, but existing reservations can still be completed.
        */
        
        factorycontractactive = factorycontractactive == true ? false : true;
    }
    

    function createnewReservation(address host, uint reservationstart, uint reservationend, uint dailyprice, uint advancepayment , bytes8 rstartformat, bytes8 rendformat) external 
    {
        /*
            Will Create a new reservation between guest and host
        */
        
        require(factorycontractactive, \"Factory Contract should be Active\");
        require(reservationid \u003c maxuint, \"Maximum reservationid reached\");
        require(msg.sender != host,\"Host and Guest can not be same\");
        require(dailyprice \u003e 0, \"Daily Price should be \u003e 0\");
        
        require(now \u003c mathlib.add(reservationstart,secondsin21hrs),\"Too late to start this reservation\");
        
        uint lengthofstay = mathlib.calculatereservationdays(reservationstart,reservationend);
        
        require(lengthofstay \u003e 0,\"Length of Stay should be \u003e 0\");
        
        uint totalreservationamount = mathlib.mul(dailyprice,lengthofstay);
        
        uint minadvpayment = lengthofstay \u003e 5 ? mathlib.mul(dailyprice,2) : dailyprice;
        
        require(advancepayment \u003e= minadvpayment \u0026\u0026 advancepayment \u003c= totalreservationamount ,\"Advance Payment should be \u003e= minadvpayment and \u003c= reservation amount \");
        
        //Check daitoken allowance for Factory contract
        require(daiToken.allowance(msg.sender,address(this)) \u003e= mathlib.add(advancepayment, reservationfee), \"daiToken allowance exceeded\");
        
        bytes32 rsvid = keccak256(abi.encodePacked(reservationid));
        
        Reservations[rsvid] = Reservation(msg.sender, host, reservationstart, reservationend, dailyprice, advancepayment, reservationstatus.ACTIVATED);
        
        reservationid = mathlib.add(reservationid,1);
        
        //Transfer the advance payment to this contract
        daiToken.transferFrom(msg.sender, address(this), advancepayment);
        
        //Transfer the reservation fee to factory manager
        daiToken.transferFrom(msg.sender, manager, reservationfee);

        emit NewReservationEvent(rsvid, msg.sender, host, reservationstart, reservationend, dailyprice, advancepayment, rstartformat, rendformat, now);
        
    }
    
     modifier onlyGuest(bytes32 rsvid)
    {
        require(msg.sender == Reservations[rsvid].guest, \"Only Guest\");
        _;
    }
    
    modifier onlyHost(bytes32 rsvid)
    {
        require(msg.sender == Reservations[rsvid].host, \"Only Host\");
        _;
    }
    
     function getReservationDetails(bytes32 rsvid) external view returns (reservationstatus, uint)
    {
    \t/*
    \t   Will get the changing variables for each reservation based on reservation ID\t
    \t*/

        Reservation memory thisreservation = Reservations[rsvid];
        
        require(thisreservation.guest !=address(0),\"Reservation does not exist\");
        
        return(thisreservation.rstatus, thisreservation.advancepayment);
    }
    
   
    function setHostCancelsReservation(bytes32 rsvid) external onlyHost(rsvid)
    {
        /*
            1) Allows the host to cancel the reservation upto 21 Hrs after reservation start if ACTIVATED
            2) Guest gets a Full Refund Instantly, since the Host is cancelling
        */
        
        Reservation storage thisreservation = Reservations[rsvid];
        
        require(thisreservation.rstatus == reservationstatus.ACTIVATED,\"Reservation must be ACTIVATED\");
        
        uint reservationstart21Hrs = mathlib.add(thisreservation.reservationstart,secondsin21hrs);
        
        require(now \u003c reservationstart21Hrs,\"Reservation Can be CANCELLED upto 21 Hrs after reservation start\");
        
        uint rsvbalance = thisreservation.advancepayment;
        
        thisreservation.advancepayment = 0;
        thisreservation.rstatus = reservationstatus.CANCELLED;
        
\t    //Guest is refunded the entire advance payment balance
        daiToken.transfer(thisreservation.guest, rsvbalance);
        
        emit ReservationStatusEvent(rsvid, thisreservation.rstatus, thisreservation.advancepayment, now);
    }
    
     function setGuestCancelReservation(bytes32 rsvid) external onlyGuest(rsvid)
    {
        /*
            1) Guest can cancel the reservation upto 21 Hrs after reservation start if ACTIVATED
            2) If length of stay is 5 days or less, cancel upto 3 days before reservation start, otherwise a cancellation fee of dailyprice is applied
            3) If length of stay is greater than 5 days, cancel upto 5 days before reservation start, otherwise a cancellation fee of 2*dailyprice is applied
        */
        
        Reservation storage thisreservation = Reservations[rsvid];
        
        require(thisreservation.rstatus == reservationstatus.ACTIVATED,\"Reservation must be ACTIVATED\");
        
        uint reservationstart21Hrs = mathlib.add(thisreservation.reservationstart,secondsin21hrs);
            
        require(now \u003c reservationstart21Hrs,\"Guest can only cancel upto 21 Hrs after reservation start\");
        
        uint lengthofstay = mathlib.calculatereservationdays(thisreservation.reservationstart,thisreservation.reservationend); 
            
        uint cancellationperiod = lengthofstay \u003e 5 ? 5 : 3;
        
        uint rsvbalance = thisreservation.advancepayment;
        
        thisreservation.advancepayment = 0;
        thisreservation.rstatus = reservationstatus.CANCELLED;
            
            if (now \u003c mathlib.sub(thisreservation.reservationstart,mathlib.mul(cancellationperiod,secondsinday)))
            {
                daiToken.transfer(thisreservation.guest,rsvbalance);
            }
            else
            {
                uint cancellationfee = lengthofstay \u003e 5 ? mathlib.mul(thisreservation.dailyprice,2) : thisreservation.dailyprice;
                
                uint guestdue = mathlib.sub(rsvbalance,cancellationfee);
                
                //Host gets compensated for cancellation    
                 daiToken.transfer(thisreservation.host,cancellationfee);
                 
                 //Guest gets refunded the remaining balance
                 if (guestdue \u003e 0)
                 {
                    daiToken.transfer(thisreservation.guest,guestdue);
                 }
            }
        
         emit ReservationStatusEvent(rsvid, thisreservation.rstatus, thisreservation.advancepayment, now);
    }
    
    
     function setHostClaimsRent(bytes32 rsvid) external onlyHost(rsvid)
    {
        /*
            Host can claim the rent 21 Hrs after reservation start
        */
        
        Reservation storage thisreservation = Reservations[rsvid];
        
        require(thisreservation.rstatus == reservationstatus.ACTIVATED,\"Reservation must be ACTIVATED\");
        
        uint reservationstart21Hrs = mathlib.add(thisreservation.reservationstart,secondsin21hrs);
            
        require(now \u003e= reservationstart21Hrs,\"Host can only claim the rent 21 Hrs after reservation start\");
        
        uint rsvbalance = thisreservation.advancepayment;
        
        thisreservation.advancepayment = 0;
        thisreservation.rstatus = reservationstatus.COMPLETED;
        
        //Host claims the entire advance payment balance
         daiToken.transfer(thisreservation.host,rsvbalance);
         
        emit ReservationStatusEvent(rsvid, thisreservation.rstatus, thisreservation.advancepayment, now);
    }
    
    
     function setHostRefundsPartRent(bytes32 rsvid, uint refundamount) external onlyHost(rsvid)
    {
        /*
            1) Host can refund the Guest a part or full amount 21 Hrs after reservation start
            2) The remaining balance if any will be transferred to the Host. 
        */
        
        Reservation storage thisreservation = Reservations[rsvid];
        
        require(thisreservation.rstatus == reservationstatus.ACTIVATED, \"Reservation has to be ACTIVATED\");
        
        uint reservationstart21Hrs = mathlib.add(thisreservation.reservationstart,secondsin21hrs);
        
        require(now \u003e= reservationstart21Hrs, \"Host can refund part of contract balance 21 Hrs after Reservation Start\");
        
        uint rsvbalance = thisreservation.advancepayment;
        
        require(refundamount \u003e 0 \u0026\u0026 refundamount \u003c= rsvbalance, \"Refund amount should be \u003e 0 \u0026\u0026 \u003c= rsvbalance\");
        
        uint hostdue = mathlib.sub(rsvbalance,refundamount);
        
        thisreservation.advancepayment = 0;
        thisreservation.rstatus = reservationstatus.COMPLETED;
        
        //The refund amount is transferred to the guest
        daiToken.transfer(thisreservation.guest,refundamount);
        
        //The remaining amount is transferred to the Host
        if (hostdue \u003e 0)
        {
            daiToken.transfer(thisreservation.host,hostdue);
        }
        
        emit ReservationStatusEvent(rsvid, thisreservation.rstatus, thisreservation.advancepayment, now);
    }
    
}
























