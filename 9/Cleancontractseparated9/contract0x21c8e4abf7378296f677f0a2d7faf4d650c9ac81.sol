// SPDX-License-Identifier: MIT
/*
 * JuiceBox.sol
 *
 * Created: October 27, 2021
 *
 * Price: FREE
 * Rinkeby: 0x09494437a042494eAdA9801A85eE494cFB27D75b
 * Mainnet:
 *
 * Description: An ERC-721 token that will be claimable by anyone who owns 'the Plug'
 *
 *  - There will be 4 variations, each with a different rarity (based on how likely it is
 *    to receive, i.e. v1:60%, v2:20%, v3:15%, v4:5%)
 *  - Owners with multiple Plugs will benefit through a distribution scheme that shifts 
 *    the probability of minting each variation towards 25%
 */

pragma solidity >=0.5.16 <0.9.0;

import \"./Kasbeer721.sol\";

//@title Juice Box
//@author Jack Kasbeer (gh:@jcksber, tw:@satoshigoat, ig:overprivilegd)
contract JuiceBox is Kasbeer721 {

\t// -------------
\t// EVENTS & VARS
\t// -------------

\tevent JuiceBoxMinted(uint256 indexed a);
\tevent JuiceBoxBurned(uint256 indexed a);

\t//@dev This is how we'll keep track of who has already minted a JuiceBox
\tmapping (address => bool) internal _boxHolders;

\t//@dev Keep track of which token ID is associated with which hash
\tmapping (uint256 => string) internal _tokenToHash;

\t//@dev Initial production hashes
\tstring [NUM_ASSETS] boxHashes = [\"QmbZH1NZLvUTqeaHXH37fVnb7QHcCFDsn4QKvicM1bmn5j\", 
\t\t\t\t\t\t\t\t\t \"QmVXiZFCwxBiJQJCpiSCKz8TkaWmnEBpT6stmxYqRK4FeY\", 
\t\t\t\t\t\t\t\t\t \"Qmds7Ag48sfeodwmtWmTokFGZoHiGZXYN2YbojtjTR7GhR\", 
\t\t\t\t\t\t\t\t\t \"QmeUPdEvAU5sSEv1Nwixbu1oxkSFCY194m8Drm9u3rtVWg\"];
\t\t\t\t\t\t\t\t\t //cherry, berry, kiwi, lemon

\t//@dev Associated weights of probability for hashes
\tuint16 [NUM_ASSETS] boxWeights = [60, 23, 15, 2];//cherry, berry, kiwi, lemon

\t//@dev Secret word to prevent etherscan claims
\tstring private _secret;

\tconstructor(string memory secret) Kasbeer721(\"Juice Box\", \"\") {
\t\t_whitelistActive = true;
\t\t_secret = secret;
\t\t_contractUri = \"ipfs://QmdafigFsnSjondbSFKWhV2zbCf8qF5xEkgNoCYcnanhD6\";
\t\tpayoutAddress = 0x6b8C6E15818C74895c31A1C91390b3d42B336799;//logik
\t}

\t// -----------
\t// RESTRICTORS
\t// -----------

\tmodifier boxAvailable()
\t{
\t\trequire(getCurrentId() < MAX_NUM_TOKENS, \"JuiceBox: no JuiceBox's left to mint\");
\t\t_;
\t}

\tmodifier tokenExists(uint256 tokenId)
\t{
\t\trequire(_exists(tokenId), \"JuiceBox: nonexistent token\");
\t\t_;
\t}

\t// ---------------
\t// JUICE BOX MAGIC 
\t// ---------------

\t//@dev Override 'tokenURI' to account for asset/hash cycling
\tfunction tokenURI(uint256 tokenId) 
\t\tpublic view virtual override tokenExists(tokenId) 
\t\treturns (string memory) 
\t{\t
\t\treturn string(abi.encodePacked(_baseURI(), _tokenToHash[tokenId]));
\t}

\t//@dev Get the secret word
\tfunction _getSecret() private view returns (string memory)
\t{
\t\treturn _secret;
\t}

\t//// ----------------------
    //// IPFS HASH MANIPULATION
    //// ----------------------

    //@dev Get the hash stored at `idx` 
\tfunction getHashByIndex(uint8 idx) public view hashIndexInRange(idx)
\t  returns (string memory)
\t{
\t\treturn boxHashes[idx];
\t}

\t//@dev Allows us to update the IPFS hash values (one at a time)
\t// 0:cherry, 1:berry, 2:kiwi, 3:lemon
\tfunction updateHashForIndex(uint8 idx, string memory str) 
\t\tpublic isSquad hashIndexInRange(idx)
\t{
\t\tboxHashes[idx] = str;
\t}

    // ------------------
    // MINTING & CLAIMING
    // ------------------

    //@dev Allows owners to mint for free
    function mint(address to) public virtual override isSquad boxAvailable
    \treturns (uint256 tid)
    {
    \ttid = _mintInternal(to);
    \t_assignHash(tid, 1);
    }

    //@dev Mint a specific juice box (owners only)
    function mintWithHash(address to, string memory hash) public isSquad returns (uint256 tid)
    {
    \ttid = _mintInternal(to);
    \t_tokenToHash[tid] = hash;
    }

    //@dev Claim a JuiceBox if you're a Plug holder
    function claim(address to, uint8 numPlugs, string memory secret) public 
    \tboxAvailable whitelistEnabled onlyWhitelist(to) saleActive
    \treturns (uint256 tid, string memory hash)
    {
    \trequire(!_boxHolders[to], \"JuiceBox: cannot claim more than 1\");
    \trequire(!_isContract(to), \"JuiceBox: silly rabbit :P\");
    \trequire(_stringsEqual(secret, _getSecret()), \"JuiceBox: silly rabbit :P\");

    \ttid = _mintInternal(to);
    \thash = _assignHash(tid, numPlugs);
    }

\t//@dev Mints a single Juice Box & updates `_boxHolders` accordingly
\tfunction _mintInternal(address to) internal virtual returns (uint256 newId)
\t{
\t\t_incrementTokenId();

\t\tnewId = getCurrentId();

\t\t_safeMint(to, newId);
\t\t_markAsClaimed(to);

\t\temit JuiceBoxMinted(newId);
\t}

\t//@dev Based on the number of Plugs owned by the sender, randomly select 
\t// a JuiceBox hash that will be associated with their token id
\tfunction _assignHash(uint256 tid, uint8 numPlugs) private tokenExists(tid)
\t\treturns (string memory hash)
\t{
\t\tuint8[] memory weights = new uint8[](NUM_ASSETS);
\t\t//calculate new weights based on `numPlugs`
\t\tif (numPlugs > 50) numPlugs = 50;
\t\tweights[0] = uint8(boxWeights[0] - 35*((numPlugs-1)/50));//cherry: 60% -> 25%
\t\tweights[1] = uint8(boxWeights[1] +  2*((numPlugs-1)/50));//berry:  23% -> 25%
\t\tweights[2] = uint8(boxWeights[2] + 10*((numPlugs-1)/50));//kiwi:   15% -> 25%
\t\tweights[3] = uint8(boxWeights[3] + 23*((numPlugs-1)/50));//lemon:   2% -> 25%

\t\tuint16 rnd = random() % 100;//should be b/n 0 & 100
\t\t//randomly select a juice box hash
\t\tuint8 i;
\t\tfor (i = 0; i < NUM_ASSETS; i++) {
\t\t\tif (rnd < weights[i]) {
\t\t\t\thash = boxHashes[i];
\t\t\t\tbreak;
\t\t\t}
\t\t\trnd -= weights[i];
\t\t}
\t\t//assign the selected hash to this token id
\t\t_tokenToHash[tid] = hash;
\t}

\t//@dev Update `_boxHolders` so that `a` cannot claim another juice box
\tfunction _markAsClaimed(address a) private
\t{
\t\t_boxHolders[a] = true;
\t}

\tfunction getHashForTid(uint256 tid) public view tokenExists(tid) 
\t\treturns (string memory)
\t{
\t\treturn _tokenToHash[tid];
\t}

\t//@dev Pseudo-random number generator
\tfunction random() public view returns (uint16 rnd)
\t{
\t\treturn uint16(uint(keccak256(abi.encodePacked(block.difficulty, block.timestamp, boxWeights))));
\t}
}
"
    
// SPDX-License-Identifier: MIT
/*
 * Kasbeer721.sol
 *
 * Created: October 27, 2021
 *
 * Tuned-up ERC721 that covers a multitude of features that are generally useful
 * so that it can be easily extended for quick customization .
 */

pragma solidity >=0.5.16 <0.9.0;


import \"@openzeppelin/contracts/token/ERC721/ERC721.sol\";
import \"@openzeppelin/contracts/utils/Counters.sol\";
import \"@openzeppelin/contracts/utils/math/SafeMath.sol\";

import \"./access/Whitelistable.sol\";
import \"./access/Pausable.sol\";
import \"./utils/LibPart.sol\";


//@title Kasbeer721 - Beefed up ERC721
//@author Jack Kasbeer (git:@jcksber, tw:@satoshigoat, ig:overprivilegd)
contract Kasbeer721 is ERC721, Whitelistable, Pausable {

\tusing SafeMath for uint256;
\tusing Counters for Counters.Counter;

\t// -------------
\t// EVENTS & VARS
\t// -------------

\tevent Kasbeer721Minted(uint256 indexed tokenId);
\tevent Kasbeer721Burned(uint256 indexed tokenId);

    //@dev Token incrementing
\tCounters.Counter internal _tokenIds;

\t//@dev Important numbers
\tuint constant NUM_ASSETS = 4;//4 juice box variations
\tuint constant MAX_NUM_TOKENS = 413;//nomad+chi+st.l dream chasers

\t//@dev Properties
\tstring internal _contractUri;
\taddress public payoutAddress;

\t//@dev These are needed for contract compatability
\tuint256 constant public royaltyFeeBps = 1000; // 10%
    bytes4 internal constant _INTERFACE_ID_ERC165 = 0x01ffc9a7;
    bytes4 internal constant _INTERFACE_ID_ERC721 = 0x80ac58cd;
    bytes4 internal constant _INTERFACE_ID_ERC721_METADATA = 0x5b5e139f;
    bytes4 internal constant _INTERFACE_ID_ERC721_ENUMERABLE = 0x780e9d63;
    bytes4 internal constant _INTERFACE_ID_EIP2981 = 0x2a55205a;
    bytes4 internal constant _INTERFACE_ID_ROYALTIES = 0xcad96cca;

\tconstructor(string memory temp_name, string memory temp_symbol) 
\t\tERC721(temp_name, temp_symbol) {}

\t// ---------
\t// MODIFIERS
\t// ---------

\tmodifier hashIndexInRange(uint8 idx)
\t{
\t\trequire(0 <= idx && idx < NUM_ASSETS, \"Kasbeer721: index OOB\");
\t\t_;
\t}
\t
\tmodifier onlyValidTokenId(uint256 tokenId)
\t{
\t\trequire(1 <= tokenId && tokenId <= MAX_NUM_TOKENS, \"Kasbeer721: tokenId OOB\");
\t\t_;
\t}

\t// ----------
\t// MAIN LOGIC
\t// ----------

\t//@dev All of the asset's will be pinned to IPFS
\tfunction _baseURI() 
\t\tinternal view virtual override returns (string memory)
\t{
\t\treturn \"ipfs://\";
\t}

\t//@dev This is here as a reminder to override for custom transfer functionality
\tfunction _beforeTokenTransfer(address from, address to, uint256 tokenId) 
\t\tinternal virtual override 
\t{ 
\t\tsuper._beforeTokenTransfer(from, to, tokenId);
\t}

\t//@dev Allows owners to mint for free
    function mint(address to) public virtual isSquad returns (uint256)
    {
    \t_tokenIds.increment();

\t\tuint256 newId = _tokenIds.current();
\t\t_safeMint(to, newId);
\t\temit Kasbeer721Minted(newId);

\t\treturn newId;
    }

\t//@dev Custom burn function - nothing special
\tfunction burn(uint256 tokenId) public virtual
\t{
\t\trequire(
\t\t\tisInSquad(_msgSender()) || 
\t\t\t_msgSender() == ownerOf(tokenId), 
\t\t\t\"Kasbeer721: not owner or in squad.\"
\t\t);

\t\t_burn(tokenId);
\t\temit Kasbeer721Burned(tokenId);
\t}

    //// --------
\t//// CONTRACT
\t//// --------

\t//@dev Controls the contract-level metadata to include things like royalties
\tfunction contractURI() public view returns(string memory)
\t{
\t\treturn _contractUri;
\t}

\t//@dev Ability to change the contract URI
\tfunction updateContractUri(string memory updatedContractUri) public isSquad
\t{
        _contractUri = updatedContractUri;
    }

    //@dev Allows us to withdraw funds collected
\tfunction withdraw(address payable wallet, uint256 amount) public isSquad
\t{
\t\trequire(amount <= address(this).balance,
\t\t\t\"Kasbeer721: Insufficient funds to withdraw\");
\t\twallet.transfer(amount);
\t}

\t//@dev Destroy contract and reclaim leftover funds
    function kill() public onlyOwner 
    {
        selfdestruct(payable(_msgSender()));
    }
\t
    // -------
    // HELPERS
    // -------

    //@dev Returns the current token id (number minted so far)
\tfunction getCurrentId() public view returns (uint256)
\t{
\t\treturn _tokenIds.current();
\t}

\t//@dev Increments `_tokenIds` by 1
\tfunction _incrementTokenId() internal 
\t{
\t\t_tokenIds.increment();
\t}

\t//@dev Determine if two strings are equal using the length + hash method
\tfunction _stringsEqual(string memory a, string memory b) 
\t\tinternal pure returns (bool)
\t{
\t\tbytes memory A = bytes(a);
\t\tbytes memory B = bytes(b);

\t\tif (A.length != B.length) {
\t\t\treturn false;
\t\t} else {
\t\t\treturn keccak256(A) == keccak256(B);
\t\t}
\t}

\t//@dev Determine if an address is a smart contract 
\tfunction _isContract(address a) internal view returns (bool)
\t{
\t\tuint32 size;
\t\tassembly {
\t\t\tsize := extcodesize(a)
\t\t}
\t\treturn size > 0;
\t}

\t// -----------------
    // SECONDARY MARKETS
\t// -----------------

\t//@dev Rarible Royalties V2
    function getRaribleV2Royalties(uint256 id) 
    \tonlyValidTokenId(id) external view returns (LibPart.Part[] memory) 
    {
        LibPart.Part[] memory royalties = new LibPart.Part[](1);
        royalties[0] = LibPart.Part({
            account: payable(payoutAddress),
            value: uint96(royaltyFeeBps)
        });

        return royalties;
    }

    //@dev EIP-2981
    function royaltyInfo(uint256 tokenId, uint256 salePrice) external view onlyValidTokenId(tokenId) returns (address receiver, uint256 amount) {
        uint256 ourCut = SafeMath.div(SafeMath.mul(salePrice, royaltyFeeBps), 10000);
        return (payoutAddress, ourCut);
    }

    // -----------------
    // INTERFACE SUPPORT
    // -----------------

    //@dev Confirm that this contract is compatible w/ these interfaces
    function supportsInterface(bytes4 interfaceId) 
\t\tpublic view virtual override returns (bool)
\t{
\t\treturn interfaceId == _INTERFACE_ID_ERC165
        || interfaceId == _INTERFACE_ID_ROYALTIES
        || interfaceId == _INTERFACE_ID_ERC721
        || interfaceId == _INTERFACE_ID_ERC721_METADATA
        || interfaceId == _INTERFACE_ID_ERC721_ENUMERABLE
        || interfaceId == _INTERFACE_ID_EIP2981
        || super.supportsInterface(interfaceId);
\t}
}
"
    
// SPDX-License-Identifier: MIT
// OpenZeppelin Contracts v4.4.1 (token/ERC721/ERC721.sol)

pragma solidity ^0.8.0;

import \"./IERC721.sol\";
import \"./IERC721Receiver.sol\";
import \"./extensions/IERC721Metadata.sol\";
import \"../../utils/Address.sol\";
import \"../../utils/Context.sol\";
import \"../../utils/Strings.sol\";
import \"../../utils/introspection/ERC165.sol\";

/**
 * @dev Implementation of https://eips.ethereum.org/EIPS/eip-721[ERC721] Non-Fungible Token Standard, including
 * the Metadata extension, but not including the Enumerable extension, which is available separately as
 * {ERC721Enumerable}.
 */
contract ERC721 is Context, ERC165, IERC721, IERC721Metadata {
    using Address for address;
    using Strings for uint256;

    // Token name
    string private _name;

    // Token symbol
    string private _symbol;

    // Mapping from token ID to owner address
    mapping(uint256 => address) private _owners;

    // Mapping owner address to token count
    mapping(address => uint256) private _balances;

    // Mapping from token ID to approved address
    mapping(uint256 => address) private _tokenApprovals;

    // Mapping from owner to operator approvals
    mapping(address => mapping(address => bool)) private _operatorApprovals;

    /**
     * @dev Initializes the contract by setting a `name` and a `symbol` to the token collection.
     */
    constructor(string memory name_, string memory symbol_) {
        _name = name_;
        _symbol = symbol_;
    }

    /**
     * @dev See {IERC165-supportsInterface}.
     */
    function supportsInterface(bytes4 interfaceId) public view virtual override(ERC165, IERC165) returns (bool) {
        return
            interfaceId == type(IERC721).interfaceId ||
            interfaceId == type(IERC721Metadata).interfaceId ||
            super.supportsInterface(interfaceId);
    }

    /**
     * @dev See {IERC721-balanceOf}.
     */
    function balanceOf(address owner) public view virtual override returns (uint256) {
        require(owner != address(0), \"ERC721: balance query for the zero address\");
        return _balances[owner];
    }

    /**
     * @dev See {IERC721-ownerOf}.
     */
    function ownerOf(uint256 tokenId) public view virtual override returns (address) {
        address owner = _owners[tokenId];
        require(owner != address(0), \"ERC721: owner query for nonexistent token\");
        return owner;
    }

    /**
     * @dev See {IERC721Metadata-name}.
     */
    function name() public view virtual override returns (string memory) {
        return _name;
    }

    /**
     * @dev See {IERC721Metadata-symbol}.
     */
    function symbol() public view virtual override returns (string memory) {
        return _symbol;
    }

    /**
     * @dev See {IERC721Metadata-tokenURI}.
     */
    function tokenURI(uint256 tokenId) public view virtual override returns (string memory) {
        require(_exists(tokenId), \"ERC721Metadata: URI query for nonexistent token\");

        string memory baseURI = _baseURI();
        return bytes(baseURI).length > 0 ? string(abi.encodePacked(baseURI, tokenId.toString())) : \"\";
    }

    /**
     * @dev Base URI for computing {tokenURI}. If set, the resulting URI for each
     * token will be the concatenation of the `baseURI` and the `tokenId`. Empty
     * by default, can be overriden in child contracts.
     */
    function _baseURI() internal view virtual returns (string memory) {
        return \"\";
    }

    /**
     * @dev See {IERC721-approve}.
     */
    function approve(address to, uint256 tokenId) public virtual override {
        address owner = ERC721.ownerOf(tokenId);
        require(to != owner, \"ERC721: approval to current owner\");

        require(
            _msgSender() == owner || isApprovedForAll(owner, _msgSender()),
            \"ERC721: approve caller is not owner nor approved for all\"
        );

        _approve(to, tokenId);
    }

    /**
     * @dev See {IERC721-getApproved}.
     */
    function getApproved(uint256 tokenId) public view virtual override returns (address) {
        require(_exists(tokenId), \"ERC721: approved query for nonexistent token\");

        return _tokenApprovals[tokenId];
    }

    /**
     * @dev See {IERC721-setApprovalForAll}.
     */
    function setApprovalForAll(address operator, bool approved) public virtual override {
        _setApprovalForAll(_msgSender(), operator, approved);
    }

    /**
     * @dev See {IERC721-isApprovedForAll}.
     */
    function isApprovedForAll(address owner, address operator) public view virtual override returns (bool) {
        return _operatorApprovals[owner][operator];
    }

    /**
     * @dev See {IERC721-transferFrom}.
     */
    function transferFrom(
        address from,
        address to,
        uint256 tokenId
    ) public virtual override {
        //solhint-disable-next-line max-line-length
        require(_isApprovedOrOwner(_msgSender(), tokenId), \"ERC721: transfer caller is not owner nor approved\");

        _transfer(from, to, tokenId);
    }

    /**
     * @dev See {IERC721-safeTransferFrom}.
     */
    function safeTransferFrom(
        address from,
        address to,
        uint256 tokenId
    ) public virtual override {
        safeTransferFrom(from, to, tokenId, \"\");
    }

    /**
     * @dev See {IERC721-safeTransferFrom}.
     */
    function safeTransferFrom(
        address from,
        address to,
        uint256 tokenId,
        bytes memory _data
    ) public virtual override {
        require(_isApprovedOrOwner(_msgSender(), tokenId), \"ERC721: transfer caller is not owner nor approved\");
        _safeTransfer(from, to, tokenId, _data);
    }

    /**
     * @dev Safely transfers `tokenId` token from `from` to `to`, checking first that contract recipients
     * are aware of the ERC721 protocol to prevent tokens from being forever locked.
     *
     * `_data` is additional data, it has no specified format and it is sent in call to `to`.
     *
     * This internal function is equivalent to {safeTransferFrom}, and can be used to e.g.
     * implement alternative mechanisms to perform token transfer, such as signature-based.
     *
     * Requirements:
     *
     * - `from` cannot be the zero address.
     * - `to` cannot be the zero address.
     * - `tokenId` token must exist and be owned by `from`.
     * - If `to` refers to a smart contract, it must implement {IERC721Receiver-onERC721Received}, which is called upon a safe transfer.
     *
     * Emits a {Transfer} event.
     */
    function _safeTransfer(
        address from,
        address to,
        uint256 tokenId,
        bytes memory _data
    ) internal virtual {
        _transfer(from, to, tokenId);
        require(_checkOnERC721Received(from, to, tokenId, _data), \"ERC721: transfer to non ERC721Receiver implementer\");
    }

    /**
     * @dev Returns whether `tokenId` exists.
     *
     * Tokens can be managed by their owner or approved accounts via {approve} or {setApprovalForAll}.
     *
     * Tokens start existing when they are minted (`_mint`),
     * and stop existing when they are burned (`_burn`).
     */
    function _exists(uint256 tokenId) internal view virtual returns (bool) {
        return _owners[tokenId] != address(0);
    }

    /**
     * @dev Returns whether `spender` is allowed to manage `tokenId`.
     *
     * Requirements:
     *
     * - `tokenId` must exist.
     */
    function _isApprovedOrOwner(address spender, uint256 tokenId) internal view virtual returns (bool) {
        require(_exists(tokenId), \"ERC721: operator query for nonexistent token\");
        address owner = ERC721.ownerOf(tokenId);
        return (spender == owner || getApproved(tokenId) == spender || isApprovedForAll(owner, spender));
    }

    /**
     * @dev Safely mints `tokenId` and transfers it to `to`.
     *
     * Requirements:
     *
     * - `tokenId` must not exist.
     * - If `to` refers to a smart contract, it must implement {IERC721Receiver-onERC721Received}, which is called upon a safe transfer.
     *
     * Emits a {Transfer} event.
     */
    function _safeMint(address to, uint256 tokenId) internal virtual {
        _safeMint(to, tokenId, \"\");
    }

    /**
     * @dev Same as {xref-ERC721-_safeMint-address-uint256-}[`_safeMint`], with an additional `data` parameter which is
     * forwarded in {IERC721Receiver-onERC721Received} to contract recipients.
     */
    function _safeMint(
        address to,
        uint256 tokenId,
        bytes memory _data
    ) internal virtual {
        _mint(to, tokenId);
        require(
            _checkOnERC721Received(address(0), to, tokenId, _data),
            \"ERC721: transfer to non ERC721Receiver implementer\"
        );
    }

    /**
     * @dev Mints `tokenId` and transfers it to `to`.
     *
     * WARNING: Usage of this method is discouraged, use {_safeMint} whenever possible
     *
     * Requirements:
     *
     * - `tokenId` must not exist.
     * - `to` cannot be the zero address.
     *
     * Emits a {Transfer} event.
     */
    function _mint(address to, uint256 tokenId) internal virtual {
        require(to != address(0), \"ERC721: mint to the zero address\");
        require(!_exists(tokenId), \"ERC721: token already minted\");

        _beforeTokenTransfer(address(0), to, tokenId);

        _balances[to] += 1;
        _owners[tokenId] = to;

        emit Transfer(address(0), to, tokenId);
    }

    /**
     * @dev Destroys `tokenId`.
     * The approval is cleared when the token is burned.
     *
     * Requirements:
     *
     * - `tokenId` must exist.
     *
     * Emits a {Transfer} event.
     */
    function _burn(uint256 tokenId) internal virtual {
        address owner = ERC721.ownerOf(tokenId);

        _beforeTokenTransfer(owner, address(0), tokenId);

        // Clear approvals
        _approve(address(0), tokenId);

        _balances[owner] -= 1;
        delete _owners[tokenId];

        emit Transfer(owner, address(0), tokenId);
    }

    /**
     * @dev Transfers `tokenId` from `from` to `to`.
     *  As opposed to {transferFrom}, this imposes no restrictions on msg.sender.
     *
     * Requirements:
     *
     * - `to` cannot be the zero address.
     * - `tokenId` token must be owned by `from`.
     *
     * Emits a {Transfer} event.
     */
    function _transfer(
        address from,
        address to,
        uint256 tokenId
    ) internal virtual {
        require(ERC721.ownerOf(tokenId) == from, \"ERC721: transfer of token that is not own\");
        require(to != address(0), \"ERC721: transfer to the zero address\");

        _beforeTokenTransfer(from, to, tokenId);

        // Clear approvals from the previous owner
        _approve(address(0), tokenId);

        _balances[from] -= 1;
        _balances[to] += 1;
        _owners[tokenId] = to;

        emit Transfer(from, to, tokenId);
    }

    /**
     * @dev Approve `to` to operate on `tokenId`
     *
     * Emits a {Approval} event.
     */
    function _approve(address to, uint256 tokenId) internal virtual {
        _tokenApprovals[tokenId] = to;
        emit Approval(ERC721.ownerOf(tokenId), to, tokenId);
    }

    /**
     * @dev Approve `operator` to operate on all of `owner` tokens
     *
     * Emits a {ApprovalForAll} event.
     */
    function _setApprovalForAll(
        address owner,
        address operator,
        bool approved
    ) internal virtual {
        require(owner != operator, \"ERC721: approve to caller\");
        _operatorApprovals[owner][operator] = approved;
        emit ApprovalForAll(owner, operator, approved);
    }

    /**
     * @dev Internal function to invoke {IERC721Receiver-onERC721Received} on a target address.
     * The call is not executed if the target address is not a contract.
     *
     * @param from address representing the previous owner of the given token ID
     * @param to target address that will receive the tokens
     * @param tokenId uint256 ID of the token to be transferred
     * @param _data bytes optional data to send along with the call
     * @return bool whether the call correctly returned the expected magic value
     */
    function _checkOnERC721Received(
        address from,
        address to,
        uint256 tokenId,
        bytes memory _data
    ) private returns (bool) {
        if (to.isContract()) {
            try IERC721Receiver(to).onERC721Received(_msgSender(), from, tokenId, _data) returns (bytes4 retval) {
                return retval == IERC721Receiver.onERC721Received.selector;
            } catch (bytes memory reason) {
                if (reason.length == 0) {
                    revert(\"ERC721: transfer to non ERC721Receiver implementer\");
                } else {
                    assembly {
                        revert(add(32, reason), mload(reason))
                    }
                }
            }
        } else {
            return true;
        }
    }

    /**
     * @dev Hook that is called before any token transfer. This includes minting
     * and burning.
     *
     * Calling conditions:
     *
     * - When `from` and `to` are both non-zero, ``from``'s `tokenId` will be
     * transferred to `to`.
     * - When `from` is zero, `tokenId` will be minted for `to`.
     * - When `to` is zero, ``from``'s `tokenId` will be burned.
     * - `from` and `to` are never both zero.
     *
     * To learn more about hooks, head to xref:ROOT:extending-contracts.adoc#using-hooks[Using Hooks].
     */
    function _beforeTokenTransfer(
        address from,
        address to,
        uint256 tokenId
    ) internal virtual {}
}
"
    
// SPDX-License-Identifier: MIT
// OpenZeppelin Contracts v4.4.1 (utils/Counters.sol)

pragma solidity ^0.8.0;

/**
 * @title Counters
 * @author Matt Condon (@shrugs)
 * @dev Provides counters that can only be incremented, decremented or reset. This can be used e.g. to track the number
 * of elements in a mapping, issuing ERC721 ids, or counting request ids.
 *
 * Include with `using Counters for Counters.Counter;`
 */
library Counters {
    struct Counter {
        // This variable should never be directly accessed by users of the library: interactions must be restricted to
        // the library's function. As of Solidity v0.5.2, this cannot be enforced, though there is a proposal to add
        // this feature: see https://github.com/ethereum/solidity/issues/4637
        uint256 _value; // default: 0
    }

    function current(Counter storage counter) internal view returns (uint256) {
        return counter._value;
    }

    function increment(Counter storage counter) internal {
        unchecked {
            counter._value += 1;
        }
    }

    function decrement(Counter storage counter) internal {
        uint256 value = counter._value;
        require(value > 0, \"Counter: decrement overflow\");
        unchecked {
            counter._value = value - 1;
        }
    }

    function reset(Counter storage counter) internal {
        counter._value = 0;
    }
}
"
    
// SPDX-License-Identifier: MIT
// OpenZeppelin Contracts v4.4.1 (utils/math/SafeMath.sol)

pragma solidity ^0.8.0;

// CAUTION
// This version of SafeMath should only be used with Solidity 0.8 or later,
// because it relies on the compiler's built in overflow checks.

/**
 * @dev Wrappers over Solidity's arithmetic operations.
 *
 * NOTE: `SafeMath` is generally not needed starting with Solidity 0.8, since the compiler
 * now has built in overflow checking.
 */
library SafeMath {
    /**
     * @dev Returns the addition of two unsigned integers, with an overflow flag.
     *
     * _Available since v3.4._
     */
    function tryAdd(uint256 a, uint256 b) internal pure returns (bool, uint256) {
        unchecked {
            uint256 c = a + b;
            if (c < a) return (false, 0);
            return (true, c);
        }
    }

    /**
     * @dev Returns the substraction of two unsigned integers, with an overflow flag.
     *
     * _Available since v3.4._
     */
    function trySub(uint256 a, uint256 b) internal pure returns (bool, uint256) {
        unchecked {
            if (b > a) return (false, 0);
            return (true, a - b);
        }
    }

    /**
     * @dev Returns the multiplication of two unsigned integers, with an overflow flag.
     *
     * _Available since v3.4._
     */
    function tryMul(uint256 a, uint256 b) internal pure returns (bool, uint256) {
        unchecked {
            // Gas optimization: this is cheaper than requiring 'a' not being zero, but the
            // benefit is lost if 'b' is also tested.
            // See: https://github.com/OpenZeppelin/openzeppelin-contracts/pull/522
            if (a == 0) return (true, 0);
            uint256 c = a * b;
            if (c / a != b) return (false, 0);
            return (true, c);
        }
    }

    /**
     * @dev Returns the division of two unsigned integers, with a division by zero flag.
     *
     * _Available since v3.4._
     */
    function tryDiv(uint256 a, uint256 b) internal pure returns (bool, uint256) {
        unchecked {
            if (b == 0) return (false, 0);
            return (true, a / b);
        }
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers, with a division by zero flag.
     *
     * _Available since v3.4._
     */
    function tryMod(uint256 a, uint256 b) internal pure returns (bool, uint256) {
        unchecked {
            if (b == 0) return (false, 0);
            return (true, a % b);
        }
    }

    /**
     * @dev Returns the addition of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity's `+` operator.
     *
     * Requirements:
     *
     * - Addition cannot overflow.
     */
    function add(uint256 a, uint256 b) internal pure returns (uint256) {
        return a + b;
    }

    /**
     * @dev Returns the subtraction of two unsigned integers, reverting on
     * overflow (when the result is negative).
     *
     * Counterpart to Solidity's `-` operator.
     *
     * Requirements:
     *
     * - Subtraction cannot overflow.
     */
    function sub(uint256 a, uint256 b) internal pure returns (uint256) {
        return a - b;
    }

    /**
     * @dev Returns the multiplication of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity's `*` operator.
     *
     * Requirements:
     *
     * - Multiplication cannot overflow.
     */
    function mul(uint256 a, uint256 b) internal pure returns (uint256) {
        return a * b;
    }

    /**
     * @dev Returns the integer division of two unsigned integers, reverting on
     * division by zero. The result is rounded towards zero.
     *
     * Counterpart to Solidity's `/` operator.
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function div(uint256 a, uint256 b) internal pure returns (uint256) {
        return a / b;
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
     * reverting when dividing by zero.
     *
     * Counterpart to Solidity's `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function mod(uint256 a, uint256 b) internal pure returns (uint256) {
        return a % b;
    }

    /**
     * @dev Returns the subtraction of two unsigned integers, reverting with custom message on
     * overflow (when the result is negative).
     *
     * CAUTION: This function is deprecated because it requires allocating memory for the error
     * message unnecessarily. For custom revert reasons use {trySub}.
     *
     * Counterpart to Solidity's `-` operator.
     *
     * Requirements:
     *
     * - Subtraction cannot overflow.
     */
    function sub(
        uint256 a,
        uint256 b,
        string memory errorMessage
    ) internal pure returns (uint256) {
        unchecked {
            require(b <= a, errorMessage);
            return a - b;
        }
    }

    /**
     * @dev Returns the integer division of two unsigned integers, reverting with custom message on
     * division by zero. The result is rounded towards zero.
     *
     * Counterpart to Solidity's `/` operator. Note: this function uses a
     * `revert` opcode (which leaves remaining gas untouched) while Solidity
     * uses an invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function div(
        uint256 a,
        uint256 b,
        string memory errorMessage
    ) internal pure returns (uint256) {
        unchecked {
            require(b > 0, errorMessage);
            return a / b;
        }
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
     * reverting with custom message when dividing by zero.
     *
     * CAUTION: This function is deprecated because it requires allocating memory for the error
     * message unnecessarily. For custom revert reasons use {tryMod}.
     *
     * Counterpart to Solidity's `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function mod(
        uint256 a,
        uint256 b,
        string memory errorMessage
    ) internal pure returns (uint256) {
        unchecked {
            require(b > 0, errorMessage);
            return a % b;
        }
    }
}
"
    
// SPDX-License-Identifier: MIT
/*
 * Whitelistable.sol
 *
 * Created: December 21, 2021
 *
 * Provides functionality for a \"whitelist\"/\"guestlist\" for inheriting contracts.
 */

pragma solidity >=0.5.16 <0.9.0;

import \"./SquadOwnable.sol\";

//@title Whitelistable
//@author Jack Kasbeer (gh:@jcksber, tw:@satoshigoat, ig:overprivilegd)
contract Whitelistable is SquadOwnable {

\t// -------------
\t// EVENTS & VARS
\t// -------------

\tevent WhitelistActivated(address indexed a);
\tevent WhitelistDeactivated(address indexed a);

\t//@dev Whitelist mapping for client addresses
\tmapping (address => bool) internal _whitelist;

\t//@dev Whitelist flag for active/inactive states
\tbool internal _whitelistActive;

\tconstructor() {
\t\t_whitelistActive = false;
\t\t//add myself and then logik (client)
\t\t_whitelist[0xB9699469c0b4dD7B1Dda11dA7678Fa4eFD51211b] = true;
\t\t_whitelist[0x6b8C6E15818C74895c31A1C91390b3d42B336799] = true;
\t}

\t// ---------
\t// MODIFIERS
\t// ---------

\t//@dev Determine if someone is in the whitelsit
\tmodifier onlyWhitelist(address a)
\t{
\t\trequire(isWhitelisted(a), \"Whitelistable: must be on whitelist\");
\t\t_;
\t}

\t//@dev Prevent non-whitelist minting functions from being used 
\tmodifier whitelistDisabled()
\t{
\t\trequire(_whitelistActive == false, \"Whitelistable: whitelist still active\");
\t\t_;
\t}

\t//@dev Require that the whitelist is currently enabled
\tmodifier whitelistEnabled() 
\t{
\t\trequire(_whitelistActive == true, \"Whitelistable: whitelist not active\");
\t\t_;
\t}

\t// ----------
\t// MAIN LOGIC
\t// ----------

\t//@dev Toggle the state of the whitelist (on/off)
\tfunction toggleWhitelist() public isSquad
\t{
\t\t_whitelistActive = !_whitelistActive;

\t\tif (_whitelistActive) {
\t\t\temit WhitelistActivated(_msgSender());
\t\t} else {
\t\t\temit WhitelistDeactivated(_msgSender());
\t\t}
\t}

\t//@dev Determine if `a` is in the `_whitelist
\tfunction isWhitelisted(address a) public view returns (bool)
\t{
\t\treturn _whitelist[a];
\t}

\t//// ----------
\t//// ADD/REMOVE
\t//// ----------

\t//@dev Add a single address to whitelist
\tfunction addToWhitelist(address a) public isSquad
\t{
\t\trequire(!isWhitelisted(a), \"Whitelistable: already whitelisted\"); 
\t\t_whitelist[a] = true;
\t}

\t//@dev Remove a single address from the whitelist
\tfunction removeFromWhitelist(address a) public isSquad
\t{
\t\trequire(isWhitelisted(a), \"Whitelistable: not in whitelist\");
\t\t_whitelist[a] = false;
\t}

\t//@dev Add a list of addresses to the whitelist
\tfunction bulkAddToWhitelist(address[] memory addresses) public isSquad
\t{
\t\tuint addrLen = addresses.length;

\t\trequire(addrLen > 1, \"Whitelistable: use `addToWhitelist` instead\");
\t\trequire(addrLen < 65536, \"Whitelistable: cannot add more than 65535 at once\");

\t\tuint16 i;
\t\tfor (i = 0; i < addrLen; i++) {
\t\t\tif (!isWhitelisted(addresses[i])) {
\t\t\t\t_whitelist[addresses[i]] = true;
\t\t\t}
\t\t}
\t}
}
"
    
// SPDX-License-Identifier: MIT
/*
 * Pausable.sol
 *
 * Created: December 21, 2021
 *
 * Provides functionality for pausing and unpausing the sale (or other functionality)
 */

pragma solidity >=0.5.16 <0.9.0;

import \"./SquadOwnable.sol\";

//@title Pausable
//@author Jack Kasbeer (gh:@jcksber, tw:@satoshigoat, ig:overprivilegd)
contract Pausable is SquadOwnable {

\t// -------------
\t// EVENTS & VARS
\t// -------------

\tevent Paused(address indexed a);
\tevent Unpaused(address indexed a);

\tbool private _paused;

\tconstructor() {
\t\t_paused = false;
\t}

\t// ---------
\t// MODIFIERS
\t// ---------

\t//@dev This will require the sale to be unpaused
\tmodifier saleActive()
\t{
\t\trequire(!_paused, \"Pausable: sale paused.\");
\t\t_;
\t}

\t// ----------
\t// MAIN LOGIC
\t// ----------

\t//@dev Pause or unpause minting
\tfunction toggleSaleActive() public isSquad
\t{
\t\t_paused = !_paused;

\t\tif (_paused) {
\t\t\temit Paused(_msgSender());
\t\t} else {
\t\t\temit Unpaused(_msgSender());
\t\t}
\t}

\t//@dev Determine if the sale is currently paused
\tfunction paused() public view virtual returns (bool)
\t{
\t\treturn _paused;
\t}
}
"
    
// SPDX-License-Identifier: MIT
/*
 * LibPart.sol
 *
 * Author: Jack Kasbeer (taken from 'dot')
 * Created: October 20, 2021
 */

pragma solidity >=0.5.16 <0.9.0;

//@dev We need this libary for Rarible
library LibPart {
    bytes32 public constant TYPE_HASH = keccak256(\"Part(address account,uint96 value)\");

    struct Part {
        address payable account;
        uint96 value;
    }

    function hash(Part memory part) internal pure returns (bytes32) {
        return keccak256(abi.encode(TYPE_HASH, part.account, part.value));
    }
}
"
    
// SPDX-License-Identifier: MIT
// OpenZeppelin Contracts v4.4.1 (token/ERC721/IERC721.sol)

pragma solidity ^0.8.0;

import \"../../utils/introspection/IERC165.sol\";

/**
 * @dev Required interface of an ERC721 compliant contract.
 */
interface IERC721 is IERC165 {
    /**
     * @dev Emitted when `tokenId` token is transferred from `from` to `to`.
     */
    event Transfer(address indexed from, address indexed to, uint256 indexed tokenId);

    /**
     * @dev Emitted when `owner` enables `approved` to manage the `tokenId` token.
     */
    event Approval(address indexed owner, address indexed approved, uint256 indexed tokenId);

    /**
     * @dev Emitted when `owner` enables or disables (`approved`) `operator` to manage all of its assets.
     */
    event ApprovalForAll(address indexed owner, address indexed operator, bool approved);

    /**
     * @dev Returns the number of tokens in ``owner``'s account.
     */
    function balanceOf(address owner) external view returns (uint256 balance);

    /**
     * @dev Returns the owner of the `tokenId` token.
     *
     * Requirements:
     *
     * - `tokenId` must exist.
     */
    function ownerOf(uint256 tokenId) external view returns (address owner);

    /**
     * @dev Safely transfers `tokenId` token from `from` to `to`, checking first that contract recipients
     * are aware of the ERC721 protocol to prevent tokens from being forever locked.
     *
     * Requirements:
     *
     * - `from` cannot be the zero address.
     * - `to` cannot be the zero address.
     * - `tokenId` token must exist and be owned by `from`.
     * - If the caller is not `from`, it must be have been allowed to move this token by either {approve} or {setApprovalForAll}.
     * - If `to` refers to a smart contract, it must implement {IERC721Receiver-onERC721Received}, which is called upon a safe transfer.
     *
     * Emits a {Transfer} event.
     */
    function safeTransferFrom(
        address from,
        address to,
        uint256 tokenId
    ) external;

    /**
     * @dev Transfers `tokenId` token from `from` to `to`.
     *
     * WARNING: Usage of this method is discouraged, use {safeTransferFrom} whenever possible.
     *
     * Requirements:
     *
     * - `from` cannot be the zero address.
     * - `to` cannot be the zero address.
     * - `tokenId` token must be owned by `from`.
     * - If the caller is not `from`, it must be approved to move this token by either {approve} or {setApprovalForAll}.
     *
     * Emits a {Transfer} event.
     */
    function transferFrom(
        address from,
        address to,
        uint256 tokenId
    ) external;

    /**
     * @dev Gives permission to `to` to transfer `tokenId` token to another account.
     * The approval is cleared when the token is transferred.
     *
     * Only a single account can be approved at a time, so approving the zero address clears previous approvals.
     *
     * Requirements:
     *
     * - The caller must own the token or be an approved operator.
     * - `tokenId` must exist.
     *
     * Emits an {Approval} event.
     */
    function approve(address to, uint256 tokenId) external;

    /**
     * @dev Returns the account approved for `tokenId` token.
     *
     * Requirements:
     *
     * - `tokenId` must exist.
     */
    function getApproved(uint256 tokenId) external view returns (address operator);

    /**
     * @dev Approve or remove `operator` as an operator for the caller.
     * Operators can call {transferFrom} or {safeTransferFrom} for any token owned by the caller.
     *
     * Requirements:
     *
     * - The `operator` cannot be the caller.
     *
     * Emits an {ApprovalForAll} event.
     */
    function setApprovalForAll(address operator, bool _approved) external;

    /**
     * @dev Returns if the `operator` is allowed to manage all of the assets of `owner`.
     *
     * See {setApprovalForAll}
     */
    function isApprovedForAll(address owner, address operator) external view returns (bool);

    /**
     * @dev Safely transfers `tokenId` token from `from` to `to`.
     *
     * Requirements:
     *
     * - `from` cannot be the zero address.
     * - `to` cannot be the zero address.
     * - `tokenId` token must exist and be owned by `from`.
     * - If the caller is not `from`, it must be approved to move this token by either {approve} or {setApprovalForAll}.
     * - If `to` refers to a smart contract, it must implement {IERC721Receiver-onERC721Received}, which is called upon a safe transfer.
     *
     * Emits a {Transfer} event.
     */
    function safeTransferFrom(
        address from,
        address to,
        uint256 tokenId,
        bytes calldata data
    ) external;
}
"
    
// SPDX-License-Identifier: MIT
// OpenZeppelin Contracts v4.4.1 (token/ERC721/IERC721Receiver.sol)

pragma solidity ^0.8.0;

/**
 * @title ERC721 token receiver interface
 * @dev Interface for any contract that wants to support safeTransfers
 * from ERC721 asset contracts.
 */
interface IERC721Receiver {
    /**
     * @dev Whenever an {IERC721} `tokenId` token is transferred to this contract via {IERC721-safeTransferFrom}
     * by `operator` from `from`, this function is called.
     *
     * It must return its Solidity selector to confirm the token transfer.
     * If any other value is returned or the interface is not implemented by the recipient, the transfer will be reverted.
     *
     * The selector can be obtained in Solidity with `IERC721.onERC721Received.selector`.
     */
    function onERC721Received(
        address operator,
        address from,
        uint256 tokenId,
        bytes calldata data
    ) external returns (bytes4);
}
"
    
// SPDX-License-Identifier: MIT
// OpenZeppelin Contracts v4.4.1 (token/ERC721/extensions/IERC721Metadata.sol)

pragma solidity ^0.8.0;

import \"../IERC721.sol\";

/**
 * @title ERC-721 Non-Fungible Token Standard, optional metadata extension
 * @dev See https://eips.ethereum.org/EIPS/eip-721
 */
interface IERC721Metadata is IERC721 {
    /**
     * @dev Returns the token collection name.
     */
    function name() external view returns (string memory);

    /**
     * @dev Returns the token collection symbol.
     */
    function symbol() external view returns (string memory);

    /**
     * @dev Returns the Uniform Resource Identifier (URI) for `tokenId` token.
     */
    function tokenURI(uint256 tokenId) external view returns (string memory);
}
"
    
// SPDX-License-Identifier: MIT
// OpenZeppelin Contracts v4.4.1 (utils/Address.sol)

pragma solidity ^0.8.0;

/**
 * @dev Collection of functions related to the address type
 */
library Address {
    /**
     * @dev Returns true if `account` is a contract.
     *
     * [IMPORTANT]
     * ====
     * It is unsafe to assume that an address for which this function returns
     * false is an externally-owned account (EOA) and not a contract.
     *
     * Among others, `isContract` will return false for the following
     * types of addresses:
     *
     *  - an externally-owned account
     *  - a contract in construction
     *  - an address where a contract will be created
     *  - an address where a contract lived, but was destroyed
     * ====
     */
    function isContract(address account) internal view returns (bool) {
        // This method relies on extcodesize, which returns 0 for contracts in
        // construction, since the code is only stored at the end of the
        // constructor execution.

        uint256 size;
        assembly {
            size := extcodesize(account)
        }
        return size > 0;
    }

    /**
     * @dev Replacement for Solidity's `transfer`: sends `amount` wei to
     * `recipient`, forwarding all available gas and reverting on errors.
     *
     * https://eips.ethereum.org/EIPS/eip-1884[EIP1884] increases the gas cost
     * of certain opcodes, possibly making contracts go over the 2300 gas limit
     * imposed by `transfer`, making them unable to receive funds via
     * `transfer`. {sendValue} removes this limitation.
     *
     * https://diligence.consensys.net/posts/2019/09/stop-using-soliditys-transfer-now/[Learn more].
     *
     * IMPORTANT: because control is transferred to `recipient`, care must be
     * taken to not create reentrancy vulnerabilities. Consider using
     * {ReentrancyGuard} or the
     * https://solidity.readthedocs.io/en/v0.5.11/security-considerations.html#use-the-checks-effects-interactions-pattern[checks-effects-interactions pattern].
     */
    function sendValue(address payable recipient, uint256 amount) internal {
        require(address(this).balance >= amount, \"Address: insufficient balance\");

        (bool success, ) = recipient.call{value: amount}(\"\");
        require(success, \"Address: unable to send value, recipient may have reverted\");
    }

    /**
     * @dev Performs a Solidity function call using a low level `call`. A
     * plain `call` is an unsafe replacement for a function call: use this
     * function instead.
     *
     * If `target` reverts with a revert reason, it is bubbled up by this
     * function (like regular Solidity function calls).
     *
     * Returns the raw returned data. To convert to the expected return value,
     * use https://solidity.readthedocs.io/en/latest/units-and-global-variables.html?highlight=abi.decode#abi-encoding-and-decoding-functions[`abi.decode`].
     *
     * Requirements:
     *
     * - `target` must be a contract.
     * - calling `target` with `data` must not revert.
     *
     * _Available since v3.1._
     */
    function functionCall(address target, bytes memory data) internal returns (bytes memory) {
        return functionCall(target, data, \"Address: low-level call failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`], but with
     * `errorMessage` as a fallback revert reason when `target` reverts.
     *
     * _Available since v3.1._
     */
    function functionCall(
        address target,
        bytes memory data,
        string memory errorMessage
    ) internal returns (bytes memory) {
        return functionCallWithValue(target, data, 0, errorMessage);
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`],
     * but also transferring `value` wei to `target`.
     *
     * Requirements:
     *
     * - the calling contract must have an ETH balance of at least `value`.
     * - the called Solidity function must be `payable`.
     *
     * _Available since v3.1._
     */
    function functionCallWithValue(
        address target,
        bytes memory data,
        uint256 value
    ) internal returns (bytes memory) {
        return functionCallWithValue(target, data, value, \"Address: low-level call with value failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCallWithValue-address-bytes-uint256-}[`functionCallWithValue`], but
     * with `errorMessage` as a fallback revert reason when `target` reverts.
     *
     * _Available since v3.1._
     */
    function functionCallWithValue(
        address target,
        bytes memory data,
        uint256 value,
        string memory errorMessage
    ) internal returns (bytes memory) {
        require(address(this).balance >= value, \"Address: insufficient balance for call\");
        require(isContract(target), \"Address: call to non-contract\");

        (bool success, bytes memory returndata) = target.call{value: value}(data);
        return verifyCallResult(success, returndata, errorMessage);
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`],
     * but performing a static call.
     *
     * _Available since v3.3._
     */
    function functionStaticCall(address target, bytes memory data) internal view returns (bytes memory) {
        return functionStaticCall(target, data, \"Address: low-level static call failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-string-}[`functionCall`],
     * but performing a static call.
     *
     * _Available since v3.3._
     */
    function functionStaticCall(
        address target,
        bytes memory data,
        string memory errorMessage
    ) internal view returns (bytes memory) {
        require(isContract(target), \"Address: static call to non-contract\");

        (bool success, bytes memory returndata) = target.staticcall(data);
        return verifyCallResult(success, returndata, errorMessage);
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`],
     * but performing a delegate call.
     *
     * _Available since v3.4._
     */
    function functionDelegateCall(address target, bytes memory data) internal returns (bytes memory) {
        return functionDelegateCall(target, data, \"Address: low-level delegate call failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-string-}[`functionCall`],
     * but performing a delegate call.
     *
     * _Available since v3.4._
     */
    function functionDelegateCall(
        address target,
        bytes memory data,
        string memory errorMessage
    ) internal returns (bytes memory) {
        require(isContract(target), \"Address: delegate call to non-contract\");

        (bool success, bytes memory returndata) = target.delegatecall(data);
        return verifyCallResult(success, returndata, errorMessage);
    }

    /**
     * @dev Tool to verifies that a low level call was successful, and revert if it wasn't, either by bubbling the
     * revert reason using the provided one.
     *
     * _Available since v4.3._
     */
    function verifyCallResult(
        bool success,
        bytes memory returndata,
        string memory errorMessage
    ) internal pure returns (bytes memory) {
        if (success) {
            return returndata;
        } else {
            // Look for revert reason and bubble it up if present
            if (returndata.length > 0) {
                // The easiest way to bubble the revert reason is using memory via assembly

                assembly {
                    let returndata_size := mload(returndata)
                    revert(add(32, returndata), returndata_size)
                }
            } else {
                revert(errorMessage);
            }
        }
    }
}
"
    
// SPDX-License-Identifier: MIT
// OpenZeppelin Contracts v4.4.1 (utils/Context.sol)

pragma solidity ^0.8.0;

/**
 * @dev Provides information about the current execution context, including the
 * sender of the transaction and its data. While these are generally available
 * via msg.sender and msg.data, they should not be accessed in such a direct
 * manner, since when dealing with meta-transactions the account sending and
 * paying for execution may not be the actual sender (as far as an application
 * is concerned).
 *
 * This contract is only required for intermediate, library-like contracts.
 */
abstract contract Context {
    function _msgSender() internal view virtual returns (address) {
        return msg.sender;
    }

    function _msgData() internal view virtual returns (bytes calldata) {
        return msg.data;
    }
}
"
    
// SPDX-License-Identifier: MIT
// OpenZeppelin Contracts v4.4.1 (utils/Strings.sol)

pragma solidity ^0.8.0;

/**
 * @dev String operations.
 */
library Strings {
    bytes16 private constant _HEX_SYMBOLS = \"0123456789abcdef\";

    /**
     * @dev Converts a `uint256` to its ASCII `string` decimal representation.
     */
    function toString(uint256 value) internal pure returns (string memory) {
        // Inspired by OraclizeAPI's implementation - MIT licence
        // https://github.com/oraclize/ethereum-api/blob/b42146b063c7d6ee1358846c198246239e9360e8/oraclizeAPI_0.4.25.sol

        if (value == 0) {
            return \"0\";
        }
        uint256 temp = value;
        uint256 digits;
        while (temp != 0) {
            digits++;
            temp /= 10;
        }
        bytes memory buffer = new bytes(digits);
        while (value != 0) {
            digits -= 1;
            buffer[digits] = bytes1(uint8(48 + uint256(value % 10)));
            value /= 10;
        }
        return string(buffer);
    }

    /**
     * @dev Converts a `uint256` to its ASCII `string` hexadecimal representation.
     */
    function toHexString(uint256 value) internal pure returns (string memory) {
        if (value == 0) {
            return \"0x00\";
        }
        uint256 temp = value;
        uint256 length = 0;
        while (temp != 0) {
            length++;
            temp >>= 8;
        }
        return toHexString(value, length);
    }

    /**
     * @dev Converts a `uint256` to its ASCII `string` hexadecimal representation with fixed length.
     */
    function toHexString(uint256 value, uint256 length) internal pure returns (string memory) {
        bytes memory buffer = new bytes(2 * length + 2);
        buffer[0] = \"0\";
        buffer[1] = \"x\";
        for (uint256 i = 2 * length + 1; i > 1; --i) {
            buffer[i] = _HEX_SYMBOLS[value & 0xf];
            value >>= 4;
        }
        require(value == 0, \"Strings: hex length insufficient\");
        return string(buffer);
    }
}
"
    
// SPDX-License-Identifier: MIT
// OpenZeppelin Contracts v4.4.1 (utils/introspection/ERC165.sol)

pragma solidity ^0.8.0;

import \"./IERC165.sol\";

/**
 * @dev Implementation of the {IERC165} interface.
 *
 * Contracts that want to implement ERC165 should inherit from this contract and override {supportsInterface} to check
 * for the additional interface id that will be supported. For example:
 *
 * ```solidity
 * function supportsInterface(bytes4 interfaceId) public view virtual override returns (bool) {
 *     return interfaceId == type(MyInterface).interfaceId || super.supportsInterface(interfaceId);
 * }
 * ```
 *
 * Alternatively, {ERC165Storage} provides an easier to use but more expensive implementation.
 */
abstract contract ERC165 is IERC165 {
    /**
     * @dev See {IERC165-supportsInterface}.
     */
    function supportsInterface(bytes4 interfaceId) public view virtual override returns (bool) {
        return interfaceId == type(IERC165).interfaceId;
    }
}
"
    
// SPDX-License-Identifier: MIT
// OpenZeppelin Contracts v4.4.1 (utils/introspection/IERC165.sol)

pragma solidity ^0.8.0;

/**
 * @dev Interface of the ERC165 standard, as defined in the
 * https://eips.ethereum.org/EIPS/eip-165[EIP].
 *
 * Implementers can declare support of contract interfaces, which can then be
 * queried by others ({ERC165Checker}).
 *
 * For an implementation, see {ERC165}.
 */
interface IERC165 {
    /**
     * @dev Returns true if this contract implements the interface defined by
     * `interfaceId`. See the corresponding
     * https://eips.ethereum.org/EIPS/eip-165#how-interfaces-are-identified[EIP section]
     * to learn more about how these ids are created.
     *
     * This function call must use less than 30 000 gas.
     */
    function supportsInterface(bytes4 interfaceId) external view returns (bool);
}
"
    
// SPDX-License-Identifier: MIT
/*
 * SquadOwnable.sol
 *
 * Created: December 21, 2021
 *
 * An extension of `Ownable.sol` to accomodate for a potential list of owners.
 * NOTE: this will need to be the last inherited contract to give all parents
 *       access to the modifiers it provides
 */

pragma solidity >=0.5.16 <0.9.0;

import \"@openzeppelin/contracts/access/Ownable.sol\";

//@title SquadOwnable
//@author Jack Kasbeer (gh:@jcksber, tw:@satoshigoat, ig:overprivilegd)
contract SquadOwnable is Ownable {

\t//@dev Ownership - list of squad members (owners)
\tmapping (address => bool) internal _squad;

\tconstructor() {
\t\t//add myself and then logik (client)
\t\t_squad[0xB9699469c0b4dD7B1Dda11dA7678Fa4eFD51211b] = true;
\t\t_squad[0x6b8C6E15818C74895c31A1C91390b3d42B336799] = true;
\t}

\t//@dev Custom modifier for multiple owners
\tmodifier isSquad()
\t{
\t\trequire(isInSquad(_msgSender()), \"SquadOwnable: Caller not part of squad.\");
\t\t_;
\t}

\t//@dev Determine if address `a` is an approved owner
\tfunction isInSquad(address a) public view returns (bool) 
\t{
\t\treturn _squad[a];
\t}

\t//@dev Add `a` to the squad
\tfunction addToSquad(address a) public onlyOwner
\t{
\t\trequire(!isInSquad(a), \"SquadOwnable: Address already in squad.\");
\t\t_squad[a] = true;
\t}

\t//@dev Remove `a` from the squad
\tfunction removeFromSquad(address a) public onlyOwner
\t{
\t\trequire(isInSquad(a), \"SquadOwnable: Address already not in squad.\");
\t\t_squad[a] = false;
\t}
}
"
    
// SPDX-License-Identifier: MIT
// OpenZeppelin Contracts v4.4.1 (access/Ownable.sol)

pragma solidity ^0.8.0;

import \"../utils/Context.sol\";

/**
 * @dev Contract module which provides a basic access control mechanism, where
 * there is an account (an owner) that can be granted exclusive access to
 * specific functions.
 *
 * By default, the owner account will be the one that deploys the contract. This
 * can later be changed with {transferOwnership}.
 *
 * This module is used through inheritance. It will make available the modifier
 * `onlyOwner`, which can be applied to your functions to restrict their use to
 * the owner.
 */
abstract contract Ownable is Context {
    address private _owner;

    event OwnershipTransferred(address indexed previousOwner, address indexed newOwner);

    /**
     * @dev Initializes the contract setting the deployer as the initial owner.
     */
    constructor() {
        _transferOwnership(_msgSender());
    }

    /**
     * @dev Returns the address of the current owner.
     */
    function owner() public view virtual returns (address) {
        return _owner;
    }

    /**
     * @dev Throws if called by any account other than the owner.
     */
    modifier onlyOwner() {
        require(owner() == _msgSender(), \"Ownable: caller is not the owner\");
        _;
    }

    /**
     * @dev Leaves the contract without owner. It will not be possible to call
     * `onlyOwner` functions anymore. Can only be called by the current owner.
     *
     * NOTE: Renouncing ownership will leave the contract without an owner,
     * thereby removing any functionality that is only available to the owner.
     */
    function renounceOwnership() public virtual onlyOwner {
        _transferOwnership(address(0));
    }

    /**
     * @dev Transfers ownership of the contract to a new account (`newOwner`).
     * Can only be called by the current owner.
     */
    function transferOwnership(address newOwner) public virtual onlyOwner {
        require(newOwner != address(0), \"Ownable: new owner is the zero address\");
        _transferOwnership(newOwner);
    }

    /**
     * @dev Transfers ownership of the contract to a new account (`newOwner`).
     * Internal function without access restriction.
     */
    function _transferOwnership(address newOwner) internal virtual {
        address oldOwner = _owner;
        _owner = newOwner;
        emit OwnershipTransferred(oldOwner, newOwner);
    }
}
"
    }
  
