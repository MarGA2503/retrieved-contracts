pragma solidity ^0.6.12;

// ----------------------------------------------------------------------------
// Safe maths
// ----------------------------------------------------------------------------
library SafeMath {
    function add(uint a, uint b) internal pure returns (uint c) {
        c = a + b;
        require(c \u003e= a, \u0027SafeMath:INVALID_ADD\u0027);
    }

    function sub(uint a, uint b) internal pure returns (uint c) {
        require(b \u003c= a, \u0027SafeMath:OVERFLOW_SUB\u0027);
        c = a - b;
    }

    function mul(uint a, uint b, uint decimal) internal pure returns (uint) {
        uint dc = 10**decimal;
        uint c0 = a * b;
        require(a == 0 || c0 / a == b, \"SafeMath: multiple overflow\");
        uint c1 = c0 + (dc / 2);
        require(c1 \u003e= c0, \"SafeMath: multiple overflow\");
        uint c2 = c1 / dc;
        return c2;
    }

    function div(uint256 a, uint256 b, uint decimal) internal pure returns (uint256) {
        require(b != 0, \"SafeMath: division by zero\");
        uint dc = 10**decimal;
        uint c0 = a * dc;
        require(a == 0 || c0 / a == dc, \"SafeMath: division internal\");
        uint c1 = c0 + (b / 2);
        require(c1 \u003e= c0, \"SafeMath: division internal\");
        uint c2 = c1 / b;
        return c2;
    }
}
"},"TransferHelper.sol":{"content":"pragma solidity ^0.6.12;

// helper methods for interacting with ERC20 tokens and sending ETH that do not consistently return true/false
library TransferHelper {
    function safeApprove(
        address token,
        address to,
        uint256 value
    ) internal {
        // bytes4(keccak256(bytes(\u0027approve(address,uint256)\u0027)));
        (bool success, bytes memory data) = token.call(abi.encodeWithSelector(0x095ea7b3, to, value));
        require(
            success \u0026\u0026 (data.length == 0 || abi.decode(data, (bool))),
            \u0027TransferHelper::safeApprove: approve failed\u0027
        );
    }

    function safeTransfer(
        address token,
        address to,
        uint256 value
    ) internal {
        // bytes4(keccak256(bytes(\u0027transfer(address,uint256)\u0027)));
        (bool success, bytes memory data) = token.call(abi.encodeWithSelector(0xa9059cbb, to, value));
        require(
            success \u0026\u0026 (data.length == 0 || abi.decode(data, (bool))),
            \u0027TransferHelper::safeTransfer: transfer failed\u0027
        );
    }

    function safeTransferFrom(
        address token,
        address from,
        address to,
        uint256 value
    ) internal {
        // bytes4(keccak256(bytes(\u0027transferFrom(address,address,uint256)\u0027)));
        (bool success, bytes memory data) = token.call(abi.encodeWithSelector(0x23b872dd, from, to, value));
        require(
            success \u0026\u0026 (data.length == 0 || abi.decode(data, (bool))),
            \u0027TransferHelper::transferFrom: transferFrom failed\u0027
        );
    }

    function safeTransferETH(address to, uint256 value) internal {
        (bool success, ) = to.call{value: value}(new bytes(0));
        require(success, \u0027TransferHelper::safeTransferETH: ETH transfer failed\u0027);
    }
}
"},"TubeChief.sol":{"content":"pragma solidity ^0.6.12;

import \u0027./TransferHelper.sol\u0027;
import \u0027./SafeMath.sol\u0027;

contract TubeChief {
    using SafeMath for uint;

    uint constant DECIMAL = 18;
    uint constant FARMER  = 750000000000000000;
    uint constant DEV     = 100000000000000000;
    uint constant LOTTERY = 150000000000000000;

    struct PoolInfo {
        address lpTokenAddress; // the LP token pair address
        uint rewardPerBlock;    // number of TUBE will mint per block
        uint lastBlockNo;       // record pool mint finish last block number
        uint lastDevBlockNo;    // record token mint to development last block number
        uint lastLotBlockNo;    // record token mint to lottery last block number
        uint accLpStaked;       // accumulate number of LP token user staked
        uint accLastBlockNo;    // record last pass in block number
        uint multiplier;        // reward multiplier
        uint accTokenPerShare;  // accumulated token per share
        bool locked;            // pool is locked
        bool finished;          // pool is stop mint token. disable deposit. only allow claim
    }

    struct UserPoolInfo {
        uint lpStaked;       // user staked LP
        uint rewardDebt;     // user debt
        uint lastClaimBlock; // last block number user retrieve reward
    }

    mapping(uint =\u003e PoolInfo) public pools; // dynamic pool container (pool ID =\u003e pool related data)
    mapping(address =\u003e uint[]) poolIdByLp;  // pool ids recorder (LP token =\u003e pool ids)

    // user pool allocate (user addr =\u003e (\u003cpool ID\u003e =\u003e user pool data))
    mapping(address =\u003e mapping(uint =\u003e UserPoolInfo)) public users;

    address public owner;   // owner of tube chief
    address public tube;    // the TUBE token
    address public devaddr; // development address
    address public lotaddr; // lottery address
    uint public poolLength; // next pool id. current length is (poolLength - 1)

    event CreatePool(address lpTokenAddress, uint rewardPerBlock, uint poolId);
    event UpdatePool(uint poolId, uint rewardPerBlock, uint multiplier, bool locked);
    event Claim(uint poolId, uint amount, uint claimable);
    event TransferCompany(address old_owner, address new_owner);

    modifier onlyOwner {
        require(msg.sender == owner, \u0027NOT OWNER\u0027);
        _;
    }

    constructor (address _tube, address _devaddr, address _lotaddr) public {
        owner   = msg.sender;
        tube    = _tube;
        devaddr = _devaddr;
        lotaddr = _lotaddr;
    }

    // create new pool. only owner executable
    // XX do not create twice on same LP token. reward will mess up if you do
    function createPool(address _lpTokenAddress, uint _rewardPerBlock, uint _multiplier) public onlyOwner {
        require(_lpTokenAddress != address(0), \u0027CREATE_POOL_EMPTY_ADDRESS\u0027);

        emit CreatePool(_lpTokenAddress, _rewardPerBlock, poolLength);
        pools[poolLength].lpTokenAddress = _lpTokenAddress;
        pools[poolLength].rewardPerBlock = _rewardPerBlock;
        pools[poolLength].multiplier     = _multiplier;
        pools[poolLength].accLastBlockNo = block.number;
        pools[poolLength].lastDevBlockNo = block.number;
        pools[poolLength].lastLotBlockNo = block.number;
        poolIdByLp[_lpTokenAddress].push(poolLength);
        poolLength = poolLength.add(1);
    }

    // update pool setting, edit wisely. only owner executable
    function updatePool(uint poolId, uint _rewardPerBlock, uint _multiplier, bool _locked) public onlyOwner {
        _updateAccTokenPerShare(poolId);
        pools[poolId].rewardPerBlock = _rewardPerBlock;
        pools[poolId].multiplier     = _multiplier;
        pools[poolId].locked         = _locked;
        emit UpdatePool(poolId, _rewardPerBlock, _multiplier, _locked);
    }

    // update development address. only owner executable
    function updateDevAddr(address _address) public onlyOwner {
        devaddr = _address;
    }

    // update lottery address. only owner executable
    function updateLotAddr(address _address) public onlyOwner {
        lotaddr = _address;
    }

    // set pool stop mint token. claim reward based on last block number recorded. only owner executable
    function updatePoolFinish(uint poolId, bool _finished) public onlyOwner {
        pools[poolId].finished    = _finished;
        pools[poolId].lastBlockNo = _finished ? block.number : 0;
    }

    // stake LP token to earn TUBE
    function stake(uint poolId, uint amount) public {
        require(pools[poolId].lpTokenAddress != address(0), \u0027STAKE_POOL_NOT_EXIST\u0027);
        require(pools[poolId].locked == false, \u0027STAKE_POOL_LOCKED\u0027);
        require(pools[poolId].finished == false, \u0027STAKE_POOL_FINISHED\u0027);

        claim(poolId, 0);
        TransferHelper.safeTransferFrom(pools[poolId].lpTokenAddress, msg.sender, address(this), amount);
        pools[poolId].accLpStaked = pools[poolId].accLpStaked.add(amount);
        users[msg.sender][poolId].lpStaked       = users[msg.sender][poolId].lpStaked.add(amount);
        users[msg.sender][poolId].lastClaimBlock = block.number;
        users[msg.sender][poolId].rewardDebt     = pools[poolId].accTokenPerShare.mul(users[msg.sender][poolId].lpStaked, DECIMAL);
    }

    // claim TUBE token. input LP token to exit pool
    function claim(uint poolId, uint amount) public {
        require(pools[poolId].lpTokenAddress != address(0), \u0027CLAIM_POOL_NOT_EXIST\u0027);
        require(pools[poolId].locked == false, \u0027CLAIM_POOL_LOCKED\u0027);
        
        _updateAccTokenPerShare(poolId);

        uint claimable = _getRewardAmount(poolId);
        if (claimable \u003e 0) {
            ITubeToken(tube).farmMint(address(this), claimable);
            TransferHelper.safeTransfer(tube, msg.sender, claimable);
            users[msg.sender][poolId].lastClaimBlock = block.number;
        }

        if (amount \u003e 0) {
            TransferHelper.safeTransfer(pools[poolId].lpTokenAddress, msg.sender, amount);
            users[msg.sender][poolId].lpStaked = users[msg.sender][poolId].lpStaked.sub(amount);
            pools[poolId].accLpStaked = pools[poolId].accLpStaked.sub(amount);
        }

        // emit if necessary. cost saving
        if (claimable \u003e 0 || amount \u003e 0) {
            emit Claim(poolId, amount, claimable);
        }

        // update the user reward debt at this moment
        users[msg.sender][poolId].rewardDebt = pools[poolId].accTokenPerShare.mul(users[msg.sender][poolId].lpStaked, DECIMAL);
    }

    // get token per share with current block number
    function getAccTokenInfo(uint poolId) public view returns (uint) {
        if (pools[poolId].accLpStaked \u003c= 0) {
            return 0;
        }

        uint reward_block = pools[poolId].rewardPerBlock;
        uint multiplier   = pools[poolId].multiplier;
        uint total_staked = pools[poolId].accLpStaked;
        uint pending      = block.number.sub(pools[poolId].accLastBlockNo);
        pending           = pending * 10**DECIMAL; // cast to \"wei\" unit
        uint result       = reward_block.mul(multiplier, DECIMAL).mul(pending, DECIMAL).mul(FARMER, DECIMAL);

        return result.div(total_staked, DECIMAL);
    }

    // emergency collect token from the contract. only owner executable
    function emergencyCollectToken(address token, uint amount) public onlyOwner {
        IERC20(token).transfer(owner, amount);
    }

    // emergency collect eth from the contract. only owner executable
    function emergencyCollectEth(uint amount) public onlyOwner {
        address payable owner_address = payable(owner);
        owner_address.send(amount);
    }

    // transfer ownership. proceed wisely. only owner executable
    function transferCompany(address new_owner) public onlyOwner {
        owner = new_owner;
        emit TransferCompany(owner, new_owner);
    }

    // transfer mintable token to development address
    function transferDev(uint poolId) public onlyOwner {
        uint mintable = getExMintable(poolId, true);
        require(mintable \u003e 0, \u0027TRANSFER_DEV_EMPTY\u0027);
        ITubeToken(tube).farmMint(address(this), mintable);
        TransferHelper.safeTransfer(tube, devaddr, mintable);
        pools[poolId].lastDevBlockNo = block.number;
    }

    // transfer mintable token to lottery address
    function transferLottery(uint poolId) public onlyOwner {
        uint mintable = getExMintable(poolId, false);
        require(mintable \u003e 0, \u0027TRANSFER_LOT_EMPTY\u0027);
        ITubeToken(tube).farmMint(address(this), mintable);
        TransferHelper.safeTransfer(tube, lotaddr, mintable);
        pools[poolId].lastLotBlockNo = block.number;
    }

    // retrieve the mintable amount for development or lottery
    function getExMintable(uint poolId, bool is_dev) public view returns (uint) {
        uint last_block   = 0;
        uint rate         = 0;

        if (is_dev) {
            last_block = pools[poolId].lastDevBlockNo;
            rate       = DEV;
        } else {
            last_block = pools[poolId].lastLotBlockNo;
            rate       = LOTTERY;
        }

        uint block_diff = block.number.sub(last_block);
        block_diff      = block_diff * 10**DECIMAL;

        return block_diff.mul(pools[poolId].rewardPerBlock, DECIMAL).mul(pools[poolId].multiplier, DECIMAL).mul(rate, DECIMAL);
    }

    // retrieve pool ids by LP token address
    function getPidByLpToken(address _lpTokenAddress) public view returns (uint[] memory) {
        return poolIdByLp[_lpTokenAddress];
    }

    // retrieve user reward info on the pool with current block number
    function getUserReward(uint poolId) public view returns (uint, uint, uint, uint, uint) {
        uint accTokenPerShare = getAccTokenInfo(poolId);
        accTokenPerShare      = accTokenPerShare.add(pools[poolId].accTokenPerShare);
        
        uint claimable = accTokenPerShare.mul(users[msg.sender][poolId].lpStaked, DECIMAL).sub(users[msg.sender][poolId].rewardDebt);
        return (block.number, claimable, accTokenPerShare, users[msg.sender][poolId].lpStaked, users[msg.sender][poolId].rewardDebt);
    }
    
    function _updateAccTokenPerShare(uint poolId) internal {
        uint result = getAccTokenInfo(poolId);
        pools[poolId].accTokenPerShare = pools[poolId].accTokenPerShare.add(result);
        pools[poolId].accLastBlockNo   = block.number;
    }

    function _getRewardAmount(uint poolId) view internal returns (uint) {
        if (pools[poolId].accLpStaked \u003c= 0) {
            return (0);
        }

        uint user_staked = users[msg.sender][poolId].lpStaked;
        uint user_debt   = users[msg.sender][poolId].rewardDebt;
        uint claimable   = pools[poolId].accTokenPerShare.mul(user_staked, DECIMAL).sub(user_debt);

        return (claimable);
    }

    fallback() external payable {
    }
}

interface ITubeToken {
    function farmMint(address _address, uint amount) external;
}

interface IERC20 {
    function transfer(address to, uint tokens) external returns (bool success);
}

