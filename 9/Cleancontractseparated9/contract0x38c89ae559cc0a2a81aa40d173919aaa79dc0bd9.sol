// Be name Khoda
// SPDX-License-Identifier: MIT
pragma solidity 0.8.9;
pragma abicoder v2;

// =================================================================================================================
//  _|_|_|    _|_|_|_|  _|    _|    _|_|_|      _|_|_|_|  _|                                                       |
//  _|    _|  _|        _|    _|  _|            _|            _|_|_|      _|_|_|  _|_|_|      _|_|_|    _|_|       |
//  _|    _|  _|_|_|    _|    _|    _|_|        _|_|_|    _|  _|    _|  _|    _|  _|    _|  _|        _|_|_|_|     |
//  _|    _|  _|        _|    _|        _|      _|        _|  _|    _|  _|    _|  _|    _|  _|        _|           |
//  _|_|_|    _|_|_|_|    _|_|    _|_|_|        _|        _|  _|    _|    _|_|_|  _|    _|    _|_|_|    _|_|_|     |
// =================================================================================================================
// ========================= DEUSZapper =========================
// ==============================================================
// DEUS Finance: https://github.com/DeusFinance

// Primary Author(s)
// Vahid Gh: https://github.com/vahid-dev

import \"@openzeppelin/contracts/access/Ownable.sol\";
import \"@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol\";


library Babylonian {
\tfunction sqrt(uint256 y) internal pure returns (uint256 z) {
\t\tif (y > 3) {
\t\t\tz = y;
\t\t\tuint256 x = y / 2 + 1;
\t\t\twhile (x < z) {
\t\t\t\tz = x;
\t\t\t\tx = (y / x + x) / 2;
\t\t\t}
\t\t} else if (y != 0) {
\t\t\tz = 1;
\t\t}
\t\t// else z = 0
\t}
}

interface IUniswapV2Router02 {
\tfunction factory() external pure returns (address);
\tfunction addLiquidity(
\t\taddress tokenA,
\t\taddress tokenB,
\t\tuint256 amountADesired,
\t\tuint256 amountBDesired,
\t\tuint256 amountAMin,
\t\tuint256 amountBMin,
\t\taddress to,
\t\tuint256 deadline
\t) external returns (uint256 amountA, uint256 amountB, uint256 liquidity);

\tfunction swapExactTokensForTokens(
\t\tuint256 amountIn,
\t\tuint256 amountOutMin,
\t\taddress[] calldata path,
\t\taddress to,
\t\tuint256 deadline
\t) external returns (uint256[] memory amounts);

\tfunction swapExactETHForTokens(
\t\tuint256 amountOutMin,
\t\taddress[] calldata path,
\t\taddress to,
\t\tuint256 deadline
\t) external payable returns (uint256[] memory amounts);

\tfunction getAmountsOut(
\t\tuint256 amountIn, 
\t\taddress[] calldata path
\t) external view returns (uint256[] memory amounts);
}

interface IUniwapV2Pair {
\tfunction token0() external pure returns (address);
\tfunction token1() external pure returns (address);
\tfunction totalSupply() external view returns (uint);
\tfunction getReserves() external view returns (uint112 _reserve0, uint112 _reserve1, uint32 _blockTimestampLast);
}

interface IStaking {
\tfunction depositFor(address _user, uint256 amount) external;
}

struct ProxyInput {
\tuint amountIn;
\tuint minAmountOut;
\tuint deusPriceUSD;
\tuint colPriceUSD;
\tuint usdcForMintAmount;
\tuint deusNeededAmount;
\tuint expireBlock;
\tbytes[] sigs;
}

interface IDEIProxy {
\tfunction getUSDC2DEIInputs(
\t\tuint amountIn, 
\t\tuint deusPriceUSD, 
\t\tuint colPriceUSD
\t) external view returns (uint amountOut, uint usdcForMintAmount, uint deusNeededAmount);
\tfunction USDC2DEI(ProxyInput memory proxyInput) external returns (uint deiAmount);
}

contract DEI_DEUS_ZAPPER is Ownable {
\tusing SafeERC20 for IERC20;

\t/* ========== STATE VARIABLES ========== */

\tbool public stopped;
\taddress public uniswapRouter;
\taddress public pairAddress;
\taddress public usdcAddress;
\taddress public deiAddress;
\taddress public deusAddress;
\taddress public stakingAddress;
\taddress public deiProxyAddress;
\tuint private constant deadline = 0xf000000000000000000000000000000000000000000000000000000000000000;

\t/* ========== CONSTRUCTOR ========== */

\tconstructor (
\t\taddress _pairAddress,
\t\taddress _usdcAddress,
\t\taddress _deiAddress,
\t\taddress _deusAddress,
\t\taddress _uniswapRouter,
\t\taddress _stakingAddress,
\t\taddress _deiProxyAddress
\t) {
\t\tuniswapRouter = _uniswapRouter;
\t\tpairAddress = _pairAddress;
\t\tusdcAddress = _usdcAddress;
\t\tdeiAddress = _deiAddress;
\t\tdeusAddress = _deusAddress;
\t\tstakingAddress = _stakingAddress;
\t\tdeiProxyAddress = _deiProxyAddress;
\t\tinit();
\t}

\t/* ========== RESTRICTED FUNCTIONS ========== */

\tfunction approve(address token, address to) public onlyOwner {
\t\tIERC20(token).safeApprove(to, type(uint256).max);
\t}

\tfunction emergencyWithdrawERC20(address token, address to, uint amount) external onlyOwner {
\t\tIERC20(token).safeTransfer(to, amount);
\t}

\tfunction emergencyWithdrawETH(address recv, uint amount) external onlyOwner {
\t\tpayable(recv).transfer(amount);
\t}

\tfunction setStaking(address _stakingAddress) external onlyOwner {
\t\tstakingAddress = _stakingAddress;
\t\temit StakingSet(stakingAddress);
\t}

\tfunction setVariables(
\t\taddress _pairAddress,
\t\taddress _usdcAddress,
\t\taddress _deiAddress,
\t\taddress _deusAddress,
\t\taddress _uniswapRouter,
\t\taddress _stakingAddress,
\t\taddress _deiProxyAddress
\t) external onlyOwner {
\t\tuniswapRouter = _uniswapRouter;
\t\tpairAddress = _pairAddress;
\t\tusdcAddress = _usdcAddress;
\t\tdeiAddress = _deiAddress;
\t\tdeusAddress = _deusAddress;
\t\tstakingAddress = _stakingAddress;
\t\tdeiProxyAddress = _deiProxyAddress;
\t}

\t// circuit breaker modifiers
\tmodifier stopInEmergency() {
\t\trequire(!stopped, \"ZAPPER: temporarily paused\");
\t\t_;
\t}

\t/* ========== PUBLIC FUNCTIONS ========== */

\tfunction zapInNativecoin(
\t\tProxyInput memory proxyInput,
\t\tuint256 minLPAmount,
\t\taddress[] calldata path,
\t\tbool transferResidual  // Set false to save gas by donating the residual remaining after a Zap
\t) external payable {
\t\tproxyInput.amountIn = IUniswapV2Router02(uniswapRouter).swapExactETHForTokens{value: msg.value}(1, path, address(this), deadline)[path.length-1];

\t\tuint deiAmount = IDEIProxy(deiProxyAddress).USDC2DEI(proxyInput);

\t\t(uint256 token0Bought, uint256 token1Bought) = _buyTokens(deiAddress, deiAmount);

\t\tuint256 LPBought = _uniDeposit(IUniwapV2Pair(pairAddress).token0(),
\t\t\t\t\t\t\t\t\t\tIUniwapV2Pair(pairAddress).token1(),
\t\t\t\t\t\t\t\t\t\ttoken0Bought,
\t\t\t\t\t\t\t\t\t\ttoken1Bought,
\t\t\t\t\t\t\t\t\t\ttransferResidual);

\t\trequire(LPBought >= minLPAmount, \"ZAPPER: Insufficient output amount\");

\t\tIStaking(stakingAddress).depositFor(msg.sender, LPBought);

\t\temit ZappedIn(address(0), pairAddress, msg.value, LPBought, transferResidual);
\t}


\tfunction zapInERC20(
\t\tProxyInput memory proxyInput,
\t\tuint256 amountIn,
\t\tuint256 minLPAmount,
\t\taddress[] calldata path,
\t\tbool transferResidual  // Set false to save gas by donating the residual remaining after a Zap
\t) external {
\t\tIERC20(path[0]).safeTransferFrom(msg.sender, address(this), amountIn);
\t\t
\t\tif (path[0] != usdcAddress) {
\t\t\t// approve token if doesn't have allowance
\t\t\tif (IERC20(path[0]).allowance(address(this), uniswapRouter) == 0) IERC20(path[0]).safeApprove(uniswapRouter, type(uint).max);
\t\t\tproxyInput.amountIn = IUniswapV2Router02(uniswapRouter).swapExactTokensForTokens(amountIn, 1, path, address(this), deadline)[path.length-1];
\t\t}

\t\tuint deiAmount = IDEIProxy(deiProxyAddress).USDC2DEI(proxyInput);

\t\t(uint256 token0Bought, uint256 token1Bought) = _buyTokens(deiAddress, deiAmount);

\t\tuint256 LPBought = _uniDeposit(IUniwapV2Pair(pairAddress).token0(),
\t\t\t\t\t\t\t\t\t\tIUniwapV2Pair(pairAddress).token1(),
\t\t\t\t\t\t\t\t\t\ttoken0Bought,
\t\t\t\t\t\t\t\t\t\ttoken1Bought,
\t\t\t\t\t\t\t\t\t\ttransferResidual);

\t\trequire(LPBought >= minLPAmount, \"ZAPPER: Insufficient output amount\");

\t\tIStaking(stakingAddress).depositFor(msg.sender, LPBought);

\t\temit ZappedIn(path[0], pairAddress, amountIn, LPBought, transferResidual);
\t}


\tfunction zapInDEI(
\t\tuint256 amountIn,
\t\tuint256 minLPAmount,
\t\tbool transferResidual  // Set false to save gas by donating the residual remaining after a Zap
\t) public stopInEmergency {
\t\tIERC20(deiAddress).safeTransferFrom(msg.sender, address(this), amountIn);

\t\t(uint256 token0Bought, uint256 token1Bought) = _buyTokens(deiAddress, amountIn);

\t\tuint256 LPBought = _uniDeposit(IUniwapV2Pair(pairAddress).token0(),
\t\t\t\t\t\t\t\t\t\tIUniwapV2Pair(pairAddress).token1(),
\t\t\t\t\t\t\t\t\t\ttoken0Bought,
\t\t\t\t\t\t\t\t\t\ttoken1Bought,
\t\t\t\t\t\t\t\t\t\ttransferResidual);

\t\trequire(LPBought >= minLPAmount, \"ZAPPER: Insufficient output amount\");

\t\tIStaking(stakingAddress).depositFor(msg.sender, LPBought);

\t\temit ZappedIn(deiAddress, pairAddress, amountIn, LPBought, transferResidual);
\t}

\tfunction zapInDEUS(
\t\tuint amountIn,
\t\tuint minLPAmount,
\t\tbool transferResidual
\t) external stopInEmergency {
\t\tIERC20(deusAddress).safeTransferFrom(msg.sender, address(this), amountIn);

\t\t(uint256 token0Bought, uint256 token1Bought) = _buyTokens(deusAddress, amountIn);

\t\tuint256 LPBought = _uniDeposit(IUniwapV2Pair(pairAddress).token0(),
\t\t\t\t\t\t\t\t\tIUniwapV2Pair(pairAddress).token1(),
\t\t\t\t\t\t\t\t\ttoken0Bought,
\t\t\t\t\t\t\t\t\ttoken1Bought,
\t\t\t\t\t\t\t\t\ttransferResidual);

\t\trequire(LPBought >= minLPAmount, \"ZAPPER: Insufficient output amount\");

\t\tIStaking(stakingAddress).depositFor(msg.sender, LPBought);

\t\temit ZappedIn(deiAddress, pairAddress, amountIn, LPBought, transferResidual);
\t}

\tfunction _buyTokens(address inputToken, uint256 _amount) internal returns(uint256 token0Bought, uint256 token1Bought) {
\t\tIUniwapV2Pair pair = IUniwapV2Pair(pairAddress);
\t\t(uint res0, uint256 res1, ) = pair.getReserves();
\t\tif (inputToken == pair.token0()) {
\t\t\tuint256 amountToSwap = calculateSwapInAmount(res0, _amount);\t
\t\t\tif (amountToSwap <= 0) amountToSwap = _amount / 2;
\t\t\ttoken1Bought = _token2Token(
\t\t\t\t\t\t\tinputToken,
\t\t\t\t\t\t\tpair.token1(), // it depend on pair tokens (token0 or token1)
\t\t\t\t\t\t\tamountToSwap
\t\t\t\t\t\t);
\t\t\ttoken0Bought = _amount - amountToSwap;
\t\t} else {
\t\t\tuint256 amountToSwap = calculateSwapInAmount(res1, _amount);
\t\t\tif (amountToSwap <= 0) amountToSwap = _amount / 2;
\t\t\ttoken0Bought = _token2Token(
\t\t\t\t\t\t\t\tinputToken,
\t\t\t\t\t\t\t\tpair.token0(), // it depend on pair tokens (token0 or token1)
\t\t\t\t\t\t\t\tamountToSwap
\t\t\t\t\t\t\t);
\t\t\ttoken1Bought = _amount - amountToSwap;
\t\t}
\t}

\tfunction _token2Token(
\t\taddress _fromToken,
\t\taddress _toToken,
\t\tuint256 tokens2Trade
\t) internal returns (uint256 tokenBought) {
\t\taddress[] memory path = new address[](2);
\t\tpath[0] = _fromToken;
\t\tpath[1] = _toToken;

\t\ttokenBought = IUniswapV2Router02(uniswapRouter).swapExactTokensForTokens(tokens2Trade,
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t1,
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tpath,
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\taddress(this),
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tdeadline)[path.length-1];

\t\trequire(tokenBought > 0, \"ZAPPER: Error swapExactTokensForTokens\");
\t}

\tfunction calculateSwapInAmount(uint256 reserveIn, uint256 userIn)
\t\tinternal
\t\tpure
\t\treturns (uint256)
\t{
\t\treturn (Babylonian.sqrt(reserveIn * ((userIn * 3988000) + (reserveIn * 3988009))) - (reserveIn * 1997)) / 1994;
\t}

\tfunction _uniDeposit(
\t\taddress _toUnipoolToken0,
\t\taddress _toUnipoolToken1,
\t\tuint256 token0Bought,
\t\tuint256 token1Bought,
\t\tbool transferResidual
\t) internal returns(uint256) {
\t\t(uint256 amountA, uint256 amountB, uint256 LP) = IUniswapV2Router02(uniswapRouter).addLiquidity(_toUnipoolToken0,
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t_toUnipoolToken1,
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\ttoken0Bought,
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\ttoken1Bought,
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t1,
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t1,
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\taddress(this),
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tdeadline);

\t\tif (transferResidual) {
\t\t\t//Returning Residue in token0, if any.
\t\t\tif (token0Bought - amountA > 0) {
\t\t\t\tIERC20(_toUnipoolToken0).safeTransfer(
\t\t\t\t\tmsg.sender,
\t\t\t\t\ttoken0Bought - amountA
\t\t\t\t);
\t\t\t}

\t\t\t//Returning Residue in token1, if any
\t\t\tif (token1Bought - amountB > 0) {
\t\t\t\tIERC20(_toUnipoolToken1).safeTransfer(
\t\t\t\t\tmsg.sender,
\t\t\t\t\ttoken1Bought - amountB
\t\t\t\t);
\t\t\t}
\t\t}

\t\treturn LP;
\t}

\t/* ========== VIEWS ========== */

\tfunction getAmountOutLPERC20ORNativecoin(uint amountIn, uint deusPriceUSD, uint colPriceUSD, address[] memory path) public view returns (uint percentage, uint lp, uint usdcForMintAmount, uint deusNeededAmount) {
\t\tuint deiAmount;
\t\tuint usdcAmount;

\t\tif (path[0] != usdcAddress) {
\t\t\tusdcAmount = IUniswapV2Router02(uniswapRouter).getAmountsOut(amountIn, path)[path.length - 1];
\t\t} else {
\t\t\tusdcAmount = amountIn;
\t\t}

\t\t(deiAmount, usdcForMintAmount, deusNeededAmount) = IDEIProxy(deiProxyAddress).getUSDC2DEIInputs(usdcAmount, deusPriceUSD, colPriceUSD);
\t\t
\t\tIUniwapV2Pair pair = IUniwapV2Pair(pairAddress);
\t\tuint totalSupply = pair.totalSupply();
\t\t(uint res0, uint256 res1, ) = pair.getReserves(); 
\t\tuint dei_to_deus_amount;
\t\tif (deiAddress == pair.token0()) {
\t\t\tdei_to_deus_amount = calculateSwapInAmount(res0, deiAmount);
\t\t\tuint dei_pure_amount = deiAmount - dei_to_deus_amount; 
\t\t\tpercentage = dei_pure_amount * 1e6 / (res0 + dei_pure_amount);
\t\t\tlp = dei_pure_amount * totalSupply / res0;
\t\t} else {
\t\t\tdei_to_deus_amount = calculateSwapInAmount(res1, deiAmount);
\t\t\tuint dei_pure_amount = deiAmount - dei_to_deus_amount; 
\t\t\tpercentage = dei_pure_amount * 1e6 / (res1 + dei_pure_amount);
\t\t\tlp = dei_pure_amount * totalSupply / res1;
\t\t}
\t}

\tfunction getAmountOutLPDEUS(uint amount) public view returns (uint percentage, uint lp) {
\t\tIUniwapV2Pair pair = IUniwapV2Pair(pairAddress);
\t\tuint totalSupply = pair.totalSupply();
\t\t(uint res0, uint256 res1, ) = pair.getReserves(); 
\t\tuint deus_to_dei_amount;
\t\tif (deusAddress == pair.token0()) {
\t\t\tdeus_to_dei_amount = calculateSwapInAmount(res0, amount);
\t\t\tuint deus_pure_amount = amount - deus_to_dei_amount; 
\t\t\tpercentage = deus_pure_amount * 1e6 / (res0 + amount);
\t\t\tlp = deus_pure_amount * totalSupply / res0;
\t\t} else {
\t\t\tdeus_to_dei_amount = calculateSwapInAmount(res1, amount);
\t\t\tuint deus_pure_amount = amount - deus_to_dei_amount; 
\t\t\tpercentage = deus_pure_amount * 1e6 / (res1 + amount);
\t\t\tlp = deus_pure_amount * totalSupply / res1;
\t\t}
\t}

\tfunction getAmountOutLPDEI(uint dei_amount) public view returns (uint percentage, uint lp){
\t\tIUniwapV2Pair pair = IUniwapV2Pair(pairAddress);
\t\tuint totalSupply = pair.totalSupply();
\t\t(uint res0, uint256 res1, ) = pair.getReserves(); 
\t\tuint dei_to_deus_amount;
\t\tif (deiAddress == pair.token0()) {
\t\t\tdei_to_deus_amount = calculateSwapInAmount(res0, dei_amount);
\t\t\tuint dei_pure_amount = dei_amount - dei_to_deus_amount; 
\t\t\tpercentage = dei_pure_amount * 1e6 / (res0 + dei_pure_amount);
\t\t\tlp = dei_pure_amount * totalSupply / res0;
\t\t} else {
\t\t\tdei_to_deus_amount = calculateSwapInAmount(res1, dei_amount);
\t\t\tuint dei_pure_amount = dei_amount - dei_to_deus_amount; 
\t\t\tpercentage = dei_pure_amount * 1e6 / (res1 + dei_pure_amount);
\t\t\tlp = dei_pure_amount * totalSupply / res1;
\t\t}
\t}

\tfunction init() public onlyOwner {
\t\tapprove(IUniwapV2Pair(pairAddress).token0(), uniswapRouter);
\t\tapprove(IUniwapV2Pair(pairAddress).token1(), uniswapRouter);
\t\tapprove(pairAddress, stakingAddress);
\t\tapprove(usdcAddress, deiProxyAddress);
\t}

\t// to Pause the contract
\tfunction toggleContractActive() external onlyOwner {
\t\tstopped = !stopped;
\t}

\tevent StakingSet(address staking);
\tevent ZappedIn(address input_token, address output_token, uint input_amount, uint output_amount, bool transfer_residual);
}

// Dar panahe Khoda"
    
// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import \"../utils/Context.sol\";

/**
 * @dev Contract module which provides a basic access control mechanism, where
 * there is an account (an owner) that can be granted exclusive access to
 * specific functions.
 *
 * By default, the owner account will be the one that deploys the contract. This
 * can later be changed with {transferOwnership}.
 *
 * This module is used through inheritance. It will make available the modifier
 * `onlyOwner`, which can be applied to your functions to restrict their use to
 * the owner.
 */
abstract contract Ownable is Context {
    address private _owner;

    event OwnershipTransferred(address indexed previousOwner, address indexed newOwner);

    /**
     * @dev Initializes the contract setting the deployer as the initial owner.
     */
    constructor() {
        _setOwner(_msgSender());
    }

    /**
     * @dev Returns the address of the current owner.
     */
    function owner() public view virtual returns (address) {
        return _owner;
    }

    /**
     * @dev Throws if called by any account other than the owner.
     */
    modifier onlyOwner() {
        require(owner() == _msgSender(), \"Ownable: caller is not the owner\");
        _;
    }

    /**
     * @dev Leaves the contract without owner. It will not be possible to call
     * `onlyOwner` functions anymore. Can only be called by the current owner.
     *
     * NOTE: Renouncing ownership will leave the contract without an owner,
     * thereby removing any functionality that is only available to the owner.
     */
    function renounceOwnership() public virtual onlyOwner {
        _setOwner(address(0));
    }

    /**
     * @dev Transfers ownership of the contract to a new account (`newOwner`).
     * Can only be called by the current owner.
     */
    function transferOwnership(address newOwner) public virtual onlyOwner {
        require(newOwner != address(0), \"Ownable: new owner is the zero address\");
        _setOwner(newOwner);
    }

    function _setOwner(address newOwner) private {
        address oldOwner = _owner;
        _owner = newOwner;
        emit OwnershipTransferred(oldOwner, newOwner);
    }
}
"
    
// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import \"../IERC20.sol\";
import \"../../../utils/Address.sol\";

/**
 * @title SafeERC20
 * @dev Wrappers around ERC20 operations that throw on failure (when the token
 * contract returns false). Tokens that return no value (and instead revert or
 * throw on failure) are also supported, non-reverting calls are assumed to be
 * successful.
 * To use this library you can add a `using SafeERC20 for IERC20;` statement to your contract,
 * which allows you to call the safe operations as `token.safeTransfer(...)`, etc.
 */
library SafeERC20 {
    using Address for address;

    function safeTransfer(
        IERC20 token,
        address to,
        uint256 value
    ) internal {
        _callOptionalReturn(token, abi.encodeWithSelector(token.transfer.selector, to, value));
    }

    function safeTransferFrom(
        IERC20 token,
        address from,
        address to,
        uint256 value
    ) internal {
        _callOptionalReturn(token, abi.encodeWithSelector(token.transferFrom.selector, from, to, value));
    }

    /**
     * @dev Deprecated. This function has issues similar to the ones found in
     * {IERC20-approve}, and its usage is discouraged.
     *
     * Whenever possible, use {safeIncreaseAllowance} and
     * {safeDecreaseAllowance} instead.
     */
    function safeApprove(
        IERC20 token,
        address spender,
        uint256 value
    ) internal {
        // safeApprove should only be called when setting an initial allowance,
        // or when resetting it to zero. To increase and decrease it, use
        // 'safeIncreaseAllowance' and 'safeDecreaseAllowance'
        require(
            (value == 0) || (token.allowance(address(this), spender) == 0),
            \"SafeERC20: approve from non-zero to non-zero allowance\"
        );
        _callOptionalReturn(token, abi.encodeWithSelector(token.approve.selector, spender, value));
    }

    function safeIncreaseAllowance(
        IERC20 token,
        address spender,
        uint256 value
    ) internal {
        uint256 newAllowance = token.allowance(address(this), spender) + value;
        _callOptionalReturn(token, abi.encodeWithSelector(token.approve.selector, spender, newAllowance));
    }

    function safeDecreaseAllowance(
        IERC20 token,
        address spender,
        uint256 value
    ) internal {
        unchecked {
            uint256 oldAllowance = token.allowance(address(this), spender);
            require(oldAllowance >= value, \"SafeERC20: decreased allowance below zero\");
            uint256 newAllowance = oldAllowance - value;
            _callOptionalReturn(token, abi.encodeWithSelector(token.approve.selector, spender, newAllowance));
        }
    }

    /**
     * @dev Imitates a Solidity high-level call (i.e. a regular function call to a contract), relaxing the requirement
     * on the return value: the return value is optional (but if data is returned, it must not be false).
     * @param token The token targeted by the call.
     * @param data The call data (encoded using abi.encode or one of its variants).
     */
    function _callOptionalReturn(IERC20 token, bytes memory data) private {
        // We need to perform a low level call here, to bypass Solidity's return data size checking mechanism, since
        // we're implementing it ourselves. We use {Address.functionCall} to perform this call, which verifies that
        // the target address contains contract code and also asserts for success in the low-level call.

        bytes memory returndata = address(token).functionCall(data, \"SafeERC20: low-level call failed\");
        if (returndata.length > 0) {
            // Return data is optional
            require(abi.decode(returndata, (bool)), \"SafeERC20: ERC20 operation did not succeed\");
        }
    }
}
"
    
// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

/*
 * @dev Provides information about the current execution context, including the
 * sender of the transaction and its data. While these are generally available
 * via msg.sender and msg.data, they should not be accessed in such a direct
 * manner, since when dealing with meta-transactions the account sending and
 * paying for execution may not be the actual sender (as far as an application
 * is concerned).
 *
 * This contract is only required for intermediate, library-like contracts.
 */
abstract contract Context {
    function _msgSender() internal view virtual returns (address) {
        return msg.sender;
    }

    function _msgData() internal view virtual returns (bytes calldata) {
        return msg.data;
    }
}
"
    
// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

/**
 * @dev Interface of the ERC20 standard as defined in the EIP.
 */
interface IERC20 {
    /**
     * @dev Returns the amount of tokens in existence.
     */
    function totalSupply() external view returns (uint256);

    /**
     * @dev Returns the amount of tokens owned by `account`.
     */
    function balanceOf(address account) external view returns (uint256);

    /**
     * @dev Moves `amount` tokens from the caller's account to `recipient`.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transfer(address recipient, uint256 amount) external returns (bool);

    /**
     * @dev Returns the remaining number of tokens that `spender` will be
     * allowed to spend on behalf of `owner` through {transferFrom}. This is
     * zero by default.
     *
     * This value changes when {approve} or {transferFrom} are called.
     */
    function allowance(address owner, address spender) external view returns (uint256);

    /**
     * @dev Sets `amount` as the allowance of `spender` over the caller's tokens.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * IMPORTANT: Beware that changing an allowance with this method brings the risk
     * that someone may use both the old and the new allowance by unfortunate
     * transaction ordering. One possible solution to mitigate this race
     * condition is to first reduce the spender's allowance to 0 and set the
     * desired value afterwards:
     * https://github.com/ethereum/EIPs/issues/20#issuecomment-263524729
     *
     * Emits an {Approval} event.
     */
    function approve(address spender, uint256 amount) external returns (bool);

    /**
     * @dev Moves `amount` tokens from `sender` to `recipient` using the
     * allowance mechanism. `amount` is then deducted from the caller's
     * allowance.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transferFrom(
        address sender,
        address recipient,
        uint256 amount
    ) external returns (bool);

    /**
     * @dev Emitted when `value` tokens are moved from one account (`from`) to
     * another (`to`).
     *
     * Note that `value` may be zero.
     */
    event Transfer(address indexed from, address indexed to, uint256 value);

    /**
     * @dev Emitted when the allowance of a `spender` for an `owner` is set by
     * a call to {approve}. `value` is the new allowance.
     */
    event Approval(address indexed owner, address indexed spender, uint256 value);
}
"
    
// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

/**
 * @dev Collection of functions related to the address type
 */
library Address {
    /**
     * @dev Returns true if `account` is a contract.
     *
     * [IMPORTANT]
     * ====
     * It is unsafe to assume that an address for which this function returns
     * false is an externally-owned account (EOA) and not a contract.
     *
     * Among others, `isContract` will return false for the following
     * types of addresses:
     *
     *  - an externally-owned account
     *  - a contract in construction
     *  - an address where a contract will be created
     *  - an address where a contract lived, but was destroyed
     * ====
     */
    function isContract(address account) internal view returns (bool) {
        // This method relies on extcodesize, which returns 0 for contracts in
        // construction, since the code is only stored at the end of the
        // constructor execution.

        uint256 size;
        assembly {
            size := extcodesize(account)
        }
        return size > 0;
    }

    /**
     * @dev Replacement for Solidity's `transfer`: sends `amount` wei to
     * `recipient`, forwarding all available gas and reverting on errors.
     *
     * https://eips.ethereum.org/EIPS/eip-1884[EIP1884] increases the gas cost
     * of certain opcodes, possibly making contracts go over the 2300 gas limit
     * imposed by `transfer`, making them unable to receive funds via
     * `transfer`. {sendValue} removes this limitation.
     *
     * https://diligence.consensys.net/posts/2019/09/stop-using-soliditys-transfer-now/[Learn more].
     *
     * IMPORTANT: because control is transferred to `recipient`, care must be
     * taken to not create reentrancy vulnerabilities. Consider using
     * {ReentrancyGuard} or the
     * https://solidity.readthedocs.io/en/v0.5.11/security-considerations.html#use-the-checks-effects-interactions-pattern[checks-effects-interactions pattern].
     */
    function sendValue(address payable recipient, uint256 amount) internal {
        require(address(this).balance >= amount, \"Address: insufficient balance\");

        (bool success, ) = recipient.call{value: amount}(\"\");
        require(success, \"Address: unable to send value, recipient may have reverted\");
    }

    /**
     * @dev Performs a Solidity function call using a low level `call`. A
     * plain `call` is an unsafe replacement for a function call: use this
     * function instead.
     *
     * If `target` reverts with a revert reason, it is bubbled up by this
     * function (like regular Solidity function calls).
     *
     * Returns the raw returned data. To convert to the expected return value,
     * use https://solidity.readthedocs.io/en/latest/units-and-global-variables.html?highlight=abi.decode#abi-encoding-and-decoding-functions[`abi.decode`].
     *
     * Requirements:
     *
     * - `target` must be a contract.
     * - calling `target` with `data` must not revert.
     *
     * _Available since v3.1._
     */
    function functionCall(address target, bytes memory data) internal returns (bytes memory) {
        return functionCall(target, data, \"Address: low-level call failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`], but with
     * `errorMessage` as a fallback revert reason when `target` reverts.
     *
     * _Available since v3.1._
     */
    function functionCall(
        address target,
        bytes memory data,
        string memory errorMessage
    ) internal returns (bytes memory) {
        return functionCallWithValue(target, data, 0, errorMessage);
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`],
     * but also transferring `value` wei to `target`.
     *
     * Requirements:
     *
     * - the calling contract must have an ETH balance of at least `value`.
     * - the called Solidity function must be `payable`.
     *
     * _Available since v3.1._
     */
    function functionCallWithValue(
        address target,
        bytes memory data,
        uint256 value
    ) internal returns (bytes memory) {
        return functionCallWithValue(target, data, value, \"Address: low-level call with value failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCallWithValue-address-bytes-uint256-}[`functionCallWithValue`], but
     * with `errorMessage` as a fallback revert reason when `target` reverts.
     *
     * _Available since v3.1._
     */
    function functionCallWithValue(
        address target,
        bytes memory data,
        uint256 value,
        string memory errorMessage
    ) internal returns (bytes memory) {
        require(address(this).balance >= value, \"Address: insufficient balance for call\");
        require(isContract(target), \"Address: call to non-contract\");

        (bool success, bytes memory returndata) = target.call{value: value}(data);
        return _verifyCallResult(success, returndata, errorMessage);
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`],
     * but performing a static call.
     *
     * _Available since v3.3._
     */
    function functionStaticCall(address target, bytes memory data) internal view returns (bytes memory) {
        return functionStaticCall(target, data, \"Address: low-level static call failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-string-}[`functionCall`],
     * but performing a static call.
     *
     * _Available since v3.3._
     */
    function functionStaticCall(
        address target,
        bytes memory data,
        string memory errorMessage
    ) internal view returns (bytes memory) {
        require(isContract(target), \"Address: static call to non-contract\");

        (bool success, bytes memory returndata) = target.staticcall(data);
        return _verifyCallResult(success, returndata, errorMessage);
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`],
     * but performing a delegate call.
     *
     * _Available since v3.4._
     */
    function functionDelegateCall(address target, bytes memory data) internal returns (bytes memory) {
        return functionDelegateCall(target, data, \"Address: low-level delegate call failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-string-}[`functionCall`],
     * but performing a delegate call.
     *
     * _Available since v3.4._
     */
    function functionDelegateCall(
        address target,
        bytes memory data,
        string memory errorMessage
    ) internal returns (bytes memory) {
        require(isContract(target), \"Address: delegate call to non-contract\");

        (bool success, bytes memory returndata) = target.delegatecall(data);
        return _verifyCallResult(success, returndata, errorMessage);
    }

    function _verifyCallResult(
        bool success,
        bytes memory returndata,
        string memory errorMessage
    ) private pure returns (bytes memory) {
        if (success) {
            return returndata;
        } else {
            // Look for revert reason and bubble it up if present
            if (returndata.length > 0) {
                // The easiest way to bubble the revert reason is using memory via assembly

                assembly {
                    let returndata_size := mload(returndata)
                    revert(add(32, returndata), returndata_size)
                }
            } else {
                revert(errorMessage);
            }
        }
    }
}
"
    }
  
