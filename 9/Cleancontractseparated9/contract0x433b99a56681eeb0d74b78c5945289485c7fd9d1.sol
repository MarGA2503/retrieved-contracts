// SPDX-License-Identifier: UNLICENSED

pragma solidity ^0.8.4;

/*______/\\\\\\\\\\\\\\\\\\__/\\\\\\_______/\\\\\\__/\\\\\\\\\\\\\\\\\\\\\\__/\\\\\\\\\\\\\\\\\\\\\\\\\\___        
 _____/\\\\\\////////__\\///\\\\\\___/\\\\\\/__\\/////\\\\\\///__\\/\\\\\\/////////\\\\\\_       
  ___/\\\\\\/_____________\\///\\\\\\\\\\\\/________\\/\\\\\\_____\\/\\\\\\_______\\/\\\\\\_      
   __/\\\\\\_________________\\//\\\\\\\\__________\\/\\\\\\_____\\/\\\\\\\\\\\\\\\\\\\\\\\\\\/__     
    _\\/\\\\\\__________________\\/\\\\\\\\__________\\/\\\\\\_____\\/\\\\\\/////////____    
     _\\//\\\\\\_________________/\\\\\\\\\\\\_________\\/\\\\\\_____\\/\\\\\\_____________   
      __\\///\\\\\\_____________/\\\\\\////\\\\\\_______\\/\\\\\\_____\\/\\\\\\_____________  
       ____\\////\\\\\\\\\\\\\\\\\\__/\\\\\\/___\\///\\\\\\__/\\\\\\\\\\\\\\\\\\\\\\_\\/\\\\\\_____________ 
        _______\\/////////__\\///_______\\///__\\///////////__\\///____________*/

import \u0027./ICxipRegistry.sol\u0027;

contract CxipIdentityProxy {

\tfallback () payable external {
\t\taddress _target = ICxipRegistry (0xC267d41f81308D7773ecB3BDd863a902ACC01Ade).getIdentitySource ();
\t\tassembly {
\t\t\tcalldatacopy (0, 0, calldatasize ())
\t\t\tlet result := delegatecall (gas (), _target, 0, calldatasize (), 0, 0)
\t\t\treturndatacopy (0, 0, returndatasize ())
\t\t\tswitch result
\t\t\t\tcase 0 {
\t\t\t\t\trevert (0, returndatasize ())
\t\t\t\t}
\t\t\t\tdefault {
\t\t\t\t\treturn (0, returndatasize ())
\t\t\t\t}
\t\t}
\t}

}
"},"ICxipRegistry.sol":{"content":"// SPDX-License-Identifier: UNLICENSED

pragma solidity ^0.8.4;

/*______/\\\\\\\\\\\\\\\\\\__/\\\\\\_______/\\\\\\__/\\\\\\\\\\\\\\\\\\\\\\__/\\\\\\\\\\\\\\\\\\\\\\\\\\___        
 _____/\\\\\\////////__\\///\\\\\\___/\\\\\\/__\\/////\\\\\\///__\\/\\\\\\/////////\\\\\\_       
  ___/\\\\\\/_____________\\///\\\\\\\\\\\\/________\\/\\\\\\_____\\/\\\\\\_______\\/\\\\\\_      
   __/\\\\\\_________________\\//\\\\\\\\__________\\/\\\\\\_____\\/\\\\\\\\\\\\\\\\\\\\\\\\\\/__     
    _\\/\\\\\\__________________\\/\\\\\\\\__________\\/\\\\\\_____\\/\\\\\\/////////____    
     _\\//\\\\\\_________________/\\\\\\\\\\\\_________\\/\\\\\\_____\\/\\\\\\_____________   
      __\\///\\\\\\_____________/\\\\\\////\\\\\\_______\\/\\\\\\_____\\/\\\\\\_____________  
       ____\\////\\\\\\\\\\\\\\\\\\__/\\\\\\/___\\///\\\\\\__/\\\\\\\\\\\\\\\\\\\\\\_\\/\\\\\\_____________ 
        _______\\/////////__\\///_______\\///__\\///////////__\\///____________*/

interface ICxipRegistry {

\tfunction getPA1D () external view returns (address);

\tfunction setPA1D (address proxy) external;

\tfunction getPA1DSource () external view returns (address);

\tfunction setPA1DSource (address source) external;

\tfunction getAsset () external view returns (address);

\tfunction setAsset (address proxy) external;

\tfunction getAssetSource () external view returns (address);

\tfunction setAssetSource (address source) external;

\tfunction getCopyright () external view returns (address);

\tfunction setCopyright (address proxy) external;

\tfunction getCopyrightSource () external view returns (address);

\tfunction setCopyrightSource (address source) external;

\tfunction getProvenance () external view returns (address);

\tfunction setProvenance (address proxy) external;

\tfunction getProvenanceSource () external view returns (address);

\tfunction setProvenanceSource (address source) external;

\tfunction getIdentitySource () external view returns (address);

\tfunction setIdentitySource (address source) external;

\tfunction getERC721CollectionSource () external view returns (address);

\tfunction setERC721CollectionSource (address source) external;

\tfunction getERC1155CollectionSource () external view returns (address);

\tfunction setERC1155CollectionSource (address source) external;

\tfunction getAssetSigner () external view returns (address);

\tfunction setAssetSigner (address source) external;

\tfunction getCustomSource (bytes32 name) external view returns (address);

\tfunction getCustomSourceFromString (string memory name) external view returns (address);

\tfunction setCustomSource (string memory name, address source) external;

\tfunction owner () external view returns (address);

}
