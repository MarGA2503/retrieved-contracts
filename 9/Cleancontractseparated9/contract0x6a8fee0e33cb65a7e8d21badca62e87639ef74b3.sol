pragma solidity ^0.5.0;

/**
 * @title ERC20 interface
 * @dev see https://eips.ethereum.org/EIPS/eip-20
 */
interface IERC20 {
    function transfer(address to, uint256 value) external returns (bool);

    function approve(address spender, uint256 value) external returns (bool);

    function transferFrom(address from, address to, uint256 value) external returns (bool);

    function totalSupply() external view returns (uint256);

    function balanceOf(address who) external view returns (uint256);

    function allowance(address owner, address spender) external view returns (uint256);
}"},"OwnershipStorage.sol":{"content":"pragma solidity ^0.5.0;

contract OwnershipStorage {
    address internal _admin;

    constructor () public {
        _admin = msg.sender;
    }

    function admin() public view returns (address) {
        return _admin;
    }

    function setNewAdmin(address newAdmin) public {
        require(msg.sender == _admin);
        require(newAdmin != address(0));
        _admin = newAdmin;
    }
}"},"Proxy.sol":{"content":"pragma solidity ^0.5.0;

contract Proxy {
    function implementation() public view returns (address);

    function() payable external {
        address _impl = implementation();
        require(_impl != address(0));
        bytes memory data = msg.data;

        // forward the call to the implemetation contract
        assembly {
            let result := delegatecall(gas, _impl, add(data, 0x20), mload(data), 0, 0)
            let size := returndatasize
            let ptr := mload(0x40)
            returndatacopy(ptr, 0, size)
            switch result
            case 0 { revert(ptr, size) }
            default { return(ptr, size) }
        }
    }
}"},"ReentrancyGuard.sol":{"content":"pragma solidity ^0.5.0;

/**
 * @title Helps contracts guard against reentrancy attacks.
 * @dev If you mark a function `nonReentrant`, you should also
 * mark it `external`.
 */
contract ReentrancyGuard {
    /// @dev counter to allow mutex lock with only one SSTORE operation
    uint256 private _guardCounter;

    constructor () internal {
        // The counter starts at one to prevent changing it from zero to a non-zero
        // value, which is a more expensive operation.
        _guardCounter = 1;
    }

    /**
     * @dev Prevents a contract from calling itself, directly or indirectly.
     * Calling a `nonReentrant` function from another `nonReentrant`
     * function is not supported. It is possible to prevent this from happening
     * by making the `nonReentrant` function external, and make it call a
     * `private` function that does the actual work.
     */
    modifier nonReentrant() {
        _guardCounter += 1;
        uint256 localCounter = _guardCounter;
        _;
        require(localCounter == _guardCounter);
    }
}"},"SafeMath.sol":{"content":"pragma solidity ^0.5.0;

/**
 * @title SafeMath
 * @dev Unsigned math operations with safety checks that revert on error
 */
library SafeMath {
    /**
     * @dev Multiplies two unsigned integers, reverts on overflow.
     */
    function mul(uint256 a, uint256 b) internal pure returns (uint256) {
        // Gas optimization: this is cheaper than requiring \u0027a\u0027 not being zero, but the
        // benefit is lost if \u0027b\u0027 is also tested.
        // See: https://github.com/OpenZeppelin/openzeppelin-solidity/pull/522
        if (a == 0) {
            return 0;
        }

        uint256 c = a * b;
        require(c / a == b);

        return c;
    }

    /**
     * @dev Integer division of two unsigned integers truncating the quotient, reverts on division by zero.
     */
    function div(uint256 a, uint256 b) internal pure returns (uint256) {
        // Solidity only automatically asserts when dividing by 0
        require(b \u003e 0);
        uint256 c = a / b;
        // assert(a == b * c + a % b); // There is no case in which this doesn\u0027t hold

        return c;
    }

    /**
     * @dev Subtracts two unsigned integers, reverts on overflow (i.e. if subtrahend is greater than minuend).
     */
    function sub(uint256 a, uint256 b) internal pure returns (uint256) {
        require(b \u003c= a);
        uint256 c = a - b;

        return c;
    }

    /**
     * @dev Adds two unsigned integers, reverts on overflow.
     */
    function add(uint256 a, uint256 b) internal pure returns (uint256) {
        uint256 c = a + b;
        require(c \u003e= a);

        return c;
    }

    /**
     * @dev Divides two unsigned integers and returns the remainder (unsigned integer modulo),
     * reverts when dividing by zero.
     */
    function mod(uint256 a, uint256 b) internal pure returns (uint256) {
        require(b != 0);
        return a % b;
    }
}
"},"TokenProxy.sol":{"content":"pragma solidity ^0.5.0;

import \"./UpgradeabilityProxy.sol\";
import \"./TokenStorage.sol\";
import \"./OwnershipStorage.sol\";

contract TokenProxy is UpgradeabilityProxy, TokenStorage, OwnershipStorage {
    constructor (
        string memory name,
        string memory symbol,
        uint8 decimals
    ) 
        public 
    {
        _name = name;
        _symbol = symbol;
        _decimals = decimals;
        _tokensMinted = false;
    } 
}"},"TokenStorage.sol":{"content":"pragma solidity ^0.5.0;

contract TokenStorage {
    string internal _name;
    string internal _symbol;
    uint8 internal _decimals;
    uint256 internal _totalSupply;
    mapping(address =\u003e uint256) internal _balances;
    mapping(address =\u003e mapping(address =\u003e uint256)) internal _allowances;
    bool internal _tokensMinted = true;
}"},"TokenV0.sol":{"content":"pragma solidity ^0.5.0;

import \"./UpgradeableTokenStorage.sol\";
import \"./SafeMath.sol\";
import \"./IERC20.sol\";

contract TokenV0 is IERC20, UpgradeableTokenStorage {
    using SafeMath for uint256;

    event Transfer(address indexed from, address indexed to, uint256 value);

    event Approval(address indexed owner, address indexed spender, uint256 value);

    /**
     * @return the name of the token.
     */
    function name() public view returns (string memory) {
        return _name;
    }

    /**
     * @return the symbol of the token.
     */
    function symbol() public view returns (string memory) {
        return _symbol;
    }

    /**
     * @return the number of decimals of the token.
     */
    function decimals() public view returns (uint8) {
        return _decimals;
    }

    /**
     * @dev Total number of tokens in existence
     */
    function totalSupply() public view returns (uint256) {
        return _totalSupply;
    }

    /**
     * @dev Gets the balance of the specified address.
     * @param owner The address to query the balance of.
     * @return A uint256 representing the amount owned by the passed address.
     */
    function balanceOf(address owner) public view returns (uint256) {
        return _balances[owner];
    }

    /**
     * @dev Function to check the amount of tokens that an owner allowed to a spender.
     * @param owner address The address which owns the funds.
     * @param spender address The address which will spend the funds.
     * @return A uint256 specifying the amount of tokens still available for the spender.
     */
    function allowance(address owner, address spender) public view returns (uint256) {
        return _allowances[owner][spender];
    }

    /**
     * @dev Transfer token to a specified address
     * @param to The address to transfer to.
     * @param value The amount to be transferred.
     */
    function transfer(address to, uint256 value) public returns (bool) {
        _transfer(msg.sender, to, value);
        return true;
    }

    /**
     * @dev Approve the passed address to spend the specified amount of tokens on behalf of msg.sender.
     * Beware that changing an allowance with this method brings the risk that someone may use both the old
     * and the new allowance by unfortunate transaction ordering. One possible solution to mitigate this
     * race condition is to first reduce the spender\u0027s allowance to 0 and set the desired value afterwards:
     * https://github.com/ethereum/EIPs/issues/20#issuecomment-263524729
     * @param spender The address which will spend the funds.
     * @param value The amount of tokens to be spent.
     */
    function approve(address spender, uint256 value) public returns (bool) {
        _approve(msg.sender, spender, value);
        return true;
    }

    /**
     * @dev Transfer tokens from one address to another.
     * Note that while this function emits an Approval event, this is not required as per the specification,
     * and other compliant implementations may not emit the event.
     * @param from address The address which you want to send tokens from
     * @param to address The address which you want to transfer to
     * @param value uint256 the amount of tokens to be transferred
     */
    function transferFrom(address from, address to, uint256 value) public returns (bool) {
        _transfer(from, to, value);
        _approve(from, msg.sender, _allowances[from][msg.sender].sub(value));
        return true;
    }

    /**
     * @dev Function that mints an amount of the token and assigns it to
     * an account. This encapsulates the modification of balances such that the
     * proper events are emitted. Minting allowed only once.
     * @param account The account that will receive the created tokens.
     * @param value The amount that will be created.
     */
    function mint(address account, uint256 value) public {
        require(!_tokensMinted);
        require(msg.sender == _admin);
        require(account != address(0));

        _tokensMinted = true;
        _totalSupply = _totalSupply.add(value);
        _balances[account] = _balances[account].add(value);
        emit Transfer(address(0), account, value);
    }

    /**
     * @dev Transfer token for a specified addresses
     * @param from The address to transfer from.
     * @param to The address to transfer to.
     * @param value The amount to be transferred.
     */
    function _transfer(address from, address to, uint256 value) internal {
        require(to != address(0));
        
        _balances[from] = _balances[from].sub(value);
        _balances[to] = _balances[to].add(value);
        emit Transfer(from, to, value);
    }

    /**
     * @dev Approve an address to spend another addresses\u0027 tokens.
     * @param owner The address that owns the tokens.
     * @param spender The address that will spend the tokens.
     * @param value The number of tokens that can be spent.
     */
    function _approve(address owner, address spender, uint256 value) internal {
        require(spender != address(0));
        require(owner != address(0));

        _allowances[owner][spender] = value;
        emit Approval(owner, spender, value);
    }
}"},"UpgradeabilityProxy.sol":{"content":"pragma solidity ^0.5.0;

import \"./Proxy.sol\";
import \"./UpgradeabilityStorage.sol\";

contract UpgradeabilityProxy is Proxy, UpgradeabilityStorage {
    function admin() public view returns (address);

    event Upgraded(string version, address indexed implementation);

    function upgradeTo(string memory version, address implementation) public {
        require(msg.sender == admin());
        require(_implementation != implementation);
        _version = version;
        _implementation = implementation;
        emit Upgraded(version, implementation);
    }
}"},"UpgradeabilityStorage.sol":{"content":"pragma solidity ^0.5.0;

contract UpgradeabilityStorage {
    string internal _version;
    address internal _implementation;

    function version() public view returns (string memory) {
        return _version;
    }

    function implementation() public view returns (address) {
        return _implementation;
    }
}"},"UpgradeableTokenStorage.sol":{"content":"pragma solidity ^0.5.0;

import \"./UpgradeabilityProxy.sol\";
import \"./TokenStorage.sol\";
import \"./OwnershipStorage.sol\";

contract UpgradeableTokenStorage is UpgradeabilityProxy, TokenStorage, OwnershipStorage {}
