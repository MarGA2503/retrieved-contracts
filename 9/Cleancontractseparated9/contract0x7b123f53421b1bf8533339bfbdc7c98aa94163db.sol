pragma solidity ^0.6.0;

interface ICommonUtilities {
    function toString(address _addr) external pure returns(string memory);
    function toString(uint _i) external pure returns(string memory);
    function toUint256(bytes calldata bs) external pure returns(uint256 x);
    function toAddress(bytes calldata b) external pure returns (address addr);
    function compareStrings(string calldata a, string calldata b) external pure returns(bool);
    function getFirstJSONPart(address sourceLocation, uint256 sourceLocationId, address location) external pure returns(bytes memory);
    function formatReturnAbiParametersArray(string calldata m) external pure returns(string memory);
    function toLowerCase(string calldata str) external pure returns(string memory);
}"},"IERC20.sol":{"content":"pragma solidity ^0.6.0;

interface IERC20 {
    function totalSupply() external view returns (uint256);
    function balanceOf(address account) external view returns (uint256);
    function transfer(address recipient, uint256 amount) external returns (bool);
    function allowance(address owner, address spender) external view returns (uint256);
    function approve(address spender, uint256 amount) external returns (bool);
    function transferFrom(address sender, address recipient, uint256 amount) external returns (bool);

    event Transfer(address indexed from, address indexed to, uint256 value);
    event Approval(address indexed owner, address indexed spender, uint256 value);
}"},"IERC721.sol":{"content":"pragma solidity ^0.6.0;

interface IERC721 {
    function ownerOf(uint256 _tokenId) external view returns (address);
    function transferFrom(address _from, address _to, uint256 _tokenId) external payable;
    function safeTransferFrom(address from, address to, uint256 tokenId, bytes calldata data) external;
}"},"IERC721Receiver.sol":{"content":"pragma solidity ^0.6.0;

interface IERC721Receiver {
    function onERC721Received(address operator, address from, uint256 tokenId, bytes calldata data) external returns (bytes4);
}"},"IMVDFunctionalitiesManager.sol":{"content":"pragma solidity ^0.6.0;

interface IMVDFunctionalitiesManager {

    function getProxy() external view returns (address);
    function setProxy() external;

    function init(address sourceLocation,
        uint256 getMinimumBlockNumberSourceLocationId, address getMinimumBlockNumberFunctionalityAddress,
        uint256 getEmergencyMinimumBlockNumberSourceLocationId, address getEmergencyMinimumBlockNumberFunctionalityAddress,
        uint256 getEmergencySurveyStakingSourceLocationId, address getEmergencySurveyStakingFunctionalityAddress,
        uint256 checkVoteResultSourceLocationId, address checkVoteResultFunctionalityAddress) external;

    function addFunctionality(string calldata codeName, address sourceLocation, uint256 sourceLocationId, address location, bool submitable, string calldata methodSignature, string calldata returnAbiParametersArray, bool isInternal, bool needsSender) external;
    function addFunctionality(string calldata codeName, address sourceLocation, uint256 sourceLocationId, address location, bool submitable, string calldata methodSignature, string calldata returnAbiParametersArray, bool isInternal, bool needsSender, uint256 position) external;
    function removeFunctionality(string calldata codeName) external returns(bool removed, uint256 position);
    function isValidFunctionality(address functionality) external view returns(bool);
    function isAuthorizedFunctionality(address functionality) external view returns(bool);
    function setCallingContext(address location) external returns(bool);
    function clearCallingContext() external;
    function getFunctionalityData(string calldata codeName) external view returns(address, uint256, string memory, address, uint256);
    function hasFunctionality(string calldata codeName) external view returns(bool);
    function getFunctionalitiesAmount() external view returns(uint256);
    function functionalitiesToJSON() external view returns(string memory);
    function functionalitiesToJSON(uint256 start, uint256 l) external view returns(string memory functionsJSONArray);
    function functionalityNames() external view returns(string memory);
    function functionalityNames(uint256 start, uint256 l) external view returns(string memory functionsJSONArray);
    function functionalityToJSON(string calldata codeName) external view returns(string memory);

    function preConditionCheck(string calldata codeName, bytes calldata data, uint8 submitable, address sender, uint256 value) external view returns(address location, bytes memory payload);

    function setupFunctionality(address proposalAddress) external returns (bool);
}"},"IMVDFunctionalityModelsManager.sol":{"content":"pragma solidity ^0.6.0;

interface IMVDFunctionalityModelsManager {
    function init() external;
    function checkWellKnownFunctionalities(string calldata codeName, bool submitable, string calldata methodSignature, string calldata returnAbiParametersArray, bool isInternal, bool needsSender, string calldata replaces) external view;
}"},"IMVDFunctionalityProposal.sol":{"content":"pragma solidity ^0.6.0;

interface IMVDFunctionalityProposal {

    function init(string calldata codeName, address location, string calldata methodSignature, string calldata returnAbiParametersArray, string calldata replaces, address proxy) external;
    function setCollateralData(bool emergency, address sourceLocation, uint256 sourceLocationId, bool submitable, bool isInternal, bool needsSender, address proposer, uint256 votesHardCap) external;

    function getProxy() external view returns(address);
    function getCodeName() external view returns(string memory);
    function isEmergency() external view returns(bool);
    function getSourceLocation() external view returns(address);
    function getSourceLocationId() external view returns(uint256);
    function getLocation() external view returns(address);
    function isSubmitable() external view returns(bool);
    function getMethodSignature() external view returns(string memory);
    function getReturnAbiParametersArray() external view returns(string memory);
    function isInternal() external view returns(bool);
    function needsSender() external view returns(bool);
    function getReplaces() external view returns(string memory);
    function getProposer() external view returns(address);
    function getSurveyEndBlock() external view returns(uint256);
    function getSurveyDuration() external view returns(uint256);
    function isVotesHardCapReached() external view returns(bool);
    function getVotesHardCapToReach() external view returns(uint256);
    function toJSON() external view returns(string memory);
    function getVote(address addr) external view returns(uint256 accept, uint256 refuse);
    function getVotes() external view returns(uint256, uint256);
    function start() external;
    function disable() external;
    function isDisabled() external view returns(bool);
    function isTerminated() external view returns(bool);
    function accept(uint256 amount) external;
    function retireAccept(uint256 amount) external;
    function moveToAccept(uint256 amount) external;
    function refuse(uint256 amount) external;
    function retireRefuse(uint256 amount) external;
    function moveToRefuse(uint256 amount) external;
    function retireAll() external;
    function withdraw() external;
    function terminate() external;
    function set() external;

    event Accept(address indexed voter, uint256 amount);
    event RetireAccept(address indexed voter, uint256 amount);
    event MoveToAccept(address indexed voter, uint256 amount);
    event Refuse(address indexed voter, uint256 amount);
    event RetireRefuse(address indexed voter, uint256 amount);
    event MoveToRefuse(address indexed voter, uint256 amount);
    event RetireAll(address indexed voter, uint256 amount);
}"},"IMVDFunctionalityProposalManager.sol":{"content":"pragma solidity ^0.6.0;

interface IMVDFunctionalityProposalManager {
    function newProposal(string calldata codeName, address location, string calldata methodSignature, string calldata returnAbiParametersArray, string calldata replaces) external returns(address);
    function checkProposal(address proposalAddress) external;
    function getProxy() external view returns (address);
    function setProxy() external;
    function isValidProposal(address proposal) external view returns (bool);
}"},"IMVDProxy.sol":{"content":"pragma solidity ^0.6.0;

interface IMVDProxy {

    function init(address votingTokenAddress, address functionalityProposalManagerAddress, address stateHolderAddress, address functionalityModelsManagerAddress, address functionalitiesManagerAddress, address walletAddress) external;

    function getDelegates() external view returns(address,address,address,address,address,address);
    function getToken() external view returns(address);
    function getMVDFunctionalityProposalManagerAddress() external view returns(address);
    function getStateHolderAddress() external view returns(address);
    function getMVDFunctionalityModelsManagerAddress() external view returns(address);
    function getMVDFunctionalitiesManagerAddress() external view returns(address);
    function getMVDWalletAddress() external view returns(address);
    function setDelegate(uint256 position, address newAddress) external returns(address oldAddress);
    function changeProxy(address newAddress, bytes calldata initPayload) external;
    function isValidProposal(address proposal) external view returns (bool);
    function isAuthorizedFunctionality(address functionality) external view returns(bool);
    function newProposal(string calldata codeName, bool emergency, address sourceLocation, uint256 sourceLocationId, address location, bool submitable, string calldata methodSignature, string calldata returnParametersJSONArray, bool isInternal, bool needsSender, string calldata replaces) external returns(address proposalAddress);
    function startProposal(address proposalAddress) external;
    function disableProposal(address proposalAddress) external;
    function transfer(address receiver, uint256 value, address token) external;
    function transfer721(address receiver, uint256 tokenId, bytes calldata data, bool safe, address token) external;
    function flushToWallet(address tokenAddress, bool is721, uint256 tokenId) external;
    function setProposal() external;
    function read(string calldata codeName, bytes calldata data) external view returns(bytes memory returnData);
    function submit(string calldata codeName, bytes calldata data) external payable returns(bytes memory returnData);
    function callFromManager(address location, bytes calldata payload) external returns(bool, bytes memory);
    function emitFromManager(string calldata codeName, address proposal, string calldata replaced, address replacedSourceLocation, uint256 replacedSourceLocationId, address location, bool submitable, string calldata methodSignature, bool isInternal, bool needsSender, address proposalAddress) external;

    function emitEvent(string calldata eventSignature, bytes calldata firstIndex, bytes calldata secondIndex, bytes calldata data) external;

    event ProxyChanged(address indexed newAddress);
    event DelegateChanged(uint256 position, address indexed oldAddress, address indexed newAddress);

    event Proposal(address proposal);
    event ProposalCheck(address indexed proposal);
    event ProposalSet(address indexed proposal, bool success);
    event FunctionalitySet(string codeName, address indexed proposal, string replaced, address replacedSourceLocation, uint256 replacedSourceLocationId, address indexed replacedLocation, bool replacedWasSubmitable, string replacedMethodSignature, bool replacedWasInternal, bool replacedNeededSender, address indexed replacedProposal);

    event Event(string indexed key, bytes32 indexed firstIndex, bytes32 indexed secondIndex, bytes data);
}"},"IMVDWallet.sol":{"content":"pragma solidity ^0.6.0;

interface IMVDWallet {

    function getProxy() external view returns (address);

    function setProxy() external;

    function setNewWallet(address payable newWallet, address tokenAddress) external;

    function transfer(address receiver, uint256 value, address tokenAddress) external;
    
    function transfer(address receiver, uint256 tokenId, bytes calldata data, bool safe, address token) external;

    function flushToNewWallet(address token) external;

    function flush721ToNewWallet(uint256 tokenId, bytes calldata data, bool safe, address tokenAddress) external;
}"},"IStateHolder.sol":{"content":"pragma solidity ^0.6.0;

interface IStateHolder {

    function init() external;

    function getProxy() external view returns (address);
    function setProxy() external;
    function toJSON() external view returns(string memory);
    function toJSON(uint256 start, uint256 l) external view returns(string memory);
    function getStateSize() external view returns (uint256);
    function exists(string calldata varName) external view returns(bool);
    function getDataType(string calldata varName) external view returns(string memory dataType);
    function clear(string calldata varName) external returns(string memory oldDataType, bytes memory oldVal);
    function setBytes(string calldata varName, bytes calldata val) external returns(bytes memory);
    function getBytes(string calldata varName) external view returns(bytes memory);
    function setString(string calldata varName, string calldata val) external returns(string memory);
    function getString(string calldata varName) external view returns (string memory);
    function setBool(string calldata varName, bool val) external returns(bool);
    function getBool(string calldata varName) external view returns (bool);
    function getUint256(string calldata varName) external view returns (uint256);
    function setUint256(string calldata varName, uint256 val) external returns(uint256);
    function getAddress(string calldata varName) external view returns (address);
    function setAddress(string calldata varName, address val) external returns (address);
}"},"IVotingToken.sol":{"content":"pragma solidity ^0.6.0;

interface IVotingToken {
    function init(string calldata name, string calldata symbol, uint256 decimals, uint256 totalSupply) external;

    function getProxy() external view returns (address);
    function setProxy() external;

    function name() external view returns(string memory);
    function symbol() external view returns(string memory);
    function decimals() external view returns(uint256);

    function mint(uint256 amount) external;
    function burn(uint256 amount) external;

    function increaseAllowance(address spender, uint256 addedValue) external returns (bool);
    function decreaseAllowance(address spender, uint256 subtractedValue) external returns (bool);
}"},"VotingToken.sol":{"content":"pragma solidity ^0.6.0;

import \"./IMVDProxy.sol\";
import \"./IERC20.sol\";
import \"./IVotingToken.sol\";
import \"./IMVDFunctionalityProposalManager.sol\";
import \"./IMVDFunctionalitiesManager.sol\";

contract VotingToken is IERC20, IVotingToken {

    mapping (address =\u003e uint256) private _balances;

    mapping (address =\u003e mapping (address =\u003e uint256)) private _allowances;

    uint256 private _totalSupply;
    uint256 private _decimals;
    address private _proxy;
    string private _name;
    string private _symbol;

    constructor(string memory name, string memory symbol, uint256 decimals, uint256 totalSupply) public {
        if(totalSupply == 0) {
            return;
        }
        init(name, symbol, decimals, totalSupply);
    }

    function init(string memory name, string memory symbol, uint256 decimals, uint256 totalSupply) public override {
        require(_totalSupply == 0, \"Init already called!\");

        _name = name;
        _symbol = symbol;
        _decimals = decimals;
        _totalSupply = totalSupply * (10 ** decimals);
        _balances[msg.sender] = _totalSupply;
        emit Transfer(address(this), msg.sender, _totalSupply);
    }

    receive() external payable {
        revert(\"ETH not accepted\");
    }

    function getProxy() public override view returns(address) {
        return _proxy;
    }

    function name() public override view returns(string memory) {
        return _name;
    }

    function symbol() public override view returns(string memory) {
        return _symbol;
    }

    function decimals() public override view returns(uint256) {
        return _decimals;
    }

    function totalSupply() public override view returns (uint256) {
        return _totalSupply;
    }

    function balanceOf(address account) public override view returns (uint256) {
        return _balances[account];
    }

    function transfer(address recipient, uint256 amount) public override returns (bool) {
        _transfer(msg.sender, recipient, amount);
        return true;
    }

    function allowance(address owner, address spender) public override view returns (uint256) {
        return _allowances[owner][spender];
    }

    function approve(address spender, uint256 amount) public override returns (bool) {
        _approve(msg.sender, spender, amount);
        return true;
    }

    function transferFrom(address sender, address recipient, uint256 amount) public override returns (bool) {
        _transfer(sender, recipient, amount);
        address txSender = msg.sender;
        if(_proxy == address(0) || !(IMVDFunctionalityProposalManager(IMVDProxy(_proxy).getMVDFunctionalityProposalManagerAddress()).isValidProposal(txSender) \u0026\u0026 recipient == txSender)) {
            _approve(sender, txSender, _allowances[sender][txSender] = sub(_allowances[sender][txSender], amount, \"ERC20: transfer amount exceeds allowance\"));
        }
        return true;
    }

    function increaseAllowance(address spender, uint256 addedValue) public override returns (bool) {
        _approve(msg.sender, spender, add(_allowances[msg.sender][spender], addedValue));
        return true;
    }

    function decreaseAllowance(address spender, uint256 subtractedValue) public override returns (bool) {
        _approve(msg.sender, spender, sub(_allowances[msg.sender][spender], subtractedValue, \"ERC20: decreased allowance below zero\"));
        return true;
    }

    function _transfer(address sender, address recipient, uint256 amount) internal {
        require(sender != address(0), \"ERC20: transfer from the zero address\");
        require(recipient != address(0), \"ERC20: transfer to the zero address\");

        _balances[sender] = sub(_balances[sender], amount, \"ERC20: transfer amount exceeds balance\");
        _balances[recipient] = add(_balances[recipient], amount);
        emit Transfer(sender, recipient, amount);
    }

    function _approve(address owner, address spender, uint256 amount) internal {
        require(owner != address(0), \"ERC20: approve from the zero address\");
        require(spender != address(0), \"ERC20: approve to the zero address\");

        _allowances[owner][spender] = amount;
        emit Approval(owner, spender, amount);
    }

    function add(uint256 a, uint256 b) internal pure returns (uint256 c) {
        c = a + b;
        require(c \u003e= a, \"SafeMath: addition overflow\");
    }

    function sub(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256 c) {
        require(b \u003c= a, errorMessage);
        c = a - b;
    }

    function setProxy() public override {
        require(_totalSupply != 0, \"Init not called!\");
        require(_proxy == address(0) || _proxy == msg.sender, _proxy != address(0) ? \"Proxy already set!\" : \"Only Proxy can toggle itself!\");
        _proxy = _proxy == address(0) ?  msg.sender : address(0);
    }

    function mint(uint256 amount) public override {
        require(IMVDFunctionalitiesManager(IMVDProxy(_proxy).getMVDFunctionalitiesManagerAddress()).isAuthorizedFunctionality(msg.sender), \"Unauthorized access!\");

        _totalSupply = add(_totalSupply, amount);
        _balances[_proxy] = add(_balances[_proxy], amount);
        emit Transfer(address(0), _proxy, amount);
    }

    function burn(uint256 amount) public override {
        _balances[msg.sender] = sub(_balances[msg.sender], amount, \"VotingToken: burn amount exceeds balance\");
        _totalSupply = sub(_totalSupply, amount, \"VotingToken: burn amount exceeds total supply\");
        emit Transfer(msg.sender, address(0), amount);
    }
}
