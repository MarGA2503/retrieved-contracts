// SPDX-License-Identifier: MIT
pragma solidity ^0.6.0;

import \u0027./SafeMath.sol\u0027;

contract RoyaleToken{

    using SafeMath for uint256;
    
    string public name = \"Royale\";
    string public symbol = \"ROYA\";
    uint8  public decimals=18;
    uint256 public totalSupply=72000000 * (uint256(10) ** decimals);
    mapping(address =\u003e uint256)  balances;
    mapping (address =\u003e mapping (address =\u003e uint256)) allowances;
    
    event Transfer(
      address indexed _from,
      address indexed _to,
      uint256 _value
    );

    event Approval(
        address indexed _owner,
        address indexed _spender,
        uint256 _value
    );
    
    
     constructor () public {
            balances[msg.sender] = totalSupply;
           
    }
    
    
   
    function balanceOf(address _address) public view returns (uint256){
            return balances[_address];
    }

   
     function transfer(address _to, uint256 _amount) public  returns (bool success) {
            require(_amount\u003e0 , \"amount can not be zero \");
            require(balances[msg.sender]\u003e=_amount  , \"Insufficient balance \");
            balances[msg.sender] = balances[msg.sender].sub(_amount);     
            balances[_to] =balances[_to].add(_amount);
            emit Transfer(msg.sender, _to, _amount);
            return true;
    }
    
   
    function approve(address _spender, uint256 _amount) public returns (bool success) {
            allowances[msg.sender][_spender]=allowances[msg.sender][_spender].add(_amount);                                
            emit Approval(msg.sender,_spender, _amount);
            return true;
    }
    
    function transferFrom(address _from, address _to, uint256 _amount) public  returns (bool success) {
         require(_amount\u003e0 , \"amount can not be zero \");
         require(_amount \u003c= allowances[_from][msg.sender] , \"Transfer amount exceeds allowance\");  //checking that whether sender is approved or not...
         balances[_from]= balances[_from].sub(_amount);
         balances[_to] = balances[_to].add(_amount);
         allowances[_from][msg.sender]=allowances[_from][msg.sender].sub(_amount);
          emit Transfer(_from, _to, _amount);
         return true;
    }

    function allowance(address _owner, address _spender) public view returns (uint256 remaining){
      return allowances[_owner][_spender];
    }
    
}


"},"SafeMath.sol":{"content":"// SPDX-License-Identifier: MIT
pragma solidity ^0.6.0;
library SafeMath {
    /**
     * @dev Returns the addition of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity\u0027s `+` operator.
     *
     * Requirements:
     *
     * - Addition cannot overflow.
     */
    function add(uint256 a, uint256 b) internal pure returns (uint256) {
        uint256 c = a + b;
        require(c \u003e= a, \"SafeMath: addition overflow\");

        return c;
    }

    /**
     * @dev Returns the subtraction of two unsigned integers, reverting on
     * overflow (when the result is negative).
     *
     * Counterpart to Solidity\u0027s `-` operator.
     *
     * Requirements:
     *
     * - Subtraction cannot overflow.
     */
    function sub(uint256 a, uint256 b) internal pure returns (uint256) {
        return sub(a, b, \"SafeMath: subtraction overflow\");
    }

    /**
     * @dev Returns the subtraction of two unsigned integers, reverting with custom message on
     * overflow (when the result is negative).
     *
     * Counterpart to Solidity\u0027s `-` operator.
     *
     * Requirements:
     *
     * - Subtraction cannot overflow.
     */
    function sub(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b \u003c= a, errorMessage);
        uint256 c = a - b;

        return c;
    }

    /**
     * @dev Returns the multiplication of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity\u0027s `*` operator.
     *
     * Requirements:
     *
     * - Multiplication cannot overflow.
     */
    function mul(uint256 a, uint256 b) internal pure returns (uint256) {
        // Gas optimization: this is cheaper than requiring \u0027a\u0027 not being zero, but the
        // benefit is lost if \u0027b\u0027 is also tested.
        // See: https://github.com/OpenZeppelin/openzeppelin-contracts/pull/522
        if (a == 0) {
            return 0;
        }

        uint256 c = a * b;
        require(c / a == b, \"SafeMath: multiplication overflow\");

        return c;
    }

    /**
     * @dev Returns the integer division of two unsigned integers. Reverts on
     * division by zero. The result is rounded towards zero.
     *
     * Counterpart to Solidity\u0027s `/` operator. Note: this function uses a
     * `revert` opcode (which leaves remaining gas untouched) while Solidity
     * uses an invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function div(uint256 a, uint256 b) internal pure returns (uint256) {
        return div(a, b, \"SafeMath: division by zero\");
    }

    /**
     * @dev Returns the integer division of two unsigned integers. Reverts with custom message on
     * division by zero. The result is rounded towards zero.
     *
     * Counterpart to Solidity\u0027s `/` operator. Note: this function uses a
     * `revert` opcode (which leaves remaining gas untouched) while Solidity
     * uses an invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function div(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b \u003e 0, errorMessage);
        uint256 c = a / b;
        // assert(a == b * c + a % b); // There is no case in which this doesn\u0027t hold

        return c;
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
     * Reverts when dividing by zero.
     *
     * Counterpart to Solidity\u0027s `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function mod(uint256 a, uint256 b) internal pure returns (uint256) {
        return mod(a, b, \"SafeMath: modulo by zero\");
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
     * Reverts with custom message when dividing by zero.
     *
     * Counterpart to Solidity\u0027s `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function mod(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b != 0, errorMessage);
        return a % b;
    }
}
