/*
* SPDX-License-Identifier: UNLICENSED
* Copyright © 2021 Blocksquare d.o.o.
*/

pragma solidity ^0.6.12;
pragma experimental ABIEncoderV2;

import \"./Ownable.sol\";

interface CertifiedPartnersHelpers {
    function hasSystemAdminRights(address addr) external view returns (bool);

    function isCPAdminOf(address account, bytes32 cpBytes) external view returns (bool);

    function getSpecialWallet() external view returns (address);
}

// @title Certified Partners
contract CertifiedPartners is Ownable {
    struct CertifiedPartner {
        bool canDistributeRent;
        bool isCP;
        mapping(bytes32 =\u003e bool) whitelistedUsers;
    }

    CertifiedPartnersHelpers private _roles;

    mapping(bytes32 =\u003e CertifiedPartner) private _certifiedPartner;
    mapping(address =\u003e bytes32) _associatedWallets;

    address private _dataProxy;


    event AddedCertifiedPartner(bytes32 indexed cpBytes, string cp);
    event AddedWallet(bytes32 indexed cp, address wallet, string cpName);
    event RemovedWallet(bytes32 indexed cp, address wallet);
    event AddedWhitelisted(bytes32 indexed cp, string[] users, string cpName);
    event RemovedWhitelisted(bytes32 indexed cp, string[] users, string cpName);

    modifier onlySystemAdmin {
        require(_roles.hasSystemAdminRights(_msgSender()), \"CertifiedPartners: You need to have system admin rights!\");
        _;
    }


    modifier onlySystemAdminOrSpecialWallet {
        require(_roles.hasSystemAdminRights(_msgSender()) || CertifiedPartnersHelpers(_dataProxy).getSpecialWallet() == _msgSender(), \"CertifiedPartners: You don\u0027t have rights!\");
        _;
    }

    modifier onlyCPAdmin(string memory cp) {
        bytes32 cpBytes = getUserBytes(cp);
        require(_roles.isCPAdminOf(_msgSender(), cpBytes) || CertifiedPartnersHelpers(_dataProxy).getSpecialWallet() == _msgSender(), \"CertifiedPartners: You need to be admin of this CP!\");
        _;
    }

    modifier onlyCPOrManager(string memory cp) {
        bytes32 cpBytes = getUserBytes(cp);
        require(_roles.isCPAdminOf(_msgSender(), cpBytes) || _associatedWallets[_msgSender()] == cpBytes || CertifiedPartnersHelpers(_dataProxy).getSpecialWallet() == _msgSender(), \"CertifiedPartners: You need to have CP admin rights or you have to be CP!\");
        _;
    }

    constructor(address roles) public {
        _roles = CertifiedPartnersHelpers(roles);
    }

    function changeRolesAddress(address newRoles) public onlyOwner {
        _roles = CertifiedPartnersHelpers(newRoles);
    }

    function changeDataProxy(address dataProxy) public onlyOwner {
        _dataProxy = dataProxy;
    }

    /// @notice added new Certified Partner
    /// @param cp Certified Partner identifier
    function addCertifiedPartner(string memory cp) public onlySystemAdmin {
        bytes32 cpBytes = getUserBytes(cp);
        _certifiedPartner[cpBytes] = CertifiedPartner({canDistributeRent : true, isCP : true});
        emit AddedCertifiedPartner(cpBytes, cp);
    }

    /// @notice add wallets to Certified Partner
    /// @param cp Certified Partner identifier
    /// @param wallets Array of wallet address
    function addWalletsToCP(string memory cp, address[] memory wallets) public onlyCPAdmin(cp) {
        bytes32 cpBytes = getUserBytes(cp);
        require(_certifiedPartner[cpBytes].isCP, \"CertifiedPartners: Not a certified partner\");
        for (uint256 i = 0; i \u003c wallets.length; i++) {
            _associatedWallets[wallets[i]] = cpBytes;
            emit AddedWallet(cpBytes, wallets[i], cp);
        }
    }

    /// @notice add wallet to Certified Partner
    /// @param cp Certified Partner identifier
    /// @param wallet Wallet address
    function addCPAndWallet(string memory cp, address wallet) public onlySystemAdmin {
        bytes32 cpBytes = getUserBytes(cp);
        addCertifiedPartner(cp);
        _associatedWallets[wallet] = cpBytes;
        emit AddedWallet(cpBytes, wallet, cp);
    }

    /// @notice remove wallets
    /// @param wallets Array of wallet addresses
    function removeWallets(address[] memory wallets) public onlySystemAdminOrSpecialWallet {
        bytes32 cpBytes = _associatedWallets[wallets[0]];
        for (uint256 i = 0; i \u003c wallets.length; i++) {
            delete _associatedWallets[wallets[i]];
            emit RemovedWallet(cpBytes, wallets[i]);
        }
    }

    /// @notice change whether Certified Partner can distribute revenue
    /// @param cp Certified Partner identifier
    function changeCanDistributeRent(string memory cp) public onlyCPAdmin(cp) {
        bytes32 cpBytes = getUserBytes(cp);
        _certifiedPartner[cpBytes].canDistributeRent = !_certifiedPartner[cpBytes].canDistributeRent;
    }

    /// @notice whitelist users to Certified Partner allowing them to trade Certified Partner\u0027s properties
    /// @param cp Certified Partner identifier
    /// @param users Array of user identifiers
    function addWhitelisted(string memory cp, string[] memory users) public onlyCPOrManager(cp) {
        bytes32 cpBytes = getUserBytes(cp);
        for (uint256 i = 0; i \u003c users.length; i++) {
            bytes32 userBytes = getUserBytes(users[i]);
            _certifiedPartner[cpBytes].whitelistedUsers[userBytes] = true;
        }
        emit AddedWhitelisted(cpBytes, users, cp);
    }

    /// @notice remove users from Certified Partner\u0027s whitelist
    /// @param cp Certified Partner identifier
    /// @param users Array of user identifiers
    function removeWhitelisted(string memory cp, string[] memory users) public onlyCPOrManager(cp) {
        bytes32 cpBytes = getUserBytes(cp);
        for (uint256 i = 0; i \u003c users.length; i++) {
            bytes32 userBytes = getUserBytes(users[i]);
            _certifiedPartner[cpBytes].whitelistedUsers[userBytes] = false;
        }
        emit RemovedWhitelisted(cpBytes, users, cp);
    }

    /// @notice check if Certified Partner identifier is registered
    /// @param name Identifier to check
    /// @return true if name is a registered Certified Partner otherwise false
    function isCertifiedPartnerName(string memory name) public view returns (bool) {
        bytes32 nameBytes = getUserBytes(name);
        return _certifiedPartner[nameBytes].isCP;
    }

    /// @notice check if addr belongs to a registered Certified Partner
    /// @param addr Wallet address to check
    /// @return true if addr belongs to a registered Certified Partner
    function isCertifiedPartner(address addr) public view returns (bool) {
        return _certifiedPartner[_associatedWallets[addr]].isCP;
    }

    /// @notice check if Certified Partner with wallet addr can distribute revenue
    /// @param addr Wallet address of Certified Partner
    /// @return true if Certified Partner with wallet addr can distribute revenue otherwise false
    function canCertifiedPartnerDistributeRent(address addr) public view returns (bool) {
        return _certifiedPartner[_associatedWallets[addr]].canDistributeRent;
    }

    /// @notice check if user is whitelisted for cp
    /// @param cp Certified Partner identifier
    /// @param user User identifier
    /// @return true if user is whitelisted for cp otherwise false
    function isUserWhitelistedByName(string memory cp, string memory user) public view returns (bool) {
        bytes32 cpBytes = getUserBytes(cp);
        return _certifiedPartner[cpBytes].whitelistedUsers[getUserBytes(user)];
    }

    /// @notice if admin is admin of cp based on wallet addresses
    /// @param admin Wallet address of admin
    /// @param cp Wallet address of Certified Partner
    /// @return true if admin is admin of cp otherwise false
    function isCPAdmin(address admin, address cp) external view returns (bool) {
        return _roles.isCPAdminOf(admin, _associatedWallets[cp]);
    }

    /// @notice check if user whitelisted for cp based on bytes
    /// @param cp Keccak256 hash of Certified Partner identifier
    /// @param user Keccak256 hash of user identifier
    /// @return true if user is whitelisted for cp otherwise false
    function isUserWhitelisted(bytes32 cp, bytes32 user) external view returns (bool) {
        return _certifiedPartner[cp].whitelistedUsers[user];
    }

    /// @notice get keccak256 hash of string
    /// @param user User or Certified Partner identifier
    /// @return keccak256 hash
    function getUserBytes(string memory user) public pure returns (bytes32) {
        return keccak256(abi.encode(user));
    }

    /// @notice retrieves keccak256 hash of Certified Partner based on wallet
    /// @param wallet Wallet address of Certified Partner
    /// @return keccak256 hash
    function getCPBytesFromWallet(address wallet) public view returns (bytes32) {
        return _associatedWallets[wallet];
    }
}
"},"Context.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.6.0;

/*
 * @dev Provides information about the current execution context, including the
 * sender of the transaction and its data. While these are generally available
 * via msg.sender and msg.data, they should not be accessed in such a direct
 * manner, since when dealing with GSN meta-transactions the account sending and
 * paying for execution may not be the actual sender (as far as an application
 * is concerned).
 *
 * This contract is only required for intermediate, library-like contracts.
 */
abstract contract Context {
    function _msgSender() internal view virtual returns (address payable) {
        return msg.sender;
    }

    function _msgData() internal view virtual returns (bytes memory) {
        this; // silence state mutability warning without generating bytecode - see https://github.com/ethereum/solidity/issues/2691
        return msg.data;
    }
}
"},"Ownable.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.6.0;

import \"./Context.sol\";
/**
 * @dev Contract module which provides a basic access control mechanism, where
 * there is an account (an owner) that can be granted exclusive access to
 * specific functions.
 *
 * By default, the owner account will be the one that deploys the contract. This
 * can later be changed with {transferOwnership}.
 *
 * This module is used through inheritance. It will make available the modifier
 * `onlyOwner`, which can be applied to your functions to restrict their use to
 * the owner.
 */
contract Ownable is Context {
    address private _owner;

    event OwnershipTransferred(address indexed previousOwner, address indexed newOwner);

    /**
     * @dev Initializes the contract setting the deployer as the initial owner.
     */
    constructor () internal {
        address msgSender = _msgSender();
        _owner = msgSender;
        emit OwnershipTransferred(address(0), msgSender);
    }

    /**
     * @dev Returns the address of the current owner.
     */
    function owner() public view returns (address) {
        return _owner;
    }

    /**
     * @dev Throws if called by any account other than the owner.
     */
    modifier onlyOwner() {
        require(_owner == _msgSender(), \"Ownable: caller is not the owner\");
        _;
    }

    /**
     * @dev Leaves the contract without owner. It will not be possible to call
     * `onlyOwner` functions anymore. Can only be called by the current owner.
     *
     * NOTE: Renouncing ownership will leave the contract without an owner,
     * thereby removing any functionality that is only available to the owner.
     */
    function renounceOwnership() public virtual onlyOwner {
        emit OwnershipTransferred(_owner, address(0));
        _owner = address(0);
    }

    /**
     * @dev Transfers ownership of the contract to a new account (`newOwner`).
     * Can only be called by the current owner.
     */
    function transferOwnership(address newOwner) public virtual onlyOwner {
        require(newOwner != address(0), \"Ownable: new owner is the zero address\");
        emit OwnershipTransferred(_owner, newOwner);
        _owner = newOwner;
    }
}

