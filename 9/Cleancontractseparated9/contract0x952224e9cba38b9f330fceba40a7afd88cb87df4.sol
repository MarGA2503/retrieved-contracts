// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import \"Context.sol\";
import \"ERC165.sol\";

/**
 * @dev External interface of AccessControl declared to support ERC165 detection.
 */
interface IAccessControl {
    function hasRole(bytes32 role, address account) external view returns (bool);
    function getRoleAdmin(bytes32 role) external view returns (bytes32);
    function grantRole(bytes32 role, address account) external;
    function revokeRole(bytes32 role, address account) external;
    function renounceRole(bytes32 role, address account) external;
}

/**
 * @dev Contract module that allows children to implement role-based access
 * control mechanisms. This is a lightweight version that doesn\u0027t allow enumerating role
 * members except through off-chain means by accessing the contract event logs. Some
 * applications may benefit from on-chain enumerability, for those cases see
 * {AccessControlEnumerable}.
 *
 * Roles are referred to by their `bytes32` identifier. These should be exposed
 * in the external API and be unique. The best way to achieve this is by
 * using `public constant` hash digests:
 *
 * ```
 * bytes32 public constant MY_ROLE = keccak256(\"MY_ROLE\");
 * ```
 *
 * Roles can be used to represent a set of permissions. To restrict access to a
 * function call, use {hasRole}:
 *
 * ```
 * function foo() public {
 *     require(hasRole(MY_ROLE, msg.sender));
 *     ...
 * }
 * ```
 *
 * Roles can be granted and revoked dynamically via the {grantRole} and
 * {revokeRole} functions. Each role has an associated admin role, and only
 * accounts that have a role\u0027s admin role can call {grantRole} and {revokeRole}.
 *
 * By default, the admin role for all roles is `DEFAULT_ADMIN_ROLE`, which means
 * that only accounts with this role will be able to grant or revoke other
 * roles. More complex role relationships can be created by using
 * {_setRoleAdmin}.
 *
 * WARNING: The `DEFAULT_ADMIN_ROLE` is also its own admin: it has permission to
 * grant and revoke this role. Extra precautions should be taken to secure
 * accounts that have been granted it.
 */
abstract contract AccessControl is Context, IAccessControl, ERC165 {
    struct RoleData {
        mapping (address =\u003e bool) members;
        bytes32 adminRole;
    }

    mapping (bytes32 =\u003e RoleData) private _roles;

    bytes32 public constant DEFAULT_ADMIN_ROLE = 0x00;

    /**
     * @dev Emitted when `newAdminRole` is set as ``role``\u0027s admin role, replacing `previousAdminRole`
     *
     * `DEFAULT_ADMIN_ROLE` is the starting admin for all roles, despite
     * {RoleAdminChanged} not being emitted signaling this.
     *
     * _Available since v3.1._
     */
    event RoleAdminChanged(bytes32 indexed role, bytes32 indexed previousAdminRole, bytes32 indexed newAdminRole);

    /**
     * @dev Emitted when `account` is granted `role`.
     *
     * `sender` is the account that originated the contract call, an admin role
     * bearer except when using {_setupRole}.
     */
    event RoleGranted(bytes32 indexed role, address indexed account, address indexed sender);

    /**
     * @dev Emitted when `account` is revoked `role`.
     *
     * `sender` is the account that originated the contract call:
     *   - if using `revokeRole`, it is the admin role bearer
     *   - if using `renounceRole`, it is the role bearer (i.e. `account`)
     */
    event RoleRevoked(bytes32 indexed role, address indexed account, address indexed sender);

    /**
     * @dev See {IERC165-supportsInterface}.
     */
    function supportsInterface(bytes4 interfaceId) public view virtual override returns (bool) {
        return interfaceId == type(IAccessControl).interfaceId
            || super.supportsInterface(interfaceId);
    }

    /**
     * @dev Returns `true` if `account` has been granted `role`.
     */
    function hasRole(bytes32 role, address account) public view override returns (bool) {
        return _roles[role].members[account];
    }

    /**
     * @dev Returns the admin role that controls `role`. See {grantRole} and
     * {revokeRole}.
     *
     * To change a role\u0027s admin, use {_setRoleAdmin}.
     */
    function getRoleAdmin(bytes32 role) public view override returns (bytes32) {
        return _roles[role].adminRole;
    }

    /**
     * @dev Grants `role` to `account`.
     *
     * If `account` had not been already granted `role`, emits a {RoleGranted}
     * event.
     *
     * Requirements:
     *
     * - the caller must have ``role``\u0027s admin role.
     */
    function grantRole(bytes32 role, address account) public virtual override {
        require(hasRole(getRoleAdmin(role), _msgSender()), \"AccessControl: sender must be an admin to grant\");

        _grantRole(role, account);
    }

    /**
     * @dev Revokes `role` from `account`.
     *
     * If `account` had been granted `role`, emits a {RoleRevoked} event.
     *
     * Requirements:
     *
     * - the caller must have ``role``\u0027s admin role.
     */
    function revokeRole(bytes32 role, address account) public virtual override {
        require(hasRole(getRoleAdmin(role), _msgSender()), \"AccessControl: sender must be an admin to revoke\");

        _revokeRole(role, account);
    }

    /**
     * @dev Revokes `role` from the calling account.
     *
     * Roles are often managed via {grantRole} and {revokeRole}: this function\u0027s
     * purpose is to provide a mechanism for accounts to lose their privileges
     * if they are compromised (such as when a trusted device is misplaced).
     *
     * If the calling account had been granted `role`, emits a {RoleRevoked}
     * event.
     *
     * Requirements:
     *
     * - the caller must be `account`.
     */
    function renounceRole(bytes32 role, address account) public virtual override {
        require(account == _msgSender(), \"AccessControl: can only renounce roles for self\");

        _revokeRole(role, account);
    }

    /**
     * @dev Grants `role` to `account`.
     *
     * If `account` had not been already granted `role`, emits a {RoleGranted}
     * event. Note that unlike {grantRole}, this function doesn\u0027t perform any
     * checks on the calling account.
     *
     * [WARNING]
     * ====
     * This function should only be called from the constructor when setting
     * up the initial roles for the system.
     *
     * Using this function in any other way is effectively circumventing the admin
     * system imposed by {AccessControl}.
     * ====
     */
    function _setupRole(bytes32 role, address account) internal virtual {
        _grantRole(role, account);
    }

    /**
     * @dev Sets `adminRole` as ``role``\u0027s admin role.
     *
     * Emits a {RoleAdminChanged} event.
     */
    function _setRoleAdmin(bytes32 role, bytes32 adminRole) internal virtual {
        emit RoleAdminChanged(role, getRoleAdmin(role), adminRole);
        _roles[role].adminRole = adminRole;
    }

    function _grantRole(bytes32 role, address account) private {
        if (!hasRole(role, account)) {
            _roles[role].members[account] = true;
            emit RoleGranted(role, account, _msgSender());
        }
    }

    function _revokeRole(bytes32 role, address account) private {
        if (hasRole(role, account)) {
            _roles[role].members[account] = false;
            emit RoleRevoked(role, account, _msgSender());
        }
    }
}"},"Context.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

/*
 * @dev Provides information about the current execution context, including the
 * sender of the transaction and its data. While these are generally available
 * via msg.sender and msg.data, they should not be accessed in such a direct
 * manner, since when dealing with meta-transactions the account sending and
 * paying for execution may not be the actual sender (as far as an application
 * is concerned).
 *
 * This contract is only required for intermediate, library-like contracts.
 */
abstract contract Context {
    function _msgSender() internal view virtual returns (address) {
        return msg.sender;
    }

    function _msgData() internal view virtual returns (bytes calldata) {
        this; // silence state mutability warning without generating bytecode - see https://github.com/ethereum/solidity/issues/2691
        return msg.data;
    }
}"},"CryptoCatsMarketV2.sol":{"content":"pragma solidity ^0.8.0;

import \"AccessControl.sol\";
import \"SafeMath.sol\";
import \"Context.sol\";
import \"ERC165.sol\";
import \"IERC165.sol\";

interface CryptoCats{
\tfunction catIndexToAddress(uint catIndex) external view returns(address);
\tfunction buyCat(uint catIndex) external payable;
\tfunction transfer(address addressto, uint catIndex) external;
}

contract CryptoCatsMarketV2 is AccessControl {

\t/*
\tMarketplace based on the Larvalabs OGs!
\t*/

\tusing SafeMath for uint256;

\tstring public name = \"CryptoCatsMarketV2\";

\tCryptoCats public constant cryptocats = CryptoCats(0x19c320b43744254ebdBcb1F1BD0e2a3dc08E01dc);

\tuint public FEE = 20; //5%
\tuint public feesToCollect = 0;

    struct Bid {
        uint catIndex;
\t\tuint amount;
        address bidder;
    }

    // A record of the highest Cryptocat bid
    mapping (uint =\u003e Bid) public bids;
\tmapping (address =\u003e uint) public pendingWithdrawals;

    event CryptoCatsTransfer(uint indexed index, address from, address to);
    event CryptoCatsBidCreated(uint indexed index, uint amount, address bidder);
    event CryptoCatsBidWithdrawn(uint indexed index, uint amount, address bidder);
    event CryptoCatsBought(uint indexed index, uint amount, address seller, address bidder);

    constructor() public {
\t\t_setupRole(DEFAULT_ADMIN_ROLE, 0xaaEa1B588c41dddEa4afDa5105e1C4f0bdB017F5);
    }

\tfunction collectFees() public {
\t\trequire(hasRole(DEFAULT_ADMIN_ROLE, msg.sender));
\t\tuint amount = feesToCollect;
\t\tfeesToCollect = 0;
\t\tpayable(0xaaEa1B588c41dddEa4afDa5105e1C4f0bdB017F5).transfer(amount);
\t}

\tfunction changeFee(uint newFee) public {
\t\trequire(hasRole(DEFAULT_ADMIN_ROLE, msg.sender));
\t\tFEE = newFee;
\t}
\t
\t
\t
\tfunction bid(uint catIndex) public payable {
\t\t//require(etheria.getOwner(col, row) != msg.sender);
\t\trequire(msg.value \u003e 0, \"BID::Value is 0\");
\t\tBid memory bid = bids[catIndex];
\t\trequire(msg.value \u003e bid.amount, \"BID::New bid too low\");
\t\t//refund failing bid
\t\tpendingWithdrawals[bid.bidder] += bid.amount;
\t\t//new bid
\t\tbids[catIndex] = Bid(catIndex, msg.value, msg.sender);
\t\temit CryptoCatsBidCreated(catIndex, msg.value, msg.sender);
\t}

\tfunction withdrawBid(uint catIndex) public {
\t\tBid memory bid = bids[catIndex];
\t\trequire(msg.sender == bid.bidder, \"WITHDRAW_BID::Only bidder can withdraw his bid\");
\t\temit CryptoCatsBidWithdrawn(catIndex, bid.amount, msg.sender);
\t\tuint amount = bid.amount;
\t\tbids[catIndex] = Bid(catIndex, 0, address(0x0));
\t\tpayable(msg.sender).transfer(amount);
\t}


\tfunction acceptBid(uint catIndex, uint minPrice) public {
\t\trequire(cryptocats.catIndexToAddress(catIndex) == msg.sender, \"ACCEPT_BID::Only owner can accept bid\");
        Bid memory bid = bids[catIndex];
\t\trequire(bid.amount \u003e 0, \"ACCEPT_BID::Bid amount is 0\");
\t\trequire(bid.amount \u003e= minPrice, \"ACCEPT_BID::Min price not respected\");
\t\t// With the require getOwner we check already, if it can be assigned, no other checks needed
\t\tcryptocats.buyCat(catIndex);
\t\tcryptocats.transfer(bid.bidder, catIndex);

\t\t//collect fee
\t\tuint fees = bid.amount.div(FEE);
\t\tfeesToCollect += fees;

        uint amount = bid.amount.sub(fees);
\t\tbids[catIndex] = Bid(catIndex, 0, address(0x0));
        pendingWithdrawals[msg.sender] += amount;
        emit CryptoCatsBought(catIndex, amount, msg.sender, bid.bidder);
\t\temit CryptoCatsTransfer(catIndex, msg.sender, bid.bidder);
    }



\tfunction withdraw() public {
\t\tuint amount = pendingWithdrawals[msg.sender];
        // Remember to zero the pending refund before
        // sending to prevent re-entrancy attacks
        pendingWithdrawals[msg.sender] = 0;
        payable(msg.sender).transfer(amount);
\t}
}"},"ERC165.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import \"IERC165.sol\";

/**
 * @dev Implementation of the {IERC165} interface.
 *
 * Contracts that want to implement ERC165 should inherit from this contract and override {supportsInterface} to check
 * for the additional interface id that will be supported. For example:
 *
 * ```solidity
 * function supportsInterface(bytes4 interfaceId) public view virtual override returns (bool) {
 *     return interfaceId == type(MyInterface).interfaceId || super.supportsInterface(interfaceId);
 * }
 * ```
 *
 * Alternatively, {ERC165Storage} provides an easier to use but more expensive implementation.
 */
abstract contract ERC165 is IERC165 {
    /**
     * @dev See {IERC165-supportsInterface}.
     */
    function supportsInterface(bytes4 interfaceId) public view virtual override returns (bool) {
        return interfaceId == type(IERC165).interfaceId;
    }
}"},"IERC165.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

/**
 * @dev Interface of the ERC165 standard, as defined in the
 * https://eips.ethereum.org/EIPS/eip-165[EIP].
 *
 * Implementers can declare support of contract interfaces, which can then be
 * queried by others ({ERC165Checker}).
 *
 * For an implementation, see {ERC165}.
 */
interface IERC165 {
    /**
     * @dev Returns true if this contract implements the interface defined by
     * `interfaceId`. See the corresponding
     * https://eips.ethereum.org/EIPS/eip-165#how-interfaces-are-identified[EIP section]
     * to learn more about how these ids are created.
     *
     * This function call must use less than 30 000 gas.
     */
    function supportsInterface(bytes4 interfaceId) external view returns (bool);
}"},"SafeMath.sol":{"content":"pragma solidity ^0.8.0;


/**
 * @title SafeMath
 * @dev Math operations with safety checks that throw on error
 */
library SafeMath {

  /**
  * @dev Multiplies two numbers, throws on overflow.
  */
  function mul(uint256 a, uint256 b) internal pure returns (uint256 c) {
    if (a == 0) {
      return 0;
    }
    c = a * b;
    assert(c / a == b);
    return c;
  }

  /**
  * @dev Integer division of two numbers, truncating the quotient.
  */
  function div(uint256 a, uint256 b) internal pure returns (uint256) {
    // assert(b \u003e 0); // Solidity automatically throws when dividing by 0
    // uint256 c = a / b;
    // assert(a == b * c + a % b); // There is no case in which this doesn\u0027t hold
    return a / b;
  }

  /**
  * @dev Subtracts two numbers, throws on overflow (i.e. if subtrahend is greater than minuend).
  */
  function sub(uint256 a, uint256 b) internal pure returns (uint256) {
    assert(b \u003c= a);
    return a - b;
  }

  /**
  * @dev Adds two numbers, throws on overflow.
  */
  function add(uint256 a, uint256 b) internal pure returns (uint256 c) {
    c = a + b;
    assert(c \u003e= a);
    return c;
  }
}


