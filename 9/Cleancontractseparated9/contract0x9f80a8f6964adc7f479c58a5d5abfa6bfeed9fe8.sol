pragma solidity ^0.6.0;

/*
 * @dev Provides information about the current execution context, including the
 * sender of the transaction and its data. While these are generally available
 * via msg.sender and msg.data, they should not be accessed in such a direct
 * manner, since when dealing with GSN meta-transactions the account sending and
 * paying for execution may not be the actual sender (as far as an application
 * is concerned).
 *
 * This contract is only required for intermediate, library-like contracts.
 */
abstract contract Context {
    function _msgSender() internal view virtual returns (address payable) {
        return msg.sender;
    }

    function _msgData() internal view virtual returns (bytes memory) {
        this; // silence state mutability warning without generating bytecode - see https://github.com/ethereum/solidity/issues/2691
        return msg.data;
    }
}
"},"IERC20.sol":{"content":"pragma solidity ^0.6.0;

/**
 * @dev Interface of the ERC20 standard as defined in the EIP.
 */
interface IERC20 {
    /**
     * @dev Returns the amount of tokens in existence.
     */
    function totalSupply() external view returns (uint256);

    /**
     * @dev Returns the amount of tokens owned by `account`.
     */
    function balanceOf(address account) external view returns (uint256);

    /**
     * @dev Moves `amount` tokens from the caller\u0027s account to `recipient`.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transfer(address recipient, uint256 amount) external returns (bool);

    /**
     * @dev Returns the remaining number of tokens that `spender` will be
     * allowed to spend on behalf of `owner` through {transferFrom}. This is
     * zero by default.
     *
     * This value changes when {approve} or {transferFrom} are called.
     */
    function allowance(address owner, address spender) external view returns (uint256);

    /**
     * @dev Sets `amount` as the allowance of `spender` over the caller\u0027s tokens.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * IMPORTANT: Beware that changing an allowance with this method brings the risk
     * that someone may use both the old and the new allowance by unfortunate
     * transaction ordering. One possible solution to mitigate this race
     * condition is to first reduce the spender\u0027s allowance to 0 and set the
     * desired value afterwards:
     * https://github.com/ethereum/EIPs/issues/20#issuecomment-263524729
     *
     * Emits an {Approval} event.
     */
    function approve(address spender, uint256 amount) external returns (bool);

    /**
     * @dev Moves `amount` tokens from `sender` to `recipient` using the
     * allowance mechanism. `amount` is then deducted from the caller\u0027s
     * allowance.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transferFrom(address sender, address recipient, uint256 amount) external returns (bool);

    /**
     * @dev Emitted when `value` tokens are moved from one account (`from`) to
     * another (`to`).
     *
     * Note that `value` may be zero.
     */
    event Transfer(address indexed from, address indexed to, uint256 value);

    /**
     * @dev Emitted when the allowance of a `spender` for an `owner` is set by
     * a call to {approve}. `value` is the new allowance.
     */
    event Approval(address indexed owner, address indexed spender, uint256 value);
}
"},"LP_STBU_farm.sol":{"content":"pragma solidity ^0.6.0;

import \"./IERC20.sol\";
import \"./SafeMath.sol\";
import \"./Ownable.sol\";

contract Farm is Ownable {

    using SafeMath for uint;

    IERC20 public STBU;
    IERC20 public LPtoken;

    struct stakeHolder {
        uint256 calcBlock;
        uint256 totalClaimed;
    }

    bool public active;
    uint256 public roi;
    uint256 public endBlock;
    uint256 public startBlock;
    uint256 public totalBlocks;
    uint256 private percentage;
    uint256 public rewardAllocation;
    uint256 public defaultStakePerBlock;

    mapping (address =\u003e uint256) public balance;
    mapping(address =\u003e stakeHolder) public holders;

    event LogClaim(address, uint256);

    constructor(address _LP, address _STBU) public {
        LPtoken = IERC20(_LP);
        STBU = IERC20(_STBU);
        roi = 10**18;
        percentage = 10**18;
        uint256 stake = 10**18;
        defaultStakePerBlock = stake.mul(220).div(100);
        totalBlocks = 2296215;
    }

    /**
     * @dev Throws if contract is not active.
     */
    modifier isActive() {
        require(active, \"Farm: is not active\");
        _;
    }

    /**
     * @dev Activating the staking with start block.
     * Can only be called by the current owner.
     */
    function activate() public onlyOwner returns (bool){
        startBlock = block.number;
        endBlock = startBlock.add(totalBlocks);
        rewardAllocation = defaultStakePerBlock.mul(totalBlocks);
        active = true;
        return true;
    }

    /**
     * @dev Deactivating the staking.
     * Can only be called by the current owner.
     */
    function deactivate() public onlyOwner returns (bool) {
        active = false;
        endBlock = block.number;
        require(STBU.transfer(owner(), STBU.balanceOf(address(this))), \"Deactivation: transfer failure\");
        return true;
    }

    /**
     * @dev Pause/unpause the staking.
     * Can only be called by the current owner.
     */
    function pause(bool _value) public onlyOwner returns (bool) {
        active = _value;
        return true;
    }

    /**
     * @dev get current stake per block.
     */
    function getStakePerBlock() public view returns (uint256) {
        return rewardAllocation.div(totalBlocks);
    }

    /**
     * @dev Returns the share of the holder.
     */
    function stakeholderShare(address user) public view returns (uint256) {
        uint256 hBalance = balance[user];
        uint256 supply = LPtoken.balanceOf(address(this));
        if (supply \u003e 0) {
            return hBalance.mul(percentage).div(supply);
        }
        return 0;
    }

    /**
     * @dev Deposits the LP tokens.
     * Can only be called when contract is active.
     */
    function depositLPTokens (uint256 _amount) public isActive {
        address from = msg.sender;
        address to = address(this);
        require(block.number \u003e= holders[from].calcBlock, \"Deposit: unable to calculate block\");
        require(endBlock \u003e block.number, \"Farm: is not active\");
        if (balance[from] \u003e 0) {
            _claim(from);
        }
        require(LPtoken.transferFrom(from, to, _amount), \"LP: unable to transfer coins\");
        balance[from] = balance[from].add(_amount);
        _calculateHolder(from);
    }

    /**
     * @dev Claim STBU reward.
     * Can only be called when contract is active.
     */
    function claim () public isActive {
        address _to = msg.sender;
        _claim(_to);
        _calculateHolder(_to);
    }

    /**
     * @dev Claim STBU reward and Unstake LP tokens.
     * Can only be called when contract is active.
     */
    function claimAndUnstake () public isActive {
        address _to = msg.sender;
        uint _balance = balance[_to];
        _claim(_to);
        balance[_to] = 0;
        require(LPtoken.transfer(_to, _balance), \"LP: unable to transfer coins\");
        _calculateHolder(_to);

    }

    /**
     * @dev Unstake LP tokens.
     * Can only be called when contract is active.
     */
    function unstake(uint256 _amount) public isActive {
        address _to = msg.sender;
        uint _balance = balance[_to];
        require(_balance \u003e= _amount, \"LP: wrong amount\");
        _claim(_to);
        balance[_to] = _balance.sub(_amount);
        require(LPtoken.transfer(_to, _amount), \"LP: unable to transfer coins\");
        _calculateHolder(_to);
    }

    /**
     * @dev Calcultae share and roi for the holder
     */
    function _calculateHolder(address holder) internal {
        stakeHolder memory sH = holders[holder];
        sH.calcBlock = block.number;
        holders[holder] = sH;
    }

    /**
     * @dev Send available reward to the holder
     */
    function _claim(address _to) internal {
        uint _staked = _calculateStaked(_to);
        holders[_to].totalClaimed = holders[_to].totalClaimed.add(_staked);
        rewardAllocation = rewardAllocation.sub(_staked);
        require(STBU.transfer(_to, _staked));
        emit LogClaim(_to, _staked);
    }

    /**
     * @dev Calculate available reward for the holder
     */
    function _calculateStaked(address holder) internal view returns(uint256){
        stakeHolder memory st = holders[holder];
        uint256 share = stakeholderShare(holder);
        uint256 currentBlock = block.number;
        uint256 amountBlocks = 0;
        if(currentBlock \u003e= endBlock) {
            amountBlocks = endBlock.sub(st.calcBlock);
        } else {
            amountBlocks = currentBlock.sub(st.calcBlock);
        }

        uint256 fullAmount = rewardAllocation.div(totalBlocks).mul(amountBlocks);
        uint256 _stakeAmount = fullAmount.mul(share).div(percentage);
        return _stakeAmount;
    }

    /**
     * @dev get base staking data
     */
    function getStakerData(address _player) public view returns(address, uint256, uint256, uint256) {
        uint256 staked = _calculateStaked(_player);
        return (_player, staked, roi, stakeholderShare(_player));
    }

    function emergencyWithdraw() public {
        address user = msg.sender;
        uint _balance = balance[user];
        balance[user] = 0;
        require(LPtoken.transfer(user, _balance), \"LP: unable to transfer coins\");
        _calculateHolder(user);
    }

}
"},"Ownable.sol":{"content":"// SPDX-License-Identifier: MIT

pragma solidity \u003e=0.6.0 \u003c0.8.0;

import \"./Context.sol\";
/**
 * @dev Contract module which provides a basic access control mechanism, where
 * there is an account (an owner) that can be granted exclusive access to
 * specific functions.
 *
 * By default, the owner account will be the one that deploys the contract. This
 * can later be changed with {transferOwnership}.
 *
 * This module is used through inheritance. It will make available the modifier
 * `onlyOwner`, which can be applied to your functions to restrict their use to
 * the owner.
 */
abstract contract Ownable is Context {
    address private _owner;

    event OwnershipTransferred(address indexed previousOwner, address indexed newOwner);

    /**
     * @dev Initializes the contract setting the deployer as the initial owner.
     */
    constructor () internal {
        address msgSender = _msgSender();
        _owner = msgSender;
        emit OwnershipTransferred(address(0), msgSender);
    }

    /**
     * @dev Returns the address of the current owner.
     */
    function owner() public view virtual returns (address) {
        return _owner;
    }

    /**
     * @dev Throws if called by any account other than the owner.
     */
    modifier onlyOwner() {
        require(owner() == _msgSender(), \"Ownable: caller is not the owner\");
        _;
    }

    /**
     * @dev Leaves the contract without owner. It will not be possible to call
     * `onlyOwner` functions anymore. Can only be called by the current owner.
     *
     * NOTE: Renouncing ownership will leave the contract without an owner,
     * thereby removing any functionality that is only available to the owner.
     */
    function renounceOwnership() public virtual onlyOwner {
        emit OwnershipTransferred(_owner, address(0));
        _owner = address(0);
    }

    /**
     * @dev Transfers ownership of the contract to a new account (`newOwner`).
     * Can only be called by the current owner.
     */
    function transferOwnership(address newOwner) public virtual onlyOwner {
        require(newOwner != address(0), \"Ownable: new owner is the zero address\");
        emit OwnershipTransferred(_owner, newOwner);
        _owner = newOwner;
    }
}
"},"SafeMath.sol":{"content":"pragma solidity ^0.6.0;

/**
 * @dev Wrappers over Solidity\u0027s arithmetic operations with added overflow
 * checks.
 *
 * Arithmetic operations in Solidity wrap on overflow. This can easily result
 * in bugs, because programmers usually assume that an overflow raises an
 * error, which is the standard behavior in high level programming languages.
 * `SafeMath` restores this intuition by reverting the transaction when an
 * operation overflows.
 *
 * Using this library instead of the unchecked operations eliminates an entire
 * class of bugs, so it\u0027s recommended to use it always.
 */
library SafeMath {
    /**
     * @dev Returns the addition of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity\u0027s `+` operator.
     *
     * Requirements:
     *
     * - Addition cannot overflow.
     */
    function add(uint256 a, uint256 b) internal pure returns (uint256) {
        uint256 c = a + b;
        require(c \u003e= a, \"SafeMath: addition overflow\");

        return c;
    }

    /**
     * @dev Returns the subtraction of two unsigned integers, reverting on
     * overflow (when the result is negative).
     *
     * Counterpart to Solidity\u0027s `-` operator.
     *
     * Requirements:
     *
     * - Subtraction cannot overflow.
     */
    function sub(uint256 a, uint256 b) internal pure returns (uint256) {
        return sub(a, b, \"SafeMath: subtraction overflow\");
    }

    /**
     * @dev Returns the subtraction of two unsigned integers, reverting with custom message on
     * overflow (when the result is negative).
     *
     * Counterpart to Solidity\u0027s `-` operator.
     *
     * Requirements:
     *
     * - Subtraction cannot overflow.
     */
    function sub(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b \u003c= a, errorMessage);
        uint256 c = a - b;

        return c;
    }

    /**
     * @dev Returns the multiplication of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity\u0027s `*` operator.
     *
     * Requirements:
     *
     * - Multiplication cannot overflow.
     */
    function mul(uint256 a, uint256 b) internal pure returns (uint256) {
        // Gas optimization: this is cheaper than requiring \u0027a\u0027 not being zero, but the
        // benefit is lost if \u0027b\u0027 is also tested.
        // See: https://github.com/OpenZeppelin/openzeppelin-contracts/pull/522
        if (a == 0) {
            return 0;
        }

        uint256 c = a * b;
        require(c / a == b, \"SafeMath: multiplication overflow\");

        return c;
    }

    /**
     * @dev Returns the integer division of two unsigned integers. Reverts on
     * division by zero. The result is rounded towards zero.
     *
     * Counterpart to Solidity\u0027s `/` operator. Note: this function uses a
     * `revert` opcode (which leaves remaining gas untouched) while Solidity
     * uses an invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function div(uint256 a, uint256 b) internal pure returns (uint256) {
        return div(a, b, \"SafeMath: division by zero\");
    }

    /**
     * @dev Returns the integer division of two unsigned integers. Reverts with custom message on
     * division by zero. The result is rounded towards zero.
     *
     * Counterpart to Solidity\u0027s `/` operator. Note: this function uses a
     * `revert` opcode (which leaves remaining gas untouched) while Solidity
     * uses an invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function div(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b \u003e 0, errorMessage);
        uint256 c = a / b;
        // assert(a == b * c + a % b); // There is no case in which this doesn\u0027t hold

        return c;
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
     * Reverts when dividing by zero.
     *
     * Counterpart to Solidity\u0027s `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function mod(uint256 a, uint256 b) internal pure returns (uint256) {
        return mod(a, b, \"SafeMath: modulo by zero\");
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
     * Reverts with custom message when dividing by zero.
     *
     * Counterpart to Solidity\u0027s `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function mod(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b != 0, errorMessage);
        return a % b;
    }
}

