// SPDX-License-Identifier: MIT
pragma solidity ^0.8.11;

interface ERC20 {
\tfunction allowance(address, address) external view returns (uint256);
\tfunction balanceOf(address) external view returns (uint256);
\tfunction transfer(address, uint256) external returns (bool);
\tfunction transferFrom(address, address, uint256) external returns (bool);
}"},"Metadata.sol":{"content":"// SPDX-License-Identifier: MIT
pragma solidity ^0.8.11;

import \"./WTFNFT.sol\";

interface PriceOracle {
\tfunction getPrice() external view returns (uint256);
}


contract Metadata {
\t
\tstring public name = \"fees.wtf NFT\";
\tstring public symbol = \"fees.wtf\";

\tstring constant private TABLE = \u0027ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/\u0027;

\tWTFNFT public nft;
\tPriceOracle public oracle;

\tconstructor(WTFNFT _nft) {
\t\tnft = _nft;
\t\toracle = PriceOracle(0xe89b5B2770Aa1a6BcfAc6F3517510aB8e9146651);
\t}

\tfunction setPriceOracle(PriceOracle _oracle) external {
\t\trequire(msg.sender == nft.owner());
\t\toracle = _oracle;
\t}


\tfunction tokenURI(uint256 _tokenId) external view returns (string memory) {
\t\t( , , address _user, uint256[7] memory _info) = nft.getToken(_tokenId);
\t\treturn rawTokenURI(_user, _info[0], _info[1], _info[2], _info[3], _info[4], _info[5], _info[6], oracle.getPrice());
\t}

\tfunction rawTokenURI(address _user, uint256 _totalFees, uint256 _failFees, uint256 _totalGas, uint256 _avgGwei, uint256 _totalDonated, uint256 _totalTxs, uint256 _failTxs, uint256 _price) public pure returns (string memory) {
\t\tstring memory _json = string(abi.encodePacked(\u0027{\"name\":\"\u0027, _trimAddress(_user, 6), \u0027\",\"description\":\"[fees.wtf](https://fees.wtf) snapshot at block 13916450 for [\u0027, _address2str(_user), \u0027](https://etherscan.io/address/\u0027, _address2str(_user), \u0027)\",\u0027));
\t\t_json = string(abi.encodePacked(_json, \u0027\"image\":\"data:image/svg+xml;base64,\u0027, _encode(bytes(getRawSVG(_totalFees, _failFees, _totalGas, _avgGwei, _totalDonated, _totalTxs, _failTxs, _price))), \u0027\",\"attributes\":[\u0027));
\t\tif (_totalFees \u003e 0) {
\t\t\t_json = string(abi.encodePacked(_json, \u0027{\"trait_type\":\"Total Fees\",\"value\":\u0027, _uint2str(_totalFees, 18, 5, false, true), \u0027}\u0027));
\t\t\t_json = string(abi.encodePacked(_json, \u0027,{\"trait_type\":\"Fail Fees\",\"value\":\u0027, _uint2str(_failFees, 18, 5, false, true), \u0027}\u0027));
\t\t\t_json = string(abi.encodePacked(_json, \u0027,{\"trait_type\":\"Total Gas\",\"value\":\u0027, _uint2str(_totalGas, 0, 0, false, false), \u0027}\u0027));
\t\t\t_json = string(abi.encodePacked(_json, \u0027,{\"trait_type\":\"Average Gwei\",\"value\":\u0027, _uint2str(_avgGwei, 9, 5, false, true), \u0027}\u0027));
\t\t\t_json = string(abi.encodePacked(_json, \u0027,{\"trait_type\":\"Total Transactions\",\"value\":\u0027, _uint2str(_totalTxs, 0, 0, false, false), \u0027}\u0027));
\t\t\t_json = string(abi.encodePacked(_json, \u0027,{\"trait_type\":\"Failed Transactions\",\"value\":\u0027, _uint2str(_failTxs, 0, 0, false, false), \u0027}\u0027));
\t\t\t_json = string(abi.encodePacked(_json, \u0027,{\"display_type\":\"number\",\"trait_type\":\"Spender Level\",\"value\":\u0027, _uint2str(_logn(_totalFees / 1e13, 2), 0, 0, false, false), \u0027}\u0027));
\t\t\t_json = string(abi.encodePacked(_json, \u0027,{\"display_type\":\"number\",\"trait_type\":\"Oof Level\",\"value\":\u0027, _uint2str(_logn(_failFees / 1e13, 2), 0, 0, false, false), \u0027}\u0027));
\t\t}
\t\tif (_totalDonated \u003e 0) {
\t\t\t_json = string(abi.encodePacked(_json, _totalFees \u003e 0 ? \u0027,\u0027 : \u0027\u0027, \u0027{\"display_type\":\"number\",\"trait_type\":\"Donator Level\",\"value\":\u0027, _uint2str(_logn(_totalDonated / 1e14, 10) + 1, 0, 0, false, false), \u0027}\u0027));
\t\t}
\t\t_json = string(abi.encodePacked(_json, \u0027]}\u0027));
\t\treturn string(abi.encodePacked(\"data:application/json;base64,\", _encode(bytes(_json))));
\t}

\tfunction getSVG(uint256 _tokenId) public view returns (string memory) {
\t\tuint256[7] memory _info = nft.getTokenCompressedInfo(_tokenId);
\t\treturn getRawSVG(_info[0], _info[1], _info[2], _info[3], _info[4], _info[5], _info[6], oracle.getPrice());
\t}

\tfunction getRawSVG(uint256 _totalFees, uint256 _failFees, uint256 _totalGas, uint256 _avgGwei, uint256 _totalDonated, uint256 _totalTxs, uint256 _failTxs, uint256 _price) public pure returns (string memory svg) {
\t\tsvg = string(abi.encodePacked(\"\u003csvg xmlns=\u0027http://www.w3.org/2000/svg\u0027 version=\u00271.1\u0027 preserveAspectRatio=\u0027xMidYMid meet\u0027 viewBox=\u00270 0 512 512\u0027 width=\u0027100%\u0027 height=\u0027100%\u0027\u003e\"));
\t\tsvg = string(abi.encodePacked(svg, \"\u003cdefs\u003e\u003cstyle type=\u0027text/css\u0027\u003etext{text-anchor:middle;alignment-baseline:central;}tspan\u003etspan{fill:#03a9f4;font-weight:700;}\u003c/style\u003e\u003c/defs\u003e\"));
\t\tsvg = string(abi.encodePacked(svg, \"\u003crect width=\u0027100%\u0027 height=\u0027100%\u0027 fill=\u0027#222222\u0027 /\u003e\"));
\t\tsvg = string(abi.encodePacked(svg, \"\u003ctext x=\u00270\u0027 y=\u0027256\u0027 transform=\u0027translate(256)\u0027 fill=\u0027#f0f8ff\u0027 font-family=\u0027Arial,sans-serif\u0027 font-weight=\u0027600\u0027 font-size=\u002730\u0027\u003e\"));
\t\tif (_totalFees \u003e 0) {
\t\t\tsvg = string(abi.encodePacked(svg, unicode\"\u003ctspan x=\u00270\u0027 dy=\u0027-183\u0027\u003eYou spent \u003ctspan\u003eΞ\", _uint2str(_totalFees, 18, 5, true, false), \"\u003c/tspan\u003e on gas\u003c/tspan\u003e\"));
\t\t\tsvg = string(abi.encodePacked(svg, \"\u003ctspan x=\u00270\u0027 dy=\u002735\u0027\u003ebefore block 13916450.\u003c/tspan\u003e\"));
\t\t\tsvg = string(abi.encodePacked(svg, \"\u003ctspan x=\u00270\u0027 dy=\u002735\u0027\u003eRight now, that\u0027s\u003c/tspan\u003e\"));
\t\t\tsvg = string(abi.encodePacked(svg, \"\u003ctspan x=\u00270\u0027 dy=\u002735\u0027\u003e\u003ctspan\u003e$\", _uint2str(_totalFees * _price / 1e18, 18, 2, true, true), \"\u003c/tspan\u003e.\u003c/tspan\u003e\"));
\t\t\tsvg = string(abi.encodePacked(svg, \"\u003ctspan x=\u00270\u0027 dy=\u002770\u0027\u003eYou used \u003ctspan\u003e\", _uint2str(_totalGas, 0, 0, true, false), \"\u003c/tspan\u003e\u003c/tspan\u003e\"));
\t\t\tsvg = string(abi.encodePacked(svg, \"\u003ctspan x=\u00270\u0027 dy=\u002735\u0027\u003egas to send \u003ctspan\u003e\", _uint2str(_totalTxs, 0, 0, true, false), \"\u003c/tspan\u003e\u003c/tspan\u003e\"));
\t\t\tsvg = string(abi.encodePacked(svg, \"\u003ctspan x=\u00270\u0027 dy=\u002735\u0027\u003etransaction\", _totalTxs == 1 ? \"\" : \"s\", \", with an average\u003c/tspan\u003e\"));
\t\t\tsvg = string(abi.encodePacked(svg, \"\u003ctspan x=\u00270\u0027 dy=\u002735\u0027\u003eprice of \u003ctspan\u003e\", _uint2str(_avgGwei, 9, 3, true, false), \"\u003c/tspan\u003e Gwei.\u003c/tspan\u003e\"));
\t\t\tsvg = string(abi.encodePacked(svg, \"\u003ctspan x=\u00270\u0027 dy=\u002770\u0027\u003e\u003ctspan\u003e\", _uint2str(_failTxs, 0, 0, true, false), \"\u003c/tspan\u003e of them failed,\u003c/tspan\u003e\"));
\t\t\tsvg = string(abi.encodePacked(svg, \"\u003ctspan x=\u00270\u0027 dy=\u002735\u0027\u003ecosting you \u003ctspan\u003e\", _failFees == 0 ? \"nothing\" : string(abi.encodePacked(unicode\"Ξ\", _uint2str(_failFees, 18, 5, true, false))), \"\u003c/tspan\u003e.\u003c/tspan\u003e\u003c/text\u003e\"));
\t\t} else {
\t\t\tsvg = string(abi.encodePacked(svg, \"\u003ctspan x=\u00270\u0027 dy=\u00278\u0027\u003eDid not qualify.\u003c/tspan\u003e\u003c/text\u003e\"));
\t\t}
\t\tif (_totalDonated \u003e 0) {
\t\t\tfor (uint256 i = 0; i \u003c= _logn(_totalDonated / 1e14, 10); i++) {
\t\t\t\tfor (uint256 j = 0; j \u003c 4; j++) {
\t\t\t\t\tstring memory _prefix = string(abi.encodePacked(\"\u003ctext x=\u0027\", j \u003c 2 ? \"16\" : \"496\", \"\u0027 y=\u0027\", j % 2 == 0 ? \"18\" : \"498\", \"\u0027 font-size=\u002710\u0027 transform=\u0027translate(\"));
\t\t\t\t\tsvg = string(abi.encodePacked(svg, _prefix, j \u003c 2 ? \"\" : \"-\", _uint2str(16 * i, 0, 0, false, false), \")\u0027\u003e\", unicode\"❤️\u003c/text\u003e\"));
\t\t\t\t\tif (i \u003e 0) {
\t\t\t\t\t\tsvg = string(abi.encodePacked(svg, _prefix, \"0,\", j % 2 == 0 ? \"\" : \"-\", _uint2str(16 * i, 0, 0, false, false), \")\u0027\u003e\", unicode\"❤️\u003c/text\u003e\"));
\t\t\t\t\t}
\t\t\t\t}
\t\t\t}
\t\t}
\t\tsvg = string(abi.encodePacked(svg, \"\u003ctext x=\u00270\u0027 y=\u0027500\u0027 transform=\u0027translate(256)\u0027 fill=\u0027#f0f8ff\u0027 font-family=\u0027Arial,sans-serif\u0027 font-weight=\u0027600\u0027 font-size=\u002710\u0027\u003e\u003ctspan\u003efees\u003ctspan\u003e.wtf\u003c/tspan\u003e\u003c/tspan\u003e\u003c/text\u003e\u003c/svg\u003e\"));
\t}


\tfunction _logn(uint256 _num, uint256 _n) internal pure returns (uint256) {
\t\trequire(_n \u003e 0);
\t\tuint256 _count = 0;
\t\twhile (_num \u003e _n - 1) {
\t\t\t_num /= _n;
\t\t\t_count++;
\t\t}
\t\treturn _count;
\t}
\t
\tfunction _address2str(address _address) internal pure returns (string memory str) {
\t\tstr = \"0x\";
\t\tfor (uint256 i; i \u003c 40; i++) {
\t\t\tuint256 _hex = (uint160(_address) \u003e\u003e (4 * (39 - i))) % 16;
\t\t\tbytes memory _char = new bytes(1);
\t\t\t_char[0] = bytes1(uint8(_hex) + (_hex \u003e 9 ? 87 : 48));
\t\t\tstr = string(abi.encodePacked(str, string(_char)));
\t\t}
\t}

\tfunction _trimAddress(address _address, uint256 _padding) internal pure returns (string memory str) {
\t\trequire(_padding \u003c 20);
\t\tstr = \"\";
\t\tbytes memory _strAddress = bytes(_address2str(_address));
\t\tuint256 _length = 2 * _padding + 2;
\t\tfor (uint256 i = 0; i \u003c 2 * _padding + 2; i++) {
\t\t\tbytes memory _char = new bytes(1);
\t\t\t_char[0] = _strAddress[i \u003c _padding + 2 ? i : 42 + i - _length];
\t\t\tstr = string(abi.encodePacked(str, string(_char)));
\t\t\tif (i == _padding + 1) {
\t\t\t\tstr = string(abi.encodePacked(str, unicode\"…\"));
\t\t\t}
\t\t}
\t}
\t
\tfunction _uint2str(uint256 _value, uint256 _scale, uint256 _maxDecimals, bool _commas, bool _full) internal pure returns (string memory str) {
\t\tuint256 _d = _scale \u003e _maxDecimals ? _maxDecimals : _scale;
\t\tuint256 _n = _value / 10**(_scale \u003e _d ? _scale - _d : 0);
\t\tif (_n == 0) {
\t\t\treturn \"0\";
\t\t}
\t\tuint256 _digits = 1;
\t\tuint256 _tmp = _n;
\t\twhile (_tmp \u003e 9) {
\t\t\t_tmp /= 10;
\t\t\t_digits++;
\t\t}
\t\t_tmp = _digits \u003e _d ? _digits : _d + 1;
\t\tuint256 _offset = (!_full \u0026\u0026 _tmp \u003e _d + 1 ? _tmp - _d - 1 \u003e _d ? _d : _tmp - _d - 1 : 0);
\t\tfor (uint256 i = 0; i \u003c _tmp - _offset; i++) {
\t\t\tuint256 _dec = i \u003c _tmp - _digits ? 0 : (_n / (10**(_tmp - i - 1))) % 10;
\t\t\tbytes memory _char = new bytes(1);
\t\t\t_char[0] = bytes1(uint8(_dec) + 48);
\t\t\tstr = string(abi.encodePacked(str, string(_char)));
\t\t\tif (i \u003c _tmp - _d - 1) {
\t\t\t\tif (_commas \u0026\u0026 (i + 1) % 3 == (_tmp - _d) % 3) {
\t\t\t\t\tstr = string(abi.encodePacked(str, \",\"));
\t\t\t\t}
\t\t\t} else {
\t\t\t\tif (!_full \u0026\u0026 (_n / 10**_offset) % 10**(_tmp - _offset - i - 1) == 0) {
\t\t\t\t\tbreak;
\t\t\t\t} else if (i == _tmp - _d - 1) {
\t\t\t\t\tstr = string(abi.encodePacked(str, \".\"));
\t\t\t\t}
\t\t\t}
\t\t}
\t}
\t
\tfunction _encode(bytes memory _data) internal pure returns (string memory result) {
\t\tif (_data.length == 0) return \u0027\u0027;
\t\tstring memory _table = TABLE;
\t\tuint256 _encodedLen = 4 * ((_data.length + 2) / 3);
\t\tresult = new string(_encodedLen + 32);

\t\tassembly {
\t\t\tmstore(result, _encodedLen)
\t\t\tlet tablePtr := add(_table, 1)
\t\t\tlet dataPtr := _data
\t\t\tlet endPtr := add(dataPtr, mload(_data))
\t\t\tlet resultPtr := add(result, 32)

\t\t\tfor {} lt(dataPtr, endPtr) {}
\t\t\t{
\t\t\t\tdataPtr := add(dataPtr, 3)
\t\t\t\tlet input := mload(dataPtr)
\t\t\t\tmstore(resultPtr, shl(248, mload(add(tablePtr, and(shr(18, input), 0x3F)))))
\t\t\t\tresultPtr := add(resultPtr, 1)
\t\t\t\tmstore(resultPtr, shl(248, mload(add(tablePtr, and(shr(12, input), 0x3F)))))
\t\t\t\tresultPtr := add(resultPtr, 1)
\t\t\t\tmstore(resultPtr, shl(248, mload(add(tablePtr, and(shr( 6, input), 0x3F)))))
\t\t\t\tresultPtr := add(resultPtr, 1)
\t\t\t\tmstore(resultPtr, shl(248, mload(add(tablePtr, and(        input,  0x3F)))))
\t\t\t\tresultPtr := add(resultPtr, 1)
\t\t\t}
\t\t\tswitch mod(mload(_data), 3)
\t\t\tcase 1 { mstore(sub(resultPtr, 2), shl(240, 0x3d3d)) }
\t\t\tcase 2 { mstore(sub(resultPtr, 1), shl(248, 0x3d)) }
\t\t}
\t\treturn result;
\t}
}"},"PRBMath.sol":{"content":"// SPDX-License-Identifier: Unlicense
pragma solidity \u003e=0.8.4;

/// @notice Emitted when the result overflows uint256.
error PRBMath__MulDivFixedPointOverflow(uint256 prod1);

/// @notice Emitted when the result overflows uint256.
error PRBMath__MulDivOverflow(uint256 prod1, uint256 denominator);

/// @notice Emitted when one of the inputs is type(int256).min.
error PRBMath__MulDivSignedInputTooSmall();

/// @notice Emitted when the intermediary absolute result overflows int256.
error PRBMath__MulDivSignedOverflow(uint256 rAbs);

/// @notice Emitted when the input is MIN_SD59x18.
error PRBMathSD59x18__AbsInputTooSmall();

/// @notice Emitted when ceiling a number overflows SD59x18.
error PRBMathSD59x18__CeilOverflow(int256 x);

/// @notice Emitted when one of the inputs is MIN_SD59x18.
error PRBMathSD59x18__DivInputTooSmall();

/// @notice Emitted when one of the intermediary unsigned results overflows SD59x18.
error PRBMathSD59x18__DivOverflow(uint256 rAbs);

/// @notice Emitted when the input is greater than 133.084258667509499441.
error PRBMathSD59x18__ExpInputTooBig(int256 x);

/// @notice Emitted when the input is greater than 192.
error PRBMathSD59x18__Exp2InputTooBig(int256 x);

/// @notice Emitted when flooring a number underflows SD59x18.
error PRBMathSD59x18__FloorUnderflow(int256 x);

/// @notice Emitted when converting a basic integer to the fixed-point format overflows SD59x18.
error PRBMathSD59x18__FromIntOverflow(int256 x);

/// @notice Emitted when converting a basic integer to the fixed-point format underflows SD59x18.
error PRBMathSD59x18__FromIntUnderflow(int256 x);

/// @notice Emitted when the product of the inputs is negative.
error PRBMathSD59x18__GmNegativeProduct(int256 x, int256 y);

/// @notice Emitted when multiplying the inputs overflows SD59x18.
error PRBMathSD59x18__GmOverflow(int256 x, int256 y);

/// @notice Emitted when the input is less than or equal to zero.
error PRBMathSD59x18__LogInputTooSmall(int256 x);

/// @notice Emitted when one of the inputs is MIN_SD59x18.
error PRBMathSD59x18__MulInputTooSmall();

/// @notice Emitted when the intermediary absolute result overflows SD59x18.
error PRBMathSD59x18__MulOverflow(uint256 rAbs);

/// @notice Emitted when the intermediary absolute result overflows SD59x18.
error PRBMathSD59x18__PowuOverflow(uint256 rAbs);

/// @notice Emitted when the input is negative.
error PRBMathSD59x18__SqrtNegativeInput(int256 x);

/// @notice Emitted when the calculating the square root overflows SD59x18.
error PRBMathSD59x18__SqrtOverflow(int256 x);

/// @notice Emitted when addition overflows UD60x18.
error PRBMathUD60x18__AddOverflow(uint256 x, uint256 y);

/// @notice Emitted when ceiling a number overflows UD60x18.
error PRBMathUD60x18__CeilOverflow(uint256 x);

/// @notice Emitted when the input is greater than 133.084258667509499441.
error PRBMathUD60x18__ExpInputTooBig(uint256 x);

/// @notice Emitted when the input is greater than 192.
error PRBMathUD60x18__Exp2InputTooBig(uint256 x);

/// @notice Emitted when converting a basic integer to the fixed-point format format overflows UD60x18.
error PRBMathUD60x18__FromUintOverflow(uint256 x);

/// @notice Emitted when multiplying the inputs overflows UD60x18.
error PRBMathUD60x18__GmOverflow(uint256 x, uint256 y);

/// @notice Emitted when the input is less than 1.
error PRBMathUD60x18__LogInputTooSmall(uint256 x);

/// @notice Emitted when the calculating the square root overflows UD60x18.
error PRBMathUD60x18__SqrtOverflow(uint256 x);

/// @notice Emitted when subtraction underflows UD60x18.
error PRBMathUD60x18__SubUnderflow(uint256 x, uint256 y);

/// @dev Common mathematical functions used in both PRBMathSD59x18 and PRBMathUD60x18. Note that this shared library
/// does not always assume the signed 59.18-decimal fixed-point or the unsigned 60.18-decimal fixed-point
/// representation. When it does not, it is explicitly mentioned in the NatSpec documentation.
library PRBMath {
\t/// STRUCTS ///

\tstruct SD59x18 {
\t\tint256 value;
\t}

\tstruct UD60x18 {
\t\tuint256 value;
\t}

\t/// STORAGE ///

\t/// @dev How many trailing decimals can be represented.
\tuint256 internal constant SCALE = 1e18;

\t/// @dev Largest power of two divisor of SCALE.
\tuint256 internal constant SCALE_LPOTD = 262144;

\t/// @dev SCALE inverted mod 2^256.
\tuint256 internal constant SCALE_INVERSE =
\t\t78156646155174841979727994598816262306175212592076161876661_508869554232690281;

\t/// FUNCTIONS ///

\t/// @notice Calculates the binary exponent of x using the binary fraction method.
\t/// @dev Has to use 192.64-bit fixed-point numbers.
\t/// See https://ethereum.stackexchange.com/a/96594/24693.
\t/// @param x The exponent as an unsigned 192.64-bit fixed-point number.
\t/// @return result The result as an unsigned 60.18-decimal fixed-point number.
\tfunction exp2(uint256 x) internal pure returns (uint256 result) {
\t\tunchecked {
\t\t\t// Start from 0.5 in the 192.64-bit fixed-point format.
\t\t\tresult = 0x800000000000000000000000000000000000000000000000;

\t\t\t// Multiply the result by root(2, 2^-i) when the bit at position i is 1. None of the intermediary results overflows
\t\t\t// because the initial result is 2^191 and all magic factors are less than 2^65.
\t\t\tif (x \u0026 0x8000000000000000 \u003e 0) {
\t\t\t\tresult = (result * 0x16A09E667F3BCC909) \u003e\u003e 64;
\t\t\t}
\t\t\tif (x \u0026 0x4000000000000000 \u003e 0) {
\t\t\t\tresult = (result * 0x1306FE0A31B7152DF) \u003e\u003e 64;
\t\t\t}
\t\t\tif (x \u0026 0x2000000000000000 \u003e 0) {
\t\t\t\tresult = (result * 0x1172B83C7D517ADCE) \u003e\u003e 64;
\t\t\t}
\t\t\tif (x \u0026 0x1000000000000000 \u003e 0) {
\t\t\t\tresult = (result * 0x10B5586CF9890F62A) \u003e\u003e 64;
\t\t\t}
\t\t\tif (x \u0026 0x800000000000000 \u003e 0) {
\t\t\t\tresult = (result * 0x1059B0D31585743AE) \u003e\u003e 64;
\t\t\t}
\t\t\tif (x \u0026 0x400000000000000 \u003e 0) {
\t\t\t\tresult = (result * 0x102C9A3E778060EE7) \u003e\u003e 64;
\t\t\t}
\t\t\tif (x \u0026 0x200000000000000 \u003e 0) {
\t\t\t\tresult = (result * 0x10163DA9FB33356D8) \u003e\u003e 64;
\t\t\t}
\t\t\tif (x \u0026 0x100000000000000 \u003e 0) {
\t\t\t\tresult = (result * 0x100B1AFA5ABCBED61) \u003e\u003e 64;
\t\t\t}
\t\t\tif (x \u0026 0x80000000000000 \u003e 0) {
\t\t\t\tresult = (result * 0x10058C86DA1C09EA2) \u003e\u003e 64;
\t\t\t}
\t\t\tif (x \u0026 0x40000000000000 \u003e 0) {
\t\t\t\tresult = (result * 0x1002C605E2E8CEC50) \u003e\u003e 64;
\t\t\t}
\t\t\tif (x \u0026 0x20000000000000 \u003e 0) {
\t\t\t\tresult = (result * 0x100162F3904051FA1) \u003e\u003e 64;
\t\t\t}
\t\t\tif (x \u0026 0x10000000000000 \u003e 0) {
\t\t\t\tresult = (result * 0x1000B175EFFDC76BA) \u003e\u003e 64;
\t\t\t}
\t\t\tif (x \u0026 0x8000000000000 \u003e 0) {
\t\t\t\tresult = (result * 0x100058BA01FB9F96D) \u003e\u003e 64;
\t\t\t}
\t\t\tif (x \u0026 0x4000000000000 \u003e 0) {
\t\t\t\tresult = (result * 0x10002C5CC37DA9492) \u003e\u003e 64;
\t\t\t}
\t\t\tif (x \u0026 0x2000000000000 \u003e 0) {
\t\t\t\tresult = (result * 0x1000162E525EE0547) \u003e\u003e 64;
\t\t\t}
\t\t\tif (x \u0026 0x1000000000000 \u003e 0) {
\t\t\t\tresult = (result * 0x10000B17255775C04) \u003e\u003e 64;
\t\t\t}
\t\t\tif (x \u0026 0x800000000000 \u003e 0) {
\t\t\t\tresult = (result * 0x1000058B91B5BC9AE) \u003e\u003e 64;
\t\t\t}
\t\t\tif (x \u0026 0x400000000000 \u003e 0) {
\t\t\t\tresult = (result * 0x100002C5C89D5EC6D) \u003e\u003e 64;
\t\t\t}
\t\t\tif (x \u0026 0x200000000000 \u003e 0) {
\t\t\t\tresult = (result * 0x10000162E43F4F831) \u003e\u003e 64;
\t\t\t}
\t\t\tif (x \u0026 0x100000000000 \u003e 0) {
\t\t\t\tresult = (result * 0x100000B1721BCFC9A) \u003e\u003e 64;
\t\t\t}
\t\t\tif (x \u0026 0x80000000000 \u003e 0) {
\t\t\t\tresult = (result * 0x10000058B90CF1E6E) \u003e\u003e 64;
\t\t\t}
\t\t\tif (x \u0026 0x40000000000 \u003e 0) {
\t\t\t\tresult = (result * 0x1000002C5C863B73F) \u003e\u003e 64;
\t\t\t}
\t\t\tif (x \u0026 0x20000000000 \u003e 0) {
\t\t\t\tresult = (result * 0x100000162E430E5A2) \u003e\u003e 64;
\t\t\t}
\t\t\tif (x \u0026 0x10000000000 \u003e 0) {
\t\t\t\tresult = (result * 0x1000000B172183551) \u003e\u003e 64;
\t\t\t}
\t\t\tif (x \u0026 0x8000000000 \u003e 0) {
\t\t\t\tresult = (result * 0x100000058B90C0B49) \u003e\u003e 64;
\t\t\t}
\t\t\tif (x \u0026 0x4000000000 \u003e 0) {
\t\t\t\tresult = (result * 0x10000002C5C8601CC) \u003e\u003e 64;
\t\t\t}
\t\t\tif (x \u0026 0x2000000000 \u003e 0) {
\t\t\t\tresult = (result * 0x1000000162E42FFF0) \u003e\u003e 64;
\t\t\t}
\t\t\tif (x \u0026 0x1000000000 \u003e 0) {
\t\t\t\tresult = (result * 0x10000000B17217FBB) \u003e\u003e 64;
\t\t\t}
\t\t\tif (x \u0026 0x800000000 \u003e 0) {
\t\t\t\tresult = (result * 0x1000000058B90BFCE) \u003e\u003e 64;
\t\t\t}
\t\t\tif (x \u0026 0x400000000 \u003e 0) {
\t\t\t\tresult = (result * 0x100000002C5C85FE3) \u003e\u003e 64;
\t\t\t}
\t\t\tif (x \u0026 0x200000000 \u003e 0) {
\t\t\t\tresult = (result * 0x10000000162E42FF1) \u003e\u003e 64;
\t\t\t}
\t\t\tif (x \u0026 0x100000000 \u003e 0) {
\t\t\t\tresult = (result * 0x100000000B17217F8) \u003e\u003e 64;
\t\t\t}
\t\t\tif (x \u0026 0x80000000 \u003e 0) {
\t\t\t\tresult = (result * 0x10000000058B90BFC) \u003e\u003e 64;
\t\t\t}
\t\t\tif (x \u0026 0x40000000 \u003e 0) {
\t\t\t\tresult = (result * 0x1000000002C5C85FE) \u003e\u003e 64;
\t\t\t}
\t\t\tif (x \u0026 0x20000000 \u003e 0) {
\t\t\t\tresult = (result * 0x100000000162E42FF) \u003e\u003e 64;
\t\t\t}
\t\t\tif (x \u0026 0x10000000 \u003e 0) {
\t\t\t\tresult = (result * 0x1000000000B17217F) \u003e\u003e 64;
\t\t\t}
\t\t\tif (x \u0026 0x8000000 \u003e 0) {
\t\t\t\tresult = (result * 0x100000000058B90C0) \u003e\u003e 64;
\t\t\t}
\t\t\tif (x \u0026 0x4000000 \u003e 0) {
\t\t\t\tresult = (result * 0x10000000002C5C860) \u003e\u003e 64;
\t\t\t}
\t\t\tif (x \u0026 0x2000000 \u003e 0) {
\t\t\t\tresult = (result * 0x1000000000162E430) \u003e\u003e 64;
\t\t\t}
\t\t\tif (x \u0026 0x1000000 \u003e 0) {
\t\t\t\tresult = (result * 0x10000000000B17218) \u003e\u003e 64;
\t\t\t}
\t\t\tif (x \u0026 0x800000 \u003e 0) {
\t\t\t\tresult = (result * 0x1000000000058B90C) \u003e\u003e 64;
\t\t\t}
\t\t\tif (x \u0026 0x400000 \u003e 0) {
\t\t\t\tresult = (result * 0x100000000002C5C86) \u003e\u003e 64;
\t\t\t}
\t\t\tif (x \u0026 0x200000 \u003e 0) {
\t\t\t\tresult = (result * 0x10000000000162E43) \u003e\u003e 64;
\t\t\t}
\t\t\tif (x \u0026 0x100000 \u003e 0) {
\t\t\t\tresult = (result * 0x100000000000B1721) \u003e\u003e 64;
\t\t\t}
\t\t\tif (x \u0026 0x80000 \u003e 0) {
\t\t\t\tresult = (result * 0x10000000000058B91) \u003e\u003e 64;
\t\t\t}
\t\t\tif (x \u0026 0x40000 \u003e 0) {
\t\t\t\tresult = (result * 0x1000000000002C5C8) \u003e\u003e 64;
\t\t\t}
\t\t\tif (x \u0026 0x20000 \u003e 0) {
\t\t\t\tresult = (result * 0x100000000000162E4) \u003e\u003e 64;
\t\t\t}
\t\t\tif (x \u0026 0x10000 \u003e 0) {
\t\t\t\tresult = (result * 0x1000000000000B172) \u003e\u003e 64;
\t\t\t}
\t\t\tif (x \u0026 0x8000 \u003e 0) {
\t\t\t\tresult = (result * 0x100000000000058B9) \u003e\u003e 64;
\t\t\t}
\t\t\tif (x \u0026 0x4000 \u003e 0) {
\t\t\t\tresult = (result * 0x10000000000002C5D) \u003e\u003e 64;
\t\t\t}
\t\t\tif (x \u0026 0x2000 \u003e 0) {
\t\t\t\tresult = (result * 0x1000000000000162E) \u003e\u003e 64;
\t\t\t}
\t\t\tif (x \u0026 0x1000 \u003e 0) {
\t\t\t\tresult = (result * 0x10000000000000B17) \u003e\u003e 64;
\t\t\t}
\t\t\tif (x \u0026 0x800 \u003e 0) {
\t\t\t\tresult = (result * 0x1000000000000058C) \u003e\u003e 64;
\t\t\t}
\t\t\tif (x \u0026 0x400 \u003e 0) {
\t\t\t\tresult = (result * 0x100000000000002C6) \u003e\u003e 64;
\t\t\t}
\t\t\tif (x \u0026 0x200 \u003e 0) {
\t\t\t\tresult = (result * 0x10000000000000163) \u003e\u003e 64;
\t\t\t}
\t\t\tif (x \u0026 0x100 \u003e 0) {
\t\t\t\tresult = (result * 0x100000000000000B1) \u003e\u003e 64;
\t\t\t}
\t\t\tif (x \u0026 0x80 \u003e 0) {
\t\t\t\tresult = (result * 0x10000000000000059) \u003e\u003e 64;
\t\t\t}
\t\t\tif (x \u0026 0x40 \u003e 0) {
\t\t\t\tresult = (result * 0x1000000000000002C) \u003e\u003e 64;
\t\t\t}
\t\t\tif (x \u0026 0x20 \u003e 0) {
\t\t\t\tresult = (result * 0x10000000000000016) \u003e\u003e 64;
\t\t\t}
\t\t\tif (x \u0026 0x10 \u003e 0) {
\t\t\t\tresult = (result * 0x1000000000000000B) \u003e\u003e 64;
\t\t\t}
\t\t\tif (x \u0026 0x8 \u003e 0) {
\t\t\t\tresult = (result * 0x10000000000000006) \u003e\u003e 64;
\t\t\t}
\t\t\tif (x \u0026 0x4 \u003e 0) {
\t\t\t\tresult = (result * 0x10000000000000003) \u003e\u003e 64;
\t\t\t}
\t\t\tif (x \u0026 0x2 \u003e 0) {
\t\t\t\tresult = (result * 0x10000000000000001) \u003e\u003e 64;
\t\t\t}
\t\t\tif (x \u0026 0x1 \u003e 0) {
\t\t\t\tresult = (result * 0x10000000000000001) \u003e\u003e 64;
\t\t\t}

\t\t\t// We\u0027re doing two things at the same time:
\t\t\t//
\t\t\t//   1. Multiply the result by 2^n + 1, where \"2^n\" is the integer part and the one is added to account for
\t\t\t//      the fact that we initially set the result to 0.5. This is accomplished by subtracting from 191
\t\t\t//      rather than 192.
\t\t\t//   2. Convert the result to the unsigned 60.18-decimal fixed-point format.
\t\t\t//
\t\t\t// This works because 2^(191-ip) = 2^ip / 2^191, where \"ip\" is the integer part \"2^n\".
\t\t\tresult *= SCALE;
\t\t\tresult \u003e\u003e= (191 - (x \u003e\u003e 64));
\t\t}
\t}

\t/// @notice Finds the zero-based index of the first one in the binary representation of x.
\t/// @dev See the note on msb in the \"Find First Set\" Wikipedia article https://en.wikipedia.org/wiki/Find_first_set
\t/// @param x The uint256 number for which to find the index of the most significant bit.
\t/// @return msb The index of the most significant bit as an uint256.
\tfunction mostSignificantBit(uint256 x) internal pure returns (uint256 msb) {
\t\tif (x \u003e= 2**128) {
\t\t\tx \u003e\u003e= 128;
\t\t\tmsb += 128;
\t\t}
\t\tif (x \u003e= 2**64) {
\t\t\tx \u003e\u003e= 64;
\t\t\tmsb += 64;
\t\t}
\t\tif (x \u003e= 2**32) {
\t\t\tx \u003e\u003e= 32;
\t\t\tmsb += 32;
\t\t}
\t\tif (x \u003e= 2**16) {
\t\t\tx \u003e\u003e= 16;
\t\t\tmsb += 16;
\t\t}
\t\tif (x \u003e= 2**8) {
\t\t\tx \u003e\u003e= 8;
\t\t\tmsb += 8;
\t\t}
\t\tif (x \u003e= 2**4) {
\t\t\tx \u003e\u003e= 4;
\t\t\tmsb += 4;
\t\t}
\t\tif (x \u003e= 2**2) {
\t\t\tx \u003e\u003e= 2;
\t\t\tmsb += 2;
\t\t}
\t\tif (x \u003e= 2**1) {
\t\t\t// No need to shift x any more.
\t\t\tmsb += 1;
\t\t}
\t}

\t/// @notice Calculates floor(x*y÷denominator) with full precision.
\t///
\t/// @dev Credit to Remco Bloemen under MIT license https://xn--2-umb.com/21/muldiv.
\t///
\t/// Requirements:
\t/// - The denominator cannot be zero.
\t/// - The result must fit within uint256.
\t///
\t/// Caveats:
\t/// - This function does not work with fixed-point numbers.
\t///
\t/// @param x The multiplicand as an uint256.
\t/// @param y The multiplier as an uint256.
\t/// @param denominator The divisor as an uint256.
\t/// @return result The result as an uint256.
\tfunction mulDiv(
\t\tuint256 x,
\t\tuint256 y,
\t\tuint256 denominator
\t) internal pure returns (uint256 result) {
\t\t// 512-bit multiply [prod1 prod0] = x * y. Compute the product mod 2^256 and mod 2^256 - 1, then use
\t\t// use the Chinese Remainder Theorem to reconstruct the 512 bit result. The result is stored in two 256
\t\t// variables such that product = prod1 * 2^256 + prod0.
\t\tuint256 prod0; // Least significant 256 bits of the product
\t\tuint256 prod1; // Most significant 256 bits of the product
\t\tassembly {
\t\t\tlet mm := mulmod(x, y, not(0))
\t\t\tprod0 := mul(x, y)
\t\t\tprod1 := sub(sub(mm, prod0), lt(mm, prod0))
\t\t}

\t\t// Handle non-overflow cases, 256 by 256 division.
\t\tif (prod1 == 0) {
\t\t\tunchecked {
\t\t\t\tresult = prod0 / denominator;
\t\t\t}
\t\t\treturn result;
\t\t}

\t\t// Make sure the result is less than 2^256. Also prevents denominator == 0.
\t\tif (prod1 \u003e= denominator) {
\t\t\trevert PRBMath__MulDivOverflow(prod1, denominator);
\t\t}

\t\t///////////////////////////////////////////////
\t\t// 512 by 256 division.
\t\t///////////////////////////////////////////////

\t\t// Make division exact by subtracting the remainder from [prod1 prod0].
\t\tuint256 remainder;
\t\tassembly {
\t\t\t// Compute remainder using mulmod.
\t\t\tremainder := mulmod(x, y, denominator)

\t\t\t// Subtract 256 bit number from 512 bit number.
\t\t\tprod1 := sub(prod1, gt(remainder, prod0))
\t\t\tprod0 := sub(prod0, remainder)
\t\t}

\t\t// Factor powers of two out of denominator and compute largest power of two divisor of denominator. Always \u003e= 1.
\t\t// See https://cs.stackexchange.com/q/138556/92363.
\t\tunchecked {
\t\t\t// Does not overflow because the denominator cannot be zero at this stage in the function.
\t\t\tuint256 lpotdod = denominator \u0026 (~denominator + 1);
\t\t\tassembly {
\t\t\t\t// Divide denominator by lpotdod.
\t\t\t\tdenominator := div(denominator, lpotdod)

\t\t\t\t// Divide [prod1 prod0] by lpotdod.
\t\t\t\tprod0 := div(prod0, lpotdod)

\t\t\t\t// Flip lpotdod such that it is 2^256 / lpotdod. If lpotdod is zero, then it becomes one.
\t\t\t\tlpotdod := add(div(sub(0, lpotdod), lpotdod), 1)
\t\t\t}

\t\t\t// Shift in bits from prod1 into prod0.
\t\t\tprod0 |= prod1 * lpotdod;

\t\t\t// Invert denominator mod 2^256. Now that denominator is an odd number, it has an inverse modulo 2^256 such
\t\t\t// that denominator * inv = 1 mod 2^256. Compute the inverse by starting with a seed that is correct for
\t\t\t// four bits. That is, denominator * inv = 1 mod 2^4.
\t\t\tuint256 inverse = (3 * denominator) ^ 2;

\t\t\t// Use the Newton-Raphson iteration to improve the precision. Thanks to Hensel\u0027s lifting lemma, this also works
\t\t\t// in modular arithmetic, doubling the correct bits in each step.
\t\t\tinverse *= 2 - denominator * inverse; // inverse mod 2^8
\t\t\tinverse *= 2 - denominator * inverse; // inverse mod 2^16
\t\t\tinverse *= 2 - denominator * inverse; // inverse mod 2^32
\t\t\tinverse *= 2 - denominator * inverse; // inverse mod 2^64
\t\t\tinverse *= 2 - denominator * inverse; // inverse mod 2^128
\t\t\tinverse *= 2 - denominator * inverse; // inverse mod 2^256

\t\t\t// Because the division is now exact we can divide by multiplying with the modular inverse of denominator.
\t\t\t// This will give us the correct result modulo 2^256. Since the preconditions guarantee that the outcome is
\t\t\t// less than 2^256, this is the final result. We don\u0027t need to compute the high bits of the result and prod1
\t\t\t// is no longer required.
\t\t\tresult = prod0 * inverse;
\t\t\treturn result;
\t\t}
\t}

\t/// @notice Calculates floor(x*y÷1e18) with full precision.
\t///
\t/// @dev Variant of \"mulDiv\" with constant folding, i.e. in which the denominator is always 1e18. Before returning the
\t/// final result, we add 1 if (x * y) % SCALE \u003e= HALF_SCALE. Without this, 6.6e-19 would be truncated to 0 instead of
\t/// being rounded to 1e-18.  See \"Listing 6\" and text above it at https://accu.org/index.php/journals/1717.
\t///
\t/// Requirements:
\t/// - The result must fit within uint256.
\t///
\t/// Caveats:
\t/// - The body is purposely left uncommented; see the NatSpec comments in \"PRBMath.mulDiv\" to understand how this works.
\t/// - It is assumed that the result can never be type(uint256).max when x and y solve the following two equations:
\t///     1. x * y = type(uint256).max * SCALE
\t///     2. (x * y) % SCALE \u003e= SCALE / 2
\t///
\t/// @param x The multiplicand as an unsigned 60.18-decimal fixed-point number.
\t/// @param y The multiplier as an unsigned 60.18-decimal fixed-point number.
\t/// @return result The result as an unsigned 60.18-decimal fixed-point number.
\tfunction mulDivFixedPoint(uint256 x, uint256 y) internal pure returns (uint256 result) {
\t\tuint256 prod0;
\t\tuint256 prod1;
\t\tassembly {
\t\t\tlet mm := mulmod(x, y, not(0))
\t\t\tprod0 := mul(x, y)
\t\t\tprod1 := sub(sub(mm, prod0), lt(mm, prod0))
\t\t}

\t\tif (prod1 \u003e= SCALE) {
\t\t\trevert PRBMath__MulDivFixedPointOverflow(prod1);
\t\t}

\t\tuint256 remainder;
\t\tuint256 roundUpUnit;
\t\tassembly {
\t\t\tremainder := mulmod(x, y, SCALE)
\t\t\troundUpUnit := gt(remainder, 499999999999999999)
\t\t}

\t\tif (prod1 == 0) {
\t\t\tunchecked {
\t\t\t\tresult = (prod0 / SCALE) + roundUpUnit;
\t\t\t\treturn result;
\t\t\t}
\t\t}

\t\tassembly {
\t\t\tresult := add(
\t\t\t\tmul(
\t\t\t\t\tor(
\t\t\t\t\t\tdiv(sub(prod0, remainder), SCALE_LPOTD),
\t\t\t\t\t\tmul(sub(prod1, gt(remainder, prod0)), add(div(sub(0, SCALE_LPOTD), SCALE_LPOTD), 1))
\t\t\t\t\t),
\t\t\t\t\tSCALE_INVERSE
\t\t\t\t),
\t\t\t\troundUpUnit
\t\t\t)
\t\t}
\t}

\t/// @notice Calculates floor(x*y÷denominator) with full precision.
\t///
\t/// @dev An extension of \"mulDiv\" for signed numbers. Works by computing the signs and the absolute values separately.
\t///
\t/// Requirements:
\t/// - None of the inputs can be type(int256).min.
\t/// - The result must fit within int256.
\t///
\t/// @param x The multiplicand as an int256.
\t/// @param y The multiplier as an int256.
\t/// @param denominator The divisor as an int256.
\t/// @return result The result as an int256.
\tfunction mulDivSigned(
\t\tint256 x,
\t\tint256 y,
\t\tint256 denominator
\t) internal pure returns (int256 result) {
\t\tif (x == type(int256).min || y == type(int256).min || denominator == type(int256).min) {
\t\t\trevert PRBMath__MulDivSignedInputTooSmall();
\t\t}

\t\t// Get hold of the absolute values of x, y and the denominator.
\t\tuint256 ax;
\t\tuint256 ay;
\t\tuint256 ad;
\t\tunchecked {
\t\t\tax = x \u003c 0 ? uint256(-x) : uint256(x);
\t\t\tay = y \u003c 0 ? uint256(-y) : uint256(y);
\t\t\tad = denominator \u003c 0 ? uint256(-denominator) : uint256(denominator);
\t\t}

\t\t// Compute the absolute value of (x*y)÷denominator. The result must fit within int256.
\t\tuint256 rAbs = mulDiv(ax, ay, ad);
\t\tif (rAbs \u003e uint256(type(int256).max)) {
\t\t\trevert PRBMath__MulDivSignedOverflow(rAbs);
\t\t}

\t\t// Get the signs of x, y and the denominator.
\t\tuint256 sx;
\t\tuint256 sy;
\t\tuint256 sd;
\t\tassembly {
\t\t\tsx := sgt(x, sub(0, 1))
\t\t\tsy := sgt(y, sub(0, 1))
\t\t\tsd := sgt(denominator, sub(0, 1))
\t\t}

\t\t// XOR over sx, sy and sd. This is checking whether there are one or three negative signs in the inputs.
\t\t// If yes, the result should be negative.
\t\tresult = sx ^ sy ^ sd == 0 ? -int256(rAbs) : int256(rAbs);
\t}

\t/// @notice Calculates the square root of x, rounding down.
\t/// @dev Uses the Babylonian method https://en.wikipedia.org/wiki/Methods_of_computing_square_roots#Babylonian_method.
\t///
\t/// Caveats:
\t/// - This function does not work with fixed-point numbers.
\t///
\t/// @param x The uint256 number for which to calculate the square root.
\t/// @return result The result as an uint256.
\tfunction sqrt(uint256 x) internal pure returns (uint256 result) {
\t\tif (x == 0) {
\t\t\treturn 0;
\t\t}

\t\t// Set the initial guess to the least power of two that is greater than or equal to sqrt(x).
\t\tuint256 xAux = uint256(x);
\t\tresult = 1;
\t\tif (xAux \u003e= 0x100000000000000000000000000000000) {
\t\t\txAux \u003e\u003e= 128;
\t\t\tresult \u003c\u003c= 64;
\t\t}
\t\tif (xAux \u003e= 0x10000000000000000) {
\t\t\txAux \u003e\u003e= 64;
\t\t\tresult \u003c\u003c= 32;
\t\t}
\t\tif (xAux \u003e= 0x100000000) {
\t\t\txAux \u003e\u003e= 32;
\t\t\tresult \u003c\u003c= 16;
\t\t}
\t\tif (xAux \u003e= 0x10000) {
\t\t\txAux \u003e\u003e= 16;
\t\t\tresult \u003c\u003c= 8;
\t\t}
\t\tif (xAux \u003e= 0x100) {
\t\t\txAux \u003e\u003e= 8;
\t\t\tresult \u003c\u003c= 4;
\t\t}
\t\tif (xAux \u003e= 0x10) {
\t\t\txAux \u003e\u003e= 4;
\t\t\tresult \u003c\u003c= 2;
\t\t}
\t\tif (xAux \u003e= 0x8) {
\t\t\tresult \u003c\u003c= 1;
\t\t}

\t\t// The operations can never overflow because the result is max 2^127 when it enters this block.
\t\tunchecked {
\t\t\tresult = (result + x / result) \u003e\u003e 1;
\t\t\tresult = (result + x / result) \u003e\u003e 1;
\t\t\tresult = (result + x / result) \u003e\u003e 1;
\t\t\tresult = (result + x / result) \u003e\u003e 1;
\t\t\tresult = (result + x / result) \u003e\u003e 1;
\t\t\tresult = (result + x / result) \u003e\u003e 1;
\t\t\tresult = (result + x / result) \u003e\u003e 1; // Seven iterations should be enough
\t\t\tuint256 roundedDownResult = x / result;
\t\t\treturn result \u003e= roundedDownResult ? roundedDownResult : result;
\t\t}
\t}
}"},"PRBMathUD60x18.sol":{"content":"// SPDX-License-Identifier: Unlicense
pragma solidity \u003e=0.8.4;

import \"./PRBMath.sol\";

/// @title PRBMathUD60x18
/// @author Paul Razvan Berg
/// @notice Smart contract library for advanced fixed-point math that works with uint256 numbers considered to have 18
/// trailing decimals. We call this number representation unsigned 60.18-decimal fixed-point, since there can be up to 60
/// digits in the integer part and up to 18 decimals in the fractional part. The numbers are bound by the minimum and the
/// maximum values permitted by the Solidity type uint256.
library PRBMathUD60x18 {
\t/// @dev Half the SCALE number.
\tuint256 internal constant HALF_SCALE = 5e17;

\t/// @dev log2(e) as an unsigned 60.18-decimal fixed-point number.
\tuint256 internal constant LOG2_E = 1_442695040888963407;

\t/// @dev The maximum value an unsigned 60.18-decimal fixed-point number can have.
\tuint256 internal constant MAX_UD60x18 =
\t\t115792089237316195423570985008687907853269984665640564039457_584007913129639935;

\t/// @dev The maximum whole value an unsigned 60.18-decimal fixed-point number can have.
\tuint256 internal constant MAX_WHOLE_UD60x18 =
\t\t115792089237316195423570985008687907853269984665640564039457_000000000000000000;

\t/// @dev How many trailing decimals can be represented.
\tuint256 internal constant SCALE = 1e18;

\t/// @notice Calculates the arithmetic average of x and y, rounding down.
\t/// @param x The first operand as an unsigned 60.18-decimal fixed-point number.
\t/// @param y The second operand as an unsigned 60.18-decimal fixed-point number.
\t/// @return result The arithmetic average as an unsigned 60.18-decimal fixed-point number.
\tfunction avg(uint256 x, uint256 y) internal pure returns (uint256 result) {
\t\t// The operations can never overflow.
\t\tunchecked {
\t\t\t// The last operand checks if both x and y are odd and if that is the case, we add 1 to the result. We need
\t\t\t// to do this because if both numbers are odd, the 0.5 remainder gets truncated twice.
\t\t\tresult = (x \u003e\u003e 1) + (y \u003e\u003e 1) + (x \u0026 y \u0026 1);
\t\t}
\t}

\t/// @notice Yields the least unsigned 60.18 decimal fixed-point number greater than or equal to x.
\t///
\t/// @dev Optimized for fractional value inputs, because for every whole value there are (1e18 - 1) fractional counterparts.
\t/// See https://en.wikipedia.org/wiki/Floor_and_ceiling_functions.
\t///
\t/// Requirements:
\t/// - x must be less than or equal to MAX_WHOLE_UD60x18.
\t///
\t/// @param x The unsigned 60.18-decimal fixed-point number to ceil.
\t/// @param result The least integer greater than or equal to x, as an unsigned 60.18-decimal fixed-point number.
\tfunction ceil(uint256 x) internal pure returns (uint256 result) {
\t\tif (x \u003e MAX_WHOLE_UD60x18) {
\t\t\trevert PRBMathUD60x18__CeilOverflow(x);
\t\t}
\t\tassembly {
\t\t\t// Equivalent to \"x % SCALE\" but faster.
\t\t\tlet remainder := mod(x, SCALE)

\t\t\t// Equivalent to \"SCALE - remainder\" but faster.
\t\t\tlet delta := sub(SCALE, remainder)

\t\t\t// Equivalent to \"x + delta * (remainder \u003e 0 ? 1 : 0)\" but faster.
\t\t\tresult := add(x, mul(delta, gt(remainder, 0)))
\t\t}
\t}

\t/// @notice Divides two unsigned 60.18-decimal fixed-point numbers, returning a new unsigned 60.18-decimal fixed-point number.
\t///
\t/// @dev Uses mulDiv to enable overflow-safe multiplication and division.
\t///
\t/// Requirements:
\t/// - The denominator cannot be zero.
\t///
\t/// @param x The numerator as an unsigned 60.18-decimal fixed-point number.
\t/// @param y The denominator as an unsigned 60.18-decimal fixed-point number.
\t/// @param result The quotient as an unsigned 60.18-decimal fixed-point number.
\tfunction div(uint256 x, uint256 y) internal pure returns (uint256 result) {
\t\tresult = PRBMath.mulDiv(x, SCALE, y);
\t}

\t/// @notice Returns Euler\u0027s number as an unsigned 60.18-decimal fixed-point number.
\t/// @dev See https://en.wikipedia.org/wiki/E_(mathematical_constant).
\tfunction e() internal pure returns (uint256 result) {
\t\tresult = 2_718281828459045235;
\t}

\t/// @notice Calculates the natural exponent of x.
\t///
\t/// @dev Based on the insight that e^x = 2^(x * log2(e)).
\t///
\t/// Requirements:
\t/// - All from \"log2\".
\t/// - x must be less than 133.084258667509499441.
\t///
\t/// @param x The exponent as an unsigned 60.18-decimal fixed-point number.
\t/// @return result The result as an unsigned 60.18-decimal fixed-point number.
\tfunction exp(uint256 x) internal pure returns (uint256 result) {
\t\t// Without this check, the value passed to \"exp2\" would be greater than 192.
\t\tif (x \u003e= 133_084258667509499441) {
\t\t\trevert PRBMathUD60x18__ExpInputTooBig(x);
\t\t}

\t\t// Do the fixed-point multiplication inline to save gas.
\t\tunchecked {
\t\t\tuint256 doubleScaleProduct = x * LOG2_E;
\t\t\tresult = exp2((doubleScaleProduct + HALF_SCALE) / SCALE);
\t\t}
\t}

\t/// @notice Calculates the binary exponent of x using the binary fraction method.
\t///
\t/// @dev See https://ethereum.stackexchange.com/q/79903/24693.
\t///
\t/// Requirements:
\t/// - x must be 192 or less.
\t/// - The result must fit within MAX_UD60x18.
\t///
\t/// @param x The exponent as an unsigned 60.18-decimal fixed-point number.
\t/// @return result The result as an unsigned 60.18-decimal fixed-point number.
\tfunction exp2(uint256 x) internal pure returns (uint256 result) {
\t\t// 2^192 doesn\u0027t fit within the 192.64-bit format used internally in this function.
\t\tif (x \u003e= 192e18) {
\t\t\trevert PRBMathUD60x18__Exp2InputTooBig(x);
\t\t}

\t\tunchecked {
\t\t\t// Convert x to the 192.64-bit fixed-point format.
\t\t\tuint256 x192x64 = (x \u003c\u003c 64) / SCALE;

\t\t\t// Pass x to the PRBMath.exp2 function, which uses the 192.64-bit fixed-point number representation.
\t\t\tresult = PRBMath.exp2(x192x64);
\t\t}
\t}

\t/// @notice Yields the greatest unsigned 60.18 decimal fixed-point number less than or equal to x.
\t/// @dev Optimized for fractional value inputs, because for every whole value there are (1e18 - 1) fractional counterparts.
\t/// See https://en.wikipedia.org/wiki/Floor_and_ceiling_functions.
\t/// @param x The unsigned 60.18-decimal fixed-point number to floor.
\t/// @param result The greatest integer less than or equal to x, as an unsigned 60.18-decimal fixed-point number.
\tfunction floor(uint256 x) internal pure returns (uint256 result) {
\t\tassembly {
\t\t\t// Equivalent to \"x % SCALE\" but faster.
\t\t\tlet remainder := mod(x, SCALE)

\t\t\t// Equivalent to \"x - remainder * (remainder \u003e 0 ? 1 : 0)\" but faster.
\t\t\tresult := sub(x, mul(remainder, gt(remainder, 0)))
\t\t}
\t}

\t/// @notice Yields the excess beyond the floor of x.
\t/// @dev Based on the odd function definition https://en.wikipedia.org/wiki/Fractional_part.
\t/// @param x The unsigned 60.18-decimal fixed-point number to get the fractional part of.
\t/// @param result The fractional part of x as an unsigned 60.18-decimal fixed-point number.
\tfunction frac(uint256 x) internal pure returns (uint256 result) {
\t\tassembly {
\t\t\tresult := mod(x, SCALE)
\t\t}
\t}

\t/// @notice Converts a number from basic integer form to unsigned 60.18-decimal fixed-point representation.
\t///
\t/// @dev Requirements:
\t/// - x must be less than or equal to MAX_UD60x18 divided by SCALE.
\t///
\t/// @param x The basic integer to convert.
\t/// @param result The same number in unsigned 60.18-decimal fixed-point representation.
\tfunction fromUint(uint256 x) internal pure returns (uint256 result) {
\t\tunchecked {
\t\t\tif (x \u003e MAX_UD60x18 / SCALE) {
\t\t\t\trevert PRBMathUD60x18__FromUintOverflow(x);
\t\t\t}
\t\t\tresult = x * SCALE;
\t\t}
\t}

\t/// @notice Calculates geometric mean of x and y, i.e. sqrt(x * y), rounding down.
\t///
\t/// @dev Requirements:
\t/// - x * y must fit within MAX_UD60x18, lest it overflows.
\t///
\t/// @param x The first operand as an unsigned 60.18-decimal fixed-point number.
\t/// @param y The second operand as an unsigned 60.18-decimal fixed-point number.
\t/// @return result The result as an unsigned 60.18-decimal fixed-point number.
\tfunction gm(uint256 x, uint256 y) internal pure returns (uint256 result) {
\t\tif (x == 0) {
\t\t\treturn 0;
\t\t}

\t\tunchecked {
\t\t\t// Checking for overflow this way is faster than letting Solidity do it.
\t\t\tuint256 xy = x * y;
\t\t\tif (xy / x != y) {
\t\t\t\trevert PRBMathUD60x18__GmOverflow(x, y);
\t\t\t}

\t\t\t// We don\u0027t need to multiply by the SCALE here because the x*y product had already picked up a factor of SCALE
\t\t\t// during multiplication. See the comments within the \"sqrt\" function.
\t\t\tresult = PRBMath.sqrt(xy);
\t\t}
\t}

\t/// @notice Calculates 1 / x, rounding toward zero.
\t///
\t/// @dev Requirements:
\t/// - x cannot be zero.
\t///
\t/// @param x The unsigned 60.18-decimal fixed-point number for which to calculate the inverse.
\t/// @return result The inverse as an unsigned 60.18-decimal fixed-point number.
\tfunction inv(uint256 x) internal pure returns (uint256 result) {
\t\tunchecked {
\t\t\t// 1e36 is SCALE * SCALE.
\t\t\tresult = 1e36 / x;
\t\t}
\t}

\t/// @notice Calculates the natural logarithm of x.
\t///
\t/// @dev Based on the insight that ln(x) = log2(x) / log2(e).
\t///
\t/// Requirements:
\t/// - All from \"log2\".
\t///
\t/// Caveats:
\t/// - All from \"log2\".
\t/// - This doesn\u0027t return exactly 1 for 2.718281828459045235, for that we would need more fine-grained precision.
\t///
\t/// @param x The unsigned 60.18-decimal fixed-point number for which to calculate the natural logarithm.
\t/// @return result The natural logarithm as an unsigned 60.18-decimal fixed-point number.
\tfunction ln(uint256 x) internal pure returns (uint256 result) {
\t\t// Do the fixed-point multiplication inline to save gas. This is overflow-safe because the maximum value that log2(x)
\t\t// can return is 196205294292027477728.
\t\tunchecked {
\t\t\tresult = (log2(x) * SCALE) / LOG2_E;
\t\t}
\t}

\t/// @notice Calculates the common logarithm of x.
\t///
\t/// @dev First checks if x is an exact power of ten and it stops if yes. If it\u0027s not, calculates the common
\t/// logarithm based on the insight that log10(x) = log2(x) / log2(10).
\t///
\t/// Requirements:
\t/// - All from \"log2\".
\t///
\t/// Caveats:
\t/// - All from \"log2\".
\t///
\t/// @param x The unsigned 60.18-decimal fixed-point number for which to calculate the common logarithm.
\t/// @return result The common logarithm as an unsigned 60.18-decimal fixed-point number.
\tfunction log10(uint256 x) internal pure returns (uint256 result) {
\t\tif (x \u003c SCALE) {
\t\t\trevert PRBMathUD60x18__LogInputTooSmall(x);
\t\t}

\t\t// Note that the \"mul\" in this block is the assembly multiplication operation, not the \"mul\" function defined
\t\t// in this contract.
\t\t// prettier-ignore
\t\tassembly {
\t\t\tswitch x
\t\t\tcase 1 { result := mul(SCALE, sub(0, 18)) }
\t\t\tcase 10 { result := mul(SCALE, sub(1, 18)) }
\t\t\tcase 100 { result := mul(SCALE, sub(2, 18)) }
\t\t\tcase 1000 { result := mul(SCALE, sub(3, 18)) }
\t\t\tcase 10000 { result := mul(SCALE, sub(4, 18)) }
\t\t\tcase 100000 { result := mul(SCALE, sub(5, 18)) }
\t\t\tcase 1000000 { result := mul(SCALE, sub(6, 18)) }
\t\t\tcase 10000000 { result := mul(SCALE, sub(7, 18)) }
\t\t\tcase 100000000 { result := mul(SCALE, sub(8, 18)) }
\t\t\tcase 1000000000 { result := mul(SCALE, sub(9, 18)) }
\t\t\tcase 10000000000 { result := mul(SCALE, sub(10, 18)) }
\t\t\tcase 100000000000 { result := mul(SCALE, sub(11, 18)) }
\t\t\tcase 1000000000000 { result := mul(SCALE, sub(12, 18)) }
\t\t\tcase 10000000000000 { result := mul(SCALE, sub(13, 18)) }
\t\t\tcase 100000000000000 { result := mul(SCALE, sub(14, 18)) }
\t\t\tcase 1000000000000000 { result := mul(SCALE, sub(15, 18)) }
\t\t\tcase 10000000000000000 { result := mul(SCALE, sub(16, 18)) }
\t\t\tcase 100000000000000000 { result := mul(SCALE, sub(17, 18)) }
\t\t\tcase 1000000000000000000 { result := 0 }
\t\t\tcase 10000000000000000000 { result := SCALE }
\t\t\tcase 100000000000000000000 { result := mul(SCALE, 2) }
\t\t\tcase 1000000000000000000000 { result := mul(SCALE, 3) }
\t\t\tcase 10000000000000000000000 { result := mul(SCALE, 4) }
\t\t\tcase 100000000000000000000000 { result := mul(SCALE, 5) }
\t\t\tcase 1000000000000000000000000 { result := mul(SCALE, 6) }
\t\t\tcase 10000000000000000000000000 { result := mul(SCALE, 7) }
\t\t\tcase 100000000000000000000000000 { result := mul(SCALE, 8) }
\t\t\tcase 1000000000000000000000000000 { result := mul(SCALE, 9) }
\t\t\tcase 10000000000000000000000000000 { result := mul(SCALE, 10) }
\t\t\tcase 100000000000000000000000000000 { result := mul(SCALE, 11) }
\t\t\tcase 1000000000000000000000000000000 { result := mul(SCALE, 12) }
\t\t\tcase 10000000000000000000000000000000 { result := mul(SCALE, 13) }
\t\t\tcase 100000000000000000000000000000000 { result := mul(SCALE, 14) }
\t\t\tcase 1000000000000000000000000000000000 { result := mul(SCALE, 15) }
\t\t\tcase 10000000000000000000000000000000000 { result := mul(SCALE, 16) }
\t\t\tcase 100000000000000000000000000000000000 { result := mul(SCALE, 17) }
\t\t\tcase 1000000000000000000000000000000000000 { result := mul(SCALE, 18) }
\t\t\tcase 10000000000000000000000000000000000000 { result := mul(SCALE, 19) }
\t\t\tcase 100000000000000000000000000000000000000 { result := mul(SCALE, 20) }
\t\t\tcase 1000000000000000000000000000000000000000 { result := mul(SCALE, 21) }
\t\t\tcase 10000000000000000000000000000000000000000 { result := mul(SCALE, 22) }
\t\t\tcase 100000000000000000000000000000000000000000 { result := mul(SCALE, 23) }
\t\t\tcase 1000000000000000000000000000000000000000000 { result := mul(SCALE, 24) }
\t\t\tcase 10000000000000000000000000000000000000000000 { result := mul(SCALE, 25) }
\t\t\tcase 100000000000000000000000000000000000000000000 { result := mul(SCALE, 26) }
\t\t\tcase 1000000000000000000000000000000000000000000000 { result := mul(SCALE, 27) }
\t\t\tcase 10000000000000000000000000000000000000000000000 { result := mul(SCALE, 28) }
\t\t\tcase 100000000000000000000000000000000000000000000000 { result := mul(SCALE, 29) }
\t\t\tcase 1000000000000000000000000000000000000000000000000 { result := mul(SCALE, 30) }
\t\t\tcase 10000000000000000000000000000000000000000000000000 { result := mul(SCALE, 31) }
\t\t\tcase 100000000000000000000000000000000000000000000000000 { result := mul(SCALE, 32) }
\t\t\tcase 1000000000000000000000000000000000000000000000000000 { result := mul(SCALE, 33) }
\t\t\tcase 10000000000000000000000000000000000000000000000000000 { result := mul(SCALE, 34) }
\t\t\tcase 100000000000000000000000000000000000000000000000000000 { result := mul(SCALE, 35) }
\t\t\tcase 1000000000000000000000000000000000000000000000000000000 { result := mul(SCALE, 36) }
\t\t\tcase 10000000000000000000000000000000000000000000000000000000 { result := mul(SCALE, 37) }
\t\t\tcase 100000000000000000000000000000000000000000000000000000000 { result := mul(SCALE, 38) }
\t\t\tcase 1000000000000000000000000000000000000000000000000000000000 { result := mul(SCALE, 39) }
\t\t\tcase 10000000000000000000000000000000000000000000000000000000000 { result := mul(SCALE, 40) }
\t\t\tcase 100000000000000000000000000000000000000000000000000000000000 { result := mul(SCALE, 41) }
\t\t\tcase 1000000000000000000000000000000000000000000000000000000000000 { result := mul(SCALE, 42) }
\t\t\tcase 10000000000000000000000000000000000000000000000000000000000000 { result := mul(SCALE, 43) }
\t\t\tcase 100000000000000000000000000000000000000000000000000000000000000 { result := mul(SCALE, 44) }
\t\t\tcase 1000000000000000000000000000000000000000000000000000000000000000 { result := mul(SCALE, 45) }
\t\t\tcase 10000000000000000000000000000000000000000000000000000000000000000 { result := mul(SCALE, 46) }
\t\t\tcase 100000000000000000000000000000000000000000000000000000000000000000 { result := mul(SCALE, 47) }
\t\t\tcase 1000000000000000000000000000000000000000000000000000000000000000000 { result := mul(SCALE, 48) }
\t\t\tcase 10000000000000000000000000000000000000000000000000000000000000000000 { result := mul(SCALE, 49) }
\t\t\tcase 100000000000000000000000000000000000000000000000000000000000000000000 { result := mul(SCALE, 50) }
\t\t\tcase 1000000000000000000000000000000000000000000000000000000000000000000000 { result := mul(SCALE, 51) }
\t\t\tcase 10000000000000000000000000000000000000000000000000000000000000000000000 { result := mul(SCALE, 52) }
\t\t\tcase 100000000000000000000000000000000000000000000000000000000000000000000000 { result := mul(SCALE, 53) }
\t\t\tcase 1000000000000000000000000000000000000000000000000000000000000000000000000 { result := mul(SCALE, 54) }
\t\t\tcase 10000000000000000000000000000000000000000000000000000000000000000000000000 { result := mul(SCALE, 55) }
\t\t\tcase 100000000000000000000000000000000000000000000000000000000000000000000000000 { result := mul(SCALE, 56) }
\t\t\tcase 1000000000000000000000000000000000000000000000000000000000000000000000000000 { result := mul(SCALE, 57) }
\t\t\tcase 10000000000000000000000000000000000000000000000000000000000000000000000000000 { result := mul(SCALE, 58) }
\t\t\tcase 100000000000000000000000000000000000000000000000000000000000000000000000000000 { result := mul(SCALE, 59) }
\t\t\tdefault {
\t\t\t\tresult := MAX_UD60x18
\t\t\t}
\t\t}

\t\tif (result == MAX_UD60x18) {
\t\t\t// Do the fixed-point division inline to save gas. The denominator is log2(10).
\t\t\tunchecked {
\t\t\t\tresult = (log2(x) * SCALE) / 3_321928094887362347;
\t\t\t}
\t\t}
\t}

\t/// @notice Calculates the binary logarithm of x.
\t///
\t/// @dev Based on the iterative approximation algorithm.
\t/// https://en.wikipedia.org/wiki/Binary_logarithm#Iterative_approximation
\t///
\t/// Requirements:
\t/// - x must be greater than or equal to SCALE, otherwise the result would be negative.
\t///
\t/// Caveats:
\t/// - The results are nor perfectly accurate to the last decimal, due to the lossy precision of the iterative approximation.
\t///
\t/// @param x The unsigned 60.18-decimal fixed-point number for which to calculate the binary logarithm.
\t/// @return result The binary logarithm as an unsigned 60.18-decimal fixed-point number.
\tfunction log2(uint256 x) internal pure returns (uint256 result) {
\t\tif (x \u003c SCALE) {
\t\t\trevert PRBMathUD60x18__LogInputTooSmall(x);
\t\t}
\t\tunchecked {
\t\t\t// Calculate the integer part of the logarithm and add it to the result and finally calculate y = x * 2^(-n).
\t\t\tuint256 n = PRBMath.mostSignificantBit(x / SCALE);

\t\t\t// The integer part of the logarithm as an unsigned 60.18-decimal fixed-point number. The operation can\u0027t overflow
\t\t\t// because n is maximum 255 and SCALE is 1e18.
\t\t\tresult = n * SCALE;

\t\t\t// This is y = x * 2^(-n).
\t\t\tuint256 y = x \u003e\u003e n;

\t\t\t// If y = 1, the fractional part is zero.
\t\t\tif (y == SCALE) {
\t\t\t\treturn result;
\t\t\t}

\t\t\t// Calculate the fractional part via the iterative approximation.
\t\t\t// The \"delta \u003e\u003e= 1\" part is equivalent to \"delta /= 2\", but shifting bits is faster.
\t\t\tfor (uint256 delta = HALF_SCALE; delta \u003e 0; delta \u003e\u003e= 1) {
\t\t\t\ty = (y * y) / SCALE;

\t\t\t\t// Is y^2 \u003e 2 and so in the range [2,4)?
\t\t\t\tif (y \u003e= 2 * SCALE) {
\t\t\t\t\t// Add the 2^(-m) factor to the logarithm.
\t\t\t\t\tresult += delta;

\t\t\t\t\t// Corresponds to z/2 on Wikipedia.
\t\t\t\t\ty \u003e\u003e= 1;
\t\t\t\t}
\t\t\t}
\t\t}
\t}

\t/// @notice Multiplies two unsigned 60.18-decimal fixed-point numbers together, returning a new unsigned 60.18-decimal
\t/// fixed-point number.
\t/// @dev See the documentation for the \"PRBMath.mulDivFixedPoint\" function.
\t/// @param x The multiplicand as an unsigned 60.18-decimal fixed-point number.
\t/// @param y The multiplier as an unsigned 60.18-decimal fixed-point number.
\t/// @return result The product as an unsigned 60.18-decimal fixed-point number.
\tfunction mul(uint256 x, uint256 y) internal pure returns (uint256 result) {
\t\tresult = PRBMath.mulDivFixedPoint(x, y);
\t}

\t/// @notice Returns PI as an unsigned 60.18-decimal fixed-point number.
\tfunction pi() internal pure returns (uint256 result) {
\t\tresult = 3_141592653589793238;
\t}

\t/// @notice Raises x to the power of y.
\t///
\t/// @dev Based on the insight that x^y = 2^(log2(x) * y).
\t///
\t/// Requirements:
\t/// - All from \"exp2\", \"log2\" and \"mul\".
\t///
\t/// Caveats:
\t/// - All from \"exp2\", \"log2\" and \"mul\".
\t/// - Assumes 0^0 is 1.
\t///
\t/// @param x Number to raise to given power y, as an unsigned 60.18-decimal fixed-point number.
\t/// @param y Exponent to raise x to, as an unsigned 60.18-decimal fixed-point number.
\t/// @return result x raised to power y, as an unsigned 60.18-decimal fixed-point number.
\tfunction pow(uint256 x, uint256 y) internal pure returns (uint256 result) {
\t\tif (x == 0) {
\t\t\tresult = y == 0 ? SCALE : uint256(0);
\t\t} else {
\t\t\tresult = exp2(mul(log2(x), y));
\t\t}
\t}

\t/// @notice Raises x (unsigned 60.18-decimal fixed-point number) to the power of y (basic unsigned integer) using the
\t/// famous algorithm \"exponentiation by squaring\".
\t///
\t/// @dev See https://en.wikipedia.org/wiki/Exponentiation_by_squaring
\t///
\t/// Requirements:
\t/// - The result must fit within MAX_UD60x18.
\t///
\t/// Caveats:
\t/// - All from \"mul\".
\t/// - Assumes 0^0 is 1.
\t///
\t/// @param x The base as an unsigned 60.18-decimal fixed-point number.
\t/// @param y The exponent as an uint256.
\t/// @return result The result as an unsigned 60.18-decimal fixed-point number.
\tfunction powu(uint256 x, uint256 y) internal pure returns (uint256 result) {
\t\t// Calculate the first iteration of the loop in advance.
\t\tresult = y \u0026 1 \u003e 0 ? x : SCALE;

\t\t// Equivalent to \"for(y /= 2; y \u003e 0; y /= 2)\" but faster.
\t\tfor (y \u003e\u003e= 1; y \u003e 0; y \u003e\u003e= 1) {
\t\t\tx = PRBMath.mulDivFixedPoint(x, x);

\t\t\t// Equivalent to \"y % 2 == 1\" but faster.
\t\t\tif (y \u0026 1 \u003e 0) {
\t\t\t\tresult = PRBMath.mulDivFixedPoint(result, x);
\t\t\t}
\t\t}
\t}

\t/// @notice Returns 1 as an unsigned 60.18-decimal fixed-point number.
\tfunction scale() internal pure returns (uint256 result) {
\t\tresult = SCALE;
\t}

\t/// @notice Calculates the square root of x, rounding down.
\t/// @dev Uses the Babylonian method https://en.wikipedia.org/wiki/Methods_of_computing_square_roots#Babylonian_method.
\t///
\t/// Requirements:
\t/// - x must be less than MAX_UD60x18 / SCALE.
\t///
\t/// @param x The unsigned 60.18-decimal fixed-point number for which to calculate the square root.
\t/// @return result The result as an unsigned 60.18-decimal fixed-point .
\tfunction sqrt(uint256 x) internal pure returns (uint256 result) {
\t\tunchecked {
\t\t\tif (x \u003e MAX_UD60x18 / SCALE) {
\t\t\t\trevert PRBMathUD60x18__SqrtOverflow(x);
\t\t\t}
\t\t\t// Multiply x by the SCALE to account for the factor of SCALE that is picked up when multiplying two unsigned
\t\t\t// 60.18-decimal fixed-point numbers together (in this case, those two numbers are both the square root).
\t\t\tresult = PRBMath.sqrt(x * SCALE);
\t\t}
\t}

\t/// @notice Converts a unsigned 60.18-decimal fixed-point number to basic integer form, rounding down in the process.
\t/// @param x The unsigned 60.18-decimal fixed-point number to convert.
\t/// @return result The same number in basic integer form.
\tfunction toUint(uint256 x) internal pure returns (uint256 result) {
\t\tunchecked {
\t\t\tresult = x / SCALE;
\t\t}
\t}
}"},"StakingRewards.sol":{"content":"// SPDX-License-Identifier: MIT
pragma solidity ^0.8.11;

import \"./WTF.sol\";
import \"./ERC20.sol\";
import \"./PRBMathUD60x18.sol\";

contract StakingRewards {

\tusing PRBMathUD60x18 for uint256;

\tuint256 constant private FLOAT_SCALAR = 2**64;
\tuint256 constant private PERCENT_FEE = 5; // only for WTF staking
\tuint256 constant private X_TICK = 30 days;

\tstruct User {
\t\tuint256 deposited;
\t\tint256 scaledPayout;
\t}

\tstruct Info {
\t\tuint256 totalRewards;
\t\tuint256 startTime;
\t\tuint256 lastUpdated;
\t\tuint256 pendingFee;
\t\tuint256 scaledRewardsPerToken;
\t\tuint256 totalDeposited;
\t\tmapping(address =\u003e User) users;
\t\tWTF wtf;
\t\tERC20 token;
\t}
\tInfo private info;


\tevent Deposit(address indexed user, uint256 amount, uint256 fee);
\tevent Withdraw(address indexed user, uint256 amount, uint256 fee);
\tevent Claim(address indexed user, uint256 amount);
\tevent Reinvest(address indexed user, uint256 amount);
\tevent Reward(uint256 amount);


\tconstructor(uint256 _totalRewards, uint256 _stakingRewardsStart, ERC20 _token) {
\t\tinfo.totalRewards = _totalRewards;
\t\tinfo.startTime = block.timestamp \u003c _stakingRewardsStart ? _stakingRewardsStart : block.timestamp;
\t\tinfo.lastUpdated = startTime();
\t\tinfo.wtf = WTF(msg.sender);
\t\tinfo.token = _token;
\t}

\tfunction update() public {
\t\tuint256 _now = block.timestamp;
\t\tif (_now \u003e info.lastUpdated \u0026\u0026 totalDeposited() \u003e 0) {
\t\t\tuint256 _reward = info.totalRewards.mul(_delta(_getX(info.lastUpdated), _getX(_now)));
\t\t\tif (info.pendingFee \u003e 0) {
\t\t\t\t_reward += info.pendingFee;
\t\t\t\tinfo.pendingFee = 0;
\t\t\t}
\t\t\tuint256 _balanceBefore = info.wtf.balanceOf(address(this));
\t\t\tinfo.wtf.claimRewards();
\t\t\t_reward += info.wtf.balanceOf(address(this)) - _balanceBefore;
\t\t\tinfo.lastUpdated = _now;
\t\t\t_disburse(_reward);
\t\t}
\t}

\tfunction deposit(uint256 _amount) external {
\t\tdepositFor(msg.sender, _amount);
\t}

\tfunction depositFor(address _user, uint256 _amount) public {
\t\trequire(_amount \u003e 0);
\t\tupdate();
\t\tuint256 _balanceBefore = info.token.balanceOf(address(this));
\t\tinfo.token.transferFrom(msg.sender, address(this), _amount);
\t\tuint256 _amountReceived = info.token.balanceOf(address(this)) - _balanceBefore;
\t\t_deposit(_user, _amountReceived);
\t}

\tfunction tokenCallback(address _from, uint256 _tokens, bytes calldata) external returns (bool) {
\t\trequire(_isWTF() \u0026\u0026 msg.sender == tokenAddress());
\t\trequire(_tokens \u003e 0);
\t\tupdate();
\t\t_deposit(_from, _tokens);
\t\treturn true;
\t}

\tfunction disburse(uint256 _amount) public {
\t\trequire(_amount \u003e 0);
\t\tupdate();
\t\tuint256 _balanceBefore = info.wtf.balanceOf(address(this));
\t\tinfo.wtf.transferFrom(msg.sender, address(this), _amount);
\t\tuint256 _amountReceived = info.wtf.balanceOf(address(this)) - _balanceBefore;
\t\t_processFee(_amountReceived);
\t}

\tfunction withdrawAll() public {
\t\tuint256 _deposited = depositedOf(msg.sender);
\t\tif (_deposited \u003e 0) {
\t\t\twithdraw(_deposited);
\t\t}
\t}

\tfunction withdraw(uint256 _amount) public {
\t\trequire(_amount \u003e 0 \u0026\u0026 _amount \u003c= depositedOf(msg.sender));
\t\tupdate();
\t\tinfo.totalDeposited -= _amount;
\t\tinfo.users[msg.sender].deposited -= _amount;
\t\tinfo.users[msg.sender].scaledPayout -= int256(_amount * info.scaledRewardsPerToken);
\t\tuint256 _fee = _calculateFee(_amount);
\t\tinfo.token.transfer(msg.sender, _amount - _fee);
\t\t_processFee(_fee);
\t\temit Withdraw(msg.sender, _amount, _fee);
\t}

\tfunction claim() public {
\t\tupdate();
\t\tuint256 _rewards = rewardsOf(msg.sender);
\t\tif (_rewards \u003e 0) {
\t\t\tinfo.users[msg.sender].scaledPayout += int256(_rewards * FLOAT_SCALAR);
\t\t\tinfo.wtf.transfer(msg.sender, _rewards);
\t\t\temit Claim(msg.sender, _rewards);
\t\t}
\t}

\tfunction reinvest() public {
\t\trequire(_isWTF());
\t\tupdate();
\t\tuint256 _rewards = rewardsOf(msg.sender);
\t\tif (_rewards \u003e 0) {
\t\t\tinfo.users[msg.sender].scaledPayout += int256(_rewards * FLOAT_SCALAR);
\t\t\t_deposit(msg.sender, _rewards);
\t\t\temit Reinvest(msg.sender, _rewards);
\t\t}
\t}

\t
\tfunction wtfAddress() public view returns (address) {
\t\treturn address(info.wtf);
\t}
\t
\tfunction tokenAddress() public view returns (address) {
\t\treturn address(info.token);
\t}

\tfunction startTime() public view returns (uint256) {
\t\treturn info.startTime;
\t}

\tfunction totalDeposited() public view returns (uint256) {
\t\treturn info.totalDeposited;
\t}

\tfunction depositedOf(address _user) public view returns (uint256) {
\t\treturn info.users[_user].deposited;
\t}
\t
\tfunction rewardsOf(address _user) public view returns (uint256) {
\t\treturn uint256(int256(info.scaledRewardsPerToken * depositedOf(_user)) - info.users[_user].scaledPayout) / FLOAT_SCALAR;
\t}
\t
\tfunction currentRatePerDay() public view returns (uint256) {
\t\tif (block.timestamp \u003c startTime()) {
\t\t\treturn info.totalRewards.mul(_delta(_getX(startTime()), _getX(startTime() + 24 hours)));
\t\t} else {
\t\t\treturn info.totalRewards.mul(_delta(_getX(block.timestamp), _getX(block.timestamp + 24 hours)));
\t\t}
\t}

\tfunction totalDistributed() public view returns (uint256) {
\t\treturn info.totalRewards.mul(_sum(_getX(block.timestamp)));
\t}

\tfunction allInfoFor(address _user) external view returns (uint256 startingTime, uint256 totalRewardsDistributed, uint256 rewardsRatePerDay, uint256 currentFeePercent, uint256 totalTokensDeposited, uint256 virtualRewards, uint256 userWTF, uint256 userBalance, uint256 userAllowance, uint256 userDeposited, uint256 userRewards) {
\t\tstartingTime = startTime();
\t\ttotalRewardsDistributed = totalDistributed();
\t\trewardsRatePerDay = currentRatePerDay();
\t\tcurrentFeePercent = _calculateFee(1e20);
\t\ttotalTokensDeposited = totalDeposited();
\t\tvirtualRewards = block.timestamp \u003e info.lastUpdated ? info.totalRewards.mul(_delta(_getX(info.lastUpdated), _getX(block.timestamp))) : 0;
\t\tuserWTF = info.wtf.balanceOf(_user);
\t\tuserBalance = info.token.balanceOf(_user);
\t\tuserAllowance = info.token.allowance(_user, address(this));
\t\tuserDeposited = depositedOf(_user);
\t\tuserRewards = rewardsOf(_user);
\t}

\t
\tfunction _deposit(address _user, uint256 _amount) internal {
\t\tuint256 _fee = _calculateFee(_amount);
\t\tuint256 _deposited = _amount - _fee;
\t\tinfo.totalDeposited += _deposited;
\t\tinfo.users[_user].deposited += _deposited;
\t\tinfo.users[_user].scaledPayout += int256(_deposited * info.scaledRewardsPerToken);
\t\t_processFee(_fee);
\t\temit Deposit(_user, _amount, _fee);
\t}
\t
\tfunction _processFee(uint256 _fee) internal {
\t\tif (_fee \u003e 0) {
\t\t\tif (block.timestamp \u003c startTime() || totalDeposited() == 0) {
\t\t\t\tinfo.pendingFee += _fee;
\t\t\t} else {
\t\t\t\t_disburse(_fee);
\t\t\t}
\t\t}
\t}

\tfunction _disburse(uint256 _amount) internal {
\t\tinfo.scaledRewardsPerToken += _amount * FLOAT_SCALAR / totalDeposited();
\t\temit Reward(_amount);
\t}


\tfunction _isWTF() internal view returns (bool) {
\t\treturn wtfAddress() == tokenAddress();
\t}

\tfunction _calculateFee(uint256 _amount) internal view returns (uint256) {
\t\treturn _isWTF() ? (_amount * PERCENT_FEE / 100).mul(1e18 - _sum(_getX(block.timestamp))) : 0;
\t}
\t
\tfunction _getX(uint256 t) internal view returns (uint256) {
\t\tuint256 _start = startTime();
\t\tif (t \u003c _start) {
\t\t\treturn 0;
\t\t} else {
\t\t\treturn ((t - _start) * 1e18).div(X_TICK * 1e18);
\t\t}
\t}

\tfunction _sum(uint256 x) internal pure returns (uint256) {
\t\tuint256 _e2x = x.exp2();
\t\treturn (_e2x - 1e18).div(_e2x);
\t}

\tfunction _delta(uint256 x1, uint256 x2) internal pure returns (uint256) {
\t\trequire(x2 \u003e= x1);
\t\treturn _sum(x2) - _sum(x1);
\t}
}"},"Treasury.sol":{"content":"// SPDX-License-Identifier: MIT
pragma solidity ^0.8.11;

import \"./ERC20.sol\";
import \"./WTF.sol\";
import \"./StakingRewards.sol\";


contract FeeManager {

\tWTF private wtf;

\tconstructor() {
\t\twtf = WTF(msg.sender);
\t}

\tfunction disburse() external {
\t\twtf.claimRewards();
\t\tuint256 _balance = wtf.balanceOf(address(this));
\t\tif (_balance \u003e 0) {
\t\t\tuint256 _oneFifth = _balance / 5;
\t\t\tTreasury(payable(wtf.treasuryAddress())).collect();
\t\t\twtf.transfer(wtf.treasuryAddress(), _oneFifth); // 20%
\t\t\tStakingRewards(wtf.stakingRewardsAddress()).disburse(_oneFifth); // 20%
\t\t\tStakingRewards(wtf.lpStakingRewardsAddress()).disburse(3 * _oneFifth); // 60%
\t\t}
\t}


\tfunction wtfAddress() external view returns (address) {
\t\treturn address(wtf);
\t}
}


contract TeamReferral {
\treceive() external payable {}
\tfunction release() external {
\t\taddress _this = address(this);
\t\trequire(_this.balance \u003e 0);
\t\tpayable(0x6129E7bCb71C0d7D4580141C4E6a995f16293F42).transfer(_this.balance / 10); // 10%
\t\tpayable(0xc9AebdD8fD0d52c35A32fD9155467Cf28Ce474c3).transfer(_this.balance / 3); // 30%
\t\tpayable(0xdEE79eD62B42e30EA7EbB6f1b7A3f04143D18b7F).transfer(_this.balance / 2); // 30%
\t\tpayable(0x575446Aa9E9647C40edB7a467e45C5916add1538).transfer(_this.balance); // 30%
\t}
}


contract Treasury {

\taddress public owner;
\tuint256 public lockedUntil;
\tWTF private wtf;

\tmodifier _onlyOwner() {
\t\trequire(msg.sender == owner);
\t\t_;
\t}

\tconstructor() {
\t\towner = 0x65dd4990719bE9B20322e4E8D3Bd77a4401a0357;
\t\tlockedUntil = block.timestamp + 30 days;
\t\twtf = WTF(msg.sender);
\t}

\treceive() external payable {}

\tfunction setOwner(address _owner) external _onlyOwner {
\t\towner = _owner;
\t}

\tfunction transferETH(address payable _destination, uint256 _amount) external _onlyOwner {
\t\trequire(isUnlocked());
\t\t_destination.transfer(_amount);
\t}

\tfunction transferTokens(ERC20 _token, address _destination, uint256 _amount) external _onlyOwner {
\t\trequire(isUnlocked());
\t\t_token.transfer(_destination, _amount);
\t}

\tfunction collect() external {
\t\twtf.claimRewards();
\t}


\tfunction isUnlocked() public view returns (bool) {
\t\treturn block.timestamp \u003e lockedUntil;
\t}

\tfunction wtfAddress() external view returns (address) {
\t\treturn address(wtf);
\t}
}"},"WTF.sol":{"content":"// SPDX-License-Identifier: MIT
pragma solidity ^0.8.11;

import \"./WTFNFT.sol\";
import \"./Treasury.sol\";
import \"./StakingRewards.sol\";

interface Callable {
\tfunction tokenCallback(address _from, uint256 _tokens, bytes calldata _data) external returns (bool);
}

interface Router {
\tfunction WETH() external pure returns (address);
\tfunction factory() external pure returns (address);
}

interface Factory {
\tfunction createPair(address, address) external returns (address);
}

interface Pair {
\tfunction token0() external view returns (address);
\tfunction totalSupply() external view returns (uint256);
\tfunction balanceOf(address) external view returns (uint256);
\tfunction getReserves() external view returns (uint112 reserve0, uint112 reserve1, uint32 blockTimestampLast);
}


contract WTF {

\tuint256 constant private FLOAT_SCALAR = 2**64;
\tuint256 constant private UINT_MAX = type(uint256).max;
\tuint256 constant private TRANSFER_FEE_SCALE = 1000; // 1 = 0.1%
\tuint256 constant private WTF_STAKING_SUPPLY = 2e25; // 20M WTF
\tuint256 constant private LP_STAKING_SUPPLY = 4e25; // 40M WTF
\tuint256 constant private TREASURY_SUPPLY = 4e25; // 40M WTF
\tuint256 constant private BASE_UPGRADE_COST = 1e19; // 10 WTF
\tuint256 constant private SERVICE_FEE = 0.01 ether;

\tstring constant public name = \"fees.wtf\";
\tstring constant public symbol = \"WTF\";
\tuint8 constant public decimals = 18;

\tstruct User {
\t\tuint256 balance;
\t\tmapping(address =\u003e uint256) allowance;
\t\tint256 scaledPayout;
\t\tuint256 reflinkLevel;
\t\tbool unlocked;
\t}

\tstruct Info {
\t\tbytes32 merkleRoot;
\t\tuint256 openingTime;
\t\tuint256 closingTime;
\t\tuint256 totalSupply;
\t\tuint256 scaledRewardsPerToken;
\t\tmapping(uint256 =\u003e uint256) claimedWTFBitMap;
\t\tmapping(uint256 =\u003e uint256) claimedNFTBitMap;
\t\tmapping(address =\u003e User) users;
\t\tmapping(address =\u003e bool) toWhitelist;
\t\tmapping(address =\u003e bool) fromWhitelist;
\t\taddress owner;
\t\tRouter router;
\t\tPair pair;
\t\tbool weth0;
\t\tWTFNFT nft;
\t\tTeamReferral team;
\t\tTreasury treasury;
\t\tStakingRewards stakingRewards;
\t\tStakingRewards lpStakingRewards;
\t\taddress feeManager;
\t\tuint256 transferFee;
\t\tuint256 feeManagerPercent;
\t}
\tInfo private info;


\tevent Transfer(address indexed from, address indexed to, uint256 tokens);
\tevent Approval(address indexed owner, address indexed spender, uint256 tokens);
\tevent WhitelistUpdated(address indexed user, bool fromWhitelisted, bool toWhitelisted);
\tevent ReflinkRewards(address indexed referrer, uint256 amount);
\tevent ClaimRewards(address indexed user, uint256 amount);
\tevent Reward(uint256 amount);

\tmodifier _onlyOwner() {
\t\trequire(msg.sender == owner());
\t\t_;
\t}


\tconstructor(bytes32 _merkleRoot, uint256 _openingTime, uint256 _stakingRewardsStart) {
\t\tinfo.merkleRoot = _merkleRoot;
\t\tinfo.openingTime = block.timestamp \u003c _openingTime ? _openingTime : block.timestamp;
\t\tinfo.closingTime = openingTime() + 30 days;
\t\tinfo.router = Router(0x7a250d5630B4cF539739dF2C5dAcb4c659F2488D);
\t\tinfo.pair = Pair(Factory(info.router.factory()).createPair(info.router.WETH(), address(this)));
\t\tinfo.weth0 = info.pair.token0() == info.router.WETH();
\t\tinfo.transferFee = 40; // 4%
\t\tinfo.feeManagerPercent = 25; // 25%
\t\tinfo.owner = 0x65dd4990719bE9B20322e4E8D3Bd77a4401a0357;
\t\tinfo.nft = new WTFNFT();
\t\tinfo.team = new TeamReferral();
\t\tinfo.treasury = new Treasury();
\t\t_mint(treasuryAddress(), TREASURY_SUPPLY);
\t\tinfo.stakingRewards = new StakingRewards(WTF_STAKING_SUPPLY, _stakingRewardsStart, ERC20(address(this)));
\t\t_mint(stakingRewardsAddress(), WTF_STAKING_SUPPLY);
\t\tinfo.lpStakingRewards = new StakingRewards(LP_STAKING_SUPPLY, _stakingRewardsStart, ERC20(pairAddress()));
\t\t_mint(lpStakingRewardsAddress(), LP_STAKING_SUPPLY);
\t\tinfo.feeManager = address(new FeeManager());
\t\t_approve(feeManagerAddress(), stakingRewardsAddress(), UINT_MAX);
\t\t_approve(feeManagerAddress(), lpStakingRewardsAddress(), UINT_MAX);
\t}

\tfunction setOwner(address _owner) external _onlyOwner {
\t\tinfo.owner = _owner;
\t}

\tfunction setFeeManager(address _feeManager) external _onlyOwner {
\t\tinfo.feeManager = _feeManager;
\t}

\tfunction setClosingTime(uint256 _closingTime) external _onlyOwner {
\t\tinfo.closingTime = _closingTime;
\t}

\tfunction setTransferFee(uint256 _transferFee) external _onlyOwner {
\t\trequire(_transferFee \u003c= 100); // ≤10%
\t\tinfo.transferFee = _transferFee;
\t}

\tfunction setFeeManagerPercent(uint256 _feeManagerPercent) external _onlyOwner {
\t\trequire(_feeManagerPercent \u003c= 100);
\t\tinfo.feeManagerPercent = _feeManagerPercent;
\t}

\tfunction setWhitelisted(address _address, bool _fromWhitelisted, bool _toWhitelisted) external _onlyOwner {
\t\tinfo.fromWhitelist[_address] = _fromWhitelisted;
\t\tinfo.toWhitelist[_address] = _toWhitelisted;
\t\temit WhitelistUpdated(_address, _fromWhitelisted, _toWhitelisted);
\t}


\tfunction disburse(uint256 _amount) external {
\t\trequire(_amount \u003e 0);
\t\tuint256 _balanceBefore = balanceOf(address(this));
\t\t_transfer(msg.sender, address(this), _amount);
\t\tuint256 _amountReceived = balanceOf(address(this)) - _balanceBefore;
\t\t_disburse(_amountReceived);
\t}

\tfunction sweep() external {
\t\tif (address(this).balance \u003e 0) {
\t\t\tteamAddress().transfer(address(this).balance);
\t\t}
\t}

\tfunction upgradeReflink(uint256 _toLevel) external {
\t\tuint256 _currentLevel = reflinkLevel(msg.sender);
\t\trequire(_currentLevel \u003c _toLevel);
\t\tuint256 _totalCost = 0;
\t\tfor (uint256 i = _currentLevel; i \u003c _toLevel; i++) {
\t\t\t_totalCost += upgradeCost(i);
\t\t}
\t\tburn(_totalCost);
\t\tinfo.users[msg.sender].reflinkLevel = _toLevel;
\t}

\tfunction unlock(address _account, address payable _referrer) external payable {
\t\trequire(block.timestamp \u003c closingTime());
\t\trequire(!isUnlocked(_account));
\t\trequire(msg.value == SERVICE_FEE);
\t\tuint256 _refFee = 0;
\t\tif (_referrer != address(0x0)) {
\t\t\t_refFee = SERVICE_FEE * reflinkPercent(_referrer) / 100;
\t\t\t!_referrer.send(_refFee);
\t\t\temit ReflinkRewards(_referrer, _refFee);
\t\t}
\t\tuint256 _remaining = SERVICE_FEE - _refFee;
\t\tteamAddress().transfer(_remaining);
\t\temit ReflinkRewards(teamAddress(), _remaining);
\t\tinfo.users[_account].unlocked = true;
\t}
\t
\tfunction claim(address _account, uint256[9] calldata _data, bytes32[] calldata _proof) external {
\t\t// Data array in format: (index, amount, totalFees, failFees, totalGas, avgGwei, totalDonated, totalTxs, failTxs)
\t\tclaimWTF(_account, _data, _proof);
\t\tclaimNFT(_account, _data, _proof);
\t}
\t
\tfunction claimWTF(address _account, uint256[9] calldata _data, bytes32[] calldata _proof) public {
\t\trequire(isOpen());
\t\trequire(isUnlocked(_account));
\t\tuint256 _index = _data[0];
\t\tuint256 _amount = _data[1];
\t\trequire(!isClaimedWTF(_index));
\t\trequire(_verify(_proof, keccak256(abi.encodePacked(_account, _data))));
\t\tuint256 _claimedWordIndex = _index / 256;
\t\tuint256 _claimedBitIndex = _index % 256;
\t\tinfo.claimedWTFBitMap[_claimedWordIndex] = info.claimedWTFBitMap[_claimedWordIndex] | (1 \u003c\u003c _claimedBitIndex);
\t\t_mint(_account, _amount);
\t}

\tfunction claimNFT(address _account, uint256[9] calldata _data, bytes32[] calldata _proof) public {
\t\trequire(isOpen());
\t\trequire(isUnlocked(_account));
\t\tuint256 _index = _data[0];
\t\trequire(!isClaimedNFT(_index));
\t\trequire(_verify(_proof, keccak256(abi.encodePacked(_account, _data))));
\t\tuint256 _claimedWordIndex = _index / 256;
\t\tuint256 _claimedBitIndex = _index % 256;
\t\tinfo.claimedNFTBitMap[_claimedWordIndex] = info.claimedNFTBitMap[_claimedWordIndex] | (1 \u003c\u003c _claimedBitIndex);
\t\tinfo.nft.mint(_account, _data[2], _data[3], _data[4], _data[5], _data[6], _data[7], _data[8]);
\t}

\tfunction claimRewards() external {
\t\tboostRewards();
\t\tuint256 _rewards = rewardsOf(msg.sender);
\t\tif (_rewards \u003e 0) {
\t\t\tinfo.users[msg.sender].scaledPayout += int256(_rewards * FLOAT_SCALAR);
\t\t\t_transfer(address(this), msg.sender, _rewards);
\t\t\temit ClaimRewards(msg.sender, _rewards);
\t\t}
\t}

\tfunction boostRewards() public {
\t\taddress _this = address(this);
\t\tuint256 _rewards = rewardsOf(_this);
\t\tif (_rewards \u003e 0) {
\t\t\tinfo.users[_this].scaledPayout += int256(_rewards * FLOAT_SCALAR);
\t\t\t_disburse(_rewards);
\t\t\temit ClaimRewards(_this, _rewards);
\t\t}
\t}

\tfunction burn(uint256 _tokens) public {
\t\trequire(balanceOf(msg.sender) \u003e= _tokens);
\t\tinfo.totalSupply -= _tokens;
\t\tinfo.users[msg.sender].balance -= _tokens;
\t\tinfo.users[msg.sender].scaledPayout -= int256(_tokens * info.scaledRewardsPerToken);
\t\temit Transfer(msg.sender, address(0x0), _tokens);
\t}

\tfunction transfer(address _to, uint256 _tokens) external returns (bool) {
\t\treturn _transfer(msg.sender, _to, _tokens);
\t}

\tfunction approve(address _spender, uint256 _tokens) external returns (bool) {
\t\treturn _approve(msg.sender, _spender, _tokens);
\t}

\tfunction transferFrom(address _from, address _to, uint256 _tokens) external returns (bool) {
\t\tuint256 _allowance = allowance(_from, msg.sender);
\t\trequire(_allowance \u003e= _tokens);
\t\tif (_allowance != UINT_MAX) {
\t\t\tinfo.users[_from].allowance[msg.sender] -= _tokens;
\t\t}
\t\treturn _transfer(_from, _to, _tokens);
\t}

\tfunction transferAndCall(address _to, uint256 _tokens, bytes calldata _data) external returns (bool) {
\t\tuint256 _balanceBefore = balanceOf(_to);
\t\t_transfer(msg.sender, _to, _tokens);
\t\tuint256 _tokensReceived = balanceOf(_to) - _balanceBefore;
\t\tuint32 _size;
\t\tassembly {
\t\t\t_size := extcodesize(_to)
\t\t}
\t\tif (_size \u003e 0) {
\t\t\trequire(Callable(_to).tokenCallback(msg.sender, _tokensReceived, _data));
\t\t}
\t\treturn true;
\t}
\t

\tfunction pairAddress() public view returns (address) {
\t\treturn address(info.pair);
\t}

\tfunction nftAddress() external view returns (address) {
\t\treturn address(info.nft);
\t}

\tfunction teamAddress() public view returns (address payable) {
\t\treturn payable(address(info.team));
\t}

\tfunction treasuryAddress() public view returns (address) {
\t\treturn address(info.treasury);
\t}

\tfunction stakingRewardsAddress() public view returns (address) {
\t\treturn address(info.stakingRewards);
\t}

\tfunction lpStakingRewardsAddress() public view returns (address) {
\t\treturn address(info.lpStakingRewards);
\t}

\tfunction feeManagerAddress() public view returns (address) {
\t\treturn info.feeManager;
\t}

\tfunction owner() public view returns (address) {
\t\treturn info.owner;
\t}

\tfunction transferFee() public view returns (uint256) {
\t\treturn info.transferFee;
\t}

\tfunction feeManagerPercent() public view returns (uint256) {
\t\treturn info.feeManagerPercent;
\t}

\tfunction isFromWhitelisted(address _address) public view returns (bool) {
\t\treturn info.fromWhitelist[_address];
\t}

\tfunction isToWhitelisted(address _address) public view returns (bool) {
\t\treturn info.toWhitelist[_address];
\t}

\tfunction merkleRoot() public view returns (bytes32) {
\t\treturn info.merkleRoot;
\t}

\tfunction openingTime() public view returns (uint256) {
\t\treturn info.openingTime;
\t}

\tfunction closingTime() public view returns (uint256) {
\t\treturn info.closingTime;
\t}

\tfunction isOpen() public view returns (bool) {
\t\treturn block.timestamp \u003e openingTime() \u0026\u0026 block.timestamp \u003c closingTime();
\t}

\tfunction isUnlocked(address _user) public view returns (bool) {
\t\treturn info.users[_user].unlocked;
\t}

\tfunction isClaimedWTF(uint256 _index) public view returns (bool) {
\t\tuint256 _claimedWordIndex = _index / 256;
\t\tuint256 _claimedBitIndex = _index % 256;
\t\tuint256 _claimedWord = info.claimedWTFBitMap[_claimedWordIndex];
\t\tuint256 _mask = (1 \u003c\u003c _claimedBitIndex);
\t\treturn _claimedWord \u0026 _mask == _mask;
\t}

\tfunction isClaimedNFT(uint256 _index) public view returns (bool) {
\t\tuint256 _claimedWordIndex = _index / 256;
\t\tuint256 _claimedBitIndex = _index % 256;
\t\tuint256 _claimedWord = info.claimedNFTBitMap[_claimedWordIndex];
\t\tuint256 _mask = (1 \u003c\u003c _claimedBitIndex);
\t\treturn _claimedWord \u0026 _mask == _mask;
\t}
\t
\tfunction totalSupply() public view returns (uint256) {
\t\treturn info.totalSupply;
\t}

\tfunction balanceOf(address _user) public view returns (uint256) {
\t\treturn info.users[_user].balance;
\t}

\tfunction rewardsOf(address _user) public view returns (uint256) {
\t\treturn uint256(int256(info.scaledRewardsPerToken * balanceOf(_user)) - info.users[_user].scaledPayout) / FLOAT_SCALAR;
\t}

\tfunction allowance(address _user, address _spender) public view returns (uint256) {
\t\treturn info.users[_user].allowance[_spender];
\t}

\tfunction reflinkLevel(address _user) public view returns (uint256) {
\t\treturn info.users[_user].reflinkLevel;
\t}

\tfunction reflinkPercent(address _user) public view returns (uint256) {
\t\treturn 10 * (reflinkLevel(_user) + 1);
\t}

\tfunction upgradeCost(uint256 _reflinkLevel) public pure returns (uint256) {
\t\trequire(_reflinkLevel \u003c 4);
\t\treturn BASE_UPGRADE_COST * 10**_reflinkLevel;
\t}

\tfunction reflinkInfoFor(address _user) external view returns (uint256 balance, uint256 level, uint256 percent) {
\t\treturn (balanceOf(_user), reflinkLevel(_user), reflinkPercent(_user));
\t}

\tfunction claimInfoFor(uint256 _index, address _user) external view returns (uint256 openTime, uint256 closeTime, bool unlocked, bool claimedWTF, bool claimedNFT, uint256 wethReserve, uint256 wtfReserve) {
\t\topenTime = openingTime();
\t\tcloseTime = closingTime();
\t\tunlocked = isUnlocked(_user);
\t\tclaimedWTF = isClaimedWTF(_index);
\t\tclaimedNFT = isClaimedNFT(_index);
\t\t( , , wethReserve, wtfReserve, , , ) = allInfoFor(address(0x0));
\t}

\tfunction allInfoFor(address _user) public view returns (uint256 totalTokens, uint256 totalLPTokens, uint256 wethReserve, uint256 wtfReserve, uint256 userBalance, uint256 userRewards, uint256 userLPBalance) {
\t\ttotalTokens = totalSupply();
\t\ttotalLPTokens = info.pair.totalSupply();
\t\t(uint256 _res0, uint256 _res1, ) = info.pair.getReserves();
\t\twethReserve = info.weth0 ? _res0 : _res1;
\t\twtfReserve = info.weth0 ? _res1 : _res0;
\t\tuserBalance = balanceOf(_user);
\t\tuserRewards = rewardsOf(_user);
\t\tuserLPBalance = info.pair.balanceOf(_user);
\t}


\tfunction _mint(address _account, uint256 _amount) internal {
\t\tinfo.totalSupply += _amount;
\t\tinfo.users[_account].balance += _amount;
\t\tinfo.users[_account].scaledPayout += int256(_amount * info.scaledRewardsPerToken);
\t\temit Transfer(address(0x0), _account, _amount);
\t}
\t
\tfunction _approve(address _owner, address _spender, uint256 _tokens) internal returns (bool) {
\t\tinfo.users[_owner].allowance[_spender] = _tokens;
\t\temit Approval(_owner, _spender, _tokens);
\t\treturn true;
\t}
\t
\tfunction _transfer(address _from, address _to, uint256 _tokens) internal returns (bool) {
\t\trequire(balanceOf(_from) \u003e= _tokens);
\t\tinfo.users[_from].balance -= _tokens;
\t\tinfo.users[_from].scaledPayout -= int256(_tokens * info.scaledRewardsPerToken);
\t\tuint256 _fee = 0;
\t\tif (!_isExcludedFromFee(_from, _to)) {
\t\t\t_fee = _tokens * transferFee() / TRANSFER_FEE_SCALE;
\t\t\taddress _this = address(this);
\t\t\tinfo.users[_this].balance += _fee;
\t\t\tinfo.users[_this].scaledPayout += int256(_fee * info.scaledRewardsPerToken);
\t\t\temit Transfer(_from, _this, _fee);
\t\t}
\t\tuint256 _transferred = _tokens - _fee;
\t\tinfo.users[_to].balance += _transferred;
\t\tinfo.users[_to].scaledPayout += int256(_transferred * info.scaledRewardsPerToken);
\t\temit Transfer(_from, _to, _transferred);
\t\tif (_fee \u003e 0) {
\t\t\tuint256 _feeManagerRewards = _fee * feeManagerPercent() / 100;
\t\t\tinfo.users[feeManagerAddress()].scaledPayout -= int256(_feeManagerRewards * FLOAT_SCALAR);
\t\t\t_disburse(_fee - _feeManagerRewards);
\t\t}
\t\treturn true;
\t}

\tfunction _disburse(uint256 _amount) internal {
\t\tif (_amount \u003e 0) {
\t\t\tinfo.scaledRewardsPerToken += _amount * FLOAT_SCALAR / totalSupply();
\t\t\temit Reward(_amount);
\t\t}
\t}


\tfunction _isExcludedFromFee(address _from, address _to) internal view returns (bool) {
\t\treturn isFromWhitelisted(_from) || isToWhitelisted(_to)
\t\t\t|| _from == address(this) || _to == address(this)
\t\t\t|| _from == feeManagerAddress() || _to == feeManagerAddress()
\t\t\t|| _from == treasuryAddress() || _to == treasuryAddress()
\t\t\t|| _from == stakingRewardsAddress() || _to == stakingRewardsAddress()
\t\t\t|| _from == lpStakingRewardsAddress() || _to == lpStakingRewardsAddress();
\t}
\t
\tfunction _verify(bytes32[] memory _proof, bytes32 _leaf) internal view returns (bool) {
\t\tbytes32 _computedHash = _leaf;
\t\tfor (uint256 i = 0; i \u003c _proof.length; i++) {
\t\t\tbytes32 _proofElement = _proof[i];
\t\t\tif (_computedHash \u003c= _proofElement) {
\t\t\t\t_computedHash = keccak256(abi.encodePacked(_computedHash, _proofElement));
\t\t\t} else {
\t\t\t\t_computedHash = keccak256(abi.encodePacked(_proofElement, _computedHash));
\t\t\t}
\t\t}
\t\treturn _computedHash == merkleRoot();
\t}
}"},"WTFNFT.sol":{"content":"// SPDX-License-Identifier: MIT
pragma solidity ^0.8.11;

import \"./Metadata.sol\";

interface Receiver {
\tfunction onERC721Received(address _operator, address _from, uint256 _tokenId, bytes calldata _data) external returns (bytes4);
}


contract WTFNFT {

\tstruct User {
\t\tuint256 balance;
\t\tmapping(uint256 =\u003e uint256) list;
\t\tmapping(address =\u003e bool) approved;
\t\tmapping(uint256 =\u003e uint256) indexOf;
\t\tuint256 tokenIndex;
\t}

\tstruct Token {
\t\taddress user;
\t\taddress owner;
\t\taddress approved;
\t\tuint128 totalFees;
\t\tuint128 failFees;
\t\tuint128 totalGas;
\t\tuint128 avgGwei;
\t\tuint128 totalDonated;
\t\tuint64 totalTxs;
\t\tuint64 failTxs;
\t}

\tstruct Info {
\t\tuint256 totalSupply;
\t\tmapping(uint256 =\u003e Token) list;
\t\tmapping(address =\u003e User) users;
\t\tMetadata metadata;
\t\taddress wtf;
\t\taddress owner;
\t}
\tInfo private info;

\tmapping(bytes4 =\u003e bool) public supportsInterface;

\tevent Transfer(address indexed from, address indexed to, uint256 indexed tokenId);
\tevent Approval(address indexed owner, address indexed approved, uint256 indexed tokenId);
\tevent ApprovalForAll(address indexed owner, address indexed operator, bool approved);
\tevent Mint(address indexed owner, uint256 indexed tokenId, uint256 totalFees, uint256 failFees, uint256 totalGas, uint256 avgGwei, uint256 totalDonated, uint256 totalTxs, uint256 failTxs);


\tmodifier _onlyOwner() {
\t\trequire(msg.sender == owner());
\t\t_;
\t}


\tconstructor() {
\t\tinfo.metadata = new Metadata(this);
\t\tinfo.wtf = msg.sender;
\t\tinfo.owner = 0xdEE79eD62B42e30EA7EbB6f1b7A3f04143D18b7F;
\t\tsupportsInterface[0x01ffc9a7] = true; // ERC-165
\t\tsupportsInterface[0x80ac58cd] = true; // ERC-721
\t\tsupportsInterface[0x5b5e139f] = true; // Metadata
\t\tsupportsInterface[0x780e9d63] = true; // Enumerable
\t}

\tfunction setOwner(address _owner) external _onlyOwner {
\t\tinfo.owner = _owner;
\t}

\tfunction setMetadata(Metadata _metadata) external _onlyOwner {
\t\tinfo.metadata = _metadata;
\t}

\t
\tfunction mint(address _receiver, uint256 _totalFees, uint256 _failFees, uint256 _totalGas, uint256 _avgGwei, uint256 _totalDonated, uint256 _totalTxs, uint256 _failTxs) public {
\t\trequire(msg.sender == wtfAddress());
\t\tuint256 _tokenId = info.totalSupply++;
\t\tinfo.users[_receiver].tokenIndex = totalSupply();
\t\tToken storage _newToken = info.list[_tokenId];
\t\t_newToken.user = _receiver;
\t\t_newToken.owner = _receiver;
\t\t_newToken.totalFees = uint128(_totalFees);
\t\t_newToken.failFees = uint128(_failFees);
\t\t_newToken.totalGas = uint128(_totalGas);
\t\t_newToken.avgGwei = uint128(_avgGwei);
\t\t_newToken.totalDonated = uint128(_totalDonated);
\t\t_newToken.totalTxs = uint64(_totalTxs);
\t\t_newToken.failTxs = uint64(_failTxs);
\t\tuint256 _index = info.users[_receiver].balance++;
\t\tinfo.users[_receiver].indexOf[_tokenId] = _index + 1;
\t\tinfo.users[_receiver].list[_index] = _tokenId;
\t\temit Transfer(address(0x0), _receiver, _tokenId);
\t\temit Mint(_receiver, _tokenId, _totalFees, _failFees, _totalGas, _avgGwei, _totalDonated, _totalTxs, _failTxs);
\t}
\t
\tfunction approve(address _approved, uint256 _tokenId) external {
\t\trequire(msg.sender == ownerOf(_tokenId));
\t\tinfo.list[_tokenId].approved = _approved;
\t\temit Approval(msg.sender, _approved, _tokenId);
\t}

\tfunction setApprovalForAll(address _operator, bool _approved) external {
\t\tinfo.users[msg.sender].approved[_operator] = _approved;
\t\temit ApprovalForAll(msg.sender, _operator, _approved);
\t}

\tfunction transferFrom(address _from, address _to, uint256 _tokenId) external {
\t\t_transfer(_from, _to, _tokenId);
\t}

\tfunction safeTransferFrom(address _from, address _to, uint256 _tokenId) external {
\t\tsafeTransferFrom(_from, _to, _tokenId, \"\");
\t}

\tfunction safeTransferFrom(address _from, address _to, uint256 _tokenId, bytes memory _data) public {
\t\t_transfer(_from, _to, _tokenId);
\t\tuint32 _size;
\t\tassembly {
\t\t\t_size := extcodesize(_to)
\t\t}
\t\tif (_size \u003e 0) {
\t\t\trequire(Receiver(_to).onERC721Received(msg.sender, _from, _tokenId, _data) == 0x150b7a02);
\t\t}
\t}


\tfunction name() external view returns (string memory) {
\t\treturn info.metadata.name();
\t}

\tfunction symbol() external view returns (string memory) {
\t\treturn info.metadata.symbol();
\t}

\tfunction tokenURI(uint256 _tokenId) external view returns (string memory) {
\t\treturn info.metadata.tokenURI(_tokenId);
\t}

\tfunction metadataAddress() public view returns (address) {
\t\treturn address(info.metadata);
\t}

\tfunction wtfAddress() public view returns (address) {
\t\treturn info.wtf;
\t}

\tfunction owner() public view returns (address) {
\t\treturn info.owner;
\t}

\tfunction totalSupply() public view returns (uint256) {
\t\treturn info.totalSupply;
\t}

\tfunction balanceOf(address _owner) public view returns (uint256) {
\t\treturn info.users[_owner].balance;
\t}

\tfunction ownerOf(uint256 _tokenId) public view returns (address) {
\t\trequire(_tokenId \u003c totalSupply());
\t\treturn info.list[_tokenId].owner;
\t}

\tfunction getUser(uint256 _tokenId) public view returns (address) {
\t\trequire(_tokenId \u003c totalSupply());
\t\treturn info.list[_tokenId].user;
\t}

\tfunction getApproved(uint256 _tokenId) public view returns (address) {
\t\trequire(_tokenId \u003c totalSupply());
\t\treturn info.list[_tokenId].approved;
\t}

\tfunction getTotalFees(uint256 _tokenId) public view returns (uint256) {
\t\trequire(_tokenId \u003c totalSupply());
\t\treturn info.list[_tokenId].totalFees;
\t}

\tfunction getFailFees(uint256 _tokenId) public view returns (uint256) {
\t\trequire(_tokenId \u003c totalSupply());
\t\treturn info.list[_tokenId].failFees;
\t}

\tfunction getTotalGas(uint256 _tokenId) public view returns (uint256) {
\t\trequire(_tokenId \u003c totalSupply());
\t\treturn info.list[_tokenId].totalGas;
\t}

\tfunction getAvgGwei(uint256 _tokenId) public view returns (uint256) {
\t\trequire(_tokenId \u003c totalSupply());
\t\treturn info.list[_tokenId].avgGwei;
\t}

\tfunction getTotalDonated(uint256 _tokenId) public view returns (uint256) {
\t\trequire(_tokenId \u003c totalSupply());
\t\treturn info.list[_tokenId].totalDonated;
\t}

\tfunction getTotalTxs(uint256 _tokenId) public view returns (uint256) {
\t\trequire(_tokenId \u003c totalSupply());
\t\treturn info.list[_tokenId].totalTxs;
\t}

\tfunction getFailTxs(uint256 _tokenId) public view returns (uint256) {
\t\trequire(_tokenId \u003c totalSupply());
\t\treturn info.list[_tokenId].failTxs;
\t}

\tfunction isApprovedForAll(address _owner, address _operator) public view returns (bool) {
\t\treturn info.users[_owner].approved[_operator];
\t}

\tfunction tokenIdOf(address _user) public view returns (uint256) {
\t\tuint256 _index = info.users[_user].tokenIndex;
\t\trequire(_index \u003e 0);
\t\treturn _index - 1;
\t}

\tfunction tokenByIndex(uint256 _index) public view returns (uint256) {
\t\trequire(_index \u003c totalSupply());
\t\treturn _index;
\t}

\tfunction tokenOfOwnerByIndex(address _owner, uint256 _index) public view returns (uint256) {
\t\trequire(_index \u003c balanceOf(_owner));
\t\treturn info.users[_owner].list[_index];
\t}

\tfunction getTokenCompressedInfo(uint256 _tokenId) public view returns (uint256[7] memory compressedInfo) {
\t\tcompressedInfo[0] = getTotalFees(_tokenId);
\t\tcompressedInfo[1] = getFailFees(_tokenId);
\t\tcompressedInfo[2] = getTotalGas(_tokenId);
\t\tcompressedInfo[3] = getAvgGwei(_tokenId);
\t\tcompressedInfo[4] = getTotalDonated(_tokenId);
\t\tcompressedInfo[5] = getTotalTxs(_tokenId);
\t\tcompressedInfo[6] = getFailTxs(_tokenId);
\t}

\tfunction getToken(uint256 _tokenId) public view returns (address tokenOwner, address approved, address user, uint256[7] memory compressedInfo) {
\t\treturn (ownerOf(_tokenId), getApproved(_tokenId), getUser(_tokenId), getTokenCompressedInfo(_tokenId));
\t}

\tfunction getTokens(uint256[] memory _tokenIds) public view returns (address[] memory owners, address[] memory approveds, address[] memory users, uint256[7][] memory compressedInfos) {
\t\tuint256 _length = _tokenIds.length;
\t\towners = new address[](_length);
\t\tapproveds = new address[](_length);
\t\tusers = new address[](_length);
\t\tcompressedInfos = new uint256[7][](_length);
\t\tfor (uint256 i = 0; i \u003c _length; i++) {
\t\t\t(owners[i], approveds[i], users[i], compressedInfos[i]) = getToken(_tokenIds[i]);
\t\t}
\t}

\tfunction getTokensTable(uint256 _limit, uint256 _page, bool _isAsc) public view returns (uint256[] memory tokenIds, address[] memory owners, address[] memory approveds, address[] memory users, uint256[7][] memory compressedInfos, uint256 totalTokens, uint256 totalPages) {
\t\trequire(_limit \u003e 0);
\t\ttotalTokens = totalSupply();

\t\tif (totalTokens \u003e 0) {
\t\t\ttotalPages = (totalTokens / _limit) + (totalTokens % _limit == 0 ? 0 : 1);
\t\t\trequire(_page \u003c totalPages);

\t\t\tuint256 _offset = _limit * _page;
\t\t\tif (_page == totalPages - 1 \u0026\u0026 totalTokens % _limit != 0) {
\t\t\t\t_limit = totalTokens % _limit;
\t\t\t}

\t\t\ttokenIds = new uint256[](_limit);
\t\t\tfor (uint256 i = 0; i \u003c _limit; i++) {
\t\t\t\ttokenIds[i] = tokenByIndex(_isAsc ? _offset + i : totalTokens - _offset - i - 1);
\t\t\t}
\t\t} else {
\t\t\ttotalPages = 0;
\t\t\ttokenIds = new uint256[](0);
\t\t}
\t\t(owners, approveds, users, compressedInfos) = getTokens(tokenIds);
\t}

\tfunction getOwnerTokensTable(address _owner, uint256 _limit, uint256 _page, bool _isAsc) public view returns (uint256[] memory tokenIds, address[] memory approveds, address[] memory users, uint256[7][] memory compressedInfos, uint256 totalTokens, uint256 totalPages) {
\t\trequire(_limit \u003e 0);
\t\ttotalTokens = balanceOf(_owner);

\t\tif (totalTokens \u003e 0) {
\t\t\ttotalPages = (totalTokens / _limit) + (totalTokens % _limit == 0 ? 0 : 1);
\t\t\trequire(_page \u003c totalPages);

\t\t\tuint256 _offset = _limit * _page;
\t\t\tif (_page == totalPages - 1 \u0026\u0026 totalTokens % _limit != 0) {
\t\t\t\t_limit = totalTokens % _limit;
\t\t\t}

\t\t\ttokenIds = new uint256[](_limit);
\t\t\tfor (uint256 i = 0; i \u003c _limit; i++) {
\t\t\t\ttokenIds[i] = tokenOfOwnerByIndex(_owner, _isAsc ? _offset + i : totalTokens - _offset - i - 1);
\t\t\t}
\t\t} else {
\t\t\ttotalPages = 0;
\t\t\ttokenIds = new uint256[](0);
\t\t}
\t\t( , approveds, users, compressedInfos) = getTokens(tokenIds);
\t}

\tfunction allInfoFor(address _owner) external view returns (uint256 supply, uint256 ownerBalance) {
\t\treturn (totalSupply(), balanceOf(_owner));
\t}

\t
\tfunction _transfer(address _from, address _to, uint256 _tokenId) internal {
\t\taddress _owner = ownerOf(_tokenId);
\t\taddress _approved = getApproved(_tokenId);
\t\trequire(_from == _owner);
\t\trequire(msg.sender == _owner || msg.sender == _approved || isApprovedForAll(_owner, msg.sender));

\t\tinfo.list[_tokenId].owner = _to;
\t\tif (_approved != address(0x0)) {
\t\t\tinfo.list[_tokenId].approved = address(0x0);
\t\t\temit Approval(address(0x0), address(0x0), _tokenId);
\t\t}

\t\tuint256 _index = info.users[_from].indexOf[_tokenId] - 1;
\t\tuint256 _moved = info.users[_from].list[info.users[_from].balance - 1];
\t\tinfo.users[_from].list[_index] = _moved;
\t\tinfo.users[_from].indexOf[_moved] = _index + 1;
\t\tinfo.users[_from].balance--;
\t\tdelete info.users[_from].indexOf[_tokenId];
\t\tuint256 _newIndex = info.users[_to].balance++;
\t\tinfo.users[_to].indexOf[_tokenId] = _newIndex + 1;
\t\tinfo.users[_to].list[_newIndex] = _tokenId;
\t\temit Transfer(_from, _to, _tokenId);
\t}
}

