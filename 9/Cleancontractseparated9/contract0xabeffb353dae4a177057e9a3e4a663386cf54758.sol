// SPDX-License-Identifier: MIT
pragma solidity 0.8.7;

import \"../interfaces/ERC20Spec.sol\";
import \"../interfaces/ERC721Spec.sol\";
import \"../utils/AccessControl.sol\";

/**
 * @title NFT Staking
 *
 * @notice Enables NFT staking for a given NFT smart contract defined on deployment
 *
 * @notice Doesn't introduce any rewards, just tracks the stake/unstake dates for each
 *      token/owner, this data will be used later on to process the rewards
 */
contract NFTStaking is AccessControl {
\t/**
\t * @dev Main staking data structure keeping track of a stake,
\t *      used in `tokenStakes` array mapping
\t */
\tstruct StakeData {
\t\t/**
\t\t * @dev Who owned and staked the token, who will be the token
\t\t *      returned to once unstaked
\t\t */
\t\taddress owner;

\t\t/**
\t\t * @dev When the token was staked and transferred from the owner,
\t\t *      unix timestamp
\t\t */
\t\tuint32 stakedOn;

\t\t/**
\t\t * @dev When token was unstaked and returned back to the owner,
\t\t *      unix timestamp
\t\t * @dev Zero value means the token is still staked
\t\t */
\t\tuint32 unstakedOn;
\t}

\t/**
\t * @dev Auxiliary data structure to help iterate over NFT owner stakes,
\t *      used in `userStakes` array mapping
\t */
\tstruct StakeIndex {
\t\t/**
\t\t * @dev Staked token ID
\t\t */
\t\tuint32 tokenId;

\t\t/**
\t\t * @dev Where to look for main staking data `StakeData`
\t\t *      in `tokenStakes` array mapping
\t\t */
\t\tuint32 index;
\t}

\t/**
\t * @dev NFT smart contract to stake/unstake tokens of
\t */
\taddress public immutable targetContract;

\t/**
\t * @notice For each token ID stores the history of its stakes,
\t *      last element of the history may be \"open\" (unstakedOn = 0),
\t *      meaning the token is still staked and is ot be returned to the `owner`
\t *
\t * @dev Maps token ID => StakeData[]
\t */
\tmapping(uint32 => StakeData[]) public tokenStakes;

\t/**
\t * @notice For each owner address stores the links to its stakes,
\t *      the link is represented as StakeIndex data struct
\t *
\t * @dev Maps owner address => StakeIndex[]
\t */
\tmapping(address => StakeIndex[]) public userStakes;

\t/**
\t * @dev Enables staking, stake(), stakeBatch()
\t */
\tuint32 public constant FEATURE_STAKING = 0x0000_0001;

\t/**
\t * @dev Enables unstaking, unstake(), unstakeBatch()
\t */
\tuint32 public constant FEATURE_UNSTAKING = 0x0000_0002;

\t/**
\t * @notice People do mistake and may send tokens by mistake; since
\t *      staking contract is not designed to accept the tokens directly,
\t *      it allows the rescue manager to \"rescue\" such lost tokens
\t *
\t * @notice Rescue manager is responsible for \"rescuing\" ERC20/ERC721 tokens
\t *      accidentally sent to the smart contract
\t *
\t * @dev Role ROLE_RESCUE_MANAGER allows withdrawing non-staked ERC20/ERC721
\t *      tokens stored on the smart contract balance
\t */
\tuint32 public constant ROLE_RESCUE_MANAGER = 0x0001_0000;

\t/**
\t * @dev Fired in stake(), stakeBatch()
\t *
\t * @param _by token owner, tx executor
\t * @param _tokenId token ID staked and transferred into the smart contract
\t * @param _when unix timestamp of when staking happened
\t */
\tevent Staked(address indexed _by, uint32 indexed _tokenId, uint32 _when);

\t/**
\t * @dev Fired in unstake(), unstakeBatch()
\t *
\t * @param _by token owner, tx executor
\t * @param _tokenId token ID unstaked and transferred back to owner
\t * @param _when unix timestamp of when unstaking happened
\t */
\tevent Unstaked(address indexed _by, uint32 indexed _tokenId, uint32 _when);

\t/**
\t * @dev Creates/deploys NFT staking contract bound to the already deployed
\t *      target NFT ERC721 smart contract to be staked
\t *
\t * @param _nft address of the deployed NFT smart contract instance
\t */
\tconstructor(address _nft) {
\t\t// verify input is set
\t\trequire(_nft != address(0), \"target contract is not set\");

\t\t// verify input is valid smart contract of the expected interface
\t\trequire(ERC165(_nft).supportsInterface(type(ERC721).interfaceId), \"unexpected target type\");

\t\t// setup smart contract internal state
\t\ttargetContract = _nft;
\t}

\t/**
\t * @notice How many times a particular token was staked
\t *
\t * @dev Used to iterate `tokenStakes(tokenId, i)`, `i < numStakes(tokenId)`
\t *
\t * @param tokenId token ID to query number of times staked for
\t * @return number of times token was staked
\t */
\tfunction numStakes(uint32 tokenId) public view returns(uint256) {
\t\t// just read the array length and return it
\t\treturn tokenStakes[tokenId].length;
\t}

\t/**
\t * @notice How many stakes a particular address has done
\t *
\t * @dev Used to iterate `userStakes(owner, i)`, `i < numStakes(owner)`
\t *
\t * @param owner an address to query number of times it staked
\t * @return number of times a particular address has staked
\t */
\tfunction numStakes(address owner) public view returns(uint256) {
\t\t// just read the array length and return it
\t\treturn userStakes[owner].length;
\t}

\t/**
\t * @notice Determines if the token is currently staked or not
\t *
\t * @param tokenId token ID to check state for
\t * @return true if token is staked, false otherwise
\t */
\tfunction isStaked(uint32 tokenId) public view returns(bool) {
\t\t// get an idea of current stakes for the token
\t\tuint256 n = tokenStakes[tokenId].length;

\t\t// evaluate based on the last stake element in the array
\t\treturn n > 0 && tokenStakes[tokenId][n - 1].unstakedOn == 0;
\t}

\t/**
\t * @notice Stakes the NFT; the token is transferred from its owner to the staking contract;
\t *      token must be owned by the tx executor and be transferable by staking contract
\t *
\t * @param tokenId token ID to stake
\t */
\tfunction stake(uint32 tokenId) public {
\t\t// verify staking is enabled
\t\trequire(isFeatureEnabled(FEATURE_STAKING), \"staking is disabled\");

\t\t// get an idea of current stakes for the token
\t\tuint256 n = tokenStakes[tokenId].length;

\t\t// verify the token is not currently staked
\t\trequire(n == 0 || tokenStakes[tokenId][n - 1].unstakedOn != 0, \"already staked\");

\t\t// verify token belongs to the address which executes staking
\t\trequire(ERC721(targetContract).ownerOf(tokenId) == msg.sender, \"access denied\");

\t\t// transfer the token from owner into the staking contract
\t\tERC721(targetContract).transferFrom(msg.sender, address(this), tokenId);

\t\t// current timestamp to be set as `stakedOn`
\t\tuint32 stakedOn = now32();

\t\t// save token stake data
\t\ttokenStakes[tokenId].push(StakeData({
\t\t\towner: msg.sender,
\t\t\tstakedOn: stakedOn,
\t\t\tunstakedOn: 0
\t\t}));

\t\t// save token stake index
\t\tuserStakes[msg.sender].push(StakeIndex({
\t\t\ttokenId: tokenId,
\t\t\tindex: uint32(n)
\t\t}));

\t\t// emit an event
\t\temit Staked(msg.sender, tokenId, stakedOn);
\t}

\t/**
\t * @notice Stakes several NFTs; tokens are transferred from their owner to the staking contract;
\t *      tokens must be owned by the tx executor and be transferable by staking contract
\t *
\t * @param tokenIds token IDs to stake
\t */
\tfunction stakeBatch(uint32[] memory tokenIds) public {
\t\t// iterate the collection passed
\t\tfor(uint256 i = 0; i < tokenIds.length; i++) {
\t\t\t// and stake each token one by one
\t\t\tstake(tokenIds[i]);
\t\t}
\t}

\t/**
\t * @notice Unstakes the NFT; the token is transferred from staking contract back
\t *      its previous owner
\t *
\t * @param tokenId token ID to unstake
\t */
\tfunction unstake(uint32 tokenId) public {
\t\t// verify staking is enabled
\t\trequire(isFeatureEnabled(FEATURE_UNSTAKING), \"unstaking is disabled\");

\t\t// get an idea of current stakes for the token
\t\tuint256 n = tokenStakes[tokenId].length;

\t\t// verify the token is not currently staked
\t\trequire(n != 0, \"not staked\");
\t\trequire(tokenStakes[tokenId][n - 1].unstakedOn == 0, \"already unstaked\");

\t\t// verify token belongs to the address which executes unstaking
\t\trequire(tokenStakes[tokenId][n - 1].owner == msg.sender, \"access denied\");

\t\t// current timestamp to be set as `unstakedOn`
\t\tuint32 unstakedOn = now32();

\t\t// update token stake data
\t\ttokenStakes[tokenId][n - 1].unstakedOn = unstakedOn;

\t\t// transfer the token back to owner
\t\tERC721(targetContract).transferFrom(address(this), msg.sender, tokenId);

\t\t// emit an event
\t\temit Unstaked(msg.sender, tokenId, unstakedOn);
\t}

\t/**
\t * @notice Unstakes several NFTs; tokens are transferred from staking contract back
\t *      their previous owner
\t *
\t * @param tokenIds token IDs to unstake
\t */
\tfunction unstakeBatch(uint32[] memory tokenIds) public {
\t\t// iterate the collection passed
\t\tfor(uint256 i = 0; i < tokenIds.length; i++) {
\t\t\t// and unstake each token one by one
\t\t\tunstake(tokenIds[i]);
\t\t}
\t}

\t/**
\t * @dev Restricted access function to rescue accidentally sent ERC20 tokens,
\t *      the tokens are rescued via `transfer` function call on the
\t *      contract address specified and with the parameters specified:
\t *      `_contract.transfer(_to, _value)`
\t *
\t * @dev Requires executor to have `ROLE_RESCUE_MANAGER` permission
\t *
\t * @param _contract smart contract address to execute `transfer` function on
\t * @param _to to address in `transfer(_to, _value)`
\t * @param _value value to transfer in `transfer(_to, _value)`
\t */
\tfunction rescueErc20(address _contract, address _to, uint256 _value) public {
\t\t// verify the access permission
\t\trequire(isSenderInRole(ROLE_RESCUE_MANAGER), \"access denied\");

\t\t// perform the transfer as requested, without any checks
\t\tERC20(_contract).transfer(_to, _value);
\t}

\t/**
\t * @dev Restricted access function to rescue accidentally sent ERC721 tokens,
\t *      the tokens are rescued via `transferFrom` function call on the
\t *      contract address specified and with the parameters specified:
\t *      `_contract.transferFrom(this, _to, _tokenId)`
\t *
\t * @dev Requires executor to have `ROLE_RESCUE_MANAGER` permission
\t *
\t * @param _contract smart contract address to execute `transferFrom` function on
\t * @param _to to address in `transferFrom(this, _to, _tokenId)`
\t * @param _tokenId token ID to transfer in `transferFrom(this, _to, _tokenId)`
\t */
\tfunction rescueErc721(address _contract, address _to, uint256 _tokenId) public {
\t\t// verify the access permission
\t\trequire(isSenderInRole(ROLE_RESCUE_MANAGER), \"access denied\");

\t\t// verify the NFT is not staked
\t\trequire(_contract != targetContract || !isStaked(uint32(_tokenId)), \"token is staked\");

\t\t// perform the transfer as requested, without any checks
\t\tERC721(_contract).transferFrom(address(this), _to, _tokenId);
\t}

\t/**
\t * @dev Testing time-dependent functionality may be difficult;
\t *      we override time in the helper test smart contract (mock)
\t *
\t * @return `block.timestamp` in mainnet, custom values in testnets (if overridden)
\t */
\tfunction now32() public view virtual returns (uint32) {
\t\t// return current block timestamp
\t\treturn uint32(block.timestamp);
\t}
}
"
    
// SPDX-License-Identifier: MIT
pragma solidity 0.8.7;

/**
 * @title EIP-20: ERC-20 Token Standard
 *
 * @notice The ERC-20 (Ethereum Request for Comments 20), proposed by Fabian Vogelsteller in November 2015,
 *      is a Token Standard that implements an API for tokens within Smart Contracts.
 *
 * @notice It provides functionalities like to transfer tokens from one account to another,
 *      to get the current token balance of an account and also the total supply of the token available on the network.
 *      Besides these it also has some other functionalities like to approve that an amount of
 *      token from an account can be spent by a third party account.
 *
 * @notice If a Smart Contract implements the following methods and events it can be called an ERC-20 Token
 *      Contract and, once deployed, it will be responsible to keep track of the created tokens on Ethereum.
 *
 * @notice See https://ethereum.org/en/developers/docs/standards/tokens/erc-20/
 * @notice See https://eips.ethereum.org/EIPS/eip-20
 */
interface ERC20 {
\t/**
\t * @dev Fired in transfer(), transferFrom() to indicate that token transfer happened
\t *
\t * @param from an address tokens were consumed from
\t * @param to an address tokens were sent to
\t * @param value number of tokens transferred
\t */
\tevent Transfer(address indexed from, address indexed to, uint256 value);

\t/**
\t * @dev Fired in approve() to indicate an approval event happened
\t *
\t * @param owner an address which granted a permission to transfer
\t *      tokens on its behalf
\t * @param spender an address which received a permission to transfer
\t *      tokens on behalf of the owner `_owner`
\t * @param value amount of tokens granted to transfer on behalf
\t */
\tevent Approval(address indexed owner, address indexed spender, uint256 value);

\t/**
\t * @return name of the token (ex.: USD Coin)
\t */
\t// OPTIONAL - This method can be used to improve usability,
\t// but interfaces and other contracts MUST NOT expect these values to be present.
\t// function name() external view returns (string memory);

\t/**
\t * @return symbol of the token (ex.: USDC)
\t */
\t// OPTIONAL - This method can be used to improve usability,
\t// but interfaces and other contracts MUST NOT expect these values to be present.
\t// function symbol() external view returns (string memory);

\t/**
\t * @dev Returns the number of decimals used to get its user representation.
\t *      For example, if `decimals` equals `2`, a balance of `505` tokens should
\t *      be displayed to a user as `5,05` (`505 / 10 ** 2`).
\t *
\t * @dev Tokens usually opt for a value of 18, imitating the relationship between
\t *      Ether and Wei. This is the value {ERC20} uses, unless this function is
\t *      overridden;
\t *
\t * @dev NOTE: This information is only used for _display_ purposes: it in
\t *      no way affects any of the arithmetic of the contract, including
\t *      {IERC20-balanceOf} and {IERC20-transfer}.
\t *
\t * @return token decimals
\t */
\t// OPTIONAL - This method can be used to improve usability,
\t// but interfaces and other contracts MUST NOT expect these values to be present.
\t// function decimals() external view returns (uint8);

\t/**
\t * @return the amount of tokens in existence
\t */
\tfunction totalSupply() external view returns (uint256);

\t/**
\t * @notice Gets the balance of a particular address
\t *
\t * @param _owner the address to query the the balance for
\t * @return balance an amount of tokens owned by the address specified
\t */
\tfunction balanceOf(address _owner) external view returns (uint256 balance);

\t/**
\t * @notice Transfers some tokens to an external address or a smart contract
\t *
\t * @dev Called by token owner (an address which has a
\t *      positive token balance tracked by this smart contract)
\t * @dev Throws on any error like
\t *      * insufficient token balance or
\t *      * incorrect `_to` address:
\t *          * zero address or
\t *          * self address or
\t *          * smart contract which doesn't support ERC20
\t *
\t * @param _to an address to transfer tokens to,
\t *      must be either an external address or a smart contract,
\t *      compliant with the ERC20 standard
\t * @param _value amount of tokens to be transferred,, zero
\t *      value is allowed
\t * @return success true on success, throws otherwise
\t */
\tfunction transfer(address _to, uint256 _value) external returns (bool success);

\t/**
\t * @notice Transfers some tokens on behalf of address `_from' (token owner)
\t *      to some other address `_to`
\t *
\t * @dev Called by token owner on his own or approved address,
\t *      an address approved earlier by token owner to
\t *      transfer some amount of tokens on its behalf
\t * @dev Throws on any error like
\t *      * insufficient token balance or
\t *      * incorrect `_to` address:
\t *          * zero address or
\t *          * same as `_from` address (self transfer)
\t *          * smart contract which doesn't support ERC20
\t *
\t * @param _from token owner which approved caller (transaction sender)
\t *      to transfer `_value` of tokens on its behalf
\t * @param _to an address to transfer tokens to,
\t *      must be either an external address or a smart contract,
\t *      compliant with the ERC20 standard
\t * @param _value amount of tokens to be transferred,, zero
\t *      value is allowed
\t * @return success true on success, throws otherwise
\t */
\tfunction transferFrom(address _from, address _to, uint256 _value) external returns (bool success);

\t/**
\t * @notice Approves address called `_spender` to transfer some amount
\t *      of tokens on behalf of the owner (transaction sender)
\t *
\t * @dev Transaction sender must not necessarily own any tokens to grant the permission
\t *
\t * @param _spender an address approved by the caller (token owner)
\t *      to spend some tokens on its behalf
\t * @param _value an amount of tokens spender `_spender` is allowed to
\t *      transfer on behalf of the token owner
\t * @return success true on success, throws otherwise
\t */
\tfunction approve(address _spender, uint256 _value) external returns (bool success);

\t/**
\t * @notice Returns the amount which _spender is still allowed to withdraw from _owner.
\t *
\t * @dev A function to check an amount of tokens owner approved
\t *      to transfer on its behalf by some other address called \"spender\"
\t *
\t * @param _owner an address which approves transferring some tokens on its behalf
\t * @param _spender an address approved to transfer some tokens on behalf
\t * @return remaining an amount of tokens approved address `_spender` can transfer on behalf
\t *      of token owner `_owner`
\t */
\tfunction allowance(address _owner, address _spender) external view returns (uint256 remaining);
}
"
    
// SPDX-License-Identifier: MIT
pragma solidity 0.8.7;

import \"./ERC165Spec.sol\";

/**
 * @title ERC-721 Non-Fungible Token Standard
 *
 * @notice See https://eips.ethereum.org/EIPS/eip-721
 *
 * @dev Solidity issue #3412: The ERC721 interfaces include explicit mutability guarantees for each function.
 *      Mutability guarantees are, in order weak to strong: payable, implicit nonpayable, view, and pure.
 *      Implementation MUST meet the mutability guarantee in this interface and MAY meet a stronger guarantee.
 *      For example, a payable function in this interface may be implemented as nonpayable
 *      (no state mutability specified) in implementing contract.
 *      It is expected a later Solidity release will allow stricter contract to inherit from this interface,
 *      but current workaround is that we edit this interface to add stricter mutability before inheriting:
 *      we have removed all \"payable\" modifiers.
 *
 * @dev The ERC-165 identifier for this interface is 0x80ac58cd.
 *
 * @author William Entriken, Dieter Shirley, Jacob Evans, Nastassia Sachs
 */
interface ERC721 is ERC165 {
\t/// @dev This emits when ownership of any NFT changes by any mechanism.
\t///  This event emits when NFTs are created (`from` == 0) and destroyed
\t///  (`to` == 0). Exception: during contract creation, any number of NFTs
\t///  may be created and assigned without emitting Transfer. At the time of
\t///  any transfer, the approved address for that NFT (if any) is reset to none.
\tevent Transfer(address indexed _from, address indexed _to, uint256 indexed _tokenId);

\t/// @dev This emits when the approved address for an NFT is changed or
\t///  reaffirmed. The zero address indicates there is no approved address.
\t///  When a Transfer event emits, this also indicates that the approved
\t///  address for that NFT (if any) is reset to none.
\tevent Approval(address indexed _owner, address indexed _approved, uint256 indexed _tokenId);

\t/// @dev This emits when an operator is enabled or disabled for an owner.
\t///  The operator can manage all NFTs of the owner.
\tevent ApprovalForAll(address indexed _owner, address indexed _operator, bool _approved);

\t/// @notice Count all NFTs assigned to an owner
\t/// @dev NFTs assigned to the zero address are considered invalid, and this
\t///  function throws for queries about the zero address.
\t/// @param _owner An address for whom to query the balance
\t/// @return The number of NFTs owned by `_owner`, possibly zero
\tfunction balanceOf(address _owner) external view returns (uint256);

\t/// @notice Find the owner of an NFT
\t/// @dev NFTs assigned to zero address are considered invalid, and queries
\t///  about them do throw.
\t/// @param _tokenId The identifier for an NFT
\t/// @return The address of the owner of the NFT
\tfunction ownerOf(uint256 _tokenId) external view returns (address);

\t/// @notice Transfers the ownership of an NFT from one address to another address
\t/// @dev Throws unless `msg.sender` is the current owner, an authorized
\t///  operator, or the approved address for this NFT. Throws if `_from` is
\t///  not the current owner. Throws if `_to` is the zero address. Throws if
\t///  `_tokenId` is not a valid NFT. When transfer is complete, this function
\t///  checks if `_to` is a smart contract (code size > 0). If so, it calls
\t///  `onERC721Received` on `_to` and throws if the return value is not
\t///  `bytes4(keccak256(\"onERC721Received(address,address,uint256,bytes)\"))`.
\t/// @param _from The current owner of the NFT
\t/// @param _to The new owner
\t/// @param _tokenId The NFT to transfer
\t/// @param _data Additional data with no specified format, sent in call to `_to`
\tfunction safeTransferFrom(address _from, address _to, uint256 _tokenId, bytes calldata _data) external /*payable*/;

\t/// @notice Transfers the ownership of an NFT from one address to another address
\t/// @dev This works identically to the other function with an extra data parameter,
\t///  except this function just sets data to \"\".
\t/// @param _from The current owner of the NFT
\t/// @param _to The new owner
\t/// @param _tokenId The NFT to transfer
\tfunction safeTransferFrom(address _from, address _to, uint256 _tokenId) external /*payable*/;

\t/// @notice Transfer ownership of an NFT -- THE CALLER IS RESPONSIBLE
\t///  TO CONFIRM THAT `_to` IS CAPABLE OF RECEIVING NFTS OR ELSE
\t///  THEY MAY BE PERMANENTLY LOST
\t/// @dev Throws unless `msg.sender` is the current owner, an authorized
\t///  operator, or the approved address for this NFT. Throws if `_from` is
\t///  not the current owner. Throws if `_to` is the zero address. Throws if
\t///  `_tokenId` is not a valid NFT.
\t/// @param _from The current owner of the NFT
\t/// @param _to The new owner
\t/// @param _tokenId The NFT to transfer
\tfunction transferFrom(address _from, address _to, uint256 _tokenId) external /*payable*/;

\t/// @notice Change or reaffirm the approved address for an NFT
\t/// @dev The zero address indicates there is no approved address.
\t///  Throws unless `msg.sender` is the current NFT owner, or an authorized
\t///  operator of the current owner.
\t/// @param _approved The new approved NFT controller
\t/// @param _tokenId The NFT to approve
\tfunction approve(address _approved, uint256 _tokenId) external /*payable*/;

\t/// @notice Enable or disable approval for a third party (\"operator\") to manage
\t///  all of `msg.sender`'s assets
\t/// @dev Emits the ApprovalForAll event. The contract MUST allow
\t///  multiple operators per owner.
\t/// @param _operator Address to add to the set of authorized operators
\t/// @param _approved True if the operator is approved, false to revoke approval
\tfunction setApprovalForAll(address _operator, bool _approved) external;

\t/// @notice Get the approved address for a single NFT
\t/// @dev Throws if `_tokenId` is not a valid NFT.
\t/// @param _tokenId The NFT to find the approved address for
\t/// @return The approved address for this NFT, or the zero address if there is none
\tfunction getApproved(uint256 _tokenId) external view returns (address);

\t/// @notice Query if an address is an authorized operator for another address
\t/// @param _owner The address that owns the NFTs
\t/// @param _operator The address that acts on behalf of the owner
\t/// @return True if `_operator` is an approved operator for `_owner`, false otherwise
\tfunction isApprovedForAll(address _owner, address _operator) external view returns (bool);
}

/// @dev Note: the ERC-165 identifier for this interface is 0x150b7a02.
interface ERC721TokenReceiver {
\t/// @notice Handle the receipt of an NFT
\t/// @dev The ERC721 smart contract calls this function on the recipient
\t///  after a `transfer`. This function MAY throw to revert and reject the
\t///  transfer. Return of other than the magic value MUST result in the
\t///  transaction being reverted.
\t///  Note: the contract address is always the message sender.
\t/// @param _operator The address which called `safeTransferFrom` function
\t/// @param _from The address which previously owned the token
\t/// @param _tokenId The NFT identifier which is being transferred
\t/// @param _data Additional data with no specified format
\t/// @return `bytes4(keccak256(\"onERC721Received(address,address,uint256,bytes)\"))`
\t///  unless throwing
\tfunction onERC721Received(address _operator, address _from, uint256 _tokenId, bytes calldata _data) external returns(bytes4);
}

/**
 * @title ERC-721 Non-Fungible Token Standard, optional metadata extension
 *
 * @notice See https://eips.ethereum.org/EIPS/eip-721
 *
 * @dev The ERC-165 identifier for this interface is 0x5b5e139f.
 *
 * @author William Entriken, Dieter Shirley, Jacob Evans, Nastassia Sachs
 */
interface ERC721Metadata is ERC721 {
\t/// @notice A descriptive name for a collection of NFTs in this contract
\tfunction name() external view returns (string memory _name);

\t/// @notice An abbreviated name for NFTs in this contract
\tfunction symbol() external view returns (string memory _symbol);

\t/// @notice A distinct Uniform Resource Identifier (URI) for a given asset.
\t/// @dev Throws if `_tokenId` is not a valid NFT. URIs are defined in RFC
\t///  3986. The URI may point to a JSON file that conforms to the \"ERC721
\t///  Metadata JSON Schema\".
\tfunction tokenURI(uint256 _tokenId) external view returns (string memory);
}

/**
 * @title ERC-721 Non-Fungible Token Standard, optional enumeration extension
 *
 * @notice See https://eips.ethereum.org/EIPS/eip-721
 *
 * @dev The ERC-165 identifier for this interface is 0x780e9d63.
 *
 * @author William Entriken, Dieter Shirley, Jacob Evans, Nastassia Sachs
 */
interface ERC721Enumerable is ERC721 {
\t/// @notice Count NFTs tracked by this contract
\t/// @return A count of valid NFTs tracked by this contract, where each one of
\t///  them has an assigned and queryable owner not equal to the zero address
\tfunction totalSupply() external view returns (uint256);

\t/// @notice Enumerate valid NFTs
\t/// @dev Throws if `_index` >= `totalSupply()`.
\t/// @param _index A counter less than `totalSupply()`
\t/// @return The token identifier for the `_index`th NFT,
\t///  (sort order not specified)
\tfunction tokenByIndex(uint256 _index) external view returns (uint256);

\t/// @notice Enumerate NFTs assigned to an owner
\t/// @dev Throws if `_index` >= `balanceOf(_owner)` or if
\t///  `_owner` is the zero address, representing invalid NFTs.
\t/// @param _owner An address where we are interested in NFTs owned by them
\t/// @param _index A counter less than `balanceOf(_owner)`
\t/// @return The token identifier for the `_index`th NFT assigned to `_owner`,
\t///   (sort order not specified)
\tfunction tokenOfOwnerByIndex(address _owner, uint256 _index) external view returns (uint256);
}
"
    
// SPDX-License-Identifier: MIT
pragma solidity 0.8.7;

/**
 * @title Access Control List
 *
 * @notice Access control smart contract provides an API to check
 *      if specific operation is permitted globally and/or
 *      if particular user has a permission to execute it.
 *
 * @notice It deals with two main entities: features and roles.
 *
 * @notice Features are designed to be used to enable/disable specific
 *      functions (public functions) of the smart contract for everyone.
 * @notice User roles are designed to restrict access to specific
 *      functions (restricted functions) of the smart contract to some users.
 *
 * @notice Terms \"role\", \"permissions\" and \"set of permissions\" have equal meaning
 *      in the documentation text and may be used interchangeably.
 * @notice Terms \"permission\", \"single permission\" implies only one permission bit set.
 *
 * @notice Access manager is a special role which allows to grant/revoke other roles.
 *      Access managers can only grant/revoke permissions which they have themselves.
 *      As an example, access manager with no other roles set can only grant/revoke its own
 *      access manager permission and nothing else.
 *
 * @notice Access manager permission should be treated carefully, as a super admin permission:
 *      Access manager with even no other permission can interfere with another account by
 *      granting own access manager permission to it and effectively creating more powerful
 *      permission set than its own.
 *
 * @dev Both current and OpenZeppelin AccessControl implementations feature a similar API
 *      to check/know \"who is allowed to do this thing\".
 * @dev Zeppelin implementation is more flexible:
 *      - it allows setting unlimited number of roles, while current is limited to 256 different roles
 *      - it allows setting an admin for each role, while current allows having only one global admin
 * @dev Current implementation is more lightweight:
 *      - it uses only 1 bit per role, while Zeppelin uses 256 bits
 *      - it allows setting up to 256 roles at once, in a single transaction, while Zeppelin allows
 *        setting only one role in a single transaction
 *
 * @dev This smart contract is designed to be inherited by other
 *      smart contracts which require access control management capabilities.
 *
 * @dev Access manager permission has a bit 255 set.
 *      This bit must not be used by inheriting contracts for any other permissions/features.
 */
contract AccessControl {
\t/**
\t * @notice Access manager is responsible for assigning the roles to users,
\t *      enabling/disabling global features of the smart contract
\t * @notice Access manager can add, remove and update user roles,
\t *      remove and update global features
\t *
\t * @dev Role ROLE_ACCESS_MANAGER allows modifying user roles and global features
\t * @dev Role ROLE_ACCESS_MANAGER has single bit at position 255 enabled
\t */
\tuint256 public constant ROLE_ACCESS_MANAGER = 0x8000000000000000000000000000000000000000000000000000000000000000;

\t/**
\t * @dev Bitmask representing all the possible permissions (super admin role)
\t * @dev Has all the bits are enabled (2^256 - 1 value)
\t */
\tuint256 private constant FULL_PRIVILEGES_MASK = type(uint256).max; // before 0.8.0: uint256(-1) overflows to 0xFFFF...

\t/**
\t * @notice Privileged addresses with defined roles/permissions
\t * @notice In the context of ERC20/ERC721 tokens these can be permissions to
\t *      allow minting or burning tokens, transferring on behalf and so on
\t *
\t * @dev Maps user address to the permissions bitmask (role), where each bit
\t *      represents a permission
\t * @dev Bitmask 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
\t *      represents all possible permissions
\t * @dev 'This' address mapping represents global features of the smart contract
\t */
\tmapping(address => uint256) public userRoles;

\t/**
\t * @dev Fired in updateRole() and updateFeatures()
\t *
\t * @param _by operator which called the function
\t * @param _to address which was granted/revoked permissions
\t * @param _requested permissions requested
\t * @param _actual permissions effectively set
\t */
\tevent RoleUpdated(address indexed _by, address indexed _to, uint256 _requested, uint256 _actual);

\t/**
\t * @notice Creates an access control instance,
\t *      setting contract creator to have full privileges
\t */
\tconstructor() {
\t\t// contract creator has full privileges
\t\tuserRoles[msg.sender] = FULL_PRIVILEGES_MASK;
\t}

\t/**
\t * @notice Retrieves globally set of features enabled
\t *
\t * @dev Effectively reads userRoles role for the contract itself
\t *
\t * @return 256-bit bitmask of the features enabled
\t */
\tfunction features() public view returns(uint256) {
\t\t// features are stored in 'this' address  mapping of `userRoles` structure
\t\treturn userRoles[address(this)];
\t}

\t/**
\t * @notice Updates set of the globally enabled features (`features`),
\t *      taking into account sender's permissions
\t *
\t * @dev Requires transaction sender to have `ROLE_ACCESS_MANAGER` permission
\t * @dev Function is left for backward compatibility with older versions
\t *
\t * @param _mask bitmask representing a set of features to enable/disable
\t */
\tfunction updateFeatures(uint256 _mask) public {
\t\t// delegate call to `updateRole`
\t\tupdateRole(address(this), _mask);
\t}

\t/**
\t * @notice Updates set of permissions (role) for a given user,
\t *      taking into account sender's permissions.
\t *
\t * @dev Setting role to zero is equivalent to removing an all permissions
\t * @dev Setting role to `FULL_PRIVILEGES_MASK` is equivalent to
\t *      copying senders' permissions (role) to the user
\t * @dev Requires transaction sender to have `ROLE_ACCESS_MANAGER` permission
\t *
\t * @param operator address of a user to alter permissions for or zero
\t *      to alter global features of the smart contract
\t * @param role bitmask representing a set of permissions to
\t *      enable/disable for a user specified
\t */
\tfunction updateRole(address operator, uint256 role) public {
\t\t// caller must have a permission to update user roles
\t\trequire(isSenderInRole(ROLE_ACCESS_MANAGER), \"access denied\");

\t\t// evaluate the role and reassign it
\t\tuserRoles[operator] = evaluateBy(msg.sender, userRoles[operator], role);

\t\t// fire an event
\t\temit RoleUpdated(msg.sender, operator, role, userRoles[operator]);
\t}

\t/**
\t * @notice Determines the permission bitmask an operator can set on the
\t *      target permission set
\t * @notice Used to calculate the permission bitmask to be set when requested
\t *     in `updateRole` and `updateFeatures` functions
\t *
\t * @dev Calculated based on:
\t *      1) operator's own permission set read from userRoles[operator]
\t *      2) target permission set - what is already set on the target
\t *      3) desired permission set - what do we want set target to
\t *
\t * @dev Corner cases:
\t *      1) Operator is super admin and its permission set is `FULL_PRIVILEGES_MASK`:
\t *        `desired` bitset is returned regardless of the `target` permission set value
\t *        (what operator sets is what they get)
\t *      2) Operator with no permissions (zero bitset):
\t *        `target` bitset is returned regardless of the `desired` value
\t *        (operator has no authority and cannot modify anything)
\t *
\t * @dev Example:
\t *      Consider an operator with the permissions bitmask     00001111
\t *      is about to modify the target permission set          01010101
\t *      Operator wants to set that permission set to          00110011
\t *      Based on their role, an operator has the permissions
\t *      to update only lowest 4 bits on the target, meaning that
\t *      high 4 bits of the target set in this example is left
\t *      unchanged and low 4 bits get changed as desired:      01010011
\t *
\t * @param operator address of the contract operator which is about to set the permissions
\t * @param target input set of permissions to operator is going to modify
\t * @param desired desired set of permissions operator would like to set
\t * @return resulting set of permissions given operator will set
\t */
\tfunction evaluateBy(address operator, uint256 target, uint256 desired) public view returns(uint256) {
\t\t// read operator's permissions
\t\tuint256 p = userRoles[operator];

\t\t// taking into account operator's permissions,
\t\t// 1) enable the permissions desired on the `target`
\t\ttarget |= p & desired;
\t\t// 2) disable the permissions desired on the `target`
\t\ttarget &= FULL_PRIVILEGES_MASK ^ (p & (FULL_PRIVILEGES_MASK ^ desired));

\t\t// return calculated result
\t\treturn target;
\t}

\t/**
\t * @notice Checks if requested set of features is enabled globally on the contract
\t *
\t * @param required set of features to check against
\t * @return true if all the features requested are enabled, false otherwise
\t */
\tfunction isFeatureEnabled(uint256 required) public view returns(bool) {
\t\t// delegate call to `__hasRole`, passing `features` property
\t\treturn __hasRole(features(), required);
\t}

\t/**
\t * @notice Checks if transaction sender `msg.sender` has all the permissions required
\t *
\t * @param required set of permissions (role) to check against
\t * @return true if all the permissions requested are enabled, false otherwise
\t */
\tfunction isSenderInRole(uint256 required) public view returns(bool) {
\t\t// delegate call to `isOperatorInRole`, passing transaction sender
\t\treturn isOperatorInRole(msg.sender, required);
\t}

\t/**
\t * @notice Checks if operator has all the permissions (role) required
\t *
\t * @param operator address of the user to check role for
\t * @param required set of permissions (role) to check
\t * @return true if all the permissions requested are enabled, false otherwise
\t */
\tfunction isOperatorInRole(address operator, uint256 required) public view returns(bool) {
\t\t// delegate call to `__hasRole`, passing operator's permissions (role)
\t\treturn __hasRole(userRoles[operator], required);
\t}

\t/**
\t * @dev Checks if role `actual` contains all the permissions required `required`
\t *
\t * @param actual existent role
\t * @param required required role
\t * @return true if actual has required role (all permissions), false otherwise
\t */
\tfunction __hasRole(uint256 actual, uint256 required) internal pure returns(bool) {
\t\t// check the bitmask for the role required and return the result
\t\treturn actual & required == required;
\t}
}
"
    
// SPDX-License-Identifier: MIT
pragma solidity 0.8.7;

/**
 * @title ERC-165 Standard Interface Detection
 *
 * @dev Interface of the ERC165 standard, as defined in the
 *       https://eips.ethereum.org/EIPS/eip-165[EIP].
 *
 * @dev Implementers can declare support of contract interfaces,
 *      which can then be queried by others.
 *
 * @author Christian Reitwießner, Nick Johnson, Fabian Vogelsteller, Jordi Baylina, Konrad Feldmeier, William Entriken
 */
interface ERC165 {
\t/**
\t * @notice Query if a contract implements an interface
\t *
\t * @dev Interface identification is specified in ERC-165.
\t *      This function uses less than 30,000 gas.
\t *
\t * @param interfaceID The interface identifier, as specified in ERC-165
\t * @return `true` if the contract implements `interfaceID` and
\t *      `interfaceID` is not 0xffffffff, `false` otherwise
\t */
\tfunction supportsInterface(bytes4 interfaceID) external view returns (bool);
}
"
    }
  
