pragma solidity \u003e=0.4.22 \u003c0.7.0;

//
import \"./TokenERC20.sol\";
import \"./YearsDataSetOptimized.sol\";
// import \"./Functions.sol\";

contract EncropyTokenOptimized is TokenERC20 {

    uint8 public constant decimals = 8;  // 8 is the most common number of decimal places

    uint256 constant initialDate = 1584460800; // 2020-03-18 发行日期 timezone is PRC
    uint256 constant firstYearSupply = 500000000*10**uint256(decimals); // 首年发行量5亿个

    string constant PROFIT_LEDGER = \u0027PROFIT_LEDGER\u0027; // 盈利总账
    string constant HOLDING_LEDGER = \u0027HOLDING_LEDGER\u0027; // 持有总账
    string constant PROGRAM_LEDGER = \u0027PROGRAM_LEDGER\u0027; // 编程总账
    string constant PROPOSAL_LEDGER = \u0027PROPOSAL_LEDGER\u0027; // 参议总账
    string constant NODE_LEDGER = \u0027NODE_LEDGER\u0027; // 服务器节点总账
    string constant FUND_LEDGER = \u0027FUND_LEDGER\u0027; // 基金总账
    string constant DESTROY_LEDGER = \u0027DESTROY_LEDGER\u0027; // 销毁总账

    string public constant name = \u0027Encropy\u0027;
    string public constant symbol = \u0027ECP\u0027;

    mapping(string =\u003e address) ledgers; // 账本
    mapping(string =\u003e uint8) allocationRatio; // 分配比例
    mapping(uint256 =\u003e uint256) allocatedDates; // 已经分配ECP的日期

    YearsDataSetOptimized yearsData;//  = new YearsDataSetOptimized(initialDate,firstYearSupply); // 分配数据


    event DailyMined(uint256 indexed date, uint256 volume);
    event LedgerChanged(string indexed ledger_name, address new_address);

    modifier checkMined(uint256 _time) {
        require(isAllocated(_time) == false, \u0027today have been mined!\u0027);
        _;
    }
    modifier checkNowMined() {
        uint256 _time = now;
        require(_time \u003c= 3383222400, \u0027time is exceed the limit\u0027);
        require(isAllocated(_time) == false, \u0027today have been mined!\u0027);
        _;
    }

    // event FallbackIsCalled(address caller_address, uint256 _value, bytes data);
    // event ReceiveIsCalled(address caller_address, uint256 _value);

    constructor() public {
        owner = msg.sender;
        yearsData = new YearsDataSetOptimized(initialDate,firstYearSupply); // 分配数据
        setLedgers();
    }

    // receive() external payable {
    //     emit ReceiveIsCalled(msg.sender, msg.value);
    // }

    // fallback() external payable {
    //     emit FallbackIsCalled(msg.sender, msg.value, msg.data);
    // }

    // 设定账本地址和比例
    function setLedgers() private {
        // ledgers[\u0027GENERAL_LEDGER\u0027] = 0xc5A2D4ffBb95570602616A7ACAA4904C88A3BE33; // 总账
        ledgers[PROFIT_LEDGER] = 0x775a40c61f2Af5Ae9E7DC6A1f5E022ED9E58455D;
        ledgers[HOLDING_LEDGER] = 0x677d514Fb8D6FCDC2f741575aa8FE506210B5781;
        ledgers[PROGRAM_LEDGER] = 0x948E284E0222b35ca6E5404b0766f933e077b118;
        ledgers[PROPOSAL_LEDGER] = 0x6F7b95C8CEd86D091002A3546f5154256d6c0AA1;
        ledgers[NODE_LEDGER] = 0xC46E4B28703C1dDfA77507B8c6Bc7dC495a3b1de;
        ledgers[FUND_LEDGER] = 0x18731261A0cA711e67877389FBa962021CdfE1BD;
        ledgers[DESTROY_LEDGER] = 0xCEA7B41F90069Cf88F004ee806f88d4840EFc530;

        allocationRatio[PROFIT_LEDGER] = 50;
        allocationRatio[HOLDING_LEDGER] = 20;
        allocationRatio[PROGRAM_LEDGER] = 10;
        allocationRatio[PROPOSAL_LEDGER] = 10;
        allocationRatio[NODE_LEDGER] = 5;
        allocationRatio[FUND_LEDGER] = 5;

        // prevMine();
    }

    // 获取账本地址
    function getLedgerAddress(string memory _name) public view returns (address) {
        return ledgers[_name];
    }

    // 获取账本分成比例
    function getLedgerRadio(string memory _name) public view returns (uint8) {
        return allocationRatio[_name];
    }

    // 获取该年度的数据
    function getYearData(uint16 _year) public view returns(uint16 year, uint256 start_time, uint256 end_time, uint16 daysInYear, uint256 issueVolume) {
        return yearsData.getYearDataFromYear(_year);
    }

    // 从时间戳里面获取日期的时间戳
    function getYearDataFromTimestamp(uint256 _time) public view returns(uint16 year, uint256 start_time, uint256 end_time, uint16 daysInYear, uint256 issueVolume) {
        return yearsData.getYearDataFromTimestamp(_time);
    }

    // 从时间戳里面获取日期的时间戳
    function getDayTimestampFromTimestamp(uint256 _time) public view returns(uint256) {
        return yearsData.getDayTimestamp(_time);
    }

    // 获取从现在到发行日的所有历史的日期时间戳
    function getHistoryDaysTimestamp() public view returns(uint256[] memory){
        uint256 time = now - 3600*24;
        return yearsData.getHistoryDaysTimestamp(time);
    }

    // 该日期是否已经挖过了
    function isAllocated(uint256 _dayTimestamp) public view returns(bool) {
        if (allocatedDates[_dayTimestamp] \u003e 0)
        {
            return true;
        }

        _dayTimestamp = getDayTimestampFromTimestamp(_dayTimestamp);

        if (allocatedDates[_dayTimestamp] \u003e 0)
        {
            return true;
        }

        return false;
    }
    // 指定日期的挖矿挖矿
    function mine(uint256 _dayTimestamp) checkMined(_dayTimestamp) private onlyOwner{
        uint256 __dayTimestamp = getDayTimestampFromTimestamp(_dayTimestamp);

        (, , , uint16 daysInYear, uint256 issueVolume) = getYearDataFromTimestamp(__dayTimestamp);

        uint256 dayVolume = issueVolume / daysInYear; // 每日的出矿量

        totalSupply += dayVolume; // 总量增加

        balanceOf[msg.sender] += dayVolume; // 创建人人余额增加

        emit DailyMined(__dayTimestamp, dayVolume);

        allocatedDates[__dayTimestamp] = dayVolume; // 记录已挖的日期

        allocatingMine(dayVolume); // 按规则分配矿池

    }

    // 挖今天的矿
    function mine() checkNowMined public onlyOwner {
        uint256 _now = now;

        mine(_now);
    }

    // 按规则分配矿池
    function allocatingMine(uint256 _dayVolume) internal onlyOwner{
        uint256 value = 0;

        value = (_dayVolume * 50) / 100;
        transfer(ledgers[PROFIT_LEDGER], value);

        value = (_dayVolume * 20) / 100;
        transfer(ledgers[HOLDING_LEDGER], value);

        value = (_dayVolume * 10) / 100;
        transfer(ledgers[PROGRAM_LEDGER], value);

        value = (_dayVolume * 10) / 100;
        transfer(ledgers[PROPOSAL_LEDGER], value);

        value = (_dayVolume * 5) / 100;
        transfer(ledgers[NODE_LEDGER], value);


        // value = (_dayVolume * allocationRatio[FUND_LEDGER]) / 100;
        value = balanceOf[msg.sender]; // 剩下的全给基金会，防止有余数
        transfer(ledgers[FUND_LEDGER], value);
    }
    // 将之前的先挖出来
    function prevMine() public onlyOwner{
        uint256[] memory historyDaysTimestamp = getHistoryDaysTimestamp();

        uint256 max = 5;
        for (uint256 i=0; i\u003chistoryDaysTimestamp.length; i++)
        {
            // if (max \u003c 0)
            // {
            //     break;
            // }

            if (!isAllocated(historyDaysTimestamp[i]))
            {
                mine(historyDaysTimestamp[i]);
                max = max-1;
            }
        }
    }

    // 一天的发行量
    function dateOfSupply(uint256 _date) public view returns (uint256) {
        (,,,uint256 daysOfYear, uint256 supplyOfYear) = getYearDataFromTimestamp(_date);

        return supplyOfYear / daysOfYear;
    }

    // 今天的发行量
    function todayOfSupply() public view returns(uint256) {
        return dateOfSupply(now);
    }

    function changeLedgerAddress(string memory _ledger_name, address _new_address) onlyOwner public {
        require(ledgers[_ledger_name] != address(0), \u0027ledger is not exists.\u0027);

        ledgers[_ledger_name] = _new_address;

        emit LedgerChanged(_ledger_name, _new_address);
    }

}
"},"owned.sol":{"content":"pragma solidity \u003e=0.4.22 \u003c0.7.0;

contract owned {
    address public owner;

    constructor() public {
        owner = msg.sender;
    }

    modifier onlyOwner {
        require(msg.sender == owner);
        _;
    }

    function transferOwnership(address newOwner) onlyOwner public {
        owner = newOwner;
    }
}"},"TokenERC20.sol":{"content":"pragma solidity \u003e=0.4.22 \u003c0.7.0;

interface tokenRecipient { function receiveApproval(address _from, uint256 _value, address _token, bytes calldata _extraData) external; }

import \"./owned.sol\";

contract TokenERC20 is owned{
    // string public name;
    // string public symbol;
    // uint8 public decimals;
    uint256 public totalSupply;

    // 用mapping保存每个地址对应的余额
    mapping (address =\u003e uint256) public balanceOf;
    // 存储对账号的控制
    mapping (address =\u003e mapping (address =\u003e uint256)) public allowance;

    mapping (address =\u003e bool) public frozenAccount;

    /* This generates a public event on the blockchain that will notify clients */
    event FrozenFunds(address indexed target, bool frozen);

    // 事件，用来通知客户端交易发生
    event Transfer(address indexed from, address indexed to, uint256 value);

    // 事件，用来通知客户端代币被消费
    event Burn(address indexed from, uint256 value);

    /**
     * 初始化构造
     */
    // constructor(uint256 initialSupply, string memory tokenName, string memory tokenSymbol) public {
    //     totalSupply = initialSupply * 10 ** uint256(decimals);  // 供应的份额，份额跟最小的代币单位有关，份额 = 币数 * 10 ** decimals。
    //     balanceOf[msg.sender] = totalSupply;                // 创建者拥有所有的代币
    //     name = tokenName;                                   // 代币名称
    //     symbol = tokenSymbol;                               // 代币符号
    // }

    /**
     * 代币交易转移的内部实现
     */
    // function _transfer(address _from, address _to, uint _value) internal {
    //     // 确保目标地址不为0x0，因为0x0地址代表销毁
    //     require(_to != address(0x0));
    //     // 检查发送者余额
    //     require(balanceOf[_from] \u003e= _value);
    //     // 确保转移为正数个
    //     require(balanceOf[_to] + _value \u003e balanceOf[_to]);

    //     // 以下用来检查交易，
    //     uint previousBalances = balanceOf[_from] + balanceOf[_to];
    //     // Subtract from the sender
    //     balanceOf[_from] -= _value;
    //     // Add the same to the recipient
    //     balanceOf[_to] += _value;
    //     emit Transfer(_from, _to, _value);

    //     // 用assert来检查代码逻辑。
    //     assert(balanceOf[_from] + balanceOf[_to] == previousBalances);
    // }

    /**
     *  代币交易转移
     * 从创建交易者账号发送`_value`个代币到 `_to`账号
     *
     * @param _to 接收者地址
     * @param _value 转移数额
     */
    function transfer(address _to, uint256 _value) public {
        _transfer(msg.sender, _to, _value);
    }

    /**
     * 账号之间代币交易转移
     * @param _from 发送者地址
     * @param _to 接收者地址
     * @param _value 转移数额
     */
    function transferFrom(address _from, address _to, uint256 _value) public returns (bool success) {
        require(_value \u003c= allowance[_from][msg.sender], \u0027You have not been allowed to transfer.\u0027);     // Check allowance
        allowance[_from][msg.sender] -= _value;
        _transfer(_from, _to, _value);
        return true;
    }

    /**
     * 设置某个地址（合约）可以交易者名义花费的代币数。
     *
     * 允许发送者`_spender` 花费不多于 `_value` 个代币
     *
     * @param _spender The address authorized to spend
     * @param _value the max amount they can spend
     */
    function approve(address _spender, uint256 _value) public
        returns (bool success) {
        allowance[msg.sender][_spender] = _value;
        return true;
    }

    /**
     * 设置允许一个地址（合约）以交易者名义可最多花费的代币数。
     *
     * @param _spender 被授权的地址（合约）
     * @param _value 最大可花费代币数
     * @param _extraData 发送给合约的附加数据
     */
    // function approveAndCall(address _spender, uint256 _value, bytes memory _extraData)
    //     public
    //     returns (bool success) {
    //     tokenRecipient spender = tokenRecipient(_spender);
    //     if (approve(_spender, _value)) {
    //         spender.receiveApproval(msg.sender, _value, address(this), _extraData);
    //         return true;
    //     }
    // }

    /**
     * 销毁创建者账户中指定个代币
     */
    function burn(uint256 _value) public returns (bool success) {
        require(balanceOf[msg.sender] \u003e= _value, \u0027This msg.sender have not enough value to burn.\u0027);   // Check if the sender has enough
        balanceOf[msg.sender] -= _value;            // Subtract from the sender
        totalSupply -= _value;                      // Updates totalSupply
        emit Burn(msg.sender, _value);
        return true;
    }

    /**
     * 销毁用户账户中指定个代币
     *
     * Remove `_value` tokens from the system irreversibly on behalf of `_from`.
     *
     * @param _from the address of the sender
     * @param _value the amount of money to burn
     */
    function burnFrom(address _from, uint256 _value) public returns (bool success) {
        require(balanceOf[_from] \u003e= _value, \u0027This address have not enough value to burn.\u0027);                // Check if the targeted balance is enough
        require(_value \u003c= allowance[_from][msg.sender], \u0027This address have not been allowed to burn.\u0027);    // Check allowance
        balanceOf[_from] -= _value;                         // Subtract from the targeted balance
        allowance[_from][msg.sender] -= _value;             // Subtract from the sender\u0027s allowance
        totalSupply -= _value;                              // Update totalSupply
        emit Burn(_from, _value);
        return true;
    }
    
    /* Internal transfer, only can be called by this contract */
    function _transfer(address _from, address _to, uint _value) internal {
        require (_to != address(0x0), \u0027Can not transfer to a empty address.\u0027);                               // Prevent transfer to 0x0 address. Use burn() instead
        require (balanceOf[_from] \u003e= _value, \u0027This address have not enough value to transfer.\u0027);               // Check if the sender has enough
        require (balanceOf[_to] + _value \u003e balanceOf[_to], \u0027The receiving address have too many token to transfer.\u0027); // Check for overflows
        require(!frozenAccount[_from], \u0027This address have been frozen.\u0027);                     // Check if sender is frozen
        require(!frozenAccount[_to], \u0027The receiving address have been frozen.\u0027);                       // Check if recipient is frozen
        balanceOf[_from] -= _value;                         // Subtract from the sender
        balanceOf[_to] += _value;                           // Add the same to the recipient
        emit Transfer(_from, _to, _value);
    }

    /// @notice Create `mintedAmount` tokens and send it to `target`
    /// @param target Address to receive the tokens
    /// @param mintedAmount the amount of tokens it will receive
    // function mintToken(address target, uint256 mintedAmount) onlyOwner public {
    //     balanceOf[target] += mintedAmount;
    //     totalSupply += mintedAmount;
    //     emit Transfer(address(0), address(this), mintedAmount);
    //     emit Transfer(address(this), target, mintedAmount);
    // }

    /// @notice `freeze? Prevent | Allow` `target` from sending \u0026 receiving tokens
    /// @param target Address to be frozen
    /// @param freeze either to freeze it or not
    function freezeAccount(address target, bool freeze) onlyOwner public {
        frozenAccount[target] = freeze;
        emit FrozenFunds(target, freeze);
    }
}
"},"YearsDataSetOptimized.sol":{"content":"pragma solidity \u003e=0.4.0 \u003c0.7.0;

// time_zone is PRC
contract YearsDataSetOptimized {

    // gap of UTC and PRC time
    uint16 constant time_zone_seconds = 3600*8; // PRC的时区秒数
    uint16 constant start_year = 2020; // 开始年份
    uint16 constant end_year = 2076; // 结束年份
    // uint16[14] LeapYears = [2023,2027,2031,2035,2039,2043,2047,2051,2055,2059,2063,2067,2071,2075];
    uint16[14] LeapYears = [2024,2028,2032,2036,2040,2044,2048,2052,2056,2060,2064,2068,2072,2076]; // 闰年年份

    uint256 constant secondsInDay = 86400; // 一天的秒数
    uint256 constant maxTimestamp = 3383222400; // 最大的时间戳，用来限制输入数据
    uint256 initialDateTimestamp; // 开始发行的时间戳，PRC time zone
    uint256 firstYearSupply;

    // 限制年份
    modifier checkYear(uint256 _year)  {
        require(_year \u003e= 2020 \u0026\u0026 _year \u003c= 2076, \u0027Year number must be between 2020 and 2076\u0027);
        _;
    }

    // 限制时间戳
    modifier checkTimestamp(uint256 _time) {
        require(_time \u003e 0 \u0026\u0026 _time \u003c= maxTimestamp, \u0027Timestamp must not be be between 0 and 3383222400\u0027);
        _;
    }

    constructor(uint256 _initialDateTimestamp, uint256 _firstYearSupply) public {
        initialDateTimestamp = _initialDateTimestamp;
        firstYearSupply = _firstYearSupply;

        // for test
/*        if (initialDateTimestamp == 0)
        {
            initialDateTimestamp = 1584460800;
        }

        if (firstYearSupply == 0)
        {
            firstYearSupply = 50000000000000000;
        }
*/
        require(initialDateTimestamp \u003e 0, \u0027initial DateTime can not be 0.\u0027);
        require(firstYearSupply \u003e 0, \u0027First Year Supply can not be 0.\u0027);

        // 设定年度的开始和结束时间戳、一年里面的天数，总发行量，作为数据验证
        //yearsData[2020] = dayInYear(2020,1584460800,1615996800,365,50000000000000000);
        //yearsData[2021] = dayInYear(2021,1615996800,1647532800,365,25000000000000000);
        //yearsData[2022] = dayInYear(2022,1647532800,1679068800,365,12500000000000000);
        //yearsData[2023] = dayInYear(2023,1679068800,1710691200,366,6250000000000000);
        //yearsData[2024] = dayInYear(2024,1710691200,1742227200,365,3125000000000000);
        //yearsData[2025] = dayInYear(2025,1742227200,1773763200,365,1562500000000000);
        //yearsData[2026] = dayInYear(2026,1773763200,1805299200,365,781250000000000);
        //yearsData[2027] = dayInYear(2027,1805299200,1836921600,366,390625000000000);
        //yearsData[2028] = dayInYear(2028,1836921600,1868457600,365,195312500000000);
        //yearsData[2029] = dayInYear(2029,1868457600,1899993600,365,97656250000000);
        //yearsData[2030] = dayInYear(2030,1899993600,1931529600,365,48828125000000);
        //yearsData[2031] = dayInYear(2031,1931529600,1963152000,366,24414062500000);
        //yearsData[2032] = dayInYear(2032,1963152000,1994688000,365,12207031250000);
        //yearsData[2033] = dayInYear(2033,1994688000,2026224000,365,6103515625000);
        //yearsData[2034] = dayInYear(2034,2026224000,2057760000,365,3051757812500);
        //yearsData[2035] = dayInYear(2035,2057760000,2089382400,366,1525878906250);
        //yearsData[2036] = dayInYear(2036,2089382400,2120918400,365,762939453125);
        //yearsData[2037] = dayInYear(2037,2120918400,2152454400,365,381469726562);
        //yearsData[2038] = dayInYear(2038,2152454400,2183990400,365,190734863281);
        //yearsData[2039] = dayInYear(2039,2183990400,2215612800,366,95367431640);
        //yearsData[2040] = dayInYear(2040,2215612800,2247148800,365,47683715820);
        //yearsData[2041] = dayInYear(2041,2247148800,2278684800,365,23841857910);
        //yearsData[2042] = dayInYear(2042,2278684800,2310220800,365,11920928955);
        //yearsData[2043] = dayInYear(2043,2310220800,2341843200,366,5960464477);
        //yearsData[2044] = dayInYear(2044,2341843200,2373379200,365,2980232238);
        //yearsData[2045] = dayInYear(2045,2373379200,2404915200,365,1490116119);
        //yearsData[2046] = dayInYear(2046,2404915200,2436451200,365,745058059);
        //yearsData[2047] = dayInYear(2047,2436451200,2468073600,366,372529029);
        //yearsData[2048] = dayInYear(2048,2468073600,2499609600,365,186264514);
        //yearsData[2049] = dayInYear(2049,2499609600,2531145600,365,93132257);
        //yearsData[2050] = dayInYear(2050,2531145600,2562681600,365,46566128);
        //yearsData[2051] = dayInYear(2051,2562681600,2594304000,366,23283064);
        //yearsData[2052] = dayInYear(2052,2594304000,2625840000,365,11641532);
        //yearsData[2053] = dayInYear(2053,2625840000,2657376000,365,5820766);
        //yearsData[2054] = dayInYear(2054,2657376000,2688912000,365,2910383);
        //yearsData[2055] = dayInYear(2055,2688912000,2720534400,366,1455191);
        //yearsData[2056] = dayInYear(2056,2720534400,2752070400,365,727595);
        //yearsData[2057] = dayInYear(2057,2752070400,2783606400,365,363797);
        //yearsData[2058] = dayInYear(2058,2783606400,2815142400,365,181898);
        //yearsData[2059] = dayInYear(2059,2815142400,2846764800,366,90949);
        //yearsData[2060] = dayInYear(2060,2846764800,2878300800,365,45474);
        //yearsData[2061] = dayInYear(2061,2878300800,2909836800,365,22737);
        //yearsData[2062] = dayInYear(2062,2909836800,2941372800,365,11368);
        //yearsData[2063] = dayInYear(2063,2941372800,2972995200,366,5684);
        //yearsData[2064] = dayInYear(2064,2972995200,3004531200,365,2842);
        //yearsData[2065] = dayInYear(2065,3004531200,3036067200,365,1421);
        //yearsData[2066] = dayInYear(2066,3036067200,3067603200,365,710);
        //yearsData[2067] = dayInYear(2067,3067603200,3099225600,366,355);
        //yearsData[2068] = dayInYear(2068,3099225600,3130761600,365,177);
        //yearsData[2069] = dayInYear(2069,3130761600,3162297600,365,88);
        //yearsData[2070] = dayInYear(2070,3162297600,3193833600,365,44);
        //yearsData[2071] = dayInYear(2071,3193833600,3225456000,366,22);
        //yearsData[2072] = dayInYear(2072,3225456000,3256992000,365,11);
        //yearsData[2073] = dayInYear(2073,3256992000,3288528000,365,5);
        //yearsData[2074] = dayInYear(2074,3288528000,3320064000,365,2);
        //yearsData[2075] = dayInYear(2075,3320064000,3351686400,366,1);
        //yearsData[2076] = dayInYear(2076,3351686400,3383222400,365,0);

    }


    // 获取该年度的数据
    // return (年度, 开始时间戳，结束时间戳，该年度的天数，该年度的总发行量)
    function getYearDataFromYear(uint16 _year) checkYear(_year) public view  returns(uint16 , uint256 , uint256 , uint16, uint256) {

        uint256 issueVolumeOfYear = firstYearSupply; // 首年发行量
        uint256 start_gep_days = 0;
        uint256 last_year;

        uint16[14] memory _leap_years = LeapYears;

        // 算出开始时间距离发行日的天数
        for (uint16 i=start_year; i\u003c=_year; i++)
        {
            last_year = i;
            if (i \u003e start_year)
            {
                issueVolumeOfYear = issueVolumeOfYear / 2;
                start_gep_days += 365;
            }

            for (uint256 ii=0; ii\u003c_leap_years.length; ii++)
            {
                if (last_year == _leap_years[ii])
                {
                    // 闰年加一天
                    start_gep_days++;
                    break;
                }
            }
        }

        uint16 _daysInYear = 365;

        last_year++;

        for (uint256 ii=0; ii\u003c_leap_years.length; ii++)
        {
            if (last_year == _leap_years[ii])
            {
                // 闰年加一天
                _daysInYear++;
                break;
            }
        }

        uint256 start_time = initialDateTimestamp + start_gep_days*1 days; // 开始时间
        uint256 end_time = initialDateTimestamp + (start_gep_days + _daysInYear)*1 days; // 结束时间

        return (_year, start_time, end_time ,_daysInYear, issueVolumeOfYear);
    }

    // 根据时间戳来获取当年度的数据
    // return (年度, 开始时间戳，结束时间戳，该年度的天数，该年度的总发行量)
    function getYearDataFromTimestamp(uint256 _time) checkTimestamp(_time) public view returns(uint16, uint256 , uint256 , uint16 , uint256 ) {
        uint16 year;
        uint256 start_time;
        uint256 end_time;
        uint16 daysInYear;
        uint256 issueVolumeOfYear;

        // 计算_time时间戳落在那个年份内
        for (uint16 i=start_year; i\u003c=end_year; i++)
        {
            (year, start_time, end_time ,daysInYear,issueVolumeOfYear) = getYearDataFromYear(i);

            if (_time \u003e= start_time \u0026\u0026 _time \u003c end_time)
            {
                break;
            }

        }
        return (year, start_time, end_time ,daysInYear, issueVolumeOfYear);
    }

    // 从时间戳获取日期的时间戳
    function getDayTimestamp(uint256 _time) checkTimestamp(_time) public pure returns (uint256) {
        uint256 mod = (_time + time_zone_seconds) % secondsInDay;

        uint256 dayTimestamp = _time - mod;
        return dayTimestamp;
    }

    // 获取时间区间内的所有日期时间戳
    function getHistoryDaysTimestamp(uint256 _dayTimestamp) checkTimestamp(_dayTimestamp) public view returns(uint256[] memory) {
        uint256 dayTimestamp = getDayTimestamp(_dayTimestamp);
        uint256 dayTimestamp2 = dayTimestamp;
        uint256 _initialDateTimestamp = initialDateTimestamp;
        uint16 count = 0;

        // 因为不支持动态数组，只能先计算数组的大小，然后再赋值过去，我日~~~~
        for (uint16 i=0; i\u003c365; i++)
        {
            if (dayTimestamp \u003c= _initialDateTimestamp)
            {
                break;
            }

            count++;

            dayTimestamp -= secondsInDay;
        }

        uint256[] memory daysTimestamp = new uint256[](count);

        dayTimestamp = dayTimestamp2;

        // 赋值给数组
        daysTimestamp[0] = dayTimestamp;
        for (uint16 i=1; i\u003ccount; i++)
        {
            dayTimestamp -= secondsInDay;
            daysTimestamp[i] = dayTimestamp;
        }

        return daysTimestamp;
    }
}

