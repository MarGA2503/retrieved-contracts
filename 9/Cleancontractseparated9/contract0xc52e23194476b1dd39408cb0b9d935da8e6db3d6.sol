pragma solidity ^0.5.0;

import \"hardhat/console.sol\";

import \"./CleverProtocol.sol\";

import \"@openzeppelin/contracts/token/ERC20/IERC20.sol\";
import \"@openzeppelin/contracts/ownership/Ownable.sol\";
import \"@openzeppelin/contracts/access/roles/MinterRole.sol\";
import \"@openzeppelin/contracts/math/SafeMath.sol\";

/**
 * @dev Implementation of the ERC20 CleverToken
 *
 * At construction, the deployer of the contract is the only owner, 
 * which will be used to solely set the Protocol and then renounce,
 * bricking any onlyOwner functions in this token. Additionally, the
 * owner is set as the only minter, and should revoke minting ability 
 * 
 */
contract CleverToken is Ownable, MinterRole, IERC20 {
    using SafeMath for uint256;
    
    string private _name;
    string private _symbol;
    uint8 private _decimals;

    // Used for authentication of distribution
    address payable public Protocol;
    address payable private _admin;

    event LogCycleDistribution(uint256 indexed cycle, uint256 newly_added_tokens);
    event LogProtocolSet(address Protocol);

    uint256 private fragsPerToken;
    uint256 private lastCyclePaid = 0;
    
    uint256 private DECIMALS = 18;
    uint256 private constant MAX_UINT256 = ~uint256(0);

    uint256 private totalFrags = 1e60;
    
    bool private lockedSwap = false;
    uint256 internal _cap;
    
    uint256 private _totalSupply;
    mapping (address => uint256) private _fragBalances;
    mapping (address => mapping (address => uint256)) private _allowances;

    modifier onlyProtocol() {
        require(msg.sender == Protocol,\"Sender is not the Protocol!\");
        _;
    }
    
    modifier validRecipient(address to) {
        require(to != address(0x0));
        require(to != address(this));
        _;
    }

    constructor (string memory name, string memory symbol, uint8 decimals) public {
        _name = name;
        _symbol = symbol;
        _decimals = decimals;

        _totalSupply = 0;
        fragsPerToken = 1e30;
        _cap = uint256(1_000_000_000_000).mul(1e18); //1 trillion tokens
    }
    
    function distribute(uint256 _cycle, uint256 _percentageFactor,uint256 _bonusPercentageFactor) external onlyProtocol {
        //Protocol ensures that this will not be called prior to swap/minting phase ending
        //require this cycle to be +1 to the last cycle paid
        require(_cycle > lastCyclePaid, \"Cycle attempting to be paid out is in the past!\");

        if(!lockedSwap){
            //set the contract constants based on the _totalSupply at the time of the first distrubtion
            //totalFrags from here forth is permanent-- this locks in the proper % distributions
            // 1e30 is the maximum resolution required since max_supply is 1e12
            totalFrags = _totalSupply.mul(uint256(1e30));
            
            //lock Minting from here
            lockedSwap = true;
        }

        require(lockedSwap,\"Swapping phase is not over!\");
        
        //retrieve the initial supply
        uint256 initial_total_supply = _totalSupply;
        
        //factor is percentage taken out of 1e5, therefore if factor is 1 then the percentage is 0.00001
        //since the max supply is 1e12*1e18, instability is not taken into account
        uint256 delta = (initial_total_supply.mul(_percentageFactor)).div(1e5);
        require(delta > 0, \"supply delta was not greater than 0\");
        
        //set the internal supply of the CLVA token
        _totalSupply = _totalSupply.add(delta);
        
        //distribute the bonus if applicable
        if(_bonusPercentageFactor > 0) {
            //calculate initial supply
            uint256 initial_total_supply_two = _totalSupply;

            uint256 delta_two = (initial_total_supply_two.mul(_bonusPercentageFactor)).div(1e5);
            require(delta_two > 0, \"supply delta was not greater than 0\");

            //set the internal supply of the CLVA token
            _totalSupply = _totalSupply.add(delta_two);
        }

        //if cap is ever reached, simply set the supply in circulation equal to that and continue
        if (_totalSupply > _cap) {
            _totalSupply = _cap;
        } else{
            
            fragsPerToken = totalFrags.div(_totalSupply);

            //send to admin as long as cap is not met
            //admin receives 0.1% of new total supply
            uint256 forAdmin = (_totalSupply.mul(1e2)).div(1e5);

            //update total supply AND total frags so that the rate does not change
            _totalSupply = _totalSupply.add(forAdmin);
            totalFrags = totalFrags.add(forAdmin.mul(fragsPerToken));

            _fragBalances[_admin] = _fragBalances[_admin].add(forAdmin.mul(fragsPerToken));

            fragsPerToken = totalFrags.div(_totalSupply);
        }
        
        
        lastCyclePaid = lastCyclePaid.add(1);
        
        emit LogCycleDistribution(lastCyclePaid, delta);
    }
    
    function setProtocol(address payable _protocol) external onlyOwner {
        Protocol = _protocol;
        CleverProtocol protocolContract = CleverProtocol(Protocol);
        _admin = protocolContract.admin();

        emit LogProtocolSet(Protocol);
    }
    
    function protocol() public view returns(address){
        return Protocol;
    }

    function balanceOf(address account) public view returns (uint256) {
        return _fragBalances[account].div(fragsPerToken);
    }

    function fragsOf(address account) public view returns (uint256){
        return _fragBalances[account];
    }

    function admin() public view returns(address){
        return _admin;
    }
    
    function mint(address account, uint256 amount) public onlyMinter returns (bool) {
        require(totalSupply().add(amount) <= _cap, \"CLVA in circulation has reached 1 trillion!\");
        if(lockedSwap){
            // different minting funciton
            _mintAfterSwap(account,amount);
        } else{
            _mint(account, amount);    
        }
        
        return true;
    }
    
     /**
     * @dev Override interal mint function
     * @param account The account to receive newly minted coins
     * @param amount The amount to be transferred.
     */
     
    function _mint(address account, uint256 amount) internal {
        require(!lockedSwap, \"minting phase is over\");
        require(account != address(0), \"ERC20: mint to the zero address\");
        
        _totalSupply = _totalSupply.add(amount);
        
        _fragBalances[account] = _fragBalances[account].add(amount.mul(fragsPerToken));
        emit Transfer(address(0), account, amount);
    }
    
    function _mintAfterSwap(address account, uint256 amount) internal{
        require(account != address(0), \"ERC20: mint to the zero address\");
        
        //add amount to total supply
        _totalSupply = _totalSupply.add(amount);
        
        //calculate the number of frags this is
        uint256 fragAmount = amount.mul(fragsPerToken);
        
        //add this frag amount to the address account
        _fragBalances[account] = _fragBalances[account].add(fragAmount);    
        
        //fragsPerToken ~ 1e60 - O(totalSupply), amount ~ O(totalSupply), therefore fragAmount~ 1e48 - O(totalSupply) + O(totalSupply) ~ 1e48 
        require(amount < _totalSupply, \"amount < totalSupply -- bounding this ensures no inflationary issues in regards to the internal FRAG denomination -- will always be O(e60)\");
        
        //update total frags
        totalFrags = totalFrags.add(fragAmount);
        
        //update fragsPerToken
        fragsPerToken = totalFrags.div(_totalSupply);
        
        emit Transfer(address(0), account, amount);
    }
    
    function adminDistribute(uint256 _percentageFactor) external onlyOwner {
        //Protocol ensures that this will not be called prior to swap/minting phase ending
        //require this cycle to be +1 to the last cycle paid
        if(!lockedSwap){
            revert(\"no distribution available while in intial minting phase.\");
        }
        
        require(lockedSwap,\"Swapping phase is not over!\");
        
        if (_totalSupply > _cap) {
            revert(\"max supply has been reached!\");
        }
        //retrieve the initial supply
        uint256 initial_total_supply = _totalSupply;
        
        //factor is percentage taken out of 1e5, therefore if factor is 1 then the percentage is 0.00001
        //since the max supply is 1e12*1e18, instability is not taken into account
        uint256 delta = (initial_total_supply.mul(_percentageFactor)).div(1e5);
        require(delta > 0, \"supply delta was not greater than 0\");
        
        //set the internal supply of the CLVA token
        _totalSupply = _totalSupply.add(delta);
        
        //if cap is ever reached, simply set the supply in circulation equal to that
        if (_totalSupply > _cap) {
            _totalSupply = _cap;
        }
        
        fragsPerToken = totalFrags.div(_totalSupply);
        
        emit LogCycleDistribution(999, delta);
    }
    
    /**
     * @param to The address to transfer to.
     * @param value The amount to be transferred.
     * @return True on success, false otherwise.
     */
    function transfer(address to, uint256 value)
        public
        returns (bool)
    {
        require(to != address(0), \"ERC20: transfer from the zero address\");

        uint256 fragValue = value.mul(fragsPerToken);
        _fragBalances[msg.sender] = _fragBalances[msg.sender].sub(fragValue);
        _fragBalances[to] = _fragBalances[to].add(fragValue);
        emit Transfer(msg.sender, to, value);
        return true;
    }    
    
     /**
     * Transfer tokens from one address to another.
     * @param from The address you want to send tokens from.
     * @param to The address you want to transfer to.
     * @param value The amount of tokens to be transferred.
     */
    function transferFrom(address from, address to, uint256 value)
        public
        returns (bool)
    {
        require(to != address(0), \"ERC20: transfer from the zero address\");
        require(from != address(0), \"ERC20: transfer from the zero address\");
        
        _allowances[from][msg.sender] = _allowances[from][msg.sender].sub(value);

        uint256 fragValue = value.mul(fragsPerToken);
        _fragBalances[from] = _fragBalances[from].sub(fragValue);
        _fragBalances[to] = _fragBalances[to].add(fragValue);
        emit Transfer(from, to, value);

        return true;
    }

    /**
     * @dev Function to check the amount of tokens that an owner has allowed to a spender.
     * @param owner_ The address which owns the funds.
     * @param spender The address which will spend the funds.
     * @return The number of tokens still available for the spender.
     */
    function allowance(address owner_, address spender)
        public
        view
        returns (uint256)
    {
        return _allowances[owner_][spender];
    }
    
    /**
     * @dev Approve the passed address to spend the specified amount of tokens on behalf of
     * msg.sender. This method is included for ERC20 compatibility.
     * Changing an allowance with this method brings the risk that someone may transfer both
     * the old and the new allowance - if they are both greater than zero - if a transfer
     * transaction is mined before the later approve() call is mined.
     *
     * @param spender The address which will spend the funds.
     * @param value The amount of tokens to be spent.
     */
    function approve(address spender, uint256 value)
        public
        returns (bool)
    {
        _allowances[msg.sender][spender] = value;
        emit Approval(msg.sender, spender, value);
        return true;
    }

    /**
     * @dev Atomically increases the allowance granted to `spender` by the caller.
     *
     * This is an alternative to {approve} that can be used as a mitigation for
     * problems described in {IERC20-approve}.
     *
     * Emits an {Approval} event indicating the updated allowance.
     *
     * Requirements:
     *
     * - `spender` cannot be the zero address.
     */
    function increaseAllowance(address spender, uint256 addedValue) public returns (bool) {
        require(spender != address(0), \"ERC20: zero address\");

        _allowances[msg.sender][spender] = _allowances[msg.sender][spender].add(addedValue);
        emit Approval(msg.sender, spender, _allowances[msg.sender][spender].add(addedValue));
        return true;
    }

    /**
     * @dev Atomically decreases the allowance granted to `spender` by the caller.
     *
     * This is an alternative to {approve} that can be used as a mitigation for
     * problems described in {IERC20-approve}.
     *
     * Emits an {Approval} event indicating the updated allowance.
     *
     * Requirements:
     *
     * - `spender` cannot be the zero address.
     * - `spender` must have allowance for the caller of at least
     * `subtractedValue`.
     */
    function decreaseAllowance(address spender, uint256 subtractedValue) public returns (bool) {
        require(spender != address(0), \"ERC20: zero address\");
        _allowances[msg.sender][spender] = _allowances[msg.sender][spender].sub(subtractedValue);
        emit Approval(msg.sender, spender, _allowances[msg.sender][spender].sub(subtractedValue));
        return true;
    }


    /**
     * @dev Returns the cap on the token's total supply.
     */
    function cap() public view returns (uint256) {
        return _cap;
    }

    /**
     * @dev Returns the token's total supply.
     */
    function totalSupply() public view returns (uint256) {
        return _totalSupply;
    }
    
        /**
     * @dev Returns the name of the token.
     */
    function name() public view returns (string memory) {
        return _name;
    }

    /**
     * @dev Returns the symbol of the token, usually a shorter version of the
     * name.
     */
    function symbol() public view returns (string memory) {
        return _symbol;
    }

    /**
     * @dev Returns the number of decimals used to get its user representation.
     * For example, if `decimals` equals `2`, a balance of `505` tokens should
     * be displayed to a user as `5,05` (`505 / 10 ** 2`).
     *
     * Tokens usually opt for a value of 18, imitating the relationship between
     * Ether and Wei.
     *
     * NOTE: This information is only used for _display_ purposes: it in
     * no way affects any of the arithmetic of the contract, including
     * {IERC20-balanceOf} and {IERC20-transfer}.
     */
    function decimals() public view returns (uint8) {
        return _decimals;
    }
    
    
}
"
    
// SPDX-License-Identifier: MIT
pragma solidity >= 0.4.22 <0.8.0;

library console {
\taddress constant CONSOLE_ADDRESS = address(0x000000000000000000636F6e736F6c652e6c6f67);

\tfunction _sendLogPayload(bytes memory payload) private view {
\t\tuint256 payloadLength = payload.length;
\t\taddress consoleAddress = CONSOLE_ADDRESS;
\t\tassembly {
\t\t\tlet payloadStart := add(payload, 32)
\t\t\tlet r := staticcall(gas(), consoleAddress, payloadStart, payloadLength, 0, 0)
\t\t}
\t}

\tfunction log() internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log()\"));
\t}

\tfunction logInt(int p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(int)\", p0));
\t}

\tfunction logUint(uint p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint)\", p0));
\t}

\tfunction logString(string memory p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string)\", p0));
\t}

\tfunction logBool(bool p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool)\", p0));
\t}

\tfunction logAddress(address p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address)\", p0));
\t}

\tfunction logBytes(bytes memory p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes)\", p0));
\t}

\tfunction logByte(byte p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(byte)\", p0));
\t}

\tfunction logBytes1(bytes1 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes1)\", p0));
\t}

\tfunction logBytes2(bytes2 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes2)\", p0));
\t}

\tfunction logBytes3(bytes3 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes3)\", p0));
\t}

\tfunction logBytes4(bytes4 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes4)\", p0));
\t}

\tfunction logBytes5(bytes5 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes5)\", p0));
\t}

\tfunction logBytes6(bytes6 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes6)\", p0));
\t}

\tfunction logBytes7(bytes7 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes7)\", p0));
\t}

\tfunction logBytes8(bytes8 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes8)\", p0));
\t}

\tfunction logBytes9(bytes9 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes9)\", p0));
\t}

\tfunction logBytes10(bytes10 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes10)\", p0));
\t}

\tfunction logBytes11(bytes11 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes11)\", p0));
\t}

\tfunction logBytes12(bytes12 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes12)\", p0));
\t}

\tfunction logBytes13(bytes13 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes13)\", p0));
\t}

\tfunction logBytes14(bytes14 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes14)\", p0));
\t}

\tfunction logBytes15(bytes15 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes15)\", p0));
\t}

\tfunction logBytes16(bytes16 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes16)\", p0));
\t}

\tfunction logBytes17(bytes17 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes17)\", p0));
\t}

\tfunction logBytes18(bytes18 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes18)\", p0));
\t}

\tfunction logBytes19(bytes19 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes19)\", p0));
\t}

\tfunction logBytes20(bytes20 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes20)\", p0));
\t}

\tfunction logBytes21(bytes21 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes21)\", p0));
\t}

\tfunction logBytes22(bytes22 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes22)\", p0));
\t}

\tfunction logBytes23(bytes23 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes23)\", p0));
\t}

\tfunction logBytes24(bytes24 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes24)\", p0));
\t}

\tfunction logBytes25(bytes25 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes25)\", p0));
\t}

\tfunction logBytes26(bytes26 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes26)\", p0));
\t}

\tfunction logBytes27(bytes27 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes27)\", p0));
\t}

\tfunction logBytes28(bytes28 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes28)\", p0));
\t}

\tfunction logBytes29(bytes29 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes29)\", p0));
\t}

\tfunction logBytes30(bytes30 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes30)\", p0));
\t}

\tfunction logBytes31(bytes31 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes31)\", p0));
\t}

\tfunction logBytes32(bytes32 p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bytes32)\", p0));
\t}

\tfunction log(uint p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint)\", p0));
\t}

\tfunction log(string memory p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string)\", p0));
\t}

\tfunction log(bool p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool)\", p0));
\t}

\tfunction log(address p0) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address)\", p0));
\t}

\tfunction log(uint p0, uint p1) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,uint)\", p0, p1));
\t}

\tfunction log(uint p0, string memory p1) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,string)\", p0, p1));
\t}

\tfunction log(uint p0, bool p1) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,bool)\", p0, p1));
\t}

\tfunction log(uint p0, address p1) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,address)\", p0, p1));
\t}

\tfunction log(string memory p0, uint p1) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,uint)\", p0, p1));
\t}

\tfunction log(string memory p0, string memory p1) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,string)\", p0, p1));
\t}

\tfunction log(string memory p0, bool p1) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,bool)\", p0, p1));
\t}

\tfunction log(string memory p0, address p1) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,address)\", p0, p1));
\t}

\tfunction log(bool p0, uint p1) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,uint)\", p0, p1));
\t}

\tfunction log(bool p0, string memory p1) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,string)\", p0, p1));
\t}

\tfunction log(bool p0, bool p1) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,bool)\", p0, p1));
\t}

\tfunction log(bool p0, address p1) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,address)\", p0, p1));
\t}

\tfunction log(address p0, uint p1) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,uint)\", p0, p1));
\t}

\tfunction log(address p0, string memory p1) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,string)\", p0, p1));
\t}

\tfunction log(address p0, bool p1) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,bool)\", p0, p1));
\t}

\tfunction log(address p0, address p1) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,address)\", p0, p1));
\t}

\tfunction log(uint p0, uint p1, uint p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,uint,uint)\", p0, p1, p2));
\t}

\tfunction log(uint p0, uint p1, string memory p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,uint,string)\", p0, p1, p2));
\t}

\tfunction log(uint p0, uint p1, bool p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,uint,bool)\", p0, p1, p2));
\t}

\tfunction log(uint p0, uint p1, address p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,uint,address)\", p0, p1, p2));
\t}

\tfunction log(uint p0, string memory p1, uint p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,string,uint)\", p0, p1, p2));
\t}

\tfunction log(uint p0, string memory p1, string memory p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,string,string)\", p0, p1, p2));
\t}

\tfunction log(uint p0, string memory p1, bool p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,string,bool)\", p0, p1, p2));
\t}

\tfunction log(uint p0, string memory p1, address p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,string,address)\", p0, p1, p2));
\t}

\tfunction log(uint p0, bool p1, uint p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,bool,uint)\", p0, p1, p2));
\t}

\tfunction log(uint p0, bool p1, string memory p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,bool,string)\", p0, p1, p2));
\t}

\tfunction log(uint p0, bool p1, bool p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,bool,bool)\", p0, p1, p2));
\t}

\tfunction log(uint p0, bool p1, address p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,bool,address)\", p0, p1, p2));
\t}

\tfunction log(uint p0, address p1, uint p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,address,uint)\", p0, p1, p2));
\t}

\tfunction log(uint p0, address p1, string memory p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,address,string)\", p0, p1, p2));
\t}

\tfunction log(uint p0, address p1, bool p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,address,bool)\", p0, p1, p2));
\t}

\tfunction log(uint p0, address p1, address p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,address,address)\", p0, p1, p2));
\t}

\tfunction log(string memory p0, uint p1, uint p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,uint,uint)\", p0, p1, p2));
\t}

\tfunction log(string memory p0, uint p1, string memory p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,uint,string)\", p0, p1, p2));
\t}

\tfunction log(string memory p0, uint p1, bool p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,uint,bool)\", p0, p1, p2));
\t}

\tfunction log(string memory p0, uint p1, address p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,uint,address)\", p0, p1, p2));
\t}

\tfunction log(string memory p0, string memory p1, uint p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,string,uint)\", p0, p1, p2));
\t}

\tfunction log(string memory p0, string memory p1, string memory p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,string,string)\", p0, p1, p2));
\t}

\tfunction log(string memory p0, string memory p1, bool p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,string,bool)\", p0, p1, p2));
\t}

\tfunction log(string memory p0, string memory p1, address p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,string,address)\", p0, p1, p2));
\t}

\tfunction log(string memory p0, bool p1, uint p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,bool,uint)\", p0, p1, p2));
\t}

\tfunction log(string memory p0, bool p1, string memory p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,bool,string)\", p0, p1, p2));
\t}

\tfunction log(string memory p0, bool p1, bool p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,bool,bool)\", p0, p1, p2));
\t}

\tfunction log(string memory p0, bool p1, address p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,bool,address)\", p0, p1, p2));
\t}

\tfunction log(string memory p0, address p1, uint p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,address,uint)\", p0, p1, p2));
\t}

\tfunction log(string memory p0, address p1, string memory p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,address,string)\", p0, p1, p2));
\t}

\tfunction log(string memory p0, address p1, bool p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,address,bool)\", p0, p1, p2));
\t}

\tfunction log(string memory p0, address p1, address p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,address,address)\", p0, p1, p2));
\t}

\tfunction log(bool p0, uint p1, uint p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,uint,uint)\", p0, p1, p2));
\t}

\tfunction log(bool p0, uint p1, string memory p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,uint,string)\", p0, p1, p2));
\t}

\tfunction log(bool p0, uint p1, bool p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,uint,bool)\", p0, p1, p2));
\t}

\tfunction log(bool p0, uint p1, address p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,uint,address)\", p0, p1, p2));
\t}

\tfunction log(bool p0, string memory p1, uint p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,string,uint)\", p0, p1, p2));
\t}

\tfunction log(bool p0, string memory p1, string memory p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,string,string)\", p0, p1, p2));
\t}

\tfunction log(bool p0, string memory p1, bool p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,string,bool)\", p0, p1, p2));
\t}

\tfunction log(bool p0, string memory p1, address p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,string,address)\", p0, p1, p2));
\t}

\tfunction log(bool p0, bool p1, uint p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,bool,uint)\", p0, p1, p2));
\t}

\tfunction log(bool p0, bool p1, string memory p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,bool,string)\", p0, p1, p2));
\t}

\tfunction log(bool p0, bool p1, bool p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,bool,bool)\", p0, p1, p2));
\t}

\tfunction log(bool p0, bool p1, address p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,bool,address)\", p0, p1, p2));
\t}

\tfunction log(bool p0, address p1, uint p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,address,uint)\", p0, p1, p2));
\t}

\tfunction log(bool p0, address p1, string memory p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,address,string)\", p0, p1, p2));
\t}

\tfunction log(bool p0, address p1, bool p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,address,bool)\", p0, p1, p2));
\t}

\tfunction log(bool p0, address p1, address p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,address,address)\", p0, p1, p2));
\t}

\tfunction log(address p0, uint p1, uint p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,uint,uint)\", p0, p1, p2));
\t}

\tfunction log(address p0, uint p1, string memory p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,uint,string)\", p0, p1, p2));
\t}

\tfunction log(address p0, uint p1, bool p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,uint,bool)\", p0, p1, p2));
\t}

\tfunction log(address p0, uint p1, address p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,uint,address)\", p0, p1, p2));
\t}

\tfunction log(address p0, string memory p1, uint p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,string,uint)\", p0, p1, p2));
\t}

\tfunction log(address p0, string memory p1, string memory p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,string,string)\", p0, p1, p2));
\t}

\tfunction log(address p0, string memory p1, bool p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,string,bool)\", p0, p1, p2));
\t}

\tfunction log(address p0, string memory p1, address p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,string,address)\", p0, p1, p2));
\t}

\tfunction log(address p0, bool p1, uint p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,bool,uint)\", p0, p1, p2));
\t}

\tfunction log(address p0, bool p1, string memory p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,bool,string)\", p0, p1, p2));
\t}

\tfunction log(address p0, bool p1, bool p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,bool,bool)\", p0, p1, p2));
\t}

\tfunction log(address p0, bool p1, address p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,bool,address)\", p0, p1, p2));
\t}

\tfunction log(address p0, address p1, uint p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,address,uint)\", p0, p1, p2));
\t}

\tfunction log(address p0, address p1, string memory p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,address,string)\", p0, p1, p2));
\t}

\tfunction log(address p0, address p1, bool p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,address,bool)\", p0, p1, p2));
\t}

\tfunction log(address p0, address p1, address p2) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,address,address)\", p0, p1, p2));
\t}

\tfunction log(uint p0, uint p1, uint p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,uint,uint,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, uint p1, uint p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,uint,uint,string)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, uint p1, uint p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,uint,uint,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, uint p1, uint p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,uint,uint,address)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, uint p1, string memory p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,uint,string,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, uint p1, string memory p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,uint,string,string)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, uint p1, string memory p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,uint,string,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, uint p1, string memory p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,uint,string,address)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, uint p1, bool p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,uint,bool,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, uint p1, bool p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,uint,bool,string)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, uint p1, bool p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,uint,bool,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, uint p1, bool p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,uint,bool,address)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, uint p1, address p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,uint,address,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, uint p1, address p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,uint,address,string)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, uint p1, address p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,uint,address,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, uint p1, address p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,uint,address,address)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, string memory p1, uint p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,string,uint,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, string memory p1, uint p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,string,uint,string)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, string memory p1, uint p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,string,uint,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, string memory p1, uint p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,string,uint,address)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, string memory p1, string memory p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,string,string,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, string memory p1, string memory p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,string,string,string)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, string memory p1, string memory p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,string,string,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, string memory p1, string memory p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,string,string,address)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, string memory p1, bool p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,string,bool,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, string memory p1, bool p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,string,bool,string)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, string memory p1, bool p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,string,bool,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, string memory p1, bool p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,string,bool,address)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, string memory p1, address p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,string,address,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, string memory p1, address p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,string,address,string)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, string memory p1, address p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,string,address,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, string memory p1, address p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,string,address,address)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, bool p1, uint p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,bool,uint,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, bool p1, uint p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,bool,uint,string)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, bool p1, uint p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,bool,uint,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, bool p1, uint p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,bool,uint,address)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, bool p1, string memory p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,bool,string,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, bool p1, string memory p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,bool,string,string)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, bool p1, string memory p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,bool,string,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, bool p1, string memory p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,bool,string,address)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, bool p1, bool p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,bool,bool,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, bool p1, bool p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,bool,bool,string)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, bool p1, bool p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,bool,bool,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, bool p1, bool p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,bool,bool,address)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, bool p1, address p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,bool,address,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, bool p1, address p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,bool,address,string)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, bool p1, address p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,bool,address,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, bool p1, address p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,bool,address,address)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, address p1, uint p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,address,uint,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, address p1, uint p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,address,uint,string)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, address p1, uint p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,address,uint,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, address p1, uint p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,address,uint,address)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, address p1, string memory p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,address,string,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, address p1, string memory p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,address,string,string)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, address p1, string memory p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,address,string,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, address p1, string memory p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,address,string,address)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, address p1, bool p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,address,bool,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, address p1, bool p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,address,bool,string)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, address p1, bool p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,address,bool,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, address p1, bool p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,address,bool,address)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, address p1, address p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,address,address,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, address p1, address p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,address,address,string)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, address p1, address p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,address,address,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(uint p0, address p1, address p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(uint,address,address,address)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, uint p1, uint p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,uint,uint,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, uint p1, uint p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,uint,uint,string)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, uint p1, uint p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,uint,uint,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, uint p1, uint p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,uint,uint,address)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, uint p1, string memory p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,uint,string,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, uint p1, string memory p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,uint,string,string)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, uint p1, string memory p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,uint,string,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, uint p1, string memory p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,uint,string,address)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, uint p1, bool p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,uint,bool,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, uint p1, bool p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,uint,bool,string)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, uint p1, bool p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,uint,bool,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, uint p1, bool p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,uint,bool,address)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, uint p1, address p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,uint,address,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, uint p1, address p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,uint,address,string)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, uint p1, address p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,uint,address,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, uint p1, address p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,uint,address,address)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, string memory p1, uint p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,string,uint,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, string memory p1, uint p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,string,uint,string)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, string memory p1, uint p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,string,uint,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, string memory p1, uint p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,string,uint,address)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, string memory p1, string memory p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,string,string,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, string memory p1, string memory p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,string,string,string)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, string memory p1, string memory p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,string,string,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, string memory p1, string memory p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,string,string,address)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, string memory p1, bool p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,string,bool,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, string memory p1, bool p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,string,bool,string)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, string memory p1, bool p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,string,bool,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, string memory p1, bool p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,string,bool,address)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, string memory p1, address p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,string,address,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, string memory p1, address p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,string,address,string)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, string memory p1, address p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,string,address,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, string memory p1, address p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,string,address,address)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, bool p1, uint p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,bool,uint,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, bool p1, uint p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,bool,uint,string)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, bool p1, uint p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,bool,uint,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, bool p1, uint p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,bool,uint,address)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, bool p1, string memory p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,bool,string,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, bool p1, string memory p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,bool,string,string)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, bool p1, string memory p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,bool,string,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, bool p1, string memory p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,bool,string,address)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, bool p1, bool p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,bool,bool,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, bool p1, bool p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,bool,bool,string)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, bool p1, bool p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,bool,bool,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, bool p1, bool p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,bool,bool,address)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, bool p1, address p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,bool,address,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, bool p1, address p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,bool,address,string)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, bool p1, address p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,bool,address,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, bool p1, address p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,bool,address,address)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, address p1, uint p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,address,uint,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, address p1, uint p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,address,uint,string)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, address p1, uint p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,address,uint,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, address p1, uint p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,address,uint,address)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, address p1, string memory p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,address,string,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, address p1, string memory p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,address,string,string)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, address p1, string memory p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,address,string,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, address p1, string memory p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,address,string,address)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, address p1, bool p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,address,bool,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, address p1, bool p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,address,bool,string)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, address p1, bool p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,address,bool,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, address p1, bool p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,address,bool,address)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, address p1, address p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,address,address,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, address p1, address p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,address,address,string)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, address p1, address p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,address,address,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(string memory p0, address p1, address p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(string,address,address,address)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, uint p1, uint p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,uint,uint,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, uint p1, uint p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,uint,uint,string)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, uint p1, uint p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,uint,uint,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, uint p1, uint p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,uint,uint,address)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, uint p1, string memory p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,uint,string,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, uint p1, string memory p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,uint,string,string)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, uint p1, string memory p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,uint,string,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, uint p1, string memory p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,uint,string,address)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, uint p1, bool p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,uint,bool,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, uint p1, bool p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,uint,bool,string)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, uint p1, bool p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,uint,bool,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, uint p1, bool p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,uint,bool,address)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, uint p1, address p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,uint,address,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, uint p1, address p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,uint,address,string)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, uint p1, address p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,uint,address,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, uint p1, address p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,uint,address,address)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, string memory p1, uint p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,string,uint,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, string memory p1, uint p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,string,uint,string)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, string memory p1, uint p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,string,uint,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, string memory p1, uint p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,string,uint,address)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, string memory p1, string memory p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,string,string,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, string memory p1, string memory p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,string,string,string)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, string memory p1, string memory p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,string,string,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, string memory p1, string memory p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,string,string,address)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, string memory p1, bool p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,string,bool,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, string memory p1, bool p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,string,bool,string)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, string memory p1, bool p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,string,bool,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, string memory p1, bool p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,string,bool,address)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, string memory p1, address p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,string,address,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, string memory p1, address p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,string,address,string)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, string memory p1, address p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,string,address,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, string memory p1, address p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,string,address,address)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, bool p1, uint p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,bool,uint,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, bool p1, uint p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,bool,uint,string)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, bool p1, uint p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,bool,uint,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, bool p1, uint p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,bool,uint,address)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, bool p1, string memory p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,bool,string,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, bool p1, string memory p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,bool,string,string)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, bool p1, string memory p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,bool,string,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, bool p1, string memory p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,bool,string,address)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, bool p1, bool p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,bool,bool,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, bool p1, bool p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,bool,bool,string)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, bool p1, bool p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,bool,bool,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, bool p1, bool p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,bool,bool,address)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, bool p1, address p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,bool,address,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, bool p1, address p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,bool,address,string)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, bool p1, address p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,bool,address,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, bool p1, address p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,bool,address,address)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, address p1, uint p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,address,uint,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, address p1, uint p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,address,uint,string)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, address p1, uint p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,address,uint,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, address p1, uint p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,address,uint,address)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, address p1, string memory p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,address,string,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, address p1, string memory p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,address,string,string)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, address p1, string memory p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,address,string,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, address p1, string memory p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,address,string,address)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, address p1, bool p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,address,bool,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, address p1, bool p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,address,bool,string)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, address p1, bool p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,address,bool,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, address p1, bool p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,address,bool,address)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, address p1, address p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,address,address,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, address p1, address p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,address,address,string)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, address p1, address p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,address,address,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(bool p0, address p1, address p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(bool,address,address,address)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, uint p1, uint p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,uint,uint,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, uint p1, uint p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,uint,uint,string)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, uint p1, uint p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,uint,uint,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, uint p1, uint p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,uint,uint,address)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, uint p1, string memory p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,uint,string,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, uint p1, string memory p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,uint,string,string)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, uint p1, string memory p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,uint,string,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, uint p1, string memory p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,uint,string,address)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, uint p1, bool p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,uint,bool,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, uint p1, bool p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,uint,bool,string)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, uint p1, bool p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,uint,bool,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, uint p1, bool p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,uint,bool,address)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, uint p1, address p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,uint,address,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, uint p1, address p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,uint,address,string)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, uint p1, address p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,uint,address,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, uint p1, address p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,uint,address,address)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, string memory p1, uint p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,string,uint,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, string memory p1, uint p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,string,uint,string)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, string memory p1, uint p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,string,uint,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, string memory p1, uint p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,string,uint,address)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, string memory p1, string memory p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,string,string,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, string memory p1, string memory p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,string,string,string)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, string memory p1, string memory p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,string,string,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, string memory p1, string memory p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,string,string,address)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, string memory p1, bool p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,string,bool,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, string memory p1, bool p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,string,bool,string)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, string memory p1, bool p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,string,bool,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, string memory p1, bool p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,string,bool,address)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, string memory p1, address p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,string,address,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, string memory p1, address p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,string,address,string)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, string memory p1, address p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,string,address,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, string memory p1, address p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,string,address,address)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, bool p1, uint p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,bool,uint,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, bool p1, uint p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,bool,uint,string)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, bool p1, uint p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,bool,uint,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, bool p1, uint p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,bool,uint,address)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, bool p1, string memory p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,bool,string,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, bool p1, string memory p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,bool,string,string)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, bool p1, string memory p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,bool,string,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, bool p1, string memory p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,bool,string,address)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, bool p1, bool p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,bool,bool,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, bool p1, bool p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,bool,bool,string)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, bool p1, bool p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,bool,bool,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, bool p1, bool p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,bool,bool,address)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, bool p1, address p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,bool,address,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, bool p1, address p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,bool,address,string)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, bool p1, address p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,bool,address,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, bool p1, address p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,bool,address,address)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, address p1, uint p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,address,uint,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, address p1, uint p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,address,uint,string)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, address p1, uint p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,address,uint,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, address p1, uint p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,address,uint,address)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, address p1, string memory p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,address,string,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, address p1, string memory p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,address,string,string)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, address p1, string memory p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,address,string,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, address p1, string memory p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,address,string,address)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, address p1, bool p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,address,bool,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, address p1, bool p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,address,bool,string)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, address p1, bool p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,address,bool,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, address p1, bool p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,address,bool,address)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, address p1, address p2, uint p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,address,address,uint)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, address p1, address p2, string memory p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,address,address,string)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, address p1, address p2, bool p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,address,address,bool)\", p0, p1, p2, p3));
\t}

\tfunction log(address p0, address p1, address p2, address p3) internal view {
\t\t_sendLogPayload(abi.encodeWithSignature(\"log(address,address,address,address)\", p0, p1, p2, p3));
\t}

}
"
    
pragma solidity ^0.5.0;

import \"hardhat/console.sol\";

import \"./TimedSwap.sol\";
import \"./CleverToken.sol\";

import \"@openzeppelin/contracts/utils/ReentrancyGuard.sol\";
import \"@openzeppelin/contracts/ownership/Ownable.sol\";

/**
 * @title CleverProtocol
 * @dev Extension of Crowdsale contract that increases the price of tokens linearly in time.
 * Note that what should be provided to the constructor is the addresses of mintable token and the TimedSwap contract
 */
contract CleverProtocol is ReentrancyGuard, Ownable {
    using SafeMath for uint256;
    
    //state variables
    uint256 private openingTime;
    uint256 private closingTime;
    uint256 private _interval;
    uint256 public fortnight;
    bool public isLive;
    uint256 MAX_SUPPLY;
    
    //set DECIMALS
    uint DECIMALS = 1e18;
    
    // The token being managed
    CleverToken private _cleverToken;
    TimedSwap private _timedSwap; // just for loading isOpen() and openingTime/interval
    address payable adminWallet;
    
    // Track Cycles completed
    uint256 private cycles;
    
    //Store the cycle varaibles
    mapping(uint256 => uint256) private cycleAwards;
    mapping(uint256 => uint256) private cycleBonus;
    mapping(uint256 => uint256) private payoutPercent;
    
    //track the addresses being paid out on a cycle basis
    mapping(uint256 => bool) private cyclePaid;
    mapping(uint256 => bool) private ETHflushed;
    
    
    /*
     * @dev Constructor, sets the addresses of token and TimedSwap
    */
    constructor (CleverToken _token, address payable _adminWallet)  public {
        adminWallet = _adminWallet;
        
        //set the on-chain contract addresses
        _cleverToken = CleverToken(_token);
        
        //set the state variables
        fortnight = 14;
        MAX_SUPPLY = uint256(1_000_000_000_000).mul(DECIMALS);
        
        //init the # of cycles
        cycles = 0;
        
        //setFundsRequired & cycleAwards
        //-- setting these in the constructor function gurantees they will not be manipulated
        setCycleAwards();
        setCycleBonus();
        
        //for admin fee
        setThePayoutPercentage();
        
        /*just for testing*/
        //openingTime = now.sub(30 minutes);
        //closingTime = now;
        
        
    }
    function() payable external {
        //here simply to get forwarded funds from timedswap
    }

    function checkIsLive() public returns(bool){
        require(now > openingTime, \"Timed Swap has not begun\");

        if(!_timedSwap.hasClosed()){
            return false;
        }else{
            isLive = true;
            return isLive;    
        }
        
    }
    
    function setTimedSwap(address payable _TimedSwap) public onlyOwner{
        //inits the timedSwap contract
        _timedSwap = TimedSwap(_TimedSwap);
        openingTime = _timedSwap.openingTime();
        closingTime = _timedSwap.closingTime();
        _interval = _timedSwap.interval();
    }
    function timedSwap() public view returns(address){
        return address(_timedSwap);
    }
    
    function distributeCycleAward() public nonReentrant returns (uint256){
        //update isLive
        require(checkIsLive(), \"Not live yet\");
        
        //calculate cycle
        uint256 cycle = getCycle();
        require(cycle > 0, \"cycle is not greater than 0\");
        
        if (cyclePaid[cycle]){
            revert(\"cycle has been paid already\");
        }
        
        uint256 in_application_cycle = cycle;
        
        //map the cycle -> range 
        if (cycle < 51) {
            in_application_cycle = cycle;
        } else if(cycle > 50 && cycle <101){
            in_application_cycle = 51;
        } else if (cycle > 100 && cycle <201) {
            in_application_cycle = 101;
        } else if (cycle > 201 && cycle < 401){
            in_application_cycle = 201;
        } else if (cycle > 400 && cycle < 601){
            in_application_cycle = 401;
        } else if (cycle > 600 && cycle < 801){
            in_application_cycle = 601;
        } else {
            in_application_cycle = 801;
        }
        
        //differentiation is made between cycle and in_application_cycle as the token will
        //only accept non repeating cycles, this is done to ensure only one payout is possible
        //per cycle

        uint256 cycleAwardPercentage = cycleAwards[in_application_cycle];
        uint256 cycleBonusPercentage = cycleBonus[in_application_cycle];

        //cycle award first, then the cycle bonus basedo newly established balance
        //call on the Coin to distribute set percentageo each wallet's holdings
        _cleverToken.distribute(cycle, cycleAwardPercentage, cycleBonusPercentage);

        
        
        
        //this check is made to prevent bricking the function in case of low balance amount within the contract
        if(address(this).balance >= 10000000) {
            //flush the ETH with the corresponding percentage for this cycle and update payment flag
            uint256 cyclepayoutPercent = getPayoutPercentage(in_application_cycle);

            adminWallet.transfer(address(this).balance.mul(cyclepayoutPercent).div(1e5));
            ETHflushed[cycle] = true;
        }

        //set this cycle to being paid
        cyclePaid[cycle] = true;
        
        //update cycles complete
        cycles = cycle;
        
        return cycles;
    }
    
    function getPayoutPercentage(uint256 _cycle) internal view returns(uint256){
        uint256 cycle;
        if( _cycle < 9){
            cycle = _cycle;
        } else{
            cycle = 8;
        }
        return payoutPercent[cycle];
    }
    
    /* returns floor of vision therefore giving the proper cycle #*/
    function getCycle() public view returns (uint256){
        //automatically determines if in isLive window
        require(isLive, \"The swapping phase must have ended! -- no cycles to return\");
        
        //auto reverts when day < 1st fortnight
        return ((getDay().sub(30)).div(fortnight));
    }
     
    function getDay() public view returns (uint256){
        require(now > openingTime, \"Swap has not begun yet\");
        return getElapsedTime().div(_interval); 
    }
    
    function getElapsedTime() public view returns (uint256){
        return block.timestamp.sub(openingTime);
    }
    function cyclesCompleted() public view returns (uint256) {
        return cycles;
    }
    function timeRestrictedWithdraw() public onlyOwner{
        require(cycles>8, \"It is too soon to be able to withdraw remaining ETH\");
        adminWallet.transfer(address(this).balance);
    }
    
    function token() public view returns(address){
        return address(_cleverToken);
    }
    function admin() public view returns(address payable){
        return adminWallet;
    }

    /*
    Called once on contract creation
    */
    function setThePayoutPercentage () internal {
        payoutPercent[1] = 45000;
        payoutPercent[2] = 40000;
        payoutPercent[3] = 35000;
        payoutPercent[4] = 30000;
        payoutPercent[5] = 20000;
        payoutPercent[6] = 10000;
        payoutPercent[7] = 5000;
        payoutPercent[8] = 4000;
    }
    function setCycleAwards() private {
        //0.01% as 10, 0.1% as 1e2, 1% as 1e3 ,10% as 1e4, 100% as 1e5
        cycleAwards[1] = 1e4;
        cycleAwards[2] = 5e3;
        cycleAwards[3] = 49e2;
        cycleAwards[4] = 48e2;
        cycleAwards[5] = 47e2;
        cycleAwards[6] = 46e2;
        cycleAwards[7] = 45e2;
        cycleAwards[8] = 44e2;
        cycleAwards[9] = 43e2;
        cycleAwards[10] = 42e2;
        cycleAwards[11] = 41e2;
        cycleAwards[12] = 40e2; //4.0%
        cycleAwards[13] = 39e2;
        cycleAwards[14] = 38e2;
        cycleAwards[15] = 37e2;
        cycleAwards[16] = 36e2;
        cycleAwards[17] = 35e2;
        cycleAwards[18] = 34e2;
        cycleAwards[19] = 33e2;
        cycleAwards[20] = 32e2;
        cycleAwards[21] = 31e2;
        cycleAwards[22] = 30e2;
        cycleAwards[23] = 29e2;
        cycleAwards[24] = 28e2;
        cycleAwards[25] = 27e2;
        cycleAwards[26] = 26e2;
        cycleAwards[27] = 25e2; //2.5%
        cycleAwards[28] = 24e2;
        cycleAwards[29] = 23e2;
        cycleAwards[30] = 22e2;
        cycleAwards[31] = 21e2;
        cycleAwards[32] = 20e2;
        cycleAwards[33] = 19e2;
        cycleAwards[34] = 18e2;
        cycleAwards[35] = 17e2;
        cycleAwards[36] = 16e2;
        cycleAwards[37] = 15e2;
        cycleAwards[38] = 14e2;
        cycleAwards[39] = 13e2;
        cycleAwards[40] = 12e2;
        cycleAwards[41] = 11e2;
        cycleAwards[42] = 10e2; //1%
        cycleAwards[43] = 950;
        cycleAwards[44] = 900;
        cycleAwards[45] = 850;
        cycleAwards[46] = 800;
        cycleAwards[47] = 750;
        cycleAwards[48] = 700;
        cycleAwards[49] = 650;
        cycleAwards[50] = 600;
        cycleAwards[51] = 500;
        cycleAwards[101] = 250;
        cycleAwards[201] = 200;
        cycleAwards[401] = 150;
        cycleAwards[601] = 100;
        cycleAwards[801] = 50; //%0.05%
    }
    function setCycleBonus() private {
        //0.01% as 10, 0.1% as 1e2, 1% as 1e3 ,10% as 1e4, 100% as 1e5
        cycleBonus[1] = 1e3;
        cycleBonus[2] = 1e3;
        cycleBonus[3] = 1e3;
        cycleBonus[4] = 1e3;
        cycleBonus[5] = 1e3;
        cycleBonus[6] = 1e3;
        cycleBonus[7] = 1e3;
        cycleBonus[8] = 1e3;
        cycleBonus[9] = 0;
        cycleBonus[10] = 0;
        cycleBonus[11] = 0;
        cycleBonus[12] = 0; 
        cycleBonus[13] = 0;
        cycleBonus[14] = 0;
        cycleBonus[15] = 0;
        cycleBonus[16] = 1e3;
        cycleBonus[17] = 0;
        cycleBonus[18] = 0;
        cycleBonus[19] = 0;
        cycleBonus[20] = 0;
        cycleBonus[21] = 0;
        cycleBonus[22] = 0;
        cycleBonus[23] = 0;
        cycleBonus[24] = 1e3;
        cycleBonus[25] = 0;
        cycleBonus[26] = 0;
        cycleBonus[27] = 0;
        cycleBonus[28] = 0;
        cycleBonus[29] = 0;
        cycleBonus[30] = 0;
        cycleBonus[31] = 0;
        cycleBonus[32] = 1e3;
        cycleBonus[33] = 0;
        cycleBonus[34] = 0;
        cycleBonus[35] = 0;
        cycleBonus[36] = 0;
        cycleBonus[37] = 0;
        cycleBonus[38] = 0;
        cycleBonus[39] = 0;
        cycleBonus[40] = 1e3;
        cycleBonus[41] = 0;
        cycleBonus[42] = 0; //1%
        cycleBonus[43] = 0;
        cycleBonus[44] = 0;
        cycleBonus[45] = 0;
        cycleBonus[46] = 0;
        cycleBonus[47] = 0;
        cycleBonus[48] = 1e3;
        cycleBonus[49] = 0;
        cycleBonus[50] = 0;
        cycleBonus[51] = 0;
        cycleBonus[101] = 0;
        cycleBonus[201] = 0;
        cycleBonus[401] = 0;
        cycleBonus[601] = 0;
        cycleBonus[801] = 0; 

    }
}
"
    
pragma solidity ^0.5.0;

/**
 * @dev Interface of the ERC20 standard as defined in the EIP. Does not include
 * the optional functions; to access them see {ERC20Detailed}.
 */
interface IERC20 {
    /**
     * @dev Returns the amount of tokens in existence.
     */
    function totalSupply() external view returns (uint256);

    /**
     * @dev Returns the amount of tokens owned by `account`.
     */
    function balanceOf(address account) external view returns (uint256);

    /**
     * @dev Moves `amount` tokens from the caller's account to `recipient`.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transfer(address recipient, uint256 amount) external returns (bool);

    /**
     * @dev Returns the remaining number of tokens that `spender` will be
     * allowed to spend on behalf of `owner` through {transferFrom}. This is
     * zero by default.
     *
     * This value changes when {approve} or {transferFrom} are called.
     */
    function allowance(address owner, address spender) external view returns (uint256);

    /**
     * @dev Sets `amount` as the allowance of `spender` over the caller's tokens.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * IMPORTANT: Beware that changing an allowance with this method brings the risk
     * that someone may use both the old and the new allowance by unfortunate
     * transaction ordering. One possible solution to mitigate this race
     * condition is to first reduce the spender's allowance to 0 and set the
     * desired value afterwards:
     * https://github.com/ethereum/EIPs/issues/20#issuecomment-263524729
     *
     * Emits an {Approval} event.
     */
    function approve(address spender, uint256 amount) external returns (bool);

    /**
     * @dev Moves `amount` tokens from `sender` to `recipient` using the
     * allowance mechanism. `amount` is then deducted from the caller's
     * allowance.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transferFrom(address sender, address recipient, uint256 amount) external returns (bool);

    /**
     * @dev Emitted when `value` tokens are moved from one account (`from`) to
     * another (`to`).
     *
     * Note that `value` may be zero.
     */
    event Transfer(address indexed from, address indexed to, uint256 value);

    /**
     * @dev Emitted when the allowance of a `spender` for an `owner` is set by
     * a call to {approve}. `value` is the new allowance.
     */
    event Approval(address indexed owner, address indexed spender, uint256 value);
}
"
    
pragma solidity ^0.5.0;

import \"../GSN/Context.sol\";
/**
 * @dev Contract module which provides a basic access control mechanism, where
 * there is an account (an owner) that can be granted exclusive access to
 * specific functions.
 *
 * This module is used through inheritance. It will make available the modifier
 * `onlyOwner`, which can be applied to your functions to restrict their use to
 * the owner.
 */
contract Ownable is Context {
    address private _owner;

    event OwnershipTransferred(address indexed previousOwner, address indexed newOwner);

    /**
     * @dev Initializes the contract setting the deployer as the initial owner.
     */
    constructor () internal {
        address msgSender = _msgSender();
        _owner = msgSender;
        emit OwnershipTransferred(address(0), msgSender);
    }

    /**
     * @dev Returns the address of the current owner.
     */
    function owner() public view returns (address) {
        return _owner;
    }

    /**
     * @dev Throws if called by any account other than the owner.
     */
    modifier onlyOwner() {
        require(isOwner(), \"Ownable: caller is not the owner\");
        _;
    }

    /**
     * @dev Returns true if the caller is the current owner.
     */
    function isOwner() public view returns (bool) {
        return _msgSender() == _owner;
    }

    /**
     * @dev Leaves the contract without owner. It will not be possible to call
     * `onlyOwner` functions anymore. Can only be called by the current owner.
     *
     * NOTE: Renouncing ownership will leave the contract without an owner,
     * thereby removing any functionality that is only available to the owner.
     */
    function renounceOwnership() public onlyOwner {
        emit OwnershipTransferred(_owner, address(0));
        _owner = address(0);
    }

    /**
     * @dev Transfers ownership of the contract to a new account (`newOwner`).
     * Can only be called by the current owner.
     */
    function transferOwnership(address newOwner) public onlyOwner {
        _transferOwnership(newOwner);
    }

    /**
     * @dev Transfers ownership of the contract to a new account (`newOwner`).
     */
    function _transferOwnership(address newOwner) internal {
        require(newOwner != address(0), \"Ownable: new owner is the zero address\");
        emit OwnershipTransferred(_owner, newOwner);
        _owner = newOwner;
    }
}
"
    
pragma solidity ^0.5.0;

import \"../../GSN/Context.sol\";
import \"../Roles.sol\";

contract MinterRole is Context {
    using Roles for Roles.Role;

    event MinterAdded(address indexed account);
    event MinterRemoved(address indexed account);

    Roles.Role private _minters;

    constructor () internal {
        _addMinter(_msgSender());
    }

    modifier onlyMinter() {
        require(isMinter(_msgSender()), \"MinterRole: caller does not have the Minter role\");
        _;
    }

    function isMinter(address account) public view returns (bool) {
        return _minters.has(account);
    }

    function addMinter(address account) public onlyMinter {
        _addMinter(account);
    }

    function renounceMinter() public {
        _removeMinter(_msgSender());
    }

    function _addMinter(address account) internal {
        _minters.add(account);
        emit MinterAdded(account);
    }

    function _removeMinter(address account) internal {
        _minters.remove(account);
        emit MinterRemoved(account);
    }
}
"
    
pragma solidity ^0.5.0;

/**
 * @dev Wrappers over Solidity's arithmetic operations with added overflow
 * checks.
 *
 * Arithmetic operations in Solidity wrap on overflow. This can easily result
 * in bugs, because programmers usually assume that an overflow raises an
 * error, which is the standard behavior in high level programming languages.
 * `SafeMath` restores this intuition by reverting the transaction when an
 * operation overflows.
 *
 * Using this library instead of the unchecked operations eliminates an entire
 * class of bugs, so it's recommended to use it always.
 */
library SafeMath {
    /**
     * @dev Returns the addition of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity's `+` operator.
     *
     * Requirements:
     * - Addition cannot overflow.
     */
    function add(uint256 a, uint256 b) internal pure returns (uint256) {
        uint256 c = a + b;
        require(c >= a, \"SafeMath: addition overflow\");

        return c;
    }

    /**
     * @dev Returns the subtraction of two unsigned integers, reverting on
     * overflow (when the result is negative).
     *
     * Counterpart to Solidity's `-` operator.
     *
     * Requirements:
     * - Subtraction cannot overflow.
     */
    function sub(uint256 a, uint256 b) internal pure returns (uint256) {
        return sub(a, b, \"SafeMath: subtraction overflow\");
    }

    /**
     * @dev Returns the subtraction of two unsigned integers, reverting with custom message on
     * overflow (when the result is negative).
     *
     * Counterpart to Solidity's `-` operator.
     *
     * Requirements:
     * - Subtraction cannot overflow.
     *
     * _Available since v2.4.0._
     */
    function sub(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b <= a, errorMessage);
        uint256 c = a - b;

        return c;
    }

    /**
     * @dev Returns the multiplication of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity's `*` operator.
     *
     * Requirements:
     * - Multiplication cannot overflow.
     */
    function mul(uint256 a, uint256 b) internal pure returns (uint256) {
        // Gas optimization: this is cheaper than requiring 'a' not being zero, but the
        // benefit is lost if 'b' is also tested.
        // See: https://github.com/OpenZeppelin/openzeppelin-contracts/pull/522
        if (a == 0) {
            return 0;
        }

        uint256 c = a * b;
        require(c / a == b, \"SafeMath: multiplication overflow\");

        return c;
    }

    /**
     * @dev Returns the integer division of two unsigned integers. Reverts on
     * division by zero. The result is rounded towards zero.
     *
     * Counterpart to Solidity's `/` operator. Note: this function uses a
     * `revert` opcode (which leaves remaining gas untouched) while Solidity
     * uses an invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     * - The divisor cannot be zero.
     */
    function div(uint256 a, uint256 b) internal pure returns (uint256) {
        return div(a, b, \"SafeMath: division by zero\");
    }

    /**
     * @dev Returns the integer division of two unsigned integers. Reverts with custom message on
     * division by zero. The result is rounded towards zero.
     *
     * Counterpart to Solidity's `/` operator. Note: this function uses a
     * `revert` opcode (which leaves remaining gas untouched) while Solidity
     * uses an invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     * - The divisor cannot be zero.
     *
     * _Available since v2.4.0._
     */
    function div(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        // Solidity only automatically asserts when dividing by 0
        require(b > 0, errorMessage);
        uint256 c = a / b;
        // assert(a == b * c + a % b); // There is no case in which this doesn't hold

        return c;
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
     * Reverts when dividing by zero.
     *
     * Counterpart to Solidity's `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     * - The divisor cannot be zero.
     */
    function mod(uint256 a, uint256 b) internal pure returns (uint256) {
        return mod(a, b, \"SafeMath: modulo by zero\");
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
     * Reverts with custom message when dividing by zero.
     *
     * Counterpart to Solidity's `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     * - The divisor cannot be zero.
     *
     * _Available since v2.4.0._
     */
    function mod(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b != 0, errorMessage);
        return a % b;
    }
}
"
    
pragma solidity ^0.5.0;

import \"hardhat/console.sol\";

import \"./CleverToken.sol\";
import \"./CleverProtocol.sol\";

import \"@openzeppelin/contracts/math/SafeMath.sol\";
import \"@openzeppelin/contracts/GSN/Context.sol\";
import \"@openzeppelin/contracts/token/ERC20/IERC20.sol\";
import \"@openzeppelin/contracts/token/ERC20/SafeERC20.sol\";
import \"@openzeppelin/contracts/utils/ReentrancyGuard.sol\";

/**
 * @title TimedSwap
 * @dev Extension of Crowdsale contract that increases the price of tokens
 * Note that what should be provided to the constructor is the address for the token and policy
 * established the amount of tokens per wei contributed. 
 */
contract TimedSwap is Context, ReentrancyGuard{
    using SafeMath for uint256;
    using SafeERC20 for CleverToken;

    //events
    event TokensPurchased(address indexed purchaser, address indexed beneficiary, uint256 value, uint256 amount);

    // Rates
    uint256 private _firstRate;
    uint256 private _secondRate;
    uint256 private _thirdRate;
    uint256 private _fourthRate;
    uint256 private _finalRate;
    
    // Windows
    uint256 private _firstWindow;
    uint256 private _secondWindow;
    uint256 private _thirdWindow;
    uint256 private _fourthWindow;
    uint256 private _finalWindow;

    // Times
    uint256 private _openingTime;
    uint256 private _closingTime;
    uint256 private _interval;

    // The token being sold
    CleverToken private _token;
    
    // Protocol to forward the funds
    address payable internal _protocol;

    // Amount of wei raised
    uint256 private _weiRaised;

    //modifiers
    modifier onlyWhileOpen {
        require(isOpen(), \"TimedSwap is not open\");
        _;
    }

    /*Fall back*/
    function () external payable {
        buyTokens(_msgSender());
    }

    /**
     * @dev Constructor, sets the rates of tokens received per wei contributed.
     */
    constructor  (address payable protocol, address token, uint256 open, bool daysFlag) public {

        //1e18 wei = 1 ETH, therefore 0.002 ETH = 2e15 or 20e14
        // Refer to _getTokenAmount() to clarify _Xrate usage

        // interval type set
        if(daysFlag) {
            _interval = (1 days);
        }else{ //mock minutes instead of days -- testing
            _interval = (1 minutes);    
        }
        
        // Set rates
        _firstRate = 500; //1e18 CLVA = 2e15 * _firstRate
        _secondRate = 476; //5% penalty
        _thirdRate = 455; //10% penalty
        _fourthRate = 435; //15% penalty
        _finalRate = 417; //20% penalty

        // Windows for time frames
        _firstWindow = uint(1).mul(_interval); //Day 1
        _secondWindow = uint(2).mul(_interval); //Day 2-3
        _thirdWindow = uint(4).mul(_interval); //Day 4-7
        _fourthWindow = uint(3).mul(_interval); //Day  8-10
        _finalWindow = uint(20).mul(_interval); //Day 11-30

        // Set times
        _openingTime = open; // unix time
        _closingTime = _openingTime + uint(30).mul(_interval);//30 days; //TODO: set the correct days
        require(_closingTime > _openingTime, \"Opening time is not before closing time\");

        // Setup the external contracts
        require(protocol != address(0), \"Crowdsale: wallet is the zero address\");
        require(address(token) != address(0), \"Crowdsale: token is the zero address\");
        
        _protocol = protocol;
        _token = CleverToken(token);
    }

    /**
     * @dev low level token purchase
     * This function has a non-reentrancy guard, so it shouldn't be called by
     * another `nonReentrant` function.
     * @param beneficiary Recipient of the token purchase
     */
    function buyTokens(address beneficiary) public nonReentrant onlyWhileOpen payable {
        uint256 weiAmount = msg.value;

        //makes sure sale is open from valid address with value sent
        _preValidatePurchase(beneficiary, weiAmount); 

        // calculate token amount to be created
        uint256 tokens = _getTokenAmount(weiAmount); 

        // update state
        _weiRaised = _weiRaised.add(weiAmount);

        //call internal deliver -- delivers tokens
        _processPurchase(beneficiary, tokens);

        //forward funds to the protocol address
        _forwardFunds();
        
        //emit purchase event
        emit TokensPurchased(_msgSender(), beneficiary, weiAmount, tokens);
    }


    /**
     * @dev Returns the rate of tokens per wei at the present time.
     * @return The number of tokens a buyer gets per wei at a given time
     */
    function getCurrentRate() public view returns (uint256) {
        require(now > openingTime(), \"Swapping has not begun\");
        require(isOpen(), \"The initial swapping has ended\");
        // solhint-disable-next-line not-rely-on-time
        uint256 elapsedTime = getElapsedTime();
        uint256 numberOfDaysElapsed = uint(elapsedTime).div(_interval);
        require (numberOfDaysElapsed < 30, \"The number of days passed exceeds the initial swap\");
        
        //find the window for the swap
        if (numberOfDaysElapsed < 1){
            require(elapsedTime < _firstWindow);
            //in the first window
            return _firstRate;
        } else if (numberOfDaysElapsed < 3) {
            require(elapsedTime < (_firstWindow + _secondWindow));
            //in second window
            return _secondRate;
        } else if (numberOfDaysElapsed < 7) {
            require(elapsedTime < (_firstWindow + _secondWindow + _thirdWindow));
            //in third window
            return _thirdRate;
        } else if (numberOfDaysElapsed < 10) {
            require(elapsedTime < (_firstWindow + _secondWindow + _thirdWindow + _fourthWindow));
            //in fourth window
            return _fourthRate;
        } else if (numberOfDaysElapsed < 30) {
            require(elapsedTime < (_firstWindow + _secondWindow + _thirdWindow + _fourthWindow + _finalWindow));
            //in final window
            return _finalRate;
        }
        revert(\"error with getting current rate; swap is not open\");
    }



    /*********************
    * INTERNAL FUNCTIONS *
    **********************/

    /**
     * @dev Overrides parent method taking into account variable rate.
     * @param weiAmount The value in wei to be converted into tokens
     * @return The number of tokens _weiAmount wei will buy at present time
     */
    function _getTokenAmount(uint256 weiAmount) internal view returns (uint256) {
        uint256 currentRate = getCurrentRate();
        return currentRate.mul(weiAmount);
    }

    /**
     * @dev Determines how ETH is stored/forwarded on purchases.
     */
    function _forwardFunds() internal {
        //send funds to the protocol
        _protocol.transfer(msg.value);
        
    }

    /**
     * @dev Overrides delivery by minting tokens upon purchase.
     * @param beneficiary Token purchaser
     * @param tokenAmount Number of tokens to be minted
     */
    function _deliverTokens(address beneficiary, uint256 tokenAmount) internal {
        // Potentially dangerous assumption about the type of the token.
        require(
            CleverToken(address(token())).mint(beneficiary, tokenAmount),
                \"MintedCrowdsale: minting failed\"
        );
    }

    /**
     * @dev Executed when a purchase has been validated and is ready to be executed. Doesn't necessarily emit/send
     * tokens.
     * @param beneficiary Address receiving the tokens
     * @param tokenAmount Number of tokens to be purchased
     */
    function _processPurchase(address beneficiary, uint256 tokenAmount) internal {
        _deliverTokens(beneficiary, tokenAmount);
    }

    /**
     * @dev Validation of an incoming purchase. Use require statements to revert state when conditions are not met.
     * Use `super` in contracts that inherit from Crowdsale to extend their validations.
     * Example from CappedCrowdsale.sol's _preValidatePurchase method:
     *     super._preValidatePurchase(beneficiary, weiAmount);
     *     require(weiRaised().add(weiAmount) <= cap);
     * @param beneficiary Address performing the token purchase
     * @param weiAmount Value in wei involved in the purchase
     */
    function _preValidatePurchase(address beneficiary, uint256 weiAmount) internal onlyWhileOpen view {
        require(beneficiary != address(0), \"Crowdsale: beneficiary is the zero address\");
        require(weiAmount != 0, \"Crowdsale: weiAmount is 0\");
        this; // silence state mutability warning without generating bytecode - see https://github.com/ethereum/solidity/issues/2691
    }

    /***************
    * PUBLIC UTILS *
    ****************/

    /**
     * @return the crowdsale opening time.
     */
    function openingTime() public view returns (uint256) {
        return _openingTime;
    }

    /**
     * @return the crowdsale closing time.
     */
    function closingTime() public view returns (uint256) {
        return _closingTime;
    }

    function interval() public view returns(uint256){
        return _interval;
    }

    /**
     * @return true if the crowdsale is open, false otherwise.
     */
    function isOpen() public view returns (bool) {
        // solhint-disable-next-line not-rely-on-time
        return block.timestamp >= _openingTime && block.timestamp <= _closingTime;
    }
    
    function getElapsedTime() public view returns (uint256){
        require(now > openingTime());
        return block.timestamp.sub(openingTime());
    }

    /**
     * @dev Checks whether the period in which the crowdsale is open has already elapsed.
     * @return Whether crowdsale period has elapsed
     */
    function hasClosed() public returns (bool) {
        // solhint-disable-next-line not-rely-on-time
        //renounce minting if 
        if(block.timestamp > _closingTime){
            if(token().isMinter(address(this))){

                //renounce minter role if this contract is still a minter and it has closed
                token().renounceMinter();

            }
            //and return true
            return true;
        }else{
            return false;
        }
        
    }

    /**
     * @return the token being sold.
     */
    function token() public view returns (CleverToken) {
        return _token;
    }

    /**
     * @return the address where funds are collected.
     */
    function wallet() public view returns (address payable) {
        return _protocol;
    }

    /**
     * @return the amount of wei raised.
     */
    function weiRaised() public view returns (uint256) {
        return _weiRaised;
    }

    /**
     * @return the initial rate of the swap
     */
    function initialRate() public view returns (uint256) {
        return _firstRate;
    }



}
"
    
pragma solidity ^0.5.0;

/**
 * @dev Contract module that helps prevent reentrant calls to a function.
 *
 * Inheriting from `ReentrancyGuard` will make the {nonReentrant} modifier
 * available, which can be applied to functions to make sure there are no nested
 * (reentrant) calls to them.
 *
 * Note that because there is a single `nonReentrant` guard, functions marked as
 * `nonReentrant` may not call one another. This can be worked around by making
 * those functions `private`, and then adding `external` `nonReentrant` entry
 * points to them.
 *
 * TIP: If you would like to learn more about reentrancy and alternative ways
 * to protect against it, check out our blog post
 * https://blog.openzeppelin.com/reentrancy-after-istanbul/[Reentrancy After Istanbul].
 *
 * _Since v2.5.0:_ this module is now much more gas efficient, given net gas
 * metering changes introduced in the Istanbul hardfork.
 */
contract ReentrancyGuard {
    bool private _notEntered;

    constructor () internal {
        // Storing an initial non-zero value makes deployment a bit more
        // expensive, but in exchange the refund on every call to nonReentrant
        // will be lower in amount. Since refunds are capped to a percetange of
        // the total transaction's gas, it is best to keep them low in cases
        // like this one, to increase the likelihood of the full refund coming
        // into effect.
        _notEntered = true;
    }

    /**
     * @dev Prevents a contract from calling itself, directly or indirectly.
     * Calling a `nonReentrant` function from another `nonReentrant`
     * function is not supported. It is possible to prevent this from happening
     * by making the `nonReentrant` function external, and make it call a
     * `private` function that does the actual work.
     */
    modifier nonReentrant() {
        // On the first call to nonReentrant, _notEntered will be true
        require(_notEntered, \"ReentrancyGuard: reentrant call\");

        // Any calls to nonReentrant after this point will fail
        _notEntered = false;

        _;

        // By storing the original value once again, a refund is triggered (see
        // https://eips.ethereum.org/EIPS/eip-2200)
        _notEntered = true;
    }
}
"
    
pragma solidity ^0.5.0;

/*
 * @dev Provides information about the current execution context, including the
 * sender of the transaction and its data. While these are generally available
 * via msg.sender and msg.data, they should not be accessed in such a direct
 * manner, since when dealing with GSN meta-transactions the account sending and
 * paying for execution may not be the actual sender (as far as an application
 * is concerned).
 *
 * This contract is only required for intermediate, library-like contracts.
 */
contract Context {
    // Empty internal constructor, to prevent people from mistakenly deploying
    // an instance of this contract, which should be used via inheritance.
    constructor () internal { }
    // solhint-disable-previous-line no-empty-blocks

    function _msgSender() internal view returns (address payable) {
        return msg.sender;
    }

    function _msgData() internal view returns (bytes memory) {
        this; // silence state mutability warning without generating bytecode - see https://github.com/ethereum/solidity/issues/2691
        return msg.data;
    }
}
"
    
pragma solidity ^0.5.0;

import \"./IERC20.sol\";
import \"../../math/SafeMath.sol\";
import \"../../utils/Address.sol\";

/**
 * @title SafeERC20
 * @dev Wrappers around ERC20 operations that throw on failure (when the token
 * contract returns false). Tokens that return no value (and instead revert or
 * throw on failure) are also supported, non-reverting calls are assumed to be
 * successful.
 * To use this library you can add a `using SafeERC20 for ERC20;` statement to your contract,
 * which allows you to call the safe operations as `token.safeTransfer(...)`, etc.
 */
library SafeERC20 {
    using SafeMath for uint256;
    using Address for address;

    function safeTransfer(IERC20 token, address to, uint256 value) internal {
        callOptionalReturn(token, abi.encodeWithSelector(token.transfer.selector, to, value));
    }

    function safeTransferFrom(IERC20 token, address from, address to, uint256 value) internal {
        callOptionalReturn(token, abi.encodeWithSelector(token.transferFrom.selector, from, to, value));
    }

    function safeApprove(IERC20 token, address spender, uint256 value) internal {
        // safeApprove should only be called when setting an initial allowance,
        // or when resetting it to zero. To increase and decrease it, use
        // 'safeIncreaseAllowance' and 'safeDecreaseAllowance'
        // solhint-disable-next-line max-line-length
        require((value == 0) || (token.allowance(address(this), spender) == 0),
            \"SafeERC20: approve from non-zero to non-zero allowance\"
        );
        callOptionalReturn(token, abi.encodeWithSelector(token.approve.selector, spender, value));
    }

    function safeIncreaseAllowance(IERC20 token, address spender, uint256 value) internal {
        uint256 newAllowance = token.allowance(address(this), spender).add(value);
        callOptionalReturn(token, abi.encodeWithSelector(token.approve.selector, spender, newAllowance));
    }

    function safeDecreaseAllowance(IERC20 token, address spender, uint256 value) internal {
        uint256 newAllowance = token.allowance(address(this), spender).sub(value, \"SafeERC20: decreased allowance below zero\");
        callOptionalReturn(token, abi.encodeWithSelector(token.approve.selector, spender, newAllowance));
    }

    /**
     * @dev Imitates a Solidity high-level call (i.e. a regular function call to a contract), relaxing the requirement
     * on the return value: the return value is optional (but if data is returned, it must not be false).
     * @param token The token targeted by the call.
     * @param data The call data (encoded using abi.encode or one of its variants).
     */
    function callOptionalReturn(IERC20 token, bytes memory data) private {
        // We need to perform a low level call here, to bypass Solidity's return data size checking mechanism, since
        // we're implementing it ourselves.

        // A Solidity high level call has three parts:
        //  1. The target address is checked to verify it contains contract code
        //  2. The call itself is made, and success asserted
        //  3. The return value is decoded, which in turn checks the size of the returned data.
        // solhint-disable-next-line max-line-length
        require(address(token).isContract(), \"SafeERC20: call to non-contract\");

        // solhint-disable-next-line avoid-low-level-calls
        (bool success, bytes memory returndata) = address(token).call(data);
        require(success, \"SafeERC20: low-level call failed\");

        if (returndata.length > 0) { // Return data is optional
            // solhint-disable-next-line max-line-length
            require(abi.decode(returndata, (bool)), \"SafeERC20: ERC20 operation did not succeed\");
        }
    }
}
"
    
pragma solidity ^0.5.0;

/**
 * @title Roles
 * @dev Library for managing addresses assigned to a Role.
 */
library Roles {
    struct Role {
        mapping (address => bool) bearer;
    }

    /**
     * @dev Give an account access to this role.
     */
    function add(Role storage role, address account) internal {
        require(!has(role, account), \"Roles: account already has role\");
        role.bearer[account] = true;
    }

    /**
     * @dev Remove an account's access to this role.
     */
    function remove(Role storage role, address account) internal {
        require(has(role, account), \"Roles: account does not have role\");
        role.bearer[account] = false;
    }

    /**
     * @dev Check if an account has this role.
     * @return bool
     */
    function has(Role storage role, address account) internal view returns (bool) {
        require(account != address(0), \"Roles: account is the zero address\");
        return role.bearer[account];
    }
}
"
    
pragma solidity ^0.5.5;

/**
 * @dev Collection of functions related to the address type
 */
library Address {
    /**
     * @dev Returns true if `account` is a contract.
     *
     * [IMPORTANT]
     * ====
     * It is unsafe to assume that an address for which this function returns
     * false is an externally-owned account (EOA) and not a contract.
     *
     * Among others, `isContract` will return false for the following 
     * types of addresses:
     *
     *  - an externally-owned account
     *  - a contract in construction
     *  - an address where a contract will be created
     *  - an address where a contract lived, but was destroyed
     * ====
     */
    function isContract(address account) internal view returns (bool) {
        // According to EIP-1052, 0x0 is the value returned for not-yet created accounts
        // and 0xc5d2460186f7233c927e7db2dcc703c0e500b653ca82273b7bfad8045d85a470 is returned
        // for accounts without code, i.e. `keccak256('')`
        bytes32 codehash;
        bytes32 accountHash = 0xc5d2460186f7233c927e7db2dcc703c0e500b653ca82273b7bfad8045d85a470;
        // solhint-disable-next-line no-inline-assembly
        assembly { codehash := extcodehash(account) }
        return (codehash != accountHash && codehash != 0x0);
    }

    /**
     * @dev Converts an `address` into `address payable`. Note that this is
     * simply a type cast: the actual underlying value is not changed.
     *
     * _Available since v2.4.0._
     */
    function toPayable(address account) internal pure returns (address payable) {
        return address(uint160(account));
    }

    /**
     * @dev Replacement for Solidity's `transfer`: sends `amount` wei to
     * `recipient`, forwarding all available gas and reverting on errors.
     *
     * https://eips.ethereum.org/EIPS/eip-1884[EIP1884] increases the gas cost
     * of certain opcodes, possibly making contracts go over the 2300 gas limit
     * imposed by `transfer`, making them unable to receive funds via
     * `transfer`. {sendValue} removes this limitation.
     *
     * https://diligence.consensys.net/posts/2019/09/stop-using-soliditys-transfer-now/[Learn more].
     *
     * IMPORTANT: because control is transferred to `recipient`, care must be
     * taken to not create reentrancy vulnerabilities. Consider using
     * {ReentrancyGuard} or the
     * https://solidity.readthedocs.io/en/v0.5.11/security-considerations.html#use-the-checks-effects-interactions-pattern[checks-effects-interactions pattern].
     *
     * _Available since v2.4.0._
     */
    function sendValue(address payable recipient, uint256 amount) internal {
        require(address(this).balance >= amount, \"Address: insufficient balance\");

        // solhint-disable-next-line avoid-call-value
        (bool success, ) = recipient.call.value(amount)(\"\");
        require(success, \"Address: unable to send value, recipient may have reverted\");
    }
}
"
    }
  
