/**

Exceed Infinity ($XFIN) || ExceedInfinity.io

By the people, for the people.
We invest and farm on the whole Defi ecosystem, bring infinity compound yield to $XFIN holders.

Features: 
- Treasury Wallet be protected by Multi-Sig Gnosis Safe.
- Ethereum Naming System setup up. (ExceedInfinity.eth)
- 100% LP will be locked on Team Finance at the launch.
- Contract will be renounced after the launch.
- No presale or team tokens
- Web3 DAPP publishing in hours after the launch.

Fairness Protection:
- First 2 blocks will be blacklisted to avoid Sniping Bot.
- Max transaction amount ?% - 2% of supply at the first 10 mins.
- Max Wallet Holding Amount is 2% of supply

Tax for Buying/Selling: 10%
- 4% of each transaction sent to holders as ETH rewards
- 3% of each transaction sent to Treasury Wallet
- 3% of each transaction sent to the Liquidity Pool

Socials:
Twitter: https://twitter.com/0xceedInfinity
Telegram: https://t.me/exceedinfinity
Website: https://exceedinfinity.io
Medium: https://medium.com/@0xceedInfinity/

*/

// SPDX-License-Identifier: MIT
pragma solidity =0.8.10 >=0.8.0 <0.9.0;
pragma experimental ABIEncoderV2;

/**
 * @dev Provides information about the current execution context, including the
 * sender of the transaction and its data. While these are generally available
 * via msg.sender and msg.data, they should not be accessed in such a direct
 * manner, since when dealing with meta-transactions the account sending and
 * paying for execution may not be the actual sender (as far as an application
 * is concerned).
 *
 * This contract is only required for intermediate, library-like contracts.
 */
abstract contract Context {
\tfunction _msgSender() internal view virtual returns (address) {
\t\treturn msg.sender;
\t}

\tfunction _msgData() internal view virtual returns (bytes calldata) {
\t\treturn msg.data;
\t}
}

/**
 * @dev Contract module which provides a basic access control mechanism, where
 * there is an account (an owner) that can be granted exclusive access to
 * specific functions.
 *
 * By default, the owner account will be the one that deploys the contract. This
 * can later be changed with {transferOwnership}.
 *
 * This module is used through inheritance. It will make available the modifier
 * `onlyOwner`, which can be applied to your functions to restrict their use to
 * the owner.
 */
abstract contract Ownable is Context {
\taddress private _owner;

\tevent OwnershipTransferred(address indexed previousOwner, address indexed newOwner);

\t/**
\t * @dev Initializes the contract setting the deployer as the initial owner.
\t */
\tconstructor() {
\t\t_transferOwnership(_msgSender());
\t}

\t/**
\t * @dev Returns the address of the current owner.
\t */
\tfunction owner() public view virtual returns (address) {
\t\treturn _owner;
\t}

\t/**
\t * @dev Throws if called by any account other than the owner.
\t */
\tmodifier onlyOwner() {
\t\trequire(owner() == _msgSender(), \"Ownable: caller is not the owner\");
\t\t_;
\t}

\t/**
\t * @dev Leaves the contract without owner. It will not be possible to call
\t * `onlyOwner` functions anymore. Can only be called by the current owner.
\t *
\t * NOTE: Renouncing ownership will leave the contract without an owner,
\t * thereby removing any functionality that is only available to the owner.
\t */
\tfunction renounceOwnership() public virtual onlyOwner {
\t\t_transferOwnership(address(0));
\t}

\t/**
\t * @dev Transfers ownership of the contract to a new account (`newOwner`).
\t * Can only be called by the current owner.
\t */
\tfunction transferOwnership(address newOwner) public virtual onlyOwner {
\t\trequire(newOwner != address(0), \"Ownable: new owner is the zero address\");
\t\t_transferOwnership(newOwner);
\t}

\t/**
\t * @dev Transfers ownership of the contract to a new account (`newOwner`).
\t * Internal function without access restriction.
\t */
\tfunction _transferOwnership(address newOwner) internal virtual {
\t\taddress oldOwner = _owner;
\t\t_owner = newOwner;
\t\temit OwnershipTransferred(oldOwner, newOwner);
\t}
}

/**
 * @dev Interface of the ERC20 standard as defined in the EIP.
 */
interface IERC20 {
\t/**
\t * @dev Returns the amount of tokens in existence.
\t */
\tfunction totalSupply() external view returns (uint256);

\t/**
\t * @dev Returns the amount of tokens owned by `account`.
\t */
\tfunction balanceOf(address account) external view returns (uint256);

\t/**
\t * @dev Moves `amount` tokens from the caller's account to `recipient`.
\t *
\t * Returns a boolean value indicating whether the operation succeeded.
\t *
\t * Emits a {Transfer} event.
\t */
\tfunction transfer(address recipient, uint256 amount) external returns (bool);

\t/**
\t * @dev Returns the remaining number of tokens that `spender` will be
\t * allowed to spend on behalf of `owner` through {transferFrom}. This is
\t * zero by default.
\t *
\t * This value changes when {approve} or {transferFrom} are called.
\t */
\tfunction allowance(address owner, address spender) external view returns (uint256);

\t/**
\t * @dev Sets `amount` as the allowance of `spender` over the caller's tokens.
\t *
\t * Returns a boolean value indicating whether the operation succeeded.
\t *
\t * IMPORTANT: Beware that changing an allowance with this method brings the risk
\t * that someone may use both the old and the new allowance by unfortunate
\t * transaction ordering. One possible solution to mitigate this race
\t * condition is to first reduce the spender's allowance to 0 and set the
\t * desired value afterwards:
\t * https://github.com/ethereum/EIPs/issues/20#issuecomment-263524729
\t *
\t * Emits an {Approval} event.
\t */
\tfunction approve(address spender, uint256 amount) external returns (bool);

\t/**
\t * @dev Moves `amount` tokens from `sender` to `recipient` using the
\t * allowance mechanism. `amount` is then deducted from the caller's
\t * allowance.
\t *
\t * Returns a boolean value indicating whether the operation succeeded.
\t *
\t * Emits a {Transfer} event.
\t */
\tfunction transferFrom(
\t\taddress sender,
\t\taddress recipient,
\t\tuint256 amount
\t) external returns (bool);

\t/**
\t * @dev Emitted when `value` tokens are moved from one account (`from`) to
\t * another (`to`).
\t *
\t * Note that `value` may be zero.
\t */
\tevent Transfer(address indexed from, address indexed to, uint256 value);

\t/**
\t * @dev Emitted when the allowance of a `spender` for an `owner` is set by
\t * a call to {approve}. `value` is the new allowance.
\t */
\tevent Approval(address indexed owner, address indexed spender, uint256 value);
}

/**
 * @dev Interface for the optional metadata functions from the ERC20 standard.
 *
 * _Available since v4.1._
 */
interface IERC20Metadata is IERC20 {
\t/**
\t * @dev Returns the name of the token.
\t */
\tfunction name() external view returns (string memory);

\t/**
\t * @dev Returns the symbol of the token.
\t */
\tfunction symbol() external view returns (string memory);

\t/**
\t * @dev Returns the decimals places of the token.
\t */
\tfunction decimals() external view returns (uint8);
}

/**
 * @dev Implementation of the {IERC20} interface.
 *
 * This implementation is agnostic to the way tokens are created. This means
 * that a supply mechanism has to be added in a derived contract using {_mint}.
 * For a generic mechanism see {ERC20PresetMinterPauser}.
 *
 * TIP: For a detailed writeup see our guide
 * https://forum.zeppelin.solutions/t/how-to-implement-erc20-supply-mechanisms/226[How
 * to implement supply mechanisms].
 *
 * We have followed general OpenZeppelin Contracts guidelines: functions revert
 * instead returning `false` on failure. This behavior is nonetheless
 * conventional and does not conflict with the expectations of ERC20
 * applications.
 *
 * Additionally, an {Approval} event is emitted on calls to {transferFrom}.
 * This allows applications to reconstruct the allowance for all accounts just
 * by listening to said events. Other implementations of the EIP may not emit
 * these events, as it isn't required by the specification.
 *
 * Finally, the non-standard {decreaseAllowance} and {increaseAllowance}
 * functions have been added to mitigate the well-known issues around setting
 * allowances. See {IERC20-approve}.
 */
contract ERC20 is Context, IERC20, IERC20Metadata {
\tmapping(address => uint256) private _balances;

\tmapping(address => mapping(address => uint256)) private _allowances;

\tuint256 private _totalSupply;

\tstring private _name;
\tstring private _symbol;

\t/**
\t * @dev Sets the values for {name} and {symbol}.
\t *
\t * The default value of {decimals} is 18. To select a different value for
\t * {decimals} you should overload it.
\t *
\t * All two of these values are immutable: they can only be set once during
\t * construction.
\t */
\tconstructor(string memory name_, string memory symbol_) {
\t\t_name = name_;
\t\t_symbol = symbol_;
\t}

\t/**
\t * @dev Returns the name of the token.
\t */
\tfunction name() public view virtual override returns (string memory) {
\t\treturn _name;
\t}

\t/**
\t * @dev Returns the symbol of the token, usually a shorter version of the
\t * name.
\t */
\tfunction symbol() public view virtual override returns (string memory) {
\t\treturn _symbol;
\t}

\t/**
\t * @dev Returns the number of decimals used to get its user representation.
\t * For example, if `decimals` equals `2`, a balance of `505` tokens should
\t * be displayed to a user as `5.05` (`505 / 10 ** 2`).
\t *
\t * Tokens usually opt for a value of 18, imitating the relationship between
\t * Ether and Wei. This is the value {ERC20} uses, unless this function is
\t * overridden;
\t *
\t * NOTE: This information is only used for _display_ purposes: it in
\t * no way affects any of the arithmetic of the contract, including
\t * {IERC20-balanceOf} and {IERC20-transfer}.
\t */
\tfunction decimals() public view virtual override returns (uint8) {
\t\treturn 18;
\t}

\t/**
\t * @dev See {IERC20-totalSupply}.
\t */
\tfunction totalSupply() public view virtual override returns (uint256) {
\t\treturn _totalSupply;
\t}

\t/**
\t * @dev See {IERC20-balanceOf}.
\t */
\tfunction balanceOf(address account) public view virtual override returns (uint256) {
\t\treturn _balances[account];
\t}

\t/**
\t * @dev See {IERC20-transfer}.
\t *
\t * Requirements:
\t *
\t * - `recipient` cannot be the zero address.
\t * - the caller must have a balance of at least `amount`.
\t */
\tfunction transfer(address recipient, uint256 amount) public virtual override returns (bool) {
\t\t_transfer(_msgSender(), recipient, amount);
\t\treturn true;
\t}

\t/**
\t * @dev See {IERC20-allowance}.
\t */
\tfunction allowance(address owner, address spender) public view virtual override returns (uint256) {
\t\treturn _allowances[owner][spender];
\t}

\t/**
\t * @dev See {IERC20-approve}.
\t *
\t * Requirements:
\t *
\t * - `spender` cannot be the zero address.
\t */
\tfunction approve(address spender, uint256 amount) public virtual override returns (bool) {
\t\t_approve(_msgSender(), spender, amount);
\t\treturn true;
\t}

\t/**
\t * @dev See {IERC20-transferFrom}.
\t *
\t * Emits an {Approval} event indicating the updated allowance. This is not
\t * required by the EIP. See the note at the beginning of {ERC20}.
\t *
\t * Requirements:
\t *
\t * - `sender` and `recipient` cannot be the zero address.
\t * - `sender` must have a balance of at least `amount`.
\t * - the caller must have allowance for ``sender``'s tokens of at least
\t * `amount`.
\t */
\tfunction transferFrom(
\t\taddress sender,
\t\taddress recipient,
\t\tuint256 amount
\t) public virtual override returns (bool) {
\t\t_transfer(sender, recipient, amount);

\t\tuint256 currentAllowance = _allowances[sender][_msgSender()];
\t\trequire(currentAllowance >= amount, \"ERC20: transfer amount exceeds allowance\");
\t\tunchecked {
\t\t\t_approve(sender, _msgSender(), currentAllowance - amount);
\t\t}

\t\treturn true;
\t}

\t/**
\t * @dev Atomically increases the allowance granted to `spender` by the caller.
\t *
\t * This is an alternative to {approve} that can be used as a mitigation for
\t * problems described in {IERC20-approve}.
\t *
\t * Emits an {Approval} event indicating the updated allowance.
\t *
\t * Requirements:
\t *
\t * - `spender` cannot be the zero address.
\t */
\tfunction increaseAllowance(address spender, uint256 addedValue) public virtual returns (bool) {
\t\t_approve(_msgSender(), spender, _allowances[_msgSender()][spender] + addedValue);
\t\treturn true;
\t}

\t/**
\t * @dev Atomically decreases the allowance granted to `spender` by the caller.
\t *
\t * This is an alternative to {approve} that can be used as a mitigation for
\t * problems described in {IERC20-approve}.
\t *
\t * Emits an {Approval} event indicating the updated allowance.
\t *
\t * Requirements:
\t *
\t * - `spender` cannot be the zero address.
\t * - `spender` must have allowance for the caller of at least
\t * `subtractedValue`.
\t */
\tfunction decreaseAllowance(address spender, uint256 subtractedValue) public virtual returns (bool) {
\t\tuint256 currentAllowance = _allowances[_msgSender()][spender];
\t\trequire(currentAllowance >= subtractedValue, \"ERC20: decreased allowance below zero\");
\t\tunchecked {
\t\t\t_approve(_msgSender(), spender, currentAllowance - subtractedValue);
\t\t}

\t\treturn true;
\t}

\t/**
\t * @dev Moves `amount` of tokens from `sender` to `recipient`.
\t *
\t * This internal function is equivalent to {transfer}, and can be used to
\t * e.g. implement automatic token fees, slashing mechanisms, etc.
\t *
\t * Emits a {Transfer} event.
\t *
\t * Requirements:
\t *
\t * - `sender` cannot be the zero address.
\t * - `recipient` cannot be the zero address.
\t * - `sender` must have a balance of at least `amount`.
\t */
\tfunction _transfer(
\t\taddress sender,
\t\taddress recipient,
\t\tuint256 amount
\t) internal virtual {
\t\trequire(sender != address(0), \"ERC20: transfer from the zero address\");
\t\trequire(recipient != address(0), \"ERC20: transfer to the zero address\");

\t\t_beforeTokenTransfer(sender, recipient, amount);

\t\tuint256 senderBalance = _balances[sender];
\t\trequire(senderBalance >= amount, \"ERC20: transfer amount exceeds balance\");
\t\tunchecked {
\t\t\t_balances[sender] = senderBalance - amount;
\t\t}
\t\t_balances[recipient] += amount;

\t\temit Transfer(sender, recipient, amount);

\t\t_afterTokenTransfer(sender, recipient, amount);
\t}

\t/** @dev Creates `amount` tokens and assigns them to `account`, increasing
\t * the total supply.
\t *
\t * Emits a {Transfer} event with `from` set to the zero address.
\t *
\t * Requirements:
\t *
\t * - `account` cannot be the zero address.
\t */
\tfunction _mint(address account, uint256 amount) internal virtual {
\t\trequire(account != address(0), \"ERC20: mint to the zero address\");

\t\t_beforeTokenTransfer(address(0), account, amount);

\t\t_totalSupply += amount;
\t\t_balances[account] += amount;
\t\temit Transfer(address(0), account, amount);

\t\t_afterTokenTransfer(address(0), account, amount);
\t}

\t/**
\t * @dev Destroys `amount` tokens from `account`, reducing the
\t * total supply.
\t *
\t * Emits a {Transfer} event with `to` set to the zero address.
\t *
\t * Requirements:
\t *
\t * - `account` cannot be the zero address.
\t * - `account` must have at least `amount` tokens.
\t */
\tfunction _burn(address account, uint256 amount) internal virtual {
\t\trequire(account != address(0), \"ERC20: burn from the zero address\");

\t\t_beforeTokenTransfer(account, address(0), amount);

\t\tuint256 accountBalance = _balances[account];
\t\trequire(accountBalance >= amount, \"ERC20: burn amount exceeds balance\");
\t\tunchecked {
\t\t\t_balances[account] = accountBalance - amount;
\t\t}
\t\t_totalSupply -= amount;

\t\temit Transfer(account, address(0), amount);

\t\t_afterTokenTransfer(account, address(0), amount);
\t}

\t/**
\t * @dev Sets `amount` as the allowance of `spender` over the `owner` s tokens.
\t *
\t * This internal function is equivalent to `approve`, and can be used to
\t * e.g. set automatic allowances for certain subsystems, etc.
\t *
\t * Emits an {Approval} event.
\t *
\t * Requirements:
\t *
\t * - `owner` cannot be the zero address.
\t * - `spender` cannot be the zero address.
\t */
\tfunction _approve(
\t\taddress owner,
\t\taddress spender,
\t\tuint256 amount
\t) internal virtual {
\t\trequire(owner != address(0), \"ERC20: approve from the zero address\");
\t\trequire(spender != address(0), \"ERC20: approve to the zero address\");

\t\t_allowances[owner][spender] = amount;
\t\temit Approval(owner, spender, amount);
\t}

\t/**
\t * @dev Hook that is called before any transfer of tokens. This includes
\t * minting and burning.
\t *
\t * Calling conditions:
\t *
\t * - when `from` and `to` are both non-zero, `amount` of ``from``'s tokens
\t * will be transferred to `to`.
\t * - when `from` is zero, `amount` tokens will be minted for `to`.
\t * - when `to` is zero, `amount` of ``from``'s tokens will be burned.
\t * - `from` and `to` are never both zero.
\t *
\t * To learn more about hooks, head to xref:ROOT:extending-contracts.adoc#using-hooks[Using Hooks].
\t */
\tfunction _beforeTokenTransfer(
\t\taddress from,
\t\taddress to,
\t\tuint256 amount
\t) internal virtual {}

\t/**
\t * @dev Hook that is called after any transfer of tokens. This includes
\t * minting and burning.
\t *
\t * Calling conditions:
\t *
\t * - when `from` and `to` are both non-zero, `amount` of ``from``'s tokens
\t * has been transferred to `to`.
\t * - when `from` is zero, `amount` tokens have been minted for `to`.
\t * - when `to` is zero, `amount` of ``from``'s tokens have been burned.
\t * - `from` and `to` are never both zero.
\t *
\t * To learn more about hooks, head to xref:ROOT:extending-contracts.adoc#using-hooks[Using Hooks].
\t */
\tfunction _afterTokenTransfer(
\t\taddress from,
\t\taddress to,
\t\tuint256 amount
\t) internal virtual {}
}

/**
 * @dev Collection of functions related to the address type
 */
library Address {
\t/**
\t * @dev Returns true if `account` is a contract.
\t *
\t * [IMPORTANT]
\t * ====
\t * It is unsafe to assume that an address for which this function returns
\t * false is an externally-owned account (EOA) and not a contract.
\t *
\t * Among others, `isContract` will return false for the following
\t * types of addresses:
\t *
\t *  - an externally-owned account
\t *  - a contract in construction
\t *  - an address where a contract will be created
\t *  - an address where a contract lived, but was destroyed
\t * ====
\t */
\tfunction isContract(address account) internal view returns (bool) {
\t\t// This method relies on extcodesize, which returns 0 for contracts in
\t\t// construction, since the code is only stored at the end of the
\t\t// constructor execution.

\t\tuint256 size;
\t\tassembly {
\t\t\tsize := extcodesize(account)
\t\t}
\t\treturn size > 0;
\t}

\t/**
\t * @dev Replacement for Solidity's `transfer`: sends `amount` wei to
\t * `recipient`, forwarding all available gas and reverting on errors.
\t *
\t * https://eips.ethereum.org/EIPS/eip-1884[EIP1884] increases the gas cost
\t * of certain opcodes, possibly making contracts go over the 2300 gas limit
\t * imposed by `transfer`, making them unable to receive funds via
\t * `transfer`. {sendValue} removes this limitation.
\t *
\t * https://diligence.consensys.net/posts/2019/09/stop-using-soliditys-transfer-now/[Learn more].
\t *
\t * IMPORTANT: because control is transferred to `recipient`, care must be
\t * taken to not create reentrancy vulnerabilities. Consider using
\t * {ReentrancyGuard} or the
\t * https://solidity.readthedocs.io/en/v0.5.11/security-considerations.html#use-the-checks-effects-interactions-pattern[checks-effects-interactions pattern].
\t */
\tfunction sendValue(address payable recipient, uint256 amount) internal {
\t\trequire(address(this).balance >= amount, \"Address: insufficient balance\");

\t\t(bool success, ) = recipient.call{ value: amount }(\"\");
\t\trequire(success, \"Address: unable to send value, recipient may have reverted\");
\t}

\t/**
\t * @dev Performs a Solidity function call using a low level `call`. A
\t * plain `call` is an unsafe replacement for a function call: use this
\t * function instead.
\t *
\t * If `target` reverts with a revert reason, it is bubbled up by this
\t * function (like regular Solidity function calls).
\t *
\t * Returns the raw returned data. To convert to the expected return value,
\t * use https://solidity.readthedocs.io/en/latest/units-and-global-variables.html?highlight=abi.decode#abi-encoding-and-decoding-functions[`abi.decode`].
\t *
\t * Requirements:
\t *
\t * - `target` must be a contract.
\t * - calling `target` with `data` must not revert.
\t *
\t * _Available since v3.1._
\t */
\tfunction functionCall(address target, bytes memory data) internal returns (bytes memory) {
\t\treturn functionCall(target, data, \"Address: low-level call failed\");
\t}

\t/**
\t * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`], but with
\t * `errorMessage` as a fallback revert reason when `target` reverts.
\t *
\t * _Available since v3.1._
\t */
\tfunction functionCall(
\t\taddress target,
\t\tbytes memory data,
\t\tstring memory errorMessage
\t) internal returns (bytes memory) {
\t\treturn functionCallWithValue(target, data, 0, errorMessage);
\t}

\t/**
\t * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`],
\t * but also transferring `value` wei to `target`.
\t *
\t * Requirements:
\t *
\t * - the calling contract must have an ETH balance of at least `value`.
\t * - the called Solidity function must be `payable`.
\t *
\t * _Available since v3.1._
\t */
\tfunction functionCallWithValue(
\t\taddress target,
\t\tbytes memory data,
\t\tuint256 value
\t) internal returns (bytes memory) {
\t\treturn functionCallWithValue(target, data, value, \"Address: low-level call with value failed\");
\t}

\t/**
\t * @dev Same as {xref-Address-functionCallWithValue-address-bytes-uint256-}[`functionCallWithValue`], but
\t * with `errorMessage` as a fallback revert reason when `target` reverts.
\t *
\t * _Available since v3.1._
\t */
\tfunction functionCallWithValue(
\t\taddress target,
\t\tbytes memory data,
\t\tuint256 value,
\t\tstring memory errorMessage
\t) internal returns (bytes memory) {
\t\trequire(address(this).balance >= value, \"Address: insufficient balance for call\");
\t\trequire(isContract(target), \"Address: call to non-contract\");

\t\t(bool success, bytes memory returndata) = target.call{ value: value }(data);
\t\treturn verifyCallResult(success, returndata, errorMessage);
\t}

\t/**
\t * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`],
\t * but performing a static call.
\t *
\t * _Available since v3.3._
\t */
\tfunction functionStaticCall(address target, bytes memory data) internal view returns (bytes memory) {
\t\treturn functionStaticCall(target, data, \"Address: low-level static call failed\");
\t}

\t/**
\t * @dev Same as {xref-Address-functionCall-address-bytes-string-}[`functionCall`],
\t * but performing a static call.
\t *
\t * _Available since v3.3._
\t */
\tfunction functionStaticCall(
\t\taddress target,
\t\tbytes memory data,
\t\tstring memory errorMessage
\t) internal view returns (bytes memory) {
\t\trequire(isContract(target), \"Address: static call to non-contract\");

\t\t(bool success, bytes memory returndata) = target.staticcall(data);
\t\treturn verifyCallResult(success, returndata, errorMessage);
\t}

\t/**
\t * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`],
\t * but performing a delegate call.
\t *
\t * _Available since v3.4._
\t */
\tfunction functionDelegateCall(address target, bytes memory data) internal returns (bytes memory) {
\t\treturn functionDelegateCall(target, data, \"Address: low-level delegate call failed\");
\t}

\t/**
\t * @dev Same as {xref-Address-functionCall-address-bytes-string-}[`functionCall`],
\t * but performing a delegate call.
\t *
\t * _Available since v3.4._
\t */
\tfunction functionDelegateCall(
\t\taddress target,
\t\tbytes memory data,
\t\tstring memory errorMessage
\t) internal returns (bytes memory) {
\t\trequire(isContract(target), \"Address: delegate call to non-contract\");

\t\t(bool success, bytes memory returndata) = target.delegatecall(data);
\t\treturn verifyCallResult(success, returndata, errorMessage);
\t}

\t/**
\t * @dev Tool to verifies that a low level call was successful, and revert if it wasn't, either by bubbling the
\t * revert reason using the provided one.
\t *
\t * _Available since v4.3._
\t */
\tfunction verifyCallResult(
\t\tbool success,
\t\tbytes memory returndata,
\t\tstring memory errorMessage
\t) internal pure returns (bytes memory) {
\t\tif (success) {
\t\t\treturn returndata;
\t\t} else {
\t\t\t// Look for revert reason and bubble it up if present
\t\t\tif (returndata.length > 0) {
\t\t\t\t// The easiest way to bubble the revert reason is using memory via assembly

\t\t\t\tassembly {
\t\t\t\t\tlet returndata_size := mload(returndata)
\t\t\t\t\trevert(add(32, returndata), returndata_size)
\t\t\t\t}
\t\t\t} else {
\t\t\t\trevert(errorMessage);
\t\t\t}
\t\t}
\t}
}

/**
 * @dev Wrappers over Solidity's arithmetic operations.
 *
 * NOTE: `SafeMath` is generally not needed starting with Solidity 0.8, since the compiler
 * now has built in overflow checking.
 */
library SafeMath {
\t/**
\t * @dev Returns the addition of two unsigned integers, with an overflow flag.
\t *
\t * _Available since v3.4._
\t */
\tfunction tryAdd(uint256 a, uint256 b) internal pure returns (bool, uint256) {
\t\tunchecked {
\t\t\tuint256 c = a + b;
\t\t\tif (c < a) return (false, 0);
\t\t\treturn (true, c);
\t\t}
\t}

\t/**
\t * @dev Returns the substraction of two unsigned integers, with an overflow flag.
\t *
\t * _Available since v3.4._
\t */
\tfunction trySub(uint256 a, uint256 b) internal pure returns (bool, uint256) {
\t\tunchecked {
\t\t\tif (b > a) return (false, 0);
\t\t\treturn (true, a - b);
\t\t}
\t}

\t/**
\t * @dev Returns the multiplication of two unsigned integers, with an overflow flag.
\t *
\t * _Available since v3.4._
\t */
\tfunction tryMul(uint256 a, uint256 b) internal pure returns (bool, uint256) {
\t\tunchecked {
\t\t\t// Gas optimization: this is cheaper than requiring 'a' not being zero, but the
\t\t\t// benefit is lost if 'b' is also tested.
\t\t\t// See: https://github.com/OpenZeppelin/openzeppelin-contracts/pull/522
\t\t\tif (a == 0) return (true, 0);
\t\t\tuint256 c = a * b;
\t\t\tif (c / a != b) return (false, 0);
\t\t\treturn (true, c);
\t\t}
\t}

\t/**
\t * @dev Returns the division of two unsigned integers, with a division by zero flag.
\t *
\t * _Available since v3.4._
\t */
\tfunction tryDiv(uint256 a, uint256 b) internal pure returns (bool, uint256) {
\t\tunchecked {
\t\t\tif (b == 0) return (false, 0);
\t\t\treturn (true, a / b);
\t\t}
\t}

\t/**
\t * @dev Returns the remainder of dividing two unsigned integers, with a division by zero flag.
\t *
\t * _Available since v3.4._
\t */
\tfunction tryMod(uint256 a, uint256 b) internal pure returns (bool, uint256) {
\t\tunchecked {
\t\t\tif (b == 0) return (false, 0);
\t\t\treturn (true, a % b);
\t\t}
\t}

\t/**
\t * @dev Returns the addition of two unsigned integers, reverting on
\t * overflow.
\t *
\t * Counterpart to Solidity's `+` operator.
\t *
\t * Requirements:
\t *
\t * - Addition cannot overflow.
\t */
\tfunction add(uint256 a, uint256 b) internal pure returns (uint256) {
\t\treturn a + b;
\t}

\t/**
\t * @dev Returns the subtraction of two unsigned integers, reverting on
\t * overflow (when the result is negative).
\t *
\t * Counterpart to Solidity's `-` operator.
\t *
\t * Requirements:
\t *
\t * - Subtraction cannot overflow.
\t */
\tfunction sub(uint256 a, uint256 b) internal pure returns (uint256) {
\t\treturn a - b;
\t}

\t/**
\t * @dev Returns the multiplication of two unsigned integers, reverting on
\t * overflow.
\t *
\t * Counterpart to Solidity's `*` operator.
\t *
\t * Requirements:
\t *
\t * - Multiplication cannot overflow.
\t */
\tfunction mul(uint256 a, uint256 b) internal pure returns (uint256) {
\t\treturn a * b;
\t}

\t/**
\t * @dev Returns the integer division of two unsigned integers, reverting on
\t * division by zero. The result is rounded towards zero.
\t *
\t * Counterpart to Solidity's `/` operator.
\t *
\t * Requirements:
\t *
\t * - The divisor cannot be zero.
\t */
\tfunction div(uint256 a, uint256 b) internal pure returns (uint256) {
\t\treturn a / b;
\t}

\t/**
\t * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
\t * reverting when dividing by zero.
\t *
\t * Counterpart to Solidity's `%` operator. This function uses a `revert`
\t * opcode (which leaves remaining gas untouched) while Solidity uses an
\t * invalid opcode to revert (consuming all remaining gas).
\t *
\t * Requirements:
\t *
\t * - The divisor cannot be zero.
\t */
\tfunction mod(uint256 a, uint256 b) internal pure returns (uint256) {
\t\treturn a % b;
\t}

\t/**
\t * @dev Returns the subtraction of two unsigned integers, reverting with custom message on
\t * overflow (when the result is negative).
\t *
\t * CAUTION: This function is deprecated because it requires allocating memory for the error
\t * message unnecessarily. For custom revert reasons use {trySub}.
\t *
\t * Counterpart to Solidity's `-` operator.
\t *
\t * Requirements:
\t *
\t * - Subtraction cannot overflow.
\t */
\tfunction sub(
\t\tuint256 a,
\t\tuint256 b,
\t\tstring memory errorMessage
\t) internal pure returns (uint256) {
\t\tunchecked {
\t\t\trequire(b <= a, errorMessage);
\t\t\treturn a - b;
\t\t}
\t}

\t/**
\t * @dev Returns the integer division of two unsigned integers, reverting with custom message on
\t * division by zero. The result is rounded towards zero.
\t *
\t * Counterpart to Solidity's `/` operator. Note: this function uses a
\t * `revert` opcode (which leaves remaining gas untouched) while Solidity
\t * uses an invalid opcode to revert (consuming all remaining gas).
\t *
\t * Requirements:
\t *
\t * - The divisor cannot be zero.
\t */
\tfunction div(
\t\tuint256 a,
\t\tuint256 b,
\t\tstring memory errorMessage
\t) internal pure returns (uint256) {
\t\tunchecked {
\t\t\trequire(b > 0, errorMessage);
\t\t\treturn a / b;
\t\t}
\t}

\t/**
\t * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
\t * reverting with custom message when dividing by zero.
\t *
\t * CAUTION: This function is deprecated because it requires allocating memory for the error
\t * message unnecessarily. For custom revert reasons use {tryMod}.
\t *
\t * Counterpart to Solidity's `%` operator. This function uses a `revert`
\t * opcode (which leaves remaining gas untouched) while Solidity uses an
\t * invalid opcode to revert (consuming all remaining gas).
\t *
\t * Requirements:
\t *
\t * - The divisor cannot be zero.
\t */
\tfunction mod(
\t\tuint256 a,
\t\tuint256 b,
\t\tstring memory errorMessage
\t) internal pure returns (uint256) {
\t\tunchecked {
\t\t\trequire(b > 0, errorMessage);
\t\t\treturn a % b;
\t\t}
\t}
}

////// src/IUniswapV2Factory.sol
/* pragma solidity 0.8.10; */
/* pragma experimental ABIEncoderV2; */

interface IUniswapV2Factory {
\tevent PairCreated(address indexed token0, address indexed token1, address pair, uint256);

\tfunction feeTo() external view returns (address);

\tfunction feeToSetter() external view returns (address);

\tfunction getPair(address tokenA, address tokenB) external view returns (address pair);

\tfunction allPairs(uint256) external view returns (address pair);

\tfunction allPairsLength() external view returns (uint256);

\tfunction createPair(address tokenA, address tokenB) external returns (address pair);

\tfunction setFeeTo(address) external;

\tfunction setFeeToSetter(address) external;
}

////// src/IUniswapV2Pair.sol
/* pragma solidity 0.8.10; */
/* pragma experimental ABIEncoderV2; */

interface IUniswapV2Pair {
\tevent Approval(address indexed owner, address indexed spender, uint256 value);
\tevent Transfer(address indexed from, address indexed to, uint256 value);

\tfunction name() external pure returns (string memory);

\tfunction symbol() external pure returns (string memory);

\tfunction decimals() external pure returns (uint8);

\tfunction totalSupply() external view returns (uint256);

\tfunction balanceOf(address owner) external view returns (uint256);

\tfunction allowance(address owner, address spender) external view returns (uint256);

\tfunction approve(address spender, uint256 value) external returns (bool);

\tfunction transfer(address to, uint256 value) external returns (bool);

\tfunction transferFrom(
\t\taddress from,
\t\taddress to,
\t\tuint256 value
\t) external returns (bool);

\tfunction DOMAIN_SEPARATOR() external view returns (bytes32);

\tfunction PERMIT_TYPEHASH() external pure returns (bytes32);

\tfunction nonces(address owner) external view returns (uint256);

\tfunction permit(
\t\taddress owner,
\t\taddress spender,
\t\tuint256 value,
\t\tuint256 deadline,
\t\tuint8 v,
\t\tbytes32 r,
\t\tbytes32 s
\t) external;

\tevent Mint(address indexed sender, uint256 amount0, uint256 amount1);
\tevent Burn(address indexed sender, uint256 amount0, uint256 amount1, address indexed to);
\tevent Swap(address indexed sender, uint256 amount0In, uint256 amount1In, uint256 amount0Out, uint256 amount1Out, address indexed to);
\tevent Sync(uint112 reserve0, uint112 reserve1);

\tfunction MINIMUM_LIQUIDITY() external pure returns (uint256);

\tfunction factory() external view returns (address);

\tfunction token0() external view returns (address);

\tfunction token1() external view returns (address);

\tfunction getReserves()
\t\texternal
\t\tview
\t\treturns (
\t\t\tuint112 reserve0,
\t\t\tuint112 reserve1,
\t\t\tuint32 blockTimestampLast
\t\t);

\tfunction price0CumulativeLast() external view returns (uint256);

\tfunction price1CumulativeLast() external view returns (uint256);

\tfunction kLast() external view returns (uint256);

\tfunction mint(address to) external returns (uint256 liquidity);

\tfunction burn(address to) external returns (uint256 amount0, uint256 amount1);

\tfunction swap(
\t\tuint256 amount0Out,
\t\tuint256 amount1Out,
\t\taddress to,
\t\tbytes calldata data
\t) external;

\tfunction skim(address to) external;

\tfunction sync() external;

\tfunction initialize(address, address) external;
}

////// src/IUniswapV2Router02.sol
/* pragma solidity 0.8.10; */
/* pragma experimental ABIEncoderV2; */

interface IUniswapV2Router02 {
\tfunction factory() external pure returns (address);

\tfunction WETH() external pure returns (address);

\tfunction addLiquidity(
\t\taddress tokenA,
\t\taddress tokenB,
\t\tuint256 amountADesired,
\t\tuint256 amountBDesired,
\t\tuint256 amountAMin,
\t\tuint256 amountBMin,
\t\taddress to,
\t\tuint256 deadline
\t)
\t\texternal
\t\treturns (
\t\t\tuint256 amountA,
\t\t\tuint256 amountB,
\t\t\tuint256 liquidity
\t\t);

\tfunction addLiquidityETH(
\t\taddress token,
\t\tuint256 amountTokenDesired,
\t\tuint256 amountTokenMin,
\t\tuint256 amountETHMin,
\t\taddress to,
\t\tuint256 deadline
\t)
\t\texternal
\t\tpayable
\t\treturns (
\t\t\tuint256 amountToken,
\t\t\tuint256 amountETH,
\t\t\tuint256 liquidity
\t\t);

\tfunction swapExactTokensForTokensSupportingFeeOnTransferTokens(
\t\tuint256 amountIn,
\t\tuint256 amountOutMin,
\t\taddress[] calldata path,
\t\taddress to,
\t\tuint256 deadline
\t) external;

\tfunction swapExactETHForTokensSupportingFeeOnTransferTokens(
\t\tuint256 amountOutMin,
\t\taddress[] calldata path,
\t\taddress to,
\t\tuint256 deadline
\t) external payable;

\tfunction swapExactTokensForETHSupportingFeeOnTransferTokens(
\t\tuint256 amountIn,
\t\tuint256 amountOutMin,
\t\taddress[] calldata path,
\t\taddress to,
\t\tuint256 deadline
\t) external;
}

contract ExceedInfinity is Ownable, IERC20 {
\taddress UNISWAPROUTER = address(0x7a250d5630B4cF539739dF2C5dAcb4c659F2488D);
\taddress DEAD = 0x000000000000000000000000000000000000dEaD;
\taddress ZERO = 0x0000000000000000000000000000000000000000;

\tstring private _name = \"Exceed Infinity\";
\tstring private _symbol = \"XFIN\";

\tuint256 public treasuryFeeBPS = 300;
\tuint256 public liquidityFeeBPS = 300;
\tuint256 public dividendFeeBPS = 400;
\tuint256 public totalFeeBPS = 1000;

\tuint256 public swapTokensAtAmount = 100000 * (10**18);
\tuint256 public lastSwapTime;
\tbool swapAllToken = true;

\tbool public swapEnabled = true;
\tbool public taxEnabled = true;
\tbool public compoundingEnabled = true;

\tuint256 private _totalSupply;
\tbool private swapping;

\taddress marketingWallet;
\taddress liquidityWallet;

\tmapping(address => uint256) private _balances;
\tmapping(address => mapping(address => uint256)) private _allowances;
\tmapping(address => bool) private _isExcludedFromFees;
\tmapping(address => bool) public automatedMarketMakerPairs;
\tmapping(address => bool) private _whiteList;
\tmapping(address => bool) isBlacklisted;

\tevent SwapAndAddLiquidity(uint256 tokensSwapped, uint256 nativeReceived, uint256 tokensIntoLiquidity);
\tevent SendDividends(uint256 tokensSwapped, uint256 amount);
\tevent ExcludeFromFees(address indexed account, bool isExcluded);
\tevent SetAutomatedMarketMakerPair(address indexed pair, bool indexed value);
\tevent UpdateUniswapV2Router(address indexed newAddress, address indexed oldAddress);
\tevent SwapEnabled(bool enabled);
\tevent TaxEnabled(bool enabled);
\tevent CompoundingEnabled(bool enabled);
\tevent BlacklistEnabled(bool enabled);

\tDividendTracker public dividendTracker;
\tIUniswapV2Router02 public uniswapV2Router;

\taddress public uniswapV2Pair;

\tuint256 public maxTxBPS = 49;
\tuint256 public maxWalletBPS = 200;

\tbool isOpen = false;

\tmapping(address => bool) private _isExcludedFromMaxTx;
\tmapping(address => bool) private _isExcludedFromMaxWallet;

\tconstructor(
\t\taddress _marketingWallet,
\t\taddress _liquidityWallet,
\t\taddress[] memory whitelistAddress
\t) {
\t\tmarketingWallet = _marketingWallet;
\t\tliquidityWallet = _liquidityWallet;
\t\tincludeToWhiteList(whitelistAddress);

\t\tdividendTracker = new DividendTracker(address(this), UNISWAPROUTER);

\t\tIUniswapV2Router02 _uniswapV2Router = IUniswapV2Router02(UNISWAPROUTER);

\t\taddress _uniswapV2Pair = IUniswapV2Factory(_uniswapV2Router.factory()).createPair(address(this), _uniswapV2Router.WETH());

\t\tuniswapV2Router = _uniswapV2Router;
\t\tuniswapV2Pair = _uniswapV2Pair;

\t\t_setAutomatedMarketMakerPair(_uniswapV2Pair, true);

\t\tdividendTracker.excludeFromDividends(address(dividendTracker), true);
\t\tdividendTracker.excludeFromDividends(address(this), true);
\t\tdividendTracker.excludeFromDividends(owner(), true);
\t\tdividendTracker.excludeFromDividends(address(_uniswapV2Router), true);

\t\texcludeFromFees(owner(), true);
\t\texcludeFromFees(address(this), true);
\t\texcludeFromFees(address(dividendTracker), true);

\t\texcludeFromMaxTx(owner(), true);
\t\texcludeFromMaxTx(address(this), true);
\t\texcludeFromMaxTx(address(dividendTracker), true);

\t\texcludeFromMaxWallet(owner(), true);
\t\texcludeFromMaxWallet(address(this), true);
\t\texcludeFromMaxWallet(address(dividendTracker), true);

\t\t_mint(owner(), 10000000000 * (10**18));
\t}

\treceive() external payable {}

\tfunction name() public view returns (string memory) {
\t\treturn _name;
\t}

\tfunction symbol() public view returns (string memory) {
\t\treturn _symbol;
\t}

\tfunction decimals() public pure returns (uint8) {
\t\treturn 18;
\t}

\tfunction totalSupply() public view virtual override returns (uint256) {
\t\treturn _totalSupply;
\t}

\tfunction balanceOf(address account) public view virtual override returns (uint256) {
\t\treturn _balances[account];
\t}

\tfunction allowance(address owner, address spender) public view virtual override returns (uint256) {
\t\treturn _allowances[owner][spender];
\t}

\tfunction approve(address spender, uint256 amount) public virtual override returns (bool) {
\t\t_approve(_msgSender(), spender, amount);
\t\treturn true;
\t}

\tfunction increaseAllowance(address spender, uint256 addedValue) public returns (bool) {
\t\t_approve(_msgSender(), spender, _allowances[_msgSender()][spender] + addedValue);
\t\treturn true;
\t}

\tfunction decreaseAllowance(address spender, uint256 subtractedValue) public returns (bool) {
\t\tuint256 currentAllowance = _allowances[_msgSender()][spender];
\t\trequire(currentAllowance >= subtractedValue, \"ExceedInfinity: decreased allowance below zero\");
\t\t_approve(_msgSender(), spender, currentAllowance - subtractedValue);
\t\treturn true;
\t}

\tfunction transfer(address recipient, uint256 amount) public virtual override returns (bool) {
\t\t_transfer(_msgSender(), recipient, amount);
\t\treturn true;
\t}

\tfunction transferFrom(
\t\taddress sender,
\t\taddress recipient,
\t\tuint256 amount
\t) public virtual override returns (bool) {
\t\t_transfer(sender, recipient, amount);
\t\tuint256 currentAllowance = _allowances[sender][_msgSender()];
\t\trequire(currentAllowance >= amount, \"ExceedInfinity: transfer amount exceeds allowance\");
\t\t_approve(sender, _msgSender(), currentAllowance - amount);
\t\treturn true;
\t}

\tfunction openTrading() external onlyOwner {
\t\tisOpen = true;
\t}

\tfunction _transfer(
\t\taddress sender,
\t\taddress recipient,
\t\tuint256 amount
\t) internal {
\t\trequire(isOpen || sender == owner() || recipient == owner() || _whiteList[sender] || _whiteList[recipient], \"Not Open\");

\t\trequire(!isBlacklisted[sender], \"ExceedInfinity: Sender is blacklisted\");
\t\trequire(!isBlacklisted[recipient], \"ExceedInfinity: Recipient is blacklisted\");

\t\trequire(sender != address(0), \"ExceedInfinity: transfer from the zero address\");
\t\trequire(recipient != address(0), \"ExceedInfinity: transfer to the zero address\");

\t\tuint256 _maxTxAmount = (totalSupply() * maxTxBPS) / 10000;
\t\tuint256 _maxWallet = (totalSupply() * maxWalletBPS) / 10000;
\t\trequire(amount <= _maxTxAmount || _isExcludedFromMaxTx[sender], \"TX Limit Exceeded\");

\t\tif (sender != owner() && recipient != address(this) && recipient != address(DEAD) && recipient != uniswapV2Pair) {
\t\t\tuint256 currentBalance = balanceOf(recipient);
\t\t\trequire(_isExcludedFromMaxWallet[recipient] || (currentBalance + amount <= _maxWallet));
\t\t}

\t\tuint256 senderBalance = _balances[sender];
\t\trequire(senderBalance >= amount, \"ExceedInfinity: transfer amount exceeds balance\");

\t\tuint256 contractTokenBalance = balanceOf(address(this));
\t\tuint256 contractNativeBalance = address(this).balance;

\t\tbool canSwap = contractTokenBalance >= swapTokensAtAmount;

\t\tif (
\t\t\tswapEnabled && // True
\t\t\tcanSwap && // true
\t\t\t!swapping && // swapping=false !false true
\t\t\t!automatedMarketMakerPairs[sender] && // no swap on remove liquidity step 1 or DEX buy
\t\t\tsender != address(uniswapV2Router) && // no swap on remove liquidity step 2
\t\t\tsender != owner() &&
\t\t\trecipient != owner()
\t\t) {
\t\t\tswapping = true;

\t\t\tif (!swapAllToken) {
\t\t\t\tcontractTokenBalance = swapTokensAtAmount;
\t\t\t}
\t\t\t_executeSwap(contractTokenBalance, contractNativeBalance);

\t\t\tlastSwapTime = block.timestamp;
\t\t\tswapping = false;
\t\t}

\t\tbool takeFee;

\t\tif (sender == address(uniswapV2Pair) || recipient == address(uniswapV2Pair)) {
\t\t\ttakeFee = true;
\t\t}

\t\tif (_isExcludedFromFees[sender] || _isExcludedFromFees[recipient]) {
\t\t\ttakeFee = false;
\t\t}

\t\tif (swapping || !taxEnabled) {
\t\t\ttakeFee = false;
\t\t}

\t\tif (takeFee) {
\t\t\tuint256 fees = (amount * totalFeeBPS) / 10000;
\t\t\tamount -= fees;
\t\t\t_executeTransfer(sender, address(this), fees);
\t\t}

\t\t_executeTransfer(sender, recipient, amount);

\t\tdividendTracker.setBalance(payable(sender), balanceOf(sender));
\t\tdividendTracker.setBalance(payable(recipient), balanceOf(recipient));
\t}

\tfunction _executeTransfer(
\t\taddress sender,
\t\taddress recipient,
\t\tuint256 amount
\t) private {
\t\trequire(sender != address(0), \"ExceedInfinity: transfer from the zero address\");
\t\trequire(recipient != address(0), \"ExceedInfinity: transfer to the zero address\");
\t\tuint256 senderBalance = _balances[sender];
\t\trequire(senderBalance >= amount, \"ExceedInfinity: transfer amount exceeds balance\");
\t\t_balances[sender] = senderBalance - amount;
\t\t_balances[recipient] += amount;
\t\temit Transfer(sender, recipient, amount);
\t}

\tfunction _approve(
\t\taddress owner,
\t\taddress spender,
\t\tuint256 amount
\t) private {
\t\trequire(owner != address(0), \"ExceedInfinity: approve from the zero address\");
\t\trequire(spender != address(0), \"ExceedInfinity: approve to the zero address\");
\t\t_allowances[owner][spender] = amount;
\t\temit Approval(owner, spender, amount);
\t}

\tfunction _mint(address account, uint256 amount) private {
\t\trequire(account != address(0), \"ExceedInfinity: mint to the zero address\");
\t\t_totalSupply += amount;
\t\t_balances[account] += amount;
\t\temit Transfer(address(0), account, amount);
\t}

\tfunction _burn(address account, uint256 amount) private {
\t\trequire(account != address(0), \"ExceedInfinity: burn from the zero address\");
\t\tuint256 accountBalance = _balances[account];
\t\trequire(accountBalance >= amount, \"ExceedInfinity: burn amount exceeds balance\");
\t\t_balances[account] = accountBalance - amount;
\t\t_totalSupply -= amount;
\t\temit Transfer(account, address(0), amount);
\t}

\tfunction swapTokensForNative(uint256 tokens) private {
\t\taddress[] memory path = new address[](2);
\t\tpath[0] = address(this);
\t\tpath[1] = uniswapV2Router.WETH();
\t\t_approve(address(this), address(uniswapV2Router), tokens);
\t\tuniswapV2Router.swapExactTokensForETHSupportingFeeOnTransferTokens(
\t\t\ttokens,
\t\t\t0, // accept any amount of native
\t\t\tpath,
\t\t\taddress(this),
\t\t\tblock.timestamp
\t\t);
\t}

\tfunction addLiquidity(uint256 tokens, uint256 native) private {
\t\t_approve(address(this), address(uniswapV2Router), tokens);
\t\tuniswapV2Router.addLiquidityETH{ value: native }(
\t\t\taddress(this),
\t\t\ttokens,
\t\t\t0, // slippage unavoidable
\t\t\t0, // slippage unavoidable
\t\t\tliquidityWallet,
\t\t\tblock.timestamp
\t\t);
\t}

\tfunction includeToWhiteList(address[] memory _users) private {
\t\tfor (uint8 i = 0; i < _users.length; i++) {
\t\t\t_whiteList[_users[i]] = true;
\t\t}
\t}

\tfunction _executeSwap(uint256 tokens, uint256 native) private {
\t\tif (tokens <= 0) {
\t\t\treturn;
\t\t}

\t\tuint256 swapTokensMarketing;
\t\tif (address(marketingWallet) != address(0)) {
\t\t\tswapTokensMarketing = (tokens * treasuryFeeBPS) / totalFeeBPS;
\t\t}

\t\tuint256 swapTokensDividends;
\t\tif (dividendTracker.totalSupply() > 0) {
\t\t\tswapTokensDividends = (tokens * dividendFeeBPS) / totalFeeBPS;
\t\t}

\t\tuint256 tokensForLiquidity = tokens - swapTokensMarketing - swapTokensDividends;
\t\tuint256 swapTokensLiquidity = tokensForLiquidity / 2;
\t\tuint256 addTokensLiquidity = tokensForLiquidity - swapTokensLiquidity;
\t\tuint256 swapTokensTotal = swapTokensMarketing + swapTokensDividends + swapTokensLiquidity;

\t\tuint256 initNativeBal = address(this).balance;
\t\tswapTokensForNative(swapTokensTotal);
\t\tuint256 nativeSwapped = (address(this).balance - initNativeBal) + native;

\t\tuint256 nativeMarketing = (nativeSwapped * swapTokensMarketing) / swapTokensTotal;
\t\tuint256 nativeDividends = (nativeSwapped * swapTokensDividends) / swapTokensTotal;
\t\tuint256 nativeLiquidity = nativeSwapped - nativeMarketing - nativeDividends;

\t\tif (nativeMarketing > 0) {
\t\t\tpayable(marketingWallet).transfer(nativeMarketing);
\t\t}

\t\taddLiquidity(addTokensLiquidity, nativeLiquidity);
\t\temit SwapAndAddLiquidity(swapTokensLiquidity, nativeLiquidity, addTokensLiquidity);

\t\tif (nativeDividends > 0) {
\t\t\t(bool success, ) = address(dividendTracker).call{ value: nativeDividends }(\"\");
\t\t\tif (success) {
\t\t\t\temit SendDividends(swapTokensDividends, nativeDividends);
\t\t\t}
\t\t}
\t}

\tfunction excludeFromFees(address account, bool excluded) public onlyOwner {
\t\trequire(_isExcludedFromFees[account] != excluded, \"ExceedInfinity: account is already set to requested state\");
\t\t_isExcludedFromFees[account] = excluded;
\t\temit ExcludeFromFees(account, excluded);
\t}

\tfunction isExcludedFromFees(address account) public view returns (bool) {
\t\treturn _isExcludedFromFees[account];
\t}

\tfunction manualSendDividend(uint256 amount, address holder) external onlyOwner {
\t\tdividendTracker.manualSendDividend(amount, holder);
\t}

\tfunction excludeFromDividends(address account, bool excluded) public onlyOwner {
\t\tdividendTracker.excludeFromDividends(account, excluded);
\t}

\tfunction isExcludedFromDividends(address account) public view returns (bool) {
\t\treturn dividendTracker.isExcludedFromDividends(account);
\t}

\tfunction setWallet(address payable _marketingWallet, address payable _liquidityWallet) external onlyOwner {
\t\tmarketingWallet = _marketingWallet;
\t\tliquidityWallet = _liquidityWallet;
\t}

\tfunction setAutomatedMarketMakerPair(address pair, bool value) public onlyOwner {
\t\trequire(pair != uniswapV2Pair, \"ExceedInfinity: DEX pair can not be removed\");
\t\t_setAutomatedMarketMakerPair(pair, value);
\t}

\tfunction setFee(
\t\tuint256 _treasuryFee,
\t\tuint256 _liquidityFee,
\t\tuint256 _dividendFee
\t) external onlyOwner {
\t\ttreasuryFeeBPS = _treasuryFee;
\t\tliquidityFeeBPS = _liquidityFee;
\t\tdividendFeeBPS = _dividendFee;
\t\ttotalFeeBPS = _treasuryFee + _liquidityFee + _dividendFee;
\t}

\tfunction _setAutomatedMarketMakerPair(address pair, bool value) private {
\t\trequire(automatedMarketMakerPairs[pair] != value, \"ExceedInfinity: automated market maker pair is already set to that value\");
\t\tautomatedMarketMakerPairs[pair] = value;
\t\tif (value) {
\t\t\tdividendTracker.excludeFromDividends(pair, true);
\t\t}
\t\temit SetAutomatedMarketMakerPair(pair, value);
\t}

\tfunction updateUniswapV2Router(address newAddress) public onlyOwner {
\t\trequire(newAddress != address(uniswapV2Router), \"ExceedInfinity: the router is already set to the new address\");
\t\temit UpdateUniswapV2Router(newAddress, address(uniswapV2Router));
\t\tuniswapV2Router = IUniswapV2Router02(newAddress);
\t\taddress _uniswapV2Pair = IUniswapV2Factory(uniswapV2Router.factory()).createPair(address(this), uniswapV2Router.WETH());
\t\tuniswapV2Pair = _uniswapV2Pair;
\t}

\tfunction claim() public {
\t\tdividendTracker.processAccount(payable(_msgSender()));
\t}

\tfunction compound() public {
\t\trequire(compoundingEnabled, \"ExceedInfinity: compounding is not enabled\");
\t\tdividendTracker.compoundAccount(payable(_msgSender()));
\t}

\tfunction withdrawableDividendOf(address account) public view returns (uint256) {
\t\treturn dividendTracker.withdrawableDividendOf(account);
\t}

\tfunction withdrawnDividendOf(address account) public view returns (uint256) {
\t\treturn dividendTracker.withdrawnDividendOf(account);
\t}

\tfunction totalDividendsWithdrawn() public view returns (uint256) {
\t\treturn dividendTracker.totalDividendsWithdrawn();
\t}

\tfunction totalDividendsDistributed() public view returns (uint256) {
\t\treturn dividendTracker.totalDividendsDistributed();
\t}

\tfunction accumulativeDividendOf(address account) public view returns (uint256) {
\t\treturn dividendTracker.accumulativeDividendOf(account);
\t}

\tfunction getAccountInfo(address account)
\t\tpublic
\t\tview
\t\treturns (
\t\t\taddress,
\t\t\tuint256,
\t\t\tuint256,
\t\t\tuint256,
\t\t\tuint256
\t\t)
\t{
\t\treturn dividendTracker.getAccountInfo(account);
\t}

\tfunction getLastClaimTime(address account) public view returns (uint256) {
\t\treturn dividendTracker.getLastClaimTime(account);
\t}

\tfunction setSwapEnabled(bool _enabled) external onlyOwner {
\t\tswapEnabled = _enabled;
\t\temit SwapEnabled(_enabled);
\t}

\tfunction setTaxEnabled(bool _enabled) external onlyOwner {
\t\ttaxEnabled = _enabled;
\t\temit TaxEnabled(_enabled);
\t}

\tfunction setCompoundingEnabled(bool _enabled) external onlyOwner {
\t\tcompoundingEnabled = _enabled;
\t\temit CompoundingEnabled(_enabled);
\t}

\tfunction updateDividendSettings(
\t\tbool _swapEnabled,
\t\tuint256 _swapTokensAtAmount,
\t\tbool _swapAllToken
\t) external onlyOwner {
\t\tswapEnabled = _swapEnabled;
\t\tswapTokensAtAmount = _swapTokensAtAmount;
\t\tswapAllToken = _swapAllToken;
\t}

\tfunction setMaxTxBPS(uint256 bps) external onlyOwner {
\t\trequire(bps >= 75 && bps <= 10000, \"BPS must be between 75 and 10000\");
\t\tmaxTxBPS = bps;
\t}

\tfunction excludeFromMaxTx(address account, bool excluded) public onlyOwner {
\t\t_isExcludedFromMaxTx[account] = excluded;
\t}

\tfunction isExcludedFromMaxTx(address account) public view returns (bool) {
\t\treturn _isExcludedFromMaxTx[account];
\t}

\tfunction setMaxWalletBPS(uint256 bps) external onlyOwner {
\t\trequire(bps >= 175 && bps <= 10000, \"BPS must be between 175 and 10000\");
\t\tmaxWalletBPS = bps;
\t}

\tfunction excludeFromMaxWallet(address account, bool excluded) public onlyOwner {
\t\t_isExcludedFromMaxWallet[account] = excluded;
\t}

\tfunction isExcludedFromMaxWallet(address account) public view returns (bool) {
\t\treturn _isExcludedFromMaxWallet[account];
\t}

\tfunction rescueToken(address _token, uint256 _amount) external onlyOwner {
\t\tIERC20(_token).transfer(msg.sender, _amount);
\t}

\tfunction rescueETH(uint256 _amount) external onlyOwner {
\t\tpayable(msg.sender).transfer(_amount);
\t}

\tfunction blackList(address _user) public onlyOwner {
\t\trequire(!isBlacklisted[_user], \"user already blacklisted\");
\t\tisBlacklisted[_user] = true;
\t\t// events?
\t}

\tfunction removeFromBlacklist(address _user) public onlyOwner {
\t\trequire(isBlacklisted[_user], \"user already whitelisted\");
\t\tisBlacklisted[_user] = false;
\t\t//events?
\t}

\tfunction blackListMany(address[] memory _users) public onlyOwner {
\t\tfor (uint8 i = 0; i < _users.length; i++) {
\t\t\tisBlacklisted[_users[i]] = true;
\t\t}
\t}

\tfunction unBlackListMany(address[] memory _users) public onlyOwner {
\t\tfor (uint8 i = 0; i < _users.length; i++) {
\t\t\tisBlacklisted[_users[i]] = false;
\t\t}
\t}
}

contract DividendTracker is Ownable, IERC20 {
\taddress UNISWAPROUTER;

\tstring private _name = \"ExceedInfinity_DividendTracker\";
\tstring private _symbol = \"ExceedInfinity_DividendTracker\";

\tuint256 public lastProcessedIndex;

\tuint256 private _totalSupply;
\tmapping(address => uint256) private _balances;

\tuint256 private constant magnitude = 2**128;
\tuint256 public immutable minTokenBalanceForDividends;
\tuint256 private magnifiedDividendPerShare;
\tuint256 public totalDividendsDistributed;
\tuint256 public totalDividendsWithdrawn;

\taddress public tokenAddress;

\tmapping(address => bool) public excludedFromDividends;
\tmapping(address => int256) private magnifiedDividendCorrections;
\tmapping(address => uint256) private withdrawnDividends;
\tmapping(address => uint256) private lastClaimTimes;

\tevent DividendsDistributed(address indexed from, uint256 weiAmount);
\tevent DividendWithdrawn(address indexed to, uint256 weiAmount);
\tevent ExcludeFromDividends(address indexed account, bool excluded);
\tevent Claim(address indexed account, uint256 amount);
\tevent Compound(address indexed account, uint256 amount, uint256 tokens);

\tstruct AccountInfo {
\t\taddress account;
\t\tuint256 withdrawableDividends;
\t\tuint256 totalDividends;
\t\tuint256 lastClaimTime;
\t}

\tconstructor(address _tokenAddress, address _uniswapRouter) {
\t\tminTokenBalanceForDividends = 10000 * (10**18);
\t\ttokenAddress = _tokenAddress;
\t\tUNISWAPROUTER = _uniswapRouter;
\t}

\treceive() external payable {
\t\tdistributeDividends();
\t}

\tfunction distributeDividends() public payable {
\t\trequire(_totalSupply > 0);
\t\tif (msg.value > 0) {
\t\t\tmagnifiedDividendPerShare = magnifiedDividendPerShare + ((msg.value * magnitude) / _totalSupply);
\t\t\temit DividendsDistributed(msg.sender, msg.value);
\t\t\ttotalDividendsDistributed += msg.value;
\t\t}
\t}

\tfunction setBalance(address payable account, uint256 newBalance) external onlyOwner {
\t\tif (excludedFromDividends[account]) {
\t\t\treturn;
\t\t}
\t\tif (newBalance >= minTokenBalanceForDividends) {
\t\t\t_setBalance(account, newBalance);
\t\t} else {
\t\t\t_setBalance(account, 0);
\t\t}
\t}

\tfunction excludeFromDividends(address account, bool excluded) external onlyOwner {
\t\trequire(excludedFromDividends[account] != excluded, \"ExceedInfinity_DividendTracker: account already set to requested state\");
\t\texcludedFromDividends[account] = excluded;
\t\tif (excluded) {
\t\t\t_setBalance(account, 0);
\t\t} else {
\t\t\tuint256 newBalance = IERC20(tokenAddress).balanceOf(account);
\t\t\tif (newBalance >= minTokenBalanceForDividends) {
\t\t\t\t_setBalance(account, newBalance);
\t\t\t} else {
\t\t\t\t_setBalance(account, 0);
\t\t\t}
\t\t}
\t\temit ExcludeFromDividends(account, excluded);
\t}

\tfunction isExcludedFromDividends(address account) public view returns (bool) {
\t\treturn excludedFromDividends[account];
\t}

\tfunction manualSendDividend(uint256 amount, address holder) external onlyOwner {
\t\tuint256 contractETHBalance = address(this).balance;
\t\tpayable(holder).transfer(amount > 0 ? amount : contractETHBalance);
\t}

\tfunction _setBalance(address account, uint256 newBalance) internal {
\t\tuint256 currentBalance = _balances[account];
\t\tif (newBalance > currentBalance) {
\t\t\tuint256 addAmount = newBalance - currentBalance;
\t\t\t_mint(account, addAmount);
\t\t} else if (newBalance < currentBalance) {
\t\t\tuint256 subAmount = currentBalance - newBalance;
\t\t\t_burn(account, subAmount);
\t\t}
\t}

\tfunction _mint(address account, uint256 amount) private {
\t\trequire(account != address(0), \"ExceedInfinity_DividendTracker: mint to the zero address\");
\t\t_totalSupply += amount;
\t\t_balances[account] += amount;
\t\temit Transfer(address(0), account, amount);
\t\tmagnifiedDividendCorrections[account] = magnifiedDividendCorrections[account] - int256(magnifiedDividendPerShare * amount);
\t}

\tfunction _burn(address account, uint256 amount) private {
\t\trequire(account != address(0), \"ExceedInfinity_DividendTracker: burn from the zero address\");
\t\tuint256 accountBalance = _balances[account];
\t\trequire(accountBalance >= amount, \"ExceedInfinity_DividendTracker: burn amount exceeds balance\");
\t\t_balances[account] = accountBalance - amount;
\t\t_totalSupply -= amount;
\t\temit Transfer(account, address(0), amount);
\t\tmagnifiedDividendCorrections[account] = magnifiedDividendCorrections[account] + int256(magnifiedDividendPerShare * amount);
\t}

\tfunction processAccount(address payable account) public onlyOwner returns (bool) {
\t\tuint256 amount = _withdrawDividendOfUser(account);
\t\tif (amount > 0) {
\t\t\tlastClaimTimes[account] = block.timestamp;
\t\t\temit Claim(account, amount);
\t\t\treturn true;
\t\t}
\t\treturn false;
\t}

\tfunction _withdrawDividendOfUser(address payable account) private returns (uint256) {
\t\tuint256 _withdrawableDividend = withdrawableDividendOf(account);
\t\tif (_withdrawableDividend > 0) {
\t\t\twithdrawnDividends[account] += _withdrawableDividend;
\t\t\ttotalDividendsWithdrawn += _withdrawableDividend;
\t\t\temit DividendWithdrawn(account, _withdrawableDividend);
\t\t\t(bool success, ) = account.call{ value: _withdrawableDividend, gas: 3000 }(\"\");
\t\t\tif (!success) {
\t\t\t\twithdrawnDividends[account] -= _withdrawableDividend;
\t\t\t\ttotalDividendsWithdrawn -= _withdrawableDividend;
\t\t\t\treturn 0;
\t\t\t}
\t\t\treturn _withdrawableDividend;
\t\t}
\t\treturn 0;
\t}

\tfunction compoundAccount(address payable account) public onlyOwner returns (bool) {
\t\t(uint256 amount, uint256 tokens) = _compoundDividendOfUser(account);
\t\tif (amount > 0) {
\t\t\tlastClaimTimes[account] = block.timestamp;
\t\t\temit Compound(account, amount, tokens);
\t\t\treturn true;
\t\t}
\t\treturn false;
\t}

\tfunction _compoundDividendOfUser(address payable account) private returns (uint256, uint256) {
\t\tuint256 _withdrawableDividend = withdrawableDividendOf(account);
\t\tif (_withdrawableDividend > 0) {
\t\t\twithdrawnDividends[account] += _withdrawableDividend;
\t\t\ttotalDividendsWithdrawn += _withdrawableDividend;
\t\t\temit DividendWithdrawn(account, _withdrawableDividend);

\t\t\tIUniswapV2Router02 uniswapV2Router = IUniswapV2Router02(UNISWAPROUTER);

\t\t\taddress[] memory path = new address[](2);
\t\t\tpath[0] = uniswapV2Router.WETH();
\t\t\tpath[1] = address(tokenAddress);

\t\t\tbool success;
\t\t\tuint256 tokens;

\t\t\tuint256 initTokenBal = IERC20(tokenAddress).balanceOf(account);
\t\t\ttry uniswapV2Router.swapExactETHForTokensSupportingFeeOnTransferTokens{ value: _withdrawableDividend }(0, path, address(account), block.timestamp) {
\t\t\t\tsuccess = true;
\t\t\t\ttokens = IERC20(tokenAddress).balanceOf(account) - initTokenBal;
\t\t\t} catch Error(
\t\t\t\tstring memory /*err*/
\t\t\t) {
\t\t\t\tsuccess = false;
\t\t\t}

\t\t\tif (!success) {
\t\t\t\twithdrawnDividends[account] -= _withdrawableDividend;
\t\t\t\ttotalDividendsWithdrawn -= _withdrawableDividend;
\t\t\t\treturn (0, 0);
\t\t\t}

\t\t\treturn (_withdrawableDividend, tokens);
\t\t}
\t\treturn (0, 0);
\t}

\tfunction withdrawableDividendOf(address account) public view returns (uint256) {
\t\treturn accumulativeDividendOf(account) - withdrawnDividends[account];
\t}

\tfunction withdrawnDividendOf(address account) public view returns (uint256) {
\t\treturn withdrawnDividends[account];
\t}

\tfunction accumulativeDividendOf(address account) public view returns (uint256) {
\t\tint256 a = int256(magnifiedDividendPerShare * balanceOf(account));
\t\tint256 b = magnifiedDividendCorrections[account]; // this is an explicit int256 (signed)
\t\treturn uint256(a + b) / magnitude;
\t}

\tfunction getAccountInfo(address account)
\t\tpublic
\t\tview
\t\treturns (
\t\t\taddress,
\t\t\tuint256,
\t\t\tuint256,
\t\t\tuint256,
\t\t\tuint256
\t\t)
\t{
\t\tAccountInfo memory info;
\t\tinfo.account = account;
\t\tinfo.withdrawableDividends = withdrawableDividendOf(account);
\t\tinfo.totalDividends = accumulativeDividendOf(account);
\t\tinfo.lastClaimTime = lastClaimTimes[account];
\t\treturn (info.account, info.withdrawableDividends, info.totalDividends, info.lastClaimTime, totalDividendsWithdrawn);
\t}

\tfunction getLastClaimTime(address account) public view returns (uint256) {
\t\treturn lastClaimTimes[account];
\t}

\tfunction name() public view returns (string memory) {
\t\treturn _name;
\t}

\tfunction symbol() public view returns (string memory) {
\t\treturn _symbol;
\t}

\tfunction decimals() public pure returns (uint8) {
\t\treturn 18;
\t}

\tfunction totalSupply() public view override returns (uint256) {
\t\treturn _totalSupply;
\t}

\tfunction balanceOf(address account) public view override returns (uint256) {
\t\treturn _balances[account];
\t}

\tfunction transfer(address, uint256) public pure override returns (bool) {
\t\trevert(\"ExceedInfinity_DividendTracker: method not implemented\");
\t}

\tfunction allowance(address, address) public pure override returns (uint256) {
\t\trevert(\"ExceedInfinity_DividendTracker: method not implemented\");
\t}

\tfunction approve(address, uint256) public pure override returns (bool) {
\t\trevert(\"ExceedInfinity_DividendTracker: method not implemented\");
\t}

\tfunction transferFrom(
\t\taddress,
\t\taddress,
\t\tuint256
\t) public pure override returns (bool) {
\t\trevert(\"ExceedInfinity_DividendTracker: method not implemented\");
\t}
}
"
    }
  
