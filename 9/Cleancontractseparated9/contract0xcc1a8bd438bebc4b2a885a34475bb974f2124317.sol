// SPDX-License-Identifier: MIT

pragma solidity ^0.5.0;

/*
 * @dev Provides information about the current execution context, including the
 * sender of the transaction and its data. While these are generally available
 * via msg.sender and msg.data, they should not be accessed in such a direct
 * manner, since when dealing with GSN meta-transactions the account sending and
 * paying for execution may not be the actual sender (as far as an application
 * is concerned).
 *
 * This contract is only required for intermediate, library-like contracts.
 */
contract Context {
    function _msgSender() internal view returns (address payable) {
        return msg.sender;
    }

    function _msgData() internal view returns (bytes memory) {
        this; // silence state mutability warning without generating bytecode - see https://github.com/ethereum/solidity/issues/2691
        return msg.data;
    }
}
"
    
pragma solidity ^0.5.0;

import \"./base/ERC20.sol\";
import \"./base/ERC20Burnable.sol\";
import \"./base/ERC20Mintable.sol\";
import \"./base/ERC20Pausable.sol\";

contract WholeEarthCoinToken is ERC20, ERC20Burnable, ERC20Mintable, ERC20Pausable {
  string private _name = \"WholeEarthCoin\";
\tstring private _symbol = \"WEC\";
\tuint8 private _decimals = 18;

  constructor(address _initialAddress, uint256 totalSupply) public {
\t\t_mint(_initialAddress, totalSupply);
\t}

  /**
\t * @dev Returns the name of the token.
\t */
\tfunction name() public view returns (string memory) {
\t\treturn _name;
\t}

\t/**
\t * @dev Returns the symbol of the token, usually a shorter version of the
\t * name.
\t */
\tfunction symbol() public view returns (string memory) {
\t\treturn _symbol;
\t}

\t/**
\t * @dev Returns the number of decimals used to get its user representation.
\t * For example, if `decimals` equals `2`, a balance of `505` tokens should
\t * be displayed to a user as `5,05` (`505 / 10 ** 2`).
\t *
\t * Tokens usually opt for a value of 18, imitating the relationship between
\t * Ether and Wei.
\t *
\t * NOTE: This information is only used for _display_ purposes: it in
\t * no way affects any of the arithmetic of the contract, including
\t * {IERC20-balanceOf} and {IERC20-transfer}.
\t */
\tfunction decimals() public view returns (uint8) {
\t\treturn _decimals;
\t}
}
"
    
pragma solidity ^0.5.0;

import \"../interfaces/IERC20.sol\";
import \"../libraries/SafeMath.sol\";

/**
 * @dev Implementation of the {IERC20} interface.
 *
 * This implementation is agnostic to the way tokens are created. This means
 * that a supply mechanism has to be added in a derived contract using {_mint}.
 * For a generic mechanism see {ERC20Mintable}.
 *
 * TIP: For a detailed writeup see our guide
 * https://forum.zeppelin.solutions/t/how-to-implement-erc20-supply-mechanisms/226[How
 * to implement supply mechanisms].
 *
 * We have followed general OpenZeppelin guidelines: functions revert instead
 * of returning `false` on failure. This behavior is nonetheless conventional
 * and does not conflict with the expectations of ERC20 applications.
 *
 * Additionally, an {Approval} event is emitted on calls to {transferFrom}.
 * This allows applications to reconstruct the allowance for all accounts just
 * by listening to said events. Other implementations of the EIP may not emit
 * these events, as it isn't required by the specification.
 *
 * Finally, the non-standard {decreaseAllowance} and {increaseAllowance}
 * functions have been added to mitigate the well-known issues around setting
 * allowances. See {IERC20-approve}.
 */
contract ERC20 is IERC20 {
\tusing SafeMath for uint256;

\tmapping(address => uint256) private _balances;

\tmapping(address => mapping(address => uint256)) private _allowances;

\tuint256 private _totalSupply;

\t/**
\t * @dev See {IERC20-totalSupply}.
\t */
\tfunction totalSupply() public view returns (uint256) {
\t\treturn _totalSupply;
\t}

\t/**
\t * @dev See {IERC20-balanceOf}.
\t */
\tfunction balanceOf(address account) public view returns (uint256) {
\t\treturn _balances[account];
\t}

\t/**
\t * @dev See {IERC20-transfer}.
\t *
\t * Requirements:
\t *
\t * - `recipient` cannot be the zero address.
\t * - the caller must have a balance of at least `amount`.
\t */
\tfunction transfer(address recipient, uint256 amount) public returns (bool) {
\t\t_transfer(msg.sender, recipient, amount);
\t\treturn true;
\t}

\t/**
\t * @dev See {IERC20-allowance}.
\t */
\tfunction allowance(address owner, address spender)
\t\tpublic
\t\tview
\t\treturns (uint256)
\t{
\t\treturn _allowances[owner][spender];
\t}

\t/**
\t * @dev See {IERC20-approve}.
\t *
\t * Requirements:
\t *
\t * - `spender` cannot be the zero address.
\t */
\tfunction approve(address spender, uint256 amount) public returns (bool) {
\t\t_approve(msg.sender, spender, amount);
\t\treturn true;
\t}

\t/**
\t * @dev See {IERC20-transferFrom}.
\t *
\t * Emits an {Approval} event indicating the updated allowance. This is not
\t * required by the EIP. See the note at the beginning of {ERC20};
\t *
\t * Requirements:
\t * - `sender` and `recipient` cannot be the zero address.
\t * - `sender` must have a balance of at least `amount`.
\t * - the caller must have allowance for `sender`'s tokens of at least
\t * `amount`.
\t */
\tfunction transferFrom(address sender, address recipient, uint256 amount)
\t\tpublic
\t\treturns (bool)
\t{
\t\t_transfer(sender, recipient, amount);
\t\t_approve(
\t\t\tsender,
\t\t\tmsg.sender,
\t\t\t_allowances[sender][msg.sender].sub(
\t\t\t\tamount,
\t\t\t\t\"ERC20: transfer amount exceeds allowance\"
\t\t\t)
\t\t);
\t\treturn true;
\t}

\t/**
\t * @dev Atomically increases the allowance granted to `spender` by the caller.
\t *
\t * This is an alternative to {approve} that can be used as a mitigation for
\t * problems described in {IERC20-approve}.
\t *
\t * Emits an {Approval} event indicating the updated allowance.
\t *
\t * Requirements:
\t *
\t * - `spender` cannot be the zero address.
\t */
\tfunction increaseAllowance(address spender, uint256 addedValue)
\t\tpublic
\t\treturns (bool)
\t{
\t\t_approve(
\t\t\tmsg.sender,
\t\t\tspender,
\t\t\t_allowances[msg.sender][spender].add(addedValue)
\t\t);
\t\treturn true;
\t}

\t/**
\t * @dev Atomically decreases the allowance granted to `spender` by the caller.
\t *
\t * This is an alternative to {approve} that can be used as a mitigation for
\t * problems described in {IERC20-approve}.
\t *
\t * Emits an {Approval} event indicating the updated allowance.
\t *
\t * Requirements:
\t *
\t * - `spender` cannot be the zero address.
\t * - `spender` must have allowance for the caller of at least
\t * `subtractedValue`.
\t */
\tfunction decreaseAllowance(address spender, uint256 subtractedValue)
\t\tpublic
\t\treturns (bool)
\t{
\t\t_approve(
\t\t\tmsg.sender,
\t\t\tspender,
\t\t\t_allowances[msg.sender][spender].sub(
\t\t\t\tsubtractedValue,
\t\t\t\t\"ERC20: decreased allowance below zero\"
\t\t\t)
\t\t);
\t\treturn true;
\t}

\t/**
\t * @dev Moves tokens `amount` from `sender` to `recipient`.
\t *
\t * This is internal function is equivalent to {transfer}, and can be used to
\t * e.g. implement automatic token fees, slashing mechanisms, etc.
\t *
\t * Emits a {Transfer} event.
\t *
\t * Requirements:
\t *
\t * - `sender` cannot be the zero address.
\t * - `recipient` cannot be the zero address.
\t * - `sender` must have a balance of at least `amount`.
\t */
\tfunction _transfer(address sender, address recipient, uint256 amount)
\t\tinternal
\t{
\t\trequire(sender != address(0), \"ERC20: transfer from the zero address\");
\t\trequire(recipient != address(0), \"ERC20: transfer to the zero address\");

\t\t_balances[sender] = _balances[sender].sub(
\t\t\tamount,
\t\t\t\"ERC20: transfer amount exceeds balance\"
\t\t);
\t\t_balances[recipient] = _balances[recipient].add(amount);
\t\temit Transfer(sender, recipient, amount);
\t}

\t/** @dev Creates `amount` tokens and assigns them to `account`, increasing
\t * the total supply.
\t *
\t * Emits a {Transfer} event with `from` set to the zero address.
\t *
\t * Requirements
\t *
\t * - `to` cannot be the zero address.
\t */
\tfunction _mint(address account, uint256 amount) internal {
\t\trequire(account != address(0), \"ERC20: mint to the zero address\");

\t\t_totalSupply = _totalSupply.add(amount);
\t\t_balances[account] = _balances[account].add(amount);
\t\temit Transfer(address(0), account, amount);
\t}

\t/**
\t * @dev Destroys `amount` tokens from `account`, reducing the
\t * total supply.
\t *
\t * Emits a {Transfer} event with `to` set to the zero address.
\t *
\t * Requirements
\t *
\t * - `account` cannot be the zero address.
\t * - `account` must have at least `amount` tokens.
\t */
\tfunction _burn(address account, uint256 amount) internal {
\t\trequire(account != address(0), \"ERC20: burn from the zero address\");

\t\t_balances[account] = _balances[account].sub(
\t\t\tamount,
\t\t\t\"ERC20: burn amount exceeds balance\"
\t\t);
\t\t_totalSupply = _totalSupply.sub(amount);
\t\temit Transfer(account, address(0), amount);
\t}

\t/**
\t * @dev Sets `amount` as the allowance of `spender` over the `owner`s tokens.
\t *
\t * This is internal function is equivalent to `approve`, and can be used to
\t * e.g. set automatic allowances for certain subsystems, etc.
\t *
\t * Emits an {Approval} event.
\t *
\t * Requirements:
\t *
\t * - `owner` cannot be the zero address.
\t * - `spender` cannot be the zero address.
\t */
\tfunction _approve(address owner, address spender, uint256 amount) internal {
\t\trequire(owner != address(0), \"ERC20: approve from the zero address\");
\t\trequire(spender != address(0), \"ERC20: approve to the zero address\");

\t\t_allowances[owner][spender] = amount;
\t\temit Approval(owner, spender, amount);
\t}

\t/**
\t * @dev Destroys `amount` tokens from `account`.`amount` is then deducted
\t * from the caller's allowance.
\t *
\t * See {_burn} and {_approve}.
\t */
\tfunction _burnFrom(address account, uint256 amount) internal {
\t\t_burn(account, amount);
\t\t_approve(
\t\t\taccount,
\t\t\tmsg.sender,
\t\t\t_allowances[account][msg.sender].sub(
\t\t\t\tamount,
\t\t\t\t\"ERC20: burn amount exceeds allowance\"
\t\t\t)
\t\t);
\t}
}"
    
// SPDX-License-Identifier: MIT

pragma solidity ^0.5.0;

import \"../GSN/Context.sol\";
import \"./ERC20.sol\";

/**
 * @dev Extension of {ERC20} that allows token holders to destroy both their own
 * tokens and those that they have an allowance for, in a way that can be
 * recognized off-chain (via event analysis).
 */
contract ERC20Burnable is Context, ERC20 {
    using SafeMath for uint256;

    /**
     * @dev Destroys `amount` tokens from the caller.
     *
     * See {ERC20-_burn}.
     */
    function burn(uint256 amount) public {
        _burn(_msgSender(), amount);
    }

    /**
     * @dev Destroys `amount` tokens from `account`, deducting from the caller's
     * allowance.
     *
     * See {ERC20-_burn} and {ERC20-allowance}.
     *
     * Requirements:
     *
     * - the caller must have allowance for ``accounts``'s tokens of at least
     * `amount`.
     */
    function burnFrom(address account, uint256 amount) public {
        uint256 decreasedAllowance = allowance(account, _msgSender()).sub(amount, \"ERC20: burn amount exceeds allowance\");

        _approve(account, _msgSender(), decreasedAllowance);
        _burn(account, amount);
    }
}
"
    
pragma solidity ^0.5.0;

import \"./ERC20.sol\";
import \"../dependencies/Ownable.sol\";


/**
 * @dev Extension of {ERC20} that adds a set of accounts with the {MinterRole},
 * which have permission to mint (create) new tokens as they see fit.
 *
 * At construction, the deployer of the contract is the only minter.
 */
contract ERC20Mintable is ERC20, Ownable {
\t/**
\t * @dev See {ERC20-_mint}.
\t *
\t * Requirements:
\t *
\t * - the caller must have the {MinterRole}.
\t */
\tfunction mint(address account, uint256 amount)
\t\tpublic
\t\tonlyOwner
\t\treturns (bool)
\t{
\t\t_mint(account, amount);
\t\treturn true;
\t}
}"
    
pragma solidity ^0.5.0;
import \"./ERC20.sol\";
import \"../dependencies/Pausable.sol\";


/**
 * @title Pausable token
 * @dev ERC20 with pausable transfers and allowances.
 *
 * Useful if you want to stop trades until the end of a crowdsale, or have
 * an emergency switch for freezing all token transfers in the event of a large
 * bug.
 */
contract ERC20Pausable is ERC20, Pausable {
\tfunction transfer(address to, uint256 value)
\t\tpublic
\t\twhenNotPaused
\t\treturns (bool)
\t{
\t\treturn super.transfer(to, value);
\t}

\tfunction transferFrom(address from, address to, uint256 value)
\t\tpublic
\t\twhenNotPaused
\t\treturns (bool)
\t{
\t\treturn super.transferFrom(from, to, value);
\t}

\tfunction approve(address spender, uint256 value)
\t\tpublic
\t\twhenNotPaused
\t\treturns (bool)
\t{
\t\treturn super.approve(spender, value);
\t}

\tfunction increaseAllowance(address spender, uint256 addedValue)
\t\tpublic
\t\twhenNotPaused
\t\treturns (bool)
\t{
\t\treturn super.increaseAllowance(spender, addedValue);
\t}

\tfunction decreaseAllowance(address spender, uint256 subtractedValue)
\t\tpublic
\t\twhenNotPaused
\t\treturns (bool)
\t{
\t\treturn super.decreaseAllowance(spender, subtractedValue);
\t}
}"
    
pragma solidity ^0.5.0;


/**
 * @dev Contract module which provides a basic access control mechanism, where
 * there is an account (an owner) that can be granted exclusive access to
 * specific functions.
 *
 * This module is used through inheritance. It will make available the modifier
 * `onlyOwner`, which can be applied to your functions to restrict their use to
 * the owner.
 */
contract Ownable {
\taddress private _owner;

\tevent OwnershipTransferred(
\t\taddress indexed previousOwner,
\t\taddress indexed newOwner
\t);

\t/**
\t * @dev Initializes the contract setting the deployer as the initial owner.
\t */
\tconstructor() internal {
\t\t_owner = msg.sender;
\t\temit OwnershipTransferred(address(0), _owner);
\t}

\t/**
\t * @dev Returns the address of the current owner.
\t */
\tfunction owner() public view returns (address) {
\t\treturn _owner;
\t}

\t/**
\t * @dev Throws if called by any account other than the owner.
\t */
\tmodifier onlyOwner() {
\t\trequire(isOwner(), \"Ownable: caller is not the owner\");
\t\t_;
\t}

\t/**
\t * @dev Returns true if the caller is the current owner.
\t */
\tfunction isOwner() public view returns (bool) {
\t\treturn msg.sender == _owner;
\t}

\t/**
\t * @dev Leaves the contract without owner. It will not be possible to call
\t * `onlyOwner` functions anymore. Can only be called by the current owner.
\t *
\t * NOTE: Renouncing ownership will leave the contract without an owner,
\t * thereby removing any functionality that is only available to the owner.
\t */
\tfunction renounceOwnership() public onlyOwner {
\t\temit OwnershipTransferred(_owner, address(0));
\t\t_owner = address(0);
\t}

\t/**
\t * @dev Transfers ownership of the contract to a new account (`newOwner`).
\t * Can only be called by the current owner.
\t */
\tfunction transferOwnership(address newOwner) public onlyOwner {
\t\t_transferOwnership(newOwner);
\t}

\t/**
\t * @dev Transfers ownership of the contract to a new account (`newOwner`).
\t */
\tfunction _transferOwnership(address newOwner) internal {
\t\trequire(
\t\t\tnewOwner != address(0),
\t\t\t\"Ownable: new owner is the zero address\"
\t\t);
\t\temit OwnershipTransferred(_owner, newOwner);
\t\t_owner = newOwner;
\t}
}"
    
pragma solidity ^0.5.0;
import \"./Ownable.sol\";


/**
 * @dev Contract module which allows children to implement an emergency stop
 * mechanism that can be triggered by an authorized account.
 *
 * This module is used through inheritance. It will make available the
 * modifiers `whenNotPaused` and `whenPaused`, which can be applied to
 * the functions of your contract. Note that they will not be pausable by
 * simply including this module, only once the modifiers are put in place.
 */
contract Pausable is Ownable {
\t/**
\t * @dev Emitted when the pause is triggered by a pauser (`account`).
\t */
\tevent Paused(address account);

\t/**
\t * @dev Emitted when the pause is lifted by a pauser (`account`).
\t */
\tevent Unpaused(address account);

\tbool private _paused;

\t/**
\t * @dev Initializes the contract in unpaused state. Assigns the Pauser role
\t * to the deployer.
\t */
\tconstructor() internal {
\t\t_paused = false;
\t}

\t/**
\t * @dev Returns true if the contract is paused, and false otherwise.
\t */
\tfunction paused() public view returns (bool) {
\t\treturn _paused;
\t}

\t/**
\t * @dev Modifier to make a function callable only when the contract is not paused.
\t */
\tmodifier whenNotPaused() {
\t\trequire(!_paused, \"Pausable: paused\");
\t\t_;
\t}

\t/**
\t * @dev Modifier to make a function callable only when the contract is paused.
\t */
\tmodifier whenPaused() {
\t\trequire(_paused, \"Pausable: not paused\");
\t\t_;
\t}

\t/**
\t * @dev Called by a pauser to pause, triggers stopped state.
\t */
\tfunction pause() public onlyOwner whenNotPaused {
\t\t_paused = true;
\t\temit Paused(msg.sender);
\t}

\t/**
\t * @dev Called by a pauser to unpause, returns to normal state.
\t */
\tfunction unpause() public onlyOwner whenPaused {
\t\t_paused = false;
\t\temit Unpaused(msg.sender);
\t}
}"
    
pragma solidity ^0.5.0;


/**
 * @dev Interface of the ERC20 standard as defined in the EIP. Does not include
 * the optional functions; to access them see {ERC20Detailed}.
 */
interface IERC20 {
\t/**
\t * @dev Returns the amount of tokens in existence.
\t */
\tfunction totalSupply() external view returns (uint256);

\t/**
\t * @dev Returns the amount of tokens owned by `account`.
\t */
\tfunction balanceOf(address account) external view returns (uint256);

\t/**
\t * @dev Moves `amount` tokens from the caller's account to `recipient`.
\t *
\t * Returns a boolean value indicating whether the operation succeeded.
\t *
\t * Emits a {Transfer} event.
\t */
\tfunction transfer(address recipient, uint256 amount)
\t\texternal
\t\treturns (bool);

\t/**
\t * @dev Returns the remaining number of tokens that `spender` will be
\t * allowed to spend on behalf of `owner` through {transferFrom}. This is
\t * zero by default.
\t *
\t * This value changes when {approve} or {transferFrom} are called.
\t */
\tfunction allowance(address owner, address spender)
\t\texternal
\t\tview
\t\treturns (uint256);

\t/**
\t * @dev Sets `amount` as the allowance of `spender` over the caller's tokens.
\t *
\t * Returns a boolean value indicating whether the operation succeeded.
\t *
\t * IMPORTANT: Beware that changing an allowance with this method brings the risk
\t * that someone may use both the old and the new allowance by unfortunate
\t * transaction ordering. One possible solution to mitigate this race
\t * condition is to first reduce the spender's allowance to 0 and set the
\t * desired value afterwards:
\t * https://github.com/ethereum/EIPs/issues/20#issuecomment-263524729
\t *
\t * Emits an {Approval} event.
\t */
\tfunction approve(address spender, uint256 amount) external returns (bool);

\t/**
\t * @dev Moves `amount` tokens from `sender` to `recipient` using the
\t * allowance mechanism. `amount` is then deducted from the caller's
\t * allowance.
\t *
\t * Returns a boolean value indicating whether the operation succeeded.
\t *
\t * Emits a {Transfer} event.
\t */
\tfunction transferFrom(address sender, address recipient, uint256 amount)
\t\texternal
\t\treturns (bool);

\t/**
\t * @dev Emitted when `value` tokens are moved from one account (`from`) to
\t * another (`to`).
\t *
\t * Note that `value` may be zero.
\t */
\tevent Transfer(address indexed from, address indexed to, uint256 value);

\t/**
\t * @dev Emitted when the allowance of a `spender` for an `owner` is set by
\t * a call to {approve}. `value` is the new allowance.
\t */
\tevent Approval(
\t\taddress indexed owner,
\t\taddress indexed spender,
\t\tuint256 value
\t);
}"
    
pragma solidity ^0.5.0;


/**
 * @dev Wrappers over Solidity's arithmetic operations with added overflow
 * checks.
 *
 * Arithmetic operations in Solidity wrap on overflow. This can easily result
 * in bugs, because programmers usually assume that an overflow raises an
 * error, which is the standard behavior in high level programming languages.
 * `SafeMath` restores this intuition by reverting the transaction when an
 * operation overflows.
 *
 * Using this library instead of the unchecked operations eliminates an entire
 * class of bugs, so it's recommended to use it always.
 */
library SafeMath {
\t/**
\t * @dev Returns the addition of two unsigned integers, reverting on
\t * overflow.
\t *
\t * Counterpart to Solidity's `+` operator.
\t *
\t * Requirements:
\t * - Addition cannot overflow.
\t */
\tfunction add(uint256 a, uint256 b) internal pure returns (uint256) {
\t\tuint256 c = a + b;
\t\trequire(c >= a, \"SafeMath: addition overflow\");

\t\treturn c;
\t}

\t/**
\t * @dev Returns the subtraction of two unsigned integers, reverting on
\t * overflow (when the result is negative).
\t *
\t * Counterpart to Solidity's `-` operator.
\t *
\t * Requirements:
\t * - Subtraction cannot overflow.
\t */
\tfunction sub(uint256 a, uint256 b) internal pure returns (uint256) {
\t\treturn sub(a, b, \"SafeMath: subtraction overflow\");
\t}

\t/**
\t * @dev Returns the subtraction of two unsigned integers, reverting with custom message on
\t * overflow (when the result is negative).
\t *
\t * Counterpart to Solidity's `-` operator.
\t *
\t * Requirements:
\t * - Subtraction cannot overflow.
\t *
\t * _Available since v2.4.0._
\t */
\tfunction sub(uint256 a, uint256 b, string memory errorMessage)
\t\tinternal
\t\tpure
\t\treturns (uint256)
\t{
\t\trequire(b <= a, errorMessage);
\t\tuint256 c = a - b;

\t\treturn c;
\t}

\t/**
\t * @dev Returns the multiplication of two unsigned integers, reverting on
\t * overflow.
\t *
\t * Counterpart to Solidity's `*` operator.
\t *
\t * Requirements:
\t * - Multiplication cannot overflow.
\t */
\tfunction mul(uint256 a, uint256 b) internal pure returns (uint256) {
\t\t// Gas optimization: this is cheaper than requiring 'a' not being zero, but the
\t\t// benefit is lost if 'b' is also tested.
\t\t// See: https://github.com/OpenZeppelin/openzeppelin-contracts/pull/522
\t\tif (a == 0) {
\t\t\treturn 0;
\t\t}

\t\tuint256 c = a * b;
\t\trequire(c / a == b, \"SafeMath: multiplication overflow\");

\t\treturn c;
\t}

\t/**
\t * @dev Returns the integer division of two unsigned integers. Reverts on
\t * division by zero. The result is rounded towards zero.
\t *
\t * Counterpart to Solidity's `/` operator. Note: this function uses a
\t * `revert` opcode (which leaves remaining gas untouched) while Solidity
\t * uses an invalid opcode to revert (consuming all remaining gas).
\t *
\t * Requirements:
\t * - The divisor cannot be zero.
\t */
\tfunction div(uint256 a, uint256 b) internal pure returns (uint256) {
\t\treturn div(a, b, \"SafeMath: division by zero\");
\t}

\t/**
\t * @dev Returns the integer division of two unsigned integers. Reverts with custom message on
\t * division by zero. The result is rounded towards zero.
\t *
\t * Counterpart to Solidity's `/` operator. Note: this function uses a
\t * `revert` opcode (which leaves remaining gas untouched) while Solidity
\t * uses an invalid opcode to revert (consuming all remaining gas).
\t *
\t * Requirements:
\t * - The divisor cannot be zero.
\t *
\t * _Available since v2.4.0._
\t */
\tfunction div(uint256 a, uint256 b, string memory errorMessage)
\t\tinternal
\t\tpure
\t\treturns (uint256)
\t{
\t\t// Solidity only automatically asserts when dividing by 0
\t\trequire(b > 0, errorMessage);
\t\tuint256 c = a / b;
\t\t// assert(a == b * c + a % b); // There is no case in which this doesn't hold

\t\treturn c;
\t}

\t/**
\t * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
\t * Reverts when dividing by zero.
\t *
\t * Counterpart to Solidity's `%` operator. This function uses a `revert`
\t * opcode (which leaves remaining gas untouched) while Solidity uses an
\t * invalid opcode to revert (consuming all remaining gas).
\t *
\t * Requirements:
\t * - The divisor cannot be zero.
\t */
\tfunction mod(uint256 a, uint256 b) internal pure returns (uint256) {
\t\treturn mod(a, b, \"SafeMath: modulo by zero\");
\t}

\t/**
\t * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
\t * Reverts with custom message when dividing by zero.
\t *
\t * Counterpart to Solidity's `%` operator. This function uses a `revert`
\t * opcode (which leaves remaining gas untouched) while Solidity uses an
\t * invalid opcode to revert (consuming all remaining gas).
\t *
\t * Requirements:
\t * - The divisor cannot be zero.
\t *
\t * _Available since v2.4.0._
\t */
\tfunction mod(uint256 a, uint256 b, string memory errorMessage)
\t\tinternal
\t\tpure
\t\treturns (uint256)
\t{
\t\trequire(b != 0, errorMessage);
\t\treturn a % b;
\t}
}"
    }
  
