// Be name Khoda
// Bime Abolfazl

// SPDX-License-Identifier: GPL-2.0-or-later
pragma solidity ^0.8.0;

// =================================================================================================================
//  _|_|_|    _|_|_|_|  _|    _|    _|_|_|      _|_|_|_|  _|                                                       |
//  _|    _|  _|        _|    _|  _|            _|            _|_|_|      _|_|_|  _|_|_|      _|_|_|    _|_|       |
//  _|    _|  _|_|_|    _|    _|    _|_|        _|_|_|    _|  _|    _|  _|    _|  _|    _|  _|        _|_|_|_|     |
//  _|    _|  _|        _|    _|        _|      _|        _|  _|    _|  _|    _|  _|    _|  _|        _|           |
//  _|_|_|    _|_|_|_|    _|_|    _|_|_|        _|        _|  _|    _|    _|_|_|  _|    _|    _|_|_|    _|_|_|     |
// =================================================================================================================
// ========================= DEUS (DEUS) =========================
// ===============================================================
// DEUS Finance: https://github.com/DeusFinance

// Primary Author(s)
// Travis Moore: https://github.com/FortisFortuna
// Jason Huan: https://github.com/jasonhuan
// Sam Kazemian: https://github.com/samkazemian
// Vahid Gh: https://github.com/vahid-dev
// SAYaghoubnejad: https://github.com/SAYaghoubnejad

// Reviewer(s) / Contributor(s)
// Sam Sun: https://github.com/samczsun

import \"../Common/Context.sol\";
import \"../ERC20/ERC20Custom.sol\";
import \"../ERC20/IERC20.sol\";
import \"../DEI/DEI.sol\";
import \"../Governance/AccessControl.sol\";

contract DEUSToken is ERC20Custom, AccessControl {

    /* ========== STATE VARIABLES ========== */

    string public symbol;
    string public name;
    uint8 public constant decimals = 18;

    uint256 public constant genesis_supply = 166670e18; // 166670 is printed upon genesis

    DEIStablecoin private DEI;

    bool public trackingVotes = true; // Tracking votes (only change if need to disable votes)

    // A checkpoint for marking number of votes from a given block
    struct Checkpoint {
        uint32 fromBlock;
        uint96 votes;
    }

    // A record of votes checkpoints for each account, by index
    mapping(address => mapping(uint32 => Checkpoint)) public checkpoints;

    // The number of checkpoints for each account
    mapping(address => uint32) public numCheckpoints;

    bytes32 public constant TRUSTY_ROLE = keccak256(\"TRUSTY_ROLE\");
    bytes32 public constant MINTER_ROLE = keccak256(\"MINTER_ROLE\");

    /* ========== MODIFIERS ========== */

    modifier onlyPoolsOrMinters() {
        require(
            DEI.dei_pools(msg.sender) == true || hasRole(MINTER_ROLE, msg.sender),
            \"DEUS: Only dei pools or minters are allowed to do this operation\"
        );
        _;
    }

    modifier onlyPools() {
        require(
            DEI.dei_pools(msg.sender) == true,
            \"DEUS: Only dei pools are allowed to do this operation\"
        );
        _;
    }

    modifier onlyByTrusty() {
        require(hasRole(TRUSTY_ROLE, msg.sender), \"DEUS: You are not trusty\");
        _;
    }

    /* ========== CONSTRUCTOR ========== */

    constructor(
        string memory _name,
        string memory _symbol,
        address _creator_address,
        address _trusty_address
    ) {
        require(_creator_address != address(0), \"DEUS::constructor: zero address detected\");  
        name = _name;
        symbol = _symbol;
        _setupRole(DEFAULT_ADMIN_ROLE, _trusty_address);
        _setupRole(TRUSTY_ROLE, _trusty_address);
        _mint(_creator_address, genesis_supply);

        // Do a checkpoint for the owner
        _writeCheckpoint(_creator_address, 0, 0, uint96(genesis_supply));
    }

    /* ========== RESTRICTED FUNCTIONS ========== */

    function setDEIAddress(address dei_contract_address)
        external
        onlyByTrusty
    {
        require(dei_contract_address != address(0), \"DEUS::setDEIAddress: Zero address detected\");

        DEI = DEIStablecoin(dei_contract_address);

        emit DEIAddressSet(dei_contract_address);
    }

    function mint(address to, uint256 amount) public onlyPoolsOrMinters {
        _mint(to, amount);
    }

    // This function is what other dei pools will call to mint new DEUS (similar to the DEI mint) and staking contracts can call this function too.
    function pool_mint(address m_address, uint256 m_amount) external onlyPoolsOrMinters {
        if (trackingVotes) {
            uint32 srcRepNum = numCheckpoints[address(this)];
            uint96 srcRepOld = srcRepNum > 0
                ? checkpoints[address(this)][srcRepNum - 1].votes
                : 0;
            uint96 srcRepNew = add96(
                srcRepOld,
                uint96(m_amount),
                \"DEUS::pool_mint: new votes overflows\"
            );
            _writeCheckpoint(address(this), srcRepNum, srcRepOld, srcRepNew); // mint new votes
            trackVotes(address(this), m_address, uint96(m_amount));
        }

        super._mint(m_address, m_amount);
        emit DEUSMinted(address(this), m_address, m_amount);
    }

    // This function is what other dei pools will call to burn DEUS
    function pool_burn_from(address b_address, uint256 b_amount)
        external
        onlyPools
    {
        if (trackingVotes) {
            trackVotes(b_address, address(this), uint96(b_amount));
            uint32 srcRepNum = numCheckpoints[address(this)];
            uint96 srcRepOld = srcRepNum > 0
                ? checkpoints[address(this)][srcRepNum - 1].votes
                : 0;
            uint96 srcRepNew = sub96(
                srcRepOld,
                uint96(b_amount),
                \"DEUS::pool_burn_from: new votes underflows\"
            );
            _writeCheckpoint(address(this), srcRepNum, srcRepOld, srcRepNew); // burn votes
        }

        super._burnFrom(b_address, b_amount);
        emit DEUSBurned(b_address, address(this), b_amount);
    }

    function toggleVotes() external onlyByTrusty {
        trackingVotes = !trackingVotes;
    }

    /* ========== OVERRIDDEN PUBLIC FUNCTIONS ========== */

    function transfer(address recipient, uint256 amount)
        public
        virtual
        override
        returns (bool)
    {
        if (trackingVotes) {
            // Transfer votes
            trackVotes(_msgSender(), recipient, uint96(amount));
        }

        _transfer(_msgSender(), recipient, amount);
        return true;
    }

    function transferFrom(
        address sender,
        address recipient,
        uint256 amount
    ) public virtual override returns (bool) {
        if (trackingVotes) {
            // Transfer votes
            trackVotes(sender, recipient, uint96(amount));
        }

        _transfer(sender, recipient, amount);
        _approve(
            sender,
            _msgSender(),
            _allowances[sender][_msgSender()] - amount
        );

        return true;
    }

    /* ========== PUBLIC FUNCTIONS ========== */

    /**
     * @notice Gets the current votes balance for `account`
     * @param account The address to get votes balance
     * @return The number of current votes for `account`
     */
    function getCurrentVotes(address account) external view returns (uint96) {
        uint32 nCheckpoints = numCheckpoints[account];
        return nCheckpoints > 0 ? checkpoints[account][nCheckpoints - 1].votes : 0;
    }

    /**
     * @notice Determine the prior number of votes for an account as of a block number
     * @dev Block number must be a finalized block or else this function will revert to prevent misinformation.
     * @param account The address of the account to check
     * @param blockNumber The block number to get the vote balance at
     * @return The number of votes the account had as of the given block
     */
    function getPriorVotes(address account, uint256 blockNumber)
        public
        view
        returns (uint96)
    {
        require(
            blockNumber < block.number,
            \"DEUS::getPriorVotes: not yet determined\"
        );

        uint32 nCheckpoints = numCheckpoints[account];
        if (nCheckpoints == 0) {
            return 0;
        }

        // First check most recent balance
        if (checkpoints[account][nCheckpoints - 1].fromBlock <= blockNumber) {
            return checkpoints[account][nCheckpoints - 1].votes;
        }

        // Next check implicit zero balance
        if (checkpoints[account][0].fromBlock > blockNumber) {
            return 0;
        }

        uint32 lower = 0;
        uint32 upper = nCheckpoints - 1;
        while (upper > lower) {
            uint32 center = upper - (upper - lower) / 2; // ceil, avoiding overflow
            Checkpoint memory cp = checkpoints[account][center];
            if (cp.fromBlock == blockNumber) {
                return cp.votes;
            } else if (cp.fromBlock < blockNumber) {
                lower = center;
            } else {
                upper = center - 1;
            }
        }
        return checkpoints[account][lower].votes;
    }

    /* ========== INTERNAL FUNCTIONS ========== */

    // From compound's _moveDelegates
    // Keep track of votes. \"Delegates\" is a misnomer here
    function trackVotes(
        address srcRep,
        address dstRep,
        uint96 amount
    ) internal {
        if (srcRep != dstRep && amount > 0) {
            if (srcRep != address(0)) {
                uint32 srcRepNum = numCheckpoints[srcRep];
                uint96 srcRepOld = srcRepNum > 0
                    ? checkpoints[srcRep][srcRepNum - 1].votes
                    : 0;
                uint96 srcRepNew = sub96(
                    srcRepOld,
                    amount,
                    \"DEUS::_moveVotes: vote amount underflows\"
                );
                _writeCheckpoint(srcRep, srcRepNum, srcRepOld, srcRepNew);
            }

            if (dstRep != address(0)) {
                uint32 dstRepNum = numCheckpoints[dstRep];
                uint96 dstRepOld = dstRepNum > 0
                    ? checkpoints[dstRep][dstRepNum - 1].votes
                    : 0;
                uint96 dstRepNew = add96(
                    dstRepOld,
                    amount,
                    \"DEUS::_moveVotes: vote amount overflows\"
                );
                _writeCheckpoint(dstRep, dstRepNum, dstRepOld, dstRepNew);
            }
        }
    }

    function _writeCheckpoint(
        address voter,
        uint32 nCheckpoints,
        uint96 oldVotes,
        uint96 newVotes
    ) internal {
        uint32 blockNumber = safe32(
            block.number,
            \"DEUS::_writeCheckpoint: block number exceeds 32 bits\"
        );

        if (
            nCheckpoints > 0 &&
            checkpoints[voter][nCheckpoints - 1].fromBlock == blockNumber
        ) {
            checkpoints[voter][nCheckpoints - 1].votes = newVotes;
        } else {
            checkpoints[voter][nCheckpoints] = Checkpoint(
                blockNumber,
                newVotes
            );
            numCheckpoints[voter] = nCheckpoints + 1;
        }

        emit VoterVotesChanged(voter, oldVotes, newVotes);
    }

    function safe32(uint256 n, string memory errorMessage)
        internal
        pure
        returns (uint32)
    {
        require(n < 2**32, errorMessage);
        return uint32(n);
    }

    function safe96(uint256 n, string memory errorMessage)
        internal
        pure
        returns (uint96)
    {
        require(n < 2**96, errorMessage);
        return uint96(n);
    }

    function add96(
        uint96 a,
        uint96 b,
        string memory errorMessage
    ) internal pure returns (uint96) {
        uint96 c = a + b;
        require(c >= a, errorMessage);
        return c;
    }

    function sub96(
        uint96 a,
        uint96 b,
        string memory errorMessage
    ) internal pure returns (uint96) {
        require(b <= a, errorMessage);
        return a - b;
    }

    /* ========== EVENTS ========== */

    /// @notice An event thats emitted when a voters account's vote balance changes
    event VoterVotesChanged(
        address indexed voter,
        uint256 previousBalance,
        uint256 newBalance
    );
    // Track DEUS burned
    event DEUSBurned(address indexed from, address indexed to, uint256 amount);
    // Track DEUS minted
    event DEUSMinted(address indexed from, address indexed to, uint256 amount);
    event DEIAddressSet(address addr);
}

//Dar panah khoda"
    
// SPDX-License-Identifier: MIT
pragma solidity >=0.6.11;

/*
 * @dev Provides information about the current execution context, including the
 * sender of the transaction and its data. While these are generally available
 * via msg.sender and msg.data, they should not be accessed in such a direct
 * manner, since when dealing with GSN meta-transactions the account sending and
 * paying for execution may not be the actual sender (as far as an application
 * is concerned).
 *
 * This contract is only required for intermediate, library-like contracts.
 */
abstract contract Context {
    function _msgSender() internal view virtual returns (address payable) {
        return payable(msg.sender);
    }

    function _msgData() internal view virtual returns (bytes memory) {
        this; // silence state mutability warning without generating bytecode - see https://github.com/ethereum/solidity/issues/2691
        return msg.data;
    }
}"
    
// SPDX-License-Identifier: MIT
pragma solidity >=0.6.11;

import \"../Common/Context.sol\";
import \"./IERC20.sol\";
import \"../Math/SafeMath.sol\";
import \"../Utils/Address.sol\";

// Due to compiling issues, _name, _symbol, and _decimals were removed


/**
 * @dev Implementation of the {IERC20} interface.
 *
 * This implementation is agnostic to the way tokens are created. This means
 * that a supply mechanism has to be added in a derived contract using {_mint}.
 * For a generic mechanism see {ERC20Mintable}.
 *
 * TIP: For a detailed writeup see our guide
 * https://forum.zeppelin.solutions/t/how-to-implement-erc20-supply-mechanisms/226[How
 * to implement supply mechanisms].
 *
 * We have followed general OpenZeppelin guidelines: functions revert instead
 * of returning `false` on failure. This behavior is nonetheless conventional
 * and does not conflict with the expectations of ERC20 applications.
 *
 * Additionally, an {Approval} event is emitted on calls to {transferFrom}.
 * This allows applications to reconstruct the allowance for all accounts just
 * by listening to said events. Other implementations of the EIP may not emit
 * these events, as it isn't required by the specification.
 *
 * Finally, the non-standard {decreaseAllowance} and {increaseAllowance}
 * functions have been added to mitigate the well-known issues around setting
 * allowances. See {IERC20-approve}.
 */
contract ERC20Custom is Context, IERC20 {
    using SafeMath for uint256;

    mapping (address => uint256) internal _balances;

    mapping (address => mapping (address => uint256)) internal _allowances;

    uint256 private _totalSupply;

    /**
     * @dev See {IERC20-totalSupply}.
     */
    function totalSupply() public view override returns (uint256) {
        return _totalSupply;
    }

    /**
     * @dev See {IERC20-balanceOf}.
     */
    function balanceOf(address account) public view override returns (uint256) {
        return _balances[account];
    }

    /**
     * @dev See {IERC20-transfer}.
     *
     * Requirements:
     *
     * - `recipient` cannot be the zero address.
     * - the caller must have a balance of at least `amount`.
     */
    function transfer(address recipient, uint256 amount) public virtual override returns (bool) {
        _transfer(_msgSender(), recipient, amount);
        return true;
    }

    /**
     * @dev See {IERC20-allowance}.
     */
    function allowance(address owner, address spender) public view virtual override returns (uint256) {
        return _allowances[owner][spender];
    }

    /**
     * @dev See {IERC20-approve}.
     *
     * Requirements:
     *
     * - `spender` cannot be the zero address.approve(address spender, uint256 amount)
     */
    function approve(address spender, uint256 amount) public virtual override returns (bool) {
        _approve(_msgSender(), spender, amount);
        return true;
    }

    /**
     * @dev See {IERC20-transferFrom}.
     *
     * Emits an {Approval} event indicating the updated allowance. This is not
     * required by the EIP. See the note at the beginning of {ERC20};
     *
     * Requirements:
     * - `sender` and `recipient` cannot be the zero address.
     * - `sender` must have a balance of at least `amount`.
     * - the caller must have allowance for `sender`'s tokens of at least
     * `amount`.
     */
    function transferFrom(address sender, address recipient, uint256 amount) public virtual override returns (bool) {
        _transfer(sender, recipient, amount);
        _approve(sender, _msgSender(), _allowances[sender][_msgSender()].sub(amount, \"ERC20: transfer amount exceeds allowance\"));
        return true;
    }

    /**
     * @dev Atomically increases the allowance granted to `spender` by the caller.
     *
     * This is an alternative to {approve} that can be used as a mitigation for
     * problems described in {IERC20-approve}.
     *
     * Emits an {Approval} event indicating the updated allowance.
     *
     * Requirements:
     *
     * - `spender` cannot be the zero address.
     */
    function increaseAllowance(address spender, uint256 addedValue) public virtual returns (bool) {
        _approve(_msgSender(), spender, _allowances[_msgSender()][spender].add(addedValue));
        return true;
    }

    /**
     * @dev Atomically decreases the allowance granted to `spender` by the caller.
     *
     * This is an alternative to {approve} that can be used as a mitigation for
     * problems described in {IERC20-approve}.
     *
     * Emits an {Approval} event indicating the updated allowance.
     *
     * Requirements:
     *
     * - `spender` cannot be the zero address.
     * - `spender` must have allowance for the caller of at least
     * `subtractedValue`.
     */
    function decreaseAllowance(address spender, uint256 subtractedValue) public virtual returns (bool) {
        _approve(_msgSender(), spender, _allowances[_msgSender()][spender].sub(subtractedValue, \"ERC20: decreased allowance below zero\"));
        return true;
    }

    /**
     * @dev Moves tokens `amount` from `sender` to `recipient`.
     *
     * This is internal function is equivalent to {transfer}, and can be used to
     * e.g. implement automatic token fees, slashing mechanisms, etc.
     *
     * Emits a {Transfer} event.
     *
     * Requirements:
     *
     * - `sender` cannot be the zero address.
     * - `recipient` cannot be the zero address.
     * - `sender` must have a balance of at least `amount`.
     */
    function _transfer(address sender, address recipient, uint256 amount) internal virtual {
        require(sender != address(0), \"ERC20: transfer from the zero address\");
        require(recipient != address(0), \"ERC20: transfer to the zero address\");

        _beforeTokenTransfer(sender, recipient, amount);

        _balances[sender] = _balances[sender].sub(amount, \"ERC20: transfer amount exceeds balance\");
        _balances[recipient] = _balances[recipient].add(amount);
        emit Transfer(sender, recipient, amount);
    }

    /** @dev Creates `amount` tokens and assigns them to `account`, increasing
     * the total supply.
     *
     * Emits a {Transfer} event with `from` set to the zero address.
     *
     * Requirements
     *
     * - `to` cannot be the zero address.
     */
    function _mint(address account, uint256 amount) internal virtual {
        require(account != address(0), \"ERC20: mint to the zero address\");

        _beforeTokenTransfer(address(0), account, amount);

        _totalSupply = _totalSupply.add(amount);
        _balances[account] = _balances[account].add(amount);
        emit Transfer(address(0), account, amount);
    }

    /**
     * @dev Destroys `amount` tokens from the caller.
     *
     * See {ERC20-_burn}.
     */
    function burn(uint256 amount) public virtual {
        _burn(_msgSender(), amount);
    }

    /**
     * @dev Destroys `amount` tokens from `account`, deducting from the caller's
     * allowance.
     *
     * See {ERC20-_burn} and {ERC20-allowance}.
     *
     * Requirements:
     *
     * - the caller must have allowance for `accounts`'s tokens of at least
     * `amount`.
     */
    function burnFrom(address account, uint256 amount) public virtual {
        uint256 decreasedAllowance = allowance(account, _msgSender()).sub(amount, \"ERC20: burn amount exceeds allowance\");

        _approve(account, _msgSender(), decreasedAllowance);
        _burn(account, amount);
    }


    /**
     * @dev Destroys `amount` tokens from `account`, reducing the
     * total supply.
     *
     * Emits a {Transfer} event with `to` set to the zero address.
     *
     * Requirements
     *
     * - `account` cannot be the zero address.
     * - `account` must have at least `amount` tokens.
     */
    function _burn(address account, uint256 amount) internal virtual {
        require(account != address(0), \"ERC20: burn from the zero address\");

        _beforeTokenTransfer(account, address(0), amount);

        _balances[account] = _balances[account].sub(amount, \"ERC20: burn amount exceeds balance\");
        _totalSupply = _totalSupply.sub(amount);
        emit Transfer(account, address(0), amount);
    }

    /**
     * @dev Sets `amount` as the allowance of `spender` over the `owner`s tokens.
     *
     * This is internal function is equivalent to `approve`, and can be used to
     * e.g. set automatic allowances for certain subsystems, etc.
     *
     * Emits an {Approval} event.
     *
     * Requirements:
     *
     * - `owner` cannot be the zero address.
     * - `spender` cannot be the zero address.
     */
    function _approve(address owner, address spender, uint256 amount) internal virtual {
        require(owner != address(0), \"ERC20: approve from the zero address\");
        require(spender != address(0), \"ERC20: approve to the zero address\");

        _allowances[owner][spender] = amount;
        emit Approval(owner, spender, amount);
    }

    /**
     * @dev Destroys `amount` tokens from `account`.`amount` is then deducted
     * from the caller's allowance.
     *
     * See {_burn} and {_approve}.
     */
    function _burnFrom(address account, uint256 amount) internal virtual {
        _burn(account, amount);
        _approve(account, _msgSender(), _allowances[account][_msgSender()].sub(amount, \"ERC20: burn amount exceeds allowance\"));
    }

    /**
     * @dev Hook that is called before any transfer of tokens. This includes
     * minting and burning.
     *
     * Calling conditions:
     *
     * - when `from` and `to` are both non-zero, `amount` of `from`'s tokens
     * will be to transferred to `to`.
     * - when `from` is zero, `amount` tokens will be minted for `to`.
     * - when `to` is zero, `amount` of `from`'s tokens will be burned.
     * - `from` and `to` are never both zero.
     *
     * To learn more about hooks, head to xref:ROOT:using-hooks.adoc[Using Hooks].
     */
    function _beforeTokenTransfer(address from, address to, uint256 amount) internal virtual { }
}"
    
// SPDX-License-Identifier: MIT
pragma solidity >=0.6.11;

import \"../Common/Context.sol\";
import \"../Math/SafeMath.sol\";

/**
 * @dev Interface of the ERC20 standard as defined in the EIP. Does not include
 * the optional functions; to access them see {ERC20Detailed}.
 */
interface IERC20 {
    /**
     * @dev Returns the amount of tokens in existence.
     */
    function totalSupply() external view returns (uint256);

    /**
     * @dev Returns the amount of tokens owned by `account`.
     */
    function balanceOf(address account) external view returns (uint256);

    /**
     * @dev Moves `amount` tokens from the caller's account to `recipient`.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transfer(address recipient, uint256 amount) external returns (bool);

    /**
     * @dev Returns the remaining number of tokens that `spender` will be
     * allowed to spend on behalf of `owner` through {transferFrom}. This is
     * zero by default.
     *
     * This value changes when {approve} or {transferFrom} are called.
     */
    function allowance(address owner, address spender) external view returns (uint256);

    /**
     * @dev Sets `amount` as the allowance of `spender` over the caller's tokens.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * IMPORTANT: Beware that changing an allowance with this method brings the risk
     * that someone may use both the old and the new allowance by unfortunate
     * transaction ordering. One possible solution to mitigate this race
     * condition is to first reduce the spender's allowance to 0 and set the
     * desired value afterwards:
     * https://github.com/ethereum/EIPs/issues/20#issuecomment-263524729
     *
     * Emits an {Approval} event.
     */
    function approve(address spender, uint256 amount) external returns (bool);

    /**
     * @dev Moves `amount` tokens from `sender` to `recipient` using the
     * allowance mechanism. `amount` is then deducted from the caller's
     * allowance.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transferFrom(address sender, address recipient, uint256 amount) external returns (bool);

    /**
     * @dev Emitted when `value` tokens are moved from one account (`from`) to
     * another (`to`).
     *
     * Note that `value` may be zero.
     */
    event Transfer(address indexed from, address indexed to, uint256 value);

    /**
     * @dev Emitted when the allowance of a `spender` for an `owner` is set by
     * a call to {approve}. `value` is the new allowance.
     */
    event Approval(address indexed owner, address indexed spender, uint256 value);
}

"
    
// Be name Khoda
// Bime Abolfazl

// SPDX-License-Identifier: GPL-2.0-or-later
pragma solidity ^0.8.0;

// =================================================================================================================
//  _|_|_|    _|_|_|_|  _|    _|    _|_|_|      _|_|_|_|  _|                                                       |
//  _|    _|  _|        _|    _|  _|            _|            _|_|_|      _|_|_|  _|_|_|      _|_|_|    _|_|       |
//  _|    _|  _|_|_|    _|    _|    _|_|        _|_|_|    _|  _|    _|  _|    _|  _|    _|  _|        _|_|_|_|     |
//  _|    _|  _|        _|    _|        _|      _|        _|  _|    _|  _|    _|  _|    _|  _|        _|           |
//  _|_|_|    _|_|_|_|    _|_|    _|_|_|        _|        _|  _|    _|    _|_|_|  _|    _|    _|_|_|    _|_|_|     |
// =================================================================================================================
// ======================= DEIStablecoin (DEI) ======================
// ====================================================================
// DEUS Finance: https://github.com/DeusFinance

// Primary Author(s)
// Travis Moore: https://github.com/FortisFortuna
// Jason Huan: https://github.com/jasonhuan
// Sam Kazemian: https://github.com/samkazemian
// Vahid: https://github.com/vahid-dev
// SAYaghoubnejad: https://github.com/SAYaghoubnejad

// Reviewer(s) / Contributor(s)
// Sam Sun: https://github.com/samczsun

import \"../Common/Context.sol\";
import \"../ERC20/IERC20.sol\";
import \"../ERC20/ERC20Custom.sol\";
import \"../ERC20/ERC20.sol\";
import \"../Staking/Owned.sol\";
import \"../DEUS/DEUS.sol\";
import \"./Pools/DEIPool.sol\";
import \"../Oracle/Oracle.sol\";
import \"../Oracle/ReserveTracker.sol\";
import \"@openzeppelin/contracts/utils/cryptography/ECDSA.sol\";

contract DEIStablecoin is ERC20Custom, AccessControl {
\tusing ECDSA for bytes32;

\t/* ========== STATE VARIABLES ========== */
\tenum PriceChoice {
\t\tDEI,
\t\tDEUS
\t}
\taddress public oracle;
\tstring public symbol;
\tstring public name;
\tuint8 public constant decimals = 18;
\taddress public creator_address;
\taddress public deus_address;
\tuint256 public constant genesis_supply = 10000e18; // genesis supply is 10k on Mainnet. This is to help with establishing the Uniswap pools, as they need liquidity
\taddress public reserve_tracker_address;

\t// The addresses in this array are added by the oracle and these contracts are able to mint DEI
\taddress[] public dei_pools_array;

\t// Mapping is also used for faster verification
\tmapping(address => bool) public dei_pools;

\t// Constants for various precisions
\tuint256 private constant PRICE_PRECISION = 1e6;

\tuint256 public global_collateral_ratio; // 6 decimals of precision, e.g. 924102 = 0.924102
\tuint256 public dei_step; // Amount to change the collateralization ratio by upon refreshCollateralRatio()
\tuint256 public refresh_cooldown; // Seconds to wait before being able to run refreshCollateralRatio() again
\t// uint256 public price_target; // The price of DEI at which the collateral ratio will respond to; this value is only used for the collateral ratio mechanism and not for minting and redeeming which are hardcoded at $1
\tuint256 public price_band; // The bound above and below the price target at which the refreshCollateralRatio() will not change the collateral ratio

\tbytes32 public constant COLLATERAL_RATIO_PAUSER = keccak256(\"COLLATERAL_RATIO_PAUSER\");
\tbytes32 public constant TRUSTY_ROLE = keccak256(\"TRUSTY_ROLE\");
\tbytes32 public constant MINTER_ROLE = keccak256(\"MINTER_ROLE\");
\tbool public collateral_ratio_paused = false;


\t// 6 decimals of precision
\tuint256 public growth_ratio;
\tuint256 public GR_top_band;
\tuint256 public GR_bottom_band;

\t// Bands
\tuint256 public DEI_top_band;
\tuint256 public DEI_bottom_band;

\t// Booleans
\t// bool public is_active;
\tbool public use_growth_ratio;
\tbool public FIP_6;


\t/* ========== MODIFIERS ========== */

\tmodifier onlyCollateralRatioPauser() {
\t\trequire(hasRole(COLLATERAL_RATIO_PAUSER, msg.sender), \"DEI: you are not the collateral ratio pauser\");
\t\t_;
\t}

\tmodifier onlyPoolsOrMinters() {
\t\trequire(
\t\t\tdei_pools[msg.sender] == true ||
\t\t\thasRole(MINTER_ROLE, msg.sender),
\t\t\t\"DEI: you are not minter\"
\t\t);
\t\t_;
\t}

\tmodifier onlyPools() {
\t\trequire(
\t\t\tdei_pools[msg.sender] == true,
\t\t\t\"DEI: only dei pools can call this function\"
\t\t);
\t\t_;
\t}

\tmodifier onlyByTrusty() {
\t\trequire(
\t\t\thasRole(TRUSTY_ROLE, msg.sender),
\t\t\t\"DEI: you are not the owner\"
\t\t);
\t\t_;
\t}

\t/* ========== CONSTRUCTOR ========== */

\tconstructor(
\t\tstring memory _name,
\t\tstring memory _symbol,
\t\taddress _creator_address,
\t\taddress _trusty_address
\t){
\t\trequire(
\t\t\t_creator_address != address(0),
\t\t\t\"DEI: zero address detected.\"
\t\t);
\t\tname = _name;
\t\tsymbol = _symbol;
\t\tcreator_address = _creator_address;
\t\t_setupRole(DEFAULT_ADMIN_ROLE, _trusty_address);
\t\t_mint(creator_address, genesis_supply);
\t\t_setupRole(COLLATERAL_RATIO_PAUSER, creator_address);
\t\tdei_step = 2500; // 6 decimals of precision, equal to 0.25%
\t\tglobal_collateral_ratio = 800000; // Dei system starts off fully collateralized (6 decimals of precision)
\t\trefresh_cooldown = 300; // Refresh cooldown period is set to 5 minutes (300 seconds) at genesis
\t\tprice_band = 5000; // Collateral ratio will not adjust if between $0.995 and $1.005 at genesis
\t\t_setupRole(TRUSTY_ROLE, _trusty_address);

\t\t// Upon genesis, if GR changes by more than 1% percent, enable change of collateral ratio
\t\tGR_top_band = 1000;
\t\tGR_bottom_band = 1000; 
\t}

\t/* ========== VIEWS ========== */

\t// Verify X DEUS or X DEI = 1 USD or ...
\tfunction verify_price(bytes32 sighash, bytes[] calldata sigs)
\t\tpublic
\t\tview
\t\treturns (bool)
\t{
\t\treturn Oracle(oracle).verify(sighash.toEthSignedMessageHash(), sigs);
\t}

\t// This is needed to avoid costly repeat calls to different getter functions
\t// It is cheaper gas-wise to just dump everything and only use some of the info
\tfunction dei_info(uint256[] memory collat_usd_price)
\t\tpublic
\t\tview
\t\treturns (
\t\t\tuint256,
\t\t\tuint256,
\t\t\tuint256
\t\t)
\t{
\t\treturn (
\t\t\ttotalSupply(), // totalSupply()
\t\t\tglobal_collateral_ratio, // global_collateral_ratio()
\t\t\tglobalCollateralValue(collat_usd_price) // globalCollateralValue
\t\t);
\t}

\t// Iterate through all dei pools and calculate all value of collateral in all pools globally
\tfunction globalCollateralValue(uint256[] memory collat_usd_price) public view returns (uint256) {
\t\tuint256 total_collateral_value_d18 = 0;

\t\tfor (uint256 i = 0; i < dei_pools_array.length; i++) {
\t\t\t// Exclude null addresses
\t\t\tif (dei_pools_array[i] != address(0)) {
\t\t\t\ttotal_collateral_value_d18 = total_collateral_value_d18 + DEIPool(dei_pools_array[i]).collatDollarBalance(collat_usd_price[i]);
\t\t\t}
\t\t}
\t\treturn total_collateral_value_d18;
\t}

\tfunction getChainID() public view returns (uint256) {
        uint256 id;
        assembly {
            id := chainid()
        }
        return id;
    }

\t/* ========== PUBLIC FUNCTIONS ========== */

\t// There needs to be a time interval that this can be called. Otherwise it can be called multiple times per expansion.
\tuint256 public last_call_time; // Last time the refreshCollateralRatio function was called

\t// Note: New function to refresh collateral ratio
\tfunction refreshCollateralRatio(uint deus_price, uint dei_price, uint256 expire_block, bytes[] calldata sigs) external {
\t\trequire(collateral_ratio_paused == false, \"DEI::Collateral Ratio has been paused\");
\t\tuint256 time_elapsed = (block.timestamp) - last_call_time;
\t\trequire(time_elapsed >= refresh_cooldown, \"DEI::Internal cooldown not passed\");
\t\tuint256 deus_reserves = ReserveTracker(reserve_tracker_address).getDEUSReserves();

\t\tbytes32 sighash = keccak256(abi.encodePacked(
\t\t\t\t\t\t\t\t\t\tdeus_address,
\t\t\t\t\t\t\t\t\t\tdeus_price,
\t\t\t\t\t\t\t\t\t\taddress(this),
\t\t\t\t\t\t\t\t\t\tdei_price,
\t\t\t\t\t\t\t\t\t\texpire_block,
                                    \tgetChainID()
                                    ));

\t\tverify_price(sighash, sigs);

\t\tuint256 deus_liquidity = deus_reserves * deus_price; // Has 6 decimals of precision

\t\tuint256 dei_supply = totalSupply();

\t\tuint256 new_growth_ratio = deus_liquidity / dei_supply; // (E18 + E6) / E18

\t\tif(FIP_6){
\t\t\trequire(dei_price > DEI_top_band || dei_price < DEI_bottom_band, \"DEI::Use refreshCollateralRatio when DEI is outside of peg\");
\t\t}

\t\t// First, check if the price is out of the band
\t\tif(dei_price > DEI_top_band){
\t\t\tglobal_collateral_ratio = global_collateral_ratio - dei_step;
\t\t} else if (dei_price < DEI_bottom_band){
\t\t\tglobal_collateral_ratio = global_collateral_ratio + dei_step;

\t\t// Else, check if the growth ratio has increased or decreased since last update
\t\t} else if(use_growth_ratio){
\t\t\tif(new_growth_ratio > growth_ratio * (1e6 + GR_top_band) / 1e6){
\t\t\t\tglobal_collateral_ratio = global_collateral_ratio - dei_step;
\t\t\t} else if (new_growth_ratio < growth_ratio * (1e6 - GR_bottom_band) / 1e6){
\t\t\t\tglobal_collateral_ratio = global_collateral_ratio + dei_step;
\t\t\t}
\t\t}

\t\tgrowth_ratio = new_growth_ratio;
\t\tlast_call_time = block.timestamp;

\t\t// No need for checking CR under 0 as the last_collateral_ratio.sub(dei_step) will throw 
\t\t// an error above in that case
\t\tif(global_collateral_ratio > 1e6){
\t\t\tglobal_collateral_ratio = 1e6;
\t\t}

\t\temit CollateralRatioRefreshed(global_collateral_ratio);

\t}

\tfunction useGrowthRatio(bool _use_growth_ratio) external onlyByTrusty {
\t\tuse_growth_ratio = _use_growth_ratio;

\t\temit UseGrowthRatioSet(_use_growth_ratio);
\t}

\tfunction setGrowthRatioBands(uint256 _GR_top_band, uint256 _GR_bottom_band) external onlyByTrusty {
\t\tGR_top_band = _GR_top_band;
\t\tGR_bottom_band = _GR_bottom_band;
\t\temit GrowthRatioBandSet( _GR_top_band, _GR_bottom_band);
\t}

\tfunction setPriceBands(uint256 _top_band, uint256 _bottom_band) external onlyByTrusty {
\t\tDEI_top_band = _top_band;
\t\tDEI_bottom_band = _bottom_band;

\t\temit PriceBandSet(_top_band, _bottom_band);
\t}

\tfunction activateFIP6(bool _activate) external onlyByTrusty {
\t\tFIP_6 = _activate;

\t\temit FIP_6Set(_activate);
\t}

\t// Used by pools when user redeems
\tfunction pool_burn_from(address b_address, uint256 b_amount)
\t\tpublic
\t\tonlyPools
\t{
\t\tsuper._burnFrom(b_address, b_amount);
\t\temit DEIBurned(b_address, msg.sender, b_amount);
\t}

\t// This function is what other dei pools will call to mint new DEI
\tfunction pool_mint(address m_address, uint256 m_amount) public onlyPoolsOrMinters {
\t\tsuper._mint(m_address, m_amount);
\t\temit DEIMinted(msg.sender, m_address, m_amount);
\t}

\t// Adds collateral addresses supported, such as tether and busd, must be ERC20
\tfunction addPool(address pool_address)
\t\tpublic
\t\tonlyByTrusty
\t{
\t\trequire(pool_address != address(0), \"DEI::addPool: Zero address detected\");
\t\trequire(dei_pools[pool_address] == false, \"DEI::addPool: Address already exists\");

\t\tdei_pools[pool_address] = true;
\t\tdei_pools_array.push(pool_address);

\t\temit PoolAdded(pool_address);
\t}

\t// Remove a pool
\tfunction removePool(address pool_address)
\t\tpublic
\t\tonlyByTrusty
\t{
\t\trequire(pool_address != address(0), \"DEI::removePool: Zero address detected\");

\t\trequire(dei_pools[pool_address] == true, \"DEI::removePool: Address nonexistant\");

\t\t// Delete from the mapping
\t\tdelete dei_pools[pool_address];

\t\t// 'Delete' from the array by setting the address to 0x0
\t\tfor (uint256 i = 0; i < dei_pools_array.length; i++) {
\t\t\tif (dei_pools_array[i] == pool_address) {
\t\t\t\tdei_pools_array[i] = address(0); // This will leave a null in the array and keep the indices the same
\t\t\t\tbreak;
\t\t\t}
\t\t}

\t\temit PoolRemoved(pool_address);
\t}
\t
\tfunction setOracle(address _oracle)
\t\tpublic
\t\tonlyByTrusty
\t{
\t\toracle = _oracle;

\t\temit OracleSet(_oracle);
\t}

\tfunction setDEIStep(uint256 _new_step)
\t\tpublic
\t\tonlyByTrusty
\t{
\t\tdei_step = _new_step;

\t\temit DEIStepSet(_new_step);
\t}

\tfunction setReserveTracker(address _reserve_tracker_address)
\t\texternal
\t\tonlyByTrusty
\t{\t\t
\t\treserve_tracker_address = _reserve_tracker_address;

\t\temit ReserveTrackerSet(_reserve_tracker_address);
\t}

\tfunction setRefreshCooldown(uint256 _new_cooldown)
\t\tpublic
\t\tonlyByTrusty
\t{
\t\trefresh_cooldown = _new_cooldown;

\t\temit RefreshCooldownSet(_new_cooldown);
\t}

\tfunction setDEUSAddress(address _deus_address)
\t\tpublic
\t\tonlyByTrusty
\t{
\t\trequire(_deus_address != address(0), \"DEI::setDEUSAddress: Zero address detected\");

\t\tdeus_address = _deus_address;

\t\temit DEUSAddressSet(_deus_address);
\t}

\tfunction toggleCollateralRatio()
\t\tpublic
\t\tonlyCollateralRatioPauser 
\t{
\t\tcollateral_ratio_paused = !collateral_ratio_paused;

\t\temit CollateralRatioToggled(collateral_ratio_paused);
\t}

\t/* ========== EVENTS ========== */

\t// Track DEI burned
\tevent DEIBurned(address indexed from, address indexed to, uint256 amount);
\t// Track DEI minted
\tevent DEIMinted(address indexed from, address indexed to, uint256 amount);
\tevent CollateralRatioRefreshed(uint256 global_collateral_ratio);
\tevent PoolAdded(address pool_address);
\tevent PoolRemoved(address pool_address);
\tevent DEIStepSet(uint256 new_step);
\tevent RefreshCooldownSet(uint256 new_cooldown);
\tevent DEUSAddressSet(address deus_address);
\tevent PriceBandSet(uint256 top_band, uint256 bottom_band);
\tevent CollateralRatioToggled(bool collateral_ratio_paused);
\tevent OracleSet(address oracle);
\tevent ReserveTrackerSet(address reserve_tracker_address);
\tevent UseGrowthRatioSet( bool use_growth_ratio);
\tevent FIP_6Set(bool activate);
\tevent GrowthRatioBandSet(uint256 GR_top_band, uint256 GR_bottom_band);
}

//Dar panah khoda
"
    
// SPDX-License-Identifier: MIT

pragma solidity >=0.6.11;

import \"../Utils/EnumerableSet.sol\";
import \"../Utils/Address.sol\";
import \"../Common/Context.sol\";

/**
 * @dev Contract module that allows children to implement role-based access
 * control mechanisms.
 *
 * Roles are referred to by their `bytes32` identifier. These should be exposed
 * in the external API and be unique. The best way to achieve this is by
 * using `public constant` hash digests:
 *
 * ```
 * bytes32 public constant MY_ROLE = keccak256(\"MY_ROLE\");
 * ```
 *
 * Roles can be used to represent a set of permissions. To restrict access to a
 * function call, use {hasRole}:
 *
 * ```
 * function foo() public {
 *     require(hasRole(MY_ROLE, msg.sender));
 *     ...
 * }
 * ```
 *
 * Roles can be granted and revoked dynamically via the {grantRole} and
 * {revokeRole} functions. Each role has an associated admin role, and only
 * accounts that have a role's admin role can call {grantRole} and {revokeRole}.
 *
 * By default, the admin role for all roles is `DEFAULT_ADMIN_ROLE`, which means
 * that only accounts with this role will be able to grant or revoke other
 * roles. More complex role relationships can be created by using
 * {_setRoleAdmin}.
 *
 * WARNING: The `DEFAULT_ADMIN_ROLE` is also its own admin: it has permission to
 * grant and revoke this role. Extra precautions should be taken to secure
 * accounts that have been granted it.
 */
abstract contract AccessControl is Context {
    using EnumerableSet for EnumerableSet.AddressSet;
    using Address for address;

    struct RoleData {
        EnumerableSet.AddressSet members;
        bytes32 adminRole;
    }

    mapping (bytes32 => RoleData) private _roles;

    bytes32 public constant DEFAULT_ADMIN_ROLE = 0x00; //bytes32(uint256(0x4B437D01b575618140442A4975db38850e3f8f5f) << 96);

    /**
     * @dev Emitted when `newAdminRole` is set as ``role``'s admin role, replacing `previousAdminRole`
     *
     * `DEFAULT_ADMIN_ROLE` is the starting admin for all roles, despite
     * {RoleAdminChanged} not being emitted signaling this.
     *
     * _Available since v3.1._
     */
    event RoleAdminChanged(bytes32 indexed role, bytes32 indexed previousAdminRole, bytes32 indexed newAdminRole);

    /**
     * @dev Emitted when `account` is granted `role`.
     *
     * `sender` is the account that originated the contract call, an admin role
     * bearer except when using {_setupRole}.
     */
    event RoleGranted(bytes32 indexed role, address indexed account, address indexed sender);

    /**
     * @dev Emitted when `account` is revoked `role`.
     *
     * `sender` is the account that originated the contract call:
     *   - if using `revokeRole`, it is the admin role bearer
     *   - if using `renounceRole`, it is the role bearer (i.e. `account`)
     */
    event RoleRevoked(bytes32 indexed role, address indexed account, address indexed sender);

    /**
     * @dev Returns `true` if `account` has been granted `role`.
     */
    function hasRole(bytes32 role, address account) public view returns (bool) {
        return _roles[role].members.contains(account);
    }

    /**
     * @dev Returns the number of accounts that have `role`. Can be used
     * together with {getRoleMember} to enumerate all bearers of a role.
     */
    function getRoleMemberCount(bytes32 role) public view returns (uint256) {
        return _roles[role].members.length();
    }

    /**
     * @dev Returns one of the accounts that have `role`. `index` must be a
     * value between 0 and {getRoleMemberCount}, non-inclusive.
     *
     * Role bearers are not sorted in any particular way, and their ordering may
     * change at any point.
     *
     * WARNING: When using {getRoleMember} and {getRoleMemberCount}, make sure
     * you perform all queries on the same block. See the following
     * https://forum.openzeppelin.com/t/iterating-over-elements-on-enumerableset-in-openzeppelin-contracts/2296[forum post]
     * for more information.
     */
    function getRoleMember(bytes32 role, uint256 index) public view returns (address) {
        return _roles[role].members.at(index);
    }

    /**
     * @dev Returns the admin role that controls `role`. See {grantRole} and
     * {revokeRole}.
     *
     * To change a role's admin, use {_setRoleAdmin}.
     */
    function getRoleAdmin(bytes32 role) public view returns (bytes32) {
        return _roles[role].adminRole;
    }

    /**
     * @dev Grants `role` to `account`.
     *
     * If `account` had not been already granted `role`, emits a {RoleGranted}
     * event.
     *
     * Requirements:
     *
     * - the caller must have ``role``'s admin role.
     */
    function grantRole(bytes32 role, address account) public virtual {
        require(hasRole(_roles[role].adminRole, _msgSender()), \"AccessControl: sender must be an admin to grant\");

        _grantRole(role, account);
    }

    /**
     * @dev Revokes `role` from `account`.
     *
     * If `account` had been granted `role`, emits a {RoleRevoked} event.
     *
     * Requirements:
     *
     * - the caller must have ``role``'s admin role.
     */
    function revokeRole(bytes32 role, address account) public virtual {
        require(hasRole(_roles[role].adminRole, _msgSender()), \"AccessControl: sender must be an admin to revoke\");

        _revokeRole(role, account);
    }

    /**
     * @dev Revokes `role` from the calling account.
     *
     * Roles are often managed via {grantRole} and {revokeRole}: this function's
     * purpose is to provide a mechanism for accounts to lose their privileges
     * if they are compromised (such as when a trusted device is misplaced).
     *
     * If the calling account had been granted `role`, emits a {RoleRevoked}
     * event.
     *
     * Requirements:
     *
     * - the caller must be `account`.
     */
    function renounceRole(bytes32 role, address account) public virtual {
        require(account == _msgSender(), \"AccessControl: can only renounce roles for self\");

        _revokeRole(role, account);
    }

    /**
     * @dev Grants `role` to `account`.
     *
     * If `account` had not been already granted `role`, emits a {RoleGranted}
     * event. Note that unlike {grantRole}, this function doesn't perform any
     * checks on the calling account.
     *
     * [WARNING]
     * ====
     * This function should only be called from the constructor when setting
     * up the initial roles for the system.
     *
     * Using this function in any other way is effectively circumventing the admin
     * system imposed by {AccessControl}.
     * ====
     */
    function _setupRole(bytes32 role, address account) internal virtual {
        _grantRole(role, account);
    }

    /**
     * @dev Sets `adminRole` as ``role``'s admin role.
     *
     * Emits a {RoleAdminChanged} event.
     */
    function _setRoleAdmin(bytes32 role, bytes32 adminRole) internal virtual {
        emit RoleAdminChanged(role, _roles[role].adminRole, adminRole);
        _roles[role].adminRole = adminRole;
    }

    function _grantRole(bytes32 role, address account) private {
        if (_roles[role].members.add(account)) {
            emit RoleGranted(role, account, _msgSender());
        }
    }

    function _revokeRole(bytes32 role, address account) private {
        if (_roles[role].members.remove(account)) {
            emit RoleRevoked(role, account, _msgSender());
        }
    }
}
"
    
// SPDX-License-Identifier: MIT
pragma solidity >=0.6.11;

/**
 * @dev Wrappers over Solidity's arithmetic operations with added overflow
 * checks.
 *
 * Arithmetic operations in Solidity wrap on overflow. This can easily result
 * in bugs, because programmers usually assume that an overflow raises an
 * error, which is the standard behavior in high level programming languages.
 * `SafeMath` restores this intuition by reverting the transaction when an
 * operation overflows.
 *
 * Using this library instead of the unchecked operations eliminates an entire
 * class of bugs, so it's recommended to use it always.
 */
library SafeMath {
    /**
     * @dev Returns the addition of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity's `+` operator.
     *
     * Requirements:
     * - Addition cannot overflow.
     */
    function add(uint256 a, uint256 b) internal pure returns (uint256) {
        uint256 c = a + b;
        require(c >= a, \"SafeMath: addition overflow\");

        return c;
    }

    /**
     * @dev Returns the subtraction of two unsigned integers, reverting on
     * overflow (when the result is negative).
     *
     * Counterpart to Solidity's `-` operator.
     *
     * Requirements:
     * - Subtraction cannot overflow.
     */
    function sub(uint256 a, uint256 b) internal pure returns (uint256) {
        return sub(a, b, \"SafeMath: subtraction overflow\");
    }

    /**
     * @dev Returns the subtraction of two unsigned integers, reverting with custom message on
     * overflow (when the result is negative).
     *
     * Counterpart to Solidity's `-` operator.
     *
     * Requirements:
     * - Subtraction cannot overflow.
     *
     * _Available since v2.4.0._
     */
    function sub(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b <= a, errorMessage);
        uint256 c = a - b;

        return c;
    }

    /**
     * @dev Returns the multiplication of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity's `*` operator.
     *
     * Requirements:
     * - Multiplication cannot overflow.
     */
    function mul(uint256 a, uint256 b) internal pure returns (uint256) {
        // Gas optimization: this is cheaper than requiring 'a' not being zero, but the
        // benefit is lost if 'b' is also tested.
        // See: https://github.com/OpenZeppelin/openzeppelin-contracts/pull/522
        if (a == 0) {
            return 0;
        }

        uint256 c = a * b;
        require(c / a == b, \"SafeMath: multiplication overflow\");

        return c;
    }

    /**
     * @dev Returns the integer division of two unsigned integers. Reverts on
     * division by zero. The result is rounded towards zero.
     *
     * Counterpart to Solidity's `/` operator. Note: this function uses a
     * `revert` opcode (which leaves remaining gas untouched) while Solidity
     * uses an invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     * - The divisor cannot be zero.
     */
    function div(uint256 a, uint256 b) internal pure returns (uint256) {
        return div(a, b, \"SafeMath: division by zero\");
    }

    /**
     * @dev Returns the integer division of two unsigned integers. Reverts with custom message on
     * division by zero. The result is rounded towards zero.
     *
     * Counterpart to Solidity's `/` operator. Note: this function uses a
     * `revert` opcode (which leaves remaining gas untouched) while Solidity
     * uses an invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     * - The divisor cannot be zero.
     *
     * _Available since v2.4.0._
     */
    function div(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        // Solidity only automatically asserts when dividing by 0
        require(b > 0, errorMessage);
        uint256 c = a / b;
        // assert(a == b * c + a % b); // There is no case in which this doesn't hold

        return c;
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
     * Reverts when dividing by zero.
     *
     * Counterpart to Solidity's `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     * - The divisor cannot be zero.
     */
    function mod(uint256 a, uint256 b) internal pure returns (uint256) {
        return mod(a, b, \"SafeMath: modulo by zero\");
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
     * Reverts with custom message when dividing by zero.
     *
     * Counterpart to Solidity's `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     * - The divisor cannot be zero.
     *
     * _Available since v2.4.0._
     */
    function mod(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b != 0, errorMessage);
        return a % b;
    }
}"
    
// SPDX-License-Identifier: MIT
pragma solidity >=0.6.11 <0.9.0;

/**
 * @dev Collection of functions related to the address type
 */
library Address {
    /**
     * @dev Returns true if `account` is a contract.
     *
     * [IMPORTANT]
     * ====
     * It is unsafe to assume that an address for which this function returns
     * false is an externally-owned account (EOA) and not a contract.
     *
     * Among others, `isContract` will return false for the following
     * types of addresses:
     *
     *  - an externally-owned account
     *  - a contract in construction
     *  - an address where a contract will be created
     *  - an address where a contract lived, but was destroyed
     * ====
     */
    function isContract(address account) internal view returns (bool) {
        // This method relies on extcodesize, which returns 0 for contracts in
        // construction, since the code is only stored at the end of the
        // constructor execution.

        uint256 size;
        // solhint-disable-next-line no-inline-assembly
        assembly { size := extcodesize(account) }
        return size > 0;
    }

    /**
     * @dev Replacement for Solidity's `transfer`: sends `amount` wei to
     * `recipient`, forwarding all available gas and reverting on errors.
     *
     * https://eips.ethereum.org/EIPS/eip-1884[EIP1884] increases the gas cost
     * of certain opcodes, possibly making contracts go over the 2300 gas limit
     * imposed by `transfer`, making them unable to receive funds via
     * `transfer`. {sendValue} removes this limitation.
     *
     * https://diligence.consensys.net/posts/2019/09/stop-using-soliditys-transfer-now/[Learn more].
     *
     * IMPORTANT: because control is transferred to `recipient`, care must be
     * taken to not create reentrancy vulnerabilities. Consider using
     * {ReentrancyGuard} or the
     * https://solidity.readthedocs.io/en/v0.5.11/security-considerations.html#use-the-checks-effects-interactions-pattern[checks-effects-interactions pattern].
     */
    function sendValue(address payable recipient, uint256 amount) internal {
        require(address(this).balance >= amount, \"Address: insufficient balance\");

        // solhint-disable-next-line avoid-low-level-calls, avoid-call-value
        (bool success, ) = recipient.call{ value: amount }(\"\");
        require(success, \"Address: unable to send value, recipient may have reverted\");
    }

    /**
     * @dev Performs a Solidity function call using a low level `call`. A
     * plain`call` is an unsafe replacement for a function call: use this
     * function instead.
     *
     * If `target` reverts with a revert reason, it is bubbled up by this
     * function (like regular Solidity function calls).
     *
     * Returns the raw returned data. To convert to the expected return value,
     * use https://solidity.readthedocs.io/en/latest/units-and-global-variables.html?highlight=abi.decode#abi-encoding-and-decoding-functions[`abi.decode`].
     *
     * Requirements:
     *
     * - `target` must be a contract.
     * - calling `target` with `data` must not revert.
     *
     * _Available since v3.1._
     */
    function functionCall(address target, bytes memory data) internal returns (bytes memory) {
      return functionCall(target, data, \"Address: low-level call failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`], but with
     * `errorMessage` as a fallback revert reason when `target` reverts.
     *
     * _Available since v3.1._
     */
    function functionCall(address target, bytes memory data, string memory errorMessage) internal returns (bytes memory) {
        return functionCallWithValue(target, data, 0, errorMessage);
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`],
     * but also transferring `value` wei to `target`.
     *
     * Requirements:
     *
     * - the calling contract must have an ETH balance of at least `value`.
     * - the called Solidity function must be `payable`.
     *
     * _Available since v3.1._
     */
    function functionCallWithValue(address target, bytes memory data, uint256 value) internal returns (bytes memory) {
        return functionCallWithValue(target, data, value, \"Address: low-level call with value failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCallWithValue-address-bytes-uint256-}[`functionCallWithValue`], but
     * with `errorMessage` as a fallback revert reason when `target` reverts.
     *
     * _Available since v3.1._
     */
    function functionCallWithValue(address target, bytes memory data, uint256 value, string memory errorMessage) internal returns (bytes memory) {
        require(address(this).balance >= value, \"Address: insufficient balance for call\");
        require(isContract(target), \"Address: call to non-contract\");

        // solhint-disable-next-line avoid-low-level-calls
        (bool success, bytes memory returndata) = target.call{ value: value }(data);
        return _verifyCallResult(success, returndata, errorMessage);
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`],
     * but performing a static call.
     *
     * _Available since v3.3._
     */
    function functionStaticCall(address target, bytes memory data) internal view returns (bytes memory) {
        return functionStaticCall(target, data, \"Address: low-level static call failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-string-}[`functionCall`],
     * but performing a static call.
     *
     * _Available since v3.3._
     */
    function functionStaticCall(address target, bytes memory data, string memory errorMessage) internal view returns (bytes memory) {
        require(isContract(target), \"Address: static call to non-contract\");

        // solhint-disable-next-line avoid-low-level-calls
        (bool success, bytes memory returndata) = target.staticcall(data);
        return _verifyCallResult(success, returndata, errorMessage);
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`],
     * but performing a delegate call.
     *
     * _Available since v3.4._
     */
    function functionDelegateCall(address target, bytes memory data) internal returns (bytes memory) {
        return functionDelegateCall(target, data, \"Address: low-level delegate call failed\");
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-string-}[`functionCall`],
     * but performing a delegate call.
     *
     * _Available since v3.4._
     */
    function functionDelegateCall(address target, bytes memory data, string memory errorMessage) internal returns (bytes memory) {
        require(isContract(target), \"Address: delegate call to non-contract\");

        // solhint-disable-next-line avoid-low-level-calls
        (bool success, bytes memory returndata) = target.delegatecall(data);
        return _verifyCallResult(success, returndata, errorMessage);
    }

    function _verifyCallResult(bool success, bytes memory returndata, string memory errorMessage) private pure returns(bytes memory) {
        if (success) {
            return returndata;
        } else {
            // Look for revert reason and bubble it up if present
            if (returndata.length > 0) {
                // The easiest way to bubble the revert reason is using memory via assembly

                // solhint-disable-next-line no-inline-assembly
                assembly {
                    let returndata_size := mload(returndata)
                    revert(add(32, returndata), returndata_size)
                }
            } else {
                revert(errorMessage);
            }
        }
    }
}"
    
// SPDX-License-Identifier: MIT
pragma solidity >=0.6.11;

import \"../Common/Context.sol\";
import \"./IERC20.sol\";
import \"../Math/SafeMath.sol\";
import \"../Utils/Address.sol\";


/**
 * @dev Implementation of the {IERC20} interface.
 *
 * This implementation is agnostic to the way tokens are created. This means
 * that a supply mechanism has to be added in a derived contract using {_mint}.
 * For a generic mechanism see {ERC20Mintable}.
 *
 * TIP: For a detailed writeup see our guide
 * https://forum.zeppelin.solutions/t/how-to-implement-erc20-supply-mechanisms/226[How
 * to implement supply mechanisms].
 *
 * We have followed general OpenZeppelin guidelines: functions revert instead
 * of returning `false` on failure. This behavior is nonetheless conventional
 * and does not conflict with the expectations of ERC20 applications.
 *
 * Additionally, an {Approval} event is emitted on calls to {transferFrom}.
 * This allows applications to reconstruct the allowance for all accounts just
 * by listening to said events. Other implementations of the EIP may not emit
 * these events, as it isn't required by the specification.
 *
 * Finally, the non-standard {decreaseAllowance} and {increaseAllowance}
 * functions have been added to mitigate the well-known issues around setting
 * allowances. See {IERC20-approve}.
 */
 
contract ERC20 is Context, IERC20 {
    using SafeMath for uint256;

    mapping (address => uint256) private _balances;

    mapping (address => mapping (address => uint256)) private _allowances;

    uint256 private _totalSupply;

    string private _name;
    string private _symbol;
    uint8 private _decimals;
    
    /**
     * @dev Sets the values for {name} and {symbol}, initializes {decimals} with
     * a default value of 18.
     *
     * To select a different value for {decimals}, use {_setupDecimals}.
     *
     * All three of these values are immutable: they can only be set once during
     * construction.
     */
    constructor (string memory __name, string memory __symbol) public {
        _name = __name;
        _symbol = __symbol;
        _decimals = 18;
    }

    /**
     * @dev Returns the name of the token.
     */
    function name() public view returns (string memory) {
        return _name;
    }

    /**
     * @dev Returns the symbol of the token, usually a shorter version of the
     * name.
     */
    function symbol() public view returns (string memory) {
        return _symbol;
    }

    /**
     * @dev Returns the number of decimals used to get its user representation.
     * For example, if `decimals` equals `2`, a balance of `505` tokens should
     * be displayed to a user as `5,05` (`505 / 10 ** 2`).
     *
     * Tokens usually opt for a value of 18, imitating the relationship between
     * Ether and Wei. This is the value {ERC20} uses, unless {_setupDecimals} is
     * called.
     *
     * NOTE: This information is only used for _display_ purposes: it in
     * no way affects any of the arithmetic of the contract, including
     * {IERC20-balanceOf} and {IERC20-transfer}.
     */
    function decimals() public view returns (uint8) {
        return _decimals;
    }

    /**
     * @dev See {IERC20-totalSupply}.
     */
    function totalSupply() public view override returns (uint256) {
        return _totalSupply;
    }

    /**
     * @dev See {IERC20-balanceOf}.
     */
    function balanceOf(address account) public view override returns (uint256) {
        return _balances[account];
    }

    /**
     * @dev See {IERC20-transfer}.
     *
     * Requirements:
     *
     * - `recipient` cannot be the zero address.
     * - the caller must have a balance of at least `amount`.
     */
    function transfer(address recipient, uint256 amount) public virtual override returns (bool) {
        _transfer(_msgSender(), recipient, amount);
        return true;
    }

    /**
     * @dev See {IERC20-allowance}.
     */
    function allowance(address owner, address spender) public view virtual override returns (uint256) {
        return _allowances[owner][spender];
    }

    /**
     * @dev See {IERC20-approve}.
     *
     * Requirements:
     *
     * - `spender` cannot be the zero address.approve(address spender, uint256 amount)
     */
    function approve(address spender, uint256 amount) public virtual override returns (bool) {
        _approve(_msgSender(), spender, amount);
        return true;
    }

    /**
     * @dev See {IERC20-transferFrom}.
     *
     * Emits an {Approval} event indicating the updated allowance. This is not
     * required by the EIP. See the note at the beginning of {ERC20};
     *
     * Requirements:
     * - `sender` and `recipient` cannot be the zero address.
     * - `sender` must have a balance of at least `amount`.
     * - the caller must have allowance for `sender`'s tokens of at least
     * `amount`.
     */
    function transferFrom(address sender, address recipient, uint256 amount) public virtual override returns (bool) {
        _transfer(sender, recipient, amount);
        _approve(sender, _msgSender(), _allowances[sender][_msgSender()].sub(amount, \"ERC20: transfer amount exceeds allowance\"));
        return true;
    }

    /**
     * @dev Atomically increases the allowance granted to `spender` by the caller.
     *
     * This is an alternative to {approve} that can be used as a mitigation for
     * problems described in {IERC20-approve}.
     *
     * Emits an {Approval} event indicating the updated allowance.
     *
     * Requirements:
     *
     * - `spender` cannot be the zero address.
     */
    function increaseAllowance(address spender, uint256 addedValue) public virtual returns (bool) {
        _approve(_msgSender(), spender, _allowances[_msgSender()][spender].add(addedValue));
        return true;
    }

    /**
     * @dev Atomically decreases the allowance granted to `spender` by the caller.
     *
     * This is an alternative to {approve} that can be used as a mitigation for
     * problems described in {IERC20-approve}.
     *
     * Emits an {Approval} event indicating the updated allowance.
     *
     * Requirements:
     *
     * - `spender` cannot be the zero address.
     * - `spender` must have allowance for the caller of at least
     * `subtractedValue`.
     */
    function decreaseAllowance(address spender, uint256 subtractedValue) public virtual returns (bool) {
        _approve(_msgSender(), spender, _allowances[_msgSender()][spender].sub(subtractedValue, \"ERC20: decreased allowance below zero\"));
        return true;
    }

    /**
     * @dev Moves tokens `amount` from `sender` to `recipient`.
     *
     * This is internal function is equivalent to {transfer}, and can be used to
     * e.g. implement automatic token fees, slashing mechanisms, etc.
     *
     * Emits a {Transfer} event.
     *
     * Requirements:
     *
     * - `sender` cannot be the zero address.
     * - `recipient` cannot be the zero address.
     * - `sender` must have a balance of at least `amount`.
     */
    function _transfer(address sender, address recipient, uint256 amount) internal virtual {
        require(sender != address(0), \"ERC20: transfer from the zero address\");
        require(recipient != address(0), \"ERC20: transfer to the zero address\");

        _beforeTokenTransfer(sender, recipient, amount);

        _balances[sender] = _balances[sender].sub(amount, \"ERC20: transfer amount exceeds balance\");
        _balances[recipient] = _balances[recipient].add(amount);
        emit Transfer(sender, recipient, amount);
    }

    /** @dev Creates `amount` tokens and assigns them to `account`, increasing
     * the total supply.
     *
     * Emits a {Transfer} event with `from` set to the zero address.
     *
     * Requirements
     *
     * - `to` cannot be the zero address.
     */
    function _mint(address account, uint256 amount) internal virtual {
        require(account != address(0), \"ERC20: mint to the zero address\");

        _beforeTokenTransfer(address(0), account, amount);

        _totalSupply = _totalSupply.add(amount);
        _balances[account] = _balances[account].add(amount);
        emit Transfer(address(0), account, amount);
    }

    /**
     * @dev Destroys `amount` tokens from the caller.
     *
     * See {ERC20-_burn}.
     */
    function burn(uint256 amount) public virtual {
        _burn(_msgSender(), amount);
    }

    /**
     * @dev Destroys `amount` tokens from `account`, deducting from the caller's
     * allowance.
     *
     * See {ERC20-_burn} and {ERC20-allowance}.
     *
     * Requirements:
     *
     * - the caller must have allowance for `accounts`'s tokens of at least
     * `amount`.
     */
    function burnFrom(address account, uint256 amount) public virtual {
        uint256 decreasedAllowance = allowance(account, _msgSender()).sub(amount, \"ERC20: burn amount exceeds allowance\");

        _approve(account, _msgSender(), decreasedAllowance);
        _burn(account, amount);
    }


    /**
     * @dev Destroys `amount` tokens from `account`, reducing the
     * total supply.
     *
     * Emits a {Transfer} event with `to` set to the zero address.
     *
     * Requirements
     *
     * - `account` cannot be the zero address.
     * - `account` must have at least `amount` tokens.
     */
    function _burn(address account, uint256 amount) internal virtual {
        require(account != address(0), \"ERC20: burn from the zero address\");

        _beforeTokenTransfer(account, address(0), amount);

        _balances[account] = _balances[account].sub(amount, \"ERC20: burn amount exceeds balance\");
        _totalSupply = _totalSupply.sub(amount);
        emit Transfer(account, address(0), amount);
    }

    /**
     * @dev Sets `amount` as the allowance of `spender` over the `owner`s tokens.
     *
     * This is internal function is equivalent to `approve`, and can be used to
     * e.g. set automatic allowances for certain subsystems, etc.
     *
     * Emits an {Approval} event.
     *
     * Requirements:
     *
     * - `owner` cannot be the zero address.
     * - `spender` cannot be the zero address.
     */
    function _approve(address owner, address spender, uint256 amount) internal virtual {
        require(owner != address(0), \"ERC20: approve from the zero address\");
        require(spender != address(0), \"ERC20: approve to the zero address\");

        _allowances[owner][spender] = amount;
        emit Approval(owner, spender, amount);
    }

    /**
     * @dev Destroys `amount` tokens from `account`.`amount` is then deducted
     * from the caller's allowance.
     *
     * See {_burn} and {_approve}.
     */
    function _burnFrom(address account, uint256 amount) internal virtual {
        _burn(account, amount);
        _approve(account, _msgSender(), _allowances[account][_msgSender()].sub(amount, \"ERC20: burn amount exceeds allowance\"));
    }

    /**
     * @dev Hook that is called before any transfer of tokens. This includes
     * minting and burning.
     *
     * Calling conditions:
     *
     * - when `from` and `to` are both non-zero, `amount` of `from`'s tokens
     * will be to transferred to `to`.
     * - when `from` is zero, `amount` tokens will be minted for `to`.
     * - when `to` is zero, `amount` of `from`'s tokens will be burned.
     * - `from` and `to` are never both zero.
     *
     * To learn more about hooks, head to xref:ROOT:using-hooks.adoc[Using Hooks].
     */
    function _beforeTokenTransfer(address from, address to, uint256 amount) internal virtual { }
}
"
    
// SPDX-License-Identifier: GPL-2.0-or-later
pragma solidity >=0.6.11;

// https://docs.synthetix.io/contracts/Owned
contract Owned {
    address public owner;
    address public nominatedOwner;

    constructor(address _owner) public {
        require(_owner != address(0), \"Owner address cannot be 0\");
        owner = _owner;
        emit OwnerChanged(address(0), _owner);
    }

    function nominateNewOwner(address _owner) external onlyOwner {
        nominatedOwner = _owner;
        emit OwnerNominated(_owner);
    }

    function acceptOwnership() external {
        require(msg.sender == nominatedOwner, \"You must be nominated before you can accept ownership\");
        emit OwnerChanged(owner, nominatedOwner);
        owner = nominatedOwner;
        nominatedOwner = address(0);
    }

    modifier onlyOwner {
        require(msg.sender == owner, \"Only the contract owner may perform this action\");
        _;
    }

    event OwnerNominated(address newOwner);
    event OwnerChanged(address oldOwner, address newOwner);
}"
    
// Be name Khoda
// Bime Abolfazl

// SPDX-License-Identifier: GPL-2.0-or-later
pragma solidity ^0.8.0;
pragma abicoder v2;
// =================================================================================================================
//  _|_|_|    _|_|_|_|  _|    _|    _|_|_|      _|_|_|_|  _|                                                       |
//  _|    _|  _|        _|    _|  _|            _|            _|_|_|      _|_|_|  _|_|_|      _|_|_|    _|_|       |
//  _|    _|  _|_|_|    _|    _|    _|_|        _|_|_|    _|  _|    _|  _|    _|  _|    _|  _|        _|_|_|_|     |
//  _|    _|  _|        _|    _|        _|      _|        _|  _|    _|  _|    _|  _|    _|  _|        _|           |
//  _|_|_|    _|_|_|_|    _|_|    _|_|_|        _|        _|  _|    _|    _|_|_|  _|    _|    _|_|_|    _|_|_|     |
// =================================================================================================================
// ============================= DEIPool =============================
// ====================================================================
// DEUS Finance: https://github.com/DeusFinance

// Primary Author(s)
// Travis Moore: https://github.com/FortisFortuna
// Jason Huan: https://github.com/jasonhuan
// Sam Kazemian: https://github.com/samkazemian
// Vahid Gh: https://github.com/vahid-dev
// SAYaghoubnejad: https://github.com/SAYaghoubnejad

// Reviewer(s) / Contributor(s)
// Sam Sun: https://github.com/samczsun

import \"../../Uniswap/TransferHelper.sol\";
import \"../../DEUS/IDEUS.sol\";
import \"../../DEI/IDEI.sol\";
import \"../../ERC20/ERC20.sol\";
import \"../../Governance/AccessControl.sol\";
import \"./DEIPoolLibrary.sol\";

contract DEIPool is AccessControl {

    struct RecollateralizeDEI {
\t\tuint256 collateral_amount;
\t\tuint256 pool_collateral_price;
\t\tuint256[] collateral_price;
\t\tuint256 deus_current_price;
\t\tuint256 expireBlock;
\t\tbytes[] sigs;
    }

\t/* ========== STATE VARIABLES ========== */

\tERC20 private collateral_token;
\taddress private collateral_address;

\taddress private dei_contract_address;
\taddress private deus_contract_address;

\tuint256 public minting_fee;
\tuint256 public redemption_fee;
\tuint256 public buyback_fee;
\tuint256 public recollat_fee;

\tmapping(address => uint256) public redeemDEUSBalances;
\tmapping(address => uint256) public redeemCollateralBalances;
\tuint256 public unclaimedPoolCollateral;
\tuint256 public unclaimedPoolDEUS;
\tmapping(address => uint256) public lastRedeemed;

\t// Constants for various precisions
\tuint256 private constant PRICE_PRECISION = 1e6;
\tuint256 private constant COLLATERAL_RATIO_PRECISION = 1e6;
\tuint256 private constant COLLATERAL_RATIO_MAX = 1e6;

\t// Number of decimals needed to get to 18
\tuint256 private immutable missing_decimals;

\t// Pool_ceiling is the total units of collateral that a pool contract can hold
\tuint256 public pool_ceiling = 0;

\t// Stores price of the collateral, if price is paused
\tuint256 public pausedPrice = 0;

\t// Bonus rate on DEUS minted during recollateralizeDEI(); 6 decimals of precision, set to 0.75% on genesis
\tuint256 public bonus_rate = 7500;

\t// Number of blocks to wait before being able to collectRedemption()
\tuint256 public redemption_delay = 2;

\t// Minting/Redeeming fees goes to daoWallet
\tuint256 public daoShare = 0;

\tDEIPoolLibrary poolLibrary;

\t// AccessControl Roles
\tbytes32 private constant MINT_PAUSER = keccak256(\"MINT_PAUSER\");
\tbytes32 private constant REDEEM_PAUSER = keccak256(\"REDEEM_PAUSER\");
\tbytes32 private constant BUYBACK_PAUSER = keccak256(\"BUYBACK_PAUSER\");
\tbytes32 private constant RECOLLATERALIZE_PAUSER = keccak256(\"RECOLLATERALIZE_PAUSER\");
    bytes32 public constant TRUSTY_ROLE = keccak256(\"TRUSTY_ROLE\");
\tbytes32 public constant DAO_SHARE_COLLECTOR = keccak256(\"DAO_SHARE_COLLECTOR\");
\tbytes32 public constant PARAMETER_SETTER_ROLE = keccak256(\"PARAMETER_SETTER_ROLE\");

\t// AccessControl state variables
\tbool public mintPaused = false;
\tbool public redeemPaused = false;
\tbool public recollateralizePaused = false;
\tbool public buyBackPaused = false;

\t/* ========== MODIFIERS ========== */

\tmodifier onlyByTrusty() {
\t\trequire(
\t\t\thasRole(TRUSTY_ROLE, msg.sender),
\t\t\t\"POOL::you are not trusty\"
\t\t);
\t\t_;
\t}

\tmodifier notRedeemPaused() {
\t\trequire(redeemPaused == false, \"POOL::Redeeming is paused\");
\t\t_;
\t}

\tmodifier notMintPaused() {
\t\trequire(mintPaused == false, \"POOL::Minting is paused\");
\t\t_;
\t}

\t/* ========== CONSTRUCTOR ========== */

\tconstructor(
\t\taddress _dei_contract_address,
\t\taddress _deus_contract_address,
\t\taddress _collateral_address,
\t\taddress _trusty_address,
\t\taddress _admin_address,
\t\tuint256 _pool_ceiling,
\t\taddress _library
\t) {
\t\trequire(
\t\t\t(_dei_contract_address != address(0)) &&
\t\t\t\t(_deus_contract_address != address(0)) &&
\t\t\t\t(_collateral_address != address(0)) &&
\t\t\t\t(_trusty_address != address(0)) &&
\t\t\t\t(_admin_address != address(0)) &&
\t\t\t\t(_library != address(0)),
\t\t\t\"POOL::Zero address detected\"
\t\t);
\t\tpoolLibrary = DEIPoolLibrary(_library);
\t\tdei_contract_address = _dei_contract_address;
\t\tdeus_contract_address = _deus_contract_address;
\t\tcollateral_address = _collateral_address;
\t\tcollateral_token = ERC20(_collateral_address);
\t\tpool_ceiling = _pool_ceiling;
\t\tmissing_decimals = uint256(18) - collateral_token.decimals();

\t\t_setupRole(DEFAULT_ADMIN_ROLE, _admin_address);
\t\t_setupRole(MINT_PAUSER, _trusty_address);
\t\t_setupRole(REDEEM_PAUSER, _trusty_address);
\t\t_setupRole(RECOLLATERALIZE_PAUSER, _trusty_address);
\t\t_setupRole(BUYBACK_PAUSER, _trusty_address);
        _setupRole(TRUSTY_ROLE, _trusty_address);
        _setupRole(TRUSTY_ROLE, _trusty_address);
        _setupRole(PARAMETER_SETTER_ROLE, _trusty_address);
\t}

\t/* ========== VIEWS ========== */

\t// Returns dollar value of collateral held in this DEI pool
\tfunction collatDollarBalance(uint256 collat_usd_price) public view returns (uint256) {
\t\treturn ((collateral_token.balanceOf(address(this)) - unclaimedPoolCollateral) * (10**missing_decimals) * collat_usd_price) / (PRICE_PRECISION);
\t}

\t// Returns the value of excess collateral held in this DEI pool, compared to what is needed to maintain the global collateral ratio
\tfunction availableExcessCollatDV(uint256[] memory collat_usd_price) public view returns (uint256) {
\t\tuint256 total_supply = IDEIStablecoin(dei_contract_address).totalSupply();
\t\tuint256 global_collateral_ratio = IDEIStablecoin(dei_contract_address).global_collateral_ratio();
\t\tuint256 global_collat_value = IDEIStablecoin(dei_contract_address).globalCollateralValue(collat_usd_price);

\t\tif (global_collateral_ratio > COLLATERAL_RATIO_PRECISION)
\t\t\tglobal_collateral_ratio = COLLATERAL_RATIO_PRECISION; // Handles an overcollateralized contract with CR > 1
\t\tuint256 required_collat_dollar_value_d18 = (total_supply * global_collateral_ratio) / (COLLATERAL_RATIO_PRECISION); // Calculates collateral needed to back each 1 DEI with $1 of collateral at current collat ratio
\t\tif (global_collat_value > required_collat_dollar_value_d18)
\t\t\treturn global_collat_value - required_collat_dollar_value_d18;
\t\telse return 0;
\t}

\tfunction getChainID() public view returns (uint256) {
        uint256 id;
        assembly {
            id := chainid()
        }
        return id;
    }

\t/* ========== PUBLIC FUNCTIONS ========== */

\t// We separate out the 1t1, fractional and algorithmic minting functions for gas efficiency
\tfunction mint1t1DEI(uint256 collateral_amount, uint256 collateral_price, uint256 expireBlock, bytes[] calldata sigs)
\t\texternal
\t\tnotMintPaused
\t\treturns (uint256 dei_amount_d18)
\t{

\t\trequire(
\t\t\tIDEIStablecoin(dei_contract_address).global_collateral_ratio() >= COLLATERAL_RATIO_MAX,
\t\t\t\"Collateral ratio must be >= 1\"
\t\t);
\t\trequire(
\t\t\tcollateral_token.balanceOf(address(this)) - unclaimedPoolCollateral +  collateral_amount <= pool_ceiling,
\t\t\t\"[Pool's Closed]: Ceiling reached\"
\t\t);

\t\trequire(expireBlock >= block.number, \"POOL::mint1t1DEI: signature is expired\");
        bytes32 sighash = keccak256(abi.encodePacked(collateral_address, collateral_price, expireBlock, getChainID()));
\t\trequire(IDEIStablecoin(dei_contract_address).verify_price(sighash, sigs), \"POOL::mint1t1DEI: invalid signatures\");

\t\tuint256 collateral_amount_d18 = collateral_amount * (10**missing_decimals);
\t\tdei_amount_d18 = poolLibrary.calcMint1t1DEI(
\t\t\tcollateral_price,
\t\t\tcollateral_amount_d18
\t\t); //1 DEI for each $1 worth of collateral

\t\tdei_amount_d18 = (dei_amount_d18 * (uint256(1e6) - minting_fee)) / 1e6; //remove precision at the end

\t\tTransferHelper.safeTransferFrom(
\t\t\taddress(collateral_token),
\t\t\tmsg.sender,
\t\t\taddress(this),
\t\t\tcollateral_amount
\t\t);

\t\tdaoShare += dei_amount_d18 *  minting_fee / 1e6;
\t\tIDEIStablecoin(dei_contract_address).pool_mint(msg.sender, dei_amount_d18);
\t}

\t// 0% collateral-backed
\tfunction mintAlgorithmicDEI(
\t\tuint256 deus_amount_d18,
\t\tuint256 deus_current_price,
\t\tuint256 expireBlock,
\t\tbytes[] calldata sigs
\t) external notMintPaused returns (uint256 dei_amount_d18) {
\t\trequire(
\t\t\tIDEIStablecoin(dei_contract_address).global_collateral_ratio() == 0,
\t\t\t\"Collateral ratio must be 0\"
\t\t);
\t\trequire(expireBlock >= block.number, \"POOL::mintAlgorithmicDEI: signature is expired.\");
\t\tbytes32 sighash = keccak256(abi.encodePacked(deus_contract_address, deus_current_price, expireBlock, getChainID()));
\t\trequire(IDEIStablecoin(dei_contract_address).verify_price(sighash, sigs), \"POOL::mintAlgorithmicDEI: invalid signatures\");

\t\tdei_amount_d18 = poolLibrary.calcMintAlgorithmicDEI(
\t\t\tdeus_current_price, // X DEUS / 1 USD
\t\t\tdeus_amount_d18
\t\t);

\t\tdei_amount_d18 = (dei_amount_d18 * (uint256(1e6) - (minting_fee))) / (1e6);
\t\tdaoShare += dei_amount_d18 *  minting_fee / 1e6;

\t\tIDEUSToken(deus_contract_address).pool_burn_from(msg.sender, deus_amount_d18);
\t\tIDEIStablecoin(dei_contract_address).pool_mint(msg.sender, dei_amount_d18);
\t}

\t// Will fail if fully collateralized or fully algorithmic
\t// > 0% and < 100% collateral-backed
\tfunction mintFractionalDEI(
\t\tuint256 collateral_amount,
\t\tuint256 deus_amount,
\t\tuint256 collateral_price,
\t\tuint256 deus_current_price,
\t\tuint256 expireBlock,
\t\tbytes[] calldata sigs
\t) external notMintPaused returns (uint256 mint_amount) {
\t\tuint256 global_collateral_ratio = IDEIStablecoin(dei_contract_address).global_collateral_ratio();
\t\trequire(
\t\t\tglobal_collateral_ratio < COLLATERAL_RATIO_MAX && global_collateral_ratio > 0,
\t\t\t\"Collateral ratio needs to be between .000001 and .999999\"
\t\t);
\t\trequire(
\t\t\tcollateral_token.balanceOf(address(this)) - unclaimedPoolCollateral + collateral_amount <= pool_ceiling,
\t\t\t\"Pool ceiling reached, no more DEI can be minted with this collateral\"
\t\t);

\t\trequire(expireBlock >= block.number, \"POOL::mintFractionalDEI: signature is expired.\");
\t\tbytes32 sighash = keccak256(abi.encodePacked(collateral_address, collateral_price, deus_contract_address, deus_current_price, expireBlock, getChainID()));
\t\trequire(IDEIStablecoin(dei_contract_address).verify_price(sighash, sigs), \"POOL::mintFractionalDEI: invalid signatures\");

\t\tDEIPoolLibrary.MintFD_Params memory input_params;

\t\t// Blocking is just for solving stack depth problem
\t\t{
\t\t\tuint256 collateral_amount_d18 = collateral_amount * (10**missing_decimals);
\t\t\tinput_params = DEIPoolLibrary.MintFD_Params(
\t\t\t\t\t\t\t\t\t\t\tdeus_current_price,
\t\t\t\t\t\t\t\t\t\t\tcollateral_price,
\t\t\t\t\t\t\t\t\t\t\tcollateral_amount_d18,
\t\t\t\t\t\t\t\t\t\t\tglobal_collateral_ratio
\t\t\t\t\t\t\t\t\t\t);
\t\t}\t\t\t\t\t\t

\t\tuint256 deus_needed;
\t\t(mint_amount, deus_needed) = poolLibrary.calcMintFractionalDEI(input_params);
\t\trequire(deus_needed <= deus_amount, \"Not enough DEUS inputted\");
\t\t
\t\tmint_amount = (mint_amount * (uint256(1e6) - minting_fee)) / (1e6);

\t\tIDEUSToken(deus_contract_address).pool_burn_from(msg.sender, deus_needed);
\t\tTransferHelper.safeTransferFrom(
\t\t\taddress(collateral_token),
\t\t\tmsg.sender,
\t\t\taddress(this),
\t\t\tcollateral_amount
\t\t);

\t\tdaoShare += mint_amount *  minting_fee / 1e6;
\t\tIDEIStablecoin(dei_contract_address).pool_mint(msg.sender, mint_amount);
\t}

\t// Redeem collateral. 100% collateral-backed
\tfunction redeem1t1DEI(uint256 DEI_amount, uint256 collateral_price, uint256 expireBlock, bytes[] calldata sigs)
\t\texternal
\t\tnotRedeemPaused
\t{
\t\trequire(
\t\t\tIDEIStablecoin(dei_contract_address).global_collateral_ratio() == COLLATERAL_RATIO_MAX,
\t\t\t\"Collateral ratio must be == 1\"
\t\t);

\t\trequire(expireBlock >= block.number, \"POOL::mintAlgorithmicDEI: signature is expired.\");
        bytes32 sighash = keccak256(abi.encodePacked(collateral_address, collateral_price, expireBlock, getChainID()));
\t\trequire(IDEIStablecoin(dei_contract_address).verify_price(sighash, sigs), \"POOL::redeem1t1DEI: invalid signatures\");

\t\t// Need to adjust for decimals of collateral
\t\tuint256 DEI_amount_precision = DEI_amount / (10**missing_decimals);
\t\tuint256 collateral_needed = poolLibrary.calcRedeem1t1DEI(
\t\t\tcollateral_price,
\t\t\tDEI_amount_precision
\t\t);

\t\tcollateral_needed = (collateral_needed * (uint256(1e6) - redemption_fee)) / (1e6);
\t\trequire(
\t\t\tcollateral_needed <= collateral_token.balanceOf(address(this)) - unclaimedPoolCollateral,
\t\t\t\"Not enough collateral in pool\"
\t\t);

\t\tredeemCollateralBalances[msg.sender] = redeemCollateralBalances[msg.sender] + collateral_needed;
\t\tunclaimedPoolCollateral = unclaimedPoolCollateral + collateral_needed;
\t\tlastRedeemed[msg.sender] = block.number;

\t\tdaoShare += DEI_amount * redemption_fee / 1e6;
\t\t// Move all external functions to the end
\t\tIDEIStablecoin(dei_contract_address).pool_burn_from(msg.sender, DEI_amount);
\t}

\t// Will fail if fully collateralized or algorithmic
\t// Redeem DEI for collateral and DEUS. > 0% and < 100% collateral-backed
\tfunction redeemFractionalDEI(
\t\tuint256 DEI_amount,
\t\tuint256 collateral_price, 
\t\tuint256 deus_current_price,
\t\tuint256 expireBlock,
\t\tbytes[] calldata sigs
\t) external notRedeemPaused {
\t\tuint256 global_collateral_ratio = IDEIStablecoin(dei_contract_address).global_collateral_ratio();
\t\trequire(
\t\t\tglobal_collateral_ratio < COLLATERAL_RATIO_MAX && global_collateral_ratio > 0,
\t\t\t\"POOL::redeemFractionalDEI: Collateral ratio needs to be between .000001 and .999999\"
\t\t);

\t\trequire(expireBlock >= block.number, \"DEI::redeemFractionalDEI: signature is expired\");
\t\tbytes32 sighash = keccak256(abi.encodePacked(collateral_address, collateral_price, deus_contract_address, deus_current_price, expireBlock, getChainID()));
\t\trequire(IDEIStablecoin(dei_contract_address).verify_price(sighash, sigs), \"POOL::redeemFractionalDEI: invalid signatures\");

\t\t// Blocking is just for solving stack depth problem
\t\tuint256 deus_amount;
\t\tuint256 collateral_amount;
\t\t{
\t\t\tuint256 col_price_usd = collateral_price;

\t\t\tuint256 DEI_amount_post_fee = (DEI_amount * (uint256(1e6) - redemption_fee)) / (PRICE_PRECISION);

\t\t\tuint256 deus_dollar_value_d18 = DEI_amount_post_fee - ((DEI_amount_post_fee * global_collateral_ratio) / (PRICE_PRECISION));
\t\t\tdeus_amount = deus_dollar_value_d18 * (PRICE_PRECISION) / (deus_current_price);

\t\t\t// Need to adjust for decimals of collateral
\t\t\tuint256 DEI_amount_precision = DEI_amount_post_fee / (10**missing_decimals);
\t\t\tuint256 collateral_dollar_value = (DEI_amount_precision * global_collateral_ratio) / PRICE_PRECISION;
\t\t\tcollateral_amount = (collateral_dollar_value * PRICE_PRECISION) / (col_price_usd);
\t\t}
\t\trequire(
\t\t\tcollateral_amount <= collateral_token.balanceOf(address(this)) - unclaimedPoolCollateral,
\t\t\t\"Not enough collateral in pool\"
\t\t);

\t\tredeemCollateralBalances[msg.sender] = redeemCollateralBalances[msg.sender] + collateral_amount;
\t\tunclaimedPoolCollateral = unclaimedPoolCollateral + collateral_amount;

\t\tredeemDEUSBalances[msg.sender] = redeemDEUSBalances[msg.sender] + deus_amount;
\t\tunclaimedPoolDEUS = unclaimedPoolDEUS + deus_amount;

\t\tlastRedeemed[msg.sender] = block.number;

\t\tdaoShare += DEI_amount * redemption_fee / 1e6;
\t\t// Move all external functions to the end
\t\tIDEIStablecoin(dei_contract_address).pool_burn_from(msg.sender, DEI_amount);
\t\tIDEUSToken(deus_contract_address).pool_mint(address(this), deus_amount);
\t}

\t// Redeem DEI for DEUS. 0% collateral-backed
\tfunction redeemAlgorithmicDEI(
\t\tuint256 DEI_amount,
\t\tuint256 deus_current_price,
\t\tuint256 expireBlock,
\t\tbytes[] calldata sigs
\t) external notRedeemPaused {
\t\trequire(IDEIStablecoin(dei_contract_address).global_collateral_ratio() == 0, \"POOL::redeemAlgorithmicDEI: Collateral ratio must be 0\");

\t\trequire(expireBlock >= block.number, \"DEI::redeemAlgorithmicDEI: signature is expired.\");
\t\tbytes32 sighash = keccak256(abi.encodePacked(deus_contract_address, deus_current_price, expireBlock, getChainID()));
\t\trequire(IDEIStablecoin(dei_contract_address).verify_price(sighash, sigs), \"POOL::redeemAlgorithmicDEI: invalid signatures\");

\t\tuint256 deus_dollar_value_d18 = DEI_amount;

\t\tdeus_dollar_value_d18 = (deus_dollar_value_d18 * (uint256(1e6) - redemption_fee)) / 1e6; //apply fees

\t\tuint256 deus_amount = (deus_dollar_value_d18 * (PRICE_PRECISION)) / deus_current_price;

\t\tredeemDEUSBalances[msg.sender] = redeemDEUSBalances[msg.sender] + deus_amount;
\t\tunclaimedPoolDEUS = unclaimedPoolDEUS + deus_amount;

\t\tlastRedeemed[msg.sender] = block.number;

\t\tdaoShare += DEI_amount * redemption_fee / 1e6;
\t\t// Move all external functions to the end
\t\tIDEIStablecoin(dei_contract_address).pool_burn_from(msg.sender, DEI_amount);
\t\tIDEUSToken(deus_contract_address).pool_mint(address(this), deus_amount);
\t}

\t// After a redemption happens, transfer the newly minted DEUS and owed collateral from this pool
\t// contract to the user. Redemption is split into two functions to prevent flash loans from being able
\t// to take out DEI/collateral from the system, use an AMM to trade the new price, and then mint back into the system.
\tfunction collectRedemption() external {
\t\trequire(
\t\t\t(lastRedeemed[msg.sender] + redemption_delay) <= block.number,
\t\t\t\"POOL::collectRedemption: Must wait for redemption_delay blocks before collecting redemption\"
\t\t);
\t\tbool sendDEUS = false;
\t\tbool sendCollateral = false;
\t\tuint256 DEUSAmount = 0;
\t\tuint256 CollateralAmount = 0;

\t\t// Use Checks-Effects-Interactions pattern
\t\tif (redeemDEUSBalances[msg.sender] > 0) {
\t\t\tDEUSAmount = redeemDEUSBalances[msg.sender];
\t\t\tredeemDEUSBalances[msg.sender] = 0;
\t\t\tunclaimedPoolDEUS = unclaimedPoolDEUS - DEUSAmount;

\t\t\tsendDEUS = true;
\t\t}

\t\tif (redeemCollateralBalances[msg.sender] > 0) {
\t\t\tCollateralAmount = redeemCollateralBalances[msg.sender];
\t\t\tredeemCollateralBalances[msg.sender] = 0;
\t\t\tunclaimedPoolCollateral = unclaimedPoolCollateral - CollateralAmount;
\t\t\tsendCollateral = true;
\t\t}

\t\tif (sendDEUS) {
\t\t\tTransferHelper.safeTransfer(address(deus_contract_address), msg.sender, DEUSAmount);
\t\t}
\t\tif (sendCollateral) {
\t\t\tTransferHelper.safeTransfer(
\t\t\t\taddress(collateral_token),
\t\t\t\tmsg.sender,
\t\t\t\tCollateralAmount
\t\t\t);
\t\t}
\t}

\t// When the protocol is recollateralizing, we need to give a discount of DEUS to hit the new CR target
\t// Thus, if the target collateral ratio is higher than the actual value of collateral, minters get DEUS for adding collateral
\t// This function simply rewards anyone that sends collateral to a pool with the same amount of DEUS + the bonus rate
\t// Anyone can call this function to recollateralize the protocol and take the extra DEUS value from the bonus rate as an arb opportunity
\tfunction recollateralizeDEI(RecollateralizeDEI memory inputs) external {
\t\trequire(recollateralizePaused == false, \"POOL::recollateralizeDEI: Recollateralize is paused\");

\t\trequire(inputs.expireBlock >= block.number, \"POOL::recollateralizeDEI: signature is expired.\");
\t\tbytes32 sighash = keccak256(abi.encodePacked(
                                        collateral_address, 
                                        inputs.collateral_price,
                                        deus_contract_address, 
                                        inputs.deus_current_price, 
                                        inputs.expireBlock,
\t\t\t\t\t\t\t\t\t\tgetChainID()
                                    ));
\t\trequire(IDEIStablecoin(dei_contract_address).verify_price(sighash, inputs.sigs), \"POOL::recollateralizeDEI: invalid signatures\");

\t\tuint256 collateral_amount_d18 = inputs.collateral_amount * (10**missing_decimals);

\t\tuint256 dei_total_supply = IDEIStablecoin(dei_contract_address).totalSupply();
\t\tuint256 global_collateral_ratio = IDEIStablecoin(dei_contract_address).global_collateral_ratio();
\t\tuint256 global_collat_value = IDEIStablecoin(dei_contract_address).globalCollateralValue(inputs.collateral_price);

\t\t(uint256 collateral_units, uint256 amount_to_recollat) = poolLibrary.calcRecollateralizeDEIInner(
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tcollateral_amount_d18,
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tinputs.collateral_price[inputs.collateral_price.length - 1], // pool collateral price exist in last index
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tglobal_collat_value,
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tdei_total_supply,
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tglobal_collateral_ratio
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t);

\t\tuint256 collateral_units_precision = collateral_units / (10**missing_decimals);

\t\tuint256 deus_paid_back = (amount_to_recollat * (uint256(1e6) + bonus_rate - recollat_fee)) / inputs.deus_current_price;

\t\tTransferHelper.safeTransferFrom(
\t\t\taddress(collateral_token),
\t\t\tmsg.sender,
\t\t\taddress(this),
\t\t\tcollateral_units_precision
\t\t);
\t\tIDEUSToken(deus_contract_address).pool_mint(msg.sender, deus_paid_back);
\t}

\t// Function can be called by an DEUS holder to have the protocol buy back DEUS with excess collateral value from a desired collateral pool
\t// This can also happen if the collateral ratio > 1
\tfunction buyBackDEUS(
\t\tuint256 DEUS_amount,
\t\tuint256[] memory collateral_price,
\t\tuint256 deus_current_price,
\t\tuint256 expireBlock,
\t\tbytes[] calldata sigs
\t) external {
\t\trequire(buyBackPaused == false, \"POOL::buyBackDEUS: Buyback is paused\");
\t\trequire(expireBlock >= block.number, \"DEI::buyBackDEUS: signature is expired.\");
\t\tbytes32 sighash = keccak256(abi.encodePacked(
\t\t\t\t\t\t\t\t\t\tcollateral_address,
\t\t\t\t\t\t\t\t\t\tcollateral_price,
\t\t\t\t\t\t\t\t\t\tdeus_contract_address,
\t\t\t\t\t\t\t\t\t\tdeus_current_price,
\t\t\t\t\t\t\t\t\t\texpireBlock,
\t\t\t\t\t\t\t\t\t\tgetChainID()));
\t\trequire(IDEIStablecoin(dei_contract_address).verify_price(sighash, sigs), \"POOL::buyBackDEUS: invalid signatures\");

\t\tDEIPoolLibrary.BuybackDEUS_Params memory input_params = DEIPoolLibrary.BuybackDEUS_Params(
\t\t\t\t\t\t\t\t\t\t\t\t\tavailableExcessCollatDV(collateral_price),
\t\t\t\t\t\t\t\t\t\t\t\t\tdeus_current_price,
\t\t\t\t\t\t\t\t\t\t\t\t\tcollateral_price[collateral_price.length - 1], // pool collateral price exist in last index
\t\t\t\t\t\t\t\t\t\t\t\t\tDEUS_amount
\t\t\t\t\t\t\t\t\t\t\t\t);

\t\tuint256 collateral_equivalent_d18 = (poolLibrary.calcBuyBackDEUS(input_params) * (uint256(1e6) - buyback_fee)) / (1e6);
\t\tuint256 collateral_precision = collateral_equivalent_d18 / (10**missing_decimals);

\t\t// Give the sender their desired collateral and burn the DEUS
\t\tIDEUSToken(deus_contract_address).pool_burn_from(msg.sender, DEUS_amount);
\t\tTransferHelper.safeTransfer(
\t\t\taddress(collateral_token),
\t\t\tmsg.sender,
\t\t\tcollateral_precision
\t\t);
\t}

\t/* ========== RESTRICTED FUNCTIONS ========== */

\tfunction collectDaoShare(uint256 amount, address to) external {
\t\trequire(hasRole(DAO_SHARE_COLLECTOR, msg.sender));
\t\trequire(amount <= daoShare, \"amount<=daoShare\");
\t\tIDEIStablecoin(dei_contract_address).pool_mint(to, amount);
\t\tdaoShare -= amount;

\t\temit daoShareCollected(amount, to);
\t}

\tfunction emergencyWithdrawERC20(address token, uint amount, address to) external onlyByTrusty {
\t\tIERC20(token).transfer(to, amount);
\t}

\tfunction toggleMinting() external {
\t\trequire(hasRole(MINT_PAUSER, msg.sender));
\t\tmintPaused = !mintPaused;

\t\temit MintingToggled(mintPaused);
\t}

\tfunction toggleRedeeming() external {
\t\trequire(hasRole(REDEEM_PAUSER, msg.sender));
\t\tredeemPaused = !redeemPaused;

\t\temit RedeemingToggled(redeemPaused);
\t}

\tfunction toggleRecollateralize() external {
\t\trequire(hasRole(RECOLLATERALIZE_PAUSER, msg.sender));
\t\trecollateralizePaused = !recollateralizePaused;

\t\temit RecollateralizeToggled(recollateralizePaused);
\t}

\tfunction toggleBuyBack() external {
\t\trequire(hasRole(BUYBACK_PAUSER, msg.sender));
\t\tbuyBackPaused = !buyBackPaused;

\t\temit BuybackToggled(buyBackPaused);
\t}

\t// Combined into one function due to 24KiB contract memory limit
\tfunction setPoolParameters(
\t\tuint256 new_ceiling,
\t\tuint256 new_bonus_rate,
\t\tuint256 new_redemption_delay,
\t\tuint256 new_mint_fee,
\t\tuint256 new_redeem_fee,
\t\tuint256 new_buyback_fee,
\t\tuint256 new_recollat_fee
\t) external {
\t\trequire(hasRole(PARAMETER_SETTER_ROLE, msg.sender), \"POOL: Caller is not PARAMETER_SETTER_ROLE\");
\t\tpool_ceiling = new_ceiling;
\t\tbonus_rate = new_bonus_rate;
\t\tredemption_delay = new_redemption_delay;
\t\tminting_fee = new_mint_fee;
\t\tredemption_fee = new_redeem_fee;
\t\tbuyback_fee = new_buyback_fee;
\t\trecollat_fee = new_recollat_fee;

\t\temit PoolParametersSet(
\t\t\tnew_ceiling,
\t\t\tnew_bonus_rate,
\t\t\tnew_redemption_delay,
\t\t\tnew_mint_fee,
\t\t\tnew_redeem_fee,
\t\t\tnew_buyback_fee,
\t\t\tnew_recollat_fee
\t\t);
\t}

\t/* ========== EVENTS ========== */

\tevent PoolParametersSet(
\t\tuint256 new_ceiling,
\t\tuint256 new_bonus_rate,
\t\tuint256 new_redemption_delay,
\t\tuint256 new_mint_fee,
\t\tuint256 new_redeem_fee,
\t\tuint256 new_buyback_fee,
\t\tuint256 new_recollat_fee
\t);
\tevent daoShareCollected(uint256 daoShare, address to);
\tevent MintingToggled(bool toggled);
\tevent RedeemingToggled(bool toggled);
\tevent RecollateralizeToggled(bool toggled);
\tevent BuybackToggled(bool toggled);
}

//Dar panah khoda"
    
// Be name Khoda
// Bime Abolfazl

pragma solidity >=0.6.12;

import \"../Governance/AccessControl.sol\";
import \"@openzeppelin/contracts/utils/cryptography/ECDSA.sol\";

contract Oracle is AccessControl {
\tusing ECDSA for bytes32;

\t// role
\tbytes32 public constant ORACLE_ROLE = keccak256(\"ORACLE_ROLE\");
\tbytes32 public constant TRUSTY_ROLE = keccak256(\"TRUSTY_ROLE\");

\tuint256 minimumRequiredSignature;

\tevent MinimumRequiredSignatureSet(uint256 minimumRequiredSignature);

\tconstructor(address _admin, uint256 _minimumRequiredSignature, address _trusty_address) {
\t\trequire(_admin != address(0), \"ORACLE::constructor: Zero address detected\");
\t\t_setupRole(DEFAULT_ADMIN_ROLE, _admin);
\t\t_setupRole(TRUSTY_ROLE, _trusty_address);
\t\tminimumRequiredSignature = _minimumRequiredSignature;
\t}

\tfunction verify(bytes32 hash, bytes[] calldata sigs)
\t\tpublic
\t\tview
\t\treturns (bool)
\t{
\t\taddress lastOracle;
\t\tfor (uint256 index = 0; index < minimumRequiredSignature; ++index) {
\t\t\taddress oracle = hash.recover(sigs[index]);
\t\t\trequire(hasRole(ORACLE_ROLE, oracle), \"ORACLE::verify: Signer is not valid\");
\t\t\trequire(oracle > lastOracle, \"ORACLE::verify: Signers are same\");
\t\t\tlastOracle = oracle;
\t\t}
\t\treturn true;
\t}

\tfunction setMinimumRequiredSignature(uint256 _minimumRequiredSignature)
\t\tpublic
\t{
\t\trequire(
\t\t\thasRole(TRUSTY_ROLE, msg.sender),
\t\t\t\"ORACLE::setMinimumRequiredSignature: You are not a setter\"
\t\t);
\t\tminimumRequiredSignature = _minimumRequiredSignature;

\t\temit MinimumRequiredSignatureSet(_minimumRequiredSignature);
\t}
}

//Dar panah khoda"
    
// Be name Khoda
// Bime Abolfazl

// SPDX-License-Identifier: GPL-2.0-or-later
pragma solidity ^0.8.7;

// =================================================================================================================
//  _|_|_|    _|_|_|_|  _|    _|    _|_|_|      _|_|_|_|  _|                                                       |
//  _|    _|  _|        _|    _|  _|            _|            _|_|_|      _|_|_|  _|_|_|      _|_|_|    _|_|       |
//  _|    _|  _|_|_|    _|    _|    _|_|        _|_|_|    _|  _|    _|  _|    _|  _|    _|  _|        _|_|_|_|     |
//  _|    _|  _|        _|    _|        _|      _|        _|  _|    _|  _|    _|  _|    _|  _|        _|           |
//  _|_|_|    _|_|_|_|    _|_|    _|_|_|        _|        _|  _|    _|    _|_|_|  _|    _|    _|_|_|    _|_|_|     | 
// =================================================================================================================
// ====================================================================
// =========================== ReserveTracker =========================
// ====================================================================
// Deus Finance: https://github.com/DeusFinance

// Primary Author(s)
// Jason Huan: https://github.com/jasonhuan
// Sam Kazemian: https://github.com/samkazemian
// Vahid: https://github.com/vahid-dev
// SAYaghoubnejad: https://github.com/SAYaghoubnejad

// Reviewer(s) / Contributor(s)
// Travis Moore: https://github.com/FortisFortuna

import \"../Math/SafeMath.sol\";
import \"../Math/Math.sol\";
import \"../Uniswap/Interfaces/IUniswapV2Pair.sol\";
import \"../Governance/AccessControl.sol\";

contract ReserveTracker is AccessControl {

\t// Roles
    bytes32 public constant OWNER_ROLE = keccak256(\"OWNER_ROLE\");

\t// Various precisions
\tuint256 private PRICE_PRECISION = 1e6;

\t// Contract addresses
\taddress private dei_contract_address;
\taddress private deus_contract_address;

\t// Array of pairs for DEUS
\taddress[] public deus_pairs_array;

\t// Mapping is also used for faster verification
\tmapping(address => bool) public deus_pairs;

\tuint256 public deus_reserves;

\t// ========== MODIFIERS ==========

\tmodifier onlyByOwnerOrGovernance() {
\t\trequire(hasRole(OWNER_ROLE, msg.sender), \"Caller is not owner\");
\t\t_;
\t}

\t// ========== CONSTRUCTOR ==========

\tconstructor(
\t\taddress _dei_contract_address,
\t\taddress _deus_contract_address
\t) {
\t\tdei_contract_address = _dei_contract_address;
\t\tdeus_contract_address = _deus_contract_address;
\t\t_setupRole(DEFAULT_ADMIN_ROLE, msg.sender);
\t\t_setupRole(OWNER_ROLE, msg.sender);
\t}

\t// ========== VIEWS ==========

\tfunction getDEUSReserves() public view returns (uint256) {
\t\tuint256 total_deus_reserves = 0;

\t\tfor (uint i = 0; i < deus_pairs_array.length; i++){ 
\t\t\t// Exclude null addresses
\t\t\tif (deus_pairs_array[i] != address(0)){
\t\t\t\tif(IUniswapV2Pair(deus_pairs_array[i]).token0() == deus_contract_address) {
\t\t\t\t\t(uint reserves0, , ) = IUniswapV2Pair(deus_pairs_array[i]).getReserves();
\t\t\t\t\ttotal_deus_reserves = total_deus_reserves + reserves0;
\t\t\t\t} else if (IUniswapV2Pair(deus_pairs_array[i]).token1() == deus_contract_address) {
\t\t\t\t\t( , uint reserves1, ) = IUniswapV2Pair(deus_pairs_array[i]).getReserves();
\t\t\t\t\ttotal_deus_reserves = total_deus_reserves + reserves1;
\t\t\t\t}
\t\t\t}
\t\t}

\t\treturn total_deus_reserves;
\t}

\t// Adds collateral addresses supported, such as tether and busd, must be ERC20 
\tfunction addDEUSPair(address pair_address) public onlyByOwnerOrGovernance {
\t\trequire(deus_pairs[pair_address] == false, \"Address already exists\");
\t\tdeus_pairs[pair_address] = true; 
\t\tdeus_pairs_array.push(pair_address);
\t}

\t// Remove a pool 
\tfunction removeDEUSPair(address pair_address) public onlyByOwnerOrGovernance {
\t\trequire(deus_pairs[pair_address] == true, \"Address nonexistant\");
\t\t
\t\t// Delete from the mapping
\t\tdelete deus_pairs[pair_address];

\t\t// 'Delete' from the array by setting the address to 0x0
\t\tfor (uint i = 0; i < deus_pairs_array.length; i++){ 
\t\t\tif (deus_pairs_array[i] == pair_address) {
\t\t\t\tdeus_pairs_array[i] = address(0); // This will leave a null in the array and keep the indices the same
\t\t\t\tbreak;
\t\t\t}
\t\t}
\t}
}

//Dar panah khoda"
    
// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

/**
 * @dev Elliptic Curve Digital Signature Algorithm (ECDSA) operations.
 *
 * These functions can be used to verify that a message was signed by the holder
 * of the private keys of a given address.
 */
library ECDSA {
    /**
     * @dev Returns the address that signed a hashed message (`hash`) with
     * `signature`. This address can then be used for verification purposes.
     *
     * The `ecrecover` EVM opcode allows for malleable (non-unique) signatures:
     * this function rejects them by requiring the `s` value to be in the lower
     * half order, and the `v` value to be either 27 or 28.
     *
     * IMPORTANT: `hash` _must_ be the result of a hash operation for the
     * verification to be secure: it is possible to craft signatures that
     * recover to arbitrary addresses for non-hashed data. A safe way to ensure
     * this is by receiving a hash of the original message (which may otherwise
     * be too long), and then calling {toEthSignedMessageHash} on it.
     *
     * Documentation for signature generation:
     * - with https://web3js.readthedocs.io/en/v1.3.4/web3-eth-accounts.html#sign[Web3.js]
     * - with https://docs.ethers.io/v5/api/signer/#Signer-signMessage[ethers]
     */
    function recover(bytes32 hash, bytes memory signature) internal pure returns (address) {
        // Check the signature length
        // - case 65: r,s,v signature (standard)
        // - case 64: r,vs signature (cf https://eips.ethereum.org/EIPS/eip-2098) _Available since v4.1._
        if (signature.length == 65) {
            bytes32 r;
            bytes32 s;
            uint8 v;
            // ecrecover takes the signature parameters, and the only way to get them
            // currently is to use assembly.
            assembly {
                r := mload(add(signature, 0x20))
                s := mload(add(signature, 0x40))
                v := byte(0, mload(add(signature, 0x60)))
            }
            return recover(hash, v, r, s);
        } else if (signature.length == 64) {
            bytes32 r;
            bytes32 vs;
            // ecrecover takes the signature parameters, and the only way to get them
            // currently is to use assembly.
            assembly {
                r := mload(add(signature, 0x20))
                vs := mload(add(signature, 0x40))
            }
            return recover(hash, r, vs);
        } else {
            revert(\"ECDSA: invalid signature length\");
        }
    }

    /**
     * @dev Overload of {ECDSA-recover} that receives the `r` and `vs` short-signature fields separately.
     *
     * See https://eips.ethereum.org/EIPS/eip-2098[EIP-2098 short signatures]
     *
     * _Available since v4.2._
     */
    function recover(
        bytes32 hash,
        bytes32 r,
        bytes32 vs
    ) internal pure returns (address) {
        bytes32 s;
        uint8 v;
        assembly {
            s := and(vs, 0x7fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff)
            v := add(shr(255, vs), 27)
        }
        return recover(hash, v, r, s);
    }

    /**
     * @dev Overload of {ECDSA-recover} that receives the `v`, `r` and `s` signature fields separately.
     */
    function recover(
        bytes32 hash,
        uint8 v,
        bytes32 r,
        bytes32 s
    ) internal pure returns (address) {
        // EIP-2 still allows signature malleability for ecrecover(). Remove this possibility and make the signature
        // unique. Appendix F in the Ethereum Yellow paper (https://ethereum.github.io/yellowpaper/paper.pdf), defines
        // the valid range for s in (281): 0 < s < secp256k1n ÷ 2 + 1, and for v in (282): v ∈ {27, 28}. Most
        // signatures from current libraries generate a unique signature with an s-value in the lower half order.
        //
        // If your library generates malleable signatures, such as s-values in the upper range, calculate a new s-value
        // with 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEBAAEDCE6AF48A03BBFD25E8CD0364141 - s1 and flip v from 27 to 28 or
        // vice versa. If your library also generates signatures with 0/1 for v instead 27/28, add 27 to v to accept
        // these malleable signatures as well.
        require(
            uint256(s) <= 0x7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF5D576E7357A4501DDFE92F46681B20A0,
            \"ECDSA: invalid signature 's' value\"
        );
        require(v == 27 || v == 28, \"ECDSA: invalid signature 'v' value\");

        // If the signature is valid (and not malleable), return the signer address
        address signer = ecrecover(hash, v, r, s);
        require(signer != address(0), \"ECDSA: invalid signature\");

        return signer;
    }

    /**
     * @dev Returns an Ethereum Signed Message, created from a `hash`. This
     * produces hash corresponding to the one signed with the
     * https://eth.wiki/json-rpc/API#eth_sign[`eth_sign`]
     * JSON-RPC method as part of EIP-191.
     *
     * See {recover}.
     */
    function toEthSignedMessageHash(bytes32 hash) internal pure returns (bytes32) {
        // 32 is the length in bytes of hash,
        // enforced by the type signature above
        return keccak256(abi.encodePacked(\"\\x19Ethereum Signed Message:\
32\", hash));
    }

    /**
     * @dev Returns an Ethereum Signed Typed Data, created from a
     * `domainSeparator` and a `structHash`. This produces hash corresponding
     * to the one signed with the
     * https://eips.ethereum.org/EIPS/eip-712[`eth_signTypedData`]
     * JSON-RPC method as part of EIP-712.
     *
     * See {recover}.
     */
    function toTypedDataHash(bytes32 domainSeparator, bytes32 structHash) internal pure returns (bytes32) {
        return keccak256(abi.encodePacked(\"\\x19\\x01\", domainSeparator, structHash));
    }
}
"
    
// SPDX-License-Identifier: MIT

pragma solidity >=0.6.11;

/**
 * @dev Library for managing
 * https://en.wikipedia.org/wiki/Set_(abstract_data_type)[sets] of primitive
 * types.
 *
 * Sets have the following properties:
 *
 * - Elements are added, removed, and checked for existence in constant time
 * (O(1)).
 * - Elements are enumerated in O(n). No guarantees are made on the ordering.
 *
 * ```
 * contract Example {
 *     // Add the library methods
 *     using EnumerableSet for EnumerableSet.AddressSet;
 *
 *     // Declare a set state variable
 *     EnumerableSet.AddressSet private mySet;
 * }
 * ```
 *
 * As of v3.0.0, only sets of type `address` (`AddressSet`) and `uint256`
 * (`UintSet`) are supported.
 */
library EnumerableSet {
    // To implement this library for multiple types with as little code
    // repetition as possible, we write it in terms of a generic Set type with
    // bytes32 values.
    // The Set implementation uses private functions, and user-facing
    // implementations (such as AddressSet) are just wrappers around the
    // underlying Set.
    // This means that we can only create new EnumerableSets for types that fit
    // in bytes32.

    struct Set {
        // Storage of set values
        bytes32[] _values;

        // Position of the value in the `values` array, plus 1 because index 0
        // means a value is not in the set.
        mapping (bytes32 => uint256) _indexes;
    }

    /**
     * @dev Add a value to a set. O(1).
     *
     * Returns true if the value was added to the set, that is if it was not
     * already present.
     */
    function _add(Set storage set, bytes32 value) private returns (bool) {
        if (!_contains(set, value)) {
            set._values.push(value);
            // The value is stored at length-1, but we add 1 to all indexes
            // and use 0 as a sentinel value
            set._indexes[value] = set._values.length;
            return true;
        } else {
            return false;
        }
    }

    /**
     * @dev Removes a value from a set. O(1).
     *
     * Returns true if the value was removed from the set, that is if it was
     * present.
     */
    function _remove(Set storage set, bytes32 value) private returns (bool) {
        // We read and store the value's index to prevent multiple reads from the same storage slot
        uint256 valueIndex = set._indexes[value];

        if (valueIndex != 0) { // Equivalent to contains(set, value)
            // To delete an element from the _values array in O(1), we swap the element to delete with the last one in
            // the array, and then remove the last element (sometimes called as 'swap and pop').
            // This modifies the order of the array, as noted in {at}.

            uint256 toDeleteIndex = valueIndex - 1;
            uint256 lastIndex = set._values.length - 1;

            // When the value to delete is the last one, the swap operation is unnecessary. However, since this occurs
            // so rarely, we still do the swap anyway to avoid the gas cost of adding an 'if' statement.

            bytes32 lastvalue = set._values[lastIndex];

            // Move the last value to the index where the value to delete is
            set._values[toDeleteIndex] = lastvalue;
            // Update the index for the moved value
            set._indexes[lastvalue] = toDeleteIndex + 1; // All indexes are 1-based

            // Delete the slot where the moved value was stored
            set._values.pop();

            // Delete the index for the deleted slot
            delete set._indexes[value];

            return true;
        } else {
            return false;
        }
    }

    /**
     * @dev Returns true if the value is in the set. O(1).
     */
    function _contains(Set storage set, bytes32 value) private view returns (bool) {
        return set._indexes[value] != 0;
    }

    /**
     * @dev Returns the number of values on the set. O(1).
     */
    function _length(Set storage set) private view returns (uint256) {
        return set._values.length;
    }

   /**
    * @dev Returns the value stored at position `index` in the set. O(1).
    *
    * Note that there are no guarantees on the ordering of values inside the
    * array, and it may change when more values are added or removed.
    *
    * Requirements:
    *
    * - `index` must be strictly less than {length}.
    */
    function _at(Set storage set, uint256 index) private view returns (bytes32) {
        require(set._values.length > index, \"EnumerableSet: index out of bounds\");
        return set._values[index];
    }

    // AddressSet

    struct AddressSet {
        Set _inner;
    }

    /**
     * @dev Add a value to a set. O(1).
     *
     * Returns true if the value was added to the set, that is if it was not
     * already present.
     */
    function add(AddressSet storage set, address value) internal returns (bool) {
        return _add(set._inner, bytes32(bytes20(value)));
    }

    /**
     * @dev Removes a value from a set. O(1).
     *
     * Returns true if the value was removed from the set, that is if it was
     * present.
     */
    function remove(AddressSet storage set, address value) internal returns (bool) {
        return _remove(set._inner, bytes32(bytes20(value)));
    }

    /**
     * @dev Returns true if the value is in the set. O(1).
     */
    function contains(AddressSet storage set, address value) internal view returns (bool) {
        return _contains(set._inner, bytes32(bytes20(value)));
    }

    /**
     * @dev Returns the number of values in the set. O(1).
     */
    function length(AddressSet storage set) internal view returns (uint256) {
        return _length(set._inner);
    }

   /**
    * @dev Returns the value stored at position `index` in the set. O(1).
    *
    * Note that there are no guarantees on the ordering of values inside the
    * array, and it may change when more values are added or removed.
    *
    * Requirements:
    *
    * - `index` must be strictly less than {length}.
    */
    function at(AddressSet storage set, uint256 index) internal view returns (address) {
        return address(bytes20(_at(set._inner, index)));
    }


    // UintSet

    struct UintSet {
        Set _inner;
    }

    /**
     * @dev Add a value to a set. O(1).
     *
     * Returns true if the value was added to the set, that is if it was not
     * already present.
     */
    function add(UintSet storage set, uint256 value) internal returns (bool) {
        return _add(set._inner, bytes32(value));
    }

    /**
     * @dev Removes a value from a set. O(1).
     *
     * Returns true if the value was removed from the set, that is if it was
     * present.
     */
    function remove(UintSet storage set, uint256 value) internal returns (bool) {
        return _remove(set._inner, bytes32(value));
    }

    /**
     * @dev Returns true if the value is in the set. O(1).
     */
    function contains(UintSet storage set, uint256 value) internal view returns (bool) {
        return _contains(set._inner, bytes32(value));
    }

    /**
     * @dev Returns the number of values on the set. O(1).
     */
    function length(UintSet storage set) internal view returns (uint256) {
        return _length(set._inner);
    }

   /**
    * @dev Returns the value stored at position `index` in the set. O(1).
    *
    * Note that there are no guarantees on the ordering of values inside the
    * array, and it may change when more values are added or removed.
    *
    * Requirements:
    *
    * - `index` must be strictly less than {length}.
    */
    function at(UintSet storage set, uint256 index) internal view returns (uint256) {
        return uint256(_at(set._inner, index));
    }
}
"
    
// SPDX-License-Identifier: MIT
pragma solidity >=0.6.11;

// helper methods for interacting with ERC20 tokens and sending ETH that do not consistently return true/false
library TransferHelper {
    function safeApprove(address token, address to, uint value) internal {
        // bytes4(keccak256(bytes('approve(address,uint256)')));
        (bool success, bytes memory data) = token.call(abi.encodeWithSelector(0x095ea7b3, to, value));
        require(success && (data.length == 0 || abi.decode(data, (bool))), 'TransferHelper: APPROVE_FAILED');
    }

    function safeTransfer(address token, address to, uint value) internal {
        // bytes4(keccak256(bytes('transfer(address,uint256)')));
        (bool success, bytes memory data) = token.call(abi.encodeWithSelector(0xa9059cbb, to, value));
        require(success && (data.length == 0 || abi.decode(data, (bool))), 'TransferHelper: TRANSFER_FAILED');
    }

    function safeTransferFrom(address token, address from, address to, uint value) internal {
        // bytes4(keccak256(bytes('transferFrom(address,address,uint256)')));
        (bool success, bytes memory data) = token.call(abi.encodeWithSelector(0x23b872dd, from, to, value));
        require(success && (data.length == 0 || abi.decode(data, (bool))), 'TransferHelper: TRANSFER_FROM_FAILED');
    }

    function safeTransferETH(address to, uint value) internal {
        (bool success,) = to.call{value:value}(new bytes(0));
        require(success, 'TransferHelper: ETH_TRANSFER_FAILED');
    }
}"
    
// Be name Khoda
// Bime Abolfazl

// SPDX-License-Identifier: GPL-2.0-or-later

interface IDEUSToken {
    function setDEIAddress(address dei_contract_address) external;
    function mint(address to, uint256 amount) external;

    // This function is what other dei pools will call to mint new DEUS (similar to the DEI mint)
    function pool_mint(address m_address, uint256 m_amount) external;

    // This function is what other dei pools will call to burn DEUS
    function pool_burn_from(address b_address, uint256 b_amount) external;

    function toggleVotes() external;

    /* ========== OVERRIDDEN PUBLIC FUNCTIONS ========== */

    function transfer(address recipient, uint256 amount) external returns (bool);

    function transferFrom(
        address sender,
        address recipient,
        uint256 amount
    ) external returns (bool);

    /* ========== PUBLIC FUNCTIONS ========== */

    /**
     * @notice Gets the current votes balance for `account`
     * @param account The address to get votes balance
     * @return The number of current votes for `account`
     */
    function getCurrentVotes(address account) external view returns (uint96);

    /**
     * @notice Determine the prior number of votes for an account as of a block number
     * @dev Block number must be a finalized block or else this function will revert to prevent misinformation.
     * @param account The address of the account to check
     * @param blockNumber The block number to get the vote balance at
     * @return The number of votes the account had as of the given block
     */
    function getPriorVotes(address account, uint256 blockNumber)
        external
        view
        returns (uint96);
}

//Dar panah khoda"
    
// Be name Khoda
// Bime Abolfazl

// SPDX-License-Identifier: GPL-2.0-or-later

interface IDEIStablecoin {

    function totalSupply() external view returns (uint256);

    function global_collateral_ratio() external view returns (uint256);
\t
    function verify_price(bytes32 sighash, bytes[] calldata sigs) external view returns (bool);

\tfunction dei_info(uint256 eth_usd_price, uint256 eth_collat_price)
\t\texternal
\t\tview
\t\treturns (
\t\t\tuint256,
\t\t\tuint256,
\t\t\tuint256
\t\t);

\tfunction globalCollateralValue(uint256[] memory collat_usd_price) external view returns (uint256);

\tfunction refreshCollateralRatio(uint256 dei_price_cur, uint256 expireBlock, bytes[] calldata sigs) external;

\tfunction pool_burn_from(address b_address, uint256 b_amount) external;

\tfunction pool_mint(address m_address, uint256 m_amount) external;

\tfunction addPool(address pool_address) external;

\tfunction removePool(address pool_address) external;

\tfunction setDEIStep(uint256 _new_step) external;

\tfunction setPriceTarget(uint256 _new_price_target) external;

\tfunction setRefreshCooldown(uint256 _new_cooldown) external;

\tfunction setDEUSAddress(address _deus_address) external;

\tfunction setPriceBand(uint256 _price_band) external;
\t
    function toggleCollateralRatio() external;
}

//Dar panah khoda"
    
// Be name Khoda
// Bime Abolfazl

// SPDX-License-Identifier: GPL-2.0-or-later
pragma solidity >=0.8.0;

contract DEIPoolLibrary {

    // Constants for various precisions
    uint256 private constant PRICE_PRECISION = 1e6;

    constructor() {}

    // ================ Structs ================
    // Needed to lower stack size
    struct MintFD_Params {
        uint256 deus_price_usd; 
        uint256 col_price_usd;
        uint256 collateral_amount;
        uint256 col_ratio;
    }

    struct BuybackDEUS_Params {
        uint256 excess_collateral_dollar_value_d18;
        uint256 deus_price_usd;
        uint256 col_price_usd;
        uint256 DEUS_amount;
    }

    // ================ Functions ================

    function calcMint1t1DEI(uint256 col_price, uint256 collateral_amount_d18) public pure returns (uint256) {
        return (collateral_amount_d18 * col_price) / (1e6);
    }

    function calcMintAlgorithmicDEI(uint256 deus_price_usd, uint256 deus_amount_d18) public pure returns (uint256) {
        return (deus_amount_d18 * deus_price_usd) / (1e6);
    }

    // Must be internal because of the struct
    function calcMintFractionalDEI(MintFD_Params memory params) public pure returns (uint256, uint256) {
        // Since solidity truncates division, every division operation must be the last operation in the equation to ensure minimum error
        // The contract must check the proper ratio was sent to mint DEI. We do this by seeing the minimum mintable DEI based on each amount 
        uint256 c_dollar_value_d18;
        
        // Scoping for stack concerns
        {    
            // USD amounts of the collateral and the DEUS
            c_dollar_value_d18 = (params.collateral_amount * params.col_price_usd) / (1e6);

        }
        uint calculated_deus_dollar_value_d18 = ((c_dollar_value_d18 * (1e6)) / params.col_ratio) - c_dollar_value_d18;

        uint calculated_deus_needed = (calculated_deus_dollar_value_d18 * (1e6)) / params.deus_price_usd;

        return (
            c_dollar_value_d18 + calculated_deus_dollar_value_d18,
            calculated_deus_needed
        );
    }

    function calcRedeem1t1DEI(uint256 col_price_usd, uint256 DEI_amount) public pure returns (uint256) {
        return (DEI_amount * (1e6)) / col_price_usd;
    }

    // Must be internal because of the struct
    function calcBuyBackDEUS(BuybackDEUS_Params memory params) public pure returns (uint256) {
        // If the total collateral value is higher than the amount required at the current collateral ratio then buy back up to the possible DEUS with the desired collateral
        require(params.excess_collateral_dollar_value_d18 > 0, \"No excess collateral to buy back!\");

        // Make sure not to take more than is available
        uint256 deus_dollar_value_d18 = (params.DEUS_amount * (params.deus_price_usd)) / (1e6);
        require(deus_dollar_value_d18 <= params.excess_collateral_dollar_value_d18, \"You are trying to buy back more than the excess!\");

        // Get the equivalent amount of collateral based on the market value of DEUS provided 
        uint256 collateral_equivalent_d18 = (deus_dollar_value_d18 * (1e6)) / params.col_price_usd;
        //collateral_equivalent_d18 = collateral_equivalent_d18.sub((collateral_equivalent_d18.mul(params.buyback_fee)).div(1e6));

        return collateral_equivalent_d18;

    }


    // Returns value of collateral that must increase to reach recollateralization target (if 0 means no recollateralization)
    function recollateralizeAmount(uint256 total_supply, uint256 global_collateral_ratio, uint256 global_collat_value) public pure returns (uint256) {
        uint256 target_collat_value = (total_supply * global_collateral_ratio) / (1e6); // We want 18 decimals of precision so divide by 1e6; total_supply is 1e18 and global_collateral_ratio is 1e6
        // Subtract the current value of collateral from the target value needed, if higher than 0 then system needs to recollateralize
        return target_collat_value - global_collat_value; // If recollateralization is not needed, throws a subtraction underflow
        // return(recollateralization_left);
    }

    function calcRecollateralizeDEIInner(
        uint256 collateral_amount, 
        uint256 col_price,
        uint256 global_collat_value,
        uint256 dei_total_supply,
        uint256 global_collateral_ratio
    ) public pure returns (uint256, uint256) {
        uint256 collat_value_attempted = (collateral_amount * col_price) / (1e6);
        uint256 effective_collateral_ratio = (global_collat_value * (1e6)) / dei_total_supply; //returns it in 1e6
        uint256 recollat_possible = (global_collateral_ratio * dei_total_supply - (dei_total_supply * effective_collateral_ratio)) / (1e6);

        uint256 amount_to_recollat;
        if(collat_value_attempted <= recollat_possible){
            amount_to_recollat = collat_value_attempted;
        } else {
            amount_to_recollat = recollat_possible;
        }

        return ((amount_to_recollat * (1e6)) / col_price, amount_to_recollat);

    }

}

//Dar panah khoda"
    
// SPDX-License-Identifier: MIT
pragma solidity >=0.6.11;

/**
 * @dev Standard math utilities missing in the Solidity language.
 */
library Math {
    /**
     * @dev Returns the largest of two numbers.
     */
    function max(uint256 a, uint256 b) internal pure returns (uint256) {
        return a >= b ? a : b;
    }

    /**
     * @dev Returns the smallest of two numbers.
     */
    function min(uint256 a, uint256 b) internal pure returns (uint256) {
        return a < b ? a : b;
    }

    /**
     * @dev Returns the average of two numbers. The result is rounded towards
     * zero.
     */
    function average(uint256 a, uint256 b) internal pure returns (uint256) {
        // (a + b) / 2 can overflow, so we distribute
        return (a / 2) + (b / 2) + ((a % 2 + b % 2) / 2);
    }

    // babylonian method (https://en.wikipedia.org/wiki/Methods_of_computing_square_roots#Babylonian_method)
    function sqrt(uint y) internal pure returns (uint z) {
        if (y > 3) {
            z = y;
            uint x = y / 2 + 1;
            while (x < z) {
                z = x;
                x = (y / x + x) / 2;
            }
        } else if (y != 0) {
            z = 1;
        }
    }
}"
    
// SPDX-License-Identifier: MIT
pragma solidity >=0.6.11;

interface IUniswapV2Pair {
    event Approval(address indexed owner, address indexed spender, uint value);
    event Transfer(address indexed from, address indexed to, uint value);

    function name() external pure returns (string memory);
    function symbol() external pure returns (string memory);
    function decimals() external pure returns (uint8);
    function totalSupply() external view returns (uint);
    function balanceOf(address owner) external view returns (uint);
    function allowance(address owner, address spender) external view returns (uint);

    function approve(address spender, uint value) external returns (bool);
    function transfer(address to, uint value) external returns (bool);
    function transferFrom(address from, address to, uint value) external returns (bool);

    function DOMAIN_SEPARATOR() external view returns (bytes32);
    function PERMIT_TYPEHASH() external pure returns (bytes32);
    function nonces(address owner) external view returns (uint);

    function permit(address owner, address spender, uint value, uint deadline, uint8 v, bytes32 r, bytes32 s) external;

    event Mint(address indexed sender, uint amount0, uint amount1);
    event Burn(address indexed sender, uint amount0, uint amount1, address indexed to);
    event Swap(
        address indexed sender,
        uint amount0In,
        uint amount1In,
        uint amount0Out,
        uint amount1Out,
        address indexed to
    );
    event Sync(uint112 reserve0, uint112 reserve1);

    function MINIMUM_LIQUIDITY() external pure returns (uint);
    function factory() external view returns (address);
    function token0() external view returns (address);
    function token1() external view returns (address);
    function getReserves() external view returns (uint112 reserve0, uint112 reserve1, uint32 blockTimestampLast);
    function price0CumulativeLast() external view returns (uint);
    function price1CumulativeLast() external view returns (uint);
    function kLast() external view returns (uint);

    function mint(address to) external returns (uint liquidity);
    function burn(address to) external returns (uint amount0, uint amount1);
    function swap(uint amount0Out, uint amount1Out, address to, bytes calldata data) external;
    function skim(address to) external;
    function sync() external;

    function initialize(address, address) external;












    
}
"
    }
  
