// SPDX-License-Identifier: -- 💰 --

pragma solidity ^0.7.3;

abstract contract Declaration  {

    string public name = \"Feyorra\";
    string public symbol = \"FEY\";

    uint256 public decimals = 18;
    uint256 public totalSupply = 1000000000E18;

    uint256 public constant YEARLY_INTEREST = 410;
    uint256 public constant MINIMUM_STAKE = 100E18;
    uint256 public constant SECONDS_IN_DAY = 86400;
    uint256 public constant MAX_STAKE_DAYS = 1825;

    uint256 public immutable LAUNCH_TIME;

    struct Globals {
        uint256 stakingId;
        uint256 currentFeyDay;
        uint256 totalStakedAmount;
    }

    struct StakeElement {
        address userAddress;
        uint256 stakedAmount;
        uint256 returnAmount;
        uint256 interestAmount;
        uint256 stakedAt;
        bool isActive;
    }

    struct SnapShot {
        uint256 totalSupply;
        uint256 totalStakedAmount;
    }

    mapping(address =\u003e uint256) public balances;
    mapping(address =\u003e mapping(address =\u003e uint256)) public allowances;

    mapping(uint256 =\u003e SnapShot) public snapshots;
    mapping(uint256 =\u003e StakeElement) public stakeList;

    Globals public globals;

    modifier incrementId() {
        _;
        globals.stakingId++;
    }

    constructor() {
        LAUNCH_TIME = block.timestamp;
        balances[msg.sender] = totalSupply;
    }
}"},"Events.sol":{"content":"// SPDX-License-Identifier: -- 💰 --

pragma solidity ^0.7.3;

abstract contract Events  {

    event StakeStart(
        uint256 indexed _stakingId,
        address _address,
        uint256 _amount
    );

    event StakeEnd(
        uint256 indexed _stakingId,
        address _address,
        uint256 _amount
    );

    event Transfer(
        address indexed _from,
        address indexed _to,
        uint256 _value
    );

    event Approval(
        address indexed _owner,
        address indexed _spender,
        uint256 _value
    );

    event ClosedGhostStake(
        uint256 daysOld,
        uint256 secondsOld,
        uint256 stakeId
    );

    event SnapshotCaptured(
        uint256 _totalSupply,
        uint256 _totalStakedAmount,
        uint64 _snapshotDay
    );
}"},"FEYToken.sol":{"content":"// SPDX-License-Identifier: -- 💰 --

import \u0027./Token.sol\u0027;

pragma solidity ^0.7.3;

contract FEYToken is Token {

    using SafeMath for uint256;

    /**
        * @notice returns the interest rate by getting the global variable
        * YEARLY_INTEREST and subtracting the percentage passed.
        * Lowers the default interest rate as amount staked rises towards the totalSupply.
        * Used to record the rates for snapshots + to calculate stake interest
        * @param _percentage any uint256 figure
        * @return interestRate
     */
    function getInterestRateYearly(
        uint256 _percentage
    )
        public
        pure
        returns (uint256 interestRate)
    {
        return _percentage \u003e 100
            ? uint256(YEARLY_INTEREST).mul(uint256(10000)).div(_percentage)
            : YEARLY_INTEREST.mul(100);
    }
    
    /**
        * @notice a no args function used to get current APY
        * @dev _precision in getPercent is fixed to 4
        * @return percentage -- totalStaked on a particular day out of totalSupply
        * @return interestRateYearly -- APY based on relative size of current total stakes
     */
    function getYearlyInterestLatest()
        public
        view
        returns (
            uint256 percentage,
            uint256 interestRateYearly
        )
    {
        percentage = getPercent(
            globals.totalStakedAmount,
            totalSupply,
            4
        );

        interestRateYearly = getInterestRateYearly(
            percentage
        );
    }

    /**
        * @notice function used to get APY of a specific day
        * @param _day integer for the target day, starting @ 0
        * @dev _precision in getPercent is fixed to 4
        * @return percentage -- totalStaked on a particular day out of totalSupply
        * @return interestRateYearly -- APY based on relative size of stake
     */
    function getYearlyInterestHistorical(
        uint256 _day
    )
        public
        view
        returns (
            uint256 percentage,
            uint256 interestRateYearly
        )
    {
        SnapShot memory s = snapshots[_day];

        if (s.totalSupply == 0) {
            return getYearlyInterestLatest();
        }

        percentage = getPercent(
            s.totalStakedAmount,
            s.totalSupply,
            4
        );

        interestRateYearly = getInterestRateYearly(
            percentage
        );
    }

    /**
        * @notice calculates amount of interest earned per second
        * @param _stakedAmount principal amount
        * @param _totalStakedAmount summation of principal amount staked by everyone
        * @param _seconds time spent earning interest on a particular day
        * _seconds will be passed as the full SECONDS_IN_DAY for full days that we staked
        * _seconds will be the seconds that have passed by the time getInterest is called on the last day
        * @dev _precision in getPercent is fixed to 4
        * @return durationInterestAmt -- totalStaked on a particular day out of totalSupply
     */
    function getInterest(
        uint256 _stakedAmount,
        uint256 _totalStakedAmount,
        uint256 _seconds
    )
        public
        view
        returns (uint256 durationInterestAmt)
    {
        uint256 percentage = getPercent(
            _totalStakedAmount,
            totalSupply,
            4
        );

        uint256 interestRateYearly = getInterestRateYearly(
            percentage
        );

        uint256 yearFullAmount = _stakedAmount
            .mul(interestRateYearly)
            .div(100);

        uint256 dailyInterestAmt = getPercent(
            yearFullAmount,
            31556952,
            0
        );

        durationInterestAmt = dailyInterestAmt
            .mul(_seconds)
            .div(100);
    }

    /**
         * @notice admin function to close a matured stake OBO the staker
         * @param _stakingId ID of the stake, used as the Key from the stakeList mapping
         * @dev can only close after all of the seconds of the last day have passed
      */
    function closeGhostStake(
        uint256 _stakingId
    )
        external
        onlyOwner
    {
        (uint256 daysOld, uint256 secondsOld) =

        getStakeAge(
            _stakingId
        );

        require(
            daysOld == MAX_STAKE_DAYS \u0026\u0026
            secondsOld == SECONDS_IN_DAY,
            \u0027FEYToken: not old enough\u0027
        );

        _closeStake(
            stakeList[_stakingId].userAddress,
            _stakingId
        );

        emit ClosedGhostStake(
            daysOld,
            secondsOld,
            _stakingId
        );
    }

    /**
        * @notice calculates number of days and remaining seconds on current day that a stake is open
        * @param _stakingId ID of the stake, used as the Key from the stakeList mapping
        * @return daysTotal -- number of complete days that the stake has been open
        * @return secondsToday -- number of seconds the stake has been open on the current day
     */
    function getStakeAge(
        uint256 _stakingId
    )
        public
        view
        returns (
            uint256 daysTotal,
            uint256 secondsToday
        )
    {
        StakeElement memory _stakeElement = stakeList[_stakingId];

        uint256 secondsTotal = getNow()
            .sub(_stakeElement.stakedAt);

        daysTotal = secondsTotal
            .div(SECONDS_IN_DAY);

        if (daysTotal \u003e MAX_STAKE_DAYS) {

            daysTotal = MAX_STAKE_DAYS;
            secondsToday = SECONDS_IN_DAY;

        } else {
            secondsToday = secondsTotal
                .mod(SECONDS_IN_DAY);
        }
    }

    /**
        * @notice calculates amount of interest due to be credited to the staker based on:
        * number of days and remaining seconds on current day that a stake is open
        * @param _stakingId ID of the stake, used as the Key from the stakeList mapping
        * @return stakeInterest -- total interest per second the stake was open on each day
     */
    function getStakeInterest(
        uint256 _stakingId
    )
        public
        view
        returns (
            uint256 stakeInterest
        )
    {
        StakeElement memory _stakeElement = stakeList[_stakingId];

        if (_stakeElement.isActive == false) {

            stakeInterest = _stakeElement.interestAmount;

        } else {

            (
                uint256 daysTotal,
                uint256 secondsToday
            ) = getStakeAge(_stakingId);

            uint256 finalDay = _currentFeyDay();
            uint256 startDay = finalDay.sub(daysTotal);

            for (uint256 _day = startDay; _day \u003c finalDay; _day++) {
                stakeInterest += getInterest(
                    _stakeElement.stakedAmount,
                    snapshots[_day].totalStakedAmount,
                    SECONDS_IN_DAY
                );
            }

            stakeInterest += getInterest(
                _stakeElement.stakedAmount,
                globals.totalStakedAmount,
                secondsToday
            );
        }
    }

    /**
        * @notice penalties are taken if you close a stake before the completion of the 4th day
        * if closed before the end of the 15th day: 7.5% of staked amount is penalized
        * if closed before the end of the 30th day: 5% of staked amount is penalized
        * if closed before the end of the 45th day: 2.5% of staked amount is penalized
        * @param _stakingId ID of the stake, used as the Key from the stakeList mapping
        * @return penaltyAmount -- amount that will be debited from the stakers principal when they close their stake
     */
    function getStakePenalty(
        uint256 _stakingId
    )
        public
        view
        returns (uint256 penaltyAmount)
    {
        StakeElement memory _stakeElement = stakeList[_stakingId];

        uint256 daysDifference = getNow()
            .sub(_stakeElement.stakedAt)
            .div(SECONDS_IN_DAY);

        if (daysDifference \u003c 15) {

            penaltyAmount = percentCalculator(
                _stakeElement.stakedAmount,
                750
            );

        } else if (daysDifference \u003c 30) {

            penaltyAmount = percentCalculator(
                _stakeElement.stakedAmount,
                500
            );

        } else if (daysDifference \u003c 45) {

            penaltyAmount = percentCalculator(
                _stakeElement.stakedAmount,
                250
            );
        }
    }

    /**
        * @notice calculates principal + interest - penalty (if applicable)
        * Note: this does not calculate a return rate, only what the sum would be if the stake was closed at that moment
        * @param _stakingId ID of the stake, used as the Key from the stakeList mapping
        * @dev the calculated value is only in memory
        * @return uint256 -- principal + interest - penalty
     */
    function estimateReturn(
        uint256 _stakingId
    )
        public
        view
        returns (uint256)
    {
        StakeElement memory _stakeElement = stakeList[_stakingId];

        if (_stakeElement.isActive == false) {
            return  _stakeElement.returnAmount;
        }

        return _stakeElement.stakedAmount
            .add(getStakeInterest(_stakingId))
            .sub(getStakePenalty(_stakingId));
    }

    /**
        * @notice close a stake older than 1 full day to:
        * 1) credit principal + interest - penalty to the balance of the staker
        * 2) update totalStakedAmount in globals
        * 3) take snapshot of current FEY status before the stake closes
        * No interest is accrued unless the stake is at least on its 4th day
        * Updates global variables to reflect the closed stake
        * @param _stakingId ID of the stake, used as the Key from the stakeList mapping
        * @return stakedAmount -- represents the total calculated by: principal + interest - penalty
        * @return penaltyAmount -- amount that will be debited from the stakers principal when they close their stake
        * @return interestAmount -- amount that will be debited from the stakers principal when they close their stake
     */
    function closeStake(
        uint256 _stakingId
    )
        public
        snapshotTriggerOnClose
        returns (
            uint256 stakedAmount,
            uint256 penaltyAmount,
            uint256 interestAmount
        )
    {
        return _closeStake(
            msg.sender,
            _stakingId
        );
    }

    function _closeStake(
        address _staker,
        uint256 _stakingId
    )
        internal
        returns (
            uint256 stakedAmount,
            uint256 penaltyAmount,
            uint256 interestAmount
        )
    {
        StakeElement memory _stakeElement = stakeList[_stakingId];

        uint256 daysDifference = getNow()
            .sub(_stakeElement.stakedAt)
            .div(SECONDS_IN_DAY);

        require(
            daysDifference \u003e= 3,
            \u0027FEYToken: immature stake\u0027
        );

        require(
            _stakeElement.userAddress == _staker,
            \u0027FEYToken: wrong stake owner\u0027
        );

        require(
            _stakeElement.isActive,
            \u0027FEYToken: stake not active\u0027
        );

        _stakeElement.isActive = false;

        stakedAmount = _stakeElement.stakedAmount;

        if (daysDifference \u003e= 45) {
            interestAmount = getStakeInterest(
                _stakingId
            );
        }

        penaltyAmount = getStakePenalty(
            _stakingId
        );

        totalSupply = totalSupply
            .add(interestAmount)
            .sub(penaltyAmount);

        _stakeElement.interestAmount = interestAmount;
        _stakeElement.returnAmount = stakedAmount
            .add(interestAmount)
            .sub(penaltyAmount);

        stakeList[_stakingId] = _stakeElement;

        balances[_staker] =
        balances[_staker].add(_stakeElement.returnAmount);

        globals.totalStakedAmount =
        globals.totalStakedAmount.sub(stakedAmount);

        emit StakeEnd(
            _stakingId,
            _staker,
            _stakeElement.returnAmount
        );

        emit Transfer(
            address(0x0),
            _staker,
            _stakeElement.returnAmount
        );
    }

    /**
        * @notice open a stake:
        * 1) must be greater than MINIMUM_STAKE in Declarations
        * 2) address opening the stake must have the amount of funds that they wish to stake in their balances[0xaddress]
        * 3) increment the global incrementId that is used to set the stakingId
        * 3) take snapshot of current FEY status before the stake is opened
        * Updates global variables to reflect the new stake
        * @param _amount the amount that you want to stake, will become your principal amount
        * @return true if no revert or error occurs
     */
    function openStake(
        uint256 _amount
    )
        external
        incrementId
        snapshotTriggerOnOpen
        returns (bool)
    {
        require(
            _transferCheck(
                msg.sender,
                address(0x0),
                _amount,
                true
            ),
            \u0027FEYToken: _transferCheck failed\u0027
        );

        require(
            _amount \u003e= MINIMUM_STAKE,
            \u0027FEYToken: stake below minimum\u0027
        );

        balances[msg.sender] =
        balances[msg.sender].sub(_amount);

        stakeList[globals.stakingId] = StakeElement(
            msg.sender,
            _amount,
            0,
            0,
            getNow(),
            true
        );

        globals.totalStakedAmount =
        globals.totalStakedAmount.add(_amount);

        emit StakeStart(
            globals.stakingId,
            msg.sender,
            _amount
        );

        emit Transfer(
            msg.sender,
            address(0),
            _amount
        );

        return true;
    }

    /**
        * @notice getter for the data of a specific stake
        * @param _stakingId ID of the stake, used as the Key from the stakeList mapping
        * @return _stakedAmount -- represents the total calculated by: principal + interest - penalty
        * @return _userAddress -- address that was used to open the stake
        * @return _returnAmount -- principal + interest - penalty
        * @return interestAmount -- amount of interest accrued after closing the stake
        * @return _stakedAt -- timestamp of when stake was opened
        * @return _isActive -- boolean for if the stake is open and accruing interest
     */
    function getStaking(
        uint256 _stakingId
    )
        external
        view
        returns (
            uint256 _stakedAmount,
            address _userAddress,
            uint256 _returnAmount,
            uint256 interestAmount,
            uint256 _stakedAt,
            bool _isActive
        )
    {
        StakeElement memory _stakeElement = stakeList[_stakingId];

        return (
            _stakeElement.stakedAmount,
            _stakeElement.userAddress,
            _stakeElement.returnAmount,
            _stakeElement.interestAmount,
            _stakeElement.stakedAt,
            _stakeElement.isActive
        );
    }
}"},"Helper.sol":{"content":"// SPDX-License-Identifier: -- 💰 --

pragma solidity ^0.7.3;

import \u0027./Timing.sol\u0027;
import \u0027./Ownable.sol\u0027;
import \u0027./Events.sol\u0027;
import \u0027./SafeMath.sol\u0027;

contract Helper is Ownable, Timing, Events {

    using SafeMath for uint256;

    /**
    * @notice burns set amount of tokens
    * @dev currently unused based on changing requirements
    * @param _amount -- amount to be burned
    * @return true if burn() succeeds
    */
    function burn(
        uint256 _amount
    )
        external
        onlyOwner
        returns (bool)
    {
        require(
            balances[msg.sender].sub(_amount) \u003e= 0,
            \u0027FEYToken: exceeding balance\u0027
        );

        totalSupply =
        totalSupply.sub(_amount);

        balances[msg.sender] =
        balances[msg.sender].sub(_amount);

        emit Transfer(
            msg.sender,
            address(0x0),
            _amount
       );

        return true;
    }

    /**
    * @notice Groups common requirements in global, internal function
    * @dev Used by Transfer(), TransferFrom(), OpenStake()
    * @param _sender -- msg.sender of the functions listed above
    * @param _recipient -- recipient of amount
    * @param _amount -- amount that is transferred
    * @param _allowBurnAddress -- boolean to allow burning tokens
    * @return balance[] value of the input address
    */
    function _transferCheck(
        address _sender,
        address _recipient,
        uint256 _amount,
        bool _allowBurnAddress
    )
        internal
        view
        returns (bool)
    {

        if (_allowBurnAddress == false) {
            require(
                _recipient != address(0x0),
                \u0027FEYToken: cannot send to burn address\u0027
            );
        }

        require(
            balances[_sender] \u003e= _amount,
            \u0027FEYToken: exceeding balance\u0027
        );

        require(
            balances[_recipient].add(_amount) \u003e= balances[_recipient],
            \u0027FEYToken: overflow detected\u0027
        );

        return true;
    }

    /**
    * @notice Used to calculate % that is staked out of the totalSupply
    * @dev Used by getYearlyInterestLatest(), getYearlyInterestHistorical(), + twice in getInterest()
    * @param _numerator -- numerator, typically globals.totalStakedAmount
    * @param _denominator -- denominator, typically totalSupply
    * @param _precision -- number of decimal points, fixed at 4
    * @return quotient -- calculated value
    */
    function getPercent(
        uint256 _numerator,
        uint256 _denominator,
        uint256 _precision
    )
        public
        pure
        returns(uint256 quotient)
    {
        uint256 numerator = _numerator * 10 ** (_precision + 1);
        quotient = ((numerator / _denominator) + 5) / 10;
    }

    /**
    * @notice Used to reduce value by a set percentage amount
    * @dev Used to calculate penaltyAmount
    * @param _value -- initial value, typically _stakeElement.stakedAmount
    * @param _perc -- percentage reduction that will be applied
    * @return percentageValue -- value reduced by the input percentage
    */
    function percentCalculator(
        uint256 _value,
        uint256 _perc
    )
        public
        pure
        returns (uint256 percentageValue)
    {
        percentageValue = _value
            .mul(_perc)
            .div(10000);
    }

}"},"Migrations.sol":{"content":"// SPDX-License-Identifier: MIT
pragma solidity \u003e=0.4.22 \u003c0.8.0;

contract Migrations {
    address public owner;
    uint public last_completed_migration;

    constructor() {
        owner = msg.sender;
    }

    modifier restricted() {
        if (msg.sender == owner) _;
    }

    function setCompleted(uint completed) public restricted {
        last_completed_migration = completed;
    }

    function upgrade(address new_address) public restricted {
        Migrations upgraded = Migrations(new_address);
        upgraded.setCompleted(last_completed_migration);
    }
}"},"Ownable.sol":{"content":"// SPDX-License-Identifier: -- 💰 --

pragma solidity ^0.7.3;

contract Ownable {

    address public owner;

    event ownershipChanged(
        address indexed _invoker,
        address indexed _newOwner
    );

    constructor() {
        owner = msg.sender;
    }

    modifier onlyOwner() {
        require(
            msg.sender == owner,
            \u0027Ownable: must be the owner\u0027
        );
        _;
    }

    function changeOwner(
        address _newOwner
    )
        external
        onlyOwner
        returns (bool)
    {
        
        require(
            _newOwner != address(0),
            \u0027Ownable: new owner must not be the blackhole address\u0027
        );
        
        owner = _newOwner;

        emit ownershipChanged(
            msg.sender,
            _newOwner
        );

        return true;
    }
}"},"SafeMath.sol":{"content":"// SPDX-License-Identifier: -- 🎲 --

pragma solidity ^0.7.0;

library SafeMath {

    function add(uint256 a, uint256 b) internal pure returns (uint256) {
        uint256 c = a + b;
        require(c \u003e= a, \u0027SafeMath: addition overflow\u0027);
        return c;
    }

    function sub(uint256 a, uint256 b) internal pure returns (uint256) {
        require(b \u003c= a, \u0027SafeMath: subtraction overflow\u0027);
        uint256 c = a - b;
        return c;
    }

    function mul(uint256 a, uint256 b) internal pure returns (uint256) {
        if (a == 0) {
            return 0;
        }

        uint256 c = a * b;
        require(c / a == b, \u0027SafeMath: multiplication overflow\u0027);
        return c;
    }

    function div(uint256 a, uint256 b) internal pure returns (uint256) {
        require(b \u003e 0, \u0027SafeMath: division by zero\u0027);
        uint256 c = a / b;
        // assert(a == b * c + a % b); // There is no case in which this doesn\u0027t hold
        return c;
    }

    function mod(uint256 a, uint256 b) internal pure returns (uint256) {
        require(b != 0, \u0027SafeMath: modulo by zero\u0027);
        return a % b;
    }
}"},"Snapshot.sol":{"content":"// SPDX-License-Identifier: -- 💰 --

pragma solidity ^0.7.3;

import \"./Helper.sol\";

abstract contract Snapshot is Helper {

    using SafeMath for uint;


    /**
    * @notice modifier to capture snapshots when a stake is opened
    * @dev used in OpenStake() in FeyToken 
    */
    modifier snapshotTriggerOnOpen() 
    {
        _;
        _dailySnapshotPoint(
            _currentFeyDay()
        );
    }
    
    
    /**
    * @notice modifier to capture snapshots when a stake is closed
    * @dev used in CloseStake() in FeyToken 
    */
    modifier snapshotTriggerOnClose() 
    {
        _dailySnapshotPoint(
            _currentFeyDay()
        );
        _;
    }

    /**
    * @notice Manually capture snapshot
    */
    function manualDailySnapshot() 
        external
    {
        _dailySnapshotPoint(
            _currentFeyDay()
        );
    }

    /**
    * @notice takes in todays feyday + updates all missing snapshot days with todays data
    * @param _updateDay -- current FeyDay as outputted from timing\u0027s _currentFeyDay() function
    * Emits SnapshotCaptured event
    */
    function _dailySnapshotPoint(
        uint64 _updateDay
    )
        private
    {
        for (uint256 _day = globals.currentFeyDay; _day \u003c _updateDay; _day++) {

            SnapShot memory s = snapshots[_day];

            s.totalSupply = totalSupply;
            s.totalStakedAmount = globals.totalStakedAmount;
            

            snapshots[_day] = s;

            globals.currentFeyDay++;
        }

        emit SnapshotCaptured(
            totalSupply,
            globals.totalStakedAmount,
            _updateDay
        );
    }
}"},"Timing.sol":{"content":"// SPDX-License-Identifier: -- 💰 --

pragma solidity ^0.7.3;

import \u0027./Declaration.sol\u0027;

abstract contract Timing is Declaration {

    /**
    * @notice external view function to get current FeyDay, unless called at LAUNCH_TIME, in which case it will return 0 to save gas
    * @dev called by _currentFeyDay
    * @return current FeyDay
    */
    function currentFeyDay()
        public
        view
        returns (uint64)
    {
        return getNow() \u003e= LAUNCH_TIME
            ? _currentFeyDay()
            : 0;
    }

    /**
    * @notice internal view function to calculate current FeyDay by using _feyDayFromStamp()
    * @dev called by snapshotTrigger(), manualDailySnapshot(), + getStakeInterest()
    * @return current FeyDay
    */
    function _currentFeyDay()
        internal
        view
        returns (uint64)
    {
        return _feyDayFromStamp(getNow());
    }

    /**
    * @notice calculates difference between passed timestamp + original LAUNCH_TIME, set when contract was deployed
    * @dev called by _currentFeyDay
    * @param _timestamp -- timestamp to use for difference
    * @return number of days between timestamp param + LAUNCH_TIME 
    */
    function _feyDayFromStamp(
        uint256 _timestamp
    )
        internal
        view
        returns (uint64)
    {
        return uint64((_timestamp - LAUNCH_TIME) / SECONDS_IN_DAY);
    }
    
    /**
    * @dev called by getStakeAge(), getStakePenalty, closeStake(), openStake(), + _currentFeyDay
    * @return current block.timestamp
    */
    function getNow()
        public
        view
        returns (uint256)
    {
        return block.timestamp;
    }

}"},"Token.sol":{"content":"// SPDX-License-Identifier: -- 💰 --

pragma solidity ^0.7.3;

import \"./Snapshot.sol\";

contract Token is Snapshot {

    using SafeMath for uint256;

    /**
    * @notice Moves amount tokens from the caller’s account to recipient.
    * Returns a boolean value indicating whether the operation succeeded.
    * @dev See {IERC20-transfer}.
    * Emits an {Transfer} event indicating a successful transfer.
    * @param _receiver -- recipient of amount
    * @param _amount -- amount that is transferred
    * @return true if transfer() succeeds
    */
    function transfer(
        address _receiver,
        uint256 _amount
    )
        external
        returns (bool)
    {
        require(
            _transferCheck(
                msg.sender,
                _receiver,
                _amount,
                false
            ),
            \u0027Token: _transferCheck failed\u0027
        );

        balances[msg.sender] =
        balances[msg.sender].sub(_amount);

        balances[_receiver] =
        balances[_receiver].add(_amount);

        emit Transfer(
            msg.sender,
            _receiver,
            _amount
        );

        return true;
    }

    /**
    * @notice Moves amount tokens from sender to recipient using the allowance mechanism.
    * Amount is then deducted from the caller’s allowance.
    * Returns a boolean value indicating whether the operation succeeded.
    * @dev See {IERC20-transferFrom}.
    * Emits an {Transfer} event indicating a successful transfer.
    * @param _owner -- address who is sending the transfer amount
    * @param _receiver -- recipient of amount
    * @param _amount -- amount that is transferred
    * @return true if transferFrom() succeeds
     */
    function transferFrom(
        address _owner,
        address _receiver,
        uint256 _amount
    )
        external
        returns (bool)
    {
        require(
            _transferCheck(
                _owner,
                _receiver,
                _amount,
                false
            ),
            \u0027Token: _transferCheck failed\u0027
        );

        require(
            allowances[_owner][msg.sender] \u003e= _amount,
            \u0027Token: exceeding allowance\u0027
        );

        allowances[_owner][msg.sender] =
        allowances[_owner][msg.sender].sub(_amount);

        balances[_owner] =
        balances[_owner].sub(_amount);

        balances[_receiver] =
        balances[_receiver].add(_amount);

        emit Transfer(
            _owner,
            _receiver,
            _amount
        );

        return true;
    }

    /**
    * @notice Sets amount as the allowance of spender over the caller’s tokens.
    * @dev See {IERC20-approve}.
    * Emits an {Approval} event indicating how much was approved and whom is the spender
    * @param _spender -- approved address
    * @param _amount -- amount that they are approved to spend
    * @return true if Approve() succeeds
    */
    function approve(
        address _spender,
        uint256 _amount
    )
        external
        returns (bool)
    {

        allowances[msg.sender][_spender] = _amount;

        emit Approval(
            msg.sender,
            _spender,
            _amount
        );

        return true;
    }

    /**
    * @notice Returns the amount of tokens owned by account.
    * @dev See {IERC20-approve}.
    * @param _address -- address whose balance will be returned
    * @return balance[] value of the input address
    */
    function balanceOf(
        address _address
    )
        external
        view
        returns (uint256)
    {
        return balances[_address];
    }

    /**
    * @notice Returns the remaining number of tokens that spender will be allowed to spend
    * on behalf of owner through transferFrom. This is zero by default.
    * This value changes when approve or transferFrom are called.
    * @dev See {IERC20-allowance}.
    * @param _owner -- owner address
    * @param _spender -- address that is approved to spend tokens
    * @return allowances[] value of the input addresses to reflect the value mapped to the _spender\u0027s address
    */
    function allowance(
        address _owner,
        address _spender
    )
        external
        view
        returns (uint256)
    {
        return allowances[_owner][_spender];
    }
}
