// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see \u003chttp://www.gnu.org/licenses/\u003e.

pragma solidity 0.5.12;

contract MColor {
    function getColor()
    external view
    returns (bytes32);
}

contract MBronze is MColor {
    function getColor()
    external view
    returns (bytes32) {
        return bytes32(\"BRONZE\");
    }
}
"},"MConst.sol":{"content":"// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see \u003chttp://www.gnu.org/licenses/\u003e.

pragma solidity 0.5.12;

import \"./MColor.sol\";

contract MConst is MBronze {
    uint internal constant BONE              = 10**18;

    uint internal constant MIN_BOUND_TOKENS  = 2;
    uint internal constant MAX_BOUND_TOKENS  = 8;

    uint internal constant MIN_FEE           = BONE / 10**6;
    uint internal constant MAX_FEE           = BONE / 10;

    uint internal constant MIN_WEIGHT        = BONE;
    uint internal constant MAX_WEIGHT        = BONE * 50;
    uint internal constant MAX_TOTAL_WEIGHT  = BONE * 50;
    uint internal constant MIN_BALANCE       = BONE / 10**12;

    uint internal constant INIT_POOL_SUPPLY  = BONE * 100;

    uint internal constant MIN_BPOW_BASE     = 1 wei;
    uint internal constant MAX_BPOW_BASE     = (2 * BONE) - 1 wei;
    uint internal constant BPOW_PRECISION    = BONE / 10**10;

    uint internal constant MAX_IN_RATIO      = BONE / 2;
    uint internal constant MAX_OUT_RATIO     = (BONE / 3) + 1 wei;
}
"},"MMath.sol":{"content":"// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see \u003chttp://www.gnu.org/licenses/\u003e.

pragma solidity 0.5.12;

import \"./MNum.sol\";

contract MMath is MBronze, MConst, MNum {
    /**********************************************************************************************
    // calcSpotPrice                                                                             //
    // sP = spotPrice                                                                            //
    // bI = tokenBalanceIn                ( bI / wI )         1                                  //
    // bO = tokenBalanceOut         sP =  -----------  *  ----------                             //
    // wI = tokenWeightIn                 ( bO / wO )     ( 1 - sF )                             //
    // wO = tokenWeightOut                                                                       //
    // sF = swapFee                                                                              //
    **********************************************************************************************/
    function calcSpotPrice(
        uint tokenBalanceIn,
        uint tokenWeightIn,
        uint tokenBalanceOut,
        uint tokenWeightOut,
        uint swapFee
    )
    public pure
    returns (uint spotPrice)
    {
        uint numer = bdiv(tokenBalanceIn, tokenWeightIn);
        uint denom = bdiv(tokenBalanceOut, tokenWeightOut);
        uint ratio = bdiv(numer, denom);
        uint scale = bdiv(BONE, bsub(BONE, swapFee));
        return  (spotPrice = bmul(ratio, scale));
    }

    /**********************************************************************************************
    // calcOutGivenIn                                                                            //
    // aO = tokenAmountOut                                                                       //
    // bO = tokenBalanceOut                                                                      //
    // bI = tokenBalanceIn              /      /            bI             \\    (wI / wO) \\      //
    // aI = tokenAmountIn    aO = bO * |  1 - | --------------------------  | ^            |     //
    // wI = tokenWeightIn               \\      \\ ( bI + ( aI * ( 1 - sF )) /              /      //
    // wO = tokenWeightOut                                                                       //
    // sF = swapFee                                                                              //
    **********************************************************************************************/
    function calcOutGivenIn(
        uint tokenBalanceIn,
        uint tokenWeightIn,
        uint tokenBalanceOut,
        uint tokenWeightOut,
        uint tokenAmountIn,
        uint swapFee
    )
    public pure
    returns (uint tokenAmountOut)
    {
        uint weightRatio = bdiv(tokenWeightIn, tokenWeightOut);
        uint adjustedIn = bsub(BONE, swapFee);
        adjustedIn = bmul(tokenAmountIn, adjustedIn);
        uint y = bdiv(tokenBalanceIn, badd(tokenBalanceIn, adjustedIn));
        uint foo = bpow(y, weightRatio);
        uint bar = bsub(BONE, foo);
        tokenAmountOut = bmul(tokenBalanceOut, bar);
        return tokenAmountOut;
    }

    /**********************************************************************************************
    // calcInGivenOut                                                                            //
    // aI = tokenAmountIn                                                                        //
    // bO = tokenBalanceOut               /  /     bO      \\    (wO / wI)      \\                 //
    // bI = tokenBalanceIn          bI * |  | ------------  | ^            - 1  |                //
    // aO = tokenAmountOut    aI =        \\  \\ ( bO - aO ) /                   /                 //
    // wI = tokenWeightIn           --------------------------------------------                 //
    // wO = tokenWeightOut                          ( 1 - sF )                                   //
    // sF = swapFee                                                                              //
    **********************************************************************************************/
    function calcInGivenOut(
        uint tokenBalanceIn,
        uint tokenWeightIn,
        uint tokenBalanceOut,
        uint tokenWeightOut,
        uint tokenAmountOut,
        uint swapFee
    )
    public pure
    returns (uint tokenAmountIn)
    {
        uint weightRatio = bdiv(tokenWeightOut, tokenWeightIn);
        uint diff = bsub(tokenBalanceOut, tokenAmountOut);
        uint y = bdiv(tokenBalanceOut, diff);
        uint foo = bpow(y, weightRatio);
        foo = bsub(foo, BONE);
        tokenAmountIn = bsub(BONE, swapFee);
        tokenAmountIn = bdiv(bmul(tokenBalanceIn, foo), tokenAmountIn);
        return tokenAmountIn;
    }

    /**********************************************************************************************
    // calcPoolOutGivenSingleIn                                                                  //
    // pAo = poolAmountOut         /                                              \\              //
    // tAi = tokenAmountIn        ///      /     //    wI \\      \\\\       \\     wI \\             //
    // wI = tokenWeightIn        //| tAi *| 1 - || 1 - --  | * sF || + tBi \\    --  \\            //
    // tW = totalWeight     pAo=||  \\      \\     \\\\    tW /      //         | ^ tW   | * pS - pS //
    // tBi = tokenBalanceIn      \\\\  ------------------------------------- /        /            //
    // pS = poolSupply            \\\\                    tBi               /        /             //
    // sF = swapFee                \\                                              /              //
    **********************************************************************************************/
    function calcPoolOutGivenSingleIn(
        uint tokenBalanceIn,
        uint tokenWeightIn,
        uint poolSupply,
        uint totalWeight,
        uint tokenAmountIn,
        uint swapFee
    )
    internal pure
    returns (uint poolAmountOut)
    {
        // Charge the trading fee for the proportion of tokenAi
        ///  which is implicitly traded to the other pool tokens.
        // That proportion is (1- weightTokenIn)
        // tokenAiAfterFee = tAi * (1 - (1-weightTi) * poolFee);
        uint normalizedWeight = bdiv(tokenWeightIn, totalWeight);
        uint zaz = bmul(bsub(BONE, normalizedWeight), swapFee);
        uint tokenAmountInAfterFee = bmul(tokenAmountIn, bsub(BONE, zaz));

        uint newTokenBalanceIn = badd(tokenBalanceIn, tokenAmountInAfterFee);
        uint tokenInRatio = bdiv(newTokenBalanceIn, tokenBalanceIn);

        // uint newPoolSupply = (ratioTi ^ weightTi) * poolSupply;
        uint poolRatio = bpow(tokenInRatio, normalizedWeight);
        uint newPoolSupply = bmul(poolRatio, poolSupply);
        poolAmountOut = bsub(newPoolSupply, poolSupply);
        return poolAmountOut;
    }

    /**********************************************************************************************
    // calcSingleOutGivenPoolIn                                                                  //
    // tAo = tokenAmountOut            /      /                                             \\\\   //
    // bO = tokenBalanceOut           /      //        pS - pAi       \\     /    1    \\      \\\\  //
    // pAi = poolAmountIn            | bO - || ----------------------- | ^ | --------- | * b0 || //
    // ps = poolSupply                \\      \\\\          pS           /     \\(wO / tW)/      //  //
    // wI = tokenWeightIn      tAo =   \\      \\                                             //   //
    // tW = totalWeight                    /     /      wO \\       \\                             //
    // sF = swapFee                    *  | 1 - |  1 - ---- | * sF  |                            //
    // eF = exitFee                        \\     \\      tW /       /                             //
    **********************************************************************************************/
    function calcSingleOutGivenPoolIn(
        uint tokenBalanceOut,
        uint tokenWeightOut,
        uint poolSupply,
        uint totalWeight,
        uint poolAmountIn,
        uint swapFee
    )
    internal pure
    returns (uint tokenAmountOut)
    {
        uint normalizedWeight = bdiv(tokenWeightOut, totalWeight);
        // charge exit fee on the pool token side
        // pAiAfterExitFee = pAi*(1-exitFee)
        uint poolAmountInAfterExitFee = bmul(poolAmountIn, BONE);
        uint newPoolSupply = bsub(poolSupply, poolAmountInAfterExitFee);
        uint poolRatio = bdiv(newPoolSupply, poolSupply);

        // newBalTo = poolRatio^(1/weightTo) * balTo;
        uint tokenOutRatio = bpow(poolRatio, bdiv(BONE, normalizedWeight));
        uint newTokenBalanceOut = bmul(tokenOutRatio, tokenBalanceOut);

        uint tokenAmountOutBeforeSwapFee = bsub(tokenBalanceOut, newTokenBalanceOut);

        // charge swap fee on the output token side
        //uint tAo = tAoBeforeSwapFee * (1 - (1-weightTo) * swapFee)
        uint zaz = bmul(bsub(BONE, normalizedWeight), swapFee);
        tokenAmountOut = bmul(tokenAmountOutBeforeSwapFee, bsub(BONE, zaz));
        return tokenAmountOut;
    }

}
"},"MNum.sol":{"content":"// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see \u003chttp://www.gnu.org/licenses/\u003e.

pragma solidity 0.5.12;

import \"./MConst.sol\";

contract MNum is MConst {

    function btoi(uint a)
    internal pure
    returns (uint)
    {
        return a / BONE;
    }

    function bfloor(uint a)
    internal pure
    returns (uint)
    {
        return btoi(a) * BONE;
    }

    function badd(uint a, uint b)
    internal pure
    returns (uint)
    {
        uint c = a + b;
        require(c \u003e= a, \"ERR_ADD_OVERFLOW\");
        return c;
    }

    function bsub(uint a, uint b)
    internal pure
    returns (uint)
    {
        (uint c, bool flag) = bsubSign(a, b);
        require(!flag, \"ERR_SUB_UNDERFLOW\");
        return c;
    }

    function bsubSign(uint a, uint b)
    internal pure
    returns (uint, bool)
    {
        if (a \u003e= b) {
            return (a - b, false);
        } else {
            return (b - a, true);
        }
    }

    function bmul(uint a, uint b)
    internal pure
    returns (uint)
    {
        uint c0 = a * b;
        require(a == 0 || c0 / a == b, \"ERR_MUL_OVERFLOW\");
        uint c1 = c0 + (BONE / 2);
        require(c1 \u003e= c0, \"ERR_MUL_OVERFLOW\");
        uint c2 = c1 / BONE;
        return c2;
    }

    function bdiv(uint a, uint b)
    internal pure
    returns (uint)
    {
        require(b != 0, \"ERR_DIV_ZERO\");
        uint c0 = a * BONE;
        require(a == 0 || c0 / a == BONE, \"ERR_DIV_INTERNAL\"); // bmul overflow
        uint c1 = c0 + (b / 2);
        require(c1 \u003e= c0, \"ERR_DIV_INTERNAL\"); //  badd require
        uint c2 = c1 / b;
        return c2;
    }

    // DSMath.wpow
    function bpowi(uint a, uint n)
    internal pure
    returns (uint)
    {
        uint z = n % 2 != 0 ? a : BONE;

        for (n /= 2; n != 0; n /= 2) {
            a = bmul(a, a);

            if (n % 2 != 0) {
                z = bmul(z, a);
            }
        }
        return z;
    }

    // Compute b^(e.w) by splitting it into (b^e)*(b^0.w).
    // Use `bpowi` for `b^e` and `bpowK` for k iterations
    // of approximation of b^0.w
    function bpow(uint base, uint exp)
    internal pure
    returns (uint)
    {
        require(base \u003e= MIN_BPOW_BASE, \"ERR_BPOW_BASE_TOO_LOW\");
        require(base \u003c= MAX_BPOW_BASE, \"ERR_BPOW_BASE_TOO_HIGH\");

        uint whole  = bfloor(exp);
        uint remain = bsub(exp, whole);

        uint wholePow = bpowi(base, btoi(whole));

        if (remain == 0) {
            return wholePow;
        }

        uint partialResult = bpowApprox(base, remain, BPOW_PRECISION);
        return bmul(wholePow, partialResult);
    }

    function bpowApprox(uint base, uint exp, uint precision)
    internal pure
    returns (uint)
    {
        // term 0:
        uint a     = exp;
        (uint x, bool xneg)  = bsubSign(base, BONE);
        uint term = BONE;
        uint sum   = term;
        bool negative = false;


        // term(k) = numer / denom 
        //         = (product(a - i - 1, i=1--\u003ek) * x^k) / (k!)
        // each iteration, multiply previous term by (a-(k-1)) * x / k
        // continue until term is less than precision
        for (uint i = 1; term \u003e= precision; i++) {
            uint bigK = i * BONE;
            (uint c, bool cneg) = bsubSign(a, bsub(bigK, BONE));
            term = bmul(term, bmul(c, x));
            term = bdiv(term, bigK);
            if (term == 0) break;

            if (xneg) negative = !negative;
            if (cneg) negative = !negative;
            if (negative) {
                sum = bsub(sum, term);
            } else {
                sum = badd(sum, term);
            }
        }

        return sum;
    }

}
"},"MPool.sol":{"content":"pragma solidity 0.5.12;

import \"./MToken.sol\";
import \"./MMath.sol\";

interface IMFactory {
    function isWhiteList(address w) external view returns (bool);
    function getMining() external returns (address lpMiningAdr, address swapMiningAdr);
    function getFeeTo() external view returns (address);
}

interface IMining {
    // pair
    function addLiquidity(bool isGp, address _user, uint256 _amount) external;
    function removeLiquidity(bool isGp, address _user, uint256 _amount) external;
    function updateGPInfo(address[] calldata gps, uint256[] calldata amounts) external;
    // lp mining
    function onTransferLiquidity(address from, address to, uint256 lpAmount) external;
    function claimLiquidityShares(address user, address[] calldata tokens, uint256[] calldata balances, uint256[] calldata weights, uint256 amount, bool _add) external;
    // swap mining
    function claimSwapShare(address user, address tokenIn, uint256 amountIn, address tokenOut, uint256 amountOut) external;
}

interface IPairFactory {
    function newPair(address pool, uint256 perBlock, uint256 rate) external returns (IPairToken);
    function getPairToken(address pool) external view returns (address);
}

interface IPairToken {
    function setController(address _controller) external ;
}

contract MPool is MBronze, MToken, MMath {

    struct Record {
        bool bound;   // is token bound to pool
        uint index;   // private
        uint denorm;  // denormalized weight
        uint balance;
    }

    event LOG_SWAP(
        address indexed caller,
        address indexed tokenIn,
        address indexed tokenOut,
        uint256 tokenAmountIn,
        uint256 tokenAmountOut
    );

    event LOG_JOIN(
        address indexed caller,
        address indexed tokenIn,
        uint256 tokenAmountIn
    );

    event LOG_EXIT(
        address indexed caller,
        address indexed tokenOut,
        uint256 tokenAmountOut
    );

    event LOG_CALL(
        bytes4  indexed sig,
        address indexed caller,
        bytes data
    ) anonymous;

    modifier _logs_() {
        emit LOG_CALL(msg.sig, msg.sender, msg.data);
        _;
    }

    modifier _lock_() {
        require(!_mutex, \"ERR_REENTRY\");
        _mutex = true;
        _;
        _mutex = false;
    }

    modifier _viewlock_() {
        require(!_mutex, \"ERR_REENTRY\");
        _;
    }

    bool private _mutex;

    IMFactory private _factory;    // MFactory address to push token exitFee to and check whitelist from factory
    IMining private _pair;
    address public controller;     // has CONTROL role

    // `setSwapFee` and `finalize` require CONTROL
    // `finalize` sets `PUBLIC can SWAP`, `PUBLIC can JOIN`
    uint private _swapFee;
    bool private _finalized;
    bool private _publicSwap;     // true if PUBLIC can call SWAP functions

    address[] private _tokens;
    mapping(address =\u003e Record) private  _records;
    uint private _totalWeight;

    constructor() public {
        controller = msg.sender;
        _factory = IMFactory(msg.sender);

        _swapFee = MIN_FEE;
        _publicSwap = false;
        _finalized = false;
    }

    function isPublicSwap()
    external view
    returns (bool)
    {
        return _publicSwap;
    }

    function isFinalized()
    external view
    returns (bool)
    {
        return _finalized;
    }

    function isBound(address t)
    external view
    returns (bool)
    {
        return _records[t].bound;
    }

    function getNumTokens()
    external view
    returns (uint)
    {
        return _tokens.length;
    }

    function getCurrentTokens()
    external view _viewlock_
    returns (address[] memory tokens)
    {
        return _tokens;
    }

    function getFinalTokens()
    external view
    _viewlock_
    returns (address[] memory tokens)
    {
        require(_finalized, \"ERR_NOT_FINALIZED\");
        return _tokens;
    }

    function getDenormalizedWeight(address token)
    external view
    _viewlock_
    returns (uint)
    {

        require(_records[token].bound, \"ERR_NOT_BOUND\");
        return _records[token].denorm;
    }

    function getTotalDenormalizedWeight()
    external view
    _viewlock_
    returns (uint)
    {
        return _totalWeight;
    }

    function getNormalizedWeight(address token)
    external view
    _viewlock_
    returns (uint)
    {

        require(_records[token].bound, \"ERR_NOT_BOUND\");
        uint denorm = _records[token].denorm;
        return bdiv(denorm, _totalWeight);
    }

    function getBalance(address token)
    external view
    _viewlock_
    returns (uint)
    {

        require(_records[token].bound, \"ERR_NOT_BOUND\");
        return _records[token].balance;
    }

    function getSwapFee()
    external view
    _viewlock_
    returns (uint)
    {
        return _swapFee;
    }

    function getPair()
    external view
    _viewlock_
    returns (address)
    {
        return address(_pair);
    }

    function setSwapFee(uint swapFee)
    external
    _logs_
    _lock_
    {
        require(!_finalized, \"ERR_IS_FINALIZED\");
        require(msg.sender == controller, \"ERR_NOT_CONTROLLER\");
        require(swapFee \u003e= MIN_FEE, \"ERR_MIN_FEE\");
        require(swapFee \u003c= MAX_FEE, \"ERR_MAX_FEE\");
        _swapFee = swapFee;
    }

    function setController(address manager)
    external
    _logs_
    _lock_
    {
        require(msg.sender == controller, \"ERR_NOT_CONTROLLER\");
        controller = manager;
    }

    function setPair(IMining pair)
    external
    _logs_
    _lock_
    {
        require(msg.sender == controller, \"ERR_NOT_CONTROLLER\");
        _setPair(pair);
    }

    function _setPair(IMining pair)
    internal
    {
        _pair = pair;
    }

    function finalize(address beneficiary, uint fixPoolSupply)
    external
    _logs_
    _lock_
    {
        require(msg.sender == controller, \"ERR_NOT_CONTROLLER\");
        require(!_finalized || totalSupply() == 0, \"ERR_IS_FINALIZED\");
        require(_tokens.length \u003e= MIN_BOUND_TOKENS, \"ERR_MIN_TOKENS\");

        _finalized = true;
        _publicSwap = true;

        uint256 supply = fixPoolSupply == 0 ? INIT_POOL_SUPPLY : fixPoolSupply;

        _mintPoolShare(supply);
        _pushPoolShare(beneficiary, supply);
        _lpChanging(true, beneficiary, supply);
    }


    function bind(address token, uint balance, uint denorm)
    external
    _logs_
        // _lock_  Bind does not lock because it jumps to `rebind`, which does
    {
        require(msg.sender == controller, \"ERR_NOT_CONTROLLER\");
        require(!_records[token].bound, \"ERR_IS_BOUND\");
        require(!_finalized, \"ERR_IS_FINALIZED\");

        require(_tokens.length \u003c MAX_BOUND_TOKENS, \"ERR_MAX_TOKENS\");

        _records[token] = Record({
        bound: true,
        index: _tokens.length,
        denorm: 0,    // balance and denorm will be validated
        balance: 0    // and set by `rebind`
        });
        _tokens.push(token);
        rebind(token, balance, denorm);
    }

    function rebind(address token, uint balance, uint denorm)
    public
    _logs_
    _lock_
    {

        require(msg.sender == controller, \"ERR_NOT_CONTROLLER\");
        require(_records[token].bound, \"ERR_NOT_BOUND\");
        require(!_finalized || totalSupply() == 0, \"ERR_IS_FINALIZED\");

        require(denorm \u003e= MIN_WEIGHT, \"ERR_MIN_WEIGHT\");
        require(denorm \u003c= MAX_WEIGHT, \"ERR_MAX_WEIGHT\");
        require(balance \u003e= MIN_BALANCE, \"ERR_MIN_BALANCE\");

        // Adjust the denorm and totalWeight
        uint oldWeight = _records[token].denorm;
        if (denorm \u003e oldWeight) {
            _totalWeight = badd(_totalWeight, bsub(denorm, oldWeight));
            require(_totalWeight \u003c= MAX_TOTAL_WEIGHT, \"ERR_MAX_TOTAL_WEIGHT\");
        } else if (denorm \u003c oldWeight) {
            _totalWeight = bsub(_totalWeight, bsub(oldWeight, denorm));
        }
        _records[token].denorm = denorm;

        // Adjust the balance record and actual token balance
        uint oldBalance = _records[token].balance;
        _records[token].balance = balance;
        if (balance \u003e oldBalance) {
            _pullUnderlying(token, msg.sender, bsub(balance, oldBalance));
        } else if (balance \u003c oldBalance) {
            uint tokenBalanceWithdrawn = bsub(oldBalance, balance);
            _pushUnderlying(token, msg.sender, tokenBalanceWithdrawn);
        }
    }

    function unbind(address token)
    external
    _logs_
    _lock_
    {

        require(msg.sender == controller, \"ERR_NOT_CONTROLLER\");
        require(_records[token].bound, \"ERR_NOT_BOUND\");
        require(!_finalized, \"ERR_IS_FINALIZED\");

        uint tokenBalance = _records[token].balance;
        _totalWeight = bsub(_totalWeight, _records[token].denorm);

        // Swap the token-to-unbind with the last token,
        // then delete the last token
        uint index = _records[token].index;
        uint last = _tokens.length - 1;
        _tokens[index] = _tokens[last];
        _records[_tokens[index]].index = index;
        _tokens.pop();
        _records[token] = Record({
        bound: false,
        index: 0,
        denorm: 0,
        balance: 0
        });

        _pushUnderlying(token, msg.sender, tokenBalance);
    }

    // Absorb any tokens that have been sent to this contract into the pool
    function gulp(address token)
    external
    _logs_
    _lock_
    {
        require(_records[token].bound, \"ERR_NOT_BOUND\");
        _records[token].balance = IERC20(token).balanceOf(address(this));
    }

    function getSpotPrice(address tokenIn, address tokenOut)
    external view
    _viewlock_
    returns (uint spotPrice)
    {
        require(_records[tokenIn].bound, \"ERR_NOT_BOUND\");
        require(_records[tokenOut].bound, \"ERR_NOT_BOUND\");
        Record storage inRecord = _records[tokenIn];
        Record storage outRecord = _records[tokenOut];
        return calcSpotPrice(inRecord.balance, inRecord.denorm, outRecord.balance, outRecord.denorm, _swapFee);
    }

    function getSpotPriceSansFee(address tokenIn, address tokenOut)
    external view
    _viewlock_
    returns (uint spotPrice)
    {
        require(_records[tokenIn].bound, \"ERR_NOT_BOUND\");
        require(_records[tokenOut].bound, \"ERR_NOT_BOUND\");
        Record storage inRecord = _records[tokenIn];
        Record storage outRecord = _records[tokenOut];
        return calcSpotPrice(inRecord.balance, inRecord.denorm, outRecord.balance, outRecord.denorm, 0);
    }

    function joinPool(address beneficiary, uint poolAmountOut)
    external
    _logs_
    _lock_
    {
        require(_finalized, \"ERR_NOT_FINALIZED\");

        uint poolTotal = totalSupply();
        uint ratio = bdiv(poolAmountOut, poolTotal);
        require(ratio != 0, \"ERR_MATH_APPROX\");

        for (uint i = 0; i \u003c _tokens.length; i++) {
            address t = _tokens[i];
            uint bal = _records[t].balance;
            uint tokenAmountIn = bmul(ratio, bal);
            require(tokenAmountIn != 0, \"ERR_MATH_APPROX\");
            require(bsub(IERC20(_tokens[i]).balanceOf(address(this)), _records[t].balance) \u003e= tokenAmountIn);
            _records[t].balance = badd(_records[t].balance, tokenAmountIn);
            emit LOG_JOIN(msg.sender, t, tokenAmountIn);
        }
        _mintPoolShare(poolAmountOut);
        _pushPoolShare(beneficiary, poolAmountOut);

        _lpChanging(true, beneficiary, poolAmountOut);
    }

    function exitPool(uint poolAmountIn, uint[] calldata minAmountsOut)
    external
    _logs_
    _lock_
    {
        require(_finalized, \"ERR_NOT_FINALIZED\");

        uint poolTotal = totalSupply();
        uint ratio = bdiv(poolAmountIn, poolTotal);
        require(ratio != 0, \"ERR_MATH_APPROX\");

        _pullPoolShare(msg.sender, poolAmountIn);
        _burnPoolShare(poolAmountIn);

        for (uint i = 0; i \u003c _tokens.length; i++) {
            address t = _tokens[i];
            uint bal = _records[t].balance;
            uint tokenAmountOut = bmul(ratio, bal);
            require(tokenAmountOut != 0, \"ERR_MATH_APPROX\");
            require(tokenAmountOut \u003e= minAmountsOut[i], \"ERR_LIMIT_OUT\");
            _records[t].balance = bsub(_records[t].balance, tokenAmountOut);
            emit LOG_EXIT(msg.sender, t, tokenAmountOut);
            _pushUnderlying(t, msg.sender, tokenAmountOut);
        }

        _lpChanging(false, msg.sender, poolAmountIn);
    }


    function swapExactAmountIn(
        address user,
        address tokenIn,
        address tokenOut,
        uint minAmountOut,
        address to,
        uint maxPrice
    )
    external
    _lock_
    returns (uint tokenAmountOut, uint spotPriceAfter)
    {

        require(_records[tokenIn].bound, \"ERR_NOT_BOUND\");
        require(_records[tokenOut].bound, \"ERR_NOT_BOUND\");
        require(_publicSwap, \"ERR_SWAP_NOT_PUBLIC\");

        Record storage inRecord = _records[address(tokenIn)];
        Record storage outRecord = _records[address(tokenOut)];

        uint tokenAmountIn = bsub(IERC20(tokenIn).balanceOf(address(this)), inRecord.balance);
        require(tokenAmountIn \u003e 0, \"ERR_AMOUNTIN_NOT_IN_Pool\");
        require(tokenAmountIn \u003c= bmul(inRecord.balance, MAX_IN_RATIO), \"ERR_MAX_IN_RATIO\");

        uint256 factoryFee = bmul(tokenAmountIn, bmul(bdiv(_swapFee, 6), 1));

        uint spotPriceBefore = calcSpotPrice(
            inRecord.balance,
            inRecord.denorm,
            outRecord.balance,
            outRecord.denorm,
            _swapFee
        );
        require(spotPriceBefore \u003c= maxPrice, \"ERR_BAD_LIMIT_PRICE\");

        tokenAmountOut = calcOutGivenIn(
            inRecord.balance,
            inRecord.denorm,
            outRecord.balance,
            outRecord.denorm,
            tokenAmountIn,
            _swapFee
        );
        require(tokenAmountOut \u003e= minAmountOut, \"ERR_LIMIT_OUT\");

        uint inAfterFee = bsub(tokenAmountIn, factoryFee);
        inRecord.balance = badd(inRecord.balance, inAfterFee);
        outRecord.balance = bsub(outRecord.balance, tokenAmountOut);

        spotPriceAfter = calcSpotPrice(
            inRecord.balance,
            inRecord.denorm,
            outRecord.balance,
            outRecord.denorm,
            _swapFee
        );
        require(spotPriceAfter \u003e= spotPriceBefore, \"ERR_MATH_APPROX\");
        require(spotPriceAfter \u003c= maxPrice, \"ERR_LIMIT_PRICE\");
        require(spotPriceBefore \u003c= bdiv(tokenAmountIn, tokenAmountOut), \"ERR_MATH_APPROX\");

        emit LOG_SWAP(msg.sender, tokenIn, tokenOut, tokenAmountIn, tokenAmountOut);

        _pushUnderlying(tokenOut, to, tokenAmountOut);
        _pushUnderlying(tokenIn, _factory.getFeeTo(), factoryFee);

        _swapMining(user, tokenIn, tokenOut, tokenAmountIn, tokenAmountOut);

        return (tokenAmountOut, spotPriceAfter);
    }

    function swapExactAmountOut(
        address user,
        address tokenIn,
        uint maxAmountIn,
        address tokenOut,
        uint tokenAmountOut,
        address to,
        uint maxPrice
    )
    external
    _lock_
    returns (uint tokenAmountIn, uint spotPriceAfter)
    {
        require(_records[tokenIn].bound, \"ERR_NOT_BOUND\");
        require(_records[tokenOut].bound, \"ERR_NOT_BOUND\");
        require(_publicSwap, \"ERR_SWAP_NOT_PUBLIC\");

        Record storage inRecord = _records[address(tokenIn)];
        Record storage outRecord = _records[address(tokenOut)];

        require(tokenAmountOut \u003c= bmul(outRecord.balance, MAX_OUT_RATIO), \"ERR_MAX_OUT_RATIO\");

        uint spotPriceBefore = calcSpotPrice(
            inRecord.balance,
            inRecord.denorm,
            outRecord.balance,
            outRecord.denorm,
            _swapFee
        );
        require(spotPriceBefore \u003c= maxPrice, \"ERR_BAD_LIMIT_PRICE\");

        tokenAmountIn = calcInGivenOut(
            inRecord.balance,
            inRecord.denorm,
            outRecord.balance,
            outRecord.denorm,
            tokenAmountOut,
            _swapFee
        );
        uint user_deposit_amount = bsub(IERC20(tokenIn).balanceOf(address(this)), inRecord.balance);
        require(tokenAmountIn == user_deposit_amount \u0026\u0026 user_deposit_amount \u003c= maxAmountIn, \"ERR_LIMIT_IN\");

        uint256 factoryFee = bmul(tokenAmountIn, bmul(bdiv(_swapFee, 6), 1));

        inRecord.balance = badd(inRecord.balance, bsub(tokenAmountIn, factoryFee));
        outRecord.balance = bsub(outRecord.balance, tokenAmountOut);

        spotPriceAfter = calcSpotPrice(
            inRecord.balance,
            inRecord.denorm,
            outRecord.balance,
            outRecord.denorm,
            _swapFee
        );
        require(spotPriceAfter \u003e= spotPriceBefore, \"ERR_MATH_APPROX\");
        require(spotPriceAfter \u003c= maxPrice, \"ERR_LIMIT_PRICE\");
        require(spotPriceBefore \u003c= bdiv(tokenAmountIn, tokenAmountOut), \"ERR_MATH_APPROX\");

        emit LOG_SWAP(msg.sender, tokenIn, tokenOut, tokenAmountIn, tokenAmountOut);

        _pushUnderlying(tokenOut, to, tokenAmountOut);
        _pushUnderlying(tokenIn, _factory.getFeeTo(), factoryFee);

        _swapMining(user, tokenIn, tokenOut, tokenAmountIn, tokenAmountOut);
        return (tokenAmountIn, spotPriceAfter);
    }

    function calcDesireByGivenAmount(address tokenIn, address tokenOut, uint256 inAmount, uint256 outAmount)
    external view
    returns (uint desireAmount)
    {
        require(inAmount != 0 || outAmount != 0, \"ERR_AMOUNT_IS_ZERO\");
        Record memory inRecord = _records[address(tokenIn)];
        Record memory outRecord = _records[address(tokenOut)];
        if (inAmount != 0) {
            desireAmount = calcOutGivenIn(inRecord.balance, inRecord.denorm, outRecord.balance, outRecord.denorm, inAmount, _swapFee);
        } else {
            desireAmount = calcInGivenOut(inRecord.balance, inRecord.denorm, outRecord.balance, outRecord.denorm, outAmount, _swapFee);
        }
    }
    function calcPoolSpotPrice(address tokenIn, address tokenOut, uint256 inAmount, uint256 outAmount)
    external view
    returns (uint256 price)
    {
        Record memory inRecord = _records[address(tokenIn)];
        Record memory outRecord = _records[address(tokenOut)];
        if (inAmount != 0 \u0026\u0026 outAmount != 0) {
            uint256 factoryFee = bmul(inAmount, bmul(bdiv(_swapFee, 6), 1));
            price = calcSpotPrice(
                badd(inRecord.balance, bsub(inAmount, factoryFee)),
                inRecord.denorm,
                bsub(outRecord.balance, outAmount),
                outRecord.denorm,
                _swapFee);
        } else {
            price = calcSpotPrice(inRecord.balance, inRecord.denorm, outRecord.balance, outRecord.denorm, _swapFee);
        }
    }

    function updatePairGPInfo(address[] calldata gps, uint[] calldata shares)
    external
    {
        require(msg.sender == controller, \"ERR_NOT_CONTROLLER\");
        if (address(_pair) != address(0))
        {
            _pair.updateGPInfo(gps, shares);
        }
    }

    // \u0027Underlying\u0027 token-manipulation functions make external calls but are NOT locked
    // You must `_lock_` or otherwise ensure reentry-safety

    function _pullUnderlying(address erc20, address from, uint amount)
    internal
    {
        safeTransferFrom(erc20, from, address(this), amount);
    }

    function _pushUnderlying(address erc20, address to, uint amount)
    internal
    {
        safeTransfer(erc20, to, amount);
    }

    function _pullPoolShare(address from, uint amount)
    internal
    {
        _pull(from, amount);
    }

    function _pushPoolShare(address to, uint amount)
    internal
    {
        _push(to, amount);
    }

    function _mintPoolShare(uint amount)
    internal
    {
        _mint(amount);
    }

    function _burnPoolShare(uint amount)
    internal
    {
        _burn(amount);
    }

    function _lpChanging(bool add, address user, uint256 amount)
    internal
    {
        if (address(_pair) != address(0))
        {
            add == true ? _pair.addLiquidity(false, user, amount) : _pair.removeLiquidity(false, user, amount);
        }

        (address lpMiningAdr, ) = _factory.getMining();
        if (lpMiningAdr != address(0))
        {
            IMining mining = IMining(lpMiningAdr);
            uint256[] memory balances = new uint256[](_tokens.length);
            uint256[] memory weights = new uint256[](_tokens.length);
            for (uint i = 0; i \u003c _tokens.length; i++) {
                balances[i] = _records[_tokens[i]].balance;
                weights[i] = bdiv(_records[_tokens[i]].denorm, _totalWeight);
            }

            mining.claimLiquidityShares(user, _tokens, balances, weights, amount, add);
        }

    }

    function _swapMining(address user, address tokenIn, address tokenOut, uint256 tokenAmountIn, uint256 tokenAmountOut)
    internal
    {
        ( ,address swapMiningAdr) = _factory.getMining();
        if (swapMiningAdr != address(0)){
            IMining mining = IMining(swapMiningAdr);
            mining.claimSwapShare(user, tokenIn, tokenAmountIn, tokenOut, tokenAmountOut);
        }
    }

    function _transferLiquidity(address src, address dst, uint amt) internal {
        (address lpMiningAdr, ) = _factory.getMining();
        if (lpMiningAdr != address(0)){
            IMining mining = IMining(lpMiningAdr);
            mining.onTransferLiquidity(src, dst, amt);
        }
    }

    function transfer(address dst, uint amt) external returns (bool) {
        require(_factory.isWhiteList(msg.sender) || _factory.isWhiteList(dst), \"ERR_NOT_WHITELIST\");
        _move(msg.sender, dst, amt);
        if(dst != address(this)){
            _transferLiquidity(msg.sender, dst, amt);
        }
        return true;
    }

    function transferFrom(address src, address dst, uint amt) external returns (bool) {
        require(_factory.isWhiteList(msg.sender) || _factory.isWhiteList(dst), \"ERR_NOT_WHITELIST\");
        require(msg.sender == src || amt \u003c= _allowance[src][msg.sender], \"ERR_BTOKEN_BAD_CALLER\");
        _move(src, dst, amt);
        if (msg.sender != src \u0026\u0026 _allowance[src][msg.sender] != uint256(-1)) {
            _allowance[src][msg.sender] = bsub(_allowance[src][msg.sender], amt);
            emit Approval(msg.sender, dst, _allowance[src][msg.sender]);
        }
        if(dst != address(this)){
            _transferLiquidity(src, dst, amt);
        }
        return true;
    }

    function bindPair(
        IPairFactory pairFactory,
        address[] calldata gps,
        uint[] calldata shares,
        uint gpRate
    ) external {
        require(msg.sender == controller, \"ERR_NOT_CONTROLLER\");
        IPairToken pair = pairFactory.newPair(address(this), 4 * 10 ** 18, gpRate);
        _setPair(IMining(address(pair)));
        if (gpRate \u003e 0 \u0026\u0026 gpRate \u003c= 15 \u0026\u0026 gps.length != 0 \u0026\u0026 gps.length == shares.length) {
            _pair.updateGPInfo(gps, shares);
        }
        pair.setController(msg.sender);
    }
}
"},"MToken.sol":{"content":"// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see \u003chttp://www.gnu.org/licenses/\u003e.

pragma solidity 0.5.12;

import \"./MNum.sol\";

// Highly opinionated token implementation

interface IERC20 {
    event Approval(address indexed src, address indexed dst, uint amt);
    event Transfer(address indexed src, address indexed dst, uint amt);

    function totalSupply() external view returns (uint);
    function balanceOf(address whom) external view returns (uint);
    function allowance(address src, address dst) external view returns (uint);

    function approve(address dst, uint amt) external returns (bool);
    function transfer(address dst, uint amt) external returns (bool);
    function transferFrom(address src, address dst, uint amt) external returns (bool);
}

contract MTokenBase is MNum {

    mapping(address =\u003e uint)                   internal _balance;
    mapping(address =\u003e mapping(address=\u003euint)) internal _allowance;
    uint internal _totalSupply;

    event Approval(address indexed src, address indexed dst, uint amt);
    event Transfer(address indexed src, address indexed dst, uint amt);

    function _mint(uint amt) internal {
        _balance[address(this)] = badd(_balance[address(this)], amt);
        _totalSupply = badd(_totalSupply, amt);
        emit Transfer(address(0), address(this), amt);
    }

    function _burn(uint amt) internal {
        require(_balance[address(this)] \u003e= amt, \"ERR_INSUFFICIENT_BAL\");
        _balance[address(this)] = bsub(_balance[address(this)], amt);
        _totalSupply = bsub(_totalSupply, amt);
        emit Transfer(address(this), address(0), amt);
    }

    function _move(address src, address dst, uint amt) internal {
        require(_balance[src] \u003e= amt, \"ERR_INSUFFICIENT_BAL\");
        _balance[src] = bsub(_balance[src], amt);
        _balance[dst] = badd(_balance[dst], amt);
        emit Transfer(src, dst, amt);
    }

    function _push(address to, uint amt) internal {
        _move(address(this), to, amt);
    }

    function _pull(address from, uint amt) internal {
        _move(from, address(this), amt);
    }
}

contract MToken is MTokenBase, IERC20 {

    string  private _name     = \"Mercurity Pool Token\";
    string  private _symbol   = \"MPT\";
    uint8   private _decimals = 18;

    function name() public view returns (string memory) {
        return _name;
    }

    function symbol() public view returns (string memory) {
        return _symbol;
    }

    function decimals() public view returns(uint8) {
        return _decimals;
    }

    function allowance(address src, address dst) external view returns (uint) {
        return _allowance[src][dst];
    }

    function balanceOf(address whom) external view returns (uint) {
        return _balance[whom];
    }

    function totalSupply() public view returns (uint) {
        return _totalSupply;
    }

    function approve(address dst, uint amt) external returns (bool) {
        _allowance[msg.sender][dst] = amt;
        emit Approval(msg.sender, dst, amt);
        return true;
    }

    function increaseApproval(address dst, uint amt) external returns (bool) {
        _allowance[msg.sender][dst] = badd(_allowance[msg.sender][dst], amt);
        emit Approval(msg.sender, dst, _allowance[msg.sender][dst]);
        return true;
    }

    function decreaseApproval(address dst, uint amt) external returns (bool) {
        uint oldValue = _allowance[msg.sender][dst];
        if (amt \u003e oldValue) {
            _allowance[msg.sender][dst] = 0;
        } else {
            _allowance[msg.sender][dst] = bsub(oldValue, amt);
        }
        emit Approval(msg.sender, dst, _allowance[msg.sender][dst]);
        return true;
    }

    function transfer(address dst, uint amt) external returns (bool) {
        _move(msg.sender, dst, amt);
        return true;
    }

    function transferFrom(address src, address dst, uint amt) external returns (bool) {
        require(msg.sender == src || amt \u003c= _allowance[src][msg.sender], \"ERR_BTOKEN_BAD_CALLER\");
        _move(src, dst, amt);
        if (msg.sender != src \u0026\u0026 _allowance[src][msg.sender] != uint256(-1)) {
            _allowance[src][msg.sender] = bsub(_allowance[src][msg.sender], amt);
            emit Approval(msg.sender, dst, _allowance[src][msg.sender]);
        }
        return true;
    }

    function safeTransfer(
        address token,
        address to,
        uint256 value
    ) internal {
        // bytes4(keccak256(bytes(\u0027transfer(address,uint256)\u0027)));
        (bool success, bytes memory data) = token.call(abi.encodeWithSelector(0xa9059cbb, to, value));
        require(
            success \u0026\u0026 (data.length == 0 || abi.decode(data, (bool))),
            \u0027TransferHelper::safeTransfer: transfer failed\u0027
        );
    }

    function safeTransferFrom(
        address token,
        address from,
        address to,
        uint256 value
    ) internal {
        // bytes4(keccak256(bytes(\u0027transferFrom(address,address,uint256)\u0027)));
        (bool success, bytes memory data) = token.call(abi.encodeWithSelector(0x23b872dd, from, to, value));
        require(
            success \u0026\u0026 (data.length == 0 || abi.decode(data, (bool))),
            \u0027TransferHelper::transferFrom: transferFrom failed\u0027
        );
    }
}

